﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BranchCTSDownload
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim TeamID As Integer
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1127) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Download Files"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//

            Dim FilterID As Integer = -1
            Dim FilterText As String = ""
            If Not IsNothing(Request.QueryString.Get("FilterID")) Then
                FilterID = CInt(Request.QueryString.Get("FilterID"))
            End If
            If Not IsNothing(Request.QueryString.Get("FilterText")) Then
                FilterText = Request.QueryString.Get("FilterText")
            End If
            'DT = DB.ExecuteDataSet("select * from (select top 10000 a.REQUEST_ID,a.TICKET_NO,a.REMARKS,c.PROBLEM,d.group_Name,e.Sub_Group_Name,NOOF_ATTACHMENTS,a.status_id,dbo.udfProperCase(br.Branch_Name)as Branch_Name,p.department_name,convert(varchar(19),a.REQUEST_DT)as Req_Dt,ISNULL(case when x.TICKET_TYPE=1 then 'New' else case when x.TICKET_TYPE=2 then 'Repair' else '' end end,'')AssetStatus,d.Group_ID,e.Sub_Group_ID,ISNULL((select e.Emp_Name from (select r.REQUEST_ID,r.USER_ID,r.Update_ID,MAX(r.ORDER_ID) ORDER_ID from HD_REQUEST_CYCLE r where r.Update_ID in(17,18) group by r.REQUEST_ID,r.USER_ID,r.Update_ID) am, EMP_MASTER e where am.USER_ID=e.Emp_Code and am.REQUEST_ID=a.request_id and am.Update_ID=a.STATUS_ID),'') UserName from HD_REQUEST_MASTER a left outer join HD_ASSET_TICKET x on a.REQUEST_ID=x.REQUEST_ID ,HD_TEAM_MASTER b,HD_PROBLEM_TYPE c,HD_GROUP_MASTER d,HD_SUB_GROUP e,BRANCH_MASTER br,DEPARTMENT_MASTER  p where a.TEAM_ID=b.TEAM_ID and a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and e.group_ID=d.group_ID and a.DEPARTMENT_ID=p.Department_ID and a.BRANCH_ID=br.Branch_ID and a.STATUS_ID in(1,17,18) and a.Team_ID=4 order by URGENT_REQ,REQUEST_DT)mm").Tables(0)
            DT = DB.ExecuteDataSet("select  a.REQUEST_ID,dbo.udfProperCase(br.Branch_Name)as Branch_Name ,NOOF_ATTACHMENTS" & _
                " ,convert(varchar(19),a.created_on)as Req_Dt,created_by from CTS_REQUEST_MASTER a ,BRANCH_MASTER br where  a.BRANCH_ID=br.Branch_ID  " & _
                " and convert(varchar,created_on,105)=convert(varchar,getdate(),105)").Tables(0)
            'ToDo//
            hid_Items.Value = ""
            hid_EmpDetails.Value = ""
            Dim i As Integer = 0
            For Each DR As DataRow In DT.Rows
                '                  RequestID 0               TicketNo1                 Branch 2                  Department 3              Request Date 4              Group 5                   
                hid_Items.Value += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString()
                If i <> DT.Rows.Count - 1 Then
                    hid_Items.Value += "¥"
                End If
                i = i + 1
            Next

            Me.ddlFilter.SelectedValue = CStr(FilterID)
            Me.txtFilter.Text = FilterText

            hid_ItemAll.Value = hid_Items.Value
            Me.hid_User.Value = Session("UserID").ToString()
            Me.ddlFilter.Attributes.Add("onchange", "FilterOnChange()")
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        
           

    End Sub
#End Region

End Class
