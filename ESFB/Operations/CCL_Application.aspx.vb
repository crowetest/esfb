﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CCL_Application
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1427) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "COVID Care Loan Application Form"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim BranchID As Integer
            BranchID = CInt(Session("BranchID"))
            DT = DB.ExecuteDataSet("select distinct [branch_ID] , [Branch_Name] + ' | ' + convert(varchar,cast([branch_ID] as int)) from BRANCH_MASTER where [branch_ID]=" + BranchID.ToString() + " order by 2").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
            cmbBranch.SelectedIndex = 0

            DT = DB.ExecuteDataSet("Select  -1 as center_id , '----#Select#-----' AS center_name  UNION  select distinct center_id , center_name + ' ( ' + convert(varchar,cast(center_id as int)) + ' ) '  AS center_name from ccl_master_data where branch_id=" + BranchID.ToString() + " order by 2  ").Tables(0)
            GN.ComboFill(cmbSangam, DT, 0, 1)

            Me.cmbSangam.Attributes.Add("onchange", "return SangamOnChange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)

            cmbSangam.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim CenterID As String = Data(1).ToString()
            DT1 = DB.ExecuteDataSet("select count(*) FROM CCL_MASTER_DATA WHERE CENTER_ID = " + CenterID).Tables(0)
            Dim count As Integer = CInt(DT1.Rows(0)(0).ToString())
            Dim id As Integer = CInt(Math.Ceiling(count / 15))
            DT2 = DB.ExecuteDataSet("SELECT CLIENT_ID,CLIENT_NAME,CCL_ELIGIBLE_AMOUNT FROM CCL_MASTER_DATA WHERE CENTER_ID = " + CenterID.ToString()).Tables(0)
            Dim strClient As String = ""
            For n As Integer = 0 To DT2.Rows.Count - 1
                strClient += DT2.Rows(n)(0).ToString() + "µ" + DT2.Rows(n)(1).ToString() + "µ" + DT2.Rows(n)(2).ToString()
                If n < DT2.Rows.Count - 1 Then
                    strClient += "¥"
                End If
            Next
            CallBackReturn = id.ToString() + "Ø" + CenterID + "Ø" + strClient
        ElseIf CInt(Data(0)) = 2 Then
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim ClientDtl As String = CStr(Data(2))
            Dim SangamID As String = CStr(Data(1))
            Dim UserID As Integer = CInt(Session("UserID"))
            Try
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@ClientID_Dtl", SqlDbType.VarChar, 5000)
                Params(0).Value = ClientDtl
                Params(1) = New SqlParameter("@User_Id", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@Center_ID", SqlDbType.VarChar, 50)
                Params(4).Value = SangamID
                DB.ExecuteNonQuery("SP_CCL_FORM_Generation", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            DT1 = DB.ExecuteDataSet("select count(*) FROM CCL_MASTER_DATA WHERE [print] = 1 and CENTER_ID = " + SangamID).Tables(0)
            Dim count As Integer = CInt(DT1.Rows(0)(0).ToString())
            Dim id As Integer = CInt(Math.Ceiling(count / 15))
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + SangamID + "Ø" + id.ToString()
        End If
    End Sub
#End Region


End Class
