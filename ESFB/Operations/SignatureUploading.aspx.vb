﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class SignatureUploading
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim UserID As String
    Dim TypeId As Integer
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            UserID = Session("UserID").ToString()
            TypeId = CInt(Request.QueryString.[Get]("TypeId"))
            ' if TypeId = 1 create; TypeId =2 modify
            hdnTypeId.Value = TypeId.ToString()
            If TypeId = 1 Then
                Me.Master.subtitle = "Signature Uploading"
                If GN.FormAccess(CInt(Session("UserID")), 1194) = False Then
                    Response.Redirect("~/AccessDenied.aspx", False)
                    Return
                End If
            Else
                Me.Master.subtitle = "Signature Uploading Modify"
                If GN.FormAccess(CInt(Session("UserID")), 1195) = False Then
                    Response.Redirect("~/AccessDenied.aspx", False)
                    Return
                End If
            End If
            If Not IsPostBack Then

                '--//---------- Script Registrations -----------//--
                '/--- For Call Back ---//
                Dim DTT As New DataTable
                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

                txtEmpCode.Attributes.Add("onblur", "return EmpCodeOnBlur()")
                Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
                txtEmpName.Attributes.Add("readonly", "readonly")
                txtBranchCode.Attributes.Add("readonly", "readonly")
                txtBranchName.Attributes.Add("readonly", "readonly")
                txtDepartment.Attributes.Add("readonly", "readonly")
                txtDesignation.Attributes.Add("readonly", "readonly")
                txtCadre.Attributes.Add("readonly", "readonly")
                txtDOJ.Attributes.Add("readonly", "readonly")

                txtEmpCode.Focus()
            End If

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EmpCode As String
        If CInt(Data(0)) = 1 Then
            EmpCode = CStr(Data(1))
            DT = DB.ExecuteDataSet("select A.emp_code,emp_name,a.branch_id, B.branch_name, C.Department_name,D.Designation_name,E.Cadre_name,Date_of_join,F.sign_id, isnull(F.Checker_id,0) from emp_master A " +
                " inner join Branch_master B on A.branch_id = B.Branch_id  " +
                " inner join Department_master C on A.Department_id = C.Department_id " +
                " inner join Designation_master D on A.Designation_id = D.Designation_id " +
                " inner join Cadre_master E on A.Cadre_id = E.Cadre_id " +
                " left join Sign_master F on A.emp_code = F.Emp_code  and isnull(f.checker1_status,0) not  in (2) and isnull(f.checker_status,0) not  in (2) " +
                " where (A.emp_code = " + EmpCode + " )").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString + "|" + DT.Rows(0)(1).ToString + "|" + DT.Rows(0)(2).ToString + "|" + DT.Rows(0)(3).ToString + "|" + DT.Rows(0)(4).ToString + "|" + DT.Rows(0)(5).ToString + "|" + DT.Rows(0)(6).ToString + "|" + CDate(DT.Rows(0)(7)).ToString("dd-MMM-yyyy") + "|" + DT.Rows(0)(8).ToString + "|" + DT.Rows(0)(9).ToString
            End If
        ElseIf (CInt(Data(0)) = 2 Or CInt(Data(0)) = 3) Then
            Dim StrWhere As String = ""
            If CInt(Data(0)) = 2 Then
                StrWhere = " F.Maker_id = " + UserID.ToString
            ElseIf CInt(Data(0)) = 3 Then
                StrWhere = " F.Sign_Id = " + CInt(Data(1)).ToString + " And F.Maker_id = " + UserID.ToString
            End If
            DT = DB.ExecuteDataSet("select A.emp_code,emp_name,a.branch_id, B.branch_name, C.Department_name,D.Designation_name,E.Cadre_name,Date_of_join,F.POA_No,F.sign_id from emp_master A " +
                " inner join Branch_master B on A.branch_id = B.Branch_id  " +
                " inner join Department_master C on A.Department_id = C.Department_id " +
                " inner join Designation_master D on A.Designation_id = D.Designation_id " +
                " inner join Cadre_master E on A.Cadre_id = E.Cadre_id " +
                " inner join Sign_master F on A.emp_code = F.Emp_code  and isnull(f.checker1_status,0)  in (0) " +
                " where (" + StrWhere + " )").Tables(0)
            If (DT.Rows.Count > 0) Then
                If CInt(Data(0)) = 2 Then
                    For n As Integer = 0 To DT.Rows.Count - 1
                        CallBackReturn += "¥" + DT.Rows(n)(0).ToString + "µ" + DT.Rows(n)(1).ToString + "µ" + DT.Rows(n)(2).ToString + "µ" + DT.Rows(n)(3).ToString + "µ" + DT.Rows(n)(8).ToString + "µ" + DT.Rows(n)(9).ToString
                    Next
                ElseIf CInt(Data(0)) = 3 Then
                    CallBackReturn += DT.Rows(0)(0).ToString + "|" + DT.Rows(0)(1).ToString + "|" + DT.Rows(0)(2).ToString + "|" + DT.Rows(0)(3).ToString + "|" + DT.Rows(0)(4).ToString + "|" + DT.Rows(0)(5).ToString + "|" + DT.Rows(0)(6).ToString + "|" + CDate(DT.Rows(0)(7)).ToString("dd-MMM-yyyy") + "|" + DT.Rows(0)(8).ToString + "|" + DT.Rows(0)(9).ToString
                    DT = DB.ExecuteDataSet("SELECT ATTACHMENT,content_Type,isnull(case when Attach_file_name='' then 'Attachment' else Attach_file_name end,'Attachment') FileName FROM DMS_ESFB.dbo.SIGN_UPLOAD where Sign_Id = " + CInt(Data(1)).ToString).Tables(0)
                    If (DT.Rows.Count > 0) Then
                        Dim bytes As Byte() = DirectCast(DT.Rows(0)(0), Byte())
                        Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                        CallBackReturn += "|" + Convert.ToString("data:" + DT.Rows(0)(1).ToString + ";base64,") & base64String + "|" + DT.Rows(0)(2).ToString

                    End If
                End If


            End If
        ElseIf CInt(Data(0)) = 4 Then
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim Params(3) As SqlParameter

            Params(0) = New SqlParameter("@SIGN_ID", SqlDbType.Int)
            Params(0).Value = CInt(Data(1))
            Params(1) = New SqlParameter("@USERID", SqlDbType.VarChar, 500)
            Params(1).Value = UserID
            Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(2).Direction = ParameterDirection.Output
            Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(3).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_SIGN_UPLOAD_DELETE", Params)
            ErrorFlag = CInt(Params(2).Value)
            Message = CStr(Params(3).Value)
            CallBackReturn = ErrorFlag.ToString + "|" + Message
            If ErrorFlag = 0 Then
                initializeControls()
            End If
        End If
    End Sub
#End Region
    Private Sub initializeControls()
        txtEmpCode.Text = ""
        txtEmpName.Text = ""
        txtBranchCode.Text = ""
        txtBranchName.Text = ""
        txtDepartment.Text = ""
        txtDesignation.Text = ""
        txtCadre.Text = ""
        txtDOJ.Text = ""
        txtPOA.Text = ""
        If fup1.Value <> "" Then
            fup1.Value = ""
        End If
        txtEmpCode.Focus()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ContentType As String = ""
            Dim AttachSign As Byte() = Nothing

            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim EMPCODE As Integer = CInt(Data(1))
            Dim POA As String = CStr(Data(2))
            Dim ImgSrc As String = CStr(Data(3))
            Dim SignID As Integer = CInt(IIf(Data(4).ToString() = "", 0, Data(4)))
            Dim UserID As String = Session("UserID").ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim NoofAttachments As Integer = 0
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim myFile As HttpPostedFile = hfc(0)
            Dim nFileLen As Integer = myFile.ContentLength
            Dim FileName As String = ""
            TypeId = CInt(hdnTypeId.Value)
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachSign = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachSign, 0, nFileLen)
            Else
                If TypeId = 1 Or (TypeId = 2 And ImgSrc = "") Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('Please attach the signature to upload');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
            End If
            Try
                Dim Params(8) As SqlParameter
                Params(0) = New SqlParameter("@SIGN_ID", SqlDbType.Int)
                Params(0).Value = SignID
                Params(1) = New SqlParameter("@EMP_CODE", SqlDbType.Int)
                Params(1).Value = EMPCODE
                Params(2) = New SqlParameter("@POA", SqlDbType.VarChar, 15)
                Params(2).Value = POA
                Params(3) = New SqlParameter("@ATTACHSIGN", SqlDbType.VarBinary)
                Params(3).Value = AttachSign
                Params(4) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                Params(4).Value = ContentType
                Params(5) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                Params(5).Value = FileName
                Params(6) = New SqlParameter("@USERID", SqlDbType.VarChar, 500)
                Params(6).Value = UserID
                Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(8).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_SIGN_UPLOAD", Params)
                ErrorFlag = CInt(Params(7).Value)
                Message = CStr(Params(8).Value)

            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
            End Try

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)

            If ErrorFlag = 0 Then
                initializeControls()
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub

End Class
