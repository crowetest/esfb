﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CheckListRegistration.aspx.vb" Inherits="CheckListRegistration" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;
          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);
           
           
        }
        
        .Button:hover
        {
            
           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            
            color:#801424;
        }   
                 
     
     .bg
     {
         background-color:#FFF;
     }
           
    </style>
    
    <script language="javascript" type="text/javascript">
        var HisId = 0;
        function IntialCtrl(){
            document.getElementById('btnFinish').disabled = true; 
 //           if(document.getElementById("<%= hid_editstart.ClientID %>").value != 1 )
//            alert("Saved Successfully");
            document.getElementById("<%= hid_Id.ClientID %>").value = 0;
            document.getElementById("<%= hid_Edit.ClientID %>").value = 0;
            document.getElementById("cmbfreq").style.display = 'none';
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1) {
                CategoryClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2) {
                ModuleClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3) {
                ProblemClick();
            }
                             
        }
        function FromServer(arg, context) {
           if (context == 2) 
           {
                var Data = arg.split("~");
                document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                
                if (document.getElementById("<%= hid_dtls.ClientID %>").value !="") 
                {
                    table_fill();
                    document.getElementById("<%= txtCateName.ClientID %>").focus();
                }
                else
                {
                    document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
                    document.getElementById("<%= txtCateName.ClientID %>").focus();
                }               
            }
            else if (context==3) 
            {
                ComboFill(arg, "<%= cmbCategory.ClientID %>");
                if(document.getElementById("<%= hid_editstart.ClientID %>").value != 1)
                {
                document.getElementById("<%= txtModuleName.ClientID %>").value=""
                document.getElementById("<%= txtRemarks.ClientID %>").value=""
                document.getElementById("<%= txtWeightage.ClientID %>").value=""
                document.getElementById("<%= drpFreq.ClientID %>").value="-1"
                }
                document.getElementById("<%= cmbCategory.ClientID %>").focus();
                if  (document.getElementById("<%= hid_Cate.ClientID %>").value != "")  
                {
                    document.getElementById("<%= cmbCategory.ClientID %>").value=document.getElementById("<%= hid_Cate.ClientID %>").value;
                    document.getElementById("<%= hid_Cate.ClientID %>").value="";
                    CategoryOnChange();
                }            
            }
            else if (context==4) 
            {                
                var Data = arg.split("~");
                    document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                    table_fill();
                    if(document.getElementById("<%= hid_editstart.ClientID %>").value == 1)
                    {
                        document.getElementById("<%= hid_editstart.ClientID %>").value = 0;
                    }  
            }
            else if (context==5) 
            {
               

                if  (document.getElementById("<%= hid_SubCate.ClientID %>").value != "")  
                {
                  
                    document.getElementById("<%= hid_SubCate.ClientID %>").value="";
                    ModuleOnChange();
                }
            }
            else if (context==6) 
            {                
                var Data = arg.split("~");
                if (Data[0] == 1) {
                    document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                    table_fill();
                   
                    
                    document.getElementById("ViewAtur").style.display = 'none';
                }
                else
                    document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
            }
            else if (context==7) 
            {
                var Data = arg.split("~");                               
                if (Data[0] == 1) 
                {
                    if  (document.getElementById("<%= hid_Tab.ClientID %>").value==1)
                    {                        
                        CategoryFill();
                        document.getElementById("<%= txtCateName.ClientID %>").focus();
                    }                                    
                    else if (document.getElementById("<%= hid_Tab.ClientID %>").value==2) 
                    {
                        var ToData = "4Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbCategory.ClientID  %>").value;                         
                        ToServer(ToData, 4)
                        document.getElementById("<%= txtModuleName.ClientID %>").focus();
                    }
                    else if (document.getElementById("<%= hid_Tab.ClientID %>").value=3) 
                    {
                        var ToData = "6Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
                        ToServer(ToData, 6)
                    }
                    alert(Data[1]);
                }
                if (Data[0] == 2)
                alert(Data[1]); 
            }
            // 'shima 17/10
            else if (context==11)
            {    
                var Data = arg.split("~");
                alert(Data[1]);
                IntialCtrl();            
            }
             else if (context==12)
            {    
                var Data = arg.split("~");
                alert(Data[1]);
                IntialCtrl();            
            }
            else if (context==9)
            {  
                var Data = arg.split("~");
                if(Data[0] == 1)
                {
                    alert(Data[1]);
                    document.getElementById("<%= txtWeightage.ClientID %>").value = 0
                    document.getElementById("btnSave").disabled = true;              
                }
                if(Data[0] == 2)
                {
                      if(Data[1] != "")
                      {
                        alert(Data[1]);
                        document.getElementById("<%= txtWeightage.ClientID %>").value = 0
                        document.getElementById('btnSave').disabled = true;   
                      }
                      else
                      {
                       document.getElementById('btnSave').disabled = false;   
                      }
                }
                if(Data[0] == 3)
                {
                      if(Data[1] != "")
                      {
                        alert(Data[1]);
                        document.getElementById('btnSave').disabled = false;   
                      }
                      else
                      {
                        document.getElementById('btnSave').disabled = false;   
                      }
                }
            }
            else if (context==10)
            {
                  var Data = arg.split("~");
                  if(Data[0] == 1)
                  {
                    document.getElementById("<%= hid_Edit.ClientID %>").value = 0;
                    alert(Data[1]);
                    IntialCtrl();
                  }
                  else
                  {
                    document.getElementById("<%= hid_Edit.ClientID %>").value = 1;
                    document.getElementById("<%= hid_error.ClientID %>").value = 999;
                    IntialCtrl();
                    alert(Data[1]);
                  }          
            }
        }
        function CategoryFill() {
            var ToData = "2Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
            ToServer(ToData, 2)
        }
        function FillCategoryCombo(){
            
            var ToData = "3Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
            ToServer(ToData, 3);
        }
        function CategoryOnChange() 
        {    
            if(document.getElementById("<%= hid_editstart.ClientID %>").value != 1)
            {
            document.getElementById("<%= hid_Edit.ClientID %>").value = 0;
            document.getElementById("<%= txtRemarks.ClientID %>").value = "";
            document.getElementById("<%= txtWeightage.ClientID %>").value = "";
            document.getElementById("<%= txtModuleName.ClientID %>").value = "";  
            document.getElementById("<%= drptasksts.ClientID %>").value = 111;
            }      
            if  (document.getElementById("<%= hid_Tab.ClientID %>").value == 2)
            {
                var ToData = "4Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbCategory.ClientID  %>").value + "Ø" + document.getElementById("<%= txtRemarks.ClientID  %>").value + + "Ø" + document.getElementById("<%= txtWeightage.ClientID  %>").value + "Ø" + document.getElementById("<%= drpFreq.ClientID  %>").value;
                ToServer(ToData, 4)
            }
        }
        function CategoryFreqOnChange()
        {
            var ToData = "9Ø" + document.getElementById("<%= cmbCategory.ClientID %>").value;
            ToServer(ToData, 9)
        }
        function FrequencyOnChange()
        {            
            if  (document.getElementById("<%= drpcheck.ClientID %>").value == 1)
            {
                document.getElementById("<%= txtinterval.ClientID %>").value = 0;
                document.getElementById("<%= drpFreq.ClientID %>").value = 1;
            }
            if  (document.getElementById("<%= drpcheck.ClientID %>").value == 2)
            {
                document.getElementById("<%= txtinterval.ClientID %>").value = 6;
                document.getElementById("<%= drpFreq.ClientID %>").value = 2;
            }
            if  (document.getElementById("<%= drpcheck.ClientID %>").value == 3)
            {
                document.getElementById("<%= txtinterval.ClientID %>").value = 13;
                document.getElementById("<%= drpFreq.ClientID %>").value = 3;
            }
            if  (document.getElementById("<%= drpcheck.ClientID %>").value == 4)
            {
                document.getElementById("<%= txtinterval.ClientID %>").value = 29;
                document.getElementById("<%= drpFreq.ClientID %>").value = 4;
            }
            if  (document.getElementById("<%= drpcheck.ClientID %>").value == 5)
            {
                document.getElementById("<%= txtinterval.ClientID %>").value = 89;
                document.getElementById("<%= drpFreq.ClientID %>").value = 5;
            }
            if  (document.getElementById("<%= drpcheck.ClientID %>").value == 6)
            {
                document.getElementById("<%= txtinterval.ClientID %>").value = 179;
                document.getElementById("<%= drpFreq.ClientID %>").value = 6;
            }
            if  (document.getElementById("<%= drpcheck.ClientID %>").value == 7)
            {
                document.getElementById("<%= txtinterval.ClientID %>").value = 364;
                document.getElementById("<%= drpFreq.ClientID %>").value = 7;
            }
        }
        function ModuleOnChange() 
        {
                ToServer(ToData, 6)
                document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
        }  
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function CategoryClick() {
            document.getElementById('btnSave').disabled = false;
            document.getElementById('btnFinish').disabled = true; 
            document.getElementById("trstatus").style.display = 'none';
            document.getElementById("trdrptasksts").style.display = 'none';
            document.getElementById("CategoryName").style.display = '';
            document.getElementById("applieddate").style.display = '';
            document.getElementById("trIntervel").style.display = '';
            document.getElementById("checkRemarks").style.display = '';
            document.getElementById("drpcheckdata").style.display = '';
            document.getElementById("Category").style.display = 'none';
            document.getElementById("ModuleName").style.display = 'none';
            document.getElementById("Remarks").style.display = 'none';
            document.getElementById("Weightage").style.display = 'none';
            document.getElementById("RemarkMandatory").style.display = 'none';
            document.getElementById("cmbfreq").style.display = 'none';
            document.getElementById("<%= hid_tab.ClientID %>").value = 1;
            document.getElementById("<%= hid_Id.ClientID %>").value = 0;
            document.getElementById("<%= hid_Edit.ClientID %>").value = 0;
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
            document.getElementById("1").style.background = '-moz-radial-gradient(center, ellipse cover,  #EEB8A6 0%, #801424 90%, #EEB8A6 100%)';
            document.getElementById("1").style.color = '#CF0D0D';
            document.getElementById("2").style.background = '-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("2").style.color = '#CF0D0D';
            document.getElementById("<%= txtCateName.ClientID %>").value = "";
            document.getElementById("<%= txtcheckremarks.ClientID %>").value = "";
            document.getElementById("<%= drpcheck.ClientID %>").value = "";
            document.getElementById("<%= cmbCategory.ClientID %>").value = "";
            document.getElementById("<%= txtModuleName.ClientID %>").value = "";
            document.getElementById("<%= txtRemarks.ClientID %>").value = "";
            document.getElementById("<%= txtWeightage.ClientID %>").value = "";
            document.getElementById("<%= drpFreq.ClientID %>").value = "-1";
            document.getElementById("<%= txtFromDt.ClientID %>").value = "";
            document.getElementById("<%= txtinterval.ClientID %>").value = "";
            CategoryFill();                  
            document.getElementById("<%= txtCateName.ClientID %>").focus();
        }
        function ModuleClick() {
            document.getElementById('btnSave').disabled = false;
            document.getElementById('btnFinish').disabled = false;
            document.getElementById("trstatus").style.display = 'none';
            document.getElementById("trdrptasksts").style.display = 'none';
            document.getElementById("CategoryName").style.display = 'none';
            document.getElementById("applieddate").style.display = 'none';
            document.getElementById("trIntervel").style.display = 'none';
            document.getElementById("checkRemarks").style.display = 'none';
            document.getElementById("drpcheckdata").style.display = 'none';
            document.getElementById("Category").style.display = '';
            document.getElementById("ModuleName").style.display = '';
            document.getElementById("Remarks").style.display = '';
            document.getElementById("Weightage").style.display = '';
            document.getElementById("RemarkMandatory").style.display = '';
            document.getElementById("cmbfreq").style.display = 'none';
            document.getElementById("<%= hid_tab.ClientID %>").value = 2;
            document.getElementById("<%= hid_Id.ClientID %>").value = 0;
            document.getElementById("<%= hid_Edit.ClientID %>").value = 0;
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';            

            document.getElementById("1").style.background =  '-moz-radial-gradient(center, ellipse cover,#F1F3F4 0%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("1").style.color = '#CF0D0D';
            document.getElementById("2").style.background =  '-moz-radial-gradient(center, ellipse cover,  #EEB8A6 20%, #801424 90%, #EEB8A6 100%)';
            document.getElementById("2").style.color = '#CF0D0D';
          
            document.getElementById("<%= txtCateName.ClientID %>").value = "";
            document.getElementById("<%= txtcheckremarks.ClientID %>").value = "";
            document.getElementById("<%= drpcheck.ClientID %>").value = "";
            document.getElementById("<%= cmbCategory.ClientID %>").value = "";
            if(document.getElementById("<%= hid_editstart.ClientID %>").value != 1)
            {
                document.getElementById("<%= txtModuleName.ClientID %>").value = "";
                document.getElementById("<%= txtRemarks.ClientID %>").value = "";
                document.getElementById("<%= txtWeightage.ClientID %>").value = "";
            }
            document.getElementById("<%= drpFreq.ClientID %>").value = -1;           
            FillCategoryCombo();
            document.getElementById("<%= cmbCategory.ClientID %>").focus();            
        }
       
        function Saveonclick()         
        {     
            document.getElementById("<%= hid_Save.ClientID %>").value="";
            document.getElementById("<%= hid_Cate.ClientID %>").value="";
//            document.getElementById("<%= hid_Edit.ClientID %>").value = 0;
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1)
            {
                if (document.getElementById("<%= txtCateName.ClientID %>").value == "")
                {
                    alert("Enter Checklist");
                    document.getElementById("<%= txtCateName.ClientID %>").focus();
                    return false;
                }
//                if (document.getElementById("<%= txtcheckremarks.ClientID %>").value == "")
//                {
//                    alert("Enter Remarks");
//                    document.getElementById("<%= txtcheckremarks.ClientID %>").focus();
//                    return false;
//                }
                if (document.getElementById("<%= drpcheck.ClientID %>").value == "-1")
                {
                    alert("Select Frequency");
                    document.getElementById("<%= drpcheck.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= txtinterval.ClientID %>").value == "")
                {
                    alert("Enter interval");
                    document.getElementById("<%= txtinterval.ClientID  %>").focus();
                    return false;
                }
                if(document.getElementById("<%= drpcheck.ClientID %>").value == 1)
                    {
                            if (document.getElementById("<%= txtinterval.ClientID %>").value > 0)
                            {
                                    alert('For daily checklist maximum interval days is zero');
                            document.getElementById("<%= txtinterval.ClientID  %>").focus();
                        return false;
                            }
                    }
                else if (document.getElementById("<%= drpcheck.ClientID %>").value == 2)
                    {
                            if (document.getElementById("<%= txtinterval.ClientID %>").value > 6)
                            {
                                    alert('For weekly checklist maximum interval days is 6');
                        document.getElementById("<%= txtinterval.ClientID  %>").focus();
                        return false;
                            }
                    }
                    else if (document.getElementById("<%= drpcheck.ClientID %>").value == 3)
                    {
                            if (document.getElementById("<%= txtinterval.ClientID %>").value > 13)
                            {
                                    alert('For fortnightly checklist maximum interval days is 13');
                        document.getElementById("<%= txtinterval.ClientID  %>").focus();
                        return false;
                            }
                    }
                    else if (document.getElementById("<%= drpcheck.ClientID %>").value == 4)
                    {
                            if (document.getElementById("<%= txtinterval.ClientID %>").value > 29)
                            {
                                    alert('For monthly checklist maximum interval days is 29');
                        document.getElementById("<%= txtinterval.ClientID  %>").focus();
                        return false;
                            }
                    }
                    else if (document.getElementById("<%= drpcheck.ClientID %>").value == 5)
                    {
                            if (document.getElementById("<%= txtinterval.ClientID %>").value > 89)
                            {
                                    alert('For quarterly checklist maximum interval days is 89');
                        document.getElementById("<%= txtinterval.ClientID  %>").focus();
                        return false;
                            }
                    }
                    else if (document.getElementById("<%= drpcheck.ClientID %>").value == 6)
                    {
                            if (document.getElementById("<%= txtinterval.ClientID %>").value > 179)
                            {
                                    alert('For halfyearly checklist maximum interval days is 179');
                        document.getElementById("<%= txtinterval.ClientID  %>").focus();
                        return false;
                            }
                    }
                    else if (document.getElementById("<%= drpcheck.ClientID %>").value == 7)
                    {
                            if (document.getElementById("<%= txtinterval.ClientID %>").value > 364)
                            {
                                    alert('For yearly checklist maximum interval days is 364');
                        document.getElementById("<%= txtinterval.ClientID  %>").focus();
                        return false;
                            }
                    }
                if (document.getElementById("<%= hid_Edit.ClientID %>").value==0)
                    var ToData = "11Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= txtCateName.ClientID %>").value + "Ø0" + "Ø" + document.getElementById("<%= txtcheckremarks.ClientID %>").value +  "Ø" + document.getElementById("<%= drpcheck.ClientID %>").value +  "Ø" + document.getElementById("<%= txtFromDt.ClientID %>").value +  "Ø" + document.getElementById("<%= txtinterval.ClientID %>").value;
                else
                    var ToData = "11Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= txtCateName.ClientID %>").value + "Ø" + document.getElementById("<%= hid_Id.ClientID %>").value + "Ø" + document.getElementById("<%= txtcheckremarks.ClientID %>").value + "Ø" + document.getElementById("<%= drpcheck.ClientID %>").value + "Ø" + document.getElementById("<%= drpStatus.ClientID %>").value + "Ø" + HisId +  "Ø" + document.getElementById("<%= txtFromDt.ClientID %>").value +  "Ø" + document.getElementById("<%= txtinterval.ClientID %>").value;               
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2)
            {
                if (document.getElementById("<%= hid_Edit.ClientID %>").value=="0")
                {
                    if (document.getElementById("<%= cmbCategory.ClientID %>").value =="-1") 
                    {
                        alert("Select Checklist");
                        document.getElementById("<%= cmbCategory.ClientID %>").focus();
                        return false;
                    }
                    if (document.getElementById("<%= txtModuleName.ClientID %>").value == "") 
                    {
                        alert("Enter task");
                        document.getElementById("<%= txtModuleName.ClientID %>").focus();
                        return false;
                    }
//                    if (document.getElementById("<%= txtRemarks.ClientID %>").value == "") 
//                    {
//                        alert("Enter Remarks");
//                        document.getElementById("<%= txtRemarks.ClientID %>").focus();
//                        return false;
//                    }
                    if (document.getElementById("<%= txtWeightage.ClientID %>").value == "") 
                    {
                        alert("Enter Weightage");
                        document.getElementById("<%= txtWeightage.ClientID %>").focus();
                        return false;
                    }
                }
                else
                {
                    if (document.getElementById("<%= cmbCategory.ClientID %>").value =="-1") 
                    {
                        alert("Select checklist");
                        document.getElementById("<%= cmbCategory.ClientID %>").focus();
                        return false;
                    }
                    if (document.getElementById("<%= hidedit_taskname.ClientID %>").value == "") 
                    {
                        alert("Enter task");
                        document.getElementById("<%= hidedit_taskname.ClientID %>").focus();
                        return false;
                    }
//                    if (document.getElementById("<%= hidedit_remarks.ClientID %>").value == "") 
//                    {
//                        alert("Enter Remarks");
//                        document.getElementById("<%= hidedit_remarks.ClientID %>").focus();
//                        return false;
//                    }
                    if (document.getElementById("<%= hidedit_weightage.ClientID %>").value == "") 
                    {
                        alert("Enter Weightage");
                        document.getElementById("<%= hidedit_weightage.ClientID %>").focus();
                        return false;
                    }
                }
                if (document.getElementById("<%= hid_Edit.ClientID %>").value=="1")
                {
                    var ToData = "11Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" +document.getElementById("<%= cmbCategory.ClientID %>").value+ "Ø" +document.getElementById("<%= hidedit_taskname.ClientID %>").value + "Ø0" + "Ø" + document.getElementById("<%= hidedit_remarks.ClientID %>").value + "Ø" + document.getElementById("<%= hidedit_weightage.ClientID %>").value + "Ø" + document.getElementById("<%= drpFreq.ClientID %>").value + "Ø" + document.getElementById("<%= hidMandate_remark.ClientID %>").value;                    
                }
                else 
                {    
                   if (document.getElementById("<%= txtRemarkMand.ClientID %>").checked==true)
                        var chkremark=1;
                   else
                        var chkremark=0;  
                                   
                    var ToData = "11Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" +document.getElementById("<%= cmbCategory.ClientID %>").value+ "Ø" +document.getElementById("<%= txtModuleName.ClientID %>").value + "Ø0" + "Ø" + document.getElementById("<%= txtRemarks.ClientID %>").value + "Ø" + document.getElementById("<%= txtWeightage.ClientID %>").value + "Ø" + document.getElementById("<%= drpFreq.ClientID %>").value + "Ø" + chkremark;                    
                }
                document.getElementById("<%= hid_Cate.ClientID %>").value=document.getElementById("<%= cmbCategory.ClientID %>").value;                    
            }
            document.getElementById("<%= hid_Save.ClientID %>").value =ToData;
            ToServer(ToData, 11);
        }
        function Finishonclick()
        {
            if (document.getElementById("<%= cmbCategory.ClientID %>").value ==-1) 
            {
                alert("Select checklist");
                document.getElementById("<%= cmbCategory.ClientID %>").focus();
                return false;
            }
//            if (document.getElementById("<%= txtModuleName.ClientID %>").value == "") 
//            {
//                alert("Enter Module");
//                document.getElementById("<%= txtModuleName.ClientID %>").focus();
//                return false;
//            }
//            if (document.getElementById("<%= txtRemarks.ClientID %>").value == "") 
//            {
//                alert("Enter Remarks");
//                document.getElementById("<%= txtRemarks.ClientID %>").focus();
//                return false;
//            }
//            if (document.getElementById("<%= txtWeightage.ClientID %>").value == "") 
//            {
//                alert("Enter Weightage");
//                document.getElementById("<%= txtWeightage.ClientID %>").focus();
//                return false;
//            }
                document.getElementById("<%= hid_Cate.ClientID %>").value=document.getElementById("<%= cmbCategory.ClientID %>").value;  
                var ToData = "12Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" +document.getElementById("<%= cmbCategory.ClientID %>").value;
                document.getElementById("<%= hid_Save.ClientID %>").value =ToData;
                ToServer(ToData, 12)
        }
        function EditSaveonclick()         
        {      
            document.getElementById("<%= hid_Save.ClientID %>").value="";
            document.getElementById("<%= hid_Cate.ClientID %>").value="";
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2)
            {
                if (document.getElementById("<%= cmbCategory.ClientID %>").value =="-1") 
                {
                    alert("Select checklist");
                    document.getElementById("<%= cmbCategory.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= hidedit_taskname.ClientID %>").value == "") 
                {
                    alert("Enter task");
                    document.getElementById("<%= hidedit_taskname.ClientID %>").focus();
                    return false;
                }
//                if (document.getElementById("<%= hidedit_remarks.ClientID %>").value == "") 
//                {
//                    alert("Enter Remarks");
//                    document.getElementById("<%= hidedit_remarks.ClientID %>").focus();
//                    return false;
//                }
                if (document.getElementById("<%= hidedit_weightage.ClientID %>").value == "") 
                {
                    alert("Enter Weightage");
                    document.getElementById("<%= hidedit_weightage.ClientID %>").focus();
                    return false;
                }
                
                var ToData = "10Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" +document.getElementById("<%= cmbCategory.ClientID %>").value+ "Ø" +document.getElementById("<%= hidedit_taskname.ClientID %>").value + "Ø" + document.getElementById("<%= hid_Id.ClientID %>").value + "Ø" + document.getElementById("<%= hidedit_remarks.ClientID %>").value + "Ø" + document.getElementById("<%= hidedit_weightage.ClientID %>").value + "Ø" + document.getElementById("<%= drpFreq.ClientID %>").value + "Ø" +document.getElementById("<%= hidedie_status.ClientID %>").value + "Ø" + document.getElementById("<%= hidMandate_remark.ClientID %>").value;             
                document.getElementById("<%= hid_Cate.ClientID %>").value=document.getElementById("<%= cmbCategory.ClientID %>").value;                    
            }
            document.getElementById("<%= hid_Save.ClientID %>").value =ToData;
            ToServer(ToData, 10); 
        }
        function delete_row(GroupId)
        {    
            if(document.getElementById("<%= hid_editstart.ClientID %>").value != 1)
            {    
                if (confirm("Are You sure to Delete?")==1)
                {                
                    var ToData = "7Ø"+document.getElementById("<%= hid_Tab.ClientID %>").value+"Ø"+ GroupId;
                    ToServer(ToData, 7);
                }
            }
            else
            {
                document.getElementById("<%= hid_editstart.ClientID %>").value = 0;
                document.getElementById("<%= hid_Edit.ClientID %>").value = 0; 
                table_fill();
            }
        }
        function edit_row(GroupID,GroupName)
        {
            document.getElementById("<%= hid_editstart.ClientID %>").value = 1; 
            document.getElementById("<%= hid_Edit.ClientID %>").value = 1; 
            document.getElementById("<%= hid_Id.ClientID %>").value=GroupID;
            if (document.getElementById("<%= hid_Tab.ClientID %>").value==2)
            {
                table_fill();
            }
 
            col = GroupName.split("¥");  
            if (document.getElementById("<%= hid_Tab.ClientID %>").value==1)
            {
                document.getElementById("trstatus").style.display = '';
                document.getElementById("<%= txtCateName.ClientID %>").value=col[0].replace(/[Ø]/g," " );
                document.getElementById("<%= txtcheckremarks.ClientID %>").value=col[1].replace(/[Ø]/g," " );
                document.getElementById("<%= drpcheck.ClientID %>").value=col[3];
                document.getElementById("<%= drpStatus.ClientID %>").value=col[4];
                HisId = col[5];
                document.getElementById("<%= txtFromDt.ClientID %>").value=col[6];
                document.getElementById("<%= txtinterval.ClientID %>").value=col[7];
                document.getElementById("<%= txtCateName.ClientID %>").focus();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value==2)
            {
                document.getElementById("trdrptasksts").style.display = 'none';
                document.getElementById('btnSave').disabled = true; 
//                document.getElementById("<%= txtModuleName.ClientID %>").value=col[0].replace(/[Ø]/g," " );
//                document.getElementById("<%= txtRemarks.ClientID %>").value=col[1].replace(/[Ø]/g," " );
//                document.getElementById("<%= txtWeightage.ClientID %>").value=col[2];
                document.getElementById("<%= hid_edweightage.ClientID %>").value = col[2];
                document.getElementById("chkRemark").value = col[4];
//                alert(document.getElementById("<%= hidMandate_remark.ClientID %>").value);
////                document.getElementById("<%= drpFreq.ClientID %>").value=col[3];
//                document.getElementById("<%= drptasksts.ClientID %>").value=col[4];
//                document.getElementById("<%= txtModuleName.ClientID %>").focus();                
            }
           
        }
         function save_row(GroupID,GroupName)
        {
         
            document.getElementById("<%= hid_Edit.ClientID %>").value = 1;
            document.getElementById("<%= hid_Id.ClientID %>").value=GroupID;
            col = GroupName.split("¥");  
            if (document.getElementById("<%= hid_Tab.ClientID %>").value==2)
            {
              
                document.getElementById("trdrptasksts").style.display = 'none';
                var taskname=document.getElementById("txttask"+GroupID).value;           
                var remark=document.getElementById("txtRemark"+GroupID).value;            
                var weightage=document.getElementById("txtweightage"+GroupID).value;                 
                var status=document.getElementById("cmbstatus"+GroupID).value;
                var chkRemark=document.getElementById("chkRemark"+GroupID).checked;
                if (chkRemark==true)
                  chkRemark=1;
                else
                   chkRemark=0; 
                
                document.getElementById("<%= hidedit_taskname.ClientID %>").value=taskname;
                document.getElementById("<%= hidedit_remarks.ClientID %>").value=remark;
                document.getElementById("<%= hidedit_weightage.ClientID %>").value=weightage;
                document.getElementById("<%= hid_edweightage.ClientID %>").value = weightage;
                document.getElementById("<%= hidMandate_remark.ClientID %>").value=chkRemark;
                document.getElementById("<%= drpFreq.ClientID %>").value=col[3];
                document.getElementById("<%= hidedie_status.ClientID %>").value=status;
                document.getElementById("<%= txtModuleName.ClientID %>").focus();               
            }
            EditSaveonclick(); 
        }
        function ViewAttachment() 
        {
            var ProblemID = document.getElementById("<%= hid_Id.ClientID %>").value;
            if (ProblemID > 0)
                window.open("ShowFormat.aspx?ProblemID=" + btoa(ProblemID) + "");
            return false;
        }
        function table_fill() {

            document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
            
            var row_bg = 0;
            var tab = "";
            tab += "<br/><div class=mainhead style='width:100%; height:auto; overflow:auto; padding-top:0px;margin:0px auto;' ><table style='width:100%;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1){
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "")
            { 
                tab += "<td style='width:5%;text-align:center;' class=NormalText>#</td>";
                tab += "<td style='width:25%;text-align:center' class=NormalText>Checklist</td>";
                tab += "<td style='width:35%;text-align:center' class=NormalText>Remarks</td>";
                tab += "<td style='width:5%;text-align:center' class=NormalText>Frequency</td>";
                tab += "<td style='width:8%;text-align:center' class=NormalText>AppliedOn</td>";
                tab += "<td style='width:8%;text-align:center' class=NormalText>Interval</td>";
                tab += "<td style='width:5%;text-align:center' class=NormalText>Status</td>";
                tab += "<td style='width:5%;text-align:center' class=NormalText>Approved?</td>";
                tab += "<td style='width:2%;'/></td>";
                tab += "<td style='width:2%;'/></td>";
             }
                tab += "</tr></table></div><div class=mainhead style='width:100%; height:auto; overflow:auto; margin:0px auto;' ><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2){
                if (document.getElementById("<%= hid_dtls.ClientID %>").value != "")
                { 
                tab += "<td style='width:5%;text-align:center;' class=NormalText>#</td>";
                tab += "<td style='width:25%;text-align:center' class=NormalText>Task</td>";
                tab += "<td style='width:30%;text-align:center' class=NormalText>Remarks</td>";
                tab += "<td style='width:5%;text-align:center' class=NormalText>Weightage</td>";
                tab += "<td style='width:5%;text-align:center' class=NormalText>Is Remark Mandatory</td>";
                tab += "<td style='width:10%;text-align:center' class=NormalText>Status</td>";
                tab += "<td style='width:10%;text-align:center' class=NormalText>Approved?</td>";
                tab += "<td style='width:2%;'/></td>";
                tab += "<td style='width:2%;'/></td>";
                }
                tab += "</tr></table></div><div class=mainhead style='width:100%; height:auto; overflow:auto; margin:0px auto;' ><table style='width:100%; margin:0px auto;font-family:'cambria';' align='center'>";
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3){
                tab += "<td style='width:75%;text-align:left' class=NormalText>Findings</td>";
                tab += "<td style='width:5%;'/></td>";
                tab += "<td style='width:5%;'/></td>";
                tab += "</tr></table></div><div class=mainhead style='width:85%; height:auto; overflow:auto; margin:0px auto;;' ><table style='width:100%; margin:0px auto;font-family:'cambria';' align='center'>";
            }           
                
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");  
                for (n = 0; n <= row.length - 1; n++) {                    
                    col = row[n].split("µ");  
                                    
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;                   
                    tab += "<td style='width:5%;text-align:center;' >" + i + "</td>";
                    var gp=col[0];
                    if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3)
                    {   
                        col1 = gp.split("ÿ"); 
                        tab += "<td style='width:75%;text-align:left;' >" + col1[0] + "</td>";
                    }
                    if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1)
                    {
                        var txtBox = "<textarea id='txtRemarkschklist" + i + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;' onkeypress='return this.value.length < '4999'' maxlength='5000'>"+ col[0] +"</textarea>";
                        tab += "<td style='width:25%;text-align:left;' >" + txtBox + "</td>";
                        txtBox = "<textarea id='txtchkRemarks" + i + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;' onkeypress='return this.value.length < 4999' maxlength='5000'>"+ col[3] +"</textarea>";
                        tab += "<td style='width:35%;text-align:left;' >" + txtBox + "</td>";
                        tab += "<td style='width:5%;text-align:left;' >" + col[4] + "</td>";
                        tab += "<td style='width:8%;text-align:left;' >" + col[9] + "</td>";
                        tab += "<td style='width:8%;text-align:left;' >" + col[10] + "</td>";
                        tab += "<td style='width:5%;text-align:left;' >" + col[6] + "</td>";
                        tab += "<td style='width:5%;text-align:left;' >" + col[8] + "</td>";
                        tab += "<td style='width:2%;text-align:center' onclick=edit_row(" + col[1] + ",'"+ col[0].replace(/\s/g, 'Ø') + '¥' + col[3].replace(/\s/g, 'Ø') + '¥' + col[4].replace(/\s/g, 'Ø')+ '¥' + col[5] + '¥' + col[7] + '¥' + col[2] + '¥' + col[9] + '¥' + col[10] +"')><img  src='../Image/Edit.png' style='align:middle;cursor:pointer; height:15px; width:15px ;'title='Edit' /></td>";
                        tab += "<td style='width:2%;text-align:center' onclick=delete_row(" + col[1] + ") colspan='2'><img  src='../Image/cross.png' style='align:middle;cursor:pointer; height:15px; width:15px ;'title='Delete' /></td>";
                    } 
                    if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2)
                    {
                        if (document.getElementById("<%= hid_edit.ClientID %>").value == 0)
                        {
                        var txtBox = "<textarea id='txtRemarks" + col[1] + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;'  maxlength='5000' onkeypress='return TextAreaCheck(event)' >"+ col[0] +"</textarea>";
                        tab += "<td style='width:25%;text-align:left;' >" + txtBox + "</td>";
                        txtBox = "<textarea id='txtRemarks" + col[1] + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;'  maxlength='5000' onkeypress='return TextAreaCheck(event)' >"+ col[2] +"</textarea>";
                        tab += "<td style='width:30%;text-align:left;' >" + txtBox + "</td>";
                        
                        var Weightage = col[3];
                        txtBox = "<input id='txtRemarks" + col[1] + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;'  maxlength='6' onchange='return sumweightagepanel(this)' value='" + Math.abs(Weightage).toFixed(2) + "'></textarea>";
                        tab += "<td style='width:5%;text-align:left;' >" + txtBox + "</td>";
                        if (col[9]==1)
                        
                            var chkbox="<input type = 'checkbox' ID='chkRemark" + col[1] + "' checked='checked'/>";
                        else 
        
                            var chkbox="<input type = 'checkbox' ID='chkRemark" + col[1] + "'/>";

                         
                        tab += "<td style='width:5%;text-align:center;' >" + chkbox + "</td>";
                        tab += "<td style='width:10%;text-align:left;' >" + col[6] + "</td>";
                        tab += "<td style='width:10%;text-align:left;' >" + col[8] + "</td>";
                        tab += "<td style='width:2%;text-align:center' onclick=edit_row(" + col[1] + ",'"+ col[0].replace(/\s/g, 'Ø') + '¥' + col[2].replace(/\s/g, 'Ø') + '¥' + col[3].replace(/\s/g, 'Ø')+ '¥' + col[4] + '¥' + col[7] + '¥' + col[9] +"')><img  src='../Image/Edit.png' style='align:middle;cursor:pointer; height:15px; width:15px ;'title='Edit' /></td>";
                        tab += "<td style='width:2%;text-align:center' onclick=delete_row(" + col[1] + ") colspan='2'><img  src='../Image/cross.png' style='align:middle;cursor:pointer; height:15px; width:15px ;'title='Delete' /></td>";
                        }
                        if (document.getElementById("<%= hid_edit.ClientID %>").value == 1)
                        {
                            if(document.getElementById("<%= hid_Id.ClientID %>").value == col[1])
                            {
                                var txtBox = "<textarea id='txttask" + col[1] + "' name='txtRemarks" + i + "' type='Text' style='width:99%;'  maxlength='5000' onkeypress='return TextAreaCheck(event)' >"+ col[0] +"</textarea>";
                                tab += "<td style='width:25%;text-align:left;' >" + txtBox + "</td>";
                                txtBox = "<textarea id='txtRemark" + col[1] + "' name='txtRemarks" + i + "' type='Text' style='width:99%;'  maxlength='5000' onkeypress='return TextAreaCheck(event)' >"+ col[2] +"</textarea>";
                                tab += "<td style='width:30%;text-align:left;' >" + txtBox + "</td>";
                                var Weightage = col[3];
                                txtBox = "<input id='txtweightage" + col[1] + "' name='txtRemarks" + i + "' type='Text' style='width:99%;'  maxlength='6' onchange='return sumweightagepanel(this)' value='" + Math.abs(Weightage).toFixed(2) + "'></textarea>";
                                tab += "<td style='width:5%;text-align:left;' >" + txtBox + "</td>";
                                if (col[9]==1)
                        
                                var chkbox="<input type = 'checkbox' ID='chkRemark" + col[1] + "' checked='checked'/>";
                                else 
        
                                var chkbox="<input type = 'checkbox' ID='chkRemark" + col[1] + "'/>";

                         
                                tab += "<td style='width:5%;text-align:center;' >" + chkbox + "</td>";
                                var select = "<select id='cmbstatus" + col[1] + "' class='NormalText' name='cmbResp" + i + "' style='width:99%'>";
                                select += "<option value='111' selected=true>-Select-</option>";
                                if ((col[7] == -1))
                                    select += "<option value='-1' selected=true>Task Inactive</option>";
                                else
                                select += "<option value='-1'>Task Inactive</option>";
                                if (col[7] == 0)
                                select += "<option value='0' selected=true>Task not approved</option>";
                                else
                                select += "<option value='0'>Task not approved</option>";
                                if (col[7] == 1)
                                    select += "<option value='1' selected=true>Task active</option>";
                                else
                                select += "<option value='1'>Task active</option>";
                                if (col[7] == 100)
                                    select += "<option value='1' selected=true>Task just added</option>";
                                else
                                select += "<option value='1'>Task just added</option>";
                                if (col[7] == 999)
                                    select += "<option value='999' selected=true>Task deleted</option>";
                                else
                                select += "<option value='999'>Task deleted</option>";
                                tab += "<td style='width:10%;text-align:left;' >" + select + "</td>";
                                tab += "<td style='width:7%;text-align:left;' >" + col[8] + "</td>";
                                tab += "<td style='width:2%;text-align:center' onclick=edit_row(" + col[1] + ",'"+ col[0].replace(/\s/g, 'Ø') + '¥' + col[2].replace(/\s/g, 'Ø') + '¥' + col[3].replace(/\s/g, 'Ø')+ '¥' + col[4] + '¥' + col[7] +"')><img  src='../Image/Edit.png' style='align:middle;cursor:pointer; height:15px; width:15px ;'title='Edit' /></td>";
                                tab += "<td style='width:3%;text-align:center' onclick=save_row(" + col[1] + ",'"+ col[0].replace(/\s/g, 'Ø') + '¥' + col[2].replace(/\s/g, 'Ø') + '¥' + col[3].replace(/\s/g, 'Ø')+ '¥' + col[4] + '¥' + col[7] +"')><img  src='../Image/Save.png' style='align:middle;cursor:pointer; height:15px; width:15px ;'title='Save' /></td>";
                                tab += "<td style='width:2%;text-align:center' onclick=delete_row(" + col[1] + ") colspan='2'><img  src='../Image/cross.png' style='align:middle;cursor:pointer; height:15px; width:15px ;'title='Cancel' /></td>";
                            }
                            else
                            {
                            var txtBox = "<textarea id='txtRemarks" + col[1] + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;'  maxlength='5000' onkeypress='return TextAreaCheck(event)' >"+ col[0] +"</textarea>";
                            tab += "<td style='width:25%;text-align:left;' >" + txtBox + "</td>";
                            txtBox = "<textarea id='txtRemarks" + col[1] + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;'  maxlength='5000' onkeypress='return TextAreaCheck(event)' >"+ col[2] +"</textarea>";
                            tab += "<td style='width:30%;text-align:left;' >" + txtBox + "</td>";
                            var Weightage = col[3];
                            txtBox = "<input id='txtRemarks" + col[1] + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;'  maxlength='6' onchange='return sumweightagepanel(this)' value='" + Math.abs(Weightage).toFixed(2) + "'></textarea>";
                            tab += "<td style='width:5%;text-align:left;' >" + txtBox + "</td>";
                             if (col[9]==1)
                        
                                var chkbox="<input type = 'checkbox' ID='chkRemark" + col[1] + "' checked='checked'/>";
                            
                            else 
        
                                 var chkbox="<input type = 'checkbox' ID='chkRemark" + col[1] + "'/>";

                         
                        tab += "<td style='width:5%;text-align:center;' >" + chkbox + "</td>";
                            tab += "<td style='width:10%;text-align:left;' >" + col[6] + "</td>";
                            tab += "<td style='width:10%;text-align:left;' >" + col[8] + "</td>";
                            tab += "<td style='width:2%;text-align:center' onclick=edit_row(" + col[1] + ",'"+ col[0].replace(/\s/g, 'Ø') + '¥' + col[2].replace(/\s/g, 'Ø') + '¥' + col[3].replace(/\s/g, 'Ø')+ '¥' + col[4] + '¥' + col[7] + '¥' + col[9] +"')><img  src='../Image/Edit.png' style='align:middle;cursor:pointer; height:15px; width:15px ;'title='Edit' /></td>";
                            tab += "<td style='width:2%;text-align:center' onclick=delete_row(" + col[1] + ") colspan='2'><img  src='../Image/cross.png' style='align:middle;cursor:pointer; height:15px; width:15px ;'title='Delete' /></td>";
                            }
                        }
                    }                     
                    tab += "</tr>";
                }                
            }
            else
            {
                tab += "<td style='width:25%;text-align:center;'><b>No Tasklist</b></td>";
                tab += "</tr>";
            }
            tab += "</table></div>";            
            document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
            //--------------------- Clearing Data ------------------------//
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
        function btnClear_onclick() {
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1){
                CategoryClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2){
                ModuleClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3){   
                ProblemClick();
            }
        }
        function ApprovalOnChange()
        {   
        }
        function sumweightagepanel(id) {
            
//            var ToData = "9Ø" + document.getElementById("<%= txtWeightage.ClientID %>").value + "Ø" + document.getElementById("<%= cmbCategory.ClientID %>").value + "Ø" + document.getElementById("<%= hid_Edit.ClientID %>").value + "Ø" + document.getElementById("<%= hid_edweightage.ClientID %>").value;
//            ToServer(ToData, 9)
              if(id.value > 100)
              {
                alert("Maximum weightage value is 100");
                id.value = 0;
              }
        }
        function sumweightage() {
            
//            var ToData = "9Ø" + document.getElementById("<%= txtWeightage.ClientID %>").value + "Ø" + document.getElementById("<%= cmbCategory.ClientID %>").value + "Ø" + document.getElementById("<%= hid_Edit.ClientID %>").value + "Ø" + document.getElementById("<%= hid_edweightage.ClientID %>").value;
//            ToServer(ToData, 9)
              if(document.getElementById("<%= txtWeightage.ClientID %>").value > 100)
              {
                alert("Maximum weightage value is 100");
                document.getElementById("<%= txtWeightage.ClientID %>").value = 0;
              }
        }
        function white_space(field)
        {
            if(field.value.length==1){
                field.value = (field.value).replace(' ','');
            }
        }
        function DecimalCheck(e,el)
        {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                    if (unicode == 37 || unicode == 38) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (unicode == 46) {
                        var parts = el.value.split('.');
                        if (parts.length == 2)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            else {
                return true;
            }
        }
    </script>
</head>
</html>
 <br />
 <div  style="width:90%; height:auto; margin:0px auto; background-color:#EEB8A6;">
    <div style="width:80%;height:auto; padding-left:10px;margin:0px auto;">
    <div id='1' class="Button" 
             style="width:30%; float:left;background:-moz-radial-gradient(center, ellipse cover, #EEB8A6 0%, #801424 90%, #EEB8A6 100%);" 
            onclick="return CategoryClick()">CheckList</div>
     <div id='2' class="Button" style="border-left:thick double #FFF;width:30%;float:left;" onclick="return ModuleClick()">Tasks</div>
      <br /> <br />
       </div>
    <div style="width:93%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
    <br /><br />
            <table style="width:90%;height:auto;margin:0px auto;">
               
                <tr id="CategoryName" >
                    <td style="width:12%;"></td><td style="width:15%; align:left;">Checklist</td> 
                    <td colspan="2"><asp:TextBox ID="txtCateName" runat="server" class="NormalText" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="1"  onkeypress="return this.value.length < 4999" onKeyup="white_space(this)" MaxLength="5000" textmode="MultiLine" />
                    </td>
                </tr>
                <tr id="checkRemarks">
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">Remarks</td> 
                    <td colspan="2"><asp:TextBox ID="txtcheckremarks" class="NormalText" runat="server" onkeypress="return this.value.length < 4999" onKeyup="white_space(this)" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="1"  MaxLength="5000" textmode="MultiLine"  />
                        </td>
                </tr>
               <tr id="drpcheckdata"> 
               <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria"></Font-Names>Frequency</td> 
               <td colspan="2"><asp:DropDownList ID="drpcheck" class="NormalText" runat="server" Font-Names="Cambria" 
                Width="61%" ForeColor="Black">
                <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                <asp:ListItem Value="1">Daily</asp:ListItem>
                <asp:ListItem Value="2">Weekly</asp:ListItem>
                <asp:ListItem Value="3">Fortnightly</asp:ListItem>
                <asp:ListItem Value="4">Monthly</asp:ListItem>
                <asp:ListItem Value="5">Quarterly</asp:ListItem>
                <asp:ListItem Value="6">HalfYearly</asp:ListItem>
                <asp:ListItem Value="7">Yearly</asp:ListItem>
                </asp:DropDownList></td>
                </tr>
                <tr id="applieddate">
                <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria"></Font-Names>AppliedOn</td>
                <td colspan="2"><asp:TextBox ID="txtFromDt" class="NormalText" runat="server" 
                style=" font-family:Cambria;font-size:10pt;" Width="61%" onkeypress="return false" readonly=true></asp:TextBox>
                <asp:CalendarExtender ID="txtFromDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtFromDt" Format="MM'/'dd'/'yyyy">
                </asp:CalendarExtender>
                </td>
                </tr>
                <tr id="trIntervel" >
                    <td style="width:12%;"></td><td style="width:15%; align:left;">Interval</td> 
                    <td colspan="2"><asp:TextBox ID="txtinterval" runat="server" class="NormalText" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="1" onpaste="return false" onkeypress='return NumericCheck(event)'/>
                    </td>
                </tr>
               <tr id="trstatus"style="display:none;"> 
               <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria"></Font-Names>Status</td> 
               <td colspan="2"><asp:DropDownList ID="drpStatus" class="NormalText" runat="server" Font-Names="Cambria" 
                Width="61%" ForeColor="Black">
                <asp:ListItem Value="-1">Checklist Inactive</asp:ListItem>
                <asp:ListItem Value="0">No task available</asp:ListItem>
                <asp:ListItem Value="1">Checklist active</asp:ListItem>
                <asp:ListItem Value="999">Checklist deleted</asp:ListItem>
                </asp:DropDownList></td>
                </tr>
                <tr id="Category" style="display:none;"> 
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria"></Font-Names>Checklist</td> 
                    <td colspan="2"><asp:DropDownList ID="cmbCategory" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="61%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList></td>
                    </tr>
                    
                <tr id="ModuleName" style="display:none;">
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">Tasks</td> 
                    <td colspan="2"><asp:TextBox ID="txtModuleName" class="NormalText" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="1"  onkeypress="return this.value.length < 4999"  onKeyup="white_space(this)" MaxLength="5000" textmode="MultiLine"/>
                        </td>
                </tr>
                <tr id="Remarks" style="display:none;">
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">Remarks</td> 
                    <td colspan="2"><asp:TextBox ID="txtRemarks" class="NormalText" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="1" onkeypress="return this.value.length < 4999"  onKeyup="white_space(this)" MaxLength="5000" textmode="MultiLine"/>
                        </td>
                </tr>
                <tr id="Weightage" style="display:none;">
                <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">Weightage</td> 
                <%--<td colspan="2"><asp:TextBox ID="txtWeightage" class="NormalText" runat="server" 
                        style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="3"  
                        MaxLength="500" onkeypress='return DecimalCheck(event,this)' onpaste="return false" onchange='return sumweightage()'/>
                </td>--%>
                <td colspan="2"><asp:TextBox ID="txtWeightage" class="NormalText" runat="server" 
                        style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="3"  
                        MaxLength="3" onkeypress='return DecimalCheck(event,this)' 
                        onpaste="return false" onchange='return sumweightage()'/>
                </td>
                </tr>
                 <tr id="RemarkMandatory" style="display:none;">
                <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">Is Remark mandatory</td> 
                <td colspan="2"><asp:CheckBox ID="txtRemarkMand" class="NormalText" runat="server"/>
                </td>
                </tr>
               <tr id="cmbfreq" style="display:none;"> 
               <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria"></Font-Names>Frequency</td> 
               <td colspan="2"><asp:DropDownList ID="drpFreq" class="NormalText" runat="server" Font-Names="Cambria" 
                Width="61%" Enabled="false" ForeColor="Black">
                <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                <asp:ListItem Value="1">Daily</asp:ListItem>
                <asp:ListItem Value="2">Weekly</asp:ListItem>
                <asp:ListItem Value="3">Fortnightly</asp:ListItem>
                <asp:ListItem Value="4">Monthly</asp:ListItem>
                <asp:ListItem Value="5">Quarterly</asp:ListItem>
                <asp:ListItem Value="6">HalfYearly</asp:ListItem>
                <asp:ListItem Value="7">Yearly</asp:ListItem>
                </asp:DropDownList></td>
                </tr>
                <tr id="trdrptasksts"style="display:none;"> 
               <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria"></Font-Names>Status</td> 
               <td colspan="2"><asp:DropDownList ID="drptasksts" class="NormalText" runat="server" Font-Names="Cambria" 
                Width="61%" ForeColor="Black">
                <asp:ListItem Value="111">--Select--</asp:ListItem>
                <asp:ListItem Value="-1">Task Inactive</asp:ListItem>
                <asp:ListItem Value="0">Task not approved</asp:ListItem>
                <asp:ListItem Value="1">Task active</asp:ListItem>
                <asp:ListItem Value="999">Task deleted</asp:ListItem>
                </asp:DropDownList></td>
                </tr>
                <tr id="List">
                    <td style="text-align: center;" colspan="4">
                        <asp:Panel ID="pnlDtls" Style="width: 100%; height:auto; text-align: right; float: right;" runat="server">
                        </asp:Panel>
                        <asp:HiddenField ID="hid_Edit" runat="server" />
                        <asp:HiddenField ID="hid_Id" runat="server" />
                        <asp:HiddenField ID="hid_tab" runat="server" />
                        <asp:HiddenField ID="hid_dtls" runat="server" />
                        <asp:HiddenField ID="hid_Save" runat="server" />
                        <asp:HiddenField ID="hid_Cate" runat="server" />
                        <asp:HiddenField ID="hid_SubCate" runat="server" />
                        <asp:HiddenField ID="hid_hisID" runat="server" />
                        <asp:HiddenField ID="hid_error" runat="server" />
                        <asp:HiddenField ID="hid_edweightage" runat="server" />
                        <asp:HiddenField ID="hidedit_taskname" runat="server" />
                        <asp:HiddenField ID="hidedit_remarks" runat="server" />
                        <asp:HiddenField ID="hidedit_weightage" runat="server" />
                        <asp:HiddenField ID="hidedie_status" runat="server" />
                        <asp:HiddenField ID="hid_editstart" runat="server" />
                        <asp:HiddenField ID="hidMandate_remark" runat="server" />
                        
                    </td>
                </tr>
            </table> 
            
    </div>
    <div style="text-align:center; height: 63px;"><br />
    <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;" 
                            type="button" value="SAVE" onclick="return Saveonclick()"/>
    
 &nbsp;
 <input id="btnFinish" style="font-family: cambria; cursor: pointer; width:8%;" 
                            type="button" value="FINISH" onclick="return Finishonclick()" disabled/>
    
 &nbsp;
                        &nbsp;

                        <input id="Button1" style="font-family: cambria; cursor: pointer; width:8%;" 
                            type="button" value="CLEAR" onclick="return btnClear_onclick()" onclick="return btnClear_onclick()"/> &nbsp;
                         &nbsp;
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" 
                            type="button" value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" /></div>
    </div>   
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

