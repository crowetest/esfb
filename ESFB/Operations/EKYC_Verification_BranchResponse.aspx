﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="EKYC_Verification_BranchResponse.aspx.vb" Inherits="EKYC_Verification_BranchResponse" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server" >
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
    <script language="javascript" type="text/javascript">

  function  window_onload() 
  {

       if(document.getElementById("<%= hid_Admin.ClientID %>").value ==1)
       { 
       document.getElementById("Sec").style.display="";
        document.getElementById("btnSave").style.display="none";
         document.getElementById("<%= btnExport.ClientID %>").style.display="";
       }
       else
       {
      
       table_fill()

       }
   }



        function FromServer(arg, context) {
            if (context == 1)
             {
                document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
               table_fill()
            }
            else if (context == 2) {
               var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0]==0)
                {
                    window.open("EKYC_Verification_BranchResponse.aspx", "_self");  
   
                }
            }
            else if (context == 3) {
               
            }
            else if (context == 4) {
                
            }
        }


        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

  function BranchOnchange() {
 
            var BranchID=document.getElementById("<%= cmbBranch.ClientID %>").value;
            var ToData = "1Ø" + BranchID;          
            ToServer(ToData, 1);
          
        }
//   function SelectAll() {
//            if (document.getElementById("chkSelectAll").checked==true)
//            {
//              for (n = 1; n <= Rowlength - 1; n++) { 
//                      document.getElementById("chkResponse_"+n).checked=true;
//                    }
//            }
//            else if(document.getElementById("chkSelectAll").checked==false)
//            {
//            for (n = 1; n <= Rowlength - 1; n++) { 
//                      document.getElementById("chkResponse_"+n).checked=false;
//                    }
//            }


//        }
       
        function table_fill() {

            document.getElementById("<%= pnEkyc.ClientID %>").style.display = '';
            var tab = "";
            var Remarks = "";
            tab +=" <div class='mainhead' style='width:100%; height:auto; padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
            tab += "<table id = 'x' style='width:100%; margin:0px auto;font-family:cambria;line-height:20px;background-color:#DF8A6E;' align='left'>";
            tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;' align='left'>";
            tab += "<td style='width:2%;text-align:center'>#</td>";
            tab += "<td style='width:4%;text-align:center;'>Branch code</td>";
            tab += "<td style='width:5%;text-align:center;'>Branch Name</td>";
            tab += "<td style='width:7%;text-align:center;'>Cluster</td>";
            tab += "<td style='width:8%;text-align:center;'>Customer Number</td>";
            tab += "<td style='width:7%;text-align:center;'>Customer Name</td>";
            tab += "<td style='width:7%;text-align:center;'>ACcount Open Date</td>";
            tab += "<td style='width:7%;text-align:center;'>CIF Activate Date</td>";
            tab += "<td style='width:5%;text-align:center;'>CIF Status</td>";
            tab += "<td style='width:10%;text-align:center'>ADDRESS 1</td>";
            tab += "<td style='width:7%;text-align:center'>ADDRESS 2</td>";
            tab += "<td style='width:5%;text-align:center'>PinCode</td>";
            tab += "<td style='width:7%;text-align:center'>Phone</td>";
            tab += "<td style='width:7%;text-align:center'>Branch Response</td>";
            tab += "<td style='width:12%;text-align:center'>If Yes provide SR No/If No give Justification</td>";
            tab += "<td style='width:0%;text-align:center;display:none;'>EKYCID</td>";
//          tab += "<td style='width:2%;text-align:center'>Select All<input type = 'checkbox' ID='chkSelectAll' onclick='return SelectAll()'/></td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
            {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");
//              Rowlength=row.length;
                for (n = 1; n <= row.length - 1; n++) 
                {
                    col = row[n].split("ÿ");
                    tab += "<tr class='sub_first Disp_Section1'>";
                    tab += "<td style='width:2%;text-align:center'>" + n + "</td>";
                    tab += "<td style='width:4%;text-align:center;'>" + col[0] + "</td>";
                    tab += "<td style='width:5%;text-align:center'>" + col[1] + "</td>";
                    tab += "<td style='width:7%;text-align:center'>"+ col[2] +"</td>";
                    tab += "<td style='width:8%;text-align:center;'>" + col[3] + "</td>";
                    tab += "<td style='width:7%;text-align:center'>" + col[4] + "</td>";
                    tab += "<td style='width:7%;text-align:center'>" + col[5].toString("dd/mm/yyyy")+ "</td>";
                    tab += "<td style='width:7%;text-align:center;'>" + col[6].toString("dd/mm/yyyy") + "</td>";
                    tab += "<td style='width:5%;text-align:center'>" + col[7] + "</td>";
                    tab += "<td style='width:10%;text-align:center'>" + col[8] + "</td>";
                    tab += "<td style='width:7%;text-align:center;overflow-wrap:anywhere;'>" + col[9] + "</td>";
                    tab += "<td style='width:5%;text-align:center'>" + col[10] + "</td>";
                    tab += "<td style='width:7%;text-align:center'>" + col[11] + "</td>";    
                    tab += "<td style='width:0%;text-align:left;display:none;'>" + col[12] + "</td>";
                    if(document.getElementById("<%= hid_Admin.ClientID %>").value =="1")
                    { 
                        var select = "<select id='cmbResponse_" + n + "' class='NormalText' name='cmbResp_" + n + "' style='width:99%'>"; 
                        
                        if ((col[13] == 1))
                           select += "<option value='1' selected=true>YES  </option>";
                        
                        if (col[13] == 2)
                           select += "<option value='2' selected=true>NO  </option>";
                        
                    
                        tab += "<td style='width:7%;text-align:left;' >" + select + "</td>";  

                        Remarks = "";
                        if (col[14] != "")
                            Remarks = col[14];
                        tab += "<td style='width:10%;text-align:center'><textarea id='txtComment_"+n+"'  name='txtComment_"+n+"'  type='Text' ReadOnly='true' style='width:99%;'  class='NormalText' onkeypress='return TextAreaCheck(event)' >" + Remarks + "</textarea></td>";

                     }
                     else
                     {

                        var select = "<select id='cmbResponse_" + n + "' class='NormalText' name='cmbResp_" + n + "' style='width:99%'>"; 
                        select += "<option value='-1' selected=true>-Select-</option>";
                        select += "<option value='1'>YES  </option>";
                        select += "<option value='2'>NO  </option>";                       
                        tab += "<td style='width:7%;text-align:left;' >" + select + "</td>";  
                        tab += "<td style='width:12%;text-align:center'><textarea id='txtComment_"+n+"'  name='txtComment_"+n+"'  type='Text' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' ></textarea></td>";
                      }
//                        tab += "<td style='width:2%;text-align:center'><input type = 'checkbox' ID='chkResponse_"+n+"'/></td>";
                      tab += "</tr>";
               
               }
            }
           

            tab += "</table></div></div></div>";
            document.getElementById("<%= pnEkyc.ClientID %>").innerHTML = tab;
            //--------------------- Clearing Data ------------------------//
        }


        function btnSave_onclick()
        {

        var  BranchResponse;
        var  Description;
        var EKYC_ID;
        var  passData ='';

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("ÿ");
                    if(document.getElementById("cmbResponse_" + (n)).value!="-1")
                    {
                      if ((document.getElementById("cmbResponse_" + n).value != "-1")&&(document.getElementById("txtComment_" + n).value == ""))
                       {
                          alert("Please give description");
                          document.getElementById("txtComment_" + n).focus();
                          return false;
                         }

                     BranchResponse =  document.getElementById("cmbResponse_" + (n)).value;
                    Description =  document.getElementById("txtComment_" + (n)).value;
                    EKYC_ID=col[12];
                   
                     passData += BranchResponse + "µ" + Description +  "µ" + EKYC_ID + "¥" ;                    

                    }
                  }

                var mode=document.getElementById("<%= hid_Admin.ClientID %>").value;
               
                var ToData = "2Ø" + passData + "Ø" + mode;
                ToServer(ToData,2);


        }

        function btnExit_onclick()
         {

                 window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");

        }
        

    </script>
</head>
</html>
<br />
<div style="text-align:center;" align="center">
    <div id="Sec" style="display:none;">
            Branch Code
            <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" 
                Font-Names="Cambria" Width="15%" 
                 ForeColor="Black" Height="20px"></asp:DropDownList>
         </div> 

<table style="text-align:center;width:100%; margin: 0px auto; ">
    <tr><td style="text-align:center;" colspan="4">&nbsp;</td></tr>
   <tr >
         

        <td colspan="4" >
            <asp:Panel ID="pnEkyc" runat="server" Width="100%" >
            </asp:Panel> 
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <br />
        </td>
    </tr>
 
    <tr><td style="text-align:center;" colspan="4">&nbsp;</td></tr>
    <tr>
        <td style="text-align:center;" colspan="4">
           
            &nbsp;
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 5%; " 
                type="button" value="SAVE" onclick="return btnSave_onclick()" />
            &nbsp;
             <asp:Button ID="btnExport" runat="server" Text="EXPORT" 
                    Font-Names="Cambria" style="margin-bottom: 0px;display:none;" Width="5%"/>
            &nbsp;
            &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 5%; " 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />


            <asp:HiddenField 
                ID="hid_dtls" runat="server" /> 
                <asp:HiddenField 
                ID="hid_Admin" runat="server" />
        </td>
    </tr>
</table>    
</div>
<br /><br />
</asp:Content>

