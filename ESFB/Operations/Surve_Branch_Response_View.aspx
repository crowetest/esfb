﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Surve_Branch_Response_View.aspx.vb" Inherits="Surve_Branch_Response_View" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server" >
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
    <script language="javascript" type="text/javascript">
        function UnwantedCharCheck(e)//------------function to check whether a value is > or <
        {
        alert (e.which);
        alert (e.keyWhich);
        alert (e.keyCode);

//            var valid = ((e.which >= 48 && e.which <= 57) || (e.keyWhich == 9) || (e.keyCode == 9) || (e.keyWhich == 8) || (e.keyCode == 8));
//            //alert(valid);
//            if (!valid) {
//                e.preventDefault();
//            }

        }
        function FromServer(arg, context) {
            if (context == 1) {
                ComboFill(arg, "<%= cmbBranch.ClientID %>");
                document.getElementById("<%= cmbBranch.ClientID %>").focus();
            }
            else if (context == 2) {
                document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                table_fill();
                document.getElementById("<%= drpFreq.ClientID %>").disabled=false;
                document.getElementById("<%= drpcheck.ClientID %>").disabled=false;
                document.getElementById("<%= drpType.ClientID %>").disabled=false;
                document.getElementById("<%= txtFromDt.ClientID %>").disabled=false;
                document.getElementById("<%= txtToDate.ClientID %>").disabled=false;
                document.getElementById("<%= drpadminStatus.ClientID %>").disabled=false;
                FillCheckcombo();
            }
            else if (context == 3 || context == 4) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if ((Data[0] == 0) && (Data[3] == 1)) window.open("Surve_Branch_Response_view.aspx", "_self");
              
            }
             else if (context == 5) 
             {
                document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                table_fill();
 //               FillCheckcombo();
//                document.getElementById("drpcheck").disabled = true;            
                document.getElementById("<%= cmbBranch.ClientID %>").disabled=false;
                document.getElementById("<%= drpFreq.ClientID %>").disabled=false;
                document.getElementById("<%= drpcheck.ClientID %>").disabled=false;
                document.getElementById("<%= drpType.ClientID %>").disabled=false;
                document.getElementById("<%= txtFromDt.ClientID %>").disabled=false;
                document.getElementById("<%= txtToDate.ClientID %>").disabled=false;
                document.getElementById("<%= drpadminStatus.ClientID %>").disabled=false;
                FillCheckcombo();
            }
            else if (context == 6) 
            {
                 ComboFill(arg, "<%= drpcheck.ClientID %>");
            }
            else if (context == 7) 
            {
                document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                table_fill();
          
                document.getElementById("<%= cmbBranch.ClientID %>").disabled=false;
                document.getElementById("<%= drpFreq.ClientID %>").disabled=false;
                document.getElementById("<%= drpType.ClientID %>").disabled=false;
                document.getElementById("<%= txtFromDt.ClientID %>").disabled=false;
                document.getElementById("<%= txtToDate.ClientID %>").disabled=false;
                document.getElementById("<%= drpadminStatus.ClientID %>").disabled=false;
            }
            else if (context == 8) 
            {
                document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                table_fill();
//                FillCheckcombo();
                document.getElementById("<%= cmbBranch.ClientID %>").disabled=false;
                document.getElementById("<%= drpFreq.ClientID %>").disabled=false;
                document.getElementById("<%= drpcheck.ClientID %>").disabled=false;
                document.getElementById("<%= txtFromDt.ClientID %>").disabled=false;
                document.getElementById("<%= txtToDate.ClientID %>").disabled=false;
                FillCheckcombo();
            }
            else if(context==9)
            {
                document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                table_fill();
//                FillCheckcombo();
                document.getElementById("<%= cmbBranch.ClientID %>").disabled=false;
                document.getElementById("<%= drpFreq.ClientID %>").disabled=false;
                document.getElementById("<%= drpcheck.ClientID %>").disabled=false;
                document.getElementById("<%= drpType.ClientID %>").disabled=false;        
                document.getElementById("<%= drpadminStatus.ClientID %>").disabled=false;
                FillCheckcombo();
            }
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
         function FillCheckcombo(data, ddlName) 
         {
            var BranchID=document.getElementById("<%= cmbBranch.ClientID %>").value;
            var Freqid = document.getElementById("<%= drpFreq.ClientID %>").value;
            var TypeId = document.getElementById("<%= drpType.ClientID %>").value;
            var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDate.ClientID %>").value;
            var AdmStatus = document.getElementById("<%= drpadminStatus.ClientID %>").value;
            var checkStatus = document.getElementById("<%= drpcheck.ClientID %>").value;
//            if(CheckListID>0)
//            {
                var ToData = "6Ø" + BranchID + "Ø" + Freqid + "Ø" + TypeId + "Ø" + FromDt + "Ø" + ToDt + "Ø" + AdmStatus + "Ø" + checkStatus;          
                ToServer(ToData, 6);
//            }
         }
        function block_unblock_remark(i) {
           
            if (document.getElementById("cmbResp"+i).value != -1)
                document.getElementById("txtRemarks"+i).disabled = false;
            else
                document.getElementById("txtRemarks"+i).disabled = true;

        }
        
        function BranchOnchange() {
             document.getElementById("Category").style.display = 'none';
//            var CheckListID=document.getElementById("<%= cmbBranch.ClientID %>").value;
            var BranchID=document.getElementById("<%= cmbBranch.ClientID %>").value;
            var TypeId = document.getElementById("<%= drpType.ClientID %>").value;
            var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDate.ClientID %>").value;
            var AdmStatus = document.getElementById("<%= drpadminStatus.ClientID %>").value;
            //---------------
            var checklistId = document.getElementById("<%= drpcheck.ClientID %>").value;
            var Freqid = document.getElementById("<%= drpFreq.ClientID %>").value;
            //-----------------
            document.getElementById("<%= drpFreq.ClientID %>").disabled=true;
            document.getElementById("<%= drpcheck.ClientID %>").disabled=true;
            document.getElementById("<%= drpType.ClientID %>").disabled=true;
            document.getElementById("<%= txtFromDt.ClientID %>").disabled=true;
            document.getElementById("<%= txtToDate.ClientID %>").disabled=true;
            document.getElementById("<%= drpadminStatus.ClientID %>").disabled=true;
//            if(CheckListID>0)0
//            {
//                var ToData = "2Ø" + CheckListID + "Ø" + TypeId + "Ø" + FromDt + "Ø" + ToDt + "Ø" + AdmStatus;   
                var ToData = "2Ø" + BranchID + "Ø" + TypeId + "Ø" + FromDt + "Ø" + ToDt + "Ø" + AdmStatus+"Ø"+checklistId+"Ø"+Freqid;        
                ToServer(ToData, 2);
//            }
          
        }
        function FreqOnchange() {
             document.getElementById("Category").style.display = 'none';
            var BranchID=document.getElementById("<%= cmbBranch.ClientID %>").value;
            var Freqid = document.getElementById("<%= drpFreq.ClientID %>").value;
            var TypeId = document.getElementById("<%= drpType.ClientID %>").value;
            var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDate.ClientID %>").value;
            var AdmStatus = document.getElementById("<%= drpadminStatus.ClientID %>").value;
            var ChecklistId = document.getElementById("<%= drpcheck.ClientID %>").value;
            document.getElementById("<%= cmbBranch.ClientID %>").disabled=true;
            document.getElementById("<%= drpcheck.ClientID %>").disabled=true;
            document.getElementById("<%= drpType.ClientID %>").disabled=true;
            document.getElementById("<%= txtFromDt.ClientID %>").disabled=true;
            document.getElementById("<%= txtToDate.ClientID %>").disabled=true;
            document.getElementById("<%= drpadminStatus.ClientID %>").disabled=true;
//            if(CheckListID>0)
//            {
                var ToData = "5Ø" + BranchID + "Ø" + Freqid + "Ø" + TypeId + "Ø" + FromDt + "Ø" + ToDt + "Ø" + AdmStatus+"Ø"+ChecklistId;          
                ToServer(ToData, 5);
//            }
          
        }
        function CheckOnchange() {
             document.getElementById("Category").style.display = 'none';
            var BranchID=document.getElementById("<%= cmbBranch.ClientID %>").value;
            var Freqid = document.getElementById("<%= drpFreq.ClientID %>").value;
            var TypeId = document.getElementById("<%= drpType.ClientID %>").value;
            var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDate.ClientID %>").value;
            var checkid = document.getElementById("<%= drpcheck.ClientID %>").value;
            var AdmStatus = document.getElementById("<%= drpadminStatus.ClientID %>").value;            
            document.getElementById("<%= cmbBranch.ClientID %>").disabled=true;
            document.getElementById("<%= drpFreq.ClientID %>").disabled=true;
            document.getElementById("<%= drpType.ClientID %>").disabled=true;
            document.getElementById("<%= txtFromDt.ClientID %>").disabled=true;
            document.getElementById("<%= txtToDate.ClientID %>").disabled=true;
            document.getElementById("<%= drpadminStatus.ClientID %>").disabled=true;
//            if(CheckListID>0)
//            {
                var ToData = "7Ø" + BranchID + "Ø" + Freqid + "Ø" + TypeId + "Ø" + FromDt + "Ø" + ToDt + "Ø" + checkid + "Ø" + AdmStatus;          
                //ToServer(ToData, 5);        
               ToServer(ToData, 7);
//            }
        }
        function StatusOnchange() {
            document.getElementById("Category").style.display = 'none';
            var BranchID=document.getElementById("<%= cmbBranch.ClientID %>").value;
            var Freqid = document.getElementById("<%= drpFreq.ClientID %>").value;
            var TypeId = document.getElementById("<%= drpType.ClientID %>").value;
            var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDate.ClientID %>").value;
            var checkid = document.getElementById("<%= drpcheck.ClientID %>").value;
            var AdmStatus = document.getElementById("<%= drpadminStatus.ClientID %>").value;
            document.getElementById("<%= cmbBranch.ClientID %>").disabled=true;
            document.getElementById("<%= drpFreq.ClientID %>").disabled=true;
            document.getElementById("<%= drpcheck.ClientID %>").disabled=true;
            document.getElementById("<%= txtFromDt.ClientID %>").disabled=true;
            document.getElementById("<%= txtToDate.ClientID %>").disabled=true;
//            if(CheckListID>0)
//            {
                var ToData = "8Ø" + BranchID + "Ø" + Freqid + "Ø" + TypeId + "Ø" + FromDt + "Ø" + ToDt + "Ø" + checkid + "Ø" + AdmStatus;         
                ToServer(ToData, 8);
//            }        
        }
         function DateOnchange()
        {
            document.getElementById("Category").style.display = 'none';
            var BranchID=document.getElementById("<%= cmbBranch.ClientID %>").value;
            var Freqid = document.getElementById("<%= drpFreq.ClientID %>").value;
            var TypeId = document.getElementById("<%= drpType.ClientID %>").value;
            var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDate.ClientID %>").value;
            var checkid = document.getElementById("<%= drpcheck.ClientID %>").value;
            var AdmStatus = document.getElementById("<%= drpadminStatus.ClientID %>").value;
            document.getElementById("<%= cmbBranch.ClientID %>").disabled=true;
            document.getElementById("<%= drpFreq.ClientID %>").disabled=true;
            document.getElementById("<%= drpcheck.ClientID %>").disabled=true;
            document.getElementById("<%= drpadminStatus.ClientID %>").disabled=true;
//            if(CheckListID>0)
//            {
                var ToData = "9Ø" + BranchID + "Ø" + Freqid + "Ø" + TypeId + "Ø" + FromDt + "Ø" + ToDt + "Ø" + checkid + "Ø" + AdmStatus;         
                ToServer(ToData, 9);
//            }  
        }
         function AdminStatusOnchange() {
            document.getElementById("Category").style.display = 'none';
            var BranchID=document.getElementById("<%= cmbBranch.ClientID %>").value;
            var Freqid = document.getElementById("<%= drpFreq.ClientID %>").value;
            var TypeId = document.getElementById("<%= drpType.ClientID %>").value;
            var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDate.ClientID %>").value;
            var checkid = document.getElementById("<%= drpcheck.ClientID %>").value;
            var AdmStatus = document.getElementById("<%= drpadminStatus.ClientID %>").value;
            document.getElementById("<%= cmbBranch.ClientID %>").disabled=true;
            document.getElementById("<%= drpFreq.ClientID %>").disabled=true;
            document.getElementById("<%= drpcheck.ClientID %>").disabled=true;
            document.getElementById("<%= txtFromDt.ClientID %>").disabled=true;
            document.getElementById("<%= txtToDate.ClientID %>").disabled=true;  
//            if(CheckListID>0)
//            {
                var ToData = "8Ø" + BranchID + "Ø" + Freqid + "Ø" + TypeId + "Ø" + FromDt + "Ø" + ToDt + "Ø" + checkid + "Ø" + AdmStatus;        
                ToServer(ToData, 8);
//            }        
        }
        function table_fill() {

            document.getElementById("<%= pnTaskResp.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var Remarks = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:5%;text-align:center'>Sl No</td>";
            tab += "<td style='width:10%;text-align:center'>Branch</td>";
            tab += "<td style='width:8%;text-align:center'>Frequency</td>";
//            tab += "<td style='width:10%;text-align:center'>ChecklistID</td>";
            tab += "<td style='width:10%;text-align:center'>Checklist Name</td>";
//            tab += "<td style='width:10%;text-align:center'>TaskID</td>";
            tab += "<td style='width:23%;text-align:center'>TaskDesc</td>";
            tab += "<td style='width:5%;text-align:center'>RespType</td>";
            tab += "<td style='width:10%;text-align:center'>RespComments</td>";
            tab += "<td style='width:5%;text-align:center'>Date</td>";
            tab += "<td style='width:14%;text-align:center'>Remark</td>";
            tab += "<td style='width:10%;text-align:center'>Status</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n;
                    tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[0]+" - "+col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:23%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[11] + "</td>";
//                    tab += "<td style='width:10%;text-align:left'>" + col[7] + "</td>";
                    if (col[9] != "")
                    {
//                        var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' type='Text' style='width:99%;'  maxlength='500' text="+col[8]+"></textarea>";
                        var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)' >"+col[9].toString()+"</textarea>";
                    }
                    else
                    {
                        var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)' ></textarea>";
                    }
//                        tab += "<td style='width:25%;text-align:left;' >" + txtBox + "</td>";
//                    var txtRemarks = "<input id='txtRemarks" + i + "' name='txtRemarks" + i + "' value ='"+ Remarks +"' type='Text' style='width:99%;' class='NormalText' maxlength='50' />";                                            
                    tab += "<td style='width:14%;text-align:left'>" + txtBox + "</td>"; 
                    var select = "<select id='cmbResp" + i + "' class='NormalText' name='cmbResp" + i + "' style='width:100%' onchange='block_unblock_remark("+i+")'   >";

                    select += "<option value='-1' selected=true>-Select-</option>";
                    if ((col[10] == 1))
                        select += "<option value='1' selected=true>OPEN</option>";
                    else
                        select += "<option value='1'>OPEN</option>";
                    if (col[10] == 2)
                        select += "<option value='2' selected=true>CLOSED</option>";
                    else
                        select += "<option value='2'>CLOSED</option>";
                    if (col[10] == 3)
                        select += "<option value='3' selected=true>N/A</option>";
                    else
                        select += "<option value='3'>N/A</option>";
                       

                    tab += "<td style='width:10%;text-align:left'>" + select + "</td>";           

                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnTaskResp.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//
        }
         function UpdateValue() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("Nothing to save");
                return false;
            }
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 1; n <= row.length - 1; n++) 
            {
                col = row[n].split("µ");

                document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[8] + "µ" + document.getElementById("cmbResp" + n).value + "µ" + document.getElementById("txtRemarks" + n).value + "µ" + col[12];
            }
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "") {
                alert("Nothing to save");
                return false;
            }
            else
                return true;
        }
        function btnDraft_onclick() 
        {
            var ret = UpdateValue();
            if (ret == true) 
            {
                var Data = "3Ø" + document.getElementById("<%= hid_temp.ClientID %>").value;
                ToServer(Data, 3);
            }        
        }
        function btnView_onclick() 
        {
              document.getElementById("Category").style.display = '';
        }
//        function DateOnchange()
//        {
//            document.getElementById("Category").style.display = 'none';
//        }
        function btnSave_onclick() 
        {
            var ret = UpdateValue();
            if (ret == true) 
            {
                var Data = "4Ø" + document.getElementById("<%= hid_temp.ClientID %>").value;
                ToServer(Data, 4);
            }        
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        

    </script>
</head>
</html>
<br />
<div style="text-align:center; width:90%; height:auto; margin:0px auto;" align="center">
<table style="text-align:center;width:80%; margin: 0px auto; ">
    <tr>
        <td style="text-align:right; width:5px">
            Branch:</td>
            <td style="text-align:left; width:70">
            <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" 
                Font-Names="Cambria" Width="50%" 
                 ForeColor="Black" Height="20px">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="text-align:right; width:5px">
            Frequency:</td>
            <td style="text-align:left; width:70">
            <asp:DropDownList ID="drpFreq" class="NormalText" runat="server" 
                Font-Names="Cambria" Width="50%" 
                 ForeColor="Black" Height="20px">
            </asp:DropDownList>
        </td>
    </tr>
    <%--<tr>
        <td style="text-align:right; width:5px">
            Checklist:</td>
            <td style="text-align:left; width:70">
            <asp:DropDownList ID="drpcheck" class="NormalText" runat="server" 
                Font-Names="Cambria" Width="50%" 
                 ForeColor="Black" Height="20px">
            </asp:DropDownList>
        </td>
    </tr>--%>
    <tr>
        <td style="text-align:right; width:5px">
            Response Type:</td>
            <td style="text-align:left; width:70">
            <asp:DropDownList ID="drpType" class="NormalText" runat="server" 
                Font-Names="Cambria" Width="50%" 
                 ForeColor="Black" Height="20px">
                <asp:ListItem Value="2">NO</asp:ListItem>
                <asp:ListItem Value="1">YES</asp:ListItem>
                <asp:ListItem Value="3">NA</asp:ListItem>
                <asp:ListItem Value="-1">--ALL--</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="text-align:right; width:5px">
            Admin Response Status:</td>
            <td style="text-align:left; width:70">
            <asp:DropDownList ID="drpadminStatus" class="NormalText" runat="server" 
                Font-Names="Cambria" Width="50%" 
                 ForeColor="Black" Height="20px">
                <asp:ListItem Value="1">OPEN</asp:ListItem>
                <asp:ListItem Value="2">CLOSED</asp:ListItem>
                <asp:ListItem Value="3">NA</asp:ListItem>
                <asp:ListItem Value="-1">--ALL--</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="text-align:right; width:5px">
        From Date:</td> <td style="text-align:left; width:70">
        <asp:TextBox ID="txtFromDt" class="NormalText" runat="server" 
        style=" font-family:Cambria;font-size:10pt;" Width="50%" onkeypress="return false" ></asp:TextBox>
        <asp:CalendarExtender ID="txtFromDt_CalendarExtender" runat="server" 
        Enabled="True" TargetControlID="txtFromDt" Format="MM'/'dd'/'yyyy">
        </asp:CalendarExtender>
        </td>
    </tr>
    <tr>
    <td style="text-align:right; width:5px">
    To Date:</td>
    <td style="text-align:left; width:70px">
    <asp:TextBox ID="txtToDate" class="NormalText" runat="server" 
    style=" font-family:Cambria;font-size:10pt;" Width="50%" onkeypress="return false" ></asp:TextBox>
    <asp:CalendarExtender ID="CalendarExtender1" runat="server" 
    Enabled="True" TargetControlID="txtToDate" Format="MM'/'dd'/'yyyy">
    </asp:CalendarExtender>
    </td>
    </tr>
    <tr>
        <td style="text-align:right; width:5px">
            Checklist:</td>
            <td style="text-align:left; width:70">
            <asp:DropDownList ID="drpcheck" class="NormalText" runat="server" 
                Font-Names="Cambria" Width="50%" 
                 ForeColor="Black" Height="20px">
            </asp:DropDownList>
        </td>
    </tr>
    </table>    
    <table style="text-align:center;width:100%; margin: 0px auto; ">

    <tr id="Category" style="display:none">
    <td colspan="4" >
    <asp:Panel ID="pnTaskResp" runat="server" Width="100%">
    </asp:Panel> 
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <br />
    </td>
    </tr>
 </table>    
 <table style="text-align:center;width:80%; margin: 0px auto; ">

    <tr><td style="text-align:center;" colspan="4">&nbsp;</td></tr>

    <tr>
        <td style="text-align:center;" colspan="4">
            &nbsp;
            <input id="btnview" style="font-family: cambria; cursor: pointer; width: 5%; " 
                type="button" value="VIEW" onclick="return btnView_onclick()" />
            &nbsp;
            &nbsp;
            <input id="btnDraft" style="font-family: cambria; cursor: pointer; width: 5%; display:none" 
                type="button" value="DRAFT" onclick="return btnDraft_onclick()" />
            &nbsp;
            &nbsp;
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 5%; " 
                type="button" value="SAVE" onclick="return btnSave_onclick()" />
            &nbsp;
            &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 5%; " 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />


            <asp:HiddenField 
                ID="hid_dtls" runat="server" /> 
                <asp:HiddenField 
                ID="hid_temp" runat="server" />
        </td>
    </tr>
</table>    
</div>
<br /><br />
</asp:Content>

