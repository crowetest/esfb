﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO

Partial Class CTSMemoBulkUpload
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1253) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Upload Return Memo Files"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0

            DT = DB.ExecuteDataSet(" select b.Branch_ID,b.Branch_Name from EMP_MASTER e , BRANCH_MASTER b where e.Branch_ID=b.Branch_ID  and e.emp_code=" & CInt(Session("UserID")) & "").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
            cmbBranch.SelectedIndex = 0


            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "RequestOnchange();", True)

            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            cmbBranch.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent


    End Sub
#End Region

    Private Sub initializeControls()
        cmbBranch.Focus()

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim DateValue As Date = CDate(Data(1))
            Dim RequestID As Integer = 0
            Dim UserID As String = Session("UserID").ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim NoofAttachments As Integer = 0
            'If hfc.Count > 0 Then
            '    For i = 1 To hfc.Count - 1
            '        Dim myFile As HttpPostedFile = hfc(i)
            '        Dim nFileLen As Integer = myFile.ContentLength
            '        Dim FileName As String = ""
            '        If (nFileLen > 0) Then
            '            NoofAttachments += 1
            '        End If
            '    Next
            'End If

            Try
                'Dim Params(5) As SqlParameter
                'Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
                'Params(0).Value = Branch
                'Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                'Params(1).Value = UserID
                'Params(2) = New SqlParameter("@REQUEST_ID", SqlDbType.Int)
                'Params(2).Direction = ParameterDirection.Output
                'Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                'Params(3).Direction = ParameterDirection.Output
                'Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                'Params(4).Direction = ParameterDirection.Output
                'Params(5) = New SqlParameter("@NOOFATTACHMENTS", SqlDbType.Int)
                'Params(5).Value = NoofAttachments
                'DB.ExecuteNonQuery("SP_CTS_REQUEST_FROM_BRANCH", Params)
                'ErrorFlag = CInt(Params(3).Value)
                'Message = CStr(Params(4).Value)
                'RequestID = CInt(Params(2).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
            End Try
            'If ErrorFlag = 0 Then
            '    initializeControls()
            'End If
            If hfc.Count - 1 > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        Dim result As String
                        result = Path.GetFileNameWithoutExtension(FileName)
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                        Dim Params(6) As SqlParameter
                        Params(0) = New SqlParameter("@Userid", SqlDbType.Int)
                        Params(0).Value = CInt(Session("UserID"))
                        Params(1) = New SqlParameter("@EmpPhoto", SqlDbType.VarBinary)
                        Params(1).Value = AttachImg
                        Params(2) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                        Params(2).Value = ContentType
                        Params(3) = New SqlParameter("@date", SqlDbType.Date)
                        Params(3).Value = DateValue
                        Params(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 250)
                        Params(4).Value = result
                        Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(5).Direction = ParameterDirection.Output
                        Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(6).Direction = ParameterDirection.Output
                        DB.ExecuteNonQuery("SP_ATTACH_IMAGE_FILES", Params)
                        ErrorFlag = CInt(Params(5).Value)
                        Message = CStr(Params(6).Value)
                    End If

                Next
            End If

            'Dim AttachementNo As Integer = 0
            'DT = DB.ExecuteDataSet("select COUNT(PkId) from DMS_ESFB.dbo.CTS_REQUEST_ATTACH where Request_ID=" & RequestID & "").Tables(0)
            'If CInt(DT.Rows(0)(0)) >= 0 Then
            '    AttachementNo = CInt(DT.Rows(0)(0))
            '    Dim Err As Integer = DB.ExecuteNonQuery("update CTS_REQUEST_MASTER set NOOF_ATTACHMENTS=" & AttachementNo & " where REQUEST_ID=" & RequestID & "")
            'End If

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub


    Private Function DialogResult() As Object
        Throw New NotImplementedException
    End Function

End Class
