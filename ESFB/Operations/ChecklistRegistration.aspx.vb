﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CheckListRegistration
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1219) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            txtFromDt_CalendarExtender.StartDate = DateTime.Now
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Me.Master.subtitle = "Checklist Registration"
            If hid_error.Value = "999" Then
                Return
            End If
            If Not IsPostBack Then
                hid_tab.Value = CStr(1)
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "CategoryFill();", True)
            End If
            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            Me.drpcheck.Attributes.Add("onchange", "return FrequencyOnChange()")
            'Me.drpFreq.Attributes.Add("onchange", "return CategoryFreqOnChange()")
            hid_Edit.Value = CStr(0)
            hid_Id.Value = CStr(0)
            'Me.btnSave.Attributes.Add("onclick", "return Saveonclick()")
            'Me.btnFinish.Attributes.Add("onclick", "return Finishonclick()")
            'btnSave.Enabled = False
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim HDID As Integer = CInt(Data(0))
        Dim StatusID As Integer = 1
        Dim TabID As Integer = CInt(Data(1))

        If HDID = 2 Then 'fill category in panel
            Dim StrGrpnam As String = GF.GetChecklist(CStr(Session("UserID")))
            If StrGrpnam <> "" Then
                CallBackReturn = "1~" + CStr(StrGrpnam)
            Else
                CallBackReturn = "2~"
            End If
        ElseIf HDID = 3 Then 'fill category in combo
            DT = GF.GetActiveChecklistData(CStr(Session("UserID")))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf HDID = 4 Then 'fill sub category in panel
            If TabID <> -1 Then
                Dim StrModulenam As String = GF.GetTasklist(CInt(Data(2)), CStr(Session("UserID")))

                If StrModulenam = "" Then
                    CallBackReturn = "2~"

                Else
                    CallBackReturn = "1~" + CStr(StrModulenam)
                End If
            Else
                CallBackReturn = "2~"
            End If
        ElseIf HDID = 5 Then 'fill sub category in combo
            DT = HD.GetModule(CInt(Data(2)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf HDID = 6 Then 'fill findings in panel
            If TabID <> -1 Then
                Dim StrProblemnam As String = HD.GetProblemName(CInt(Data(2)))
                If StrProblemnam = "" Then
                    CallBackReturn = "2~"

                Else
                    CallBackReturn = "1~" + CStr(StrProblemnam)
                End If
            Else
                CallBackReturn = "2~"
            End If
        ElseIf HDID = 7 Then 'delete 
            If TabID = 1 Then 'category

                DT = GF.GetChecklistDataTable(CInt(Data(2)))
                If DT.Rows.Count > 0 Then
                    Dim StrModulenam As String = GF.GetActiveTasklist(CInt(Data(2)), CStr(Session("UserID")))
                    If StrModulenam <> "" Then
                        CallBackReturn = "2~Checklist can not be deleted. Task exist under this checklist.~"
                    Else
                        Dim RetVal As Integer = GF.DeleteChecklist(TabID, CInt(Data(2)), CStr(Session("UserID")))
                        If RetVal = 1 Then
                            CallBackReturn = "1~Deleted Successfully~"
                        Else
                            CallBackReturn = "2~Error Occured"
                        End If
                    End If
                Else
                    CallBackReturn = "2~Can't delete , Data not found ."
                End If


            ElseIf TabID = 2 Then 'Sub category

                DT = GF.GetTasklistDataTable(CInt(Data(2)))
                If DT.Rows.Count > 0 Then
                    Dim RetVal As Integer = GF.DeleteChecklist(TabID, CInt(Data(2)), CStr(Session("UserID")))
                    If RetVal = 1 Then
                        CallBackReturn = "1~Deleted Successfully~"
                    Else
                        CallBackReturn = "2~Error Occured"
                    End If
                Else
                    CallBackReturn = "2~Can't delete , Data not found ."
                End If


            ElseIf TabID = 3 Then 'Finding

                DT = HD.GetRequestexits(CInt(Data(2)))
                If DT.Rows.Count = 0 Then
                    Dim RetVal As Integer = HD.DeleteGroup(TabID, CInt(Data(2)))
                    If RetVal = 1 Then
                        CallBackReturn = "1~Deleted Successfully"
                    Else
                        CallBackReturn = "2~Error Occured"
                    End If
                Else
                    CallBackReturn = "2~Can't delete ,Requests received under this finding ."
                End If


            End If
        ElseIf HDID = 8 Then 'edit, approval assign 'shima 17/10
            Dim StrEdit As String = ""
            StrEdit = HD.EditMasterData(TabID, CInt(Data(2)))
            If StrEdit <> "" Then
                CallBackReturn = "1~" + StrEdit
            Else
                CallBackReturn = "2~"
            End If
        ElseIf HDID = 9 Then 'For finding the sum of weightage value
            DT = GF.GetSumWeightageValue(CInt(Data(2)))
            Dim weightage As Double = CDbl(Data(1))
            Dim val As Double = 0
            If DT.Rows(0).Item(0).ToString() = "" And weightage <> 100 Then
                val = 0
                CallBackReturn = "1~" + "This is the first task under this checklist. Please enter weightage value as 100"
            Else
                If DT.Rows(0).Item(0).ToString() <> "" Then
                    val = CDbl(DT.Rows(0).Item(0).ToString())
                Else
                    val = 0
                End If
                Dim curval As Double = CDbl(Data(1))
                Dim edit As Double = CDbl(Data(3))
                Dim message As String
                If (edit = 0.0) Then
                    If curval <> 100 - val Then
                        Dim remain As Double = 100 - val
                        message = "Sum of the weightage value should be 100. The remaining weightage value of this checklist is " + remain.ToString() + ".But you entered value is " + curval.ToString() + ". Please check"
                        CallBackReturn = "1~" + message
                    Else
                        CallBackReturn = "2~"
                        'btnSave.Enabled = True
                    End If
                ElseIf (edit = 1) Then
                    val = val - CDbl(Data(4))
                    Dim total As Double = val + curval
                    If curval > 100 - val Then
                        message = "Sum of the weightage value should be 100. Currently the total weightage is " + total.ToString() + ".Adjust the weightage values for getting total weightage as 100"
                        CallBackReturn = "2~" + message
                    ElseIf total <> 100 Then
                        message = "Sum of the weightage value should be 100. Currently the total weightage is " + total.ToString() + ".Adjust the weightage values for getting total weightage as 100"
                        CallBackReturn = "3~" + message
                        'btnSave.Enabled = True
                    Else
                        CallBackReturn = "3~" + ""
                    End If
                End If
            End If
        ElseIf HDID = 10 Then

            Try
                Dim Datanew() As String = eventArgument.Split(CChar("Ø"))
                Dim SubGrpName As String = CStr(Data(3))
                Dim IntGrpID As Integer = 0
                Dim Messagee As String = Nothing
                Dim ErrorFlag As Integer = 0
                Dim Remarks As String = ""
                Dim Freq As Integer = 0
                Dim ChkRemark As Integer
                Dim Weightagee As Double = 0.0
                Dim App_status As Integer = 0
                Dim Id As Integer = 0
                IntGrpID = CInt(Data(2))
                Id = CInt(Datanew(4))
                Remarks = CStr(Data(5))
                Freq = 0
                Weightagee = CDbl(Data(6))
                ChkRemark = CInt(Data(9))
                If Data.Length = 8 Then
                    StatusID = CInt(Data(7))
                    App_status = 0
                End If

                Dim Params(11) As SqlParameter
                Params(0) = New SqlParameter("@ChecklistID", SqlDbType.Int)
                Params(0).Value = IntGrpID
                Params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 5000)
                Params(1).Value = SubGrpName
                Params(2) = New SqlParameter("@StatusID", SqlDbType.Int)
                Params(2).Value = StatusID
                Params(3) = New SqlParameter("@TaskID", SqlDbType.Int)
                Params(3).Value = Id
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output

                Params(6) = New SqlParameter("@Remarks", SqlDbType.VarChar, 5000)
                Params(6).Value = Remarks
                Params(7) = New SqlParameter("@Weightage", SqlDbType.Money)
                Params(7).Value = Weightagee
                Params(8) = New SqlParameter("@Frequency", SqlDbType.Int)
                Params(8).Value = Freq
                Params(9) = New SqlParameter("@AppStatus", SqlDbType.Int)
                Params(9).Value = App_status
                Params(10) = New SqlParameter("@CreatedBy", SqlDbType.Int)
                Params(10).Value = CStr(Session("UserID"))
                Params(11) = New SqlParameter("@IsMandate", SqlDbType.Int)
                Params(11).Value = ChkRemark
                DB.ExecuteNonQuery("SP_BRANCH_TASKLIST_REG", Params)
                ErrorFlag = CInt(Params(4).Value)
                Messagee = CStr(Params(5).Value)
                If ErrorFlag = 0 Then
                    'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "IntialCtrl();", True)
                    CallBackReturn = "1~" + Messagee
                Else
                    CallBackReturn = "2~" + Messagee
                End If
            Catch ex As Exception
             
                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try
        ElseIf HDID = 11 Then
            Try
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Dim Dataa() As String = hid_Save.Value.Split(CChar("Ø"))
                Dim HDIDD As Integer = CInt(Data(0))
                Dim StatusIDD As Integer = 0
                Dim TabIDD As Integer = CInt(Data(1))
                'Dim Message As String = Nothing
                'Dim ErrorFlag As Integer = 0
                Dim Id As Integer = 0
                Dim Remarks As String = ""
                Dim Freq As Integer = 0
                Dim ChkRemark As Integer = 0
                Dim Weightage As Double = 0
                Dim App_status As Integer = 0
                Dim His_ID As Integer = 0
                Dim interval As Integer = 0
                Dim FromDateDt As String = ""
                If TabIDD = 1 Then
                    Dim CateName As String = CStr(Data(2))
                    Id = CInt(Data(3))
                    Remarks = CStr(Data(4))
                    Freq = CInt(Data(5))
                    App_status = 0
                    Dim dateval As DateTime
                    If Data.Length = 10 Then
                        StatusID = CInt(Data(6))
                        App_status = 0
                        His_ID = CInt(Data(7))
                        dateval = Convert.ToDateTime(Data(8))
                        FromDateDt = dateval.ToString("yyyy-MM-dd")
                        interval = CInt(Data(9))
                    Else
                        dateval = Convert.ToDateTime(Data(6))
                        FromDateDt = dateval.ToString("yyyy-MM-dd")
                        interval = CInt(Data(7))
                    End If
                    Dim Params(11) As SqlParameter
                    Params(0) = New SqlParameter("@StatusID", SqlDbType.Int)
                    Params(0).Value = StatusID
                    Params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 5000)
                    Params(1).Value = CateName
                    Params(2) = New SqlParameter("@Mode", SqlDbType.Int)
                    Params(2).Value = Id
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@Remarks", SqlDbType.VarChar, 5000)
                    Params(5).Value = Remarks
                    Params(6) = New SqlParameter("@Frequency", SqlDbType.Int)
                    Params(6).Value = Freq
                    Params(7) = New SqlParameter("@AppStatus", SqlDbType.Int)
                    Params(7).Value = App_status
                    Params(8) = New SqlParameter("@CreatedBy", SqlDbType.Int)
                    Params(8).Value = CStr(Session("UserID"))
                    Params(9) = New SqlParameter("@hisid", SqlDbType.Int)
                    Params(9).Value = His_ID
                    Params(10) = New SqlParameter("@AppliedOn", SqlDbType.VarChar, 100)
                    Params(10).Value = FromDateDt
                    Params(11) = New SqlParameter("@Interval", SqlDbType.Int)
                    Params(11).Value = interval
                    DB.ExecuteNonQuery("SP_BRANCH_CHECKLIST_REG", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                ElseIf TabIDD = 2 Then
                    Dim SubGrpName As String = CStr(Data(3))
                    Dim IntGrpID As Integer = 0
                    IntGrpID = CInt(Data(2))
                    Id = CInt(Data(4))
                    Remarks = CStr(Data(5))
                    Freq = 0
                    ChkRemark = CInt(Data(8))
                    Weightage = CDbl(Data(6))
                    If Data.Length = 9 Then
                        StatusID = CInt(Data(8))
                        App_status = 0
                    End If
                    Dim Params(11) As SqlParameter
                    Params(0) = New SqlParameter("@ChecklistID", SqlDbType.Int)
                    Params(0).Value = IntGrpID
                    Params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 5000)
                    Params(1).Value = SubGrpName
                    Params(2) = New SqlParameter("@StatusID", SqlDbType.Int)
                    Params(2).Value = 0
                    Params(3) = New SqlParameter("@TaskID", SqlDbType.Int)
                    Params(3).Value = Id
                    Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@Remarks", SqlDbType.VarChar, 5000)
                    Params(6).Value = Remarks
                    Params(7) = New SqlParameter("@Weightage", SqlDbType.Money)
                    Params(7).Value = Weightage
                    Params(8) = New SqlParameter("@Frequency", SqlDbType.Int)
                    Params(8).Value = Freq
                    Params(9) = New SqlParameter("@AppStatus", SqlDbType.Int)
                    Params(9).Value = 100
                    Params(10) = New SqlParameter("@CreatedBy", SqlDbType.Int)
                    Params(10).Value = CStr(Session("UserID"))
                    Params(11) = New SqlParameter("@IsMandate", SqlDbType.Int)
                    Params(11).Value = ChkRemark
                    DB.ExecuteNonQuery("SP_BRANCH_TASKLIST_REG", Params)
                    ErrorFlag = CInt(Params(4).Value)
                    Message = CStr(Params(5).Value)
                End If
                If ErrorFlag = 0 Then
                    CallBackReturn += "1~" + Message
                Else
                    CallBackReturn += "2~" + Message
                End If
            Catch ex As Exception

                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try
        ElseIf HDID = 12 Then
            Try
                Dim HDIDD As Integer = CInt(Data(0))
                Dim StatusIDD As Integer = 0
                Dim TabIDD As Integer = CInt(Data(1))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Dim Id As Integer = 0
                Dim Remarks As String = ""
                Dim Freq As Integer = 0
                Dim Weightage As Double = 0
                Dim App_status As Integer = 0
                Dim His_ID As Integer = 0
                Dim interval As Integer = 0
                Dim FromDateDt As String = ""
                If TabIDD = 2 Then
                    Dim IntGrpID As Integer = 0
                    IntGrpID = CInt(Data(2))
                    Dim Params(3) As SqlParameter
                    Params(0) = New SqlParameter("@ChecklistID", SqlDbType.Int)
                    Params(0).Value = IntGrpID
                    Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(1).Direction = ParameterDirection.Output
                    Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(2).Direction = ParameterDirection.Output
                    Params(3) = New SqlParameter("@CreatedBy", SqlDbType.Int)
                    Params(3).Value = CStr(Session("UserID"))
                    DB.ExecuteNonQuery("SP_BRANCH_TASKLIST_FINISH", Params)
                    ErrorFlag = CInt(Params(1).Value)
                    Message = CStr(Params(2).Value)
                End If
                If ErrorFlag = 0 Then
                    CallBackReturn += "1~" + Message
                Else
                    CallBackReturn += "2~" + Message
                End If
            Catch ex As Exception

                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try
        End If
    End Sub
    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    'Try
    '    Dim Dataa() As String = hid_Save.Value.Split(CChar("Ø"))
    '    Dim HDID As Integer = CInt(Dataa(0))
    '    Dim StatusID As Integer = 0
    '    Dim TabID As Integer = CInt(Dataa(1))
    '    Dim Message As String = Nothing
    '    Dim ErrorFlag As Integer = 0
    '    Dim Id As Integer = 0
    '    Dim Remarks As String = ""
    '    Dim Freq As Integer = 0
    '    Dim Weightage As Double = 0
    '    Dim App_status As Integer = 0
    '    Dim His_ID As Integer = 0
    '    Dim interval As Integer = 0
    '    Dim FromDateDt As String = ""
    '    Try
    '        If TabID = 1 Then
    '            Dim CateName As String = CStr(Data(2))
    '            Id = CInt(Data(3))
    '            Remarks = CStr(Data(4))
    '            Freq = CInt(Data(5))
    '            App_status = 0
    '            Dim dateval As DateTime
    '            If Data.Length = 10 Then
    '                StatusID = CInt(Data(6))
    '                App_status = 0
    '                His_ID = CInt(Data(7))
    '                dateval = Convert.ToDateTime(Data(8))
    '                FromDateDt = dateval.ToString("yyyy-MM-dd")
    '                interval = CInt(Data(9))
    '            Else
    '                dateval = Convert.ToDateTime(Data(6))
    '                FromDateDt = dateval.ToString("yyyy-MM-dd")
    '                interval = CInt(Data(7))
    '            End If
    '            Dim Params(11) As SqlParameter
    '            Params(0) = New SqlParameter("@StatusID", SqlDbType.Int)
    '            Params(0).Value = StatusID
    '            Params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 500)
    '            Params(1).Value = CateName
    '            Params(2) = New SqlParameter("@Mode", SqlDbType.Int)
    '            Params(2).Value = Id
    '            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
    '            Params(3).Direction = ParameterDirection.Output
    '            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
    '            Params(4).Direction = ParameterDirection.Output
    '            Params(5) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
    '            Params(5).Value = Remarks
    '            Params(6) = New SqlParameter("@Frequency", SqlDbType.Int)
    '            Params(6).Value = Freq
    '            Params(7) = New SqlParameter("@AppStatus", SqlDbType.Int)
    '            Params(7).Value = App_status
    '            Params(8) = New SqlParameter("@CreatedBy", SqlDbType.Int)
    '            Params(8).Value = CStr(Session("UserID"))
    '            Params(9) = New SqlParameter("@hisid", SqlDbType.Int)
    '            Params(9).Value = His_ID
    '            Params(10) = New SqlParameter("@AppliedOn", SqlDbType.VarChar, 100)
    '            Params(10).Value = FromDateDt
    '            Params(11) = New SqlParameter("@Interval", SqlDbType.Int)
    '            Params(11).Value = interval
    '            DB.ExecuteNonQuery("SP_BRANCH_CHECKLIST_REG", Params)
    '            ErrorFlag = CInt(Params(3).Value)
    '            Message = CStr(Params(4).Value)
    '        ElseIf TabID = 2 Then
    '            Dim SubGrpName As String = CStr(Data(3))
    '            Dim IntGrpID As Integer = 0
    '            IntGrpID = CInt(Data(2))
    '            Id = CInt(Data(4))
    '            Remarks = CStr(Data(5))
    '            Freq = 0
    '            Weightage = CDbl(Data(6))
    '            If Data.Length = 9 Then
    '                StatusID = CInt(Data(8))
    '                App_status = 0
    '            End If
    '            Dim Params(10) As SqlParameter
    '            Params(0) = New SqlParameter("@ChecklistID", SqlDbType.Int)
    '            Params(0).Value = IntGrpID
    '            Params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 500)
    '            Params(1).Value = SubGrpName
    '            Params(2) = New SqlParameter("@StatusID", SqlDbType.Int)
    '            Params(2).Value = 100
    '            Params(3) = New SqlParameter("@TaskID", SqlDbType.Int)
    '            Params(3).Value = Id
    '            Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
    '            Params(4).Direction = ParameterDirection.Output
    '            Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
    '            Params(5).Direction = ParameterDirection.Output
    '            Params(6) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
    '            Params(6).Value = Remarks
    '            Params(7) = New SqlParameter("@Weightage", SqlDbType.Money)
    '            Params(7).Value = Weightage
    '            Params(8) = New SqlParameter("@Frequency", SqlDbType.Int)
    '            Params(8).Value = Freq
    '            Params(9) = New SqlParameter("@AppStatus", SqlDbType.Int)
    '            Params(9).Value = 100
    '            Params(10) = New SqlParameter("@CreatedBy", SqlDbType.Int)
    '            Params(10).Value = CStr(Session("UserID"))
    '            DB.ExecuteNonQuery("SP_BRANCH_TASKLIST_REG", Params)
    '            ErrorFlag = CInt(Params(4).Value)
    '            Message = CStr(Params(5).Value)
    '            'ElseIf TabID = 3 Then
    '            '    Dim IntSubGrpID As Integer = 0
    '            '    IntSubGrpID = CInt(Data(2))
    '            '    Dim SubProblemName As String = CStr(Data(3))
    '            '    Id = CInt(Data(4))
    '            '    Dim App As String = CStr(Data(6))
    '            '    Dim Appmode As Integer = 0
    '            '    Appmode = CInt(Data(5))
    '            '    Dim Description As String = CStr(Data(7))
    '            '    Dim Critical As Integer = CInt(Data(8))

    '            '    Dim AttachImg As Byte() = Nothing
    '            '    Dim ContentType As String = ""
    '            '    AttachImg = Nothing
    '            '    Dim FileName As String = ""
    '            '    Dim myFile As HttpPostedFile '= fupAnnexture.PostedFile
    '            '    Dim nFileLen As Integer = myFile.ContentLength
    '            '    If (nFileLen > 0) Then
    '            '        ContentType = myFile.ContentType
    '            '        AttachImg = New Byte(nFileLen - 1) {}
    '            '        myFile.InputStream.Read(AttachImg, 0, nFileLen)
    '            '        FileName = myFile.FileName
    '            '    End If

    '            '    Dim Params(12) As SqlParameter
    '            '    Params(0) = New SqlParameter("@StatusID", SqlDbType.Int)
    '            '    Params(0).Value = StatusID
    '            '    Params(1) = New SqlParameter("@Problem", SqlDbType.VarChar, 500)
    '            '    Params(1).Value = SubProblemName
    '            '    Params(2) = New SqlParameter("@SubGrpID", SqlDbType.Int)
    '            '    Params(2).Value = IntSubGrpID
    '            '    Params(3) = New SqlParameter("@ID", SqlDbType.Int)
    '            '    Params(3).Value = Id
    '            '    Params(4) = New SqlParameter("@APP", SqlDbType.VarChar, 25)
    '            '    Params(4).Value = App.Substring(1)
    '            '    Params(5) = New SqlParameter("@APPMODE", SqlDbType.Int)
    '            '    Params(5).Value = Appmode
    '            '    Params(6) = New SqlParameter("@Descrpt", SqlDbType.VarChar, 500)
    '            '    Params(6).Value = Description
    '            '    Params(7) = New SqlParameter("@IsCritical", SqlDbType.Int)
    '            '    Params(7).Value = Critical
    '            '    Params(8) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
    '            '    Params(8).Value = AttachImg
    '            '    Params(9) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
    '            '    Params(9).Value = ContentType
    '            '    Params(10) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
    '            '    Params(10).Value = FileName
    '            '    Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
    '            '    Params(11).Direction = ParameterDirection.Output
    '            '    Params(12) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
    '            '    Params(12).Direction = ParameterDirection.Output

    '            '    DB.ExecuteNonQuery("SP_HD_PROBLEM", Params)
    '            '    ErrorFlag = CInt(Params(11).Value)
    '            '    Message = CStr(Params(12).Value)

    '        End If
    '        If ErrorFlag = 0 Then
    '            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "IntialCtrl();", True)
    '        Else
    '            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
    '            'MsgBox(Message)
    '            cl_script1.Append("         alert('" + Message + "');")
    '            cl_script1.Append("         window.open('CheckListRegistration.aspx','_self');")
    '            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    '            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "ModuleClick();", True)
    '            'hid_error.Value = "999"
    '        End If
    '    Catch ex As Exception
    '        Message = ex.Message.ToString
    '        ErrorFlag = 1
    '    End Try
    'Catch ex As Exception
    '    If Response.IsRequestBeingRedirected Then
    '        Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
    '        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
    '    End If
    'End Try
    'End Sub
    'Protected Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
    '    Try
    '        Dim Data() As String = hid_Save.Value.Split(CChar("Ø"))
    '        Dim HDID As Integer = CInt(Data(0))
    '        Dim StatusID As Integer = 0
    '        Dim TabID As Integer = CInt(Data(1))
    '        Dim Message As String = Nothing
    '        Dim ErrorFlag As Integer = 0
    '        Dim Id As Integer = 0
    '        Dim Remarks As String = ""
    '        Dim Freq As Integer = 0
    '        Dim Weightage As Double = 0
    '        Dim App_status As Integer = 0
    '        Dim His_ID As Integer = 0
    '        Dim interval As Integer = 0
    '        Dim FromDateDt As String = ""
    '        Try
    '            If TabID = 2 Then
    '                'Dim SubGrpName As String = CStr(Data(3))
    '                Dim IntGrpID As Integer = 0
    '                IntGrpID = CInt(Data(2))
    '                'Id = CInt(Data(4))
    '                'Remarks = CStr(Data(5))
    '                'Freq = 0
    '                'Weightage = CDbl(Data(6))
    '                'If Data.Length = 9 Then
    '                '    StatusID = CInt(Data(8))
    '                '    App_status = 0
    '                'End If
    '                Dim Params(3) As SqlParameter
    '                Params(0) = New SqlParameter("@ChecklistID", SqlDbType.Int)
    '                Params(0).Value = IntGrpID
    '                'Params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 500)
    '                'Params(1).Value = SubGrpName
    '                'Params(2) = New SqlParameter("@StatusID", SqlDbType.Int)
    '                'Params(2).Value = 100
    '                'Params(3) = New SqlParameter("@TaskID", SqlDbType.Int)
    '                'Params(3).Value = Id
    '                Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
    '                Params(1).Direction = ParameterDirection.Output
    '                Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
    '                Params(2).Direction = ParameterDirection.Output
    '                'Params(6) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
    '                'Params(6).Value = Remarks
    '                'Params(7) = New SqlParameter("@Weightage", SqlDbType.Money)
    '                'Params(7).Value = Weightage
    '                'Params(8) = New SqlParameter("@Frequency", SqlDbType.Int)
    '                'Params(8).Value = Freq
    '                'Params(9) = New SqlParameter("@AppStatus", SqlDbType.Int)
    '                'Params(9).Value = 100
    '                Params(3) = New SqlParameter("@CreatedBy", SqlDbType.Int)
    '                Params(3).Value = CStr(Session("UserID"))
    '                DB.ExecuteNonQuery("SP_BRANCH_TASKLIST_FINISH", Params)
    '                ErrorFlag = CInt(Params(1).Value)
    '                Message = CStr(Params(2).Value)
    '                'ElseIf TabID = 3 Then
    '                '    Dim IntSubGrpID As Integer = 0
    '                '    IntSubGrpID = CInt(Data(2))
    '                '    Dim SubProblemName As String = CStr(Data(3))
    '                '    Id = CInt(Data(4))
    '                '    Dim App As String = CStr(Data(6))
    '                '    Dim Appmode As Integer = 0
    '                '    Appmode = CInt(Data(5))
    '                '    Dim Description As String = CStr(Data(7))
    '                '    Dim Critical As Integer = CInt(Data(8))

    '                '    Dim AttachImg As Byte() = Nothing
    '                '    Dim ContentType As String = ""
    '                '    AttachImg = Nothing
    '                '    Dim FileName As String = ""
    '                '    Dim myFile As HttpPostedFile '= fupAnnexture.PostedFile
    '                '    Dim nFileLen As Integer = myFile.ContentLength
    '                '    If (nFileLen > 0) Then
    '                '        ContentType = myFile.ContentType
    '                '        AttachImg = New Byte(nFileLen - 1) {}
    '                '        myFile.InputStream.Read(AttachImg, 0, nFileLen)
    '                '        FileName = myFile.FileName
    '                '    End If

    '                '    Dim Params(12) As SqlParameter
    '                '    Params(0) = New SqlParameter("@StatusID", SqlDbType.Int)
    '                '    Params(0).Value = StatusID
    '                '    Params(1) = New SqlParameter("@Problem", SqlDbType.VarChar, 500)
    '                '    Params(1).Value = SubProblemName
    '                '    Params(2) = New SqlParameter("@SubGrpID", SqlDbType.Int)
    '                '    Params(2).Value = IntSubGrpID
    '                '    Params(3) = New SqlParameter("@ID", SqlDbType.Int)
    '                '    Params(3).Value = Id
    '                '    Params(4) = New SqlParameter("@APP", SqlDbType.VarChar, 25)
    '                '    Params(4).Value = App.Substring(1)
    '                '    Params(5) = New SqlParameter("@APPMODE", SqlDbType.Int)
    '                '    Params(5).Value = Appmode
    '                '    Params(6) = New SqlParameter("@Descrpt", SqlDbType.VarChar, 500)
    '                '    Params(6).Value = Description
    '                '    Params(7) = New SqlParameter("@IsCritical", SqlDbType.Int)
    '                '    Params(7).Value = Critical
    '                '    Params(8) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
    '                '    Params(8).Value = AttachImg
    '                '    Params(9) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
    '                '    Params(9).Value = ContentType
    '                '    Params(10) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
    '                '    Params(10).Value = FileName
    '                '    Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
    '                '    Params(11).Direction = ParameterDirection.Output
    '                '    Params(12) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
    '                '    Params(12).Direction = ParameterDirection.Output

    '                '    DB.ExecuteNonQuery("SP_HD_PROBLEM", Params)
    '                '    ErrorFlag = CInt(Params(11).Value)
    '                '    Message = CStr(Params(12).Value)

    '            End If
    '            If ErrorFlag = 0 Then
    '                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "IntialCtrl();", True)
    '            Else
    '                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
    '                'MsgBox(Message)
    '                cl_script1.Append("         alert('" + Message + "');")
    '                cl_script1.Append("         window.open('CheckListRegistration.aspx','_self');")
    '                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    '                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "ModuleClick();", True)
    '                'hid_error.Value = "999"
    '            End If
    '        Catch ex As Exception
    '            Message = ex.Message.ToString
    '            ErrorFlag = 1
    '        End Try
    '    Catch ex As Exception
    '        If Response.IsRequestBeingRedirected Then
    '            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
    '            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
    '        End If
    '    End Try
    'End Sub
End Class
