﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true" CodeFile="CCL_Application.aspx.vb" Inherits="CCL_Application" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
           var rowLen;
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");       
        for (a = 0; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function ClearCombo(control) {
        document.getElementById(control).options.length = 0;
        var option1 = document.createElement("OPTION");
        option1.value = -1;
        option1.text = " -----Select-----";
        document.getElementById(control).add(option1);
    }  
    function FromServer(arg, context) 
    {
        switch (context) 
        {
            case 1:
                {
                    var data = arg.split("Ø");
                    var id = data[0];
                    var SangamID = data[1];
                    document.getElementById("<%= hidRowData.ClientID %>").value = data[2];
                    TableFill();
                    break;
                }
            case 2:
                {    
                    var data = arg.split("Ø");
                    var id = data[3];
                    var SangamID = data[2];
                    for(var i = 1;i<=id;i++){    
                        window.open("Reports/CCLApplicationView.aspx?center=" + SangamID + "&ID=" + i + "", "_blank");
                    }     
                    window.open("CCL_Application.aspx", "_self");               
                }
        }
    }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }   
    function btnGenerate_onclick() {
        var SangamID = document.getElementById("<%= cmbSangam.ClientID %>").value;
        if (SangamID == -1){
            alert("Select Sangam");
            return false;
        } 
         
        var newStr = "";
        for (var i = 1; i <= rowLen; i++) {
            if (document.getElementById("chk_" + i).checked == true) {
                var clientID = document.getElementById("client_" + i).innerHTML;
                newStr += clientID + "µ¥";
            }
        }
        if(newStr == ""){
            alert("No client selected");
            return false;
        }
        var ToData = "2Ø" + SangamID.toString()+ "Ø" + newStr;
        ToServer(ToData, 2);
    }
    function SangamOnChange(){
        var SangamID = document.getElementById("<%= cmbSangam.ClientID %>").value;
        if (SangamID == -1){
            alert("Select Sangam");
            return false;
        }
        var ToData = "1Ø" + SangamID.toString();
            ToServer(ToData, 1);
    }
    function TableFill() {
            document.getElementById("clientTr").style.display = '';
            document.getElementById("<%= clientPanel.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<br /><div style='width:90%; height:auto;background-color:#A34747; overflow:auto;text-align:left;margin: 0px auto;' align='left'>";
            tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
            tab += "<tr class=mainhead>";
            tab += "<td style='width:10%;text-align:center'>#</td>";
            tab += "<td style='width:10%;text-align:center'>Sl.No</td>";
            tab += "<td style='width:25%;text-align:center' >Client ID</td>";
            tab += "<td style='width:30%;text-align:center;'>Client Name</td>";
            tab += "<td style='width:25%;text-align:center;'>Eligible Loan Amount</td>";
            tab += "</tr>";
            var rowData = document.getElementById("<%= hidRowData.ClientID %>").value.split("¥");
            rowLen = rowData.length;
            for (var i = 0; i < rowData.length; i++) {
                var data = rowData[i].toString();
                var rData = data.split("µ");
                tab += "<tr class=sub_first style ='height:auto;'>";
                tab += "<td style='width:10%;text-align:center'><input type='checkbox' id='chk_" + (i + 1) + "' name='chk_" + (i + 1) + "' onchange='return CheckOnChange("+(i+1)+")'></td>";
                tab += "<td style='width:10%;text-align:center'>" + (i+1) + "</td>";
                tab += "<td style='width:25%;text-align:center' id='client_"+(i+1)+"'>" + rData[0].toString() + "</td>";
                tab += "<td style='width:30%;text-align:center'>" + rData[1].toString() + "</td>";
                tab += "<td style='width:25%;text-align:center' id='eligibleAmount_"+(i+1)+"'>" + rData[2].toString() + "</td>";                              
                tab += "</tr>";
            }
            tab += "</table>";
            tab += "</div>";
            document.getElementById("<%= clientPanel.ClientID %>").innerHTML = tab;
        }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return RequestOnchange()
            // ]]>
        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 100%; margin: 0px auto;">
        <tr id="branch">
            <td style="width: 25%;">
            <asp:HiddenField ID="hidRowData" runat="server" />
            </td>
            <td style="width: 25%; text-align: center;padding left:10%;">
                Branch
            </td>
            <td style="width: 25%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="width: 25%;">
            </td>
        </tr>
        <tr id="Sangam">
            <td style="width: 25%;">
            </td>
            <td style="width: 25%; text-align: center;padding left:10%;">
                Sangam
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbSangam" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="clientTr" style="display:none">
            <td style="width: 25%;">
            </td>
            <td style="width: 25%; text-align: left;" colspan="2">
                <asp:Panel id="clientPanel" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="4">
                <br />
                <input id="btnGenerate" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                    value="GENERATE" onclick="return btnGenerate_onclick()" />&nbsp;
&nbsp;
&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

