﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ViewOperatingManual.aspx.vb" Inherits="Operations_ViewOperatingManual" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
 <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
 <style type="text/css">
 .sub_first
{
   background-color:white; height:20px;
   font-family:Cambria;color:#47476B;font-size:5px;
}
 </style>
<br /><br /><br /><br />
<div   style="width: 60%;height:200px; margin:0px auto; background-color:silver">
    <table align="center" style="width: 100%; margin:0px auto;"  >
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                1</td>
            <td style="width:80%;text-align:left;">
                Branch Operation Manual</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/OperatingManual/Branch Operations Manual.pdf' target="_blank"  >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                2</td>
            <td style="width:80%;text-align:left;">
                CASA Operations Manual</td>
            <td style="width:10%;text-align:center;">'
                <a href='http://eweb.emfil.org/manual/OperatingManual/CASA Operations Manual.pdf' target="_blank"  >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                3</td>
            <td style="width:80%;text-align:left;">
                Cash  Operations Manual</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/OperatingManual/Cash  Operations Manual.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                4</td>
            <td style="width:80%;text-align:left;">
                Clearing  Operation Manual</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/OperatingManual/Clearing  Operation Manual.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                5</td>
            <td style="width:80%;text-align:left;">
                Forex Operations manual</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/OperatingManual/forex op manual.pdf' target="_blank"  >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                6</td>
            <td style="width:80%;text-align:left;">
                Locker Operations Manual</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/OperatingManual/Locker Operations Manual.pdf' target="_blank"  >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                7</td>
            <td style="width:80%;text-align:left;">
               NR Deposits Operations Manual</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/OperatingManual/NR Deposits Operations Manual.pdf' target="_blank"  >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                8</td>
            <td style="width:80%;text-align:left;">
               Remittances Operations Manual</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/OperatingManual/Remittances _Operations Manual.pdf' target="_blank"  >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                9</td>
            <td style="width:80%;text-align:left;">
               Term Deposits Operations Manual</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/OperatingManual/Term Deposits Operations Manual.pdf' target="_blank"  >View</a></td>
        </tr>
        <tr class="sub_first" style="height:30px;">
            <td style="text-align:center;" colspan="3">
                <input id="btnExit" type="button" value="EXIT" style="width:10%; font-family:Cambria;" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table></div>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../Home.aspx","_self");
        }

// ]]>
    </script>
</asp:Content>

