﻿
Partial Class Operations_viewDownload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim RequestID As Integer = CInt(Request.QueryString.Get("ID"))
        If RequestID = 1 Then
            iframe.Attributes.Add("src", "http://eweb.emfil.org/Manual/Downloads/Form 15G.pdf")
        ElseIf RequestID = 2 Then
            iframe.Attributes.Add("src", "http://eweb.emfil.org/Manual/Downloads/Form 15H.pdf")
        End If
    End Sub
End Class
