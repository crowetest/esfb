﻿
Partial Class Operations_viewPolicy
    Inherits System.Web.UI.Page
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim ID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("ID")))
        If Not IsPostBack Then
            If ID = 1 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Accounting%20Policy_ESAF%20SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 2 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/ALM and MR Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 3 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/AML KYC Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 4 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Compliance Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 5 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Credit Policy_ESAF SFB_27.06.2017.pdf&embedded=true#toolbar=0")
            ElseIf ID = 6 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Credit Risk Management Policy_ESA SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 7 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Customer Grievance Redressal Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 8 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/ESAF SFB Risk based Internal Audit Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 9 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Fair Practice and Customer Service Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 10 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/FATCA Policy_ESAF SFB_30.03.2017.pdf&embedded=true#toolbar=0")
            ElseIf ID = 11 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Fraud Risk Management Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 12 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/HR Policies_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 13 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/ICAAP Policy_ESAF SFB_30.03.2017.pdf&embedded=true#toolbar=0")
            ElseIf ID = 14 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Investment Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 15 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Liquidity Management Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 16 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/New Product Approval Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 17 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/NPA Management Provisioning Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 18 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Outsourcing Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 19 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Policy to avoid mis-selling_ESAF SFB_03.03.2017.pdf&embedded=true#toolbar=0")
            ElseIf ID = 20 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Premises Acquisition Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 21 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Procurement Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 22 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Related Party Transaction Policy_ESAF SFB_30.03.2017.pdf&embedded=true#toolbar=0")
            ElseIf ID = 23 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Risk Management  Governance Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 24 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Stress Testing Policy_ESAF SFB.pdf&embedded=true#toolbar=0")
            ElseIf ID = 25 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Vehicle Management Policy_ESAF SFB_30.03.2017.pdf&embedded=true#toolbar=0")
            ElseIf ID = 26 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/Nonit/Whistle blower policy_ESAF SFB_03.03.2017.pdf&embedded=true#toolbar=0")
            ElseIf ID = 27 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/1.0 Change Management Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 28 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/2.0 Migration Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 29 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/3.0 Asset and Media Disposal Policy and Procedures V1.1.pdf&embedded=true#toolbar=0")
            ElseIf ID = 30 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/4.0 IT Outsourcing Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 31 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/5.0 Third party vendor Management Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 32 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/6.0 Exception Handling Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 33 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/7.0 Physical and Environmental Security Policy and Procedures V1.1.pdf&embedded=true#toolbar=0")
            ElseIf ID = 34 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/8.0 Password Policy and Procedures V1.1.pdf&embedded=true#toolbar=0")
            ElseIf ID = 35 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/9.0 Operating System Security Policy and Procedures V1.1.pdf&embedded=true#toolbar=0")
            ElseIf ID = 36 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/10.0 Database Security Policy and Procedures V1.1.pdf&embedded=true#toolbar=0")
            ElseIf ID = 37 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/11.0 Firewall Policy and Procedures V1.1.pdf&embedded=true#toolbar=0")
            ElseIf ID = 38 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/12.0 Network Security Policy and Procedures V1.1.pdf&embedded=true#toolbar=0")
            ElseIf ID = 39 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/13.0 Patch Management Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 40 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/14.0 Incident Management Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 41 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/15.0 Email Security Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 42 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/16.0 Acceptable Usage Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 43 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/17.0 Personnel Security Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 44 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/18.0 IT Asset and Information Classification Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 45 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/19.0 Anti-virus Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 46 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/20.0 Cryptography Control Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 47 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/21.0 Wireless Access Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 48 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/22.0 Application Security Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 49 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/23.0 Software Development Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 50 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/24.0 Backup and Archival Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 51 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/25.0 Access Control Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 52 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/26.0 IT Service Continuity Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 53 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/27.0 Capacity management Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 54 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/28.0 e-Channels Security Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 55 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/29.0 Mobile computing Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 56 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/30.0 Cloud Security  Governance Policy and Procedures V1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 57 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/31.0 Cyber Security Policy Ver 1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 58 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/32.0 Website Usage Policy Ver 1.0.pdf&embedded=true#toolbar=0")
            ElseIf ID = 59 Then
                Me.iframe.Attributes.Add("src", "http://docs.google.com/viewer?url=http://eweb.emfil.org/manual/policies/IT/33.0 Privacy Policy Ver 1.0.pdf&embedded=true#toolbar=0")

            End If
        End If

    End Sub


End Class
