﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CCL_Entry.aspx.vb" Inherits="CCL_Entry" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
 <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">   </asp:ToolkitScriptManager>
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 25%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script src="../../Script/jquery.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var rowLen;
        window.onload = function () {
            
        }
        Date.prototype.toShortFormat = function () {
            var month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var day = this.getDate();
            var month_index = this.getMonth();
            var year = this.getFullYear();
            return "" + day + " " + month_names[month_index] + " " + year;
        }
        function AlphaSpaceCheck(e) {
            var inputValue = e.which;
            if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0 && inputValue != 121)) {
                e.preventDefault();
            }
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                document.getElementById("<%= hidRowData.ClientID %>").value = Data[0];
                document.getElementById("<%= txtCenterName.ClientID %>").value = Data[1];
                TableFill();
            } else if (context == 2) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) {
                    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
                }
            }
        }
        function btnConfirm_onclick() {
            var branchCode = document.getElementById("<%= txtBranchCode.ClientID %>").value;
            var centerID = document.getElementById("<%= cmbCenterID.ClientID %>").value;
            var newStr = "";
            for (var i = 1; i <= rowLen; i++) {
                if (document.getElementById("chk_" + i).checked == true) {
                    var clientID = document.getElementById("client_" + i).innerHTML;
                    var customerAmount = document.getElementById("customerAmount_" + i).value;
                    var branchAmount = document.getElementById("branchAmount_" + i).value;
                    var eligibleAmount = document.getElementById("eligibleAmount_" + i).innerHTML;
                    if(customerAmount == -1){
                        alert("Select customer requested amount for all selected clients");
                        return false;
                    }
                    if(branchAmount == -1){
                        alert("Select branch recommended amount for all selected clients");
                        return false;
                    }

                    //new begin
                    if(Number(customerAmount) > Number(eligibleAmount)){
                        alert("The client can only request upto "+eligibleAmount +"for client - "+clientID);
                        return false;
                    }
                    if(Number(customerAmount) <= Number(eligibleAmount)){
                        if(Number(branchAmount) > Number(customerAmount)){
                            alert("The branch can only recommend upto "+customerAmount +"for client - "+clientID);
                            return false;
                        }
                    }else{
                        if(Number(branchAmount) > Number(eligibleAmount)){
                            alert("The branch can only recommend upto "+eligibleAmount +"for client - "+clientID);
                            return false;
                        }
                    }
                    //new end

                    newStr += clientID + "µ" + customerAmount + "µ" + branchAmount + "¥";
                }
            }
            var ToData = "2Ø" + branchCode + "Ø" + centerID + "Ø" + newStr;
            ToServer(ToData, 2);
        }
        function CenterIDOnchange() {
            var centerId = document.getElementById("<%= cmbCenterID.ClientID %>").value;
            var branchId = document.getElementById("<%= txtBranchCode.ClientID %>").value;
            var ToData = "1Ø" + centerId.toString() + "Ø" + branchId.toString();
            ToServer(ToData, 1);
        }
        function TableFill() {
            document.getElementById("<%= clientPanel.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto;background-color:#A34747; overflow:auto;text-align:left;margin: 0px auto;' align='left'>";
            tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
            tab += "<tr class=mainhead>";
            tab += "<td style='width:4%;text-align:center'>#</td>";
            tab += "<td style='width:4%;text-align:center'>Sl.No</td>";
            tab += "<td style='width:15%;text-align:center' >Client ID</td>";
            tab += "<td style='width:18%;text-align:center;'>Client Name</td>";
            tab += "<td style='width:15%;text-align:center;'>Frequency</td>";
            tab += "<td style='width:14%;text-align:center;'>Eligible Loan Amount</td>";
            tab += "<td style='width:15%;text-align:center;'>Customer Requested Amount</td>";
            tab += "<td style='width:15%;text-align:center;'>Branch Recommended Amount</td>";
            tab += "</tr>";
            var rowData = document.getElementById("<%= hidRowData.ClientID %>").value.split("¥");
            rowLen = rowData.length;
            for (var i = 0; i < rowData.length; i++) {
                var data = rowData[i].toString();
                var rData = data.split("µ");
                tab += "<tr class=sub_first style ='height:auto;'>";
                tab += "<td style='width:4%;text-align:center'><input type='checkbox' id='chk_" + (i + 1) + "' name='chk_" + (i + 1) + "' onchange='return CheckOnChange("+(i+1)+")'></td>";
                tab += "<td style='width:4%;text-align:center'>" + (i+1) + "</td>";
                tab += "<td style='width:15%;text-align:center' id='client_"+(i+1)+"'>" + rData[0].toString() + "</td>";
                tab += "<td style='width:18%;text-align:center'>" + rData[1].toString() + "</td>";
                tab += "<td style='width:15%;text-align:center;'>" + rData[3].toString() + "</td>";
                tab += "<td style='width:14%;text-align:center' id='eligibleAmount_"+(i+1)+"'>" + rData[2].toString() + "</td>";
                var select1 = "<select onchange='return customerAmountChange(" + (i + 1) + ")' id='customerAmount_" + (i + 1) + "' class='NormalText' name='customerAmount_" + (i + 1) + "' style='width:97.5%;text-align:center;' disabled='true'>";
                select1 += "<option value='-1'>-----SELECT-----</option>";
                select1 += "<option value='0'>0</option>";
                select1 += "<option value='5000'>5000</option>";
                select1 += "<option value='10000'>10000</option>";
                select1 += "<option value='15000'>15000</option>";
                select1 += "<option value='20000'>20000</option>";
                select1 += "<option value='25000'>25000</option>";
                select1 += "<option value='30000'>30000</option>";
                tab += "<td style='width:15%;text-align:center;'>"+select1+"</td>";
                var select2 = "<select onchange='return branchAmountChange(" + (i + 1) + ")' id='branchAmount_" + (i + 1) + "' class='NormalText' name='branchAmount_" + (i + 1) + "' style='width:97.5%;text-align:center;' disabled='true'>";
                select2 += "<option value='-1'>-----SELECT-----</option>";
                select2 += "<option value='0'>0</option>";
                select2 += "<option value='5000'>5000</option>";
                select2 += "<option value='10000'>10000</option>";
                select2 += "<option value='15000'>15000</option>";
                select2 += "<option value='20000'>20000</option>";
                select2 += "<option value='25000'>25000</option>";
                select2 += "<option value='30000'>30000</option>";
                tab += "<td style='width:15%;text-align:center;'>"+select2+"</td>";                
                tab += "</tr>";
            }
            tab += "</table>";
            tab += "</div>";
            document.getElementById("<%= clientPanel.ClientID %>").innerHTML = tab;
        }
        function CheckOnChange(i) {
            if (document.getElementById("chk_" + i).checked == true) {
                document.getElementById("customerAmount_" + i).disabled = false;
                document.getElementById("branchAmount_" + i).disabled = false;        
            } else {
                document.getElementById("customerAmount_" + i).disabled = true;
                document.getElementById("branchAmount_" + i).disabled = true;
            }
        }
        function customerAmountChange(i){
            var customerAmount = document.getElementById("customerAmount_" + i).value;
            var eligibleAmount = document.getElementById("eligibleAmount_" + i).innerHTML;
            if(Number(customerAmount) > Number(eligibleAmount)){
                alert("The client can only request upto "+eligibleAmount);
                document.getElementById("customerAmount_" + i).value = -1;
                return false;
            }
        }
        function branchAmountChange(i){
            var customerAmount = document.getElementById("customerAmount_" + i).value;
            var branchAmount = document.getElementById("branchAmount_" + i).value;
            var eligibleAmount = document.getElementById("eligibleAmount_" + i).innerHTML;
            if(Number(customerAmount) <= Number(eligibleAmount)){
                if(Number(branchAmount) > Number(customerAmount)){
                    alert("The branch can only recommend upto "+customerAmount);
                    document.getElementById("branchAmount_" + i).value = -1;
                    return false;
                }
            }else{
                if(Number(branchAmount) > Number(eligibleAmount)){
                    alert("The branch can only recommend upto "+eligibleAmount);
                    document.getElementById("branchAmount_" + i).value = -1;
                    return false;
                }
            }
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
    </script>   
</head>
</html>
<br />
<div  style="width:90%;margin:0px auto; background-color:#EEB8A6;">
    <br />
    <div id = "divSection1" class = "sec1" style="width:97%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">
            <tr> 
                <td style="width:25%;">
                    <asp:HiddenField ID="hidPDD" runat="server" />
                    <asp:HiddenField ID="hidRowData" runat="server" />
                </td>
                <td style="width:25%; text-align:left;"></td>
                <td style="width:25%"></td>
                <td style="width:25%"></td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Branch Code
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtBranchCode" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%"></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Branch Name
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtBranchName" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%"></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Center ID
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:DropDownList ID="cmbCenterID" runat="server" class="NormalText" Width="100%">
                       <asp:ListItem Value="-1"> -----SELECT-----</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Center Name
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtCenterName" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width:25%;" colspan="4">
                    <br />
                </td>
            </tr>
            <tr>
                <td style="width:25%;" colspan="4">
                    <asp:Panel ID="clientPanel" runat="server">
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="text-align:center;" colspan="2"><br />
                    <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="SAVE" onclick="return btnConfirm_onclick()"/>
                    &nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
                </td>
            </tr>
        </table>
    </div>
    <br />
</div>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

