﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class RepaymentMoratoriumStatus
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1418) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim UserID As String = CStr(Session("UserID"))
            Dim BranchID As String = CStr(Session("BranchID"))
            
            If Not IsPostBack Then
                DT3 = DB.ExecuteDataSet("SELECT '0' As CenterID,'--SELECT--' CenterName UNION ALL Select distinct CenterID, CenterName FROM BRD_NonMoratoriumCustomers where Repayment_Start_Date is null and  BranchCode = " + BranchID.ToString() + " ORDER BY CenterID").Tables(0)
                GF.ComboFill(cmbcentre, DT3, 0, 0)


            End If
            cmbcentre.Attributes.Add("onchange", "CentreOnChange()")


            Me.Master.subtitle = "Repayment Moratorium Status"
            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", " DisplayGroup();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then

            Dim Created_By As Integer = CInt(Session("UserID"))
            Dim centerid As Integer = CInt(Data(1).ToString())
            Dim startdate As Date = CDate(Data(2).ToString())
            Dim Ans1 As String = Data(3).ToString()
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@startdate", SqlDbType.Date)
                Params(0).Value = startdate
                Params(1) = New SqlParameter("@empid", SqlDbType.Int)
                Params(1).Value = Created_By
                Params(2) = New SqlParameter("@centerid", SqlDbType.BigInt)
                Params(2).Value = centerid
                Params(3) = New SqlParameter("@Ans1", SqlDbType.VarChar, 1000)
                Params(3).Value = Ans1
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_RepaymentMoratoriumStatus_Save", Params)
                Message = CStr(Params(5).Value)
                ErrorFlag = CInt(Params(4).Value)
            Catch ex As Exception
                Message = Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
        End If

        If CInt(Data(0)) = 2 Then
            Dim centername As String = Nothing
            Dim Centre_Id As Integer = CInt(Data(1).ToString())
            DT4 = DB.ExecuteDataSet("select CenterName from  BRD_NonMoratoriumCustomers where CenterID=" + Centre_Id.ToString()).Tables(0)
            If (DT4.Rows.Count > 0) Then
                centername = DT4.Rows(0)(0).ToString()
            End If
            Dim QS1 As String = ""
            DT1 = DB.ExecuteDataSet("select BRD_ID,ClientID,ClientName,MeetingDay from  BRD_NonMoratoriumCustomers where Repayment_Start_Date is null and Status=1 and CenterID=" + Centre_Id.ToString()).Tables(0)
            For n As Integer = 0 To DT1.Rows.Count - 1
                QS1 += DT1.Rows(n)(0).ToString() + "ÿ" + DT1.Rows(n)(1).ToString() + "ÿ" + DT1.Rows(n)(2).ToString() + "ÿ" + DT1.Rows(n)(3).ToString() + "Ñ"
            Next

                CallBackReturn = QS1 + "Ø" + centername.ToString()

        End If

    End Sub
End Class


