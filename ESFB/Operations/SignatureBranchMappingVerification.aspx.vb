﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class SignatureBranchMappingVerification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1200) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Me.Master.subtitle = "Signature - Branch Mapping Verification"

            DT = DB.ExecuteDataSet("select A.branch_id,branch_name, D.emp_code,emp_name, " +
                  " E.Department_name,F.Designation_name,G.Cadre_name,Date_of_join, POA_No,case when H.sign_id is null then '' else H.sign_id end  as Signature,I.Map_id, " +
                  " case when I.Appr_type = 1 then 'Permanant' when I.Appr_type = 2 then 'Temporary' else 'Un-Map' end, Start_Dt, End_Dt, I.Maker_Remarks from branch_master A " +
                  " inner join District_master B on A.district_id = B.district_id " +
                  " inner join State_master C on A.State_id = C.State_id   " +
                  " inner join emp_master D on A.branch_id = D.Branch_Id   " +
                  " inner join Department_master E on D.Department_id = E.Department_id " +
                  " inner join Designation_master F on D.Designation_id = F.Designation_id " +
                  " inner join Cadre_master G on D.Cadre_id = G.Cadre_id " +
                  " inner join Sign_master H on D.emp_code = H.Emp_code  and isnull(H.checker_status,0)  in (1) " +
                  " inner join Sign_Branch_Mapping I on H.emp_code = I.Emp_code and  I.checker_status is null and I.status_id is null " +
                  " where D.Status_id = 1").Tables(0)

            Dim StrUpload As String = ""
            Dim Sdat, Tdat As String
            For n As Integer = 0 To DT.Rows.Count - 1
                If IsDBNull(DT.Rows(n)(12)) Then
                    Sdat = ""
                Else
                    Sdat = CDate(DT.Rows(n)(12)).ToString("dd-MMM-yyyy").ToString()
                End If
                If IsDBNull(DT.Rows(n)(13)) Then
                    Tdat = ""
                Else
                    Tdat = CDate(DT.Rows(n)(13)).ToString("dd-MMM-yyyy").ToString()
                End If
                StrUpload += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + CDate(DT.Rows(n)(7)).ToString("dd-MMM-yyyy") + "µ" + DT.Rows(n)(8).ToString() + "µ" + DT.Rows(n)(9).ToString().ToUpper() + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + DT.Rows(n)(11).ToString().ToUpper() + "µ" + Sdat + "µ" + Tdat + "µ" + DT.Rows(n)(14).ToString().ToUpper()
            Next
            hid_dtls.Value = StrUpload

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CDbl(Data(0)) = 1 Then
            Dim dataval As String = CStr(Data(1))
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@userID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@MappVerifyDtl", SqlDbType.VarChar)
                Params(1).Value = dataval.Substring(1)
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_SIGN_BRANCH_MAPPING_VERIFICATION", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If



    End Sub
#End Region

End Class
