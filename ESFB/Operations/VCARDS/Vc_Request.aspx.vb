﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vc_Request
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim GF As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim UserID As String
    Dim TypeId As Integer
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            UserID = Session("UserID").ToString()
            Me.txtEmpCode.Text = CStr(Session("UserID"))
            Me.Master.subtitle = "Visiting Card Request"
           
            'If Not IsPostBack Then

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)


            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            txtEmpCode.Focus()
            'End If

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EmpCode As String
        If CInt(Data(0)) = 1 Then
            EmpCode = CStr(Data(1))
            DT = DB.ExecuteDataSet("select A.emp_code,emp_name,B.branch_name,B.address,C.Department_name,D.Designation_name,E.Cadre_name,f.email,f.cug_no from emp_master A " +
                " inner join Branch_master B on A.branch_id = B.Branch_id  " +
                " inner join Department_master C on A.Department_id = C.Department_id " +
                " inner join Designation_master D on A.Designation_id = D.Designation_id " +
                " inner join Cadre_master E on A.Cadre_id = E.Cadre_id " +
                " inner join emp_profile f on A.emp_code = f.emp_code " +
                " where (A.emp_code = " + EmpCode + " And A.Status_id = 1)").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString + "|" + DT.Rows(0)(1).ToString + "|" + DT.Rows(0)(2).ToString + "|" + DT.Rows(0)(3).ToString + "|" + DT.Rows(0)(4).ToString + "|" + DT.Rows(0)(5).ToString + "|" + DT.Rows(0)(6).ToString + "|" + DT.Rows(0)(7).ToString + "|" + DT.Rows(0)(8).ToString
            End If
        ElseIf CInt(Data(0)) = 2 Then
            EmpCode = CStr(Data(1))
            DT = DB.ExecuteDataSet("select a.emp_code,d.emp_name,b.branch_id,c.branch_name,b.branch_address, " +
           " E.Department_name, b.display_designation_name, b.display_cadre, b.email_id, b.contact_no, b.No_of_cards, b.remarks, a.Request_No,b.current_role " +
 " from VC_request_Master a " +
 " inner join  VC_request_cycle  b on A.Request_No = b.Request_No and a.status_id=b.status_id " +
" left join Branch_master C on b.branch_id = c.Branch_id " +
" left join emp_master D on a.emp_code = D.emp_code " +
" left join Department_master E on b.Department_id = E.Department_id  " +
                " where (A.emp_code = " + EmpCode + " And A.Status_id = 13)").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString + "|" + DT.Rows(0)(1).ToString + "|" + DT.Rows(0)(2).ToString + "|" + DT.Rows(0)(3).ToString + "|" + DT.Rows(0)(4).ToString + "|" + DT.Rows(0)(5).ToString + "|" + DT.Rows(0)(6).ToString + "|" + DT.Rows(0)(7).ToString + "|" + DT.Rows(0)(8).ToString + "|" + DT.Rows(0)(9).ToString + "|" + DT.Rows(0)(10).ToString + "|" + DT.Rows(0)(11).ToString + "|" + DT.Rows(0)(12).ToString + "|" + DT.Rows(0)(13).ToString
            End If
        ElseIf CInt(Data(0)) = 3 Then
            EmpCode = CStr(Data(1))
            DT = DB.ExecuteDataSet("select A.emp_code,d.emp_name,b.display_branch_name,b.branch_address,b.display_Department_name,b.display_Designation_name,  " +
" b.display_Cadre,b.email_id,b.contact_no,b.No_of_cards,b.remarks,a.Request_No,b.current_role from VC_request_Master a  " +
" inner join  VC_request_cycle  b on A.Request_No = b.Request_No  " +
" inner join emp_master D on a.emp_code = D.emp_code  " +
                " where (A.emp_code = " + EmpCode + " And A.Status_id = 1)").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString + "|" + DT.Rows(0)(1).ToString + "|" + DT.Rows(0)(2).ToString + "|" + DT.Rows(0)(3).ToString + "|" + DT.Rows(0)(4).ToString + "|" + DT.Rows(0)(5).ToString + "|" + DT.Rows(0)(6).ToString + "|" + DT.Rows(0)(7).ToString + "|" + DT.Rows(0)(8).ToString + "|" + DT.Rows(0)(9).ToString + "|" + DT.Rows(0)(10).ToString + "|" + DT.Rows(0)(11).ToString + "|" + DT.Rows(0)(12).ToString
            End If
        ElseIf CInt(Data(0)) = 4 Then
            Try
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0

                Try

                    'Dim Data() As String = CStr(Data(1)).Split(CChar("ÿ"))
                    Dim Params(7) As SqlParameter
                    Dim bit As Integer = 11
                    Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(0).Value = CInt(Data(1))
                    Params(1) = New SqlParameter("@checkbit", SqlDbType.Int)
                    Params(1).Value = bit
                    Params(2) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                    Params(2).Value = CStr(Data(2))
                    Params(3) = New SqlParameter("@status", SqlDbType.Int)
                    Params(3).Value = CInt(Data(3))
                    Params(4) = New SqlParameter("@Request_No", SqlDbType.Int)
                    Params(4).Value = CInt(Data(4))
                    Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@UserId", SqlDbType.VarChar, 500)
                    Params(7).Value = UserID

                    DB.ExecuteNonQuery("[SP_VC_REQUEST]", Params)
                    ErrorFlag = CInt(Params(5).Value)
                    Message = CStr(Params(6).Value)

                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try

                CallBackReturn = ErrorFlag.ToString + "Ø" + Message


            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try


        End If
    End Sub
#End Region
   
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            Try

                Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
                Dim ReqNo As Integer
                If Data(11).ToString() <> "" Then
                    ReqNo = CInt(Data(11).ToString())
                Else
                    ReqNo = 0
                End If
                Dim Params(16) As SqlParameter
                Dim bit As Integer = 1
                Params(0) = New SqlParameter("@EmpCode", SqlDbType.VarChar, 500)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@CountCard", SqlDbType.Int)
                Params(1).Value = CInt(Data(2))
                Params(2) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                Params(2).Value = CStr(Data(3))
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@checkbit", SqlDbType.Int)
                Params(5).Value = bit
                Params(6) = New SqlParameter("@status", SqlDbType.Int)
                Params(6).Value = CInt(Data(4))
                Params(7) = New SqlParameter("@BranchAddress", SqlDbType.VarChar, 500)
                Params(7).Value = CStr(Data(5))
                Params(8) = New SqlParameter("@CurrentRole", SqlDbType.VarChar, 500)
                Params(8).Value = CStr(Data(6))
                Params(9) = New SqlParameter("@UserId", SqlDbType.VarChar, 500)
                Params(9).Value = UserID
                Params(10) = New SqlParameter("@Email", SqlDbType.VarChar, 500)
                Params(10).Value = CStr(Data(7))
                Params(11) = New SqlParameter("@DispDesignation", SqlDbType.VarChar, 500)
                Params(11).Value = CStr(Data(8))
                Params(12) = New SqlParameter("@Display_cadre", SqlDbType.VarChar, 500)
                Params(12).Value = CStr(Data(9))
                Params(13) = New SqlParameter("@DispDepartment", SqlDbType.VarChar, 500)
                Params(13).Value = CStr(Data(10))
                Params(14) = New SqlParameter("@Request_No", SqlDbType.Int)
                Params(14).Value = ReqNo
                Params(15) = New SqlParameter("@contact", SqlDbType.VarChar, 500)
                Params(15).Value = CStr(Data(12))
                Params(16) = New SqlParameter("@DispBranchName", SqlDbType.VarChar, 500)
                Params(16).Value = CStr(Data(13))
                

                DB.ExecuteNonQuery("[SP_VC_REQUEST]", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)

            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            'cl_script1.Append("         alert('" + Message + "');window.open('Vc_Request.aspx','_self')")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

End Class
