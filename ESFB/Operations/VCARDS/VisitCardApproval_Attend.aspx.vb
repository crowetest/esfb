﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class VisitCardApproval_Attend
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim GF As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim UserID As String
    Dim TypeId As Integer
    Dim ReqID As Integer
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Visiting Card Approval"
           
            If Not IsPostBack Then

                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
                If Not IsNothing(Request.QueryString.Get("ReqID")) Then
                    ReqID = CInt(GF.Decrypt(Request.QueryString.Get("ReqID")))
                End If
                hid_ReqID.Value = ReqID.ToString()

                Me.btnSave.Attributes.Add("onclick", "return ApproveOnClick()")
                txtEmpCode.Focus()
            End If

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))

        If CInt(Data(0)) = 1 Then
            ID = hid_ReqID.Value


            DT = DB.ExecuteDataSet("select A.emp_code,c.emp_name,b.display_branch_name,b.branch_address,b.display_Department_name,b.display_Designation_name, " +
           " b.Display_cadre, b.email_id, b.contact_no, b.No_of_cards, b.remarks, a.Request_No, b.current_role " +
 " from VC_request_master A  " +
 " inner join (select * from VC_request_cycle where cycle_id in  (select max(cycle_id) from VC_request_cycle group by Request_No)) B on A.Request_No = b.Request_No " +
 "  inner join emp_master c on A.emp_code = c.emp_code " +
                " where (A.Request_No = " + ID + ")").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString + "|" + DT.Rows(0)(1).ToString + "|" + DT.Rows(0)(2).ToString + "|" + DT.Rows(0)(3).ToString + "|" + DT.Rows(0)(4).ToString + "|" + DT.Rows(0)(5).ToString + "|" + DT.Rows(0)(6).ToString + "|" + DT.Rows(0)(7).ToString + "|" + DT.Rows(0)(8).ToString + "|" + DT.Rows(0)(9).ToString + "|" + DT.Rows(0)(10).ToString + "|" + DT.Rows(0)(11).ToString + "|" + DT.Rows(0)(12).ToString
            End If
        ElseIf CInt(Data(0)) = 2 Then
            Try
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0

                Try

                    'Dim Data() As String = CStr(Data(1)).Split(CChar("ÿ"))
                    Dim Params(19) As SqlParameter
                    Dim bit As Integer = 3
                    Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(0).Value = CInt(Data(1))
                    Params(1) = New SqlParameter("@checkbit", SqlDbType.Int)
                    Params(1).Value = bit
                    Params(2) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                    Params(2).Value = CStr(Data(2))
                    Params(3) = New SqlParameter("@status", SqlDbType.Int)
                    Params(3).Value = CInt(Data(3))
                    Params(4) = New SqlParameter("@Request_No", SqlDbType.Int)
                    Params(4).Value = CInt(Data(4))
                    Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@DispBranchName", SqlDbType.VarChar, 500)
                    Params(7).Value = CStr(Data(5))
                    Params(8) = New SqlParameter("@DispBranchAddress", SqlDbType.VarChar, 500)
                    Params(8).Value = CStr(Data(6))
                    Params(9) = New SqlParameter("@DispDepartment", SqlDbType.VarChar, 500)
                    Params(9).Value = CStr(Data(7))
                    Params(10) = New SqlParameter("@DispDesignation", SqlDbType.VarChar, 500)
                    Params(10).Value = CStr(Data(8))
                    Params(11) = New SqlParameter("@DispEmail", SqlDbType.VarChar, 500)
                    Params(11).Value = CStr(Data(9))
                    Params(12) = New SqlParameter("@DispContact", SqlDbType.VarChar, 500)
                    Params(12).Value = CStr(Data(10))
                    Params(13) = New SqlParameter("@CountCard", SqlDbType.Int)
                    Params(13).Value = CInt(Data(11))
                    Params(14) = New SqlParameter("@UserId", SqlDbType.VarChar, 500)
                    Params(14).Value = Session("UserID").ToString()
                    Params(15) = New SqlParameter("@Display_cadre", SqlDbType.VarChar, 500)
                    Params(15).Value = CStr(Data(12))
                    Params(16) = New SqlParameter("@BranchAddress", SqlDbType.VarChar, 500)
                    Params(16).Value = CStr(Data(13))
                    Params(17) = New SqlParameter("@Email", SqlDbType.VarChar, 500)
                    Params(17).Value = CStr(Data(14))
                    Params(18) = New SqlParameter("@contact", SqlDbType.VarChar, 500)
                    Params(18).Value = CStr(Data(15))
                    Params(19) = New SqlParameter("@CurrentRole", SqlDbType.VarChar, 500)
                    Params(19).Value = CStr(Data(16))

                    DB.ExecuteNonQuery("[SP_VC_REQUEST]", Params)
                    ErrorFlag = CInt(Params(5).Value)
                    Message = CStr(Params(6).Value)

                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try

                 CallBackReturn = ErrorFlag.ToString + "Ø" + Message


            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try


         ElseIf CInt(Data(0)) = 3 Then
        Try
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            Try

                    'Dim Data1() As String = hdnValue.Value.Split(CChar("Ø"))
                Dim Params(6) As SqlParameter
                    Dim bit As Integer = 4
                Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(0).Value = CInt(Data(1))
                Params(1) = New SqlParameter("@checkbit", SqlDbType.Int)
                Params(1).Value = bit
                Params(2) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                    Params(2).Value = CStr(Data(2))
                Params(3) = New SqlParameter("@status", SqlDbType.VarChar, 500)
                    Params(3).Value = CStr(Data(3))
                Params(4) = New SqlParameter("@Request_No", SqlDbType.Int)
                    Params(4).Value = CInt(Data(4))
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("[SP_VC_REQUEST]", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)

            Catch ex As Exception
                Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try

           CallBackReturn = ErrorFlag.ToString + "Ø" + Message

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

        End If
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            Try

                Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
                Dim Params(18) As SqlParameter
                Dim bit As Integer = 2
                Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(0).Value = CInt(Data(0))
                Params(1) = New SqlParameter("@checkbit", SqlDbType.Int)
                Params(1).Value = bit
                Params(2) = New SqlParameter("@DispBranchName", SqlDbType.VarChar, 500)
                Params(2).Value = CStr(Data(1))
                Params(3) = New SqlParameter("@DispBranchAddress", SqlDbType.VarChar, 500)
                Params(3).Value = CStr(Data(2))
                Params(4) = New SqlParameter("@DispDepartment", SqlDbType.VarChar, 500)
                Params(4).Value = CStr(Data(3))
                Params(5) = New SqlParameter("@DispDesignation", SqlDbType.VarChar, 500)
                Params(5).Value = CStr(Data(4))
                Params(6) = New SqlParameter("@DispEmail", SqlDbType.VarChar, 500)
                Params(6).Value = CStr(Data(5))
                Params(7) = New SqlParameter("@DispContact", SqlDbType.VarChar, 500)
                Params(7).Value = CStr(Data(6))
                Params(8) = New SqlParameter("@CountCard", SqlDbType.Int)
                Params(8).Value = CInt(Data(7))
                Params(9) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                Params(9).Value = CStr(Data(8))
                Params(10) = New SqlParameter("@status", SqlDbType.VarChar, 500)
                Params(10).Value = CStr(Data(9))
                Params(11) = New SqlParameter("@Request_No", SqlDbType.Int)
                Params(11).Value = CInt(Data(10))                
                Params(12) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(12).Direction = ParameterDirection.Output
                Params(13) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(13).Direction = ParameterDirection.Output
                Params(14) = New SqlParameter("@Display_cadre", SqlDbType.VarChar, 500)
                Params(14).Value = CStr(Data(11))
                Params(15) = New SqlParameter("@BranchAddress", SqlDbType.VarChar, 500)
                Params(15).Value = CStr(Data(12))
                Params(16) = New SqlParameter("@Email", SqlDbType.VarChar, 500)
                Params(16).Value = CStr(Data(13))
                Params(17) = New SqlParameter("@contact", SqlDbType.VarChar, 500)
                Params(17).Value = CStr(Data(14))
                Params(18) = New SqlParameter("@CurrentRole", SqlDbType.VarChar, 500)
                Params(18).Value = CStr(Data(15))
                DB.ExecuteNonQuery("[SP_VC_REQUEST]", Params)
                ErrorFlag = CInt(Params(12).Value)
                Message = CStr(Params(13).Value)

            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            'cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("         alert('" + Message + "');window.open('Vc_approval.aspx','_self')")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

End Class
