﻿
Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Vc_despatch
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim HD As New HelpDesk
    Dim TeamID As Integer
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
    Dim GF As New GeneralFunctions
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Me.Master.subtitle = "Visiting Card Despatch"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.btnSave.Attributes.Add("onclick", "return btnSave_onclick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then

            DT = DB.ExecuteDataSet("  select a.emp_code,c.emp_name,l.no_of_cards,k.remarks as request_Remarks ,l.remarks as approval_Remarks, m.remarks as Design_Remarks,o.remarks as Print_Remarks,a.request_no " +
 " from VC_request_Master  a  " +
   " left join (select * from VC_request_cycle where cycle_id in (select max(cycle_id) from VC_request_cycle group by Request_No)) b on a.Request_No=b.Request_No " +
   "  left join emp_master c on a.emp_code=c.emp_code  " +
 "	left join VC_request_cycle k on a.Request_No =k.Request_No and k.status_id =1 and k.cycle_id in (select max(cycle_id) from VC_request_cycle where status_id=1 group by Request_No) " +
  " left join VC_request_cycle l on a.Request_No =l.Request_No and l.status_id =2  and l.cycle_id in (select max(cycle_id) from VC_request_cycle where status_id=2 group by Request_No)  " +
  " left join VC_request_cycle m on a.Request_No =m.Request_No and m.status_id =3 and m.cycle_id in (select max(cycle_id) from VC_request_cycle where status_id=3 group by Request_No) " +
  "  left join VC_request_cycle o on a.Request_No =o.Request_No and o.status_id =4 and o.cycle_id in (select max(cycle_id) from VC_request_cycle where status_id=4 group by Request_No) " +
  "   where a.status_id in (4)").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "~" + DR(0).ToString() + "ÿ" + CStr(DR(1)) + "ÿ" + DR(2).ToString() + "ÿ" + DR(3).ToString() + "ÿ" + DR(4).ToString() + "ÿ" + DR(5).ToString() + "ÿ" + DR(6).ToString() + "ÿ" + DR(7).ToString()
            Next
        ElseIf CInt(Data(0)) = 2 Then
            Try
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0

                Try

                    Dim Data1 As String() = CStr(Data(1)).Split(CChar("ÿ"))
                    Dim Params(5) As SqlParameter
                    Dim bit As Integer = 10
                    Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(0).Value = CInt(Data1(0))
                    Params(1) = New SqlParameter("@checkbit", SqlDbType.Int)
                    Params(1).Value = bit
                    Params(2) = New SqlParameter("@status", SqlDbType.Int)
                    Params(2).Value = CInt(Data1(1))
                    Params(3) = New SqlParameter("@Request_No", SqlDbType.Int)
                    Params(3).Value = CInt(Data1(2))
                    Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(5).Direction = ParameterDirection.Output

                    DB.ExecuteNonQuery("[SP_VC_REQUEST]", Params)
                    ErrorFlag = CInt(Params(4).Value)
                    Message = CStr(Params(5).Value)

                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try

                CallBackReturn = ErrorFlag.ToString + "Ø" + Message

            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try

        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim RequestId As String = ""
        Dim HeaderText As String
        Try

            Dim Data1 As String() = hid_Edit.Value.Split(CChar("~"))
            Dim i As Integer = 0
            Dim Req As String = ""
            For Each dt1 In Data1
                If Data1(i) <> "" Then
                    Dim Data2 As String() = CStr(Data1(i)).Split(CChar("ÿ"))
                    Req = CInt(Data2(2)).ToString()
                    Try
                        Dim Params(7) As SqlParameter
                        Dim bit As Integer = 9
                        Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                        Params(0).Value = CInt(Data2(0))
                        Params(1) = New SqlParameter("@checkbit", SqlDbType.Int)
                        Params(1).Value = bit
                        Params(2) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                        Params(2).Value = CStr(Data2(1))
                        Params(3) = New SqlParameter("@Request_No", SqlDbType.Int)
                        Params(3).Value = CInt(Data2(2))
                        Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(4).Direction = ParameterDirection.Output
                        Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(5).Direction = ParameterDirection.Output
                        Params(6) = New SqlParameter("@status", SqlDbType.Int)
                        Params(6).Value = CInt(Data2(3))
                        Params(7) = New SqlParameter("@ExpectedDate", SqlDbType.Date)
                        Params(7).Value = CDate(Data2(4))
                        DB.ExecuteNonQuery("[SP_VC_REQUEST]", Params)
                        ErrorFlag = CInt(Params(4).Value)
                        Message = CStr(Params(5).Value)


                    Catch ex As Exception
                        Message = ex.Message.ToString
                        ErrorFlag = 1
                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                    End Try

                    i += 1

                End If
                RequestId += Req + ","

            Next
            RequestId = RequestId.TrimEnd(CChar(","))
            DT = DB.ExecuteDataSet("  select a.emp_code,c.emp_name,l.no_of_cards,l.contact_no,l.display_designation_name,l.email_id,l.branch_address,l.display_branch_name,p.Expected_delivery_date, " +
" k.remarks as request_Remarks ,l.remarks as approval_Remarks, m.remarks as Design_Remarks,o.remarks as Print_Remarks,p.remarks as despatch_Remarks,a.request_no  " +
 " from VC_request_Master  a  " +
   " left join (select * from VC_request_cycle where cycle_id in (select max(cycle_id) from VC_request_cycle group by Request_No)) b on a.Request_No=b.Request_No " +
   "  left join emp_master c on a.emp_code=c.emp_code  " +
 "	left join VC_request_cycle k on a.Request_No =k.Request_No and k.status_id =1 and k.cycle_id in (select max(cycle_id) from VC_request_cycle where status_id=1 group by Request_No) " +
  " left join VC_request_cycle l on a.Request_No =l.Request_No and l.status_id =2  and l.cycle_id in (select max(cycle_id) from VC_request_cycle where status_id=2 group by Request_No)  " +
  " left join VC_request_cycle m on a.Request_No =m.Request_No and m.status_id =3 and m.cycle_id in (select max(cycle_id) from VC_request_cycle where status_id=3 group by Request_No) " +
  "  left join VC_request_cycle o on a.Request_No =o.Request_No and o.status_id =4 and o.cycle_id in (select max(cycle_id) from VC_request_cycle where status_id=4 group by Request_No) " +
  "  	left join VC_request_cycle p on a.Request_No =p.Request_No and p.status_id =5 and p.cycle_id in (select max(cycle_id) from VC_request_cycle where status_id=5 group by Request_No)  " +
"   where a.status_id in (5) and a.Request_No in (" & RequestId & ")").Tables(0)

            HeaderText = "Despatch Excel Report "

            WebTools.ExporttoExcel(DT, HeaderText)

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');window.open('Vc_despatch.aspx','_self')")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
#End Region

End Class
