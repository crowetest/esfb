﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
 Async="true" CodeFile="Vc_DesighnUpload.aspx.vb" Inherits="Vc_DesighnUpload" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            font-size: 11px;
            color: #000000;
            border: 1px solid #7f9db9;
            }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
        .style1
        {
            width: 46%;
        }
        .style2
        {
            width: 74%;
        }
         
        </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <script type="text/javascript" src="../../Script/jquery-1.2.6.min.js"></script>

        <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
       <script type="text/javascript" src="../../Script/Validations.js"></script>
        <script language="javascript" type="text/javascript">
            var FileSelected = 0;
            function window_onload() {


                var ToData = "1Ø";
                ToServer(ToData, 1);
                //    $(".style1").hide();
                //    $(".style2").show();

            }

          
            function FromServer(arg, context) {

                if (context == 1) {
                    if (arg != "") {
                        var Data = arg.split("|");
                        document.getElementById("<%= txtEmpCode.ClientID %>").value = Data[0];
                        document.getElementById("<%= txtEmpName.ClientID %>").value = Data[1];
                        document.getElementById("<%= txtReqNo.ClientID %>").value = Data[2];
                       
                    }

                    else {
                        alert("Invalid Request Number");
                       

                    }


                }
                else if (context == 2) {

                }

            }
           

            function btnExit_onclick() {

                window.open("Vc_Designing.aspx", "_self");
            }
            function UploadOnClick() {


                var empcode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
                var DesignRemark = document.getElementById("<%= txtRemarks.ClientID %>").value;
                var ReqId = document.getElementById("<%= txtReqNo.ClientID %>").value;
                var attachment = document.getElementById("<%= fup1.ClientID %>").value;
                if (DesignRemark == "") 
                {
                    alert("Please enter Remark");
                    return false;
                }

                document.getElementById("<%= hdnValue.ClientID %>").value = empcode + "Ø" + DesignRemark + "Ø" + 3 + "Ø" + ReqId + "Ø" + attachment;
//                alert(document.getElementById("<%= hdnValue.ClientID %>").value);
                window.location.reload();

            }
     
  
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
////return window_onload()
////// ]]>
        </script>
    </head>
    </html>
    
    <br />
    <br />
    <br />
    <br />
        <div id ="div"/>
         <table class="style1" style="width:100%">
            <tr> 
                <td colspan="3"><asp:Panel ID="pnBranch" runat="server">
                </asp:Panel></td>
            </tr>
        </table> 
        <div />

    <br />
    
   
       <table align="center" style="width: 60%; margin:0px auto;">
      <tr>
            <td style="width:30%">
                Request No</td>
            <td class="style1">
                <asp:TextBox ID="txtReqNo" runat="server" Width="30%" class="ReadOnlyTextBox" onkeypress='return NumericCheck(event)' MaxLength="10"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Employee Code</td>
            <td class="style1">
                <asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="ReadOnlyTextBox" onkeypress='return NumericCheck(event)' MaxLength="10"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Employee Name</td>
            <td class="style1">
            <asp:TextBox ID="txtEmpName" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
   
                        
                   
       
         <tr>
            <td style="width:30%">
                Remarks</td>
            <td class="style1">
                <%--<asp:TextBox ID="txtRemarks" runat="server" Width="89%"  TextMode="MultiLine"  MaxLength="25"  onkeypress='return AlphaNumericCheck(event)' ></asp:TextBox>--%>
          <textarea id="txtRemarks" class="NormalText" cols="20" name="S1" rows="3" runat="server" 
                    maxlength="400" style="width: 89%"  onkeypress="return blockSpecialChar(event)"></textarea> 
            </td>
           
        </tr> 
        <tr id="Tr7">
            <td style="width:30%">
            
                Attachment If any
            </td>
            <td class="style1">
                <div id="fileUploadarea">
                    <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" /><br />
                </div></td>
               
           
            <td style="width: 12%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 20%">
                &nbsp;
            </td>
        </tr>
            <tr>
            <td style="width:30%">
                &nbsp;</td>
            <td class="style1">
                <asp:Button ID="btnSave" runat="server" Text="UPLOAD" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="21%" /> &nbsp;
              
                    <input id="Button3" type="button" value="EXIT" class="NormalText" 
                    style="width: 21%" onclick="return btnExit_onclick()" />
                    </td>
            
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                &nbsp;</td>
            <td style="text-align:center" class="style2">
                &nbsp;</td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnValue" runat="server" />
    <asp:HiddenField ID="hdnSignId" runat="server" />
   
    <asp:HiddenField ID="hdnVerify" runat="server" />
    <asp:HiddenField ID="hid_ReqID" runat="server" />
    <asp:HiddenField ID="hid_dtls" runat="server" />

    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
