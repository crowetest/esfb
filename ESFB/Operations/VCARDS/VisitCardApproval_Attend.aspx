﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
 Async="true" CodeFile="VisitCardApproval_Attend.aspx.vb" Inherits="VisitCardApproval_Attend" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            font-size: 11px;
            color: #000000;
            border: 1px solid #7f9db9;
            }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
        .style1
        {
            width: 45%;
        }
        .style2
        {
            width: 50%;
        }
         
        </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <script type="text/javascript" src="../../Script/jquery-1.2.6.min.js"></script>

        <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
       <script type="text/javascript" src="../../Script/Validations.js"></script>
        <script language="javascript" type="text/javascript">
        var FileSelected = 0;
function window_onload()
{


    var ToData = "1Ø";
    ToServer(ToData, 1);
//    $(".style1").hide();
//    $(".style2").show();

}

var ReqId;



function checkMail(email) {

    var filter = /^[\w.+\-]+@esafbank\.com$/;


    if (filter.test(email.value)) {
        return true;
    }
    else

        return false;
} 



    function FromServer(arg, context) 
    {
   
       if (context == 1)
       {
            if(arg != "")
            {
                var Data = arg.split("|");
                document.getElementById("<%= txtEmpCode.ClientID %>").value = Data[0];
                document.getElementById("<%= txtEmpName.ClientID %>").value = Data[1] ;
                document.getElementById("<%= txtBranchName.ClientID %>").value = Data[2];
                document.getElementById("<%= txtBranchAddress.ClientID %>").value = Data[3];
                document.getElementById("<%= txtDepartment.ClientID %>").value = Data[4] ;
                document.getElementById("<%= txtDesignation.ClientID %>").value = Data[5] ;
                document.getElementById("<%= txtCadre.ClientID %>").value = Data[6] ;
                document.getElementById("<%= txtEmail.ClientID %>").value = Data[7] ;
                document.getElementById("<%= txtContact.ClientID %>").value = Data[8];
                document.getElementById("<%= txtCards.ClientID %>").value = Data[9];
                document.getElementById("<%= txtRemarks.ClientID %>").value = Data[10];
                document.getElementById("<%= hid_ReqID.ClientID %>").value = Data[11];
                document.getElementById("<%= txtRole.ClientID %>").value = Data[12];
                ReqId = document.getElementById("<%= hid_ReqID.ClientID %>").value;
                document.getElementById("<%= txtDpBranchName.ClientID %>").value = Data[2];
                document.getElementById("<%= txtDpBranchAddress.ClientID %>").value = Data[3];
                document.getElementById("<%= txtDpDepartment.ClientID %>").value = Data[4];
                document.getElementById("<%= txtDpDesignation.ClientID %>").value = Data[5];
                document.getElementById("<%= txtDpEmail.ClientID %>").value = Data[7];
                document.getElementById("<%= txtDpContact.ClientID %>").value = Data[8];
                document.getElementById("<%= txtDpCards.ClientID %>").value = Data[9];
                document.getElementById("<%= txtDispRole.ClientID %>").value = Data[12];
           }

           else
           {
                alert("Invalid Request Number");
              
                document.getElementById("<%= txtBranchName.ClientID %>").value = "";
                document.getElementById("<%= txtBranchAddress.ClientID %>").value = "";
                document.getElementById("<%= txtDepartment.ClientID %>").value = "" ;
                document.getElementById("<%= txtDesignation.ClientID %>").value = "" ;
                document.getElementById("<%= txtCadre.ClientID %>").value = "" ;
                document.getElementById("<%= txtEmail.ClientID %>").value = "";
                document.getElementById("<%= txtContact.ClientID %>").value = "" ;
                
           }

         
        }
        else if (context == 2)
        {
            var Data = arg.split("Ø");

            alert(Data[1]);
            window.open("Vc_approval.aspx", "_self");

            
        }
        else if (context == 3)
         {
             var Data = arg.split("Ø");

             alert(Data[1]);
             window.open("Vc_approval.aspx", "_self");

        }

    }
    function btnReturn_onclick() {
        if (document.getElementById("<%= txtAppRemarks.ClientID %>").value == "") {
            alert("Enter Approval Remarks");
            document.getElementById("<%= txtAppRemarks.ClientID %>").focus();
            return false;
        }
        var empcode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
        var ReturnRemark = document.getElementById("<%= txtAppRemarks.ClientID %>").value;
        var brname = document.getElementById("<%= txtDpBranchName.ClientID %>").value;
        var braddress = document.getElementById("<%= txtDpBranchAddress.ClientID %>").value;
        var dept = document.getElementById("<%= txtDpDepartment.ClientID %>").value;
        var designation = document.getElementById("<%= txtDpDesignation.ClientID %>").value;
        var Dispemail = document.getElementById("<%= txtDpEmail.ClientID %>").value;
        var Dispcontact = document.getElementById("<%= txtDpContact.ClientID %>").value;
        var cardNo = document.getElementById("<%= txtDpCards.ClientID %>").value;
        var cadre = document.getElementById("<%= txtCadre.ClientID %>").value;
        var branchaddress = document.getElementById("<%= txtBranchAddress.ClientID %>").value;
        var email = document.getElementById("<%= txtEmail.ClientID %>").value;
        var contact = document.getElementById("<%= txtContact.ClientID %>").value;
        var currentRole =document.getElementById("<%= txtDispRole.ClientID %>").value;
        document.getElementById("<%= hdnValue.ClientID %>").value = empcode + "Ø" + ReturnRemark + "Ø" + 13 + "Ø" + ReqId + "Ø" + brname + "Ø" + braddress + "Ø" + dept + "Ø" + designation + "Ø" + Dispemail + "Ø" + Dispcontact + "Ø" + cardNo + "Ø" + cadre + "Ø" + branchaddress + "Ø" + email + "Ø" + contact + "Ø" + currentRole;
        var dtl = document.getElementById("<%= hdnValue.ClientID %>").value;  
        ToServer("2Ø" + dtl, 2);
        $("#txtAppRemarks").hide();
    }
    function btnReject_onclick() {
        if (document.getElementById("<%= txtAppRemarks.ClientID %>").value == "") {
            alert("Enter Approval Remarks");
            document.getElementById("<%= txtAppRemarks.ClientID %>").focus();
            return false;
        }
        var empcode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
        var RejectRemark = document.getElementById("<%= txtAppRemarks.ClientID %>").value;
        document.getElementById("<%= hdnValue.ClientID %>").value = empcode + "Ø" + RejectRemark + "Ø" + 12 + "Ø" + ReqId;
        var dtl = document.getElementById("<%= hdnValue.ClientID %>").value;
        ToServer("3Ø" + dtl, 3);
        $(".style2").hide();
    }

    function btnExit_onclick() {

        window.open("Vc_approval.aspx", "_self");
    }
     function ApproveOnClick() {
         

         if (document.getElementById("<%= txtDpBranchName.ClientID %>").value == "") 
        {
            alert("Enter Branch name for card design ");
            document.getElementById("<%= txtDpBranchName.ClientID %>").focus();
            return false;
        }
      
       
        else if (document.getElementById("<%= txtDpBranchAddress.ClientID %>").value == "") {
            alert("Enter Branch address for card design");
            document.getElementById("<%= txtDpBranchAddress.ClientID %>").focus();
            return false;
        }
        else if (document.getElementById("<%= txtDpDepartment.ClientID %>").value == "") {
            alert("Enter department");
            document.getElementById("<%= txtDpDepartment.ClientID %>").focus();
            return false;
        }
        else if (document.getElementById("<%= txtDpDesignation.ClientID %>").value == "") {
            alert("Enter Designation for card design");
            document.getElementById("<%= txtDpDesignation.ClientID %>").focus();
            return false;
        }
        else if (document.getElementById("<%= txtDpEmail.ClientID %>").value == "") {
            alert("Enter Email for card design");
            document.getElementById("<%= txtDpEmail.ClientID %>").focus();
            return false;
        }
       
        else if (document.getElementById("<%= txtDpContact.ClientID %>").value == "") {
            alert("Enter contact number for card design");
            document.getElementById("<%= txtDpContact.ClientID %>").focus();
            return false;
        }
        else if (document.getElementById("<%= txtDpCards.ClientID %>").value == "") {
            alert("Enter No of cards required");
            document.getElementById("<%= txtDpCards.ClientID %>").focus();
            return false;
        }
        else if (document.getElementById("<%= txtAppRemarks.ClientID %>").value == "") {
            alert("Enter Approval Remarks");
            document.getElementById("<%= txtAppRemarks.ClientID %>").focus();
            return false;
        }


        if (document.getElementById("<%= txtDpEmail.ClientID %>").value != "") {
            var ret = checkMail(document.getElementById("<%= txtDpEmail.ClientID %>"));
            if (ret == false) {
                alert('Please provide a valid official email address');
                return false;
            }
        }

        var empcode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
        var brname = document.getElementById("<%= txtDpBranchName.ClientID %>").value;
        var braddress = document.getElementById("<%= txtDpBranchAddress.ClientID %>").value;
        var dept = document.getElementById("<%= txtDpDepartment.ClientID %>").value;
        var designation = document.getElementById("<%= txtDpDesignation.ClientID %>").value;
        var Dispemail = document.getElementById("<%= txtDpEmail.ClientID %>").value;
        var Dispcontact = document.getElementById("<%= txtDpContact.ClientID %>").value;
        var cardNo = document.getElementById("<%= txtDpCards.ClientID %>").value;
        var AppRemark = document.getElementById("<%= txtAppRemarks.ClientID %>").value;
        var cadre = document.getElementById("<%= txtCadre.ClientID %>").value;
        var branchaddress = document.getElementById("<%= txtBranchAddress.ClientID %>").value;
        var email = document.getElementById("<%= txtEmail.ClientID %>").value;
        var contact = document.getElementById("<%= txtContact.ClientID %>").value;
        var currentRole = document.getElementById("<%= txtDispRole.ClientID %>").value;
        document.getElementById("<%= hdnValue.ClientID %>").value = empcode + "Ø" + brname + "Ø" + braddress + "Ø" + dept + "Ø" + designation + "Ø" + Dispemail + "Ø" + Dispcontact + "Ø" + cardNo + "Ø" + AppRemark + "Ø" + 2 + "Ø" + ReqId + "Ø" + cadre + "Ø" + branchaddress + "Ø" + email + "Ø" + contact + "Ø" + currentRole;
        $(".style2").hide();
        document.getElementById("<%= txtDpBranchName.ClientID %>").value = "";
        document.getElementById("<%= txtDpBranchAddress.ClientID %>").value = "";
        document.getElementById("<%= txtDpDepartment.ClientID %>").value = "";
        document.getElementById("<%= txtDpDesignation.ClientID %>").value = "";
        document.getElementById("<%= txtDpEmail.ClientID %>").value = "";
        document.getElementById("<%= txtDpContact.ClientID %>").value = "";
        document.getElementById("<%= txtDpCards.ClientID %>").value = "";
        document.getElementById("<%= txtDispRole.ClientID %>").value = "";
        document.getElementById("<%= txtAppRemarks.ClientID %>").value = "";
        document.getElementById("<%= txtRemarks.ClientID %>").value = "";


    }
     
  
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
////return window_onload()
////// ]]>
        </script>
    </head>
    </html>
    
    <br />
    <br />
    <br />
    <br />
        <div id ="div"/>
         <table class="style1" style="width:100%">
            <tr> 
                <td colspan="3"><asp:Panel ID="pnBranch" runat="server">
                </asp:Panel></td>
            </tr>
        </table> 
        <div />

    <br />
    
   
       <table align="center" style="width: 60%; margin:0px auto;">
      
        <tr>
            <td style="width:15%">
                Employee Code</td>
            <td class="style1">
                <asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="ReadOnlyTextBox" onkeypress='return NumericCheck(event)' MaxLength="10"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%">
                Employee Name</td>
            <td class="style1">
            <asp:TextBox ID="txtEmpName" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
            </td>
            <td class="style2">

                &nbsp;</td>
        </tr>
   
                        
                   <tr>
                  
            <td style="width:15%">
                Branch Name</td>
                
                    
            <td class="style1">
            <asp:TextBox ID="txtBranchName" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true" ></asp:TextBox>
               
            </td> 
         
            <td class="style2">
            <asp:TextBox ID="txtDpBranchName" runat="server" Width="89%" class="normaltext" ></asp:TextBox>
          
                &nbsp;</td>
       </tr>
               
        <tr>
            <td style="width:15%">
                Branch Address</td>
            <td class="style1">
                <asp:TextBox ID="txtBranchAddress" runat="server" Width="89%" TextMode="MultiLine" class="ReadOnlyTextBox" ReadOnly="true" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
            </td>
            <td class="style2">
            <asp:TextBox ID="txtDpBranchAddress" runat="server" Width="89%" TextMode="MultiLine" class="NormalText" onkeypress='return TextAreaCheck(event)'  ></asp:TextBox>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%">
                Department</td>
            <td class="style1">
                <asp:TextBox ID="txtDepartment" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
            </td>
            <td class="style2">
            <asp:TextBox ID="txtDpDepartment" runat="server" Width="89%" ></asp:TextBox>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td style="width:15%">
                Designation</td>
            <td class="style1">
                <asp:TextBox ID="txtDesignation" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
            </td>
            <td class="style2">
            <asp:TextBox ID="txtDpDesignation" runat="server" Width="89%" ></asp:TextBox>
                &nbsp;</td>
        </tr>
         <tr>
            <td style="width:15%">
                Current Role</td>
            <td class="style1">
                <asp:TextBox ID="txtRole" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
            </td>
            <td class="style2">
            <asp:TextBox ID="txtDispRole" runat="server" Width="89%" ></asp:TextBox>
                &nbsp;</td>
        </tr>
         <tr>
            <td style="width:15%">
                Grade</td>
            <td class="style1">
                <asp:TextBox ID="txtCadre" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr> 
        <tr>
            <td style="width:15%">
                Email</td>
            <td class="style1">
                <asp:TextBox ID="txtEmail" runat="server" Width="89%" class="ReadOnlyTextBox" MaxLength="50"  ReadOnly="true"  onkeypress='return AlphaNumericCheck(event)' ></asp:TextBox>
            </td>
            <td class="style2">
            <asp:TextBox ID="txtDpEmail" runat="server" Width="89%" class="NormalTextBox" MaxLength="50" ></asp:TextBox>
                &nbsp;</td>
        </tr>  
         <tr>
            <td style="width:15%">
                Contact No</td>
            <td class="style1">
                <asp:TextBox ID="txtContact" runat="server" Width="89%" class="ReadOnlyTextBox" MaxLength="10" onkeypress='return NumericCheck(event)' ReadOnly="true"></asp:TextBox>
            </td>
            <td class="style2">
            <asp:TextBox ID="txtDpContact" runat="server" Width="89%" class="NormalTextBox" MaxLength="10" onkeypress='return NumericCheck(event)' ></asp:TextBox>

                &nbsp;</td>
        </tr>  
         <tr>
            <td style="width:15%">
                No. of cards</td>
            <td class="style1">
                <asp:TextBox ID="txtCards" runat="server" Width="89%" class="NormalTextBox" MaxLength="15"  onkeypress='return NumericCheck(event)' ></asp:TextBox>
            </td>
            <td class="style2">
                            <asp:TextBox ID="txtDpCards" runat="server" Width="89%" class="NormalTextBox" MaxLength="15" onkeypress='return NumericCheck(event)' ></asp:TextBox>

                &nbsp;</td>
        </tr>  
         <tr>
            <td style="width:15%">
                Remarks</td>
            <td class="style1">
                 <textarea id="txtRemarks" class="NormalText" cols="20" name="S1" rows="3" runat="server" 
                    maxlength="400" style="width: 89%"  onkeypress="return blockSpecialChar(event)"></textarea>            </td>
            <td class="style2">
            <textarea id="txtAppRemarks" class="NormalText" cols="20" name="S1" rows="3" runat="server" 
                    maxlength="400" style="width: 89%"  onkeypress="return blockSpecialChar(event)"></textarea>
                &nbsp;</td>
        </tr> 
        
       
            <tr>
            <td style="width:15%">
                &nbsp;</td>
            <td class="style1">
                <asp:Button ID="btnSave" runat="server" Text="APPROVE" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="21%" /> &nbsp;
                <input id="Button2" type="button" value="RETURN" class="NormalText" 
                    style="width: 21%" onclick="return btnReturn_onclick()" />
                    <input id="Button1" type="button" value="REJECT" class="NormalText" 
                    style="width: 21%" onclick="return btnReject_onclick()" />
                    <input id="Button3" type="button" value="EXIT" class="NormalText" 
                    style="width: 21%" onclick="return btnExit_onclick()" />
                    </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                &nbsp;</td>
            <td style="text-align:center" class="style2">
                &nbsp;</td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnValue" runat="server" />
    <asp:HiddenField ID="hdnSignId" runat="server" />
   
    <asp:HiddenField ID="hdnVerify" runat="server" />
    <asp:HiddenField ID="hid_ReqID" runat="server" />
    <asp:HiddenField ID="hid_dtls" runat="server" />

    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
