﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class View_VC_Detailed_Status_Report_User
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTExcel As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date
    Dim AdminFlag As Boolean = False
    Dim GMASTER As New Master
    Dim UserID As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim DT As New DataTable
            Dim Emp_Code As String = CStr(Request.QueryString.Get("emp_code"))
            Dim Request_no As String = CStr(Request.QueryString.Get("req_no"))
            Dim From_Dt As Date = CDate(Request.QueryString.Get("From"))
            Dim To_Dt As Date = CDate(Request.QueryString.Get("To"))
            UserID = CInt(Session("UserID"))
           

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT1 As New DataTable
            DT1 = DB.ExecuteDataSet("Select emp_name from emp_master where emp_code = " + Emp_Code).Tables(0)


            Dim RowBG As Integer = 0
            Dim DR As DataRow
            RH.Heading(Session("FirmName"), tb, "Detailed Status Report", 100)
            tb.Attributes.Add("width", "100%")
            Dim TR1 As New TableRow
            Dim TR1_00, TR1_01 As New TableCell
            RH.AddColumn(TR1, TR1_00, 15, 15, "l", "Employee Code : ")
            RH.AddColumn(TR1, TR1_01, 30, 30, "l", Emp_Code.ToString())
            tb.Controls.Add(TR1)
            RH.BlankRow(tb, 3)

            Dim TR2 As New TableRow
            Dim TR2_00, TR2_01 As New TableCell
            RH.AddColumn(TR2, TR2_00, 15, 15, "l", "Employee Name : ")
            For Each DR1 In DT1.Rows
                RH.AddColumn(TR2, TR2_01, 30, 30, "l", DR1(0).ToString())
            Next
            tb.Controls.Add(TR2)
            RH.BlankRow(tb, 3)

            Dim TR4 As New TableRow
            Dim TR4_00, TR4_01 As New TableCell
            RH.AddColumn(TR4, TR4_00, 15, 15, "l", "Request Number : ")
            RH.AddColumn(TR4, TR4_01, 30, 30, "l", Request_no.ToString())
            tb.Controls.Add(TR4)
            RH.BlankRow(tb, 3)

           
            
            Dim i As Integer

            DT = DB.ExecuteDataSet(" select c.status_id,case when c.status_id=1 then 'User Requested'  " +
 "when c.status_id=1 then 'User Request' when c.status_id=2 then 'HR Approved'  " +
 "when c.status_id=3 then ' Designed' when c.status_id=4 then 'Printed'    " +
 "when c.status_id=5 then 'Despatched' when c.status_id=6 then ' Receive'  " +
 "when c.status_id=11 then 'Request canceled by User' when c.status_id=12 then 'Request rejected by HR'  " +
 "when c.status_id=13 then 'Request return by HR' when c.status_id=14 then 'Returned by Design Team' " +
 "when c.status_id=15 then 'Returned by Print Team' when c.status_id=16 then 'Returned by Despatch Team' end, " +
"case  when b.status_id=1 then b.No_of_cards else   k.No_of_cards end, " +
"ISNULL(b.branch_id,'') as branch_id, " +
 "ISNULL(d.designation_name,'') as designation_name ,  " +
 "ISNULL(b.display_designation_name,'') as display_designation_name,  " +
 "ISNULL(f.department_name,'') as department_name ,ISNULL(b.Display_department_name,'') as Display_department_name, " +
 "ISNULL(b.Display_cadre,'') as Display_cadre,  b.email_id,ISNULL(b.display_email,'') as display_email ,b.contact_no, " +
"ISNULL(b.display_contact_no,'') as display_contact_no,  ISNULL(b.display_branch_name,'') as display_branch_name , " +
"ISNULL(b.display_branch_address,'') as display_branch_address , b.tra_date,b.remarks, " +
" ISNULL(b.Expected_delivery_date,'') as Expected_delivery_date from vc_request_master a  " +
"inner join vc_request_cycle b on a.request_no = b.request_no   " +
"inner join vc_status_master c on b.status_id = c.status_id   " +
"left join designation_master d on b.designation_id=d.designation_id  " +
"left join cadre_master e on b.cadre_id=e.cadre_id   " +
"left join department_master f on b.department_id=f.department_id  " +
"left join VC_request_cycle k on a.emp_code =k.emp_code and k.status_id =2  and k.cycle_id in (select max(cycle_id) from VC_request_cycle  where status_id=2 group by emp_code)  " +
" where a.request_no = " + Request_no + " and a.emp_code= " + Emp_Code + " order by b.cycle_id").Tables(0)

            '            DT = DB.ExecuteDataSet("select a.request_no," +
            '"case when b.status_id=1 then 'User Requested' " +
            '" when b.status_id=1 then 'User Request' when b.status_id=2 then 'HR Approved' " +
            '" when b.status_id=3 then ' Designed'" +
            '" when b.status_id=4 then 'Printed'" +
            '" when b.status_id=5 then 'Despatched'" +
            '" when b.status_id=6 then ' Receive'" +
            '" when b.status_id=11 then 'Request canceled by User'" +
            '" when b.status_id=12 then 'Request rejected by HR'" +
            '" when b.status_id=13 then 'Request return by HR'" +
            '" when b.status_id=14 then 'Returned by Design Team'" +
            '" when b.status_id=15 then 'Returned by Print Team'" +
            '" when b.status_id=15 then 'Returned by Despatch Team' end," +
            '" a.tra_date,a.remarks from vc_request_cycle a " +
            '" inner join vc_status_master b on a.status_id = b.status_id where a.request_no = " + Request_no + " and convert(varchar,cast(a.tra_date as date)) >= convert(varchar,cast('" + From_Dt + "' as date)) and convert(varchar,cast(a.tra_date as date))<=convert(varchar,cast('" + To_Dt + "' as date)) order by a.cycle_id").Tables(0)
            '  


            'For Each DR In DT.Rows
            '    Dim TR00 As New TableRow
            '    Dim TR00_01, TR00_02, TR00_03, TR00_04, TR00_05 As New TableCell
            '    TR00.BorderWidth = "1"
            '    TR00.BorderColor = Drawing.Color.Silver
            '    TR00.BorderStyle = BorderStyle.Solid
            '    RH.AddColumn(TR00, TR00_01, 10, 10, "l", DR(1).ToString())
            '    RH.AddColumn(TR00, TR00_02, 10, 10, "l", DR(1).ToString())
            '    RH.AddColumn(TR00, TR00_03, 10, 10, "l", DR(1).ToString())
            '    RH.AddColumn(TR00, TR00_04, 10, 10, "l", DR(1).ToString())
            '    RH.AddColumn(TR00, TR00_05, 10, 10, "l", DR(1).ToString())
            '    tb.Controls.Add(TR00)
            '    RH.BlankRow(tb, 3)

            '    i += 1
            'Next

            For Each DR In DT.Rows


                i += 1


                Dim TR00 As New TableRow
                Dim TR00_00, TR00_01, TR00_02, TR00_03, TR00_04, TR00_05 As New TableCell
                TR00.BorderWidth = "1"
                TR00_00.Font.Bold = True
                TR00_01.Font.Bold = True
                RH.AddColumn(TR00, TR00_00, 1, 1, "l", i.ToString() + ".")
                RH.AddColumn(TR00, TR00_01, 10, 10, "l", DR(1).ToString())

                tb.Controls.Add(TR00)
                RH.BlankRow(tb, 3)



               

                




                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12,
TR3_13, TR3_14, TR3_15, TR3_16, TR3_17, TR3_18, TR3_19, TR3_20 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"
                TR3_15.BorderWidth = "1"
                TR3_16.BorderWidth = "1"
                TR3_17.BorderWidth = "1"
                TR3_18.BorderWidth = "1"
                TR3_19.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver
                TR3_15.BorderColor = Drawing.Color.Silver
                TR3_16.BorderColor = Drawing.Color.Silver
                TR3_17.BorderColor = Drawing.Color.Silver
                TR3_18.BorderColor = Drawing.Color.Silver
                TR3_19.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid
                TR3_15.BorderStyle = BorderStyle.Solid
                TR3_16.BorderStyle = BorderStyle.Solid
                TR3_17.BorderStyle = BorderStyle.Solid
                TR3_18.BorderStyle = BorderStyle.Solid
                TR3_19.BorderStyle = BorderStyle.Solid



                Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09 As New TableCell

                TRHead_00.BorderWidth = "1"
                TRHead_01.BorderWidth = "1"
                TRHead_02.BorderWidth = "1"
                TRHead_03.BorderWidth = "1"
                TRHead_04.BorderWidth = "1"
                TRHead_05.BorderWidth = "1"
                TRHead_06.BorderWidth = "1"
                TRHead_07.BorderWidth = "1"
                TRHead_08.BorderWidth = "1"
                TRHead_09.BorderWidth = "1"


                TRHead_00.BorderColor = Drawing.Color.Silver
                TRHead_01.BorderColor = Drawing.Color.Silver
                TRHead_02.BorderColor = Drawing.Color.Silver
                TRHead_03.BorderColor = Drawing.Color.Silver
                TRHead_04.BorderColor = Drawing.Color.Silver
                TRHead_05.BorderColor = Drawing.Color.Silver
                TRHead_06.BorderColor = Drawing.Color.Silver
                TRHead_07.BorderColor = Drawing.Color.Silver
                TRHead_08.BorderColor = Drawing.Color.Silver
                TRHead_09.BorderColor = Drawing.Color.Silver

                TRHead_00.BorderStyle = BorderStyle.Solid
                TRHead_01.BorderStyle = BorderStyle.Solid
                TRHead_02.BorderStyle = BorderStyle.Solid
                TRHead_03.BorderStyle = BorderStyle.Solid
                TRHead_04.BorderStyle = BorderStyle.Solid
                TRHead_05.BorderStyle = BorderStyle.Solid
                TRHead_06.BorderStyle = BorderStyle.Solid
                TRHead_07.BorderStyle = BorderStyle.Solid
                TRHead_08.BorderStyle = BorderStyle.Solid
                TRHead_09.BorderStyle = BorderStyle.Solid


                If DR(0) = 1 Then
                    

                    RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "No Of cards")
                    RH.AddColumn(TRHead, TRHead_01, 10, 10, "l", "Branch Code")
                    RH.AddColumn(TRHead, TRHead_02, 10, 10, "l", "Designation")
                    RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Department")
                    RH.AddColumn(TRHead, TRHead_04, 10, 10, "l", "Cadre")
                    RH.AddColumn(TRHead, TRHead_05, 15, 15, "l", "Email Id")
                    RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "Contact Number")
                    RH.AddColumn(TRHead, TRHead_07, 10, 10, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_08, 15, 15, "l", "Remarks")
                    tb.Controls.Add(TRHead)

                    RH.BlankRow(tb, 3)


                    RH.AddColumn(TR3, TR3_02, 10, 10, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(3).ToString())
                    RH.AddColumn(TR3, TR3_04, 10, 10, "l", DR(4).ToString())
                    RH.AddColumn(TR3, TR3_06, 10, 10, "l", DR(6).ToString())
                    RH.AddColumn(TR3, TR3_08, 10, 10, "l", DR(8).ToString())
                    RH.AddColumn(TR3, TR3_09, 15, 15, "l", DR(9).ToString())
                    RH.AddColumn(TR3, TR3_10, 10, 10, "l", DR(11).ToString())
                    RH.AddColumn(TR3, TR3_15, 10, 10, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 15, 15, "l", DR(16).ToString())



                ElseIf DR(0) = 2 Then

                    

                    RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "No Of cards")
                    RH.AddColumn(TRHead, TRHead_01, 5, 5, "l", "Branch Code")
                    RH.AddColumn(TRHead, TRHead_02, 10, 10, "l", "Designation")
                    RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Department")
                    RH.AddColumn(TRHead, TRHead_04, 15, 15, "l", "Email Id")
                    RH.AddColumn(TRHead, TRHead_05, 10, 10, "l", "Contact Number")
                    RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "Branch Name")
                    RH.AddColumn(TRHead, TRHead_07, 15, 15, "l", "Branch Address")
                    RH.AddColumn(TRHead, TRHead_08, 10, 10, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_09, 10, 10, "l", "Remarks")
                    tb.Controls.Add(TRHead)

                    RH.BlankRow(tb, 3)

                    RH.AddColumn(TR3, TR3_02, 5, 5, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_03, 5, 5, "l", DR(3).ToString())

                    RH.AddColumn(TR3, TR3_05, 10, 10, "l", DR(5).ToString())

                    RH.AddColumn(TR3, TR3_07, 10, 10, "l", DR(7).ToString())

                    RH.AddColumn(TR3, TR3_10, 15, 15, "l", DR(10).ToString())

                    RH.AddColumn(TR3, TR3_12, 10, 10, "l", DR(12).ToString())
                    RH.AddColumn(TR3, TR3_13, 10, 10, "l", DR(13).ToString())
                    RH.AddColumn(TR3, TR3_14, 15, 15, "l", DR(14).ToString())
                    RH.AddColumn(TR3, TR3_15, 10, 10, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 10, 10, "l", DR(16).ToString())

                ElseIf DR(0) = 3 Then

                   

                    RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "No Of cards")
                    RH.AddColumn(TRHead, TRHead_01, 30, 30, "l", "Branch Code")
                    RH.AddColumn(TRHead, TRHead_02, 30, 30, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_03, 30, 30, "l", "Remarks")
                    tb.Controls.Add(TRHead)

                    RH.BlankRow(tb, 3)

                    RH.AddColumn(TR3, TR3_02, 10, 10, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_03, 30, 30, "l", DR(3).ToString())

                    RH.AddColumn(TR3, TR3_15, 30, 30, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 30, 30, "l", DR(16).ToString())

                ElseIf DR(0) = 4 Then
                   

                    RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "No Of cards")
                    RH.AddColumn(TRHead, TRHead_01, 30, 30, "l", "Branch Code")
                    RH.AddColumn(TRHead, TRHead_02, 30, 30, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_03, 30, 30, "l", "Remarks")
                    tb.Controls.Add(TRHead)

                    RH.BlankRow(tb, 3)

                    RH.AddColumn(TR3, TR3_02, 10, 10, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_03, 30, 30, "l", DR(3).ToString())

                    RH.AddColumn(TR3, TR3_15, 30, 30, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 30, 30, "l", DR(16).ToString())

                ElseIf DR(0) = 5 Then

                    

                    RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "No Of cards")
                    RH.AddColumn(TRHead, TRHead_01, 10, 10, "l", "Branch Code")
                    RH.AddColumn(TRHead, TRHead_02, 20, 20, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_03, 30, 30, "l", "Remarks")
                    RH.AddColumn(TRHead, TRHead_04, 30, 30, "l", "Expected Date")
                    tb.Controls.Add(TRHead)

                    RH.BlankRow(tb, 3)

                    RH.AddColumn(TR3, TR3_02, 10, 10, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(3).ToString())

                    RH.AddColumn(TR3, TR3_15, 20, 20, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 30, 30, "l", DR(16).ToString())
                    RH.AddColumn(TR3, TR3_17, 30, 30, "l", DR(17).ToString())

                ElseIf DR(0) = 6 Then
                    RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Remarks")
                    tb.Controls.Add(TRHead)
                    RH.BlankRow(tb, 3)
                    RH.AddColumn(TR3, TR3_15, 10, 10, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 20, 20, "l", DR(16).ToString())

                ElseIf DR(0) = 11 Then
                    RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Remarks")
                    tb.Controls.Add(TRHead)
                    RH.BlankRow(tb, 3)
                    RH.AddColumn(TR3, TR3_15, 10, 10, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 20, 20, "l", DR(16).ToString())
                ElseIf DR(0) = 12 Then
                    RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Remarks")
                    tb.Controls.Add(TRHead)
                    RH.BlankRow(tb, 3)
                    RH.AddColumn(TR3, TR3_15, 10, 10, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 20, 20, "l", DR(16).ToString())
                ElseIf DR(0) = 13 Then
                    RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Remarks")
                    tb.Controls.Add(TRHead)
                    RH.BlankRow(tb, 3)
                    RH.AddColumn(TR3, TR3_15, 10, 10, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 20, 20, "l", DR(16).ToString())
                ElseIf DR(0) = 14 Then
                    RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Remarks")
                    tb.Controls.Add(TRHead)
                    RH.BlankRow(tb, 3)
                    RH.AddColumn(TR3, TR3_15, 10, 10, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 20, 20, "l", DR(16).ToString())
                ElseIf DR(0) = 15 Then
                    RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Remarks")
                    tb.Controls.Add(TRHead)
                    RH.BlankRow(tb, 3)
                    RH.AddColumn(TR3, TR3_15, 10, 10, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 20, 20, "l", DR(16).ToString())
                ElseIf DR(0) = 16 Then
                    RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "Date")
                    RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Remarks")
                    tb.Controls.Add(TRHead)
                    RH.BlankRow(tb, 3)
                    RH.AddColumn(TR3, TR3_15, 10, 10, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_16, 20, 20, "l", DR(16).ToString())


                End If


                tb.Controls.Add(TR3)

            Next

            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

End Class
