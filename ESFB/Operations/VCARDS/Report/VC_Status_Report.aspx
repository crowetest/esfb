﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="VC_Status_Report.aspx.vb" Inherits="VC_Status_Report"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css" />
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
<br />
    <table align="center" style="width: 30%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:30%;">
                Date From</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:TextBox ID="txt_From_Dt" class="NormalText" runat="server" 
                    Width="75%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txt_From_Dt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txt_From_Dt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>           
        </tr>
        <tr>
            <td style="width:30%;">
                Date To</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:TextBox ID="txt_To_Dt" class="NormalText" runat="server" 
                    Width="75%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txt_To_Dt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txt_To_Dt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                Status</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbStatus"  class="NormalText" runat="server" Font-Names="Cambria" 
                Width="75%" ForeColor="Black">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">             
                <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;
                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 10%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
            </td>        
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
               
                 <asp:HiddenField ID="hdnValue" runat="server" />
                 <asp:HiddenField ID="hdnAdmin" runat="server" />
                 <asp:HiddenField ID="hdnUser" runat="server" />
                 <asp:HiddenField ID="hdnReqNum" runat="server" />
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
     
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }

     
      
         
    function btnView_onclick() 
        {
            var adminFlag = document.getElementById("<%= hdnAdmin.ClientID %>").value.toString();
            var From_Dt = document.getElementById("<%= txt_From_Dt.ClientID %>").value;
            var To_Dt = document.getElementById("<%= txt_To_Dt.ClientID %>").value;
            var Status = document.getElementById("<%= cmbStatus.ClientID %>").value;
            if(adminFlag == "True"){
                window.open("View_VC_Status_Report.aspx?From=" +  From_Dt + "&To=" + To_Dt +"&Status=" + Status +" ", "_self"); 
            }       
            else{
                var emp_code = document.getElementById("<%= hdnUser.ClientID %>").value;
                window.open("View_VC_User_Status_Report.aspx?emp_code=" + emp_code + "&From=" +  From_Dt + "&To=" + To_Dt +"&Status=" + Status +" ", "_self"); 
            }           
        }    
   function btnExcelView_onclick() 
        {
            
        }      
   function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }      
   function FromServer(arg, context) {
         switch (context) {
            case 1:
                {                   
                      ComboFill(arg, "<%= cmbStatus.ClientID %>");
                            break;
                }           
            case 2:
               {                           
               
               }                            
            }
        }
    </script>
</asp:Content>

 