﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class View_VC_Detailed_Status_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTExcel As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date
    Dim AdminFlag As Boolean = False
    Dim GMASTER As New Master
    Dim UserID As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim DT As New DataTable
            Dim Emp_Code As String = CStr(Request.QueryString.Get("emp_code"))
            Dim Request_no As String = CStr(Request.QueryString.Get("req_no"))
            Dim From_Dt As Date = CDate(Request.QueryString.Get("From"))
            Dim To_Dt As Date = CDate(Request.QueryString.Get("To"))
            UserID = CInt(Session("UserID"))
            DT = GMASTER.GetEmpRoleList(UserID)
            For Each DRow In DT.Rows
                If DRow(2) = 3 Then
                    AdminFlag = True
                    Exit For
                End If
            Next
            Me.hdnAdmin.Value = AdminFlag.ToString()

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT1 As New DataTable
            DT1 = DB.ExecuteDataSet("Select emp_name from emp_master where emp_code = " + Emp_Code).Tables(0)


            Dim RowBG As Integer = 0
            Dim DR As DataRow
            RH.Heading(Session("FirmName"), tb, "Detailed Status Report", 100)
            tb.Attributes.Add("width", "100%")
            Dim TR1 As New TableRow
            Dim TR1_00, TR1_01 As New TableCell
            RH.AddColumn(TR1, TR1_00, 15, 15, "l", "Employee Code : ")
            RH.AddColumn(TR1, TR1_01, 30, 30, "l", Emp_Code.ToString())
            tb.Controls.Add(TR1)
            RH.BlankRow(tb, 3)

            Dim TR2 As New TableRow
            Dim TR2_00, TR2_01 As New TableCell
            RH.AddColumn(TR2, TR2_00, 15, 15, "l", "Employee Name : ")
            For Each DR1 In DT1.Rows
                RH.AddColumn(TR2, TR2_01, 30, 30, "l", DR1(0).ToString())
            Next
            tb.Controls.Add(TR2)
            RH.BlankRow(tb, 3)

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "Sl No")
            RH.AddColumn(TRHead, TRHead_01, 30, 30, "l", "Status")
            RH.AddColumn(TRHead, TRHead_02, 30, 30, "l", "Date")
            RH.AddColumn(TRHead, TRHead_03, 30, 30, "l", "Remarks")
            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer
            'If AdminFlag = True Then
            DT = DB.ExecuteDataSet("select a.request_no," +
"case when b.status_id=1 then 'User Requested' " +
" when b.status_id=1 then 'User Request' when b.status_id=2 then 'HR Approved' " +
" when b.status_id=3 then ' Designed'" +
" when b.status_id=4 then 'Printed'" +
" when b.status_id=5 then 'Despatched'" +
" when b.status_id=6 then ' Receive'" +
" when b.status_id=11 then 'Request canceled by User'" +
" when b.status_id=12 then 'Request rejected by HR'" +
" when b.status_id=13 then 'Request return by HR'" +
" when b.status_id=14 then 'Returned by Design Team'" +
" when b.status_id=15 then 'Returned by Print Team'" +
" when b.status_id=15 then 'Returned by Despatch Team' end," +
" a.tra_date,a.remarks from vc_request_cycle a " +
" inner join vc_status_master b on a.status_id = b.status_id where a.request_no = " + Request_no + " order by a.cycle_id").Tables(0)
            'Else
            '            DT = DB.ExecuteDataSet("select a.request_no," +
            '"case when b.status_id=1 then 'User Requested' " +
            '" when b.status_id=1 then 'User Request' when b.status_id=2 then 'HR Approved' " +
            '" when b.status_id=3 then ' Designed'" +
            '" when b.status_id=4 then 'Printed'" +
            '" when b.status_id=5 then 'Despatched'" +
            '" when b.status_id=6 then ' Receive'" +
            '" when b.status_id=11 then 'Request canceled by User'" +
            '" when b.status_id=12 then 'Request rejected by HR'" +
            '" when b.status_id=13 then 'Request return by HR'" +
            '" when b.status_id=14 then 'Returned by Design Team'" +
            '" when b.status_id=15 then 'Returned by Print Team'" +
            '" when b.status_id=15 then 'Returned by Despatch Team' end," +
            '" a.tra_date,a.remarks from vc_request_cycle a " +
            '" inner join vc_status_master b on a.status_id = b.status_id where a.request_no = " + Request_no + " and convert(varchar,cast(a.tra_date as date)) >= convert(varchar,cast('" + From_Dt + "' as date)) and convert(varchar,cast(a.tra_date as date))<=convert(varchar,cast('" + To_Dt + "' as date)) order by a.cycle_id").Tables(0)
            '            End If
            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 10, 10, "l", i.ToString())
                RH.AddColumn(TR3, TR3_01, 30, 30, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_02, 30, 30, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_03, 30, 30, "l", DR(3).ToString())

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

End Class
