﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class View_VC_Status_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTExcel As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim DT As New DataTable
            Dim From_Dt As Date = CDate(Request.QueryString.Get("From"))
            Dim To_Dt As Date = CDate(Request.QueryString.Get("To"))
            Dim Status As Integer = CInt(Request.QueryString.Get("Status"))

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            If Status = -1 Then
                RH.Heading(Session("FirmName"), tb, "Status Report", 100)
            Else
                DT = DB.ExecuteDataSet("select status_name from vc_status_master where status_id = " + Status.ToString()).Tables(0)
                For Each dtr In DT.Rows
                    RH.Heading(Session("FirmName"), tb, dtr(0).ToString() + " Report", 100)
                Next
            End If

            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid

            If Status = -1 Then
                RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "Sl No")
                RH.AddColumn(TRHead, TRHead_01, 5, 5, "l", "Emp Code")
                RH.AddColumn(TRHead, TRHead_02, 25, 25, "l", "Emp Name")
                RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Request Number")
                RH.AddColumn(TRHead, TRHead_04, 25, 25, "l", "Request Date")
                RH.AddColumn(TRHead, TRHead_05, 20, 20, "l", "Current Status")
                RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "")
                tb.Controls.Add(TRHead)
            Else
                RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "Sl No")
                RH.AddColumn(TRHead, TRHead_01, 10, 10, "l", "Emp Code")
                RH.AddColumn(TRHead, TRHead_02, 25, 25, "l", "Emp Name")
                RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Request Number")
                RH.AddColumn(TRHead, TRHead_04, 25, 25, "l", "Date")
                RH.AddColumn(TRHead, TRHead_05, 20, 20, "l", "")
                tb.Controls.Add(TRHead)
            End If
            

            RH.BlankRow(tb, 3)
            Dim i As Integer
            If Status = -1 Then
                DT = DB.ExecuteDataSet("select a.Emp_code,b.Emp_name,a.request_no,d.tra_date,c.Status_name from vc_request_master a inner join emp_master b on a.emp_code = b.emp_code inner join vc_status_master c on c.status_id = a.status_id inner join vc_request_cycle d on d.request_no = a.request_no and d.cycle_id in (select max(cycle_id) from vc_request_cycle group by request_no) where convert(varchar,cast(d.tra_date as date)) >= convert(varchar,cast('" + From_Dt + "' as date)) and convert(varchar,cast(d.tra_date as date))<=convert(varchar,cast('" + To_Dt + "' as date))").Tables(0)
            Else
                'DT = DB.ExecuteDataSet("select a.Emp_code,b.Emp_name,d.tra_date,c.Status_name from vc_request_master a inner join emp_master b on a.emp_code = b.emp_code inner join vc_status_master c on c.status_id = a.status_id inner join vc_request_cycle d on d.emp_code = a.emp_code where convert(varchar,cast(d.tra_date as date)) >= convert(varchar,cast('" + From_Dt + "' as date)) and convert(varchar,cast(d.tra_date as date))<=convert(varchar,cast('" + To_Dt + "' as date)) and cycle_id in (select min(cycle_id) from vc_request_cycle group by emp_code) and status_id = " + Status.ToString() + " order by emp_code").Tables(0)
                DT = DB.ExecuteDataSet("select a.emp_code,b.emp_name,a.request_no,c.tra_date from vc_request_master a inner join emp_master b on a.emp_code = b.emp_code inner join vc_request_cycle c on c.request_no = a.request_no where convert(varchar,cast(c.tra_date as date)) >= convert(varchar,cast('" + From_Dt + "' as date)) and convert(varchar,cast(c.tra_date as date))<=convert(varchar,cast('" + To_Dt + "' as date)) and c.status_id = " + Status.ToString() + " and c.cycle_id in (select max(cycle_id) from vc_request_cycle where status_id = " + Status.ToString() + " group by emp_code)").Tables(0)
            End If
            For Each DR In DT.Rows
                i += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_05.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid

                If Status = -1 Then
                    RH.AddColumn(TR3, TR3_00, 5, 5, "l", i.ToString())
                    RH.AddColumn(TR3, TR3_01, 5, 5, "l", DR(0).ToString())
                    RH.AddColumn(TR3, TR3_02, 25, 25, "l", DR(1).ToString())
                    RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_04, 25, 25, "l", CDate(DR(3)).ToString("dd/MM/yyyy"))
                    RH.AddColumn(TR3, TR3_05, 20, 20, "l", DR(4).ToString())
                    RH.AddColumn(TR3, TR3_06, 10, 10, "l", "<a href='View_VC_Detailed_Status_Report.aspx?req_no=" + DR(2).ToString() + "&emp_code=" + DR(0).ToString() + "&From=" + From_Dt + "&To=" + To_Dt + "' style='text-align:right;' target='_blank'> View Detail</a>")
                Else
                    RH.AddColumn(TR3, TR3_00, 10, 10, "l", i.ToString())
                    RH.AddColumn(TR3, TR3_01, 10, 10, "l", DR(0).ToString())
                    RH.AddColumn(TR3, TR3_02, 25, 25, "l", DR(1).ToString())
                    RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_04, 25, 25, "l", CDate(DR(3)).ToString("dd/MM/yyyy"))
                    RH.AddColumn(TR3, TR3_05, 20, 20, "l", "<a href='View_VC_Detailed_Status_Report.aspx?req_no=" + DR(2).ToString() + "&emp_code=" + DR(0).ToString() + "&From=" + From_Dt + "&To=" + To_Dt + "' style='text-align:right;' target='_blank'> View Detail</a>")
                End If
                
                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

End Class
