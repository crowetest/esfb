﻿
Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vc_DesighnUpload
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim GF As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim UserID As String
    Dim TypeId As Integer
    Dim ReqID As Integer
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Visiting Card Upload"

            If Not IsPostBack Then

                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
                If Not IsNothing(Request.QueryString.Get("ReqID")) Then
                    ReqID = CInt(GF.Decrypt(Request.QueryString.Get("ReqID")))
                End If
                hid_ReqID.Value = ReqID.ToString()

                Me.btnSave.Attributes.Add("onclick", "return UploadOnClick()")
                txtEmpCode.Focus()
            End If

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))

        If CInt(Data(0)) = 1 Then
            ID = hid_ReqID.Value


            DT = DB.ExecuteDataSet("select A.emp_code,b.emp_name,a.Request_No from VC_request_cycle A " +
                " inner join emp_master B on A.emp_code = b.emp_code " +
                " where (A.Request_No = " + ID + ")").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString + "|" + DT.Rows(0)(1).ToString + "|" + DT.Rows(0)(2).ToString
            End If

        ElseIf CInt(Data(0)) = 3 Then
           
        End If
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim ContentType As String = ""
            Dim FileName As String = ""
            Dim AttachImg As Byte() = Nothing
           

            Try

                Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
                If hfc.Count > 0 Then
                    For i = 0 To hfc.Count - 1
                        Dim myFile As HttpPostedFile = hfc(i)
                        Dim FileLen As Integer = 0
                        Dim Params(9) As SqlParameter
                        Dim bit As Integer = 5

                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        myFile = fup1.PostedFile
                        FileLen = myFile.ContentLength
                        AttachImg = New Byte(FileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, FileLen)
                        Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                        Params(0).Value = CInt(Data(0))
                        Params(1) = New SqlParameter("@checkbit", SqlDbType.Int)
                        Params(1).Value = bit
                        Params(2) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                        Params(2).Value = CStr(Data(1))
                        Params(3) = New SqlParameter("@status", SqlDbType.VarChar, 500)
                        Params(3).Value = CStr(Data(2))
                        Params(4) = New SqlParameter("@Request_No", SqlDbType.Int)
                        Params(4).Value = CInt(Data(3))
                        Params(5) = New SqlParameter("@attachment", SqlDbType.Image)
                        Params(5).Value = AttachImg
                        Params(6) = New SqlParameter("@attachmentType", SqlDbType.VarChar, 500)
                        Params(6).Value = ContentType
                        Params(7) = New SqlParameter("@attachmentFileName", SqlDbType.VarChar, 500)
                        Params(7).Value = FileName
                        Params(8) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(8).Direction = ParameterDirection.Output
                        Params(9) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(9).Direction = ParameterDirection.Output

                        DB.ExecuteNonQuery("[SP_VC_REQUEST]", Params)
                        ErrorFlag = CInt(Params(8).Value)
                        Message = CStr(Params(9).Value)
                    Next
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            'cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("         alert('" + Message + "');window.open('Vc_Designing.aspx','_self')")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

End Class
