﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile=" Vc_despatch.aspx.vb" Inherits="Vc_despatch" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">

   
     <style>
        .tblQal
        {
            border:9px;background-color:#F4D7C3; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
   
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <link href="../Style/ExportMenu.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript">return window_onload()</script>
        <script language="javascript" type="text/javascript">
      var RequestId;
        function window_onload() { 
            var ToData = "1Ø";                           
            ToServer(ToData, 1);
        }
        function btnExit_onclick() {

            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }    
         
        function FromServer(arg, context) {
            switch (context) {
                case 1:
                    var Data = arg;
                    if (Data!="")
                      {
                               document.getElementById("<%= hid_dtls.ClientID %>").value = Data;
                              table_fill();
                                
                      }
                       else
                      {
                        alert("no request for Despatch");
                        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");

                      }
                      break; 
                case 2:
                     var Data = arg.split("Ø");
                     alert(Data[1]);
                      window.open("Vc_despatch.aspx", "_self"); 
                    break; 
                case 3:
//                     var Data = arg.split("Ø");
//                    alert(Data[1]);
//                   window.open("Vc_despatch.aspx", "_self"); 
//                   break; 
            }
        }
        
   
   
    function btnSave_onclick() 
    {
           document.getElementById("<%= hid_Edit.ClientID %>").value = "";
          
                   row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("~");
                   
                 for (n = 1; n <= row.length - 1; n++) 
                  {   
                    col = row[n].split("ÿ");
                    if(document.getElementById("chk" + col[7]).checked==true)
                     {
                       var  expdate = document.getElementById("DateExpect" + col[7]).value;
                       if (expdate=="")
                       {
                         alert('Please select Expected Delivery Date of ' + col[1]);
                         return false;
                       }
                         var DespatchRemark= document.getElementById("txtRemarks" + col[7]).value;
                         if (DespatchRemark=="")
                       {
                         alert('Please Enter Despatch Remark of ' + col[1]);
                         return false;
                       }
                         
                           document.getElementById("<%= hid_Edit.ClientID %>").value += col[0].toString() + 'ÿ' +  DespatchRemark + 'ÿ' +  col[7].toString() + 'ÿ' + 5 + 'ÿ' + expdate + '~';
                           var dtl = document.getElementById("<%= hid_Edit.ClientID %>").value;
                     }
                    
                  }
                  if(document.getElementById("<%= hid_Edit.ClientID %>").value == "")
                  {
                     alert("Please tik the box for despatch");
                     return false;

                  }
                  
          
     }     



    function RejectOnClick(empcode,ReqId) 
    {  
       
        document.getElementById("<%= hdnValue.ClientID %>").value = empcode + "ÿ" + 16 + "ÿ" + ReqId;
        var dtl = document.getElementById("<%= hdnValue.ClientID %>").value;
        ToServer("2Ø" + dtl, 2);
    
    }
   
       
        function AutoRefresh( t ) {
	        setTimeout("location.reload(true);", t);
        }
        function table_fill()
        {
            var tab="";
            var row_bg = 0;
           
           document.getElementById("<%= pnHistory.ClientID %>").style.display = '';
            tab += "<div style='width:100%;  height:auto; margin: 0px auto; overflow-y:scroll' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;line-height:30px;background-color:#A27C82;font-family:'cambria';' align='center'>";
           
             tab += "<tr height=30px; class='tblQal'>";
                               
           tab += "<td style='width:5%;text-align:center' >#</td>";
            tab += "<td style='width:5%;text-align:center' >Emp Code</td>";
            tab += "<td style='width:10%;text-align:center' >Emp Name</td>";
            tab += "<td style='width:10%;text-align:center' >No: of cards</td>";
            tab += "<td style='width:10%;text-align:center' >Request Remark</td>";
             tab += "<td style='width:10%;text-align:center' >Approval Remark</td>";
              tab += "<td style='width:10%;text-align:center' >Design Remark</td>";
               tab += "<td style='width:10%;text-align:center' >Print Remark</td>";
                tab += "<td style='width:15%;text-align:center' >Expected delivery Date</td>";
//            tab += "<td style='width:3%;text-align:center' >Return</td>";
             tab += "<td style='width:5%;text-align:center' >Despatch</td>";
          tab += "<td style='width:10%;text-align:center' >Despatch Remarks</td>";
          
                           
            tab += "</tr>";
            tab += "</table></div>";
            tab += "<div id='ScrollDiv' style='width:100%; height:274px;overflow: scroll;margin: 0px auto;' >";
            tab += "<table style='width:100%;margin:0px auto;background-color:#A27C82;font-family:'cambria';' align='left'>";
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("~");
            for (n = 1; n <= row.length - 1; n++) {
                col = row[n].split("ÿ");
                if (document.getElementById("<%= hid_Dtls.ClientID %>").value != "") {
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr style='width:100%;font-family:cambria;background-color:#DDD8D1;' align='left' class=sub_first>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr style='width:100%;font-family:cambria;background-color:#F9F3F9;' align='left' class=sub_second>";
                }
                i = n ;
                
                tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                tab += "<td style='width:5%;text-align:center' >" + col[0] + "</td>";
                tab += "<td style='width:10%;text-align:center' >" + col[1] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[2] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[3] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[4] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[5] + "</td>";
                 tab += "<td style='width:10%;text-align:center'>" + col[6] + "</td>";

                
//                 var ExpectedDate = "<input type='date' id='DateExpect"+col[7]+"' name='DateExpect" + col[7] + "'></td>";                                            
                  tab += "<td style='width:15%;text-align:center'><input type='date' id='DateExpect"+col[7]+"' name='DateExpect" + col[7] + "'></td>";
//               tab += "<td style='width:5%;text-align:center'><input type='date' name='Expected Date"+col[7]+"' id='Expected_Date"+col[7]+"'></td>";
//                tab += "<td style='width:3%;text-align:center'><img src='../../image/return2.png' title='Return to HR' style='height:30px; width:30px; cursor:pointer;' onclick='RejectOnClick("+ col[0] +","+ col[7] +")' /></td>";              
               
               var txtid = "chk" +  col[7] ;  
                tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='" + txtid + "' /></td>";
  
                        
                          
//               tab+="<td  style='width:5%;text-align:center; '><img id='ViewReport' src='../../Image/attchment2.png' onclick='viewReport("+ col[7] +")' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' >";			                       
//                    tab+="</td>"; 
                     Remarks = "";
                     var  remarkId="txtRemarks" + col[7] ;
                    var txtRemarks = "<input id='" + remarkId + "' name='txtRemarks" + col[7] + "' value ='"+ Remarks +"' type='Text' style='width:99%;' class='NormalText' maxlength='50' onkeypress='return UnwantedCharCheck(event)'/>";                                            
                tab += "<td style='width:10%;text-align:left'>" + txtRemarks + "</td>";
                tab += "</tr>";
                
                }
            }
            tab += "</table></div>";
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;
            if (row.length == 0) {
                document.getElementById("<%= pnDisplay.ClientID %>").style.display = "none";
            }
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';  
         
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;  
        }
       
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    </html>
   
  
    <br />

    <table class="style1" style="width: 90%; margin: 0px auto;">
        <tr>
            <td style="width: 15%; text-align: Left;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 15%; text-align: Left;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 15%;" colspan="4">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
        </tr>
         <tr>
            <td style="width: 15%;" colspan="4">
                <asp:Panel ID="pnHistory" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="4">
                <br />
                 
<%--             <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="SAVE" onclick="return btnSave_onclick()" />&nbsp;
--%>    
                        <asp:Button ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="10%" /> &nbsp;
              
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;<asp:HiddenField ID="hid_Items"
                        runat="server" />
                        
                <asp:HiddenField ID="hdnValue" runat="server" />
                 <asp:HiddenField ID="hid_Edit" runat="server" />
                <asp:HiddenField ID="hid_dtls" runat="server" />
               
                <br />
               
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="4">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
