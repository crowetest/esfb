﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vc_Designing
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim HD As New HelpDesk
    Dim TeamID As Integer
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
    Dim GF As New GeneralFunctions
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
           
            Me.Master.subtitle = "Visiting Card Design"



            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then

            DT = DB.ExecuteDataSet("select a.emp_code,c.emp_name," +
"  case when a.status_id=15 then(select display_branch_name from VC_request_cycle where cycle_id in( select max(cycle_id) from  VC_request_cycle where status_id=2 group by emp_code) and  Request_No=a.Request_No) else b.display_branch_name end, " +
  "  case when a.status_id=15 then(select display_branch_address from VC_request_cycle where cycle_id in( select max(cycle_id) from  VC_request_cycle where status_id=2 group by emp_code) and  Request_No=a.Request_No) else b.display_branch_address end, " +
 " case when a.status_id=15 then(select display_department_name from VC_request_cycle where cycle_id in( select max(cycle_id) from  VC_request_cycle where status_id=2 group by emp_code) and  Request_No=a.Request_No) else b.display_department_name end, " +
 " case when a.status_id=15 then(select display_designation_name from VC_request_cycle where cycle_id in( select max(cycle_id) from  VC_request_cycle where status_id=2 group by emp_code) and  Request_No=a.Request_No) else b.display_designation_name end, " +
"  case when a.status_id=15 then(select display_email from VC_request_cycle where cycle_id in( select max(cycle_id) from  VC_request_cycle where status_id=2 group by emp_code) and  Request_No=a.Request_No) else b.display_email end, " +
 "   case when a.status_id=15 then(select display_contact_no from VC_request_cycle where cycle_id in( select max(cycle_id) from  VC_request_cycle where status_id=2 group by emp_code) and  Request_No=a.Request_No) else b.display_contact_no end, " +
" g.remarks as request_Remarks ,h.remarks as approval_Remarks,a.request_no  from VC_request_Master  a " +
" left join (select * from VC_request_cycle where cycle_id in (select max(cycle_id) from VC_request_cycle group by emp_code)) b  on a.Request_No=b.Request_No  " +
" left join emp_master c on a.emp_code=c.emp_code " +
" left join VC_request_cycle g on a.emp_code =g.emp_code and g.status_id =1 and  g.cycle_id in (select max(cycle_id) from VC_request_cycle where status_id =1  group by emp_code)" +
" left join VC_request_cycle h on a.emp_code =h.emp_code and h.status_id =2  and  h.cycle_id in (select max(cycle_id) from VC_request_cycle where status_id =2 group by emp_code)" +
" where a.status_id in (2,15) ").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "~" + DR(0).ToString() + "ÿ" + CStr(DR(1)) + "ÿ" + DR(2).ToString() + "ÿ" + DR(3).ToString() + "ÿ" + DR(4).ToString() + "ÿ" + DR(5).ToString() + "ÿ" + DR(6).ToString() + "ÿ" + DR(7).ToString() + "ÿ" + DR(8).ToString() + "ÿ" + DR(9).ToString() + "ÿ" + DR(10).ToString()
            Next
        ElseIf CInt(Data(0)) = 2 Then
            Try
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0

                Try

                    Dim Data1 As String() = CStr(Data(1)).Split(CChar("ÿ"))
                    Dim Params(5) As SqlParameter
                    Dim bit As Integer = 6
                    Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(0).Value = CInt(Data1(0))
                    Params(1) = New SqlParameter("@checkbit", SqlDbType.Int)
                    Params(1).Value = bit
                    Params(2) = New SqlParameter("@status", SqlDbType.Int)
                    Params(2).Value = CInt(Data1(1))
                    Params(3) = New SqlParameter("@Request_No", SqlDbType.Int)
                    Params(3).Value = CInt(Data1(2))
                    Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(5).Direction = ParameterDirection.Output

                    DB.ExecuteNonQuery("[SP_VC_REQUEST]", Params)
                    ErrorFlag = CInt(Params(4).Value)
                    Message = CStr(Params(5).Value)

                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try

                CallBackReturn = ErrorFlag.ToString + "Ø" + Message

            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End If
    End Sub
    
#End Region

End Class
