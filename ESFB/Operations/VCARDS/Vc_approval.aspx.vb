﻿
Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vc_approval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim HD As New HelpDesk
    Dim TeamID As Integer
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
    Dim GF As New GeneralFunctions
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GN.FormAccess(CInt(Session("UserID")), 108) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Me.Master.subtitle = "Visiting Card Approval"
            
            

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then

            DT = DB.ExecuteDataSet(" select a.emp_code,c.emp_name,b.tra_date," +
  "   case when a.status_id=14 then(select no_of_cards from VC_request_cycle where     cycle_id in  (select max(cycle_id) from VC_request_cycle  where request_no=a.Request_No and status_id=2 group by Request_No) ) else b.No_of_Cards end, " +
   " case when a.status_id=14 then(select remarks from VC_request_cycle where  cycle_id in  (select max(cycle_id) from VC_request_cycle  where request_no=a.Request_No and status_id=1 group by Request_No) ) else b.remarks end,a.Request_No, " +
   " case when a.status_id=14 then 'returned from Designer' else 'Requested by user' end,b.status_id,a.status_id from VC_request_Master  a " +
" inner join (select * from VC_request_cycle where cycle_id in " +
" (select max(cycle_id) from VC_request_cycle group by emp_code)) b on a.Request_No=b.Request_No " +
" inner join emp_master c on a.emp_code=c.emp_code where a.status_id in (1,14)").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "~" + DR(0).ToString() + "ÿ" + CStr(DR(1)) + "ÿ" + DR(2).ToString() + "ÿ" + DR(3).ToString() + "ÿ" + DR(4).ToString() + "ÿ" + DR(5).ToString() + "ÿ" + DR(6).ToString()
            Next
        End If
    End Sub
#End Region

End Class
