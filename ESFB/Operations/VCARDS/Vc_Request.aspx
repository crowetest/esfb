﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
 Async="true" CodeFile="Vc_Request.aspx.vb" Inherits="Vc_Request" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            font-size: 11px;
            color: #000000;
            border: 1px solid #7f9db9;
            }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
        .style1
        {
            width: 55%;
        }
        .style2
        {
            width: 74%;
        }
        .style3
        {
            width: 30%;
            height: 16px;
        }
        .style4
        {
            width: 46%;
            height: 16px;
        }
        .style5
        {
            width: 74%;
            height: 16px;
        }
        </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
  var ReqId;     
function window_onload()
{
    
   
        if (document.getElementById("<%= txtEmpCode.ClientID %>").value != "") 
        {
            ToServer("1Ø" + document.getElementById("<%= txtEmpCode.ClientID %>").value,1);

        }

}

 function checkMail(email) {

var filter = /^[\w.+\-]+@esafbank\.com$/;


                if (filter.test(email.value))
                {
                return true;
            }
            else   
              
                return false;
        }        

    function FromServer(arg, context) 
    {
   
       if (context == 1)
       {
            if(arg != "")
            {
                var Data = arg.split("|");

                document.getElementById("<%= txtEmpCode.ClientID %>").value = Data[0] ;
                document.getElementById("<%= txtEmpName.ClientID %>").value = Data[1] ;
                document.getElementById("<%= txtBranchAddress.ClientID %>").value = Data[3] ;
                document.getElementById("<%= txtBranchName.ClientID %>").value = Data[2] ;
                document.getElementById("<%= txtDepartment.ClientID %>").value = Data[4] ;
                document.getElementById("<%= txtDesignation.ClientID %>").value = Data[5] ;
                document.getElementById("<%= txtCadre.ClientID %>").value = Data[6] ;
                document.getElementById("<%= txtEmail.ClientID %>").value = Data[7] ;
                document.getElementById("<%= txtContact.ClientID %>").value = Data[8] ;
           }

           else
           {
                alert("Invalid Employee Code");
                document.getElementById("<%= txtEmpName.ClientID %>").value = "" ;
                document.getElementById("<%= txtBranchAddress.ClientID %>").value = "" ;
                document.getElementById("<%= txtBranchName.ClientID %>").value = "" ;
                document.getElementById("<%= txtDepartment.ClientID %>").value = "" ;
                document.getElementById("<%= txtDesignation.ClientID %>").value = "" ;
                document.getElementById("<%= txtCadre.ClientID %>").value = "" ;
                document.getElementById("<%= txtEmail.ClientID %>").value = "" ;
                document.getElementById("<%= txtContact.ClientID %>").value = "" ;
                
           }

         
        }
        else if (context == 2)
        {
         if(arg != "")
            {
                var Data = arg.split("|");
                document.getElementById("<%= txtEmpCode.ClientID %>").value = Data[0] ;
                document.getElementById("<%= txtEmpName.ClientID %>").value = Data[1] ;
                document.getElementById("<%= txtBranchName.ClientID %>").value = Data[3] ;
                document.getElementById("<%= txtBranchAddress.ClientID %>").value = Data[4] ;               
                document.getElementById("<%= txtDepartment.ClientID %>").value = Data[5] ;
                document.getElementById("<%= txtDesignation.ClientID %>").value = Data[6] ;
                document.getElementById("<%= txtCadre.ClientID %>").value = Data[7] ;
                document.getElementById("<%= txtEmail.ClientID %>").value = Data[8] ;
                document.getElementById("<%= txtContact.ClientID %>").value = Data[9] ;
                document.getElementById("<%= txtCards.ClientID %>").value = Data[10] ;
                document.getElementById("<%= txtReturnedRemark.ClientID %>").value = Data[11] ;
                document.getElementById("<%= hid_ReqID.ClientID %>").value = Data[12] ;
                document.getElementById("<%= txtRole.ClientID %>").value = Data[13];

                document.getElementById("RetRmrk").style.display='';
           }

          else
          {
             alert("No one request is returned by hr");
          }
        }
        else if (context == 3)
        {
         if(arg != "")
            {
                var Data = arg.split("|");
                document.getElementById("RetRmrk").style.display='none';
                document.getElementById("<%= txtEmpCode.ClientID %>").value = Data[0] ;
                document.getElementById("<%= txtEmpName.ClientID %>").value = Data[1] ;
                document.getElementById("<%= txtBranchAddress.ClientID %>").value = Data[3] ;
                document.getElementById("<%= txtBranchName.ClientID %>").value = Data[2] ;
                document.getElementById("<%= txtDepartment.ClientID %>").value = Data[4] ;
                document.getElementById("<%= txtDesignation.ClientID %>").value = Data[5] ;
                document.getElementById("<%= txtCadre.ClientID %>").value = Data[6] ;
                document.getElementById("<%= txtEmail.ClientID %>").value = Data[7] ;
                document.getElementById("<%= txtContact.ClientID %>").value = Data[8] ;
                 document.getElementById("<%= txtCards.ClientID %>").value = Data[9];
                document.getElementById("<%= hid_ReqID.ClientID %>").value = Data[11];
                 document.getElementById("<%= txtRole.ClientID %>").value = Data[12];

                ReqId = document.getElementById("<%= hid_ReqID.ClientID %>").value;
                var r = confirm("Do you want to cancel the previous request ");
                if (r) {
                            
//                            $("input[type=button]").attr("disabled", "disabled");

                            if (document.getElementById("<%= txtRemarks.ClientID %>").value == "") 
                            {
                             alert("Enter  Remarks");
                             document.getElementById("<%= txtRemarks.ClientID %>").focus();
                             return false;
                             }
                             var empcode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
                             var CancelRemark = document.getElementById("<%= txtRemarks.ClientID %>").value;
                             
                             document.getElementById("<%= hdnValue.ClientID %>").value = empcode + "Ø" + CancelRemark + "Ø" + 11 + "Ø" + ReqId;
                             var dtl = document.getElementById("<%= hdnValue.ClientID %>").value;
                             ToServer("4Ø" + dtl, 4);
                        }
                else 
                    {
                          
                        return false;
                        
                     } 
           }

          else
          {
             alert("no request for cancel");
          }
      }
      else if (context == 4)
        {
            var Data = arg.split("Ø");

            alert(Data[1]);
            window.open("Vc_Request.aspx", "_self");

            
        }
 }
  
     function btnReturnedRequest_onclick() 
     {
   
          if (document.getElementById("<%= txtEmpCode.ClientID %>").value != "") 
          {
            ToServer("2Ø" + document.getElementById("<%= txtEmpCode.ClientID %>").value,2);

          }   
        
     }
     function btnCancelRequest_onclick() 
     {
   
          if (document.getElementById("<%= txtEmpCode.ClientID %>").value != "") 
          {

            ToServer("3Ø" + document.getElementById("<%= txtEmpCode.ClientID %>").value,3);

          }   
        
     }
    function btnExit_onclick() {
   
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
     function RequestOnClick() 
     {  
        if (document.getElementById("<%= txtBranchAddress.ClientID %>").value == "") 
        {
            alert("Enter Branch Address");
            document.getElementById("<%= txtBranchAddress.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= txtRole.ClientID %>").value == "") 
        {
            alert("Enter Current Role");
            document.getElementById("<%= txtRole.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= txtCards.ClientID %>").value == "") 
        {
            alert("Enter No of cards required");
            document.getElementById("<%= txtCards.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= txtCards.ClientID %>").value >500) 
        {
            alert("maximum number of cards for print is 500");
            document.getElementById("<%= txtCards.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= txtRemarks.ClientID %>").value == "") 
        {
            alert("Enter Remarks");
            document.getElementById("<%= txtRemarks.ClientID %>").focus();
            return false;
        }

      
         if (document.getElementById("<%= txtEmail.ClientID %>").value != "") {
                var ret = checkMail(document.getElementById("<%= txtEmail.ClientID %>"));
                if (ret == false) {
                    alert('Please provide a valid official email address');
                    return false;
                }
            }
        
         document.getElementById("<%= hdnValue.ClientID %>").value =  "2Ø" + document.getElementById("<%= txtEmpCode.ClientID %>").value + "Ø" + document.getElementById("<%= txtCards.ClientID %>").value + "Ø" + document.getElementById("<%= txtRemarks.ClientID %>").value + "Ø" + 1 + "Ø" + document.getElementById("<%= txtBranchAddress.ClientID %>").value + "Ø" + document.getElementById("<%= txtRole.ClientID %>").value + "Ø" + document.getElementById("<%= txtEmail.ClientID %>").value + "Ø" + document.getElementById("<%= txtDesignation.ClientID %>").value  + "Ø" + document.getElementById("<%= txtCadre.ClientID %>").value + "Ø" + document.getElementById("<%= txtDepartment.ClientID %>").value + "Ø" + document.getElementById("<%= hid_ReqID.ClientID %>").value + "Ø" + document.getElementById("<%= txtContact.ClientID %>").value + "Ø" +document.getElementById("<%= txtBranchName.ClientID %>").value ;
         document.getElementById("<%= txtCards.ClientID %>").value = "";
         document.getElementById("<%= txtRemarks.ClientID %>").value = "";
         document.getElementById("<%= txtEmail.ClientID %>").value = "";
          document.getElementById("<%= txtBranchAddress.ClientID %>").value = "";
          document.getElementById("<%= txtRole.ClientID %>").value = "";
     }

    
  
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
//return window_onload()
////// ]]>
        </script>
    </head>
    </html>
    
    <br />
    <br />
    <br />
    <br />
        <div id ="div"/>
         <table class="style1" style="width:100%">
            <tr> 
                <td colspan="3"><asp:Panel ID="pnBranch" runat="server">
                </asp:Panel></td>
            </tr>
        </table> 
        <div />

    <br />
   
       <table align="center" style="width: 60%; margin:0px auto;">
      
        <tr>
            <td style="width:30%">
                Employee Code</td>
            <td class="style1">
                <asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="ReadOnlyTextBox" onkeypress='return NumericCheck(event)' MaxLength="10"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Employee Name</td>
            <td class="style1">
            <asp:TextBox ID="txtEmpName" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
       <tr>
            <td style="width:30%">
                Branch Name</td>
            <td class="style1">
 <asp:TextBox ID="txtBranchName" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>

            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Branch Address <span style="color:red;font-family:Cambria;">[Must be given Exact registered address]</span></td>
            <td class="style1">  
             <asp:TextBox ID="txtBranchAddress" runat="server" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)'  Width="89%"></asp:TextBox>

            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Department</td>
            <td class="style1">
                <asp:TextBox ID="txtDepartment" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        
        <tr>
            <td style="width:30%">
                Designation</td>
            <td class="style1">
                <asp:TextBox ID="txtDesignation" runat="server" placeholder="Enter Your Designation" Width="89%" class="NormalTextBox"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Current Role <span style="color:red;">[Teller,Sales officer,Branch Incharge etc.]</span></td>
            <td class="style1">
                <asp:TextBox ID="txtRole" runat="server" Width="89%" placeholder="Enter Your Current Role" class="NormalTextBox"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
         <tr>
            <td style="width:30%">
                Grade <span style="color:red;">[A0,B2,F1 Etc.]</span></td>
            <td class="style1">
                <asp:TextBox ID="txtCadre" runat="server" Width="89%" class="NormalTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr> 
        <tr>
            <td style="width:30%">
                Email <span style="color:red;">[Must be given bank email ID]</span></td>
            <td class="style1">
                <asp:TextBox ID="txtEmail" runat="server" Width="89%" class="NormalTextBox" MaxLength="50" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>  
         <tr>
            <td style="width:30%">
                Contact No <span style="color:red;">[Must be given bank CUG number]</span></td>
            <td class="style1">
                <asp:TextBox ID="txtContact" runat="server" Width="89%" class="NormalTextBox" MaxLength="15" onkeypress='return NumericCheck(event)' ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>  
         <tr>
            <td style="width:30%">
                No. of cards <span style="color:red;">[Maximum 500]</span></td>
            <td class="style1">
                <asp:TextBox ID="txtCards" runat="server" Width="89%" class="NormalTextBox" MaxLength="15" onkeypress='return NumericCheck(event)' ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>  
         <tr>
            <td style="width:30%">
                Remarks </td>
            <td class="style1">
            <textarea id="txtRemarks" class="NormalText" cols="20" name="S1" rows="3" runat="server" 
                    maxlength="400" style="width: 89%"  onkeypress="return blockSpecialChar(event)"></textarea>
                        </td>
            <td class="style2">
                &nbsp;</td>
        </tr> 
        <tr  id="RetRmrk" style="display:none;">
            <td style="width:30%">
               Returned Remarks </td>
            <td class="style1">
            <textarea id="txtReturnedRemark" class="NormalText" cols="20" name="S1" rows="3" runat="server" 
                    maxlength="400" style="width: 89%"  onkeypress="return blockSpecialChar(event)"></textarea>
                        </td>
            <td class="style2">
                &nbsp;</td>
        </tr>   
        <tr>
          <td style="width:30%">
               </td>
            <td class="style1">
            
                        </td>
            <td class="style2">
                &nbsp;</td>
        </tr>

            <tr>
            <td style="width:30%">
                &nbsp;</td>
            <td class="style1">
                <asp:Button ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="21%" /> &nbsp;
                    <input id="ReturnedRequest" type="button" value="RETURNED REQUEST" class="NormalText" 
                    style="width: 37%" onclick="return btnReturnedRequest_onclick()" />
                     <input id="Button1" type="button" value="CANCEL REQUEST" class="NormalText" 
                    style="width: 35%" onclick="return btnCancelRequest_onclick()" />
                   </td>

            <td class="style2"> 
                <input id="Button2" type="button" value="EXIT" class="NormalText" 
                    style="width: 53%" onclick="return btnExit_onclick()" />
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                &nbsp;</td>
            <td style="text-align:center" class="style2">
                &nbsp;</td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnValue" runat="server" />
    <asp:HiddenField ID="hid_ReqID" runat="server" />
    <asp:HiddenField ID="hdnSignId" runat="server" />
    <asp:HiddenField ID="hdnTypeId" runat="server" />
    <asp:HiddenField ID="hdnVerify" runat="server" />
    <asp:HiddenField ID="hid_dtls" runat="server" />

    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
