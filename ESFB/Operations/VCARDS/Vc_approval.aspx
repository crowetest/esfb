﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Vc_approval.aspx.vb" Inherits="Vc_approval" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <style>
      #loadingmsg   
      {
      width:60%;
      background: #fff; 
      padding: 0px;
      position: fixed;     
      z-index: 100;
      margin-left: 20%;
      margin-bottom: -25%;   
       
      }
      #loadingover {
      background: black;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }

   </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <link href="../../Style/ExportMenu.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript">return window_onload()</script>
        <script language="javascript" type="text/javascript">
      var RequestId;
        function window_onload() { 
            var ToData = "1Ø";                           
            ToServer(ToData, 1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }     
        function FromServer(arg, context) {
            switch (context) {
                case 1:
                    var Data = arg;
                    if (Data!="")
                    {
                       document.getElementById("<%= hid_dtls.ClientID %>").value = Data;
                       table_fill();
                       
                       
                    }
                    else
                    {
                        alert("no request for approval");
                        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
                    }
                      break;
                case 2:
                    
                       break; 
            }
        }
        function RequestAttend(RequestID)
         {
           
            window.open("VisitCardApproval_Attend.aspx?ReqID=" + btoa(RequestID),"_self");
         }
        function AutoRefresh( t ) {
	        setTimeout("location.reload(true);", t);
        }
        function table_fill()
        {
            var tab="";
            var row_bg = 0;
           
           document.getElementById("<%= pnHistory.ClientID %>").style.display = '';
            tab += "<div style='width:100%;  height:auto; margin: 0px auto; overflow-y:scroll' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px;>";
                                
           tab += "<td style='width:3%;text-align:center' >#</td>";
           tab += "<td style='width:5%;text-align:center' >Request Number</td>";
            tab += "<td style='width:5%;text-align:center' >Emp Code</td>";
            tab += "<td style='width:10%;text-align:center' >Emp Name</td>";
            tab += "<td style='width:10%;text-align:center' >Date of Request</td>";
            tab += "<td style='width:5%;text-align:center' >No. of cards</td>";
            tab += "<td style='width:10%;text-align:center' >Request Remark</td>";
            tab += "<td style='width:10%;text-align:center' >Case</td>";
            tab += "<td style='width:5%;text-align:center'>Attend</td>";
                           
            tab += "</tr>";
            tab += "</table></div>";
             if(document.getElementById("<%= hid_dtls.ClientID %>").value=="")
            {
                document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none'; 
                return;
            }         
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';  
            tab += "<div id='ScrollDiv' style='width:100%; height:274px;overflow: scroll;margin: 0px auto;' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='left'>";
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("~");
            for (n = 1; n <= row.length - 1; n++) {
                col = row[n].split("ÿ");
                if (document.getElementById("<%= hid_Dtls.ClientID %>").value != "") {
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr class=sub_first>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class=sub_second>";
                }
                i = n ;
              
                tab += "<td style='width:3%;text-align:center'>" + i + "</td>";
                 tab += "<td style='width:5%;text-align:center' >" + col[5] + "</td>";
                tab += "<td style='width:5%;text-align:center' >" + col[0] + "</td>";
                tab += "<td style='width:10%;text-align:center' >" + col[1] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[2] + "</td>";
                tab += "<td style='width:5%;text-align:center'>" + col[3] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[4] + "</td>";
                 tab += "<td style='width:10%;text-align:center'>" + col[6] + "</td>";
                tab += "<td style='width:5%;text-align:center'><input id='btnAttend' style='font-family: cambria; cursor: pointer; width: 67px;' type='button' value='ATTEND' onclick='RequestAttend("+ col[5] +")' /></td>";
                tab += "</tr>";
                
                }
            }
            tab += "</table><div><div>";   
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;     
            if (row.length == 0) {
                document.getElementById("<%= pnDisplay.ClientID %>").style.display = "none";   
            }
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';  
            tab += "<div id='ScrollDiv' style='width:100%; height:274px;margin: 0px auto;' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='left'>";  
                                         
         
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;  
        }
       
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    </html>
   
  
    <br />

    <table class="style1" style="width: 90%; margin: 0px auto;">
        <tr>
            <td style="width: 15%; text-align: Left;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 15%; text-align: Left;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 15%;" colspan="4">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
        </tr>
         <tr>
            <td style="width: 15%;" colspan="4">
                <asp:Panel ID="pnHistory" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="4">
                <br />

              
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;<asp:HiddenField ID="hid_Items"
                        runat="server" />
                <asp:HiddenField ID="hid_Value" runat="server" />
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_EmpDetails" runat="server" />
                <asp:HiddenField ID="hid_ItemAll" runat="server" />
                <br />
                <asp:HiddenField ID="hid_User" runat="server" />
                <asp:HiddenField ID="hdnRequest" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="4">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
