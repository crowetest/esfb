﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="SignatureBranchMapping.aspx.vb" Inherits="SignatureBranchMapping" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
   
        var today = new Date(),
        day = today.getDate(),
        month = today.getMonth()+1, //January is 0
        year = today.getFullYear();
        if(day<10){
                day='0'+day
            } 
        if(month<10){
            month='0'+month
        }
        today = year + '-' + month + '-' + day;

        function BranchCodeOnBlur() {
            var BrCode=document.getElementById("<%= txtBranchCode.ClientID %>").value;
            if(BrCode.length>0)
            {
                var ToData = "2Ø" + BrCode;          
                ToServer(ToData, 2);
            }
            else
            {
                Initialize();
                document.getElementById("<%= txtBranchCode.ClientID %>").focus();
            }
        }
        function block_unblock_dtp(i) {
           
            if (document.getElementById("cmb"+i).value == 2)
            {
                document.getElementById("txtFromDate"+i).disabled = false;
                document.getElementById("txtToDate"+i).disabled = false;
            }
            else
            {
                document.getElementById("txtFromDate"+i).disabled = true;
                document.getElementById("txtToDate"+i).disabled = true;                
            }
        }
        function Initialize()
        {  
            document.getElementById("<%= txtBranchCode.ClientID %>").value="";
            document.getElementById("<%= txtBranchName.ClientID %>").value="";
            document.getElementById("<%= txtDistrict.ClientID %>").value="";
            document.getElementById("<%= txtState.ClientID %>").value="";
            document.getElementById("<%= hid_dtls.ClientID %>").value="";
            table_fill();
        }
        function table_fill() {

            document.getElementById("<%= pnBranch.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var POA = "";
            var SIGN = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center'>Sl No</td>";
            tab += "<td style='width:4%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:10%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:8%;text-align:center'>Department</td>";
            tab += "<td style='width:8%;text-align:center'>Designation</td>";
            tab += "<td style='width:4%;text-align:center'>Grade</td>";
            tab += "<td style='width:6%;text-align:center'>Date of Joining</td>";
            tab += "<td style='width:5%;text-align:center'>POA</td>";
            tab += "<td style='width:4%;text-align:center'>View Signature</td>";
            tab += "<td style='width:7%;text-align:center'>Approval Type</td>";
            tab += "<td style='width:4%;text-align:center'>Start Date</td>";
            tab += "<td style='width:7%;text-align:center'>End Date</td>";
            tab += "<td style='width:7%;text-align:center'>Remarks</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n;
                    tab += "<td style='width:2%;text-align:center'>" + i + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[5] + "</td>";                  
                    if (col[6]=="")
                        POA = "";
                    else
                        POA = col[6];

                    tab += "<td style='width:5%;text-align:left'>" + POA + "</td>";
                    if (col[7] != 0)
                    {
                        
                        tab += "<td style='width:4%;text-align:left'><img id='ImgSignature' src='../Image/attchment2.png' onclick='viewSignature("+ col[7] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                        var select = "<select id='cmb" + i + "' class='NormalText' name='cmb" + i + "' style='width:100%' onchange='block_unblock_dtp("+i+")'   >";

                        if ((col[8] != 1) && (col[8] != 2))
                            select += "<option value='-1'>Select</option>";
                        if ((col[8] == 1))
                            select += "<option value='1' selected=true>PERMENANT  </option>";
                        else
                            select += "<option value='1'>PERMENANT  </option>";
                        if (col[8] == 2)
                            select += "<option value='2' selected=true>TEMPORARY  </option>";
                        else
                            select += "<option value='2'>TEMPORARY  </option>";

                        if ((col[8] == 1) || (col[8] == 2))
                            select += "<option value='3'>UN-MAP  </option>";

                        tab += "<td style='width:4%;text-align:left'>" + select + "</td>";

                        
                        var dd;
                        var mm;
                        var yyyy;
                 
                        if (col[9] != "1/1/1900 12:00:00 AM")
                        {
                     
                            var CreateDate = new Date(col[9]); 
                            dd = CreateDate.getDate(); if (dd<10) dd = "0"+dd;
                            mm = CreateDate.getMonth()+1; if (mm<10) mm = "0"+mm;
                            yyyy = CreateDate.getFullYear(); 
                            CreateDate = yyyy+'-'+mm+'-'+dd;
                            var FromDate = "<input type = 'date' ; id='txtFromDate" + i + "' name='txtFromDate" + i + "' value ='"+ CreateDate + "' min ='" + today +"' style='width:99%; float:left;' maxlength='300' ; />";
                        }
                        else
                            var FromDate = "<input type = 'date'; id='txtFromDate" + i + "' name='txtFromDate" + i + "' min ='" + today +"' style='width:99%; float:left;' maxlength='300' ; disabled/>";

                        tab += "<td style='width:4%;text-align:left'>" + FromDate + "</td>";


                        if (col[10] != "1/1/1900 12:00:00 AM")
                        {                           
                            var CreateDate1 = new Date(col[10]); 
                            dd = CreateDate1.getDate(); if (dd<10) dd = "0"+dd;
                            mm = CreateDate1.getMonth()+1; if (mm<10) mm = "0"+mm;
                            yyyy = CreateDate1.getFullYear(); 
                            CreateDate1 = yyyy+'-'+mm+'-'+dd;

                            var ToDate = "<input type = 'date'; id='txtToDate" + i + "' name='txtToDate" + i + "' value ='"+ CreateDate1 + "' min ='" + today +"' style='width:99%; float:left;' maxlength='300' ; />";
                        }
                        else
                        var ToDate = "<input type = 'date'; id='txtToDate" + i + "' name='txtToDate" + i + "' min ='" + today +"' style='width:99%; float:left;' maxlength='300' ; disabled ; />";

                        tab += "<td style='width:4%;text-align:left'>" + ToDate + "</td>";

                        var txtRemarks = "<input id='txtRemarks" + i + "' name='txtRemarks" + i + "' type='Text' style='width:99%;' class='NormalText' maxlength='50' />";                                            
                        tab += "<td style='width:7%;text-align:left'>" + txtRemarks + "</td>";
                    }
                    else
                    {
                        tab += "<td style='width:4%;text-align:left'></td>";
                        tab += "<td style='width:4%;text-align:left'></td>";
                        tab += "<td style='width:4%;text-align:left'></td>";
                        tab += "<td style='width:4%;text-align:left'></td>";
                        tab += "<td style='width:4%;text-align:left'></td>";
                    }
                   

                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnBranch.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//
        }
        function table_fill_history() {
            document.getElementById("<%= pnBranchHistory.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var POA = "";
            var SIGN = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:4%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:10%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:5%;text-align:center'>POA</td>";
            tab += "<td style='width:0%;text-align:center;display:none;'>SignId</td>";
            tab += "<td style='width:4%;text-align:center'>View Signature</td>";
            tab += "<td style='width:7%;text-align:center'>Approval Type</td>";
            tab += "<td style='width:4%;text-align:center'>Start Date</td>";
            tab += "<td style='width:7%;text-align:center'>End Date</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_history_dtls.ClientID %>").value != "") 
            {
                row = document.getElementById("<%= hid_history_dtls.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) 
                {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n;
                    tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:0%;text-align:left;display:none;' >" + col[3] + "</td>";
                   
                    if (col[3] != 0)
                        tab += "<td style='width:4%;text-align:left'><img id='ImgSignature' src='../Image/attchment2.png' onclick='viewSignature("+ col[3] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    else
                        tab += "<td style='width:8%;text-align:left'></td>";
    
                    if (col[4] != 0)
                        tab += "<td style='width:4%;text-align:left'>" + col[4] + "</td>";
                    else
                        tab += "<td style='width:4%;text-align:left'></td>";

                    if (col[5] != "")
                        tab += "<td style='width:4%;text-align:left'>" + col[5] + "</td>";
                    else
                        tab += "<td style='width:4%;text-align:left'></td>";

                    if (col[6] != "")
                        tab += "<td style='width:4%;text-align:left'>" + col[6] + "</td>";
                    else
                        tab += "<td style='width:4%;text-align:left'></td>";
              
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnBranchHistory.ClientID %>").innerHTML = tab;
            //--------------------- Clearing Data ------------------------//
        }

         function viewSignature(SignID)
        {
            window.open("Reports/ShowAttachment.aspx?SignID=" + btoa(SignID) + "&RptID=10");
            return false;
        }

        function UpdateValue() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("Nothing to save");
                return false;
            }
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 1; n <= row.length - 1; n++) 
            {
                col = row[n].split("µ");
                if (col[7] != 0)
                {
                    if (document.getElementById("cmb" + n).value == 2)
                    { 
                        if (document.getElementById("txtFromDate" + n).value == "") 
                        {
                            alert("Select Start Date");
                            document.getElementById("txtFromDate"+n).focus();
                            return false;
                        }
                        else if (document.getElementById("txtToDate" + n).value == "")
                        {
                            alert("Select End Date");
                            document.getElementById("txtToDate"+n).focus();
                            return false;
                        }

                        var todate = document.getElementById("txtToDate" + n).value;
                        var fromdate = document.getElementById("txtFromDate" + n).value;

                        if( (new Date(fromdate).getTime() > new Date(todate).getTime()))
                        {
                            alert("Start date should be less than End date");
                            document.getElementById("txtFromDate"+n).focus();
                            return false;
                        }
                    }

                    if ((document.getElementById("cmb" + n).value == 1) || (document.getElementById("cmb" + n).value == 2) || (document.getElementById("cmb" + n).value == 3))
                        document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[0] + "µ" + document.getElementById("cmb" + n).value + "µ" +document.getElementById("txtFromDate" + n).value + "µ" + document.getElementById("txtToDate" + n).value + "µ" + document.getElementById("txtRemarks" + n).value ;
                }
            }
            return true;
        }
        function FromServer(arg, context) {
            switch (context) {
                case 1:
                    {   
                        var Data = arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("SignatureBranchMapping.aspx", "_self");
                        break;
                    }
                case 2:
                    {
                        if(arg=="")
                        {
                             alert("Invalid Branch Code");
                             document.getElementById("<%= txtBranchCode.ClientID %>").value="";
                             Initialize();
                        }
                        else
                        {   
                            var Dtl=arg.split("~");
                            document.getElementById("<%= txtBranchName.ClientID %>").value=Dtl[0];
                            document.getElementById("<%= txtDistrict.ClientID %>").value=Dtl[1];
                            document.getElementById("<%= txtState.ClientID %>").value=Dtl[2];
                            document.getElementById("<%= hid_dtls.ClientID %>").value=Dtl[3];
                            table_fill();
                        }
                        break;
                    }
                case 3:
                    {
                        if(arg=="")
                        {
                             alert("History not found");
                             document.getElementById("<%= hid_history_dtls.ClientID %>").value="";
                        }
                        else
                        {   
                            document.getElementById("<%= hid_history_dtls.ClientID %>").value=arg;
                            document.getElementById("btnHistoryBranch").value= "History --"
                            table_fill_history();
                        }
                        break;
                    }
            }
        }
        function btnHistory_onclick()
        {
            var BrCode=document.getElementById("<%= txtBranchCode.ClientID %>").value;
            if(BrCode.length>0)
            {
                if (document.getElementById("btnHistoryBranch").value== "History ++")
                {
                    var ToData = "3Ø" + BrCode;          
                    ToServer(ToData, 3);
                }
                else
                {
                    document.getElementById("<%= hid_history_dtls.ClientID %>").value="";
                    document.getElementById("btnHistoryBranch").value= "History ++"
                    document.getElementById("<%= pnBranchHistory.ClientID %>").innerHTML = "";
                }
            }
        }
        function btnApprove_onclick() {
            var ret = UpdateValue();
            if (ret == true) 
            {
                if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                    alert("Nothing to save");
                    return false;
                }
                var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
                var Data = "1Ø" + document.getElementById("<%= txtBranchCode.ClientID %>").value + "Ø" + strempcode;
                ToServer(Data, 1);
            }
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

</script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_history_dtls" runat="server" />

<br />
 

    <div id ="divBranch">
    <table class="style1" style="width: 80%; margin: 0px auto;">

         <tr>
            <td style="width:25%"></td>
             <td style="width: 12%; text-align: left;">
                Branch Code
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranchCode" runat="server" Width="30%" class="NormalTextBox" onkeypress='return NumericCheck(event)' MaxLength="10"></asp:TextBox>
              </td>
        </tr>
         <tr>
            <td style="width:25%"></td>
             <td style="width: 12%; text-align: left;">
                Branch Name
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranchName" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width:25%"></td>
             <td style="width: 12%; text-align: left;">
                District
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDistrict" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width:25%"></td>
             <td style="width: 12%; text-align: left;">
                State
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtState" runat="server" Width="89%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox></td>
        </tr>
    </table> 

        <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnBranch" runat="server">
            </asp:Panel></td>
        </tr>
        
        </table> 
    </div>
   
    <table class="style1" style="width:100%">

        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
        <tr>
            <td style="text-align:left;" colspan="3">
                <br />
                <input id="btnHistoryBranch" style="font-family: cambria; cursor: pointer; width: 5%;"
                    type="button" value="History ++" onclick="return btnHistory_onclick()" />

            </td>
        &nbsp;&nbsp;
        </tr>
        <tr> 
            <td colspan="3"><asp:Panel ID="pnBranchHistory" runat="server">
            </asp:Panel></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>


