﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class EKYC_Verification_BranchResponse
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT, DT1, DTX As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim BranchID As Integer = CInt(Session("BranchID"))
            Dim UserID As Integer = CInt(Session("UserID"))
            DT1 = DB.ExecuteDataSet("select branch_id from emp_master where emp_code=" + UserID.ToString() + "").Tables(0)
            If CInt(DT1.Rows(0)(0).ToString()) < 1000 Then
                Me.hid_Admin.Value = "1"
            Else
                Me.hid_Admin.Value = "0"
            End If
            Me.Master.subtitle = "ReKYC Verification"

            DT = DB.ExecuteDataSet("select Branch_code,Branch_name,Cluster,Cust_NBR,Cust_Name,ACCT_OPEN_DT,CIF_ACTVT_DT," +
"   CIF_STATUS,ADDRESS_1,ADDRESS_2,Pin_Code,Ph_No,ReKYC_ID from ReKYC_VERIFICATION_MASTER " +
"  where Branch_code = " + BranchID.ToString() + " and Rekyc_status in (0,5)").Tables(0)
            Dim EKYC As String = ""
            For m As Integer = 0 To DT.Rows.Count - 1
                EKYC += "Ñ" + DT.Rows(m)(0).ToString() + "ÿ" + DT.Rows(m)(1).ToString() + "ÿ" + DT.Rows(m)(2).ToString() + "ÿ" + DT.Rows(m)(3).ToString() + "ÿ" + DT.Rows(m)(4).ToString() + "ÿ" + CDate(DT.Rows(0)(5).ToString()).ToString("dd-MMM-yyyy") + "ÿ" + CDate(DT.Rows(0)(6).ToString()).ToString("dd-MMM-yyyy") + "ÿ" + DT.Rows(m)(7).ToString() + "ÿ" + DT.Rows(m)(8).ToString() + "ÿ" + DT.Rows(m)(9).ToString() + "ÿ" + DT.Rows(m)(10).ToString() + "ÿ" + DT.Rows(m)(11).ToString() + "ÿ" + DT.Rows(m)(12).ToString()
            Next
           
            Me.hid_dtls.Value = EKYC




            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            If Not IsPostBack Then
                DT = GetBranch()
                If Not DT Is Nothing Then
                    'If hid_Admin.Value = "1" Then
                    GF.ComboFill(cmbBranch, DT, 0, 1)
                    cmbBranch.Items.Insert(0, New ListItem("-----select----", "-1"))
                    'Else
                    'GF.ComboFill(cmbBranch, DT, 0, 1)
                    'cmbBranch.Items.Insert(0, New ListItem("-----select----", "-1"))
                    'End If
                End If
            End If
            Me.cmbBranch.Attributes.Add("onchange", "return BranchOnchange()")

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
   
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim BranchId As Integer
            BranchId = CInt(Data(1))
            Dim StrWhere As String = " 1=1"
            Dim SqlStr As String = " "
            If BranchId <> 0 Then
                StrWhere += "and Branch_code = " + BranchId.ToString()
            End If
            SqlStr = "select Branch_code,Branch_name,Cluster,Cust_NBR,Cust_Name,ACCT_OPEN_DT,CIF_ACTVT_DT," +
         "   CIF_STATUS,ADDRESS_1,ADDRESS_2,Pin_Code,Ph_No,ReKYC_ID,Branch_Response,Description from ReKYC_VERIFICATION_MASTER " +
          "  where  Rekyc_status in (1) and "
            SqlStr += StrWhere
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            If DT.Rows.Count > 0 Then

                For m As Integer = 0 To DT.Rows.Count - 1
                    CallBackReturn += "Ñ" + DT.Rows(m)(0).ToString() + "ÿ" + DT.Rows(m)(1).ToString() + "ÿ" + DT.Rows(m)(2).ToString() + "ÿ" + DT.Rows(m)(3).ToString() + "ÿ" + DT.Rows(m)(4).ToString() + "ÿ" + CDate(DT.Rows(0)(5).ToString()).ToString("dd-MMM-yyyy") + "ÿ" + CDate(DT.Rows(0)(6).ToString()).ToString("dd-MMM-yyyy") + "ÿ" + DT.Rows(m)(7).ToString() + "ÿ" + DT.Rows(m)(8).ToString() + "ÿ" + DT.Rows(m)(9).ToString() + "ÿ" + DT.Rows(m)(10).ToString() + "ÿ" + DT.Rows(m)(11).ToString() + "ÿ" + DT.Rows(m)(12).ToString() + "ÿ" + DT.Rows(m)(13).ToString() + "ÿ" + DT.Rows(m)(14).ToString()
                Next

            End If
        ElseIf CInt(Data(0)) = 2 Then

            Dim UserID As Integer = CInt(Session("UserID"))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim SaveData As String = CStr(Data(1))
            If SaveData = "¥" Then
                SaveData = ""
            End If
            Dim mode As Integer = CInt(Data(2))
            Try

                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@SaveData", SqlDbType.VarChar, 5000)
                Params(1).Value = SaveData
                Params(2) = New SqlParameter("@Mode", SqlDbType.Int)
                Params(2).Value = mode
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_REKYC_VERIFICATION", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If

    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            Dim HeaderText As String
            HeaderText = "REKYC VERIFICATION REPORT"
            Dim BID As Integer = CInt(Me.cmbBranch.SelectedValue)
            '            DTX = DB.ExecuteDataSet("select Branch_code as [Branch Code],Branch_name as [Branch Name] ,Cluster,Cust_NBR as [Customer Number],Cust_Name as [Customer Name]," +
            '" ACCT_OPEN_DT as [Account Opening Date],CIF_ACTVT_DT as [CIF Activation Date],CIF_STATUS as [CIF Status],ADDRESS_1,ADDRESS_2,Pin_Code," +
            '" Ph_No,case when Branch_Response=1 then 'YES' when Branch_Response=2 then 'NO' end  as [Branch Response],Description" +
            '" from ReKYC_VERIFICATION_MASTER where Branch_code = " + BID.ToString() + " and Rekyc_status in (1)").Tables(0)

            Dim StrWhere As String = " 1=1"
            Dim SqlStr As String = " "
            If BID <> 0 Then
                StrWhere += "and Branch_code = " + BID.ToString()
            End If
            SqlStr = "select Branch_code as [Branch Code],Branch_name as [Branch Name] ,Cluster,Cust_NBR as [Customer Number],Cust_Name as [Customer Name]," +
" ACCT_OPEN_DT as [Account Opening Date],CIF_ACTVT_DT as [CIF Activation Date],CIF_STATUS as [CIF Status],ADDRESS_1,ADDRESS_2,Pin_Code," +
" Ph_No,case when Branch_Response=1 then 'YES' when Branch_Response=2 then 'NO' end  as [Branch Response],Description" +
" from ReKYC_VERIFICATION_MASTER  where  Rekyc_status in (1) and "
            SqlStr += StrWhere
            DTX = DB.ExecuteDataSet(SqlStr).Tables(0)



            WebTools.ExporttoExcel(DTX, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

#End Region

#Region "Function"
    Public Function GetBranch() As DataTable

        Return DB.ExecuteDataSet("select '0' as Branch_id,'-------All------' as Branch_name  union all select Branch_id,Branch_name from branch_master where branch_type=1").Tables(0)

    End Function
#End Region
End Class
