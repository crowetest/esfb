﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class SignatureUploadingVerification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1196) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

           

            Me.Master.subtitle = "Signature Uploading Verification"

            hid_dtls.Value = GetData(1) ''Default approval for is -Upload
            cmbApproval.Attributes.Add("onchange", "return ApprovalForOnChange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))

        If CInt(Data(0)) = 1 Then
            Dim dataval As String = CStr(Data(1))

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@VerifyDtl", SqlDbType.VarChar, 500)
                Params(1).Value = dataval.Substring(1)
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@App_Level", SqlDbType.Int)
                Params(4).Value = CInt(Me.hid_AppLevel.Value)
                DB.ExecuteNonQuery("SP_SIGN_UPLOAD_VERIFICATION", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString
        ElseIf CInt(Data(0)) = 2 Then
            Dim ApprovalFor As Integer = CInt(Data(1))
            CallBackReturn = GetData(ApprovalFor)
        End If




    End Sub
#End Region
    Public Function GetData(ByVal ApprovalFor As Integer) As String
        Dim StrUpload As String = ""
        If ApprovalFor = 1 Then
            If Is_User_HR_RO(CInt(Session("UserID"))) = 1 Then 'HR
                DT = DB.ExecuteDataSet("select A.emp_code,emp_name,a.branch_id, B.branch_name, C.Department_name,D.Designation_name,E.Cadre_name,Date_of_join, F.POA_No, F.sign_id from emp_master A  inner join Branch_master B on A.branch_id = B.Branch_id   inner join Department_master C on A.Department_id = C.Department_id  inner join Designation_master D on A.Designation_id = D.Designation_id  inner join Cadre_master E on A.Cadre_id = E.Cadre_id  inner join Sign_master F on A.emp_code = F.Emp_code where F.checker1_status =1 and F.checker_status is null").Tables(0)
                Me.hid_AppLevel.Value = "1"
            ElseIf Is_User_HR_RO(CInt(Session("UserID"))) = 2 Then 'RO
                DT = DB.ExecuteDataSet("select A.emp_code,emp_name,a.branch_id, B.branch_name, C.Department_name,D.Designation_name,E.Cadre_name,Date_of_join, F.POA_No, F.sign_id from emp_master A  inner join Branch_master B on A.branch_id = B.Branch_id   inner join Department_master C on A.Department_id = C.Department_id  inner join Designation_master D on A.Designation_id = D.Designation_id  inner join Cadre_master E on A.Cadre_id = E.Cadre_id  inner join Sign_master F on A.emp_code = F.Emp_code where F.checker1_status is null and A.reporting_to = " + CStr(Session("UserID")) + "").Tables(0)
                Me.hid_AppLevel.Value = "2"
            End If
            For n As Integer = 0 To DT.Rows.Count - 1
                StrUpload += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & CDate(DT.Rows(n)(7)).ToString("dd MMM yyyy") & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString
                If n < DT.Rows.Count - 1 Then
                    StrUpload += "¥"
                End If
            Next
        Else
            If Is_User_HR_RO(CInt(Session("UserID"))) = 1 Then 'HR
                DT = DB.ExecuteDataSet("select A.emp_code,emp_name,a.branch_id, B.branch_name, C.Department_name,D.Designation_name,E.Cadre_name,case when G.Status_Id =1 then 'Active' when G.Status_Id =2 then 'In-Active' end  from emp_master A  inner join Branch_master B on A.branch_id = B.Branch_id inner join Department_master C on A.Department_id = C.Department_id  inner join Designation_master D on A.Designation_id = D.Designation_id  inner join Cadre_master E on A.Cadre_id = E.Cadre_id  inner join Sign_master F on A.emp_code = F.Emp_code inner join Sign_Status_Change G on G.emp_code = F.Emp_code  where G.checker1_status =1 and G.checker2_status is null").Tables(0)
                Me.hid_AppLevel.Value = "1"
            ElseIf Is_User_HR_RO(CInt(Session("UserID"))) = 2 Then 'RO
                DT = DB.ExecuteDataSet("select A.emp_code,emp_name,a.branch_id, B.branch_name, C.Department_name,D.Designation_name,E.Cadre_name,case when G.Status_Id =1 then 'Active' when G.Status_Id =2 then 'In-Active' end  from emp_master A  inner join Branch_master B on A.branch_id = B.Branch_id inner join Department_master C on A.Department_id = C.Department_id  inner join Designation_master D on A.Designation_id = D.Designation_id  inner join Cadre_master E on A.Cadre_id = E.Cadre_id  inner join Sign_master F on A.emp_code = F.Emp_code inner join Sign_Status_Change G on G.emp_code = F.Emp_code  where G.checker1_status is null").Tables(0)
                Me.hid_AppLevel.Value = "2"
            End If
            For n As Integer = 0 To DT.Rows.Count - 1
                StrUpload += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString
                If n < DT.Rows.Count - 1 Then
                    StrUpload += "¥"
                End If
            Next
        End If

        Return StrUpload
    End Function
    Public Function Is_User_HR_RO(ByVal Emp_Code As Integer) As Integer
        '' if RO =2 ; HR=1
        DT = DB.ExecuteDataSet("select isnull(Count(*),0) from Emp_master where emp_code = " + Emp_Code.ToString() + " and Department_Id = 10 ").Tables(0)
        If CInt(DT.Rows(0)(0)) > 0 Then
            Return 1
        Else
            DT = DB.ExecuteDataSet("select isnull(Count(*),0) from Emp_master where status_id =1 and Reporting_to = " + Emp_Code.ToString() + "").Tables(0)
            If CInt(DT.Rows(0)(0)) > 0 Then
                Return 2
            End If
        End If

        Return 0
    End Function
End Class
