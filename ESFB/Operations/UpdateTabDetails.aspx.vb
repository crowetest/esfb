﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Operations_UpdateTabDetails
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String
    Dim GF As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Update TAB Details"
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        If Not IsPostBack Then
            txtEmpCode.Value = Session("USERID")
            txtEmpName.Value = Session("USERNAME").ToString().ToUpper()
            DT = DB.ExecuteDataSet("select -1 as Role_ID,'SELECT' as Role_Name union all select role_id,role_name from esfb.dbo.tab_role_master where status_id=1").Tables(0)
            GF.ComboFill(ddlRole, DT, 0, 1)
            DT = DB.ExecuteDataSet("select -1 as provider_id,'SELECT' as Provider_Name union all select provider_id,Provider_Name from esfb.dbo.tab_sim_provider where status_id=1").Tables(0)
            GF.ComboFill(ddlSIMProvider, DT, 0, 1)
            DT = DB.ExecuteDataSet("select -1 as branch_id,' SELECT' as BranchName union all select  branch_id,branch_name from esfb.dbo.branch_master where status_id=1 order by 1").Tables(0)
            GF.ComboFill(ddlBranch, DT, 0, 1)
            ddlBranch.SelectedValue = Session("BranchID")
            Me.txtAllotedOn_CalendarExtender.EndDate = CDate(Session("TraDt"))
        End If
        txtEmpCode.Attributes.Add("onchange", "return EmpCodeOnChange()")
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If Data(0) = 1 Then
            Dim ErrorFlag As Integer = 0
            Dim Message As String = ""
            Dim EmpCode As Integer = CInt(Data(1))
            Dim BranchID As Integer = CInt(Data(2))
            Dim RoleID As Integer = CInt(Data(3))
            Dim IMEINo As String = Data(4)
            Dim SIMProviderID As Integer = CInt(Data(5))
            Dim SIMNo As String = Data(6)
            Dim OwnerShip As Integer = CInt(Data(7))
            Dim DateOfJoin As Date = CDate(Data(8))
            Dim UserID As Integer = CInt(Session("UserID"))
            Try
                Dim Params(10) As SqlParameter
                Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(0).Value = EmpCode
                Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(1).Value = BranchID
                Params(2) = New SqlParameter("@RoleID", SqlDbType.Int)
                Params(2).Value = RoleID
                Params(3) = New SqlParameter("@IMEINo", SqlDbType.VarChar, 20)
                Params(3).Value = IMEINo
                Params(4) = New SqlParameter("@SimProviderID", SqlDbType.Int)
                Params(4).Value = SIMProviderID
                Params(5) = New SqlParameter("@SimNo", SqlDbType.VarChar, 12)
                Params(5).Value = SIMNo
                Params(6) = New SqlParameter("@OwnerShip", SqlDbType.Int)
                Params(6).Value = OwnerShip
                Params(7) = New SqlParameter("@AllotedOn", SqlDbType.Date)
                Params(7).Value = DateOfJoin
                Params(8) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(8).Value = UserID
                Params(9) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(9).Direction = ParameterDirection.Output
                Params(10) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(10).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_TAB_DETAILS_UPDATE", Params)
                ErrorFlag = CInt(Params(9).Value)
                Message = CStr(Params(10).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message
        ElseIf Data(0) = 2 Then
            Dim EmpCode As Integer = CInt(Data(1))
            DT = DB.ExecuteDataSet("select emp_name,branch_id from emp_master where emp_code=" + EmpCode.ToString()).Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn = DT.Rows(0)(0).ToString().ToUpper() + "~" + DT.Rows(0)(1).ToString()
            Else
                CallBackReturn = ""
            End If
        End If
    End Sub
End Class
