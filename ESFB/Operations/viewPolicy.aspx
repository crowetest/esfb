﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="viewPolicy.aspx.vb" Inherits="Operations_viewPolicy" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>  
    <script src="../Script/jquery1.6.4.min.js" type="text/javascript"></script>
   <style type="text/css">
        #overlay {
        width:99%;
        height:650px;
        z-index: 1;      
        }
        iframe {
        width:100%;
        height:650px;

        }
        #container 
        {
            width:100%;
            position: relative;
            text-align:center;
            margin:0px auto;
        }
        #overlay, iframe {
        position: absolute;
        top: 0;
        left: 0;
        }
   </style>
   <script type="text/javascript" language="javascript">
       document.oncontextmenu = document.body.oncontextmenu = function () { return false; }
   </script>
</head>
<body  oncontextmenu="return false;">
    <form id="form1" runat="server">
    <div ID="container" >
    <div id="overlay">
  </div>
   <iframe id="iframe" runat="server"  width="100%" height="650px" frameborder="0" scrolling="no" seamless="" ></iframe>  
</div>
 
    </form>
</body>
</html>
</asp:Content>