﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="viewITPolicies.aspx.vb" Inherits="Operations_viewITPolicies" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
 <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
 <script src="../Script/jquery1.6.4.min.js" type="text/javascript"></script>
 <style type="text/css">
 .sub_first
{
   background-color:white; height:20px;
   font-family:Cambria;color:#47476B;font-size:5px;
}
     .style1
     {
         width: 10%;
         height: 20px;
     }
     .style2
     {
         width: 80%;
         height: 20px;
     }
 </style>
<br />
<div   style="width: 60%;height:auto; margin:0px auto; background-color:silver">
    <%--<table align="center" style="width: 100%; margin:0px auto;"  >
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                1</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Change Management Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(27)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                2</td>
            <td style="width:80%;text-align:left;">
                 &nbsp;Migration Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(28)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                3</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Asset & Media Disposal Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(29)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                4</td>
            <td style="width:80%;text-align:left;">
                &nbsp;IT Outsourcing Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(30)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                5</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Third party vendor Management Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(31)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                6</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Exception Handling Policy and Procedure</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(32)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                7</td>
            <td style="width:80%;text-align:left;">
               &nbsp;Physical and Environmental Security Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(33)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                8</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Password Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(34)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                9</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Operating System Security Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(35)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                10</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Database Security Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(36)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                11</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Firewall Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(37)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                12</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Network Security Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(38)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                13</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Patch Management Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(39)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                14</td>
            <td style="width:80%;text-align:left;">
               &nbsp;Incident Management Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(40)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                15</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Email Security Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(41)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                16</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Acceptable Usage Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(42)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                17</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Personnel Security Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(43)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                18</td>
            <td style="width:80%;text-align:left;">
                &nbsp;IT Asset & Information Classification Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(44)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                19</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Anti-virus Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(45)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                20</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Cryptography Control Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(46)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                21</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Wireless Access Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(47)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                22</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Application Security Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(48)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                23</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Software Development Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(49)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                24</td>
            <td style="width:80%;text-align:left;">
               &nbsp;Backup & Archival Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(50)" target="_blank" >View</td>
        </tr>
         <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                25</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Access Control Policy and Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(51)" target="_blank" >View</td>
        </tr>
         <tr class="sub_first">
            <td style="text-align:center;" class="style1">
                26</td>
            <td style="text-align:left;" class="style2">
               &nbsp;IT Service Continuity Policy and Procedures</td>
            <td style="text-align:center;" class="style1">
                <a href='' onclick="return viewOnClick(52)" target="_blank" >View</td>
        </tr>
         <tr class="sub_first">
            <td style="text-align:center;" class="style1">
                27</td>
            <td style="text-align:left;" class="style2">
               &nbsp;Capacity management Policy & Procedures</td>
            <td style="text-align:center;" class="style1">
                <a href='' onclick="return viewOnClick(53)" target="_blank" >View</td>
        </tr>
         <tr class="sub_first">
            <td style="text-align:center;" class="style1">
                28</td>
            <td style="text-align:left;" class="style2">
               &nbsp;e-Channels Security Policy & Procedures</td>
            <td style="text-align:center;" class="style1">
                <a href='' onclick="return viewOnClick(54)" target="_blank" >View</td>
        </tr>
         <tr class="sub_first">
            <td style="text-align:center;" class="style1">
                29</td>
            <td style="text-align:left;" class="style2">
               &nbsp;Mobile computing Policy & Procedures</td>
            <td style="text-align:center;" class="style1">
                <a href='' onclick="return viewOnClick(55)" target="_blank" >View</td>
        </tr>
         <tr class="sub_first">
            <td style="text-align:center;" class="style1">
                30</td>
            <td style="text-align:left;" class="style2">
               &nbsp;Cloud Security  Governance Policy & Procedures</td>
            <td style="text-align:center;" class="style1">
                <a href='' onclick="return viewOnClick(56)" target="_blank" >View</td>
        </tr>
         <tr class="sub_first">
            <td style="text-align:center;" class="style1">
                31</td>
            <td style="text-align:left;" class="style2">
               &nbsp;Cyber Security Policy</td>
            <td style="text-align:center;" class="style1">
                <a href='' onclick="return viewOnClick(57)" target="_blank" >View</td>
        </tr>
         <tr class="sub_first">
            <td style="text-align:center;" class="style1">
                32</td>
            <td style="text-align:left;" class="style2">
               &nbsp;Website Usage Policy</td>
            <td style="text-align:center;" class="style1">
                <a href='' onclick="return viewOnClick(58)" target="_blank" >View</td>
        </tr>
         <tr class="sub_first">
            <td style="text-align:center;" class="style1">
                33</td>
            <td style="text-align:left;" class="style2">
               &nbsp;Privacy Policy</td>
            <td style="text-align:center;" class="style1">
                <a href='' onclick="return viewOnClick(59)" target="_blank" >View</td>
        </tr>
        <tr class="sub_first" style="height:30px;">
            <td style="text-align:center;" colspan="3">
                <input id="btnExit" type="button" value="EXIT" style="width:10%; font-family:Cambria;" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table></div>--%>
    <table align="center" style="width: 100%; margin:0px auto; height:auto;"  >
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                1</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Asset & Media Disposal Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href="Procedures/AssetMediaDisposalProceduresV1.pdf" target="_blank">View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                2</td>
            <td style="width:80%;text-align:left;">
                 &nbsp;Backup & Archival Procedures</td>
            <td style="width:10%;text-align:center;">
                <a href='Procedures/BackupArchivalProceduresV1.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                3</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Capacity Management Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='Procedures/CapacityManagementProceduresV1.2.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                4</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Change Management Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='Procedures/ChangeManagementProceduresV1.2.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                5</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Exception Handling Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='Procedures/ExceptionHandlingProceduresV1.2.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                6</td>
            <td style="width:80%;text-align:left;">
                &nbsp;IT Asset & Information Classification Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='Procedures/ITAssetInformationClassificationProceduresV1.2.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                7</td>
            <td style="width:80%;text-align:left;">
               &nbsp;IT Outsourcing Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='Procedures/ITOutsourcingProceduresV1.2.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                8</td>
            <td style="width:80%;text-align:left;">
                &nbsp;IT Service Continuity Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='Procedures/ITServiceContinuityProceduresV1.2.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                9</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Migration Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='Procedures/MigrationProceduresV1.2.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                10</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Software Development Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='Procedures/SoftwareDevelopmentProceduresV1.2.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                11</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Third Party Vendor Management Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='Procedures/ThirdPartyVendorManagementProceduresV1.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                12</td>
            <td style="width:80%;text-align:left;">
                &nbsp;User Access Management Procedures</td>
            <td style="width:10%;text-align:center;">
                 <a href='Procedures/UserAccessManagementProceduresV1.0.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first" style="height:30px;">
            <td style="text-align:center;" colspan="3">
                <input id="btnExit" type="button" value="EXIT" style="width:10%; font-family:Cambria;" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table></div>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../Home.aspx","_self");
        }

        function viewOnClick(ID) {
            window.open("viewPolicy.aspx?ID=" + btoa(ID), "_blank");
            return false;
        }

       
// ]]>
    </script>
</asp:Content>

