﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ApproveChecklist
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1220) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Me.Master.subtitle = "Checklist Approval"

            If Not IsPostBack Then
                hid_tab.Value = CStr(1)
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "CategoryFill();", True)
            End If
            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            hid_Id.Value = CStr(0)
            Me.btnApprove.Attributes.Add("onclick", "return Saveonclick()")
            Me.btnReject.Attributes.Add("onclick", "return Rejectonclick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim HDID As Integer = CInt(Data(0))
        Dim StatusID As Integer = 1
        Dim TabID As Integer = CInt(Data(1))

        If HDID = 2 Then 'fill category in panel
            Dim StrGrpnam As String = GF.GetPendingChecklist()
            If StrGrpnam <> "" Then
                CallBackReturn = "1~" + CStr(StrGrpnam)
            Else
                CallBackReturn = "2~"
            End If
        ElseIf HDID = 3 Then 'fill category in combo
            DT = GF.GetValidChecklistData()
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf HDID = 4 Then 'fill sub category in panel
            If TabID <> -1 Then
                Dim StrModulenam As String = GF.GetTasklistForApproval(CInt(Data(2)))

                If StrModulenam = "" Then
                    CallBackReturn = "2~"

                Else
                    CallBackReturn = "1~" + CStr(StrModulenam)
                End If
            Else
                CallBackReturn = "2~"
            End If
        ElseIf HDID = 5 Then 'fill sub category in combo
            DT = HD.GetModule(CInt(Data(2)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf HDID = 6 Then 'fill findings in panel
            If TabID <> -1 Then
                Dim StrProblemnam As String = HD.GetProblemName(CInt(Data(2)))
                If StrProblemnam = "" Then
                    CallBackReturn = "2~"

                Else
                    CallBackReturn = "1~" + CStr(StrProblemnam)
                End If
            Else
                CallBackReturn = "2~"
            End If
            'ElseIf HDID = 7 Then 'delete 
            'If TabID = 1 Then 'category

            '    DT = GF.GetChecklistDataTable(CInt(Data(2)))
            '    If DT.Rows.Count > 0 Then
            '        Dim RetVal As Integer = GF.DeleteChecklist(TabID, CInt(Data(2)))
            '        If RetVal = 1 Then
            '            CallBackReturn = "1~Deleted Successfully~"
            '        Else
            '            CallBackReturn = "2~Error Occured"
            '        End If
            '    Else
            '        CallBackReturn = "2~Can't delete , Data not found ."
            '    End If


            'ElseIf TabID = 2 Then 'Sub category

            'DT = GF.GetTasklistDataTable(CInt(Data(2)))
            'If DT.Rows.Count > 0 Then
            '    Dim RetVal As Integer = GF.DeleteChecklist(TabID, CInt(Data(2)))
            '    If RetVal = 1 Then
            '        CallBackReturn = "1~Deleted Successfully~"
            '    Else
            '        CallBackReturn = "2~Error Occured"
            '    End If
            'Else
            '    CallBackReturn = "2~Can't delete , Data not found ."
            'End If


            'ElseIf TabID = 3 Then 'Finding

            '    DT = HD.GetRequestexits(CInt(Data(2)))
            '    If DT.Rows.Count = 0 Then
            '        Dim RetVal As Integer = HD.DeleteGroup(TabID, CInt(Data(2)))
            '        If RetVal = 1 Then
            '            CallBackReturn = "1~Deleted Successfully"
            '        Else
            '            CallBackReturn = "2~Error Occured"
            '        End If
            '    Else
            '        CallBackReturn = "2~Can't delete ,Requests received under this finding ."
            '    End If


            'End If
        Else If HDID = 8 Then 'edit, approval assign 'shima 17/10
        Dim StrEdit As String = ""
        StrEdit = HD.EditMasterData(TabID, CInt(Data(2)))
        If StrEdit <> "" Then
            CallBackReturn = "1~" + StrEdit
        Else
            CallBackReturn = "2~"
        End If
        End If

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            Dim Data() As String = hid_Save.Value.Split(CChar("Ø"))
            Dim length As Integer = Data.Length
            Dim TabID As Integer = CInt(Data(0))
            'Dim StatusID As Integer = 1
            'Dim TabID As Integer = CInt(Data(1))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            'Dim Id As Integer = 0
            'Dim Remarks As String = ""
            'Dim Freq As Integer = 0
            'Dim Weightage As Integer = 0
            Dim Data1() As String = hid_Edit.Value.Split(CChar("Ø"))
            Try
                If TabID = 1 Then
                    Dim Params(3) As SqlParameter
                    If Data1.Length = 1 Then
                        ErrorFlag = 0
                        Message = "No items"
                    Else
                        For i As Integer = 1 To Data1.Length - 1
                            Params(0) = New SqlParameter("@hisid", SqlDbType.Int)
                            Params(0).Value = CInt(Data1(i))
                            Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                            Params(1).Direction = ParameterDirection.Output
                            Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                            Params(2).Direction = ParameterDirection.Output
                            Params(3) = New SqlParameter("@CreatedBy", SqlDbType.Int)
                            Params(3).Value = CStr(Session("UserID"))
                            DB.ExecuteNonQuery("SP_BRANCH_CHECKLIST_APPROVE", Params)
                            ErrorFlag = CInt(Params(1).Value)
                            Message = CStr(Params(2).Value)
                        Next
                    End If
                ElseIf TabID = 2 Then
                    Dim Params(3) As SqlParameter
                    For i As Integer = 1 To Data1.Length - 1
                        Params(0) = New SqlParameter("@TasklistID", SqlDbType.Int)
                        Params(0).Value = CInt(Data1(i))
                        Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(1).Direction = ParameterDirection.Output
                        Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(2).Direction = ParameterDirection.Output
                        Params(3) = New SqlParameter("@CreatedBy", SqlDbType.Int)
                        Params(3).Value = CStr(Session("UserID"))
                        DB.ExecuteNonQuery("SP_BRANCH_TASKLIST_APPROVE", Params)
                        ErrorFlag = CInt(Params(1).Value)
                        Message = CStr(Params(2).Value)
                    Next
                End If
                    If ErrorFlag = 0 Then
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "IntialCtrl();", True)
                    End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Dim Data() As String = hid_Save.Value.Split(CChar("Ø"))
            Dim length As Integer = Data.Length
            Dim TabID As Integer = CInt(Data(0))
            'Dim StatusID As Integer = 1
            'Dim TabID As Integer = CInt(Data(1))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            'Dim Id As Integer = 0
            'Dim Remarks As String = ""
            'Dim Freq As Integer = 0
            'Dim Weightage As Integer = 0
            Dim Data1() As String = hid_Edit.Value.Split(CChar("Ø"))
            Try
                If TabID = 1 Then
                    Dim Params(3) As SqlParameter
                    For i As Integer = 1 To Data1.Length - 1
                        Params(0) = New SqlParameter("@hisid", SqlDbType.Int)
                        Params(0).Value = CInt(Data1(i))
                        Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(1).Direction = ParameterDirection.Output
                        Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(2).Direction = ParameterDirection.Output
                        Params(3) = New SqlParameter("@CreatedBy", SqlDbType.Int)
                        Params(3).Value = CStr(Session("UserID"))
                        DB.ExecuteNonQuery("SP_BRANCH_CHECKLIST_REJECT", Params)
                        ErrorFlag = CInt(Params(1).Value)
                        Message = CStr(Params(2).Value)
                    Next
                ElseIf TabID = 2 Then
                    Dim Params(3) As SqlParameter
                    For i As Integer = 1 To Data1.Length - 1
                        Params(0) = New SqlParameter("@TasklistID", SqlDbType.Int)
                        Params(0).Value = CInt(Data1(i))
                        Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(1).Direction = ParameterDirection.Output
                        Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(2).Direction = ParameterDirection.Output
                        Params(3) = New SqlParameter("@CreatedBy", SqlDbType.Int)
                        Params(3).Value = CStr(Session("UserID"))
                        DB.ExecuteNonQuery("SP_BRANCH_TASKLIST_REJECT", Params)
                        ErrorFlag = CInt(Params(1).Value)
                        Message = CStr(Params(2).Value)
                    Next
                End If
                If ErrorFlag = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "IntialCtrl();", True)
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
