﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CCL_Entry
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1421) = False Then 'uat
                Response.Redirect("~/AccessDenied.aspx", False)
                Return

            End If
            Me.Master.subtitle = "COVID Care Loan"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim EmpCode = CInt(Session("UserID"))
            Dim BranchCode As String
            DT = DB.ExecuteDataSet("SELECT B.BRANCH_ID, B.BRANCH_NAME FROM BRANCH_MASTER B INNER JOIN EMP_MASTER A ON A.BRANCH_ID = B.BRANCH_ID WHERE A.EMP_CODE =" + EmpCode.ToString()).Tables(0)
            If DT.Rows.Count > 0 Then
                txtBranchCode.Text = DT.Rows(0)(0).ToString()
                txtBranchName.Text = DT.Rows(0)(1).ToString()
                BranchCode = DT.Rows(0)(0).ToString()
            End If
            DT1 = DB.ExecuteDataSet("SELECT '-1' AS CENTER_ID,'-----SELECT-----' AS CENTER_NAME UNION ALL SELECT DISTINCT CENTER_ID,CONVERT(VARCHAR,CENTER_ID) FROM CCL_MASTER_DATA WHERE BRANCH_ID = " + BranchCode.ToString()).Tables(0)
            GF.ComboFill(cmbCenterID, DT1, 0, 1)
            Me.cmbCenterID.Attributes.Add("onchange", "return CenterIDOnchange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window.onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim CenterID As String = Data(1).ToString()
            Dim BranchCode As String = Data(2).ToString()
            DT1 = DB.ExecuteDataSet("SELECT CLIENT_ID,CLIENT_NAME,CCL_ELIGIBLE_AMOUNT,FREQUENCY FROM CCL_MASTER_DATA WHERE BRANCH_ID = " + BranchCode.ToString() + " AND CENTER_ID = " + CenterID.ToString() + " AND STATUS IS NULL").Tables(0)
            Dim strClient As String = ""
            For n As Integer = 0 To DT1.Rows.Count - 1
                strClient += DT1.Rows(n)(0).ToString() + "µ" + DT1.Rows(n)(1).ToString() + "µ" + DT1.Rows(n)(2).ToString() + "µ" + DT1.Rows(n)(3).ToString()
                If n < DT1.Rows.Count - 1 Then
                    strClient += "¥"
                End If
            Next
            DT = DB.ExecuteDataSet("SELECT CENTER_NAME FROM CCL_MASTER_DATA WHERE CENTER_ID = " + CenterID.ToString() + " AND BRANCH_ID = " + BranchCode.ToString()).Tables(0)
            Dim CenterName As String = DT.Rows(0)(0).ToString()
            CallBackReturn = strClient + "Ø" + CenterName
        ElseIf CInt(Data(0)) = 2 Then
            Dim branchCode As Integer = CInt(Data(1).ToString())
            Dim centerID As String = Data(2).ToString()
            Dim loanData As String = Data(3).ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@Branch_ID", SqlDbType.Int)
                Params(0).Value = branchCode
                Params(1) = New SqlParameter("@Center_ID", SqlDbType.Int)
                Params(1).Value = centerID
                Params(2) = New SqlParameter("@LoanData", SqlDbType.VarChar, 5000)
                Params(2).Value = loanData
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@User_ID", SqlDbType.Int)
                Params(5).Value = UserID
                DB.ExecuteNonQuery("SP_CCL_ENTRY_SAVE", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn += ErrorFlag.ToString() + "Ø" + Message.ToString()
        End If
    End Sub
#End Region
End Class
