﻿'Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Text
Imports Excel = Microsoft.Office.Interop.Excel

Partial Class EC_Report_BTI
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1270) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.btnSave.Attributes.Add("onclick", "return btnUpload_onclick()")
            Me.Master.subtitle = "Excess Cash Report - BTI"
            If Not IsPostBack Then
                hid_tab.Value = CStr(1)
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim entrydate = CDate(Data(1).ToString())
            Dim xlApp As Excel.Application = New Microsoft.Office.Interop.Excel.Application()
            If xlApp Is Nothing Then
                MsgBox("Excel is not properly installed!!")
                Return
            End If
            Dim xlWorkBook As Excel.Workbook
            Dim xlWorkSheet As Excel.Worksheet
            Dim misValue As Object = System.Reflection.Missing.Value
            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("Sheet1")
            Dim i As Integer = 0
            Dim j As Integer = 2


            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim Params(2) As SqlParameter
            Params(0) = New SqlParameter("@EntryDate", SqlDbType.DateTime)
            Params(0).Value = entrydate
            Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(1).Direction = ParameterDirection.Output
            Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(2).Direction = ParameterDirection.Output
            DT = DB.ExecuteDataSet("SP_EC_Entry_REPORT", Params).Tables(0)
            ErrorFlag = CInt(Params(1).Value)
            Message = CStr(Params(2).Value)


            DT = DB.ExecuteDataSet("select entrydate as [Date],branchid as [Branch Code],branchname as [Branch Name],[2000] as [2000 (Count)], [500] as [500 (Count)], [200] as [200 (Count)],[100] as [100 (Count)],totalamount as [Total Amount],'' as [BTI Requested 2000 (Count)],'' as [BTI Requested 500 (Count)],'' as [BTI Requested 200 (Count)],'' as [BTI Requested 100 (Count)],'' as [Total Amount (BTI)] from ec_entry_master_temp where (cast(entrydate as date)) = (cast('" + entrydate + "' as date))").Tables(0)
            For Each DR In DT.Rows
                xlWorkSheet.Range("A1:M1").Font.Bold = True
                xlWorkSheet.Columns.WrapText = True
                xlWorkSheet.Columns("A").ColumnWidth = 15
                xlWorkSheet.Columns("B").ColumnWidth = 10
                xlWorkSheet.Columns("C").ColumnWidth = 15
                xlWorkSheet.Columns.Range("D:M").ColumnWidth = 10
                xlWorkSheet.Cells.HorizontalAlignment = Excel.Constants.xlCenter
                xlWorkSheet.Range("A1:M1").Interior.ColorIndex = 15
                xlWorkSheet.Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous
                xlWorkSheet.Cells(1, 1) = "Date"
                xlWorkSheet.Cells(1, 2) = "Branch Code"
                xlWorkSheet.Cells(1, 3) = "Branch Name"
                xlWorkSheet.Cells(1, 4) = "2000 (Count)"
                xlWorkSheet.Cells(1, 5) = "500 (Count)"
                xlWorkSheet.Cells(1, 6) = "200 (Count)"
                xlWorkSheet.Cells(1, 7) = "100 (Count)"
                xlWorkSheet.Cells(1, 8) = "Total Amount"
                xlWorkSheet.Cells(1, 9) = "BTI Requested 2000 (Count)"
                xlWorkSheet.Cells(1, 10) = "BTI Requested 500 (Count)"
                xlWorkSheet.Cells(1, 11) = "BTI Requested 200 (Count)"
                xlWorkSheet.Cells(1, 12) = "BTI Requested 100 (Count)"
                xlWorkSheet.Cells(1, 13) = "Total (BTI)"
                'xlWorkSheet.Cells(j, 1) = DR(0).ToString()
                xlWorkSheet.Cells(j, 1) = CDate(DR(0).ToString())
                xlWorkSheet.Cells(j, 2) = DR(1).ToString()
                xlWorkSheet.Cells(j, 3) = DR(2).ToString()
                xlWorkSheet.Cells(j, 4) = DR(3).ToString()
                xlWorkSheet.Cells(j, 5) = DR(4).ToString()
                xlWorkSheet.Cells(j, 6) = DR(5).ToString()
                xlWorkSheet.Cells(j, 7) = DR(6).ToString()
                xlWorkSheet.Cells(j, 8) = CInt(DR(7).ToString())
                xlWorkSheet.Cells(j, 9) = ""
                xlWorkSheet.Cells(j, 10) = ""
                xlWorkSheet.Cells(j, 11) = ""
                xlWorkSheet.Cells(j, 12) = ""
                xlWorkSheet.Cells(j, 13) = "=I" + j.ToString() + "*2000+J" + j.ToString() + "*500+K" + j.ToString() + "*200+L" + j.ToString() + "*100"
                xlWorkSheet.Range("I" + j.ToString() + ":M" + j.ToString()).Locked = False
                'xlWorkSheet.Range("I" + j.ToString() + ":M" + j.ToString()).NumberFormat = 
                i += 1
                j += 1
            Next
            xlWorkSheet.Protect(UserInterfaceOnly:=True)

            Dim today = CDate(DT.Rows(0)(0).ToString())
            Dim filename = "C:\Users\user\Downloads\Consolidated_Report_" + today.ToString("dd_MMM_yyyy") + ".xls"

            xlWorkBook.SaveAs(filename, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, _
             Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue)
            xlWorkBook.Close(True, misValue, misValue)
            xlApp.Quit()

            releaseObject(xlWorkSheet)
            releaseObject(xlWorkBook)
            releaseObject(xlApp)

            CallBackReturn = "Excel file created , you can find the file in downloads"
        End If
    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim msg As String = ""
        Dim connectionString As String = ""
        Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
        Dim DateValue As Date = CDate(Data(0))
        Dim user As String = Session("UserID").ToString()
        If Me.fup1.HasFile = False Then
            Dim cl_script0 As New System.Text.StringBuilder
            cl_script0.Append("         alert('Please browse excel');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
            Exit Sub
        Else
            Dim fileName As String = Path.GetFileName(fup1.PostedFile.FileName)
            Dim fileExtension As String = Path.GetExtension(fup1.PostedFile.FileName)
            Dim fileLocation As String = Server.MapPath("../" & fileName)
            fup1.SaveAs(fileLocation)

            If fileExtension = ".xls" Then
                'If Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE") = "x86" Then
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=NO;"""
                'Else
                '    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If

            ElseIf fileExtension = ".xlsx" Then
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties='Excel 12.0 Xml;HDR=NO'"
            Else
                'fileExtension = ".xlsx"
                Dim cl_script0 As New System.Text.StringBuilder
                cl_script0.Append("         alert('Uploaded Only Excel file with Extention .xls or .xlsx ');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
                Exit Sub

            End If
            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim values As New DataTable
            values.Columns.Add("[Date]", GetType(String))
            values.Columns.Add("[Branchid]", GetType(Long))
            values.Columns.Add("[BranchName‎]", GetType(String))
            values.Columns.Add("[2000]", GetType(Long))
            values.Columns.Add("[500]", GetType(Long))
            values.Columns.Add("[200]", GetType(Long))
            values.Columns.Add("[100]", GetType(Long))
            values.Columns.Add("[TotalAmount]", GetType(Long))
            values.Columns.Add("[BTI_2000]", GetType(Long))
            values.Columns.Add("[BTI_500‎]", GetType(Long))
            values.Columns.Add("[BTI_200]", GetType(Long))
            values.Columns.Add("[BTI_100]", GetType(Long))
            values.Columns.Add("[BTI_TotalAmount]", GetType(Long))
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            File.Delete(fileLocation)
            For count = 1 To dtExcelRecords.Rows.Count - 1
                Dim row = values.NewRow()
                row(0) = dtExcelRecords.Rows(count).Item(0)
                row(1) = dtExcelRecords.Rows(count).Item(1)
                row(2) = dtExcelRecords.Rows(count).Item(2)
                row(3) = dtExcelRecords.Rows(count).Item(3)
                row(4) = dtExcelRecords.Rows(count).Item(4)
                row(5) = dtExcelRecords.Rows(count).Item(5)
                row(6) = dtExcelRecords.Rows(count).Item(6)
                row(7) = dtExcelRecords.Rows(count).Item(7)
                row(8) = dtExcelRecords.Rows(count).Item(8)
                row(9) = dtExcelRecords.Rows(count).Item(9)
                row(10) = dtExcelRecords.Rows(count).Item(10)
                row(11) = dtExcelRecords.Rows(count).Item(11)
                row(12) = dtExcelRecords.Rows(count).Item(12)
                'row(13) = dtExcelRecords.Rows(count).Item(13)
                'row(14) = dtExcelRecords.Rows(count).Item(14)
                values.Rows.Add(row)
            Next
            con.Close()
            Try
                'Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TableData", dtExcelRecords), New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0), New System.Data.SqlClient.SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000), New System.Data.SqlClient.SqlParameter("@Date", hdnDate.Value)}
                Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TableData", values),
                                                                          New System.Data.SqlClient.SqlParameter("@Date", DateValue),
                                                                          New System.Data.SqlClient.SqlParameter("@User", user),
                                                                          New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0),
                                                                          New System.Data.SqlClient.SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)}
                Parameters(3).Direction = ParameterDirection.Output
                Parameters(4).Direction = ParameterDirection.Output
                'DB.ExecuteNonQuery("SALARY_DATA_MIGRATION", Parameters)
                DB.ExecuteNonQuery("SP_EC_Upload", Parameters)
                ErrorFlag = CInt(Parameters(3).Value)
                Message = CStr(Parameters(4).Value)

                Dim cl_script1 As New System.Text.StringBuilder
                cl_script1.Append("         alert('" + Message.ToString + "');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)


            Catch ex As Exception
                MsgBox("Exceptional Error Occurred.Please Inform Application Team")
                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try
        End If
    End Sub
    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownload.Click
    '    Dim xlApp As Excel.Application = New Microsoft.Office.Interop.Excel.Application()
    '    If xlApp Is Nothing Then
    '        MsgBox("Excel is not properly installed!!")
    '        Return
    '    End If
    '    Dim xlWorkBook As Excel.Workbook
    '    Dim xlWorkSheet As Excel.Worksheet
    '    Dim misValue As Object = System.Reflection.Missing.Value
    '    xlWorkBook = xlApp.Workbooks.Add(misValue)
    '    xlWorkSheet = xlWorkBook.Sheets("Sheet1")
    '    Dim i As Integer = 0
    '    Dim j As Integer = 2

    '    DT = DB.ExecuteDataSet("select entrydate as [Date],branchid as [Branch Code],branchname as [Branch Name],[2000] as [2000 (Count)], [500] as [500 (Count)], [200] as [200 (Count)],[100] as [100 (Count)],totalamount as [Total Amount],'' as [BTI Requested 2000 (Count)],'' as [BTI Requested 500 (Count)],'' as [BTI Requested 200 (Count)],'' as [BTI Requested 100 (Count)],'' as [Total Amount (BTI)] from ec_entry_master_temp ").Tables(0)
    '    'where (cast(entrydate as date)) = (cast('" + EntryDate + "' as date))
    '    For Each DR In DT.Rows
    '        xlWorkSheet.Range("A1:M1").Font.Bold = True
    '        xlWorkSheet.Columns.WrapText = True
    '        xlWorkSheet.Columns("A").ColumnWidth = 15
    '        xlWorkSheet.Columns("B").ColumnWidth = 10
    '        xlWorkSheet.Columns("C").ColumnWidth = 15
    '        xlWorkSheet.Columns.Range("D:M").ColumnWidth = 10
    '        xlWorkSheet.Cells.HorizontalAlignment = Excel.Constants.xlCenter
    '        xlWorkSheet.Range("A1:M1").Interior.ColorIndex = 15
    '        xlWorkSheet.Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous
    '        xlWorkSheet.Cells(1, 1) = "Date"
    '        xlWorkSheet.Cells(1, 2) = "Branch Code"
    '        xlWorkSheet.Cells(1, 3) = "Branch Name"
    '        xlWorkSheet.Cells(1, 4) = "2000 (Count)"
    '        xlWorkSheet.Cells(1, 5) = "500 (Count)"
    '        xlWorkSheet.Cells(1, 6) = "200 (Count)"
    '        xlWorkSheet.Cells(1, 7) = "100 (Count)"
    '        xlWorkSheet.Cells(1, 8) = "Total Amount"
    '        xlWorkSheet.Cells(1, 9) = "BTI Requested 2000 (Count)"
    '        xlWorkSheet.Cells(1, 10) = "BTI Requested 500 (Count)"
    '        xlWorkSheet.Cells(1, 11) = "BTI Requested 200 (Count)"
    '        xlWorkSheet.Cells(1, 12) = "BTI Requested 100 (Count)"
    '        xlWorkSheet.Cells(1, 13) = "Total (BTI)"
    '        'xlWorkSheet.Cells(j, 1) = DR(0).ToString()
    '        xlWorkSheet.Cells(j, 1) = CDate(DR(0).ToString())
    '        xlWorkSheet.Cells(j, 2) = DR(1).ToString()
    '        xlWorkSheet.Cells(j, 3) = DR(2).ToString()
    '        xlWorkSheet.Cells(j, 4) = DR(3).ToString()
    '        xlWorkSheet.Cells(j, 5) = DR(4).ToString()
    '        xlWorkSheet.Cells(j, 6) = DR(5).ToString()
    '        xlWorkSheet.Cells(j, 7) = DR(6).ToString()
    '        xlWorkSheet.Cells(j, 8) = CInt(DR(7).ToString())
    '        xlWorkSheet.Cells(j, 9) = ""
    '        xlWorkSheet.Cells(j, 10) = ""
    '        xlWorkSheet.Cells(j, 11) = ""
    '        xlWorkSheet.Cells(j, 12) = ""
    '        xlWorkSheet.Cells(j, 13) = "=I" + j.ToString() + "*2000+J" + j.ToString() + "*500+K" + j.ToString() + "*200+L" + j.ToString() + "*100"
    '        xlWorkSheet.Range("I" + j.ToString() + ":M" + j.ToString()).Locked = False
    '        i += 1
    '        j += 1
    '    Next
    '    xlWorkSheet.Protect(UserInterfaceOnly:=True)

    '    Dim today = Date.Now
    '    Dim filename = "C:\Users\user\Downloads\Consolidated_Report_" + today.ToString("dd_MMM_yyyy") + ".xls"

    '    xlWorkBook.SaveAs(filename, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, _
    '     Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue)
    '    xlWorkBook.Close(True, misValue, misValue)
    '    xlApp.Quit()

    '    releaseObject(xlWorkSheet)
    '    releaseObject(xlWorkBook)
    '    releaseObject(xlApp)

    '    MsgBox("Excel file created , you can find the file d:\csharp-Excel.xls")
    'End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
End Class
