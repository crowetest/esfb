﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ExcessCashApprove.aspx.vb" Inherits="ExcessCashApprove" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);         
        }        
        .Button:hover
        {            
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            color:#801424;
        }                  
     .bg
     {
         background-color:#FFF;
     }           
    </style>    
    <script language="javascript" type="text/javascript">
        var width;
        window.onload = function () {
            TableFill();
        }
        function TableFill(){
            document.getElementById("<%= pnlEntries.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div class=mainhead style='width:100%; height:auto; padding-top:0px;margin:0px auto;'>";
            tab += "<table style='width:100%;font-family:'cambria';' align='center'>";
            var DTData = document.getElementById("<%= hidData.ClientID %>").value.toString();
            var data = DTData;
            tab += "<tr>";
            tab += "<td style='width:7%;text-align:center;' class=NormalText>Date</td>";
            tab += "<td style='width:5%;text-align:center;' class=NormalText>Branch Code</td>";
            tab += "<td style='width:10%;text-align:center;' class=NormalText>Branch Name</td>";
            var data_master = document.getElementById("<%= hidDenominations.ClientID %>").value;
            var deno_master = data_master.split("Ñ");            
            width = 25/(deno_master.length - 1);
            for( var i = 0; i < deno_master.length-1;i++){
                tab += "<td style='width:"+width+"%;text-align:center;' class=NormalText>"+deno_master[i]+"<br/> (X)</td>";
            }
            tab += "<td style='width:10%;text-align:center;' class=NormalText>Total</td>";
            tab += "<td style='width:18%;text-align:center;' class=NormalText>In words</td>";
            tab += "<td style='width:15%;text-align:center;' class=NormalText>Remarks</td>";
            tab += "<td style='width:10%;text-align:center;' class=NormalText>Status</td>";
            tab += "</tr>";
            tab += "</table></div>";

            if(data!=''){
                tab += "<div class=mainhead style='width:100%; height:auto; overflow:auto; margin:0px auto;' >";
                tab += "<table style='width:100%; margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr class=sub_first>";
                var EData = data.split("Ñ");
                tab += "<td style='width:7%;text-align:center;' class=NormalText>"+EData[2]+"</td>";
                tab += "<td style='width:5%;text-align:center;' class=NormalText>"+EData[3]+"</td>";
                tab += "<td style='width:10%;text-align:center;' class=NormalText>"+EData[4]+"</td>";                                    
                var Deno = document.getElementById("<%= hidDenoData.ClientID %>").value.split("Ñ");
                var data_master = document.getElementById("<%= hidDenominations.ClientID %>").value;
                var deno_master = data_master.split("Ñ");
                for(var i = 0;i<Deno.length-1;i++){
                    for(var k = 0;k<deno_master.length-1;k++){
                        var entryData = Deno[i].toString().split("ÿ");
                        if(entryData[0]==EData[0]){ 
                            if(entryData[1]==deno_master[k]){
                                tab += "<td style='width:"+width+"%;text-align:center;' class=NormalText>"+entryData[2]+"</td>";
                            }
                        }
                    }
                }
                tab += "<td style='width:10%;text-align:center;' class=NormalText>"+Math.abs(EData[5]).toFixed(2)+"</td>";
                tab += "<td style='width:18%;text-align:center;' class=NormalText>"+EData[6]+"</td>";
                var txtBox = "<textarea id='txtRemarks' name='txtRemarks' type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)' ></textarea>";
                tab += "<td style='width:15%;text-align:center;' class=NormalText>"+txtBox+"</td>";
                var select = "<select id='drpStatus' class='NormalText' name='drpStatus' style='width:90%;'>";      
                select += "<option value='-1'> ---Select---  </option>";
                select += "<option value='1'>Approve</option>";
                select += "<option value='2'>Reject</option>";
                select += "</select>";
                tab += "<td style='width:10%;text-align:center;' class=NormalText>"+select+"</td>";
                tab += "</tr>";
                tab += "</table></div>";
            }
            else{
                tab += "<div class=mainhead style='width:100%; height:auto; overflow:auto; margin:0px auto;' >";
                tab += "<table style='width:100%; margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr class=sub_first>";
                tab += "<td style='width:10%;height:35px;text-align:center;' class=NormalText>No entries pending for approval</td>";
                tab += "</tr>";
                tab += "</table></div>";
            }
            document.getElementById("<%= pnlEntries.ClientID %>").innerHTML = tab;            
        }
        function btnSave_onclick(){
            var remarks = document.getElementById("txtRemarks").value;
            var status = document.getElementById("drpStatus").value;
            var checker = document.getElementById("<%= hidUser.ClientID %>").value;
            var eData = document.getElementById("<%= hidData.ClientID %>").value.split("Ñ");
            var entryID = eData[0];
            var data = entryID + "Ñ" + checker + "Ñ" + remarks + "Ñ" + status
            var saveData = "1Ø" + data;
            ToServer(saveData ,1);
        }
        function FromServer(arg, context) {           
           if(context == 1){
                var Data = arg.split("Ø");
                if(Data[0]==0){
                    alert(Data[1]);
                    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self"); 
                }
            }
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
    </script>
</head>
</html>
 <br />
 <div  style="width:90%;margin:0px auto; background-color:#EEB8A6;">
    <div style="width:80%;padding-left:10px;margin:0px auto;">
        <asp:HiddenField ID="hidData" runat="server" />
        <asp:HiddenField ID = "hidDenominations" runat="server" />
        <asp:HiddenField ID = "hidDenoData" runat="server" />
        <asp:HiddenField ID = "hidUser" runat="server" />
        <br /> 
    </div>
    <div style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table style="width:98%;height:90px;margin:0px auto;">            
            <tr id="List">
                <td style="text-align: center;" colspan="4">
                    <asp:Panel ID="pnlEntries" Style="width: 100%;  text-align: right; float: right;" runat="server">
                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />            
    </div>
    <div style="text-align:center; height: 63px;"><br />
        <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="SAVE" onclick="return btnSave_onclick()"/> 
        &nbsp;&nbsp;
        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()"/>
    </div>   
</div>
</asp:Content>

