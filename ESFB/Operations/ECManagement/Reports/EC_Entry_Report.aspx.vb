﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class EC_Entry_Report
    Inherits System.Web.UI.Page

    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim Meeting_Dt As Date
    Dim UserID As Integer
    Dim TraDt As String
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim DB As New MS_SQL.Connect
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1273) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim UserID As String = CStr(Session("UserID"))
            DT = DB.ExecuteDataSet("select b.branch_id,b.branch_name from emp_master a inner join branch_master b on a.branch_id = b.branch_id where a.emp_code = '" + UserID + "'").Tables(0)
            Me.hidBID.Value = DT.Rows(0)(0).ToString()
            Me.Master.subtitle = "Excess Cash Entry Approval Status Report"


            If Not IsPostBack Then
            End If


            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "onload()", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
        DB.dispose()
    End Sub

#End Region
#Region "Events"

#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        'Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'CallBackReturn = ""
        'If CInt(Data(0)) = 1 Then

        'End If
    End Sub
#End Region
End Class

