﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports System.Data.SqlClient

Partial Class View_EC_Consolidated_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTExcel As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim ViewType As Integer
            ViewType = CInt(Request.QueryString.Get("ViewType"))
            Dim EntryDate As Date
            EntryDate = CDate(Request.QueryString.Get("Date"))
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            'cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 9, 9, "l", "Date")
            RH.AddColumn(TRHead, TRHead_01, 9, 9, "l", "Branch Code")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "l", "Branch Name")
            RH.AddColumn(TRHead, TRHead_03, 7, 7, "l", "2000 (Count)")
            RH.AddColumn(TRHead, TRHead_04, 7, 7, "l", "500 (Count)")
            RH.AddColumn(TRHead, TRHead_05, 7, 7, "l", "200 (Count)")
            RH.AddColumn(TRHead, TRHead_06, 7, 7, "l", "100 (Count)")
            RH.AddColumn(TRHead, TRHead_07, 8, 8, "l", "Total")
            RH.AddColumn(TRHead, TRHead_08, 7, 7, "l", "BTI Requested 2000 (Count)")
            RH.AddColumn(TRHead, TRHead_09, 7, 7, "l", "BTI Requested 500 (Count)")
            RH.AddColumn(TRHead, TRHead_10, 7, 7, "l", "BTI Requested 200 (Count)")
            RH.AddColumn(TRHead, TRHead_11, 7, 7, "l", "BTI Requested 100 (Count)")
            RH.AddColumn(TRHead, TRHead_12, 8, 8, "l", "Total")
            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim Params(2) As SqlParameter
            Params(0) = New SqlParameter("@EntryDate", SqlDbType.DateTime)
            Params(0).Value = EntryDate
            Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(1).Direction = ParameterDirection.Output
            Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(2).Direction = ParameterDirection.Output
            DT = DB.ExecuteDataSet("SP_EC_Entry_REPORT", Params).Tables(0)
            ErrorFlag = CInt(Params(1).Value)
            Message = CStr(Params(2).Value)


            Dim SqlStr As String = " "
            Dim ExSqlStr As String = " "

            SqlStr = "select entrydate,branchid,branchname,[2000],[500],[200],[100],totalamount from ec_entry_master_temp where (cast(entrydate as date)) = (cast('" + EntryDate + "' as date))"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)

            ExSqlStr = "select entrydate as [Date],branchid as [Branch Code],branchname as [Branch Name],[2000] as [2000 (Count)], [500] as [500 (Count)], [200] as [200 (Count)],[100] as [100 (Count)],totalamount as [Total Amount],'' as [BTI Requested 2000 (Count)],'' as [BTI Requested 500 (Count)],'' as [BTI Requested 200 (Count)],'' as [BTI Requested 100 (Count)],'' as [Total Amount (BTI)] from ec_entry_master_temp where (cast(entrydate as date)) = (cast('" + EntryDate + "' as date))"
            DTExcel = DB.ExecuteDataSet(ExSqlStr).Tables(0)

            If ViewType = 1 Then
                Export_Excel_Click()
            End If

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"



                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver



                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 9, 9, "l", CDate(DR(0)).ToString("dd/MM/yyyy"))
                RH.AddColumn(TR3, TR3_01, 9, 9, "c", DR(1).ToString())
                RH.AddColumn(TR3, TR3_02, 10, 10, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_03, 7, 7, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_04, 7, 7, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_05, 7, 7, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_06, 7, 7, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_07, 8, 8, "l", CInt(DR(7).ToString()))
                RH.AddColumn(TR3, TR3_08, 7, 7, "l", "")
                RH.AddColumn(TR3, TR3_09, 7, 7, "l", "")
                RH.AddColumn(TR3, TR3_10, 7, 7, "l", "")
                RH.AddColumn(TR3, TR3_11, 7, 7, "l", "")
                RH.AddColumn(TR3, TR3_12, 8, 8, "l", "")
                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    'Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    'End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Consolidated Branchwise Report"
            WebTools.ExporttoExcel(DTExcel, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Protected Sub Export_Excel_Click()
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Consolidated_Branchwise_Report"
            WebTools.ExporttoExcel(DTExcel, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
