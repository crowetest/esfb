﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="EC_Consolidated_Report.aspx.vb" Inherits="EC_Consolidated_Report"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css" />
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
    <tr>
        <td style="width:15%;">
            From </td>
        <td  style="width:85%;">
                 <asp:TextBox ID="txtFromDate" class="NormalText" runat="server" 
                    Width="50%" onkeypress="return false" ></asp:TextBox>
             <asp:CalendarExtender ID="txtFromDate_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtFromDate" Format="dd MMM yyyy">
             </asp:CalendarExtender>
        </td>
    </tr> 
    <tr>
        <td style="width:15%;">
            To </td>
        <td  style="width:85%;">
                 <asp:TextBox ID="txtToDate" class="NormalText" runat="server" 
                    Width="50%" onkeypress="return false" ></asp:TextBox>
             <asp:CalendarExtender ID="txtToDate_CalendarExtende" runat="server" 
                 Enabled="True" TargetControlID="txtToDate" Format="dd MMM yyyy">
             </asp:CalendarExtender>
        </td>
    </tr>    
    <tr>
        <td style="width:30%;">
        <%-- Discussion Status--%>
        </td>
        <td  style="width:85%;">
             
        </td>           
    </tr>
    <tr>
        <td style="width:15%;">
            &nbsp;</td>
        <td  style="width:85%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width:15%;">
            &nbsp;</td>
        <td  style="width:85%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width:15%;">
            &nbsp;</td>
        <td  style="width:85%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width:100%; height: 18px; text-align:center;" colspan="2">             
            <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 67px;" 
            type="button" value="VIEW"   onclick="return btnView_onclick()" />
            &nbsp;&nbsp;
            <input id="cmd_Export_Excel" style="font-family: Cambria; cursor: pointer; width: 87px;" 
            type="button" value="VIEW EXCEL" onclick="return btnExcelView_onclick()" />
            &nbsp;&nbsp;
            <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 10%" 
            type="button" value="EXIT"  onclick="return btnExit_onclick()" />
        </td>            
    </tr>
    <tr>
        <td style="width:15%;">
            &nbsp;</td>
        <td  style="width:85%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width:15%;">
            &nbsp;</td>
        <td  style="width:85%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width:15%;">
            &nbsp;</td>
        <td  style="width:85%;">
            <asp:HiddenField ID="hdnReportID" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="width:15%;">
            &nbsp;
        </td>
        <td  style="width:85%;">
            &nbsp;
        </td>
        <asp:HiddenField ID="hdnValue" runat="server" />
    </tr>
    </table>
    <script language="javascript" type="text/javascript">
     
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }            
    //ViewType = -1 for normal report view
    //ViewType = 1 for excel report view
    function btnView_onclick() 
        {
            var FromDate = document.getElementById("<%= txtFromDate.ClientID %>").value;  
            var ToDate = document.getElementById("<%= txtToDate.ClientID %>").value; 
            window.open("View_EC_Consolidated_Report_Sep.aspx?ViewType=-1 &FromDate=" + FromDate +" &ToDate="+ToDate+" ", "");                   
       }    
   function btnExcelView_onclick() 
        {
            var FromDate = document.getElementById("<%= txtFromDate.ClientID %>").value;  
            var ToDate = document.getElementById("<%= txtToDate.ClientID %>").value; 
            window.open("View_EC_Consolidated_Report_Sep.aspx?ViewType=1 &FromDate=" + FromDate +" &ToDate="+ToDate+" ", "_self");      
        }      
   function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
       
     function FromServer(arg, context) {

         switch (context) {
            case 1:
                {                   
                           
                }
             
               
            case 2:
               {    
                         
                    
                    
                }                            
              
             }
            }

    </script>
</asp:Content>

 