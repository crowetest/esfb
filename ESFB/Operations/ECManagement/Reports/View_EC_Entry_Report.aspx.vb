﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports System.Data.SqlClient
Partial Class View_EC_Entry_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DS As New DataTable
    Dim DTExcel As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim From_Dt As String = CStr(Request.QueryString.Get("From"))
            Dim BranchCode As String = CStr(Request.QueryString.Get("BranchCode"))
            Dim DenoCount As Integer = 0
            Dim j As Integer = 8
            Dim k As Integer = 0
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim Params(3) As SqlParameter
            Params(0) = New SqlParameter("@EntryDate", SqlDbType.VarChar, 50)
            Params(0).Value = From_Dt
            Params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            Params(1).Value = BranchCode
            Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(2).Direction = ParameterDirection.Output
            Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(3).Direction = ParameterDirection.Output
            DT = DB.ExecuteDataSet("[SP_EC_ENTRY__TRACKER_REPORT]", Params).Tables(0)
            ErrorFlag = CInt(Params(2).Value)
            Message = CStr(Params(3).Value)
            RH.Heading(Session("FirmName"), tb, "ESAF", 100)
            tb.Attributes.Add("width", "100%")
            RH.BlankRow(tb, 3)

            DS = DB.ExecuteDataSet("SELECT Denomination_ID,Value FROM EC_DENOMINATION_MASTER WHERE Status = 1").Tables(0)
            DenoCount = DS.Rows.Count
            Dim denowidth As Integer = 20 / DenoCount

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11 As New TableCell
            'Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_0j, TRHead_08, TRHead_09, TRHead_10, TRHead_11 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            'k = 0
            'j = 4
            'For k = 0 To k < DenoCount
            '    TRHead_0j.BorderWidth = "1"
            '    j += 1
            '    k += 1
            'Next
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            'k = 0
            'j = 4
            'For k = 0 To k < DenoCount
            '    TRHead_0j.BorderColor = Drawing.Color.Silver
            '    j += 1
            '    k += 1
            'Next

            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            'k = 0
            'j = 4
            'For k = 0 To k < DenoCount
            '    TRHead_0j.BorderStyle = BorderStyle.Solid
            '    j += 1
            'Next
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid



            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No")
            RH.AddColumn(TRHead, TRHead_01, 15, 15, "c", "EntryDate")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "c", "Branch Code")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "c", "Branch Name")
            ''k = 0
            'j = 4
            'For Each ds1 In DS.Rows
            '    RH.AddColumn(TRHead, TRHead_0j, denowidth, denowidth, "c", ds1(1).ToString() + " (Count)")
            '    'k += 1
            '    j += 1
            'Next
            RH.AddColumn(TRHead, TRHead_04, 5, 5, "c", "2000 (Count)")
            RH.AddColumn(TRHead, TRHead_05, 5, 5, "c", "500 (Count)")
            RH.AddColumn(TRHead, TRHead_06, 5, 5, "c", "200 (Count)")
            RH.AddColumn(TRHead, TRHead_07, 5, 5, "c", "100 (Count)")
            RH.AddColumn(TRHead, TRHead_08, 10, 10, "c", "Total Amount")
            RH.AddColumn(TRHead, TRHead_09, 10, 10, "c", "Words")
            RH.AddColumn(TRHead, TRHead_10, 10, 10, "c", "Status")
            RH.AddColumn(TRHead, TRHead_11, 10, 10, "c", "Remarks")
            tb.Controls.Add(TRHead)



            RH.BlankRow(tb, 3)
            Dim i As Integer

            'DT = DB.ExecuteDataSet("select EntryDate,Branchid,BranchName,[2000],[500],[200],[100],[TotalAmount],[words],[ApprovedStatus],[Checker_Remarks] from EC_Entry_Master_Tracker_Temp  where entrydate=' " + From_Dt + "' and branchId=" & BranchCode & " ").Tables(0)


            For Each DR In DT.Rows
                i += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11 As New TableCell
                'Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_0j, TR3_08, TR3_09, TR3_10, TR3_11 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                'k = 0
                'j = 4
                'For k = 0 To k < DenoCount
                '    TR3_0j.BorderWidth = "1"
                '    j += 1
                '    k += 1
                'Next
                
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                'k = 0
                'j = 4
                'For k = 0 To k < DenoCount
                '    TR3_0j.BorderColor = Drawing.Color.Silver
                '    j += 1
                '    k += 1
                'Next

                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                'k = 0
                'j = 4
                'For k = 0 To k < DenoCount
                '    TR3_0j.BorderStyle = BorderStyle.Solid
                '    j += 1
                '    k += 1
                'Next
                
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", i.ToString())
                RH.AddColumn(TR3, TR3_01, 15, 15, "c", CDate(DR(0)).ToString("dd/MM/yyyy"))
                RH.AddColumn(TR3, TR3_02, 10, 10, "c", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 10, 10, "c", DR(2).ToString())
                'k = 0
                'j = 8
                'For Each ds1 In DS.Rows
                '    RH.AddColumn(TR3, TR3_0j, denowidth, denowidth, "c", DR(j).ToString())
                '    'k += 1
                '    j += 1
                'Next
                RH.AddColumn(TR3, TR3_04, 5, 5, "c", DR(8).ToString())
                RH.AddColumn(TR3, TR3_05, 5, 5, "c", DR(9).ToString())
                RH.AddColumn(TR3, TR3_06, 5, 5, "c", DR(10).ToString())
                RH.AddColumn(TR3, TR3_07, 5, 5, "c", DR(11).ToString())
                RH.AddColumn(TR3, TR3_08, 10, 10, "c", DR(3).ToString())
                RH.AddColumn(TR3, TR3_09, 10, 10, "c", DR(4).ToString())
                RH.AddColumn(TR3, TR3_10, 10, 10, "c", DR(6).ToString())
                RH.AddColumn(TR3, TR3_11, 10, 10, "c", DR(5).ToString())
                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

End Class
