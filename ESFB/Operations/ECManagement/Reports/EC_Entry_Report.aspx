﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="EC_Entry_Report.aspx.vb" Inherits="EC_Entry_Report"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <link href="../../Style/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css" />
    
    <body>
    <script language="javascript" type="text/javascript">
    window.onload = function (){
        document.getElementById("<%= Branch_code.ClientID %>").value = document.getElementById("<%= hidBID.ClientID %>").value;
     }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }    
      
         
    function btnView_onclick() 
        {
           var BranchCode=document.getElementById("<%= Branch_code.ClientID %>").value;
            var From_Dt = document.getElementById("<%= txt_From_Dt.ClientID %>").value;
           window.open("View_EC_Entry_Report.aspx?BranchCode=" + BranchCode + "&From= " + From_Dt +" ", "_self"); 
                 
        }    
   function btnExcelView_onclick() 
        {
            
        }      
  
             
   function FromServer(arg, context) {
         switch (context) {
            case 1:
                {                   
                      
                            break;
                }           
            case 2:
               {                           
               
               }                            
            }
        }
    </script>

<br />
    <table align="center" style="width: 30%; text-align:center; margin:0px auto;">
       <tr>
            <td style="width:30%;">
                Branch Code </td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:TextBox ID="Branch_code" class="NormalText" runat="server" 
                    Width="75%" ></asp:TextBox>
               
            </td>           
        </tr>
        <tr>
            <td style="width:30%;">
                Date </td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:TextBox ID="txt_From_Dt" class="NormalText" runat="server" 
                    Width="75%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txt_From_Dt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txt_From_Dt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>           
        </tr>
       
        
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">             
                <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;
                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
            </td>        
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
               
                 <asp:HiddenField ID="hdnValue" runat="server" />
                 <asp:HiddenField ID="hdnAdmin" runat="server" />
                 <asp:HiddenField ID="hdnUser" runat="server" />
                 <asp:HiddenField ID="hidBID" runat="server" />
        </tr>
    </table>
    </body>
    </html>
</asp:Content>

 