﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ExcessCashApprove
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1269) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Excess Cash Entry Approval"
            Dim UserID As Integer = CInt(Session("UserID"))
            Me.hidUser.Value = CStr(UserID)
            DT1 = DB.ExecuteDataSet("select a.* from EC_Entry_Master a inner join emp_master b on b.emp_code = " + UserID.ToString() + " where a.branchid = b.branch_id and a.ApprovedStatus = 0").Tables(0)
            Dim EntryData As String = ""
            Dim i As Integer = 0
            For Each dtr As DataRow In DT1.Rows
                For k As Integer = 0 To DT1.Columns.Count - 1
                    EntryData += DT1.Rows(i)(k).ToString() + "Ñ"
                Next
                i += 1
            Next

            DT3 = DB.ExecuteDataSet("SELECT Value FROM EC_Denomination_Master WHERE Status = 1 ORDER BY Value DESC").Tables(0)
            Dim Deno_Master As String = ""
            Dim l As Integer = 0
            For Each dtt In DT3.Rows
                Deno_Master += DT3.Rows(l)(0).ToString() + "Ñ"
                l += 1
            Next
            Me.hidDenominations.Value = Deno_Master


            DT2 = DB.ExecuteDataSet("select a.EntryID,a.value,a.count,a.total from ec_denomination_entry a inner join EC_Entry_Master b on a.entryid = b.entryid inner join emp_master c on c.emp_code = " + UserID.ToString() + " where c.branch_id = b.branchid and b.ApprovedStatus = 0").Tables(0)
            Dim Denominations As String = ""
            Dim j As Integer = 0
            For Each dtt In DT2.Rows
                Denominations += DT2.Rows(j)(0).ToString() + "ÿ" + DT2.Rows(j)(1).ToString() + "ÿ" + DT2.Rows(j)(2).ToString() + "Ñ"
                j += 1
            Next
            Me.hidData.Value = EntryData.ToString()
            Me.hidDenoData.Value = Denominations.ToString
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Try
            Dim Data() As String = eventArgument.Split(CChar("Ø"))
            If CInt(Data(0)) = 1 Then
                Dim saveData = Data(1).Split(CChar("Ñ"))
                Dim entryId As Integer = CInt(saveData(0).ToString())
                Dim CheckerId As Integer = CInt(saveData(1).ToString())
                Dim Remarks As String = saveData(2)
                Dim Status As Integer = CInt(saveData(3).ToString())
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@EntryId", SqlDbType.Int)
                Params(0).Value = entryId
                Params(1) = New SqlParameter("@CheckerID", SqlDbType.Int)
                Params(1).Value = CheckerId
                Params(2) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                Params(2).Value = Remarks
                Params(3) = New SqlParameter("@Status", SqlDbType.Int)
                Params(3).Value = Status
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_EC_Entry_Approve", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
                CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
            End If
        Catch ex As Exception
          
            Response.Redirect("~/CatchException.aspx?ErrorNo=1")
        End Try
    End Sub
    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click

    'End Sub
    'Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click

    'End Sub
End Class
