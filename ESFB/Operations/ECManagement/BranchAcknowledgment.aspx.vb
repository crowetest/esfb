﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BranchAcknowledgment
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1271) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim UserID As String = CStr(Session("UserID"))
            DT = DB.ExecuteDataSet("select b.branch_id,b.branch_name from emp_master a inner join branch_master b on a.branch_id = b.branch_id where a.emp_code = '" + UserID + "'").Tables(0)
            Me.hidBID.Value = DT.Rows(0)(0).ToString()
            Me.hidBName.Value = DT.Rows(0)(1).ToString()
            Me.Master.subtitle = "Branch Acknowledgment"

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim BranchID As Integer = CInt(Data(1))
            Dim BranchName As String = ""
            Dim EntryData As String = ""
            DT = DB.ExecuteDataSet("SELECT Branch_Name FROM BRANCH_MASTER WHERE Branch_ID = " + BranchID.ToString()).Tables(0)
            If DT.Rows.Count > 0 Then
                BranchName = DT.Rows(0)(0).ToString()
            End If
                CallBackReturn = BranchName
        End If
        If CInt(Data(0)) = 2 Then
            Try
                Dim EntryDate As DateTime = Convert.ToDateTime(Data(1))
                Dim BranchID As Integer = CInt(Data(2))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@EntryDate", SqlDbType.DateTime)
                Params(0).Value = EntryDate
                Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(1).Value = BranchID
                Params(2) = New SqlParameter("@User", SqlDbType.VarChar, 500)
                Params(2).Value = CStr(Session("UserID"))
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_EC_BranchAcknowledgment", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
                CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
            Catch ex As Exception
               
                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try
        End If
    End Sub
End Class
