﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="EC_Report_BTI.aspx.vb" Inherits="EC_Report_BTI" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script src="../../Script/jquery-1.10.2.js" type="text/javascript"></script>
    <style type="text/css">    
          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;
          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);
           
           
        }
        
        .Button:hover
        {
            
           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            
            color:#801424;
        }   
                 
     
     .bg
     {
         background-color:#FFF;
     }
           
    </style>
    
    <script language="javascript" type="text/javascript">

        window.onload = function(){
            $('.Dld').show();
            $('.Uld').hide();
        }
        function IntialCtrl(){
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1) {
                DownloadClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2) {
                UploadClick();
            }              
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function DownloadClick() {
            document.getElementById("1").style.background = '-moz-radial-gradient(center, ellipse cover,  #EEB8A6 0%, #801424 90%, #EEB8A6 100%)';
            document.getElementById("1").style.color = '#CF0D0D';
            document.getElementById("2").style.background = '-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("2").style.color = '#CF0D0D';
            $('.Dld').show();
            $('.Uld').hide();
        }
        function UploadClick() {
            document.getElementById("1").style.background =  '-moz-radial-gradient(center, ellipse cover,#F1F3F4 0%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("1").style.color = '#CF0D0D';
            document.getElementById("2").style.background =  '-moz-radial-gradient(center, ellipse cover,  #EEB8A6 20%, #801424 90%, #EEB8A6 100%)';
            document.getElementById("2").style.color = '#CF0D0D';
            $('.Uld').show();
            $('.Dld').hide();
        }
        function FromServer(arg, context) {           
           if(context==1){
               alert(arg);
           }
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
        function btnDownload_onclick(){
            var entrydate = document.getElementById("<%= txtDate.ClientID %>").value;
            var type = document.getElementById("<%= drpFormat.ClientID %>").value;            
            if(type == 1){ //excel
                var Data = "1Ø" + entrydate;
                ToServer(Data ,1);
//                window.open("Reports/View_EC_Consolidated_Report.aspx?ViewType=1 &Date=" + entrydate +" ", "");
            }
            else if(type == 2){ //pdf
                window.open("Reports/View_EC_Consolidated_Report.aspx?ViewType=-1 &Date=" + entrydate +" ", "");
            }
            else{
                alert("Select type");
                return false;
            }            
        }
        function btnUpload_onclick(){
            var entrydate = document.getElementById("<%= txtDate.ClientID %>").value;
            if(entrydate ==""){
                alert("Select date");
                return false;
            }
            document.getElementById("<%= hdnValue.ClientID %>").value = entrydate.toString();
        }

    </script>
</head>
</html>
 <br />
 
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
 <div  style="width:90%;margin:0px auto; background-color:#EEB8A6;">
    <div style="width:30%;margin:0px auto;" align="center">
        <div id='1' class="Button" style="width:49%; float:left;background:-moz-radial-gradient(center, ellipse cover, #EEB8A6 0%, #801424 90%, #EEB8A6 100%);" onclick="return DownloadClick()">
            Download
        </div>
        <div id='2' class="Button" style="border-left:thick double #FFF;width:49%;float:left;" onclick="return UploadClick()">
            Upload
        </div>
        <br /> <br />
    </div>
    <div style="width:93%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
            <br /><br />  
            <table style="width:90%;height:90px;margin:0px auto;">  
                <tr> 
                    <td style="width:30%;">                      
                        <asp:HiddenField ID="hid_tab" runat="server" />
                        <asp:HiddenField ID="hid_Branch" runat="server" />
                        <asp:HiddenField ID="hdnValue" runat="server" />
                    </td>
                    <td style="width:20%; text-align:left;">
                    </td>
                    <td style="width:25%">
                    </td>
                    <td style="width:25%">
                    </td>
                </tr>    
                <tr>    
                    <td style="width:30%;">
                    </td>
                    <td style="width:20%;">
                        Date  
                    </td>
                    <td style="width:25%; text-align:left;">
                        <asp:TextBox ID="txtDate" class="NormalText" runat="server" Width="85%" onkeypress="return false" >
                        </asp:TextBox>
                        <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtDate" Format="dd MMM yyyy">
                        </asp:CalendarExtender>
                    </td>
                </tr>
                <tr class="Dld">    
                    <td style="width:30%;">
                    </td>
                    <td style="width:20%;">
                        Select Type  
                    </td>
                    <td style="width:25%; text-align:left;">
                        <asp:DropDownList ID="drpFormat" class="NormalText" runat="server" Font-Names="Cambria" Width="86%" ForeColor="Black">
                            <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                            <asp:ListItem Value="1">EXCEL</asp:ListItem>
                            <asp:ListItem Value="2">PDF</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="Dld">   
                    <td style="width:30%;">
                    </td>
                    <td style="width:20%;">                          
                    </td>
                    <td style="width:25%; text-align:left;"><br />
                        <input id="btnDownload" style="font-family: cambria; cursor: pointer; width:60%;" type="button" value="DOWNLOAD REPORT" onclick="return btnDownload_onclick()"/>
                        <%--<asp:Button ID="btnDownload" Text="DOWNLOAD REPORT" runat="server" />--%>
                    </td>
                </tr>
                <tr class="Uld">   
                    <td style="width:30%;">
                    </td>
                    <td style="width:20%;">  
                        Select File                        
                    </td>
                    <td style="width:25%; text-align:left;"><br />
                        &nbsp; &nbsp;
                        <div id="fileUploadarea">
                            <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload"/><br />
                        </div>
                        <br />
                    </td>
                </tr>
                <tr class="Uld">   
                    <td style="width:30%;">
                    </td>
                    <td style="width:20%;">                          
                    </td>
                    <td style="width:25%; text-align:left;"><br />
                        <asp:Button id="btnSave" runat="server" style="font-family: cambria; cursor: pointer; width:60%;" type="button" value="UPLOAD REPORT" text = "UPLOAD REPORT"/>
                    </td>
                </tr>
            </table>             
        </div>
    <div style="text-align:center; height: 63px;"><br />
        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()"/>
    </div>  
</div>
     
</asp:Content>

