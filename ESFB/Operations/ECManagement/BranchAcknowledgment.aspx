﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="BranchAcknowledgment.aspx.vb" Inherits="BranchAcknowledgment" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);          
        }        
        .Button:hover
        {           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);            
            color:#801424;
        }                   
     .bg
     {
         background-color:#FFF;
     }
     .style1
        {
            width: 40%;
            height: 104px;
        }           
    </style>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script language="javascript" type="text/javascript">       
        window.onload = function () {
            document.getElementById("<%= txtBranchID.ClientID %>").value = document.getElementById("<%= hidBID.ClientID %>").value;
            document.getElementById("<%= txtBranchName.ClientID %>").value = document.getElementById("<%= hidBName.ClientID %>").value;
        }
//        function BranchCodeOnChange(){
//            var branch_Id = document.getElementById("<%= txtBranchId.ClientID %>").value;
//            var ToData = "1Ø" + branch_Id;
//            ToServer(ToData,1);
//        }
        function Saveonclick(){
            var entryDate = document.getElementById("<%= txtDate.ClientID %>").value;
            var branchCode = document.getElementById("<%= txtBranchId.ClientID %>").value;
            if(entryDate == "")
            {
                alert("Please select entry date");
                return false;
            }
            if(branchCode == "")
            {
                alert("Please enter branch code");
                return false;
            }
            if(document.getElementById("ackRadio").checked == false){
                alert("Please confirm that the cash is handed over to BTI officials");
                return false;
            }
            var ToData = "2Ø" + entryDate+ "Ø" + branchCode + "Ø1";
            ToServer(ToData,2);
        }
        function btnDownload_onclick(){
            var entryDate = document.getElementById("<%= txtDate.ClientID %>").value;
            var branchCode = document.getElementById("<%= txtBranchId.ClientID %>").value;
            window.open("View_EC_Uploaded_Report.aspx?Date=" + entryDate +"&branchID=" + branchCode + " ", ""); 
        }
        function FromServer(arg, context) {
            if (context == 1) {
                document.getElementById("<%= txtBranchName.ClientID %>").value = arg.toString();
            }
            if(context == 2){
                var data = arg.split("Ø");
                alert(data[1]);
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self"); 
            }
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
        function ApprovalOnChange()
        {   
        }
        function white_space(field)
        {
            if(field.value.length==1){
                field.value = (field.value).replace(' ','');
            }
        }
    </script>
</head>
</html>
<br />
<table class="style1" style="width:80%;margin: 0px auto;">
    <tr> 
        <td style="width:30%;">
            <asp:HiddenField ID="hidData" runat="server" />
            <asp:HiddenField ID="hidDenominations" runat="server" />
            <asp:HiddenField ID="hidUpdateData" runat="server" />
            <asp:HiddenField ID="hidUpdateDenoData" runat="server" />
            <asp:HiddenField ID="hidBID" runat="server" />
            <asp:HiddenField ID="hidBName" runat="server" />
        </td>
        <td style="width:20%; text-align:left;">
        </td>
        <td style="width:25%">
        </td>
        <td style="width:25%">
        </td>
    </tr>
    <tr>    
        <td style="width:30%;">
        </td>
        <td style="width:20%;">
            Excess Cash Entry Date 
        </td>
        <td style="width:25%; text-align:left;">
            <asp:TextBox ID="txtDate" class="NormalText" ReadOnly="true" runat="server" Width="87%" onkeypress = "return false"></asp:TextBox>
            <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtDate" Format="dd MMM yyyy">
            </asp:CalendarExtender>
        </td>
    </tr>   
    <tr>    
        <td style="width:30%;">
        </td>
        <td style="width:20%;">
            Branch Code  
        </td>
        <td style="width:25%; text-align:left;">
            <asp:TextBox ID="txtBranchId" class="NormalText" runat="server" Width="87%" onkeypress='return DecimalCheck(event,this)' onchange = "BranchCodeOnChange()" MaxLength="10"></asp:TextBox>
        </td>
    </tr>   
    <tr>        
        <td style="width:30%;">
        </td>
        <td style="width:20%;">
            Branch Name  
        </td>
        <td style="width:25%; text-align:left;">
            <asp:TextBox ID="txtBranchName" class="NormalText" ReadOnly="true" runat="server" Width="87%" ></asp:TextBox>
        </td>
        <td style="width:25%; text-align:left;">
            <input id="btnDownload" style="font-family: cambria; cursor: pointer; width:60%;" type="button" value="VIEW REPORT" onclick="return btnDownload_onclick()"/>
        </td>
    </tr>
    <tr>        
        <td style="width:30%;">            
        </td>
        <td style="width:20%;">
            
        </td>
        <td style="width:25%; text-align:left;">
            
        </td>
    </tr>
    <tr>        
        <td style="width:30%;">            
        </td>
        <td style="width:20%;">
             <input id="ackRadio" name="ackRadio" type="radio" />&nbsp;&nbsp;&nbsp;&nbsp;
             Cash handed over to BTI officials
        </td>
        <td style="width:25%; text-align:left;">
            
        </td>
    </tr>
</table>

<div  style="width:90%; height:auto; margin:0px auto; background-color:#EEB8A6;">
    <div style="width:80%;height:auto; padding-left:10px;margin:0px auto;">
        <div style="text-align:center; height: 63px;"><br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="SAVE" onclick="return Saveonclick()"/>       
            &nbsp;
            &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" />
        </div>
    </div>
</div>   
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

