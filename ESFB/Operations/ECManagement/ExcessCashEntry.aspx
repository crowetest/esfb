﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ExcessCashEntry.aspx.vb" Inherits="ExcessCashEntry" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);          
        }        
        .Button:hover
        {           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);            
            color:#801424;
        }                   
     .bg
     {
         background-color:#FFF;
     }
     .style1
        {
            width: 40%;
            height: 104px;
        }           
    </style>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script language="javascript" type="text/javascript">       
        window.onload = function () {
            var today = new Date();
            document.getElementById("<%= txtDate.ClientID %>").value = today.format('MM/dd/yyyy hh:mm');
            document.getElementById("<%= txtBranchID.ClientID %>").value = document.getElementById("<%= hidBID.ClientID %>").value;
            document.getElementById("<%= txtBranchName.ClientID %>").value = document.getElementById("<%= hidBName.ClientID %>").value;
            TableFill();
        }
        var denoLength;
        function ChangeDenoCount(i){
            var val = document.getElementById("deno_"+i).value;
            var count = document.getElementById("dCount_"+i).value;
            var dTotal = val * count;
            var dSum =0;
            document.getElementById("dCountTot_"+i).value = dTotal;
            for(var j=0;j<=denoLength;j++){
                try{
                    dSum += Number(document.getElementById("dCountTot_"+j).value);
                    }catch(e){}
            }
            document.getElementById("TotAmount").value = dSum;
            price_in_words(dSum);
        }
        function TableFill(){
            document.getElementById("<%= pnlDenomination.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:80%; height:auto;margin: 0px 0px;overflow:auto;text-align:center;' align='center'>";
            tab += "<table style='width:100%;font-family:cambria;margin: 0px 0px;' id='DenoTable'>";
            var data = document.getElementById("<%= hidDenominations.ClientID %>").value;
            var deno = data.split("Ñ");
            denoLength = deno.length;
            for( var i = 0; i < deno.length-1;i++){
                tab += "<tr style ='height:20px;'>";
                tab += "<td style='width:25%;text-align:center;'><input type='text' class='NormalText' id='deno_"+i+"' value='"+deno[i]+"' style='width:100%;text-align:center;' maxlength='50' readonly='true' /></td>"
                tab += "<td style='width:10%;text-align:center;'><input type='text' class='NormalText' value='X' style='width:100%;text-align:center;' maxlength='50' readonly='true' /></td>";
                tab += "<td style='width:25%;text-align:center;'><input type='text' class='NormalText' style='width:100%;text-align:center;' id='dCount_"+i+"' onkeypress='return DecimalCheck(event,this)' onchange='return ChangeDenoCount("+i+")' maxlength='5'></td>"
                tab += "<td style='width:25%;text-align:center;'><input type='text' class='NormalText' id='dCountTot_"+i+"' style='width:100%;' maxlength='50' readonly='true' /></td>"
                tab += "</tr>";
            }      
            tab += "<tr style ='height:20px;'>";
            tab += "<td style='width:25%;text-align:center'></td>"
            tab += "<td style='width:10%;text-align:center;' ></td>";
            tab += "<td style='width:25%;text-align:center;' ><input type='text' class='NormalText' style='width:100%;text-align:center;' value='Total' readonly='true'></td>"
            tab += "<td style='width:25%;text-align:center'><input type='text' class='NormalText' id='TotAmount' style='width:100%;' maxlength='50' readonly = 'true'/></td>"
            tab += "</tr>";    
            tab += "</table></div>";
            document.getElementById("<%= pnlDenomination.ClientID %>").innerHTML = tab;
        }
//        function BranchCodeOnChange(){
//            var branch_Id = document.getElementById("<%= txtBranchId.ClientID %>").value;
//            var ToData = "1Ø" + branch_Id;
//            ToServer(ToData,1);
//        }
        function Saveonclick(){
            if(document.getElementById("<%= txtBranchId.ClientID %>").value == ""){
                alert("Enter Branch Code");
                return false;
            }
            var EntryDate = document.getElementById("<%= txtDate.ClientID %>").value;
            var BranchID = document.getElementById("<%= txtBranchId.ClientID %>").value;
            var BranchName = document.getElementById("<%= txtBranchName.ClientID %>").value;
            var Denomination = "";
            for(var i = 0;i<denoLength;i++){
                try{
                    Denomination += document.getElementById("deno_"+i).value + "ÿ" + document.getElementById("dCount_"+i).value + "ÿ" + document.getElementById("dCountTot_"+i).value + "Ñ";
                }catch(e){}
            }
            var TotalAmount = document.getElementById("TotAmount").value;
            var Words = document.getElementById("<%= txtWords.ClientID %>").value;
            var ToData = "2Ø" + EntryDate+ "Ø" + BranchID + "Ø" + BranchName + "Ø" + Denomination + "Ø" + TotalAmount + "Ø" + Words;
            ToServer(ToData,2);
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                document.getElementById("<%= txtBranchName.ClientID %>").value = Data[0];
                try{
                    var gData = Data[1].split("Ñ");
                    document.getElementById("<%= txtWords.ClientID %>").value = gData[1];
                    document.getElementById("TotAmount").value = Math.abs(gData[0]).toFixed(2);
                    var denoData = Data[2].split("Ñ");
                    for(var i = 0;i<denoData.length-1;i++){
                        var dData = denoData[i].split("ÿ");
                        if(document.getElementById("deno_"+i).value==dData[0]){
                            document.getElementById("dCount_"+i).value=dData[1];
                            document.getElementById("dCountTot_"+i).value=dData[2];
                        }
                    }
                }catch(e){}
            }
            if(context == 2){
                var Data = arg.split("Ø");
                if(Data[0]==0){
                    alert(Data[1]);
                    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self"); 
                }
            }
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
        function ApprovalOnChange()
        {   
        }
        function white_space(field)
        {
            if(field.value.length==1){
                field.value = (field.value).replace(' ','');
            }
        }
        function DecimalCheck(e,el)
        {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                    if (unicode == 37 || unicode == 38) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (unicode == 46) {
                        var parts = el.value.split('.');
                        if (parts.length == 2)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            else {
                return true;
            }
        }
        function price_in_words(price) {
            var sglDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"],
                dblDigit = ["Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"],
                tensPlace = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"],
                handle_tens = function(dgt, prevDgt) {
                    return 0 == dgt ? "" : " " + (1 == dgt ? dblDigit[prevDgt] : tensPlace[dgt])
                },
                handle_utlc = function(dgt, nxtDgt, denom) {
                    return (0 != dgt && 1 != nxtDgt ? " " + sglDigit[dgt] : "") + (0 != nxtDgt || dgt > 0 ? " " + denom : "")
                };

                var str = "",
                    digitIdx = 0,
                    digit = 0,
                    nxtDigit = 0,
                    words = [];
                if (price += "", isNaN(parseInt(price))) str = "";
                else if (parseInt(price) > 0 && price.length <= 10) {
                    for (digitIdx = price.length - 1; digitIdx >= 0; digitIdx--) switch (digit = price[digitIdx] - 0, nxtDigit = digitIdx > 0 ? price[digitIdx - 1] - 0 : 0, price.length - digitIdx - 1) {
                        case 0:
                            words.push(handle_utlc(digit, nxtDigit, ""));
                            break;
                        case 1:
                            words.push(handle_tens(digit, price[digitIdx + 1]));
                            break;
                        case 2:
                            words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != price[digitIdx + 1] && 0 != price[digitIdx + 2] ? " and" : "") : "");
                            break;
                        case 3:
                            words.push(handle_utlc(digit, nxtDigit, "Thousand"));
                            break;
                        case 4:
                            words.push(handle_tens(digit, price[digitIdx + 1]));
                            break;
                        case 5:
                            words.push(handle_utlc(digit, nxtDigit, "Lakh"));
                            break;
                        case 6:
                            words.push(handle_tens(digit, price[digitIdx + 1]));
                            break;
                        case 7:
                            words.push(handle_utlc(digit, nxtDigit, "Crore"));
                            break;
                        case 8:
                            words.push(handle_tens(digit, price[digitIdx + 1]));
                            break;
                        case 9:
                            words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != price[digitIdx + 1] || 0 != price[digitIdx + 2] ? " and" : " Crore") : "")
                    }
                    str = words.reverse().join("")
                    str += " Only"
                } else str = "";
                document.getElementById("<%= txtWords.ClientID %>").value = str;
    }
    </script>
</head>
</html>
<br />
<table class="style1" style="width:80%;margin: 0px auto;">
    <tr> 
        <td style="width:30%;">
            <asp:HiddenField ID="hidData" runat="server" />
            <asp:HiddenField ID="hidDenominations" runat="server" />
            <asp:HiddenField ID="hidUpdateData" runat="server" />
            <asp:HiddenField ID="hidUpdateDenoData" runat="server" />
            <asp:HiddenField ID="hidBID" runat="server" />
            <asp:HiddenField ID="hidBName" runat="server" />
        </td>
        <td style="width:20%; text-align:left;">
        </td>
        <td style="width:25%">
        </td>
        <td style="width:25%">
        </td>
    </tr>
    <tr>    
        <td style="width:30%;">
        </td>
        <td style="width:20%;">
            Date 
        </td>
        <td style="width:25%; text-align:left;">
            <asp:TextBox ID="txtDate" class="NormalText" ReadOnly="true" runat="server" Width="87%"></asp:TextBox>
        </td>
    </tr>   
    <tr>    
        <td style="width:30%;">
        </td>
        <td style="width:20%;">
            Branch Code  
        </td>
        <td style="width:25%; text-align:left;">
            <asp:TextBox ID="txtBranchId" class="NormalText" runat="server" Width="87%" onkeypress='return DecimalCheck(event,this)' readonly = "true" onchange = "BranchCodeOnChange()" MaxLength="10"></asp:TextBox>
        </td>
    </tr>   
    <tr>        
        <td style="width:30%;">
        </td>
        <td style="width:20%;">
            Branch Name  
        </td>
        <td style="width:25%; text-align:left;">
            <asp:TextBox ID="txtBranchName" class="NormalText" ReadOnly="true" runat="server" Width="87%" ></asp:TextBox>
        </td>
    </tr>
    <tr>    
        <td style="width:30%;">
        </td>
        <td style="width:20%;">
            Denomination 
        </td>
    </tr>  
</table>
<table class="style1" style="width:80%;margin: 0px auto;">
    <tr>
        <td style="width:30%;"></td>
        <td style="width:52.2%;">
            <asp:Panel ID="pnlDenomination" runat="server">
            </asp:Panel>
        </td>
        <td style="width:17.8%;"></td>
    </tr> 
</table>
<table class="style1" style="width:80%;margin: 0px auto;">
<tr> 
        <td style="width:30%;">
        </td>
        <td style="width:20%; text-align:left;">
        </td>
        <td style="width:25%">
        </td>
        <td style="width:25%">
        </td>
    </tr>
    <tr>    
        <td style="width:30%;">
        </td>
        <td style="width:20%;">
            In words
        </td>
        <td style="width:25%; text-align:left;">
            <asp:TextBox ID="txtWords" class="NormalText" ReadOnly="true" runat="server" Width="87%" MaxLength="5000" textmode="MultiLine" onkeypress='return TextAreaCheck(event)'  style=" font-family:Cambria;font-size:10pt;"></asp:TextBox>
        </td>
    </tr>  
</table>

<div  style="width:90%; height:auto; margin:0px auto; background-color:#EEB8A6;">
    <div style="width:80%;height:auto; padding-left:10px;margin:0px auto;">
        <div style="text-align:center; height: 63px;"><br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="SAVE" onclick="return Saveonclick()"/>       
            &nbsp;
            &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" />
        </div>
    </div>
</div>   
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

