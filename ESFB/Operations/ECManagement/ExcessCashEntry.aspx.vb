﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ExcessCashEntry
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1268) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            DT = DB.ExecuteDataSet("SELECT Value FROM EC_Denomination_Master WHERE Status = 1 ORDER BY Value DESC").Tables(0)
            Dim Denominations As String = ""
            Dim i As Integer = 0
            For Each dtt In DT.Rows
                Denominations += DT.Rows(i)(0).ToString() + "Ñ"
                i += 1
            Next
            Me.hidDenominations.Value = Denominations
            Dim UserID As String = CStr(Session("UserID"))
            DT = DB.ExecuteDataSet("select b.branch_id,b.branch_name from emp_master a inner join branch_master b on a.branch_id = b.branch_id where a.emp_code = '" + UserID + "'").Tables(0)
            Me.hidBID.Value = DT.Rows(0)(0).ToString()
            Me.hidBName.Value = DT.Rows(0)(1).ToString()
            Me.Master.subtitle = "Excess Cash Entry"
            
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim BranchID As Integer = CInt(Data(1))
            Dim BranchName As String = ""
            Dim EntryData As String = ""
            Dim Denominations As String = ""
            DT = DB.ExecuteDataSet("SELECT Branch_Name FROM BRANCH_MASTER WHERE Branch_ID = " + BranchID.ToString()).Tables(0)
            If DT.Rows.Count > 0 Then
                BranchName = DT.Rows(0)(0).ToString()
            End If
            DT1 = DB.ExecuteDataSet("select TotalAmount,words,entryid from ec_entry_master where branchid = " + BranchID.ToString() + " and (select cast(entrydate as date) from ec_entry_master where branchid = " + BranchID.ToString() + " and approvedstatus <> 1) = (select cast(getdate() as date)) and approvedstatus <> 1").Tables(0)
            If DT1.Rows.Count > 0 Then
                Dim i As Integer = 0
                For Each dtr As DataRow In DT1.Rows
                    For k As Integer = 0 To DT1.Columns.Count - 1
                        EntryData += DT1.Rows(i)(k).ToString() + "Ñ"
                    Next
                    i += 1
                Next
                Dim entryid = DT1.Rows(0)(2)
                DT2 = DB.ExecuteDataSet("Select value,count,total from ec_denomination_entry where entryid = " + entryid.ToString()).Tables(0)
                Dim j As Integer = 0
                For Each dtt In DT2.Rows
                    Denominations += DT2.Rows(j)(0).ToString() + "ÿ" + DT2.Rows(j)(1).ToString() + "ÿ" + DT2.Rows(j)(2).ToString() + "Ñ"
                    j += 1
                Next
            End If
            If EntryData = "" Then
                CallBackReturn = BranchName
            Else
                CallBackReturn = BranchName + "Ø" + EntryData + "Ø" + Denominations
            End If
        End If
        If CInt(Data(0)) = 2 Then
            Try
                Dim EntryDate As DateTime = Convert.ToDateTime(Data(1))
                Dim BranchID As Integer = CInt(Data(2))
                Dim BranchName As String = CStr(Data(3))
                Dim Denomination As String = CStr(Data(4))
                Dim TotalAmount As Integer = CInt(Data(5))
                Dim Words As String = CStr(Data(6))
                Dim MakerID As String = CStr(Session("UserID"))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Dim Params(8) As SqlParameter
                Params(0) = New SqlParameter("@EntryDate", SqlDbType.DateTime)
                Params(0).Value = EntryDate
                Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(1).Value = BranchID
                Params(2) = New SqlParameter("@BranchName", SqlDbType.VarChar, 50)
                Params(2).Value = BranchName
                Params(3) = New SqlParameter("@TotalAmount", SqlDbType.Int)
                Params(3).Value = TotalAmount
                Params(4) = New SqlParameter("@Words", SqlDbType.VarChar, 500)
                Params(4).Value = Words
                Params(5) = New SqlParameter("@MakerID", SqlDbType.VarChar, 500)
                Params(5).Value = MakerID
                Params(6) = New SqlParameter("@Denomination", SqlDbType.VarChar, 5000)
                Params(6).Value = Denomination
                Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(8).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_EC_Entry_Reg", Params)
                ErrorFlag = CInt(Params(7).Value)
                Message = CStr(Params(8).Value)
                CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
            Catch ex As Exception
            
                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try
        End If
    End Sub
End Class
