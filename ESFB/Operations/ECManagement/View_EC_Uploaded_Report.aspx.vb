﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports System.Data.SqlClient
Partial Class View_EC_Uploaded_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DS As New DataSet
    Dim DTExcel As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim DT As New DataTable
            Dim EntryDate As Date = CDate(Request.QueryString.Get("Date"))
            Dim BranchCode As Integer = CInt(Request.QueryString.Get("BranchID"))


            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            'cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim RowBG As Integer = 0
            Dim DR As DataRow

            RH.Heading(Session("FirmName"), tb, "Excess Cash Report - " + EntryDate.ToString(), 100)
            tb.Attributes.Add("width", "100%")
            RH.BlankRow(tb, 3)

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 9, 9, "c", "Entry Date")
            RH.AddColumn(TRHead, TRHead_01, 9, 9, "c", "Branch Code")
            RH.AddColumn(TRHead, TRHead_02, 14, 14, "c", "Branch Name")
            RH.AddColumn(TRHead, TRHead_03, 6, 6, "c", "2000 (Count)")
            RH.AddColumn(TRHead, TRHead_04, 6, 6, "c", "500 (Count)")
            RH.AddColumn(TRHead, TRHead_05, 6, 6, "c", "200 (Count)")
            RH.AddColumn(TRHead, TRHead_06, 6, 6, "c", "100 (Count)")
            RH.AddColumn(TRHead, TRHead_07, 10, 10, "c", "Total Amount")
            RH.AddColumn(TRHead, TRHead_08, 6, 6, "c", "BTI Requested 2000 (Count)")
            RH.AddColumn(TRHead, TRHead_09, 6, 6, "c", "BTI Requested 500 (Count)")
            RH.AddColumn(TRHead, TRHead_10, 6, 6, "c", "BTI Requested 200 (Count)")
            RH.AddColumn(TRHead, TRHead_11, 6, 6, "c", "BTI Requested 100 (Count)")
            RH.AddColumn(TRHead, TRHead_12, 10, 10, "c", "BTI Requested Total Amount")
            tb.Controls.Add(TRHead)



            RH.BlankRow(tb, 3)
            Dim i As Integer

            DT = DB.ExecuteDataSet("SELECT * FROM EC_UPLOADED_DATA WHERE BranchID = " + BranchCode.ToString() + " and (cast(Date as date)) = cast('" + EntryDate.ToString() + "' as date)").Tables(0)


            For Each DR In DT.Rows
                i += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 9, 9, "c", CDate(DR(1)).ToString("dd/MM/yyyy"))
                RH.AddColumn(TR3, TR3_01, 9, 9, "c", DR(2).ToString())
                RH.AddColumn(TR3, TR3_02, 14, 14, "c", DR(3).ToString())
                RH.AddColumn(TR3, TR3_03, 6, 6, "c", DR(4).ToString())
                RH.AddColumn(TR3, TR3_04, 6, 6, "c", DR(5).ToString())
                RH.AddColumn(TR3, TR3_05, 6, 6, "c", DR(6).ToString())
                RH.AddColumn(TR3, TR3_06, 6, 6, "c", DR(7).ToString())
                RH.AddColumn(TR3, TR3_07, 10, 10, "c", DR(8).ToString())
                RH.AddColumn(TR3, TR3_08, 6, 6, "c", DR(9).ToString())
                RH.AddColumn(TR3, TR3_09, 6, 6, "c", DR(10).ToString())
                RH.AddColumn(TR3, TR3_10, 6, 6, "c", DR(11).ToString())
                RH.AddColumn(TR3, TR3_11, 6, 6, "c", DR(12).ToString())
                RH.AddColumn(TR3, TR3_12, 10, 10, "c", DR(13).ToString())
                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    'Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    'End Sub

End Class
