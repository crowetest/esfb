﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="viewNonITPolicies.aspx.vb" Inherits="Operations_viewNonITPolicies" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
 <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
  
 <style type="text/css">
 .sub_first
{
   background-color:white; height:20px;
   font-family:Cambria;color:#47476B;font-size:5px;
}
     .style1
     {
         width: 10%;
         height: 20px;
     }
     .style2
     {
         width: 80%;
         height: 20px;
     }
 </style>
<br />
<div   style="width: 60%;height:600px; margin:0px auto; background-color:silver">
    <table align="center" style="width: 100%; margin:0px auto;"  >
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                1</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Accounting Policy</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(1)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                2</td>
            <td style="width:80%;text-align:left;">
                 &nbsp;ALM and MR Policy</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(2)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                3</td>
            <td style="width:80%;text-align:left;">
                &nbsp;AML KYC Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(3)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                4</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Compliance Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(4)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                5</td>
            <td style="width:80%;text-align:left;">
                Credit Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(5)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                6</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Credit Risk Management Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(6)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                7</td>
            <td style="width:80%;text-align:left;">
               &nbsp;Customer Grievance Redressal Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(7)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                8</td>
            <td style="width:80%;text-align:left;">
                &nbsp;ESAF SFB Risk based Internal Audit Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(8)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                9</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Fair Practice and Customer Service Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(9)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                10</td>
            <td style="width:80%;text-align:left;">
                &nbsp;FATCA Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(10)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                11</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Fraud Risk Management Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(11)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                12</td>
            <td style="width:80%;text-align:left;">
                &nbsp;HR Policies</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(12)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                13</td>
            <td style="width:80%;text-align:left;">
                &nbsp;ICAAP Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(13)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                14</td>
            <td style="width:80%;text-align:left;">
               &nbsp;Investment Policy</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(14)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                15</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Liquidity Management Policy</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(15)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                16</td>
            <td style="width:80%;text-align:left;">
                &nbsp;New Product Approval Policy</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(16)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                17</td>
            <td style="width:80%;text-align:left;">
                &nbsp;NPA Management Provisioning Policy</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(17)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                18</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Outsourcing Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(18)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                19</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Policy to avoid mis-selling</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(19)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                20</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Premises Acquisition Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(20)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                21</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Procurement Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(21)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                22</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Related Party Transaction Policy</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(22)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                23</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Risk Management  Governance Policy</td>
            <td style="width:10%;text-align:center;">
                 <a href='' onclick="return viewOnClick(23)" target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                24</td>
            <td style="width:80%;text-align:left;">
               &nbsp;Stress Testing Policy</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(24)" target="_blank" >View</td>
        </tr>
         <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                25</td>
            <td style="width:80%;text-align:left;">
                &nbsp;Vehicle Management Policy</td>
            <td style="width:10%;text-align:center;">
                <a href='' onclick="return viewOnClick(25)" target="_blank" >View</td>
        </tr>
         <tr class="sub_first">
            <td style="text-align:center;" class="style1">
                26</td>
            <td style="text-align:left;" class="style2">
               &nbsp;Whistle blower policy</td>
            <td style="text-align:center;" class="style1">
                <a href='' onclick="return viewOnClick(26)" target="_blank" >View</td>
        </tr>
        <tr class="sub_first" style="height:30px;">
            <td style="text-align:center;" colspan="3">
                <input id="btnExit" type="button" value="EXIT" style="width:10%; font-family:Cambria;" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table></div>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../Home.aspx","_self");
        }

        function viewOnClick(ID) {
            window.open("viewPolicy.aspx?ID=" + btoa(ID), "_blank");
            return false;
        }

       
// ]]>
    </script>
</asp:Content>

