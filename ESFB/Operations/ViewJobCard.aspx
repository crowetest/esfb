﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ViewJobCard.aspx.vb" Inherits="Operations_ViewJobCard" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
 <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
 <style type="text/css">
 .sub_first
{
   background-color:white; height:20px;
   font-family:Cambria;color:#47476B;font-size:5px;
}
 </style>
<br />
<div   style="width: 60%;height:540px; margin:0px auto; background-color:silver">
    <table align="center" style="width: 100%; margin:0px auto;"  >
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                1</td>
            <td style="width:80%;text-align:left;">
                ATM Debit Card and Alternate Channel</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job Card Profile ATM Debit Card and Alternate Channel Ver2.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                2</td>
            <td style="width:80%;text-align:left;">
                Branch EOD Process</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_Branch_EOD_Process_Ver3.1.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                3</td>
            <td style="width:80%;text-align:left;">
                Branches Daily Activities</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Guidelines_to_Branches_Daily_Activities-Dated160317.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                4</td>
            <td style="width:80%;text-align:left;">
                CASA Account Maintenance</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_CASA_Account_Maintenance_Ver3.0.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                5</td>
            <td style="width:80%;text-align:left;">
                Cash Operations</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_Cash_Operations_Ver3.1.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                6</td>
            <td style="width:80%;text-align:left;">
                Clearing Operations</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_Clearing_Ver_3.0.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                7</td>
            <td style="width:80%;text-align:left;">
                DD &amp; Transfer Transaction</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_DD_and_Transfer_Transaction_Ver3.1.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                8</td>
            <td style="width:80%;text-align:left;">
                General Account Services</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_General_Account_Services_Final_Ver3.1.pdf' target="_blank">View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                9</td>
            <td style="width:80%;text-align:left;">
                Group Loans</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/ESAF_Op  Manual_Group Loans_v4.docx' >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                10</td>
            <td style="width:80%;text-align:left;">
                Inventory Management</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_Inventory Management_Ver3.0.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                11</td>
            <td style="width:80%;text-align:left;">
                Loans</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_Loans_Ver3.0.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                12</td>
            <td style="width:80%;text-align:left;">
                Locker</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/ESAF_Op. Manual_Locker_v5.docx' >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                13</td>
            <td style="width:80%;text-align:left;">
                Locker Operations</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_Lockers_Ver3.00.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                14</td>
            <td style="width:80%;text-align:left;">
                Mbank Mobility Solution Microfinance Loan</td>
            <td style="width:10%;text-align:center;">
               <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Mbank_Mobility_Solution_MicrofinanceLoan_Ver_1.0-Dated160317.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                15</td>
            <td style="width:80%;text-align:left;">
                NewGen Customer Account Origination</td>
            <td style="width:10%;text-align:center;">
               <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_NewGen_Customer_Account_Origination_Ver3.3.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                16</td>
            <td style="width:80%;text-align:left;">
                Profile Customer Master</td>
            <td style="width:10%;text-align:center;">
               <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_Cust_Master_Ver3.1.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                17</td>
            <td style="width:80%;text-align:left;">
                Profile NACH Mandat</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_NACH_Mandate_Ver3.0.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                18</td>
            <td style="width:80%;text-align:left;">
                Profile Path Guide</td>
            <td style="width:10%;text-align:center;">
               <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_Path_Guide_Ver2.1.pdf' target="_blank">View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                19</td>
            <td style="width:80%;text-align:left;">
                Retail Loan Origination</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_Retail_Loan Origination_Ver1.1-Dated160317.pdf' target="_blank">View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                20</td>
            <td style="width:80%;text-align:left;">
                RTGS NEFT GLReports</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_RTGS_NEFT_GLReports_Ver3.0.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                21</td>
            <td style="width:80%;text-align:left;">
                TermDeposit</td>
            <td style="width:10%;text-align:center;">
               <a href='http://eweb.emfil.org/manual/jobcards/Job_Cards_Profile_TermDeposit_Ver3.0.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                22</td>
            <td style="width:80%;text-align:left;">
                User Management</td>
            <td style="width:10%;text-align:center;">
                <a href='http://eweb.emfil.org/manual/jobcards/Job_Card_Profile_User_Management_Ver3.1.pdf' target="_blank" >View</a></td>
        </tr>
        <tr class="sub_first">
            <td style="width:10%;text-align:center;">
                &nbsp;</td>
            <td style="width:80%;text-align:left;">
                &nbsp;</td>
            <td style="width:10%;text-align:center;">
                &nbsp;</td>
        </tr>
        <tr class="sub_first" style="height:30px;">
            <td style="text-align:center;" colspan="3">
                <input id="btnExit" type="button" value="EXIT" style="width:10%; font-family:Cambria;" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table></div>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../Home.aspx","_self");
        }

// ]]>
    </script>
</asp:Content>

