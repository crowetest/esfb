﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    EnableEventValidation="false" CodeFile="State_District_Dtl.aspx.vb" Inherits="Filter_Application" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
        .style1
        {
            width: 15%;
        }
        .style2
        {
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {   
                document.getElementById("rowDistrict").style.display = "none";                          
                ToServer("1ʘ", 1);
            }
            function StateOnChange()
            {
                 ToServer("2ʘ"+document.getElementById("<%= cmbState.ClientID %>").value, 2);
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {
                        if(Arg != "")
                        {
                            ComboFill(Arg, "<%= cmbState.ClientID %>");
                        }
                        break;
                    }
                    case 2:
                    {
                        if(Arg != "")
                        {
                            document.getElementById("<%= hdnApplication.ClientID %>").value= Arg;
                            DisplayTable();
                        }
                        break;
                    }
                }
            }
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function DisplayTable()
            {
                document.getElementById("rowDistrict").style.display = "";
                document.getElementById("<%= pnlDoc.ClientID %>").style.display = "";
                var tab = "";
                var row_bg = 0;
                tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
                tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr>";
                tab += "<td style='width:4%;text-align:center;'>Sl No</td>";
                tab += "<td style='width:48%;text-align:left' >District Name</td>";
                tab += "<td style='width:48%;text-align:center' >District Code</td>";
                tab += "</tr>";
                if (document.getElementById("<%= hdnApplication.ClientID %>").value != "") {
                    row = document.getElementById("<%= hdnApplication.ClientID %>").value.split("Ĉ");

                    for (n = 0; n <= row.length - 2; n++) {
                        col = row[n].split("µ");

                        if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class=sub_first>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class=sub_second>";
                        }
                        i = n + 1;
                        
                        tab += "<td style='width:4%;text-align:center;'>" + i + "</td>";
                        tab += "<td style='width:49%;text-align:left' >" + col[0] + "</td>";
                        tab += "<td style='width:49%;text-align:center' >" + col[1] + "</td>";
                        tab += "</tr>";
                    }
                }
                tab += "</table></div></div></div>";
                document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = tab;
            }
            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
            <br />
    <div style="width: 80%; margin: 0px auto;">
        <table class="style1" style="width: 100%; top: 350px auto;">
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    State</td>
                <td style="text-align: left; " colspan="2">
                    <asp:DropDownList ID="cmbState" class="NormalText" Style="text-align: left;" runat="server"
                        Font-Names="Cambria" Width="98%" ForeColor="Black">
                        <asp:ListItem Value="-1"> Select</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; " colspan="2">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
            <tr id="rowDistrict">
                <td style="text-align: left; " class="style2" colspan="5">
                    <asp:Panel ID="pnlDoc" runat="server">
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 12%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                     &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="5">
                    <br />
                    <br />
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                        value="EXIT" onclick="return btnExit_onclick()" /></td>
            </tr>
        </table>
    </div>
            <asp:HiddenField ID="hdnApplication" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
