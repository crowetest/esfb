﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="SignatureBranchMappingVerification.aspx.vb" Inherits="SignatureBranchMappingVerification" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">        
        function table_fill() {
            document.getElementById("<%= pnBranchHistory.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var POA = "";
            var SIGN = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:3%;text-align:center' >Branch Code</td>";
            tab += "<td style='width:5%;text-align:center' >Branch Name</td>";
            tab += "<td style='width:3%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:5%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:5%;text-align:center' >Department</td>";
            tab += "<td style='width:5%;text-align:center' >Designation</td>";
            tab += "<td style='width:5%;text-align:center' >Grade</td>";
            tab += "<td style='width:4%;text-align:center' >Date of join</td>";
            tab += "<td style='width:5%;text-align:center'>POA</td>";
            tab += "<td style='width:3%;text-align:center'>View Signature</td>";
            tab += "<td style='width:0%;text-align:center;display:none;'>MappId</td>";
            tab += "<td style='width:5%;text-align:center'>Approval Type</td>";
            tab += "<td style='width:4%;text-align:center'>Start Date</td>";
            tab += "<td style='width:4%;text-align:center'>End Date</td>";
            tab += "<td style='width:5%;text-align:center'>Remarks</td>";
            tab += "<td style='width:4%;text-align:center'>Action</td>";
            tab += "<td style='width:5%;text-align:center'>Reason</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
            {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) 
                {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n;
                    tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:3%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:3%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[7] + "</td>"; 
                    tab += "<td style='width:5%;text-align:left'>" + col[8] + "</td>";
                    if (col[9] != 0)
                        tab += "<td style='width:3%;text-align:left'><img id='ImgSignature' src='../Image/attchment2.png' onclick='viewSignature("+ col[9] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    else
                        tab += "<td style='width:3%;text-align:left'></td>";

                    tab += "<td style='width:0%;text-align:left;display:none;'>" + col[10] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[11] + "</td>";
                  
                    if (col[12] != "1/1/1900 12:00:00 AM")
                        tab += "<td style='width:4%;text-align:left'>" + col[12] + "</td>";
                    else
                        tab += "<td style='width:4%;text-align:left'></td>";
                    if (col[13] != "1/1/1900 12:00:00 AM")
                        tab += "<td style='width:4%;text-align:left'>" + col[13] + "</td>";
                    else
                        tab += "<td style='width:4%;text-align:left'></td>";
                    if (col[14] != "")
                        tab += "<td style='width:4%;text-align:left'>" + col[14] + "</td>";
                    else
                        tab += "<td style='width:4%;text-align:left'></td>";                  

                    var select = "<select style='width: 100px' id='cmb" + i + "' class='NormalText' name='cmb" + i + "' >";
                    select += "<option value='-1'>Select</option>";
                    select += "<option value='1'>Approved  </option>";
                    select += "<option value='2'>Rejected  </option>";
                    tab += "<td style='width:4%;text-align:left'>" + select + "</td>";
                    
                    var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)' ></textarea>";

                    tab += "<td style='width:5%;text-align:left'>" + txtBox + "</td>";
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnBranchHistory.ClientID %>").innerHTML = tab;
            //--------------------- Clearing Data ------------------------//
        }

         function viewSignature(SignID)
        {
            window.open("Reports/ShowAttachment.aspx?SignID=" + btoa(SignID) + "&RptID=10");
            return false;
        }
        function UpdateValue() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("No pending verification");
                return false;
            }
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 1; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                if (document.getElementById("cmb" + (n)).value != -1) {
                    if ((document.getElementById("cmb" + (n)).value == 2) && (document.getElementById("txtRemarks" + (n)).value == "")) {
                        alert("Enter Rejected Reason");
                        document.getElementById("txtRemarks" + (n)).focus();
                        return false;
                    }
                    document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[10] + "µ" + document.getElementById("cmb" + (n)).value + "µ" + document.getElementById("txtRemarks" + (n)).value;
                   
                }
            }
            return true;
        }
        function FromServer(arg, context) {
               switch (context) {
                case 1:
                {   
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == 0) window.open("SignatureBranchMappingVerification.aspx", "_self");
                    break;
                }
            }
        }
      
        function btnApprove_onclick() {
            var ret = UpdateValue();
            if (ret == true) 
            {
                if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                    alert("Nothing to save");
                    return false;
                }
                var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
                var Data = "1Ø" + strempcode;
                ToServer(Data, 1);
            }
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

</script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_history_dtls" runat="server" />

<br />
 

    <div id ="divBranch">
        <table class="style1" style="width:100%">
        <tr> 
            <td colspan="3"><asp:Panel ID="pnBranchHistory" runat="server">
            </asp:Panel></td>
        </tr>
        
        </table> 
    </div>
   
    <table class="style1" style="width:100%">

        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />

                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
     
     
    </table>    
<br /><br />
</asp:Content>


