﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="SignatureUploadingVerification.aspx.vb" Inherits="SignatureUploadingVerification" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">    
     
        function table_fill() {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:3%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:6%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:10%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:5%;text-align:center'>Branch Code</td>";
            tab += "<td style='width:10%;text-align:center'>Branch Name</td>";
            tab += "<td style='width:8%;text-align:center'>Department</td>";
            tab += "<td style='width:8%;text-align:center'>Designation</td>";
            tab += "<td style='width:4%;text-align:center'>Grade</td>";
            tab += "<td style='width:6%;text-align:center'>Date of Joining</td>";
            tab += "<td style='width:5%;text-align:center'>POA</td>";
            tab += "<td style='width:4%;text-align:center'>Signature</td>";
            tab += "<td style='width:4%;text-align:center'>Action</td>";
            tab += "<td style='width:5%;text-align:center'>Remarks</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                
                    tab += "<td style='width:3%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[8] + "</td>";

                    tab += "<td style='width:4%;text-align:left'><img id='ImgSignature' src='../Image/attchment2.png' onclick='viewSignature("+ col[9] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    var select = "<select style='width: 100px' id='cmb" + i + "' class='NormalText' name='cmb" + i + "' >";
                    select += "<option value='-1'>Select</option>";
                    select += "<option value='1'>Approved  </option>";
                    select += "<option value='2'>Rejected  </option>";
                    tab += "<td style='width:4%;text-align:left'>" + select + "</td>";

                    
                    var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' style='width:99%; float:left;' maxlength='300' ></textarea>";

                    tab += "<td style='width:5%;text-align:left'>" + txtBox + "</td>";

                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//


        }
                function table_fill_status() {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:3%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:6%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:10%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:5%;text-align:center'>Branch Code</td>";
            tab += "<td style='width:10%;text-align:center'>Branch Name</td>";
            tab += "<td style='width:8%;text-align:center'>Department</td>";
            tab += "<td style='width:8%;text-align:center'>Designation</td>";
            tab += "<td style='width:4%;text-align:center'>Grade</td>";
            tab += "<td style='width:4%;text-align:center'>Changed Status</td>";
            tab += "<td style='width:4%;text-align:center'>Action</td>";
            tab += "<td style='width:5%;text-align:center'>Remarks</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                
                    tab += "<td style='width:3%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[7] + "</td>";
                 
                    var select = "<select style='width: 100px' id='cmb" + i + "' class='NormalText' name='cmb" + i + "' >";
                    select += "<option value='-1'>Select</option>";
                    select += "<option value='1'>Approved  </option>";
                    select += "<option value='2'>Rejected  </option>";
                    tab += "<td style='width:4%;text-align:left'>" + select + "</td>";

                    
                    var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' style='width:99%; float:left;' maxlength='300' ></textarea>";

                    tab += "<td style='width:5%;text-align:left'>" + txtBox + "</td>";

                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//


        }
         function viewSignature(SignID)
        {
       
            window.open("Reports/ShowAttachment.aspx?SignID=" + btoa(SignID) + "&RptID=10");
            return false;
        }

        function UpdateValue() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("No Verification");
                return false;
            }

            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");
         
                if (document.getElementById("cmb" + (n+1)).value != -1) {
                    if ((document.getElementById("cmb" + (n+1)).value == 2) && (document.getElementById("txtRemarks" + (n+1)).value == "")) {
                        alert("Enter Rejected Reason");
                        document.getElementById("txtRemarks" + (n+1)).focus();
                        return false;
                    }
                    var SignID_EmpCode;
                    if (document.getElementById("<%= cmbApproval.ClientID %>").value == 1)
                        SignID_EmpCode = col[9] ;
                    else
                        SignID_EmpCode = col[0] ;
                    document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + SignID_EmpCode + "µ" + document.getElementById("cmb" + (n+1)).value + "µ" + document.getElementById("txtRemarks" + (n+1)).value;
                   
                }
            }
            return true;
        }
        function FromServer(arg, context) {
            if (context == 1)
            {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("SignatureUploadingVerification.aspx", "_self");
            }
            else if (context == 2)
            {

                document.getElementById("<%= hid_dtls.ClientID %>").value = arg;
                if (document.getElementById("<%= cmbApproval.ClientID %>").value == 1)
                    table_fill();
                else
                    table_fill_status();
            }
        }
        function ApprovalForOnChange() {
          
           
            var strempcode = document.getElementById("<%= cmbApproval.ClientID %>").value;
            var Data = "2Ø" + strempcode;
            ToServer(Data, 2);
        }
        function btnApprove_onclick() {
            var ret = UpdateValue();
            if (ret == 0) return false;
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                alert("Select Any Request for Approval");
                return false;
            }
            var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
            var Data = "1Ø" + document.getElementById("<%= cmbApproval.ClientID %>").value + "Ø" + strempcode;
            ToServer(Data, 1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
         <tr> 
            <td class="style9" style="width: 400px">
             Approval For</td>
            <td class="style10" style="text-align:left;width: 451px">
            <asp:DropDownList ID="cmbApproval" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="46%" ForeColor="Black" Height="16px">
                            <asp:ListItem Value="1">Upload</asp:ListItem>
                            <asp:ListItem Value="2">Status Change</asp:ListItem>

                        </asp:DropDownList>
                </td>
            
        </tr>
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <asp:HiddenField ID="hid_AppLevel" runat="server" />

                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

