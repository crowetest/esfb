﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Text

Partial Class CTSMemoExcelUpload
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1252) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Upload Return Memo Details"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0
            DT = DB.ExecuteDataSet(" select b.Branch_ID,b.Branch_Name from EMP_MASTER e , BRANCH_MASTER b where e.Branch_ID=b.Branch_ID  and e.emp_code=" & CInt(Session("UserID")) & "").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
            cmbBranch.SelectedIndex = 0
            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "RequestOnchange();", True)
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            cmbBranch.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent


    End Sub
#End Region

    Private Sub initializeControls()
        cmbBranch.Focus()

    End Sub
    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Try
    '        Dim ContentType As String = ""
    '        Dim AttachImg As Byte() = Nothing
    '        Dim hfc As HttpFileCollection
    '        hfc = Request.Files

    '        Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
    '        Dim Branch As Integer = CInt(Data(1))
    '        Dim RequestID As Integer = 0
    '        Dim UserID As String = Session("UserID").ToString()
    '        Dim Message As String = Nothing
    '        Dim ErrorFlag As Integer = 0
    '        Dim NoofAttachments As Integer = 0
    '        Dim FileName As String = ""
    '        'Dim DTEXCELDATA As New DataTable
    '        'Dim filepath As String = ""
    '        'Dim folderPath As String = ""
    '        'If hfc.Count > 0 Then
    '        '    For i = 0 To hfc.Count - 1
    '        '        Dim myFile As HttpPostedFile = hfc(i)
    '        '        Dim nFileLen As Integer = myFile.ContentLength
    '        '        FileName = myFile.FileName()
    '        '        'fup1.SaveAs(Server.MapPath("" + FileName))
    '        '        'filepath = Server.MapPath("" + FileName)
    '        '        If (nFileLen > 0) Then
    '        '            NoofAttachments += 1
    '        '        End If
    '        '        'string folderPath = Server.MapPath("~/Files/");
    '        '        folderPath = "E:\\Images\"
    '        '        ' Your path Where you want to save other than Server.MapPath
    '        '        'Check whether Directory (Folder) exists.
    '        '        If Not Directory.Exists(folderPath) Then
    '        '            'If Directory (Folder) does not exists. Create it.
    '        '            Directory.CreateDirectory(folderPath)
    '        '        End If

    '        '        'Save the File to the Directory (Folder).
    '        '        fup1.SaveAs(folderPath & Path.GetFileName(fup1.FileName))
    '        '    Next
    '        'End If
    '        'filepath = folderPath & Path.GetFileName(fup1.FileName)
    '        'Dim conStr As String = ""
    '        '' if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
    '        ''    conStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"
    '        ''Else
    '        ''    conStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"
    '        'conStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source='"D:\\TDS_DEMO_DOWNLOAD"';Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'"
    '        ''conStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + ";Extended Properties=Excel 12.0;"
    '        ''conStr = String.Format("Provider={0};Data Source={1};Extended Properties=Excel 8.0;HDR=YES;FMT=Delimited""", "Microsoft.Jet.OLEDB.4.0", filepath)
    '        'Dim connExcel As New OleDbConnection(conStr)
    '        'Dim cmdExcel As New OleDbCommand()
    '        'Dim oda As New OleDbDataAdapter()
    '        'cmdExcel.Connection = connExcel
    '        ''Get the name of First Sheet
    '        'connExcel.Open()
    '        'Dim dtExcelSchema As DataTable = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
    '        'Dim SheetName As String = ""
    '        'If dtExcelSchema.Rows.Count > 0 Then
    '        '    SheetName = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
    '        'End If
    '        'connExcel.Close()

    '        ''Read Data from First Sheet
    '        'connExcel.Open()
    '        'cmdExcel.CommandText = "SELECT * From [" & SheetName & "]"
    '        'oda.SelectCommand = cmdExcel
    '        'oda.Fill(DTEXCELDATA)
    '        'DTEXCELDATA.TableName = SheetName.ToString().Replace("$", "")

    '        'Try
    '        '    Dim Params(5) As SqlParameter
    '        '    Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
    '        '    Params(0).Value = Branch
    '        '    Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
    '        '    Params(1).Value = UserID
    '        '    Params(2) = New SqlParameter("@REQUEST_ID", SqlDbType.Int)
    '        '    Params(2).Direction = ParameterDirection.Output
    '        '    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
    '        '    Params(3).Direction = ParameterDirection.Output
    '        '    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
    '        '    Params(4).Direction = ParameterDirection.Output
    '        '    Params(5) = New SqlParameter("@NOOFATTACHMENTS", SqlDbType.Int)
    '        '    Params(5).Value = NoofAttachments
    '        '    DB.ExecuteNonQuery("SP_CTS_REQUEST_FROM_BRANCH", Params)
    '        '    ErrorFlag = CInt(Params(3).Value)
    '        '    Message = CStr(Params(4).Value)
    '        '    RequestID = CInt(Params(2).Value)
    '        'Catch ex As Exception
    '        '    Message = ex.Message.ToString
    '        '    ErrorFlag = 1
    '        'End Try
    '        'If ErrorFlag = 0 Then
    '        '    initializeControls()
    '        'End If
    '        'If RequestID > 0 And hfc.Count > 0 Then
    '        '    For i = 0 To hfc.Count - 1
    '        '        Dim myFile As HttpPostedFile = hfc(i)
    '        '        Dim nFileLen As Integer = myFile.ContentLength
    '        '        'Dim FileName As String = ""
    '        '        If (nFileLen > 0) Then
    '        '            ContentType = myFile.ContentType
    '        '            FileName = myFile.FileName
    '        '            AttachImg = New Byte(nFileLen - 1) {}
    '        '            myFile.InputStream.Read(AttachImg, 0, nFileLen)
    '        '            Dim Params(3) As SqlParameter
    '        '            Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
    '        '            Params(0).Value = RequestID
    '        '            Params(1) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
    '        '            Params(1).Value = AttachImg
    '        '            Params(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
    '        '            Params(2).Value = ContentType
    '        '            Params(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
    '        '            Params(3).Value = Branch.ToString + "_" + Now().ToString + "_" + FileName

    '        '            DB.ExecuteNonQuery("SP_CTS_REQUEST_ATTACH", Params)
    '        '        End If

    '        '    Next
    '        'End If

    '        'Dim AttachementNo As Integer = 0
    '        'DT = DB.ExecuteDataSet("select COUNT(PkId) from DMS_ESFB.dbo.CTS_REQUEST_ATTACH where Request_ID=" & RequestID & "").Tables(0)
    '        'If CInt(DT.Rows(0)(0)) >= 0 Then
    '        '    AttachementNo = CInt(DT.Rows(0)(0))
    '        '    Dim Err As Integer = DB.ExecuteNonQuery("update CTS_REQUEST_MASTER set NOOF_ATTACHMENTS=" & AttachementNo & " where REQUEST_ID=" & RequestID & "")
    '        'End If

    '        'Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
    '        'cl_script1.Append("         alert('" + Message + "');")
    '        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    '        Dim fileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
    '        Dim fileExtension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)
    '        Dim fileLocation As String = Server.MapPath("../../" & fileName)
    '        FileUpload1.SaveAs(fileLocation)

    '        If fileExtension = ".xls" Then
    '            If Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE") = "x86" Then
    '                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=YES;"""
    '            Else
    '                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
    '            End If

    '            'ElseIf fileExtension = ".xlsx" Then
    '            '    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties='Excel 12.0 Xml;HDR=yes'"
    '        Else
    '            'fileExtension = ".xlsx"
    '            Dim cl_script0 As New System.Text.StringBuilder
    '            cl_script0.Append("         alert('Uploaded Only Excel file with Extention .xls, Ex::sample.xls ');")
    '            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
    '            Exit Sub

    '        End If
    '        Dim con As New OleDbConnection(connectionString)
    '        Dim cmd As New OleDbCommand()
    '        cmd.CommandType = System.Data.CommandType.Text
    '        cmd.Connection = con
    '        Dim dAdapter As New OleDbDataAdapter(cmd)
    '        Dim dtExcelRecords As New DataTable()
    '        con.Open()
    '        Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
    '        Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
    '        cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
    '        dAdapter.SelectCommand = cmd
    '        dAdapter.Fill(dtExcelRecords)
    '        con.Close()
    '    Catch ex As Exception
    '        If Response.IsRequestBeingRedirected Then
    '            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
    '            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
    '        End If

    '    End Try
    'End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim msg As String = ""
        Dim connectionString As String = ""
        Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
        Dim branch_id As String = Data(0)
        Dim DateValue As Date = CDate(Data(1))
        Dim user As String = Session("UserID").ToString()
        If Me.fup1.HasFile = False Then
            Dim cl_script0 As New System.Text.StringBuilder
            cl_script0.Append("         alert('Please browse excel');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
            Exit Sub
        Else
            Dim fileName As String = Path.GetFileName(fup1.PostedFile.FileName)
            Dim fileExtension As String = Path.GetExtension(fup1.PostedFile.FileName)
            Dim fileLocation As String = Server.MapPath("../" & fileName)
            fup1.SaveAs(fileLocation)

            If fileExtension = ".xls" Then
                'If Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE") = "x86" Then
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=NO;"""
                'Else
                '    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If

            ElseIf fileExtension = ".xlsx" Then
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties='Excel 12.0 Xml;HDR=NO'"
            Else
                'fileExtension = ".xlsx"
                Dim cl_script0 As New System.Text.StringBuilder
                cl_script0.Append("         alert('Uploaded Only Excel file with Extention .xls or .xlsx ');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
                Exit Sub

            End If
            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim values As New DataTable
            values.Columns.Add("[Branch Code]", GetType(Long))
            values.Columns.Add("[Item Seq Num[", GetType(Long))
            values.Columns.Add("[RBI Seq Num‎]", GetType(Long))
            values.Columns.Add("[Depositor Account Name]", GetType(Long))
            values.Columns.Add("[Presentment RT Num]", GetType(Long))
            values.Columns.Add("[Cheque No]", GetType(Long))
            values.Columns.Add("[TC]", GetType(Long))
            values.Columns.Add("[Amount]", GetType(Long))
            values.Columns.Add("[Return Reasons]", GetType(String))
            values.Columns.Add("[Issuing BranchName‎]", GetType(String))
            values.Columns.Add("[Branch Name‎]", GetType(String))
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            File.Delete(fileLocation)
            For count = 0 To dtExcelRecords.Rows.Count - 1
                Dim row = values.NewRow()
                row(0) = dtExcelRecords.Rows(count).Item(0)
                row(1) = dtExcelRecords.Rows(count).Item(1)
                row(2) = dtExcelRecords.Rows(count).Item(2)
                row(3) = dtExcelRecords.Rows(count).Item(3)
                row(4) = dtExcelRecords.Rows(count).Item(4)
                row(5) = dtExcelRecords.Rows(count).Item(5)
                row(6) = dtExcelRecords.Rows(count).Item(6)
                row(7) = dtExcelRecords.Rows(count).Item(7)
                row(8) = dtExcelRecords.Rows(count).Item(8)
                row(9) = dtExcelRecords.Rows(count).Item(9)
                row(10) = dtExcelRecords.Rows(count).Item(10)
                'row(11) = dtExcelRecords.Rows(count).Item(11)
                'row(12) = dtExcelRecords.Rows(count).Item(12)
                'row(13) = dtExcelRecords.Rows(count).Item(13)
                'row(14) = dtExcelRecords.Rows(count).Item(14)
                values.Rows.Add(row)
            Next
            con.Close()
            Try
                'Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TableData", dtExcelRecords), New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0), New System.Data.SqlClient.SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000), New System.Data.SqlClient.SqlParameter("@Date", hdnDate.Value)}
                Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TableData", values),
                                                                          New System.Data.SqlClient.SqlParameter("@Date", DateValue),
                                                                          New System.Data.SqlClient.SqlParameter("@User", user),
                                                                          New System.Data.SqlClient.SqlParameter("@BranchID", branch_id),
                                                                          New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0),
                                                                          New System.Data.SqlClient.SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)}
                Parameters(4).Direction = ParameterDirection.Output
                Parameters(5).Direction = ParameterDirection.Output
                'DB.ExecuteNonQuery("SALARY_DATA_MIGRATION", Parameters)
                DB.ExecuteNonQuery("SP_MEMO_DETAILS", Parameters)
                ErrorFlag = CInt(Parameters(4).Value)
                Message = CStr(Parameters(5).Value)

                Dim cl_script1 As New System.Text.StringBuilder
                cl_script1.Append("         alert('" + Message.ToString + "');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)


            Catch ex As Exception
                MsgBox("Exceptional Error Occurred.Please Inform Application Team")

            End Try
        End If
    End Sub
End Class
