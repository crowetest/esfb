﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Surve_Branch_Response.aspx.vb" Inherits="Surve_Branch_Response" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server" >
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
    <script language="javascript" type="text/javascript">
        function UnwantedCharCheck(e)//------------function to check whether a value is > or <
        {
        
           if ((e.which == 60) || (e.which == 62)) {
                e.preventDefault();
            }

        }
        function FromServer(arg, context) {
            if (context == 1) {
                ComboFill(arg, "<%= cmbChecklist.ClientID %>");
                document.getElementById("<%= hid_dtls.ClientID %>").value = "";
                table_fill();
                document.getElementById("<%= cmbChecklist.ClientID %>").focus();
            }
            else if (context == 2) {
                var Data = arg.split("Ø");
                document.getElementById("<%= hid_dtls.ClientID %>").value=Data[1];
                table_fill();
                if (Data[0]==1)
                {
                    document.getElementById("btnSave").disabled = true;
                    document.getElementById("btnDraft").disabled = true;
                }
                else
                {
                    document.getElementById("btnSave").disabled = false;
                    document.getElementById("btnDraft").disabled = false;
                }
            }
            else if (context == 3) {
                var Data = arg.split("Ø");
                alert(Data[1]);
            }
            else if (context == 4) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                document.getElementById("<%= cmbChecklist.ClientID %>").value = -1;
                document.getElementById("<%= hid_dtls.ClientID %>").value="";
                table_fill();
            }
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function block_unblock_remark(i) {
            if (document.getElementById("cmbResp"+i).value != -1)
                document.getElementById("txtRemarks"+i).disabled = false;
            else
                document.getElementById("txtRemarks"+i).disabled = true;

        }
        function FrequencyOnchange() {
            var FreqID=document.getElementById("<%= cmbFrequency.ClientID %>").value;
            var ToData = "1Ø" + FreqID;          
            ToServer(ToData, 1);
          
        }
        function CheckListOnchange() {
            var CheckListID=document.getElementById("<%= cmbChecklist.ClientID %>").value;
            var ToData = "2Ø" + CheckListID;          
            ToServer(ToData, 2);         
        }
       
        function table_fill() {

            document.getElementById("<%= pnTaskResp.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var Remarks = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:10%;text-align:center'>Sl No</td>";
            tab += "<td style='width:0%;text-align:center';display:none; >Task_Id</td>";
            tab += "<td style='width:50%;text-align:center'>Task</td>";
            tab += "<td style='width:15%;text-align:center'>Response</td>";
            tab += "<td style='width:25%;text-align:center'>Remarks</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n;
                    tab += "<td style='width:10%;text-align:center'>" + i + "</td>";
                    tab += "<td style='width:0%;text-align:left';display:none;>" + col[0] + "</td>";
                    tab += "<td style='width:50%;text-align:left'>" + col[1] + "</td>";

                    var select = "<select id='cmbResp" + i + "' class='NormalText' name='cmbResp" + i + "' style='width:100%' onchange='block_unblock_remark("+i+")'   >";

                    select += "<option value='-1' selected=true>-Select-</option>";
                    if ((col[2] == 1))
                        select += "<option value='1' selected=true>YES  </option>";
                    else
                        select += "<option value='1'>YES  </option>";
                    if (col[2] == 2)
                        select += "<option value='2' selected=true>NO  </option>";
                    else
                        select += "<option value='2'>NO  </option>";
                    if (col[2] == 3)
                        select += "<option value='3' selected=true>N/A  </option>";
                    else
                        select += "<option value='3'>N/A  </option>";
                       

                    tab += "<td style='width:15%;text-align:left'>" + select + "</td>";

                    Remarks = "";
                    if (col[3] != "")
                         Remarks = col[3] ;

                    var txtRemarks = "<input id='txtRemarks" + i + "' name='txtRemarks" + i + "' value ='"+ Remarks +"' type='Text' style='width:99%;' class='NormalText' maxlength='50' onkeypress='return UnwantedCharCheck(event)'/>";                                            
                    tab += "<td style='width:25%;text-align:left'>" + txtRemarks + "</td>";                 

                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnTaskResp.ClientID %>").innerHTML = tab;
            //--------------------- Clearing Data ------------------------//
        }
         function UpdateValue(SaveFlag) {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                if (SaveFlag == 1)
                alert("Nothing to save");
                return false;
            }
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 1; n <= row.length - 1; n++) 
            {
                col = row[n].split("µ");
                if ((SaveFlag == 1) && (document.getElementById("cmbResp" + n).value == -1))
                {
                    alert("Select Response Type");
                    document.getElementById("cmbResp" + n).focus();
                    return false;
                }
                if ((document.getElementById("txtRemarks" + n).value == "") && (document.getElementById("cmbResp" + n).value == 2))
                {
                    alert("Enter Remarks for your response <No> ");
                    document.getElementById("txtRemarks" + n).focus();
                    return false;
                }
                document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[0] + "µ" + document.getElementById("cmbResp" + n).value + "µ" + document.getElementById("txtRemarks" + n).value ;
            }
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "") {
                alert("Nothing to save");
                return false;
            }
            else
                return true;
        }
        function btnDraft_onclick() 
        {

            var ret = UpdateValue(0);
            if (ret == true) 
            {
                var Data = "3Ø" + document.getElementById("<%= hid_temp.ClientID %>").value + "Ø" + document.getElementById("<%= cmbChecklist.ClientID %>").value ;
                ToServer(Data, 3);
            }        
        }
        function btnSave_onclick() 
        {
            var ret = UpdateValue(1);
            if (ret == true) 
            {
                var Data = "4Ø" + document.getElementById("<%= hid_temp.ClientID %>").value  + "Ø" + document.getElementById("<%= cmbChecklist.ClientID %>").value ;
                ToServer(Data, 4);
            }        
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        

    </script>
</head>
</html>
<br />
<div style="text-align:center;" align="center">
<table style="text-align:center;width:80%; margin: 0px auto; ">



   
    <tr>
        <td style="width:10%;text-align:left; ">
            Frequency</td>
        <td style="width:40%">
            <asp:DropDownList ID="cmbFrequency" class="NormalText" runat="server" 
                Font-Names="Cambria" Width="92%" 
                 ForeColor="Black" Height="20px">
            </asp:DropDownList>
        </td>
        <td style="width:10%;text-align:left; ">
            Checklist</td>
        <td style="width:40%">
            <asp:DropDownList ID="cmbChecklist" class="NormalText" runat="server" Font-Names="Cambria" 
                Width="100%" ForeColor="Black">
            </asp:DropDownList>
        </td>
    </tr>
    <tr><td style="text-align:center;" colspan="4">&nbsp;</td></tr>
   <tr >
         

        <td colspan="4" >
            <asp:Panel ID="pnTaskResp" runat="server" Width="100%" >
            </asp:Panel> 
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <br />
        </td>
    </tr>
 
    <tr><td style="text-align:center;" colspan="4">&nbsp;</td></tr>
    <tr>
        <td style="text-align:center;" colspan="4">
            &nbsp;
            <input id="btnDraft" style="font-family: cambria; cursor: pointer; width: 5%; " 
                type="button" value="DRAFT" onclick="return btnDraft_onclick()" />
            &nbsp;
            &nbsp;
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 5%; " 
                type="button" value="SAVE" onclick="return btnSave_onclick()" />
            &nbsp;
            &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 5%; " 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />


            <asp:HiddenField 
                ID="hid_dtls" runat="server" /> 
                <asp:HiddenField 
                ID="hid_temp" runat="server" />
        </td>
    </tr>
</table>    
</div>
<br /><br />
</asp:Content>

