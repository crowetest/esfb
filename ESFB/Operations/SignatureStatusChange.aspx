﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="SignatureStatusChange.aspx.vb" Inherits="SignatureStatusChange" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            font-size: 11px;
            color: #000000;
            border: 1px solid #7f9db9;
            }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
        .style1
        {
            width: 46%;
        }
        .style2
        {
            width: 74%;
        }
        .style6
        {
            width: 30%;
            height: 30px;
        }
        .style7
        {
            width: 46%;
            height: 30px;
        }
        .style8
        {
            width: 74%;
            height: 30px;
        }
        .style9
        {
            width: 30%;
            height: 28px;
        }
        .style10
        {
            width: 46%;
            height: 28px;
        }
        .style11
        {
            width: 74%;
            height: 28px;
        }
        </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
      
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

//        function viewSignature(SignID)
//        {       
//            window.open("Reports/ShowAttachment.aspx?SignID=" + btoa(SignID) + "&RptID=10");
//            return false;
//        }
        
        
    function FromServer(arg, context) 
    {
   
       if (context == 1) 
       {
            if(arg != "")
            {
                var Data1 = arg.split("µ");
                var Data = Data1[0].split("|");
                document.getElementById("<%= txtEmpCode.ClientID %>").value = Data[0] ;
                document.getElementById("<%= txtEmpName.ClientID %>").value = Data[1] ;
                document.getElementById("<%= txtBranchCode.ClientID %>").value = Data[2] ;
                document.getElementById("<%= txtBranchName.ClientID %>").value = Data[3] ;
                document.getElementById("<%= txtDepartment.ClientID %>").value = Data[4] ;
                document.getElementById("<%= txtDesignation.ClientID %>").value = Data[5] ;
                document.getElementById("<%= txtCadre.ClientID %>").value = Data[6] ;
                document.getElementById("<%= hdnSignId.ClientID %>").value = Data[7] ; 
                ComboFill(Data1[1], "<%= cmbStatus.ClientID %>");

           }

           else
           {
                alert("Signature not uploaded");
                document.getElementById("<%= txtEmpName.ClientID %>").value = "" ;
                document.getElementById("<%= txtBranchCode.ClientID %>").value = "" ;
                document.getElementById("<%= txtBranchName.ClientID %>").value = "" ;
                document.getElementById("<%= txtDepartment.ClientID %>").value = "" ;
                document.getElementById("<%= txtDesignation.ClientID %>").value = "" ;
                document.getElementById("<%= txtCadre.ClientID %>").value = "" ;
                document.getElementById("<%= hdnSignId.ClientID %>").value = 0 ;
           }
         
        }
     

        else if (context == 2)
        {
            var Data = arg.split("|");
            alert(Data[1]);
            if (Data[0]==0)
            {
                window.open("SignatureStatusChange.aspx", "_self");  
            }
        }   
    }
    function btnExit_onclick() {
   
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

    function EmpCodeOnBlur() {  
  
        if (document.getElementById("<%= txtEmpCode.ClientID %>").value != "") 
        {
            ToServer("1Ø" + document.getElementById("<%= txtEmpCode.ClientID %>").value,1);

        }
    }
    function RequestOnClick() {  
           
        if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "") 
        {
            alert("Enter Employee Code");
            document.getElementById("<%= txtEmpCode.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= hdnSignId.ClientID %>").value == "") 
        {
            alert("Signature not uploaded");
            document.getElementById("<%= txtEmpCode.ClientID %>").focus();

            return false;
        }
        if (document.getElementById("<%= cmbStatus.ClientID %>").value == -1) 
        {
            alert("Select Status to update");
            document.getElementById("<%= cmbStatus.ClientID %>").focus();

            return false;
        }
      
        var Data =  "2Ø" + document.getElementById("<%= hdnSignId.ClientID %>").value + "Ø" + document.getElementById("<%= txtEmpCode.ClientID %>").value + "Ø" + document.getElementById("<%= cmbStatus.ClientID %>").value + "Ø" + document.getElementById("<%= txtRemarks.ClientID %>").value ;
        ToServer(Data, 2);
    }

   
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">

        </script>
    </head>
    </html>
    
    <br />
    <br />
    <br />
    <br />
        <div id ="div"/>
         <table class="style1" style="width:100%">
            <tr> 
                <td colspan="3"><asp:Panel ID="pnBranch" runat="server">
                </asp:Panel></td>
            </tr>
        </table> 
        <div />

    <br />
   
       <table align="center" style="width: 60%; margin:0px auto;">
      
        <tr>
            <td style="width:30%">
                Employee Code</td>
            <td class="style1">
                <asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="NormalTextBox" onkeypress='return NumericCheck(event)' MaxLength="10"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Employee Name</td>
            <td class="style1">
            <asp:TextBox ID="txtEmpName" runat="server" Width="89%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
       <tr>
            <td style="width:30%">
                Branch Code</td>
            <td class="style1">
                <asp:TextBox ID="txtBranchCode" runat="server" Width="30%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Branch Name</td>
            <td class="style1">
                <asp:TextBox ID="txtBranchName" runat="server" Width="89%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Department</td>
            <td class="style1">
                <asp:TextBox ID="txtDepartment" runat="server" Width="89%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        
        <tr>
            <td style="width:30%">
                Designation</td>
            <td class="style1">
                <asp:TextBox ID="txtDesignation" runat="server" Width="89%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
         <tr>
            <td style="width:30%">
                Grade</td>
            <td class="style1">
                <asp:TextBox ID="txtCadre" runat="server" Width="89%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr> 

        <tr>
            <td class="style9">
                Status</td>
            <td class="style10">
            <asp:DropDownList ID="cmbStatus" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="46%" ForeColor="Black" Height="24px"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList>
                </td>
            <td class="style11">
                </td>
        </tr>
        <tr>
            <td class="style9">
                Remarks</td>
            <td class="style10">
             <asp:TextBox ID="txtRemarks" runat="server" Rows="5" 
                     TextMode="MultiLine" Width="89%" maxlength=1000></asp:TextBox>
                </td>
            <td class="style11">
                </td>
        </tr>
        <tr>
            <td class="style6">
                </td>
            <td class="style7">
                <input id="Button1" type="button" value="SAVE" class="NormalText" 
                    style="width: 21%" onclick="return RequestOnClick()" /> &nbsp;
                <input id="Button2" type="button" value="EXIT" class="NormalText" 
                    style="width: 21%" onclick="return btnExit_onclick()" /></td>
            <td class="style8">
                </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                &nbsp;</td>
            <td style="text-align:center" class="style2">
                &nbsp;</td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnValue" runat="server" />
    <asp:HiddenField ID="hdnSignId" runat="server" />
    <asp:HiddenField ID="hdnTypeId" runat="server" />
    <asp:HiddenField ID="hdnVerify" runat="server" />
    <asp:HiddenField ID="hid_dtls" runat="server" />

    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
