﻿
Partial Class Operations_viewNonITPolicies
    Inherits System.Web.UI.Page
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1093) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Non IT Policies"
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
