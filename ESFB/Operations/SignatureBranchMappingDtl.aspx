﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="SignatureBranchMappingDtl.aspx.vb" Inherits="SignatureBranchMappingDtl" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function window_onload() {
           document.getElementById("<%= cmbSearch.ClientID %>").value = 1;
           SearchOnChange();
        }
        function SearchOnChange() {
            var SearchId=document.getElementById("<%= cmbSearch.ClientID %>").value;
            Initialize();
            DivVisible(SearchId);
        }   
        function BranchCodeOnChange() {
            var BranchId=document.getElementById("<%= txtBranchCode.ClientID %>").value;
            var Data = "1Ø" + BranchId;
            ToServer(Data, 1);

        }
        function EmployeeCodeOnChange() {
            var EmpCode=document.getElementById("<%= txtEmpCode.ClientID %>").value;
            var Data = "2Ø" + EmpCode;
            ToServer(Data, 2);

        }
        function POAOnChange() {
            var POA=document.getElementById("<%= txtPOA.ClientID %>").value;
            var Data = "3Ø" + POA;
            ToServer(Data, 3);
        }

        function DivVisible(SearchId)
        {
            document.getElementById("divBranch").style.display = "none";
            document.getElementById("divEmployee").style.display = "none";
            document.getElementById("divPOA").style.display = "none";

            if (SearchId ==1)// branch
                document.getElementById("divBranch").style.display = "block";
            else if (SearchId ==2)// Employee
                document.getElementById("divEmployee").style.display = "block";
            else
                document.getElementById("divPOA").style.display = "block";

        }
        function Initialize()
        {
            document.getElementById("<%= txtBranchCode.ClientID %>").value = "";
            document.getElementById("<%= txtEmpCode.ClientID %>").value = "";
            document.getElementById("<%= txtPOA.ClientID %>").value = "";
            document.getElementById("<%= pnBranch.ClientID %>").innerHTML = "";
            document.getElementById("<%= pnEmployee.ClientID %>").innerHTML = "";
            document.getElementById("<%= pnPOA.ClientID %>").innerHTML = "";
            document.getElementById("<%= pnBranchHistory.ClientID %>").innerHTML = "";
            document.getElementById("<%= pnEmployeeHistory.ClientID %>").innerHTML = "";
            document.getElementById("<%= pnPOAHistory.ClientID %>").innerHTML = "";
        }
        function btnPOAHistory_onclick()
        {
            var POA=document.getElementById("<%= txtPOA.ClientID %>").value;
            if(POA.length>0)
            {
                if (document.getElementById("btnPOAHistory").value== "History ++")
                {
                    var ToData = "6Ø" + POA;          
                    ToServer(ToData, 6);
                }
                else
                {
                    document.getElementById("<%= hid_history_dtls.ClientID %>").value="";
                    document.getElementById("btnPOAHistory").value= "History ++"
                    document.getElementById("<%= pnPOAHistory.ClientID %>").innerHTML = "";
                }
            }

        }
        function btnHistoryEmployee_onclick()
        {
            var EmpCode=document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if(EmpCode.length>0)
            {
                if (document.getElementById("btnHistoryEmployee").value== "History ++")
                {
                    var ToData = "5Ø" + EmpCode;          
                    ToServer(ToData, 5);
                }
                else
                {
                    document.getElementById("<%= hid_history_dtls.ClientID %>").value="";
                    document.getElementById("btnHistoryEmployee").value= "History ++"
                    document.getElementById("<%= pnEmployeeHistory.ClientID %>").innerHTML = "";
                }
            }
        }
        function btnHistoryBranch_onclick()
        {
            var BrCode=document.getElementById("<%= txtBranchCode.ClientID %>").value;
            if(BrCode.length>0)
            {
                if (document.getElementById("btnHistoryBranch").value== "History ++")
                {
                    var ToData = "4Ø" + BrCode;          
                    ToServer(ToData, 4);
                }
                else
                {
                    document.getElementById("<%= hid_history_dtls.ClientID %>").value="";
                    document.getElementById("btnHistoryBranch").value= "History ++"
                    document.getElementById("<%= pnBranchHistory.ClientID %>").innerHTML = "";
                }
            }
        }

        function table_fill_Branch() {
            document.getElementById("<%= pnBranch.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:4%;text-align:center'>Employee Code</td>";
            tab += "<td style='width:10%;text-align:center'>Employee Name</td>";
            tab += "<td style='width:8%;text-align:center'>Department</td>";
            tab += "<td style='width:8%;text-align:center'>Designation</td>";
            tab += "<td style='width:4%;text-align:center'>Grade</td>";
            tab += "<td style='width:4%;text-align:center'>Date of Joining</td>";
            tab += "<td style='width:5%;text-align:center'>POA</td>";
            tab += "<td style='width:4%;text-align:center'>View Signature</td>";
//            tab += "<td style='width:7%;text-align:center'>Approval Type</td>";
//            tab += "<td style='width:4%;text-align:center'>Start Date</td>";
//            tab += "<td style='width:7%;text-align:center'>End Date</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                                   
                    tab += "<td style='width:2%;text-align:center;'>" + n + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[6] + "</td>";

                    if (col[7] != 0)
                        tab += "<td style='width:4%;text-align:center'><img id='ImgSignature' src='../Image/attchment2.png' onclick='viewSignature("+ col[7] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    else
                        tab += "<td style='width:8%;text-align:left'></td>";

//                    tab += "<td style='width:4%;text-align:left'>" + col[8] + "</td>";
//                    tab += "<td style='width:5%;text-align:left'>" + col[9] + "</td>";
//                    tab += "<td style='width:4%;text-align:left'>" + col[10] + "</td>";
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnBranch.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//
        }
        function table_fill_Branch_History() {
            document.getElementById("<%= pnBranchHistory.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var POA = "";
            var SIGN = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:4%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:10%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:5%;text-align:center'>POA</td>";
            tab += "<td style='width:0%;text-align:center;display:none;'>SignId</td>";
            tab += "<td style='width:4%;text-align:center'>View Signature</td>";
//            tab += "<td style='width:7%;text-align:center'>Approval Type</td>";
//            tab += "<td style='width:4%;text-align:center'>Start Date</td>";
//            tab += "<td style='width:7%;text-align:center'>End Date</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_history_dtls.ClientID %>").value != "") 
            {
                row = document.getElementById("<%= hid_history_dtls.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) 
                {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n;
                    tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:0%;text-align:left;display:none;'>" + col[3] + "</td>";
                   
                    if (col[3] != 0)
                        tab += "<td style='width:4%;text-align:center'><img id='ImgSignature' src='../Image/attchment2.png' onclick='viewSignature("+ col[3] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    else
                        tab += "<td style='width:8%;text-align:left'></td>";
    
//                    if (col[4] != 0)
//                        tab += "<td style='width:4%;text-align:left'>" + col[4] + "</td>";
//                    else
//                        tab += "<td style='width:4%;text-align:left'></td>";

//                    if (col[5] != "")
//                        tab += "<td style='width:4%;text-align:left'>" + col[5] + "</td>";
//                    else
//                        tab += "<td style='width:4%;text-align:left'></td>";

//                    if (col[6] != "")
//                        tab += "<td style='width:4%;text-align:left'>" + col[6] + "</td>";
//                    else
//                        tab += "<td style='width:4%;text-align:left'></td>";
              
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnBranchHistory.ClientID %>").innerHTML = tab;
            //--------------------- Clearing Data ------------------------//
        }

         function table_fill_Employee() {
            document.getElementById("<%= pnEmployee.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:10%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:4%;text-align:center' >Branch Code</td>";
            tab += "<td style='width:4%;text-align:center' >Branch Name</td>";
            tab += "<td style='width:8%;text-align:center'>Department</td>";
            tab += "<td style='width:8%;text-align:center'>Designation</td>";
            tab += "<td style='width:4%;text-align:center'>Grade</td>";
            tab += "<td style='width:4%;text-align:center'>Date of Joining</td>";
            tab += "<td style='width:5%;text-align:center'>POA</td>";
            tab += "<td style='width:4%;text-align:center'>View Signature</td>";
//            tab += "<td style='width:7%;text-align:center'>Approval Type</td>";
//            tab += "<td style='width:4%;text-align:center'>Start Date</td>";
//            tab += "<td style='width:7%;text-align:center'>End Date</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                 
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                                   
                    tab += "<td style='width:2%;text-align:center;'>" + n + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[7] + "</td>";

                    if (col[8] != 0)
                        tab += "<td style='width:4%;text-align:center'><img id='ImgSignature' src='../Image/attchment2.png' onclick='viewSignature("+ col[8] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    else
                        tab += "<td style='width:8%;text-align:left'></td>";

//                    tab += "<td style='width:4%;text-align:left'>" + col[9] + "</td>";
//                    tab += "<td style='width:5%;text-align:left'>" + col[10] + "</td>";                   
//                    tab += "<td style='width:4%;text-align:left'>" + col[11] + "</td>";
                 
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnEmployee.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//


        }
        function table_fill_Employee_History() {
            document.getElementById("<%= pnEmployeeHistory.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var POA = "";
            var SIGN = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:4%;text-align:center' >Branch Code</td>";
            tab += "<td style='width:10%;text-align:center' >Branch Name</td>";
            tab += "<td style='width:10%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:5%;text-align:center'>POA</td>";
            tab += "<td style='width:0%;text-align:center;display:none;'>SignId</td>";
            tab += "<td style='width:4%;text-align:center'>View Signature</td>";
//            tab += "<td style='width:7%;text-align:center'>Approval Type</td>";
//            tab += "<td style='width:4%;text-align:center'>Start Date</td>";
//            tab += "<td style='width:7%;text-align:center'>End Date</td>";

            tab += "</tr>";
           
            if (document.getElementById("<%= hid_history_dtls.ClientID %>").value != "") 
            {
                row = document.getElementById("<%= hid_history_dtls.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) 
                {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n;
                    tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[3] + "</td>";

                    tab += "<td style='width:0%;text-align:left;display:none;'>" + col[4] + "</td>";
                   
                    if (col[4] != 0)
                        tab += "<td style='width:4%;text-align:center'><img id='ImgSignature' src='../Image/attchment2.png' onclick='viewSignature("+ col[4] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    else
                        tab += "<td style='width:8%;text-align:left'></td>";
    
//                    if (col[5] != 0)
//                        tab += "<td style='width:4%;text-align:left'>" + col[5] + "</td>";
//                    else
//                        tab += "<td style='width:4%;text-align:left'></td>";

//                    if (col[6] != "")
//                        tab += "<td style='width:4%;text-align:left'>" + col[6] + "</td>";
//                    else
//                        tab += "<td style='width:4%;text-align:left'></td>";

//                    if (col[7] != "")
//                        tab += "<td style='width:4%;text-align:left'>" + col[7] + "</td>";
//                    else
//                        tab += "<td style='width:4%;text-align:left'></td>";
              
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
          
            document.getElementById("<%= pnEmployeeHistory.ClientID %>").innerHTML = tab;
            //--------------------- Clearing Data ------------------------//
        }

          function table_fill_POA() {
            document.getElementById("<%= pnPOA.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:4%;text-align:center' >Branch Code</td>";
            tab += "<td style='width:10%;text-align:center' >Branch Name</td>";
            tab += "<td style='width:4%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:10%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:8%;text-align:center'>Department</td>";
            tab += "<td style='width:8%;text-align:center'>Designation</td>";
            tab += "<td style='width:4%;text-align:center'>Grade</td>";
            tab += "<td style='width:4%;text-align:center'>Date of Joining</td>";
            tab += "<td style='width:5%;text-align:center'>POA</td>";
            tab += "<td style='width:4%;text-align:center'>View Signature</td>";
//            tab += "<td style='width:7%;text-align:center'>Approval Type</td>";
//            tab += "<td style='width:4%;text-align:center'>Start Date</td>";
//            tab += "<td style='width:7%;text-align:center'>End Date</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                   
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                                   
                    tab += "<td style='width:2%;text-align:center;'>" + n + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[8] + "</td>";
                    if (col[9] != 0)
                        tab += "<td style='width:4%;text-align:center'><img id='ImgSignature' src='../Image/attchment2.png' onclick='viewSignature("+ col[9] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    else
                        tab += "<td style='width:8%;text-align:left'></td>";
//                    tab += "<td style='width:4%;text-align:left'>" + col[10] + "</td>";
//                    tab += "<td style='width:5%;text-align:left'>" + col[11] + "</td>";
//                    tab += "<td style='width:4%;text-align:left'>" + col[12] + "</td>";
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnPOA.ClientID %>").innerHTML = tab;
        }

        function table_fill_POA_History() {
            document.getElementById("<%= pnPOAHistory.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var POA = "";
            var SIGN = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:4%;text-align:center' >Branch Code</td>";
            tab += "<td style='width:10%;text-align:center' >Branch Name</td>";
            tab += "<td style='width:10%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:5%;text-align:center'>Employee Name</td>";
            tab += "<td style='width:0%;text-align:center;display:none;'>SignId</td>";
            tab += "<td style='width:4%;text-align:center'>View Signature</td>";
//            tab += "<td style='width:7%;text-align:center'>Approval Type</td>";
//            tab += "<td style='width:4%;text-align:center'>Start Date</td>";
//            tab += "<td style='width:7%;text-align:center'>End Date</td>";

            tab += "</tr>";
      
            if (document.getElementById("<%= hid_history_dtls.ClientID %>").value != "") 
            {
                row = document.getElementById("<%= hid_history_dtls.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) 
                {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n;
                    tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[3] + "</td>";

                    tab += "<td style='width:0%;text-align:left;display:none;'>" + col[4] + "</td>";
                   
                    if (col[4] != 0)
                        tab += "<td style='width:4%;text-align:center'><img id='ImgSignature' src='../Image/attchment2.png' onclick='viewSignature("+ col[4] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    else
                        tab += "<td style='width:8%;text-align:left'></td>";
//    
//                    if (col[5] != 0)
//                        tab += "<td style='width:4%;text-align:left'>" + col[5] + "</td>";
//                    else
//                        tab += "<td style='width:4%;text-align:left'></td>";

//                    if (col[6] != "")
//                        tab += "<td style='width:4%;text-align:left'>" + col[6] + "</td>";
//                    else
//                        tab += "<td style='width:4%;text-align:left'></td>";

//                    if (col[7] != "")
//                        tab += "<td style='width:4%;text-align:left'>" + col[7] + "</td>";
//                    else
//                        tab += "<td style='width:4%;text-align:left'></td>";
              
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
   
            document.getElementById("<%= pnPOAHistory.ClientID %>").innerHTML = tab;
        }

        function viewSignature(SignID)
        {
       
            window.open("Reports/ShowAttachment.aspx?SignID=" + btoa(SignID) + "&RptID=10");
            return false;
        }

       
        function FromServer(arg, context) {
            switch (context) {

                case 1:// branch
                    {   
                        var Dtl=arg.split("~");
                        document.getElementById("<%= txtBranchName.ClientID %>").value=Dtl[0];
                        document.getElementById("<%= txtDistrict.ClientID %>").value=Dtl[1];
                        document.getElementById("<%= txtState.ClientID %>").value=Dtl[2];
                        document.getElementById("<%= hid_dtls.ClientID %>").value=Dtl[3];
                        table_fill_Branch();
                        break;
                    }
                case 2:
                    {
                        document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                        table_fill_Employee();
                        break;
                    }                            
                case 3:
                    {
                        document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                        table_fill_POA();
                        break;
                    }
                case 4:
                    {
                        if(arg=="")
                        {
                             alert("History not found");
                             document.getElementById("<%= hid_history_dtls.ClientID %>").value="";
                        }
                        else
                        {   
                            document.getElementById("<%= hid_history_dtls.ClientID %>").value=arg;
                            document.getElementById("btnHistoryBranch").value= "History --";
                            table_fill_Branch_History();
                        }
                        break;
                    }
                case 5:
                    {
                        if(arg=="")
                        {
                             alert("History not found");
                             document.getElementById("<%= hid_history_dtls.ClientID %>").value="";
                        }
                        else
                        {   
                            document.getElementById("<%= hid_history_dtls.ClientID %>").value=arg;
                            document.getElementById("btnHistoryEmployee").value= "History --";
                            table_fill_Employee_History();
                        }
                        break;
                    }
                case 6:
                    {
                        if(arg=="")
                        {
                             alert("History not found");
                             document.getElementById("<%= hid_history_dtls.ClientID %>").value="";
                        }
                        else
                        {   
                            document.getElementById("<%= hid_history_dtls.ClientID %>").value=arg;
                            document.getElementById("btnPOAHistory").value= "History --";
                            table_fill_POA_History();
                        }
                        break;
                    }
            }
        }
       
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />
  <table class="style1" style="width: 80%; margin: 0px auto;">
        <tr id="SearchType">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Search Type
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbSearch" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="1"> BRANCH</asp:ListItem>
                    <asp:ListItem Value="2"> EMPLOYEE</asp:ListItem>
                    <asp:ListItem Value="3"> POA</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        </table>
&nbsp; &nbsp;&nbsp; &nbsp;
    <div id ="divBranch">
    <table class="style1" style="width: 80%; margin: 0px auto;">

         <tr>
            <td style="width:25%"></td>
             <td style="width: 12%; text-align: left;">
                Branch Code
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<input id="txtBranchCode" type="text" class="NormalText" onkeypress='return NumericCheck(event)' style="width: 50%" runat="server" /></td>
        </tr>
         <tr>
            <td style="width:25%"></td>
             <td style="width: 12%; text-align: left;">
                Branch Name
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<input id="txtBranchName" type="text" class="NormalText" style="width: 50%" runat="server" /></td>
        </tr>
        <tr>
            <td style="width:25%"></td>
             <td style="width: 12%; text-align: left;">
                District
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<input id="txtDistrict" type="text" class="NormalText" style="width: 50%" runat="server" /></td>
        </tr>
        <tr>
            <td style="width:25%"></td>
             <td style="width: 12%; text-align: left;">
                State
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<input id="txtState" type="text" class="NormalText" style="width: 50%" runat="server" /></td>
        </tr>
    </table> 

        <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnBranch" runat="server">
            </asp:Panel></td>
        </tr>
        <tr>
            <td style="text-align:left;" colspan="3">
                <br />
                <input id="btnHistoryBranch" style="font-family: cambria; cursor: pointer; width: 5%;"
                    type="button" value="History ++" onclick="return btnHistoryBranch_onclick()" />

            </td>
        </tr>
        <tr> 
            <td colspan="3"><asp:Panel ID="pnBranchHistory" runat="server">
            </asp:Panel></td>
        </tr>
        </table> 
    </div>
    <div id = "divEmployee">
    
        <table class="style1" style="width: 80%; margin: 0px auto;">
             <tr>
                <td style="width:25%"></td>
                 <td style="width: 12%; text-align: left;">
                    Employee Code
                </td>
                <td style="width: 63%">
                    &nbsp; &nbsp;<input id="txtEmpCode" type="text" class="NormalText" onkeypress='return NumericCheck(event)' style="width: 50%" runat="server" /></td>
            </tr>


        </table>

        <table class="style1" style="width:100%">
            <tr> 
                <td colspan="3"><asp:Panel ID="pnEmployee" runat="server">
                </asp:Panel></td>
            </tr>
            <tr>
                <td style="text-align:left;" colspan="3">
                    <br />
                    <input id="btnHistoryEmployee" style="font-family: cambria; cursor: pointer; width: 5%;"
                        type="button" value="History ++" onclick="return btnHistoryEmployee_onclick()" />
                </td>
            </tr>
            <tr> 
                <td colspan="3"><asp:Panel ID="pnEmployeeHistory" runat="server">
                </asp:Panel></td>
            </tr>
        </table> 
    </div>
    <div id ="divPOA">
        <table class="style1" style="width: 80%; margin: 0px auto;">
             <tr>
                <td style="width:25%"></td>
                 <td style="width: 12%; text-align: left;">
                    POA
                </td>
                <td style="width: 63%">
                    &nbsp; &nbsp;<input id="txtPOA" type="text" class="NormalText" style="width: 50%" runat="server" onkeypress='return AlphaNumericCheck(event)' /></td>
            </tr>
        </table> 
        <table class="style1" style="width:100%">
            <tr> 
                <td colspan="3"><asp:Panel ID="pnPOA" runat="server">
                </asp:Panel></td>
            </tr>
            <tr>
                <td style="text-align:left;" colspan="3">
                    <br />
                    <input id="btnPOAHistory" style="font-family: cambria; cursor: pointer; width: 5%;" type="button" value="History ++" onclick="return btnPOAHistory_onclick()" />
                </td>
            </tr>
            <tr> 
                <td colspan="3"><asp:Panel ID="pnPOAHistory" runat="server">
                </asp:Panel></td>
            </tr>
        </table> 
    </div>

    <table class="style1" style="width:100%">
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <asp:HiddenField ID="hid_history_dtls" runat="server" />
                <br />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
            </td>
        </tr>
    </table>    
<br /><br />
</asp:Content>


