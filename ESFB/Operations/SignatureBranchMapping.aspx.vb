﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class SignatureBranchMapping
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1198) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Me.Master.subtitle = "Signature - Branch Mapping"
            txtBranchCode.Attributes.Add("onblur", "return BranchCodeOnBlur()")

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CDbl(Data(0)) = 1 Then
            Dim BranchID As Integer = CInt(Data(1))
            Dim dataval As String = CStr(Data(2))
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@TypeID", SqlDbType.Int)
                Params(0).Value = 1
                Params(1) = New SqlParameter("@userID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(2).Value = BranchID
                Params(3) = New SqlParameter("@MappDtl", SqlDbType.VarChar)
                Params(3).Value = dataval.Substring(1)
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_SIGN_BRANCH_MAPPING", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString
        ElseIf CDbl(Data(0)) = 2 Then
            Dim BrID As Integer = CInt(Data(1))
            Dim Sdat, Tdat As String
            DT = DB.ExecuteDataSet("select A.branch_id,branch_name, District_name , state_name ,D.emp_code,emp_name, E.Department_name,F.Designation_name,G.Cadre_name,Date_of_join, POA_No,case when H.sign_id is null then '' else H.sign_id end  as Signature,I.Appr_type, isnull(Start_Dt,''), isnull(End_Dt,'') from branch_master A " +
                " inner join District_master B on A.district_id = B.district_id " +
                " inner join State_master C on A.State_id = C.State_id   " +
                " inner join emp_master D on A.branch_id = D.Branch_Id   " +
                " inner join Department_master E on D.Department_id = E.Department_id " +
                " inner join Designation_master F on D.Designation_id = F.Designation_id " +
                " inner join Cadre_master G on D.Cadre_id = G.Cadre_id " +
                " left join Sign_master H on D.emp_code = H.Emp_code  and isnull(H.checker_status,0)  in (1) " +
                " left join Sign_Branch_Mapping I on H.emp_code = I.Emp_code and  isnull(I.checker_status,0)  in (1) and isnull(I.status_id,0) in (1) " +
                " where A.Branch_id = " + BrID.ToString() + " And D.Status_id = 1").Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn = DT.Rows(0)(1).ToString().ToUpper() + "~" + DT.Rows(0)(2).ToString().ToUpper() + "~" + DT.Rows(0)(3).ToString().ToUpper() + "~"
                For n As Integer = 0 To DT.Rows.Count - 1
                    If IsDBNull(DT.Rows(n)(13)) Then
                        Sdat = ""
                    Else
                        Sdat = CDate(DT.Rows(n)(13)).ToString("dd-MMM-yyyy").ToString()
                    End If
                    If IsDBNull(DT.Rows(n)(14)) Then
                        Tdat = ""
                    Else
                        Tdat = CDate(DT.Rows(n)(14)).ToString("dd-MMM-yyyy").ToString()
                    End If
                    CallBackReturn += "¥" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + CDate(DT.Rows(n)(9)).ToString("dd-MMM-yyyy") + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + DT.Rows(n)(11).ToString().ToUpper() + "µ" + DT.Rows(n)(12).ToString().ToUpper() + "µ" + DT.Rows(n)(13).ToString().ToUpper() + "µ" + DT.Rows(n)(14).ToString().ToUpper()
                Next
            Else
                CallBackReturn = ""
            End If
        ElseIf CDbl(Data(0)) = 3 Then
            Dim BrID As Integer = CInt(Data(1))
            Dim Sdat, Tdat As String

            DT = DB.ExecuteDataSet("select A.branch_id,branch_name, District_name , state_name ,D.emp_code,emp_name, E.Department_name,F.Designation_name,G.Cadre_name,Date_of_join, POA_No,case when H.sign_id is null then '' else H.sign_id end  as Signature ,case when Appr_Type = 1 then 'Permanant' when Appr_Type = 2 then 'Temporary' else 'Un-Map' end , Start_Dt, End_Dt " +
            " from branch_master A  inner join District_master B on A.district_id = B.district_id  inner join State_master C on A.State_id = C.State_id   " +
            " inner join Sign_Branch_Mapping I on I.branch_id = A.branch_id and isnull(I.checker_status,0)  in (1) and I.status_id = 0 " +
            " inner join emp_master D on I.emp_code = d.emp_code    inner join Department_master E on D.Department_id = E.Department_id   " +
            " inner join Designation_master F on D.Designation_id = F.Designation_id  inner join Cadre_master G on D.Cadre_id = G.Cadre_id   " +
            " inner join Sign_master H on I.emp_code = H.Emp_code  and isnull(H.checker_status,0)  in (1)   " +
            " where A.Branch_id = " + BrID.ToString() + " And D.Status_id = 1").Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn = DT.Rows(0)(1).ToString().ToUpper() + "~" + DT.Rows(0)(2).ToString().ToUpper() + "~" + DT.Rows(0)(3).ToString().ToUpper() + "~"
                For n As Integer = 0 To DT.Rows.Count - 1
                    If IsDBNull(DT.Rows(n)(13)) Then
                        Sdat = ""
                    Else
                        Sdat = CDate(DT.Rows(n)(13)).ToString("dd-MMM-yyyy").ToString()
                    End If
                    If IsDBNull(DT.Rows(n)(14)) Then
                        Tdat = ""
                    Else
                        Tdat = CDate(DT.Rows(n)(14)).ToString("dd-MMM-yyyy").ToString()
                    End If
                    CallBackReturn += "¥" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + DT.Rows(n)(11).ToString().ToUpper() + "µ" + DT.Rows(n)(12).ToString().ToUpper() + "µ" + Sdat + "µ" + Tdat
                Next
            Else
                CallBackReturn = ""
            End If
        End If



    End Sub
#End Region

End Class
