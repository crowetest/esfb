﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="SignatureUploading.aspx.vb" Inherits="SignatureUploading" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            font-size: 11px;
            color: #000000;
            border: 1px solid #7f9db9;
            }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
        .style1
        {
            width: 46%;
        }
        .style2
        {
            width: 74%;
        }
        .style3
        {
            width: 30%;
            height: 16px;
        }
        .style4
        {
            width: 46%;
            height: 16px;
        }
        .style5
        {
            width: 74%;
            height: 16px;
        }
        .style6
        {
            width: 30%;
            height: 30px;
        }
        .style7
        {
            width: 46%;
            height: 30px;
        }
        .style8
        {
            width: 74%;
            height: 30px;
        }
        </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
        var FileSelected = 0;
function window_onload()
{
    
    if (document.getElementById("<%= hdnTypeId.ClientID %>").value == 2)
    {
        ToServer("2Ø" ,2);
    }

}
     function table_fill_Branch() {
  
            document.getElementById("<%= pnBranch.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:4%;text-align:center'>Employee Code</td>";
            tab += "<td style='width:10%;text-align:center'>Employee Name</td>";
            tab += "<td style='width:8%;text-align:center'>Branch Code</td>";
            tab += "<td style='width:8%;text-align:center'>Branch Name</td>";
            tab += "<td style='width:5%;text-align:center'>POA</td>";
            tab += "<td style='width:0%;text-align:center;display:none;'>SignId</td>";
            tab += "<td style='width:4%;text-align:center'>View Signature</td>";
            tab += "<td style='width:4%;text-align:center'>Edit</td>";
            tab += "<td style='width:4%;text-align:center'>Delete</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                                   
                    tab += "<td style='width:2%;text-align:center;'>" + n + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:4%;text-align:left;display:none;'>" + col[5] + "</td>";
                    if (col[5] != 0)
                        tab += "<td style='width:4%;text-align:center'><img id='ImgSignature1' src='../Image/attchment2.png' onclick='viewSignature("+ col[5] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    else
                        tab += "<td style='width:8%;text-align:left'></td>";
                    tab += "<td  style='width:4%;text-align:center' ><img src='../image/edit1.png' title='Edit'  style='height:18px;width:18px;cursor:pointer'  onclick=UploadModify("+ col[5] +") /></td>";
                    tab += "<td  style='width:4%;text-align:center' ><img src='../image/delete.png' title='Delete'  style='height:18px;width:18px;cursor:pointer'  onclick=UploadDelete(" + col[5] + ") /></td>";
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnBranch.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//
        }
        
        function viewSignature(SignID)
        {
        
            window.open("Reports/ShowAttachment.aspx?SignID=" + btoa(SignID) + "&RptID=10");
            return false;
        }
        function UploadModify(SignID)
        {
         
            ToServer("3Ø" + SignID,3);

        }
        function UploadDelete(SignID)
        {
      
            ToServer("3Ø" + SignID,3);
            if(confirm("Do you want to delete this upload?")==1) 
                ToServer("4Ø" + SignID,4);
            else
                alert("Deletion aborted by user");
        }
    function FromServer(arg, context) 
    {
   
       if ((context == 1) || (context == 3))
       {
            if(arg != "")
            {
                var Data = arg.split("|");

                document.getElementById("<%= txtEmpCode.ClientID %>").value = Data[0] ;
                document.getElementById("<%= txtEmpName.ClientID %>").value = Data[1] ;
                document.getElementById("<%= txtBranchCode.ClientID %>").value = Data[2] ;
                document.getElementById("<%= txtBranchName.ClientID %>").value = Data[3] ;
                document.getElementById("<%= txtDepartment.ClientID %>").value = Data[4] ;
                document.getElementById("<%= txtDesignation.ClientID %>").value = Data[5] ;
                document.getElementById("<%= txtCadre.ClientID %>").value = Data[6] ;
                document.getElementById("<%= txtDOJ.ClientID %>").value = Data[7] ;
                if (context == 1){ 
                    document.getElementById("<%= hdnSignId.ClientID %>").value = Data[8] ;
                    document.getElementById("<%= hdnVerify.ClientID %>").value = Data[9] ;
                    document.getElementById("<%= txtPOA.ClientID %>").value = "" ;
                    document.getElementById("<%= fup1.ClientID %>").value = "";
                }
                else {
                    document.getElementById("<%= txtPOA.ClientID %>").value = Data[8] ;
                    document.getElementById("<%= hdnSignId.ClientID %>").value = Data[9] ;
                    document.getElementById("ImgSignature").setAttribute('src',Data[10]); 
                    FileSelected = 1;
                }

           }

           else
           {
                alert("Invalid Employee Code");
                document.getElementById("<%= txtEmpName.ClientID %>").value = "" ;
                document.getElementById("<%= txtBranchCode.ClientID %>").value = "" ;
                document.getElementById("<%= txtBranchName.ClientID %>").value = "" ;
                document.getElementById("<%= txtDepartment.ClientID %>").value = "" ;
                document.getElementById("<%= txtDesignation.ClientID %>").value = "" ;
                document.getElementById("<%= txtCadre.ClientID %>").value = "" ;
                document.getElementById("<%= txtDOJ.ClientID %>").value = "" ;
                document.getElementById("<%= txtPOA.ClientID %>").value = "" ;
                document.getElementById("<%= hdnSignId.ClientID %>").value = 0 ;
                document.getElementById("<%= fup1.ClientID %>").value = "";
                var preview = document.querySelector('img[id="ImgSignature"]');
                preview.src = "";
           }

         
        }
        else if (context == 2)
        {
            if (arg!="")
            {
                document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                table_fill_Branch();
            }
            else
            {
                alert("No pending entry to manipulate");
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        }
        else if (context == 4)
        {
            if(arg != "")
            {
                var Data = arg.split("|");
                alert(Data[1]);
                if (Data[0] ==0)
                    ToServer("2Ø" ,2);
            }
        }   
    }
    function btnExit_onclick() {
   
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

    function EmpCodeOnBlur() {  
  
        if (document.getElementById("<%= txtEmpCode.ClientID %>").value != "") 
        {
            ToServer("1Ø" + document.getElementById("<%= txtEmpCode.ClientID %>").value,1);

        }
    }
    function RequestOnClick() {  
     
        if (document.getElementById("<%= hdnTypeId.ClientID %>").value == 1) 
        {
            if (document.getElementById("<%= hdnSignId.ClientID %>").value != "") 
            {
                alert("Signature already uploaded");
                return false;
            }
        }
        else
        {
            if (document.getElementById("<%= hdnSignId.ClientID %>").value == "") 
            {
                alert("Signature already uploaded");
                return false;
            }        
        }
        if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "") 
        {
            alert("Enter Employee Code");
            document.getElementById("<%= txtEmpCode.ClientID %>").focus();
            return false;
        }
      
//        else if (document.getElementById("<%= txtPOA.ClientID %>").value == "") 
//        {
//            alert("Enter POA");
//            document.getElementById("<%= txtPOA.ClientID %>").focus();
//            return false;
//        }
        else if (FileSelected == 0) 
        {
            alert("Select signature to upload");
            document.getElementById("<%= fup1.ClientID %>").focus();
            return false;
        }
//        var preview = document.querySelector('img[id="ImgSignature"]');
//        var reader  = new FileReader();
//        reader.addEventListener("load", function () {
//        preview.src = reader.result;
//        }, false);

//         if (preview.src=="")
//        {
//            alert("Select signature to upload ");
//            document.getElementById("<%= fup1.ClientID %>").focus();
//            return false;
//        }

//        document.getElementById("<%= hdnValue.ClientID %>").value =  "2Ø" + document.getElementById("<%= txtEmpCode.ClientID %>").value + "Ø" + document.getElementById("<%= txtPOA.ClientID %>").value + "Ø" + preview.src + "Ø" + document.getElementById("<%= hdnSignId.ClientID %>").value  ;

        document.getElementById("<%= hdnValue.ClientID %>").value =  "2Ø" + document.getElementById("<%= txtEmpCode.ClientID %>").value + "Ø" + document.getElementById("<%= txtPOA.ClientID %>").value + "Ø" + '' + "Ø" + document.getElementById("<%= hdnSignId.ClientID %>").value  ;

    }

   
    function validate_fileupload()
    {
        var fileName = document.getElementById("<%= fup1.ClientID %>").value;
        var allowed_extensions = ["jpg","png","gif"];
        // var allowed_extensions = ["jpg","png","gif", "xls","xlsx","doc","docx","txt"];

        var file_extension = fileName.split('.').pop().toLowerCase(); 
        // split function will split the filename by dot(.), and pop function will pop the last element from the array which will give you the extension as well. If there will be no extension then it will return the filename.
        var a = allowed_extensions.indexOf(file_extension);

        if (a==-1)
        {
            alert("Select only .jpg, .png, .gif files.");
            document.getElementById("<%= fup1.ClientID %>").value = "";
            var preview = document.querySelector('img[id="ImgSignature"]');
            preview.src = "";
            FileSelected = 0;
            return false;

        }
        else
        {
            var file    = document.querySelector('input[type=file]').files[0];
            var size = Math.abs(file.size);

            if (((size/1000)>20) || ((size/1000)<10))
            {
                alert("File size should be between 10 KB to 20 KB");
                document.getElementById("<%= fup1.ClientID %>").value = "";
                var preview = document.querySelector('img[id="ImgSignature"]');
                preview.src = "";
                FileSelected = 0;
                return false;
            }
        }
        FileSelected = 1;
        return true;

    }
    function validate_new_fileupload()
    {
        var fileName = document.getElementById("<%= fup1.ClientID %>").value;
        var allowed_extensions = ["jpg","png","gif"];
      
        var file_extension = fileName.split('.').pop().toLowerCase(); 
        var a = allowed_extensions.indexOf(file_extension);

        if (a==-1)
        {
            alert("Select only .jpg, .png, .gif files.");
            document.getElementById("<%= fup1.ClientID %>").value = "";
            FileSelected = 0;
            return false;

        }
       
        FileSelected = 1;
        return true;

    }
    function previewFile() {
        var preview = document.querySelector('img[id="ImgSignature"]');
        var file    = document.querySelector('input[type=file]').files[0];
    
        var reader  = new FileReader();

        reader.addEventListener("load", function () {
        preview.src = reader.result;
        }, false);

        if (file) {
        reader.readAsDataURL(file);
        }

        
    }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">

        </script>
    </head>
    </html>
    
    <br />
    <br />
    <br />
    <br />
        <div id ="div"/>
         <table class="style1" style="width:100%">
            <tr> 
                <td colspan="3"><asp:Panel ID="pnBranch" runat="server">
                </asp:Panel></td>
            </tr>
        </table> 
        <div />

    <br />
   
       <table align="center" style="width: 60%; margin:0px auto;">
      
        <tr>
            <td style="width:30%">
                Employee Code</td>
            <td class="style1">
                <asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="NormalTextBox" onkeypress='return NumericCheck(event)' MaxLength="10"></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Employee Name</td>
            <td class="style1">
            <asp:TextBox ID="txtEmpName" runat="server" Width="89%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
       <tr>
            <td style="width:30%">
                Branch Code</td>
            <td class="style1">
                <asp:TextBox ID="txtBranchCode" runat="server" Width="30%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Branch Name</td>
            <td class="style1">
                <asp:TextBox ID="txtBranchName" runat="server" Width="89%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%">
                Department</td>
            <td class="style1">
                <asp:TextBox ID="txtDepartment" runat="server" Width="89%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        
        <tr>
            <td style="width:30%">
                Designation</td>
            <td class="style1">
                <asp:TextBox ID="txtDesignation" runat="server" Width="89%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
         <tr>
            <td style="width:30%">
                Grade</td>
            <td class="style1">
                <asp:TextBox ID="txtCadre" runat="server" Width="89%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr> 
        <tr>
            <td style="width:30%">
                Date of Joining</td>
            <td class="style1">
                <asp:TextBox ID="txtDOJ" runat="server" Width="89%" class="ReadOnlyTextBox" ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>    
          
        <tr>
            <td style="width:30%">
                POA</td>
            <td class="style1">
                <asp:TextBox ID="txtPOA" runat="server" Width="89%" class="NormalTextBox" MaxLength="15" onkeypress='return AlphaNumericCheck(event)' ></asp:TextBox>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>  
               <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style4">
            
<%--           <a href="../Help/Signature%20Module%20Format.docx" style='cursor:pointer; font-size:10pt;' target='_blank'>Download Format</a>
--%>            </td>
            <td class="style5">
                &nbsp;</td>
        </tr> 

       <tr>
            <td class="style3">
                Upload Format</td>
            <td class="style4">
            
    <input id="fup1" type="file"  runat="server" accept=".doc,.docx"
                style="font-family: Cambria; font-size: 10.5pt" onchange = 'validate_new_fileupload();'/>

        <%--    <input id="fup1" type="file" runat="server" accept=".jpg,.png,.gif"
                style="font-family: Cambria; font-size: 10.5pt"  onchange = 'validate_fileupload(); previewFile();'/>&nbsp;    
      --%>                         
                    <br/></td>
            <td class="style5">
      &nbsp;<br/>      
                     
            </td>
        </tr>          
     <%--   <tr>
            <td style="width:30%">
                &nbsp;</td>
            <td class="style1">
      <img id="ImgSignature" src="" alt="Image preview..."></td>
            <td class="style2">
                &nbsp;</td>
        </tr>--%>

        <tr>
            <td class="style6">
                </td>
            <td class="style7">
                <asp:Button ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="21%" /> &nbsp;
                <input id="Button2" type="button" value="EXIT" class="NormalText" 
                    style="width: 21%" onclick="return btnExit_onclick()" /></td>
            <td class="style8">
                </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                &nbsp;</td>
            <td style="text-align:center" class="style2">
                &nbsp;</td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnValue" runat="server" />
    <asp:HiddenField ID="hdnSignId" runat="server" />
    <asp:HiddenField ID="hdnTypeId" runat="server" />
    <asp:HiddenField ID="hdnVerify" runat="server" />
    <asp:HiddenField ID="hid_dtls" runat="server" />

    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
