﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="SignatureBranchMappingModify.aspx.vb" Inherits="SignatureBranchMappingModify" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">   
            var today = new Date(),
                day = today.getDate(),
                month = today.getMonth()+1, //January is 0
                year = today.getFullYear();
                if(day<10){
                        day='0'+day
                    } 
                if(month<10){
                    month='0'+month
                }
                today = year + '-' + month + '-' + day;
  
          function table_fill() {

            document.getElementById("<%= pnBranch.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:4%;text-align:center' >Branch Code</td>";
            tab += "<td style='width:10%;text-align:center' >Branch Name</td>";
            tab += "<td style='width:4%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:10%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:8%;text-align:center'>Department</td>";
            tab += "<td style='width:8%;text-align:center'>Designation</td>";
            tab += "<td style='width:4%;text-align:center'>Grade</td>";
            tab += "<td style='width:5%;text-align:center'>Date of Joining</td>";
            tab += "<td style='width:5%;text-align:center'>POA</td>";
            tab += "<td style='width:4%;text-align:center'>View Signature</td>";
            tab += "<td style='width:4%;text-align:center'>Approval Type</td>";
            tab += "<td style='width:4%;text-align:center'>Start Date</td>";
            tab += "<td style='width:4%;text-align:center'>End Date</td>";
            tab += "<td style='width:4%;text-align:center'>Remarks</td>";
            tab += "<td style='width:4%;text-align:center;display:none;'>Map_Id</td>";
            tab += "<td style='width:4%;text-align:center'>Delete</td>";



            tab += "</tr>";
                      

            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
            {
                     

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                for (n = 1; n <= row.length - 1; n++) 
                {
                          

                    col = row[n].split("µ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                                   
                    tab += "<td style='width:2%;text-align:center;'>" + n + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[8] + "</td>";
                   
                    if (col[9] != 0)
                    {
                            

                        tab += "<td style='width:4%;text-align:center'><img id='ImgSignature' src='../Image/attchment2.png' onclick='viewSignature("+ col[9] +")' title='View signature'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                        var select = "<select id='cmb" + n + "' class='NormalText' name='cmb" + n + "' style='width:110px' onchange='block_unblock_dtp("+ n +")'   >";
                     
                         if ((col[10] == 1))
                            select += "<option value='1' selected=true>PERMENANT  </option>";
                        else
                            select += "<option value='1'>PERMENANT  </option>";
                        if (col[10] == 2)
                            select += "<option value='2' selected=true>TEMPORARY  </option>";
                        else
                            select += "<option value='2'>TEMPORARY  </option>";
                        if (col[10] == 3)
                            select += "<option value='3' selected=true>UN-MAP  </option>";
                        else
                            select += "<option value='3'>UN-MAP  </option>";

                       
                        tab += "<td style='width:4%;text-align:left '>" + select + "</td>";
                     
                      
                        var dd;
                        var mm;
                        var yyyy;
                        var CreateDate = new Date(col[11]); 
                        if ((col[11] != "1/1/1900 12:00:00 AM") && (col[10] == 2))
                        {
                              

                            var CreateDate = new Date(col[11]); 
                          
                            dd = CreateDate.getDate(); if (dd<10) dd = "0"+dd;
                            
                            mm = CreateDate.getMonth()+1; if (mm<10) mm = "0"+mm;
                          
                            yyyy = CreateDate.getFullYear(); 
                           
                            CreateDate = yyyy+'-'+mm+'-'+dd;

                            var FromDate = "<input type = 'date' ; style='width: 110px' id='txtFromDate" + n + "' name='txtFromDate" + n + "' value ='"+ CreateDate + "' min ='" + today +"' style='width:99%; float:left;' maxlength='300' ; />";
                        }
                        else
                            var FromDate = "<input type = 'date'; style='width: 110px' id='txtFromDate" + n + "' name='txtFromDate" + n + "' min ='" + today +"' style='width:99%; float:left;' maxlength='300' ; disabled/>";

                        tab += "<td style='width:4%;text-align:left'>" + FromDate + "</td>";

                                  

                        if ((col[12] != "1/1/1900 12:00:00 AM") && (col[10] == 2))
                        {
                            var CreateDate1 = new Date(col[12]); 
                            dd = CreateDate1.getDate(); if (dd<10) dd = "0"+dd;
                            mm = CreateDate1.getMonth()+1; if (mm<10) mm = "0"+mm;
                            yyyy = CreateDate1.getFullYear(); 
                            CreateDate1 = yyyy+'-'+mm+'-'+dd;

                            var ToDate = "<input type = 'date'; style='width: 110px' id='txtToDate" + n + "' name='txtToDate" + n + "' value ='"+ CreateDate1 + "' min ='" + today +"' style='width:99%; float:left;' maxlength='300' ; />";
                        }
                        else
                        {
                            var ToDate = "<input type = 'date'; style='width: 110px' id='txtToDate" + n + "' name='txtToDate" + n + "' min ='" + today +"' style='width:99%; float:left;' maxlength='300' ; disabled ; />";
                        }
                        tab += "<td style='width:4%;text-align:left'>" + ToDate + "</td>";

                                 

                        var txtRema = "<input id='txtRemarks" +  n + "' name='txtRemarks" +  n + "' value='" + col[13] +"' type='Text' style='width:99%; style='width: 180px' class='NormalText'/>";
                        tab += "<td style='width:10%;text-align:left;'>" + txtRema + "</td>";
                        tab += "<td style='width:4%;text-align:left;display:none;'>" + col[14] + "</td>";
                        tab += "<td  style='width:4%;text-align:center' ><img src='../image/delete.png' title='Delete'  style='height:18px;width:18px;cursor:pointer'  onclick=UploadDelete(" + col[14] + ") /></td>";

                    }
                    else
                    {
                        tab += "<td style='width:4%;text-align:left'></td>";
                        tab += "<td style='width:4%;text-align:left'></td>";
                        tab += "<td style='width:4%;text-align:left'></td>";
                        tab += "<td style='width:4%;text-align:left'></td>";
                        tab += "<td style='width:4%;text-align:left;display:none;'></td>";
                        tab += "<td style='width:4%;text-align:left'></td>";

                    }
                    tab += "</tr>";
                }

            }
            else
            {
                alert("No pending entry to manipulate");
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }    
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnBranch.ClientID %>").innerHTML = tab;
        }
        function block_unblock_dtp(i) {
           
            if (document.getElementById("cmb"+i).value == 2)
            {
                document.getElementById("txtFromDate"+i).disabled = false;
                document.getElementById("txtToDate"+i).disabled = false;
            }
            else
            {
                document.getElementById("txtFromDate"+i).disabled = true;
                document.getElementById("txtToDate"+i).disabled = true;                
            }
        }
        function viewSignature(SignID)
        {
       
            window.open("Reports/ShowAttachment.aspx?SignID=" + btoa(SignID) + "&RptID=10");
            return false;
        }

        function UploadDelete(MapID)
        {
            if(confirm("Do you want to delete this branch mapping?")==1) 
                ToServer("2Ø" + MapID,2);
            else
                alert("Deletion aborted by user");
        }

        function UpdateValue() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("Nothing to save");
                return false;
            }
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 1; n <= row.length - 1; n++) 
            {
                col = row[n].split("µ");
                if (col[7] != 0)
                {
                    if (document.getElementById("cmb" + n).value == 2)
                    { 
                        if (document.getElementById("txtFromDate" + n).value == "") 
                        {
                            alert("Select Start Date");
                            document.getElementById("txtFromDate"+n).focus();
                            return false;
                        }
                        else if (document.getElementById("txtToDate" + n).value == "")
                        {
                            alert("Select End Date");
                            document.getElementById("txtToDate"+n).focus();
                            return false;
                        }

                        var todate = document.getElementById("txtToDate" + n).value;
                        var fromdate = document.getElementById("txtFromDate" + n).value;

                        if( (new Date(fromdate).getTime() > new Date(todate).getTime()))
                        {
                            alert("Start date should be less than End date");
                            document.getElementById("txtFromDate"+n).focus();
                            return false;
                        }
                    }

                    if ((document.getElementById("cmb" + n).value == 1) || (document.getElementById("cmb" + n).value == 2) || (document.getElementById("cmb" + n).value == 3))
                        document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[14] + "µ" + document.getElementById("cmb" + n).value + "µ" +document.getElementById("txtFromDate" + n).value + "µ" + document.getElementById("txtToDate" + n).value  + "µ" + document.getElementById("txtRemarks" + n).value ;

                }
            }
            return true;
        }
        function FromServer(arg, context) {
            if ((context == 1) || (context == 2))
            {
                if(arg != "")
                {
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] ==0) window.open("SignatureBranchMappingModify.aspx", "_self");
                       
                }
            }
            else
            {
                document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                table_fill();
                
            }

        }
        function btnApprove_onclick() {
            var ret = UpdateValue();
            if (ret == true) 
            {
                if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                    alert("Nothing to save");
                    return false;
                }
                var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
                var Data = "1Ø" + strempcode;
                ToServer(Data, 1);
            }
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnBranch" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

