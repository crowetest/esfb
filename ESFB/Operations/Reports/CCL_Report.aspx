﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="CCL_Report.aspx.vb" Inherits="CCL_Report"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css" />
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <tr>
        <td style="width:15%;">
            Branch
        </td>
        <td  style="width:85%;">
            <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" Width="75.25%" ForeColor="Black" >
            </asp:DropDownList>
            <br />
        </td>
    </tr>
    <tr>
        <td style="width:15%;">
            Center
        </td>
        <td  style="width:85%;">
            <asp:DropDownList ID="cmbCenter"  class="NormalText" runat="server" Font-Names="Cambria" Width="75.25%" ForeColor="Black">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="width:15%;">
            Date From
        </td>
        <td  style="width:85%;">
            <asp:TextBox ID="txt_From_Dt" class="NormalText" runat="server"  Width="75%" onkeypress="return false" ></asp:TextBox>
            <asp:CalendarExtender ID="txt_From_Dt_CalendarExtender" runat="server"  Enabled="True" TargetControlID="txt_From_Dt" Format="dd MMM yyyy">
            </asp:CalendarExtender>
        </td>
    </tr>
    <tr>
        <td style="width:15%;">
            Date To
        </td>
        <td  style="width:85%;">
            <asp:TextBox ID="txt_To_Dt" class="NormalText" runat="server"  Width="75%" onkeypress="return false" ></asp:TextBox>
            <asp:CalendarExtender ID="txt_To_Dt_CalendarExtender" runat="server"  Enabled="True" TargetControlID="txt_To_Dt" Format="dd MMM yyyy">
            </asp:CalendarExtender>
        </td>
    </tr>
    <tr>
        <td style="width:15%;">
            Status
        </td>
        <td  style="width:85%;">
            <asp:DropDownList ID="cmbStatus"  class="NormalText" runat="server" Font-Names="Cambria" Width="75.25%" ForeColor="Black">
                <asp:ListItem Value="-1">-----SELECT-----</asp:ListItem>
                <asp:ListItem Value="1">Applied</asp:ListItem>
                <asp:ListItem Value="2">Not Applied</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
             
              <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;
                <input id="cmd_Export_Excel" style="font-family: Cambria; cursor: pointer; width: 87px;" 
                type="button" value="VIEW EXCEL" onclick="return btnExcelView_onclick()" />&nbsp;&nbsp;
                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 10%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
          
      
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
               
                 <asp:HiddenField ID="hdnValue" runat="server" />
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
     window.onload = function () {
            var today = new Date();
            today = today.toShortFormat();
            document.getElementById("<%= txt_From_Dt.ClientID %>").value = today;
            document.getElementById("<%= txt_To_Dt.ClientID %>").value = today;
        }
        Date.prototype.toShortFormat = function () {
            var month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var day = this.getDate();
            var month_index = this.getMonth();
            var year = this.getFullYear();
            return "" + day + " " + month_names[month_index] + " " + year;
        }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }
    function BranchOnChange(){
        var branchCode = document.getElementById("<%= cmbBranch.ClientID %>").value;
        if(branchCode == -1){
            document.getElementById("<%= cmbCenter.ClientID %>").enabled = false;
        }else{
            document.getElementById("<%= cmbCenter.ClientID %>").enabled = true;
            var ToData = "1Ø" + branchCode ;
            ToServer(ToData, 1);
        }
    }
            //ViewType = -1 for normal report view
            //ViewType = 1 for excel report view
    function btnView_onclick() 
        {
            var Branch = document.getElementById("<%= cmbBranch.ClientID %>").value;  
            var Center = document.getElementById("<%= cmbCenter.ClientID %>").value;  
            var FromDt = document.getElementById("<%= txt_From_Dt.ClientID %>").value;  
            var ToDt = document.getElementById("<%= txt_To_Dt.ClientID %>").value;  
            var Status = document.getElementById("<%= cmbStatus.ClientID %>").value;  
                        window.open("ViewCCL_Report.aspx?ViewType=-1 &Branch=" + Branch +" &Center=" +  Center + "&FromDt=" + FromDt + "&ToDt=" + ToDt + "&Status=" + Status +" ", "_self");                   
       }    
   function btnExcelView_onclick() 
        {
            var Branch = document.getElementById("<%= cmbBranch.ClientID %>").value;  
            var Center = document.getElementById("<%= cmbCenter.ClientID %>").value;  
            var FromDt = document.getElementById("<%= txt_From_Dt.ClientID %>").value;  
            var ToDt = document.getElementById("<%= txt_To_Dt.ClientID %>").value; 
            var Status = document.getElementById("<%= cmbStatus.ClientID %>").value; 
                        window.open("ViewCCL_Report.aspx?ViewType=1 &Branch=" + Branch +" &Center=" +  Center + "&FromDt=" + FromDt + "&ToDt=" + ToDt + "&Status=" + Status +" ", "_self");                   
       }      
   function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
       
     function FromServer(arg, context) {

         switch (context) {
            case 1:
                {                   
                            ComboFill(arg, "<%= cmbCenter.ClientID %>");
                            break;
                }
             
               
            case 2:
               {    
                         
                    
                    
                }                            
              
             }
            }

    </script>
</asp:Content>

 