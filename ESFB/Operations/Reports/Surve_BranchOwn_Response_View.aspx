﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Surve_BranchOwn_Response_View.aspx.vb" Inherits="Operations_Surve_BranchOwn_Response_View" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

<br />

    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
     <tr>
            <td style="width:15%;">
     <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager></td>
   <td  style="width:85%;">
   </td></tr>
        <tr>
            <td style="width:15%;">
                Branch</td>
           <td  style="width:85%;">
                 <asp:TextBox ID="txtbranch" class="NormalText" runat="server" 
    style=" font-family:Cambria;font-size:10pt;" Width="50%" ></asp:TextBox>
                </td>
           
        </tr>
        <tr>
            <td style="width:15%;">
                Frequency</td>
           <td  style="width:85%;">
                 <asp:DropDownList ID="cmbFrequency" class="NormalText" runat="server" 
                     Font-Names="Cambria"
         Width="50%" ForeColor="Black" AppendDataBoundItems="True" Height="20px">
               
                </asp:DropDownList>
                </td>
           
        </tr>        
        <tr>
            <td style="width:15%;">
                Check List</td>
           <td  style="width:85%;">
                 <asp:DropDownList ID="cmbChecklist" class="NormalText" runat="server" 
                     Font-Names="Cambria" 
         Width="50%" ForeColor="Black" AppendDataBoundItems="True" Height="20px">
              
                </asp:DropDownList>
                </td>
           
        </tr>   
        <tr>
            <td style="width:15%;">
                Response Status</td>
           <td  style="width:85%;">
                 <asp:DropDownList ID="cmbResponse" class="NormalText" runat="server" 
                     Font-Names="Cambria" 
         Width="50%" ForeColor="Black" AppendDataBoundItems="True" Height="20px">
                    <asp:ListItem Text ="YES" Value="1" />
                    <asp:ListItem Text ="NO" Value="2"  Selected="true"/>
                    <asp:ListItem Text ="NA" Value="3" />
                     <asp:ListItem Value="0">--ALL--</asp:ListItem>
                </asp:DropDownList>
                </td>
                           
        </tr>           
           <%-- <tr>
        <td style= "width:15%;">
            Admin Response Status:</td>
             <td  style="width:85%;">
            <asp:DropDownList ID="drpadminStatus" class="NormalText" runat="server" 
                Font-Names="Cambria" Width="50%" 
                 ForeColor="Black" Height="20px">
                <asp:ListItem Value="1">OPEN</asp:ListItem>
                <asp:ListItem Value="2">CLOSED</asp:ListItem>
                <asp:ListItem Value="3">NA</asp:ListItem>
                <asp:ListItem Value="-1">--ALL--</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>   --%>
         <tr>
           
            <td style="width:15%; text-align:left;">
                From Date </td>
            <td style="width:85%">
                <asp:TextBox ID="txtFromDt" class="NormalText" runat="server" 
                    Width="50%" onkeypress="return false" Height="20px" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" 
                 Enabled="True" TargetControlID="txtFromDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>

            </td> 
 </tr>                 
         <tr>
           
            <td style="width:15%; text-align:left;">
                To Date </td>
            <td style="width:85%">
                <asp:TextBox ID="txttODt" class="NormalText" runat="server" 
                    Width="50%" onkeypress="return false" Height="20px" ></asp:TextBox>
                <asp:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtToDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>

            </td> 
 </tr>      
          <tr>
           <td style="width:15%; text-align:left;">
                 </td>
            <td style="width:85%">
            <br />
           
                <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 20%" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;
                
                <input id="cmd_Export_Excel" style="font-family: Cambria; cursor: pointer; width: 20%;" 
                type="button" value="VIEW EXCEL" onclick="return btnExcelView_onclick()" />&nbsp;&nbsp;
                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 20%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>

            </td> 


            <td style="width:100%; height: 18px; text-align:center;">
             <br />

             
             
          
      
        </tr>
       
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
               
                 <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
               
        </tr>
    </table>

  
   
    <script language="javascript" type="text/javascript">

    function FrequencyOnchange() {
            var FreqID=document.getElementById("<%= cmbFrequency.ClientID %>").value;
            var ToData = "1Ø" + FreqID;          
            ToServer(ToData, 1);
          
    }
     function FromServer(arg, context) {
            if (context == 1) {
                ComboFill(arg, "<%= cmbChecklist.ClientID %>");
            }
            
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
         function btnView_onclick() 
        {
             var BranchID = document.getElementById("<%= hdnValue.ClientID %>").value;
            var FreqID = document.getElementById("<%= cmbFrequency.ClientID %>").value;
            var ChecklistID = document.getElementById("<%= cmbChecklist.ClientID %>").value;
            var ReType = document.getElementById("<%= cmbResponse.ClientID %>").value;
       
             var FromDate= document.getElementById("<%= txtFromDt.ClientID %>").value;
             var FromDt = new Date(FromDate);
         
            var ToDate = document.getElementById("<%= txttODt.ClientID %>").value;
             var ToDt = new Date(ToDate);


             if(ToDt<FromDt)
             {
               alert("To date should be greater than From date");
             }
             else
                 window.open("View_Surve_BranchOwn_Response.aspx?ViewType=-1 &BID=" + BranchID + "&FID=" + FreqID + "&CID=" + ChecklistID + "&FDT=" + FromDate + "&TDT=" + ToDate + "&RTY=" + ReType +" ", "_self");
            
        
       }     
        function btnExcelView_onclick() 
        {
            var BranchID = document.getElementById("<%= hdnValue.ClientID %>").value;
            var FreqID = document.getElementById("<%= cmbFrequency.ClientID %>").value;
            var ChecklistID = document.getElementById("<%= cmbChecklist.ClientID %>").value;
            var ReType = document.getElementById("<%= cmbResponse.ClientID %>").value;
       
             var FromDate= document.getElementById("<%= txtFromDt.ClientID %>").value;
             var FromDt = new Date(FromDate);
         
            var ToDate = document.getElementById("<%= txttODt.ClientID %>").value;
             var ToDt = new Date(ToDate);


             if(ToDt<FromDt)
             {
               alert("To date should be greater than From date");
             }
             else
                 window.open("View_Surve_BranchOwn_Response.aspx?ViewType=1 &BID=" + BranchID + "&FID=" + FreqID + "&CID=" + ChecklistID + "&FDT=" + FromDate + "&TDT=" + ToDate + "&RTY=" + ReType +" ", "_self");
     
       }     
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }
   
  
       
    </script>
</asp:Content>





