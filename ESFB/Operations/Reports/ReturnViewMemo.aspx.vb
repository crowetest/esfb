﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ReturnViewMemo
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTMEMO As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date
    Dim DTROLE As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim DT As New DataTable
            Dim BranchID As Integer
            Dim FromDate As Date


            BranchID = CInt(Request.QueryString.Get("BranchID"))
            FromDate = CDate(Request.QueryString.Get("FromDate"))

            Dim DateFrom As String


            DateFrom = FromDate.ToString("yyyy-MM-dd")

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim headstr As String = "View Return Memo - " + "Effected Date:" + DateFrom
            RH.Heading(Session("FirmName"), tb, headstr, 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12, TRHead_13, TRHead_14, TRHead_15 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"
            TRHead_13.BorderWidth = "1"
            TRHead_14.BorderWidth = "1"
            TRHead_15.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver
            TRHead_13.BorderColor = Drawing.Color.Silver
            TRHead_14.BorderColor = Drawing.Color.Silver
            TRHead_15.BorderColor = Drawing.Color.Silver



            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid
            TRHead_13.BorderStyle = BorderStyle.Solid
            TRHead_14.BorderStyle = BorderStyle.Solid
            TRHead_15.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "SI No")
            RH.AddColumn(TRHead, TRHead_01, 5, 5, "l", "Branch code")
            RH.AddColumn(TRHead, TRHead_02, 15, 15, "l", "Item Seq No")
            RH.AddColumn(TRHead, TRHead_03, 15, 15, "l", "RBI Seq No")
            RH.AddColumn(TRHead, TRHead_04, 15, 15, "l", "Depositer Account Name")
            RH.AddColumn(TRHead, TRHead_05, 5, 5, "l", "Present RT No")
            RH.AddColumn(TRHead, TRHead_06, 5, 5, "l", "Cheque No")
            RH.AddColumn(TRHead, TRHead_07, 5, 5, "l", "TC")
            RH.AddColumn(TRHead, TRHead_08, 5, 5, "l", "Amount")
            RH.AddColumn(TRHead, TRHead_09, 5, 5, "l", "Return Reason")
            RH.AddColumn(TRHead, TRHead_10, 5, 5, "l", "Issuing Branch Name")
            RH.AddColumn(TRHead, TRHead_11, 5, 5, "l", "Branch Name")
            'RH.AddColumn(TRHead, TRHead_12, 10, 10, "l", "Effected Date")
            'RH.AddColumn(TRHead, TRHead_13, 10, 10, "l", "Uploaded Date")
            'RH.AddColumn(TRHead, TRHead_14, 10, 10, "l", "Uploaded By")
            RH.AddColumn(TRHead, TRHead_15, 10, 10, "l", "File")

            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer

            Dim StrWhere As String = ""
            If (BranchID <> -1) Then
                StrWhere = " and b.Branch_id = " & BranchID & ""
            Else
                StrWhere = " and 1=1"
            End If
            DTROLE = DB.ExecuteDataSet(" select emp_code from roles_assigned where role_id=83 and emp_code='" + Session("UserID") + "'").Tables(0)
            If (DTROLE.Rows.Count > 0) Then
                DT = DB.ExecuteDataSet(" select [Branch Code], " +
                                         " [Item Seq Num], " +
                                         " [RBI Seq Num], " +
                                         " [Depositor Account Name], " +
                                         " [Presentment RT Num‎], " +
                                         " [Cheque No],TC,cast(Amount as numeric(15,2)),[Return Reasons],[Issuing BranchName‎],[Branch Name‎],[Date], " +
                                         " [UploadedDate],[UploadedBy],bm.branch_name from branch_memo_details " +
                                         " inner join branch_master bm on bm.branch_id = branch_memo_details.[Branch Code] where convert(date,date,105) ='" + DateFrom + "'").Tables(0)
            Else

                DT = DB.ExecuteDataSet(" select [Branch Code], " +
                                       " [Item Seq Num], " +
                                       " [RBI Seq Num], " +
                                       " [Depositor Account Name], " +
                                       " [Presentment RT Num‎], " +
                                       " [Cheque No],TC,cast(Amount as numeric(15,2)),[Return Reasons],[Issuing BranchName‎],[Branch Name‎],[Date], " +
                                       " [UploadedDate],[UploadedBy],bm.branch_name from branch_memo_details " +
                                       " inner join branch_master bm on bm.branch_id = branch_memo_details.[Branch Code] where convert(date,date,105) ='" + DateFrom + "' and bm.branch_id = '" + Session("BranchID").ToString() + "'").Tables(0)
            End If
            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"
                TR3_15.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver
                TR3_15.BorderColor = Drawing.Color.Silver



                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid
                TR3_15.BorderStyle = BorderStyle.Solid



                RH.AddColumn(TR3, TR3_00, 5, 5, "l", i.ToString())
                RH.AddColumn(TR3, TR3_01, 5, 5, "l", DR(14).ToString())
                RH.AddColumn(TR3, TR3_02, 15, 15, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_04, 15, 15, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_05, 5, 5, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_06, 5, 5, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_07, 5, 5, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_08, 5, 5, "l", DR(7).ToString())
                RH.AddColumn(TR3, TR3_09, 5, 5, "l", DR(8).ToString())
                RH.AddColumn(TR3, TR3_10, 5, 5, "l", DR(9).ToString())
                RH.AddColumn(TR3, TR3_11, 5, 5, "l", DR(10).ToString())
                Dim val As String = DR(0).ToString() + "-"
                'Dim ext As String = ".xls"
                DTMEMO = DB.ExecuteDataSet(" select FileName,e_photo,content_type " +
                                           " from DMS_ESFB.DBO.MEMO_FILES where filename = '" + val + "' + " + " + CONVERT(VARCHAR(19), convert(date,'" + DateFrom + "') , 120)").Tables(0)
                Dim file As String = ""
                If DTMEMO.Rows.Count > 0 Then
                    Dim bytes As Byte() = DirectCast(DTMEMO.Rows(0)(1), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                    file = Convert.ToString("data:" + DTMEMO.Rows(0)(2) + ";base64,") & base64String
                    RH.AddColumn(TR3, TR3_15, 10, 10, "c", "<a href='" + file + "' style='text-align:right;' target='_blank'>" + " View " + "</a>")
                Else
                    RH.AddColumn(TR3, TR3_15, 10, 10, "c", "No File")
                End If

                'RH.AddColumn(TR3, TR3_12, 10, 10, "l", DR(11).ToString())
                'RH.AddColumn(TR3, TR3_13, 10, 10, "l", DR(12).ToString())
                'RH.AddColumn(TR3, TR3_14, 10, 10, "l", DR(13).ToString())
                'RH.AddColumn(TR3, TR3_15, 10, 10, "l", "")
                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            Dim DT As New DataTable
            HeaderText = "Assigned Branches Report"

            WebTools.ExporttoExcel(DT, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
