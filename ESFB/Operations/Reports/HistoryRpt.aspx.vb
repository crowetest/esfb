﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class HistoryRpt
    Inherits System.Web.UI.Page

    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim Meeting_Dt As Date
    Dim UserID As Integer
    Dim TraDt As String
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim DB As New MS_SQL.Connect
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1231) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "HISTORY REPORT"
            UserID = CInt(Session("UserID"))



            If Not IsPostBack Then



            End If


            Me.cmbItem.Attributes.Add("onchange", "return ItemOnChange()")
            Me.cmbChecklist.Attributes.Add("onchange", "return ChecklistOnChange()")


            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
        DB.dispose()
    End Sub

#End Region
#Region "Events"

#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            'Dim ItemId As Integer = CInt(Data(1))
            DT4 = DB.ExecuteDataSet("SELECT '-1' AS checklistid,'-----All-----' AS checklistNo UNION ALL SELECT distinct  checklistid as checklistid,description as checklistNo from Surve_ChkList_Master").Tables(0)

            For Each DR As DataRow In DT4.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + CStr(DR(1))
            Next
        ElseIf CInt(Data(0)) = 2 Then
            Dim checkid As Integer = CInt(Data(1))
            DT4 = DB.ExecuteDataSet("SELECT '-1' AS taskid,'-----All-----' AS taskdesc UNION ALL SELECT distinct taskid as taskid,taskdesc as taskdesc from Surve_task_Master where checklistid=" + checkid.ToString() + "").Tables(0)

            For Each DR As DataRow In DT4.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + CStr(DR(1))
            Next
        End If
    End Sub
#End Region
End Class

