﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Operations_Surve_BranchOwn_Response_View
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
        Dim DT As New DataTable
        Dim DTTS As New DataTable
        Dim DB As New MS_SQL.Connect
        Dim GF As New GeneralFunctions

        Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
            If GF.FormAccess(CInt(Session("UserID")), 1431) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "BranchWise Response Review Report"
            If Not IsPostBack Then
                txtbranch.Text = CStr(Session("BranchName"))
                Me.hdnValue.Value = CStr(Session("BranchID"))
                'DT = GetBranch()
                'If Not DT Is Nothing Then
                '    GF.ComboFill(cmbBranch, DT, 0, 1)
                'End If
                Dim DT1 As New DataTable
                DT1 = GetFrequency()
                If Not DT1 Is Nothing Then
                    GF.ComboFill(cmbFrequency, DT1, 0, 1)
                End If
                Dim DT2 As New DataTable
                DT2 = GetCheckList()
                If Not DT2 Is Nothing Then
                    GF.ComboFill(cmbChecklist, DT2, 0, 1)
                End If
                Me.txtFromDt.Text = CStr(CDate(Session("TraDt")))
                Me.txttODt.Text = CStr(CDate(Session("TraDt")))
            End If
            ' Me.cmbBranch.Attributes.Add("onchange", "return BranchOnchange()")
            Me.cmbFrequency.Attributes.Add("onchange", "return FreqOnchange()")
            Me.cmbChecklist.Attributes.Add("onchange", "return CheckOnchange()")
            Me.cmbResponse.Attributes.Add("onchange", "return StatusOnchange()")
            Me.txtFromDt.Attributes.Add("onchange", "return DateOnchange()")
            Me.txttODt.Attributes.Add("onchange", "return DateOnchange()")
            ' Me.drpadminStatus.Attributes.Add("onchange", "return AdminStatusOnchange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim FreqId As Integer
            FreqId = CInt(Data(1))
            DT = DB.ExecuteDataSet("select -1 as checklistid, '--Select--' as Description union select checklistid, Description FROM Surve_ChkList_Master where Frequency = " + FreqId.ToString).Tables(0)
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += "Ñ" + DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 2 Then
            Dim BranchId As Integer = CInt(Data(1))
            Dim Typeid As Integer = CInt(Data(2))
            Dim FromDt As DateTime = Nothing
            Dim ToDt As DateTime = Date.Today()
            Dim AdminStatus = CInt(Data(5))
            Dim ChecklistID = CInt(Data(6))
            Dim FreqID = CInt(Data(7))
            If Data(3) <> "" Then
                FromDt = CDate(Data(3))
            End If
            If Data(4) <> "" Then
                ToDt = CDate(Data(4))
            End If
            Dim StrWhere As String
            StrWhere = " 1 = 1 "
            Dim FromDateDt As String = FromDt.ToString("yyyy-MM-dd")
            Dim ToDateDt As String = ToDt.ToString("yyyy-MM-dd")
            If BranchId <> -1 Then
                StrWhere += " and sbrm.Branch_id = " & BranchId & ""
            End If
            If Typeid <> -1 Then
                StrWhere += " and sbr.Resp_type = " & Typeid & ""
            End If
            If ChecklistID <> -1 Then
                StrWhere += " and sbrm.Checklistid = " & ChecklistID & ""
            End If
            If AdminStatus <> -1 Then
                StrWhere += " and sbr.reviewstatus = " & AdminStatus & ""
            End If
            If FreqID <> -1 Then
                StrWhere += " and scm.frequency = " & FreqID & ""
            End If
            If FromDateDt <> "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt <> "" And ToDateDt = "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt = "" Then
                StrWhere += ""
            End If
            DT = DB.ExecuteDataSet("select -1 as checklistid, '--ALL--' as Description union select sbrm.checklistid as checklistid,scm.description from Surve_Branch_Response_Master sbrm " +
                                       "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                                       "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                                       "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                                       "where " + StrWhere.ToString()).Tables(0)
            If Not DT Is Nothing Then
                GF.ComboFill(cmbChecklist, DT, 0, 1)
            End If
            DT = DB.ExecuteDataSet("select bm.branch_id,bm.branch_name,sfm.value,scm.description,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments " +
                                       ",scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus,sbr.created_on,sbr.task_id from Surve_Branch_Response_Master sbrm " +
                                       "inner join Branch_master bm on bm.branch_id=sbrm.branch_id " +
                                       "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                                       "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                                       "inner join Surve_frequency_master sfm on sfm.id=scm.frequency " +
                                       "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                                       "where " + StrWhere.ToString()).Tables(0)
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper() + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + CDate(DT.Rows(n)(11)).ToString("dd/MM/yyyy").ToUpper() + "µ" + DT.Rows(n)(12).ToString().ToUpper()
            Next

            'If (Typeid <> -1 And AdminStatus <> -1) Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments " +
            '                           ",scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + BranchId.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrWhere.ToString() + " and finish_status=1 and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf (Typeid <> -1 And AdminStatus = -1) Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments " +
            '                           ",scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + BranchId.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrWhere.ToString() + " and finish_status=1 ").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf (Typeid = -1 And AdminStatus <> -1) Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments " +
            '                           ",scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + BranchId.ToString() + " and " + StrWhere.ToString() + " and finish_status=1 and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf (Typeid = -1 And AdminStatus = -1) Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments " +
            '                           ",scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + BranchId.ToString() + " and " + StrWhere.ToString() + " and finish_status=1 ").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'End If
        ElseIf CInt(Data(0)) = 3 Or CInt(Data(0)) = 4 Then
            Dim dataval As String = CStr(Data(1))
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim BranchID As Integer = CInt(Session("BranchID"))

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim Save_Flag As Integer
            Save_Flag = CInt(If(CInt(Data(0)) = 4, 1, 0))
            'Dim CheckListId As Integer = CInt(Data(2))
            Try

                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@RespDtl", SqlDbType.VarChar, 5000)
                Params(1).Value = dataval.Substring(1)
                Params(2) = New SqlParameter("@SAVE", SqlDbType.Int)
                Params(2).Value = Save_Flag
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_SURVE_ADMIN_RESPONSE", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + Save_Flag.ToString()
        ElseIf CInt(Data(0)) = 5 Then
            Dim Branchid As Integer = CInt(Data(1))
            Dim Freqid As Integer = CInt(Data(2))
            Dim Typeid As Integer = CInt(Data(3))
            Dim FromDt As DateTime = Nothing
            Dim ToDt As DateTime = Date.Today()
            Dim AdminStatus As Integer = CInt(Data(6))
            Dim ChecklistId As Integer = CInt(Data(7))
            If Data(4) <> "" Then
                FromDt = CDate(Data(4))
            End If
            If Data(5) <> "" Then
                ToDt = CDate(Data(5))
            End If
            Dim StrWhere As String
            StrWhere = " 1 = 1 "
            Dim FromDateDt As String = FromDt.ToString("yyyy-MM-dd")
            Dim ToDateDt As String = ToDt.ToString("yyyy-MM-dd")
            If FromDateDt <> "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt <> "" And ToDateDt = "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt = "" Then
                StrWhere += ""
            End If
            If Branchid <> -1 Then
                StrWhere += " and sbrm.Branch_id = " & Branchid & ""
            End If
            If Typeid <> -1 Then
                StrWhere += " and sbr.Resp_type = " & Typeid & ""
            End If
            If ChecklistId <> -1 Then
                StrWhere += " and sbrm.Checklistid = " & ChecklistId & ""
            End If
            If AdminStatus <> -1 Then
                StrWhere += " and sbr.reviewstatus = " & AdminStatus & ""
            End If
            If Freqid <> -1 Then
                StrWhere += " and scm.frequency = " & Freqid & ""
            End If
            DT = DB.ExecuteDataSet("select -1 as checklistid, '--ALL--' as Description union select sbrm.checklistid as checklistid,scm.description from Surve_Branch_Response_Master sbrm " +
                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                           "where " + StrWhere.ToString()).Tables(0)
            If Not DT Is Nothing Then
                GF.ComboFill(cmbChecklist, DT, 0, 1)
            End If
            DT = DB.ExecuteDataSet("select bm.branch_id,bm.branch_name,sfm.value,scm.description,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                                       "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus,sbr.created_on,sbr.task_id from Surve_Branch_Response_Master sbrm " +
                                       "inner join Branch_master bm on bm.branch_id=sbrm.branch_id " +
                                       "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                                       "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                                       "inner join Surve_frequency_master sfm on sfm.id=scm.frequency " +
                                       "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                                       "where " + StrWhere.ToString()).Tables(0)
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper() + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + CDate(DT.Rows(n)(11)).ToString("dd/MM/yyyy").ToUpper() + "µ" + DT.Rows(n)(12).ToString().ToUpper()
            Next
            'If (Typeid <> -1 And AdminStatus <> -1) Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf (Typeid <> -1 And AdminStatus = -1) Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf (Typeid = -1 And AdminStatus <> -1) Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                            "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                            "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                            "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                            "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                            "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf (Typeid = -1 And AdminStatus = -1) Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                            "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                            "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                            "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                            "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                            "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'End If
        ElseIf CInt(Data(0)) = 6 Then
            Dim Branchid As Integer = CInt(Data(1))
            Dim Freqid As Integer = CInt(Data(2))
            Dim Typeid As Integer = CInt(Data(3))
            Dim FromDt As DateTime = Nothing
            Dim ToDt As DateTime = Date.Today()
            Dim AdminStatus = CInt(Data(6))
            Dim ChecklistId As Integer = CInt(Data(7))
            If Data(4) <> "" Then
                FromDt = CDate(Data(4))
            End If
            If Data(5) <> "" Then
                ToDt = CDate(Data(5))
            End If
            Dim StrWhere As String
            StrWhere = " 1 = 1 "
            Dim FromDateDt As String = FromDt.ToString("yyyy-MM-dd")
            Dim ToDateDt As String = ToDt.ToString("yyyy-MM-dd")
            If FromDateDt <> "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt <> "" And ToDateDt = "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt = "" Then
                StrWhere += ""
            End If
            If Branchid <> -1 Then
                StrWhere += " and sbrm.Branch_id = " & Branchid & ""
            End If
            If Typeid <> -1 Then
                StrWhere += " and sbr.Resp_type = " & Typeid & ""
            End If
            If ChecklistId <> -1 Then
                StrWhere += " and sbrm.Checklistid = " & ChecklistId & ""
            End If
            If AdminStatus <> -1 Then
                StrWhere += " and sbr.reviewstatus = " & AdminStatus & ""
            End If
            If Freqid <> -1 Then
                StrWhere += " and scm.frequency = " & Freqid & ""
            End If
            DT = DB.ExecuteDataSet("select -1 as checklistid, '--ALL--' as Description union select sbrm.checklistid as checklistid,scm.description from Surve_Branch_Response_Master sbrm " +
                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                           "where " + StrWhere.ToString()).Tables(0)
            'If (Typeid <> -1 And AdminStatus <> -1) Then
            '    DT = DB.ExecuteDataSet("select -1 as checklistid, '--ALL--' as Description union select sbrm.checklistid as checklistid,scm.description from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "Ñ" + DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString()
            '    Next
            'ElseIf (Typeid <> -1 And AdminStatus = -1) Then
            '    DT = DB.ExecuteDataSet("select -1 as checklistid, '--ALL--' as Description union select sbrm.checklistid as checklistid,scm.description from Surve_Branch_Response_Master sbrm " +
            '                         "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                         "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                         "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                         "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "Ñ" + DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString()
            '    Next
            'ElseIf (Typeid = -1 And AdminStatus <> -1) Then
            '    DT = DB.ExecuteDataSet("select -1 as checklistid, '--ALL--' as Description union select sbrm.checklistid as checklistid,scm.description from Surve_Branch_Response_Master sbrm " +
            '                         "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                         "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                         "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                         "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "Ñ" + DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString()
            '    Next
            'ElseIf (Typeid = -1 And AdminStatus = -1) Then
            '    DT = DB.ExecuteDataSet("select -1 as checklistid, '--ALL--' as Description union select sbrm.checklistid as checklistid,scm.description from Surve_Branch_Response_Master sbrm " +
            '                        "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                        "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                        "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                        "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + "").Tables(0)
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += "Ñ" + DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString()
            Next
            'End If
        ElseIf CInt(Data(0)) = 7 Then
            Dim Branchid As Integer = CInt(Data(1))
            Dim Freqid As Integer = CInt(Data(2))
            Dim Typeid As Integer = CInt(Data(3))
            Dim FromDt As DateTime = Nothing
            Dim ToDt As DateTime = Date.Today()
            Dim Checklistid As Integer = CInt(Data(6))
            Dim AdminStatus = CInt(Data(7))
            If Data(4) <> "" Then
                FromDt = CDate(Data(4))
            End If
            If Data(5) <> "" Then
                ToDt = CDate(Data(5))
            End If
            Dim StrWhere As String
            StrWhere = " 1 = 1 "
            Dim FromDateDt As String = FromDt.ToString("yyyy-MM-dd")
            Dim ToDateDt As String = ToDt.ToString("yyyy-MM-dd")
            If FromDateDt <> "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt <> "" And ToDateDt = "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt = "" Then
                StrWhere += ""
            End If
            If Branchid <> -1 Then
                StrWhere += " and sbrm.Branch_id = " & Branchid & ""
            End If
            If Typeid <> -1 Then
                StrWhere += " and sbr.Resp_type = " & Typeid & ""
            End If
            If Checklistid <> -1 Then
                StrWhere += " and sbrm.Checklistid = " & Checklistid & ""
            End If
            If AdminStatus <> -1 Then
                StrWhere += " and sbr.reviewstatus = " & AdminStatus & ""
            End If
            If Freqid <> -1 Then
                StrWhere += " and scm.frequency = " & Freqid & ""
            End If
            DT = DB.ExecuteDataSet("select -1 as checklistid, '--ALL--' as Description union select sbrm.checklistid as checklistid,scm.description from Surve_Branch_Response_Master sbrm " +
                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                           "where " + StrWhere.ToString()).Tables(0)
            If Not DT Is Nothing Then
                GF.ComboFill(cmbChecklist, DT, 0, 1)
            End If
            DT = DB.ExecuteDataSet("select bm.branch_id,bm.branch_name,sfm.value,scm.description,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                                       "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus,sbr.created_on,sbr.task_id from Surve_Branch_Response_Master sbrm " +
                                       "inner join Branch_master bm on bm.branch_id=sbrm.branch_id " +
                                       "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                                       "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                                       "inner join Surve_frequency_master sfm on sfm.id=scm.frequency " +
                                       "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                                       "where " + StrWhere.ToString()).Tables(0)
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper() + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + CDate(DT.Rows(n)(11)).ToString("dd/MM/yyyy").ToUpper() + "µ" + DT.Rows(n)(12).ToString().ToUpper()
            Next
            'If Typeid <> -1 And AdminStatus <> -1 And checkid <> -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid <> -1 And AdminStatus <> -1 And checkid = -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid <> -1 And AdminStatus = -1 And checkid <> -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                          "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                          "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                          "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                          "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                          "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid <> -1 And AdminStatus = -1 And checkid = -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                          "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                          "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                          "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                          "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                          "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid = -1 And AdminStatus <> -1 And checkid <> -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid = -1 And AdminStatus <> -1 And checkid = -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid = -1 And AdminStatus = -1 And checkid <> -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                            "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                            "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                            "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                            "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                            "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid = -1 And AdminStatus = -1 And checkid = -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                            "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                            "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                            "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                            "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                            "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'End If
        ElseIf CInt(Data(0)) = 8 Then
            Dim Branchid As Integer = CInt(Data(1))
            Dim Freqid As Integer = CInt(Data(2))
            Dim Typeid As Integer = CInt(Data(3))
            Dim FromDt As DateTime = Nothing
            Dim ToDt As DateTime = Date.Today()
            Dim Checklistid As Integer = CInt(Data(6))
            Dim AdminStatus = CInt(Data(7))
            If Data(4) <> "" Then
                FromDt = CDate(Data(4))
            End If
            If Data(5) <> "" Then
                ToDt = CDate(Data(5))
            End If
            Dim StrWhere As String
            StrWhere = " 1 = 1 "
            Dim FromDateDt As String = FromDt.ToString("yyyy-MM-dd")
            Dim ToDateDt As String = ToDt.ToString("yyyy-MM-dd")
            If FromDateDt <> "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt <> "" And ToDateDt = "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt = "" Then
                StrWhere += ""
            End If
            If Branchid <> -1 Then
                StrWhere += " and sbrm.Branch_id = " & Branchid & ""
            End If
            If Typeid <> -1 Then
                StrWhere += " and sbr.Resp_type = " & Typeid & ""
            End If
            If Checklistid <> -1 Then
                StrWhere += " and sbrm.Checklistid = " & Checklistid & ""
            End If
            If AdminStatus <> -1 Then
                StrWhere += " and sbr.reviewstatus = " & AdminStatus & ""
            End If
            If Freqid <> -1 Then
                StrWhere += " and scm.frequency = " & Freqid & ""
            End If
            DT = DB.ExecuteDataSet("select -1 as checklistid, '--ALL--' as Description union select sbrm.checklistid as checklistid,scm.description from Surve_Branch_Response_Master sbrm " +
                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                           "where " + StrWhere.ToString()).Tables(0)
            If Not DT Is Nothing Then
                GF.ComboFill(cmbChecklist, DT, 0, 1)
            End If
            DT = DB.ExecuteDataSet("select bm.branch_id,bm.branch_name,sfm.value,scm.description,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                                       "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus,sbr.created_on,sbr.task_id from Surve_Branch_Response_Master sbrm " +
                                       "inner join Branch_master bm on bm.branch_id=sbrm.branch_id " +
                                       "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                                       "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                                       "inner join Surve_frequency_master sfm on sfm.id=scm.frequency " +
                                       "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                                       "where " + StrWhere.ToString()).Tables(0)
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper() + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + CDate(DT.Rows(n)(11)).ToString("dd/MM/yyyy").ToUpper() + "µ" + DT.Rows(n)(12).ToString().ToUpper()
            Next
            'If Typeid <> -1 And AdminStatus <> -1 And checkid <> -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid <> -1 And AdminStatus <> -1 And checkid = -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid <> -1 And AdminStatus = -1 And checkid <> -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                          "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                          "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                          "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                          "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                          "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid <> -1 And AdminStatus = -1 And checkid = -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                          "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                          "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                          "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                          "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                          "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid = -1 And AdminStatus <> -1 And checkid <> -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid = -1 And AdminStatus <> -1 And checkid = -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                           "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid = -1 And AdminStatus = -1 And checkid <> -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                            "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                            "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                            "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                            "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                            "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'ElseIf Typeid = -1 And AdminStatus = -1 And checkid = -1 Then
            '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
            '                            "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
            '                            "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
            '                            "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
            '                            "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
            '                            "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + "").Tables(0)
            '    For n As Integer = 0 To DT.Rows.Count - 1
            '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
            '    Next
            'End If
        ElseIf CInt(Data(0)) = 9 Then
            Dim Branchid As Integer = CInt(Data(1))
            Dim Freqid As Integer = CInt(Data(2))
            Dim Typeid As Integer = CInt(Data(3))
            Dim FromDt As DateTime = Nothing
            Dim ToDt As DateTime = Date.Today()
            Dim ChecklistId As Integer = CInt(Data(6))
            Dim AdminStatus = CInt(Data(7))
            If Data(4) <> "" Then
                FromDt = CDate(Data(4))
            End If
            If Data(5) <> "" Then
                ToDt = CDate(Data(5))
            End If
            Dim StrWhere As String
            StrWhere = " 1 = 1 "
            Dim FromDateDt As String = FromDt.ToString("yyyy-MM-dd")
            Dim ToDateDt As String = ToDt.ToString("yyyy-MM-dd")
            If FromDateDt <> "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt <> "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt <> "" And ToDateDt = "" Then
                StrWhere += " and  cast(sbrm.created_on as date) >='" & FromDateDt & "' and  cast(sbrm.created_on as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt = "" Then
                StrWhere += ""
            End If
            If Branchid <> -1 Then
                StrWhere += " and sbrm.Branch_id = " & Branchid & ""
            End If
            If Typeid <> -1 Then
                StrWhere += " and sbr.Resp_type = " & Typeid & ""
            End If
            If ChecklistId <> -1 Then
                StrWhere += " and sbrm.Checklistid = " & ChecklistId & ""
            End If
            If AdminStatus <> -1 Then
                StrWhere += " and sbr.reviewstatus = " & AdminStatus & ""
            End If
            If Freqid <> -1 Then
                StrWhere += " and scm.frequency = " & Freqid & ""
            End If
            DT = DB.ExecuteDataSet("select -1 as checklistid, '--ALL--' as Description union select sbrm.checklistid as checklistid,scm.description from Surve_Branch_Response_Master sbrm " +
                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                           "where " + StrWhere.ToString()).Tables(0)
            If Not DT Is Nothing Then
                GF.ComboFill(cmbChecklist, DT, 0, 1)
            End If
                DT = DB.ExecuteDataSet("select bm.branch_id,bm.branch_name,sfm.value,scm.description,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                                       "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus,sbr.created_on,sbr.task_id from Surve_Branch_Response_Master sbrm " +
                                        "inner join Branch_master bm on bm.branch_id=sbrm.branch_id " +
                                        "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                                       "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                                       "inner join Surve_frequency_master sfm on sfm.id=scm.frequency " +
                                       "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                                       "where " + StrWhere.ToString()).Tables(0)
                For n As Integer = 0 To DT.Rows.Count - 1
                    CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper() + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + CDate(DT.Rows(n)(11)).ToString("dd/MM/yyyy").ToUpper() + "µ" + DT.Rows(n)(12).ToString().ToUpper()
                Next
                'If Typeid <> -1 And AdminStatus <> -1 And checkid <> -1 Then
                '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
                '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                '                           "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
                '    For n As Integer = 0 To DT.Rows.Count - 1
                '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
                '    Next
                'ElseIf Typeid <> -1 And AdminStatus <> -1 And checkid = -1 Then
                '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
                '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                '                           "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
                '    For n As Integer = 0 To DT.Rows.Count - 1
                '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
                '    Next
                'ElseIf Typeid <> -1 And AdminStatus = -1 And checkid <> -1 Then
                '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                '                          "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
                '                          "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                '                          "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                '                          "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                '                          "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + "").Tables(0)
                '    For n As Integer = 0 To DT.Rows.Count - 1
                '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
                '    Next
                'ElseIf Typeid <> -1 And AdminStatus = -1 And checkid = -1 Then
                '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                '                          "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
                '                          "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                '                          "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                '                          "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                '                          "where sbrm.branch_id=" + Branchid.ToString() + " and sbr.resp_type= " + Typeid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + "").Tables(0)
                '    For n As Integer = 0 To DT.Rows.Count - 1
                '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
                '    Next
                'ElseIf Typeid = -1 And AdminStatus <> -1 And checkid <> -1 Then
                '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
                '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                '                           "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
                '    For n As Integer = 0 To DT.Rows.Count - 1
                '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
                '    Next
                'ElseIf Typeid = -1 And AdminStatus <> -1 And checkid = -1 Then
                '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                '                           "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
                '                           "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                '                           "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                '                           "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                '                           "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and reviewStatus=" + AdminStatus.ToString() + "").Tables(0)
                '    For n As Integer = 0 To DT.Rows.Count - 1
                '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
                '    Next
                'ElseIf Typeid = -1 And AdminStatus = -1 And checkid <> -1 Then
                '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                '                            "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
                '                            "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                '                            "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                '                            "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                '                            "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + " and scm.checklistid=" + checkid.ToString() + "").Tables(0)
                '    For n As Integer = 0 To DT.Rows.Count - 1
                '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
                '    Next
                'ElseIf Typeid = -1 And AdminStatus = -1 And checkid = -1 Then
                '    DT = DB.ExecuteDataSet("select sbrm.checklistid,scm.description,sbr.Task_id,stm.taskdesc,case when sbr.resp_type = 1 then 'YES' when sbr.resp_type = 2 then 'NO' when sbr.resp_type = 3 then 'NA' end as Resptype,sbr.Resp_comments, " +
                '                            "scm.description,sbrm.resp_master_id,sbr.reviewcomment,sbr.reviewstatus from Surve_Branch_Response_Master sbrm " +
                '                            "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                '                            "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                '                            "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                '                            "where sbrm.branch_id=" + Branchid.ToString() + " and " + StrDateWhere.ToString() + " and finish_status=1 and scm.frequency=" + Freqid.ToString() + "").Tables(0)
                '    For n As Integer = 0 To DT.Rows.Count - 1
                '        CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + DT.Rows(n)(9).ToString().ToUpper()
                '    Next
                'End If
            End If
        End Sub
#End Region
#Region "Function"
        Public Function GetBranch() As DataTable
            Return DB.ExecuteDataSet("select -1 as Id, '--ALL--' as Value union select -1 as Id, '---Select---' as Value union select bm.branch_id as id, bm.branch_name as Value FROM branch_Master bm " +
                                 "inner join surve_branch_response_master sbrm on bm.branch_id = sbrm.branch_id where bm.status_id= 1 and sbrm.Finish_status=1").Tables(0)
            'Return DB.ExecuteDataSet(" SELECT '-1' AS Branch_id,'-----ALL-----' AS Branch_name UNION ALL SELECT distinct Branch_id,Branch_name from BRANCH_MASTER where status_id=1").Tables(0)
        End Function
        Public Function GetFrequency() As DataTable
            Return DB.ExecuteDataSet("select -1 as Id, '--ALL--' as Value union select -1 as Id, '---Select---' as Value union select id as id, value as Value FROM surve_branch_response_master sbrm " +
                                 "inner join branch_master sbm on sbm.branch_id = sbrm.branch_id " +
                                 "inner join surve_chklist_master scm on scm.checklistid = sbrm.checklistid " +
                                 "inner join surve_frequency_master sm on scm.frequency=sm.id where sm.status= 1 and sbrm.Finish_status=1").Tables(0)
        End Function
        Public Function GetCheckList() As DataTable
            'Return DB.ExecuteDataSet("select -1 as Id, '---Select---' as Value union SELECT '-1' AS id,'--ALL--' AS Value ").Tables(0)
            Return DB.ExecuteDataSet("select -1 as checklistid, '--ALL--' as Description union select sbrm.checklistid as checklistid,scm.description from Surve_Branch_Response_Master sbrm " +
                                       "inner join Surve_Branch_Response sbr on sbrm.resp_master_id=sbr.resp_master_id " +
                                       "inner join surve_chklist_master scm on sbrm.checklistid = scm.checklistid " +
                                       "inner join Surve_Task_Master stm on stm.taskid = sbr.task_id " +
                                       "").Tables(0)
        End Function
#End Region
    End Class


