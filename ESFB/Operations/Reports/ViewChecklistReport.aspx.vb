﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewChecklistReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTExcel As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim DT As New DataTable
            Dim ViewType, Type, Item As Integer
            Dim Checklist As Integer
            ViewType = CInt(Request.QueryString.Get("ViewType"))
            Item = CInt(Request.QueryString.Get("Item"))
            If Request.QueryString.Get("Checklist") <> "" Then
                Checklist = CInt(Request.QueryString.Get("Checklist"))
            End If
            Type = CInt(Request.QueryString.Get("Type"))

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            If Item = -1 Then
                If (Type = -1) Then
                    RH.Heading(Session("FirmName"), tb, "Active Check List Report", 100)
                ElseIf (Type = 1) Then
                    RH.Heading(Session("FirmName"), tb, "Inactive Check List Report", 100)
                ElseIf (Type = 2) Then
                    RH.Heading(Session("FirmName"), tb, "Deleted Check List Report", 100)
                ElseIf (Type = 3) Then
                    RH.Heading(Session("FirmName"), tb, "Approved Check List Report", 100)
                ElseIf (Type = 4) Then
                    RH.Heading(Session("FirmName"), tb, "Rejected Check List Report", 100)
                End If
            Else
                If (Type = -1) Then
                    RH.Heading(Session("FirmName"), tb, "Active Task List Report", 100)
                ElseIf (Type = 1) Then
                    RH.Heading(Session("FirmName"), tb, "Inactive Task List Report", 100)
                ElseIf (Type = 2) Then
                    RH.Heading(Session("FirmName"), tb, "Deleted Task List Report", 100)
                ElseIf (Type = 3) Then
                    RH.Heading(Session("FirmName"), tb, "Approved Task List Report", 100)
                ElseIf (Type = 4) Then
                    RH.Heading(Session("FirmName"), tb, "Rejected Task List Report", 100)
                End If
            End If


            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "SI No")
            If Item = -1 Then
                RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "CheckList")
            Else
                RH.AddColumn(TRHead, TRHead_01, 30, 30, "l", "Task")
            End If

            RH.AddColumn(TRHead, TRHead_02, 30, 30, "l", "Description")
            If Item = -1 Then
                RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Frequency")
                RH.AddColumn(TRHead, TRHead_04, 10, 10, "l", "Interval")
                RH.AddColumn(TRHead, TRHead_05, 10, 10, "l", "Effected Date")
            Else
                RH.AddColumn(TRHead, TRHead_04, 20, 20, "l", "Weightage")
            End If
            RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "Approved Date")
            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer
            Dim SqlStr As String = " "
            Dim StrWhere As String = " "
            Dim ExSqlStr As String = " "
            If Item = -1 Then
                SqlStr = "select Description,Remarks,sfm.value,interval,AffectedOn,ApprovedDate from Surve_ChkList_Master scm  inner join Surve_Frequency_Master sfm  on sfm.id=scm.Frequency where 1=1 And "
                ExSqlStr = "select Description as CheckList,Remarks as Description,sfm.value as Frequency,interval as Interval,convert(varchar(11),AffectedOn,105) as [Effected Date],Convert(varchar(11),ApprovedDate,105) as [Approved Date] from Surve_ChkList_Master scm  inner join Surve_Frequency_Master sfm  on sfm.id=scm.Frequency where 1=1 And "
                If (Type = -1) Then
                    StrWhere = " status_id =1"
                ElseIf (Type = 1) Then
                    StrWhere = " status_id =-1"
                ElseIf (Type = 2) Then
                    StrWhere = " status_id =999"
                ElseIf (Type = 3) Then
                    StrWhere = " approvedstatus =1"
                ElseIf (Type = 4) Then
                    SqlStr = "select Description,Remarks,sfm.value as val,interval,AffectedOn,approveddate from Surve_ChkList_Tracker stm inner join Surve_Frequency_Master sfm  on sfm.id=stm.Frequency where 1=1 And ApprovedDate = (SELECT MAX(ApprovedDate) FROM Surve_chklist_Tracker Where checklistid = stm.checklistid) And"
                    ExSqlStr = "select Description as CheckList,Remarks as Description,sfm.value as Frequency,interval as Interval,Convert(varchar(11),AffectedOn,105) as [Effected Date],Convert(varchar(11),ApprovedDate,105) as [Approved Date] from Surve_ChkList_Tracker stm inner join Surve_Frequency_Master sfm  on sfm.id=stm.Frequency where 1=1 And ApprovedDate = (SELECT MAX(ApprovedDate) FROM Surve_chklist_Tracker Where checklistid = stm.checklistid) And"
                    StrWhere = " approvedstatus =2"
                End If
            ElseIf Item = 1 Then
                SqlStr = "SELECT distinct  taskID,Taskdesc,Remarks,weightage,approvedDate from Surve_task_Master  where checklistid = " + Checklist.ToString() + " And "
                ExSqlStr = "SELECT distinct Taskdesc as Task,Remarks as Description,weightage as Weightage,Convert(varchar(11),approvedDate,105) as [Approved Date] from Surve_task_Master  where checklistid = " + Checklist.ToString() + " And "
                If (Type = -1) Then
                    StrWhere = " status_id =1"
                ElseIf (Type = 1) Then
                    StrWhere = " status_id =-1"
                ElseIf (Type = 2) Then
                    StrWhere = " status_id =999"
                ElseIf (Type = 3) Then
                    StrWhere = " approvedstatus =1"
                ElseIf (Type = 4) Then
                    SqlStr = "SELECT distinct  taskID,Taskdesc,Remarks,weightage,approvedDate from Surve_task_Tracker s where checklistid = " + Checklist.ToString() + " And ApprovedDate = (SELECT MAX(ApprovedDate) FROM Surve_task_Tracker Where taskID = s.taskID) and "
                    ExSqlStr = "SELECT distinct Taskdesc as Task,Remarks as Description,weightage as Weightage,Convert(varchar(11),approvedDate,105) as [Approved Date] from Surve_task_Tracker s where checklistid = " + Checklist.ToString() + " And ApprovedDate = (SELECT MAX(ApprovedDate) FROM Surve_task_Tracker Where taskID = s.taskID) and "
                    'SqlStr = "SELECT distinct  taskID,Taskdesc,Frequency,Remarks,approvedDate from Surve_task_Tracker  where checklistid = " + Checklist.ToString() + " And "
                    StrWhere = " approvedstatus =2"
                End If
            End If
            SqlStr += StrWhere
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            ExSqlStr += StrWhere
            DTExcel = DB.ExecuteDataSet(ExSqlStr).Tables(0)
            If ViewType = 1 Then
                Export_Excel_Click()
            End If

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver



                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid

                If Item = -1 Then
                    RH.AddColumn(TR3, TR3_00, 10, 10, "l", i.ToString())
                    RH.AddColumn(TR3, TR3_01, 20, 20, "l", DR(0).ToString())
                    RH.AddColumn(TR3, TR3_02, 30, 30, "l", DR(1).ToString())
                    RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_04, 10, 10, "l", DR(3).ToString())
                    RH.AddColumn(TR3, TR3_05, 10, 10, "l", CDate(DR(4)).ToString("dd/MM/yyyy"))
                    RH.AddColumn(TR3, TR3_06, 10, 10, "l", CDate(DR(5)).ToString("dd/MM/yyyy"))
                Else
                    RH.AddColumn(TR3, TR3_00, 10, 10, "l", i.ToString())
                    RH.AddColumn(TR3, TR3_01, 30, 30, "l", DR(1).ToString())
                    RH.AddColumn(TR3, TR3_02, 30, 30, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_03, 20, 20, "l", DR(3).ToString())
                    'RH.AddColumn(TR3, TR3_04, 10, 10, "l", DR(4).ToString())
                    'RH.AddColumn(TR3, TR3_05, 10, 10, "l", CDate(DR(4)).ToString("dd/MM/yyyy"))
                    RH.AddColumn(TR3, TR3_06, 10, 10, "l", CDate(DR(4)).ToString("dd/MM/yyyy"))
                End If
                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Assigned Branches Report"

            WebTools.ExporttoExcel(DTExcel, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Protected Sub Export_Excel_Click()
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Checklist_Report"

            WebTools.ExporttoExcel(DTExcel, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
