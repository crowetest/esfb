﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewSignatureRequestStatus
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1457) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim DTT As New DataTable
            Dim StrType As String = ""
            Dim StrEmpCode As String = ""
            Dim StrMode As String = ""

            Dim Struser As String = Session("UserID").ToString()

            If (Request.QueryString.Get("Type")) Is Nothing = False Then
                StrType = GF.Decrypt(Request.QueryString.Get("Type"))
            Else
                StrType = 1
            End If
            If Request.QueryString.Get("Empcode") Is Nothing = False Then
                StrEmpCode = GF.Decrypt(Request.QueryString.Get("Empcode"))
            Else
                StrEmpCode = Struser
            End If
            If (Request.QueryString.Get("Mode")) Is Nothing = False Then
                StrMode = GF.Decrypt(Request.QueryString.Get("Mode"))
            Else
                StrMode = "2"
            End If


            'Dim StrFromDate As String = ""
            'If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
            '    StrFromDate = CStr(Request.QueryString.Get("frmdate"))
            'End If
            'Dim StrToDate As String = ""
            'If CStr(Request.QueryString.Get("todate")) <> "1" Then
            '    StrToDate = CStr(Request.QueryString.Get("todate"))
            'End If


            Dim StrSubQry As String = ""
            Dim StrSubQry1 As String = ""
            Dim StrQuery As String = " "



            If StrType = 1 Then
                StrSubQry += " and A.Emp_code=" & StrEmpCode & ""
                StrSubQry1 += " and A.Emp_code=" & StrEmpCode & ""
            End If

            If StrType = 1 Then
                StrSubQry += " and (isnull(A.checker1_status,0) =0 or isnull(A.checker_status,0) =0)  "
                StrSubQry1 += " and (isnull(A.checker1_status,0) =0 or isnull(A.checker2_status,0) =0)  "

            End If

            Dim SqlStr As String


            SqlStr = "select 'Upload' as Type,A.Emp_code, B.Emp_name, Maker_dt as [Request_Date],Maker_Remarks as Employee_Remark," +
                " Case when Checker1_Status = 1 and Checker_Status = 1 then 'HR Approved' " +
                " when Checker1_Status = 1 and Checker_Status = 2 then 'HR Rejected' " +
                " when Checker1_Status = 1 and isnull(Checker_Status,0) = 0 then 'Pending in HR Queue' " +
                " when Checker1_Status = 2 then 'Reporting Officer Rejected'  " +
                " when isnull(Checker1_Status,0) = 0  then 'Pending in Reporting Officer Queue' " +
                " end + " +
                " Case when Checker1_Status = 1 and Checker_Status in( 1,2) then '(' + convert(varchar,Checker_Id) + ' - ' + D.Emp_Name + ')'" +
                " when Checker1_Status = 1 and isnull(Checker_Status,0) = 0 then '' " +
                " when Checker1_Status = 2 then '(' + convert(varchar,Checker1_Id) + ' - ' + C.Emp_Name + ')'" +
                " when isnull(Checker1_Status,0) = 0  then '(' + convert(varchar,B.Reporting_to) + ' - ' + E.Emp_Name + ')'" +
                " end as Approve_Status," +
                " Case when Checker1_Status = 1 and Checker_Status in( 1,2) then convert(varchar,Checker_Dt)" +
                " when Checker1_Status in(2) and isnull(Checker_Status,0) = 0 then convert(varchar,Checker1_Dt) " +
                " else '' " +
                " end as Date," +
                " Case when isnull(Checker_Status,0) in( 1,2) then Checker_Remarks" +
                " when isnull(Checker1_Status,0) in( 1,2) then Checker1_Remarks " +
                " else '' " +
                " end as Remarks" +
                " from sign_master A " +
                " inner join Emp_master B on A.emp_code = B.Emp_code" +
                " left join Emp_master C on A.Checker1_Id = C.Emp_code " +
                " left join Emp_master D on A.Checker_Id = D.Emp_code " +
                " left join Emp_master E on B.Reporting_to = E.Emp_code " +
                " Where 1 = 1 " + StrSubQry +
                " Union all " +
                " select 'Change Status' as Type,A.Emp_code, B.Emp_name, Maker_dt as [Request_Date],Maker_Remarks as Employee_Remark," +
                " Case when Checker1_Status = 1 and Checker2_Status = 1 then 'HR Approved' " +
                " when Checker1_Status = 1 and Checker2_Status = 2 then 'HR Rejected' " +
                " when Checker1_Status = 1 and isnull(Checker2_Status,0) = 0 then 'Pending in HR Queue' " +
                " when Checker1_Status = 2 then 'Reporting Officer Rejected' " +
                " when isnull(Checker1_Status,0) = 0  then 'Pending in Reporting Officer Queue' " +
                " end + " +
                " Case when Checker1_Status = 1 and Checker2_Status in( 1,2) then '(' + convert(varchar,Checker2_Id) + ' - ' + D.Emp_Name + ')'" +
                " when Checker1_Status = 1 and isnull(Checker2_Status,0) = 0 then '' " +
                " when Checker1_Status = 2 then '(' + convert(varchar,Checker1_Id) + ' - ' + C.Emp_Name + ')'" +
                " when isnull(Checker1_Status,0) = 0  then '(' + convert(varchar,B.Reporting_to) + ' - ' + E.Emp_Name + ')'" +
                " end as Approve_Status," +
                " Case when Checker1_Status = 1 and Checker2_Status in( 1,2) then convert(varchar,Checker2_Dt)" +
                " when Checker1_Status in(2) and isnull(Checker2_Status,0) = 0 then convert(varchar,Checker1_Dt )" +
                " else '' " +
                " end as Date," +
                " Case when isnull(Checker2_Status,0) in( 1,2) then Checker2_Remarks" +
                " when isnull(Checker1_Status,0) in( 1,2) then Checker1_Remarks " +
                " else '' " +
                " end as Remarks" +
                " from sign_status_change A  " +
                " inner join Emp_master B on A.emp_code = B.Emp_code " +
                " left join Emp_master C on A.Checker1_Id = C.Emp_code " +
                " left join Emp_master D on A.Checker2_Id = D.Emp_code " +
                " left join Emp_master E on B.Reporting_to = E.Emp_code " +
                " Where 1 = 1 " + StrSubQry1


            'SqlStr += "   order by a.REQUEST_ID"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim b As Integer = DT.Rows.Count

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RH.Heading(Session("FirmName"), tb, "SIGNATURE PROCESS STATUS ", 150)

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            '
            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            Dim StrUserNam As String = Nothing

            DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
            If DTT.Rows.Count > 0 Then
                StrUserNam = DTT.Rows(0)(0)
            End If
            Dim TRHead_1 As New TableRow

            RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)


            RH.BlankRow(tb, 4)
            tb.Controls.Add(TRSHead)



            'TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"



            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver


            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid

            TRHead_1_00.Font.Bold = True
            TRHead_1_01.Font.Bold = True
            TRHead_1_02.Font.Bold = True
            TRHead_1_03.Font.Bold = True
            TRHead_1_04.Font.Bold = True
            TRHead_1_05.Font.Bold = True
            TRHead_1_06.Font.Bold = True
            TRHead_1_07.Font.Bold = True

            RH.AddColumn(TRHead_1, TRHead_1_00, 5, 8, "c", "Type") '
            RH.AddColumn(TRHead_1, TRHead_1_01, 5, 8, "c", "Emp Code")
            RH.AddColumn(TRHead_1, TRHead_1_02, 5, 20, "c", "Name")
            RH.AddColumn(TRHead_1, TRHead_1_03, 5, 8, "c", "Request Date")
            RH.AddColumn(TRHead_1, TRHead_1_04, 4, 15, "c", "Employee Remark")
            RH.AddColumn(TRHead_1, TRHead_1_05, 8, 20, "c", "Status")
            RH.AddColumn(TRHead_1, TRHead_1_06, 6, 10, "c", "Date")
            RH.AddColumn(TRHead_1, TRHead_1_07, 5, 15, "c", "Approval Remark")

            tb.Controls.Add(TRHead_1)

            For Each DR In DT.Rows

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 8, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_01, 5, 8, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_02, 5, 20, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_03, 5, 8, "l", CDate(DR(3)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_04, 4, 15, "c", DR(4).ToString())
                RH.AddColumn(TR3, TR3_05, 8, 20, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_06, 6, 10, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_07, 5, 15, "l", DR(7).ToString())

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)

        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        'Try
        WebTools.ExporttoExcel(DT, "Request")
        'Catch ex As Exception
        '    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
        '    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        'End Try
    End Sub
End Class
