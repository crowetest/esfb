﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="MoratoriumStatusReport.aspx.vb" Inherits="Operations_Reports_MoratoriumStatusReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

<br />
<table align="center" style="width: 40%; text-align:center; margin:0px auto;">
    <tr>
        <td style="width:15%;">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
        </td>
        <td  style="width:85%;">
        </td>
    </tr>      
            
    <tr>           
        <td style="width:15%; text-align:left;">
            Branch
        </td>
        <td style="width:85%">
            <asp:TextBox ID="txtbranch" class="NormalText" runat="server" 
                    Width="85%" onkeypress="return false" Height="16px" ></asp:TextBox>
        </td> 
    </tr>   
     <tr>
           
            <td style="width:15%; text-align:left;">
                From Date </td>
            <td style="width:85%">
                <asp:TextBox ID="txtFromDt" class="NormalText" runat="server" 
                    Width="85%" onkeypress="return false" Height="16px" ></asp:TextBox>
                <asp:CalendarExtender ID="txtFromDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtFromDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>

            </td> 
 </tr>                 
         <tr>
           
            <td style="width:15%; text-align:left;">
                To Date </td>
            <td style="width:85%">
                <asp:TextBox ID="txtToDt" class="NormalText" runat="server" 
                    Width="85%" onkeypress="return false" Height="16px" ></asp:TextBox>
                <asp:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtToDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>

            </td> 
 </tr>         
    <tr>
        <td style="width:15%; text-align:left;"></td>
        <td style="width:85%">
            <br />           
            <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 20%" type="button" value="VIEW"   onclick="return btnView_onclick()" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input id="cmd_Export_Excel" style="font-family: Cambria; cursor: pointer; width: 22%;" type="button" value="VIEW EXCEL" onclick="return btnExcelView_onclick()" />
            &nbsp;&nbsp;
            <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 20%" type="button" value="EXIT"  onclick="return btnExit_onclick()" />
        </td>
        <td style="width:100%; height: 18px; text-align:center;"></td>
    </tr>       
    <tr>
        <td style="width:15%;">&nbsp;</td>
        <td  style="width:85%;">
            <asp:HiddenField ID="hdnReportID" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="width:15%;">&nbsp;</td>
        <td  style="width:85%;">               
            <asp:HiddenField ID="hdnValue" runat="server" />
        </td>               
    </tr>
</table>
<script language="javascript" type="text/javascript">
    function FromServer(arg, context) {
                
    }
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 1; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function btnView_onclick() 
    {
        var Branch_Code= document.getElementById("<%= txtbranch.ClientID %>").value;
        var FromDate= document.getElementById("<%= txtFromDt.ClientID %>").value;
             var FromDt = new Date(FromDate);
         
            var ToDate = document.getElementById("<%= txttODt.ClientID %>").value;
             var ToDt = new Date(ToDate);


             if (ToDt < FromDt) {
                 alert("To date should be greater than From date");
                 return false;
             }
        if (Branch_Code == -1)
        {
            alert("Select Branch");
        }
        else
        {
            window.open("ViewMoratoriumStatusReport.aspx?ViewType=-1 &Branch_Code=" + Branch_Code + "&FDT=" + FromDate + "&TDT=" + ToDate + " ", "_self");
        }
    }   
    function btnExcelView_onclick() 
    {
        var Branch_Code= document.getElementById("<%= txtbranch.ClientID %>").value;
      var FromDate= document.getElementById("<%= txtFromDt.ClientID %>").value;
             var FromDt = new Date(FromDate);
         
            var ToDate = document.getElementById("<%= txttODt.ClientID %>").value;
             var ToDt = new Date(ToDate);


             if (ToDt < FromDt) {
                 alert("To date should be greater than From date");
                 return false;
             }
        if (Branch_Code == -1)
        {
            alert("Select Branch");
        }
        else
        {
            window.open("ViewMoratoriumStatusReport.aspx?ViewType=-1 &Branch_Code=" + Branch_Code + "&FDT=" + FromDate + "&TDT=" + ToDate + " ", "_self");
        }
    }    
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }  
</script>



</asp:Content>




