﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class Operations_Reports_MoratoriumStatusReport
    Inherits System.Web.UI.Page

    Implements Web.UI.ICallbackEventHandler
        Dim CallBackReturn As String = Nothing
        Dim DT As New DataTable
        Dim GF As New GeneralFunctions
        Dim GMASTER As New Master
        Dim TraDt As Date
        Dim DB As New MS_SQL.Connect
        Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim strWhere As String = ""
            Try
            'If GF.FormAccess(CInt(Session("UserID")), 1419) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Me.Master.subtitle = "BranchWise Moratorium Repayment Status Report"
                If Not IsPostBack Then

                txtbranch.Text = CStr(Session("BranchName"))


            End If
                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub
        Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
            GC.Collect()
            DT.Dispose()
            DB.dispose()
        End Sub
#End Region
#Region "Events"
#End Region
#Region "Call Back"
        Public Function GetCallbackResult() As String Implements ICallbackEventHandler.GetCallbackResult
            Return CallBackReturn
        End Function
        Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements ICallbackEventHandler.RaiseCallbackEvent
            Dim Data() As String = eventArgument.Split(CChar("Ø"))
            If CInt(Data(0)) = 1 Then

            End If
        End Sub
#End Region
    End Class







