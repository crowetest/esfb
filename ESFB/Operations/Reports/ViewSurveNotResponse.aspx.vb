﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewSurveNotResponse
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTEXCEL As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim DT As New DataTable
            Dim ViewType, BranchID, Freq_ID, Checklist_ID As Integer
            Dim FromDate, ToDate As Date
            Dim TDay As Date = CDate(Session("TraDt"))

            BranchID = CInt(Request.QueryString.Get("BID"))
            Freq_ID = CInt(Request.QueryString.Get("FID"))
            Checklist_ID = CInt(Request.QueryString.Get("CID"))

            FromDate = CDate(Request.QueryString.Get("FDT"))
            Dim DateFrom As String
            DateFrom = FromDate.ToString("yyyy-MM-dd")

            ViewType = CInt(Request.QueryString.Get("ViewType"))


            ToDate = CDate(Request.QueryString.Get("TDT"))
            If ToDate = TDay Then
                ToDate = DateAdd(DateInterval.Day, -1, ToDate)
            End If

            Dim DateTo As String
            DateTo = ToDate.ToString("yyyy-MM-dd")

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RH.Heading(Session("FirmName"), tb, "Not Responed Branches Report", 100)
            tb.Attributes.Add("width", "100%")



            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid

            TRHead_00.Font.Bold = True
            TRHead_01.Font.Bold = True
            TRHead_02.Font.Bold = True
            TRHead_03.Font.Bold = True
            TRHead_04.Font.Bold = True
            TRHead_05.Font.Bold = True

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "SI No")
            'RH.AddColumn(TRHead, TRHead_01, 4, 4, "c", "Branch Code")
            'RH.AddColumn(TRHead, TRHead_02, 10, 10, "c", "Branch Name")
            RH.AddColumn(TRHead, TRHead_03, 5, 5, "c", "Frequency")
            RH.AddColumn(TRHead, TRHead_04, 35, 35, "c", "Checklist")
            RH.AddColumn(TRHead, TRHead_05, 10, 10, "c", "Date")


            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer
            Dim Branch_Id As String = ""

            Dim StrWhere As String = " 1=1 "
            If (BranchID <> -1) Then
                StrWhere += " and A.Branch_id = " & BranchID & ""
            End If
            If (Freq_ID <> -1) Then
                StrWhere += " and Frequency = " & Freq_ID & ""
            End If
            If (Checklist_ID <> -1) Then
                StrWhere += " and A.checklistid = " & Checklist_ID & ""
            End If
            StrWhere += " and B.NextRespdate between '" & DateFrom & "' and '" & DateTo & "'"


            DT = DB.ExecuteDataSet("select A.branch_id ,Branch_name,A.checklistid, Description, Frequency,D.value,isnull(convert(varchar(10),B.NextRespdate,105),'')  from (select A.*,Branch_id, Branch_name from  Surve_ChkList_Master A , Branch_master D Where A.status_id = 1 and branch_type in (1,3)  ) A " +
                " inner join Surve_Checklist_RespDate_Dtl B on A.checklistid= B.checklistid  " +
                " inner join Surve_Frequency_Master D on A.Frequency = D.Id " +
                " left join  Surve_Branch_Response_master C on A.checklistid= C.checklistid and B.NextRespDate = C.NextRespDate and A.Branch_id = C.Branch_id " +
                " where C.checklistid Is NULL And " + StrWhere + " " +
                " order by A.branch_name,Description").Tables(0)

            DTEXCEL = DB.ExecuteDataSet("select A.branch_id as [Branch ID],Branch_name as [Branch Name], Description as [Checklist], Frequency as [Frequency],convert(varchar(10),B.NextRespdate,105) as Date  from (select A.*,Branch_id, Branch_name from  Surve_ChkList_Master A , Branch_master D Where A.status_id = 1  and branch_type in (1,3) ) A " +
                " inner join Surve_Checklist_RespDate_Dtl B on A.checklistid= B.checklistid  " +
                " inner join Surve_Frequency_Master D on A.Frequency = D.Id " +
                " left join  Surve_Branch_Response_master C on A.checklistid= C.checklistid and B.NextRespDate = C.NextRespDate and A.Branch_id = C.Branch_id " +
                " where C.checklistid Is NULL And " + StrWhere + " " +
                " order by A.branch_name,Description").Tables(0)

            If ViewType = 1 Then
                Export_Excel_Click()
            End If

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver



                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid

                If Branch_Id <> DR(0).ToString() Then
                    RH.BlankRow(tb, 3)
                    RH.SubHeading(tb, 100, "l", DR(1).ToString() + " - " + DR(0).ToString())
                    tb.Attributes.Add("width", "100%")
                End If

                Branch_Id = DR(0).ToString()

                RH.AddColumn(TR3, TR3_00, 5, 5, "l", i.ToString())
                'RH.AddColumn(TR3, TR3_01, 4, 4, "l", DR(0).ToString())
                'RH.AddColumn(TR3, TR3_02, 10, 10, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 5, 5, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_04, 35, 35, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_05, 10, 10, "l", DR(6).ToString())

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Assigned Branches Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Protected Sub Export_Excel_Click()
        Try
            Dim HeaderText As String
            HeaderText = "Not_Responded_Branch_Response_Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
