﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="ChecklistReport.aspx.vb" Inherits="ChecklistReport"  %>

<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css" />
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
    <tr>
            <td style="width:15%;">
                Item</td>
           <td  style="width:85%;">
                 <asp:DropDownList ID="cmbItem" class="NormalText" runat="server" Font-Names="Cambria"
         Width="90%" ForeColor="Black" >
         <asp:ListItem Value="-1">Checklist</asp:ListItem>
         <asp:ListItem Value="1">Task</asp:ListItem>
              
                </asp:DropDownList>
                 <br />
                </td>
           
        </tr>
    <tr  id="trstatus" style="display:none">
    <td style="width:15%;">
    Checklist</td>
    <td  style="width:85%;">
    <asp:DropDownList ID="cmbChecklist"  class="NormalText" runat="server" Font-Names="Cambria" 
    Width="90%" ForeColor="Black">
    </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td style="width:15%;">
    Type</td>
    <td  style="width:85%;">
    <asp:DropDownList ID="cmbType" class="NormalText" runat="server" Font-Names="Cambria"
    Width="90%" ForeColor="Black" >
     <asp:ListItem Value="-1">Active</asp:ListItem>
         <asp:ListItem Value="1">Inactive</asp:ListItem>
         <asp:ListItem Value="2">Deleted</asp:ListItem>
         <asp:ListItem Value="3">Approved</asp:ListItem>
         <asp:ListItem Value="4">Rejected</asp:ListItem>
    </asp:DropDownList>
    </td>
    </tr>

           

 
       
         <tr>
            <td style="width:30%;">
               <%-- Discussion Status--%></td>
            <td  style="width:85%;">
             
                 </td>
           
        </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
             
              <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;
                <input id="cmd_Export_Excel" style="font-family: Cambria; cursor: pointer; width: 87px;" 
                type="button" value="VIEW EXCEL" onclick="return btnExcelView_onclick()" />&nbsp;&nbsp;
                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 10%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
          
      
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
               
                 <asp:HiddenField ID="hdnValue" runat="server" />
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
     
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }

     
      
         function ItemOnChange()
            {
                    
              var Item = document.getElementById("<%= cmbItem.ClientID %>").value;
//               
              
              if(document.getElementById("<%= cmbItem.ClientID %>").value==-1)
              {
                   
                    document.getElementById("trstatus").style.display = 'none';

              }
              else
              {
               
                document.getElementById("trstatus").style.display ='';
               }
               if(Item!="")
                { 
                
                  ToServer("1Ø" + Item, 1);
                }  
               
            }
            
            //ViewType = -1 for normal report view
            //ViewType = 1 for excel report view
    function btnView_onclick() 
        {
            var Item = document.getElementById("<%= cmbItem.ClientID %>").value;  
            var Type = document.getElementById("<%= cmbType.ClientID %>").value;  
            var Checklist = document.getElementById("<%= cmbChecklist.ClientID %>").value; 
                        window.open("ViewChecklistReport.aspx?ViewType=-1 &Type=" + Type +" &Checklist=" +  Checklist + "&Item=" + Item +" ", "_self");                   
       }    
   function btnExcelView_onclick() 
        {
            var Item = document.getElementById("<%= cmbItem.ClientID %>").value;  
            var Type = document.getElementById("<%= cmbType.ClientID %>").value;  
            var Checklist = document.getElementById("<%= cmbChecklist.ClientID %>").value; 
                        window.open("ViewChecklistReport.aspx?ViewType=1 &Type=" + Type +" &Checklist=" +  Checklist + "&Item=" + Item +" ", "_self");                   
       }      
   function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
       
     function FromServer(arg, context) {

         switch (context) {
            case 1:
                {                   
                            ComboFill(arg, "<%= cmbChecklist.ClientID %>");
                            break;
                }
             
               
            case 2:
               {    
                         
                    
                    
                }                            
              
             }
            }

    </script>
</asp:Content>

 