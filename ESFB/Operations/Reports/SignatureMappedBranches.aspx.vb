﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class SignatureMappedBranches
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim TraDt As Date
    Dim DB As New MS_SQL.Connect
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1208) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Me.Master.subtitle = "Signature Mapped Report"
            If Not IsPostBack Then
                Me.txtFromDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
                DT = DB.ExecuteDataSet(" SELECT '-1' AS Branch_id,'------All------' AS Branch_name UNION ALL SELECT distinct Branch_id,Branch_name from BRANCH_MASTER where status_id=1").Tables(0)
                GF.ComboFill(cmbBranch, DT, 0, 1)
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub

#End Region
#Region "Events"

#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        
    End Sub
#End Region


End Class

