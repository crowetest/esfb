﻿Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.Drawing.Printing
Imports System.Drawing
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Diagnostics
Partial Class CCLApplicationView
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim pageIndex As Integer = 0
    Dim streams As IList(Of Stream)
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim ProcessId As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ReportViewer1.ProcessingMode = ProcessingMode.Local
                Dim centerID As String
                centerID = CStr(Request.QueryString.Get("center"))
                Dim ID As String
                ID = CStr(Request.QueryString.Get("ID"))
                Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@centerID", centerID), New System.Data.SqlClient.SqlParameter("@id", ID)}
                Dim ds As New DataTable()
                ds = DB.ExecuteDataSet("SP_CCL_FORM_DATA", Parameters).Tables(0)
                If ds.Rows.Count < 15 Then
                    Dim addCount As Integer = 15 - ds.Rows.Count
                    For n As Integer = 1 To addCount
                        Dim dr As DataRow = ds.NewRow
                        dr(0) = ""
                        dr(1) = ""
                        dr(2) = ""
                        dr(3) = ""
                        ds.Rows.Add(dr)
                    Next
                End If
                Dim rptds As ReportDataSource
                rptds = New ReportDataSource("ESFBDataSet", ds)
                rptds.Name = "DataSet1"

                Dim ds1 As New DataTable()
                ds1 = DB.ExecuteDataSet("SELECT DISTINCT CENTER_NAME + ' ( '+CENTER_ID+' ) ' FROM CCL_MASTER_DATA WHERE CENTER_ID = " + centerID).Tables(0)
                Dim rptds1 As ReportDataSource
                rptds1 = New ReportDataSource("ESFBDataSet", ds1)
                rptds1.Name = "DataSet2"

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Operations/Reports/CCL_FORM.rdlc")
                ReportViewer1.LocalReport.DataSources.Clear()
                Dim rp As ReportParameter = New ReportParameter("Center_ID", ds1.Rows(0)(0).ToString())
                ReportViewer1.LocalReport.SetParameters(rp)
                ReportViewer1.LocalReport.DataSources.Add(rptds)
                ReportViewer1.LocalReport.DataSources.Add(rptds1)
                ReportViewer1.LocalReport.Refresh()
                ReportViewer1.Visible = False
                Show_PDF()
                'Response.Redirect("CCL_Application.aspx", False)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Show_PDF()
        Try

            Dim deviceInfo As String = "<DeviceInfo>" + _
                                      "  <OutputFormat>EMF</OutputFormat>" + _
                                      "  <PageWidth>10.2in</PageWidth>" + _
                                      "  <PageHeight>11in</PageHeight>" + _
                                      "  <MarginTop>0.25in</MarginTop>" + _
                                      "  <MarginLeft>0.25in</MarginLeft>" + _
                                      "  <MarginRight>0.25in</MarginRight>" + _
                                      "  <MarginBottom>0.25in</MarginBottom>" + _
                                      "</DeviceInfo>"
            Dim warnings As Warning()
            Dim streamids As String()
            Dim mimeType As String = "application/Report.pdf"
            Dim encoding As String = String.Empty
            Dim filenameExtension As String = String.Empty
            Dim pdfContent As Byte() = ReportViewer1.LocalReport.Render("PDF", deviceInfo, mimeType, encoding, filenameExtension, streamids, warnings)
            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "Report.pdf")
            Response.BinaryWrite(pdfContent)
            Response.Flush()
        Catch ex As Exception
            MsgBox("Exception", MsgBoxStyle.Information, "")
        End Try
    End Sub
    Private Sub Printdoc()
        'ReportViewer1.print()
        Export(ReportViewer1.LocalReport)
        'pageIndex = 0
        'Print()
        Dim deviceInfo As String = "<DeviceInfo>" + _
                                  "  <OutputFormat>EMF</OutputFormat>" + _
                                  "  <PageWidth>8.5in</PageWidth>" + _
                                  "  <PageHeight>11in</PageHeight>" + _
                                  "  <MarginTop>0.25in</MarginTop>" + _
                                  "  <MarginLeft>0.25in</MarginLeft>" + _
                                  "  <MarginRight>0.25in</MarginRight>" + _
                                  "  <MarginBottom>0.25in</MarginBottom>" + _
                                  "</DeviceInfo>"
        Dim pdfContent As Byte() = ReportViewer1.LocalReport.Render("Image", deviceInfo, Nothing, Nothing, Nothing, Nothing, Nothing)

        'Creatr PDF file on disk
        Dim pdfPath As String = "C:\temp\report.pdf"
        Dim pdfFile As New System.IO.FileStream(pdfPath, System.IO.FileMode.Create)
        pdfFile.Write(pdfContent, 0, pdfContent.Length)
        pdfFile.Close()
        Process.Start(pdfPath)
    End Sub

    Private Sub Export(ByVal report As LocalReport)
        Dim deviceInfo As String = "<DeviceInfo>" + _
                                  "  <OutputFormat>PDF</OutputFormat>" + _
                                  "  <PageWidth>8.5in</PageWidth>" + _
                                  "  <PageHeight>11in</PageHeight>" + _
                                  "  <MarginTop>0.25in</MarginTop>" + _
                                  "  <MarginLeft>0.25in</MarginLeft>" + _
                                  "  <MarginRight>0.25in</MarginRight>" + _
                                  "  <MarginBottom>0.25in</MarginBottom>" + _
                                  "</DeviceInfo>"
        Dim warnings() As Warning
        streams = New List(Of Stream)
        report.Render("Image", deviceInfo, AddressOf CreateStream, warnings)
        For Each stream As Stream In streams
            stream.Position = 0
        Next

    End Sub

    Private Function CreateStream(ByVal name As String, ByVal fileNameExtension As String, ByVal encoding As Encoding, ByVal mimeType As String, ByVal willSeek As Boolean) As Stream
        Dim stream As Stream = New FileStream((Server.MapPath("~/Files/") _
                        + (name + ("." + fileNameExtension))), FileMode.OpenOrCreate)
        streams.Add(stream)
        Return stream
    End Function

    Private Sub PrintPage(ByVal sender As Object, ByVal ev As PrintPageEventArgs)
        Dim pageImage As Metafile = New Metafile(streams(pageIndex))
        ev.Graphics.DrawImage(pageImage, ev.PageBounds)
        pageIndex = (pageIndex + 1)
        ev.HasMorePages = (pageIndex < streams.Count)
    End Sub

    Private Overloads Sub Print()
        If ((streams Is Nothing) _
                    OrElse (streams.Count = 0)) Then
            Return
        End If

        Dim printDoc As PrintDocument = New PrintDocument
        AddHandler printDoc.PrintPage, AddressOf Me.PrintPage
        printDoc.Print()
    End Sub
End Class
