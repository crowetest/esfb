﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Operations_Reports_ViewBranchwiseMoratoriumStatus
    Inherits System.Web.UI.Page

    Dim DB As New MS_SQL.Connect
        Dim DT As New DataTable
        Dim DTEXCEL As New DataTable
        Dim DT1 As New DataTable
        Dim RH As New WholeHelper.ClsRepCtrl
        Dim tb As New Table
        Dim PostID As Integer
        Dim GN As New GeneralFunctions
        Dim WebTools As New WebApp.Tools
        Dim TraDt As Date

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim DT As New DataTable
                Dim ViewType As Integer
                ViewType = CInt(Request.QueryString.Get("ViewType"))
            Dim FromDate, ToDate As Date
            Dim TDay As Date = CDate(Session("TraDt"))
            Dim strwhere As String
            Dim branchID = Request.QueryString.Get("Branch_Code")
            If branchID <> -1 Then
                strwhere = " where a.BranchCode = " + branchID


            End If

            FromDate = CDate(Request.QueryString.Get("FDT"))
            Dim DateFrom As String
            DateFrom = FromDate.ToString("yyyy-MM-dd")

            ToDate = CDate(Request.QueryString.Get("TDT"))
            'If ToDate = TDay Then
            '    ToDate = DateAdd(DateInterval.Day, -1, ToDate)
            'End If

            Dim DateTo As String
            DateTo = ToDate.ToString("yyyy-MM-dd")

            strwhere += " and a.SubmittedDate between '" & DateFrom & "' and '" & DateTo & "'"
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RH.Heading(Session("FirmName"), tb, "Branchwise Moratorium Repayment Status Report", 100)
            tb.Attributes.Add("width", "100%")

                Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.WhiteSmoke

                Dim RowBG As Integer = 0
                Dim DR As DataRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10 As New TableCell

            TRHead_00.BorderWidth = "1"
                TRHead_01.BorderWidth = "1"
                TRHead_02.BorderWidth = "1"
                TRHead_03.BorderWidth = "1"
                TRHead_04.BorderWidth = "1"
                TRHead_05.BorderWidth = "1"
                TRHead_06.BorderWidth = "1"
                TRHead_07.BorderWidth = "1"
                TRHead_08.BorderWidth = "1"
                TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            ' TRHead_11.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
                TRHead_01.BorderColor = Drawing.Color.Silver
                TRHead_02.BorderColor = Drawing.Color.Silver
                TRHead_03.BorderColor = Drawing.Color.Silver
                TRHead_04.BorderColor = Drawing.Color.Silver
                TRHead_05.BorderColor = Drawing.Color.Silver
                TRHead_06.BorderColor = Drawing.Color.Silver
                TRHead_07.BorderColor = Drawing.Color.Silver
                TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            ' TRHead_11.BorderColor = Drawing.Color.Silver


            TRHead_00.BorderStyle = BorderStyle.Solid
                TRHead_01.BorderStyle = BorderStyle.Solid
                TRHead_02.BorderStyle = BorderStyle.Solid
                TRHead_03.BorderStyle = BorderStyle.Solid
                TRHead_04.BorderStyle = BorderStyle.Solid
                TRHead_05.BorderStyle = BorderStyle.Solid
                TRHead_06.BorderStyle = BorderStyle.Solid
                TRHead_07.BorderStyle = BorderStyle.Solid
                TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            ' TRHead_11.BorderStyle = BorderStyle.Solid

            TRHead_00.Font.Bold = True
                TRHead_01.Font.Bold = True
                TRHead_02.Font.Bold = True
                TRHead_03.Font.Bold = True
                TRHead_04.Font.Bold = True
                TRHead_05.Font.Bold = True
                TRHead_06.Font.Bold = True
                TRHead_07.Font.Bold = True
                TRHead_08.Font.Bold = True
            TRHead_09.Font.Bold = True
            TRHead_10.Font.Bold = True
            ' TRHead_11.Font.Bold = True



            RH.AddColumn(TRHead, TRHead_00, 4, 4, "c", "Sl.No")
            RH.AddColumn(TRHead, TRHead_01, 8, 8, "c", "BranchID")
            RH.AddColumn(TRHead, TRHead_02, 14, 14, "c", "BranchName")
            RH.AddColumn(TRHead, TRHead_03, 8, 8, "c", "CentreID")
            RH.AddColumn(TRHead, TRHead_04, 14, 14, "c", "CentreName")
            RH.AddColumn(TRHead, TRHead_05, 8, 8, "c", "ClientID")
            RH.AddColumn(TRHead, TRHead_06, 14, 14, "c", "ClientName")
            RH.AddColumn(TRHead, TRHead_07, 8, 8, "c", "Repayment Start Date")
            RH.AddColumn(TRHead, TRHead_08, 8, 8, "c", "Submitted on Date")
            RH.AddColumn(TRHead, TRHead_09, 8, 8, "c", "Submitted By EmpCode")
            RH.AddColumn(TRHead, TRHead_10, 8, 8, "c", "Submitted By EmpName")
            ' RH.AddColumn(TRHead, TRHead_11, 8, 8, "c", "MeetingDay")




            tb.Controls.Add(TRHead)

                RH.BlankRow(tb, 3)
                Dim i As Integer


            DT = DB.ExecuteDataSet("select  a.BranchCode,a.BranchName,a.CenterID,a.CenterName,a.ClientID,a.ClientName,a.Repayment_Start_Date,a.SubmittedDate,a.Emp_code,b.Emp_Name,a.MeetingDay from BRD_NonMoratoriumCustomers a inner join emp_master b on a.Emp_Code=b.Emp_Code" + strwhere).Tables(0)



            DTEXCEL = DB.ExecuteDataSet("select  a.BranchCode As [BranchCode],a.BranchName As [BranchName],a.CenterID As [CentreID],a.CenterName As [CentreName],a.ClientID As [ClientID],a.ClientName As [ClientName],a.Repayment_Start_Date As [RepaymentStartDate],a.SubmittedDate As [SubmittedDate],a.Emp_code As [EmployeeCode],b.Emp_Name As [EmployeeName],a.MeetingDay As [MeetingDay] from BRD_NonMoratoriumCustomers a inner join emp_master b on a.Emp_Code=b.Emp_Code" + strwhere).Tables(0)

            If ViewType = 1 Then
                    Export_Excel_Click()
                End If

                Dim createddate As String = ""

                For Each DR In DT.Rows
                    i += 1

                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10 As New TableCell

                TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"
                    TR3_06.BorderWidth = "1"
                    TR3_07.BorderWidth = "1"
                    TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                '  TR3_11.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver
                    TR3_06.BorderColor = Drawing.Color.Silver
                    TR3_07.BorderColor = Drawing.Color.Silver
                    TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                '  TR3_11.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid
                    TR3_06.BorderStyle = BorderStyle.Solid
                    TR3_07.BorderStyle = BorderStyle.Solid
                    TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                'TR3_11.BorderStyle = BorderStyle.Solid




                RH.AddColumn(TR3, TR3_00, 4, 4, "c", i)
                RH.AddColumn(TR3, TR3_01, 8, 8, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 14, 14, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 8, 8, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_04, 14, 14, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_05, 8, 8, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_06, 14, 14, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_07, 8, 8, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_08, 8, 8, "l", DR(7).ToString())
                RH.AddColumn(TR3, TR3_09, 8, 8, "l", DR(8).ToString())
                RH.AddColumn(TR3, TR3_10, 8, 8, "l", DR(9).ToString())
                'RH.AddColumn(TR3, TR3_11, 8, 8, "l", DR(10).ToString())




                tb.Controls.Add(TR3)

                Next
                RH.BlankRow(tb, 20)
                pnDisplay.Controls.Add(tb)
            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub

        Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

        End Sub

        Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

        End Sub

        Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
            Try
                Response.ContentType = "application/pdf"
                Response.AddHeader("content-disposition", "attachment;filename=ACReasonsReport.pdf")
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Dim sw As New StringWriter()
                Dim hw As New HtmlTextWriter(sw)
                pnDisplay.RenderControl(hw)

                Dim sr As New StringReader(sw.ToString())
                Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
                Dim htmlparser As New HTMLWorker(pdfDoc)
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
                pdfDoc.Open()
                htmlparser.Parse(sr)
                pdfDoc.Close()
                Response.Write(pdfDoc)
                Response.[End]()
            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub
        Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
            Try
                Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Branchwise Moratorium Repayment Status Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
            Catch ex As Exception
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End Try
        End Sub
        Protected Sub Export_Excel_Click()
            Try
                Dim HeaderText As String
            HeaderText = "Branchwise Moratorium Repayment Status Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
            Catch ex As Exception
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End Try
        End Sub
    End Class









