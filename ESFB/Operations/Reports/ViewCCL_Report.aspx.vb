﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewCCL_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTExcel As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim ViewType As Integer
            Dim Branch As String
            Dim Center As String
            Dim FromDt As String
            Dim ToDt As String
            Dim Status As String

            ViewType = CInt(Request.QueryString.Get("ViewType"))
            Branch = Request.QueryString.Get("Branch")
            Center = Request.QueryString.Get("Center")
            FromDt = CDate(Request.QueryString.Get("FromDt")).ToString("yyyy-MM-dd")
            ToDt = CDate(Request.QueryString.Get("ToDt")).ToString("yyyy-MM-dd")
            Status = Request.QueryString.Get("Status")
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RH.Heading(Session("FirmName"), tb, "COVID Care Loan Report", 100)


            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12, TRHead_13, TRHead_14, TRHead_15, TRHead_16 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"
            TRHead_13.BorderWidth = "1"
            TRHead_14.BorderWidth = "1"
            TRHead_15.BorderWidth = "1"
            TRHead_16.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver
            TRHead_13.BorderColor = Drawing.Color.Silver
            TRHead_14.BorderColor = Drawing.Color.Silver
            TRHead_15.BorderColor = Drawing.Color.Silver
            TRHead_16.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid
            TRHead_13.BorderStyle = BorderStyle.Solid
            TRHead_14.BorderStyle = BorderStyle.Solid
            TRHead_15.BorderStyle = BorderStyle.Solid
            TRHead_16.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 3, 3, "l", "SL No")
            RH.AddColumn(TRHead, TRHead_01, 5, 5, "l", "Branch ID")
            RH.AddColumn(TRHead, TRHead_02, 9, 9, "l", "Branch Name")
            RH.AddColumn(TRHead, TRHead_03, 6, 6, "l", "State")
            RH.AddColumn(TRHead, TRHead_04, 7, 7, "l", "Center ID")
            RH.AddColumn(TRHead, TRHead_05, 9, 9, "l", "Center Name")
            RH.AddColumn(TRHead, TRHead_06, 7, 7, "l", "Client ID")
            RH.AddColumn(TRHead, TRHead_07, 8, 8, "l", "Client Name")
            RH.AddColumn(TRHead, TRHead_08, 6, 6, "l", "Mobile")
            RH.AddColumn(TRHead, TRHead_09, 5, 5, "l", "Frequency")
            RH.AddColumn(TRHead, TRHead_10, 5, 5, "l", "Eligible Amount")
            RH.AddColumn(TRHead, TRHead_11, 5, 5, "l", "Customer Requested Amount")
            RH.AddColumn(TRHead, TRHead_12, 5, 5, "l", "Branch Recommended Amount")
            RH.AddColumn(TRHead, TRHead_13, 5, 5, "l", "Submitted On Date")
            RH.AddColumn(TRHead, TRHead_14, 4, 4, "l", "Submitted By Emp Code")
            RH.AddColumn(TRHead, TRHead_15, 6, 6, "l", "Submitted By Emp Name")
            RH.AddColumn(TRHead, TRHead_16, 5, 5, "l", "Status")
            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer
            Dim SqlStr As String = " "
            Dim StrWhere As String = " "
            Dim ExSqlStr As String = " "
            'If Branch = -1 Then
            '    'SqlStr = "SELECT A.BRANCH_ID, A.BRANCH_NAME, C.STATE_NAME, A.CENTER_ID, A.CENTER_NAME, A.CLIENT_ID, A.CLIENT_NAME, A.MOBILE, A.FREQUENCY, A.CCL_ELIGIBLE_AMOUNT, A.CUSTOMER_AMOUNT, A.BRANCH_AMOUNT, CONVERT(DATE,SUBMITTED_ON), A.SUBMITTED_BY_CODE, A.SUBMITTED_BY_NAME, CASE WHEN A.STATUS IS NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE STATUS IS NOT NULL AND CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
            '    'ExSqlStr = "SELECT A.BRANCH_ID as [BRANCH ID], A.BRANCH_NAME AS [BRANCH NAME], C.STATE_NAME AS [STATE], A.CENTER_ID AS [CENTER ID], A.CENTER_NAME AS [CENTER NAME], A.CLIENT_ID AS [CLIENT ID], A.CLIENT_NAME AS [CLIENT NAME], A.MOBILE AS [MOBILE], A.FREQUENCY AS [FREQUENCY], A.CCL_ELIGIBLE_AMOUNT AS [CCL ELIGIBLE AMOUNT], A.CUSTOMER_AMOUNT AS [CUSTOMER REQUESTED AMOUNT], A.BRANCH_AMOUNT AS [BRANCH RECOMMENDED AMOUNT], CONVERT(DATE,SUBMITTED_ON) AS [SUBMITTED DATE], A.SUBMITTED_BY_CODE AS [SUBMITTED BY EMPLOYEE CODE], A.SUBMITTED_BY_NAME AS [SUBMITTED BY EMPLOYEE NAME], CASE WHEN A.STATUS IS NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS [STATUS] FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE STATUS IS NOT NULL AND CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
            '    SqlStr = "SELECT A.BRANCH_ID, A.BRANCH_NAME, C.STATE_NAME, A.CENTER_ID, A.CENTER_NAME, A.CLIENT_ID, A.CLIENT_NAME, A.MOBILE, A.FREQUENCY, A.CCL_ELIGIBLE_AMOUNT, A.CUSTOMER_AMOUNT, A.BRANCH_AMOUNT, CONVERT(DATE,SUBMITTED_ON), A.SUBMITTED_BY_CODE, A.SUBMITTED_BY_NAME, CASE WHEN A.STATUS IS NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
            '    ExSqlStr = "SELECT A.BRANCH_ID as [BRANCH ID], A.BRANCH_NAME AS [BRANCH NAME], C.STATE_NAME AS [STATE], A.CENTER_ID AS [CENTER ID], A.CENTER_NAME AS [CENTER NAME], A.CLIENT_ID AS [CLIENT ID], A.CLIENT_NAME AS [CLIENT NAME], A.MOBILE AS [MOBILE], A.FREQUENCY AS [FREQUENCY], A.CCL_ELIGIBLE_AMOUNT AS [CCL ELIGIBLE AMOUNT], A.CUSTOMER_AMOUNT AS [CUSTOMER REQUESTED AMOUNT], A.BRANCH_AMOUNT AS [BRANCH RECOMMENDED AMOUNT], CONVERT(DATE,SUBMITTED_ON) AS [SUBMITTED DATE], A.SUBMITTED_BY_CODE AS [SUBMITTED BY EMPLOYEE CODE], A.SUBMITTED_BY_NAME AS [SUBMITTED BY EMPLOYEE NAME], CASE WHEN A.STATUS IS NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS [STATUS] FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
            'Else
            '    'SqlStr = "SELECT A.BRANCH_ID, A.BRANCH_NAME, C.STATE_NAME, A.CENTER_ID, A.CENTER_NAME, A.CLIENT_ID, A.CLIENT_NAME, A.MOBILE, A.FREQUENCY, A.CCL_ELIGIBLE_AMOUNT, A.CUSTOMER_AMOUNT, A.BRANCH_AMOUNT, CONVERT(DATE,SUBMITTED_ON), A.SUBMITTED_BY_CODE, A.SUBMITTED_BY_NAME, CASE WHEN A.STATUS IS NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE STATUS IS NOT NULL AND CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
            '    'ExSqlStr = "SELECT A.BRANCH_ID as [BRANCH ID], A.BRANCH_NAME AS [BRANCH NAME], C.STATE_NAME AS [STATE], A.CENTER_ID AS [CENTER ID], A.CENTER_NAME AS [CENTER NAME], A.CLIENT_ID AS [CLIENT ID], A.CLIENT_NAME AS [CLIENT NAME], A.MOBILE AS [MOBILE], A.FREQUENCY AS [FREQUENCY], A.CCL_ELIGIBLE_AMOUNT AS [CCL ELIGIBLE AMOUNT], A.CUSTOMER_AMOUNT AS [CUSTOMER REQUESTED AMOUNT], A.BRANCH_AMOUNT AS [BRANCH RECOMMENDED AMOUNT], CONVERT(DATE,SUBMITTED_ON) AS [SUBMITTED DATE], A.SUBMITTED_BY_CODE AS [SUBMITTED BY EMPLOYEE CODE], A.SUBMITTED_BY_NAME AS [SUBMITTED BY EMPLOYEE NAME], CASE WHEN A.STATUS IS NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE STATUS IS NOT NULL AND CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
            '    SqlStr = "SELECT A.BRANCH_ID, A.BRANCH_NAME, C.STATE_NAME, A.CENTER_ID, A.CENTER_NAME, A.CLIENT_ID, A.CLIENT_NAME, A.MOBILE, A.FREQUENCY, A.CCL_ELIGIBLE_AMOUNT, A.CUSTOMER_AMOUNT, A.BRANCH_AMOUNT, CONVERT(DATE,SUBMITTED_ON), A.SUBMITTED_BY_CODE, A.SUBMITTED_BY_NAME, CASE WHEN A.STATUS IS NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
            '    ExSqlStr = "SELECT A.BRANCH_ID as [BRANCH ID], A.BRANCH_NAME AS [BRANCH NAME], C.STATE_NAME AS [STATE], A.CENTER_ID AS [CENTER ID], A.CENTER_NAME AS [CENTER NAME], A.CLIENT_ID AS [CLIENT ID], A.CLIENT_NAME AS [CLIENT NAME], A.MOBILE AS [MOBILE], A.FREQUENCY AS [FREQUENCY], A.CCL_ELIGIBLE_AMOUNT AS [CCL ELIGIBLE AMOUNT], A.CUSTOMER_AMOUNT AS [CUSTOMER REQUESTED AMOUNT], A.BRANCH_AMOUNT AS [BRANCH RECOMMENDED AMOUNT], CONVERT(DATE,SUBMITTED_ON) AS [SUBMITTED DATE], A.SUBMITTED_BY_CODE AS [SUBMITTED BY EMPLOYEE CODE], A.SUBMITTED_BY_NAME AS [SUBMITTED BY EMPLOYEE NAME], CASE WHEN A.STATUS IS NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE AND CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
            '    If Center = -1 Then
            '        SqlStr += " AND A.BRANCH_ID = " + Branch.ToString()
            '        ExSqlStr += " AND A.BRANCH_ID = " + Branch.ToString()
            '    Else
            '        SqlStr += " AND A.BRANCH_ID = " + Branch.ToString() + " AND A.CENTER_ID = " + Center.ToString()
            '        ExSqlStr += " AND A.BRANCH_ID = " + Branch.ToString() + " AND A.CENTER_ID = " + Center.ToString()
            '    End If
            'End If
            'If Status <> -1 Then
            '    If Status = 1 Then
            '        SqlStr += " AND A.STATUS = 1"
            '        ExSqlStr += " AND A.STATUS = 1"
            '    ElseIf Status = 2 Then
            '        SqlStr += " AND A.STATUS IS NULL"
            '        ExSqlStr += " AND A.STATUS IS NULL "
            '    End If
            'End If

            If Branch = -1 Then
                    If Status = 1 Then
                    SqlStr = "SELECT A.BRANCH_ID, A.BRANCH_NAME, C.STATE_NAME, A.CENTER_ID, A.CENTER_NAME, A.CLIENT_ID, A.CLIENT_NAME, A.MOBILE, A.FREQUENCY, A.CCL_ELIGIBLE_AMOUNT, A.CUSTOMER_AMOUNT, A.BRANCH_AMOUNT, CONVERT(DATE,SUBMITTED_ON), A.SUBMITTED_BY_CODE, A.SUBMITTED_BY_NAME, CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
                    ExSqlStr = "SELECT A.BRANCH_ID as [BRANCH ID], A.BRANCH_NAME AS [BRANCH NAME], C.STATE_NAME AS [STATE], A.CENTER_ID AS [CENTER ID], A.CENTER_NAME AS [CENTER NAME], A.CLIENT_ID AS [CLIENT ID], A.CLIENT_NAME AS [CLIENT NAME], A.MOBILE AS [MOBILE], A.FREQUENCY AS [FREQUENCY], A.CCL_ELIGIBLE_AMOUNT AS [CCL ELIGIBLE AMOUNT], A.CUSTOMER_AMOUNT AS [CUSTOMER REQUESTED AMOUNT], A.BRANCH_AMOUNT AS [BRANCH RECOMMENDED AMOUNT], CONVERT(DATE,SUBMITTED_ON) AS [SUBMITTED DATE], A.SUBMITTED_BY_CODE AS [SUBMITTED BY EMPLOYEE CODE], A.SUBMITTED_BY_NAME AS [SUBMITTED BY EMPLOYEE NAME], CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS [STATUS] FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
                        SqlStr += " AND A.STATUS = 1"
                        ExSqlStr += " AND A.STATUS = 1"
                    ElseIf Status = 2 Then
                    SqlStr = "SELECT A.BRANCH_ID, A.BRANCH_NAME, C.STATE_NAME, A.CENTER_ID, A.CENTER_NAME, A.CLIENT_ID, A.CLIENT_NAME, A.MOBILE, A.FREQUENCY, A.CCL_ELIGIBLE_AMOUNT, A.CUSTOMER_AMOUNT, A.BRANCH_AMOUNT, CONVERT(DATE,SUBMITTED_ON), A.SUBMITTED_BY_CODE, A.SUBMITTED_BY_NAME, CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE 1=1 "
                    ExSqlStr = "SELECT A.BRANCH_ID as [BRANCH ID], A.BRANCH_NAME AS [BRANCH NAME], C.STATE_NAME AS [STATE], A.CENTER_ID AS [CENTER ID], A.CENTER_NAME AS [CENTER NAME], A.CLIENT_ID AS [CLIENT ID], A.CLIENT_NAME AS [CLIENT NAME], A.MOBILE AS [MOBILE], A.FREQUENCY AS [FREQUENCY], A.CCL_ELIGIBLE_AMOUNT AS [CCL ELIGIBLE AMOUNT], A.CUSTOMER_AMOUNT AS [CUSTOMER REQUESTED AMOUNT], A.BRANCH_AMOUNT AS [BRANCH RECOMMENDED AMOUNT], CONVERT(DATE,SUBMITTED_ON) AS [SUBMITTED DATE], A.SUBMITTED_BY_CODE AS [SUBMITTED BY EMPLOYEE CODE], A.SUBMITTED_BY_NAME AS [SUBMITTED BY EMPLOYEE NAME], CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS [STATUS] FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE 1=1 "
                        SqlStr += " AND A.STATUS IS NULL"
                        ExSqlStr += " AND A.STATUS IS NULL "
                    Else
                    SqlStr = "SELECT A.BRANCH_ID, A.BRANCH_NAME, C.STATE_NAME, A.CENTER_ID, A.CENTER_NAME, A.CLIENT_ID, A.CLIENT_NAME, A.MOBILE, A.FREQUENCY, A.CCL_ELIGIBLE_AMOUNT, A.CUSTOMER_AMOUNT, A.BRANCH_AMOUNT, CONVERT(DATE,SUBMITTED_ON), A.SUBMITTED_BY_CODE, A.SUBMITTED_BY_NAME, CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE 1=1 "
                    ExSqlStr = "SELECT A.BRANCH_ID as [BRANCH ID], A.BRANCH_NAME AS [BRANCH NAME], C.STATE_NAME AS [STATE], A.CENTER_ID AS [CENTER ID], A.CENTER_NAME AS [CENTER NAME], A.CLIENT_ID AS [CLIENT ID], A.CLIENT_NAME AS [CLIENT NAME], A.MOBILE AS [MOBILE], A.FREQUENCY AS [FREQUENCY], A.CCL_ELIGIBLE_AMOUNT AS [CCL ELIGIBLE AMOUNT], A.CUSTOMER_AMOUNT AS [CUSTOMER REQUESTED AMOUNT], A.BRANCH_AMOUNT AS [BRANCH RECOMMENDED AMOUNT], CONVERT(DATE,SUBMITTED_ON) AS [SUBMITTED DATE], A.SUBMITTED_BY_CODE AS [SUBMITTED BY EMPLOYEE CODE], A.SUBMITTED_BY_NAME AS [SUBMITTED BY EMPLOYEE NAME], CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS [STATUS] FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE 1=1 "
                    End If
            Else
                    If Status = 1 Then
                    SqlStr = "SELECT A.BRANCH_ID, A.BRANCH_NAME, C.STATE_NAME, A.CENTER_ID, A.CENTER_NAME, A.CLIENT_ID, A.CLIENT_NAME, A.MOBILE, A.FREQUENCY, A.CCL_ELIGIBLE_AMOUNT, A.CUSTOMER_AMOUNT, A.BRANCH_AMOUNT, CONVERT(DATE,SUBMITTED_ON), A.SUBMITTED_BY_CODE, A.SUBMITTED_BY_NAME, CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
                    ExSqlStr = "SELECT A.BRANCH_ID as [BRANCH ID], A.BRANCH_NAME AS [BRANCH NAME], C.STATE_NAME AS [STATE], A.CENTER_ID AS [CENTER ID], A.CENTER_NAME AS [CENTER NAME], A.CLIENT_ID AS [CLIENT ID], A.CLIENT_NAME AS [CLIENT NAME], A.MOBILE AS [MOBILE], A.FREQUENCY AS [FREQUENCY], A.CCL_ELIGIBLE_AMOUNT AS [CCL ELIGIBLE AMOUNT], A.CUSTOMER_AMOUNT AS [CUSTOMER REQUESTED AMOUNT], A.BRANCH_AMOUNT AS [BRANCH RECOMMENDED AMOUNT], CONVERT(DATE,SUBMITTED_ON) AS [SUBMITTED DATE], A.SUBMITTED_BY_CODE AS [SUBMITTED BY EMPLOYEE CODE], A.SUBMITTED_BY_NAME AS [SUBMITTED BY EMPLOYEE NAME], CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS [STATUS] FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE CONVERT(DATE,A.SUBMITTED_ON) BETWEEN CONVERT(DATE,'" + FromDt + "') AND CONVERT(DATE,'" + ToDt + "')"
                        SqlStr += " AND A.STATUS = 1"
                        ExSqlStr += " AND A.STATUS = 1"
                    ElseIf Status = 2 Then
                    SqlStr = "SELECT A.BRANCH_ID, A.BRANCH_NAME, C.STATE_NAME, A.CENTER_ID, A.CENTER_NAME, A.CLIENT_ID, A.CLIENT_NAME, A.MOBILE, A.FREQUENCY, A.CCL_ELIGIBLE_AMOUNT, A.CUSTOMER_AMOUNT, A.BRANCH_AMOUNT, CONVERT(DATE,SUBMITTED_ON), A.SUBMITTED_BY_CODE, A.SUBMITTED_BY_NAME, CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE 1=1 "
                    ExSqlStr = "SELECT A.BRANCH_ID as [BRANCH ID], A.BRANCH_NAME AS [BRANCH NAME], C.STATE_NAME AS [STATE], A.CENTER_ID AS [CENTER ID], A.CENTER_NAME AS [CENTER NAME], A.CLIENT_ID AS [CLIENT ID], A.CLIENT_NAME AS [CLIENT NAME], A.MOBILE AS [MOBILE], A.FREQUENCY AS [FREQUENCY], A.CCL_ELIGIBLE_AMOUNT AS [CCL ELIGIBLE AMOUNT], A.CUSTOMER_AMOUNT AS [CUSTOMER REQUESTED AMOUNT], A.BRANCH_AMOUNT AS [BRANCH RECOMMENDED AMOUNT], CONVERT(DATE,SUBMITTED_ON) AS [SUBMITTED DATE], A.SUBMITTED_BY_CODE AS [SUBMITTED BY EMPLOYEE CODE], A.SUBMITTED_BY_NAME AS [SUBMITTED BY EMPLOYEE NAME], CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS [STATUS] FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE 1=1 "
                        SqlStr += " AND A.STATUS IS NULL"
                        ExSqlStr += " AND A.STATUS IS NULL "
                    Else
                    SqlStr = "SELECT A.BRANCH_ID, A.BRANCH_NAME, C.STATE_NAME, A.CENTER_ID, A.CENTER_NAME, A.CLIENT_ID, A.CLIENT_NAME, A.MOBILE, A.FREQUENCY, A.CCL_ELIGIBLE_AMOUNT, A.CUSTOMER_AMOUNT, A.BRANCH_AMOUNT, CONVERT(DATE,SUBMITTED_ON), A.SUBMITTED_BY_CODE, A.SUBMITTED_BY_NAME, CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS STATUS FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE 1=1 "
                    ExSqlStr = "SELECT A.BRANCH_ID as [BRANCH ID], A.BRANCH_NAME AS [BRANCH NAME], C.STATE_NAME AS [STATE], A.CENTER_ID AS [CENTER ID], A.CENTER_NAME AS [CENTER NAME], A.CLIENT_ID AS [CLIENT ID], A.CLIENT_NAME AS [CLIENT NAME], A.MOBILE AS [MOBILE], A.FREQUENCY AS [FREQUENCY], A.CCL_ELIGIBLE_AMOUNT AS [CCL ELIGIBLE AMOUNT], A.CUSTOMER_AMOUNT AS [CUSTOMER REQUESTED AMOUNT], A.BRANCH_AMOUNT AS [BRANCH RECOMMENDED AMOUNT], CONVERT(DATE,SUBMITTED_ON) AS [SUBMITTED DATE], A.SUBMITTED_BY_CODE AS [SUBMITTED BY EMPLOYEE CODE], A.SUBMITTED_BY_NAME AS [SUBMITTED BY EMPLOYEE NAME], CASE WHEN A.STATUS IS NOT NULL THEN 'APPLIED' ELSE 'NOT APPLIED' END AS [STATUS] FROM CCL_MASTER_DATA A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN STATE_MASTER C ON C.STATE_ID = B.STATE_ID WHERE 1=1 "
                    End If
                If Center = -1 Then
                    SqlStr += " AND A.BRANCH_ID = " + Branch.ToString()
                    ExSqlStr += " AND A.BRANCH_ID = " + Branch.ToString()
                Else
                    SqlStr += " AND A.BRANCH_ID = " + Branch.ToString() + " AND A.CENTER_ID = " + Center.ToString()
                    ExSqlStr += " AND A.BRANCH_ID = " + Branch.ToString() + " AND A.CENTER_ID = " + Center.ToString()
                End If
            End If
            



            SqlStr += StrWhere
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            ExSqlStr += StrWhere
            DTExcel = DB.ExecuteDataSet(ExSqlStr).Tables(0)
            If ViewType = 1 Then
                Export_Excel_Click()
            End If

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"
                TR3_15.BorderWidth = "1"
                TR3_16.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver
                TR3_15.BorderColor = Drawing.Color.Silver
                TR3_16.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid
                TR3_15.BorderStyle = BorderStyle.Solid
                TR3_16.BorderStyle = BorderStyle.Solid


                RH.AddColumn(TR3, TR3_00, 3, 3, "l", i.ToString())
                RH.AddColumn(TR3, TR3_01, 5, 5, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 9, 9, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 6, 6, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_04, 7, 7, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_05, 9, 9, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_06, 7, 7, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_07, 8, 8, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_08, 6, 6, "l", DR(7).ToString())
                RH.AddColumn(TR3, TR3_09, 5, 5, "l", DR(8).ToString())
                RH.AddColumn(TR3, TR3_10, 5, 5, "l", DR(9).ToString())
                RH.AddColumn(TR3, TR3_11, 5, 5, "l", DR(10).ToString())
                RH.AddColumn(TR3, TR3_12, 5, 5, "l", DR(11).ToString())
                If DR(12).ToString() <> "" Then
                    RH.AddColumn(TR3, TR3_13, 5, 5, "l", CDate(DR(12)).ToString("dd/MM/yyyy"))
                Else
                    RH.AddColumn(TR3, TR3_13, 5, 5, "l", "")
                End If
                RH.AddColumn(TR3, TR3_14, 4, 4, "l", DR(13).ToString())
                RH.AddColumn(TR3, TR3_15, 6, 6, "l", DR(14).ToString())
                RH.AddColumn(TR3, TR3_16, 5, 5, "l", DR(15).ToString())
                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "COVID Care Loan Report"

            WebTools.ExporttoExcel(DTExcel, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Protected Sub Export_Excel_Click()
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "COVID Care Loan Report"

            WebTools.ExporttoExcel(DTExcel, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
