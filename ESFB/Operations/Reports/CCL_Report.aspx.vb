﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class CCL_Report
    Inherits System.Web.UI.Page

    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim Meeting_Dt As Date
    Dim UserID As Integer
    Dim TraDt As String
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim DB As New MS_SQL.Connect
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1422) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Covid Care Loan Report"
            UserID = CInt(Session("UserID"))

            'GF.CheckRetail(CInt(Session("Branch_Id"))) = True

            If Not IsPostBack Then
                DT1 = DB.ExecuteDataSet("SELECT '-1' AS BRANCH_ID_ID,'-----SELECT-----' AS BRANCH_NAME UNION ALL SELECT BRANCH_ID,CONVERT(VARCHAR,BRANCH_ID)+' | '+BRANCH_NAME FROM BRANCH_MASTER WHERE STATUS_ID = 1").Tables(0)
                GF.ComboFill(cmbBranch, DT1, 0, 1)
                DT = DB.ExecuteDataSet("SELECT A.BRANCH_ID FROM BRANCH_MASTER A INNER JOIN EMP_MASTER B ON B.BRANCH_ID = A.BRANCH_ID WHERE B.EMP_CODE = " + UserID.ToString()).Tables(0)
                If CInt(DT.Rows(0)(0).ToString()) <> 0 Then
                    cmbBranch.SelectedValue = DT.Rows(0)(0).ToString()
                    cmbBranch.Enabled = False
                    DT1 = DB.ExecuteDataSet("SELECT '-1' AS CENTER_ID,'-----SELECT-----' AS CENTER_NAME UNION ALL SELECT DISTINCT CENTER_ID,CONVERT(VARCHAR,CENTER_ID) FROM CCL_MASTER_DATA WHERE BRANCH_ID = " + DT.Rows(0)(0).ToString() + " ORDER BY CENTER_ID").Tables(0)
                    GF.ComboFill(cmbCenter, DT1, 0, 1)
                End If
            End If
            

            Me.cmbBranch.Attributes.Add("onchange", "return BranchOnChange()")
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
        DB.dispose()
    End Sub

#End Region
#Region "Events"

#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim BranchCode As String = Data(1)
            DT1 = DB.ExecuteDataSet("SELECT '-1' AS CENTER_ID,'-----SELECT-----' AS CENTER_NAME UNION ALL SELECT DISTINCT CENTER_ID,CONVERT(VARCHAR,CENTER_ID) FROM CCL_MASTER_DATA WHERE BRANCH_ID = " + BranchCode.ToString() + " ORDER BY CENTER_ID").Tables(0)
            For Each DR As DataRow In DT1.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + CStr(DR(1))
            Next
        End If
    End Sub
#End Region
End Class

