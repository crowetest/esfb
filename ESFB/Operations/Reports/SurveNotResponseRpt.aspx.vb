﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class SurveNotResponseRpt
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim TraDt As Date
    Dim DB As New MS_SQL.Connect
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1226) = False And (CInt(Session("UserID")) <> 3) Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Me.Master.subtitle = "Not Responded Branches Report"
            If Not IsPostBack Then
                Me.txtFromDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
                Me.txtToDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))

                DT = DB.ExecuteDataSet(" SELECT '-1' AS Branch_id,'-----ALL-----' AS Branch_name UNION ALL SELECT distinct Branch_id,Branch_name from BRANCH_MASTER where status_id=1 and branch_type in (1,3) ").Tables(0)
                GF.ComboFill(cmbBranch, DT, 0, 1)
                DT = DB.ExecuteDataSet(" SELECT '-1' AS id,'-----ALL-----' AS Value UNION ALL select id, value from Surve_Frequency_Master where status=1").Tables(0)
                GF.ComboFill(cmbFrequency, DT, 0, 1)
                DT = DB.ExecuteDataSet(" SELECT '-1' AS id,'-----ALL-----' AS Value ").Tables(0)
                GF.ComboFill(cmbChecklist, DT, 0, 1)

                Me.cmbFrequency.Attributes.Add("onchange", "return FrequencyOnchange()")

            End If

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub

#End Region
#Region "Events"

#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim FreqId As Integer
            Dim StrWhere As String = ""
            FreqId = CInt(Data(1))
            If FreqId <> -1 Then
                StrWhere = " and A.Frequency =  " + FreqId.ToString + ""
            End If
            DT = DB.ExecuteDataSet("select -1 as checklistid, '-----ALL-----' as Description union select A.checklistid, Description FROM Surve_ChkList_Master A inner join Surve_Task_Master B on A.checklistid = B.checklistid " +
                " where A.Status_id = 1 and B.Status_id = 1 " + StrWhere + " group by  A.checklistid, Description  ").Tables(0)
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += "Ñ" + DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString()
            Next
        End If
    End Sub
#End Region


End Class

