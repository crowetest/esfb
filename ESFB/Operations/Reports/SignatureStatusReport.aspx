﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="SignatureStatusReport.aspx.vb" Inherits="SignatureStatusReport" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
	    <link rel="stylesheet" href="../../Style/bootstrap-risk-multiselect.css" type="text/css" />
	    <script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
	    <script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
	    <script type="text/javascript" src="../../Script/bootstrap-multiselect_RISK.js"></script>
        <style type="text/css">
            #Button
            {
                width: 80%;
                height: 20px;
                font-weight: bold;
                line-height: 20px;
                text-align: center;
                border-top-left-radius: 25px;
                border-top-right-radius: 25px;
                border-bottom-left-radius: 25px;
                border-bottom-right-radius: 25px;
                cursor: pointer;
                background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
            }            
            #Button:hover
            {
                background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
            }
            .sub_hdRow
            {
                background-color: #EBCCD6;
                height: 20px;
                font-family: Arial;
                color: #B84D4D;
                font-size: 8.5pt;
                font-weight: bold;
            }
            .style1
            {
                width: 100%;
            }
        </style>
       
        <script language="javascript" type="text/javascript">
        

                      
        
        function btnShow_onclick() {
            if((document.getElementById("<%= cmbType.ClientID %>").value == 1) && (document.getElementById("<%= txtEmpCode.ClientID %>").value == "")){
                alert("Enter Emp Code");
                document.getElementById("<%= txtEmpCode.ClientID %>").focus();
            }
            var Empcode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            var Type = document.getElementById("<%= cmbType.ClientID %>").value;
            var Mode = document.getElementById("<%= cmbMode.ClientID %>").value;

         
            window.open("ViewSignatureRequestStatus.aspx?Type=" + btoa(Type) + "&Empcode=" + btoa(Empcode) + "&Mode=" +  btoa(Mode) , "_blank");
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }      
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[

// ]]>
        </script>
    </head>
    </html>
    <br />
    <br />
    <div style="width: 25%; margin: 0px auto;">
        <table class="style1" style="width: 100%; top: 350px auto;">
            <tr class="style1">
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Type &nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;
                    <asp:DropDownList ID="cmbType" class="NormalText" Style="text-align: left;" runat="server"
                        Font-Names="Cambria" Width="75%" ForeColor="Black" Height="16px">
                     
                        <asp:ListItem Value="1" Selected="True">Employee</asp:ListItem>
                        <asp:ListItem Value="2" >ALL</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>            
            <tr>
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Emp Code
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;
                    <asp:TextBox ID="txtEmpCode" class="NormalText" runat="server" 
                        style=" font-family:Cambria;font-size:10pt;" Width="75%" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Mode</td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;
                    <asp:DropDownList ID="cmbMode" class="NormalText" Style="text-align: left;" runat="server"
                        Font-Names="Cambria" Width="75%" ForeColor="Black" Height="16px">
                        <asp:ListItem Value="1" Selected="True">ALL</asp:ListItem>
                        <asp:ListItem Value="2" >Pending Only</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="2">
                    <br />
                    <br />
                    <input id="btnShow" style="font-family: cambria; cursor: pointer; width: 28%;" type="button"
                        value="SHOW" onclick="return btnShow_onclick()" />
                    &nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 28%;" type="button"
                        value="EXIT" onclick="return btnExit_onclick()" /><asp:HiddenField ID="hid_Items"
                            runat="server" />
                    <asp:HiddenField ID="hid_USer" runat="server" />
                    <asp:HiddenField ID="hid_Value" runat="server" /> 
                    <asp:HiddenField ID="hdnTeam" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
