﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewSurveTaskResponse
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTEXCEL As DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date
    Dim Header As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

           

            Dim DT As New DataTable
            Dim Resp_master_id, RespType As Integer
            Dim Branch, Checklist, RespDate, SubmitDate As String
            Resp_master_id = CInt(Request.QueryString.Get("Resp_master_id"))
            RespType = CInt(Request.QueryString.Get("RespoType"))

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Header = IIf(RespType = 1, "YES", IIf(RespType = 2, "NO", IIf(RespType = 3, "N/A", "ALL"))) + " - Response Report"
            RH.Heading(Session("FirmName"), tb, Header, 100)
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08 As New TableCell
            TRHead.BackColor = Drawing.Color.WhiteSmoke

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid

            TRHead_00.Font.Bold = True
            TRHead_01.Font.Bold = True
            TRHead_02.Font.Bold = True
            TRHead_03.Font.Bold = True
            TRHead_04.Font.Bold = True
            TRHead_05.Font.Bold = True

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "SI No")
            RH.AddColumn(TRHead, TRHead_01, 50, 50, "c", "Task")
            RH.AddColumn(TRHead, TRHead_02, 7, 7, "c", "Response")
            RH.AddColumn(TRHead, TRHead_05, 18, 18, "c", "Remark")
            RH.AddColumn(TRHead, TRHead_03, 7, 7, "c", "Review")
            RH.AddColumn(TRHead, TRHead_04, 20, 20, "c", "Review Comments")

            RH.BlankRow(tb, 2)
            Branch = ""
            Checklist = ""
            RespDate = ""
            SubmitDate = ""

            DT = DB.ExecuteDataSet("select  branch_name + ' - ' + convert(varchar,A.Branch_id),Description as Checklist,Convert(nvarchar(20), A.nextRespDate, 113),Convert(nvarchar(20), A.created_on, 113) from Surve_Branch_Response_Master A " +
                " inner join Branch_master B ON A.Branch_id = B.Branch_id " +
                " inner join Surve_ChkList_Master C ON A.checklistid = c.checklistid " +
                " where A.Resp_master_id = " + Resp_master_id.ToString()).Tables(0)
            If DT.Rows.Count > 0 Then
                Branch = DT.Rows(0)(0).ToString().ToUpper()
                Checklist = DT.Rows(0)(1).ToString().ToUpper()
                RespDate = DT.Rows(0)(2).ToString().ToUpper()
                SubmitDate = DT.Rows(0)(3).ToString().ToUpper()
            End If

            RH.SubHeading(tb, 100, "l", "Branch       :" + "       " + Branch)
            RH.SubHeading(tb, 100, "l", "Checklist    :" + "       " + Checklist)
            RH.SubHeading(tb, 100, "l", "Date         :" + "       " + RespDate)
            RH.SubHeading(tb, 100, "l", "Submit date  :" + "       " + SubmitDate)

            RH.BlankRow(tb, 3)
            tb.Attributes.Add("width", "100%")

            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer
            Dim Branch_Id As String = ""
            Dim StrWhere As String = " And 1=1 "

            If RespType > 0 Then
                StrWhere = " and B.Resp_type = " + RespType.ToString() + ""
            End If

            DT = DB.ExecuteDataSet("select TaskDesc,case when Resp_type = 1 then 'YES' when Resp_type = 2 then 'NO' when Resp_type = 3 then 'N/A' end as Resp_type, Resp_comments,case when reviewstatus = 1 then 'OPEN' when reviewstatus = 2 then 'CLOSED' END AS reviewstatus, ReviewComment from Surve_Task_Master A inner join Surve_Branch_Response B on A.TaskID=B.Task_ID where Resp_master_id = " + Resp_master_id.ToString + StrWhere).Tables(0)
            DTEXCEL = DB.ExecuteDataSet("select TaskDesc as Task, case when Resp_type = 1 then 'YES' when Resp_type = 2 then 'NO' when Resp_type = 3 then 'N/A' end as Resp_type, Resp_comments as Response,case when reviewstatus = 1 then 'OPEN' when reviewstatus = 2 then 'CLOSED' END AS Review, ReviewComment as [Review Comments] from Surve_Task_Master A inner join Surve_Branch_Response B on A.TaskID=B.Task_ID where Resp_master_id = " + Resp_master_id.ToString + StrWhere).Tables(0)

            For Each DR In DT.Rows
                i += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", i.ToString())
                RH.AddColumn(TR3, TR3_01, 50, 50, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 7, 7, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_05, 18, 18, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_03, 7, 7, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_04, 20, 20, "l", DR(4).ToString())

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Survey_Task_Response_Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
