﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="ReturnMemo.aspx.vb" Inherits="ReturnMemo"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
     <tr>
            <td style="width:15%;">
     <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager></td>
   <td  style="width:85%;">
   </td></tr>
        <tr style="display:none">
            <td style="width:15%;">
                Branch</td>
           <td  style="width:85%;">
                 <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" AutoPostBack="true"
         Width="50%" ForeColor="Black" AppendDataBoundItems="True">
         <asp:ListItem Value="-1">-----ALL-----</asp:ListItem>
              
                </asp:DropDownList>
                </td>
           
        </tr>
        
       
         <tr>
           
            <td style="width:15%; text-align:left;">
                Effected Date </td>
            <td style="width:85%">
                <asp:TextBox ID="txtFromDt" class="NormalText" runat="server" 
                    Width="50%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txtFromDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtFromDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>

            </td> 
 </tr>                 
      
          <tr>
           <td style="width:15%; text-align:left;">
                 </td>
            <td style="width:85%">
            <br />
           
                <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 20%" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;

                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 20%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>

            </td> 


            <td style="width:100%; height: 18px; text-align:center;">
             <br />

             
             
          
      
        </tr>
       
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
               
                 <asp:HiddenField ID="hdnValue" runat="server" />
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
     
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }
   
     function window_onload() {
     
           
         
       }
 
       
       
    function btnView_onclick() 
        {
            BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
//            

             var FromDate= document.getElementById("<%= txtFromDt.ClientID %>").value;
             var FromDT = new Date(FromDate);
//          alert(FromDT);
            
//              
            window.open("ReturnViewMemo.aspx?BranchID=" + BranchID + "&FromDate=" + FromDate +" ", "_self");  
                    
       }        
   function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
       
     function FromServer(Arg, Context) {
            switch (Context) {

                case 1:
                    {
////                        
                    }
            }
        }

       
    </script>
</asp:Content>

