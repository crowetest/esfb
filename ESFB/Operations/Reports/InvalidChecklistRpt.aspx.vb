﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class InvalidChecklistRpt
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTEXCEL As DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date
    Dim GF As New GeneralFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1230) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim DT As New DataTable
            Dim Type, Item As Integer
            'Dim Checklist As Integer
            'Item = CInt(Request.QueryString.Get("item"))
            'Type = CInt(Request.QueryString.Get("type"))
            'If Request.QueryString.Get("Checklist") <> "" Then
            '    Checklist = CInt(Request.QueryString.Get("Checklist"))
            'End If
            'Type = CInt(Request.QueryString.Get("Type"))



            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RH.Heading(Session("FirmName"), tb, "Invalid Checklist Report", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "SI No")
            'If Item = -1 Then
            RH.AddColumn(TRHead, TRHead_01, 70, 70, "l", "CheckList")
            RH.AddColumn(TRHead, TRHead_02, 20, 20, "l", "Total")
            'RH.AddColumn(TRHead, TRHead_03, 5, 5, "l", "Frequency")
            'RH.AddColumn(TRHead, TRHead_04, 5, 5, "l", "Interval")
            'RH.AddColumn(TRHead, TRHead_05, 5, 5, "l", "Status")
            'RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "Effected Date")
            'RH.AddColumn(TRHead, TRHead_07, 5, 5, "l", "Approved Status")
            'RH.AddColumn(TRHead, TRHead_08, 5, 5, "l", "Maker")
            'RH.AddColumn(TRHead, TRHead_09, 10, 10, "l", "Make Date")
            'RH.AddColumn(TRHead, TRHead_10, 5, 5, "l", "Checker")
            'RH.AddColumn(TRHead, TRHead_11, 10, 10, "l", "Approved Date")
            'Else
            'RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Task")
            'RH.AddColumn(TRHead, TRHead_02, 20, 20, "l", "Remarks")
            'RH.AddColumn(TRHead, TRHead_03, 5, 5, "l", "Weightage")
            'RH.AddColumn(TRHead, TRHead_04, 5, 5, "l", "Status")
            'RH.AddColumn(TRHead, TRHead_05, 5, 5, "l", "Approved Status")
            'RH.AddColumn(TRHead, TRHead_06, 5, 5, "l", "Maker")
            'RH.AddColumn(TRHead, TRHead_07, 10, 10, "l", "Make Date")
            'RH.AddColumn(TRHead, TRHead_08, 10, 10, "l", "Checker")
            'RH.AddColumn(TRHead, TRHead_09, 10, 10, "l", "Approved Date")
            'End If

            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer
            Dim SqlStr As String = " "
            Dim StrWhere As String = " "
            Dim ExSqlStr As String = " "

            'If Item = -1 Then
            SqlStr = "select sct.Description,sum(stt.weightage) as sum from surve_chklist_Master sct " +
                     "inner join surve_task_master stt on stt.checklistid=sct.checklistid and sct.status_id=1 " +
                     "where(stt.status_id = 1) and sct.status_id=1" +
                     " group by sct.Description having sum(stt.weightage)<> 100"
            ExSqlStr = "select sct.Description as Checklist,sum(stt.weightage) as Total from surve_chklist_Master sct " +
                     "inner join surve_task_master stt on stt.checklistid=sct.checklistid and sct.status_id=1 " +
                     "where(stt.status_id = 1) and sct.status_id=1" +
                     " group by sct.Description having sum(stt.weightage)<> 100"
            'If (Type = -1) Then
            '    StrWhere = " status_id =1"
            'ElseIf (Type = 1) Then
            '    StrWhere = " status_id =-1"
            'ElseIf (Type = 2) Then
            '    StrWhere = " status_id =0"
            'ElseIf (Type = 3) Then
            '    StrWhere = " approvedstatus =1"
            'ElseIf (Type = 4) Then
            '    StrWhere = " approvedstatus =2"
            'End If
            'ElseIf Item = 1 Then
            'SqlStr = "select TaskDesc,remarks,weightage,case when bstt.status_id=0 then 'Task not approved' " +
            '                "when bstt.status_id=1 then 'Task active' when bstt.status_id=-1 then 'Task inactive' when bstt.status_id=999 then 'Task deleted' when bstt.status_id=100 then 'Task just added'end as status," +
            '                "case when bstt.approvedstatus=0 then 'No' when bstt.approvedstatus=1 then 'Yes' when bstt.approvedstatus=2 then 'Rejected' when bstt.approvedstatus=100 then 'Task not added in Approval queue' end as appstatus " +
            '                ",em.emp_name as name,MakeDate,em1.emp_name,Approveddate " +
            '                " from surve_task_tracker bstt" +
            '                " inner join emp_master em on em.emp_code = bstt.makerid " +
            '                " inner join emp_master em1 on em1.emp_code = bstt.checkerid " +
            '                " where taskid=" + Type.ToString() + ""
            'If (Type = -1) Then
            '    StrWhere = " status_id =1"
            'ElseIf (Type = 1) Then
            '    StrWhere = " status_id =-1"
            'ElseIf (Type = 2) Then
            '    StrWhere = " status_id =0"
            'ElseIf (Type = 3) Then
            '    StrWhere = " approvedstatus =1"
            'ElseIf (Type = 4) Then
            '    StrWhere = " approvedstatus =2"
            'End If
            'End If
            SqlStr += StrWhere
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            DTEXCEL = DB.ExecuteDataSet(ExSqlStr).Tables(0)

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver

                'If Item = -1 Then
                RH.AddColumn(TR3, TR3_00, 10, 10, "l", i.ToString())
                RH.AddColumn(TR3, TR3_01, 70, 70, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 20, 20, "l", DR(1).ToString())
                'RH.AddColumn(TR3, TR3_03, 5, 5, "l", DR(2).ToString())
                'RH.AddColumn(TR3, TR3_04, 5, 5, "l", DR(3).ToString())
                'RH.AddColumn(TR3, TR3_05, 5, 5, "l", DR(4).ToString())
                'RH.AddColumn(TR3, TR3_06, 10, 10, "l", CDate(DR(5)).ToString("dd/MM/yyyy"))
                'RH.AddColumn(TR3, TR3_07, 5, 5, "l", DR(6).ToString())
                'RH.AddColumn(TR3, TR3_08, 5, 5, "l", DR(7).ToString())
                'RH.AddColumn(TR3, TR3_09, 10, 10, "l", CDate(DR(8)).ToString("dd/MM/yyyy"))
                'RH.AddColumn(TR3, TR3_10, 5, 5, "l", DR(9).ToString())
                'RH.AddColumn(TR3, TR3_11, 10, 10, "l", CDate(DR(10)).ToString("dd/MM/yyyy"))
                'tb.Controls.Add(TR3)
                'ElseIf Item = 1 Then
                'RH.AddColumn(TR3, TR3_00, 10, 10, "l", i.ToString())
                'RH.AddColumn(TR3, TR3_01, 20, 20, "l", DR(0).ToString())
                'RH.AddColumn(TR3, TR3_02, 20, 20, "l", DR(1).ToString())
                'RH.AddColumn(TR3, TR3_03, 5, 5, "l", DR(2).ToString())
                'RH.AddColumn(TR3, TR3_04, 5, 5, "l", DR(3).ToString())
                'RH.AddColumn(TR3, TR3_05, 5, 5, "l", DR(4).ToString())
                'RH.AddColumn(TR3, TR3_06, 5, 5, "l", DR(5).ToString())
                'RH.AddColumn(TR3, TR3_07, 10, 10, "l", CDate(DR(6)).ToString("dd/MM/yyyy"))
                'RH.AddColumn(TR3, TR3_08, 10, 10, "l", DR(7).ToString())
                'RH.AddColumn(TR3, TR3_09, 10, 10, "l", CDate(DR(8)).ToString("dd/MM/yyyy"))
                tb.Controls.Add(TR3)
                'End If
            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Assigned Branches Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
