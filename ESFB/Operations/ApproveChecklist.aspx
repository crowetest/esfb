﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ApproveChecklist.aspx.vb" Inherits="ApproveChecklist" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;
          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);
           
           
        }
        
        .Button:hover
        {
            
           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            
            color:#801424;
        }   
                 
     
     .bg
     {
         background-color:#FFF;
     }
           
    </style>
    
    <script language="javascript" type="text/javascript">

        function IntialCtrl(){
            alert("Saved Successfully");
            document.getElementById("<%= hid_Id.ClientID %>").value = 0;
            document.getElementById("<%= hid_Edit.ClientID %>").value = "0";
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1) {
                CategoryClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2) {
                ModuleClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3) {
                ProblemClick();
            }               
        }
        function FromServer(arg, context) {           
           if (context == 2) 
           {
                var Data = arg.split("~");
                document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                
//                if (document.getElementById("<%= hid_dtls.ClientID %>").value !="") 
//                {
                    table_fill();
                    
//                }
//                else
//                {
//                    document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
//                    
//                }               
            }
            else if (context==3) 
            {
                ComboFill(arg, "<%= cmbCategory.ClientID %>");
                document.getElementById("<%= cmbCategory.ClientID %>").focus();
                if  (document.getElementById("<%= hid_Cate.ClientID %>").value != "")  
                {
                   document.getElementById("<%= cmbCategory.ClientID %>").value=document.getElementById("<%= hid_Cate.ClientID %>").value;
                    document.getElementById("<%= hid_Cate.ClientID %>").value="";
                    CategoryOnChange();
                }            
            }
            else if (context==4) 
            {                
                var Data = arg.split("~");
//                if (Data[0] == 1) 
//                {
                    document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                    table_fill();   
//                }
//                else
//                    document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
            }
            else if (context==5) 
            {
               

                if  (document.getElementById("<%= hid_SubCate.ClientID %>").value != "")  
                {
                  
                    document.getElementById("<%= hid_SubCate.ClientID %>").value="";
                    ModuleOnChange();
                }
            }
            else if (context==6) 
            {                
                var Data = arg.split("~");
                if (Data[0] == 1) {
                    document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                    table_fill();
                   
                    
                    document.getElementById("ViewAtur").style.display = 'none';
                }
                else
                    document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
            }
            else if (context==7) 
            {
                var Data = arg.split("~");
                            
                if (Data[0] == 1) 
                {
                    if  (document.getElementById("<%= hid_Tab.ClientID %>").value==1)
                    {                        
                        CategoryFill();
                        
                    }                                    
                    else if (document.getElementById("<%= hid_Tab.ClientID %>").value==2) 
                    {
                        var ToData = "4Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbCategory.ClientID  %>").value;
                        ToServer(ToData, 4)
                        
                    }
                    else if (document.getElementById("<%= hid_Tab.ClientID %>").value=3) 
                    {
                        var ToData = "6Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
                        ToServer(ToData, 6)
                    }
                }
            }
            // 'shima 17/10
            else if (context==8)
            {                 
            }
        }
        function CategoryFill() {
            var ToData = "2Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
            ToServer(ToData, 2)
        }
        function FillCategoryCombo(){
            
            var ToData = "3Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
            ToServer(ToData, 3);
        }
        function CategoryOnChange() 
        {           
            if  (document.getElementById("<%= hid_Tab.ClientID %>").value == 2)
            {
                var ToData = "4Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbCategory.ClientID  %>").value;
                ToServer(ToData, 4)
            }
        }
        function ModuleOnChange() 
        {
                ToServer(ToData, 6)
                document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
        }  
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function CategoryClick() {
           
            document.getElementById("Category").style.display = 'none';
            document.getElementById("<%= cmbCategory.ClientID %>").value = "";
            document.getElementById("<%= hid_tab.ClientID %>").value = 1;
            document.getElementById("<%= hid_Id.ClientID %>").value = 0;
            document.getElementById("<%= hid_Edit.ClientID %>").value = "0";
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
            document.getElementById("1").style.background = '-moz-radial-gradient(center, ellipse cover,  #EEB8A6 0%, #801424 90%, #EEB8A6 100%)';
            document.getElementById("1").style.color = '#CF0D0D';
            document.getElementById("2").style.background = '-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("2").style.color = '#CF0D0D';
            CategoryFill();
            
        }
        function ModuleClick() {
           
            document.getElementById("<%= hid_tab.ClientID %>").value = 2;
            document.getElementById("<%= hid_Id.ClientID %>").value = 0;
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
            document.getElementById("<%= cmbCategory.ClientID %>").value = "";
            document.getElementById("<%= hid_Edit.ClientID %>").value = "0";           
            document.getElementById("Category").style.display = '';
            document.getElementById("1").style.background =  '-moz-radial-gradient(center, ellipse cover,#F1F3F4 0%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("1").style.color = '#CF0D0D';
            document.getElementById("2").style.background =  '-moz-radial-gradient(center, ellipse cover,  #EEB8A6 20%, #801424 90%, #EEB8A6 100%)';
            document.getElementById("2").style.color = '#CF0D0D';
            FillCategoryCombo();
            document.getElementById("<%= cmbCategory.ClientID %>").focus();            
        }
       
        function Saveonclick()         
        {            
            document.getElementById("<%= hid_Save.ClientID %>").value="";
            document.getElementById("<%= hid_Cate.ClientID %>").value="";
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1)
            {
                document.getElementById("<%= hid_Save.ClientID %>").value =document.getElementById("<%= hid_Tab.ClientID %>").value + 'Ø';
                if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
                {
                    row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");  
                    for (n = 0; n <= row.length - 1; n++) 
                    {                    
                        col = row[n].split("µ");
                        if(document.getElementById("chk" + col[1]).checked==true)
                        {
                            document.getElementById("<%= hid_Edit.ClientID %>").value = document.getElementById("<%= hid_Edit.ClientID %>").value + 'Ø' +  col[1].toString();
                        }
                    }
                }  
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2)
            {
                document.getElementById("<%= hid_Save.ClientID %>").value =document.getElementById("<%= hid_Tab.ClientID %>").value + 'Ø';
                if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
                {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                    for (n = 0; n <= row.length - 1; n++) 
                    {                    
                        col = row[n].split("µ");
                       if(document.getElementById("chk" + col[1]).checked==true)
                        {
                            document.getElementById("<%= hid_Edit.ClientID %>").value = document.getElementById("<%= hid_Edit.ClientID %>").value + 'Ø' +  col[1].toString();
                        }
                    }
                }  
            }
        }
        function Rejectonclick()         
        {            
            document.getElementById("<%= hid_Save.ClientID %>").value="";
            document.getElementById("<%= hid_Cate.ClientID %>").value="";
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1)
            {
                document.getElementById("<%= hid_Save.ClientID %>").value =document.getElementById("<%= hid_Tab.ClientID %>").value + 'Ø';
                if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
                {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");  
                    for (n = 0; n <= row.length - 1; n++) 
                    {                    
                        col = row[n].split("µ");
                     
                        if(document.getElementById("chk" + col[1]).checked==true)
                        {
                            
                            document.getElementById("<%= hid_Edit.ClientID %>").value = document.getElementById("<%= hid_Edit.ClientID %>").value + 'Ø' +  col[1].toString();
                        }
                         
                    }
                }  
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2)
            {
                document.getElementById("<%= hid_Save.ClientID %>").value =document.getElementById("<%= hid_Tab.ClientID %>").value + 'Ø';
                if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
                {
                    row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                   
                    for (n = 0; n <= row.length - 1; n++) 
                    {                    
                        col = row[n].split("µ");
                        if(document.getElementById("chk" + col[1]).checked==true)
                        {
                            
                            document.getElementById("<%= hid_Edit.ClientID %>").value = document.getElementById("<%= hid_Edit.ClientID %>").value + 'Ø' +  col[1].toString();
                        }
                        
                    }
                }  
            }
        }
        function delete_row(GroupId)
        {        
            if (confirm("Are You sure to Delete?")==1)
            {                
                var ToData = "7Ø"+document.getElementById("<%= hid_Tab.ClientID %>").value+"Ø"+ GroupId;
                ToServer(ToData, 7);
             }
        }
        function edit_row(GroupID,GroupName)
        {
            document.getElementById("<%= hid_Id.ClientID %>").value=GroupID;
            col = GroupName.split("¥");  
            
            if (document.getElementById("<%= hid_Tab.ClientID %>").value==1)
            {
               
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value==2)
            {
                         
            }
           
        }
        function ViewAttachment() 
        {
            var ProblemID = document.getElementById("<%= hid_Id.ClientID %>").value;
            if (ProblemID > 0)
                window.open("ShowFormat.aspx?ProblemID=" + btoa(ProblemID) + "");
            return false;
        }
        function table_fill() {

            document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
            
            var row_bg = 0;
            var tab = "";
            tab += "<br/><div class=mainhead style='width:100%; height:auto; padding-top:0px;margin:0px auto;' ><table style='width:100%;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1)
            {
                if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
                {
                    tab += "<td style='width:5%;text-align:center;' class=NormalText>#</td>";
                    tab += "<td style='width:25%;text-align:center' class=NormalText>Checklist</td>";
                    tab += "<td style='width:35%;text-align:center' class=NormalText>Remarks</td>";
                    tab += "<td style='width:10%;text-align:center' class=NormalText>Frequency</td>";
                    tab += "<td style='width:10%;text-align:center' class=NormalText>AffectedOn</td>";
                    tab += "<td style='width:5%;text-align:center' class=NormalText>Interval</td>";
                    tab += "<td style='width:5%;text-align:center' class=NormalText>Status</td>";
                    tab += "<td style='width:5%;'/></td>";
                }
                tab += "</tr></table></div><div class=mainhead style='width:100%; height:auto; overflow:auto; margin:0px auto;' ><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2)
            {
                if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
                {
                    tab += "<td style='width:5%;text-align:center;' class=NormalText>#</td>";
                    tab += "<td style='width:35%;text-align:center' class=NormalText>Task</td>";
                    tab += "<td style='width:35%;text-align:center' class=NormalText>Remarks</td>";
                    tab += "<td style='width:10%;text-align:center' class=NormalText>Weightage</td>";
                    tab += "<td style='width:10%;text-align:center' class=NormalText>Status</td>";
                    tab += "<td style='width:5%;'/></td>";
                }
                tab += "</tr></table></div><div class=mainhead style='width:100%; height:auto; overflow:auto; margin:0px auto;' ><table style='width:100%; margin:0px auto;font-family:'cambria';' align='center'>";
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3){
                tab += "<td style='width:75%;text-align:left' class=NormalText>Findings</td>";
                tab += "<td style='width:5%;'/></td>";
                tab += "<td style='width:5%;'/></td>";
                tab += "</tr></table></div><div class=mainhead style='width:85%; height:auto; overflow:auto; margin:0px auto;;' ><table style='width:100%; margin:0px auto;font-family:'cambria';' align='center'>";
            }           
                
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
            {
                
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");  
                for (n = 0; n <= row.length - 1; n++) 
                {                    
                    col = row[n].split("µ");  
                                    
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;                   
                    tab += "<td style='width:5%;text-align:center;' >" + i + "</td>";
                    var gp=col[0];
                    if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3)
                    {   
                        col1 = gp.split("ÿ"); 
                        tab += "<td style='width:75%;text-align:left;' >" + col1[0] + "</td>";
                    }
                    if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1)
                    {
                        var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)' >"+ col[0] +"</textarea>";
                        tab += "<td style='width:25%;text-align:left;' >" + txtBox + "</td>";
                        txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)' >"+ col[2] +"</textarea>";
                        tab += "<td style='width:35%;text-align:left;' >" + txtBox + "</td>";
                        tab += "<td style='width:10%;text-align:left;' >" + col[3] + "</td>";
                        tab += "<td style='width:10%;text-align:left;' >" + col[8] + "</td>";
                        tab += "<td style='width:5%;text-align:left;' >" + col[9] + "</td>";
                        tab += "<td style='width:5%;text-align:left;' >" + col[5] + "</td>";
                        var txtid = "chk" + col[1];
                        tab += "<td style='width:5%;text-align:center;'><input id='" + txtid + "' type='checkbox' onclick='return ApprovalOnCheck("+ col[1] +")'/></td>";
                    } 
                    if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2)
                    {
                        var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)' >"+ col[0] +"</textarea>";
                        tab += "<td style='width:35%;text-align:left;' >" + txtBox + "</td>";
                        txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)' >"+ col[2] +"</textarea>";
                        tab += "<td style='width:35%;text-align:left;' >" + txtBox + "</td>";
                        var Weightage = col[3];
                        txtBox = "<input id='txtRemarks" + col[1] + "' name='txtRemarks" + i + "' readonly='readonly' type='Text' style='width:99%;'  maxlength='5' value='" + Math.abs(Weightage).toFixed(2) + "'></textarea>";
                        tab += "<td style='width:10%;text-align:left;' >" + txtBox + "</td>";
                        tab += "<td style='width:10%;text-align:left;' >" + col[6] + "</td>";
                        var txtid = "chk" + col[1];
                        tab += "<td style='width:5%;text-align:center;'><input id='" + txtid + "' type='checkbox' onclick='return ApprovalOnCheck("+ col[1] +")'/></td>";
                    }                     
                    tab += "</tr>";
                }                
            }
            else
            {
                tab += "<td style='width:25%;text-align:center;'><b>No approval pending</b></td>";
                tab += "</tr>";
            }
            tab += "</table></div>";            
            document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
            //--------------------- Clearing Data ------------------------//
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
        function btnClear_onclick() {
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1){
                CategoryClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2){
                ModuleClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3){   
                ProblemClick();
            }
        }
        function btnViewHistory_onclick()
        {   
            window.open("Reports\\HistoryRpt.aspx", "_self");   
        }
        function btninvalid_onclick()
        {   
            window.open("Reports\\InvalidChecklistRpt.aspx", "_self"); 
        }
        function ApprovalOnCheck(checklistid)
        {
////            var row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
////            alert(row);
////            alert(checklistid);
////            if(row.length == 1)
////            {   
////                alert("hi1");
//                if(document.getElementById(checklistid).checked==true)
//                {
//                    alert("hi2");
//                    document.getElementById("<%= hid_Edit.ClientID %>").value = document.getElementById("<%= hid_Edit.ClientID %>").value + 'Ø' +  checklistid.toString();
//                }
////            }
//            var row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
//            if(row.length == 1)
//            {
              //table_fill();
//            }   
        }
    </script>
</head>
</html>
 <br />
 <div  style="width:90%;margin:0px auto; background-color:#EEB8A6;">
    <div style="width:80%;padding-left:10px;margin:0px auto;">
     <div id='1' class="Button" 
             style="width:30%; float:left;background:-moz-radial-gradient(center, ellipse cover, #EEB8A6 0%, #801424 90%, #EEB8A6 100%);" 
            onclick="return CategoryClick()">CheckList</div>
     <div id='2' class="Button" style="border-left:thick double #FFF;width:30%;float:left;" onclick="return ModuleClick()">Tasks</div>
      <br /> <br />
       </div>
    <div style="width:93%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
    <br /><br />
            <table style="width:90%;height:90px;margin:0px auto;">
            <tr id="Category" style="display:none;"> 
            <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria"></Font-Names>Checklist</td> 
                    <td colspan="2"><asp:DropDownList ID="cmbCategory" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="61%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList></td>
            </tr>
                <tr id="List">
                    <td style="text-align: center;" colspan="4">
                        <asp:Panel ID="pnlDtls" Style="width: 100%;  text-align: right; float: right;" runat="server">
                        </asp:Panel>
                        <asp:HiddenField ID="hid_Edit" runat="server" />
                        <asp:HiddenField ID="hid_Id" runat="server" />
                        <asp:HiddenField ID="hid_tab" runat="server" />
                        <asp:HiddenField ID="hid_dtls" runat="server" />
                        <asp:HiddenField ID="hid_Save" runat="server" />
                        <asp:HiddenField ID="hid_Cate" runat="server" />
                        <asp:HiddenField ID="hid_SubCate" runat="server" />
                    </td>
                </tr>
            </table> 
            
    </div>
    <div style="text-align:center; height: 63px;"><br />
    <asp:Button 
                ID="btnApprove" runat="server" Text="APPROVE" Font-Names="Cambria" style="cursor:pointer;"
                 Width="8%" />
    <asp:Button 
                ID="btnReject" runat="server" Text="REJECT" Font-Names="Cambria" style="cursor:pointer;"
                 Width="8%" />
    
 &nbsp;
                        &nbsp;

                        <input id="Button1" style="font-family: cambria; cursor: pointer; width:8%;" 
                            type="button" value="CLEAR" onclick="return btnClear_onclick()"/> &nbsp;
                         &nbsp;
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" 
                            type="button" value="EXIT" onclick="return btnExit_onclick()"/>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                        <input id="btnHistory" style="font-family: cambria; cursor: pointer; width:8%;" 
                        type="button" value="VIEW HISTORY" onclick="return btnViewHistory_onclick()"/>
                        &nbsp;
                        <input id="btnInvalid" style="font-family: cambria; cursor: pointer; width:15%;" 
                        type="button" value="INVALID CHECKLIST" onclick="return btninvalid_onclick()"/></div>

    </div>   
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

