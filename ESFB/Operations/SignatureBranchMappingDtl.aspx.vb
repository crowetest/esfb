﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class SignatureBranchMappingDtl
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1201) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Me.Master.subtitle = "Signature Mapping Details"
         
            cmbSearch.Attributes.Add("onchange", "return SearchOnChange()")
            txtBranchCode.Attributes.Add("onchange", "return BranchCodeOnChange()")
            txtEmpCode.Attributes.Add("onchange", "return EmployeeCodeOnChange()")
            txtPOA.Attributes.Add("onchange", "return POAOnChange()")

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim dataval As String = CStr(Data(1))
        Dim Sdat, Tdat As String
        Dim StrWhere As String = " 1 = 1 "
        Dim Status As String = ""
        If CInt(Data(0)) = 1 Or CInt(Data(0)) = 4 Then
            Dim BrID As Integer = CInt(Data(1))
            StrWhere = "A.Branch_id = " + BrID.ToString()
        ElseIf CInt(Data(0)) = 2 Or CInt(Data(0)) = 5 Then
            Dim EmpCode As Integer = CInt(Data(1))
            StrWhere = "D.Emp_code = " + EmpCode.ToString()
        ElseIf CInt(Data(0)) = 3 Or CInt(Data(0)) = 6 Then
            Dim POA As String = CStr(Data(1))
            StrWhere = "H.POA_No = '" + POA.ToString() + "'"
        End If
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Or CInt(Data(0)) = 2 Or CInt(Data(0)) = 3 Then
            Status = "1"
        ElseIf CInt(Data(0)) = 4 Or CInt(Data(0)) = 5 Or CInt(Data(0)) = 6 Then
            Status = "0"
        End If
        DT = DB.ExecuteDataSet("select A.branch_id,branch_name, District_name , state_name ,D.emp_code,emp_name, E.Department_name,F.Designation_name,G.Cadre_name,Date_of_join, POA_No,case when H.sign_id is null then '' else H.sign_id end  as Signature  " +
             " from branch_master A  inner join District_master B on A.district_id = B.district_id  inner join State_master C on A.State_id = C.State_id   " +
             " inner join emp_master D on A.BRANCH_ID = d.BRANCH_ID inner join Sign_master H on D.emp_code = H.Emp_code  and isnull(H.checker_status,0)  in (1) AND H.STATUS_ID =1   inner join Department_master E on D.Department_id = E.Department_id   " +
             " inner join Designation_master F on D.Designation_id = F.Designation_id  inner join Cadre_master G on D.Cadre_id = G.Cadre_id   " +
             " where " + StrWhere + " And D.Status_id = 1").Tables(0)

        If DT.Rows.Count > 0 Then
            If CInt(Data(0)) = 1 Or CInt(Data(0)) = 4 Then
                CallBackReturn = DT.Rows(0)(1).ToString().ToUpper() + "~" + DT.Rows(0)(2).ToString().ToUpper() + "~" + DT.Rows(0)(3).ToString().ToUpper() + "~"
            End If
            For n As Integer = 0 To DT.Rows.Count - 1
                'If IsDBNull(DT.Rows(n)(13)) Then
                '    Sdat = ""
                'Else
                '    Sdat = CDate(DT.Rows(n)(13)).ToString("dd-MMM-yyyy").ToString()
                'End If
                'If IsDBNull(DT.Rows(n)(14)) Then
                '    Tdat = ""
                'Else
                '    Tdat = CDate(DT.Rows(n)(14)).ToString("dd-MMM-yyyy").ToString()
                'End If
                If CInt(Data(0)) = 1 Then
                    CallBackReturn += "¥" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + CDate(DT.Rows(n)(9)).ToString("dd-MMM-yyyy") + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + DT.Rows(n)(11).ToString().ToUpper() ''+ "µ" + DT.Rows(n)(12).ToString().ToUpper() + "µ" + Sdat + "µ" + Tdat
                ElseIf CInt(Data(0)) = 2 Then
                    CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + CDate(DT.Rows(n)(9)).ToString("dd-MMM-yyyy") + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + DT.Rows(n)(11).ToString().ToUpper() ''+ "µ" + DT.Rows(n)(12).ToString().ToUpper() + "µ" + Sdat + "µ" + Tdat
                ElseIf CInt(Data(0)) = 3 Then
                    CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(6).ToString().ToUpper() + "µ" + DT.Rows(n)(7).ToString().ToUpper() + "µ" + DT.Rows(n)(8).ToString().ToUpper() + "µ" + CDate(DT.Rows(n)(9)).ToString("dd-MMM-yyyy") + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + DT.Rows(n)(11).ToString().ToUpper() '' + "µ" + DT.Rows(n)(12).ToString().ToUpper() + "µ" + Sdat + "µ" + Tdat
                ElseIf CInt(Data(0)) = 4 Then
                    CallBackReturn += "¥" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + DT.Rows(n)(11).ToString().ToUpper() ''+ "µ" + DT.Rows(n)(12).ToString().ToUpper() + "µ" + Sdat + "µ" + Tdat
                ElseIf CInt(Data(0)) = 5 Then
                    CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(10).ToString().ToUpper() + "µ" + DT.Rows(n)(11).ToString().ToUpper() ''+ "µ" + DT.Rows(n)(12).ToString().ToUpper() + "µ" + Sdat + "µ" + Tdat
                ElseIf CInt(Data(0)) = 6 Then
                    CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(4).ToString().ToUpper() + "µ" + DT.Rows(n)(5).ToString().ToUpper() + "µ" + DT.Rows(n)(11).ToString().ToUpper() ''+ "µ" + DT.Rows(n)(12).ToString().ToUpper() + "µ" + Sdat + "µ" + Tdat
                End If
            Next
        End If


    End Sub
#End Region

End Class
