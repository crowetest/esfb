﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class SignatureStatusChange

    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim UserID As String
    Dim TypeId As Integer


#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            UserID = Session("UserID").ToString()


            Me.Master.subtitle = "Signature Status Change"
            If GN.FormAccess(CInt(Session("UserID")), 1447) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            If Not IsPostBack Then

                '--//---------- Script Registrations -----------//--
                '/--- For Call Back ---//
                Dim DTT As New DataTable
                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

                txtEmpCode.Attributes.Add("onblur", "return EmpCodeOnBlur()")
                'Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
                txtEmpName.Attributes.Add("readonly", "readonly")
                txtBranchCode.Attributes.Add("readonly", "readonly")
                txtBranchName.Attributes.Add("readonly", "readonly")
                txtDepartment.Attributes.Add("readonly", "readonly")
                txtDesignation.Attributes.Add("readonly", "readonly")
                txtCadre.Attributes.Add("readonly", "readonly")

                txtEmpCode.Focus()
            End If

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))

        If CInt(Data(0)) = 1 Then
            Dim EmpCode As String
            EmpCode = CStr(Data(1))
            DT = DB.ExecuteDataSet("select A.emp_code,emp_name,a.branch_id, B.branch_name, C.Department_name,D.Designation_name,E.Cadre_name,F.sign_id, isnull(F.Status_id,0) from emp_master A " +
                " inner join Branch_master B on A.branch_id = B.Branch_id  " +
                " inner join Department_master C on A.Department_id = C.Department_id " +
                " inner join Designation_master D on A.Designation_id = D.Designation_id " +
                " inner join Cadre_master E on A.Cadre_id = E.Cadre_id " +
                " left join Sign_master F on A.emp_code = F.Emp_code  and isnull(f.checker_status,0)  in (0,1) " +
                " where (A.emp_code = " + EmpCode + " And isnull(F.Status_id,0) <> 0 )").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString + "|" + DT.Rows(0)(1).ToString + "|" + DT.Rows(0)(2).ToString + "|" + DT.Rows(0)(3).ToString + "|" + DT.Rows(0)(4).ToString + "|" + DT.Rows(0)(5).ToString + "|" + DT.Rows(0)(6).ToString + "|" + DT.Rows(0)(7).ToString
                CallBackReturn += "µ"
                For n As Integer = 0 To DT.Rows.Count - 1
                    CallBackReturn += "Ñ" + "-1" + "ÿ" + "----Select----"
                    If CInt(DT.Rows(0)(8)) = 1 Then 'Active signature. So Need to Inactive
                        CallBackReturn += "Ñ" + "2" + "ÿ" + "In-Active"
                    ElseIf CInt(DT.Rows(0)(8)) = 1 Then 'InActive signature. So Need to active
                        CallBackReturn += "Ñ" + "1" + "ÿ" + "Active"

                    End If
                Next
            End If


        ElseIf CInt(Data(0)) = 2 Then
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim EmpCode, Status, SignID As Integer
            Dim Remarks As String

            SignID = CInt(Data(1))
            EmpCode = CInt(Data(2))
            Status = CInt(Data(3))
            Remarks = CStr(Data(4))

            Dim Params(6) As SqlParameter
            Params(0) = New SqlParameter("@SIGN_ID", SqlDbType.Int)
            Params(0).Value = SignID
            Params(1) = New SqlParameter("@EMP_CODE", SqlDbType.Int)
            Params(1).Value = EmpCode
            Params(2) = New SqlParameter("@Status", SqlDbType.Int)
            Params(2).Value = Status
            Params(3) = New SqlParameter("@USERID", SqlDbType.Int)
            Params(3).Value = UserID
            Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(4).Direction = ParameterDirection.Output
            Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(5).Direction = ParameterDirection.Output
            Params(6) = New SqlParameter("@REMARKS", SqlDbType.VarChar, 2000)
            Params(6).Value = Remarks

            DB.ExecuteNonQuery("SP_SIGN_STATUS_CHANGE", Params)
            ErrorFlag = CInt(Params(4).Value)
            Message = CStr(Params(5).Value)

            CallBackReturn = ErrorFlag.ToString + "|" + Message
            If ErrorFlag = 0 Then
                initializeControls()
            End If
        End If
    End Sub
#End Region
    Private Sub initializeControls()
        txtEmpCode.Text = ""
        txtEmpName.Text = ""
        txtBranchCode.Text = ""
        txtBranchName.Text = ""
        txtDepartment.Text = ""
        txtDesignation.Text = ""
        txtCadre.Text = ""
    
        txtEmpCode.Focus()
    End Sub
   
End Class
