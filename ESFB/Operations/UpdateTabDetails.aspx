﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="UpdateTabDetails.aspx.vb" Inherits="Operations_UpdateTabDetails" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <table align="center" style="width: 40%; margin:0px auto;">
        <tr>
            <td style="width:30%">
                &nbsp;</td>
            <td style="width:70%">
                <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </ajaxToolkit:ToolkitScriptManager>
            </td>
        </tr>
        <tr>
            <td style="width:30%">
                Emp Code</td>
            <td style="width:70%">
                <input id="txtEmpCode" type="text" class="NormalText" style="width: 30%" runat="server" /></td>
        </tr>
        <tr>
            <td style="width:30%">
                Emp Name</td>
            <td style="width:70%">
                <input id="txtEmpName" style="width: 89%" type="text" class="NormalText"  runat="server"/></td>
        </tr>
        <tr>
            <td style="width:30%">
                Branch</td>
            <td style="width:70%">
                <asp:DropDownList ID="ddlBranch" runat="server" Width="90%" class="NormalText">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width:30%">
                Role</td>
            <td style="width:70%">
                <asp:DropDownList ID="ddlRole" runat="server" Width="90%" class="NormalText">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width:30%">
                IMEI No</td>
            <td style="width:70%">
                <input id="txtIMENo" style="width: 89%; text-transform:uppercase " type="text" class="NormalText" maxlength="20" /></td>
        </tr>
        <tr>
            <td style="width:30%">
                Data SIM Provider</td>
            <td style="width:70%">
                <asp:DropDownList ID="ddlSIMProvider" runat="server" Width="90%" class="NormalText">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width:30%">
                Data SIM No</td>
            <td style="width:70%">
                <input id="txtSIMNo" style="width: 89%" type="text" class="NormalText" maxlength="12" onkeypress="return NumericCheck(event)" /></td>
        </tr>
      
        <tr>
            <td style="width:30%">
                OwnerShip Type</td>
            <td style="width:70%">
                <asp:DropDownList ID="ddlOwnerType" runat="server" Width="90%" 
                    class="NormalText">
                    <asp:ListItem Value="1">OWNER</asp:ListItem>
                    <asp:ListItem Value="2">CUSTODIAN</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
          <tr>
            <td style="width:30%">
                Alloted On</td>
            <td style="width:70%">
                <asp:TextBox ID="txtAllotedOn" runat="server" class="NormalText" Width="30%" ReadOnly="true" ></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="txtAllotedOn_CalendarExtender" runat="server" format="dd MMM yyyy"
                    Enabled="True" TargetControlID="txtAllotedOn">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="width:30%">
                &nbsp;</td>
            <td style="width:70%">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                <input id="btnSave" type="button" value="SAVE" class="NormalText" 
                    style="width: 15%" onclick="return btnSave_onclick()" />&nbsp;
                <input id="btnExit" type="button" value="EXIT" class="NormalText" 
                    style="width: 15%" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
         window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
        }

        function btnSave_onclick() {
            var EmpCode             =   document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if(EmpCode=="")
            {alert("Enter Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus();return false;}
            var EmpName             =   document.getElementById("<%= txtEmpName.ClientID %>").value;
            if(EmpName=="")
            {alert("Enter Employee Name"); document.getElementById("<%= txtEmpName.ClientID %>").focus();return false;}
            var BranchID            =   document.getElementById("<%= ddlBranch.ClientID %>").value;
            if(BranchID==-1)
            {alert("Select Branch"); document.getElementById("<%= ddlBranch.ClientID %>").focus();return false;}
            var RoleID              =   document.getElementById("<%= ddlRole.ClientID %>").value;
            if(RoleID==-1)
            {alert("Select Role"); document.getElementById("<%= ddlRole.ClientID %>").focus();return false;}
            var IMEINo              =   document.getElementById("txtIMENo").value;
            if(IMEINo=="" || IMEINo.length<15)
            {alert("Enter a valid IMEI No"); document.getElementById("txtIMENo").focus();return false;}
            var SIMProvideID        =   document.getElementById("<%= ddlSIMProvider.ClientID %>").value;
            if(SIMProvideID==-1)
            {alert("Select Data SIM Provider"); document.getElementById("<%= ddlSIMProvider.ClientID %>").focus();return false;}
            var SIMNo               =   document.getElementById("txtSIMNo").value;
            if(SIMNo=="" || SIMNo.length<10)
            {alert("Enter a valid Data SIM No"); document.getElementById("txtSIMNo").focus();return false;}
            var OwnerShipType       =   document.getElementById("<%= ddlOwnerType.ClientID %>").value;
            var AllotedOn           =   document.getElementById("<%= txtAllotedOn.ClientID %>").value;
            if(AllotedOn=="")
            {alert("Select Alloted Date");document.getElementById("<%= txtAllotedOn.ClientID %>").focus();return false;}
            var ToData = "1Ø" + EmpCode+"Ø"+BranchID+"Ø"+RoleID+"Ø"+IMEINo+"Ø"+SIMProvideID+"Ø"+SIMNo+"Ø"+OwnerShipType+"Ø"+AllotedOn;          
            ToServer(ToData, 1);
        }


        function EmpCodeOnChange() {
            var EmpCode=document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if(EmpCode.length>2)
            {
                var ToData = "2Ø" + EmpCode;          
                ToServer(ToData, 2);
            }
            else
            {
                alert("Enter a valid Employee Code");
                document.getElementById("<%= txtEmpName.ClientID %>").value="";
            }
        }


           function FromServer(Arg, Context) {
            switch (Context) {

                case 1:
                    {   var Data = Arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("UpdateTabDetails.aspx", "_self");
                        break;
                    }
                case 2:
                    {
                        if(Arg=="")
                        {
                             alert("Invalid Employee Code");
                             document.getElementById("<%= txtEmpName.ClientID %>").value="";
                        }
                        else
                        {   var Dtl=Arg.split("~");
                            document.getElementById("<%= txtEmpName.ClientID %>").value=Dtl[0];
                            document.getElementById("<%= ddlBranch.ClientID %>").value=Dtl[1];
                            if(document.getElementById("<%= ddlBranch.ClientID %>").selectedIndex==-1)
                                document.getElementById("<%= ddlBranch.ClientID %>").value=-1;
                                 document.getElementById("<%= ddlBranch.ClientID %>").focus();
                            }
                        break;
                    }                            
              
            }
        }


// ]]>
    </script>
</asp:Content>

