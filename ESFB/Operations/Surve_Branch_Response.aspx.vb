﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Surve_Branch_Response
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1223) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Me.Master.subtitle = "Branch Response"
            DT = GetFrequency()
            If Not DT Is Nothing Then
                GF.ComboFill(cmbFrequency, DT, 0, 1)
            End If
            DT = DB.ExecuteDataSet("select -1, '--Select--'").Tables(0)
            If Not DT Is Nothing Then
                GF.ComboFill(cmbChecklist, DT, 0, 1)
            End If

            Me.cmbFrequency.Attributes.Add("onchange", "return FrequencyOnchange()")
            Me.cmbChecklist.Attributes.Add("onchange", "return CheckListOnchange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim BranchID As Integer = CInt(Session("BranchID"))

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim FreqId As Integer
            FreqId = CInt(Data(1))
            DT = DB.ExecuteDataSet("select -1 as checklistid, '--Select--' as Description union select A.checklistid, Description  + ' [' + case when C.Resp_master_id is NULL then 'Pending' else 'Completed' end + ']' as Description FROM Surve_ChkList_Master A inner join Surve_Task_Master B on A.checklistid = B.checklistid " +
                " left join Surve_Branch_Response_master C on A.checklistid= C.checklistid and C.branch_id = " + BranchID.ToString() + " and A.NextRespDate = C.NextRespDate  and C.Finish_Status =1 where A.Status_id = 1 and B.Status_id = 1 and A.Frequency =  " + FreqId.ToString + "  and convert(date,getdate()) between convert(date,A.NextRespDate) and convert(date,dateadd(dd,A.interval,A.NextRespDate)) group by  A.checklistid, Description,C.Resp_master_id  ").Tables(0)
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += "Ñ" + DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 2 Then
            Dim CheckListId As Integer
            CheckListId = CInt(Data(1))

            'DT = DB.ExecuteDataSet("select TaskID, TaskDesc,isnull( B.Resp_type,-1), isnull(B.Resp_comments,'') FROM Surve_Task_Master A left join Surve_Branch_Response B on A.TaskID = B.Task_ID and A.Status_ID = 1 and Frequency = " + CheckListId.ToString + " where checklistid= 1").Tables(0)

            DT = DB.ExecuteDataSet("select distinct TaskID, TaskDesc,isnull( B.Resp_type,-1), isnull(B.Resp_comments,'') ,isnull( C.Finish_Status,0) as Finish_Status " +
                " FROM Surve_Task_Master A  " +
                " left join Surve_ChkList_Master D on D.checklistid = A.checklistid  and A.status_id = 1 and D.Status_ID = 1  and convert(date,getdate()) between convert(date,D.NextRespDate) and convert(date,dateadd(dd,D.interval,D.NextRespDate))" +
                " left join Surve_Branch_Response_master C on C.checklistid= D.checklistid  And C.branch_id = " + BranchID.ToString() + " and C.NextRespDate = D.NextRespDate " +
                " left join Surve_Branch_Response B on A.TaskID = B.Task_ID  and C.Resp_master_id = B.Resp_master_id  " +
                " where D.checklistid = " + CheckListId.ToString() + " ").Tables(0)

            If DT.Rows.Count > 0 Then
                If CInt(DT.Rows(0)(4).ToString()) = 1 Then
                    'CallBackReturn += "1" + "Ø" + "¥" + "" + "µ" + "" + "µ" + "" + "µ" + ""
                    CallBackReturn += "1" + "Ø"
                Else
                    CallBackReturn += "0" + "Ø"
                End If
                For n As Integer = 0 To DT.Rows.Count - 1
                    CallBackReturn += "¥" + DT.Rows(n)(0).ToString().ToUpper() + "µ" + DT.Rows(n)(1).ToString().ToUpper() + "µ" + DT.Rows(n)(2).ToString().ToUpper() + "µ" + DT.Rows(n)(3).ToString().ToUpper()
                Next

            End If


        ElseIf CInt(Data(0)) = 3 Or CInt(Data(0)) = 4 Then
            Dim dataval As String = CStr(Data(1))
            Dim UserID As Integer = CInt(Session("UserID"))
          
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim Save_Flag As Integer
            Save_Flag = CInt(IIf(CInt(Data(0)) = 4, 1, 0))
            Dim CheckListId As Integer = CInt(Data(2))
            Try

                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(1).Value = BranchID
                Params(2) = New SqlParameter("@Checklist_Id", SqlDbType.Int)
                Params(2).Value = CheckListId
                Params(3) = New SqlParameter("@RespDtl", SqlDbType.VarChar, 2000)
                Params(3).Value = dataval.Substring(1)
                Params(4) = New SqlParameter("@SAVE", SqlDbType.Int)
                Params(4).Value = Save_Flag
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_SURVE_BRANCH_RESPONSE", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + Save_Flag.ToString()
        End If

    End Sub
#End Region

#Region "Function"
    Public Function GetFrequency() As DataTable
        'Return DB.ExecuteDataSet("select -1 as Id, '--Select--' as Value union select Id, Value FROM Surve_Frequency_Master where status= 1").Tables(0)

        Return DB.ExecuteDataSet("select -1 as Id, '--Select--' as Value union select Id, Value+space(3) + '[ Pending '+ convert(varchar, count(B.Frequency)- count(c.checklistid)) +']' as Value FROM Surve_Frequency_Master A " +
            " inner join Surve_ChkList_Master B on B.Frequency = A.id and B.Status_id = 1 and A.Status = 1 and convert(date,getdate()) between convert(date,B.NextRespDate) and convert(date,dateadd(dd,B.interval,B.NextRespDate))  " +
            " inner join (select distinct checklistid from Surve_Task_Master where Status_id = 1) D on B.checklistid = D.checklistid " +
            " left join Surve_Branch_Response_master C on B.checklistid= C.checklistid and C.branch_id = " + CInt(Session("BranchID")).ToString() + " and B.NextRespDate = C.NextRespDate  and Finish_Status= 1  " +
            " group by  Id, Value having count(B.Frequency)- count(c.checklistid) <> 0 ").Tables(0)

    End Function
#End Region
End Class
