﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Downloads
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler

    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim Leave As New Leave
    Dim CallBackReturn As String
    Dim cardID As Integer
    Private Property DT1 As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        cardID = CInt(Request.QueryString.Get("ID"))

        If cardID = 2 Then
            hid_Card.Value = 2

        End If
        'Dim depID As Integer
        Dim EMP_Code As Integer

        EMP_Code = CInt(Session("UserID"))

        hid_EmpCode.Value = EMP_Code
        DT = GF.GetEmployee(EMP_Code)
        hid_dtls.Value = GF.GetEmployeeString(DT)
        hid_User.Value = CStr(DT.Rows(0).Item(1))
        DT = GF.GetPhoto(EMP_Code)
        If DT.Rows.Count > 0 Then
            Dim bytes As Byte() = DirectCast(DT.Rows(0)(0), Byte())
            Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
            hid_image.Value = Convert.ToString("data:" + DT.Rows(0)(1) + ";base64,") & base64String
        Else
            hid_image.Value = "Image/userimage.png"
        End If
        Dim DT_Quali As New DataTable
        Dim DT_Exp As New DataTable
        Dim DT_ID As New DataTable
        Dim DT_ADDR As New DataTable
        Dim DT_FAM As New DataTable
        Dim DT_LANG As New DataTable

        Dim strTS As New StringBuilder

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim LoginUSerID As Integer = 0
        Dim StrEmployee As String = ""
        Dim strTS As New StringBuilder
        ' Dim row As Integer-0
       

        If CInt(Data(0)) = 1 Then 'Travel
            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,content_Type,Caption,View_Flag,Description from dms_ESFB.dbo .hr_policies where Dep_ID =" + Data(1).ToString() + "  and status_id=1 and CONVERT(VARCHAR(10), effective_dt<=DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)
            For n As Integer = 0 To DT.Rows.Count - 1
                StrEmployee += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString
                If n < DT.Rows.Count - 1 Then
                    StrEmployee += "¥"
                End If
            Next
        ElseIf CInt(Data(0)) = 2 Then 'folder display
            Dim Department As Integer = CInt(Data(1).ToString)

            'DT1 = DB.ExecuteDataSet("select  FOLDER_ID as value,FOLDER_NAME from HR_FOLDER where dept_id= " + Department.ToString() + " and status=1 ").Tables(0)
            DT1 = DB.ExecuteDataSet("select distinct  a.folder_id as value ,a.folder_name as value, a.dept_id from hr_folder a inner join  dms_ESFB.dbo.hr_policies b on a.folder_id = b.folder_id   where a.dept_id= " + Department.ToString() + " and status =1").Tables(0)
            'DT1 = DB.ExecuteDataSet("select distinct  a.FOLDER_ID as value,a.FOLDER_NAME,b.effective_dt from HR_FOLDER a inner join dms_ESFB.dbo.hr_policies b on a.folder_id = b.folder_id where a.dept_id= " + Department.ToString() + " and a.status=1 and b.effective_dt<= DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)
            For n As Integer = 0 To DT1.Rows.Count - 1
                StrEmployee += DT1.Rows(n)(0) & "^" & DT1.Rows(n)(1)
                If n < DT1.Rows.Count - 1 Then
                    StrEmployee += "¥"
                End If
            Next
            hid_Hrfolder.Value = StrEmployee
        ElseIf CInt(Data(0)) = 15 Then 'document display

            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,content_Type,Caption,View_Flag,Description from dms_ESFB.dbo.hr_policies where Dep_ID=" + Data(2).ToString() + "  and FOLDER_ID= " + Data(1).ToString() + " and status_id=1 and effective_dt<=DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)

            For n As Integer = 0 To DT.Rows.Count - 1
                StrEmployee += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString
                'If n <= DT.Rows.Count Then
                If n < DT.Rows.Count - 1 Then
                    StrEmployee += "¥"
                End If
            Next
        End If
        CallBackReturn = StrEmployee.ToString
    End Sub
End Class
