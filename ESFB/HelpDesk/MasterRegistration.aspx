﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="MasterRegistration.aspx.vb" Inherits="MasterRegistration" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script src="../Script/jquery.js" type="text/javascript"></script>
    <style type="text/css">
          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;
          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);
           
           
        }
        
        .Button:hover
        {
            
           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            
            color:#801424;
        }   
                 
     
     .bg
     {
         background-color:#FFF;
     }
           
    </style>
    
    <script language="javascript" type="text/javascript">

        function IntialCtrl(){
            alert("Saved Successfully");
            document.getElementById("<%= hid_Id.ClientID %>").value = 0;
            document.getElementById("<%= hid_Edit.ClientID %>").value = 0;
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1) {
                CategoryClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2) {
                ModuleClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3) {
                ProblemClick();
            }               
        }
        function FromServer(arg, context) {           
           if (context == 2) {
                var Data = arg.split("~");
                document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                
                if (document.getElementById("<%= hid_dtls.ClientID %>").value !="") {
                    table_fill();
                    document.getElementById("<%= txtCateName.ClientID %>").focus();
                }
                else
                {
                    document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
                    document.getElementById("<%= txtCateName.ClientID %>").focus();
                }               
            }
            else if (context==3) {

                var Data = arg.split("~");
                ComboFill(Data[0], "<%= cmbCategory.ClientID %>");
                if  (document.getElementById("<%= hid_Tab.ClientID %>").value==3) {
                    ComboFill(Data[1], "<%= cmbDirectTeam.ClientID %>");
                }
                document.getElementById("<%= txtModuleName.ClientID %>").value=""
                document.getElementById("<%= cmbCategory.ClientID %>").focus();
                if  (document.getElementById("<%= hid_Cate.ClientID %>").value != "")  {
                    document.getElementById("<%= cmbCategory.ClientID %>").value=document.getElementById("<%= hid_Cate.ClientID %>").value;
                    document.getElementById("<%= hid_Cate.ClientID %>").value="";
                    CategoryOnChange();
                }  

                
                          
            }
            else if (context==4) {                
                var Data = arg.split("~");
                if (Data[0] == 1) {
                    document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                    table_fill();   
                }
                else
                    document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
            }
            else if (context==5) {
                ComboFill(arg, "<%= cmbModule.ClientID %>");
                document.getElementById("<%= cmbModule.ClientID %>").focus();

                if  (document.getElementById("<%= hid_SubCate.ClientID %>").value != "")  {
                    document.getElementById("<%= cmbModule.ClientID %>").value=document.getElementById("<%= hid_SubCate.ClientID %>").value;
                    document.getElementById("<%= hid_SubCate.ClientID %>").value="";
                    ModuleOnChange();
                }
            }
            else if (context==6) {                
                var Data = arg.split("~");
                if (Data[0] == 1) {
                    document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                    table_fill();
                    document.getElementById("<%= txtProblem.ClientID %>").value=""
                    document.getElementById("<%= txtProblem.ClientID %>").focus();
                    document.getElementById("<%= txtDescription.ClientID %>").value = "";
                    document.getElementById("<%= chkCritical.ClientID %>").checked = false;
                    document.getElementById("<%= cmbApp.ClientID %>").value = "-1";
                    document.getElementById("ViewAtur").style.display = 'none';
                }
                else
                    document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
            }
            else if (context==7) {
                var Data = arg.split("~");
                alert(Data[1]);                                
                if (Data[0] == 1) {
                    if  (document.getElementById("<%= hid_Tab.ClientID %>").value==1) {                        
                        CategoryFill();
                        document.getElementById("<%= txtCateName.ClientID %>").focus();
                    }                                    
                    else if (document.getElementById("<%= hid_Tab.ClientID %>").value==2) {
                         var ToData = "4Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbCategory.ClientID  %>").value;                         
                        ToServer(ToData, 4)
                        document.getElementById("<%= txtModuleName.ClientID %>").focus();
                    }
                    else if (document.getElementById("<%= hid_Tab.ClientID %>").value=3) {
                        var ToData = "6Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbModule.ClientID  %>").value;
                        ToServer(ToData, 6)
                        document.getElementById("<%= txtProblem.ClientID %>").focus();
                    }
                }
            }
            // 'shima 17/10
            else if (context==8) {                 
                var Data = arg.split("~");                
                if (Data[0]==1) {
                    var Data1= Data[1].split("¥");                    
                    document.getElementById("<%= chkAm.ClientID %>").checked = false;
                    document.getElementById("<%= chkRm.ClientID %>").checked = false;
                    document.getElementById("<%= chkGm.ClientID %>").checked = false;
                    document.getElementById("<%= chkTm.ClientID %>").checked = false;
                    document.getElementById("<%= chkNps.ClientID %>").checked = false;
                    document.getElementById("<%= chkBranchDesk.ClientID %>").checked = false;
                    document.getElementById("<%= cmbApp.ClientID %>").value=Data1[0];  
                    var ApproveDtl =Data1[1].split("$");
                    for (a = 0; a < ApproveDtl.length-1; a++) 
                    {
                        if(ApproveDtl[a]=="6")
                        {
                            document.getElementById("<%= chkAm.ClientID %>").checked = true;
                        }
                        else if(ApproveDtl[a]=="7")
                        {
                            document.getElementById("<%= chkRm.ClientID %>").checked = true;
                        }
                        else if(ApproveDtl[a]=="8")
                        {
                            document.getElementById("<%= chkTm.ClientID %>").checked = true;
                        }
                        else if(ApproveDtl[a]=="9")
                        {
                            document.getElementById("<%= chkGm.ClientID %>").checked = true;
                        }
                        else if(ApproveDtl[a]=="10")
                        {
                            document.getElementById("<%= chkNps.ClientID %>").checked = true;
                        }
                        else if(ApproveDtl[a]=="11")
                        {
                            document.getElementById("<%= chkBranchDesk.ClientID %>").checked = true;
                        }
                    }       
                    document.getElementById("<%= txtProblem.ClientID %>").focus();
                }
            } else if (context == 9) {
                document.getElementById("<%= hidTatDtl.ClientID %>").value = arg;
                FillTatDtl();
            }
//            else if (context==9) {
//                 ComboFill(arg, "<%= cmbDirectTeam.ClientID %>");

//            }
        }
        function CategoryFill() {
            var ToData = "2Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
            ToServer(ToData, 2)
        }
        function FillCategoryCombo(){
            
            var ToData = "3Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
            ToServer(ToData, 3);
        }
        function FillDirectTeamCombo(){
            
            var ToData = "9Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
            ToServer(ToData, 9);
        }
        function CategoryOnChange() {            
            if  (document.getElementById("<%= hid_Tab.ClientID %>").value == 2){
                var ToData = "4Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbCategory.ClientID  %>").value;
                ToServer(ToData, 4)
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3){                
                if (document.getElementById("<%= cmbCategory.ClientID %>").value != -1) {
                     var ToData = "5Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbCategory.ClientID %>").value;
                    ToServer(ToData, 5);
                }
                else
                    ClearCombo("<%= cmbModule.ClientID %>")
                document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';                  
            }
        }
        function ModuleOnChange() {
            if (document.getElementById("<%= cmbModule.ClientID %>").value != -1) {
                var ToData = "6Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbModule.ClientID  %>").value;
                    ToServer(ToData, 6)
            }
            else
                document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
        }  
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function CategoryClick() {            
            document.getElementById("CategoryName").style.display = '';
            document.getElementById("Category").style.display = 'none';
            document.getElementById("ModuleName").style.display = 'none';
            document.getElementById("Module").style.display = 'none';
            document.getElementById("Problem").style.display = 'none';
            document.getElementById("Root").style.display = 'none';
            document.getElementById("AppMode").style.display = 'none';
            document.getElementById("AppMode1").style.display = 'none';
            document.getElementById("Description").style.display = 'none';
            document.getElementById("Annexture").style.display = 'none';
            document.getElementById("ViewAtur").style.display = 'none';
            document.getElementById("DirectTeam").style.display = 'none';
            $('.Tat').hide();
            $('.Severity').hide();

            document.getElementById("<%= hid_tab.ClientID %>").value = 1;
            document.getElementById("<%= hid_Id.ClientID %>").value = 0;
            document.getElementById("<%= hid_Edit.ClientID %>").value = 0;
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';

            document.getElementById("1").style.background = '-moz-radial-gradient(center, ellipse cover,  #EEB8A6 0%, #801424 90%, #EEB8A6 100%)';
            document.getElementById("1").style.color = '#CF0D0D';
            document.getElementById("2").style.background = '-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("2").style.color = '#CF0D0D';
            document.getElementById("3").style.background = '-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("3").style.color = '#CF0D0D';
            

            document.getElementById("<%= txtCateName.ClientID %>").value = "";
            document.getElementById("<%= cmbCategory.ClientID %>").value = "";
            document.getElementById("<%= txtModuleName.ClientID %>").value = "";
            CategoryFill();                      
            document.getElementById("<%= txtCateName.ClientID %>").focus();
        }
        function ModuleClick() {
            document.getElementById("CategoryName").style.display = 'none';
            document.getElementById("Category").style.display = '';
            document.getElementById("ModuleName").style.display = '';

            document.getElementById("Module").style.display = 'none';
            document.getElementById("Problem").style.display = 'none';
            document.getElementById("Root").style.display = 'none';
            document.getElementById("AppMode").style.display = 'none';
            document.getElementById("AppMode1").style.display = 'none';
            document.getElementById("Description").style.display = 'none';
            document.getElementById("Annexture").style.display = 'none';
            document.getElementById("ViewAtur").style.display = 'none';
            document.getElementById("DirectTeam").style.display = 'none';
            $('.Tat').hide();
            $('.Severity').hide();

            document.getElementById("<%= hid_tab.ClientID %>").value = 2;
            document.getElementById("<%= hid_Id.ClientID %>").value = 0;
            document.getElementById("<%= hid_Edit.ClientID %>").value = 0;
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';            

            document.getElementById("1").style.background =  '-moz-radial-gradient(center, ellipse cover,#F1F3F4 0%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("1").style.color = '#CF0D0D';
            document.getElementById("2").style.background =  '-moz-radial-gradient(center, ellipse cover,  #EEB8A6 20%, #801424 90%, #EEB8A6 100%)';
            document.getElementById("2").style.color = '#CF0D0D';
            document.getElementById("3").style.background =  '-moz-radial-gradient(center, ellipse cover,#F1F3F4 0%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("3").style.color = '#CF0D0D';

            document.getElementById("<%= txtCateName.ClientID %>").value = "";
            document.getElementById("<%= cmbCategory.ClientID %>").value = "";
            document.getElementById("<%= txtModuleName.ClientID %>").value = "";            
            
            FillCategoryCombo();
            document.getElementById("<%= cmbCategory.ClientID %>").focus();            
        }
        function ProblemClick()  {
            document.getElementById("CategoryName").style.display = 'none';
            document.getElementById("Category").style.display = '';
            document.getElementById("ModuleName").style.display = 'none';
            document.getElementById("Module").style.display = '';
            document.getElementById("Problem").style.display = '';
            document.getElementById("Root").style.display = '';
            document.getElementById("AppMode").style.display = '';
            document.getElementById("AppMode1").style.display = '';
            document.getElementById("Description").style.display = '';
            document.getElementById("Annexture").style.display = '';
            document.getElementById("ViewAtur").style.display = 'none';
            document.getElementById("DirectTeam").style.display = '';
            //$('.Tat').show();
            $('.Severity').show();

            document.getElementById("<%= hid_tab.ClientID %>").value =3;
            document.getElementById("<%= hid_Id.ClientID %>").value = 0;
            document.getElementById("<%= hid_Edit.ClientID %>").value = 0;           
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';

            document.getElementById("1").style.background =  '-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("1").style.color = '#CF0D0D';
            document.getElementById("2").style.background = '-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #F08080 90%, #F1F3F4 100%)';
            document.getElementById("2").style.color = '#CF0D0D';

            document.getElementById("3").style.background =  '-moz-radial-gradient(center, ellipse cover,  #EEB8A6 0%, #801424 90%, #EEB8A6 100%)';

            document.getElementById("3").style.color = '#CF0D0D';

            document.getElementById("<%= cmbCategory.ClientID %>").value = "";
            document.getElementById("<%= cmbModule.ClientID %>").value = "";
            document.getElementById("<%= txtProblem.ClientID %>").value = "";
            document.getElementById("<%= txtDescription.ClientID %>").value = "";
            document.getElementById("<%= chkCritical.ClientID %>").checked = false;
            ClearCombo("<%= cmbModule.ClientID %>");
            
            document.getElementById("<%= cmbApp.ClientID %>").value = "-1";
            document.getElementById("<%= chkAm.ClientID %>").checked = false;
            document.getElementById("<%= chkRm.ClientID %>").checked = false;
            document.getElementById("<%= chkGm.ClientID %>").checked = false;
            document.getElementById("<%= chkTm.ClientID %>").checked = false;
            document.getElementById("<%= chkNps.ClientID %>").checked = false;
            document.getElementById("<%= chkBranchDesk.ClientID %>").checked = false; 
            FillCategoryCombo();    
//            FillDirectTeamCombo();
           
            document.getElementById("<%= cmbCategory.ClientID %>").focus();
        }
        function Saveonclick()         
        {            
            document.getElementById("<%= hid_Save.ClientID %>").value="";
            document.getElementById("<%= hid_Cate.ClientID %>").value="";
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1){
                if (document.getElementById("<%= txtCateName.ClientID %>").value == "")
                {
                    alert("Enter Category");
                    document.getElementById("<%= txtCateName.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= hid_Edit.ClientID %>").value==0)
                    var ToData = "1Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= txtCateName.ClientID %>").value + "Ø0" ;
                else
                    var ToData = "1Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= txtCateName.ClientID %>").value + "Ø" + document.getElementById("<%= hid_Id.ClientID %>").value;               
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2){
                if (document.getElementById("<%= cmbCategory.ClientID %>").value =="-1") 
                {
                    alert("Select Category");
                    document.getElementById("<%= cmbCategory.ClientID %>").focus();
                    return false;
                    }
                    if (document.getElementById("<%= txtModuleName.ClientID %>").value == "") 
                    {
                    alert("Enter Module");
                    document.getElementById("<%= txtModuleName.ClientID %>").focus();
                    return false;
                    }
                    if (document.getElementById("<%= hid_Edit.ClientID %>").value=="1") {
                        var ToData = "1Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" +document.getElementById("<%= cmbCategory.ClientID %>").value+ "Ø" +document.getElementById("<%= txtModuleName.ClientID %>").value + "Ø" +document.getElementById("<%= hid_Id.ClientID %>").value;                                                  
                }
                else {                    
                    var ToData = "1Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" +document.getElementById("<%= cmbCategory.ClientID %>").value+ "Ø" +document.getElementById("<%= txtModuleName.ClientID %>").value + "Ø0";                    
                }
                document.getElementById("<%= hid_Cate.ClientID %>").value=document.getElementById("<%= cmbCategory.ClientID %>").value;                    
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3)
            {
                if (document.getElementById("<%= cmbCategory.ClientID %>").value =="-1") 
                {
                    alert("Select Category");
                    document.getElementById("<%= cmbCategory.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= cmbModule.ClientID %>").value =="-1") 
                {
                    alert("Select Sub Category");
                    document.getElementById("<%= cmbModule.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= txtProblem.ClientID %>").value == "") 
                {
                    alert("Enter Findings");
                    document.getElementById("<%= txtProblem.ClientID %>").focus();
                    return false;                        
                }
                if (document.getElementById("<%= cmbApp.ClientID %>").value=="-1"){
                    alert("Select Approval mode");
                    document.getElementById("<%= cmbApp.ClientID %>").focus();
                    return false;
                }
                else if (document.getElementById("<%= cmbApp.ClientID %>").value>0)                 
                {                       
                    if (document.getElementById("<%= chkAm.ClientID %>").checked == false && document.getElementById("<%= chkRm.ClientID %>").checked == false && document.getElementById("<%= chkGm.ClientID %>").checked == false)
                    {alert("Check atleast one approval ");
                    document.getElementById("<%= chkAm.ClientID %>").focus();
                    return false;}
                        
                }
                if (document.getElementById("<%= txtDescription.ClientID %>").value == ""){
                    alert("Enter Description");
                    document.getElementById("<%= txtDescription.ClientID %>").focus();
                    return false;
                }
                var Description = document.getElementById("<%= txtDescription.ClientID %>").value;
                var Critical = 0;
                if (document.getElementById("<%= chkCritical.ClientID %>").checked == true)
                    Critical = 1;
                           
                var Approval="";
                var orderno=0;

                var DirectTeam = 0;
                if (document.getElementById("<%= cmbDirectTeam.ClientID %>").value!="-1"){
                    DirectTeam = document.getElementById("<%= cmbDirectTeam.ClientID %>").value ;
                }
                var rootCause = document.getElementById("<%= cmbProbType.ClientID %>").value ;
                var severity = document.getElementById("<%= cmbSeverity.ClientID %>").value ;
                var respTatType = -1;
                var respTat = 0;
                var resolTatType = -1;
                var resolTat = 0;
                if (severity == -1) {
                    alert("Select Severity");
                    document.getElementById("<%= cmbSeverity.ClientID %>").focus();
                    return false;
                } else if (severity == 1000) {
                    respTatType = document.getElementById("<%= cmbRespTatType.ClientID %>").value;
                    if (respTatType == -1) {
                        alert("Select response TAT type");
                        document.getElementById("<%= cmbRespTatType.ClientID %>").focus();
                        return false;
                    } else {
                        if (respTatType != 0) {
                            respTat = document.getElementById("<%= txtRespTat.ClientID %>").value;
                            if (respTat == 0 || respTat == '') {
                                alert("Enter reponse TAT");
                                document.getElementById("<%= txtRespTat.ClientID %>").focus();
                                return false;
                            }
                        }
                    }
                    resolTatType = document.getElementById("<%= cmbResolTatType.ClientID %>").value;
                    if (resolTatType == -1) {
                        alert("Select resolution TAT type");
                        document.getElementById("<%= cmbResolTatType.ClientID %>").focus();
                        return false;
                    } else {
                        if (resolTatType != 0) {
                            resolTat = document.getElementById("<%= txtResolTat.ClientID %>").value;
                            if (resolTat == 0 || resolTat == '') {
                                alert("Enter resolution TAT");
                                document.getElementById("<%= txtResolTat.ClientID %>").focus();
                                return false;
                            }
                        }
                    }
                } else {
                    respTatType = document.getElementById("<%= cmbRespTatType.ClientID %>").value;
                    respTat = document.getElementById("<%= txtRespTat.ClientID %>").value;
                    resolTatType = document.getElementById("<%= cmbResolTatType.ClientID %>").value;
                    resolTat = document.getElementById("<%= txtResolTat.ClientID %>").value;
                }

                if (document.getElementById("<%= cmbApp.ClientID %>").value >0)                    
                {
                    var Appmode=document.getElementById("<%= cmbApp.ClientID %>").value;
                    if (document.getElementById("<%= cmbApp.ClientID %>").value == 1)
                    {
                        if (document.getElementById("<%= chkAm.ClientID %>").checked == true)
                        Approval= "¥"+6 +"µ" +1;
                        if (document.getElementById("<%= chkRm.ClientID %>").checked == true)
                        Approval=Approval +"¥"  + 7 +"µ"+1;
                        if (document.getElementById("<%= chkGm.ClientID %>").checked == true)
                        Approval=Approval + "¥" + 9 + "µ" + 1;
                        if (document.getElementById("<%= chkTm.ClientID %>").checked == true)
                        Approval=Approval + "¥" + 8 + "µ" + 1;
                        if (document.getElementById("<%= chkNps.ClientID %>").checked == true)
                        Approval=Approval + "¥" + 10 + "µ" + 1;
                        if (document.getElementById("<%= chkBranchDesk.ClientID %>").checked == true)
                        Approval=Approval + "¥" + 11 + "µ" + 1;
                    }
                    else
                    {
                        if (document.getElementById("<%= chkAm.ClientID %>").checked == true){
                            orderno=orderno+1;
                            Approval= "¥"+6 +"µ"+orderno;
                        }
                        if (document.getElementById("<%= chkRm.ClientID %>").checked == true)
                        {
                            orderno=orderno+1;
                            Approval=Approval +"¥" + 7 +  "µ"+orderno;
                        }
                        if (document.getElementById("<%= chkTm.ClientID %>").checked == true)
                        {
                            orderno=orderno+1;
                            Approval=Approval + "¥" + 8 + "µ" +orderno;
                        }
                        if (document.getElementById("<%= chkGm.ClientID %>").checked == true)
                        {
                            orderno=orderno+1;
                            Approval=Approval + "¥" + 9 + "µ" +orderno;
                        }                          
                        if (document.getElementById("<%= chkNps.ClientID %>").checked == true)
                        {
                            orderno=orderno+1;
                            Approval=Approval + "¥" + 10 + "µ" +orderno;
                        }
                        if (document.getElementById("<%= chkBranchDesk.ClientID %>").checked == true)
                        {
                            orderno=orderno+1;
                            Approval=Approval + "¥" + 11 + "µ" +orderno;
                        }
                    }

                    if (document.getElementById("<%= hid_Edit.ClientID %>").value==0)       
                        var ToData = "1Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbModule.ClientID %>").value + "Ø" + document.getElementById("<%= txtProblem.ClientID %>").value + "Ø0Ø" + Appmode + "Ø" + Approval + "Ø" + Description + "Ø" + Critical + "Ø" + DirectTeam + "Ø" + rootCause + "Ø" + severity + "Ø" + respTatType + "Ø" + respTat + "Ø" + resolTatType + "Ø" + resolTat;   
                    else
                        var ToData = "1Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbModule.ClientID %>").value + "Ø" + document.getElementById("<%= txtProblem.ClientID %>").value + "Ø" + document.getElementById("<%= hid_Id.ClientID %>").value + "Ø" + Appmode + "Ø" + Approval + "Ø" + Description + "Ø" + Critical + "Ø" + DirectTeam + "Ø" + rootCause + "Ø" + severity + "Ø" + respTatType + "Ø" + respTat + "Ø" + resolTatType + "Ø" + resolTat;                         
                }
                else                    
                {
                    var Appmode=0;
                    if (document.getElementById("<%= hid_Edit.ClientID %>").value==0)       
                        var ToData = "1Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbModule.ClientID %>").value + "Ø" + document.getElementById("<%= txtProblem.ClientID %>").value + "Ø0Ø0Ø0Ø" + Description + "Ø" + Critical + "Ø" + DirectTeam + "Ø" + rootCause + "Ø" + severity + "Ø" + respTatType + "Ø" + respTat + "Ø" + resolTatType + "Ø" + resolTat;
                    else
                        var ToData = "1Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" + document.getElementById("<%= cmbModule.ClientID %>").value + "Ø" + document.getElementById("<%= txtProblem.ClientID %>").value + "Ø" + document.getElementById("<%= hid_Id.ClientID %>").value + "Ø0Ø0Ø" + Description + "Ø" + Critical + "Ø" + DirectTeam + "Ø" + rootCause + "Ø" + severity + "Ø" + respTatType + "Ø" + respTat + "Ø" + resolTatType + "Ø" + resolTat;                      
                }
                document.getElementById("<%= hid_Cate.ClientID %>").value=document.getElementById("<%= cmbCategory.ClientID %>").value;
                document.getElementById("<%= hid_SubCate.ClientID %>").value=document.getElementById("<%= cmbModule.ClientID %>").value;
            }           
            document.getElementById("<%= hid_Save.ClientID %>").value =ToData; 
        }
        function delete_row(GroupId){        
            if (confirm("Are You sure to Delete?")==1){                
                var ToData = "7Ø"+document.getElementById("<%= hid_Tab.ClientID %>").value+"Ø"+ GroupId;
                ToServer(ToData, 7);
             }
        }
        function edit_row(GroupID,GroupName){
            document.getElementById("<%= hid_Edit.ClientID %>").value = 1;
            document.getElementById("<%= hid_Id.ClientID %>").value=GroupID;
           
            if (document.getElementById("<%= hid_Tab.ClientID %>").value==1){
                document.getElementById("<%= txtCateName.ClientID %>").value=GroupName.replace(/[Ø]/g," " );
                document.ementById("<%= txtCateName.ClientID %>").focus();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value==2){
                document.getElementById("<%= txtModuleName.ClientID %>").value=GroupName.replace(/[Ø]/g," " );
                document.ementById("<%= txtModuleName.ClientID %>").focus();                
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value==3){                    
                col = GroupName.split("ÿ");
                document.getElementById("<%= txtProblem.ClientID %>").value=col[0].replace(/[Ø]/g," " );
                if (col[2] == 1)
                    document.getElementById("<%= chkCritical.ClientID %>").checked=true;
                else
                    document.getElementById("<%= chkCritical.ClientID %>").checked=false;

                document.getElementById("<%= txtDescription.ClientID %>").value=col[3].replace(/[Ø]/g," " );
                if (col[4] == 1)
                    document.getElementById("ViewAtur").style.display = ''; 
                else
                    document.getElementById("ViewAtur").style.display = 'none';
           
                if (col[5] == 0)
                    document.getElementById("<%= cmbDirectTeam.ClientID %>").value=-1;
                else
                    document.getElementById("<%= cmbDirectTeam.ClientID %>").value=col[5];
                document.getElementById("<%= cmbProbType.ClientID %>").value = col[6];
                var severity = col[7];
                document.getElementById("<%= cmbSeverity.ClientID %>").value = severity;
                var respTatType = col[8];
                document.getElementById("<%= cmbRespTatType.ClientID %>").value = respTatType;
                var respTat = col[9];
                document.getElementById("<%= txtRespTat.ClientID %>").value = respTat;
                var resolTatType = col[10];
                document.getElementById("<%= cmbResolTatType.ClientID %>").value = resolTatType;
                var resolTat = col[11];
                if (severity == 1000 || severity == -1) {
                    $('#<%= cmbRespTatType.ClientID %>').removeAttr('disabled');
                    $('#<%= txtRespTat.ClientID %>').removeAttr('disabled');
                    $('#<%= cmbResolTatType.ClientID %>').removeAttr('disabled');
                    $('#<%= txtResolTat.ClientID %>').removeAttr('disabled');
                    $(".Tat").show();
                    document.getElementById("<%= cmbRespTatType.ClientID %>").value = respTatType;
                    document.getElementById("<%= txtRespTat.ClientID %>").value = respTat;
                    document.getElementById("<%= cmbResolTatType.ClientID %>").value = resolTatType;
                    document.getElementById("<%= txtResolTat.ClientID %>").value = resolTat;
                    if (respTatType == 0) {
                        $(".RespTatDtl").hide();
                    } else {
                        $(".RespTatDtl").show();
                    }
                    if (resolTatType == 0) {
                        $(".ResolTatDtl").hide();
                    } else {
                        $(".ResolTatDtl").show();
                    }
                } else {
                    $(".Tat").show();
                    document.getElementById("<%= cmbRespTatType.ClientID %>").value = respTatType;
                    document.getElementById("<%= txtRespTat.ClientID %>").value = respTat;
                    document.getElementById("<%= cmbResolTatType.ClientID %>").value = resolTatType;
                    document.getElementById("<%= txtResolTat.ClientID %>").value = resolTat;
                    $('#<%= cmbRespTatType.ClientID %>').prop("disabled", true);
                    $('#<%= txtRespTat.ClientID %>').prop("disabled", true);
                    $('#<%= cmbResolTatType.ClientID %>').prop("disabled", true);
                    $('#<%= txtResolTat.ClientID %>').prop("disabled", true);
                }
                if (col[1]!=0)
                {  
                    document.getElementById("<%= chkAm.ClientID %>").disabled=false;
                    document.getElementById("<%= chkRm.ClientID %>").disabled=false;
                    document.getElementById("<%= chkGm.ClientID %>").disabled=false;
                    document.getElementById("<%= chkTm.ClientID %>").disabled=false;
                    document.getElementById("<%= chkNps.ClientID %>").disabled=false;
                    document.getElementById("<%= chkBranchDesk.ClientID %>").disabled=false;                  
                    var ToData = "8Ø"+document.getElementById("<%= hid_Tab.ClientID %>").value +"Ø"+ GroupID;                    
                    ToServer(ToData, 8);
                }
                else
                {
                    document.getElementById("<%= cmbApp.ClientID %>").value=col[1];  
                    document.getElementById("<%= chkAm.ClientID %>").disabled=true;
                    document.getElementById("<%= chkRm.ClientID %>").disabled=true;
                    document.getElementById("<%= chkGm.ClientID %>").disabled=true;
                    document.getElementById("<%= chkTm.ClientID %>").disabled=true;
                    document.getElementById("<%= chkNps.ClientID %>").disabled=true;
                    document.getElementById("<%= chkBranchDesk.ClientID %>").disabled=true;

                    document.getElementById("<%= chkAm.ClientID %>").checked=false;
                    document.getElementById("<%= chkRm.ClientID %>").checked=false;
                    document.getElementById("<%= chkGm.ClientID %>").checked=false;
                    document.getElementById("<%= chkTm.ClientID %>").checked=false;
                    document.getElementById("<%= chkNps.ClientID %>").checked=false;
                    document.getElementById("<%= chkBranchDesk.ClientID %>").checked=false;
                }              
            }                 
        }
        function ViewAttachment() 
        {
            var ProblemID = document.getElementById("<%= hid_Id.ClientID %>").value;
            if (ProblemID > 0)
                window.open("ShowFormat.aspx?ProblemID=" + btoa(ProblemID) + "");
            return false;
        }
        function table_fill() {

            document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
            
            var row_bg = 0;
            var tab = "";
            tab += "<br/><div class=mainhead style='width:85%; height:25px; padding-top:0px;margin:0px auto;' ><table style='width:100%;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:15%;text-align:center;' class=NormalText>#</td>";
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1){
                tab += "<td style='width:75%;text-align:left' class=NormalText>Category</td>";
                tab += "<td style='width:5%;'/></td>";
                tab += "<td style='width:5%;'/></td>";
                tab += "</tr></table></div><div class=mainhead style='width:85%; height:150px; overflow:auto; margin:0px auto;' ><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2){
                tab += "<td style='width:75%;text-align:left' class=NormalText>Sub Category</td>";
                tab += "<td style='width:5%;'/></td>";
                tab += "<td style='width:5%;'/></td>";
                tab += "</tr></table></div><div class=mainhead style='width:85%; height:150px; overflow:auto; margin:0px auto;' ><table style='width:100%; margin:0px auto;font-family:'cambria';' align='center'>";
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3){
                tab += "<td style='width:75%;text-align:left' class=NormalText>Findings</td>";
                tab += "<td style='width:5%;'/></td>";
                tab += "<td style='width:5%;'/></td>";
                tab += "</tr></table></div><div class=mainhead style='width:85%; height:120px; overflow:auto; margin:0px auto;;' ><table style='width:100%; margin:0px auto;font-family:'cambria';' align='center'>";
            }           
                
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");  
                for (n = 0; n <= row.length - 1; n++) {                    
                    col = row[n].split("µ");  
                                    
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;                   
                    tab += "<td style='width:15%;text-align:center;' >" + i + "</td>";
                    var gp=col[0];
                    if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3)
                    {   
                        col1 = gp.split("ÿ"); 
                        tab += "<td style='width:75%;text-align:left;' >" + col1[0] + "</td>";
                    }
                    else
                        tab += "<td style='width:75%;text-align:left;' >" + col[0] + "</td>";  
                        
                    

                    tab += "<td style='width:5%;text-align:center' onclick=edit_row(" + col[1] + ",'"+ col[0].replace(/\s/g, 'Ø') +"')><img  src='../Image/Edit.png' style='align:middle;cursor:pointer; height:15px; width:15px ;'title='Edit' /></td>";                 
                    tab += "<td style='width:5%;text-align:center' onclick=delete_row(" + col[1] + ") colspan='2'><img  src='../Image/cross.png' style='align:middle;cursor:pointer; height:15px; width:15px ;'title='Delete' /></td>";
                    tab += "</tr>";
                }                
            }
            tab += "</table></div>";            
            document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
            //--------------------- Clearing Data ------------------------//
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
        function btnClear_onclick() {
            if (document.getElementById("<%= hid_Tab.ClientID %>").value == 1){
                CategoryClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 2){
                ModuleClick();
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value == 3){   
                ProblemClick();
            }
        }
        function ApprovalOnChange()
        {   var ApprovalMode=document.getElementById("<%= cmbApp.ClientID %>").value;         
            if (ApprovalMode>0)
            {
                document.getElementById("<%= chkAm.ClientID %>").disabled=false;
                document.getElementById("<%= chkRm.ClientID %>").disabled=false;
                document.getElementById("<%= chkGm.ClientID %>").disabled=false;
                document.getElementById("<%= chkTm.ClientID %>").disabled=false;
                document.getElementById("<%= chkNps.ClientID %>").disabled=false;
                document.getElementById("<%= chkBranchDesk.ClientID %>").disabled=false;
            }
            else
            {
                document.getElementById("<%= chkAm.ClientID %>").disabled=true;
                document.getElementById("<%= chkRm.ClientID %>").disabled=true;
                document.getElementById("<%= chkGm.ClientID %>").disabled=true;
                document.getElementById("<%= chkTm.ClientID %>").disabled=true;
                document.getElementById("<%= chkNps.ClientID %>").disabled=true;
                document.getElementById("<%= chkBranchDesk.ClientID %>").disabled=true;
                document.getElementById("<%= chkAm.ClientID %>").checked=false;
                document.getElementById("<%= chkRm.ClientID %>").checked=false;
                document.getElementById("<%= chkGm.ClientID %>").checked=false;
                document.getElementById("<%= chkTm.ClientID %>").checked=false;
                document.getElementById("<%= chkNps.ClientID %>").checked=false;
                document.getElementById("<%= chkBranchDesk.ClientID %>").checked=false;
            }
        }
        function TatTypeOnChange(n) {
            if (n == 1) {
                var type = document.getElementById("<%= cmbRespTatType.ClientID %>").value;
                if (type == 0) {
                    $(".RespTatDtl").hide();
                } else {
                    $(".RespTatDtl").show();
                }
            } else if (n == 2) {
                var type = document.getElementById("<%= cmbResolTatType.ClientID %>").value;
                if (type == 0) {
                    $(".ResolTatDtl").hide();
                } else {
                    $(".ResolTatDtl").show();
                }
            }
        }
        function SeverityOnChange() {
            var severity = document.getElementById("<%= cmbSeverity.ClientID %>").value;
            if (severity == 1000) {
                $(".Tat").show();
                $('#<%= cmbRespTatType.ClientID %>').removeAttr('disabled');
                $('#<%= txtRespTat.ClientID %>').removeAttr('disabled');
                $('#<%= cmbResolTatType.ClientID %>').removeAttr('disabled');
                $('#<%= txtResolTat.ClientID %>').removeAttr('disabled');
                document.getElementById('<%= cmbRespTatType.ClientID %>').value = -1;   
                document.getElementById('<%= txtRespTat.ClientID %>').value = '';       
                document.getElementById('<%= cmbResolTatType.ClientID %>').value = -1;       
                document.getElementById('<%= txtResolTat.ClientID %>').value = '';   
            } else if (severity == -1) {
                document.getElementById('<%= cmbRespTatType.ClientID %>').value = -1;   
                document.getElementById('<%= txtRespTat.ClientID %>').value = '';       
                document.getElementById('<%= cmbResolTatType.ClientID %>').value = -1;       
                document.getElementById('<%= txtResolTat.ClientID %>').value = '';   
                $(".Tat").hide();
            } else {
                $(".Tat").show();
                var ToData = "9Ø" + severity;
                ToServer(ToData, 9);
            }
        }
        function FillTatDtl() {            
            var tatDtl = document.getElementById('<%= hidTatDtl.ClientID %>').value.split("Ø");
            document.getElementById('<%= cmbRespTatType.ClientID %>').value = tatDtl[0];   
            document.getElementById('<%= txtRespTat.ClientID %>').value = tatDtl[1];       
            document.getElementById('<%= cmbResolTatType.ClientID %>').value = tatDtl[2];       
            document.getElementById('<%= txtResolTat.ClientID %>').value = tatDtl[3];   
            $('#<%= cmbRespTatType.ClientID %>').prop("disabled", true);
            $('#<%= txtRespTat.ClientID %>').prop("disabled", true);
            $('#<%= cmbResolTatType.ClientID %>').prop("disabled", true);
            $('#<%= txtResolTat.ClientID %>').prop("disabled", true);
                $(".RespTatDtl").show();
                $(".ResolTatDtl").show();
        }
    </script>
</head>
</html>
 <br />
 <div  style="width:50%;margin:0px auto; background-color:#EEB8A6;">
    <div style="width:80%;padding-left:10px;margin:0px auto;">
    <div id='1' class="Button" 
             style="width:30%; float:left;background:-moz-radial-gradient(center, ellipse cover, #EEB8A6 0%, #801424 90%, #EEB8A6 100%);" 
            onclick="return CategoryClick()">Category</div>
     <div id='2' class="Button" style="border-left:thick double #FFF;width:30%;float:left;" onclick="return ModuleClick()">Sub Category</div>
      <div id='3' class="Button" style="border-left:thick double #FFF;width:30%;float:left;" onclick="return ProblemClick()">Findings</div>
      
      <br /> <br />
       </div>
    <div style="width:93%;background-color:white;margin:0px auto; height:530px; border-radius:25px;">
    <br /><br />
            <table style="width:90%;height:90px;margin:0px auto;">
               
                <tr id="CategoryName" >
                    <td style="width:12%;"></td><td style="width:15%; align:left;">Category</td> 
                    <td colspan="2"><asp:TextBox ID="txtCateName" runat="server" class="NormalText" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="3"  MaxLength="100" />
                    </td>
                </tr> 
                <tr id="Category" style="display:none;"> 
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria"></Font-Names>Category Name</td> 
                    <td colspan="2"><asp:DropDownList ID="cmbCategory" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="61%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList></td>
                    </tr>
                    
                <tr id="ModuleName" style="display:none;">
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">Sub Category</td> 
                    <td colspan="2"><asp:TextBox ID="txtModuleName" class="NormalText" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="3"  MaxLength="500" />
                        </td>
                    </tr>

               <tr id="Module" style="display:none;"> 
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">Sub Category</td> 
                    <td colspan="2"><asp:DropDownList ID="cmbModule" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="61%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList></td>
                    </tr>


                <tr id="Problem" style="display:none;" >
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">Findings</td> 
                    <td colspan="2">
                        <asp:TextBox ID="txtProblem" runat="server" class="NormalText" style=" font-family:Cambria;font-size:10pt;" Width="60.5%"  Rows="3"  MaxLength="500" />&nbsp;&nbsp;
                        <asp:CheckBox ID="chkCritical" runat="server" class="NormalText" style=" font-family:Cambria;font-size:10pt;" Text="Is Critical   " 
                            TextAlign="Left" />
                    </td>
                </tr>
                 <tr id="Root" style="display:none;"> 
                    <td style="width:12%;"></td>
                     <td style="width:15%; align:left;">
                         <Font-Names="Cambria">
                        Problem Type
                    </td> 
                    <td colspan="2">
                        <asp:DropDownList ID="cmbProbType" class="NormalText" runat="server" Font-Names="Cambria" Width="61%" ForeColor="Black">
                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>

                <tr id="AppMode" style="display:none;"> 
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">Approval Mode</td> 
                    <td colspan="2"><asp:DropDownList ID="cmbApp" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="27%" ForeColor="Black">
                             <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                        <asp:ListItem Value="0">No Approval</asp:ListItem>
                        <asp:ListItem Value="1">Single</asp:ListItem><asp:ListItem Value="2"> Queued</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr> 
                <tr id="AppMode1" style="display: none;">
                    <td style="width: 12%;">
                        &nbsp;
                    </td>
                    <td style="width: 15%; align: left;">
                        Approval Order
                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="chkAm" runat="server" Font-Names="Times New Roman" Font-Size="10pt"
                            Text="AM" Enabled="False" />&nbsp;&nbsp;<asp:CheckBox ID="chkRm" runat="server" Font-Names="Times New Roman"
                                Font-Size="10pt" Text="RM" Enabled="False" />&nbsp;&nbsp;<asp:CheckBox ID="chkTm"
                                    runat="server" Font-Names="Times New Roman" Font-Size="10pt" Text="TM" Enabled="False" />&nbsp;&nbsp;<asp:CheckBox
                                        ID="chkGm" runat="server" Font-Names="Times New Roman" Font-Size="10pt" Text="GM"
                                        Enabled="False" />&nbsp;&nbsp;<asp:CheckBox ID="chkNps" runat="server" Font-Names="Times New Roman"
                                            Font-Size="10pt" Text="NPS" Enabled="False" />&nbsp;&nbsp;<asp:CheckBox ID="chkBranchDesk"
                                                runat="server" Font-Names="Times New Roman" Font-Size="10pt" Text="BRANCH DESK"
                                                Enabled="False" />
                    </td>
                </tr>
                <tr id="DirectTeam" style="display: none;"> 
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">Direct To Team</td> 
                    <td colspan="2"><asp:DropDownList ID="cmbDirectTeam" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="50%" ForeColor="Black"></asp:DropDownList>
                    </td>
                </tr> 
                <tr class="Severity" style="display: none;"> 
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">Severity</td> 
                    <td colspan="2">
                        <asp:DropDownList ID="cmbSeverity" class="NormalText" runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                            <asp:ListItem Value ="-1">-----Select-----</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>   
                <tr class="Tat" style="display: none;"> 
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">
                        Response TAT In
                    </td> 
                    <td colspan="2">
                        <asp:DropDownList ID="cmbRespTatType" class="NormalText" runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black" onchange="TatTypeOnChange(1)">
                            <asp:ListItem Value ="-1">-----Select-----</asp:ListItem>
                            <asp:ListItem Value ="0">No TAT</asp:ListItem>
                            <asp:ListItem Value ="1">In Hours</asp:ListItem>
                            <asp:ListItem Value ="2">In Days</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>                 
                <tr class="Tat RespTatDtl" style="display: none;"> 
                    <td style="width:12%;"></td><td style="width:15%; align:left;">Response TAT</td> 
                    <td colspan="2">
                        <asp:TextBox ID="txtRespTat" runat="server" class="NormalText" style=" font-family:Cambria;font-size:10pt;" onkeypress="NumericCheck(event)" Width="61%" MaxLength="3" />
                    </td>
                </tr>       
                <tr class="Tat" style="display: none;"> 
                    <td style="width:12%;"></td><td style="width:15%; align:left;"><Font-Names="Cambria">
                        Resolution TAT In
                    </td> 
                    <td colspan="2">
                        <asp:DropDownList ID="cmbResolTatType" class="NormalText" runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black" onchange="TatTypeOnChange(2)">
                            <asp:ListItem Value ="-1">-----Select-----</asp:ListItem>
                            <asp:ListItem Value ="0">No TAT</asp:ListItem>
                            <asp:ListItem Value ="1">In Hours</asp:ListItem>
                            <asp:ListItem Value ="2">In Days</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>          
                <tr class="Tat ResolTatDtl" style="display: none;"> 
                    <td style="width:12%;"></td><td style="width:15%; align:left;">Resolution TAT</td> 
                    <td colspan="2">
                        <asp:TextBox ID="txtResolTat" runat="server" class="NormalText" style=" font-family:Cambria;font-size:10pt;" onkeypress="NumericCheck(event)" Width="61%" MaxLength="3" />
                    </td>
                </tr> 
                <tr id="Description" style="display: none;">
                    <td style="width: 12%;">
                        &nbsp;
                    </td>
                    <td style="width: 15%; align: left;">
                        Description
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtDescription" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Width="80%" Rows="3" MaxLength="500" />
                    </td>
                </tr>
                <tr id="Annexture" style="display: none;">
                    <td style="width: 12%;">
                        &nbsp;
                    </td>
                    <td style="width: 15%; align: left;">
                        Annexture
                    </td>
                    <td style="width: 51%;">
                        <input id="fupAnnexture" runat="server" cssclass="fileUpload" type="file" />
                    </td>
                    <td id="ViewAtur" style="width: 12%;">
                        <asp:ImageButton ID="btnView" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Annexture" />
                    </td>
                </tr>
                <tr id="List">
                    <td style="text-align: center;" colspan="4">
                        <asp:Panel ID="pnlDtls" Style="width: 100%; text-align: right; float: right;" runat="server">
                        </asp:Panel>
                        <asp:HiddenField ID="hid_Edit" runat="server" />
                        <asp:HiddenField ID="hid_Id" runat="server" />
                        <asp:HiddenField ID="hid_tab" runat="server" />
                        <asp:HiddenField ID="hid_dtls" runat="server" />
                        <asp:HiddenField ID="hid_Save" runat="server" />
                        <asp:HiddenField ID="hid_Cate" runat="server" />
                        <asp:HiddenField ID="hid_SubCate" runat="server" />
                        <asp:HiddenField ID="hidTatDtl" runat="server" />
                    </td>
                </tr>
            </table> 
            
    </div>
    <div style="text-align:center; height: 63px;"><br />
    <asp:Button 
                ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" style="cursor:pointer;"
                 Width="8%" />
    
 &nbsp;
                        &nbsp;

                        <input id="Button1" style="font-family: cambria; cursor: pointer; width:8%;" 
                            type="button" value="CLEAR" onclick="return btnClear_onclick()" onclick="return btnClear_onclick()"/> &nbsp;
                         &nbsp;
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" 
                            type="button" value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" /></div>
    </div>   
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

