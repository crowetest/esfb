﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class HelpDesk_ProblemRootCause
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
#Region "Page Load & Dispose"
    Protected Sub Compliance_NewCompliance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 264) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Findings Root Cause"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "ChangeOnChange();", True)
            DT = DB.ExecuteDataSet("select '-1',' -----Select-----' union all select A.PROBLEM_NO+'^'+A.PROBLEM+'^'+A.IMPACT+'^'+B.CATEGORY+'^'+C.PRIORITY_name+'^'+convert(varchar,a.STATUS_ID)+'^'+ISNULL(ROOT_CAUSE,'') +'^'+ISNULL(WORK_AROUND,'') +'^'+ISNULL(SOLUTION,'') ,PROBLEM_NO+' | '+PROBLEM from HD_PROBLEM_MASTER A,HD_PROBLEM_CATEGORY B,HD_CHANGE_PRIORITY C WHERE A.STATUS_ID in (1,2) AND A.CATEGORY_ID = B.CATEGORY_ID AND A.PRIORITY_ID = C.PRIORITY_ID").Tables(0)
            GF.ComboFill(cmbProblem, DT, 0, 1)
            'DT = DB.ExecuteDataSet("select STATUS_ID,STATUS from HD_PROBLEM_STATUS WHERE STATUS_ID IN (1,2,3)").Tables(0)
            'GF.ComboFill(cmbStatus, DT, 0, 1)
            Me.cmbProblem.Attributes.Add("onchange", "ChangeOnChange()")
            Me.hlDetails.Attributes.Add("onclick", "DetailsOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Compliance_NewCompliance_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Select Case CInt(Data(0))
            Case 1 ' Confirmation
                ' "9ʘ"+ ProblemNo + "ʘ" + RootCause + "ʘ" + WorkAround + "ʘ" + Solution + "ʘ" + Status;
                Dim ProblemNo As String = CStr(Data(1))
                Dim RootCause As String = CStr(Data(2))
                Dim WorkAround As String = CStr(Data(3))
                Dim Solution As String = CStr(Data(4))
                Dim Status As Integer = CInt(Data(5))
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim ErrorFlag As Integer = 0
                Dim Message As String = Nothing
                Try
                    Dim Params(7) As SqlParameter
                    Params(0) = New SqlParameter("@ProblemNo", SqlDbType.VarChar, 20)
                    Params(0).Value = ProblemNo
                    Params(1) = New SqlParameter("@RootCause", SqlDbType.VarChar, 4000)
                    Params(1).Value = RootCause
                    Params(2) = New SqlParameter("@WorkAround", SqlDbType.VarChar, 4000)
                    Params(2).Value = WorkAround
                    Params(3) = New SqlParameter("@Solution", SqlDbType.VarChar, 4000)
                    Params(3).Value = Solution
                    Params(4) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(4).Value = Status
                    Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(5).Value = UserID
                    Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 4000)
                    Params(7).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_HD_PROBLEM_ROOTCAUSE", Params)
                    ErrorFlag = CInt(Params(6).Value)
                    Message = CStr(Params(7).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
            Case 2
                DT = DB.ExecuteDataSet("select STATUS_ID,STATUS from HD_PROBLEM_STATUS WHERE STATUS_ID IN (1,2,3) and status_id>" & Data(1) & "").Tables(0)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
        End Select
    End Sub
#End Region
End Class
