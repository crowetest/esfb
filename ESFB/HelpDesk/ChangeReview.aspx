﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ChangeReview.aspx.vb" Inherits="HelpDesk_ChangeReview" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server" >
<head>
<title></title>
<link href="../Style/Style.css" rel="stylesheet" type="text/css" />
<script src="../Script/Validations.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" for="window" event="onload">return ChangeOnChange()</script>
<script language="javascript" type="text/javascript">
    function ChangeOnChange()
    {
        if (document.getElementById("<%= cmbChange.ClientID %>").value == '-1') 
        {
            document.getElementById("<%= txtChange.ClientID %>").value = "";
            document.getElementById("<%= txtReason.ClientID %>").value = "";
            document.getElementById("<%= txtWhoWillDo.ClientID %>").value = "";
            document.getElementById("<%= txtAsset.ClientID %>").value = "";
            document.getElementById("<%= txtImpPlan.ClientID %>").value = ""; 
            document.getElementById("<%= txtVerifyPlan.ClientID %>").value = "";
            document.getElementById("<%= txtRollbackPlan.ClientID %>").value = "";
            document.getElementById("<%= txtStatus.ClientID %>").value = "";
            document.getElementById("<%= txtProposedTimeFrame.ClientID %>").value = "";
            document.getElementById("<%= txtActualTimeFrame.ClientID %>").value = "";
        }
        else
        {
            var ChangeInfo = document.getElementById("<%= cmbChange.ClientID %>").value.split("^");
            document.getElementById("<%= txtChange.ClientID %>").value = ChangeInfo[1];
            document.getElementById("<%= txtReason.ClientID %>").value = ChangeInfo[2];
            document.getElementById("<%= txtWhoWillDo.ClientID %>").value = ChangeInfo[3];
            document.getElementById("<%= txtAsset.ClientID %>").value = ChangeInfo[4];
            document.getElementById("<%= txtImpPlan.ClientID %>").value = ChangeInfo[5];
            document.getElementById("<%= txtVerifyPlan.ClientID %>").value = ChangeInfo[6];
            document.getElementById("<%= txtRollbackPlan.ClientID %>").value = ChangeInfo[7];
            document.getElementById("<%= txtStatus.ClientID %>").value = ChangeInfo[8];
            document.getElementById("<%= txtProposedTimeFrame.ClientID %>").value = ChangeInfo[9];
            document.getElementById("<%= txtActualTimeFrame.ClientID %>").value = ChangeInfo[10];
        }
    }

    function DetailsOnClick()
        {
            if(document.getElementById("<%= cmbChange.ClientID %>").value == "-1")
            {
                alert("Select Ticket");
                document.getElementById("<%= cmbChange.ClientID %>").focus();
                return false;
            }
            var ChangeInfo = document.getElementById("<%= cmbChange.ClientID %>").value.split("^");
            var ChangeNo   = ChangeInfo[0]; 
            window.open("Reports/ChangeTicketInfo.aspx?ChangeNo=" + btoa(ChangeNo) + "");
        }

    function btnSave_onclick() 
    {
        var ChangeInfo     = document.getElementById("<%= cmbChange.ClientID %>").value.split("^");
        var ChangeNo       = ChangeInfo[0];
        var ReviewComments = document.getElementById("<%= txtReviewComments.ClientID %>").value;
        var Data       = "9ʘ"+ ChangeNo + "ʘ" + ReviewComments;
        ToServer(Data,9)
    }
    function FromServer(Arg, Context) 
    {
        switch(Context)
        {
            case 9: // Confirmation
            {
                var Data = Arg.split("ʘ");
                alert(Data[1]);
                if ( Data[0] == 0 )  
                {
                    window.open("../home.aspx","_self");
                }
                break;
            }
        }
    }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
</script>
</head>
    <br />
           
    <table align="center" style="width: 90%; margin: 0px auto; ">
        <tr>
            <td style="width: 7%; text-align:right;"></td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 15%; text-align:left;">Select Change Ticket</td>
            
            <td style="width: 68%; text-align:left;cursor:pointer;">
                    <asp:DropDownList ID="cmbChange" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="70%" ForeColor="Black">
                        </asp:DropDownList>&nbsp;&nbsp;
                <asp:HyperLink ID="hlDetails" runat="server" ForeColor="#0000CC" 
                        style="text-decoration: underline" ToolTip="View Details">View Details</asp:HyperLink>
                </td>
                        <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 7%; text-align:right;"></td>
             <td style="width: 3%; "></td>
            <td style="width: 15%; text-align:left; ">Change Description</td>
           
            <td style="width: 80%; text-align:left; ">
                <asp:TextBox ID="txtChange" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;" 
                    Width="90%" MaxLength="1000" Rows="2" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)'  
                    ReadOnly="True"/></td>
                    <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr>
        <td style="width: 7%; text-align:right;"></td>
        <td style="width: 3%">&nbsp;</td>
            <td style="width: 15%; text-align:left;">Reason For Change</td>
            
            <td style="width: 68%; text-align:left;">
                <asp:TextBox ID="txtReason" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;" 
                    Width="90%" MaxLength="1000" Rows="2" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' 
                    ReadOnly="True"/></td>
                    <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr>
        <td style="width: 7%; text-align:right;"></td>
        <td style="width: 3%">&nbsp;</td>
            <td style="width: 15%; text-align:left;">&nbsp;Implemented By</td>
            
            <td style="width: 68%; text-align:left;">
                <asp:TextBox ID="txtWhoWillDo" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="90%" MaxLength="1000" Rows="1" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' 
                    ReadOnly="True"/></td>
                    <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr>
        <td style="width: 7%; text-align:right;"></td>
        <td style="width: 3%">&nbsp;</td>
            <td style="width: 15%; text-align:left;">On Which Asset(s)</td>
            
            <td style="width: 68%; text-align:left;">
                <asp:TextBox ID="txtAsset" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="90%" MaxLength="1000" Rows="1" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' 
                    ReadOnly="True"/></td>
                    <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr>
        <td style="width: 7%; text-align:right;"></td>
        <td style="width: 3%">&nbsp;</td>
            <td style="width: 15%; text-align:left;">Implementation Plan</td>
            
            <td style="width: 68%; text-align:left;">
                <asp:TextBox ID="txtImpPlan" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="90%" MaxLength="1000" Rows="2" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' 
                    ReadOnly="True"/></td>
                    <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr>
        <td style="width: 7%; text-align:right;"></td>
        <td style="width: 3%">&nbsp;</td>
            <td style="width: 15%; text-align:left;">Verification Plan</td>
            
            <td style="width: 68%; text-align:left;">
                <asp:TextBox ID="txtVerifyPlan" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="90%" MaxLength="1000" Rows="2" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' 
                    ReadOnly="True"/></td>
                    <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr>
        <td style="width: 7%; text-align:right;"></td>
        <td style="width: 3%">&nbsp;</td>
            <td style="width: 15%; text-align:left;">Rollback Plan</td>
            
            <td style="width: 68%; text-align:left;">
                <asp:TextBox ID="txtRollbackPlan" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="90%" MaxLength="1000" Rows="2" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' 
                    ReadOnly="True"/></td>
                    <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr>
        <td style="width: 7%; text-align:right;"></td>
        <td style="width: 3%">&nbsp;</td>
            <td style="width: 15%; text-align:left;">Proposed Time Frame</td>
            
            <td style="width: 68%; text-align:left;">
                <asp:TextBox ID="txtProposedTimeFrame" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="90%" MaxLength="1000" Rows="1" ReadOnly="True"/></td>
                    <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr>
        <td style="width: 7%; text-align:right;"></td>
        <td style="width: 3%">&nbsp;</td>
            <td style="width: 15%; text-align:left;">Implemented Time Frame</td>
            
            <td style="width: 68%; text-align:left;">
                <asp:TextBox ID="txtActualTimeFrame" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="90%" MaxLength="100" Rows="2" ReadOnly="True"/></td>
                    <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr>
        <td style="width: 7%; text-align:right;"></td>
        <td style="width: 3%">&nbsp;</td>
            <td style="width: 15%; text-align:left;">Status of Implementation</td>
            
            <td style="width: 68%; text-align:left;">
                <asp:TextBox ID="txtStatus" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="90%" MaxLength="100" Rows="2" ReadOnly="True"/></td>
                    <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr>
        <td style="width: 7%; text-align:right;"></td>
        <td style="width: 3%">&nbsp;</td>
            <td style="width: 15%; text-align:left;">Review Comments</td>
            
            <td style="width: 68%; text-align:left;">
                <asp:TextBox ID="txtReviewComments" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="90%" MaxLength="1000" Rows="2" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
                    <td style="width: 7%">&nbsp;</td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td style="text-align:center;"colspan="5" class="style2">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="SAVE" onclick="return btnSave_onclick()" />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
            </td>            
         </tr>
    </table>
    <br />
</asp:Content>


