﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ApproveTicket_Exception.aspx.vb" Inherits="ApproveTicket_Exception" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript" >return window_onload()</script>
    <script language="javascript" type="text/javascript" >
       
   
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }    
          
        function FromServer(arg, context) {
         switch (context) {
             case 1:
                 var Data = arg.split("Ø");
                 alert(Data[1]);             
                     if (Data[0] == 0) { window.open("ApproveTicket_Exception.aspx", "_self"); }
                   break; 
                 }
                }
function window_onload() { 
    table_Fill();
}
 function table_Fill()
        {
             var tab="";
             var row_bg = 0;

               tab += "<div style='width:100%;  height:auto; margin: 0px auto; overflow-y:scroll' class=mainhead>";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr height=30px;>";
                tab += "<td style='width:5%;text-align:center' >#</td>";                
                tab += "<td style='width:10%;text-align:left' >Ticket No</td>";
                tab += "<td style='width:10%;text-align:left' >Group</td>";
                tab += "<td style='width:10%;text-align:left' >Sub Group</td>";
                tab += "<td style='width:25%;text-align:left' >Findings</td>";
                tab += "<td style='width:15%;text-align:left' >Prev. Remarks</td>";           
                tab += "<td style='width:15%;text-align:left'>Action</td>";          
                tab += "<td style='width:15%;text-align:left' >Remarks</td>";     
                tab += "<td style='width:5%;text-align:center'>Attachment</td>";             
                tab += "</tr>";
                tab += "</table></div>";
             if(document.getElementById("<%= hid_Items.ClientID %>").value=="")
             {
                 document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none'; 
                 return;
             }
             
         
             document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';  
             tab += "<div id='ScrollDiv' style='width:100%; height:315px;overflow: scroll;margin: 0px auto;' class=mainhead >";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='left'>";     
            
               var row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");                               
                for (n = 0; n <= row.length - 1; n++) {
                   var col = row[n].split("µ");
                   if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second'; style='text-align:center;'>";
                    }
                    var i = n + 1;                 
                    tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                    tab += "<td style='width:10%;text-align:left'><a href='Reports/viewTicketDetails.aspx?RequestID="+ btoa(col[0]) +"' style='text-align:right;' target='_blank' >" + col[1] + "</a></td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:15%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:15%;text-align:left'>" + col[2] + "</td>";                   
                    var select1 = "<select id='cmbStatus" + i + "' class='NormalText' name='cmbStatus" + i + "' style='width:100%'>";                    
                    select1 += "<option value=-1 >---Select---</option>";                      
                    select1 += "<option value=1 >Approve</option>"; 
                    select1 += "<option value=2 >Reject</option>";               
                    tab += "<td style='width:15%;text-align:left'>" + select1 + "</td>";                   
                    var txtBox = "<textarea id='txtReason" + i + "' name='txtReason" + i + "' style='width:99%; float:left;'  maxlength='300' onkeypress='return TextAreaCheck(event)' ></textarea>";
                    tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>";
                    if(col[6]==0)
                        tab += "<td style='width:5%;text-align:center'></td>";
                    else
                    {
                    if (col[6]==1)
                        tab += "<td style='width:5%;text-align:center'><a href='Reports/ShowAttachment.aspx?RequestID="+ btoa(col[0])  +"' style='text-align:right;' target='_blank'> Show </a></td>";
                    else                   
                        tab += "<td style='width:5%;text-align:center'><a href='Reports/ShowMultipleAttachments.aspx?RequestID="+ btoa(col[0])  +"' style='text-align:right;' target='_blank'> Show </a></td>";
                    }
                    
                    tab += "</tr>";
                
            }
            tab += "</table></div></div></div>";            
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;  
        }
       
      
function btnSave_onclick() {
       document.getElementById("<%= hid_Value.ClientID %>").value="";
       var row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");                                     
                for (n = 0; n <= row.length - 1; n++) {
                       var col = row[n].split("µ");
                       var i = n + 1;  
                      if( document.getElementById("cmbStatus"+i).value!="-1")
                       {
                         if(document.getElementById("txtReason"+i).value=="")
                         {alert("Enter Remarks");document.getElementById("txtReason"+i).focus();return false;}
                         document.getElementById("<%= hid_Value.ClientID %>").value+="¥"+col[0]+"µ"+ document.getElementById("cmbStatus"+i).value +"µ"+document.getElementById("txtReason"+i).value ;
                       }
                   }    
               if(document.getElementById("<%= hid_Value.ClientID %>").value=="")
               {
                  alert("Select atleast one Request"); return false;
               }
             var ToData = "1Ø" + document.getElementById("<%= hid_Value.ClientID %>").value;                  
             ToServer(ToData, 1);
}

    </script>
  
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
</script>
</head>
</html>
<br /><br />

 <table class="style1" style="width:95%;margin: 0px auto;" >
           
        <tr id="Remarks"> <td style="width:25%;">
       <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
       </td>
        
       </tr>
       <tr>
            <td style="text-align:center;"><br />
                 <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="SAVE" onclick="return btnSave_onclick()" />&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />&nbsp;<asp:HiddenField 
                    ID="hid_Items" runat="server" />
                <asp:HiddenField 
                    ID="hid_Value" runat="server" />
                <asp:HiddenField 
                    ID="hid_EmpDetails" runat="server" />
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

