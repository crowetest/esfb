﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BranchRequestClose
    Inherits System.Web.UI.Page
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim HD As New HelpDesk
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 105) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Request Closing"
            Me.cmbRequest.Attributes.Add("onchange", "return RequestOnChange()")
            Me.btnSave.Attributes.Add("onclick", "return UpdateOnClick()")
            Dim strval As String
            DT = DB.ExecuteDataSet("select post_id,branch_id,department_id from emp_master  where emp_code=" & CInt(Session("UserID")) & "").Tables(0)
            If DT.Rows.Count <> 0 Then
                Dim IntPost As Integer = 0
                Dim IntDep As Integer = 0
                IntPost = CInt(DT.Rows(0)(0))
                hid_Post.Value = CStr(IntPost)
                IntDep = CInt(DT.Rows(0)(2))
                If IntPost = 5 Then 'BM
                    strval = "and (c.branch_head=" & CInt(Session("UserID")).ToString() & "  or b.user_id=" & CInt(Session("UserID")).ToString() & ")"
                Else 'Others
                    strval = "and b.user_id=" & CInt(Session("UserID")).ToString() & ""
                End If
                DT = DB.ExecuteDataSet("select '-1' as REQUEST_ID,' ------Select------' as request Union all " & _
                                   " Select cast(b.REQUEST_ID as varchar(6))+'¥'+b.REMARKS+'¥'+cast(b.PROBLEM_ID as varchar(6))+'¥'+s.status_name +'¥'+cast(s.status_id  as varchar(6))as REQUEST_ID,s.status_name + '- ' +b.TICKET_NO " & _
                                   " +'/'+a.PROBLEM as request from HD_PROBLEM_TYPE a,HD_REQUEST_MASTER b,hd_status s,brmaster c  where a.PROBLEM_ID=b.PROBLEM_ID and b.branch_id=c.branch_id and " & _
                                   " b.status_id=s.status_id and (b.STATUS_ID=4 or b.status_id in(12,19)) " & _
                                   " " & strval & "").Tables(0)
                If DT.Rows.Count > 0 Then
                    GN.ComboFill(cmbRequest, DT, 0, 1)
                End If
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Data() As String = hid_dtls.Value.Split(CChar("Ø"))
                Dim RequestId As Integer = CInt(Data(0))
                Dim StrPreremark As String = CStr(Data(2))
                Dim StrRemark As String = CStr(Data(1))
                Dim ProblemId As Integer = CInt(Data(3))
                Dim CloseOpenFlag As Integer = CInt(Data(4))
                If CloseOpenFlag = 1 Then
                    Dim Params(3) As SqlParameter
                    Params(0) = New SqlParameter("@ReqID", SqlDbType.Int)
                    Params(0).Value = RequestId
                    Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(1).Direction = ParameterDirection.Output
                    Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 500)
                    Params(2).Direction = ParameterDirection.Output
                    Params(3) = New SqlParameter("@Remark", SqlDbType.VarChar, 1000)
                    Params(3).Value = CStr(Data(2))
                    DB.ExecuteNonQuery("SP_HD_REQ_CLOSE", Params)
                    ErrorFlag = CInt(Params(1).Value)
                    Message = CStr(Params(2).Value)
                Else
                    Dim hfc As HttpFileCollection
                    hfc = Request.Files
                    Dim NoofAttachments As Integer = 0
                    If hfc.Count > 0 Then
                        For i = 0 To hfc.Count - 1
                            Dim myFile As HttpPostedFile = hfc(i)
                            Dim nFileLen As Integer = myFile.ContentLength
                            Dim FileName As String = ""
                            If (nFileLen > 0) Then
                                NoofAttachments += 1
                            End If
                        Next
                    End If
                    Dim AttachImg As Byte() = Nothing
                    Try
                        Dim Params(8) As SqlParameter
                        Params(0) = New SqlParameter("@ReqID", SqlDbType.Int)
                        Params(0).Value = RequestId
                        Params(1) = New SqlParameter("@PreRemark", SqlDbType.VarChar, 1000)
                        Params(1).Value = StrPreremark
                        Params(2) = New SqlParameter("@Remark", SqlDbType.VarChar, 1000)
                        Params(2).Value = StrRemark
                        Params(3) = New SqlParameter("@Userid", SqlDbType.Int)
                        Params(3).Value = CInt(Session("UserID"))
                        Params(4) = New SqlParameter("@Teamid", SqlDbType.Int)
                        Params(4).Value = 4
                        Params(5) = New SqlParameter("@Problemid", SqlDbType.Int)
                        Params(5).Value = ProblemId
                        Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(6).Direction = ParameterDirection.Output
                        Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 500)
                        Params(7).Direction = ParameterDirection.Output
                        Params(8) = New SqlParameter("@NOOFATTACHMENTS", SqlDbType.Int)
                        Params(8).Value = NoofAttachments
                        DB.ExecuteNonQuery("SP_HD_REQ_REOPEN", Params)
                        ErrorFlag = CInt(Params(6).Value)
                        Message = CStr(Params(7).Value)
                    Catch ex As Exception
                        Message = ex.Message.ToString
                        ErrorFlag = 1
                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                    End Try
                    If RequestId > 0 And hfc.Count > 0 Then
                        For i = 0 To hfc.Count - 1
                            Dim myFile As HttpPostedFile = hfc(i)
                            Dim nFileLen As Integer = myFile.ContentLength
                            Dim FileName As String = ""
                            If (nFileLen > 0) Then
                                ContentType = myFile.ContentType
                                FileName = myFile.FileName
                                AttachImg = New Byte(nFileLen - 1) {}
                                myFile.InputStream.Read(AttachImg, 0, nFileLen)
                                Dim Params(3) As SqlParameter
                                Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                                Params(0).Value = RequestId
                                Params(1) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                                Params(1).Value = AttachImg
                                Params(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                                Params(2).Value = ContentType
                                Params(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                                Params(3).Value = FileName
                                DB.ExecuteNonQuery("SP_HD_REQUEST_ATTACH", Params)
                            End If

                        Next
                    End If
                End If

            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            If (ErrorFlag = 0) Then
                Response.Redirect("BranchRequestClose.aspx", False)
                'InitialiseControls()
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Sub InitialiseControls()
        DT = HD.GetRequest(CInt(Session("BranchID")), CInt(Session("UserID")))
        GN.ComboFill(cmbRequest, DT, 0, 1)
    End Sub
End Class
