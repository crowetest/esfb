﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class HelpDesk_ChangePlan
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
#Region "Page Load & Dispose"
    Protected Sub Compliance_NewCompliance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 257) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Update Change Plan"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "ChangeOnChange();", True)
            DT = DB.ExecuteDataSet("select '-1','-----------Select-----------' union all select CHANGE_NO+'^'+CHANGE+'^'+REASON,CHANGE_NO+' | '+CHANGE  from HD_CHANGE_MASTER WHERE STATUS_ID = 1").Tables(0)
            GF.ComboFill(cmbChange, DT, 0, 1)
            DT = DB.ExecuteDataSet("select -1,'-----------Select-----------' union all select TYPE_ID,TYPE_NAME from HD_CHANGE_TYPES where type_id > 0").Tables(0)
            GF.ComboFill(cmbType, DT, 0, 1)
            DT = DB.ExecuteDataSet("select -1,'-----------Select-----------' union all  select PRIORITY_ID,PRIORITY_NAME from HD_CHANGE_PRIORITY where priority_id>0").Tables(0)
            GF.ComboFill(cmbPriority, DT, 0, 1)
            Me.cmbChange.Attributes.Add("onchange", "ChangeOnChange()")
            Me.hlDetails.Attributes.Add("onclick", "DetailsOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Compliance_NewCompliance_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Select Case CInt(Data(0))
            Case 9 ' Confirmation
                ' "9ʘ"+ ChangeNo + "ʘ" + TypeID + "ʘ" + PriorityID + "ʘ" + WhoWillDo + "ʘ" + Asset + "ʘ" + PrePlan + "ʘ" + ImpPlan + "ʘ" + VerifyPlan + "ʘ" + RollPlan + "ʘ" + WhomAffect + "ʘ" + DownTime + "ʘ" + TimeFrame
                Dim ChangeNo As String = CStr(Data(1))
                Dim TypeID As Integer = CInt(Data(2))
                Dim PriorityID As Integer = CInt(Data(3))
                Dim WhoWillDo As String = CStr(Data(4))
                Dim Asset As String = CStr(Data(5))
                Dim PrePlan As String = CStr(Data(6))
                Dim ImpPlan As String = CStr(Data(7))
                Dim VerifyPlan As String = CStr(Data(8))
                Dim RollPlan As String = CStr(Data(9))
                Dim WhomAffect As String = CStr(Data(10))
                Dim StartTime As String = CStr(Data(11))
                Dim EndTime As String = CStr(Data(12))
                Dim StartDownTime As String = CStr(Data(13))
                Dim EndDownTime As String = CStr(Data(14))
                Dim Email As String = CStr(Data(15))
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim ErrorFlag As Integer = 0
                Dim Message As String = Nothing
                Try
                    Dim Params(17) As SqlParameter
                    Params(0) = New SqlParameter("@ChangeNo", SqlDbType.VarChar, 20)
                    Params(0).Value = ChangeNo
                    Params(1) = New SqlParameter("@TypeID", SqlDbType.Int)
                    Params(1).Value = TypeID
                    Params(2) = New SqlParameter("@PriorityID", SqlDbType.Int)
                    Params(2).Value = PriorityID
                    Params(3) = New SqlParameter("@WhoWillDo", SqlDbType.VarChar, 1000)
                    Params(3).Value = WhoWillDo
                    Params(4) = New SqlParameter("@Asset", SqlDbType.VarChar, 1000)
                    Params(4).Value = Asset
                    Params(5) = New SqlParameter("@PrePlan", SqlDbType.VarChar, 4000)
                    Params(5).Value = PrePlan
                    Params(6) = New SqlParameter("@ImpPlan", SqlDbType.VarChar, 4000)
                    Params(6).Value = ImpPlan
                    Params(7) = New SqlParameter("@VerifyPlan", SqlDbType.VarChar, 4000)
                    Params(7).Value = VerifyPlan
                    Params(8) = New SqlParameter("@RollPlan", SqlDbType.VarChar, 1000)
                    Params(8).Value = RollPlan
                    Params(9) = New SqlParameter("@WhomAffect", SqlDbType.VarChar, 1000)
                    Params(9).Value = WhomAffect
                    Params(10) = New SqlParameter("@StartDownTime", SqlDbType.VarChar, 100)
                    Params(10).Value = StartDownTime
                    Params(11) = New SqlParameter("@EndDownTime", SqlDbType.VarChar, 100)
                    Params(11).Value = EndDownTime
                    Params(12) = New SqlParameter("@StartTime", SqlDbType.VarChar, 100)
                    Params(12).Value = StartTime
                    Params(13) = New SqlParameter("@EndTime", SqlDbType.VarChar, 100)
                    Params(13).Value = EndTime
                    Params(14) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(14).Value = UserID
                    Params(15) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(15).Direction = ParameterDirection.Output
                    Params(16) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 4000)
                    Params(16).Direction = ParameterDirection.Output
                    Params(17) = New SqlParameter("@ImpTeamMailID", SqlDbType.VarChar, 50)
                    Params(17).Value = Email
                    DB.ExecuteNonQuery("SP_HD_CHANGE_PLAN", Params)
                    ErrorFlag = CInt(Params(15).Value)
                    Message = CStr(Params(16).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region
End Class
