﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BranchRequest
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim HD As New HelpDesk
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 103) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Ticket Creation"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0
            GN.ComboFill(cmbDep, HD.GetDepartment(), 0, 1)
            DT = DB.ExecuteDataSet("select post_id,branch_id,department_id from emp_master  where emp_code=" & CInt(Session("UserID")) & "").Tables(0)
            If DT.Rows.Count <> 0 Then
                IntPost = CInt(DT.Rows(0)(0))
                hid_Post.Value = CStr(IntPost)
                IntDep = CInt(DT.Rows(0)(2))
                'If IntPost = 5 Then 'BM
                '    DT = DB.ExecuteDataSet(" select distinct b.Branch_ID,b.Branch_Name from EMP_MASTER e , BRANCH_MASTER b where e.Branch_ID=b.Branch_ID  and b.branch_head=" & CInt(Session("UserID")) & "").Tables(0)
                '    GN.ComboFill(cmbBranch, DT, 0, 1)
                '    cmbBranch.SelectedIndex = 0
                '    cmbDep.SelectedValue = CStr(21)
                If IntPost = 6 Then 'AM
                    DT = DB.ExecuteDataSet(" select '-1' as Branch_id,' -----Select------' as Branch_Name union all select branch_id,branch_name from BrMaster where Area_Head=" & CInt(Session("UserID")) & " order by branch_name").Tables(0)
                    GN.ComboFill(cmbBranch, DT, 0, 1)
                    cmbDep.SelectedValue = CStr(21)
                ElseIf IntPost = 7 Then 'RM
                    DT = DB.ExecuteDataSet("select '-1' as Branch_id,' ------Select------' as Branch_Name union all  select branch_id,branch_name from BrMaster where   region_Head=" & CInt(Session("UserID")) & " order by branch_name").Tables(0)
                    GN.ComboFill(cmbBranch, DT, 0, 1)
                    cmbDep.SelectedValue = CStr(21)
                Else 'Others
                    DT = DB.ExecuteDataSet(" select b.Branch_ID,b.Branch_Name from EMP_MASTER e , BRANCH_MASTER b where  e.Branch_ID=b.Branch_ID  and e.emp_code=" & CInt(Session("UserID")) & "").Tables(0)
                    GN.ComboFill(cmbBranch, DT, 0, 1)
                    cmbBranch.SelectedIndex = 0
                    cmbDep.SelectedValue = CStr(IntDep)
                End If
            End If

            ' 11/12/19 temporary request friom renju

            DT = DB.ExecuteDataSet("select -1 as Group_id,' -----Select-----' as Group_Name union all select Group_id,Group_Name from HD_GROUP_MASTER where status_id=1 and group_id <>1  order by 2").Tables(0)
            GN.ComboFill(cmbCategory, DT, 0, 1)
            'GN.ComboFill(cmbCategory, HD.GetGroup(), 0, 1)

            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            Me.cmbItem.Attributes.Add("onchange", "return ItemOnChange()")
            Me.cmbProblem.Attributes.Add("onchange", "return ProblemOnChange()")
            Me.cmbAssetStatus.Attributes.Add("onchange", "return AssetStatusOnChange()")
            Me.txtAssetTAG.Attributes.Add("onchange", "return AssetTagOnChange()")
            Me.btnView.Attributes.Add("onclick", "return ViewAttachment()")
            'Me.cmbAcOrCif.Attributes.Add("onchange", "return AcOrCif_OnChange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "RequestOnchange();", True)

            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            cmbBranch.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim EmpID As Integer = CInt(Data(1))
        If CInt(Data(0)) = 2 Then
            If CInt(Data(1)) > 0 Then
                'DT = HD.GetSubGroup(CInt(Data(1)))
                ' 11/12/19 temporary request friom renju
                DT = DB.ExecuteDataSet("select -1 as Sub_Group_id,' -----Select-----' as Sub_Group_Name union all select Sub_Group_id,Sub_Group_Name from HD_SUB_GROUP where group_id=" & CInt(Data(1)).ToString() & " and status_id=1 and sub_group_id <> 51  order by 2").Tables(0)

                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()

                Next
            End If
        ElseIf CInt(Data(0)) = 3 Then
            If CInt(Data(1)) > 0 Then
                DT = HD.GetPROBLEM(CInt(Data(1)))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()

                Next

            End If
        ElseIf CInt(Data(0)) = 4 Then
            Dim DTS As New DataTable
            DTS = DB.ExecuteDataSet("select isnull(phone,''),isnull(email,'') from branch_master where branch_id=" & CInt(Data(1)) & "").Tables(0)
            If DTS.Rows.Count <> 0 Then
                If CStr(DTS.Rows(0)(0)) <> "" Then
                    txtContactNo.Text = CStr(DTS.Rows(0)(0))

                End If
                If CStr(DTS.Rows(0)(1)) <> "" Then
                    txtEmailid.Text = CStr(DTS.Rows(0)(1))

                End If
            End If
            DTS.Dispose()
        ElseIf CInt(Data(0)) = 5 Then
            Dim AssetTag As String = CStr(Data(1))
            Dim BranchID As Integer = CInt(Data(2))
            Dim Dept As Integer = CInt(Data(3))
            Dim ItemDtl() As String = Data(4).Split(CChar("~"))
            Dim ItemID As Integer = CInt(ItemDtl(0))
            DT = DB.ExecuteDataSet("Select COUNT(ASSET_ID) from FA_MASTER where STATUS_ID =1  AND LOCATION_ID=" & BranchID & " and DEPARTMENT_ID=" & Dept & " and ITEM_ID in (select item_id from hd_problem_type where problem_id =  " & ItemID & ") and ASSET_TAG='" & AssetTag & "'").Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn = CStr(DT.Rows(0)(0))
            Else
                CallBackReturn = "0"
            End If

        End If

    End Sub
#End Region

    Private Sub initializeControls()
        cmbBranch.Focus()
        txtAssetTAG.Text = ""
        txtCount.Text = ""
        cmbAssetStatus.Text = "-1"
        txtCorrectionCnt.Text = ""
        txtContactNo.Text = ""
        txtEmailid.Text = ""
        cmbAcOrCif.Text = "-1"
        CifNum.Text = ""
        AcNum.Text = ""
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim Branch As Integer = CInt(Data(1))
            Dim Dep As Integer = CInt(Data(2))
            Dim Category As Integer = CInt(Data(3))
            Dim Modules As Integer = CInt(Data(4))
            Dim Problem As Integer = CInt(Data(5))
            Dim Descr As String = CStr(Data(6))
            Dim Contactno As String = CStr(Data(7))
            Dim EmailId As String = CStr(Data(8))
            Dim AssetStatus As Integer = CInt(Data(9))
            Dim AssetTag As String = CStr(Data(10))
            Dim AssetCount As Integer = CInt(Data(11))
            'Dim CorrectionCnt As Integer = CInt(Data(12))
            Dim CorrectionCnt As Integer = 0
            Dim AcOrCif As Integer = CInt(Data(13))
            Dim AcOrCif_Num As String = CStr(Data(14))
            Dim vendorTicket As String = CStr(0)
            If Data(15) <> "" Then
                vendorTicket = CStr(Data(15))
            End If
            Dim RequestID As Integer = 0
            Dim UserID As String = Session("UserID").ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim NoofAttachments As Integer = 0
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        NoofAttachments += 1
                    End If
                Next
            End If
            If (CInt(Me.hdnAnnexture.Value) = 1) Then
                If (NoofAttachments = 0) Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('Please Attach the DownLoaded Annexture of Findings');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
            End If
            Try
                Dim Params(17) As SqlParameter
                Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
                Params(0).Value = Branch
                Params(1) = New SqlParameter("@DEPARTMENT_ID", SqlDbType.Int)
                Params(1).Value = Dep
                Params(2) = New SqlParameter("@DESCR", SqlDbType.VarChar, 1000)
                Params(2).Value = Descr
                Params(3) = New SqlParameter("@PROBLEM_ID", SqlDbType.Int)
                Params(3).Value = Problem
                Params(4) = New SqlParameter("@ASSET_STATUS", SqlDbType.Int)
                Params(4).Value = AssetStatus
                Params(5) = New SqlParameter("@ASSET_TAG", SqlDbType.VarChar, 25)
                Params(5).Value = AssetTag
                Params(6) = New SqlParameter("@ASSET_COUNT", SqlDbType.Int)
                Params(6).Value = AssetCount
                Params(7) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(7).Value = UserID
                Params(8) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(8).Direction = ParameterDirection.Output
                Params(9) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(9).Direction = ParameterDirection.Output
                Params(10) = New SqlParameter("@REQUEST_ID", SqlDbType.Int)
                Params(10).Direction = ParameterDirection.Output
                Params(11) = New SqlParameter("@CONTACTNO", SqlDbType.VarChar, 15)
                Params(11).Value = Contactno
                Params(12) = New SqlParameter("@EMAIL", SqlDbType.VarChar, 100)
                Params(12).Value = EmailId
                Params(13) = New SqlParameter("@NOOFATTACHMENTS", SqlDbType.Int)
                Params(13).Value = NoofAttachments
                Params(14) = New SqlParameter("@CorrectionCnt", SqlDbType.Int)
                Params(14).Value = 0
                Params(15) = New SqlParameter("@AcOrCif", SqlDbType.Int)
                Params(15).Value = AcOrCif
                Params(16) = New SqlParameter("@AcOrCif_Num", SqlDbType.VarChar, 15)
                Params(16).Value = AcOrCif_Num
                Params(17) = New SqlParameter("@vendorTicket", SqlDbType.VarChar, 50)
                Params(17).Value = vendorTicket
                DB.ExecuteNonQuery("SP_HD_REQUEST_FROM_BRANCH", Params)
                ErrorFlag = CInt(Params(8).Value)
                Message = CStr(Params(9).Value)
                RequestID = CInt(Params(10).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            If ErrorFlag = 0 Then
                initializeControls()
            End If
            If RequestID > 0 And hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                        Dim Params(4) As SqlParameter
                        Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                        Params(0).Value = RequestID
                        Params(1) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                        Params(1).Value = AttachImg
                        Params(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                        Params(2).Value = ContentType
                        Params(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                        Params(3).Value = FileName
                        Params(4) = New SqlParameter("@AssetStatus", SqlDbType.VarChar, 500)
                        Params(4).Value = AssetStatus
                        DB.ExecuteNonQuery("SP_HD_REQUEST_ATTACH", Params)
                    End If

                Next
            End If

            Dim AttachementNo As Integer = 0
            DT = DB.ExecuteDataSet("select COUNT(PkId) from DMS_ESFB.dbo.HD_REQUEST_ATTACH where Request_ID=" & RequestID & "").Tables(0)
            If CInt(DT.Rows(0)(0)) >= 0 Then
                AttachementNo = CInt(DT.Rows(0)(0))
                Dim Err As Integer = DB.ExecuteNonQuery("update HD_REQUEST_MASTER set NOOF_ATTACHMENTS=" & AttachementNo & " where REQUEST_ID=" & RequestID & "")
            End If

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
