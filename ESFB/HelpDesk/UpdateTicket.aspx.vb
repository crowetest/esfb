﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class UpdateTicket
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim HD As New HelpDesk
    Dim CallBackReturn As String = Nothing
    Dim ReportID, GroupID, SubGroupID As Integer
    Dim GF As New GeneralFunctions
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Ticket Update"
            Dim RequestID As Integer = -1
            RequestID = CInt(GF.Decrypt(Request.QueryString.Get("RequestID")))
            Me.hdnFilterID.Value = GF.Decrypt(Request.QueryString.Get("FilterID"))
            Me.hdnFilterText.Value = GF.Decrypt(Request.QueryString.Get("FilterText"))
            GroupID = CInt(GF.Decrypt(Request.QueryString.Get("GroupID")))
            SubGroupID = CInt(GF.Decrypt(Request.QueryString.Get("SubGroupID")))

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Me.cmd_view.Attributes.Add("onclick", "return viewattachment()")
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            If Not IsPostBack Then
                GN.ComboFill(cmbItem, HD.GetSubGroup(GroupID), 0, 1)
                GN.ComboFill(cmbProblem, HD.GetPROBLEMToAssign(SubGroupID), 0, 1)
            End If

            DT = HD.GetRequestDetails(RequestID)
            cmbRequest.Value = DT.Rows(0)(0).ToString()
            GN.ComboFill(cmbStatus, HD.GetStatus(2), 0, 1)
            GN.ComboFill(cmbHover, HD.GetTeamResolving(1), 0, 1)

            DT = DB.ExecuteDataSet("select -1,' --- Select --- ' union all select ROOT_ID,ROOT_NAME from HD_ROOT_CAUSE").Tables(0)
            If CInt(DT.Rows.Count) > 0 Then 'Root Details
                GN.ComboFill(cmbRoot, DT, 0, 1)
            End If

            lblStatus.Text = "Status"
            lblRemarks.Text = "Work Log"
            DT = HD.GetTeam(CInt(Session("UserID")))
            If DT.Rows.Count > 0 Then
                hid_TeamID.Value = CStr(DT.Rows(0)(0))
            Else
                hid_TeamID.Value = CStr(0)
            End If

            GN.ComboFill(cmbCategory, HD.GetGroup(), 0, 1)
            GN.ComboFill(cmbClass, HD.GetClassification(), 0, 1)
            GN.ComboFill(cmbVendor, HD.GetVendor(CInt(hid_TeamID.Value)), 0, 1)
            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            Me.cmbItem.Attributes.Add("onchange", "return ItemOnChange()")
            Me.cmbStatus.Attributes.Add("onchange", "return StatusOnChange()")
            Me.cmbHover.Attributes.Add("onchange", "return HandOverOnChange()")
            Me.cmbProblem.Attributes.Add("onchange", "return ProblemOnChange()")
            Me.cmbAssetStatus.Attributes.Add("onchange", "return AssetStatusOnChange()")
            Me.txtAssetTAG.Attributes.Add("onchange", "return AssetTagOnChange()")
            Me.btnView.Attributes.Add("onclick", "return ViewAttachment()")
            Me.ImgSoft.Attributes.Add("onclick", "return ViewLicense()")
            '   //REQUEST_ID0,BRANCH_ID1,Branch_Name2,CLASSIFICATION_ID3,DEPARTMENT_ID4,Department_Name5,REMARKS6,TEAM_ID7,REQUEST_DT8,PROBLEM_ID9,SUB_GROUP_ID10,Group_ID11,vendor 
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EmpID As Integer = CInt(Data(1))
        ' "1Ø" + Branch + "Ø" + Dep + "Ø" + Category + "Ø" + Module + "Ø" + Problem + "Ø" + Class+ "Ø" +Status+ "Ø" +Hover+ "Ø" +Remarks+ "Ø" +document.getElementById("<%= hid_TeamID.ClientID %>").value;
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim Branch As Integer = CInt(Data(1))
            Dim Dep As Integer = CInt(Data(2))
            Dim Category As Integer = CInt(Data(3))
            Dim Modules As Integer = CInt(Data(4))
            Dim Problem As Integer = CInt(Data(5))
            Dim Classi As Integer = CInt(Data(6))
            Dim Status As Integer = CInt(Data(7))
            Dim Hover As Integer = CInt(Data(8))
            Dim Remarks As String = CStr(Data(9))
            Dim User_Team_ID As Integer = CInt(Data(10))
            Dim REQUEST_ID As Integer = CInt(Data(11))
            Dim Vendor_ID As Integer = CInt(Data(12))
            Dim ASSET_STATUS As Integer = CInt(Data(13))
            Dim ASSET_TAG As String = CStr(Data(14))
            Dim ASSET_COUNT As Integer = CInt(Data(15))
            Dim CorrectedCnt As Integer = CInt(Data(16))
            Dim RootCause As Integer = CInt(Data(17))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(16) As SqlParameter
                Params(0) = New SqlParameter("@CLASSIFICATION_ID", SqlDbType.Int)
                Params(0).Value = Classi
                Params(1) = New SqlParameter("@REMARKS", SqlDbType.VarChar, 1000)
                Params(1).Value = Remarks
                Params(2) = New SqlParameter("@PROBLEM_ID", SqlDbType.Int)
                Params(2).Value = Problem
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = UserID
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@status", SqlDbType.Int)
                Params(6).Value = Status
                Params(7) = New SqlParameter("@TEAM_ID", SqlDbType.Int)
                Params(7).Value = Hover
                Params(8) = New SqlParameter("@USER_TEAM_ID", SqlDbType.Int)
                Params(8).Value = User_Team_ID
                Params(9) = New SqlParameter("@REQUEST_ID", SqlDbType.Int)
                Params(9).Value = REQUEST_ID
                Params(10) = New SqlParameter("@VENDORID", SqlDbType.Int)
                Params(10).Value = Vendor_ID
                Params(11) = New SqlParameter("@ASSET_STATUS", SqlDbType.Int)
                Params(11).Value = ASSET_STATUS
                Params(12) = New SqlParameter("@ASSET_TAG", SqlDbType.VarChar, 25)
                Params(12).Value = ASSET_TAG
                Params(13) = New SqlParameter("@ASSET_COUNT", SqlDbType.Int)
                Params(13).Value = ASSET_COUNT
                Params(14) = New SqlParameter("@BRANCH", SqlDbType.Int)
                Params(14).Value = Branch
                Params(15) = New SqlParameter("@CorrectedCnt", SqlDbType.Int)
                Params(15).Value = CorrectedCnt
                Params(16) = New SqlParameter("@RootCause", SqlDbType.Int)
                Params(16).Value = RootCause
                DB.ExecuteNonQuery("SP_HD_REQUEST_RESOLVING", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            If CInt(Data(1)) > 0 Then
                DT = HD.GetSubGroup(CInt(Data(1)))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            End If
        ElseIf CInt(Data(0)) = 3 Then
            If CInt(Data(1)) > 0 Then
                DT = HD.GetPROBLEMToAssign(CInt(Data(1)))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            End If
        ElseIf CInt(Data(0)) = 4 Then
            DT = HD.GetPreviousRemarks(CInt(Data(1)))
            Dim StrVal As String
            StrVal = HD.GetString(DT)
            CallBackReturn = StrVal.ToString
        ElseIf CInt(Data(0)) = 5 Then
            Dim AssetTag As String = CStr(Data(1))
            Dim BranchID As Integer = CInt(Data(2))
            Dim Dept As Integer = CInt(Data(3))
            Dim ItemDtl() As String = Data(4).Split(CChar("~"))
            Dim ItemID As Integer = CInt(ItemDtl(0))
            DT = DB.ExecuteDataSet("Select COUNT(ASSET_ID) from FA_MASTER where STATUS_ID=1 AND LOCATION_ID=" & BranchID & " and DEPARTMENT_ID=" & Dept & " and ITEM_ID in (select item_id from hd_problem_type where problem_id =  " & ItemID & ") and ASSET_TAG='" & AssetTag & "'").Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn = CStr(DT.Rows(0)(0))
            Else
                CallBackReturn = "0"
            End If
        End If
    End Sub
#End Region

End Class
