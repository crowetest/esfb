﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="BranchRequestClose.aspx.vb" Inherits="BranchRequestClose" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <style type="text/css">
            #Button
            {
                width: 80%;
                height: 20px;
                font-weight: bold;
                line-height: 20px;
                text-align: center;
                border-top-left-radius: 25px;
                border-top-right-radius: 25px;
                border-bottom-left-radius: 25px;
                border-bottom-right-radius: 25px;
                cursor: pointer;
                background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
            }
            
            #Button:hover
            {
                background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
            }
            .sub_hdRow
            {
                background-color: #EBCCD6;
                height: 20px;
                font-family: Arial;
                color: #B84D4D;
                font-size: 8.5pt;
                font-weight: bold;
            }
            .style1
            {
                width: 100%;
            }
            .style2
            {
                width: 30%;
                height: 5px;
            }
            .style3
            {
                width: 5%;
                height: 5px;
            }
            .style4
            {
                width: 40%;
                height: 5px;
            }
            .ScrollClass
        {           
            text-align:center; color:#E31E24;
            background-color:#EBDDDD;
            height:20px;
           
        }
            </style>
        <script language="javascript" type="text/javascript">
   var selected_request_status = 0 ;
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
       
        function RequestOnChange()
            {  
                if (document.getElementById("<%= cmbRequest.ClientID %>").value != "-1")
                    {
                        var Remark=document.getElementById("<%= cmbRequest.ClientID %>").value.split("¥");
                         document.getElementById("txtPreremark").value= Remark[1]; 
                         
                         document.getElementById("colRequest").innerHTML= Remark[3];
                         selected_request_status = Remark[4];
                         if  (Remark[4]==12)
                            document.getElementById("colRequest").style.color='Red';
                        else 
                            document.getElementById("colRequest").style.color='Green';                        

                     }
                else
                    {document.getElementById("colRequest").innerHTML= '';
                    document.getElementById("txtPreremark").value="";}
            }



        function UpdateOnClick()
        {
            if (document.getElementById("<%= cmbRequest.ClientID %>").value == "-1") 
                {
                    alert("Select Request");
                    document.getElementById("<%= cmbRequest.ClientID %>").focus();
                    return false;
                }
                var CloseOPenFlag=0;
                if(document.getElementById("optClose").checked==true)
                {
                    CloseOPenFlag=1;
                }
                else if (document.getElementById("optReopen").checked==true)
                {
                      CloseOPenFlag=2;
                }
                if (CloseOPenFlag==0)
                {
                    alert("Select Close/Open");document.getElementById("optClose").focus();return false;
                }
                if(CloseOPenFlag==2 &&  document.getElementById("txtRemark").value=="")
                 {   alert("Enter Remark");
                    document.getElementById("txtRemark").focus();
                    return false;
                }
                var strreopenMsg="Are You Sure To Close This Ticket ?";
                if(CloseOPenFlag==2)
                    strreopenMsg="Are You Sure To Reopen This Ticket ?";
                if (confirm(strreopenMsg)==true)
                {
                    var ReqId=document.getElementById("<%= cmbRequest.ClientID %>").value.split("¥");
                    var Request= ReqId[0];
                    var PreRemark=document.getElementById("txtPreremark").value
                    var Remark=document.getElementById("txtRemark").value
                    var problemid=ReqId[2]
                    document.getElementById("<%= hid_dtls.ClientID %>").value =  Request+ "Ø"+ Remark + "Ø" + PreRemark + "Ø" +problemid+ "Ø" + CloseOPenFlag;                
	              
                }
               
        }

        
function AddMoreImages() {

            if (!document.getElementById && !document.createElement)
            
                return false;
            var fileUploadarea = document.getElementById("fileUploadarea");
            if (!fileUploadarea)
                return false;
            var newLine = document.createElement("br");
            fileUploadarea.appendChild(newLine);
            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("class", "fileUpload");
            
            if (!AddMoreImages.lastAssignedId)
                AddMoreImages.lastAssignedId = 100;
            newFile.setAttribute("id", "FileUpload" + AddMoreImages.lastAssignedId);
            newFile.setAttribute("name", "FileUpload" + AddMoreImages.lastAssignedId);
            var div = document.createElement("div");
            div.appendChild(newFile);
            div.setAttribute("id", "div" + AddMoreImages.lastAssignedId);
            fileUploadarea.appendChild(div);
            AddMoreImages.lastAssignedId++;
        }



function optClose_onclick() {
    document.getElementById("lblMSG").innerHTML="";
   EnableAttachment();
}

function EnableAttachment()
{
    if(document.getElementById("optReopen").checked==true)
    {
        document.getElementById("<%= fup1.ClientID %>").disabled=false;
         document.getElementById("btnAddMoreFiles").disabled=false;
    }
    else
    {
        document.getElementById("<%= fup1.ClientID %>").disabled=true;
        document.getElementById("btnAddMoreFiles").disabled=true;
    }
}
function optReopen_onclick() {
if (selected_request_status == 4 )
    {
    alert ( "Resolved tickets cannot reopen");
    document.getElementById("lblMSG").innerHTML="";
    document.getElementById("optReopen").checked = false;
    }
else 
    {
    document.getElementById("lblMSG").innerHTML="All existing attachments will be removed while Re-Open. Need to attach new Files if any.";
    EnableAttachment();
    }
}

        </script>
    </head>
    </html>
    <div class="ScrollClass"><b>Help Desk Contact Numbers -</b> 08589975581, 08589975582, 08589975583, 08589975584<hr style="color:#E31E24; margin-top:0px;" /></div>
    <asp:HiddenField ID="hid_dtls" runat="server" />
    <br />
  
    <table class="style1" style="width:100%">       
        <tr> 
            <td style="text-align:center; margin:0px auto; width:30%;">
                &nbsp;</td> 
            <td style="text-align:left; margin:0px auto; width:5%;">
                </td> 
            <td id="colRequest" style="text-align: center;font-weight: bold;  margin: 0px auto;width: 60%; "></td></tr> 
        <tr> 
            <td style="text-align:center; margin:0px auto; width:30%;">
                &nbsp;</td> 
            <td style="text-align:left; margin:0px auto; width:5%;">
                Request</td> 
            <td style="text-align:left; margin:0px auto; width:65%;">
                <asp:DropDownList ID="cmbRequest" class="NormalText" runat="server" Font-Names="Cambria" 
                 Width="60%" ForeColor="Black"></asp:DropDownList></td></tr>

        <tr>
            <td style="text-align:center; margin:0px auto; width:30%;"></td>            
            <td style="text-align:left; margin:0px auto; width:5%;">Prevoius Remark</td>
            <td style="width:40%" colSpan="2">
                    <textarea id="txtPreremark" cols="20" name="S1" rows="3" class="ReadOnlyTextBox" onkeypress='return TextAreaCheck(event)' 
                     style="width: 60%; resize: none;" readonly="readonly"  ></textarea></td></tr> 
         <tr>
            <td style="text-align:center; margin:0px auto; " class="style2"></td>            
            <td style="text-align:left; margin:0px auto; " class="style3"></td>       
            <td colspan="2" class="style4">
                <input id="optClose" type="radio" onclick="return optClose_onclick()" 
                   name="opt" />&nbsp;Close&nbsp;&nbsp;&nbsp;<input 
                    id="optReopen" type="radio"  onclick="return optReopen_onclick()" name="opt"  />&nbsp;Reopen</td></tr>
        
        
           <tr>            
            <td style="text-align:center; margin:0px auto; " class="style2"></td>            
            <td style="text-align:left; margin:0px auto; " class="style3"></td>       
            <td id="lblMSG" colspan="2" class="style4" style="text-align:left; font-family:Cambria:Times New Roman; font-style:italic ; font-size:10pt; color:Red;">
                </td></tr>
        
        
           <tr>
            <td style="text-align:center; margin:0px auto; width:30%;">
           <asp:HiddenField ID="hid_Post" runat="server" />
               </td>            
            <td style="text-align:left; margin:0px auto; width:5%;">Remark</td>       
            <td style="width:40%" colspan="2">
                <textarea id="txtRemark" cols="20" name="S1" rows="3" maxlength="1000" onkeypress='return TextAreaCheck(event)' 
                    style="width:60%" class="NormalText" ></textarea></td></tr>
        

           
      <tr id="Tr7"> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Attachment If any</td>
       
        <td style="width:63%"> &nbsp; &nbsp; 
            <div id="fileUploadarea"><asp:FileUpload ID="fup1" runat="server" 
                    CssClass="fileUpload" Enabled="False" /><br /></div><br />
       <div><input style="display:block;" id="btnAddMoreFiles" type="button" 
               value="Add more images" onclick="AddMoreImages();" disabled="disabled" /><br />        
        </div></td>
        <td style="width:12%;text-align:left; ">
            &nbsp;</td>
            
        <td style="width:20%">
            &nbsp;</td>
    </tr>
   
        <tr>
            <td style="text-align:center;" colspan="3">
                <br />
                &nbsp;
                &nbsp;
                <asp:Button 
                ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" style="cursor:pointer;"
                 Width="6%" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td> </tr>
       
                        
                
    </table>   
</asp:Content>
