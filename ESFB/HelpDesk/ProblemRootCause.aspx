﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ProblemRootCause.aspx.vb" Inherits="HelpDesk_ProblemRootCause" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server" >
<head>
<title></title>
<link href="../Style/Style.css" rel="stylesheet" type="text/css" />
<script src="../Script/Validations.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" for="window" event="onload">return ChangeOnChange()</script>
<script language="javascript" type="text/javascript">
    function ChangeOnChange()
    {
        if (document.getElementById("<%= cmbProblem.ClientID %>").value == '-1') return;
        var ProblemInfo = document.getElementById("<%= cmbProblem.ClientID %>").value.split("^");
        document.getElementById("<%= txtProblem.ClientID %>").value = ProblemInfo[1];
        document.getElementById("<%= txtImpact.ClientID %>").value = ProblemInfo[2];
        document.getElementById("<%= txtCategory.ClientID %>").value = ProblemInfo[3];
        document.getElementById("<%= txtPriority.ClientID %>").value = ProblemInfo[4];
        document.getElementById("<%= txtRootCause.ClientID %>").value = ProblemInfo[6];
        document.getElementById("<%= txtWorkAround.ClientID %>").value = ProblemInfo[7];
        document.getElementById("<%= txtSolution.ClientID %>").value = ProblemInfo[8];
        ClearCombo("<%= cmbStatus.ClientID %>");
        var Data        = "2ʘ"+ ProblemInfo[5];
        ToServer(Data,2)
    }
    function DetailsOnClick()
    {
        if(document.getElementById("<%= cmbProblem.ClientID %>").value =="-1")
        {
            alert("Select Ticket");
            document.getElementById("<%= cmbProblem.ClientID %>").focus();
            return false;
        }
        var ProblemInfo = document.getElementById("<%= cmbProblem.ClientID %>").value.split("^");
        var ProblemNo   = ProblemInfo[0];
        window.open("Reports/ProblemTicketInfo.aspx?ProblemNo=" + btoa(ProblemNo) + "");
    }
    function btnSave_onclick() 
    {
    if (document.getElementById("<%= cmbProblem.ClientID %>").value == "-1") { alert("Select Problem"); document.getElementById("<%= cmbProblem.ClientID %>").focus(); return false; }
    if (document.getElementById("<%= txtRootCause.ClientID %>").value == "") { alert("Enter Root Cause"); document.getElementById("<%= txtRootCause.ClientID %>").focus(); return false; }
    if (document.getElementById("<%= txtWorkAround.ClientID %>").value == "") { alert("Enter Work Around"); document.getElementById("<%= txtWorkAround.ClientID %>").focus(); return false; }
    if (document.getElementById("<%= txtSolution.ClientID %>").value == "") { alert("Enter Solution"); document.getElementById("<%= txtSolution.ClientID %>").focus(); return false; }
        var ProblemInfo = document.getElementById("<%= cmbProblem.ClientID %>").value.split("^");
        var ProblemNo   = ProblemInfo[0];
        var RootCause   = document.getElementById("<%= txtRootCause.ClientID %>").value;
        var WorkAround  = document.getElementById("<%= txtWorkAround.ClientID %>").value;
        var Solution    =  document.getElementById("<%= txtSolution.ClientID %>").value;
        var Status      = document.getElementById("<%= cmbStatus.ClientID %>").value;

        var Data        = "1ʘ"+ ProblemNo + "ʘ" + RootCause + "ʘ" + WorkAround + "ʘ" + Solution + "ʘ" + Status;
        ToServer(Data,1)
    }
    function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
    function FromServer(Arg, Context) 
    {
        switch(Context)
        {
            case 1: // Confirmation
            {
                var Data = Arg.split("ʘ");
                alert(Data[1]);
                if ( Data[0] == 0 )  
                {
                    window.open("../home.aspx","_self");
                }
                break;
            }
             case 2:
             {
                        ComboFill(Arg, "<%= cmbStatus.ClientID %>");
                        break;
              }
        }
    }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
</script>
</head>
    <br />
           
    <table align="center" style="width: 90%; margin: 0px auto; ">
        <tr>

            <td style="width: 17%; text-align:right;">Select Problem Ticket</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 80%; text-align:left;cursor:pointer;">
                    <asp:DropDownList ID="cmbProblem" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="60%" ForeColor="Black">
                            <asp:ListItem Text=" -----Select-----" Value="-1" />
                        </asp:DropDownList>&nbsp;&nbsp;&nbsp;
                    <asp:HyperLink ID="hlDetails" runat="server" ForeColor="#0000CC" 
                        style="text-decoration: underline" ToolTip="View Details">View Details</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td style="width: 17%; text-align:right;">Problem</td>
            <td style="width: 3%;"></td>
            <td style="width: 80%; text-align:left;">
                <asp:TextBox ID="txtProblem" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)'  
                    ReadOnly="True"/></td>
        </tr>
        <tr>
            <td style="width: 17%; text-align:right;">Impact</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 80%; text-align:left;">
                <asp:TextBox ID="txtImpact" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine"  onkeypress='return TextAreaCheck(event)' 
                    ReadOnly="True"/></td>
        </tr>
        <tr>
            <td style="width: 17%; text-align:right;">Category</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 80%; text-align:left;">
                <asp:TextBox ID="txtCategory" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="40%" MaxLength="1000" Rows="2" ReadOnly="True"/>

                        </td>
        </tr>
        <tr>
            <td style="width: 17%; text-align:right;">Priority</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 80%; text-align:left;">
                <asp:TextBox ID="txtPriority" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="40%" MaxLength="1000" Rows="2" ReadOnly="True"/>

                        </td>
        </tr>
        <tr>
            <td style="width: 17%; text-align:right;">Root Cause</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 80%; text-align:left;">
                <asp:TextBox ID="txtRootCause" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr>
            <td style="width: 17%; text-align:right;">Work Around Solution</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 80%; text-align:left;">
                <asp:TextBox ID="txtWorkAround" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr>
            <td style="width: 17%; text-align:right;">Permanent Solution</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 80%; text-align:left;">
                <asp:TextBox ID="txtSolution" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr>
            <td style="width: 17%; text-align:right;">Status</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 80%; text-align:left;">
                    <asp:DropDownList ID="cmbStatus" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="100%" ForeColor="Black">
                        </asp:DropDownList></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td style="text-align:center;"colspan="3" class="style2">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="SAVE" onclick="return btnSave_onclick()" />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" />
            </td>            
         </tr>
    </table>
    <br />
</asp:Content>


