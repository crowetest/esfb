﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ApproveChangePlan
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim HD As New HelpDesk
    Dim CallBackReturn As String = Nothing
    Dim UserID As Integer
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 259) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Me.Master.subtitle = "CAB Approval"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- End ---//

            UserID = CInt(Session("UserID"))
            DT = GN.GetQueryResult("select count(*) from HD_CHANGE_MASTER where STATUS_ID in (0,3) and CHANGE_NO in (select CHANGE_NO from HD_CHANGE_CAB where EMP_CODE = " & UserID & " and status_id = 0) ")
            If DT.Rows.Count = 0 Then
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_script1.Append("         alert('No Pending for Approval');window.open('../home.aspx','_self');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
                Exit Sub
            End If

            If Not IsPostBack Then
                DT = GN.GetQueryResult("SELECT '---SELECT---','-1' UNION ALL select CHANGE_NO,CHANGE_NO from HD_CHANGE_MASTER where STATUS_ID in (0,3)  and CHANGE_NO in (select CHANGE_NO from HD_CHANGE_CAB where EMP_CODE = " & UserID & " and status_id = 0)  ")
                GN.ComboFill(cmbChange, DT, 1, 0)
            End If
            Me.cmbChange.Attributes.Add("onchange", "ChangeOnChange()")
            Me.hlDetails.Attributes.Add("onclick", "DetailsOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim ChangeNo As String = CStr(Data(1))
            DT = GN.GetQueryResult("SELECT CHANGE,REASON FROM HD_CHANGE_MASTER WHERE CHANGE_NO = '" & ChangeNo & "'")
            CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString()
        ElseIf CInt(Data(0)) = 2 Then
            Dim ChangeNo As String = CStr(Data(1))
            Dim Remarks As String = CStr(Data(2))
            Dim StatusID As Integer = CInt(Data(3))
            Dim ErrorFlag As Integer = 0
            Dim Message As String = ""
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@ChangeNo", SqlDbType.VarChar, 20)
                Params(0).Value = ChangeNo
                Params(1) = New SqlParameter("@Reason", SqlDbType.VarChar, 100)
                Params(1).Value = Remarks
                Params(2) = New SqlParameter("@StatusID", SqlDbType.Int)
                Params(2).Value = StatusID
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = CInt(Session("UserID"))
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_HD_CHANGE_CAB_APPROVE", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If

    End Sub
#End Region
End Class
