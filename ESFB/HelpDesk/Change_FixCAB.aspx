﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true"  CodeFile="Change_FixCAB.aspx.vb" Inherits="Change_FixCAB" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
         .fileUpload{
    width:255px;    
    font-size:11px;
    color:#000000;
    border:solid;
    border-width:1px;
    border-color:#7f9db9;    
    height:17px;
    }
            .ScrollClass
        {           
            text-align:center; color:#E31E24;
            background-color:#EBDDDD;
            height:20px;
           
        }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function ChangeOnChange()
        {
          var ChangeID=document.getElementById("<%= cmbChange.ClientID %>").value.split("~");    
          if(ChangeID[0]==-1)    
          {
            document.getElementById("txtChange").value ="";
            document.getElementById("txtReason").value ="";
          }
          else{
            document.getElementById("txtChange").value = ChangeID[1];
            document.getElementById("txtReason").value = ChangeID[2];
          }
          document.getElementById("txtEmpCode1").focus();
        }

        function DetailsOnClick()
        {
            var ChangeInfo = document.getElementById("<%= cmbChange.ClientID %>").value.split("~");
            var ChangeNo   = ChangeInfo[0];
            if(ChangeNo == "-1")
            {
                alert("Select Ticket");
                document.getElementById("<%= cmbChange.ClientID %>").focus();
                return false;
            }
            window.open("Reports/ChangeTicketInfo.aspx?ChangeNo=" + btoa(ChangeNo) + "");
        }

        function EmpCodeOnChange(id){
          var EmpCode = document.getElementById("txtEmpCode"+id).value;
          if (EmpCode !=""){
            var ToData = "2Ø" + EmpCode +"Ø" +id;                  
            ToServer(ToData, 2);
          }
        }


        function table_fill() {

            document.getElementById("<%= pnlRequest.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr height=20px;  class='tblQal'>";
                tab += "<td style='width:5%;text-align:center' >#</td>";
                tab += "<td style='width:15%;text-align:left' >Emp_Code</td>";
                tab += "<td style='width:30%;text-align:left'>Employee Name</td>";
                tab += "<td style='width:25%;text-align:left'>Department</td>";
                tab += "<td style='width:25%;text-align:left'>Designation</td>";
                tab += "</tr>";
                
                if (document.getElementById("<%= hdnValue.ClientID %>").value != "") {
                row = document.getElementById("<%= hdnValue.ClientID %>").value.split("¥");              
                for (n = 1; n <= row.length - 1; n++) {                   
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second';>";
                    }
                    i = n;
                    //EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time     
                    tab += "<td style='width:5%;text-align:center;'>" + i + "</td>"; 
                    var txtBox1 = "<input type=text id='txtEmpCode" + i + "' name='txtEmpCode" + i + "'  style='width:99%;text-align:center;' value='"+ col[0] +"' class='NormalText'  onkeypress='NumericCheck(event)' onblur='return EmpCodeOnChange("+ i +")'   maxlength='6' />";               
                    tab += "<td style='width:15%;text-align:center' >"+ txtBox1 +"</td>";
                    var txtBox2 = "<input type=text id='txtEmpName" + i + "' name='txtEmpName" + i + "'  style='width:99%;text-align:left;' value='"+ col[1] +"' class='ReadOnlyTextBox' ReadOnly =true  />";               
                    tab += "<td style='width:30%;text-align:left'>"+ txtBox2 +"</td>";  
                    var txtBox3 = "<input type=text id='txtDep" + i + "' name='txtDep" + i + "'  style='width:99%;text-align:left;' class='ReadOnlyTextBox' value='"+ col[2] +"'  ReadOnly =true  />";                               
                    tab += "<td style='width:25%;text-align:left'>"+ txtBox3 +"</td>";                  
                    var txtBox4 = "<input type=text id='txtDes" + i + "' name='txtDes" + i + "'  style='width:99%;text-align:left;' class='ReadOnlyTextBox' value='"+ col[3] +"' ReadOnly =true  />";                               
                    tab += "<td style='width:25%;text-align:left'>"+ txtBox4 +"</td>";   
                    tab += "<td style='width:10%;text-align:center' onclick=DeleteRow('" + i + "')><img  src='../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";                  
                    tab += "</tr>";                                 
                }
            }
                tab += "</table></div>";
                tab += "</div></div>";  
                var xxx = "txtEmpCode" + i + ".focus();"
                setTimeout(xxx, 1);              
                document.getElementById("<%= pnlRequest.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }
        function AddNewRow() {                          
                if (document.getElementById("<%= hdnvalue.ClientID %>").value != "") {                      
                    row = document.getElementById("<%= hdnvalue.ClientID %>").value.split("¥");
                    var Len = row.length - 1;          
                    col = row[Len].split("µ");                      
                    if (col[0] != "" && col[1] != "" && col[2] != "" ) {

                        document.getElementById("<%= hdnvalue.ClientID %>").value += "¥µµµ";                          
                        
                    }
                }
                else
                document.getElementById("<%= hdnvalue.ClientID %>").value = "¥µµµ"; 
                table_fill();
            }
         

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }  
        function DeleteRow(id)
        {
           
            row = document.getElementById("<%= hdnvalue.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                col = row[n].split("µ"); 
                   if (id != n || col[0]=="" )                  
                     NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnvalue.ClientID %>").value=NewStr;
                if(document.getElementById("<%= hdnvalue.ClientID %>").value=="")
                    document.getElementById("<%= hdnvalue.ClientID %>").value="¥µµµ";
                table_fill();
        }
        function CreateNewRow(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValue(val);
                AddNewRow();                          
            }
        }
         function updateValue(id)
        {       
            row = document.getElementById("<%= hdnvalue.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id == n)
                   {

                           var EmpCode = document.getElementById("txtEmpCode"+id).value;
                           var Name = document.getElementById("txtEmpName"+id).value;
                           var Dep = document.getElementById("txtDep"+id).value;
                           var Des = document.getElementById("txtDes"+id).value;
                          
                           NewStr+="¥"+EmpCode+"µ"+Name+"µ"+Dep+"µ"+Des;

                   }
                   else
                   {
                        NewStr+="¥"+row[n];
                   }
                }
                document.getElementById("<%= hdnvalue.ClientID %>").value=NewStr;
              
        }
        function AddNewRow() {                          
                if (document.getElementById("<%= hdnvalue.ClientID %>").value != "") {                      
                    row = document.getElementById("<%= hdnvalue.ClientID %>").value.split("¥");
                    var Len = row.length - 1;          
                    col = row[Len].split("µ");                      
                    if (col[0] != "" && col[1] != "" && col[2] != "" && col[3] != "") {

                        document.getElementById("<%= hdnvalue.ClientID %>").value += "¥µµµ";                          
                        
                    }
                }
                else
                document.getElementById("<%= hdnvalue.ClientID %>").value = "¥µµµ"; 
                table_fill();
            }
        function FromServer(arg, context) {
         switch (context) {
             case 1:
                   var Data = arg.split("Ø");
                   alert(Data[1]);
                   if (Data[0] == 0) { window.open("../home.aspx", "_self"); }               
                   break;            
             case 2:
                    var Val=arg.split("~");
                    var Details = Val[1].split("Ø");
                    if(Details[0] == ""){alert("Invalid Employee Code");document.getElementById("txtEmpCode"+Val[0]).value="";document.getElementById("txtEmpCode"+Val[0]).focus();return false;}
                    else
                    {
                        document.getElementById("txtEmpName"+Val[0]).value = Details[0];
                        document.getElementById("txtDep"+Val[0]).value = Details[1];
                        document.getElementById("txtDes"+Val[0]).value = Details[2];
                       
                        updateValue(Val[0]);
                        AddNewRow();
                        
                    }
                    break;             
             default:
                    break;
        
            }
            
        }

function btnSave_onclick() {

          var ChangeID=document.getElementById("<%= cmbChange.ClientID %>").value.split("~");    
          if(ChangeID==-1)    
          {alert("Select Change");document.getElementById("<%= cmbChange.ClientID %>").focus();return false;}   
                   
          row = document.getElementById("<%= hdnValue.ClientID %>").value.split("¥");
             var newstrEmpCode=""
                for (n = 1; n <= row.length-1 ; n++) {     
                    var Qualcol5=row[n].split("µ");            
                    if(Qualcol5[0]!="" && Qualcol5[1]!="")
                    {                         
                        newstrEmpCode+="¥"+Qualcol5[0];
                    }
                
                }

           if(newstrEmpCode == "") 
           {
                alert("Select atleast one Employee"); return false;
           }  
          var ToData = "1Ø" + ChangeID[0]+"Ø"+newstrEmpCode;                  
            ToServer(ToData, 1);

}

    </script>
   
</head>
</html>

<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;" >
            <tr> <td style="width:20%;">Change</td>
            <td style="width:65%; height:20px; text-align:left;cursor:pointer;"  >
                <asp:DropDownList ID="cmbChange" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="80%" ForeColor="Black">
                </asp:DropDownList> &nbsp;<asp:HyperLink ID="hlDetails" runat="server" ForeColor="#0000CC" 
                        style="text-decoration: underline" ToolTip="View Details">View Details</asp:HyperLink>
                </td>
                
           <td style="width:15%;"></td>
        </tr>
            <tr> <td style="width:20%;">Change Description</td>
            <td style="width:65%; height:20px; text-align:left;"  colspan=2 >&nbsp;<textarea 
                    id="txtChange" cols="20" name="S1" rows="2"   maxlength="1000" class="ReadOnlyTextBox"
                    style="width: 95%" readonly="readonly" onkeypress='return TextAreaCheck(event)' ></textarea></td>
                
           
        </tr>
        <tr> <td style="width:20%;">Change Reason</td>
            <td style="width:65%; height:20px; text-align:left;"  colspan="2"  >&nbsp;<textarea id="txtReason" class="ReadOnlyTextBox" cols="20" name="S1" rows="2"   maxlength="1000" readonly="readonly"
                    style="width: 95%" onkeypress='return TextAreaCheck(event)' ></textarea></td>
                
           
        </tr>
        <tr>
            <td style="width:20%; height:20px; text-align:center;" colspan="3" class="mainhead" >
                <strong>ADVISORY BOARD DETAILS</strong></td>
          
        </tr>
            <tr style="text-align:center;"> 
            <td  colspan="3" style="text-align:center;">
                <asp:Panel ID="pnlRequest" runat="server">
                </asp:Panel>
               </td>
               
        </tr>
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                &nbsp;<input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="SAVE"  onclick="return btnSave_onclick()" />
                 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
                </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

