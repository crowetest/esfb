﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewVendorStatusReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim VendorID As String
    Dim GF As New GeneralFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            Dim DT As New DataTable
            Dim Struser As String = Nothing
            VendorID = Request.QueryString.Get("VendorID")
            DT = DB.ExecuteDataSet("select VENDOR_NAME from HD_VENDOR_MASTER where VENDOR_ID=" & VendorID & "").Tables(0)
            If DT.Rows.Count > 0 Then
                Struser = DT.Rows(0)(0)
            End If
            Dim SqlStr As String
            SqlStr = "select a.TICKET_NO,a.REQUEST_DT, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS,a.REQUEST_ID ,m.Emp_Name" & _
                        " from HD_REQUEST_MASTER a left outer join HD_PROBLEM_TYPE c on (a.PROBLEM_ID=c.PROBLEM_ID),  HD_GROUP_MASTER d, " & _
                        " HD_SUB_GROUP e,HD_STATUS s ,EMP_MASTER m where  a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and  " & _
                        " s.STATUS_ID = a.STATUS_ID And e.group_ID = d.group_ID and m.Emp_Code=a.USER_ID and a.VENDOR_ID = " + VendorID + "order by a.request_id asc"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RH.Heading(Session("FirmName"), tb, "STATUS OF TICKETS", 150)
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim TRSHead As New TableRow
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")
            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid



            RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "R", "VENDOR NAME :" + Struser)
            tb.Controls.Add(TRSHead)
            RH.BlankRow(tb, 4)


            Dim TRHead_1 As New TableRow
            'TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderStyle = "1"
            TRHead_1_07.BorderStyle = "1"


            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver

            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead_1, TRHead_1_00, 10, 10, "1", "Ticket No")
            RH.AddColumn(TRHead_1, TRHead_1_01, 9, 9, "l", "Date")
            RH.AddColumn(TRHead_1, TRHead_1_02, 15, 15, "l", "Category")
            RH.AddColumn(TRHead_1, TRHead_1_03, 15, 15, "l", "Sub Category")
            RH.AddColumn(TRHead_1, TRHead_1_04, 15, 15, "l", "Findings")
            RH.AddColumn(TRHead_1, TRHead_1_05, 20, 20, "l", "Status")
            RH.AddColumn(TRHead_1, TRHead_1_06, 35, 35, "l", "Remark")
            RH.AddColumn(TRHead_1, TRHead_1_07, 12, 12, "1", "User")
            tb.Controls.Add(TRHead_1)
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07 As New TableCell
                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 10, 10, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(7).ToString) + "' target='_blank'>" + DR(0).ToString())
                RH.AddColumn(TR3, TR3_01, 9, 9, "l", CDate(DR(1)).ToString("dd/MM/yyyy"))
                RH.AddColumn(TR3, TR3_02, 15, 15, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_04, 15, 15, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_05, 20, 20, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_06, 35, 35, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_07, 12, 12, "l", DR(8).ToString())
                tb.Controls.Add(TR3)
            Next
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
