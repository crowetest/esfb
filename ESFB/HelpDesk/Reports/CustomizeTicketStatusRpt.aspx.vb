﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CustomizeTicketStatusRpt
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler

    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 136) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Ticket Status Report"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            DT = HD.GetALLBranch()
            GF.ComboFill(cmbBranch, DT, 0, 1)
            DT = HD.GetALLGroup()
            GF.ComboFill(cmbGroup, DT, 0, 1)

            DT = DB.ExecuteDataSet("select team_id from hd_team_Dtl where emp_code=" & CInt(Session("UserID")) & "").Tables(0)
            If DT.Rows.Count > 0 Then
                hid_USer.Value = CStr(CInt(DT.Rows(0).Item(0)))
            End If

            If CInt(hid_USer.Value) = 5 Or CInt(hid_USer.Value) = 6 Then
                DT = DB.ExecuteDataSet("select team_id,team_name from hd_team_master where status_id=1 and TEAM_ID in(5,6)").Tables(0)
            Else
                DT = HD.GetALLTeamMembers()
            End If
            If DT.Rows.Count > 1 Then
                For Each DR As DataRow In DT.Rows
                    Me.hdnTeam.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            End If

            DT = HD.GetALLStatus()
            GF.ComboFill(cmbStatus, DT, 0, 1)
            cmbBranch.SelectedValue = CStr(-1)

            Me.cmbGroup.Attributes.Add("onchange", "return GroupOnChange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function


    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        
        If CInt(Data(0)) = 1 Then
            'If CInt(Data(1)) > 0 Then
            DT = HD.GetALLSubGroup(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            'End If
        End If
    End Sub
End Class
