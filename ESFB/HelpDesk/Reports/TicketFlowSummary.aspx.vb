﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports System.Math
Partial Class TicketFlowSummary
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim BranchID As Integer
    Dim WebTools As New WebApp.Tools
    Dim DT, DT1 As New DataTable
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 350) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RH.Heading(Session("FirmName"), tb, "Ticket Flow Summary", 100)

            tb.Attributes.Add("width", "100%")
            tb.Font.Size = 25
            BranchID = Me.Session("BranchID")

            Dim TRF As New TableRow
            TRF.BorderStyle = BorderStyle.Solid
            TRF.BackColor = Drawing.Color.WhiteSmoke
            Dim TRF_01, TRF_02 As New TableCell

            RH.InsertColumn(TRF, TRF_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Particuars")
            RH.InsertColumn(TRF, TRF_02, 40, WholeHelper.ClsRepCtrl.Allign.Centre, "Ticket Count")

            TRF.Style.Add("background-color", "#fff9e5")
            TRF.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TRF)

            Dim TRS As New TableRow
            TRS.BorderStyle = BorderStyle.Solid
            TRS.BackColor = Drawing.Color.WhiteSmoke
            Dim TRS_01, TRS_02, TRS_03 As New TableCell

            RH.InsertColumn(TRS, TRS_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "<font size='4'><b>CMPC TOTAL TICKET FLOW</b></fon>")
            RH.InsertColumn(TRS, TRS_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TRS, TRS_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TRS.Style.Add("background-color", "#002699")
            TRS.ForeColor = Drawing.Color.White
            tb.Controls.Add(TRS)

            '----------------------- Start

            DT = DB.ExecuteDataSet("[SP_HD_TICKET_FLOW_SUMMARY]").Tables(0)

            '----------------------- End

            Dim TR3 As New TableRow
            TR3.BorderStyle = BorderStyle.Solid
            TR3.BackColor = Drawing.Color.WhiteSmoke
            Dim TR3_01, TR3_02, TR3_03 As New TableCell

            RH.InsertColumn(TR3, TR3_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "<font size='3'><b>INFLOW</b></font>")
            RH.InsertColumn(TR3, TR3_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR3, TR3_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR3.Style.Add("background-color", "#ffb3b3")
            TR3.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR3)

            Dim TR4 As New TableRow
            TR4.BorderStyle = BorderStyle.Solid
            TR4.BackColor = Drawing.Color.WhiteSmoke
            Dim TR4_01, TR4_02, TR4_03 As New TableCell

            Dim Total_Opening As Integer = Math.Abs(DT.Rows(0)(1)) + Math.Abs(DT.Rows(1)(1))

            RH.InsertColumn(TR4, TR4_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "OPENING TICKET COUNT")
            RH.InsertColumn(TR4, TR4_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR4, TR4_03, 20, WholeHelper.ClsRepCtrl.Allign.Right, Total_Opening)

            TR4.Style.Add("background-color", "#e5e5ff")
            TR4.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR4)

            Dim TR5 As New TableRow
            TR5.BorderStyle = BorderStyle.Solid
            TR5.BackColor = Drawing.Color.WhiteSmoke
            Dim TR5_01, TR5_02, TR5_03 As New TableCell

            RH.InsertColumn(TR5, TR5_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "CPU")
            RH.InsertColumn(TR5, TR5_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(1))
            RH.InsertColumn(TR5, TR5_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR5.Style.Add("background-color", "#fff9e5")
            TR5.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR5)

            Dim TR6 As New TableRow
            TR6.BorderStyle = BorderStyle.Solid
            TR6.BackColor = Drawing.Color.WhiteSmoke
            Dim TR6_01, TR6_02, TR6_03 As New TableCell

            RH.InsertColumn(TR6, TR6_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "CREDIT")
            RH.InsertColumn(TR6, TR6_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(1))
            RH.InsertColumn(TR6, TR6_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR6.Style.Add("background-color", "#fff9e5")
            TR6.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR6)

            Dim TR7 As New TableRow
            TR7.BorderStyle = BorderStyle.Solid
            TR7.BackColor = Drawing.Color.WhiteSmoke
            Dim TR7_01, TR7_02, TR7_03 As New TableCell

            Dim TotAssigned As Integer = Math.Abs(DT.Rows(0)(2)) + Math.Abs(DT.Rows(1)(2))
            RH.InsertColumn(TR7, TR7_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Tickets Assigned to the team for the day")
            RH.InsertColumn(TR7, TR7_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR7, TR7_03, 20, WholeHelper.ClsRepCtrl.Allign.Right, TotAssigned)

            TR7.Style.Add("background-color", "#fff9e5")
            TR7.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR7)

            Dim TR8 As New TableRow
            TR8.BorderStyle = BorderStyle.Solid
            TR8.BackColor = Drawing.Color.WhiteSmoke
            Dim TR8_01, TR8_02, TR8_03 As New TableCell

            Dim InFlow As Integer = Math.Abs(Total_Opening) + Math.Abs(TotAssigned)
            RH.InsertColumn(TR8, TR8_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "TOTAL - INFLOW")
            RH.InsertColumn(TR8, TR8_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR8, TR8_03, 20, WholeHelper.ClsRepCtrl.Allign.Right, InFlow)

            TR8.Style.Add("background-color", "#e5e5ff")
            TR8.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR8)

            Dim TR9 As New TableRow
            TR9.BorderStyle = BorderStyle.Solid
            TR9.BackColor = Drawing.Color.WhiteSmoke
            Dim TR9_01, TR9_02, TR9_03 As New TableCell

            RH.InsertColumn(TR9, TR9_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "<font size='3'><b>OUTFLOW</b></font>")
            RH.InsertColumn(TR9, TR9_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR9, TR9_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR9.Style.Add("background-color", "#ffb3b3")
            TR9.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR9)

            Dim TR10 As New TableRow
            TR10.BorderStyle = BorderStyle.Solid
            TR10.BackColor = Drawing.Color.WhiteSmoke
            Dim TR10_01, TR10_02, TR10_03 As New TableCell

            Dim TotResolved As Integer = Math.Abs(DT.Rows(0)(5)) + Math.Abs(DT.Rows(1)(5)) + Math.Abs(DT.Rows(0)(14)) + Math.Abs(DT.Rows(1)(14))
            RH.InsertColumn(TR10, TR10_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Ticket Resolved for the day")
            RH.InsertColumn(TR10, TR10_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, TotResolved)
            RH.InsertColumn(TR10, TR10_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR10.Style.Add("background-color", "#fff9e5")
            TR10.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR10)

            Dim TR11 As New TableRow
            TR11.BorderStyle = BorderStyle.Solid
            TR11.BackColor = Drawing.Color.WhiteSmoke
            Dim TR11_01, TR11_02, TR11_03 As New TableCell

            Dim TotReturned As Integer = Math.Abs(DT.Rows(0)(4)) + Math.Abs(DT.Rows(1)(4))
            RH.InsertColumn(TR11, TR11_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Return to Branch")
            RH.InsertColumn(TR11, TR11_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, TotReturned)
            RH.InsertColumn(TR11, TR11_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR11.Style.Add("background-color", "#fff9e5")
            TR11.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR11)

            Dim TR1New As New TableRow
            TR1New.BorderStyle = BorderStyle.Solid
            TR1New.BackColor = Drawing.Color.WhiteSmoke
            Dim TR1New_01, TR1New_02, TR1New_03 As New TableCell

            Dim AssignedEachOther As Integer = Math.Abs(DT.Rows(0)(13)) + Math.Abs(DT.Rows(1)(13))
            RH.InsertColumn(TR1New, TR1New_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Assigned Each Other")
            RH.InsertColumn(TR1New, TR1New_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, AssignedEachOther)
            RH.InsertColumn(TR1New, TR1New_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR1New.Style.Add("background-color", "#fff9e5")
            TR1New.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR1New)

            Dim TR1New2 As New TableRow
            TR1New2.BorderStyle = BorderStyle.Solid
            TR1New2.BackColor = Drawing.Color.WhiteSmoke
            Dim TR1New2_01, TR1New2_02, TR1New2_03 As New TableCell


            Dim AssignTot = Math.Abs(DT.Rows(0)(3)) + Math.Abs(DT.Rows(1)(3))
            Dim AssignedOther As Integer = Math.Abs(AssignTot) - Math.Abs(AssignedEachOther)
            RH.InsertColumn(TR1New2, TR1New2_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Assigned To Others")
            RH.InsertColumn(TR1New2, TR1New2_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, AssignedOther)
            RH.InsertColumn(TR1New2, TR1New2_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR1New2.Style.Add("background-color", "#fff9e5")
            TR1New2.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR1New2)


            Dim TR12 As New TableRow
            TR12.BorderStyle = BorderStyle.Solid
            TR12.BackColor = Drawing.Color.WhiteSmoke
            Dim TR12_01, TR12_02, TR12_03 As New TableCell

            Dim OutFlow As Integer = Math.Abs(TotResolved) + Math.Abs(TotReturned) + Math.Abs(AssignedEachOther) + Math.Abs(AssignedOther)
            RH.InsertColumn(TR12, TR12_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "TOTAL - OUTFLOW")
            RH.InsertColumn(TR12, TR12_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR12, TR12_03, 20, WholeHelper.ClsRepCtrl.Allign.Right, OutFlow)

            TR12.Style.Add("background-color", "#e5e5ff")
            TR12.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR12)


            Dim TR14 As New TableRow
            TR14.BorderStyle = BorderStyle.Solid
            TR14.BackColor = Drawing.Color.WhiteSmoke
            Dim TR14_01, TR14_02, TR14_03, TR14_04 As New TableCell

            Dim ClosingBal = InFlow - OutFlow
            RH.InsertColumn(TR14, TR14_01, 40, WholeHelper.ClsRepCtrl.Allign.Left, "<font size='3'><b>CLOSING BALANCE</b></font>")
            RH.InsertColumn(TR14, TR14_02, 20, WholeHelper.ClsRepCtrl.Allign.Centre, "According To Request Date")
            RH.InsertColumn(TR14, TR14_03, 20, WholeHelper.ClsRepCtrl.Allign.Centre, "According To Assign Date")
            RH.InsertColumn(TR14, TR14_04, 20, WholeHelper.ClsRepCtrl.Allign.Right, ClosingBal)

            TR14.Style.Add("background-color", "#ffb3b3")
            TR14.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR14)

            Dim TR15 As New TableRow
            TR15.BorderStyle = BorderStyle.Solid
            TR15.BackColor = Drawing.Color.WhiteSmoke
            Dim TR15_01, TR15_02, TR15_03, TR15_04 As New TableCell

            RH.InsertColumn(TR15, TR15_01, 40, WholeHelper.ClsRepCtrl.Allign.Left, "CLOSING TICKETS AGING")
            RH.InsertColumn(TR15, TR15_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR15, TR15_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR15, TR15_04, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR15.Style.Add("background-color", "#fff9e5")
            TR15.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR15)

            Dim TR16 As New TableRow
            TR16.BorderStyle = BorderStyle.Solid
            TR16.BackColor = Drawing.Color.WhiteSmoke
            Dim TR16_01, TR16_02, TR16_03, TR16_04 As New TableCell

            RH.InsertColumn(TR16, TR16_01, 40, WholeHelper.ClsRepCtrl.Allign.Left, "CPU")
            RH.InsertColumn(TR16, TR16_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR16, TR16_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR16, TR16_04, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(6))

            TR16.Style.Add("background-color", "#bfafa") ' f6f6ee
            TR16.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR16)

            Dim TR17 As New TableRow
            TR17.BorderStyle = BorderStyle.Solid
            TR17.BackColor = Drawing.Color.WhiteSmoke
            Dim TR17_01, TR17_02, TR17_03, TR17_04 As New TableCell

            RH.InsertColumn(TR17, TR17_01, 40, WholeHelper.ClsRepCtrl.Allign.Left, "Zero days")
            RH.InsertColumn(TR17, TR17_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(7))
            RH.InsertColumn(TR17, TR17_03, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(10))
            RH.InsertColumn(TR17, TR17_04, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR17.Style.Add("background-color", "#fff9e5")
            TR17.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR17)

            Dim TR18 As New TableRow
            TR18.BorderStyle = BorderStyle.Solid
            TR18.BackColor = Drawing.Color.WhiteSmoke
            Dim TR18_01, TR18_02, TR18_03, TR18_04 As New TableCell

            RH.InsertColumn(TR18, TR18_01, 40, WholeHelper.ClsRepCtrl.Allign.Left, "1-2 days")
            RH.InsertColumn(TR18, TR18_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(8))
            RH.InsertColumn(TR18, TR18_03, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(11))
            RH.InsertColumn(TR18, TR18_04, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR18.Style.Add("background-color", "#fff9e5")
            TR18.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR18)

            Dim TR19 As New TableRow
            TR19.BorderStyle = BorderStyle.Solid
            TR19.BackColor = Drawing.Color.WhiteSmoke
            Dim TR19_01, TR19_02, TR19_03, TR19_04 As New TableCell

            RH.InsertColumn(TR19, TR19_01, 40, WholeHelper.ClsRepCtrl.Allign.Left, "> 2 days (greater than 2 days)")
            RH.InsertColumn(TR19, TR19_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(9))
            RH.InsertColumn(TR19, TR19_03, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(12))
            RH.InsertColumn(TR19, TR19_04, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR19.Style.Add("background-color", "#fff9e5")
            TR19.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR19)

            Dim TR20 As New TableRow
            TR20.BorderStyle = BorderStyle.Solid
            TR20.BackColor = Drawing.Color.WhiteSmoke
            Dim TR20_01, TR20_02, TR20_03, TR20_04 As New TableCell

            RH.InsertColumn(TR20, TR20_01, 40, WholeHelper.ClsRepCtrl.Allign.Left, "CREDIT TEAM")
            RH.InsertColumn(TR20, TR20_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR20, TR20_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR20, TR20_04, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(6))

            TR20.Style.Add("background-color", "#bfafa")
            TR20.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR20)

            Dim TR21 As New TableRow
            TR21.BorderStyle = BorderStyle.Solid
            TR21.BackColor = Drawing.Color.WhiteSmoke
            Dim TR21_01, TR21_02, TR21_03, TR21_04 As New TableCell

            RH.InsertColumn(TR21, TR21_01, 40, WholeHelper.ClsRepCtrl.Allign.Left, "Zero days")
            RH.InsertColumn(TR21, TR21_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(7))
            RH.InsertColumn(TR21, TR21_03, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(10))
            RH.InsertColumn(TR21, TR21_04, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR21.Style.Add("background-color", "#fff9e5")
            TR21.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR21)

            Dim TR22 As New TableRow
            TR22.BorderStyle = BorderStyle.Solid
            TR22.BackColor = Drawing.Color.WhiteSmoke
            Dim TR22_01, TR22_02, TR22_03, TR22_04 As New TableCell

            RH.InsertColumn(TR22, TR22_01, 40, WholeHelper.ClsRepCtrl.Allign.Left, "1-2 days")
            RH.InsertColumn(TR22, TR22_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(8))
            RH.InsertColumn(TR22, TR22_03, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(11))
            RH.InsertColumn(TR22, TR22_04, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR22.Style.Add("background-color", "#fff9e5")
            TR22.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR22)

            Dim TR23 As New TableRow
            TR23.BorderStyle = BorderStyle.Solid
            TR23.BackColor = Drawing.Color.WhiteSmoke
            Dim TR23_01, TR23_02, TR23_03, TR23_04 As New TableCell

            RH.InsertColumn(TR23, TR23_01, 40, WholeHelper.ClsRepCtrl.Allign.Left, "> 2 days (greater than 2 days)")
            RH.InsertColumn(TR23, TR23_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(9))
            RH.InsertColumn(TR23, TR23_03, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(12))
            RH.InsertColumn(TR23, TR23_04, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR23.Style.Add("background-color", "#fff9e5")
            TR23.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR23)

            Dim TR24 As New TableRow
            TR24.BorderStyle = BorderStyle.Solid
            TR24.BackColor = Drawing.Color.WhiteSmoke
            Dim TR24_01, TR24_02, TR24_03 As New TableCell

            RH.InsertColumn(TR24, TR24_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "<font size='3'><b>CPU ACTIVITY FLOW</b></font>")
            RH.InsertColumn(TR24, TR24_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR24, TR24_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR24.Style.Add("background-color", "#ffb3b3")
            TR24.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR24)

            Dim TR25 As New TableRow
            TR25.BorderStyle = BorderStyle.Solid
            TR25.BackColor = Drawing.Color.WhiteSmoke
            Dim TR25_01, TR25_02, TR25_03 As New TableCell

            RH.InsertColumn(TR25, TR25_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Opening")
            RH.InsertColumn(TR25, TR25_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(1))
            RH.InsertColumn(TR25, TR25_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR25.Style.Add("background-color", "#fff9e5")
            TR25.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR25)

            Dim TR26 As New TableRow
            TR26.BorderStyle = BorderStyle.Solid
            TR26.BackColor = Drawing.Color.WhiteSmoke
            Dim TR26_01, TR26_02, TR26_03 As New TableCell

            RH.InsertColumn(TR26, TR26_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Tickets Assigned to the team for the day")
            RH.InsertColumn(TR26, TR26_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(2))
            RH.InsertColumn(TR26, TR26_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR26.Style.Add("background-color", "#fff9e5")
            TR26.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR26)

            Dim TR27 As New TableRow
            TR27.BorderStyle = BorderStyle.Solid
            TR27.BackColor = Drawing.Color.WhiteSmoke
            Dim TR27_01, TR27_02, TR27_03 As New TableCell

            Dim CpuTotTicket As Integer = Math.Abs(DT.Rows(1)(1)) + Math.Abs(DT.Rows(1)(2))
            RH.InsertColumn(TR27, TR27_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Total Tickets")
            RH.InsertColumn(TR27, TR27_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, CpuTotTicket)
            RH.InsertColumn(TR27, TR27_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR27.Style.Add("background-color", "#bfafa")
            TR27.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR27)

            Dim TR28 As New TableRow
            TR28.BorderStyle = BorderStyle.Solid
            TR28.BackColor = Drawing.Color.WhiteSmoke
            Dim TR28_01, TR28_02, TR28_03 As New TableCell

            Dim TotInCredit As Integer = 0
            If CInt(DT.Rows(1)(3)) = CInt(DT.Rows(1)(13)) Then
                TotInCredit = CInt(DT.Rows(1)(13))
            End If

            RH.InsertColumn(TR28, TR28_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Tickets 'In Progress' To Credit Team")
            RH.InsertColumn(TR28, TR28_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, CInt(DT.Rows(1)(13)))
            RH.InsertColumn(TR28, TR28_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR28.Style.Add("background-color", "#fff9e5")
            TR28.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR28)

            Dim TROth29 As New TableRow
            TROth29.BorderStyle = BorderStyle.Solid
            TROth29.BackColor = Drawing.Color.WhiteSmoke
            Dim TROth29_01, TROth29_02, TROth29_03 As New TableCell

            Dim CpuInOther As Integer = Math.Abs(CInt(DT.Rows(1)(3))) - Math.Abs(CInt(DT.Rows(1)(13)))
            RH.InsertColumn(TROth29, TROth29_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Tickets 'In Progress' To Other Team")
            RH.InsertColumn(TROth29, TROth29_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, CpuInOther)
            RH.InsertColumn(TROth29, TROth29_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TROth29.Style.Add("background-color", "#fff9e5")
            TROth29.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TROth29)

            Dim TRResol29 As New TableRow
            TRResol29.BorderStyle = BorderStyle.Solid
            TRResol29.BackColor = Drawing.Color.WhiteSmoke
            Dim TRResol29_01, TRResol29_02, TRResol29_03 As New TableCell

            RH.InsertColumn(TRResol29, TRResol29_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Tickets Resolved For the Day")
            RH.InsertColumn(TRResol29, TRResol29_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(5))
            RH.InsertColumn(TRResol29, TRResol29_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TRResol29.Style.Add("background-color", "#fff9e5")
            TRResol29.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TRResol29)

            Dim TRRet29 As New TableRow
            TRRet29.BorderStyle = BorderStyle.Solid
            TRRet29.BackColor = Drawing.Color.WhiteSmoke
            Dim TRRet29_01, TRRet29_02, TRRet29_03 As New TableCell

            RH.InsertColumn(TRRet29, TRRet29_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Tickets Returned For the Day")
            RH.InsertColumn(TRRet29, TRRet29_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(4))
            RH.InsertColumn(TRRet29, TRRet29_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TRRet29.Style.Add("background-color", "#fff9e5")
            TRRet29.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TRRet29)

            Dim TRO29 As New TableRow
            TRO29.BorderStyle = BorderStyle.Solid
            TRO29.BackColor = Drawing.Color.WhiteSmoke
            Dim TRO29_01, TRO29_02, TRO29_03 As New TableCell

            RH.InsertColumn(TRO29, TRO29_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Resolved By Other Team")
            RH.InsertColumn(TRO29, TRO29_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(1)(14))
            RH.InsertColumn(TRO29, TRO29_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TRO29.Style.Add("background-color", "#fff9e5")
            TRO29.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TRO29)

            Dim TR29 As New TableRow
            TR29.BorderStyle = BorderStyle.Solid
            TR29.BackColor = Drawing.Color.WhiteSmoke
            Dim TR29_01, TR29_02, TR29_03 As New TableCell

            Dim CpuClose As Integer = Math.Abs(CInt(CpuTotTicket)) - (Math.Abs(CInt(DT.Rows(1)(13))) + CpuInOther + Math.Abs(CInt(DT.Rows(1)(5))) + Math.Abs(CInt(DT.Rows(1)(4))) + Math.Abs(CInt(DT.Rows(1)(14))))
            RH.InsertColumn(TR29, TR29_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Closing Balance")
            RH.InsertColumn(TR29, TR29_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, CpuClose)
            RH.InsertColumn(TR29, TR29_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR29.Style.Add("background-color", "#bfafa")
            TR29.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR29)

            Dim TR30 As New TableRow
            TR30.BorderStyle = BorderStyle.Solid
            TR30.BackColor = Drawing.Color.WhiteSmoke
            Dim TR30_01, TR30_02, TR30_03 As New TableCell

            RH.InsertColumn(TR30, TR30_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "<font size='3'><b>CREDIT ACTIVITY FLOW</b></font>")
            RH.InsertColumn(TR30, TR30_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")
            RH.InsertColumn(TR30, TR30_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR30.Style.Add("background-color", "#ffb3b3")
            TR30.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR30)

            Dim TR31 As New TableRow
            TR31.BorderStyle = BorderStyle.Solid
            TR31.BackColor = Drawing.Color.WhiteSmoke
            Dim TR31_01, TR31_02, TR31_03 As New TableCell

            RH.InsertColumn(TR31, TR31_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Opening")
            RH.InsertColumn(TR31, TR31_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(1))
            RH.InsertColumn(TR31, TR31_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR31.Style.Add("background-color", "#fff9e5")
            TR31.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR31)

            Dim TR32 As New TableRow
            TR32.BorderStyle = BorderStyle.Solid
            TR32.BackColor = Drawing.Color.WhiteSmoke
            Dim TR32_01, TR32_02, TR32_03 As New TableCell

            RH.InsertColumn(TR32, TR32_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Tickets Assigned to the team for the day")
            RH.InsertColumn(TR32, TR32_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(2))
            RH.InsertColumn(TR32, TR32_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR32.Style.Add("background-color", "#fff9e5")
            TR32.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR32)

            Dim TR33 As New TableRow
            TR33.BorderStyle = BorderStyle.Solid
            TR33.BackColor = Drawing.Color.WhiteSmoke
            Dim TR33_01, TR33_02, TR33_03 As New TableCell

            Dim CreditTotTicket As Integer = Math.Abs(CInt(DT.Rows(0)(1))) + Math.Abs(CInt(DT.Rows(0)(2)))
            RH.InsertColumn(TR33, TR33_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Total Tickets")
            RH.InsertColumn(TR33, TR33_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, CreditTotTicket)
            RH.InsertColumn(TR33, TR33_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR33.Style.Add("background-color", "#bfafa")
            TR33.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR33)

            Dim TR34 As New TableRow
            TR34.BorderStyle = BorderStyle.Solid
            TR34.BackColor = Drawing.Color.WhiteSmoke
            Dim TR34_01, TR34_02, TR34_03 As New TableCell

            RH.InsertColumn(TR34, TR34_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Tickets 'In Progress' to CPU team")
            RH.InsertColumn(TR34, TR34_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(13))
            RH.InsertColumn(TR34, TR34_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR34.Style.Add("background-color", "#fff9e5")
            TR34.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR34)

            '--------
            Dim TROth34 As New TableRow
            TROth34.BorderStyle = BorderStyle.Solid
            TROth34.BackColor = Drawing.Color.WhiteSmoke
            Dim TROth34_01, TROth34_02, TROth34_03 As New TableCell

            Dim CreditInOther As Integer = Math.Abs(CInt(DT.Rows(0)(3))) - Math.Abs(CInt(DT.Rows(0)(13)))
            RH.InsertColumn(TROth34, TROth34_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Tickets 'In Progress' To Other Team")
            RH.InsertColumn(TROth34, TROth34_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, CreditInOther)
            RH.InsertColumn(TROth34, TROth34_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TROth34.Style.Add("background-color", "#fff9e5")
            TROth34.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TROth34)

            Dim TRResol34 As New TableRow
            TRResol34.BorderStyle = BorderStyle.Solid
            TRResol34.BackColor = Drawing.Color.WhiteSmoke
            Dim TRResol34_01, TRResol34_02, TRResol34_03 As New TableCell

            RH.InsertColumn(TRResol34, TRResol34_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Tickets Resolved For the Day")
            RH.InsertColumn(TRResol34, TRResol34_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(5))
            RH.InsertColumn(TRResol34, TRResol34_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TRResol34.Style.Add("background-color", "#fff9e5")
            TRResol34.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TRResol34)

            Dim TRRet34 As New TableRow
            TRRet34.BorderStyle = BorderStyle.Solid
            TRRet34.BackColor = Drawing.Color.WhiteSmoke
            Dim TRRet34_01, TRRet34_02, TRRet34_03 As New TableCell

            RH.InsertColumn(TRRet34, TRRet34_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Tickets Returned For the Day")
            RH.InsertColumn(TRRet34, TRRet34_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(4))
            RH.InsertColumn(TRRet34, TRRet34_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TRRet34.Style.Add("background-color", "#fff9e5")
            TRRet34.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TRRet34)

            Dim TRO34 As New TableRow
            TRO34.BorderStyle = BorderStyle.Solid
            TRO34.BackColor = Drawing.Color.WhiteSmoke
            Dim TRO34_01, TRO34_02, TRO34_03 As New TableCell

            RH.InsertColumn(TRO34, TRO34_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Resolved By Other Team")
            RH.InsertColumn(TRO34, TRO34_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, DT.Rows(0)(14))
            RH.InsertColumn(TRO34, TRO34_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TRO34.Style.Add("background-color", "#fff9e5")
            TRO34.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TRO34)
            '--------

            Dim TR35 As New TableRow
            TR35.BorderStyle = BorderStyle.Solid
            TR35.BackColor = Drawing.Color.WhiteSmoke
            Dim TR35_01, TR35_02, TR35_03 As New TableCell

            Dim CreditClose As Integer = Math.Abs(CInt(CreditTotTicket)) - (Math.Abs(CInt(DT.Rows(0)(13))) + CreditInOther + Math.Abs(CInt(DT.Rows(0)(5))) + Math.Abs(CInt(DT.Rows(0)(4))) + Math.Abs(CInt(DT.Rows(0)(14))))
            RH.InsertColumn(TR35, TR35_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "Closing Balance")
            RH.InsertColumn(TR35, TR35_02, 20, WholeHelper.ClsRepCtrl.Allign.Right, CreditClose)
            RH.InsertColumn(TR35, TR35_03, 20, WholeHelper.ClsRepCtrl.Allign.Left, "")

            TR35.Style.Add("background-color", "#bfafa")
            TR35.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR35)

            Dim TR36 As New TableRow
            TR36.BorderStyle = BorderStyle.Solid
            TR36.BackColor = Drawing.Color.WhiteSmoke
            Dim TR36_01, TR36_02 As New TableCell

            DT1 = DB.ExecuteDataSet("select dbo.udfProperCase(emp_name) from EMP_MASTER where emp_code=" & Session("UserID") & "").Tables(0)

            RH.InsertColumn(TR36, TR36_01, 60, WholeHelper.ClsRepCtrl.Allign.Left, "<FONT size='3'><B>Checked By</B></FONT>")
            RH.InsertColumn(TR36, TR36_02, 40, WholeHelper.ClsRepCtrl.Allign.Left, "<FONT size='3'><B>" & DT1.Rows(0)(0) & "</B></FONT>")

            TR36.Style.Add("background-color", "#c2d1ff")
            TR36.ForeColor = Drawing.Color.Black
            tb.Controls.Add(TR36)

            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    'Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
    '    Response.ContentType = "application/pdf"
    '    Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
    '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '    Dim sw As New StringWriter()
    '    Dim hw As New HtmlTextWriter(sw)
    '    pnDisplay.RenderControl(hw)

    '    Dim sr As New StringReader(sw.ToString())
    '    Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
    '    Dim htmlparser As New HTMLWorker(pdfDoc)
    '    PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
    '    pdfDoc.Open()
    '    htmlparser.Parse(sr)
    '    pdfDoc.Close()
    '    Response.Write(pdfDoc)
    '    Response.[End]()
    'End Sub

    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        Try
            WebTools.ExporttoExcel(DT1, "Security Objective")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
