﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf

Partial Class HelpDesk_Reports_ViewCategorywiseSubReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 111) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            Dim DT As New DataTable

            Dim SubID As Integer = GF.Decrypt(Request.QueryString.Get("SubID"))
            Dim SubName As String = GF.Decrypt(Request.QueryString.Get("SubName"))
            Dim FromDt As String = Request.QueryString.Get("FromDt")
            Dim ToDt As String = Request.QueryString.Get("ToDt")
            Dim Type As String = GF.Decrypt(Request.QueryString.Get("Type"))
            Dim SubCat As Integer = CInt(Request.QueryString.Get("SubCat"))

            Dim SqlStr As String

            If SubCat = 1 Then 'Opening Balance
                RH.Heading(Session("FirmName"), tb, "Opening Balance Of " & SubName, 150)
                SqlStr = "select r.TICKET_NO,r.BRANCH_ID,b.Branch_Name,e.Emp_Name + ' (' + CONVERT(varchar(10),r.USER_ID) +')'  requestBy,r.REQUEST_DT,r.REMARKS,d.STATUS_NAME,r.CLOSE_DT,case when r.STATUS_ID in(0,4) then DATEDIFF(day,r.REQUEST_DT,r.CLOSE_DT) else DATEDIFF(day,r.REQUEST_DT,GETDATE()) end Pending,t.TEAM_NAME from HD_REQUEST_MASTER r left outer join HD_TEAM_MASTER t on r.TEAM_ID=t.TEAM_ID,HD_PROBLEM_TYPE p ,HD_SUB_GROUP s , HD_STATUS d, BRANCH_MASTER b, EMP_MASTER e where r.PROBLEM_ID= p.PROBLEM_ID and  p.Sub_Group_ID=s.SUB_GROUP_ID and DATEADD(day,DATEDIFF(day, 0, r.REQUEST_DT ),0) <DATEADD(day,DATEDIFF(day, 0, '" & FromDt & "' ),0) and  (r.STATUS_ID not in(0,4,11,13) or (r.status_id in(0,4) and DATEADD(day,DATEDIFF(day, 0, r.CLOSE_DT ),0) >=DATEADD(day,DATEDIFF(day, 0, '" & FromDt & "' ),0))) and r.STATUS_ID=d.STATUS_ID and r.BRANCH_ID=b.Branch_ID and r.USER_ID=e.Emp_Code " & Type & " and s.Sub_Group_ID= " & SubID & " order by 2"
            ElseIf SubCat = 2 Then 'For The Period Logged (How Much Ticket)
                RH.Heading(Session("FirmName"), tb, "For The Period Logged " & SubName, 150)
                SqlStr = "select r.TICKET_NO,r.BRANCH_ID,b.Branch_Name,e.Emp_Name + ' (' + CONVERT(varchar(10),r.USER_ID) +')'  requestBy,r.REQUEST_DT,r.REMARKS,d.STATUS_NAME,r.CLOSE_DT,case when r.STATUS_ID in(0,4) then DATEDIFF(day,r.REQUEST_DT,r.CLOSE_DT) else DATEDIFF(day,r.REQUEST_DT,GETDATE()) end Pending,t.TEAM_NAME from HD_REQUEST_MASTER r left outer join HD_TEAM_MASTER t on r.TEAM_ID=t.TEAM_ID,HD_PROBLEM_TYPE p ,HD_SUB_GROUP s , HD_STATUS d, BRANCH_MASTER b, EMP_MASTER e where r.PROBLEM_ID= p.PROBLEM_ID and  p.Sub_Group_ID=s.SUB_GROUP_ID and DATEADD(day,DATEDIFF(day, 0, r.REQUEST_DT ),0) <=DATEADD(day,DATEDIFF(day, 0, '" & ToDt & "' ),0) and DATEADD(day,DATEDIFF(day, 0, r.REQUEST_DT ),0) >=DATEADD(day,DATEDIFF(day, 0, '" & FromDt & "' ),0) and r.STATUS_ID=d.STATUS_ID and r.BRANCH_ID=b.Branch_ID and r.USER_ID=e.Emp_Code " & Type & " and s.Sub_Group_ID= " & SubID & " order by 2"
            ElseIf SubCat = 3 Then 'For The Period Closed
                RH.Heading(Session("FirmName"), tb, "For The Period Closed " & SubName, 150)
                SqlStr = "select r.TICKET_NO,r.BRANCH_ID,b.Branch_Name,e.Emp_Name + ' (' + CONVERT(varchar(10),r.USER_ID) +')'  requestBy,r.REQUEST_DT,r.REMARKS,d.STATUS_NAME,r.CLOSE_DT,case when r.STATUS_ID in(0,4) then DATEDIFF(day,r.REQUEST_DT,r.CLOSE_DT) else DATEDIFF(day,r.REQUEST_DT,GETDATE()) end Pending,t.TEAM_NAME from HD_REQUEST_MASTER r left outer join HD_TEAM_MASTER t on r.TEAM_ID=t.TEAM_ID,HD_PROBLEM_TYPE p ,HD_SUB_GROUP s , HD_STATUS d, BRANCH_MASTER b, EMP_MASTER e where r.PROBLEM_ID= p.PROBLEM_ID and  p.Sub_Group_ID=s.SUB_GROUP_ID and DATEADD(day,DATEDIFF(day, 0, r.close_dt ),0) <=DATEADD(day,DATEDIFF(day, 0,'" & ToDt & "' ),0) and DATEADD(day,DATEDIFF(day, 0, r.close_dt ),0) >=DATEADD(day,DATEDIFF(day, 0,'" & FromDt & "'  ),0) and r.STATUS_ID in(0,4) and r.STATUS_ID=d.STATUS_ID and r.BRANCH_ID=b.Branch_ID and r.USER_ID=e.Emp_Code " & Type & " and s.Sub_Group_ID= " & SubID & " order by 2"
            ElseIf SubCat = 4 Then 'Same Day Conversion
                RH.Heading(Session("FirmName"), tb, SubName & " Same Day Conversion", 150)
                SqlStr = "select r.TICKET_NO,r.BRANCH_ID,b.Branch_Name,e.Emp_Name + ' (' + CONVERT(varchar(10),r.USER_ID) +')'  requestBy,r.REQUEST_DT,r.REMARKS,d.STATUS_NAME,r.CLOSE_DT,case when r.STATUS_ID in(0,4) then DATEDIFF(day,r.REQUEST_DT,r.CLOSE_DT) else DATEDIFF(day,r.REQUEST_DT,GETDATE()) end Pending,t.TEAM_NAME from HD_REQUEST_MASTER r left outer join HD_TEAM_MASTER t on r.TEAM_ID=t.TEAM_ID,HD_PROBLEM_TYPE p ,HD_SUB_GROUP s , HD_STATUS d, BRANCH_MASTER b, EMP_MASTER e where r.PROBLEM_ID= p.PROBLEM_ID and  p.Sub_Group_ID=s.SUB_GROUP_ID and DATEADD(day,DATEDIFF(day, 0, r.REQUEST_DT ),0)=DATEADD(day,DATEDIFF(day, 0, r.CLOSE_DT ),0) and r.STATUS_ID in(0,4) and DATEADD(day,DATEDIFF(day, 0, r.REQUEST_DT ),0) <=DATEADD(day,DATEDIFF(day, 0, '" & ToDt & "' ),0) and	DATEADD(day,DATEDIFF(day, 0, r.REQUEST_DT ),0) >=DATEADD(day,DATEDIFF(day, 0, '" & FromDt & "' ),0) and r.STATUS_ID=d.STATUS_ID and r.BRANCH_ID=b.Branch_ID and r.USER_ID=e.Emp_Code " & Type & " and s.Sub_Group_ID= " & SubID & " order by 2"
            ElseIf SubCat = 5 Then 'For The Period Pending
                RH.Heading(Session("FirmName"), tb, "For The Period Pending " & SubName, 150)
                SqlStr = "select r.TICKET_NO,r.BRANCH_ID,b.Branch_Name,e.Emp_Name + ' (' + CONVERT(varchar(10),r.USER_ID) +')'  requestBy,r.REQUEST_DT,r.REMARKS,d.STATUS_NAME,r.CLOSE_DT,case when r.STATUS_ID in(0,4) then DATEDIFF(day,r.REQUEST_DT,r.CLOSE_DT) else DATEDIFF(day,r.REQUEST_DT,GETDATE()) end Pending,t.TEAM_NAME from HD_REQUEST_MASTER r left outer join HD_TEAM_MASTER t on r.TEAM_ID=t.TEAM_ID,HD_PROBLEM_TYPE p ,HD_SUB_GROUP s , HD_STATUS d, BRANCH_MASTER b, EMP_MASTER e where r.PROBLEM_ID= p.PROBLEM_ID and  p.Sub_Group_ID=s.SUB_GROUP_ID and DATEADD(day,DATEDIFF(day, 0, r.REQUEST_DT ),0) <= DATEADD(day,DATEDIFF(day, 0, '" & ToDt & "' ),0) and  (r.STATUS_ID not in(0,4,11,13) or (r.status_id in(0,4) and DATEADD(day,DATEDIFF(day, 0, r.CLOSE_DT ),0) > DATEADD(day,DATEDIFF(day, 0, '" & ToDt & "' ),0))) and r.STATUS_ID=d.STATUS_ID and r.BRANCH_ID=b.Branch_ID and r.USER_ID=e.Emp_Code " & Type & " and s.Sub_Group_ID= " & SubID & " order by 2"
            End If

            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim b As Integer = DT.Rows.Count
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            '
            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            Dim StrUserNam As String = Nothing

            RH.BlankRow(tb, 4)
            tb.Controls.Add(TRSHead)

            Dim TRHead_1 As New TableRow

            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"
            TRHead_1_08.BorderWidth = "1"
            TRHead_1_09.BorderWidth = "1"

            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver
            TRHead_1_08.BorderColor = Drawing.Color.Silver
            TRHead_1_09.BorderColor = Drawing.Color.Silver

            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid
            TRHead_1_08.BorderStyle = BorderStyle.Solid
            TRHead_1_09.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead_1, TRHead_1_00, 5, 5, "c", "SL No")
            RH.AddColumn(TRHead_1, TRHead_1_01, 20, 20, "l", "Ticket No")
            RH.AddColumn(TRHead_1, TRHead_1_02, 10, 10, "l", "Branch")
            RH.AddColumn(TRHead_1, TRHead_1_03, 15, 15, "l", "Request By")
            RH.AddColumn(TRHead_1, TRHead_1_04, 5, 5, "c", "Request Date")
            RH.AddColumn(TRHead_1, TRHead_1_05, 15, 15, "l", "Remark")
            RH.AddColumn(TRHead_1, TRHead_1_06, 10, 10, "l", "Status")
            RH.AddColumn(TRHead_1, TRHead_1_07, 5, 5, "c", "Close Date")
            RH.AddColumn(TRHead_1, TRHead_1_08, 5, 5, "c", "Pending")
            RH.AddColumn(TRHead_1, TRHead_1_09, 10, 10, "l", "Team")

            tb.Controls.Add(TRHead_1)
            Dim intIndex As Integer = 0

            For Each DR In DT.Rows

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09 As New TableCell
                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid

                intIndex = intIndex + 1

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", intIndex)
                RH.AddColumn(TR3, TR3_01, 20, 20, "l", DR(0).ToString)
                RH.AddColumn(TR3, TR3_02, 10, 10, "l", DR(2).ToString)
                RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(3).ToString)
                RH.AddColumn(TR3, TR3_04, 5, 5, "c", Format(DR(4), "dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_05, 15, 15, "l", DR(5).ToString)
                RH.AddColumn(TR3, TR3_06, 10, 10, "l", DR(6).ToString)
                If IsDBNull(DR(7)) Then
                    RH.AddColumn(TR3, TR3_07, 5, 5, "c", "")
                Else
                    RH.AddColumn(TR3, TR3_07, 5, 5, "c", Format(DR(7), "dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR3, TR3_08, 5, 5, "c", DR(8).ToString)
                If IsDBNull(DR(9)) Then
                    RH.AddColumn(TR3, TR3_09, 10, 10, "l", "")
                Else
                    RH.AddColumn(TR3, TR3_09, 10, 10, "l", DR(9).ToString)
                End If

                If intIndex Mod 2 = 1 Then
                    TR3_00.Style.Add("background-color", "#EEEEFF")
                    TR3_01.Style.Add("background-color", "#EEEEFF")
                    TR3_02.Style.Add("background-color", "#EEEEFF")
                    TR3_03.Style.Add("background-color", "#EEEEFF")
                    TR3_04.Style.Add("background-color", "#EEEEFF")
                    TR3_05.Style.Add("background-color", "#EEEEFF")
                    TR3_06.Style.Add("background-color", "#EEEEFF")
                    TR3_07.Style.Add("background-color", "#EEEEFF")
                    TR3_08.Style.Add("background-color", "#EEEEFF")
                    TR3_09.Style.Add("background-color", "#EEEEFF")
                End If

                tb.Controls.Add(TR3)

            Next

            RH.BlankRow(tb, 15)
            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Report  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub


    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub


End Class
