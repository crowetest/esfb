﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CustomizeTicketStatusRpt.aspx.vb" Inherits="CustomizeTicketStatusRpt" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
	    <link rel="stylesheet" href="../../Style/bootstrap-risk-multiselect.css" type="text/css" />
	    <script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
	    <script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
	    <script type="text/javascript" src="../../Script/bootstrap-multiselect_RISK.js"></script>
        <style type="text/css">
            #Button
            {
                width: 80%;
                height: 20px;
                font-weight: bold;
                line-height: 20px;
                text-align: center;
                border-top-left-radius: 25px;
                border-top-right-radius: 25px;
                border-bottom-left-radius: 25px;
                border-bottom-right-radius: 25px;
                cursor: pointer;
                background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
            }            
            #Button:hover
            {
                background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
            }
            .sub_hdRow
            {
                background-color: #EBCCD6;
                height: 20px;
                font-family: Arial;
                color: #B84D4D;
                font-size: 8.5pt;
                font-weight: bold;
            }
            .style1
            {
                width: 100%;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#chkTeam').multiselect();
                buttonWidth: '500px'
            }); 0               
        </script>
        <script language="javascript" type="text/javascript">
        $(function () {
            $('#chkTeam').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true    
            });
            $('#btnget').click(function () {
                alert($('#chkTeam').val());
            })
        });
        function window_onload() { 
            FillTeam();
            var Team = document.getElementById("<%= hid_USer.ClientID %>").value;         
            var SelectGroup = ""; 
            var rrr = document.getElementById("<%= hdnTeam.ClientID %>").value.split("Ñ");
            for (a = 1; a < rrr.length; a++) {
                var cols = rrr[a].split("ÿ");
                var Val = "";                    
                if (cols[0] == Team) {
                    Val = "selected";
                }                      
                SelectGroup += "<option value=" + cols[0] + " " + Val + " >" + cols[1] + "</option>";
            }                        
            $('#chkTeam').html(''); // clear out old list
            $('#chkTeam').multiselect('destroy');  // tell widget to clear itself
            $('#chkTeam').html(SelectGroup); // add in the new list
            $('#chkTeam').multiselect();   
            if(Team==4)
            {
                $('#chkTeam').multiselect('enable');                            
            }else if(Team == 9 || Team == 1){
                $('#chkTeam').multiselect('enable');  
            }
            else if(document.getElementById("<%= hid_USer.ClientID %>").value==5 || document.getElementById("<%= hid_USer.ClientID %>").value==6){
                $('#chkTeam').multiselect('enable');
            }
            else
            {
                 $('#chkTeam').multiselect('disable');
            }
        }
        function GroupOnChange() {            
            var Groupid = document.getElementById("<%= cmbGroup.ClientID %>").value;           
            var ToData = "1Ø" + Groupid; 
            ToServer(ToData, 1);           
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function FromServer(arg, context) {
            if (context == 1) {
                ComboFill(arg, "<%= cmbSubGroup.ClientID %>");
            }
        }
        function FillTeam() {
            jQuery('#chkTeam').multiselect('dataprovider', []);
            var dropdown2OptionList = [];
            var rrr = document.getElementById("<%= hdnTeam.ClientID %>").value.split("Ñ");
            for (a = 1; a < rrr.length; a++) {
                var cols = rrr[a].split("ÿ");
                dropdown2OptionList.push({
                    'label': cols[1],
                    'value': cols[0]
                });
            }
            jQuery('#chkTeam').multiselect('dataprovider', dropdown2OptionList);
            jQuery('#chkTeam').multiselect({
                includeSelectAllOption: true
            });
        }               
        function GetSelectedTeam() {            
            var SelectedItems = $('#chkTeam').val();
            if (SelectedItems[0] == 'multiselect-all')
            {
                SelectedItems.shift();
            }
            var ItemList = "";
            // Selected Territory
            if (SelectedItems != null) {
                ItemList = SelectedItems.toString();
            } 
        }
        function btnShow_onclick() {
            if(document.getElementById("<%= txtTicketNo.ClientID %>").value ==""){
                if(document.getElementById("<%= txtStartDt.ClientID %>").value !="" && document.getElementById("<%= txtToDt.ClientID %>").value ==""){
                    alert("Select To Date");
                    document.getElementById("<%= txtToDt.ClientID %>").focus();
                }
                if(document.getElementById("<%= txtStartDt.ClientID %>").value =="" && document.getElementById("<%= txtToDt.ClientID %>").value !=""){
                    alert("Select Start Date");
                    document.getElementById("<%= txtStartDt.ClientID %>").focus();
                }
            }
            var TicketNo=document.getElementById("<%= txtTicketNo.ClientID %>").value; 
            var frmdate=document.getElementById("<%= txtStartDt.ClientID %>").value=="" ? 1 : document.getElementById("<%= txtStartDt.ClientID %>").value;
            var todate=document.getElementById("<%= txtToDt.ClientID %>").value=="" ? 1 : document.getElementById("<%= txtToDt.ClientID %>").value;  
            var StatusID=document.getElementById("<%= cmbStatus.ClientID %>").value; 
            var BranchID=document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø"); 
            var GroupID=document.getElementById("<%= cmbGroup.ClientID %>").value; 
            var SubGroupID=document.getElementById("<%= cmbSubGroup.ClientID %>").value; 
            
            var SelectedItems = $('#chkTeam').val();
            if(SelectedItems == null)
            {
                alert("Please Select Team");               
                return false;
            }
            var rows = SelectedItems;
            if (SelectedItems[0] == 'multiselect-all') {
                if(document.getElementById("<%= hid_USer.ClientID %>").value == 4)
                    SelectedItems = "0,";
                else
                    SelectedItems = "";

                for (n = 1; n <= rows.length - 1; n++) {
                    SelectedItems += rows[n];
                    if (n != rows.length - 1)
                        SelectedItems += ',';
                }
            }                                                 
            window.open("ViewCustomizeTicketStatus.aspx?TicketNo="+ btoa(TicketNo) +"&frmdate=" + frmdate + "&todate=" + todate + "&StatusID=" + btoa(StatusID)  +"&BranchID=" +btoa(BranchID[0]) +"&GroupID=" + btoa(GroupID) +"&SubGroupID=" + btoa(SubGroupID) +"&TeamID=" + btoa(SelectedItems), "_blank");                
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }      
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    </html>
    <br />
    <br />
    <div style="width: 80%; margin: 0px auto;">
        <table class="style1" style="width: 100%; top: 350px auto;">
            <tr class="style1">
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Ticket No &nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;
                    <asp:TextBox ID="txtTicketNo" runat="server" Style="font-family: Cambria; font-size: 10pt;"
                        Width="90%" MaxLength="50" />
                </td>
                <td style="text-align: left; margin: 0px auto; width: 20%;">
                </td>
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Status &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;<asp:DropDownList ID="cmbStatus" class="NormalText" Style="text-align: left;"
                        runat="server" Font-Names="Cambria" Width="90%" ForeColor="Black">
                        <asp:ListItem Value="-3"> ALL</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="style1">
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Branch &nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;
                    <asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;" runat="server"
                        Font-Names="Cambria" Width="90%" ForeColor="Black">
                        <asp:ListItem Value="-1"> ALL</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="text-align: left; margin: 0px auto; width: 20%;">
                </td>
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Team &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    <select id='chkTeam' multiple='multiple' style='width: 90%; text-align: center;
                        font-family: Cambria;' onchange='GetSelectedTeam()' name="D1">
                    </select></td>
            </tr>            
            <tr>
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Resolved From date &nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;
                    <asp:TextBox ID="txtStartDt" class="NormalText" runat="server" Width="60%" onkeypress="return false"
                        ReadOnly="True"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtStartDt" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
                <td style="text-align: left; margin: 0px auto; width: 20%;">
                </td>
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Resolved To date &nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;<asp:TextBox ID="txtToDt" class="NormalText" runat="server" Width="60%" onkeypress="return false"
                        ReadOnly="True"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtToDt" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Group&nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;
                    <asp:DropDownList ID="cmbGroup" class="NormalText" Style="text-align: left;" runat="server"
                        Font-Names="Cambria" Width="90%" ForeColor="Black">
                        <asp:ListItem Value="-1"> ALL</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="text-align: left; margin: 0px auto; width: 20%;">
                </td>
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Sub Group &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;<asp:DropDownList ID="cmbSubGroup" class="NormalText" Style="text-align: left;"
                        runat="server" Font-Names="Cambria" Width="90%" ForeColor="Black">
                        <asp:ListItem Value="-1"> ALL</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="5">
                    <br />
                    <br />
                    <input id="btnShow" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                        value="SHOW" onclick="return btnShow_onclick()" />
                    &nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                        value="EXIT" onclick="return btnExit_onclick()" /><asp:HiddenField ID="hid_Items"
                            runat="server" />
                    <asp:HiddenField ID="hid_USer" runat="server" />
                    <asp:HiddenField ID="hid_Value" runat="server" /> 
                    <asp:HiddenField ID="hdnTeam" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
