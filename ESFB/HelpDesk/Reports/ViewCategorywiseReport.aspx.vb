﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewCategorywiseReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 111) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            Dim DT As New DataTable
            Dim Category As String = ""
            Dim class_id As String = GF.Decrypt(Request.QueryString.Get("CategoryID"))
            If class_id = "" Then class_id = "0"
            Dim Type As String = ""
            If class_id <> 0 Then
                Type = " and r.CLASSIFICATION_ID= " & CInt(class_id)
            End If

            Dim FrmDate As String = GF.Decrypt(Request.QueryString.Get("FrmDate"))
            Dim ToDate As String = GF.Decrypt(Request.QueryString.Get("ToDate"))

            DT = DB.ExecuteDataSet("select Classification_Type from HD_CLASSIFICATION  where Type_Id=" & CInt(class_id) & "").Tables(0)
            If DT.Rows.Count > 0 Then
                Category = "-" & DT.Rows(0)(0)
            End If

            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Category", Type), New System.Data.SqlClient.SqlParameter("@FrmDate", CDate(FrmDate)), New System.Data.SqlClient.SqlParameter("@ToDate", CDate(ToDate))}
            DT = DB.ExecuteDataSet("[SP_HD_CATEGORYWISE_RPT_NEW]", Parameters).Tables(0)

            If Category <> "" Then
                RH.Heading(Session("FirmName"), tb, "Categorywise Daily dashboard " & Category, 150)
            Else
                RH.Heading(Session("FirmName"), tb, "Categorywise Daily dashboard ", 150)
            End If

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            RH.BlankRow(tb, 3)

            Dim TRHead_1 As New TableRow
            'TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"

            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver

            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid

            TRHead_1_00.Style.Add("background-color", "#F4A6A6")
            TRHead_1_01.Style.Add("background-color", "#F4A6A6")
            TRHead_1_02.Style.Add("background-color", "#F4A6A6")
            TRHead_1_03.Style.Add("background-color", "#F4A6A6")
            TRHead_1_04.Style.Add("background-color", "#F4A6A6")
            TRHead_1_05.Style.Add("background-color", "#F4A6A6")

            RH.AddColumn(TRHead_1, TRHead_1_00, 50, 50, "l", "Category")
            RH.AddColumn(TRHead_1, TRHead_1_01, 10, 10, "l", "Opening Balance")
            RH.AddColumn(TRHead_1, TRHead_1_02, 10, 10, "l", "ForThePeriod Logged")
            RH.AddColumn(TRHead_1, TRHead_1_03, 10, 10, "l", "ForThePeriod Closed")
            RH.AddColumn(TRHead_1, TRHead_1_04, 10, 10, "l", "Same Day Conversion")
            RH.AddColumn(TRHead_1, TRHead_1_05, 10, 10, "l", "ForThePeriod Pending")

            tb.Controls.Add(TRHead_1)
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid

                If DR(1) = "Grand Total" Then
                    TR3_00.Style.Add("background-color", "#F6B3B3")
                    TR3_01.Style.Add("background-color", "#F6B3B3")
                    TR3_02.Style.Add("background-color", "#F6B3B3")
                    TR3_03.Style.Add("background-color", "#F6B3B3")
                    TR3_04.Style.Add("background-color", "#F6B3B3")
                    TR3_05.Style.Add("background-color", "#F6B3B3")

                    RH.AddColumn(TR3, TR3_00, 50, 50, "l", "<b>" + DR(1).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_01, 10, 10, "l", "<b>" + DR(2).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_02, 10, 10, "l", "<b>" + DR(3).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_03, 10, 10, "l", "<b>" + DR(4).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_04, 10, 10, "l", "<b>" + DR(5).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_05, 10, 10, "l", "<b>" + DR(6).ToString() + "</b>")

                Else
                    TR3_00.Style.Add("background-color", "#FAD9D9")
                    TR3_01.Style.Add("background-color", "#FAD9D9")
                    TR3_02.Style.Add("background-color", "#FAD9D9")
                    TR3_03.Style.Add("background-color", "#FAD9D9")
                    TR3_04.Style.Add("background-color", "#FAD9D9")
                    TR3_05.Style.Add("background-color", "#FAD9D9")

                    RH.AddColumn(TR3, TR3_00, 50, 50, "l", DR(1).ToString())
                    If CInt(DR(2)) = 0 Then
                        RH.AddColumn(TR3, TR3_01, 10, 10, "l", DR(2).ToString())
                    Else
                        RH.AddColumn(TR3, TR3_01, 10, 10, "l", "<a href='ViewCategorywiseSubReport.aspx?SubID=" & GF.Encrypt(DR(0).ToString) & " &SubName=" & GF.Encrypt(DR(1).ToString) & " &FromDt=" & FrmDate & " &ToDt=" & ToDate & " &Type=" & GF.Encrypt(Type) & " &SubCat=" & 1 & "' title='View Opening Balance Details' style='text-align:right;' target='_blank' >" & DR(2).ToString() & "</a>")
                    End If

                    If CInt(DR(3)) = 0 Then
                        RH.AddColumn(TR3, TR3_02, 10, 10, "l", DR(3).ToString())
                    Else
                        RH.AddColumn(TR3, TR3_02, 10, 10, "l", "<a href='ViewCategorywiseSubReport.aspx?SubID=" & GF.Encrypt(DR(0).ToString) & " &SubName=" & GF.Encrypt(DR(1).ToString) & " &FromDt=" & FrmDate & " &ToDt=" & ToDate & " &Type=" & GF.Encrypt(Type) & " &SubCat=" & 2 & "' title='View For The Period Logged Details' style='text-align:right;' target='_blank' >" & DR(3).ToString() & "</a>")
                    End If

                    If CInt(DR(3)) = 0 Then
                        RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(4).ToString())
                    Else
                        RH.AddColumn(TR3, TR3_03, 10, 10, "l", "<a href='ViewCategorywiseSubReport.aspx?SubID=" & GF.Encrypt(DR(0).ToString) & " &SubName=" & GF.Encrypt(DR(1).ToString) & " &FromDt=" & FrmDate & " &ToDt=" & ToDate & " &Type=" & GF.Encrypt(Type) & " &SubCat=" & 3 & "' title='View For The Period Closed Details' style='text-align:right;' target='_blank' >" & DR(4).ToString() & "</a>")
                    End If

                    If CInt(DR(5)) = 0 Then
                        RH.AddColumn(TR3, TR3_04, 10, 10, "l", DR(5).ToString())
                    Else
                        RH.AddColumn(TR3, TR3_04, 10, 10, "l", "<a href='ViewCategorywiseSubReport.aspx?SubID=" & GF.Encrypt(DR(0).ToString) & " &SubName=" & GF.Encrypt(DR(1).ToString) & " &FromDt=" & FrmDate & " &ToDt=" & ToDate & " &Type=" & GF.Encrypt(Type) & " &SubCat=" & 4 & "' title='View Same Day Conversion Details' style='text-align:right;' target='_blank' >" & DR(5).ToString() & "</a>")
                    End If

                    If CInt(DR(6)) = 0 Then
                        RH.AddColumn(TR3, TR3_05, 10, 10, "l", DR(6).ToString())
                    Else
                        RH.AddColumn(TR3, TR3_05, 10, 10, "l", "<a href='ViewCategorywiseSubReport.aspx?SubID=" & GF.Encrypt(DR(0).ToString) & " &SubName=" & GF.Encrypt(DR(1).ToString) & " &FromDt=" & FrmDate & " &ToDt=" & ToDate & " &Type=" & GF.Encrypt(Type) & " &SubCat=" & 5 & "' title='View For The Period Pending Details' style='text-align:right;' target='_blank' >" & DR(6).ToString() & "</a>")
                    End If

                End If

                tb.Controls.Add(TR3)
            Next
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
