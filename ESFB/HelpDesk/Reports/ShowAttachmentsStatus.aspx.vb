﻿Imports System.Data

Partial Class ShowAttachmentsStatus
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim RequestID As Integer = CInt(GF.Decrypt(Request.QueryString.[Get]("RequestID")))
            Dim dt As DataTable = DB.ExecuteDataSet("SELECT PKID,ATTACHMENT,content_Type,attach_file_name,(case when status_id=0 then 'Removed' else '' end)as Stat FROM DMS_ESFB.dbo.HD_REQUEST_ATTACH where Request_Id=" & RequestID & "").Tables(0)
            If dt IsNot Nothing Then
                Dim tb As New Table
                tb.Attributes.Add("width", "100%")
                For Each DR As DataRow In dt.Rows
                    Dim TRHead As New TableRow
                    Dim TRHead_00 As New TableCell
                    RH.AddColumn(TRHead, TRHead_00, 100, 100, "l", "<a href='ShowAttachment.aspx?RequestID=" + GF.Encrypt(DR(0).ToString()) + " &RptID=2 ' style='cursor:pointer; font-size:12pt;'>" + DR(3) + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + DR(4).ToString() + "")
                    tb.Controls.Add(TRHead)
                Next
                pnDisplay.Controls.Add(tb)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
