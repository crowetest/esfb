﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewTicketDetails
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim RequestID As String
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RequestID = GN.Decrypt(Request.QueryString.Get("RequestID"))
            Dim DT As New DataTable
            Dim SqlStr As String
            SqlStr = "select a.TICKET_NO ,a.REQUEST_DT ,b.Branch_Name,c.Department_Name ,a.REMARKS ,d.Emp_Name,isnull(a.contact_no,''),isnull(a.email_Id,''),ISNULL(f.cnt,0)as AttachFlag " & _
                " from HD_REQUEST_MASTER  a left join (select request_id,count(*)as cnt from  DMS_ESFB.dbo.HD_REQUEST_ATTACH group by request_id)f  on a.REQUEST_ID=f.REQUEST_ID,BRANCH_MASTER  b,DEPARTMENT_MASTER c,EMP_MASTER d where a.BRANCH_ID =b.Branch_ID and a.DEPARTMENT_ID=c.Department_ID " & _
                " and a.USER_ID=d.Emp_Code  and a.request_id=" + RequestID.ToString()

            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            RH.Heading(Session("FirmName"), tb, "Details Of Ticket  -" + DT.Rows(0)(0).ToString(), 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 15, 15, "l", "Ticket No")
            RH.AddColumn(TRHead, TRHead_01, 12, 12, "l", "Request Date")
            RH.AddColumn(TRHead, TRHead_02, 15, 15, "l", "Requested By")
            RH.AddColumn(TRHead, TRHead_03, 15, 15, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_04, 15, 15, "l", "Department")
            RH.AddColumn(TRHead, TRHead_05, 12, 12, "l", "Contact No")
            RH.AddColumn(TRHead, TRHead_06, 13, 13, "l", "Contact Email")

            tb.Controls.Add(TRHead)

            'RH.BlankRow(tb, 3)
            Dim i As Integer = 0
            Dim Remarks As String = ""
            For Each DR In DT.Rows


                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid



                RH.AddColumn(TR3, TR3_00, 15, 15, "l", DR(0))
                RH.AddColumn(TR3, TR3_01, 12, 12, "l", CDate(DR(1)).ToString("dd/MM/yyyy"))
                RH.AddColumn(TR3, TR3_02, 15, 15, "l", DR(5))
                RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(2))
                RH.AddColumn(TR3, TR3_04, 15, 15, "l", DR(3))
                RH.AddColumn(TR3, TR3_05, 12, 12, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_06, 13, 13, "l", DR(7).ToString())

                tb.Controls.Add(TR3)
                Remarks = DR(4)

            Next

            RH.BlankRow(tb, 4)

            Dim TRRemark As New TableRow
            'TRRemark.BorderWidth = "1"
            'TRRemark.BorderStyle = BorderStyle.Solid
            Dim TRRemark_00, TRRemark_01, TRRemark_02 As New TableCell
            'TRRemark_00.BorderWidth = "1"
            'TRRemark_00.BorderColor = Drawing.Color.Silver
            'TRRemark_00.BorderStyle = BorderStyle.Solid
            'TRRemark_01.BorderWidth = "1"
            'TRRemark_01.BorderColor = Drawing.Color.Silver
            'TRRemark_01.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRRemark, TRRemark_00, 15, 15, "l", "Remark ")
            If DT.Rows(0).Item(8) > 0 Then
                RH.AddColumn(TRRemark, TRRemark_01, 70, 70, "l", Remarks)

                RH.AddColumn(TRRemark, TRRemark_02, 15, 15, "l", "<img id='ViewReport' src='../../Image/attchment2.png' onclick='viewReport(" + RequestID.ToString + "," + DT.Rows(0).Item(8).ToString + ")' title='View attachment' style='height:20px; width:20px; cursor:pointer;'/>")

            Else
                RH.AddColumn(TRRemark, TRRemark_01, 85, 85, "l", Remarks)

            End If

            tb.Controls.Add(TRRemark)
            RH.BlankRow(tb, 4)

            Dim TRSHead As New TableRow
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")
            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            RH.AddColumn(TRSHead, TRSHead_00, 100, 100, "c", "TICKET HISTORY")
            tb.Controls.Add(TRSHead)
            RH.BlankRow(tb, 4)
            DT = DB.ExecuteDataSet("select TRA_DT,b.Emp_Name ,c.TEAM_NAME ,a.REMARKS,d.PROBLEM,case when v.Vendor_Name=null Or cast(v.Vendor_Name as varchar(50))='' then 'NA' " & _
                                   " else v.Vendor_Name end as vw ,e.status_name from HD_REQUEST_CYCLE a LEFT JOIN  HD_STATUS e ON a.update_id=e.STATUS_ID left outer join HD_VENDOR_MASTER v on (a.Vendor_ID=v.Vendor_ID),EMP_MASTER b ,HD_TEAM_MASTER c,HD_PROBLEM_TYPE d where a.USER_ID=b.Emp_Code and a.TEAM_ID=c.TEAM_ID and  a. " & _
                                   " PROBLEM_ID=d.PROBLEM_ID and  a.REQUEST_ID=" + RequestID.ToString() + " order by a.ORDER_ID asc ").Tables(0)

            Dim TRHead_1 As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"


            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver


            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead_1, TRHead_1_00, 5, 5, "C", "#")
            RH.AddColumn(TRHead_1, TRHead_1_01, 10, 10, "l", "Date")
            RH.AddColumn(TRHead_1, TRHead_1_02, 12, 12, "l", "User")
            RH.AddColumn(TRHead_1, TRHead_1_03, 15, 15, "l", "Team")
            RH.AddColumn(TRHead_1, TRHead_1_04, 23, 23, "l", "Remarks")
            RH.AddColumn(TRHead_1, TRHead_1_05, 20, 20, "l", "Findings")
            RH.AddColumn(TRHead_1, TRHead_1_06, 15, 15, "l", "Status")

            tb.Controls.Add(TRHead_1)
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                i += 1

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid




                RH.AddColumn(TR3, TR3_00, 5, 5, "C", i.ToString())
                RH.AddColumn(TR3, TR3_01, 10, 10, "l", CDate(DR(0)).ToString("dd/MM/yyyy hh:mm:ss tt"))
                RH.AddColumn(TR3, TR3_02, 12, 12, "l", DR(1))
                If IsDBNull(DR(5)) = True Then
                    RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(2))
                Else
                    If DR(5) = "NA" Then
                        RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(2))
                    Else
                        RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(2) & " (Vendor :" & DR(5) & ")")
                    End If


                End If
                RH.AddColumn(TR3, TR3_04, 23, 23, "l", DR(3))
                RH.AddColumn(TR3, TR3_05, 20, 20, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_06, 15, 15, "l", DR(6).ToString())



                tb.Controls.Add(TR3)
            Next
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
