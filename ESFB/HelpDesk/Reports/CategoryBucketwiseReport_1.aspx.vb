﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class CategoryBucketwiseReport_1
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim RequestID As String
    Dim Group_ID As Integer
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 132) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RequestID = GN.Decrypt(Request.QueryString.Get("ReqID"))
            Group_ID = GN.Decrypt(Request.QueryString.Get("Group_ID"))
            Dim DT As New DataTable
            Dim SQL As String = Nothing


            SQL = "select Sub_Group_ID,Sub_Group_Name,sum(case when x.days = 0 then 1 else 0 end) '0 Days',sum(case when x.days between 1 and 5 then 1 else 0 end) '1-5 Days',sum(case when x.days between 6 and 10 then 1 else 0 end) '6-10 Days',sum(case when x.days between 11 and 15 then 1 else 0 end) '11-15 Days',sum(case when x.days between 16 and 20 then 1 else 0 end) '16-20 Days',sum(case when x.days between 21 and 25 then 1 else 0 end) '21-25 Days',sum(case when x.days between 26 and 30 then 1 else 0 end) '26-30 Days',sum(case when x.days > 30 then 1 else 0 end) 'More 30 Days' from ( select b.Sub_Group_ID,b.Sub_Group_Name,datediff(d,a.REQUEST_DT,GETDATE()) Days from HD_REQUEST_MASTER a,HD_SUB_GROUP b,HD_PROBLEM_TYPE c where b.Group_id=" & Group_ID & " and a.PROBLEM_ID= c.PROBLEM_ID and   c.SUB_GROUP_ID = b.Sub_Group_ID  and a.STATUS_ID not in (0,4,11,13)) x  group by Sub_Group_ID,Sub_Group_Name"

            RH.Heading(Session("FirmName"), tb, "Group Wise Ageing Report", 100)


            DT = DB.ExecuteDataSet(SQL).Tables(0)


            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 4, 4, "l", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 15, 15, "l", "Status")
            RH.AddColumn(TRHead, TRHead_02, 9, 9, "c", "0 Days")
            RH.AddColumn(TRHead, TRHead_03, 9, 9, "c", "1-5 Days")
            RH.AddColumn(TRHead, TRHead_04, 9, 9, "c", "6-10 Days")
            RH.AddColumn(TRHead, TRHead_05, 9, 9, "c", "11-15 Days")
            RH.AddColumn(TRHead, TRHead_06, 9, 9, "c", "16-20 Days")
            RH.AddColumn(TRHead, TRHead_07, 9, 9, "c", "21-25 Days")
            RH.AddColumn(TRHead, TRHead_08, 9, 9, "c", "26-30 Days")
            RH.AddColumn(TRHead, TRHead_09, 9, 9, "c", "More 30 Days")
            RH.AddColumn(TRHead, TRHead_10, 9, 9, "c", "Total")
            tb.Controls.Add(TRHead)

            'RH.BlankRow(tb, 3)
            Dim Tot0 As Integer = 0
            Dim Tot1 As Integer = 0
            Dim Tot2 As Integer = 0
            Dim Tot3 As Integer = 0
            Dim Tot4 As Integer = 0
            Dim Tot5 As Integer = 0
            Dim Tot6 As Integer = 0
            Dim Tot7 As Integer = 0
            Dim Tot8 As Integer = 0
            Dim HTot As Integer = 0
            Dim i As Integer = 0
            Dim Remarks As String = ""
            For Each DR In DT.Rows

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_10.BackColor = Drawing.Color.WhiteSmoke

                i += 1
                HTot = DR(2) + DR(3) + DR(4) + DR(5) + DR(6) + DR(7) + DR(8) + DR(9)
                RH.AddColumn(TR3, TR3_00, 4, 4, "l", i)
                RH.AddColumn(TR3, TR3_01, 15, 15, "l", "<a href='CategoryBucketwiseReport_2.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + " &Sub_Group_ID=" + GN.Encrypt(DR(0).ToString()) + "&ReqID=" + GN.Encrypt(RequestID.ToString()) + "' target='_blank'>" + DR(1).ToString() + "</a>")

                If (DR(2) = 0) Then
                    RH.AddColumn(TR3, TR3_02, 9, 9, "c", 0)
                Else
                    RH.AddColumn(TR3, TR3_02, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt(DR(0).ToString()) + "&ReqID=" + GN.Encrypt("1") + " &BucketID=0' target='_blank'>" + DR(2).ToString() + "</a>")
                End If

                If (DR(3) = 0) Then
                    RH.AddColumn(TR3, TR3_03, 9, 9, "c", 0)
                Else
                    RH.AddColumn(TR3, TR3_03, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt(DR(0).ToString()) + "&ReqID=" + GN.Encrypt("1") + " &BucketID=1 &From=1 &To=5' target='_blank'>" + DR(3).ToString() + "</a>")
                End If

                If (DR(4) = 0) Then
                    RH.AddColumn(TR3, TR3_04, 9, 9, "c", 0)
                Else
                    RH.AddColumn(TR3, TR3_04, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt(DR(0).ToString()) + "&ReqID=" + GN.Encrypt("1") + " &BucketID=1 &From=6 &To=10' target='_blank'>" + DR(4).ToString() + "</a>")
                End If

                If (DR(5) = 0) Then
                    RH.AddColumn(TR3, TR3_05, 9, 9, "c", 0)
                Else
                    RH.AddColumn(TR3, TR3_05, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt(DR(0).ToString()) + "&ReqID=" + GN.Encrypt("1") + " &BucketID=1 &From=11 &To=15' target='_blank'>" + DR(5).ToString() + "</a>")
                End If

                If (DR(6) = 0) Then
                    RH.AddColumn(TR3, TR3_06, 9, 9, "c", 0)
                Else
                    RH.AddColumn(TR3, TR3_06, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt(DR(0).ToString()) + "&ReqID=" + GN.Encrypt("1") + " &BucketID=1 &From=16 &To=20' target='_blank'>" + DR(6).ToString() + "</a>")
                End If

                If (DR(7) = 0) Then
                    RH.AddColumn(TR3, TR3_07, 9, 9, "c", 0)
                Else
                    RH.AddColumn(TR3, TR3_07, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt(DR(0).ToString()) + "&ReqID=" + GN.Encrypt("1") + " &BucketID=1 &From=21 &To=25' target='_blank'>" + DR(7).ToString() + "</a>")
                End If

                If (DR(8) = 0) Then
                    RH.AddColumn(TR3, TR3_08, 9, 9, "c", 0)
                Else
                    RH.AddColumn(TR3, TR3_08, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt(DR(0).ToString()) + "&ReqID=" + GN.Encrypt("1") + " &BucketID=1 &From=26 &To=30' target='_blank'>" + DR(8).ToString() + "</a>")
                End If

                If (DR(9) = 0) Then
                    RH.AddColumn(TR3, TR3_09, 9, 9, "c", 0)
                Else
                    RH.AddColumn(TR3, TR3_09, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt(DR(0).ToString()) + "&ReqID=" + GN.Encrypt("1") + " &BucketID=2' target='_blank'>" + DR(9).ToString() + "</a>")
                End If

                If (HTot = 0) Then
                    RH.AddColumn(TR3, TR3_10, 9, 9, "c", "<b>0</b>")
                Else
                    RH.AddColumn(TR3, TR3_10, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt(DR(0).ToString()) + "&ReqID=" + GN.Encrypt("1") + " &BucketID=3' target='_blank'><b>" + HTot.ToString() + "</b>")
                End If
                Tot0 += DR(2)
                Tot1 += DR(3)
                Tot2 += DR(4)
                Tot3 += DR(5)
                Tot4 += DR(6)
                Tot5 += DR(7)
                Tot6 += DR(8)
                Tot7 += DR(9)
                Tot8 += HTot
                tb.Controls.Add(TR3)
            Next

            RH.BlankRow(tb, 4)

            Dim TR4 As New TableRow
            TR4.BorderWidth = "1"
            TR4.BorderStyle = BorderStyle.Solid
            TR4.Style.Add("font-weight", "bold")
            TR4.Style.Add("background-color", "whitesmoke")

            Dim TR4_00, TR4_01, TR4_02, TR4_03, TR4_04, TR4_05, TR4_06, TR4_07, TR4_08, TR4_09 As New TableCell

            TR4_00.BorderWidth = "1"
            TR4_01.BorderWidth = "1"
            TR4_02.BorderWidth = "1"
            TR4_03.BorderWidth = "1"
            TR4_04.BorderWidth = "1"
            TR4_05.BorderWidth = "1"
            TR4_06.BorderWidth = "1"
            TR4_07.BorderWidth = "1"
            TR4_08.BorderWidth = "1"
            TR4_09.BorderWidth = "1"

            TR4_00.BorderColor = Drawing.Color.Silver
            TR4_01.BorderColor = Drawing.Color.Silver
            TR4_02.BorderColor = Drawing.Color.Silver
            TR4_03.BorderColor = Drawing.Color.Silver
            TR4_04.BorderColor = Drawing.Color.Silver
            TR4_05.BorderColor = Drawing.Color.Silver
            TR4_06.BorderColor = Drawing.Color.Silver
            TR4_07.BorderColor = Drawing.Color.Silver
            TR4_08.BorderColor = Drawing.Color.Silver
            TR4_09.BorderColor = Drawing.Color.Silver

            TR4_00.BorderStyle = BorderStyle.Solid
            TR4_01.BorderStyle = BorderStyle.Solid
            TR4_02.BorderStyle = BorderStyle.Solid
            TR4_03.BorderStyle = BorderStyle.Solid
            TR4_04.BorderStyle = BorderStyle.Solid
            TR4_05.BorderStyle = BorderStyle.Solid
            TR4_06.BorderStyle = BorderStyle.Solid
            TR4_07.BorderStyle = BorderStyle.Solid
            TR4_08.BorderStyle = BorderStyle.Solid
            TR4_09.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR4, TR4_00, 19, 19, "l", "Total")
            If (Tot0 = 0) Then
                RH.AddColumn(TR4, TR4_01, 9, 9, "c", Tot0)
            Else
                RH.AddColumn(TR4, TR4_01, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt("0") + "  &ReqID=" + GN.Encrypt(RequestID.ToString()) + "&BucketID=4  &From=0 &To=0' target='_blank'>" + Tot0.ToString() + "</a>")
            End If
            If (Tot1 = 0) Then
                RH.AddColumn(TR4, TR4_02, 9, 9, "c", Tot1)
            Else
                RH.AddColumn(TR4, TR4_02, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt("0") + "  &ReqID=" + GN.Encrypt(RequestID.ToString()) + "&BucketID=4  &From=1 &To=5' target='_blank'>" + Tot1.ToString() + "</a>")
            End If
            If (Tot2 = 0) Then
                RH.AddColumn(TR4, TR4_03, 9, 9, "c", Tot2)
            Else
                RH.AddColumn(TR4, TR4_03, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt("0") + "   &ReqID=" + GN.Encrypt(RequestID.ToString()) + "&BucketID=4  &From=6 &To=10' target='_blank'>" + Tot2.ToString() + "</a>")
            End If
            If (Tot3 = 0) Then
                RH.AddColumn(TR4, TR4_04, 9, 9, "c", Tot3)
            Else
                RH.AddColumn(TR4, TR4_04, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt("0") + "  &ReqID=" + GN.Encrypt(RequestID.ToString()) + "&BucketID=4  &From=11 &To=15' target='_blank'>" + Tot3.ToString() + "</a>")
            End If
            If (Tot4 = 0) Then
                RH.AddColumn(TR4, TR4_05, 9, 9, "c", Tot4)
            Else
                RH.AddColumn(TR4, TR4_05, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt("0") + "   &ReqID=" + GN.Encrypt(RequestID.ToString()) + "&BucketID=4  &From=16 &To=20' target='_blank'>" + Tot4.ToString() + "</a>")
            End If
            If (Tot5 = 0) Then
                RH.AddColumn(TR4, TR4_06, 9, 9, "c", Tot5)
            Else
                RH.AddColumn(TR4, TR4_06, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt("0") + "   &ReqID=" + GN.Encrypt(RequestID.ToString()) + "&BucketID=4  &From=21 &To=25' target='_blank'>" + Tot5.ToString() + "</a>")
            End If
            If (Tot6 = 0) Then
                RH.AddColumn(TR4, TR4_07, 9, 9, "c", Tot6)
            Else
                RH.AddColumn(TR4, TR4_07, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt("0") + "  &ReqID=" + GN.Encrypt(RequestID.ToString()) + "&BucketID=4  &From=26 &To=30' target='_blank'>" + Tot6.ToString() + "</a>")
            End If
            If (Tot7 = 0) Then
                RH.AddColumn(TR4, TR4_08, 9, 9, "c", Tot7)
            Else
                RH.AddColumn(TR4, TR4_08, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt("0") + "  &ReqID=" + GN.Encrypt(RequestID.ToString()) + "&BucketID=5' target='_blank'>" + Tot7.ToString() + "</a>")
            End If
            If (Tot8 = 0) Then
                RH.AddColumn(TR4, TR4_09, 9, 9, "c", "<b>0</b>")
            Else
                RH.AddColumn(TR4, TR4_09, 9, 9, "c", "<a href='CategoryBucketwiseReport_3.aspx?Group_ID=" + GN.Encrypt(Group_ID.ToString()) + "&Sub_Group_ID=" + GN.Encrypt("0") + "  &ReqID=" + GN.Encrypt(RequestID.ToString()) + "&BucketID=6' target='_blank'>" + "<b>" + Tot8.ToString() + "</b></a>")
            End If
            tb.Controls.Add(TR4)

            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
