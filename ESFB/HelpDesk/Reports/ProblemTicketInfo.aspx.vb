﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ProblemTicketInfo
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim DT As New DataTable
    Dim MainTable, LeftTable, RightTable As New Table
    Dim SQL As String
    Dim ProblemNo As String
    Dim GF As New GeneralFunctions
    Private Sub ActivityLog()
        Try
            SQL = "SELECT A.TRA_DT,B.STATUS,C.Emp_Name FROM HD_PROBLEM_CYCLE A,HD_PROBLEM_STATUS B,EMP_MASTER C WHERE A.PROBLEM_NO = '" & ProblemNo & "' AND A.STATUS_ID = B.STATUS_ID AND A.USER_ID = C.Emp_Code ORDER BY A.TRA_DT"
            DT = DB.ExecuteDataSet(SQL).Tables(0)
            RH.SubHeading(RightTable, 100, "center", "ACTIVITY LOG")
            RH.BlankRow(RightTable, 4)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02 As New TableCell
            RH.AddColumn(TRHead, TRHead_00, 25, 25, "l", "Date")
            RH.AddColumn(TRHead, TRHead_01, 50, 50, "l", "Status")
            RH.AddColumn(TRHead, TRHead_02, 25, 25, "l", "Done By")
            RightTable.Controls.Add(TRHead)
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                Dim TR3_00, TR3_01, TR3_02 As New TableCell
                RH.AddColumn(TR3, TR3_00, 25, 25, "l", CDate(DR(0)).ToString("dd/MM/yyyy"))
                RH.AddColumn(TR3, TR3_01, 50, 50, "l", DR(1))
                RH.AddColumn(TR3, TR3_02, 25, 25, "l", DR(2))
                RightTable.Controls.Add(TR3)
            Next
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Private Sub LeftTableFill(ByVal ProblemNo As String, ByVal HdTicketNo As String, ByVal Problem As String, ByVal CreateDate As String, ByVal Category As String, ByVal Priority As String, ByVal Status As String, ByVal Impact As String, ByVal RootCause As String, ByVal WorkAround As String, ByVal Solution As String, ByVal ReviewDescr As String, ByVal Request_ID As Integer)
        Try
            Dim TR00 As New TableRow
            Dim TR00_1, TR00_2 As New TableCell
            RH.InsertColumn(TR00, TR00_1, 28, 0, "Finding Ticket No")
            RH.InsertColumn(TR00, TR00_2, 72, 0, ProblemNo)
            LeftTable.Controls.Add(TR00)
            Dim TR01 As New TableRow
            Dim TR01_1, TR01_2 As New TableCell
            RH.InsertColumn(TR01, TR01_1, 28, 0, "Helpdesk Ticket No")
            RH.InsertColumn(TR01, TR01_2, 72, 0, "<a href='viewTicketDetails.aspx?RequestID=" + Request_ID.ToString + "' target=_blank>" + HdTicketNo + "</a>")
            LeftTable.Controls.Add(TR01)
            Dim TR02 As New TableRow
            Dim TR02_1, TR02_2 As New TableCell
            RH.InsertColumn(TR02, TR02_1, 28, 0, "Ticket Created On")
            RH.InsertColumn(TR02, TR02_2, 72, 0, CDate(CreateDate).ToString("dd/MM/yyyy"))
            LeftTable.Controls.Add(TR02)
            Dim TR03 As New TableRow
            Dim TR03_1, TR03_2 As New TableCell
            RH.InsertColumn(TR03, TR03_1, 28, 0, "Finding Description")
            RH.InsertColumn(TR03, TR03_2, 72, 0, Problem)
            LeftTable.Controls.Add(TR03)
            Dim TR04 As New TableRow
            Dim TR04_1, TR04_2 As New TableCell
            RH.InsertColumn(TR04, TR04_1, 28, 0, "Category")
            RH.InsertColumn(TR04, TR04_2, 72, 0, Category)
            LeftTable.Controls.Add(TR04)
            Dim TR05 As New TableRow
            Dim TR05_1, TR05_2 As New TableCell
            RH.InsertColumn(TR05, TR05_1, 28, 0, "Priority")
            RH.InsertColumn(TR05, TR05_2, 72, 0, Priority)
            LeftTable.Controls.Add(TR05)
            Dim TR06 As New TableRow
            Dim TR06_1, TR06_2 As New TableCell
            RH.InsertColumn(TR06, TR06_1, 28, 0, "<b>Ticket Status</b>")
            RH.InsertColumn(TR06, TR06_2, 72, 0, "<b>" + Status + "</b>")
            LeftTable.Controls.Add(TR06)
            Dim TR07 As New TableRow
            Dim TR07_1, TR07_2, TR07_3 As New TableCell
            RH.InsertColumn(TR07, TR07_1, 28, 0, "Impact")
            RH.InsertColumn(TR07, TR07_2, 72, 0, Impact)
            LeftTable.Controls.Add(TR07)
            Dim TR08 As New TableRow
            Dim TR08_1, TR08_2 As New TableCell
            RH.InsertColumn(TR08, TR08_1, 28, 0, "Root Cause")
            RH.InsertColumn(TR08, TR08_2, 72, 0, RootCause)
            LeftTable.Controls.Add(TR08)
            Dim TR09 As New TableRow
            Dim TR09_1, TR09_2 As New TableCell
            RH.InsertColumn(TR09, TR09_1, 28, 0, "Work Around")
            RH.InsertColumn(TR09, TR09_2, 72, 0, WorkAround)
            LeftTable.Controls.Add(TR09)
            Dim TR10 As New TableRow
            Dim TR10_1, TR10_2 As New TableCell
            RH.InsertColumn(TR10, TR10_1, 28, 0, "Solution")
            RH.InsertColumn(TR10, TR10_2, 72, 0, Solution)
            LeftTable.Controls.Add(TR10)
            Dim TR21 As New TableRow
            Dim TR21_1, TR21_2 As New TableCell
            RH.InsertColumn(TR21, TR21_1, 28, 0, "Review Description")
            RH.InsertColumn(TR21, TR21_2, 72, 0, ReviewDescr)
            LeftTable.Controls.Add(TR21)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 266) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            ProblemNo = GF.Decrypt(Request.QueryString.Get("ProblemNo"))
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            MainTable.Attributes.Add("width", "100%")
            LeftTable.Attributes.Add("width", "100%")
            RightTable.Attributes.Add("width", "100%")

            SQL = "SELECT A.HD_TICKET_NO,A.CREATE_DT,A.PROBLEM,B.CATEGORY,C.PRIORITY_NAME,D.STATUS+' - '+D.PENDING_STATUS,A.IMPACT,A.ROOT_CAUSE,A.WORK_AROUND,A.SOLUTION,A.REVIEW_DESCR FROM HD_PROBLEM_MASTER A,HD_PROBLEM_CATEGORY B,HD_CHANGE_PRIORITY C,HD_PROBLEM_STATUS D WHERE A.PROBLEM_NO = '" & ProblemNo & "' AND A.CATEGORY_ID = B.CATEGORY_ID AND A.PRIORITY_ID = C.PRIORITY_ID AND A.STATUS_ID = D.STATUS_ID"
            DT = DB.ExecuteDataSet(SQL).Tables(0)
            Dim HdTicketNo As String = CStr(DT.Rows(0)(0))
            Dim CreateDate As String = CStr(DT.Rows(0)(1))
            Dim Problem As String = CStr(DT.Rows(0)(2))
            Dim Category As String = CStr(DT.Rows(0)(3))
            Dim Priority As String = CStr(DT.Rows(0)(4))
            Dim Status As String = CStr(DT.Rows(0)(5))
            Dim Impact As String = CStr(DT.Rows(0)(6))
            Dim RootCause As String = ""
            If Not IsDBNull(DT.Rows(0)(7)) Then
                RootCause = CStr(DT.Rows(0)(7))
            End If
            Dim WorkAround As String = ""
            If Not IsDBNull(DT.Rows(0)(8)) Then
                WorkAround = CStr(DT.Rows(0)(8))
            End If

            Dim Solution As String = ""
            If Not IsDBNull(DT.Rows(0)(9)) Then
                Solution = CStr(DT.Rows(0)(9))
            End If

            Dim ReviewDescr As String = Nothing
            If Not IsDBNull(DT.Rows(0)(10)) Then
                ReviewDescr = CStr(DT.Rows(0)(10))
            End If
            RH.Heading(Session("FirmName"), MainTable, "FINDING TICKET INFORMATION", 100)
            Dim DT_sub As New DataTable
            SQL = "select REQUEST_ID from HD_REQUEST_MASTER where TICKET_NO='" + HdTicketNo + "'"
            DT_sub = DB.ExecuteDataSet(SQL).Tables(0)
            '-- // Fill Left Top Table
            LeftTableFill(ProblemNo, HdTicketNo, Problem, CreateDate, Category, Priority, Status, Impact, RootCause, WorkAround, Solution, ReviewDescr, DT_sub.Rows(0)(0))
            ActivityLog() '-- // Fill Activity Log Table

            Dim MainRow As New TableRow
            Dim LeftCell, RightCell As New TableCell
            RH.InsertColumn(MainRow, LeftCell, 70, 0, "")
            RH.InsertColumn(MainRow, RightCell, 30, 0, "")
            LeftCell.Controls.Add(LeftTable)
            RightCell.Controls.Add(RightTable)
            MainTable.Controls.Add(MainRow)

            pnDisplay.Controls.Add(MainTable)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

#Region "Toolbar"
    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub
    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

End Class
