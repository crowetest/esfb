﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class RPT_ActivityMonitorReport
    Inherits System.Web.UI.Page
    'Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim BranchID As Integer
    Dim State_ID As Integer

    Dim UserID As Integer
    Dim GMASTER As New Master
    Dim CallBackReturn As String = Nothing
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools
    Dim AD As New Audit
    Dim PostID As Integer



#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1222) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim strWhere As String = ""
            Dim strWhere1 As String = ""
            BranchID = CInt(Session("BranchID"))
            UserID = CInt(Session("UserID"))
            Me.Master.subtitle = "RPT Activity Monitor Report"
          
            'Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            'Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
           
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

End Class
