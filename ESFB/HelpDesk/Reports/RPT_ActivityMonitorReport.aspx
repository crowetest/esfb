﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="RPT_ActivityMonitorReport.aspx.vb" Inherits="RPT_ActivityMonitorReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style2
        {
            width: 23%;
        }
    </style>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
         
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnViewOnClick() {           
            var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;  
            var ToDt = document.getElementById("<%= txtToDt.ClientID %>").value; 
            document.getElementById("<%= hidData.ClientID %>").value = FromDt + "ÿ" + ToDt;
            window.open("ViewRPTMonitor.aspx?FromDt=" + btoa(FromDt) + "&ToDt=" + btoa(ToDt), "_blank");                
        }
        
        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 65%; margin: 0px auto;">
       <tr>
       <td style="width: 10%;">
                </td>
             <td style="text-align: right;" class="style2">
             From Date
            </td>
            
            
             <td style="width: 58%">
            &nbsp;
                <asp:TextBox ID="txtFromDt" runat="server" class="NormalText" Width="40%"  
                    ReadOnly="true"></asp:TextBox> <asp:CalendarExtender ID="CALFromDt" Format="dd MMM yyyy"  runat="server" Enabled="True" TargetControlID="txtFromDt"></asp:CalendarExtender>

                </td>
                
        </tr>
       <tr>
       <td style="width: 10%;">
                </td>
             <td style="text-align: right;" class="style2">
             To Date
            </td>
            
            
             <td style="width: 58%">
      &nbsp;
                <asp:TextBox ID="txtToDt" runat="server" class="NormalText" Width="40%"  
                    ReadOnly="true"></asp:TextBox> <asp:CalendarExtender ID="CalendarExtender1" Format="dd MMM yyyy"  runat="server" Enabled="True" TargetControlID="txtToDt"></asp:CalendarExtender>

                </td>
                
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                    value="SHOW" onclick="return btnViewOnClick()" />
                    
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />
            </td>
        </tr>
            <asp:HiddenField ID="hidData" runat="server" />
    </table>
    <br />
    <br />
    <br />
    <br />
</asp:Content>
