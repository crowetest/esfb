﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class TicketStatusRpt
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 109) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Ticket Status Report"
            Me.txtStartDt.Text = CDate(Session("TraDt")).ToString("dd MMM yyyy")
            Me.txtToDt.Text = CDate(Session("TraDt")).ToString("dd MMM yyyy")
            DT = DB.ExecuteDataSet("select status_name,status_id from HD_status  order by pk_id ").Tables(0)
            hid_Items.Value = ""
            Dim i As Integer = 0
            For Each DR As DataRow In DT.Rows
                Dim Strnam As String
                Strnam = DR(0).ToString()
                hid_Items.Value += Strnam + "µ" + DR(1).ToString()
                If i <> DT.Rows.Count - 1 Then
                    hid_Items.Value += "¥"
                End If
                i = i + 1
            Next
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            DT = DB.ExecuteDataSet("select * from hd_team_Dtl where emp_code=" & CInt(Session("UserID")) & " ").Tables(0)
            If DT.Rows.Count = 0 Then
                hid_USer.Value = "1" 'only normal user
            Else
                hid_USer.Value = "0"
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub




End Class
