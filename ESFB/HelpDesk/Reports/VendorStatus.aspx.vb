﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class VendorStatus
    Inherits System.Web.UI.Page
    Dim HD As New HelpDesk
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Vendor Status Report"
            Dim DT As New DataTable
            Dim SqlStr As String
            SqlStr = "select VENDOR_NAME,VENDOR_ID from HD_VENDOR_MASTER where VENDOR_ID<>0 "
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim i As Integer = 0
            For Each DR As DataRow In DT.Rows
                hid_Items.Value += DR(0).ToString() + "µ" + DR(1).ToString()
                If i <> DT.Rows.Count - 1 Then
                    hid_Items.Value += "¥"
                End If
                i = i + 1
            Next


            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed

        DB.dispose()
        GC.Collect()
    End Sub

    
End Class
