﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AgingReport
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim HD As New HelpDesk
    Dim tb As New Table
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 113) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Aging Report"

            DT = DB.ExecuteDataSet("select 0 as TYPE_ID,' --------All---------' as Classification_Type union all select TYPE_ID,Classification_Type from HD_CLASSIFICATION where status_id=1").Tables(0)
            GN.ComboFill(cmbClass, DT, 0, 1)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
End Class
