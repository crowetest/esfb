﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewAgingReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 113) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            Dim DT As New DataTable
            Dim Category As String = ""
            Dim class_id As Integer = CInt(GF.Decrypt(Request.QueryString.Get("CategoryID")))
            Dim FromDt As Date = CDate(GF.Decrypt(Request.QueryString.Get("FromDt")))
            Dim ToDt As Date = CDate(GF.Decrypt(Request.QueryString.Get("ToDt")))
            DT = DB.ExecuteDataSet("select Classification_Type from HD_CLASSIFICATION  where Type_Id=" & CInt(class_id) & "").Tables(0)
            If DT.Rows.Count > 0 Then
                Category = "-" & DT.Rows(0)(0)
            End If
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Days", 1), New System.Data.SqlClient.SqlParameter("@Type", class_id), New System.Data.SqlClient.SqlParameter("@FromDt", FromDt), New System.Data.SqlClient.SqlParameter("@ToDt", ToDt)}
            DT = DB.ExecuteDataSet("[SP_HD_AGING]", Parameters).Tables(0)
            If Category <> "" Then
                RH.Heading(Session("FirmName"), tb, "Agewise Pending -  " & Category, 150)
            ElseIf class_id = -1 Then
                RH.Heading(Session("FirmName"), tb, "Agewise Resolved ", 150)
            Else
                RH.Heading(Session("FirmName"), tb, "Agewise Report ", 150)
            End If


            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")




            RH.BlankRow(tb, 3)


            Dim TRHead_1 As New TableRow
            'TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderStyle = "1"
            TRHead_1_07.BorderStyle = "1"
            TRHead_1_08.BorderStyle = "1"
            TRHead_1_09.BorderStyle = "1"



            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver
            TRHead_1_08.BorderColor = Drawing.Color.Silver
            TRHead_1_09.BorderColor = Drawing.Color.Silver


            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid
            TRHead_1_08.BorderStyle = BorderStyle.Solid
            TRHead_1_09.BorderStyle = BorderStyle.Solid

            TRHead_1_00.Style.Add("background-color", "#F4A6A6")
            TRHead_1_01.Style.Add("background-color", "#F4A6A6")
            TRHead_1_02.Style.Add("background-color", "#F4A6A6")
            TRHead_1_03.Style.Add("background-color", "#F4A6A6")
            TRHead_1_04.Style.Add("background-color", "#F4A6A6")
            TRHead_1_05.Style.Add("background-color", "#F4A6A6")
            TRHead_1_06.Style.Add("background-color", "#F4A6A6")
            TRHead_1_07.Style.Add("background-color", "#F4A6A6")
            TRHead_1_08.Style.Add("background-color", "#F4A6A6")
            TRHead_1_09.Style.Add("background-color", "#F4A6A6")


            RH.AddColumn(TRHead_1, TRHead_1_00, 50, 50, "1", "Category")
            RH.AddColumn(TRHead_1, TRHead_1_01, 15, 15, "l", "0&nbsp;Days")
            RH.AddColumn(TRHead_1, TRHead_1_02, 5, 5, "l", "1&nbsp;to&nbsp;5&nbsp;Days")
            RH.AddColumn(TRHead_1, TRHead_1_03, 5, 5, "l", "6&nbsp;to&nbsp;10&nbsp;Days")
            RH.AddColumn(TRHead_1, TRHead_1_04, 5, 5, "l", "11&nbsp;to&nbsp;15&nbsp;Days")
            RH.AddColumn(TRHead_1, TRHead_1_05, 5, 5, "l", "16&nbsp;to&nbsp;20&nbsp;Days")
            RH.AddColumn(TRHead_1, TRHead_1_06, 5, 5, "l", "21&nbsp;to&nbsp;25&nbsp;Days")
            RH.AddColumn(TRHead_1, TRHead_1_07, 5, 5, "l", "26&nbsp;to&nbsp;30&nbsp;Days")
            RH.AddColumn(TRHead_1, TRHead_1_08, 5, 5, "1", ">&nbsp;30&nbsp;Days")
            RH.AddColumn(TRHead_1, TRHead_1_09, 5, 5, "1", "Sub&nbsp;Total")




            tb.Controls.Add(TRHead_1)
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"

                TR3.BorderStyle = BorderStyle.Solid


                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid



                'TRHead_1_02.Style.Add("background-color", "#F3C2C2")
                If DR(1) = "Grand Total" Then
                    TR3_00.Style.Add("background-color", "#F6B3B3")
                    TR3_01.Style.Add("background-color", "#F6B3B3")
                    TR3_02.Style.Add("background-color", "#F6B3B3")
                    TR3_03.Style.Add("background-color", "#F6B3B3")
                    TR3_04.Style.Add("background-color", "#F6B3B3")
                    TR3_05.Style.Add("background-color", "#F6B3B3")
                    TR3_06.Style.Add("background-color", "#F6B3B3")
                    TR3_07.Style.Add("background-color", "#F6B3B3")
                    TR3_08.Style.Add("background-color", "#F6B3B3")
                    TR3_09.Style.Add("background-color", "#F6B3B3")


                    RH.AddColumn(TR3, TR3_00, 50, 50, "l", "<b>" + DR(1).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_01, 15, 15, "l", "<b>" + DR(2).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_02, 5, 5, "l", "<b>" + DR(3).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_03, 5, 5, "l", "<b>" + DR(4).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_04, 5, 5, "l", "<b>" + DR(5).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_05, 5, 5, "l", "<b>" + DR(6).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_06, 5, 5, "l", "<b>" + DR(7).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_07, 5, 5, "l", "<b>" + DR(8).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_08, 5, 5, "l", "<b>" + DR(9).ToString() + "</b>")
                    RH.AddColumn(TR3, TR3_09, 5, 5, "l", "<b>" + DR(10).ToString() + "</b>")

                Else
                    TR3_00.Style.Add("background-color", "#FAD9D9")
                    TR3_01.Style.Add("background-color", "#FAD9D9")
                    TR3_02.Style.Add("background-color", "#FAD9D9")
                    TR3_03.Style.Add("background-color", "#FAD9D9")
                    TR3_04.Style.Add("background-color", "#FAD9D9")
                    TR3_05.Style.Add("background-color", "#FAD9D9")
                    TR3_06.Style.Add("background-color", "#FAD9D9")
                    TR3_07.Style.Add("background-color", "#FAD9D9")
                    TR3_08.Style.Add("background-color", "#FAD9D9")
                    TR3_09.Style.Add("background-color", "#FAD9D9")

                    RH.AddColumn(TR3, TR3_00, 50, 50, "l", DR(1).ToString())
                    RH.AddColumn(TR3, TR3_01, 15, 15, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_02, 5, 5, "l", DR(3).ToString())
                    RH.AddColumn(TR3, TR3_03, 5, 5, "l", DR(4).ToString())
                    RH.AddColumn(TR3, TR3_04, 5, 5, "l", DR(5).ToString())
                    RH.AddColumn(TR3, TR3_05, 5, 5, "l", DR(6).ToString())
                    RH.AddColumn(TR3, TR3_06, 5, 5, "l", DR(7).ToString())
                    RH.AddColumn(TR3, TR3_07, 5, 5, "l", DR(8).ToString())
                    RH.AddColumn(TR3, TR3_08, 5, 5, "l", DR(9).ToString())
                    RH.AddColumn(TR3, TR3_09, 5, 5, "l", "<b>" + DR(10).ToString() + "</b>")

                End If

                tb.Controls.Add(TR3)
            Next
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
