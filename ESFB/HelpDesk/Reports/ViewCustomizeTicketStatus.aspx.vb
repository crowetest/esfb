﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewCustomizeTicketStatus
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 136) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim DTT As New DataTable
            Dim StrTicketNo As String = GF.Decrypt(Request.QueryString.Get("TicketNo"))
            Dim StrStatus As Integer = GF.Decrypt(Request.QueryString.Get("StatusID"))
            Dim StrBranch As Integer = GF.Decrypt(Request.QueryString.Get("BranchID"))
            Dim StrTeam As String = GF.Decrypt(Request.QueryString.Get("TeamID"))
            Dim StrGroup As Integer = GF.Decrypt(Request.QueryString.Get("GroupID"))
            Dim StrSubGroup As Integer = GF.Decrypt(Request.QueryString.Get("SubGroupID"))

            Dim StrFromDate As String = ""
            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(Request.QueryString.Get("frmdate"))
            End If
            Dim StrToDate As String = ""
            If CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrToDate = CStr(Request.QueryString.Get("todate"))
            End If

            Dim Struser As String = Session("UserID").ToString()
            Dim StrSubQry As String = ""
            Dim StrSubQry1 As String = ""
            Dim StrQuery As String = ""
            If StrTicketNo = "" Then
                If StrFromDate <> "" And StrToDate <> "" And StrStatus <> 4 And StrStatus <> 0 Then
                    StrQuery = " and  DATEADD(day,DATEDIFF(day, 0,a.request_dt ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
                ElseIf StrFromDate <> "" And StrToDate <> "" And StrStatus = 4 Then
                    StrQuery = " and  DATEADD(day,DATEDIFF(day, 0,a.t_resolved_Dt ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
                ElseIf StrFromDate <> "" And StrToDate <> "" And StrStatus = 0 Then
                    StrQuery = " and  DATEADD(day,DATEDIFF(day, 0,a.t_closed_dt ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
                End If
                If StrBranch <> -1 Then
                    StrQuery += " and a.BRANCH_ID=" & StrBranch & ""
                End If
                If StrSubGroup <> -1 Then
                    StrQuery += " and e.Sub_Group_ID =" & StrSubGroup & ""
                End If
                If StrGroup <> -1 Then
                    StrQuery += " and e.Group_ID =" & StrGroup & ""
                End If
                If StrStatus <> -3 And StrStatus <> -2 And StrStatus <> -1 And StrStatus <> 12 Then
                    StrQuery += " and a.STATUS_ID =" & StrStatus & ""
                Else
                    If StrStatus = -2 Then
                        StrSubQry = " and a.REQUEST_ID in (select distinct request_id from HD_REQUEST_CYCLE  where Remarks<>'Auto Closure' and update_id=0)"
                    ElseIf StrStatus = -1 Then
                        StrSubQry = " and a.REQUEST_ID in (select distinct request_id from HD_REQUEST_CYCLE  where Remarks='Auto Closure' and update_id=0)"

                    End If
                End If
                Dim strTeamQry As String = Nothing
                If StrTeam <> -1 Then

                    If StrStatus = 19 Then
                        StrQuery += " and y.team_id in(" & StrTeam & ")"
                    ElseIf StrStatus <> 12 Then
                        StrQuery += " and y.team_id in(" & StrTeam & ")"

                    End If
                    strTeamQry = " and team_id in(" & StrTeam & ")"
                End If
            If StrStatus = 12 Then
                StrSubQry = " and a.REQUEST_ID in (select distinct request_id from HD_REQUEST_CYCLE  where update_id=12 " & strTeamQry & " )  and a.STATUS_ID =" & StrStatus & ""
            End If
            'End If
            Else
            StrQuery += " and a.TICKET_NO  ='" & StrTicketNo & "'"
            End If
            Dim SqlStr As String


            'StrFromDate
            SqlStr = "select a.TICKET_NO,a.REQUEST_DT,datediff(day,a.REQUEST_DT,case when a.status_id in (0,4,11,12,13,19) then case when a.T_RESOLVED_DT IS not null then a.T_RESOLVED_DT else case when a.CLS_DT_BEFORE_AUTOCLOSE is not null then a.CLS_DT_BEFORE_AUTOCLOSE  else case when a.CLOSE_DT IS not null then a.CLOSE_DT else a.request_dt end end  end  else getdate() end) LagDays,br.Branch_ID, br.Branch_Name, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS, " & _
                " em.Emp_Name as assignto, y.up_user Last_Updated,a.URGENT_REQ,a.REQUEST_ID,convert(varchar,a.t_Closed_dt,106) Closed_Date,a.t_closed_by Closed_By,convert(varchar,a.t_Resolved_dt,106) Resolved_Date,nn.emp_name Resolved_By,convert(varchar,a.Assigned_date,106) Assigned_Date,f.team_name Team,isnull(a.CORRECTION_COUNT,1),isnull(a.CORRECTED_COUNT,0),isnull(r.ROOT_NAME,'') from HD_REQUEST_MASTER a left outer join EMP_MASTER em on(a.ASSIGNED_TO=em.Emp_Code) " & _
                " left join emp_master nn on a.t_resolved_by=nn.emp_code left outer join HD_ROOT_CAUSE r on(a.ROOT_CAUSE=r.ROOT_ID)" & _
                " left outer join (select a.REQUEST_ID as rid ,m.Emp_Name as up_user,a.team_id  from HD_REQUEST_CYCLE a inner join  " & _
                " (select MAX(order_id) as oid,request_id from HD_REQUEST_CYCLE group by REQUEST_ID) hc on a.REQUEST_ID=hc.REQUEST_ID and a.ORDER_ID=hc.oid  " & _
                "  left join EMP_MASTER m on   a.user_id=m.Emp_Code )y on (y.rid=a.REQUEST_ID),HD_PROBLEM_TYPE c ,    " & _
                "  HD_GROUP_MASTER d, HD_SUB_GROUP e,HD_STATUS s ,BRANCH_MASTER br,HD_TEAM_MASTER f  where  a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and  " & _
                " s.STATUS_ID = a.STATUS_ID And e.group_ID = d.group_ID and br.Branch_ID= a.BRANCH_ID and a.team_id = f.team_id " & StrQuery & " " & StrSubQry & " order by a.REQUEST_ID"

            'SqlStr += "   order by a.REQUEST_ID"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim b As Integer = DT.Rows.Count
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RH.Heading(Session("FirmName"), tb, "TICKET STATUS ", 150)

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            '
            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            Dim StrUserNam As String = Nothing

            DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
            If DTT.Rows.Count > 0 Then
                StrUserNam = DTT.Rows(0)(0)
            End If
            Dim TRHead_1 As New TableRow

            RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)


            RH.BlankRow(tb, 4)
            tb.Controls.Add(TRSHead)



            'TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14, TRHead_1_15, TRHead_1_16, TRHead_1_17 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"
            TRHead_1_08.BorderWidth = "1"
            TRHead_1_09.BorderWidth = "1"
            TRHead_1_10.BorderWidth = "1"
            TRHead_1_11.BorderWidth = "1"
            TRHead_1_12.BorderWidth = "1"
            TRHead_1_13.BorderWidth = "1"
            TRHead_1_14.BorderWidth = "1"
            TRHead_1_15.BorderWidth = "1"
            TRHead_1_16.BorderWidth = "1"
            TRHead_1_17.BorderWidth = "1"

            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver
            TRHead_1_08.BorderColor = Drawing.Color.Silver
            TRHead_1_09.BorderColor = Drawing.Color.Silver
            TRHead_1_10.BorderColor = Drawing.Color.Silver
            TRHead_1_11.BorderColor = Drawing.Color.Silver
            TRHead_1_12.BorderColor = Drawing.Color.Silver
            TRHead_1_13.BorderColor = Drawing.Color.Silver
            TRHead_1_14.BorderColor = Drawing.Color.Silver
            TRHead_1_15.BorderColor = Drawing.Color.Silver
            TRHead_1_16.BorderColor = Drawing.Color.Silver
            TRHead_1_17.BorderColor = Drawing.Color.Silver

            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid
            TRHead_1_08.BorderStyle = BorderStyle.Solid
            TRHead_1_09.BorderStyle = BorderStyle.Solid
            TRHead_1_10.BorderStyle = BorderStyle.Solid
            TRHead_1_11.BorderStyle = BorderStyle.Solid
            TRHead_1_12.BorderStyle = BorderStyle.Solid
            TRHead_1_13.BorderStyle = BorderStyle.Solid
            TRHead_1_14.BorderStyle = BorderStyle.Solid
            TRHead_1_15.BorderStyle = BorderStyle.Solid
            TRHead_1_16.BorderStyle = BorderStyle.Solid
            TRHead_1_17.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead_1, TRHead_1_00, 8, 8, "1", "Ticket No")
            RH.AddColumn(TRHead_1, TRHead_1_01, 5, 5, "l", "Request Date")
            RH.AddColumn(TRHead_1, TRHead_1_02, 5, 5, "l", "Assign Dt")
            RH.AddColumn(TRHead_1, TRHead_1_03, 2, 2, "c", "Lag days")
            RH.AddColumn(TRHead_1, TRHead_1_04, 4, 4, "c", "Branch Code")
            RH.AddColumn(TRHead_1, TRHead_1_05, 8, 8, "l", "Branch Name")
            RH.AddColumn(TRHead_1, TRHead_1_07, 9, 9, "l", "Findings")

            RH.AddColumn(TRHead_1, TRHead_1_15, 4, 4, "r", "Correction Count")
            RH.AddColumn(TRHead_1, TRHead_1_16, 4, 4, "r", "Corrected Count")
            RH.AddColumn(TRHead_1, TRHead_1_17, 4, 4, "l", "Root Cause")

            RH.AddColumn(TRHead_1, TRHead_1_09, 10, 10, "l", "Remark")
            RH.AddColumn(TRHead_1, TRHead_1_10, 9, 9, "l", "Attended by")
            RH.AddColumn(TRHead_1, TRHead_1_08, 5, 5, "l", "Status")
            RH.AddColumn(TRHead_1, TRHead_1_11, 5, 5, "l", "Last updated")
            RH.AddColumn(TRHead_1, TRHead_1_12, 5, 5, "l", "Resolved Dt")
            RH.AddColumn(TRHead_1, TRHead_1_13, 8, 8, "l", "Resolved By")
            RH.AddColumn(TRHead_1, TRHead_1_14, 9, 9, "l", "Team")

            tb.Controls.Add(TRHead_1)

            For Each DR In DT.Rows

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16, TR3_17 As New TableCell
                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"
                TR3_15.BorderWidth = "1"
                TR3_16.BorderWidth = "1"
                TR3_17.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver
                TR3_15.BorderColor = Drawing.Color.Silver
                TR3_16.BorderColor = Drawing.Color.Silver
                TR3_17.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid
                TR3_15.BorderStyle = BorderStyle.Solid
                TR3_16.BorderStyle = BorderStyle.Solid
                TR3_17.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 8, 8, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(13).ToString) + "' target='_blank'>" + DR(0).ToString())
                'RH.AddColumn(TR3, TR3_00, 10, 10, "1", DR(0))
                RH.AddColumn(TR3, TR3_01, 5, 5, "l", CDate(DR(1)).ToString("dd/MM/yyyy"))
                If Not IsDBNull(DR(18)) Then
                    RH.AddColumn(TR3, TR3_02, 5, 5, "l", CDate(DR(18)).ToString("dd/MM/yyyy"))
                Else
                    RH.AddColumn(TR3, TR3_02, 5, 5, "l", "")
                End If

                RH.AddColumn(TR3, TR3_03, 2, 2, "c", DR(2).ToString())
                RH.AddColumn(TR3, TR3_04, 4, 4, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_05, 8, 8, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_07, 9, 9, "l", DR(7).ToString())

                RH.AddColumn(TR3, TR3_15, 4, 4, "r", DR(20).ToString())
                RH.AddColumn(TR3, TR3_16, 4, 4, "r", DR(21).ToString())
                RH.AddColumn(TR3, TR3_17, 4, 4, "r", DR(22).ToString())

                Dim strstat As String = DR(8).ToString()

                RH.AddColumn(TR3, TR3_09, 10, 10, "l", DR(9).ToString())
                RH.AddColumn(TR3, TR3_10, 9, 9, "l", DR(10).ToString())
                RH.AddColumn(TR3, TR3_08, 5, 5, "l", strstat)
                RH.AddColumn(TR3, TR3_11, 5, 5, "l", DR(11).ToString())
                If Not IsDBNull(DR(16)) Then
                    RH.AddColumn(TR3, TR3_12, 5, 5, "l", DR(16).ToString())
                Else
                    RH.AddColumn(TR3, TR3_12, 5, 5, "l", "")
                End If

                RH.AddColumn(TR3, TR3_13, 8, 8, "l", DR(17).ToString())
                RH.AddColumn(TR3, TR3_14, 9, 9, "l", DR(19).ToString())
                If DR(12) = 1 Then
                    TR3_00.Style.Add("background-color", "#F3C2C2")
                    TR3_01.Style.Add("background-color", "#F3C2C2")
                    TR3_02.Style.Add("background-color", "#F3C2C2")
                    TR3_03.Style.Add("background-color", "#F3C2C2")
                    TR3_04.Style.Add("background-color", "#F3C2C2")
                    TR3_05.Style.Add("background-color", "#F3C2C2")
                    TR3_06.Style.Add("background-color", "#F3C2C2")
                    TR3_07.Style.Add("background-color", "#F3C2C2")
                    TR3_08.Style.Add("background-color", "#F3C2C2")
                    TR3_09.Style.Add("background-color", "#F3C2C2")
                    TR3_10.Style.Add("background-color", "#F3C2C2")
                    TR3_11.Style.Add("background-color", "#F3C2C2")
                    TR3_12.Style.Add("background-color", "#F3C2C2")
                    TR3_13.Style.Add("background-color", "#F3C2C2")
                    TR3_14.Style.Add("background-color", "#F3C2C2")
                    TR3_15.Style.Add("background-color", "#F3C2C2")
                    TR3_16.Style.Add("background-color", "#F3C2C2")
                    TR3_17.Style.Add("background-color", "#F3C2C2")
                End If

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)

        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        Try
            WebTools.ExporttoExcel(DT, "Ticket")
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
