﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewTicketStatus
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DTT As New DataTable
            Dim DT As New DataTable


            Dim StrStatus As String = Request.QueryString.Get("statusID")
            Dim Strst As String = Request.QueryString.Get("statusnam")
            Dim IntCount As Integer = CInt(Request.QueryString.Get("counts"))
            Dim Struser As String = Session("UserID").ToString()
            Dim StrUserNam As String = Nothing
            Dim SqlStr As String
            DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
            If DTT.Rows.Count > 0 Then
                StrUserNam = DTT.Rows(0)(0)
            End If
            DT = DB.ExecuteDataSet("select team_id from HD_TEAM_DTL where emp_code= " & Struser & "").Tables(0)
            If DT.Rows.Count > 0 Then
                If DT.Rows(0)(0) = 4 Then 'help desk
                    SqlStr = "select a.TICKET_NO,a.REQUEST_DT,datediff(day,a.REQUEST_DT,getdate()) , br.Branch_Name, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS, " & _
                        " em.Emp_Name as assignto, y.up_user,a.URGENT_REQ,a.REQUEST_ID from HD_REQUEST_MASTER a left outer join EMP_MASTER em on(a.ASSIGNED_TO=em.Emp_Code) " & _
                        " left outer join (select a.REQUEST_ID as rid ,m.Emp_Name as up_user  from HD_REQUEST_CYCLE a inner join  " & _
                        " (select MAX(order_id) as oid,request_id from HD_REQUEST_CYCLE group by REQUEST_ID) hc on a.REQUEST_ID=hc.REQUEST_ID and a.ORDER_ID=hc.oid  " & _
                        "  left join EMP_MASTER m on   a.user_id=m.Emp_Code )y on (y.rid=a.REQUEST_ID),HD_PROBLEM_TYPE c ,    " & _
                        "  HD_GROUP_MASTER d, HD_SUB_GROUP e,HD_STATUS s ,BRANCH_MASTER br  where  a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and  " & _
                        " s.STATUS_ID = a.STATUS_ID And e.group_ID = d.group_ID and br.Branch_ID= a.BRANCH_ID  and a.status_id=12   order by a.REQUEST_ID"
                Else

                    SqlStr = "select a.TICKET_NO,a.REQUEST_DT,datediff(day,a.REQUEST_DT,getdate()), br.Branch_Name, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS, " & _
                        " em.Emp_Name as assignto, y.up_user,a.URGENT_REQ,a.REQUEST_ID from HD_REQUEST_MASTER a left outer join EMP_MASTER em on(a.ASSIGNED_TO=em.Emp_Code) " & _
                        " left outer join (select a.REQUEST_ID as rid ,m.Emp_Name as up_user  from HD_REQUEST_CYCLE a inner join  " & _
                        " (select MAX(order_id) as oid,request_id from HD_REQUEST_CYCLE group by REQUEST_ID) hc on a.REQUEST_ID=hc.REQUEST_ID and a.ORDER_ID=hc.oid  " & _
                        "  left join EMP_MASTER m on   a.user_id=m.Emp_Code )y on (y.rid=a.REQUEST_ID),HD_PROBLEM_TYPE c ,    " & _
                        "  HD_GROUP_MASTER d, HD_SUB_GROUP e,HD_STATUS s ,BRANCH_MASTER br  where  a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and  " & _
                        " s.STATUS_ID = a.STATUS_ID And e.group_ID = d.group_ID and br.Branch_ID= a.BRANCH_ID  " & StrStatus & "  and a.team_id=" & DT.Rows(0)(0) & " order by a.REQUEST_ID"
                End If
                DT = DB.ExecuteDataSet(SqlStr).Tables(0)

                cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
                cmd_Back.Attributes.Add("onclick", "return Exitform()")


                If IntCount <> 1 Then
                    RH.Heading(Session("FirmName"), tb, "STATUS OF TICKETS", 150)
                Else
                    RH.Heading(Session("FirmName"), tb, "STATUS OF TICKETS  - ( " & Strst & " )", 150)
                End If


                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")


                '
                Dim TRSHead As New TableRow
                TRSHead.Width = "100"
                TRSHead.BorderWidth = "1"
                TRSHead.BorderStyle = BorderStyle.Solid
                TRSHead.Style.Add("background-color", "lightsteelblue")

                Dim TRSHead_00 As New TableCell
                TRSHead_00.BorderWidth = "1"
                TRSHead_00.BorderColor = Drawing.Color.Silver
                TRSHead_00.BorderStyle = BorderStyle.Solid

                Dim TRHead_1 As New TableRow
                If IntCount <> 0 Then
                    RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)
                Else
                    RH.AddColumn(TRSHead, TRSHead_00, 100, 100, "c", "USER :" + StrUserNam)
                End If

                RH.BlankRow(tb, 4)
                tb.Controls.Add(TRSHead)
                If IntCount <> 1 Then



                    'TRHead.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10 As New TableCell

                    TRHead_1_00.BorderWidth = "1"
                    TRHead_1_01.BorderWidth = "1"
                    TRHead_1_02.BorderWidth = "1"
                    TRHead_1_03.BorderWidth = "1"
                    TRHead_1_04.BorderWidth = "1"
                    TRHead_1_05.BorderWidth = "1"
                    TRHead_1_06.BorderStyle = "1"
                    TRHead_1_07.BorderStyle = "1"
                    TRHead_1_08.BorderStyle = "1"
                    TRHead_1_09.BorderStyle = "1"
                    TRHead_1_10.BorderStyle = "1"


                    TRHead_1_00.BorderColor = Drawing.Color.Silver
                    TRHead_1_01.BorderColor = Drawing.Color.Silver
                    TRHead_1_02.BorderColor = Drawing.Color.Silver
                    TRHead_1_03.BorderColor = Drawing.Color.Silver
                    TRHead_1_04.BorderColor = Drawing.Color.Silver
                    TRHead_1_05.BorderColor = Drawing.Color.Silver
                    TRHead_1_06.BorderColor = Drawing.Color.Silver
                    TRHead_1_07.BorderColor = Drawing.Color.Silver
                    TRHead_1_08.BorderColor = Drawing.Color.Silver
                    TRHead_1_09.BorderColor = Drawing.Color.Silver
                    TRHead_1_10.BorderColor = Drawing.Color.Silver


                    TRHead_1_00.BorderStyle = BorderStyle.Solid
                    TRHead_1_01.BorderStyle = BorderStyle.Solid
                    TRHead_1_02.BorderStyle = BorderStyle.Solid
                    TRHead_1_03.BorderStyle = BorderStyle.Solid
                    TRHead_1_04.BorderStyle = BorderStyle.Solid
                    TRHead_1_05.BorderStyle = BorderStyle.Solid
                    TRHead_1_06.BorderStyle = BorderStyle.Solid
                    TRHead_1_07.BorderStyle = BorderStyle.Solid
                    TRHead_1_08.BorderStyle = BorderStyle.Solid
                    TRHead_1_09.BorderStyle = BorderStyle.Solid
                    TRHead_1_10.BorderStyle = BorderStyle.Solid


                    RH.AddColumn(TRHead_1, TRHead_1_00, 6, 6, "1", "Ticket No")
                    RH.AddColumn(TRHead_1, TRHead_1_01, 5, 5, "l", "Date")
                    RH.AddColumn(TRHead_1, TRHead_1_02, 3, 3, "l", "Lag days")
                    RH.AddColumn(TRHead_1, TRHead_1_03, 10, 10, "l", "Branch Name")
                    RH.AddColumn(TRHead_1, TRHead_1_04, 8, 8, "l", "Category")
                    RH.AddColumn(TRHead_1, TRHead_1_05, 13, 13, "l", "Sub Category")
                    RH.AddColumn(TRHead_1, TRHead_1_06, 13, 13, "l", "Findings")

                    RH.AddColumn(TRHead_1, TRHead_1_07, 10, 10, "l", "Status")
                    RH.AddColumn(TRHead_1, TRHead_1_08, 22, 22, "l", "Remark")
                    RH.AddColumn(TRHead_1, TRHead_1_09, 10, 10, "l", "Attended by")
                    RH.AddColumn(TRHead_1, TRHead_1_10, 10, 10, "l", "Last updated")
                Else



                    'TRHead.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09 As New TableCell

                    TRHead_1_00.BorderWidth = "1"
                    TRHead_1_01.BorderWidth = "1"
                    TRHead_1_02.BorderWidth = "1"
                    TRHead_1_03.BorderWidth = "1"
                    TRHead_1_04.BorderWidth = "1"
                    TRHead_1_05.BorderWidth = "1"
                    TRHead_1_06.BorderStyle = "1"
                    TRHead_1_07.BorderStyle = "1"
                    TRHead_1_08.BorderStyle = "1"
                    TRHead_1_09.BorderStyle = "1"



                    TRHead_1_00.BorderColor = Drawing.Color.Silver
                    TRHead_1_01.BorderColor = Drawing.Color.Silver
                    TRHead_1_02.BorderColor = Drawing.Color.Silver
                    TRHead_1_03.BorderColor = Drawing.Color.Silver
                    TRHead_1_04.BorderColor = Drawing.Color.Silver
                    TRHead_1_05.BorderColor = Drawing.Color.Silver
                    TRHead_1_06.BorderColor = Drawing.Color.Silver
                    TRHead_1_07.BorderColor = Drawing.Color.Silver
                    TRHead_1_08.BorderColor = Drawing.Color.Silver
                    TRHead_1_09.BorderColor = Drawing.Color.Silver



                    TRHead_1_00.BorderStyle = BorderStyle.Solid
                    TRHead_1_01.BorderStyle = BorderStyle.Solid
                    TRHead_1_02.BorderStyle = BorderStyle.Solid
                    TRHead_1_03.BorderStyle = BorderStyle.Solid
                    TRHead_1_04.BorderStyle = BorderStyle.Solid
                    TRHead_1_05.BorderStyle = BorderStyle.Solid
                    TRHead_1_06.BorderStyle = BorderStyle.Solid
                    TRHead_1_07.BorderStyle = BorderStyle.Solid
                    TRHead_1_08.BorderStyle = BorderStyle.Solid
                    TRHead_1_09.BorderStyle = BorderStyle.Solid



                    RH.AddColumn(TRHead_1, TRHead_1_00, 9, 9, "1", "Ticket No")
                    RH.AddColumn(TRHead_1, TRHead_1_01, 5, 5, "l", "Date")
                    RH.AddColumn(TRHead_1, TRHead_1_02, 3, 3, "l", "Lag days")
                    RH.AddColumn(TRHead_1, TRHead_1_03, 10, 10, "l", "Branch Name")
                    RH.AddColumn(TRHead_1, TRHead_1_04, 8, 8, "l", "Category")
                    RH.AddColumn(TRHead_1, TRHead_1_05, 13, 13, "l", "Sub Category")
                    RH.AddColumn(TRHead_1, TRHead_1_06, 13, 13, "l", "Findings")
                    RH.AddColumn(TRHead_1, TRHead_1_07, 24, 24, "l", "Remark")
                    RH.AddColumn(TRHead_1, TRHead_1_08, 10, 10, "l", "Attended by")
                    RH.AddColumn(TRHead_1, TRHead_1_09, 10, 10, "l", "Last updated")
                End If
                'tb.Controls.Add(TRHead_1)
                tb.Controls.Add(TRHead_1)


                For Each DR In DT.Rows

                    If IntCount <> 1 Then
                        Dim TR3 As New TableRow
                        TR3.BorderWidth = "1"
                        TR3.BorderStyle = BorderStyle.Solid
                        Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10 As New TableCell
                        TR3_00.BorderWidth = "1"
                        TR3_01.BorderWidth = "1"
                        TR3_02.BorderWidth = "1"
                        TR3_03.BorderWidth = "1"
                        TR3_04.BorderWidth = "1"
                        TR3_05.BorderWidth = "1"
                        TR3_06.BorderWidth = "1"
                        TR3_07.BorderWidth = "1"
                        TR3_08.BorderWidth = "1"
                        TR3_09.BorderWidth = "1"
                        TR3_10.BorderWidth = "1"

                        TR3_00.BorderColor = Drawing.Color.Silver
                        TR3_01.BorderColor = Drawing.Color.Silver
                        TR3_02.BorderColor = Drawing.Color.Silver
                        TR3_03.BorderColor = Drawing.Color.Silver
                        TR3_04.BorderColor = Drawing.Color.Silver
                        TR3_05.BorderColor = Drawing.Color.Silver
                        TR3_06.BorderColor = Drawing.Color.Silver
                        TR3_07.BorderColor = Drawing.Color.Silver
                        TR3_08.BorderColor = Drawing.Color.Silver
                        TR3_09.BorderColor = Drawing.Color.Silver
                        TR3_10.BorderColor = Drawing.Color.Silver

                        TR3_00.BorderStyle = BorderStyle.Solid
                        TR3_01.BorderStyle = BorderStyle.Solid
                        TR3_02.BorderStyle = BorderStyle.Solid
                        TR3_03.BorderStyle = BorderStyle.Solid
                        TR3_04.BorderStyle = BorderStyle.Solid
                        TR3_05.BorderStyle = BorderStyle.Solid
                        TR3_06.BorderStyle = BorderStyle.Solid
                        TR3_07.BorderStyle = BorderStyle.Solid
                        TR3_08.BorderStyle = BorderStyle.Solid
                        TR3_09.BorderStyle = BorderStyle.Solid
                        TR3_10.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TR3, TR3_00, 6, 6, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(12).ToString) + "' target='_blank'>" + DR(0).ToString())
                        'RH.AddColumn(TR3, TR3_00, 10, 10, "1", DR(0))
                        RH.AddColumn(TR3, TR3_01, 5, 5, "l", CDate(DR(1)).ToString("dd/MM/yyyy"))
                        RH.AddColumn(TR3, TR3_02, 3, 3, "l", DR(2).ToString())
                        RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(3).ToString())
                        RH.AddColumn(TR3, TR3_04, 8, 8, "l", DR(4).ToString())
                        RH.AddColumn(TR3, TR3_05, 13, 13, "l", DR(5).ToString())
                        RH.AddColumn(TR3, TR3_06, 13, 13, "l", DR(6).ToString())
                        Dim strstat As String = Nothing
                        If DR(7).ToString() = "SEND TO APPROVAL" Then
                            strstat = "SEND TO AM APPROVAL"
                        Else
                            strstat = DR(7).ToString()
                        End If
                        RH.AddColumn(TR3, TR3_07, 10, 10, "l", strstat)
                        RH.AddColumn(TR3, TR3_08, 22, 22, "l", DR(8).ToString())
                        RH.AddColumn(TR3, TR3_09, 10, 10, "l", DR(9).ToString())
                        RH.AddColumn(TR3, TR3_10, 10, 10, "l", DR(10).ToString())
                        If DR(11) = 1 Then
                            TR3_00.Style.Add("background-color", "#F3C2C2")
                            TR3_01.Style.Add("background-color", "#F3C2C2")
                            TR3_02.Style.Add("background-color", "#F3C2C2")
                            TR3_03.Style.Add("background-color", "#F3C2C2")
                            TR3_04.Style.Add("background-color", "#F3C2C2")
                            TR3_05.Style.Add("background-color", "#F3C2C2")
                            TR3_06.Style.Add("background-color", "#F3C2C2")
                            TR3_07.Style.Add("background-color", "#F3C2C2")
                            TR3_08.Style.Add("background-color", "#F3C2C2")
                            TR3_09.Style.Add("background-color", "#F3C2C2")
                            TR3_10.Style.Add("background-color", "#F3C2C2")

                        End If

                        tb.Controls.Add(TR3)
                    Else

                        Dim TR4 As New TableRow
                        TR4.BorderWidth = "1"
                        TR4.BorderStyle = BorderStyle.Solid
                        Dim TR4_00, TR4_01, TR4_02, TR4_03, TR4_04, TR4_05, TR4_06, TR4_07, TR4_08, TR4_09 As New TableCell
                        TR4_00.BorderWidth = "1"
                        TR4_01.BorderWidth = "1"
                        TR4_02.BorderWidth = "1"
                        TR4_03.BorderWidth = "1"
                        TR4_04.BorderWidth = "1"
                        TR4_05.BorderWidth = "1"
                        TR4_06.BorderWidth = "1"
                        TR4_07.BorderWidth = "1"
                        TR4_08.BorderWidth = "1"
                        TR4_09.BorderWidth = "1"

                        TR4_00.BorderColor = Drawing.Color.Silver
                        TR4_01.BorderColor = Drawing.Color.Silver
                        TR4_02.BorderColor = Drawing.Color.Silver
                        TR4_03.BorderColor = Drawing.Color.Silver
                        TR4_04.BorderColor = Drawing.Color.Silver
                        TR4_05.BorderColor = Drawing.Color.Silver
                        TR4_06.BorderColor = Drawing.Color.Silver
                        TR4_07.BorderColor = Drawing.Color.Silver
                        TR4_08.BorderColor = Drawing.Color.Silver
                        TR4_09.BorderColor = Drawing.Color.Silver

                        TR4_00.BorderStyle = BorderStyle.Solid
                        TR4_01.BorderStyle = BorderStyle.Solid
                        TR4_02.BorderStyle = BorderStyle.Solid
                        TR4_03.BorderStyle = BorderStyle.Solid
                        TR4_04.BorderStyle = BorderStyle.Solid
                        TR4_05.BorderStyle = BorderStyle.Solid
                        TR4_06.BorderStyle = BorderStyle.Solid
                        TR4_07.BorderStyle = BorderStyle.Solid
                        TR4_08.BorderStyle = BorderStyle.Solid
                        TR4_09.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TR4, TR4_00, 9, 9, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(12).ToString) + "' target='_blank'>" + DR(0).ToString())
                        'RH.AddColumn(TR3, TR3_00, 10, 10, "1", DR(0))
                        RH.AddColumn(TR4, TR4_01, 5, 5, "l", CDate(DR(1)).ToString("dd/MM/yyyy"))
                        RH.AddColumn(TR4, TR4_02, 3, 3, "l", DR(2).ToString())
                        RH.AddColumn(TR4, TR4_03, 10, 10, "l", DR(3).ToString())
                        RH.AddColumn(TR4, TR4_04, 8, 8, "l", DR(4).ToString())
                        RH.AddColumn(TR4, TR4_05, 13, 13, "l", DR(5).ToString())
                        RH.AddColumn(TR4, TR4_06, 13, 13, "l", DR(6).ToString())
                        RH.AddColumn(TR4, TR4_07, 24, 24, "l", DR(8).ToString())
                        RH.AddColumn(TR4, TR4_08, 10, 10, "l", DR(9).ToString())
                        RH.AddColumn(TR4, TR4_09, 10, 10, "l", DR(10).ToString())
                        If DR(11) = 1 Then
                            TR4_00.Style.Add("background-color", "#F3C2C2")
                            TR4_01.Style.Add("background-color", "#F3C2C2")
                            TR4_02.Style.Add("background-color", "#F3C2C2")
                            TR4_03.Style.Add("background-color", "#F3C2C2")
                            TR4_04.Style.Add("background-color", "#F3C2C2")
                            TR4_05.Style.Add("background-color", "#F3C2C2")
                            TR4_06.Style.Add("background-color", "#F3C2C2")
                            TR4_07.Style.Add("background-color", "#F3C2C2")
                            TR4_08.Style.Add("background-color", "#F3C2C2")
                            TR4_09.Style.Add("background-color", "#F3C2C2")

                        End If
                        tb.Controls.Add(TR4)

                    End If

                Next
                RH.BlankRow(tb, 30)
                pnDisplay.Controls.Add(tb)
            Else
                SqlStr = "select a.TICKET_NO,a.REQUEST_DT, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS,a.REQUEST_ID,datediff(day,a.REQUEST_DT,getdate())  " & _
                            " from HD_REQUEST_MASTER a left outer join HD_PROBLEM_TYPE c on (a.PROBLEM_ID=c.PROBLEM_ID),  HD_GROUP_MASTER d, " & _
                            " HD_SUB_GROUP e,HD_STATUS s where  a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and  " & _
                            " s.STATUS_ID = a.STATUS_ID And e.group_ID = d.group_ID and a.branch_id=" & Session("branchID").ToString() & " " & StrStatus & " and a.USER_ID = " + Struser + " order by a.REQUEST_ID"


                DT = DB.ExecuteDataSet(SqlStr).Tables(0)

                cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
                cmd_Back.Attributes.Add("onclick", "return Exitform()")


                If IntCount <> 1 Then
                    RH.Heading(Session("FirmName"), tb, "STATUS OF TICKETS", 150)
                Else
                    RH.Heading(Session("FirmName"), tb, "STATUS OF TICKETS  - ( " & Strst & " )", 150)
                End If


                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")



                'RH.BlankRow(tb, 4)
                Dim TRSHead As New TableRow
                TRSHead.BorderWidth = "1"
                TRSHead.BorderStyle = BorderStyle.Solid
                TRSHead.Style.Add("background-color", "lightsteelblue")
                Dim TRSHead_00 As New TableCell
                TRSHead_00.BorderWidth = "1"
                TRSHead_00.BorderColor = Drawing.Color.Silver
                TRSHead_00.BorderStyle = BorderStyle.Solid



                RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "R", "USER :" + StrUserNam)
                tb.Controls.Add(TRSHead)
                RH.BlankRow(tb, 4)


                Dim TRHead_1 As New TableRow
                'TRHead.BackColor = Drawing.Color.WhiteSmoke
                If IntCount <> 1 Then
                    Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07 As New TableCell

                    TRHead_1_00.BorderWidth = "1"
                    TRHead_1_01.BorderWidth = "1"
                    TRHead_1_02.BorderWidth = "1"
                    TRHead_1_03.BorderWidth = "1"
                    TRHead_1_04.BorderWidth = "1"
                    TRHead_1_05.BorderWidth = "1"
                    TRHead_1_06.BorderStyle = "1"
                    TRHead_1_07.BorderStyle = "1"

                    TRHead_1_00.BorderColor = Drawing.Color.Silver
                    TRHead_1_01.BorderColor = Drawing.Color.Silver
                    TRHead_1_02.BorderColor = Drawing.Color.Silver
                    TRHead_1_03.BorderColor = Drawing.Color.Silver
                    TRHead_1_04.BorderColor = Drawing.Color.Silver
                    TRHead_1_05.BorderColor = Drawing.Color.Silver
                    TRHead_1_06.BorderColor = Drawing.Color.Silver
                    TRHead_1_07.BorderColor = Drawing.Color.Silver

                    TRHead_1_00.BorderStyle = BorderStyle.Solid
                    TRHead_1_01.BorderStyle = BorderStyle.Solid
                    TRHead_1_02.BorderStyle = BorderStyle.Solid
                    TRHead_1_03.BorderStyle = BorderStyle.Solid
                    TRHead_1_04.BorderStyle = BorderStyle.Solid
                    TRHead_1_05.BorderStyle = BorderStyle.Solid
                    TRHead_1_06.BorderStyle = BorderStyle.Solid
                    TRHead_1_07.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TRHead_1, TRHead_1_00, 12, 12, "1", "Ticket No")
                    RH.AddColumn(TRHead_1, TRHead_1_01, 7, 7, "l", "Date")
                    RH.AddColumn(TRHead_1, TRHead_1_02, 6, 6, "l", "Lag Days")
                    RH.AddColumn(TRHead_1, TRHead_1_03, 10, 10, "l", "Category")
                    RH.AddColumn(TRHead_1, TRHead_1_04, 13, 13, "l", "Sub Category")
                    RH.AddColumn(TRHead_1, TRHead_1_05, 18, 18, "l", "Findings")
                    RH.AddColumn(TRHead_1, TRHead_1_06, 14, 14, "l", "Status")
                    RH.AddColumn(TRHead_1, TRHead_1_07, 20, 20, "l", "Remark")

                    tb.Controls.Add(TRHead_1)

                    For Each DR In DT.Rows
                        Dim TR3 As New TableRow
                        TR3.BorderWidth = "1"
                        TR3.BorderStyle = BorderStyle.Solid


                        Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07 As New TableCell

                        TR3_00.BorderWidth = "1"
                        TR3_01.BorderWidth = "1"
                        TR3_02.BorderWidth = "1"
                        TR3_03.BorderWidth = "1"
                        TR3_04.BorderWidth = "1"
                        TR3_05.BorderWidth = "1"
                        TR3_06.BorderWidth = "1"
                        TR3_07.BorderWidth = "1"

                        TR3_00.BorderColor = Drawing.Color.Silver
                        TR3_01.BorderColor = Drawing.Color.Silver
                        TR3_02.BorderColor = Drawing.Color.Silver
                        TR3_03.BorderColor = Drawing.Color.Silver
                        TR3_04.BorderColor = Drawing.Color.Silver
                        TR3_05.BorderColor = Drawing.Color.Silver
                        TR3_06.BorderColor = Drawing.Color.Silver
                        TR3_07.BorderColor = Drawing.Color.Silver


                        TR3_00.BorderStyle = BorderStyle.Solid
                        TR3_01.BorderStyle = BorderStyle.Solid
                        TR3_02.BorderStyle = BorderStyle.Solid
                        TR3_03.BorderStyle = BorderStyle.Solid
                        TR3_04.BorderStyle = BorderStyle.Solid
                        TR3_05.BorderStyle = BorderStyle.Solid
                        TR3_06.BorderStyle = BorderStyle.Solid
                        TR3_07.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TR3, TR3_00, 12, 10, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(7).ToString) + "' target='_blank'>" + DR(0).ToString())
                        'RH.AddColumn(TR3, TR3_00, 10, 10, "1", DR(0))
                        RH.AddColumn(TR3, TR3_01, 7, 7, "l", CDate(DR(1)).ToString("dd/MM/yyyy"))
                        RH.AddColumn(TR3, TR3_02, 6, 6, "l", DR(8).ToString())
                        RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(2).ToString())
                        RH.AddColumn(TR3, TR3_04, 13, 13, "l", DR(3).ToString())
                        RH.AddColumn(TR3, TR3_05, 18, 18, "l", DR(4).ToString())
                        Dim strapp As String = Nothing
                        If DR(5).ToString() = "SEND TO APPROVAL" Then
                            strapp = "SEND TO AM APPROVAL"
                        Else
                            strapp = DR(5).ToString()
                        End If
                        RH.AddColumn(TR3, TR3_06, 14, 14, "l", strapp)
                        RH.AddColumn(TR3, TR3_07, 20, 20, "l", DR(6).ToString())


                        tb.Controls.Add(TR3)
                    Next
                Else
                    Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06 As New TableCell

                    TRHead_1_00.BorderWidth = "1"
                    TRHead_1_01.BorderWidth = "1"
                    TRHead_1_02.BorderWidth = "1"
                    TRHead_1_03.BorderWidth = "1"
                    TRHead_1_04.BorderWidth = "1"
                    TRHead_1_05.BorderWidth = "1"
                    TRHead_1_06.BorderStyle = "1"


                    TRHead_1_00.BorderColor = Drawing.Color.Silver
                    TRHead_1_01.BorderColor = Drawing.Color.Silver
                    TRHead_1_02.BorderColor = Drawing.Color.Silver
                    TRHead_1_03.BorderColor = Drawing.Color.Silver
                    TRHead_1_04.BorderColor = Drawing.Color.Silver
                    TRHead_1_05.BorderColor = Drawing.Color.Silver
                    TRHead_1_06.BorderColor = Drawing.Color.Silver


                    TRHead_1_00.BorderStyle = BorderStyle.Solid
                    TRHead_1_01.BorderStyle = BorderStyle.Solid
                    TRHead_1_02.BorderStyle = BorderStyle.Solid
                    TRHead_1_03.BorderStyle = BorderStyle.Solid
                    TRHead_1_04.BorderStyle = BorderStyle.Solid
                    TRHead_1_05.BorderStyle = BorderStyle.Solid
                    TRHead_1_06.BorderStyle = BorderStyle.Solid


                    RH.AddColumn(TRHead_1, TRHead_1_00, 16, 16, "1", "Ticket No")
                    RH.AddColumn(TRHead_1, TRHead_1_01, 7, 7, "l", "Date")
                    RH.AddColumn(TRHead_1, TRHead_1_02, 6, 6, "l", "Lag Days")
                    RH.AddColumn(TRHead_1, TRHead_1_03, 10, 10, "l", "Category")
                    RH.AddColumn(TRHead_1, TRHead_1_04, 13, 13, "l", "Sub Category")
                    RH.AddColumn(TRHead_1, TRHead_1_05, 24, 24, "l", "Findings")
                    RH.AddColumn(TRHead_1, TRHead_1_06, 24, 24, "l", "Remark")


                    tb.Controls.Add(TRHead_1)

                    For Each DR In DT.Rows
                        Dim TR3 As New TableRow
                        TR3.BorderWidth = "1"
                        TR3.BorderStyle = BorderStyle.Solid


                        Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                        TR3_00.BorderWidth = "1"
                        TR3_01.BorderWidth = "1"
                        TR3_02.BorderWidth = "1"
                        TR3_03.BorderWidth = "1"
                        TR3_04.BorderWidth = "1"
                        TR3_05.BorderWidth = "1"
                        TR3_06.BorderWidth = "1"

                        TR3_00.BorderColor = Drawing.Color.Silver
                        TR3_01.BorderColor = Drawing.Color.Silver
                        TR3_02.BorderColor = Drawing.Color.Silver
                        TR3_03.BorderColor = Drawing.Color.Silver
                        TR3_04.BorderColor = Drawing.Color.Silver
                        TR3_05.BorderColor = Drawing.Color.Silver
                        TR3_06.BorderColor = Drawing.Color.Silver


                        TR3_00.BorderStyle = BorderStyle.Solid
                        TR3_01.BorderStyle = BorderStyle.Solid
                        TR3_02.BorderStyle = BorderStyle.Solid
                        TR3_03.BorderStyle = BorderStyle.Solid
                        TR3_04.BorderStyle = BorderStyle.Solid
                        TR3_05.BorderStyle = BorderStyle.Solid
                        TR3_06.BorderStyle = BorderStyle.Solid


                        RH.AddColumn(TR3, TR3_00, 16, 16, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(7).ToString) + "' target='_blank'>" + DR(0).ToString())
                        'RH.AddColumn(TR3, TR3_00, 10, 10, "1", DR(0))
                        RH.AddColumn(TR3, TR3_01, 7, 7, "l", CDate(DR(1)).ToString("dd/MM/yyyy"))
                        RH.AddColumn(TR3, TR3_02, 6, 6, "l", DR(8).ToString())
                        RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(2).ToString())
                        RH.AddColumn(TR3, TR3_04, 13, 13, "l", DR(3).ToString())
                        RH.AddColumn(TR3, TR3_05, 24, 24, "l", DR(4).ToString())

                        RH.AddColumn(TR3, TR3_06, 24, 24, "l", DR(6).ToString())



                        tb.Controls.Add(TR3)
                    Next
                End If

                RH.BlankRow(tb, 30)
                pnDisplay.Controls.Add(tb)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
