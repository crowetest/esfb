﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewTicketStatus
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim WebTools As New WebApp.Tools
    Dim DTT As New DataTable
    Dim DT As New DataTable
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim StrStatus As String = GF.Decrypt(Request.QueryString.Get("statusID"))
            Dim Strst As String = GF.Decrypt(Request.QueryString.Get("statusnam"))
            Dim IntCount As Integer = CInt(GF.Decrypt(Request.QueryString.Get("counts")))
            Dim StrFromDate As Date = CDate(Request.QueryString.Get("frmdate"))
            Dim StrToDate As Date = CDate(Request.QueryString.Get("todate"))
            Dim Struser As String = Session("UserID").ToString()
            Dim StrUserNam As String = Nothing
            Dim SqlStr As String
            DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
            If DTT.Rows.Count > 0 Then
                StrUserNam = DTT.Rows(0)(0)
            End If
            DT = DB.ExecuteDataSet("select team_id from HD_TEAM_DTL where emp_code= " & Struser & "").Tables(0)
            If DT.Rows.Count > 0 Then
                If DT.Rows(0)(0) = 4 Then 'help desk
                    SqlStr = "select a.TICKET_NO,a.REQUEST_DT,datediff(day,a.REQUEST_DT,case when a.status_id in (0,4,11,12,13,19) then case when a.T_RESOLVED_DT IS not null then a.T_RESOLVED_DT else case when a.CLS_DT_BEFORE_AUTOCLOSE is not null then a.CLS_DT_BEFORE_AUTOCLOSE  else case when a.CLOSE_DT IS not null then a.CLOSE_DT else a.request_dt end end  end  else getdate() end)  Lag_Days, br.Branch_Name, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS, " & _
                        " em.Emp_Name as assignto, y.up_user,a.URGENT_REQ,a.REQUEST_ID,f.TEAM_NAME from HD_REQUEST_MASTER a left outer join EMP_MASTER em on(a.ASSIGNED_TO=em.Emp_Code) " & _
                        " left outer join (select a.REQUEST_ID as rid ,m.Emp_Name as up_user  from HD_REQUEST_CYCLE a inner join  " & _
                        " (select MAX(order_id) as oid,request_id from HD_REQUEST_CYCLE group by REQUEST_ID) hc on a.REQUEST_ID=hc.REQUEST_ID and a.ORDER_ID=hc.oid  " & _
                        "  left join EMP_MASTER m on   a.user_id=m.Emp_Code )y on (y.rid=a.REQUEST_ID),HD_PROBLEM_TYPE c ,    " & _
                        "  HD_GROUP_MASTER d, HD_SUB_GROUP e,HD_STATUS s ,BRANCH_MASTER br, HD_TEAM_MASTER f  where  a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and  " & _
                        " s.STATUS_ID = a.STATUS_ID And e.group_ID = d.group_ID and br.Branch_ID= a.BRANCH_ID and a.team_id = f.team_id and DATEADD(day,DATEDIFF(day, 0,a.request_dt ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and " & _
                        " '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "' "
                Else
                    'StrFromDate
                    'SqlStr = "select a.TICKET_NO,a.REQUEST_DT,datediff(day,a.REQUEST_DT,case when a.status_id in (0,4,11,12,13,19) then case when a.T_RESOLVED_DT IS not null then a.T_RESOLVED_DT  else case when a.CLOSE_DT IS not null then a.CLOSE_DT else a.request_dt end  end  else getdate() end)  Lag_Days, br.Branch_Name, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS, " & _
                    '    " em.Emp_Name as assignto, y.up_user,a.URGENT_REQ,a.REQUEST_ID,f.TEAM_NAME from HD_REQUEST_MASTER a left outer join EMP_MASTER em on(a.ASSIGNED_TO=em.Emp_Code) " & _
                    '    " left outer join (select a.REQUEST_ID as rid ,m.Emp_Name as up_user  from HD_REQUEST_CYCLE a inner join  " & _
                    '    " (select MAX(order_id) as oid,request_id from HD_REQUEST_CYCLE group by REQUEST_ID) hc on a.REQUEST_ID=hc.REQUEST_ID and a.ORDER_ID=hc.oid  " & _
                    '    "  left join EMP_MASTER m on   a.user_id=m.Emp_Code )y on (y.rid=a.REQUEST_ID),HD_PROBLEM_TYPE c ,    " & _
                    '    "  HD_GROUP_MASTER d, HD_SUB_GROUP e,HD_STATUS s ,BRANCH_MASTER br, HD_TEAM_MASTER f  where  a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and  " & _
                    '    " s.STATUS_ID = a.STATUS_ID And e.group_ID = d.group_ID and br.Branch_ID= a.BRANCH_ID and a.team_id = f.team_id and (a.team_id=" & DT.Rows(0)(0) & " or a.user_id=" + Session("UserID").ToString() + " ) " & _
                    '    " and DATEADD(day,DATEDIFF(day, 0,a.request_dt ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "' and  '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "' "

                    SqlStr = "select a.TICKET_NO,case when reopen_flag >= 1 then convert(varchar(10),hrc.tra_dt,103) else convert(varchar(10),hrc.tra_dt,103) end as [request_date],case when reopen_flag >= 1 then convert(varchar(10),a.REQUEST_DT,108) else convert(varchar(10),a.REQUEST_DT,108) end as [request_time],datediff(day,case when reopen_flag >= 1 then hrc.tra_dt else hrc.tra_dt end,case when a.status_id in (0,4,11,12,13,19) then case when a.T_RESOLVED_DT IS not null then a.T_RESOLVED_DT  else case when a.CLOSE_DT IS not null then a.CLOSE_DT else a.request_dt end  end  else getdate() end)  Lag_Days, br.Branch_Name, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS,  em.Emp_Name as assignto, y.up_user,a.URGENT_REQ,a.REQUEST_ID,f.TEAM_NAME,hvm.vendor_name,convert(varchar(10),hrc1.tra_dt,103) as close_date,a.vendor_ticket_no, convert(varchar(10),hrc2.tra_dt,103) as RESOLVED_DATE from HD_REQUEST_MASTER a  " & _
                        " left outer join EMP_MASTER em on(a.ASSIGNED_TO=em.Emp_Code)  left outer join (select a.REQUEST_ID as rid ,m.Emp_Name as up_user  from HD_REQUEST_CYCLE a 	inner join   (select MAX(order_id) as oid,request_id from HD_REQUEST_CYCLE group by REQUEST_ID) hc on a.REQUEST_ID=hc.REQUEST_ID and a.ORDER_ID=hc.oid     " & _
                        " left join EMP_MASTER m on   a.user_id=m.Emp_Code )y on (y.rid=a.REQUEST_ID) left join hd_vendor_master hvm on hvm.vendor_id = a.vendor_id " & _
                        " left join hd_request_cycle hrc on hrc.request_id = a.request_id and hrc.order_id = (select max(order_id) from hd_request_cycle where request_id = a.request_id and update_id = 1) left join hd_request_cycle hrc1 on hrc1.request_id = a.request_id and hrc1.order_id = (select max(order_id) from hd_request_cycle where request_id = a.request_id and update_id = 0) left join hd_request_cycle hrc2 on hrc2.request_id = a.request_id and hrc2.order_id = (select max(order_id) from hd_request_cycle where request_id = a.request_id and update_id = 4),   " & _
                        " HD_PROBLEM_TYPE c ,      HD_GROUP_MASTER d, HD_SUB_GROUP e,HD_STATUS s ,BRANCH_MASTER br, HD_TEAM_MASTER f  where  a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and   s.STATUS_ID = a.STATUS_ID  " & _
                        " And e.group_ID = d.group_ID and br.Branch_ID= a.BRANCH_ID and a.team_id = f.team_id and (a.team_id=" & DT.Rows(0)(0) & " or a.user_id=" + Session("UserID").ToString() + " ) " & _
                        " and DATEADD(day,DATEDIFF(day, 0,a.request_dt ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "' and  '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "' "

                End If
                'If IntCount <> 1 Then
                '    SqlStr += "   order by a.REQUEST_ID"
                'Else
                '    SqlStr += StrStatus + "   order by a.REQUEST_ID"
                'End If
                SqlStr += StrStatus + "   order by a.REQUEST_ID"
                DT = DB.ExecuteDataSet(SqlStr).Tables(0)
                Dim b As Integer = DT.Rows.Count
                cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
                cmd_Back.Attributes.Add("onclick", "return Exitform()")


                If IntCount <> 1 Then
                    RH.Heading(Session("FirmName"), tb, "TICKET STATUS FROM " + CDate(StrFromDate).ToString("dd MMM yyyy") + " TO " + CDate(StrToDate).ToString("dd MMM yyyy"), 150)
                Else
                    RH.Heading(Session("FirmName"), tb, "TICKET STATUS - ( " & Strst & " ) FROM " + CDate(StrFromDate).ToString("dd MMM yyyy") + " TO " + CDate(StrToDate).ToString("dd MMM yyyy"), 150)
                End If


                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")


                '
                Dim TRSHead As New TableRow
                TRSHead.Width = "100"
                TRSHead.BorderWidth = "1"
                TRSHead.BorderStyle = BorderStyle.Solid
                TRSHead.Style.Add("background-color", "lightsteelblue")

                Dim TRSHead_00 As New TableCell
                TRSHead_00.BorderWidth = "1"
                TRSHead_00.BorderColor = Drawing.Color.Silver
                TRSHead_00.BorderStyle = BorderStyle.Solid

                Dim TRHead_1 As New TableRow
                If IntCount <> 0 Then
                    RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)
                Else
                    RH.AddColumn(TRSHead, TRSHead_00, 100, 100, "c", "USER :" + StrUserNam)
                End If

                RH.BlankRow(tb, 4)
                tb.Controls.Add(TRSHead)
                If IntCount <> 1 Then



                    'TRHead.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14 As New TableCell

                    TRHead_1_00.BorderWidth = "1"
                    TRHead_1_01.BorderWidth = "1"
                    TRHead_1_02.BorderWidth = "1"
                    TRHead_1_03.BorderWidth = "1"
                    TRHead_1_04.BorderWidth = "1"
                    TRHead_1_05.BorderWidth = "1"
                    TRHead_1_06.BorderWidth = "1"
                    TRHead_1_07.BorderWidth = "1"
                    TRHead_1_08.BorderWidth = "1"
                    TRHead_1_09.BorderWidth = "1"
                    TRHead_1_10.BorderWidth = "1"
                    TRHead_1_11.BorderWidth = "1"
                    TRHead_1_12.BorderWidth = "1"
                    TRHead_1_13.BorderWidth = "1"
                    TRHead_1_14.BorderWidth = "1"

                    TRHead_1_00.BorderColor = Drawing.Color.Silver
                    TRHead_1_01.BorderColor = Drawing.Color.Silver
                    TRHead_1_02.BorderColor = Drawing.Color.Silver
                    TRHead_1_03.BorderColor = Drawing.Color.Silver
                    TRHead_1_04.BorderColor = Drawing.Color.Silver
                    TRHead_1_05.BorderColor = Drawing.Color.Silver
                    TRHead_1_06.BorderColor = Drawing.Color.Silver
                    TRHead_1_07.BorderColor = Drawing.Color.Silver
                    TRHead_1_08.BorderColor = Drawing.Color.Silver
                    TRHead_1_09.BorderColor = Drawing.Color.Silver
                    TRHead_1_10.BorderColor = Drawing.Color.Silver
                    TRHead_1_11.BorderColor = Drawing.Color.Silver
                    TRHead_1_12.BorderColor = Drawing.Color.Silver
                    TRHead_1_13.BorderColor = Drawing.Color.Silver
                    TRHead_1_14.BorderColor = Drawing.Color.Silver

                    TRHead_1_00.BorderStyle = BorderStyle.Solid
                    TRHead_1_01.BorderStyle = BorderStyle.Solid
                    TRHead_1_02.BorderStyle = BorderStyle.Solid
                    TRHead_1_03.BorderStyle = BorderStyle.Solid
                    TRHead_1_04.BorderStyle = BorderStyle.Solid
                    TRHead_1_05.BorderStyle = BorderStyle.Solid
                    TRHead_1_06.BorderStyle = BorderStyle.Solid
                    TRHead_1_07.BorderStyle = BorderStyle.Solid
                    TRHead_1_08.BorderStyle = BorderStyle.Solid
                    TRHead_1_09.BorderStyle = BorderStyle.Solid
                    TRHead_1_10.BorderStyle = BorderStyle.Solid
                    TRHead_1_11.BorderStyle = BorderStyle.Solid
                    TRHead_1_12.BorderStyle = BorderStyle.Solid
                    TRHead_1_13.BorderStyle = BorderStyle.Solid
                    TRHead_1_14.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TRHead_1, TRHead_1_00, 8, 8, "1", "Ticket No")
                    RH.AddColumn(TRHead_1, TRHead_1_01, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRHead_1, TRHead_1_02, 5, 5, "l", "Request Time")
                    RH.AddColumn(TRHead_1, TRHead_1_03, 5, 5, "l", "Lag days")
                    RH.AddColumn(TRHead_1, TRHead_1_04, 8, 8, "l", "Branch Name")
                    RH.AddColumn(TRHead_1, TRHead_1_05, 9, 9, "l", "Findings")
                    RH.AddColumn(TRHead_1, TRHead_1_06, 8, 8, "l", "Status")
                    RH.AddColumn(TRHead_1, TRHead_1_07, 12, 12, "l", "Remark")
                    RH.AddColumn(TRHead_1, TRHead_1_08, 6, 6, "l", "Attended by")
                    RH.AddColumn(TRHead_1, TRHead_1_09, 6, 6, "l", "Last updated")
                    RH.AddColumn(TRHead_1, TRHead_1_10, 6, 6, "l", "Team")
                    RH.AddColumn(TRHead_1, TRHead_1_11, 6, 6, "l", "Vendor")
                    RH.AddColumn(TRHead_1, TRHead_1_12, 5, 5, "l", "Close Date")
                    RH.AddColumn(TRHead_1, TRHead_1_13, 7, 7, "l", "Vendor Ticket No")
                    RH.AddColumn(TRHead_1, TRHead_1_14, 5, 5, "l", "Resolved Date")
                Else

                    Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14 As New TableCell

                    TRHead_1_00.BorderWidth = "1"
                    TRHead_1_01.BorderWidth = "1"
                    TRHead_1_02.BorderWidth = "1"
                    TRHead_1_03.BorderWidth = "1"
                    TRHead_1_04.BorderWidth = "1"
                    TRHead_1_05.BorderWidth = "1"
                    TRHead_1_06.BorderWidth = "1"
                    TRHead_1_07.BorderWidth = "1"
                    TRHead_1_08.BorderWidth = "1"
                    TRHead_1_09.BorderWidth = "1"
                    TRHead_1_10.BorderWidth = "1"
                    TRHead_1_11.BorderWidth = "1"
                    TRHead_1_12.BorderWidth = "1"
                    TRHead_1_13.BorderWidth = "1"
                    TRHead_1_14.BorderWidth = "1"

                    TRHead_1_00.BorderColor = Drawing.Color.Silver
                    TRHead_1_01.BorderColor = Drawing.Color.Silver
                    TRHead_1_02.BorderColor = Drawing.Color.Silver
                    TRHead_1_03.BorderColor = Drawing.Color.Silver
                    TRHead_1_04.BorderColor = Drawing.Color.Silver
                    TRHead_1_05.BorderColor = Drawing.Color.Silver
                    TRHead_1_06.BorderColor = Drawing.Color.Silver
                    TRHead_1_07.BorderColor = Drawing.Color.Silver
                    TRHead_1_08.BorderColor = Drawing.Color.Silver
                    TRHead_1_09.BorderColor = Drawing.Color.Silver
                    TRHead_1_10.BorderColor = Drawing.Color.Silver
                    TRHead_1_11.BorderColor = Drawing.Color.Silver
                    TRHead_1_12.BorderColor = Drawing.Color.Silver
                    TRHead_1_13.BorderColor = Drawing.Color.Silver
                    TRHead_1_14.BorderColor = Drawing.Color.Silver

                    TRHead_1_00.BorderStyle = BorderStyle.Solid
                    TRHead_1_01.BorderStyle = BorderStyle.Solid
                    TRHead_1_02.BorderStyle = BorderStyle.Solid
                    TRHead_1_03.BorderStyle = BorderStyle.Solid
                    TRHead_1_04.BorderStyle = BorderStyle.Solid
                    TRHead_1_05.BorderStyle = BorderStyle.Solid
                    TRHead_1_06.BorderStyle = BorderStyle.Solid
                    TRHead_1_07.BorderStyle = BorderStyle.Solid
                    TRHead_1_08.BorderStyle = BorderStyle.Solid
                    TRHead_1_09.BorderStyle = BorderStyle.Solid
                    TRHead_1_10.BorderStyle = BorderStyle.Solid
                    TRHead_1_11.BorderStyle = BorderStyle.Solid
                    TRHead_1_12.BorderStyle = BorderStyle.Solid
                    TRHead_1_13.BorderStyle = BorderStyle.Solid
                    TRHead_1_14.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TRHead_1, TRHead_1_00, 10, 10, "1", "Ticket No")
                    RH.AddColumn(TRHead_1, TRHead_1_01, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRHead_1, TRHead_1_02, 5, 5, "l", "Request Time")
                    RH.AddColumn(TRHead_1, TRHead_1_03, 5, 5, "l", "Lag days")
                    RH.AddColumn(TRHead_1, TRHead_1_04, 8, 8, "l", "Branch Name")
                    RH.AddColumn(TRHead_1, TRHead_1_05, 10, 10, "l", "Findings")
                    RH.AddColumn(TRHead_1, TRHead_1_06, 12, 12, "l", "Remark")
                    RH.AddColumn(TRHead_1, TRHead_1_07, 7, 7, "l", "Attended by")
                    RH.AddColumn(TRHead_1, TRHead_1_08, 7, 7, "l", "Last updated")
                    RH.AddColumn(TRHead_1, TRHead_1_09, 7, 7, "l", "Team")
                    RH.AddColumn(TRHead_1, TRHead_1_10, 6, 6, "l", "Vendor")
                    RH.AddColumn(TRHead_1, TRHead_1_11, 5, 5, "l", "Close Date")
                    RH.AddColumn(TRHead_1, TRHead_1_12, 9, 9, "l", "Vendor Ticket No")
                    RH.AddColumn(TRHead_1, TRHead_1_13, 5, 5, "l", "Resolved Date")
                End If

                tb.Controls.Add(TRHead_1)

                For Each DR In DT.Rows
                    If IntCount <> 1 Then
                        Dim TR3 As New TableRow
                        TR3.BorderWidth = "1"
                        TR3.BorderStyle = BorderStyle.Solid
                        Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14 As New TableCell
                        TR3_00.BorderWidth = "1"
                        TR3_01.BorderWidth = "1"
                        TR3_02.BorderWidth = "1"
                        TR3_03.BorderWidth = "1"
                        TR3_04.BorderWidth = "1"
                        TR3_05.BorderWidth = "1"
                        TR3_06.BorderWidth = "1"
                        TR3_07.BorderWidth = "1"
                        TR3_08.BorderWidth = "1"
                        TR3_09.BorderWidth = "1"
                        TR3_10.BorderWidth = "1"
                        TR3_11.BorderWidth = "1"
                        TR3_12.BorderWidth = "1"
                        TR3_13.BorderWidth = "1"
                        TR3_14.BorderWidth = "1"

                        TR3_00.BorderColor = Drawing.Color.Silver
                        TR3_01.BorderColor = Drawing.Color.Silver
                        TR3_02.BorderColor = Drawing.Color.Silver
                        TR3_03.BorderColor = Drawing.Color.Silver
                        TR3_04.BorderColor = Drawing.Color.Silver
                        TR3_05.BorderColor = Drawing.Color.Silver
                        TR3_06.BorderColor = Drawing.Color.Silver
                        TR3_07.BorderColor = Drawing.Color.Silver
                        TR3_08.BorderColor = Drawing.Color.Silver
                        TR3_09.BorderColor = Drawing.Color.Silver
                        TR3_10.BorderColor = Drawing.Color.Silver
                        TR3_11.BorderColor = Drawing.Color.Silver
                        TR3_12.BorderColor = Drawing.Color.Silver
                        TR3_13.BorderColor = Drawing.Color.Silver
                        TR3_14.BorderColor = Drawing.Color.Silver

                        TR3_00.BorderStyle = BorderStyle.Solid
                        TR3_01.BorderStyle = BorderStyle.Solid
                        TR3_02.BorderStyle = BorderStyle.Solid
                        TR3_03.BorderStyle = BorderStyle.Solid
                        TR3_04.BorderStyle = BorderStyle.Solid
                        TR3_05.BorderStyle = BorderStyle.Solid
                        TR3_06.BorderStyle = BorderStyle.Solid
                        TR3_07.BorderStyle = BorderStyle.Solid
                        TR3_08.BorderStyle = BorderStyle.Solid
                        TR3_09.BorderStyle = BorderStyle.Solid
                        TR3_10.BorderStyle = BorderStyle.Solid
                        TR3_11.BorderStyle = BorderStyle.Solid
                        TR3_12.BorderStyle = BorderStyle.Solid
                        TR3_13.BorderStyle = BorderStyle.Solid
                        TR3_14.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TR3, TR3_00, 8, 8, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(13).ToString) + "' target='_blank'>" + DR(0).ToString())
                        RH.AddColumn(TR3, TR3_01, 5, 5, "l", DR(1).ToString())
                        RH.AddColumn(TR3, TR3_02, 5, 5, "l", DR(2).ToString())
                        RH.AddColumn(TR3, TR3_03, 5, 5, "l", DR(3).ToString())
                        RH.AddColumn(TR3, TR3_04, 8, 8, "l", DR(4).ToString())
                        RH.AddColumn(TR3, TR3_05, 9, 9, "l", DR(7).ToString())
                        Dim strstat As String = DR(8).ToString()
                        RH.AddColumn(TR3, TR3_06, 8, 8, "l", strstat)
                        RH.AddColumn(TR3, TR3_07, 12, 12, "l", DR(9).ToString())
                        RH.AddColumn(TR3, TR3_08, 6, 6, "l", DR(10).ToString())
                        RH.AddColumn(TR3, TR3_09, 6, 6, "l", DR(11).ToString())
                        RH.AddColumn(TR3, TR3_10, 6, 6, "l", DR(14).ToString())
                        RH.AddColumn(TR3, TR3_11, 6, 6, "l", DR(15).ToString())
                        RH.AddColumn(TR3, TR3_12, 5, 5, "l", DR(16).ToString())
                        RH.AddColumn(TR3, TR3_13, 7, 7, "l", DR(17).ToString())
                        RH.AddColumn(TR3, TR3_14, 5, 5, "l", DR(18).ToString())
                        If DR(12) = 1 Then
                            TR3_00.Style.Add("background-color", "#F3C2C2")
                            TR3_01.Style.Add("background-color", "#F3C2C2")
                            TR3_02.Style.Add("background-color", "#F3C2C2")
                            TR3_03.Style.Add("background-color", "#F3C2C2")
                            TR3_04.Style.Add("background-color", "#F3C2C2")
                            TR3_05.Style.Add("background-color", "#F3C2C2")
                            TR3_06.Style.Add("background-color", "#F3C2C2")
                            TR3_07.Style.Add("background-color", "#F3C2C2")
                            TR3_08.Style.Add("background-color", "#F3C2C2")
                            TR3_09.Style.Add("background-color", "#F3C2C2")
                            TR3_10.Style.Add("background-color", "#F3C2C2")
                            TR3_11.Style.Add("background-color", "#F3C2C2")
                        End If

                        tb.Controls.Add(TR3)
                    Else

                        Dim TR4 As New TableRow
                        TR4.BorderWidth = "1"
                        TR4.BorderStyle = BorderStyle.Solid
                        Dim TR4_00, TR4_01, TR4_02, TR4_03, TR4_04, TR4_05, TR4_06, TR4_07, TR4_08, TR4_09, TR4_10, TR4_11, TR4_12, TR4_13, TR4_14 As New TableCell
                        TR4_00.BorderWidth = "1"
                        TR4_01.BorderWidth = "1"
                        TR4_02.BorderWidth = "1"
                        TR4_03.BorderWidth = "1"
                        TR4_04.BorderWidth = "1"
                        TR4_05.BorderWidth = "1"
                        TR4_06.BorderWidth = "1"
                        TR4_07.BorderWidth = "1"
                        TR4_08.BorderWidth = "1"
                        TR4_09.BorderWidth = "1"
                        TR4_10.BorderWidth = "1"
                        TR4_11.BorderWidth = "1"
                        TR4_12.BorderWidth = "1"
                        TR4_13.BorderWidth = "1"
                        TR4_14.BorderWidth = "1"

                        TR4_00.BorderColor = Drawing.Color.Silver
                        TR4_01.BorderColor = Drawing.Color.Silver
                        TR4_02.BorderColor = Drawing.Color.Silver
                        TR4_03.BorderColor = Drawing.Color.Silver
                        TR4_04.BorderColor = Drawing.Color.Silver
                        TR4_05.BorderColor = Drawing.Color.Silver
                        TR4_06.BorderColor = Drawing.Color.Silver
                        TR4_07.BorderColor = Drawing.Color.Silver
                        TR4_08.BorderColor = Drawing.Color.Silver
                        TR4_09.BorderColor = Drawing.Color.Silver
                        TR4_10.BorderColor = Drawing.Color.Silver
                        TR4_11.BorderColor = Drawing.Color.Silver
                        TR4_12.BorderColor = Drawing.Color.Silver
                        TR4_13.BorderColor = Drawing.Color.Silver
                        TR4_14.BorderColor = Drawing.Color.Silver

                        TR4_00.BorderStyle = BorderStyle.Solid
                        TR4_01.BorderStyle = BorderStyle.Solid
                        TR4_02.BorderStyle = BorderStyle.Solid
                        TR4_03.BorderStyle = BorderStyle.Solid
                        TR4_04.BorderStyle = BorderStyle.Solid
                        TR4_05.BorderStyle = BorderStyle.Solid
                        TR4_06.BorderStyle = BorderStyle.Solid
                        TR4_07.BorderStyle = BorderStyle.Solid
                        TR4_08.BorderStyle = BorderStyle.Solid
                        TR4_09.BorderStyle = BorderStyle.Solid
                        TR4_10.BorderStyle = BorderStyle.Solid
                        TR4_11.BorderStyle = BorderStyle.Solid
                        TR4_12.BorderStyle = BorderStyle.Solid
                        TR4_13.BorderStyle = BorderStyle.Solid
                        TR4_14.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TR4, TR4_00, 10, 10, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(13).ToString) + "' target='_blank'>" + DR(0).ToString())
                        RH.AddColumn(TR4, TR4_01, 5, 5, "l", DR(1).ToString())
                        RH.AddColumn(TR4, TR4_02, 5, 5, "l", DR(2).ToString())
                        RH.AddColumn(TR4, TR4_03, 5, 5, "l", DR(3).ToString())
                        RH.AddColumn(TR4, TR4_04, 8, 8, "l", DR(4).ToString())
                        RH.AddColumn(TR4, TR4_05, 10, 10, "l", DR(7).ToString())
                        RH.AddColumn(TR4, TR4_06, 12, 12, "l", DR(9).ToString())
                        RH.AddColumn(TR4, TR4_07, 7, 7, "l", DR(10).ToString())
                        RH.AddColumn(TR4, TR4_08, 7, 7, "l", DR(11).ToString())
                        RH.AddColumn(TR4, TR4_09, 7, 7, "l", DR(14).ToString())
                        RH.AddColumn(TR4, TR4_10, 6, 6, "l", DR(15).ToString())
                        RH.AddColumn(TR4, TR4_11, 5, 5, "l", DR(16).ToString())
                        RH.AddColumn(TR4, TR4_12, 9, 9, "l", DR(17).ToString())
                        RH.AddColumn(TR4, TR4_13, 5, 5, "l", DR(18).ToString())
                        If DR(12) = 1 Then
                            TR4_00.Style.Add("background-color", "#F3C2C2")
                            TR4_01.Style.Add("background-color", "#F3C2C2")
                            TR4_02.Style.Add("background-color", "#F3C2C2")
                            TR4_03.Style.Add("background-color", "#F3C2C2")
                            TR4_04.Style.Add("background-color", "#F3C2C2")
                            TR4_05.Style.Add("background-color", "#F3C2C2")
                            TR4_06.Style.Add("background-color", "#F3C2C2")
                            TR4_07.Style.Add("background-color", "#F3C2C2")
                            TR4_08.Style.Add("background-color", "#F3C2C2")
                            TR4_09.Style.Add("background-color", "#F3C2C2")
                            TR4_10.Style.Add("background-color", "#F3C2C2")
                        End If
                        tb.Controls.Add(TR4)
                    End If
                Next
                RH.BlankRow(tb, 30)
                pnDisplay.Controls.Add(tb)
            Else
                If Session("Post_ID") = 5 Then
                    SqlStr = "select a.TICKET_NO,a.REQUEST_DT, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS,a.REQUEST_ID,datediff(day,a.REQUEST_DT,getdate()) Lag_Days,f.TEAM_NAME " & _
                                " from HD_REQUEST_MASTER a,HD_PROBLEM_TYPE c,HD_GROUP_MASTER d, " & _
                                " HD_SUB_GROUP e,HD_STATUS s, HD_TEAM_MASTER f  where  a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and  " & _
                                " s.STATUS_ID = a.STATUS_ID And e.group_ID = d.group_ID and a.team_id = f.team_id and (a.branch_id=" & Session("branchID").ToString() & " or a.user_ID= " + Session("UserID").ToString() + ") " & _
                                " and DATEADD(day,DATEDIFF(day, 0,a.request_dt ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "' and  '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "' and a.USER_ID = " + Struser + " "
                Else
                    SqlStr = "select a.TICKET_NO,a.REQUEST_DT, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS,a.REQUEST_ID," +
                    " datediff(day,a.REQUEST_DT,getdate()) Lag_Days,f.TEAM_NAME  " +
                    " from HD_REQUEST_MASTER a,HD_PROBLEM_TYPE c,HD_GROUP_MASTER d,  HD_SUB_GROUP e,HD_STATUS s, HD_TEAM_MASTER f  , HD_request_cycle g" +
                    " where a.PROBLEM_ID = c.PROBLEM_ID And c.sub_Group_Id = e.sub_Group_ID And s.STATUS_ID = a.STATUS_ID And a.USER_ID = " + Struser + "" +
                    " And e.group_ID = d.group_ID  and DATEADD(day,DATEDIFF(day, 0,a.request_dt ),0)  " +
                    " between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "' and  '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "' and a.request_id = g.request_id  and g.team_id = f.team_id " +
                    " group by a.TICKET_NO,a.REQUEST_DT, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS,a.REQUEST_ID," +
                    " a.REQUEST_DT,f.TEAM_NAME  "

                    'SqlStr = "select a.TICKET_NO,a.REQUEST_DT, d.group_Name,e.Sub_Group_Name ,c.PROBLEM,s.STATUS_NAME,a.REMARKS,a.REQUEST_ID,datediff(day,a.REQUEST_DT,getdate()) Lag_Days,f.TEAM_NAME " & _
                    '         " from HD_REQUEST_MASTER a,HD_PROBLEM_TYPE c,HD_GROUP_MASTER d, " & _
                    '         " HD_SUB_GROUP e,HD_STATUS s, HD_TEAM_MASTER f  where  a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and  " & _
                    '         " s.STATUS_ID = a.STATUS_ID And e.group_ID = d.group_ID  and a.team_id = f.team_id " & _
                    '         " and DATEADD(day,DATEDIFF(day, 0,a.request_dt ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "' and  '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "' and a.USER_ID = " + Struser + " "
                End If
                If IntCount <> 1 Then
                    SqlStr += "   order by a.REQUEST_ID"
                Else
                    SqlStr += StrStatus + "   order by a.REQUEST_ID"
                End If
                DT = DB.ExecuteDataSet(SqlStr).Tables(0)

                cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
                cmd_Back.Attributes.Add("onclick", "return Exitform()")


                If IntCount <> 1 Then
                    RH.Heading(Session("FirmName"), tb, "TICKET STATUS FROM " + CDate(StrFromDate).ToString("dd MMM yyyy") + " TO " + CDate(StrToDate).ToString("dd MMM yyyy"), 150)
                Else
                    RH.Heading(Session("FirmName"), tb, "TICKET STATUS - ( " & Strst & " ) FROM " + CDate(StrFromDate).ToString("dd MMM yyyy") + " TO " + CDate(StrToDate).ToString("dd MMM yyyy"), 150)
                End If

                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")



                'RH.BlankRow(tb, 4)
                Dim TRSHead As New TableRow
                TRSHead.BorderWidth = "1"
                TRSHead.BorderStyle = BorderStyle.Solid
                TRSHead.Style.Add("background-color", "lightsteelblue")
                Dim TRSHead_00 As New TableCell
                TRSHead_00.BorderWidth = "1"
                TRSHead_00.BorderColor = Drawing.Color.Silver
                TRSHead_00.BorderStyle = BorderStyle.Solid



                RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "R", "USER :" + StrUserNam)
                tb.Controls.Add(TRSHead)
                RH.BlankRow(tb, 4)


                Dim TRHead_1 As New TableRow
                'TRHead.BackColor = Drawing.Color.WhiteSmoke
                If IntCount <> 1 Then
                    Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08 As New TableCell

                    TRHead_1_00.BorderWidth = "1"
                    TRHead_1_01.BorderWidth = "1"
                    TRHead_1_02.BorderWidth = "1"
                    TRHead_1_03.BorderWidth = "1"
                    TRHead_1_04.BorderWidth = "1"
                    TRHead_1_05.BorderWidth = "1"
                    TRHead_1_06.BorderWidth = "1"
                    TRHead_1_07.BorderWidth = "1"
                    TRHead_1_08.BorderWidth = "1"

                    TRHead_1_00.BorderColor = Drawing.Color.Silver
                    TRHead_1_01.BorderColor = Drawing.Color.Silver
                    TRHead_1_02.BorderColor = Drawing.Color.Silver
                    TRHead_1_03.BorderColor = Drawing.Color.Silver
                    TRHead_1_04.BorderColor = Drawing.Color.Silver
                    TRHead_1_05.BorderColor = Drawing.Color.Silver
                    TRHead_1_06.BorderColor = Drawing.Color.Silver
                    TRHead_1_07.BorderColor = Drawing.Color.Silver
                    TRHead_1_08.BorderColor = Drawing.Color.Silver

                    TRHead_1_00.BorderStyle = BorderStyle.Solid
                    TRHead_1_01.BorderStyle = BorderStyle.Solid
                    TRHead_1_02.BorderStyle = BorderStyle.Solid
                    TRHead_1_03.BorderStyle = BorderStyle.Solid
                    TRHead_1_04.BorderStyle = BorderStyle.Solid
                    TRHead_1_05.BorderStyle = BorderStyle.Solid
                    TRHead_1_06.BorderStyle = BorderStyle.Solid
                    TRHead_1_07.BorderStyle = BorderStyle.Solid
                    TRHead_1_08.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TRHead_1, TRHead_1_00, 12, 12, "1", "Ticket No")
                    RH.AddColumn(TRHead_1, TRHead_1_01, 7, 7, "l", "Date")
                    RH.AddColumn(TRHead_1, TRHead_1_02, 6, 6, "l", "Lag Days")
                    RH.AddColumn(TRHead_1, TRHead_1_05, 23, 23, "l", "Findings")
                    RH.AddColumn(TRHead_1, TRHead_1_06, 14, 14, "l", "Status")
                    RH.AddColumn(TRHead_1, TRHead_1_07, 28, 28, "l", "Remark")
                    RH.AddColumn(TRHead_1, TRHead_1_08, 10, 10, "l", "Team")

                    tb.Controls.Add(TRHead_1)

                    For Each DR In DT.Rows
                        Dim TR3 As New TableRow
                        TR3.BorderWidth = "1"
                        TR3.BorderStyle = BorderStyle.Solid


                        Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08 As New TableCell

                        TR3_00.BorderWidth = "1"
                        TR3_01.BorderWidth = "1"
                        TR3_02.BorderWidth = "1"
                        TR3_03.BorderWidth = "1"
                        TR3_04.BorderWidth = "1"
                        TR3_05.BorderWidth = "1"
                        TR3_06.BorderWidth = "1"
                        TR3_07.BorderWidth = "1"
                        TR3_08.BorderWidth = "1"

                        TR3_00.BorderColor = Drawing.Color.Silver
                        TR3_01.BorderColor = Drawing.Color.Silver
                        TR3_02.BorderColor = Drawing.Color.Silver
                        TR3_03.BorderColor = Drawing.Color.Silver
                        TR3_04.BorderColor = Drawing.Color.Silver
                        TR3_05.BorderColor = Drawing.Color.Silver
                        TR3_06.BorderColor = Drawing.Color.Silver
                        TR3_07.BorderColor = Drawing.Color.Silver
                        TR3_08.BorderColor = Drawing.Color.Silver

                        TR3_00.BorderStyle = BorderStyle.Solid
                        TR3_01.BorderStyle = BorderStyle.Solid
                        TR3_02.BorderStyle = BorderStyle.Solid
                        TR3_03.BorderStyle = BorderStyle.Solid
                        TR3_04.BorderStyle = BorderStyle.Solid
                        TR3_05.BorderStyle = BorderStyle.Solid
                        TR3_06.BorderStyle = BorderStyle.Solid
                        TR3_07.BorderStyle = BorderStyle.Solid
                        TR3_08.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TR3, TR3_00, 12, 10, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(7).ToString) + "' target='_blank'>" + DR(0).ToString())
                        RH.AddColumn(TR3, TR3_01, 7, 7, "l", CDate(DR(1)).ToString("dd/MM/yyyy"))
                        RH.AddColumn(TR3, TR3_02, 6, 6, "l", DR(8).ToString())
                        RH.AddColumn(TR3, TR3_05, 23, 23, "l", DR(4).ToString())
                        Dim strapp As String = Nothing
                        strapp = DR(5).ToString()
                        RH.AddColumn(TR3, TR3_06, 14, 14, "l", strapp)
                        RH.AddColumn(TR3, TR3_07, 28, 28, "l", DR(6).ToString())
                        RH.AddColumn(TR3, TR3_08, 10, 10, "l", DR(9).ToString())

                        tb.Controls.Add(TR3)
                    Next
                Else
                    Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07 As New TableCell

                    TRHead_1_00.BorderWidth = "1"
                    TRHead_1_01.BorderWidth = "1"
                    TRHead_1_02.BorderWidth = "1"
                    TRHead_1_03.BorderWidth = "1"
                    TRHead_1_04.BorderWidth = "1"
                    TRHead_1_05.BorderWidth = "1"
                    TRHead_1_06.BorderStyle = "1"
                    TRHead_1_07.BorderStyle = "1"

                    TRHead_1_00.BorderColor = Drawing.Color.Silver
                    TRHead_1_01.BorderColor = Drawing.Color.Silver
                    TRHead_1_02.BorderColor = Drawing.Color.Silver
                    TRHead_1_03.BorderColor = Drawing.Color.Silver
                    TRHead_1_04.BorderColor = Drawing.Color.Silver
                    TRHead_1_05.BorderColor = Drawing.Color.Silver
                    TRHead_1_06.BorderColor = Drawing.Color.Silver
                    TRHead_1_07.BorderColor = Drawing.Color.Silver

                    TRHead_1_00.BorderStyle = BorderStyle.Solid
                    TRHead_1_01.BorderStyle = BorderStyle.Solid
                    TRHead_1_02.BorderStyle = BorderStyle.Solid
                    TRHead_1_03.BorderStyle = BorderStyle.Solid
                    TRHead_1_04.BorderStyle = BorderStyle.Solid
                    TRHead_1_05.BorderStyle = BorderStyle.Solid
                    TRHead_1_06.BorderStyle = BorderStyle.Solid
                    TRHead_1_07.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TRHead_1, TRHead_1_00, 16, 16, "1", "Ticket No")
                    RH.AddColumn(TRHead_1, TRHead_1_01, 7, 7, "l", "Date")
                    RH.AddColumn(TRHead_1, TRHead_1_02, 6, 6, "l", "Lag Days")
                    RH.AddColumn(TRHead_1, TRHead_1_05, 29, 29, "l", "Findings")
                    RH.AddColumn(TRHead_1, TRHead_1_06, 32, 32, "l", "Remark")
                    RH.AddColumn(TRHead_1, TRHead_1_07, 10, 10, "l", "Remark")

                    tb.Controls.Add(TRHead_1)

                    For Each DR In DT.Rows
                        Dim TR3 As New TableRow
                        TR3.BorderWidth = "1"
                        TR3.BorderStyle = BorderStyle.Solid


                        Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07 As New TableCell

                        TR3_00.BorderWidth = "1"
                        TR3_01.BorderWidth = "1"
                        TR3_02.BorderWidth = "1"
                        TR3_03.BorderWidth = "1"
                        TR3_04.BorderWidth = "1"
                        TR3_05.BorderWidth = "1"
                        TR3_06.BorderWidth = "1"
                        TR3_07.BorderWidth = "1"

                        TR3_00.BorderColor = Drawing.Color.Silver
                        TR3_01.BorderColor = Drawing.Color.Silver
                        TR3_02.BorderColor = Drawing.Color.Silver
                        TR3_03.BorderColor = Drawing.Color.Silver
                        TR3_04.BorderColor = Drawing.Color.Silver
                        TR3_05.BorderColor = Drawing.Color.Silver
                        TR3_06.BorderColor = Drawing.Color.Silver
                        TR3_07.BorderColor = Drawing.Color.Silver

                        TR3_00.BorderStyle = BorderStyle.Solid
                        TR3_01.BorderStyle = BorderStyle.Solid
                        TR3_02.BorderStyle = BorderStyle.Solid
                        TR3_03.BorderStyle = BorderStyle.Solid
                        TR3_04.BorderStyle = BorderStyle.Solid
                        TR3_05.BorderStyle = BorderStyle.Solid
                        TR3_06.BorderStyle = BorderStyle.Solid
                        TR3_07.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TR3, TR3_00, 16, 16, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(7).ToString) + "' target='_blank'>" + DR(0).ToString())
                        RH.AddColumn(TR3, TR3_01, 7, 7, "l", CDate(DR(1)).ToString("dd/MM/yyyy"))
                        RH.AddColumn(TR3, TR3_02, 6, 6, "l", DR(8).ToString())
                        RH.AddColumn(TR3, TR3_05, 29, 29, "l", DR(4).ToString())
                        RH.AddColumn(TR3, TR3_06, 32, 32, "l", DR(6).ToString())
                        RH.AddColumn(TR3, TR3_06, 10, 10, "l", DR(9).ToString())

                        tb.Controls.Add(TR3)
                    Next
                End If

                RH.BlankRow(tb, 30)
                pnDisplay.Controls.Add(tb)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub

    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        Try
            WebTools.ExporttoExcel(DT, "Ticket")
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            If Response.IsRequestBeingRedirected Then
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
