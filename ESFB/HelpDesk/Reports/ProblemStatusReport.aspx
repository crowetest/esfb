﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="ProblemStatusReport.aspx.vb" Inherits="ProblemStatusReport" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
</head>
<body style="background-color:Black;">

 <script language="javascript" type="text/javascript">
     function PrintPanel() 
     {
         var panel = document.getElementById("<%=pnDisplay.ClientID %>");
         var printWindow = window.open('', '', 'height=400,width=800,location=0,status=0');
         printWindow.document.write('<html><head><title>ESAF</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () 
         {
             printWindow.print();
         }, 500);
         return false;
     }

     function btnExit_onclick() 
     {
         window.open('../../home.aspx','_self');
         return false;
     }
    
     </script>
    
     <form id="form1" runat="server"> 
       
    <div style="width:95%; background-color:white; min-height:750px; margin:0px auto;">
    <div>
     <br /> 
    <table  style="width:75%;margin: 0px auto;font-family:Cambria;" border="1" >
    <tr style="background-color:lightsteelblue;">
    <td style="text-align: center;" colspan="4">Problem Status</td>
    </tr>
    <tr style="background-color:lightsteelblue;">
    <td style="width:25%; text-align: center;">Category</td>
    <td style="width:25%;">
                <asp:DropDownList ID="cmbCategory" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="95%" ForeColor="Black">
                   
                </asp:DropDownList> 
                </td>
    <td style="width:25%; text-align: center;">Priority</td>
    <td style="width:25%;">
                <asp:DropDownList ID="cmbPriority" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="95%" ForeColor="Black">
                    
                </asp:DropDownList> 
                </td>
    </tr>
    <tr style="background-color:lightsteelblue;">
    <td style="width:25%; text-align: center;">Status</td>
    <td style="width:25%;">
                <asp:DropDownList ID="cmbStatus" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="95%" ForeColor="Black">
                    
                </asp:DropDownList> 
                </td>
    <td style="width:25%; text-align: center;">Group By</td>
    <td style="width:25%;">
                <asp:DropDownList ID="cmbGroupBy" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="95%" ForeColor="Black">
                    <asp:ListItem Value="2"> STATUS</asp:ListItem>
                    <asp:ListItem Value="0"> CATEGORY</asp:ListItem>
                    <asp:ListItem Value="1"> PRIORITY</asp:ListItem>
                    
                </asp:DropDownList> 
                </td>
    </tr>
    <tr style="background-color:lightsteelblue;">
    <td style="text-align: center;" colspan="4">
                <asp:Button ID="btnGenerate" runat="server" Text="GENERATE" Font-Names="Cambria" style="margin-bottom: 0px"/>
                &nbsp;&nbsp; 
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
    </tr>
    <tr><td colspan="4"><asp:Panel ID="pnDisplay"  style="text-align:center;"  runat="server" Width="100%">
            </asp:Panel></td></tr>
    </table>
     
    </div>
       <br style="background-color:white"/>
        <div style="text-align:center;margin:0px auto; width:95%; background-color:white; font-family:Cambria">
        <table style="width:100%;">
        <tr>
        <td style="width:20%;"></td>
        <td style="width:80%;">
         
        </td>
        </tr>
        </table>           
        </div>
    </div>
    </form>
</body>
</html>
</asp:Content>
