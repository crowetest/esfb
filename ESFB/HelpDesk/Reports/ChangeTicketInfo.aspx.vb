﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ChangeTicketInfo
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim DT As New DataTable
    Dim MainTable, LeftTable, RightTable, DownTable As New Table
    Dim SQL As String
    Dim ChangeNo As String
    Dim GF As New GeneralFunctions
    Private Sub ActivityLog()
        Try
            SQL = "SELECT A.TRA_DT,B.STATUS,C.Emp_Name FROM HD_CHANGE_CYCLE A,HD_CHANGE_STATUS B,EMP_MASTER C WHERE A.CHANGE_NO = '" & ChangeNo & "' AND A.STATUS_ID = B.STATUS_ID AND A.USER_ID = C.Emp_Code ORDER BY A.TRA_DT"
            DT = DB.ExecuteDataSet(SQL).Tables(0)
            RH.SubHeading(RightTable, 100, "center", "ACTIVITY LOG")
            RH.BlankRow(RightTable, 4)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02 As New TableCell
            RH.AddColumn(TRHead, TRHead_00, 25, 25, "l", "Date")
            RH.AddColumn(TRHead, TRHead_01, 50, 50, "l", "Status")
            RH.AddColumn(TRHead, TRHead_02, 25, 25, "l", "Done By")
            RightTable.Controls.Add(TRHead)
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                Dim TR3_00, TR3_01, TR3_02 As New TableCell
                RH.AddColumn(TR3, TR3_00, 25, 25, "l", CDate(DR(0)).ToString("dd/MM/yyyy"))
                RH.AddColumn(TR3, TR3_01, 50, 50, "l", DR(1))
                RH.AddColumn(TR3, TR3_02, 25, 25, "l", DR(2))
                RightTable.Controls.Add(TR3)
            Next
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Private Sub LeftTableFill(ByVal ChangeNo As String, ByVal HdTicketNo As String, ByVal Change As String, ByVal Reason As String, ByVal CreateDate As String, ByVal Type As String, ByVal Priority As String, ByVal Status As String, ByVal ProposedStartDate As String, ByVal ProposedEndDate As String, ByVal ActualStartDate As String, ByVal ActualEndDate As String, ByVal AssetDescr As String, ByVal ImplementationTeam As String, ByVal Request_ID As Integer)
        Try
            Dim TR00 As New TableRow
            Dim TR00_1, TR00_2 As New TableCell
            RH.InsertColumn(TR00, TR00_1, 28, 0, "Change Ticket No")
            RH.InsertColumn(TR00, TR00_2, 72, 0, ChangeNo)
            LeftTable.Controls.Add(TR00)
            Dim TR01 As New TableRow
            Dim TR01_1, TR01_2 As New TableCell
            RH.InsertColumn(TR01, TR01_1, 28, 0, "Helpdesk Ticket No")
            RH.InsertColumn(TR01, TR01_2, 72, 0, "<a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(Request_ID.ToString) + "' target=_blank>" + HdTicketNo + "</a>")
            LeftTable.Controls.Add(TR01)
            Dim TR02 As New TableRow
            Dim TR02_1, TR02_2 As New TableCell
            RH.InsertColumn(TR02, TR02_1, 28, 0, "Ticket Created On")
            RH.InsertColumn(TR02, TR02_2, 72, 0, CDate(CreateDate).ToString("dd/MM/yyyy"))
            LeftTable.Controls.Add(TR02)
            Dim TR03 As New TableRow
            Dim TR03_1, TR03_2 As New TableCell
            RH.InsertColumn(TR03, TR03_1, 28, 0, "Change Description")
            RH.InsertColumn(TR03, TR03_2, 72, 0, Change)
            LeftTable.Controls.Add(TR03)
            Dim TR04 As New TableRow
            Dim TR04_1, TR04_2 As New TableCell
            RH.InsertColumn(TR04, TR04_1, 28, 0, "Reason For Change")
            RH.InsertColumn(TR04, TR04_2, 72, 0, Reason)
            LeftTable.Controls.Add(TR04)
            Dim TR05 As New TableRow
            Dim TR05_1, TR05_2 As New TableCell
            RH.InsertColumn(TR05, TR05_1, 28, 0, "Type Of Change")
            RH.InsertColumn(TR05, TR05_2, 72, 0, Type)
            LeftTable.Controls.Add(TR05)
            Dim TR06 As New TableRow
            Dim TR06_1, TR06_2 As New TableCell
            RH.InsertColumn(TR06, TR06_1, 28, 0, "Change Priority")
            RH.InsertColumn(TR06, TR06_2, 72, 0, Priority)
            LeftTable.Controls.Add(TR06)
            Dim TR07 As New TableRow
            Dim TR07_1, TR07_2, TR07_3 As New TableCell
            RH.InsertColumn(TR07, TR07_1, 28, 0, "<b>Ticket Status</b>")
            RH.InsertColumn(TR07, TR07_2, 72, 0, "<b>" + Status + "</b>")
            LeftTable.Controls.Add(TR07)
            Dim TR08 As New TableRow
            Dim TR08_1, TR08_2 As New TableCell
            RH.InsertColumn(TR08, TR08_1, 28, 0, "Proposed Implementation Period")
            RH.InsertColumn(TR08, TR08_2, 72, 0, ProposedStartDate + "  -  " + ProposedEndDate)
            LeftTable.Controls.Add(TR08)
            Dim TR09 As New TableRow
            Dim TR09_1, TR09_2 As New TableCell
            RH.InsertColumn(TR09, TR09_1, 28, 0, "Actual Implementation Date")
            RH.InsertColumn(TR09, TR09_2, 72, 0, ActualStartDate + "  -  " + ActualEndDate)
            LeftTable.Controls.Add(TR09)
            Dim TR10 As New TableRow
            Dim TR10_1, TR10_2 As New TableCell
            RH.InsertColumn(TR10, TR10_1, 28, 0, "Asset Description")
            RH.InsertColumn(TR10, TR10_2, 72, 0, AssetDescr)
            LeftTable.Controls.Add(TR10)
            Dim TR21 As New TableRow
            Dim TR21_1, TR21_2 As New TableCell
            RH.InsertColumn(TR21, TR21_1, 28, 0, "Implementation Team")
            RH.InsertColumn(TR21, TR21_2, 72, 0, ImplementationTeam)
            LeftTable.Controls.Add(TR21)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Private Sub BottomTableFill(ByVal PrePlan As String, ByVal ImplementationPlan As String, ByVal VerificationPlan As String, ByVal RollbackPlan As String, ByVal AffectWhom As String, ByVal DownTime As String, ByVal ReviewDescr As String)
        Try
            RH.BlankRow(MainTable, 5)
            Dim TR11 As New TableRow
            Dim TR11_1, TR11_2 As New TableCell
            RH.InsertColumn(TR11, TR11_1, 20, 0, "Pre-Implementation Plan")
            RH.InsertColumn(TR11, TR11_2, 80, 0, PrePlan)
            DownTable.Controls.Add(TR11)
            Dim TR12 As New TableRow
            Dim TR12_1, TR12_2 As New TableCell
            RH.InsertColumn(TR12, TR12_1, 20, 0, "Implementation Plan")
            RH.InsertColumn(TR12, TR12_2, 80, 0, ImplementationPlan)
            DownTable.Controls.Add(TR12)
            Dim TR13 As New TableRow
            Dim TR13_1, TR13_2 As New TableCell
            RH.InsertColumn(TR13, TR13_1, 20, 0, "Verification Plan")
            RH.InsertColumn(TR13, TR13_2, 80, 0, VerificationPlan)
            DownTable.Controls.Add(TR13)
            Dim TR14 As New TableRow
            Dim TR14_1, TR14_2 As New TableCell
            RH.InsertColumn(TR14, TR14_1, 20, 0, "Rollback Plan")
            RH.InsertColumn(TR14, TR14_2, 80, 0, RollbackPlan)
            DownTable.Controls.Add(TR14)
            Dim TR15 As New TableRow
            Dim TR15_1, TR15_2 As New TableCell
            RH.InsertColumn(TR15, TR15_1, 20, 0, "Whom Will Affect")
            RH.InsertColumn(TR15, TR15_2, 80, 0, AffectWhom)
            DownTable.Controls.Add(TR15)
            Dim TR16 As New TableRow
            Dim TR16_1, TR16_2 As New TableCell
            RH.InsertColumn(TR16, TR16_1, 20, 0, "Down Time Needed")
            RH.InsertColumn(TR16, TR16_2, 80, 0, DownTime)
            DownTable.Controls.Add(TR16)
            Dim TR17 As New TableRow
            Dim TR17_1, TR17_2 As New TableCell
            RH.InsertColumn(TR17, TR17_1, 20, 0, "Review Description")
            RH.InsertColumn(TR17, TR17_2, 80, 0, ReviewDescr)
            DownTable.Controls.Add(TR17)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 262) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            ChangeNo = GF.Decrypt(Request.QueryString.Get("ChangeNo"))
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            MainTable.Attributes.Add("width", "100%")
            LeftTable.Attributes.Add("width", "100%")
            RightTable.Attributes.Add("width", "100%")
            DownTable.Attributes.Add("width", "100%")

            SQL = "SELECT A.CHANGE,A.REASON,A.CREATE_DT,B.TYPE_NAME,C.PRIORITY_NAME,D.STATUS + ' - ' + D.PENDING_STATUS,A.PROPOSED_STARTTIME,A.PROPOSED_ENDTIME,A.IMPLEMENTATION_START,A.IMPLEMENTATION_END,ISNULL(A.ASSET_DESCR,''),ISNULL(A.PRE_PLAN,''),ISNULL(A.IMPLEMENTATION_PLAN,''),ISNULL(A.VERIFICATION_PLAN,''),ISNULL(A.ROLLBACK_PLAN,''),ISNULL(A.AFFECT_WHOM,''),isnull(A.REVIEW_DESCR,''),A.HD_TICKET_NO,ISNULL(A.IMPLEMENT_TEAM,''),CONVERT(VARCHAR,A.DOWN_TIME_START) + ' - ' + CONVERT(VARCHAR,A.DOWN_TIME_END) FROM HD_CHANGE_MASTER A,HD_CHANGE_TYPES B,HD_CHANGE_PRIORITY C,HD_CHANGE_STATUS D WHERE A.CHANGE_NO = '" & ChangeNo & "' AND A.TYPE_ID = B.TYPE_ID AND A.PRIORITY_ID = C.PRIORITY_ID AND A.STATUS_ID = D.STATUS_ID"
            DT = DB.ExecuteDataSet(SQL).Tables(0)
            Dim Change As String = CStr(DT.Rows(0)(0))
            Dim Reason As String = CStr(DT.Rows(0)(1))
            Dim CreateDate As String = CStr(DT.Rows(0)(2))
            Dim Type As String = CStr(DT.Rows(0)(3))
            Dim Priority As String = CStr(DT.Rows(0)(4))
            Dim Status As String = CStr(DT.Rows(0)(5))
            Dim ProposedStartDate As String = Nothing
            If Not IsDBNull(DT.Rows(0)(6)) Then
                ProposedStartDate = CStr(DT.Rows(0)(6))
            End If
            Dim ProposedEndDate As String = Nothing
            If Not IsDBNull(DT.Rows(0)(7)) Then
                ProposedEndDate = CStr(DT.Rows(0)(7))
            End If
            Dim ActualStartDate As String = Nothing
            If Not IsDBNull(DT.Rows(0)(8)) Then
                ActualStartDate = CStr(DT.Rows(0)(8))
            End If
            Dim ActualEndDate As String = Nothing
            If Not IsDBNull(DT.Rows(0)(9)) Then
                ActualEndDate = CStr(DT.Rows(0)(9))
            End If
            Dim AssetDescr As String = CStr(DT.Rows(0)(10))
            Dim PrePlan As String = CStr(DT.Rows(0)(11))
            Dim ImplementationPlan As String = CStr(DT.Rows(0)(12))
            Dim VerificationPlan As String = CStr(DT.Rows(0)(13))
            Dim RollbackPlan As String = CStr(DT.Rows(0)(14))
            Dim AffectWhom As String = CStr(DT.Rows(0)(15))
            Dim ReviewDescr As String = CStr(DT.Rows(0)(16))
            Dim HdTicketNo As String = CStr(DT.Rows(0)(17))
            Dim ImplementTeam As String = CStr(DT.Rows(0)(18))
            Dim DownTime As String = Nothing
            If Not IsDBNull(DT.Rows(0)(19)) Then
                DownTime = CStr(DT.Rows(0)(19))
            End If

            RH.Heading(Session("FirmName"), MainTable, "CHANGE TICKET INFORMATION", 100)
            Dim DT_sub As New DataTable
            SQL = "select REQUEST_ID from HD_REQUEST_MASTER where TICKET_NO='" + HdTicketNo + "'"
            DT_sub = DB.ExecuteDataSet(SQL).Tables(0)
            '-- // Fill Left Top Table
            LeftTableFill(ChangeNo, HdTicketNo, Change, Reason, CreateDate, Type, Priority, Status, ProposedStartDate, ProposedEndDate, ActualStartDate, ActualEndDate, AssetDescr, ImplementTeam, CInt(DT_sub.Rows(0)(0)))
            ActivityLog() '-- // Fill Activity Log Table

            Dim MainRow As New TableRow
            Dim LeftCell, RightCell As New TableCell
            RH.InsertColumn(MainRow, LeftCell, 70, 0, "")
            RH.InsertColumn(MainRow, RightCell, 30, 0, "")
            LeftCell.Controls.Add(LeftTable)
            RightCell.Controls.Add(RightTable)
            MainTable.Controls.Add(MainRow)

            BottomTableFill(PrePlan, ImplementationPlan, VerificationPlan, RollbackPlan, AffectWhom, DownTime, ReviewDescr) ' --//-- Down Table

            pnDisplay.Controls.Add(MainTable)
            pnDisplay.Controls.Add(DownTable)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

#Region "Toolbar"
    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub
    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
    
End Class
