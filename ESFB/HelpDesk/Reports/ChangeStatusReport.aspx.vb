﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewCustomizeTicketStatus
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim UserID As Integer
    Dim RH As New WholeHelper.ClsRepCtrl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 262) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            UserID = CInt(Session("UserID"))
            If Not IsPostBack Then
                DT = GN.GetQueryResult("SELECT ' ALL',-1 UNION ALL select UPPER(STATUS),STATUS_ID from HD_CHANGE_STATUS ")
                GN.ComboFill(cmbStatus, DT, 1, 0)
                DT = DB.ExecuteDataSet("SELECT ' ALL',-1 UNION ALL select UPPER(PRIORITY_NAME),PRIORITY_ID from HD_CHANGE_PRIORITY").Tables(0)
                GN.ComboFill(cmbPriority, DT, 1, 0)
                DT = DB.ExecuteDataSet("SELECT ' ALL',-1 UNION ALL select UPPER(TYPE_NAME),TYPE_ID from HD_CHANGE_TYPES").Tables(0)
                GN.ComboFill(cmbType, DT, 1, 0)
                DT = DB.ExecuteDataSet("SELECT ' ALL',-1 UNION ALL select UPPER(CATEGORY),CATEGORY_ID from HD_PROBLEM_CATEGORY").Tables(0)
                GN.ComboFill(cmbCategory, DT, 1, 0)
            End If
            Me.btnGenerate.Attributes.Add("onclick", "return GenerateOnclick()")
            Me.cmbDate.Attributes.Add("onchange", "return DateOnchange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "OnLoad();", True)
            GenerateReport()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Public Sub GenerateReport()
        Try
            pnDisplay.Controls.Clear()
            Dim StatusID As Integer = CInt(Me.cmbStatus.SelectedValue)
            Dim PriorityID As Integer = CInt(Me.cmbPriority.SelectedValue)
            Dim TypeID As Integer = CInt(Me.cmbType.SelectedValue)
            Dim GroupBy As Integer = CInt(Me.cmbGroupBy.SelectedValue)
            Dim DateSelection As Integer = CInt(Me.cmbDate.SelectedValue)
            Dim CategoryID As Integer = CInt(Me.cmbCategory.SelectedValue)

            Dim Heading As String = Me.cmbGroupBy.SelectedItem.Text.ToUpper()
            Dim FromDt As Date
            Dim ToDt As Date
            Dim SQL As String
            Dim Selection As String = ""
            Dim TypeFilter As String = ""
            Dim PriorityFilter As String = ""
            Dim StatusFilter As String = ""
            Dim DateFilter As String = ""
            Dim GroupString As String = ""
            Dim CategoryFilter As String = ""

            Select Case GroupBy
                Case 0 '-- Type
                    Selection = "select a.TYPE_ID,b.TYPE_NAME,COUNT(*) CNT from HD_CHANGE_MASTER a,HD_CHANGE_TYPES b where  a.TYPE_ID = b.TYPE_ID"
                    GroupString = " GROUP BY a.TYPE_ID,b.TYPE_NAME"
                Case 1 '-- Priority
                    Selection = "select a.PRIORITY_ID,b.PRIORITY_NAME,COUNT(*) CNT from HD_CHANGE_MASTER a,HD_CHANGE_PRIORITY b where a.PRIORITY_ID = b.PRIORITY_ID"
                    GroupString = " GROUP BY a.PRIORITY_ID,b.PRIORITY_NAME"
                Case 2 '-- Status
                    Selection = "select a.STATUS_ID,b.STATUS,COUNT(*) CNT from HD_CHANGE_MASTER a,HD_CHANGE_STATUS b where a.STATUS_ID = b.STATUS_ID"
                    GroupString = " GROUP BY a.STATUS_ID,b.STATUS"

            End Select
            If StatusID <> -1 Then
                StatusFilter = " and STATUS_ID = " & StatusID & ""
            End If
            If TypeID <> -1 Then
                TypeFilter = " and a.TYPE_ID = " & TypeID & ""
            End If
            If PriorityID <> -1 Then
                PriorityFilter = " and a.PRIORITY_ID = " & PriorityID & ""
            End If
            If CategoryID <> -1 Then
                CategoryFilter = " and a.category_ID = " & CategoryID & ""
            End If
            If DateSelection = 1 Then
                FromDt = CDate(Me.txtFrom.Text)
                ToDt = CDate(Me.txtTo.Text)
                DateFilter = " and DATEADD(day,DATEDIFF(day, 0,proposed_starttime),0)  >= '" & CDate(FromDt).ToString("MM/dd/yyyy") & "'  and DATEADD(day,DATEDIFF(day, 0,proposed_endtime),0) <= '" & CDate(ToDt).ToString("MM/dd/yyyy") & "'"
            ElseIf DateSelection = 2 Then
                FromDt = CDate(Me.txtFrom.Text)
                ToDt = CDate(Me.txtTo.Text)
                DateFilter = " and DATEADD(day,DATEDIFF(day, 0,implementation_start),0)  >= '" & CDate(FromDt).ToString("MM/dd/yyyy") & "'  and DATEADD(day,DATEDIFF(day, 0,implementation_end),0) <= '" & CDate(ToDt).ToString("MM/dd/yyyy") & "'"
            End If
            SQL = Selection + TypeFilter + PriorityFilter + CategoryFilter + StatusFilter + DateFilter + GroupString
            DT = GN.GetQueryResult(SQL)

            Dim tb As New Table
            RH.Heading("", tb, Heading + " WISE CHANGE TICKET SUMMARY", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell


            RH.InsertColumn(TRHead, TRHead_00, 10, 2, "#")
            RH.InsertColumn(TRHead, TRHead_01, 70, 0, Heading)
            RH.InsertColumn(TRHead, TRHead_02, 20, 2, "COUNT")
            tb.Controls.Add(TRHead)

            Dim i As Integer = 1
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10 As New TableCell


                RH.InsertColumn(TR3, TR3_00, 10, 2, i)
                RH.InsertColumn(TR3, TR3_01, 70, 0, DR(1))
                RH.InsertColumn(TR3, TR3_02, 20, 2, "<a href='ChangeStatusReport_0.aspx?ID=" + GN.Encrypt(DR(0).ToString()) + "&GroupBy=" + GN.Encrypt(GroupBy.ToString()) + "&TypeID=" + GN.Encrypt(TypeID.ToString()) + "&CategoryID=" + GN.Encrypt(CategoryID.ToString()) + "&PriorityID=" + GN.Encrypt(PriorityID.ToString()) + "&StatusID=" + GN.Encrypt(StatusID.ToString()) + "&DateSelection=" + DateSelection.ToString() + "&FromDt=" + FromDt.ToString() + "&ToDt=" + ToDt.ToString() + "'   target='_blank'>" + DR(2).ToString() + "</a>")

                i += 1
                tb.Controls.Add(TR3)
            Next

            RH.BlankRow(tb, 4)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnGenerate_Click(sender As Object, e As System.EventArgs) Handles btnGenerate.Click
        GenerateReport()
    End Sub
End Class
