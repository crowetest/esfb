﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AgingReport.aspx.vb" Inherits="AgingReport" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }  
        .sub_hdRow
        {
         background-color:#EBCCD6; height:20px;
         font-family:Arial; color:#B84D4D; font-size:8.5pt;  font-weight:bold;
        }      
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 57%;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnShow_onclick() 
        {
            var Cateid=document.getElementById("<%= cmbClass.ClientID %>").value ;
            if(document.getElementById("<%= txtFromDt.ClientID %>").value ==""){
                alert("Select From Date");
                document.getElementById("<%= txtFromDt.ClientID %>").focus();
                return false;
            }
            if(document.getElementById("<%= txtToDt.ClientID %>").value =="" ){
                alert("Select To Date");
                document.getElementById("<%= txtToDt.ClientID %>").focus();
                return false;
            }
            var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt   = document.getElementById("<%= txtToDt.ClientID %>").value
            window.open("ViewAgingReport.aspx?CategoryID=" + btoa(Cateid) + "&FromDt=" + btoa(FromDt) + "&ToDt=" + btoa(ToDt) + "" , "_self");
        }
    </script>
</head>
</html>
<asp:HiddenField ID="hid_dtls" runat="server" />
<br /><br /><br />

    
    <div  style="width:50%;margin:0px auto; ">
       <table class="style1" style="width:80%;top:330px auto;"> 
           <tr> 
            <td style="text-align:center; margin:0px auto; width:25%;">
                &nbsp;</td> 
            <td style="text-align:right; margin:0px auto; width:20%;">
                Classification&nbsp;&nbsp;&nbsp;</td> 
            <td style="text-align:left; margin:0px auto; " class="style2">
                <asp:DropDownList ID="cmbClass" class="NormalText" runat="server" Font-Names="Cambria" 
                 Width="50%" ForeColor="Black">                 
                </asp:DropDownList></td>
            
                </tr>

           <tr> 
            <td style="text-align:center; margin:0px auto; width:25%;">
                &nbsp;</td> 
            <td style="text-align:right; margin:0px auto; width:20%;">
                From Date&nbsp;&nbsp;&nbsp;&nbsp; </td> 
            <td style="text-align:left; margin:0px auto; " class="style2">
                    <asp:TextBox ID="txtFromDt" class="NormalText" runat="server" Width="50%" onkeypress="return false"
                        ReadOnly="True"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtFromDt_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtFromDt" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
            
                </tr>

           <tr> 
            <td style="text-align:center; margin:0px auto; width:25%;">
                &nbsp;</td> 
            <td style="text-align:right; margin:0px auto; width:20%;">
                To Date&nbsp;&nbsp;&nbsp;&nbsp; </td> 
            <td style="text-align:left; margin:0px auto; " class="style2">
                <asp:TextBox ID="txtToDt" class="NormalText" runat="server" Width="50%" onkeypress="return false"
                        ReadOnly="True"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtToDt" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
            
                </tr>

        <tr>
            <td style="text-align:center;" colspan="3">
                <br />
                <input id="btnShow" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="SHOW"  onclick="return btnShow_onclick()" />
                 &nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td> </tr>
       
      </table>          
   </div>          
    
   
      
</asp:Content>

