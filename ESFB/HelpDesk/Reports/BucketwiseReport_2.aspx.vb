﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class BucketwiseReport_2
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim RequestID As String
    Dim GroupID, BucketID, FromNo, ToNo As Integer
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 127) = False And GN.FormAccess(CInt(Session("UserID")), 128) And GN.FormAccess(CInt(Session("UserID")), 129) Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            RequestID = GN.Decrypt(Request.QueryString.Get("ReqID"))
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            GroupID = GN.Decrypt(Request.QueryString.Get("CategoryID"))
            BucketID = Request.QueryString.Get("BucketID")
            Dim DT As New DataTable
            Dim SQL As String = Nothing
            If RequestID <> 3 Then
                SQL = "select a.REQUEST_ID,a.TICKET_NO,a.REQUEST_DT,c.Classification_Type,(case when a.REQUEST_GEN_FLAG = 1 then 'Phone' else 'Email' end),b.STATUS_NAME, " & _
             " d.Branch_Name,e.Department_Name,a.EMAIL_ID,i.State_Name,a.CONTACT_NO,f.Group_Name,g.Sub_Group_Name,a.REMARKS,DATEDIFF(DD,a.REQUEST_DT,GETDATE()) Pending, " & _
             "(case when DATEDIFF(DD,a.REQUEST_DT,GETDATE()) = 0 then  '0 Days'  else case when DATEDIFF(DD,a.REQUEST_DT,GETDATE()) between 1 and 5 then  '1-5 Days' " & _
             " else case when DATEDIFF(DD,a.REQUEST_DT,GETDATE()) between 6 and 10 then  '6-10 Days' else case when DATEDIFF(DD,a.REQUEST_DT,GETDATE()) between 11 and 15 then  '11-15 Days' " & _
             " else case when DATEDIFF(DD,a.REQUEST_DT,GETDATE()) between 16 and 20 then  '16-20 Days' " & _
             " else case when DATEDIFF(DD,a.REQUEST_DT,GETDATE()) between 21 and 25 then  '21-25 Days' " & _
             " else case when DATEDIFF(DD,a.REQUEST_DT,GETDATE()) between 26 and 30 then  '26-30 Days' " & _
             " else case when DATEDIFF(DD,a.REQUEST_DT,GETDATE()) > 30 then  'More 30 Days' end end end  end end end end end) Bucket,k.remarks,l.emp_name " & _
             " from HD_REQUEST_MASTER a inner join  HD_STATUS b on a.STATUS_ID = b.STATUS_ID inner join HD_CLASSIFICATION c on a.CLASSIFICATION_ID = c.Type_Id inner join BRANCH_MASTER d on a.BRANCH_ID = d.Branch_ID " & _
             " inner join DEPARTMENT_MASTER e on a.DEPARTMENT_ID = e.Department_ID inner join HD_PROBLEM_TYPE h on a.PROBLEM_ID = h.PROBLEM_ID inner join HD_SUB_GROUP g on h.SUB_GROUP_ID = g.Sub_Group_ID   " & _
             " inner join HD_GROUP_MASTER f on  g.Group_ID = f.Group_ID  left join DISTRICT_MASTER j on d.District_ID = j.District_ID left join STATE_MASTER i on j.State_ID = i.State_ID  " & _
             " inner join hd_request_cycle k on a.request_id=k.request_id and k.order_id=(select max(order_id) from hd_request_cycle where request_id=a.request_id) inner join emp_master l on l.emp_code=k.user_id where a.STATUS_ID = b.STATUS_ID and a.STATUS_ID not in (0,4,11,12,13)  "
            Else
                SQL = "select a.REQUEST_ID,a.TICKET_NO,a.REQUEST_DT,c.Classification_Type,(case when a.REQUEST_GEN_FLAG = 1 then 'Phone' else 'Email' end),b.STATUS_NAME, " & _
             " d.Branch_Name,e.Department_Name,a.EMAIL_ID,i.State_Name,a.CONTACT_NO,f.Group_Name,g.Sub_Group_Name,a.REMARKS,DATEDIFF(DD,isnull(a.ASSIGNED_DATE,a.REQUEST_DT),GETDATE()) Pending, " & _
             "(case when DATEDIFF(DD,isnull(a.ASSIGNED_DATE,a.REQUEST_DT),GETDATE()) = 0 then  '0 Days'  else case when DATEDIFF(DD,isnull(a.ASSIGNED_DATE,a.REQUEST_DT),GETDATE()) between 1 and 5 then  '1-5 Days' " & _
             " else case when DATEDIFF(DD,isnull(a.ASSIGNED_DATE,a.REQUEST_DT),GETDATE()) between 6 and 10 then  '6-10 Days' else case when DATEDIFF(DD,isnull(a.ASSIGNED_DATE,a.REQUEST_DT),GETDATE()) between 11 and 15 then  '11-15 Days' " & _
             " else case when DATEDIFF(DD,isnull(a.ASSIGNED_DATE,a.REQUEST_DT),GETDATE()) between 16 and 20 then  '16-20 Days' " & _
             " else case when DATEDIFF(DD,isnull(a.ASSIGNED_DATE,a.REQUEST_DT),GETDATE()) between 21 and 25 then  '21-25 Days' " & _
             " else case when DATEDIFF(DD,isnull(a.ASSIGNED_DATE,a.REQUEST_DT),GETDATE()) between 26 and 30 then  '26-30 Days' " & _
             " else case when DATEDIFF(DD,isnull(a.ASSIGNED_DATE,a.REQUEST_DT),GETDATE()) > 30 then  'More 30 Days' end end end  end end end end end) Bucket,k.remarks,l.emp_name " & _
             "  from HD_REQUEST_MASTER a inner join  HD_STATUS b on a.STATUS_ID = b.STATUS_ID inner join HD_CLASSIFICATION c on a.CLASSIFICATION_ID = c.Type_Id inner join BRANCH_MASTER d on a.BRANCH_ID = d.Branch_ID" & _
             " inner join DEPARTMENT_MASTER e on a.DEPARTMENT_ID = e.Department_ID inner join HD_PROBLEM_TYPE h on a.PROBLEM_ID = h.PROBLEM_ID inner join HD_SUB_GROUP g on h.SUB_GROUP_ID = g.Sub_Group_ID inner join HD_GROUP_MASTER f on  g.Group_ID = f.Group_ID " & _
             " left join DISTRICT_MASTER j on d.District_ID = j.District_ID left join STATE_MASTER i on j.State_ID = i.State_ID inner join hd_request_cycle k on a.request_id=k.request_id and k.order_id=(select max(order_id) from hd_request_cycle where request_id=a.request_id) inner join emp_master l on l.emp_code=k.user_id where a.STATUS_ID = b.STATUS_ID"

            End If

            Select Case RequestID
                Case 1
                    Select Case BucketID
                        Case 0
                            SQL += " and g.Sub_Group_ID = " & GroupID & " and datediff(d,a.REQUEST_DT,GETDATE()) = " & BucketID & ""
                            RH.Heading(Session("FirmName"), tb, "Group Wise Ageing Report ( 0 Days )", 100)
                        Case 1
                            FromNo = Request.QueryString.Get("From")
                            ToNo = Request.QueryString.Get("To")
                            SQL += " and g.Sub_Group_ID = " & GroupID & " and datediff(d,a.REQUEST_DT,GETDATE()) between  " & FromNo & " and " & ToNo & ""
                            RH.Heading(Session("FirmName"), tb, "Group Wise Ageing Report(" & FromNo.ToString() & " - " & ToNo & " Days)", 100)
                        Case 2
                            SQL += " and g.Sub_Group_ID = " & GroupID & "  and datediff(d,a.REQUEST_DT,GETDATE()) > 30"
                            RH.Heading(Session("FirmName"), tb, "Group Wise Ageing Report (More than 30 Days)", 100)
                        Case 3
                            SQL += " and g.Sub_Group_ID = " & GroupID & " "
                            RH.Heading(Session("FirmName"), tb, "Group Wise Ageing Report ", 100)
                        Case 4
                            FromNo = Request.QueryString.Get("From")
                            ToNo = Request.QueryString.Get("To")
                            SQL += " and datediff(d,a.REQUEST_DT,GETDATE()) between    " & FromNo & " and " & ToNo & ""
                            RH.Heading(Session("FirmName"), tb, "Group Wise Ageing Report ", 100)
                        Case 5
                            SQL += " and datediff(d,a.REQUEST_DT,GETDATE()) > 30"
                            RH.Heading(Session("FirmName"), tb, "Group Wise Ageing Report ", 100)
                        Case 6
                            SQL = SQL
                            RH.Heading(Session("FirmName"), tb, "Group Wise Ageing Report ", 100)
                    End Select
                Case 2
                    Select Case BucketID
                        Case 0
                            SQL += " and a.Vendor_ID = " & GroupID & "  and datediff(d,a.REQUEST_DT,GETDATE()) = " & BucketID & ""
                            RH.Heading(Session("FirmName"), tb, "Vendor Wise Ageing Report ( 0 Days )", 100)
                        Case 1
                            FromNo = Request.QueryString.Get("From")
                            ToNo = Request.QueryString.Get("To")
                            SQL += " and a.Vendor_ID = " & GroupID & "  and datediff(d,a.REQUEST_DT,GETDATE()) between  " & FromNo & " and " & ToNo & ""
                            RH.Heading(Session("FirmName"), tb, "Vendor Wise Ageing Report(" & FromNo.ToString() & " - " & ToNo & " Days)", 100)
                        Case 2
                            SQL += " and a.Vendor_ID = " & GroupID & "  and datediff(d,a.REQUEST_DT,GETDATE()) > 30"
                            RH.Heading(Session("FirmName"), tb, "Vendor Wise Ageing Report (More than 30 Days)", 100)
                        Case 3
                            SQL += " and a.Vendor_ID = " & GroupID & " "
                            RH.Heading(Session("FirmName"), tb, "Vendor Wise Ageing Report", 100)
                        Case 4
                            FromNo = Request.QueryString.Get("From")
                            ToNo = Request.QueryString.Get("To")
                            SQL += "  and datediff(d,a.REQUEST_DT,GETDATE()) between    " & FromNo & " and " & ToNo & " and a.Vendor_id in (select VENDOR_ID from ( select b.VENDOR_ID,b.VENDOR_NAME,datediff(d,a.REQUEST_DT,GETDATE()) Days from HD_REQUEST_MASTER a,HD_VENDOR_MASTER b where a.VENDOR_ID = b.VENDOR_ID and a.STATUS_ID <>0) x group by VENDOR_ID)"
                            RH.Heading(Session("FirmName"), tb, "Vendor Wise Ageing Report", 100)
                        Case 5
                            SQL += " and datediff(d,a.REQUEST_DT,GETDATE()) > 30  and a.Vendor_id in (select VENDOR_ID from ( select b.VENDOR_ID,b.VENDOR_NAME,datediff(d,a.REQUEST_DT,GETDATE()) Days from HD_REQUEST_MASTER a,HD_VENDOR_MASTER b where a.VENDOR_ID = b.VENDOR_ID and a.STATUS_ID <>0) x group by VENDOR_ID)"
                            RH.Heading(Session("FirmName"), tb, "Vendor Wise Ageing Report", 100)
                        Case 6
                            SQL += " and a.Vendor_id in (select VENDOR_ID from ( select b.VENDOR_ID,b.VENDOR_NAME,datediff(d,a.REQUEST_DT,GETDATE()) Days from HD_REQUEST_MASTER a,HD_VENDOR_MASTER b where a.VENDOR_ID = b.VENDOR_ID and a.STATUS_ID <>0) x group by VENDOR_ID)"
                            RH.Heading(Session("FirmName"), tb, "Vendor Wise Ageing Report", 100)

                    End Select

                Case 3
                    Select Case BucketID
                        Case 0
                            If GroupID = 999 Then
                                SQL += "  and a.STATUS_ID in (6,7,8,10,15,16) and a.Team_ID in(0,4) and datediff(d,isnull(a.ASSIGNED_DATE,a.REQUEST_DT) ,GETDATE()) = " & BucketID & ""
                            Else
                                SQL += "  and a.STATUS_ID not in (0,4,11,13,6,7,8,10,15,16,12) and a.Team_ID = " & GroupID & "  and datediff(d,isnull(a.ASSIGNED_DATE,a.REQUEST_DT) ,GETDATE()) = " & BucketID & ""
                            End If
                            RH.Heading(Session("FirmName"), tb, "Team Wise Ageing Report ( 0 Days )", 100)
                        Case 1
                            FromNo = Request.QueryString.Get("From")
                            ToNo = Request.QueryString.Get("To")
                            If GroupID = 999 Then
                                SQL += "  and a.STATUS_ID in (6,7,8,10,15,16) and a.Team_ID in(0,4) and datediff(d,isnull(a.ASSIGNED_DATE,a.REQUEST_DT) ,GETDATE()) between  " & FromNo & " and " & ToNo & ""
                            Else
                                SQL += "  and a.STATUS_ID not in (0,4,11,13,6,7,8,10,15,16,12) and a.Team_ID = " & GroupID & " and datediff(d,isnull(a.ASSIGNED_DATE,a.REQUEST_DT) ,GETDATE()) between  " & FromNo & " and " & ToNo & ""
                            End If
                            RH.Heading(Session("FirmName"), tb, "Team Wise Ageing Report(" & FromNo.ToString() & " - " & ToNo & " Days)", 100)
                        Case 2
                            If GroupID = 999 Then
                                SQL += "  and a.STATUS_ID in (6,7,8,10,15,16) and a.Team_ID in(0,4) and datediff(d,isnull(a.ASSIGNED_DATE,a.REQUEST_DT) ,GETDATE()) > 30"
                            Else
                                SQL += "  and a.STATUS_ID not in (0,4,11,13,6,7,8,10,15,16,12) and a.Team_ID = " & GroupID & "  and datediff(d,isnull(a.ASSIGNED_DATE,a.REQUEST_DT) ,GETDATE()) > 30"
                            End If
                            RH.Heading(Session("FirmName"), tb, "Team Wise Ageing Report (More than 30 Days) ", 100)
                        Case 3
                            If GroupID = 999 Then
                                SQL += "  and a.STATUS_ID in (6,7,8,10,15,16) and a.Team_ID in(0,4) "
                            Else
                                SQL += "  and a.STATUS_ID not in (0,4,11,13,6,7,8,10,15,16,12) and a.Team_ID = " & GroupID & " "
                            End If
                            RH.Heading(Session("FirmName"), tb, "Team Wise Ageing Report (More than 30 Days) ", 100)
                        Case 4
                            FromNo = Request.QueryString.Get("From")
                            ToNo = Request.QueryString.Get("To")
                            SQL += "  and a.STATUS_ID not in (0,4,11,13,12) and datediff(d,isnull(a.ASSIGNED_DATE,a.REQUEST_DT) ,GETDATE()) between " & FromNo & " and " & ToNo & " and a.Team_id in (select TEAM_ID from ( select b.TEAM_ID,b.TEAM_NAME,datediff(d,isnull(a.ASSIGNED_DATE,a.REQUEST_DT) ,GETDATE()) Days from HD_REQUEST_MASTER a,HD_TEAM_MASTER b where a.TEAM_ID = b.TEAM_ID and a.STATUS_ID <>0) x group by TEAM_ID)"
                            RH.Heading(Session("FirmName"), tb, "Team Wise Ageing Report", 100)
                        Case 5
                            SQL += "  and a.STATUS_ID not in (0,4,11,13,12) and datediff(d,isnull(a.ASSIGNED_DATE,a.REQUEST_DT) ,GETDATE()) > 30  and a.Team_id in (select TEAM_ID from ( select b.TEAM_ID,b.TEAM_NAME,datediff(d,isnull(a.ASSIGNED_DATE,a.REQUEST_DT) ,GETDATE()) Days from HD_REQUEST_MASTER a,HD_TEAM_MASTER b where a.TEAM_ID = b.TEAM_ID and a.STATUS_ID <>0) x group by TEAM_ID)"
                            RH.Heading(Session("FirmName"), tb, "Team Wise Ageing Report", 100)
                        Case 6
                            SQL += "  and a.STATUS_ID not in (0,4,11,13) and a.Team_id in (select TEAM_ID from ( select b.TEAM_ID,b.TEAM_NAME,datediff(d,isnull(a.ASSIGNED_DATE,a.REQUEST_DT) ,GETDATE()) Days from HD_REQUEST_MASTER a,HD_TEAM_MASTER b where a.TEAM_ID = b.TEAM_ID and a.STATUS_ID <>0) x group by TEAM_ID)"
                            RH.Heading(Session("FirmName"), tb, "Team Wise Ageing Report", 100)

                    End Select

            End Select
            DT = DB.ExecuteDataSet(SQL).Tables(0)


            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12, TRHead_13, TRHead_14, TRHead_15, TRHead_16, TRHead_17 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"
            TRHead_13.BorderWidth = "1"
            TRHead_14.BorderWidth = "1"
            TRHead_15.BorderWidth = "1"
            TRHead_16.BorderWidth = "1"
            TRHead_17.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver
            TRHead_13.BorderColor = Drawing.Color.Silver
            TRHead_14.BorderColor = Drawing.Color.Silver
            TRHead_15.BorderColor = Drawing.Color.Silver
            TRHead_16.BorderColor = Drawing.Color.Silver
            TRHead_17.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid
            TRHead_13.BorderStyle = BorderStyle.Solid
            TRHead_14.BorderStyle = BorderStyle.Solid
            TRHead_15.BorderStyle = BorderStyle.Solid
            TRHead_16.BorderStyle = BorderStyle.Solid
            TRHead_17.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 3, 3, "l", "Sl&nbsp;No.")
            RH.AddColumn(TRHead, TRHead_01, 13, 13, "l", "Ticket&nbsp;No")
            RH.AddColumn(TRHead, TRHead_02, 5, 5, "c", "Report&nbsp;Date")
            RH.AddColumn(TRHead, TRHead_03, 8, 8, "l", "Classification")
            RH.AddColumn(TRHead, TRHead_04, 4, 4, "l", "Call&nbsp;Logged")
            RH.AddColumn(TRHead, TRHead_05, 5, 5, "l", "Status")
            RH.AddColumn(TRHead, TRHead_06, 5, 5, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_07, 6, 6, "l", "Department")
            RH.AddColumn(TRHead, TRHead_08, 4, 4, "l", "E-mail")
            RH.AddColumn(TRHead, TRHead_09, 5, 5, "l", "State")
            RH.AddColumn(TRHead, TRHead_10, 5, 5, "l", "Contact&nbsp;No")
            RH.AddColumn(TRHead, TRHead_11, 6, 6, "l", "Main&nbsp;Category")
            RH.AddColumn(TRHead, TRHead_12, 6, 6, "l", "Sub&nbsp;Category")
            RH.AddColumn(TRHead, TRHead_13, 5, 5, "l", "Work&nbsp;Log")
            RH.AddColumn(TRHead, TRHead_14, 5, 5, "c", "Pending&nbsp;Days")
            RH.AddColumn(TRHead, TRHead_15, 5, 5, "c", "Bucket")
            RH.AddColumn(TRHead, TRHead_16, 5, 5, "c", "updated remarks")
            RH.AddColumn(TRHead, TRHead_17, 5, 5, "c", "user")


            tb.Controls.Add(TRHead)

            'RH.BlankRow(tb, 3)
            Dim Tot0 As Integer = 0
            Dim Tot1 As Integer = 0
            Dim Tot2 As Integer = 0
            Dim Tot3 As Integer = 0
            Dim Tot4 As Integer = 0
            Dim Tot5 As Integer = 0
            Dim Tot6 As Integer = 0
            Dim Tot7 As Integer = 0
            Dim Tot8 As Integer = 0
            Dim HTot As Integer = 0
            Dim i As Integer = 0
            Dim Remarks As String = ""
            For Each DR In DT.Rows

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16, TR3_17 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"
                TR3_15.BorderWidth = "1"
                TR3_16.BorderWidth = "1"
                TR3_17.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver
                TR3_15.BorderColor = Drawing.Color.Silver
                TR3_16.BorderColor = Drawing.Color.Silver
                TR3_17.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid
                TR3_15.BorderStyle = BorderStyle.Solid
                TR3_16.BorderStyle = BorderStyle.Solid
                TR3_17.BorderStyle = BorderStyle.Solid

                i += 1
                ' HTot += DR(2) + DR(3) + DR(4) + DR(5) + DR(6) + DR(7) + DR(8) + DR(9)
                RH.AddColumn(TR3, TR3_00, 3, 3, "l", i)
                RH.AddColumn(TR3, TR3_01, 13, 13, "l", "<a href = 'viewTicketDetails.aspx?RequestID=" + GN.Encrypt(DR(0).ToString()) + "' target='_blank'>" + DR(1).ToString())
                RH.AddColumn(TR3, TR3_02, 5, 5, "c", Format(DR(2), "dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_03, 8, 8, "l", DR(3))
                RH.AddColumn(TR3, TR3_04, 4, 4, "l", DR(4))
                RH.AddColumn(TR3, TR3_05, 5, 5, "l", DR(5))
                RH.AddColumn(TR3, TR3_06, 5, 5, "l", DR(6))
                RH.AddColumn(TR3, TR3_07, 6, 6, "l", DR(7))
                If IsDBNull(DR(8)) Then
                    RH.AddColumn(TR3, TR3_08, 4, 4, "l", "")
                Else
                    RH.AddColumn(TR3, TR3_08, 4, 4, "l", DR(8))
                End If

                If IsDBNull(DR(9)) Then
                    RH.AddColumn(TR3, TR3_09, 5, 5, "l", "")
                Else
                    RH.AddColumn(TR3, TR3_09, 5, 5, "l", DR(9))
                End If

                If IsDBNull(DR(10)) Then
                    RH.AddColumn(TR3, TR3_10, 5, 5, "l", "")
                Else
                    RH.AddColumn(TR3, TR3_10, 5, 5, "l", DR(10))
                End If

                RH.AddColumn(TR3, TR3_11, 6, 6, "l", DR(11))
                RH.AddColumn(TR3, TR3_12, 6, 6, "l", DR(12))
                RH.AddColumn(TR3, TR3_13, 5, 5, "l", DR(13))
                RH.AddColumn(TR3, TR3_14, 5, 5, "c", DR(14))
                RH.AddColumn(TR3, TR3_15, 5, 5, "c", DR(15))
                RH.AddColumn(TR3, TR3_16, 5, 5, "c", DR(16))
                RH.AddColumn(TR3, TR3_17, 5, 5, "c", DR(17))
                tb.Controls.Add(TR3)
            Next

            RH.BlankRow(tb, 4)

            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
