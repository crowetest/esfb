﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewCustomizeTicketStatus
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 262) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            pnDisplay.Attributes.Clear()
            Dim DT As New DataTable
            Dim TypeID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("TypeID")))
            Dim StatusID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("StatusID")))
            Dim PriorityID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("PriorityID")))
            Dim DateSelection As Integer = CInt(Request.QueryString.Get("DateSelection"))
            Dim CategoryID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("CategoryID")))
            Dim GroupBy As Integer = CInt(GF.Decrypt(Request.QueryString.Get("GroupBy")))
            Dim ID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("ID")))
            Dim FromDate, ToDate As Date
            Dim SQL As String = ""
            Dim Selection As String = ""
            Dim TypeFilter As String = ""
            Dim PriorityFilter As String = ""
            Dim StatusFilter As String = ""
            Dim DateFilter As String = ""
            Dim GroupFilter As String = ""
            Dim CategoryFilter As String = ""

            Selection = "select a.CHANGE_NO,a.CHANGE,a.REASON,a.CREATE_DT,c.PRIORITY_NAME,d.TYPE_NAME,a.PROPOSED_STARTTIME,a.PROPOSED_ENDTIME,a.IMPLEMENT_TEAM,a.ASSET_DESCR,b.STATUS,e.category   from HD_CHANGE_MASTER a,HD_CHANGE_STATUS b,HD_CHANGE_PRIORITY c,HD_CHANGE_TYPES d,hd_problem_category e " & _
                " where a.STATUS_ID = b.STATUS_ID and a.PRIORITY_ID = c.PRIORITY_ID and a.TYPE_ID = d.TYPE_ID and a.category_id = e.category_id"
            If StatusID <> -1 Then
                StatusFilter = " and a.STATUS_ID = " & StatusID & ""
            End If
            If TypeID <> -1 Then
                TypeFilter = " and a.TYPE_ID = " & TypeID & ""
            End If
            If PriorityID <> -1 Then
                PriorityFilter = " and a.PRIORITY_ID = " & PriorityID & ""
            End If
            If CategoryID <> -1 Then
                CategoryFilter = " and a.category_ID = " & CategoryID & ""
            End If
            If DateSelection = 1 Then
                FromDate = CDate(Request.QueryString.Get("FromDt"))
                ToDate = CDate(Request.QueryString.Get("ToDt"))
                DateFilter = " and DATEADD(day,DATEDIFF(day, 0,proposed_starttime),0)  >= '" & CDate(FromDate).ToString("MM/dd/yyyy") & "'  and DATEADD(day,DATEDIFF(day, 0,proposed_endtime),0) <= '" & CDate(ToDate).ToString("MM/dd/yyyy") & "'"
            ElseIf DateSelection = 2 Then
                FromDate = CDate(Request.QueryString.Get("FromDt"))
                ToDate = CDate(Request.QueryString.Get("ToDt"))
                DateFilter = " and DATEADD(day,DATEDIFF(day, 0,implementation_start),0)  >= '" & CDate(FromDate).ToString("MM/dd/yyyy") & "'  and DATEADD(day,DATEDIFF(day, 0,implementation_end),0) <= '" & CDate(ToDate).ToString("MM/dd/yyyy") & "'"
            End If
            If GroupBy = 0 Then
                GroupFilter = " and a.TYPE_ID = " & ID & ""
            ElseIf GroupBy = 1 Then
                GroupFilter = " and a.PRIORITY_ID = " & ID & ""
            Else
                GroupFilter = " and a.STATUS_ID = " & ID & ""
            End If
            SQL = Selection + TypeFilter + PriorityFilter + CategoryFilter + StatusFilter + DateFilter + GroupFilter
            DT = DB.ExecuteDataSet(SQL).Tables(0)

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RH.Heading(Session("FirmName"), tb, "STATUS REPORT", 150)
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            Dim StrUserNam As String = Nothing

            Dim TRHead_1 As New TableRow
            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10 As New TableCell


            RH.InsertColumn(TRHead_1, TRHead_1_00, 10, 0, "Change No")
            RH.InsertColumn(TRHead_1, TRHead_1_01, 20, 0, "Change")
            'RH.InsertColumn(TRHead_1, TRHead_1_02, 15, 0, "Reason")
            RH.InsertColumn(TRHead_1, TRHead_1_03, 10, 0, "Create Date")
            RH.InsertColumn(TRHead_1, TRHead_1_04, 5, 0, "Priority")
            RH.InsertColumn(TRHead_1, TRHead_1_05, 5, 0, "Type")
            RH.InsertColumn(TRHead_1, TRHead_1_06, 10, 0, "Prop.StartDt")
            RH.InsertColumn(TRHead_1, TRHead_1_07, 10, 0, "Prop.EndDt")
            RH.InsertColumn(TRHead_1, TRHead_1_08, 10, 0, "Implement Team")
            RH.InsertColumn(TRHead_1, TRHead_1_09, 10, 0, "Category")
            RH.InsertColumn(TRHead_1, TRHead_1_10, 10, 0, "Status")
            tb.Controls.Add(TRHead_1)


            For Each DR In DT.Rows

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10 As New TableCell

                RH.InsertColumn(TR3, TR3_00, 10, 0, "<a href ='ChangeTicketInfo.aspx?ChangeNo=" + GF.Encrypt(DR(0).ToString()) + "'  target='_blank'>" + DR(0).ToString() + "</a>")
                RH.InsertColumn(TR3, TR3_01, 20, 0, DR(1).ToString())
                RH.InsertColumn(TR3, TR3_03, 10, 0, Format(DR(3), "dd/MMM/yyyy"))
                If (IsDBNull(DR(4))) Then
                    RH.InsertColumn(TR3, TR3_04, 5, 0, "")
                Else
                    RH.InsertColumn(TR3, TR3_04, 5, 0, DR(4).ToString())
                End If
                If (IsDBNull(DR(5))) Then
                    RH.InsertColumn(TR3, TR3_05, 5, 0, "")
                Else
                    RH.InsertColumn(TR3, TR3_05, 5, 0, DR(5).ToString())
                End If
                If (IsDBNull(DR(6))) Then
                    RH.InsertColumn(TR3, TR3_06, 10, 0, "")
                Else
                    RH.InsertColumn(TR3, TR3_06, 10, 0, Format(DR(6), "dd/MMM/yyyy"))
                End If
                If (IsDBNull(DR(7))) Then
                    RH.InsertColumn(TR3, TR3_07, 10, 0, "")
                Else
                    RH.InsertColumn(TR3, TR3_07, 10, 0, Format(DR(7), "dd/MMM/yyyy"))
                End If
                If (IsDBNull(DR(8))) Then
                    RH.InsertColumn(TR3, TR3_08, 10, 0, "")
                Else
                    RH.InsertColumn(TR3, TR3_08, 10, 0, DR(8).ToString())
                End If
                If (IsDBNull(DR(11))) Then
                    RH.InsertColumn(TR3, TR3_09, 10, 0, "")
                Else
                    RH.InsertColumn(TR3, TR3_09, 10, 0, DR(11).ToString())
                End If
                RH.InsertColumn(TR3, TR3_10, 10, 0, DR(10).ToString())
                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
