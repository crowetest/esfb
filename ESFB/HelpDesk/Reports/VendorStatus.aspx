﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="VendorStatus.aspx.vb" Inherits="VendorStatus" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }  
        .sub_hdRow
        {
         background-color:#EBCCD6; height:20px;
         font-family:Arial; color:#B84D4D; font-size:8.5pt;  font-weight:bold;
        }      
        .style1
        {
            width: 100%;
        }
    </style>



    <script language="javascript" type="text/javascript">
     
       


       
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }


       
  
       
   function FromServer(arg, context)
   
        {if (context == 1)
            {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if(Data[0]==0) {window.open("VendorStatus.aspx","_self");}
             }
           else if (context == 2)
            {      alert(2);         
              var Data = arg.split("~");
              
              
            }
         }
   function window_onload() { 
    table_Fill();
}
        
     function table_Fill()
        {
             var tab="";
             var row_bg = 0;

               tab += "<div style='width:35%;  height:auto; margin: 0px auto; overflow-y:scroll' class=mainhead>";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr height=30px;>";
                  
                tab += "<td style='width:5%;text-align:center' >#</td>";                
                tab += "<td style='width:30%;text-align:left' >Vendor Name</td>";
                          
                tab += "</tr>";
                tab += "</table></div>";
             if(document.getElementById("<%= hid_Items.ClientID %>").value=="")
             {
                 document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none'; 
                 return;
             }
             
         
             document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';  
             tab += "<div id='ScrollDiv' style='width:35%; height:200px;overflow: scroll;margin: 0px auto;' class=mainhead >";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='left'>";     
            
               var row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");                               
                for (n = 0; n <= row.length - 1; n++) {
                   var col = row[n].split("µ");
                   if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center;padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second'; style='text-align:center; padding-left:20px;'>";
                    }
                    var i = n + 1;        
                                                      
                    tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                    
                    tab += "<td style='width:30%;text-align:left'><a href='ViewVendorStatusReport.aspx?VendorID="+ col[1] +"' style='text-align:right;' target='_blank' >"  + col[0] + "</td>";
                     
                    tab += "</tr>";
                
            }
            tab += "</table></div></div></div>";            
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;  
        }



  </script>

<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
</script>





</head>
</html>
<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;" >
           
        <tr id="Remarks"> <td style="width:25%; top:225px auto;">
       <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
       </td>
        
       </tr>
      <tr>
        <td style="text-align:center;"><br />
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />&nbsp;<asp:HiddenField 
                    ID="hid_Items" runat="server" />
               </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>