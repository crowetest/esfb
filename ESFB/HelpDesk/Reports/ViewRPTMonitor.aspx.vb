﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewRPTMonitor
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim StrFromDate As Date
            Dim Struser As String = Session("UserID").ToString()
            If CStr(Request.QueryString.Get("FromDt")) <> "" Then
                StrFromDate = CDate(GN.Decrypt(Request.QueryString.Get("FromDt")))
            End If
            Dim StrToDate As Date
            If CStr(Request.QueryString.Get("ToDt")) <> "" Then
                StrToDate = CDate(GN.Decrypt(Request.QueryString.Get("ToDt")))
            End If

            Dim DT As New DataTable
        
            Dim DateFrom As String
            Dim DateTo As String

            DateFrom = StrFromDate.ToString("yyyy-MM-dd")
            DateTo = StrToDate.ToString("yyyy-MM-dd")

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            DT = DB.ExecuteDataSet("select team_id from HD_TEAM_DTL where emp_code= " & Struser & "").Tables(0)
            Dim team_id As String = DT.Rows(0)(0).ToString()

            RH.Heading(Session("FirmName"), tb, "RPT Activity Monitor Report", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12, TRHead_13 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"
           
            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver
            TRHead_13.BorderColor = Drawing.Color.Silver
            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid
        
            RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "SI No")
            RH.AddColumn(TRHead, TRHead_01, 7, 7, "l", "Initiation Date")
            RH.AddColumn(TRHead, TRHead_02, 6, 6, "l", "Ticket No")
            RH.AddColumn(TRHead, TRHead_03, 4, 4, "l", "Branch code")
            RH.AddColumn(TRHead, TRHead_04, 8, 8, "l", "Branch name")
            RH.AddColumn(TRHead, TRHead_05, 20, 20, "l", "Findings")
            RH.AddColumn(TRHead, TRHead_06, 5, 5, "l", "AC/CIF")
            RH.AddColumn(TRHead, TRHead_07, 7, 7, "l", "AC/CIF No")
            RH.AddColumn(TRHead, TRHead_08, 8, 8, "l", "Last Updated Status")
            RH.AddColumn(TRHead, TRHead_09, 5, 5, "l", "Attended By")
            RH.AddColumn(TRHead, TRHead_10, 7, 7, "l", "Attended Date")
            RH.AddColumn(TRHead, TRHead_11, 5, 5, "l", "Closed By")
            RH.AddColumn(TRHead, TRHead_12, 7, 7, "l", "Closed Date")
            RH.AddColumn(TRHead, TRHead_13, 6, 6, "l", "Last Updated Date")

            tb.Controls.Add(TRHead)
            
            RH.BlankRow(tb, 3)
            Dim i As Integer

            Dim StrWhere As String = ""
          
            DT = DB.ExecuteDataSet("select distinct HRM.request_id,convert(varchar(10),HRM.REQUEST_DT,105) as REQUEST_DT,HRM.TICKET_NO,HRM.branch_id,bm.branch_name, " +
            " HPT.problem,case when HRM.ACCOUNT_OR_CIF = 1 THEN 'AC NO' ELSE case when HRM.ACCOUNT_OR_CIF = 2 THEN 'CIF NO' ELSE '' END END AS AcOrCif, " +
            " case when HRM.ACCOUNT_OR_CIF_NUMBER is NULL THEN '' ELSE HRM.ACCOUNT_OR_CIF_NUMBER END as AcOrCifNum," +
            " case when hs.status_name is NULL THEN 'CLOSED' ELSE hs.status_name END AS status_name,HRC1.user_id,HRC1.tra_dt,HRC2.user_id,HRC2.tra_dt,ABC.tra_dt from HD_REQUEST_MASTER HRM " +
            " inner join HD_REQUEST_CYCLE HRC on HRM.request_id=HRC.request_id " +
            " INNER JOIN HD_PROBLEM_TYPE HPT on HPT.problem_id=HRC.problem_id " +
            " inner join branch_master bm on bm.branch_id=HRM.branch_id " +
            " inner join (SELECT request_id, update_id,convert(varchar(10),tra_dt,105) as tra_dt  FROM HD_REQUEST_CYCLE WHERE order_id IN " +
            " (SELECT MAX(order_id) FROM HD_REQUEST_CYCLE  where cast(tra_dt as date)>='" + DateFrom + "' and cast(tra_dt as date)<='" + DateTo + "' " +
            " GROUP BY request_id)) as ABC on ABC.request_id=HRM.request_id " +
            " left join hd_status hs on hs.status_id=abc.update_id " +
           " left join HD_REQUEST_CYCLE HRC1 on HRM.request_id=HRC1.request_id and HRC1.update_id=14 " +
          " left join HD_REQUEST_CYCLE HRC2 on HRM.request_id=HRC2.request_id and HRC2.update_id=4 where hrm.team_id=" + team_id.ToString()).Tables(0)


            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "l", i.ToString())
                RH.AddColumn(TR3, TR3_01, 7, 7, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_02, 6, 6, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_03, 4, 4, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_04, 8, 8, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_05, 20, 20, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_06, 5, 5, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_07, 7, 7, "l", DR(7).ToString())
                RH.AddColumn(TR3, TR3_08, 8, 8, "l", DR(8).ToString())
                RH.AddColumn(TR3, TR3_09, 5, 5, "l", DR(9).ToString())
                RH.AddColumn(TR3, TR3_10, 7, 7, "l", DR(10).ToString())
                RH.AddColumn(TR3, TR3_11, 5, 5, "l", DR(11).ToString())
                RH.AddColumn(TR3, TR3_12, 7, 7, "l", DR(12).ToString())
                RH.AddColumn(TR3, TR3_13, 6, 6, "l", DR(13).ToString())

               
                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        Try
            Dim HeaderText As String
            Dim DT As New DataTable
            HeaderText = "RPT Activity Monitor Report"

            WebTools.ExporttoExcel(DT, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
