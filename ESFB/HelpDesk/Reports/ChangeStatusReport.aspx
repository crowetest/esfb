﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="ChangeStatusReport.aspx.vb" Inherits="ViewCustomizeTicketStatus" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
</head>
<body style="background-color:Black;">

 <script language="javascript" type="text/javascript">
     function OnLoad() 
     {
         DateOnchange();
     }

     function DateOnchange() 
     {
         var DateVal = document.getElementById("<%= cmbDate.ClientID %>").value;
         if (DateVal == 0) document.getElementById("rowDate").style.display = "none";
         else              document.getElementById("rowDate").style.display = "";
     }

     function GenerateOnclick() 
     {
         var Status = document.getElementById("<%= cmbStatus.ClientID %>").value;
         var FromDt = document.getElementById("<%= txtFrom.ClientID %>").value;
         var ToDt = document.getElementById("<%= txtTo.ClientID %>").value;
         var Priority = document.getElementById("<%= cmbPriority.ClientID %>").value;
         var GroupBy = document.getElementById("<%= cmbGroupBy.ClientID %>").value;
         if (document.getElementById("<%= cmbDate.ClientID %>").value > 0) 
         {
             if (FromDt == "") 
             {
                 alert("Select From Date");
                 document.getElementById("<%= txtFrom.ClientID %>").focus();
                 return false;
             }
             if (ToDt == "") 
             {
                 alert("Select To Date");
                 document.getElementById("<%= txtTo.ClientID %>").focus();
                 return false;
             }
         }
         return true;
     }

     function setStartDate(sender, args) 
     {
         var StartDt = AddDay(document.getElementById("<%= txtFrom.ClientID %>").value, 1);
         var EndDt = AddDay(document.getElementById("<%= txtTo.ClientID %>").value, 1);
         var Cnt = 1;
         var OrgStartDt = AddDay(document.getElementById("<%= txtFrom.ClientID %>").value, Cnt);
         sender._startDate = new Date(OrgStartDt);
         sender._endDate = new Date(EndDt);
     }

     function AddDay(strDate, intNum) 
     {
         sdate = new Date(strDate);
         sdate.setDate(sdate.getDate() + intNum);
         return sdate.getMonth() + 1 + " " + sdate.getDate() + " " + sdate.getFullYear();
     }

     function PrintPanel() 
     {
         var panel = document.getElementById("<%=pnDisplay.ClientID %>");
         var printWindow = window.open('', '', 'height=400,width=800,location=0,status=0');
         printWindow.document.write('<html><head><title>ESAF</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () 
         {
             printWindow.print();
         }, 500);
         return false;
     }
     function Exitform() 
     {
         window.close();
         return false;
     }

     function btnExit_onclick() {
         window.open('../../home.aspx', '_self');
         return false;
     }
     </script>
    
     <form id="form1" runat="server"> 
       
    <div style="width:95%; background-color:white; min-height:750px; margin:0px auto;">
    <div>
     <br /> 
    <table  style="width:75%;margin: 0px auto;font-family:Cambria;" border="1" >
    <tr style="background-color:lightsteelblue;">
    <td style="text-align: center;" colspan="4"><asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>Change Status Report</td>
    </tr>
    <tr style="background-color:lightsteelblue;">
    <td style="width:25%; text-align: center;">Priority</td>
    <td style="width:25%;">
                <asp:DropDownList ID="cmbPriority" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="95%" ForeColor="Black">
                    
                </asp:DropDownList> 
                </td>
    <td style="width:25%; text-align: center;">Status</td>
    <td style="width:25%;">
                <asp:DropDownList ID="cmbStatus" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="95%" ForeColor="Black">
                    
                </asp:DropDownList> 
                </td>
    </tr>
    <tr style="background-color:lightsteelblue;">
    <td style="width:25%; text-align: center;">Type</td>
    <td style="width:25%;">
                <asp:DropDownList ID="cmbType" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="95%" ForeColor="Black">
                   
                </asp:DropDownList> 
                </td>
    <td style="width:25%; text-align: center;">Date Filter</td>
    <td style="width:25%;">
                <asp:DropDownList ID="cmbDate" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="95%" ForeColor="Black">
                   <asp:ListItem Value="0"> NOT NEEDED</asp:ListItem>
                   <asp:ListItem Value="1"> BASED ON PLANNED DATE</asp:ListItem>
                   <asp:ListItem Value="2"> BASED ON IMPLEMENTATION DATE</asp:ListItem>
                </asp:DropDownList> 
                </td>
    </tr>
    <tr id="rowDate" style="background-color:lightsteelblue;">
    <td style="width:25%; text-align: center;">Start Date</td>
    <td style="width:25%;">
                <asp:TextBox ID="txtFrom" class="NormalText" runat="server" Width="95%" 
                MaxLength="100" Height="20px" onkeypress="return false" ></asp:TextBox>
             <asp:CalendarExtender ID="CalendarExtender1" runat="server"  TargetControlID="txtFrom" Format="dd MMM yyyy">
             </asp:CalendarExtender> 
                </td>
    <td style="width:25%; text-align: center;">End Date&nbsp;&nbsp;</td>
    <td style="width:25%;">
                <asp:TextBox ID="txtTo" class="NormalText" runat="server" Width="95%" 
                MaxLength="100" Height="20px" onkeypress="return false"></asp:TextBox>
             <asp:CalendarExtender ID="CEFromDate" runat="server" TargetControlID="txtTo" OnClientShowing="setStartDate" Format="dd MMM yyyy">
             </asp:CalendarExtender> 
                </td>
    </tr>
    <tr style="background-color:lightsteelblue;">
    <td style="width:25%; text-align: center;">Group By</td>
    <td style="width:25%;">
                <asp:DropDownList ID="cmbGroupBy" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="95%" ForeColor="Black">
                    <asp:ListItem Value="2"> STATUS</asp:ListItem>
                    <asp:ListItem Value="0"> TYPE</asp:ListItem>
                    <asp:ListItem Value="1"> PRIORITY</asp:ListItem>
                    
                </asp:DropDownList> 
                </td>
    <td style="width:25%; text-align: center;">Category</td>
    <td style="width:25%;">
                <asp:DropDownList ID="cmbCategory" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="95%" ForeColor="Black">
                                        
                </asp:DropDownList> 
                </td>
    
    </tr>
    <tr style="background-color:lightsteelblue;">
    <td style="text-align: center;" colspan="4">
                <asp:Button ID="btnGenerate" runat="server" Text="GENERATE" Font-Names="Cambria" style="margin-bottom: 0px"/>
                &nbsp;&nbsp; 
                <input id="btnExit" style="font-family: cambria; cursor: pointer; " 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
    <tr><td colspan="4"><asp:Panel ID="pnDisplay"  style="text-align:center;"  runat="server" Width="100%">
            </asp:Panel></td></tr>
    </table>
     
    </div>
       <br style="background-color:white"/>
        <div style="text-align:center;margin:0px auto; width:95%; background-color:white; font-family:Cambria">
        <table style="width:100%;">
        <tr>
        <td style="width:20%;"></td>
        <td style="width:80%;">
         
        </td>
        </tr>
        </table>           
        </div>
    </div>
    </form>
</body>
</html>
</asp:Content>
