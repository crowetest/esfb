﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ProblemStatusReport
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim UserID As Integer
    Dim RH As New WholeHelper.ClsRepCtrl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 266) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            UserID = CInt(Session("UserID"))
            If Not IsPostBack Then
                DT = GN.GetQueryResult("SELECT ' ALL',-1 UNION ALL select UPPER(STATUS),STATUS_ID from HD_PROBLEM_STATUS ")
                GN.ComboFill(cmbStatus, DT, 1, 0)
                DT = DB.ExecuteDataSet("SELECT ' ALL',-1 UNION ALL select UPPER(PRIORITY_NAME),PRIORITY_ID from HD_CHANGE_PRIORITY").Tables(0)
                GN.ComboFill(cmbPriority, DT, 1, 0)
                DT = DB.ExecuteDataSet("SELECT ' ALL',-1 UNION ALL select UPPER(CATEGORY),CATEGORY_ID from HD_PROBLEM_CATEGORY").Tables(0)
                GN.ComboFill(cmbCategory, DT, 1, 0)
            End If
            GenerateReport()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Public Sub GenerateReport()
        Try
            pnDisplay.Controls.Clear()
            Dim StatusID As Integer = CInt(Me.cmbStatus.SelectedValue)
            Dim PriorityID As Integer = CInt(Me.cmbPriority.SelectedValue)
            Dim CategoryID As Integer = CInt(Me.cmbCategory.SelectedValue)
            Dim GroupBy As Integer = CInt(Me.cmbGroupBy.SelectedValue)

            Dim Heading As String = Me.cmbGroupBy.SelectedItem.Text
            Dim SQL As String
            Dim Selection As String = ""
            Dim CategoryFilter As String = ""
            Dim PriorityFilter As String = ""
            Dim StatusFilter As String = ""
            Dim GroupString As String = ""

            Select Case GroupBy
                Case 0 '-- Category
                    Selection = "select a.Category_ID,b.Category,COUNT(*) CNT  from HD_PROBLEM_MASTER a,HD_PROBLEM_CATEGORY b where  a.Category_ID = b.Category_ID"
                    GroupString = " GROUP BY a.Category_ID,b.Category"
                Case 1 '-- Priority
                    Selection = "select a.PRIORITY_ID,b.PRIORITY_NAME,COUNT(*) CNT  from HD_PROBLEM_MASTER a,HD_CHANGE_PRIORITY b where a.PRIORITY_ID = b.PRIORITY_ID"
                    GroupString = " GROUP BY a.PRIORITY_ID,b.PRIORITY_NAME"
                Case 2 '-- Status
                    Selection = "select a.STATUS_ID,b.STATUS,COUNT(*) CNT  from HD_PROBLEM_MASTER a,HD_PROBLEM_STATUS b where a.STATUS_ID =b.STATUS_ID"
                    GroupString = " GROUP BY a.STATUS_ID,b.STATUS"
            End Select
            If StatusID <> -1 Then
                StatusFilter = " and a.STATUS_ID = " & StatusID & ""
            End If
            If CategoryID <> -1 Then
                CategoryFilter = " and a.Category_ID = " & CategoryID & ""
            End If
            If PriorityID <> -1 Then
                PriorityFilter = " and a.PRIORITY_ID = " & PriorityID & ""
            End If
            SQL = Selection + CategoryFilter + PriorityFilter + StatusFilter + GroupString
            DT = GN.GetQueryResult(SQL)

            Dim tb As New Table
            RH.Heading("", tb, Heading + " WISE FINDINGS TICKET SUMMARY", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")


            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell


            RH.InsertColumn(TRHead, TRHead_00, 10, 2, "#")
            RH.InsertColumn(TRHead, TRHead_01, 70, 0, Heading)
            RH.InsertColumn(TRHead, TRHead_02, 20, 2, "COUNT")
            tb.Controls.Add(TRHead)

            Dim i As Integer = 1
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10 As New TableCell


                RH.InsertColumn(TR3, TR3_00, 10, 2, i)
                RH.InsertColumn(TR3, TR3_01, 70, 0, DR(1))
                RH.InsertColumn(TR3, TR3_02, 20, 2, "<a href='ProblemStatusReport_0.aspx?ID=" + GN.Encrypt(DR(0).ToString()) + "&GroupBy=" + GN.Encrypt(GroupBy.ToString()) + "&CategoryID=" + GN.Encrypt(CategoryID.ToString()) + "&PriorityID=" + GN.Encrypt(PriorityID.ToString()) + "&StatusID=" + GN.Encrypt(StatusID.ToString()) + "'   target='_blank'>" + DR(2).ToString() + "</a>")

                i += 1
                tb.Controls.Add(TR3)
            Next

            RH.BlankRow(tb, 4)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnGenerate_Click(sender As Object, e As System.EventArgs) Handles btnGenerate.Click
        GenerateReport()
    End Sub
End Class
