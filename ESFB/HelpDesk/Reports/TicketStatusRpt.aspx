﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="TicketStatusRpt.aspx.vb" Inherits="TicketStatusRpt" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }  
        .sub_hdRow
        {
         background-color:#EBCCD6; height:20px;
         font-family:Arial; color:#B84D4D; font-size:8.5pt;  font-weight:bold;
        }      
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 57%;
        }
    </style>



    <script language="javascript" type="text/javascript">
     
       


       
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }


       
  function btnShow_onclick() {
 var count=0;
 var StrStatus=""
       document.getElementById("<%= hid_Value.ClientID %>").value="and ( ";
      var row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");  
       if(document.getElementById("<%= pnDisplay.ClientID %>").style.display!="none")
       {
                                          
        for (n = 0; n <= row.length - 1; n++) {
            var col = row[n].split("µ");
            var i = n + 1;  
            
            if(document.getElementById("chkAttend"+i).checked==true)
            {
                if (document.getElementById("<%= hid_Value.ClientID %>").value!="and ( ")
                {   document.getElementById("<%= hid_Value.ClientID %>").value+=" or " ; }
                 count=count+1;
                 StrStatus=  col[0];                                 
                document.getElementById("<%= hid_Value.ClientID %>").value+=" a.status_id= " + col[1];
            }
                     
        }
        }
        else
        {
              for (n = 0; n <= row.length - 1; n++) {
            var col = row[n].split("µ");
            var i = n + 1; 
            if (document.getElementById("<%= hid_Value.ClientID %>").value!="and ( ")
            {   document.getElementById("<%= hid_Value.ClientID %>").value+=" or " ; }
                count=count+1;
                StrStatus=  col[0];                                 
            document.getElementById("<%= hid_Value.ClientID %>").value+=" a.status_id= " + col[1];
           } 
        }
        if(document.getElementById("<%= hid_Value.ClientID %>").value=="and ( ")
        {
            return false;
        }    
        document.getElementById("<%= hid_Value.ClientID %>").value+=") ";
       
       var status=document.getElementById("<%= hid_Value.ClientID %>").value ;
       var frmdate=document.getElementById("<%= txtStartDt.ClientID %>").value;
       var todate=document.getElementById("<%= txtToDt.ClientID %>").value;                                    
       window.open("ViewTicketStatus.aspx?statusID=" +btoa(status) + "&counts=" + btoa(count)  + "&statusnam=" + btoa(StrStatus) + "&frmdate=" + frmdate + "&todate=" + todate, "_self");

                
}

function window_onload() { 
    table_Fill();
    

}


function AllSelect(){

var  row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");
    for (n = 0; n <= row.length - 1; n++) {
           
            var i = n + 1;  
            document.getElementById("chkAttend"+i).checked=document.getElementById("chkAttendAll").checked; 
            
          
        }
}

 function table_Fill()
        {
            
            var tab="";
             var row_bg = 0;

               tab += "<div style='width:50%;  height:auto; margin: 0px auto; ' class=mainhead>";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr height=27px;>";
                  
                tab += "<td style='width:5%;text-align:center' ><input type='checkbox' id='chkAttendAll' onclick='AllSelect()' /></td>";                
                tab += "<td style='width:25%;text-align:left' >STATUS</td>";
                          
                tab += "</tr>";
                tab += "</table></div>";
            
             
         
             document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';  
             tab += "<div id='ScrollDiv' style='width:50%; height:233px;;margin: 0px auto;' class=mainhead >";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='left'>";     
            
               var row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");                               
                for (n = 0; n <= row.length - 1; n++) {
                   var col = row[n].split("µ");
                   if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center;padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second'; style='text-align:center; padding-left:20px;'>";
                    }
                    var i = n + 1;        
                    tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkAttend" + i + "'  /></td>";                                  
                    tab += "<td style='width:25%;text-align:left'>" + col[0] + "</td>";
                    
                  
                     
                    tab += "</tr>";
                
            }
            tab += "</table></div></div></div>";            
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;             
            if(document.getElementById("<%= hid_USer.ClientID %>").value=="1") 
            {
                AllSelect();
                document.getElementById("<%= pnDisplay.ClientID %>").style.display="none";
            }
                
        }
      
    </script>
  
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
</script>
</head>
</html>
<br /><br />
    
    <div  style="width:60%;margin:0px auto; ">
       <table class="style1" style="width:100%;top:350px auto;"> 

        <tr>
           
            <td style="text-align:center; margin:0px auto; width:25%;">
                &nbsp;</td>
            <td style="text-align:right; margin:0px auto; width:20%;">
               From date &nbsp; &nbsp; </td> 
            <td class="style2">
                <asp:TextBox ID="txtStartDt" class="NormalText" runat="server" 
                    Width="30%" onkeypress="return false" ReadOnly="True" ></asp:TextBox>
             <ajaxToolkit:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtStartDt" Format="dd MMM yyyy"></ajaxToolkit:CalendarExtender></td>                       
             <td style="text-align:center; margin:0px auto; width:15%;">
                 &nbsp;</td>          
            </tr>
           
         <tr>
            <td style="text-align:center; margin:0px auto; width:25%;">
                &nbsp;</td>
            <td style="text-align:right; margin:0px auto; width:20%;">
                To date &nbsp; &nbsp;  </td> 
            <td class="style2">
                <asp:TextBox ID="txtToDt" class="NormalText" runat="server" 
                    Width="30%" onkeypress="return false" ReadOnly="True" ></asp:TextBox>
             <ajaxToolkit:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtToDt" Format="dd MMM yyyy"></ajaxToolkit:CalendarExtender></td>                       
             <td style="text-align:center; margin:0px auto; width:15%;">
                 &nbsp;</td>          
            </tr> 
           
        <tr> 
            <td id="Remarks"colspan="3" style="width:25%; text-align:center; top:220 auto;">
                &nbsp;&nbsp;&nbsp;<asp:Panel ID="pnDisplay" runat="server" Width="834px">
                </asp:Panel>
            </td>
        
       </tr>
       <tr>
            <td style="text-align:center;" colspan="3">
               <br /><br />
                <input id="btnShow" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="SHOW"  onclick="return btnShow_onclick()" />
                 &nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="EXIT"  onclick="return btnExit_onclick()" /><asp:HiddenField 
                    ID="hid_Items" runat="server" />
                <asp:HiddenField 
                    ID="hid_USer" runat="server" />
                <asp:HiddenField 
                    ID="hid_Value" runat="server" /></td> </tr>
      
      </table>          
   </div>          
    
   
      
</asp:Content>

