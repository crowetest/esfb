﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ProblemStatusReport_0
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 266) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim DT As New DataTable
            Dim CategoryID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("CategoryID")))
            Dim StatusID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("StatusID")))
            Dim PriorityID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("PriorityID")))
            Dim GroupBy As Integer = CInt(GF.Decrypt(Request.QueryString.Get("GroupBy")))
            Dim ID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("ID")))
            Dim SQL As String = ""
            Dim Selection As String = ""
            Dim CategoryFilter As String = ""
            Dim PriorityFilter As String = ""
            Dim StatusFilter As String = ""
            Dim GroupFilter As String = ""

            Selection = "select a.PROBLEM_NO,a.PROBLEM,a.IMPACT,a.CREATE_DT,a.ROOT_CAUSE,a.WORK_AROUND,a.SOLUTION,b.STATUS from HD_PROBLEM_MASTER a,HD_PROBLEM_STATUS b where a.STATUS_ID = b.STATUS_ID"
            If StatusID <> -1 Then
                StatusFilter = " and a.STATUS_ID = " & StatusID & ""
            End If
            If CategoryID <> -1 Then
                CategoryFilter = " and a.CATEGORY_ID = " & CategoryID & ""
            End If
            If PriorityID <> -1 Then
                PriorityFilter = " and a.PRIORITY_ID = " & PriorityID & ""
            End If
            If GroupBy = 0 Then
                GroupFilter = " and a.CATEGORY_ID = " & ID & ""
            ElseIf GroupBy = 1 Then
                GroupFilter = " and a.PRIORITY_ID = " & ID & ""
            Else
                GroupFilter = " and a.STATUS_ID = " & ID & ""
            End If

            SQL = Selection + CategoryFilter + PriorityFilter + StatusFilter + GroupFilter
            DT = DB.ExecuteDataSet(SQL).Tables(0)


            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RH.Heading(Session("FirmName"), tb, "Findings Status Report", 150)
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            Dim StrUserNam As String = Nothing

            Dim TRHead_1 As New TableRow
            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07 As New TableCell

            RH.InsertColumn(TRHead_1, TRHead_1_00, 5, 0, "Findings No")
            RH.InsertColumn(TRHead_1, TRHead_1_01, 15, 0, "Findings")
            RH.InsertColumn(TRHead_1, TRHead_1_02, 15, 0, "Impact")
            RH.InsertColumn(TRHead_1, TRHead_1_03, 5, 0, "Create Date")
            RH.InsertColumn(TRHead_1, TRHead_1_04, 20, 0, "Root Cause")
            RH.InsertColumn(TRHead_1, TRHead_1_05, 15, 0, "Work Around")
            RH.InsertColumn(TRHead_1, TRHead_1_06, 15, 0, "Solution")
            RH.InsertColumn(TRHead_1, TRHead_1_07, 10, 0, "Status")
            tb.Controls.Add(TRHead_1)


            For Each DR In DT.Rows

                Dim TR3 As New TableRow
                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07 As New TableCell
                RH.InsertColumn(TR3, TR3_00, 5, 0, "<a href ='ProblemTicketInfo.aspx?ProblemNo=" + GF.Encrypt(DR(0).ToString()) + "'  target='_blank'>" + DR(0).ToString() + "</a>")
                RH.InsertColumn(TR3, TR3_01, 15, 0, DR(1).ToString())
                RH.InsertColumn(TR3, TR3_02, 15, 0, DR(2).ToString())
                RH.InsertColumn(TR3, TR3_03, 5, 0, Format(DR(3), "dd/MMM/yyyy"))
                If (IsDBNull(DR(4))) Then
                    RH.InsertColumn(TR3, TR3_04, 20, 0, "")
                Else
                    RH.InsertColumn(TR3, TR3_04, 20, 0, DR(4).ToString())
                End If
                If (IsDBNull(DR(5))) Then
                    RH.InsertColumn(TR3, TR3_05, 15, 0, "")
                Else
                    RH.InsertColumn(TR3, TR3_05, 15, 0, DR(5).ToString())
                End If
                If (IsDBNull(DR(6))) Then
                    RH.InsertColumn(TR3, TR3_06, 15, 0, "")
                Else
                    RH.InsertColumn(TR3, TR3_06, 15, 0, DR(6).ToString())
                End If
                If (IsDBNull(DR(7))) Then
                    RH.InsertColumn(TR3, TR3_07, 10, 0, "")
                Else
                    RH.InsertColumn(TR3, TR3_07, 10, 0, DR(7).ToString())
                End If
                tb.Controls.Add(TR3)
            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
