﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CallCapture.aspx.vb" Inherits="CallCapture"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <link href="../Style/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../Style/bootstrap-multiselect.css" type="text/css" />
        <script src="../Script/jquery.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="../Script/bootstrap-2.3.2.min.js"></script>
    <script src="../Script/bootstrap-multiselect_EMP.js" type="text/javascript"></script>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
         .style1
        {
            width: 40%;
            height: 104px;
            padding-left:100px;
        }
.button1 
{
  display: inline-block;
  display: block;
  padding: 5px 2px;
   width: 50px;
   font-size: 12px;
   font-weight: bold;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #0066CC;
  border-radius: 10px;
  border: 3px solid #555555;
  white-space: normal;
}

.button1:hover 
{
    background-color:#99a3a4
}

.button1:active
{
  background-color: #85929e ;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

.button1:focus
{
  background-color:#566573;
  
} 
    </style>
        <script language="javascript" type="text/javascript">
  var click_more=0;       
           
         function window_onload() 
         {

           $('#<%= rdbEmp.ClientID %>').attr('checked', 'checked');
            SelectionOnChange();

                var ToData = "9Ø";          
                ToServer(ToData, 9);
       
         }  


            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("¥");
                for (a = 0; a < rows.length-1; a++) {
                    var cols = rows[a].split("µ");
                    var option1 = document.createElement("OPTION");

                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }

      function EmpCodeOnChange()
        {

        
          if(document.getElementById("<%= hdnCurrentTime.ClientID %>").value =="")
          {
          alert("Please Press Call button");
          document.getElementById("<%= txtEmpCode.ClientID %>").value="";
           return false;
          }

          if (document.getElementById("<%= rdbEmp.ClientID %>").checked == false && document.getElementById("<%= rdbBranch.ClientID %>").checked == false)
            { 
               alert("Select Type"); 
               document.getElementById("<%= rdbEmp.ClientID %>").focus(); 
               document.getElementById("<%= txtEmpCode.ClientID %>").value="";
               return false;
            }
           
            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if(EmpCode!="")
            {
                var ToData = "1Ø" + EmpCode;          
                ToServer(ToData, 1);
            }
        }

        function SelectionOnChange(){
            
            if(document.getElementById("<%= rdbEmp.ClientID %>").checked == true)
            {
                 document.getElementById("<%= txtEmpName.ClientID %>").disabled=true;
                document.getElementById("<%= txtBranchCode.ClientID%>").disabled=true;
                document.getElementById("<%= txtBranchName.ClientID%>").disabled=true;
 
            }
            if(document.getElementById("<%= rdbBranch.ClientID %>").checked == true)
            {

            document.getElementById("<%= txtEmpName.ClientID %>").disabled=false;
             document.getElementById("<%= txtBranchCode.ClientID%>").disabled=false;
             document.getElementById("<%= txtBranchName.ClientID%>").disabled=false;
  
            }
           
        }



        function RelationOnChange() {
            var Relation = document.getElementById("<%= cmbRelation.ClientID %>").value;
            var Branchcode = document.getElementById("<%= txtBranchCode.ClientID %>").value;
                var ToData = "2Ø" + Relation + "Ø" + Branchcode;
                ToServer(ToData, 2);
            
        }
        function BranchOnChange() {

            if (document.getElementById("<%= rdbEmp.ClientID %>").checked == false && document.getElementById("<%= rdbBranch.ClientID %>").checked == false)
            { 
               alert("Select Type "); 
               document.getElementById("<%= rdbEmp.ClientID %>").focus(); 
               return false;
            }

            var Branchcode = document.getElementById("<%= txtBranchCode.ClientID %>").value;
                var ToData = "3Ø" + Branchcode;
                ToServer(ToData, 3);
            
        }
        function CategoryOnChange() {
            var Category = document.getElementById("<%= cmbcategory.ClientID %>").value;
            var ToData = "4Ø" + Category;
            ToServer(ToData, 4);

        }

        function SubCategoryOnChange() {
            var Finding = document.getElementById("<%= cmbsubcategory.ClientID %>").value;
            var ToData = "5Ø" + Finding;
            ToServer(ToData, 5);

        }
      

        function BranchNameOnChange()
        {
            var BranchName =  document.getElementById("<%= txtBranchName.ClientID %>").options[document.getElementById("<%= txtBranchName.ClientID %>").selectedIndex].text; 
            if(BranchName!="")
            {
                var ToData = "8Ø" + BranchName;          
                ToServer(ToData, 8);
            }
        }

//        function GenereateTicketOnChange()
//        {
//               if(document.getElementById("<%= cmbNeedGenerateTicket.ClientID %>").value == 1)
//               {

////            pageData= IncomeOrOutcome + "¥" + EmpCode + "¥" + BranchCode + "¥" + Branch + "¥" + Relation + "¥" + TicketNo + "¥" + TicketDate + "¥" + Category + "¥" + SubCategory + "¥" + Finding
//               
//                var BranchID=document.getElementById("<%= txtBranchCode.ClientID %>").value;
////                 window.open("HelpDeskRequest.aspx","_self"); 
//                 window.open("HelpDeskRequest.aspx?BranchID="+ btoa(BranchID) + " &TypeID=1", "_self"); 

//               }
//        }

     function   MyFunction(htr,date) {
         document.getElementById("<%= txtticketNo.ClientID %>").value = htr;
         document.getElementById("<%= txtticketDate.ClientID %>").value = date;
     }


    function EmpMap(empcode,empname,designation){
         document.getElementById("<%= txtEmpCode.ClientID %>").value=empcode;
         document.getElementById("<%= txtEmpName.ClientID %>").value = empname;
         document.getElementById("<%= txtDesignation.ClientID %>").value = designation;
     }


        function FromServer(arg, context) {

            if (context == 1) {

                if (arg != "") 
                {
                    var Data = arg.split("Ø");
                    var Data1=Data[0].split("¥");
                    var DTL=Data1[0].split("µ");
                    document.getElementById("<%= hdnCallDtl.ClientID %>").value = Data[1];
                     document.getElementById("<%= hdnBranchDtl.ClientID %>").value = Data[2];
                    document.getElementById("<%= txtEmpCode.ClientID %>").value = DTL[0];
                    document.getElementById("<%= txtEmpName.ClientID %>").value = DTL[1];
                    document.getElementById("<%= txtEmpName.ClientID%>").disabled=true;
                    document.getElementById("<%= txtBranchCode.ClientID %>").value = DTL[2];
                    document.getElementById("<%= txtBranchName.ClientID %>").options[document.getElementById("<%= txtBranchName.ClientID %>").selectedIndex].text = DTL[3];
                   document.getElementById("<%= txtDesignation.ClientID %>").value = DTL[4]; 

                }

                else {
                    alert("Invalid Employee Code");
//                    document.getElementById("<%= txtEmpName.ClientID %>").value = "";
//                    document.getElementById("<%= txtBranchName.ClientID %>").value = "";
//                    document.getElementById("<%= txtBranchCode.ClientID %>").value = "";
                  
                    
                }


            }
            else if (context == 2)
             {
                if (arg != "") 
                {

                    var Data = arg.split("Ø");
                   
                    ComboFill(Data[1], "<%= cmbcategory.ClientID %>");
                    if(Data[2]==0)
                    {
                    document.getElementById("<%= hdnTicketDtl.ClientID %>").value = Data[0];
                    }
                    else
                    {
                       document.getElementById("<%= hdnTicketDtl.ClientID %>").value = Data[0];
                       table_fill();
                    }

                   }

                else {
//                    alert("Empty");
                }
            }
            
            else if (context == 3) {
                if (arg != "") {

                 var Data = arg.split("Ø");
                    
                    ComboFill(Data[1], "<%= txtBranchName.ClientID %>");
                   var branchcode= document.getElementById("<%= txtBranchCode.ClientID %>").value;
                    document.getElementById("<%= txtBranchName.ClientID %>").value=branchcode;
                    document.getElementById("<%= hdnBranchDtl.ClientID %>").value = Data[0];
                    BranchList_table_fill();
                     document.getElementById("<%= hdnCallDtl.ClientID %>").value = Data[2];
                     CallList_table_fill();
//                  if (document.getElementById("<%= rdbBranch.ClientID %>").checked == true)
//                  {
////                    document.getElementById("<%= txtBranchCode.ClientID%>").disabled=true;
////                    document.getElementById("<%= txtBranchName.ClientID%>").disabled=true;
//                  }
//                    
                }

                else {
                    alert("Empty");
                }

            }
            else if (context == 4) {

//                var Data = arg.split("Ø");
                               
                ComboFill(arg, "<%= cmbsubcategory.ClientID %>");

                
            }
            else if (context == 5) {

                //                var Data = arg.split("Ø");
               
                ComboFill(arg, "<%= cmbfindings.ClientID %>");


            }
            else if (context == 6) 
            {
                   var Data = arg.split("Ø");
                   if( Data[0]==0)
                   {
                     if(document.getElementById("<%= cmbNeedGenerateTicket.ClientID %>").value == 1)
                     {
                        
                        var callID=Data[2];

                         var BranchID=document.getElementById("<%= txtBranchCode.ClientID %>").value;
                          window.open("HelpDeskRequest.aspx?BranchID="+ btoa(BranchID) + " &TypeID=1&callID="+ btoa(callID) + " ", "_self"); 

                      }
                      else
                      {
                              alert(Data[1]);
                              window.open("CallCapture.aspx", "_self");
                      }
                   }
                   else
                   {
                              alert(Data[1]);
                             window.open("CallCapture.aspx", "_self");
                   }

            }
            else if (context == 7) 
            {

                      ComboFill(arg, "<%= txtBranchName.ClientID %>");
            }
            else if (context == 8)
            {  
                     alert(arg);
                     document.getElementById("<%= txtBranchCode.ClientID %>").value=arg;
                     BranchOnChange();
            }
            else if (context == 9) 
            {
                       var Data = arg.split("Ø");  
                      ComboFill(Data[0], "<%= txtBranchName.ClientID %>");
                      var count = Data[1].split("µ"); 
                      document.getElementById("txtCountDay").innerHTML=count[0];
                      document.getElementById("txtCountMonth").innerHTML=count[1];
            }

        }


        function AddNewRow()
         {
            if (document.getElementById("<%= PnlTicket.ClientID %>").innerHTML.trim() == "") 
            {

                table_fill(); 
            }
            else 
            {
                 document.getElementById("ViewMore").style.display="none";
                document.getElementById("<%= PnlTicket.ClientID %>").innerHTML = "";
            }
        }
        function AddNewRowBranchList() 
        {

            if (document.getElementById("<%= PnlBranch.ClientID %>").innerHTML.trim() == "") 
            {

                BranchList_table_fill();
            }
            else 
            {
                document.getElementById("<%= PnlBranch.ClientID %>").innerHTML = "";
            }

            
        }

        function AddNewRowCallList() 
        {

            if (document.getElementById("<%= PnlCallList.ClientID %>").innerHTML.trim() == "") 
            {

                CallList_table_fill();
            }
            else 
            {
                document.getElementById("<%= PnlCallList.ClientID %>").innerHTML = "";
            }

            
        }


       function ViewMoreTicket()
        {
            if(click_more==0)
            {
                click_more=5;
            }
            else
            {
                 click_more +=5;
            }
            var Relation = document.getElementById("<%= cmbRelation.ClientID %>").value;
            var Branchcode = document.getElementById("<%= txtBranchCode.ClientID %>").value;
            var ToData = "2Ø" + Relation + "Ø" + Branchcode + "Ø" +click_more;
                ToServer(ToData, 2);
        

        }

        function table_fill() {
            if (document.getElementById("<%= hdnTicketDtl.ClientID %>").value != "") {
                document.getElementById("<%= PnlTicket.ClientID %>").style.display = '';
                 document.getElementById("ViewMore").style.display=""; 
                var row_bg = 0;
                var tab = "";
                tab += "<div>";
                tab += "<table style='width:100%'>";
                tab += "<tr>";
                tab += "<td>";
                tab += "<div class='mainhead' style='width:100%; height:250px;overflow: scroll;padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
                tab += "<table id = 'x' style='width:100%; margin:0px auto;font-family:cambria;line-height:20px;background-color:#EEB8A6;' align='center'>";
                tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;' align='left' class='sec2'>";
                tab += "<td style='text-align:center;width:10%;' class='NormalText'>Sl No:</td>";
                tab += "<td style='text-align:center;width:25%;' class='NormalText'>Ticket number</td>";
                 tab += "<td style='text-align:center;width:25%;' class='NormalText'>Request Date</td>";
                tab += "<td style='text-align:center;width:10%;' class='NormalText'>Branch code</td>";
                tab += "<td style='text-align:center;width:30%;' class='NormalText'>Remarks</td>";
                tab += "<td style='text-align:center;width:40%;' colspan='2' class='NormalText'>";
                tab += "</td>";
                tab += "</tr>";
                var data = document.getElementById("<%= hdnTicketDtl.ClientID %>").value.split("¥");

                var count = data.length - 1;
                for (var i = 0; i < count; i++) {
                    var rowData = data[i].toString().split("µ");

                    tab += "<tr style='width:100%;font-family:cambria;' class=sub_first align='left'>";
                    tab += "<td style='text-align:center;width:10%;' class='NormalText'>" + (i + 1) + "</td>";
//                    var htr = rowData[1].toString();
                    tab += "<td style='text-align:center;width:25%;' class='NormalText'><a href='Reports/viewTicketDetails.aspx?RequestID=" + btoa(rowData[0]) + "' style='text-align:right;' target='_blank' onclick='MyFunction(\""+rowData[1]+"\",\""+rowData[2]+"\")'>" + rowData[1] + "</td>";
                    tab += "<td style='text-align:center;width:25%;' class='NormalText'>" + rowData[2] + "</td>";
                    tab += "<td style='text-align:center;width:10%;' class='NormalText'>" + rowData[3] + "</td>";
                     tab += "<td style='text-align:center;width:30%;' class='NormalText'>" + rowData[4] + "</td>";
                    tab += "</tr>";
                }
                tab += "</table>";
                tab += "</div>";
                tab += "</td>";
                tab += "</tr>";
                tab += "</table>";
                tab += "</div>";
                document.getElementById("<%= PnlTicket.ClientID %>").innerHTML = tab;
            }
            else
                document.getElementById("<%= PnlTicket.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }


        function BranchList_table_fill() {
            if (document.getElementById("<%= hdnBranchDtl.ClientID %>").value != "") {
                document.getElementById("<%= PnlBranch.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div>";
                tab += "<table style='width:100%'>";
                tab += "<tr>";
                tab += "<td>";
                tab += "<div class='mainhead' style='width:100%; height:250px;overflow: scroll; padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
                tab += "<table id = 'x' style='width:100%; margin:0px auto;font-family:cambria;line-height:20px;background-color:#EEB8A6;' align='center'>";
                tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;' align='left' class='sec2'>";
                tab += "<td style='text-align:center;width:10%;' class='NormalText'>Sl No:</td>";
                tab += "<td style='text-align:center;width:10%;' class='NormalText'>Emp code</td>";
                tab += "<td style='text-align:center;width:20%;' class='NormalText'>Emp Name</td>";
                tab += "<td style='text-align:center;width:20%;' class='NormalText'>Mobile</td>";
                tab += "<td style='text-align:center;width:30%;' class='NormalText'>Designation</td>";
                tab += "<td style='text-align:center;width:24%;' colspan='2' class='NormalText'>";
                tab += "</td>";
                tab += "</tr>";
                var data = document.getElementById("<%= hdnBranchDtl.ClientID %>").value.split("¥");

                var count = data.length - 1;
                for (var i = 0; i < count; i++) {
                    var rowData = data[i].toString().split("µ");

                    tab += "<tr style='width:100%;font-family:cambria;' class=sub_first align='left'>";
                    tab += "<td style='text-align:center;width:10%;' class='NormalText'>" + (i + 1) + "</td>";
                    tab += "<td style='text-align:center;width:10%;' class='NormalText'><a href='# " + rowData[0] + "' style='text-align:right;' target='_blank' onclick='EmpMap(\""+rowData[0]+"\",\""+rowData[1]+"\",\""+rowData[3]+"\");return false;'>" + rowData[0] + "</td>";
                    tab += "<td style='text-align:center;width:20%;' class='NormalText'>" + rowData[1] + "</td>";
                    tab += "<td style='text-align:center;width:20%;' class='NormalText'>" + rowData[2] + "</td>";
                    tab += "<td style='text-align:center;width:30%;' class='NormalText'>" + rowData[3] + "</td>";
                    tab += "</tr>";
                }
                tab += "</table>";
                tab += "</div>";
                tab += "</td>";
                tab += "</tr>";
                tab += "</table>";
                tab += "</div>";
                document.getElementById("<%= PnlBranch.ClientID %>").innerHTML = tab;
            }
            else
                document.getElementById("<%= PnlBranch.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }


         function CallList_table_fill() {
            if (document.getElementById("<%= hdnCallDtl.ClientID %>").value != "") {
                document.getElementById("<%= PnlCallList.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div>";
                tab += "<table style='width:100%'>";
                tab += "<tr>";
                tab += "<td>";
                tab += "<div class='mainhead' style='width:100%; height:250px;overflow: scroll; padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
                tab += "<table id = 'x' style='width:100%; margin:0px auto;font-family:cambria;line-height:20px;background-color:#EEB8A6;' align='center'>";
                tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;' align='left' class='sec2'>";
                tab += "<td style='text-align:center;width:5%;' class='NormalText'>Sl No:</td>";
                tab += "<td style='text-align:center;width:5%;' class='NormalText'>Emp code</td>";
                tab += "<td style='text-align:center;width:10%;' class='NormalText'>Branch Code</td>";
                tab += "<td style='text-align:center;width:10%;' class='NormalText'>Income/Outgoing</td>";
                tab += "<td style='text-align:center;width:15%;' class='NormalText'>Esaf Related/Fis Related</td>";
                tab += "<td style='text-align:center;width:15%;' class='NormalText'>Call Start Time</td>";
                tab += "<td style='text-align:center;width:15%;' class='NormalText'>Call duration</td>";
                tab += "<td style='text-align:center;width:30%;' class='NormalText'>Remark</td>";
                tab += "<td style='text-align:center;width:24%;' colspan='2' class='NormalText'>";
                tab += "</td>";
                tab += "</tr>";
                var data = document.getElementById("<%= hdnCallDtl.ClientID %>").value.split("¥");

                var count = data.length - 1;
                for (var i = 0; i < count; i++) {
                    var rowData = data[i].toString().split("µ");

                    tab += "<tr style='width:100%;font-family:cambria;' class=sub_first align='left'>";
                    tab += "<td style='text-align:center;width:5%;' class='NormalText'>" + (i + 1) + "</td>";
                    tab += "<td style='text-align:center;width:5%;' class='NormalText'>" + rowData[0] + "</td>";
                    tab += "<td style='text-align:center;width:10%;' class='NormalText'>" + rowData[1] + "</td>";
                    tab += "<td style='text-align:center;width:10%;' class='NormalText'>" + rowData[2] + "</td>";
                    tab += "<td style='text-align:center;width:15%;' class='NormalText'>" + rowData[3] + "</td>";
                    tab += "<td style='text-align:center;width:15%;' class='NormalText'>" + rowData[4] + "</td>";
                    tab += "<td style='text-align:center;width:15%;' class='NormalText'>" + rowData[5] + "</td>";
                    tab += "<td style='text-align:center;width:30%;' class='NormalText'>" + rowData[6] + "</td>";
                    tab += "</tr>";
                }
                tab += "</table>";
                tab += "</div>";
                tab += "</td>";
                tab += "</tr>";
                tab += "</table>";
                tab += "</div>";
                document.getElementById("<%= PnlCallList.ClientID %>").innerHTML = tab;
            }
            else
            {
               alert("No calls are registered in this branch");
                     
                document.getElementById("<%= PnlCallList.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//
             }

        }


        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

     function   btnSave_onclick()
        {

            var IncomeOrOutcome = document.getElementById("<%= IncomeOrOutcome.ClientID %>").value
            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value
            var BranchCode = document.getElementById("<%= txtBranchCode.ClientID %>").value
            var Branch = document.getElementById("<%= txtBranchName.ClientID %>").value
            var Relation = document.getElementById("<%= cmbRelation.ClientID %>").value
            var TicketNo = document.getElementById("<%= txtticketNo.ClientID %>").value
            var TicketDate = document.getElementById("<%= txtticketDate.ClientID %>").value
            var Type = document.getElementById("<%= cmbType.ClientID %>").value
            var Category = document.getElementById("<%= cmbcategory.ClientID %>").value
            var SubCategory = document.getElementById("<%= cmbsubcategory.ClientID %>").value
            var Finding = document.getElementById("<%= cmbfindings.ClientID %>").value
            var Remark = document.getElementById("txtremark").value
//validations

 if (document.getElementById("<%= txtEmpCode.ClientID %>").value== "") 
        {
            alert("Enter Employee code");
            document.getElementById("<%= txtEmpCode.ClientID %>").focus();
            return false;
        }
 if (document.getElementById("<%= txtBranchCode.ClientID %>").value== "") 
        {
            alert("Enter branch code");
            document.getElementById("<%= txtBranchCode.ClientID %>").focus();
            return false;
        }
 if (document.getElementById("<%= txtBranchName.ClientID %>").value== "") 
        {
            alert("Enter branch name");
            document.getElementById("<%= txtBranchName.ClientID %>").focus();
            return false;
        }
if (document.getElementById("<%= cmbRelation.ClientID %>").value== -1) 
        {
            alert("Please select Relation");
            document.getElementById("<%= cmbRelation.ClientID %>").focus();
            return false;
        }

// if (document.getElementById("<%= txtticketNo.ClientID %>").value== "") 
//        {
//            alert("Enter Ticket Number");
//            document.getElementById("<%= txtticketNo.ClientID %>").focus();
//            return false;
//        }

// if (document.getElementById("<%= txtticketDate.ClientID %>").value== "") 
//        {
//            alert("Enter Ticket Date");
//            document.getElementById("<%= txtticketDate.ClientID %>").focus();
//            return false;
//        }
//if (document.getElementById("<%= cmbcategory.ClientID %>").value== -1) 
//        {
//            alert("Please select  Category");
//            document.getElementById("<%= cmbcategory.ClientID %>").focus();
//            return false;
//        }
//if (document.getElementById("<%= cmbsubcategory.ClientID %>").value== -1) 
//        {
//            alert("Please select  subcategory");
//            document.getElementById("<%= cmbsubcategory.ClientID %>").focus();
//            return false;
//        }
//if (document.getElementById("<%= cmbfindings.ClientID %>").value== -1) 
//        {
//            alert("Please select  Problem");
//            document.getElementById("<%= cmbfindings.ClientID %>").focus();
//            return false;
//        }

//            var IncomeOrOutcome = document.getElementById("<%= cmbNeedTeamLeader.ClientID %>").value
            //            var IncomeOrOutcome = document.getElementById("<%= cmbNeedGenerateTicket.ClientID %>").value
                    var Time=document.getElementById("<%= hdnCurrentTime.ClientID %>").value

            var ToData = "6Ø" + IncomeOrOutcome + "Ø" + EmpCode + "Ø" + BranchCode + "Ø" +
             Relation + "Ø" + TicketNo + "Ø" + TicketDate + "Ø" + Category + "Ø" + SubCategory + "Ø" + Finding + "Ø" + Remark + "Ø" + Time.toString() + "Ø" + Type;

            ToServer(ToData, 6);



         }



function timeNow()
{
    var Time=new Date().toLocaleTimeString().replace("/.*(\d{2}:\d{2}:\d{2}).*/", "$1");
    document.getElementById("<%= hdnCurrentTime.ClientID %>").value=Time
    alert(document.getElementById("<%= hdnCurrentTime.ClientID %>").value);
    document.getElementById("<%= call_button.ClientID %>").disabled=true;
    return false;
}




        </script>
    

   
   
<table  style="width:100%;margin: 0px auto;">   
      <tr>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
                </br>
                </br>
            <td style="width:50% ;text-align:left;">
                <div  style="width:90%;margin:0px auto; background-color:#EEB8A6;">
                 <br />
                      <div style="width:95%;height:500px;background-color:white;margin:0px auto; border-radius:25px;">
                      <br />
                            <div style="text-align:center">
                               <asp:ImageButton ID="call_button" style="text-align:right ;" runat="server" Height="40px" Width="40px" ImageAlign="AbsMiddle" ImageUrl="~/Image/call_button.png"/>
                            </div>

                                   <table align="left" class="style1" style="width: 100%;" >
                                    <br />
                                        <tr>
                                            <td style="width:12% ;text-align:left;" >
                                             Incoming/Outgoing&nbsp;&nbsp;<b style="color:red;"></b></td>
                                            <td style="width:20% ;" class="Displays">
                                             &nbsp;&nbsp;<asp:DropDownList 
                                             ID="IncomeOrOutcome" runat="server"  Width="50%" >
                                            <asp:ListItem Value="1"> Incoming</asp:ListItem>
                                            <asp:ListItem Value="2"> Outgoing</asp:ListItem> 
                                            </asp:DropDownList>
                                           </td>
                                        </tr>
                                        <tr >
                                            <td style="width:12%; text-align:left; " >
                                               Selection&nbsp;&nbsp;</td>
                                            <td style="text-align:left; width:10%;">
                                            <asp:RadioButton ID="rdbEmp" runat="server" GroupName="rdb" Text="Employee wise" style="vertical-align:middle;" />
                                             &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:RadioButton ID="rdbBranch" runat="server" GroupName="rdb" Text="BranchWise" style="vertical-align:middle;"/>
                                            </td>
                                        </tr>
                                        <tr >
                                             <td style="width:12%; text-align:left; " >
                                             Employee Code&nbsp;&nbsp;<b style="color:red;">*</b></td>
                                             <td style="width:20% ;" >
                                              &nbsp;&nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Width="50%" class="NormalText"   MaxLength="6" onkeypress="return NumericCheck(event)"></asp:TextBox>
                                             </td>       
                                        </tr>
                                         <tr>
                                              <td style="width:12%; text-align:left; " >
                                              Employee Name&nbsp;&nbsp;<b style="color:red;">*</b></td>
                                             <td style="width:20% ;" >
                                              &nbsp;&nbsp;<asp:TextBox ID="txtEmpName" runat="server" Width="50%" class="NormalText"  ></asp:TextBox>
                                             </td>                   
                                        </tr>
                                         <tr>
                                              <td style="width:12%; text-align:left; " >
                                              Branch Code&nbsp;&nbsp;<b style="color:red;">*</b></td>
                                              <td style="width:20% ;" >
                                              &nbsp;&nbsp;<asp:TextBox ID="txtBranchCode" runat="server" Width="50%" class="NormalText"   onkeypress="return NumericCheck(event)"></asp:TextBox>
                                              </td>                    
                                         </tr>
                                          <tr>
                                               <td style="width:12%; text-align:left; " >
                                               Branch Name&nbsp;&nbsp;<b style="color:red;">*</b></td>
                                               <td style="width:20% ;" >
                                                                        &nbsp;
                                                <%--<asp:TextBox ID="txtBranchName" runat="server" Width="50%" class="NormalText" ></asp:TextBox>--%>
                                                <asp:DropDownList 
                                                 ID="txtBranchName" runat="server"  Width="50%" >
                                                </asp:DropDownList>
                                               </td>
                                          </tr>
                                          <tr>
                                                  <td style="width:12%; text-align:left; " >
                                                  Designation&nbsp;&nbsp;</td>
                                                  <td style="width:20% ;" >
                                                  &nbsp;&nbsp;<asp:TextBox ID="txtDesignation" runat="server" Width="50%" class="NormalText"  ></asp:TextBox>
                                                 </td>
                                          </tr>
                                          <tr>
                                                  <td style="width:12% ;text-align:left;">
                                                  Fis Related/Esaf Related&nbsp;&nbsp;<b style="color:red;">*</b></td>
                                                 <td style="width:20% ;">
                                                  &nbsp;&nbsp;<asp:DropDownList 
                                                   ID="cmbRelation" runat="server"  Width="50%" >
                                                    <asp:ListItem Value="-1">----Select----</asp:ListItem>
                                                    <asp:ListItem Value="1"> Esaf Related</asp:ListItem>
                                                    <asp:ListItem Value="2"> Fis Related</asp:ListItem>
                                                    <asp:ListItem Value="3"> No Ticket</asp:ListItem>
                                                    </asp:DropDownList>
                                                   </td>
                                         </tr>
                                         <tr >
                                                     <td style="width:12%; text-align:left; " >
                                                    Ticket No&nbsp;&nbsp;</td>
                                                    <td style="width:20% ;" >
                                                    &nbsp;&nbsp;<asp:TextBox ID="txtticketNo" runat="server" Width="50%" class="NormalText" ></asp:TextBox>
                                                    </td>
                                         </tr>
                                         <tr>
                                                   <td style="width:12%; text-align:left; " >
                                                   Ticket Date&nbsp;&nbsp;</td>
                                                    <td style="width:20% ;" >
                                                        &nbsp;
                                                     <asp:TextBox ID="txtticketDate" runat="server" Width="50%" class="NormalText"></asp:TextBox>
                                                    <asp:CalendarExtender ID="txtticketDate_CalendarExtender" runat="server" 
                                                      TargetControlID="txtticketDate" Format="dd MMM yyyy"></asp:CalendarExtender>
                                                    </td>
                                         </tr>
                                          <tr>
                                                     <td style="width:12% ;text-align:left;">
                                                     Type&nbsp;&nbsp;</td>
                                                     <td style="width:20% ;">
                                                     &nbsp;&nbsp;<asp:DropDownList 
                                                     ID="cmbType" runat="server"  Width="50%">
                                                     <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                                                     <asp:ListItem Value="1"> Issue </asp:ListItem>
                                                    <asp:ListItem Value="2"> Query </asp:ListItem>
                                                    <asp:ListItem Value="3"> Process</asp:ListItem>
                                                     </asp:DropDownList>
                                                     </td>
                   
                                          </tr>
                                         <tr>
                                                     <td style="width:12% ;text-align:left;">
                                                     Category&nbsp;&nbsp;</td>
                                                     <td style="width:20% ;">
                                                     &nbsp;&nbsp;<asp:DropDownList 
                                                     ID="cmbcategory" runat="server"  Width="50%">
                                                     <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                                                     </asp:DropDownList>
                                                     </td>
                   
                                          </tr>
                                          <tr>
                                                      <td style="width:12% ;text-align:left;">
                                                            Sub Category&nbsp;&nbsp;</td>
                                                      <td style="width:20% ;">
                                                             &nbsp;&nbsp;<asp:DropDownList 
                                                             ID="cmbsubcategory" runat="server"  Width="50%">
                                                        <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                                                         </asp:DropDownList>
                                                     </td>
                                            </tr>
                                          <tr>
                                                     <td style="width:12% ;text-align:left;">
                                                     Findings&nbsp;&nbsp;</td>
                                                    <td style="width:20% ;">
                                                    &nbsp;&nbsp;<asp:DropDownList 
                                                    ID="cmbfindings" runat="server"  Width="50%" >
                                                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                                                    </asp:DropDownList>
                                                    </td>
                                         </tr>
                                          <tr >
                                                    <td style="width:12%; text-align:left; " >
                                                      Remarks&nbsp;&nbsp;</td>
                                                   <td style="width:20% ;" >
                                                      &nbsp;
                                                      <TextArea id="txtremark" class="NormalText" cols="20" name="S1" rows="3"
                                                      maxlength="500" style="width: 50%"  onkeypress="return blockSpecialChar(event)"></textarea> </td>
                                          </tr>
                                          <tr>
                                                    <td style="width:12% ;text-align:left;">
                                                    Need to escalate to Team Leader?&nbsp;&nbsp;<b style="color:red;"></b></td>
                                                    <td style="width:20% ;">
                                                    &nbsp;&nbsp;<asp:DropDownList 
                                                     ID="cmbNeedTeamLeader" runat="server" Width="50%" >
                                                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                                                    <asp:ListItem Value="1"> Yes</asp:ListItem>
                                                    <asp:ListItem Value="0"> No</asp:ListItem> 
                                                   </asp:DropDownList>
                                                   </td>
                                        </tr>
                                        <tr>
                                                   <td style="width:12% ;text-align:left;">
                                                   Need to generate ticket?&nbsp;&nbsp;<b style="color:red;"></b></td>
                                                   <td style="width:20% ;">
                                                    &nbsp;&nbsp;<asp:DropDownList 
                                                   ID="cmbNeedGenerateTicket" runat="server"  Width="50%" >
                                                   <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                                                   <asp:ListItem Value="1"> Yes</asp:ListItem>
                                                   <asp:ListItem Value="0"> No</asp:ListItem> 
                                                   </asp:DropDownList>
                                                 </td>
                                        </tr>

                        </table>  
                 </div>
                <br />
             </div>    

            </td>
         
            <td style="width:50% ;text-align:right;" valign="top">
                  <table style='width:70%; margin:0px auto;font-family:cambria;line-height:30px;background-color:#B21C32;' align='center'>                        
                        <tr style='width:100%;font-family:cambria;background-color:#EEB8A6;text-align:center;' align='center' class='sec2'>
                            <td style="text-align:center;width:50%;">
                                Current Day
                            </td>
                            <td style="text-align:center;width:50%;">
                                Current Month
                            </td>
                        </tr>
                         <tr style='width:100%;font-family:cambria;background-color:white;text-align:center;' align='center' class='sec2'>
                            <td id="txtCountDay" style="text-align:center;background-color:#FFF0E6;height:30px;">
                                
                            </td>
                            <td id="txtCountMonth" style="text-align:center;background-color:#FFF0E6;height:30px;">
                                
                            </td>
                        </tr>
                    </table>
                <table align="center"  class="style1" style="width:85%">
                    <tr>
                        <td style="width:50% ;text-align:left;padding-left:196px" class="mainhead">
                        <a href="#" onclick="AddNewRow()" > 
                        <strong>TICKET LIST</strong> </a> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                         &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                         &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                         &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                       <a id="ViewMore" href="#" onclick="ViewMoreTicket()" style="display:none" > 
                        View more </a> 
                       </td>
                        
                            <tr style="text-align:center;">
                                 <td style="text-align:center;">
                                 <asp:Panel ID="PnlTicket" runat="server">
                                 </asp:Panel>
                                 </td>
                                 
                           </tr>
                   </tr> 
                    <tr>
                       <td style="width:50% ;text-align:center;"class="mainhead">
                        <a href="#" onclick="AddNewRowBranchList()" >  
                        <strong>BRANCH LIST</strong></a> 
                        </td>
                        
                           <tr style="text-align:center;">
                           <td   style="text-align:center;">
                           <asp:Panel ID="PnlBranch" runat="server">
                           </asp:Panel>
                           </td> 
                          </tr>
                 </tr>
                    <tr>
                           <td style="width:50% ;text-align:center;"class="mainhead">
                               <a href="#" onclick="AddNewRowCallList()" >  
                                <strong>CALL LIST</strong></a> 
                           </td>
                               
                          <tr style="text-align:center;">
                          <td   style="text-align:center;">
                           <asp:Panel ID="PnlCallList" runat="server">
                           </asp:Panel>
                           </td>
                  </tr>
                  </tr>
                </table>
            
               
                  

           </td>
    
      </tr>
 
       <tr>
            <td style="width:25% ;text-align:left;">
                &nbsp;</td>
            <td style="width:25% ;">
                &nbsp;</td>
            <td style="width:25% ;text-align:left;">
                &nbsp;</td>
            <td style="width:25% ; text-align:left;">
                &nbsp;</td>
                
        </tr>
           <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
               <asp:HiddenField ID="hdnTicketDtl" runat="server" />
               <asp:HiddenField ID="hdnBranchDtl" runat="server" />
                <asp:HiddenField ID="hdnCallDtl" runat="server" />
                 <asp:HiddenField ID="hdnCurrentTime" runat="server" />
                <input id="btnSave" type="button" value="SAVE" 
                    onclick="return btnSave_onclick()" 
                    style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;
                    <input id="btnExit" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />
                    
                   
               </td>
        </tr>
    </table>


</asp:Content>

