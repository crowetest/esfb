﻿Imports System.Data
Partial Class Leave_ShowAttachment
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim RequestID As Integer = CInt(Request.QueryString.[Get]("RequestID"))
            Dim dt As DataTable = DB.ExecuteDataSet("SELECT attachment,content_Type FROM DMS_ESFB.dbo.HD_REQUEST_ATTACH where Request_Id=" & RequestID & "").Tables(0)
            If dt.Rows.Count > 0 Then
                Dim bytes() As Byte = CType(dt.Rows(0)(0), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = dt.Rows(0)(1).ToString()
                'Response.AddHeader("content-disposition", "attachment") ----------  OPenwith Dialogbox 
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
