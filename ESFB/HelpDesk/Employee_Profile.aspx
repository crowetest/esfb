﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Employee_Profile.aspx.vb" Inherits="Employee_Profile"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
     <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
		<link rel="stylesheet" href="../../Style/bootstrap-multiselect.css" type="text/css" />
		<script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-multiselect_EMP.js"></script>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('#chkveg').multiselect();
            buttonWidth: '500px'
        }); 0
</script>
    <script language="javascript" type="text/javascript">
      $(function() {
			        $('#chkveg').multiselect({                                            
			            includeSelectAllOption: false
                                        
			        });
                     $('#chkveg2').multiselect({                                            
			            includeSelectAllOption: false
                                        
			        });
                      $('#chkveg1').multiselect({                                            
			            includeSelectAllOption: false
                                        
			        });
			      
			    });
     

         function cursorwait(e) {
            document.body.className = 'wait';
        }
           function cursordefault(e) {
            document.body.className = 'default';
        }
        function ReligionOnChange(){
            if(document.getElementById("<%= cmbReligion.ClientID %>").value!="-1")
            {   ClearCombo("<%= cmbCaste.ClientID %>"); 
             var Data = "7Ø" +document.getElementById("<%= cmbReligion.ClientID %>").value;
            ToServer(Data, 7);
                 }
        }
        function window_onload() {
            document.getElementById("btnSave").disabled=true;
        }
        function EmpCodeOnFocus()
        {
            document.getElementById("btnSave").disabled=true;
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
        }
         function btnClear_onclick() 
        {
            window.open("Employee_Profile.aspx","_self");
        }

        function btnSave_onclick() {
         if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "") { alert("Enter Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtEmpName.ClientID %>").value == "") { alert("Enter Employee Name"); document.getElementById("<%= txtEmpName.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1") { alert("Select Branch"); document.getElementById("<%= cmbBranch.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbDepartment.ClientID %>").value == "-1") { alert("Select Department"); document.getElementById("<%= cmbDepartment.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbCadre.ClientID %>").value == "-1") { alert("Select Cadre"); document.getElementById("<%= cmbCadre.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbDesignation.ClientID %>").value == "-1") { alert("Select Designation"); document.getElementById("<%= cmbDesignation.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtDateOfJoin.ClientID %>").value == "") { alert("Enter Date of Join"); document.getElementById("<%= txtDateOfJoin.ClientID %>").focus(); return false; }            
            if (document.getElementById("<%= cmbReportingTo.ClientID %>").value == "-1") { alert("Select Reporting Officer"); document.getElementById("<%= cmbReportingTo.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbEmpPost.ClientID %>").value == "-1") { alert("Select Post"); document.getElementById("<%= cmbEmpPost.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtDOB.ClientID %>").value == "") { alert("Enter Date of Birth"); document.getElementById("<%= txtDOB.ClientID %>").focus(); return false; }            
            if (document.getElementById("<%= cmbGender.ClientID %>").value == "-1") { alert("Select Gender"); document.getElementById("<%= cmbGender.ClientID %>").focus(); return false; }
//            if (document.getElementById("<%= cmbMaritalStatus.ClientID %>").value == "-1") { alert("Select Marital Status"); document.getElementById("<%= cmbMaritalStatus.ClientID %>").focus(); return false; }
//            if (document.getElementById("<%= txtCareOf.ClientID %>").value == "") { alert("Enter Father/Spouse Name"); document.getElementById("<%= txtCareOf.ClientID %>").focus(); return false; }
//            if (document.getElementById("<%= txtCareOfPhoneNo.ClientID %>").value == "") { alert("Enter Father/Spouse's Phone No"); document.getElementById("<%= txtCareOfPhoneNo.ClientID %>").focus(); return false; }
//            if (document.getElementById("<%= txtHouseName.ClientID %>").value == "") { alert("Enter House Name"); document.getElementById("<%= txtHouseName.ClientID %>").focus(); return false; }
//            if (document.getElementById("<%= txtLocation.ClientID %>").value == "") { alert("Enter Location"); document.getElementById("<%= txtLocation.ClientID %>").focus(); return false; }
//            if (document.getElementById("<%= cmbState.ClientID %>").value == "-1") { alert("Select State"); document.getElementById("<%= cmbState.ClientID %>").focus(); return false; }
//            if (document.getElementById("<%= cmbDistrict.ClientID %>").value == "-1") { alert("Select District"); document.getElementById("<%= cmbDistrict.ClientID %>").focus(); return false; }
//            if (document.getElementById("<%= cmbPost.ClientID %>").value == "-1") { alert("Select Post Office"); document.getElementById("<%= cmbPost.ClientID %>").focus(); return false; }
            if(document.getElementById("<%= txtEmail.ClientID %>").value!="")
            { var ret= checkEmail(document.getElementById("<%= txtEmail.ClientID %>")); if(ret==false) return false;}
//            if(document.getElementById("<%= txtLandLine.ClientID %>").value=="" && document.getElementById("<%= txtMobile.ClientID %>").value=="")
//            {alert("Either LandLine or Mobile is Mandatory");document.getElementById("<%= txtLandLine.ClientID %>").focus();return false;}
            if (document.getElementById("<%= cmbEmpType.ClientID %>").value == "-1") { alert("Enter Employee Type"); document.getElementById("<%= cmbEmpType.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbSuretyType.ClientID %>").value == "-1") { alert("Enter Surety Person"); document.getElementById("<%= cmbSuretyType.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtSuretyName.ClientID %>").value == "") { alert("Enter Surety Name"); document.getElementById("<%= txtSuretyName.ClientID %>").focus(); return false; }            
            if (document.getElementById("<%= txtSuretyAddr.ClientID %>").value == "") { alert("Enter Surety Address"); document.getElementById("<%= txtSuretyAddr.ClientID %>").focus(); return false; }            
            if (document.getElementById("<%= txtSuretyPhone.ClientID %>").value == "") { alert("Enter Surety Contact Number"); document.getElementById("<%= txtSuretyPhone.ClientID %>").focus(); return false; }            
//            if (document.getElementById("<%= txtFather.ClientID %>").value == "") { alert("Enter Fathers Name"); document.getElementById("<%= txtFather.ClientID %>").focus(); return false; }            
//            if (document.getElementById("<%= txtMother.ClientID %>").value == "") { alert("Enter Mothers Name"); document.getElementById("<%= txtMother.ClientID %>").focus(); return false; }            
////          if (document.getElementById("<%= txtMarriageDate.ClientID %>").value == "") { alert("Enter Marriage Date"); document.getElementById("<%= txtMarriageDate.ClientID %>").focus(); return false; }              
//            if (document.getElementById("<%= cmbReligion.ClientID %>").value == "-1") { alert("Select Religion"); document.getElementById("<%= cmbReligion.ClientID %>").focus(); return false; }
//            if (document.getElementById("<%= cmbCaste.ClientID %>").value == "-1") { alert("Select Caste"); document.getElementById("<%= cmbCaste.ClientID %>").focus(); return false; }
//            if (document.getElementById("<%= cmbBloodGroup.ClientID %>").value == "-1") { alert("Select Blood Group"); document.getElementById("<%= cmbBloodGroup.ClientID %>").focus(); return false; }
//            if (document.getElementById("<%= cmbMotherTongue.ClientID %>").value == "-1") { alert("Select Mother Tongue"); document.getElementById("<%= cmbMotherTongue.ClientID %>").focus(); return false; }

  //  if (document.getElementById("<%= txtCug.ClientID %>").value == "") { alert("Enter CUG Number"); document.getElementById("<%= txtCug.ClientID %>").focus(); return false; }            
      //  if (document.getElementById("<%= txtOfficialEmail.ClientID %>").value == "") { alert("Enter Official e-mail ID"); document.getElementById("<%= txtOfficialEmail.ClientID %>").focus(); return false; }            

//if(document.getElementById("<%= chkTwoWheeler.ClientID %>").checked){if (document.getElementById("<%= txtDrivingNo.ClientID %>").value == "") { alert("Enter Driving Number"); document.getElementById("<%= txtDrivingNo.ClientID %>").focus(); return false; }            }
            var newstr="";
             row = document.getElementById("<%= hdnQualificationDtl.ClientID %>").value.split("¥");
             var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {     
                    var Qualcol=row[n].split("µ");            
                    if(Qualcol[0]!=-1 && Qualcol[5]!="" && Qualcol[1]!=-1 && Qualcol[2]!="")
                    {                         
                        NewStr+="¥"+Qualcol[0]+"µ"+Qualcol[1]+"µ"+Qualcol[2]+ "µ" +Qualcol[5];
                    }
                
                }

//            if (NewStr=="")
//            {alert("Enter Qualification Details");document.getElementById("cmbQualification"+n).focus();return false; }

             row = document.getElementById("<%= hdnExperience.ClientID %>").value.split("¥");
             var newstrExp=""
                for (n = 1; n <= row.length-1 ; n++) {     
                    var Qualcol2=row[n].split("µ");            
                    if(Qualcol2[0]!="" && Qualcol2[1]!="" && Qualcol2[2]!="")
                    {                         
                        newstrExp+="¥"+Qualcol2[0]+"µ"+Qualcol2[1]+"µ"+Qualcol2[2]+"µ"+Qualcol2[3];
                    }
                
                }

//            if (newstrExp=="")
//            {alert("Enter Experience Details");document.getElementById("cmbQualification"+n).focus();return false; }

             row = document.getElementById("<%= hdnIDProof.ClientID %>").value.split("¥");
             var newstrID=""
                for (n = 1; n <= row.length-1 ; n++) {     
                    var Qualcol3=row[n].split("µ");            
                    if(Qualcol3[0]!=-1 && Qualcol3[1]!="" )
                    {                         
                        newstrID+="¥"+Qualcol3[0]+"µ"+Qualcol3[1];
                    }
                
                }

//            if (newstrID=="")
//            {alert("Enter ID Proof Details");document.getElementById("cmbQualification"+n).focus();return false; }

             row = document.getElementById("<%= hdnAddressProof.ClientID %>").value.split("¥");
             var newstrAddress=""
                for (n = 1; n <= row.length-1 ; n++) {     
                    var Qualcol4=row[n].split("µ");            
                    if(Qualcol4[0]!=-1 && Qualcol4[1]!="")
                    {                         
                        newstrAddress+="¥"+Qualcol4[0]+"µ"+Qualcol4[1];
                    }
                
                }

//            if (newstrAddress=="")
//            {alert("Enter Address Proof Details");document.getElementById("cmbQualification"+n).focus();return false; }
 row = document.getElementById("<%= hdnFamily.ClientID %>").value.split("¥");
             var newstrFamily=""
                for (n = 1; n <= row.length-1 ; n++) {     
                    var Qualcol5=row[n].split("µ");            
                    if(Qualcol5[0]!="-1" && Qualcol5[1]!="" && Qualcol5[2]!="")
                    {                         
                        newstrFamily+="¥"+Qualcol5[0]+"µ"+Qualcol5[1]+"µ"+Qualcol5[2]+"µ"+Qualcol5[3];
                    }
                
                }
//            if (newstrFamily=="")
//            {alert("Enter Family Details");document.getElementById("cmbFamily"+n).focus();return false; }
//            if (document.getElementById("<%= txtBasicPay.ClientID %>").value == "") { alert("Enter Basic Pay"); document.getElementById("<%= txtBasicPay.ClientID %>").focus(); return false; }
 if(document.getElementById("<%= txtOfficialEmail.ClientID %>").value!="")
            { var ret= checkEmail(document.getElementById("<%= txtOfficialEmail.ClientID %>")); if(ret==false){alert("Invalid email"); return false;}}
            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            var EmpName = document.getElementById("<%= txtEmpName.ClientID %>").value;
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
            var BranchID = BranchDtl[0];
            var DepartmentID = document.getElementById("<%= cmbDepartment.ClientID %>").value;
            var CadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;
            var DesignationID = document.getElementById("<%= cmbDesignation.ClientID %>").value;
            var DateOfJoin = document.getElementById("<%= txtDateOfJoin.ClientID %>").value;
            var GenderID = document.getElementById("<%= cmbGender.ClientID %>").value;
            var ReportingTo = document.getElementById("<%= cmbReportingTo.ClientID %>").value;
            var empPost = document.getElementById("<%= cmbEmpPost.ClientID %>").value;            
            var isFieldStaff = (document.getElementById("<%= chkFieldStaff.ClientID %>").checked) ? 1 : 0;   
            var CareOf= document.getElementById("<%= txtCareOf.ClientID %>").value;    
            var CareOfPhone= document.getElementById("<%= txtCareOfPhoneNo.ClientID %>").value;  
            var HouseName= document.getElementById("<%= txtHouseName.ClientID %>").value; 
            var Location=  document.getElementById("<%= txtLocation.ClientID %>").value;
            var City=  document.getElementById("<%= txtCity.ClientID %>").value;
            var PostDtl=document.getElementById("<%= cmbPost.ClientID %>").value.split("~");
            var PostID=PostDtl[0];
            var Email=document.getElementById("<%= txtEmail.ClientID %>").value;
            var Landline=document.getElementById("<%= txtLandLine.ClientID %>").value;
            var IDProofType =(document.getElementById("<%= cmbIDProof.ClientID %>").value==-1) ? 1 : document.getElementById("<%= cmbIDProof.ClientID %>").value;
            var AddressProofType=(document.getElementById("<%= cmbAddressProof.ClientID %>").value==-1) ? 1 : document.getElementById("<%= cmbAddressProof.ClientID %>").value;
            var IDProofNumber=document.getElementById("<%= txtIDProof.ClientID %>").value;
            var AddressProofNumber=document.getElementById("<%= txtAddressProof.ClientID %>").value;
            var Mobile=document.getElementById("<%= txtMobile.ClientID %>").value;
            var BasicPay=(document.getElementById("<%= txtBasicPay.ClientID %>").value=="") ? 0 : document.getElementById("<%= txtBasicPay.ClientID %>").value;
            var DA=(document.getElementById("<%= txtDA.ClientID %>").value=="" || document.getElementById("<%= txtDA.ClientID %>").value=="0" ) ? 0 : document.getElementById("<%= txtDA.ClientID %>").value;
            var HRA=(document.getElementById("<%= txtHRA.ClientID %>").value=="" || document.getElementById("<%= txtHRA.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtHRA.ClientID %>").value;
            var Conveyance=(document.getElementById("<%= txtConveyance.ClientID %>").value=="" || document.getElementById("<%= txtConveyance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtConveyance.ClientID %>").value;
            var SA=(document.getElementById("<%= txtSpecialAllowance.ClientID %>").value=="" || document.getElementById("<%= txtSpecialAllowance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtSpecialAllowance.ClientID %>").value;
            var LA=(document.getElementById("<%= txtLocalAllowance.ClientID %>").value=="" || document.getElementById("<%= txtLocalAllowance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtLocalAllowance.ClientID %>").value;
            var MA=(document.getElementById("<%= txtMedicalAllowance.ClientID %>").value=="" || document.getElementById("<%= txtMedicalAllowance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtMedicalAllowance.ClientID %>").value;
            var HA=(document.getElementById("<%= txtHospitality.ClientID %>").value=="" || document.getElementById("<%= txtHospitality.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtHospitality.ClientID %>").value;
            var PA=(document.getElementById("<%= txtPerformance.ClientID %>").value=="" || document.getElementById("<%= txtPerformance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtPerformance.ClientID %>").value;
            var OA=(document.getElementById("<%= txtOtherAllowance.ClientID %>").value=="" || document.getElementById("<%= txtOtherAllowance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtOtherAllowance.ClientID %>").value;
            var FA=(document.getElementById("<%= txtFieldAllowance.ClientID %>").value=="" || document.getElementById("<%= txtFieldAllowance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtFieldAllowance.ClientID %>").value;
            var ESI=(document.getElementById("<%= txtESI.ClientID %>").value=="" || document.getElementById("<%= txtESI.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtESI.ClientID %>").value;
            var PF=(document.getElementById("<%= txtPF.ClientID %>").value=="" || document.getElementById("<%= txtPF.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtPF.ClientID %>").value;
            var SWF=(document.getElementById("<%= txtSWF.ClientID %>").value=="" || document.getElementById("<%= txtSWF.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtSWF.ClientID %>").value;
            var ESWT=(document.getElementById("<%= txtESWT.ClientID %>").value=="" || document.getElementById("<%= txtESWT.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtESWT.ClientID %>").value;
            var Charity=(document.getElementById("<%= txtCharity.ClientID %>").value=="" || document.getElementById("<%= txtCharity.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtCharity.ClientID %>").value;
            var DOB=document.getElementById("<%= txtDOB.ClientID %>").value;
            var MStatusID=(document.getElementById("<%= cmbMaritalStatus.ClientID %>").value==-1) ? 2 :document.getElementById("<%= cmbMaritalStatus.ClientID %>").value;
            var IsTwowheeler = (document.getElementById("<%= chkTwoWheeler.ClientID %>").checked) ? 1 : 0; 
            var IsSangamMember = (document.getElementById("<%= chkSangam.ClientID %>").checked) ? 1 : 0; 
            var MarriageDate=document.getElementById("<%= txtMarriageDate.ClientID %>").value;
            var DrivingNo=document.getElementById("<%= txtDrivingNo.ClientID %>").value;
            var Father=document.getElementById("<%= txtFather.ClientID %>").value;
            var Mother=document.getElementById("<%= txtMother.ClientID %>").value;
            var ReligionId=document.getElementById("<%= cmbReligion.ClientID %>").value;
            var CasteId=document.getElementById("<%= cmbCaste.ClientID %>").value;
            var MotherTongueId=document.getElementById("<%= cmbMotherTongue.ClientID %>").value;
            var BloodGroupId=document.getElementById("<%= cmbBloodGroup.ClientID %>").value;
            var Cug=document.getElementById("<%= txtCug.ClientID %>").value;
            var OfficialEmail=document.getElementById("<%= txtOfficialEmail.ClientID %>").value;
            var ChkPanch = (document.getElementById("<%= chkPanchayath.ClientID %>").checked) ? 1 : 0; 
            var chkComm = (document.getElementById("<%= chkCommunity.ClientID %>").checked) ? 1 : 0; 
            var chkPolice = (document.getElementById("<%= chkPolice.ClientID %>").checked) ? 1 : 0; 
             
             var SelectedItems =$('#chkveg2').val(); 
             if(SelectedItems == null){
             SelectedItems="";
             }
             //alert(SelectedItems);
            
            var EmpType=document.getElementById("<%= cmbEmpType.ClientID %>").value;
            var SuretyType=document.getElementById("<%= cmbSuretyType.ClientID %>").value;
            var SuretyName=document.getElementById("<%= txtSuretyName.ClientID %>").value;
            var SuretyAddr=document.getElementById("<%= txtSuretyAddr.ClientID %>").value;
            var SuretyPhone=document.getElementById("<%= txtSuretyPhone.ClientID %>").value;
            var Data = "1Ø" + EmpCode + "Ø" + EmpName + "Ø" + BranchID + "Ø" + DepartmentID + "Ø" + CadreID + "Ø" + DesignationID + "Ø" + DateOfJoin + "Ø" + GenderID + "Ø" + ReportingTo + "Ø" + isFieldStaff + "Ø" + CareOf+ "Ø" + HouseName + "Ø" + Location+"Ø"+City+"Ø"+PostID+"Ø"+Email+"Ø"+Landline+"Ø"+Mobile+"Ø"+BasicPay+"Ø"+DA+"Ø"+HRA+"Ø"+Conveyance+"Ø"+SA+"Ø"+LA+"Ø"+MA+"Ø"+HA+"Ø"+PA+"Ø"+OA+"Ø"+FA+"Ø"+ESI+"Ø"+PF+"Ø"+SWF+"Ø"+ESWT+"Ø"+Charity+"Ø"+DOB+"Ø"+MStatusID+"Ø"+CareOfPhone+"Ø"+IDProofType+"Ø"+IDProofNumber+"Ø"+AddressProofType+"Ø"+AddressProofNumber+"Ø"+NewStr+"Ø"+empPost+ "Ø" +Father+ "Ø" +Mother+ "Ø" +ReligionId+ "Ø" +CasteId+ "Ø" +IsTwowheeler+ "Ø" +DrivingNo+ "Ø" +IsSangamMember+ "Ø" +MarriageDate+ "Ø" +MotherTongueId+ "Ø" +BloodGroupId+ "Ø" +Cug+ "Ø" +OfficialEmail+ "Ø" +ChkPanch+ "Ø" +chkComm+ "Ø" +chkPolice+ "Ø" +newstrExp+ "Ø" +newstrID+ "Ø" +newstrAddress+ "Ø" +SelectedItems+ "Ø" +EmpType+ "Ø" +SuretyType+ "Ø" +SuretyName+ "Ø" +SuretyAddr+ "Ø" +SuretyPhone+ "Ø" +newstrFamily;
             cursorwait();
            document.getElementById("btnSave").disabled=true;
          
            ToServer(Data, 1);
            
        }

        function FromServer(Arg, Context) {
           switch (Context) {

                    case 1:
                    {
                        var Data = Arg.split("Ø");
                        cursordefault();
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("HRProfileUpdation.aspx", "_self");
                        else
                        { document.getElementById("btnSave").disabled=false;}
                        break;
                    }    
                    case 2:
                    {
                     document.getElementById("ImgPhoto").setAttribute('src',Arg); 
                     break;
                    }       
                    case 3: 
                    {    
                        if(Arg=="ØØØØ")  
                        {
                            alert("Invalid Employee Number");  document.getElementById("<%= cmbState.ClientID %>").focus();    break;      
                        } 
                        else
                        {
                            FillData(Arg); 
                            if (document.getElementById("<%= hdnBtnEnable.ClientID %>").value == 1) {
                               document.getElementById("btnSave").disabled=false;   
                               }
                               else   
                               {
                                document.getElementById("btnSave").disabled=true;    
                               }                       
                        }
                        break;
                    }
                     case 4: {                   
                            ComboFill(Arg, "<%= cmbDesignation.ClientID %>");
                            break;
                         }
                    case 5: {                   
                            ComboFill(Arg, "<%= cmbDistrict.ClientID %>");
                            break;
                            }
                    case 6: {                   
                            ComboFill(Arg, "<%= cmbPost.ClientID %>");
                            break;
                            }

                    case 7: {                  
                            ComboFill(Arg, "<%= cmbCaste.ClientID %>");
                            break;
                            }
                    
                    }
        }
        function BranchOnChange() {
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
            if (BranchDtl[1] == 1 || BranchDtl[1]==3) {
                document.getElementById("<%= cmbDepartment.ClientID %>").value = 21;
                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = true ;               
            }
            else {
                document.getElementById("<%= cmbDepartment.ClientID %>").value = "-1";
                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = false;              
            }
            ChangeReportingOfficer();
        }
        function ChangeReportingOfficer() {
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
            if(BranchDtl!="-1")
            {
                var BranchID = BranchDtl[0];
                var DepartmentID = document.getElementById("<%= cmbDepartment.ClientID %>").value;
                var DesignationID = document.getElementById("<%= cmbDesignation.ClientID %>").value;
                if(DepartmentID>0 && BranchID>=0 && DesignationID>0)
                    ToServer("2Ø" + BranchID + "Ø" + DepartmentID + "Ø" + DesignationID + "Ø", 2);
            }
        }
        function CadreOnChange() {
            var CadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;           
            var ToData = "4Ø" + CadreID;          
            ToServer(ToData, 4);

        }
          function StateOnChange() {
           ClearCombo("<%= cmbPost.ClientID %>");
            var StateID = document.getElementById("<%= cmbState.ClientID %>").value;           
            var ToData = "5Ø" + StateID;          
            ToServer(ToData, 5);

        }
          function DistrictOnChange() {
            var DistrictID = document.getElementById("<%= cmbDistrict.ClientID %>").value;           
            var ToData = "6Ø" + DistrictID;          
            ToServer(ToData, 6);

        }

        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function BaicPayOnChange()
        {
           var BasicPay=document.getElementById("<%= txtBasicPay.ClientID %>").value;           
           if(BasicPay!="")
           {
             var Perc1Amount=Math.round(BasicPay*(parseFloat(document.getElementById("<%= hdnSFWTRate.ClientID %>").value/ 100)));
             document.getElementById("<%= txtESWT.ClientID %>").value=Perc1Amount;
             Perc1Amount=Math.round(BasicPay*(parseFloat(document.getElementById("<%= hdnCharityRate.ClientID %>").value/ 100)));
             document.getElementById("<%= txtCharity.ClientID %>").value=Perc1Amount;
           }
           CalculateTotalEarnings();
           CalculateTotalDeduction();
        } 
        function MaritalStatusOnChange()
        {         
          if(document.getElementById("<%= cmbMaritalStatus.ClientID %>").value==-1)
          {
            document.getElementById("FatHus").innerHTML ="Guardian";
           
          }
          else if(document.getElementById("<%= cmbMaritalStatus.ClientID %>").value==1 || document.getElementById("<%= cmbMaritalStatus.ClientID %>").value==3 )
          {
            document.getElementById("FatHus").innerHTML ="Spouse";
          
          }
          else
          {
            document.getElementById("FatHus").innerHTML ="Father";
           
          }
        }
          function AddNewRow() {                          
                if (document.getElementById("<%= hdnQualificationDtl.ClientID %>").value != "") {                      
                    row = document.getElementById("<%= hdnQualificationDtl.ClientID %>").value.split("¥");
                    var Len = row.length - 1;          
                    col = row[Len].split("µ");                      
                    if (col[0] != "-1" && col[1] != "-1" && col[2] != "") {

                        document.getElementById("<%= hdnQualificationDtl.ClientID %>").value += "¥-1µ-1µµ";                          
                        
                    }
                }
                else
                document.getElementById("<%= hdnQualificationDtl.ClientID %>").value = "¥-1µ-1µµ"; 
                table_fill();
            }
              function AddNewRowExperience() {                          
                if (document.getElementById("<%= hdnExperience.ClientID %>").value != "") {                      
                    row = document.getElementById("<%= hdnExperience.ClientID %>").value.split("¥");
                    var Len = row.length - 1;          
                    col = row[Len].split("µ");                      
                    if (col[0] != "" && col[1] != "" && col[2] != "") {

                        document.getElementById("<%= hdnExperience.ClientID %>").value += "¥µµµ";                          
                        
                    }
                }
                else
                document.getElementById("<%= hdnExperience.ClientID %>").value = "¥µµµ"; 
                table_fillExperience();
            }
            function AddNewRowIDProof() {                       
                if (document.getElementById("<%= hdnIDProof.ClientID %>").value != "") {                      
                    row = document.getElementById("<%= hdnIDProof.ClientID %>").value.split("¥");
                    var Len = row.length - 1;          
                    col = row[Len].split("µ");                      
                    if (col[0] != "-1" && col[1] != "") {

                        document.getElementById("<%= hdnIDProof.ClientID %>").value += "¥-1µµ";                          
                        
                    }
                }
                else
                document.getElementById("<%= hdnIDProof.ClientID %>").value = "¥-1µµ"; 
                
                table_fillIDProof();
            }
            function AddNewRowAddressProof() {                         
                if (document.getElementById("<%= hdnAddressProof.ClientID %>").value != "") {                      
                    row = document.getElementById("<%= hdnAddressProof.ClientID %>").value.split("¥");
                    var Len = row.length - 1;          
                    col = row[Len].split("µ");                      
                    if (col[0] != "-1" && col[1] != "" ) {

                        document.getElementById("<%= hdnAddressProof.ClientID %>").value += "¥-1µµ";                          
                        
                    }
                }
                else
                document.getElementById("<%= hdnAddressProof.ClientID %>").value = "¥-1µµ"; 
                table_fillAddressProof();
            }
            function AddNewRowFamily() {                          
                if (document.getElementById("<%= hdnFamily.ClientID %>").value != "") {                      
                    row = document.getElementById("<%= hdnFamily.ClientID %>").value.split("¥");
                    var Len = row.length - 1;          
                    col = row[Len].split("µ");                      
                    if (col[0] != "-1" && col[1] != "" && col[2] != "" && col[3] != "") {

                        document.getElementById("<%= hdnFamily.ClientID %>").value += "¥-1µµµ";                          
                        
                    }
                }
                else
                document.getElementById("<%= hdnFamily.ClientID %>").value = "¥-1µµµ"; 
                table_fillFamily();
            }
            function Fill(){
                table_fill();
                table_fillExperience();
                table_fillIDProof();
                table_fillAddressProof();
                table_fillFamily();
                 
            }
        function table_fill() 
        {
         if (document.getElementById("<%= hdnQualificationDtl.ClientID %>").value != "") {
            document.getElementById("<%= pnQualification.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>"; 
            tab += "<td style='width:15%;text-align:left'>Level</td>"; 
            tab += "<td style='width:20%;text-align:left'>Qualification</td>";   
            tab += "<td style='width:35%;text-align:left'>University</td>";
            tab += "<td style='width:15%;text-align:left'>Year Of Passing</td>";    
            tab += "<td style='width:10%;text-align:left'></td>";  
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
           
                row = document.getElementById("<%= hdnQualificationDtl.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");                    
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                                                     
                    tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";   
                    if(col[0]==-1)     
                    {           
                    var select1 = CreateSelect(n,1,col[0])                                                
                    tab += "<td style='width:15%;text-align:left'>" + select1 + "</td>";
                    }
                    else
                    {
                     tab += "<td style='width:15%;text-align:left'>" + col[3] + "</td>";
                    }

                    if(col[0]==-1)     
                    {           
                     var txtQualiBox = "<input id='txtQuali" + n + "' name='txtQuali" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='50' />";                                            
                    tab += "<td style='width:20%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    else
                    {

                     tab += "<td style='width:20%;text-align:left' class='NormalText'>" + col[5] + "</td>";
                    }

                    if(col[1]==-1)
                    {
                    var select2 = CreateSelect(n,2,col[1])                                  
                    tab += "<td style='width:35%;text-align:left'>" + select2 + "</td>";
                    }
                    else
                    {
                    tab += "<td style='width:35%;text-align:left'>" + col[4] + "</td>";
                    }
                    if(col[2]!="")
                    {
                       tab += "<td style='width:15%;text-align:center' >"+ col[2] +"</td>";
                      
                      }
                    else
                    {
                      var txtBox = "<input id='txtYear" + n + "' name='txtYear" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='4' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'  onkeydown='CreateNewRow(event," + n + ")' />";
                        tab += "<td style='width:15%;text-align:left' >"+ txtBox +"</td>";
                    }
                      
                        //var txtBox = "<input id='txtYear" + n + "' name='txtYear" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='4' value='" + col[2] + "' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")' onkeydown='CreateNewRow(event," + n + ")' />";
                    if(col[0]==-1 && col[1]==-1 && col[2]=="")
                    tab +="<td style='width:10%;text-align:left'></td>";
                    else
                    tab += "<td style='width:10%;text-align:center' onclick=DeleteRow('" + n + "')><img  src='../../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                    tab += "</tr>";
                }
            
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnQualification.ClientID %>").innerHTML = tab;
            setTimeout(function() {
                document.getElementById("cmbQualification"+(n-1)).focus().select();
                }, 4);
            }
            else
            document.getElementById("<%= pnQualification.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }
        
        function table_fillExperience() 
        {
         if (document.getElementById("<%= hdnExperience.ClientID %>").value != "") {
            document.getElementById("<%= pnExperience.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center' >#</td>"; 
            tab += "<td style='width:30%;text-align:left' >Company</td>"; 
            tab += "<td style='width:30%;text-align:left' >Address</td>";   
            tab += "<td style='width:15%;text-align:left' >Phone</td>";
             tab += "<td style='width:10%;text-align:left'>No of years</td>";  
             tab += "<td style='width:10%;text-align:left'></td>";  
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
           
                row = document.getElementById("<%= hdnExperience.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");                    
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                                                     
                    tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";   
                    if(col[0]!="")
                    {
                       tab += "<td style='width:30%;text-align:left' >"+ col[0] +"</td>";
                      
                      }
                    else
                    {
                      var txtBox = "<input id='txtCompany" + n + "' name='txtCompany" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='100' />";
                        tab += "<td style='width:30%;text-align:left' >"+ txtBox +"</td>";
                       
                    }
                    if(col[1]!="")
                    {
                       tab += "<td style='width:30%;text-align:left' >"+ col[1] +"</td>";
                      
                      }
                    else
                    {
                      var txtBox = "<input id='txtAddress" + n + "' name='txtAddress" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='100'  />";
                        tab += "<td style='width:30%;text-align:left' >"+ txtBox +"</td>";
                    }
                    if(col[2]!="")
                    {
                       tab += "<td style='width:15%;text-align:left' >"+ col[2] +"</td>";
                      
                      }
                    else
                    {
                      var txtBox = "<input id='txtPhone" + n + "' name='txtPhone" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='15' onkeypress='return NumericCheck(event)' />";
                        tab += "<td style='width:15%;text-align:left' >"+ txtBox +"</td>";
                    }
                     if(col[3]!="")
                    {
                       tab += "<td style='width:10%;text-align:left' >"+ col[3] +"</td>";
                      
                      }
                    else
                    {
                      var txtBox = "<input id='txtYears" + n + "' name='txtYears" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='4' onkeypress='return NumericWithDot(this,event)' onchange='updateValueExperience("+ n +")'  onkeydown='CreateNewRowExperience(event," + n + ")' />";
                        tab += "<td style='width:10%;text-align:left' >"+ txtBox +"</td>";
                    }
                     
                        //var txtBox = "<input id='txtYear" + n + "' name='txtYear" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='4' value='" + col[2] + "' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")' onkeydown='CreateNewRow(event," + n + ")' />";
                    if(col[0]=="" && col[1]=="" && col[2]=="")
                    tab +="<td style='width:10%;text-align:left'></td>";
                    else
                    tab += "<td style='width:10%;text-align:center' onclick=DeleteRowExperience('" + n + "')><img  src='../../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                    tab += "</tr>";
                }
            
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnExperience.ClientID %>").innerHTML = tab;
            setTimeout(function() {
                document.getElementById("txtCompany"+(n-1)).focus().select();
                }, 4);
            }
            else
            document.getElementById("<%= pnExperience.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }
         function table_fillFamily() 
        {
         if (document.getElementById("<%= hdnFamily.ClientID %>").value != "") {
            document.getElementById("<%= pnFamily.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center' >#</td>"; 
            tab += "<td style='width:30%;text-align:left' >Person Type</td>"; 
            tab += "<td style='width:30%;text-align:left' >Name</td>";   
            tab += "<td style='width:15%;text-align:left' >Age</td>";
             tab += "<td style='width:10%;text-align:left'>Occupation</td>";  
             tab += "<td style='width:10%;text-align:left'></td>";  
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
           
                row = document.getElementById("<%= hdnFamily.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");                    
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                                                     
                    tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";   
                       
                    if(col[0]=="-1")     
                    {           
                    var select1 = CreateSelectFamily(n,1,col[0])                                                
                    tab += "<td style='width:30%;text-align:left'>" + select1 + "</td>";
                    }
                    else
                    {
                     tab += "<td style='width:30%;text-align:left'>" + col[4] + "</td>";
                    }
                    if(col[1]!="")
                    {
                       tab += "<td style='width:30%;text-align:left' >"+ col[1] +"</td>";
                      
                      }
                    else
                    {
                      var txtBox = "<input id='txtPName" + n + "' name='txtPName" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='100'  />";
                        tab += "<td style='width:30%;text-align:left' >"+ txtBox +"</td>";
                    }
                    if(col[2]!="")
                    {
                       tab += "<td style='width:15%;text-align:left' >"+ col[2] +"</td>";
                      
                      }
                    else
                    {
                      var txtBox = "<input id='txtPDob" + n + "' name='txtPDob" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='15' onkeypress='return NumericCheck(event)' />";
                        tab += "<td style='width:15%;text-align:left' >"+ txtBox +"</td>";
                    }
                     if(col[3]!="")
                    {
                       tab += "<td style='width:10%;text-align:left' >"+ col[3] +"</td>";
                      
                      }
                    else
                    {
                      var txtBox = "<input id='txtOccupation" + n + "' name='txtOccupation" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='50' onchange='updateValueFamily("+ n +")'  onkeydown='CreateNewRowFamily(event," + n + ")' />";
                        tab += "<td style='width:10%;text-align:left' >"+ txtBox +"</td>";
                    }
                     
                        //var txtBox = "<input id='txtYear" + n + "' name='txtYear" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='4' value='" + col[2] + "' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")' onkeydown='CreateNewRow(event," + n + ")' />";
                    if(col[0]=="-1" && col[1]=="" && col[2]=="")
                    tab +="<td style='width:10%;text-align:left'></td>";
                    else
                    tab += "<td style='width:10%;text-align:center' onclick=DeleteRowFamily('" + n + "')><img  src='../../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                    tab += "</tr>";
                }
            
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnFamily.ClientID %>").innerHTML = tab;
            setTimeout(function() {
                document.getElementById("cmbPType"+(n-1)).focus().select();
                }, 4);
            }
            else
            document.getElementById("<%= pnFamily.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }
       
        function table_fillAddressProof() 
        {    if (document.getElementById("<%= hdnAddressProof.ClientID %>").value != "") {
            document.getElementById("<%= pnAddressProof.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center' >#</td>"; 
            tab += "<td style='width:45%;text-align:left' >Address Proof Type</td>"; 
            tab += "<td style='width:40%;text-align:left'>Address Proof No</td>";    
            tab += "<td style='width:10%;text-align:left'></td>";  
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
           
                row = document.getElementById("<%= hdnAddressProof.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");                    
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                                                     
                    tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";   
                    if(col[0]==-1)     
                    {           
                    var select1 = CreateSelectAddressProof(n,1,col[0])                                                
                    tab += "<td style='width:45%;text-align:left'>" + select1 + "</td>";
                    }
                    else
                    {
                     tab += "<td style='width:45%;text-align:left'>" + col[2] + "</td>";
                    }

                   
                    if(col[1]!="")
                    {
                       tab += "<td style='width:40%;text-align:left' >"+ col[1] +"</td>";
                      
                      }
                    else
                    {
                      var txtBox = "<input id='txtAddressProof" + n + "' name='txtAddressProof" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='50'  onchange='updateValueAddressProof("+ n +")'  onkeydown='CreateNewRowAddressProof(event," + n + ")' />";
                        tab += "<td style='width:40%;text-align:left' >"+ txtBox +"</td>";
                    }
                      
                        //var txtBox = "<input id='txtYear" + n + "' name='txtYear" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='4' value='" + col[2] + "' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")' onkeydown='CreateNewRow(event," + n + ")' />";
                    if(col[0]==-1)
                    tab +="<td style='width:10%;text-align:left'></td>";
                    else
                    tab += "<td style='width:10%;text-align:center' onclick=DeleteRowAddressProof('" + n + "')><img  src='../../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                    tab += "</tr>";
                }
            
            tab += "</table></div></div></div>";
           
            document.getElementById("<%= pnAddressProof.ClientID %>").innerHTML = tab;
            setTimeout(function() {
                document.getElementById("cmbAddressproof"+(n-1)).focus().select();
                }, 4);
            }
            else
            document.getElementById("<%= pnAddressProof.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//

        }
        function table_fillIDProof() 
        {
         if (document.getElementById("<%= hdnIDProof.ClientID %>").value != "") {
            document.getElementById("<%= pnIDProof.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center' >#</td>"; 
            tab += "<td style='width:45%;text-align:left' >ID Proof Type</td>"; 
            tab += "<td style='width:40%;text-align:left'>ID Proof No</td>";    
            tab += "<td style='width:10%;text-align:left'></td>";  
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
           
                row = document.getElementById("<%= hdnIDProof.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");                    
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                                                     
                    tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";   
                    if(col[0]==-1)     
                    {           
                    var select1 = CreateSelectIDProof(n,1,col[0]) ;                                               
                    tab += "<td style='width:45%;text-align:left'>" + select1 + "</td>";
                    }
                    else
                    {
                     tab += "<td style='width:45%;text-align:left'>" + col[2] + "</td>";
                    }

                   
                    if(col[1]!="")
                    {
                       tab += "<td style='width:40%;text-align:left' >"+ col[1] +"</td>";
                      
                      }
                    else
                    {
                      var txtBox = "<input id='txtIDProofNo" + n + "' name='txtIDProofNo" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='50' onchange='updateValueIDProof("+ n +")'  onkeydown='CreateNewRowIDProof(event," + n + ")' />";
                        tab += "<td style='width:40%;text-align:left' >"+ txtBox +"</td>";
                    }
                      
                        //var txtBox = "<input id='txtYear" + n + "' name='txtYear" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='4' value='" + col[2] + "' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")' onkeydown='CreateNewRow(event," + n + ")' />";
                    if(col[0]==-1)
                    tab +="<td style='width:10%;text-align:left'></td>";
                    else
                    tab += "<td style='width:10%;text-align:center' onclick=DeleteRowIDProof('" + n + "')><img  src='../../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                    tab += "</tr>";
                }
            
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnIDProof.ClientID %>").innerHTML = tab;
            setTimeout(function() {
                document.getElementById("cmbIDproof"+(n-1)).focus().select();
                }, 4);
            }
            else
            document.getElementById("<%= pnIDProof.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }
       function CreateNewRow(e, val) {
                        var n = (window.Event) ? e.which : e.keyCode;
                        if (n == 13 || n == 9) {
                            updateValue(val);
                            AddNewRow();                          
                        }
                    }
        function CreateNewRowExperience(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValueExperience(val);
                AddNewRowExperience();                          
            }
        }
         function CreateNewRowFamily(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValueFamily(val);
                AddNewRowFamily();                          
            }
        }
        function CreateNewRowAddressProof(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            
            if (n == 13 || n == 9) {
                updateValueAddressProof(val);
                AddNewRowAddressProof();                          
            }
        }
        function CreateNewRowIDProof(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValueIDProof(val);
                AddNewRowIDProof();                          
            }
        }
        function CreateSelect(n,id,value) {   
            if(id==1)
            {
            if (value == -1)
                var select1 = "<select id='cmbQualification" + n + "' class='NormalText' name='cmbQualification" + n + "' style='width:100%' onchange='updateValue("+ n +")'  >"; 
            else
                var select1 = "<select id='cmbQualification" + n + "' class='NormalText' name='cmbQualification" + n + "' style='width:100%' disabled=true onchange='updateValue("+ n +")' >"; 
              
            var rows =document.getElementById("<%= hdnQualification.ClientID %>") .value.split("Ñ");            
            for (a = 1; a < rows.length; a++) {
             var cols = rows[a].split("ÿ");
             if(cols[0]==value)
               select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
            else
                select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
            }
            }
            else if (id==2)
            {
               if (value == -1)
                var select1 = "<select id='cmbUniversity" + n + "' class='NormalText' name='cmbUniversity" + n + "' style='width:100%' onchange='updateValue("+ n +")' >"; 
            else
                var select1 = "<select id='cmbUniversity" + n + "' class='NormalText' name='cmbUniversity" + n + "' style='width:100%' disabled=true  onchange='updateValue("+ n +")'>"; 
              
            var rows = document.getElementById("<%= hdnUniversity.ClientID %>").value.split("Ñ");
            for (a = 1; a < rows.length; a++) {
             var cols = rows[a].split("ÿ");
               if(cols[0]==value)
               select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
                else
                select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
            }
            }
            select1+="</select>"
          return select1;
        }
        function CreateSelectIDProof(n,id,value) {   
            if(id==1)
            {
            if (value == -1)
                var select1 = "<select id='cmbIDproof" + n + "' class='NormalText' name='cmbIDproof" + n + "' style='width:100%' onchange='updateValueIDProof("+ n +")'  >"; 
            else
                var select1 = "<select id='cmbIDproof" + n + "' class='NormalText' name='cmbIDproof" + n + "' style='width:100%' disabled=true onchange='updateValueIDProof("+ n +")' >"; 
              
            var rows =document.getElementById("<%= hdnIDProofVal.ClientID %>") .value.split("Ñ");            
            for (a = 1; a < rows.length; a++) {
             var cols = rows[a].split("ÿ");
             if(cols[0]==value)
               select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
            else
                select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
            }
            }
            select1+="</select>"
          return select1;
        }
        function CreateSelectFamily(n,id,value) {   
            if(id==1)
            {
            if (value == -1)
                var select1 = "<select id='cmbPType" + n + "' class='NormalText' name='cmbPType" + n + "' style='width:100%' onchange='updateValueFamily("+ n +")'  >"; 
            else
                var select1 = "<select id='cmbPType" + n + "' class='NormalText' name='cmbPType" + n + "' style='width:100%' disabled=true onchange='updateValueFamily("+ n +")' >"; 
              
            var rows =document.getElementById("<%= hdnFamilyVal.ClientID %>") .value.split("Ñ");            
            for (a = 1; a < rows.length; a++) {
             var cols = rows[a].split("ÿ");
             if(cols[0]==value)
               select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
            else
                select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
            }
            }
            select1+="</select>"
          return select1;
        }
        function CreateSelectAddressProof(n,id,value) {   
            if(id==1)
            {
            if (value == -1)
                var select1 = "<select id='cmbAddressproof" + n + "' class='NormalText' name='cmbAddressproof" + n + "' style='width:100%' onchange='updateValueAddressProof("+ n +")'  >"; 
            else
                var select1 = "<select id='cmbAddressproof" + n + "' class='NormalText' name='cmbAddressproof" + n + "' style='width:100%' disabled=true onchange='updateValueAddressProof("+ n +")' >"; 
              
            var rows =document.getElementById("<%= hdnAddressProofVal.ClientID %>") .value.split("Ñ");            
            for (a = 1; a < rows.length; a++) {
             var cols = rows[a].split("ÿ");
             if(cols[0]==value)
               select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
            else
                select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
            }
            }
            select1+="</select>"
          return select1;
        }
        function updateValue(id)
        {       
            row = document.getElementById("<%= hdnQualificationDtl.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id==n)
                   {
//                        if(document.getElementById("cmbQualification"+id).value!=-1 && document.getElementById("cmbUniversity"+id).value!=-1 && document.getElementById("txtYear"+id).value!="")
//                        {  
                           var Qualification = document.getElementById("cmbQualification"+id).options[document.getElementById("cmbQualification"+id).selectedIndex].text;
                           var University=document.getElementById("cmbUniversity"+id).options[document.getElementById("cmbUniversity"+id).selectedIndex].text;
                           var QualDescription=document.getElementById("txtQuali"+id).value;
                           NewStr+="¥"+document.getElementById("cmbQualification"+id).value+"µ"+document.getElementById("cmbUniversity"+id).value+"µ"+document.getElementById("txtYear"+id).value+"µ"+Qualification+"µ"+University+"µ"+QualDescription;
//                        }
                   }
                   else
                   {
                        NewStr+="¥"+row[n];
                   }
                }
                document.getElementById("<%= hdnQualificationDtl.ClientID %>").value=NewStr;
              
        }
          function updateValueExperience(id)
        {       
            row = document.getElementById("<%= hdnExperience.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id==n)
                   {
//                        if(document.getElementById("cmbQualification"+id).value!=-1 && document.getElementById("cmbUniversity"+id).value!=-1 && document.getElementById("txtYear"+id).value!="")
//                        {  
                           var Company = document.getElementById("txtCompany"+id).value;
                           var Address=document.getElementById("txtAddress"+id).value;
                           var Phone=document.getElementById("txtPhone"+id).value;
                           var Year=document.getElementById("txtYears"+id).value;
                           NewStr+="¥"+Company+"µ"+Address+"µ"+Phone+"µ"+Year;
//                        }
                   }
                   else
                   {
                        NewStr+="¥"+row[n];
                   }
                }
                document.getElementById("<%= hdnExperience.ClientID %>").value=NewStr;
              
        }
         function updateValueFamily(id)
        {       
            row = document.getElementById("<%= hdnFamily.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id==n)
                   {
//                        if(document.getElementById("cmbQualification"+id).value!=-1 && document.getElementById("cmbUniversity"+id).value!=-1 && document.getElementById("txtYear"+id).value!="")
//                        {  
                           var PTypeText = document.getElementById("cmbPType"+id).options[document.getElementById("cmbPType"+id).selectedIndex].text;
                           var PType = document.getElementById("cmbPType"+id).value;
                           var Name=document.getElementById("txtPName"+id).value;
                           var Age=document.getElementById("txtPDob"+id).value;
                           var Occupation=document.getElementById("txtOccupation"+id).value;
                           NewStr+="¥"+PType+"µ"+Name+"µ"+Age+"µ"+Occupation+"µ"+PTypeText;
//                        }
                   }
                   else
                   {
                        NewStr+="¥"+row[n];
                   }
                }
                document.getElementById("<%= hdnFamily.ClientID %>").value=NewStr;
              
        }

  function updateValueIDProof(id)
        {     row = document.getElementById("<%= hdnIDProof.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id==n)
                   {
//                        if(document.getElementById("cmbIDproof"+id).value!=-1 && document.getElementById("txtIDProofNo"+id).value!="")
//                        {  
                           var IDProof = document.getElementById("cmbIDproof"+id).options[document.getElementById("cmbIDproof"+id).selectedIndex].text;
                           var IDProofNo=document.getElementById("txtIDProofNo"+id).value;
                           NewStr+="¥"+document.getElementById("cmbIDproof"+id).value+"µ"+document.getElementById("txtIDProofNo"+id).value+"µ"+IDProof;
//                        }
                   }
                   else
                   {
                        NewStr+="¥"+row[n];
                   }
                }
                document.getElementById("<%= hdnIDProof.ClientID %>").value=NewStr;
            
        }

  function updateValueAddressProof(id)
        {       
            row = document.getElementById("<%= hdnAddressProof.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id==n)
                   {
//                        if(document.getElementById("cmbAddressproof"+id).value!=-1 && document.getElementById("txtAddressProof"+id).value!="")
//                        {  
                           var AddressProofValue = document.getElementById("cmbAddressproof"+id).options[document.getElementById("cmbAddressproof"+id).selectedIndex].text;
                           var AddressProofNoValue = document.getElementById("txtAddressProof"+id).value;
                           NewStr+="¥"+document.getElementById("cmbAddressproof"+id).value+"µ"+document.getElementById("txtAddressProof"+id).value+"µ"+AddressProofValue;
//                        }
                   }
                   else
                   {
                        NewStr+="¥"+row[n];
                   }
                }
                
                
                document.getElementById("<%= hdnAddressProof.ClientID %>").value=NewStr;
              
        }


         function DeleteRow(id)
        {
           
            row = document.getElementById("<%= hdnQualificationDtl.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id!=n)                  
                     NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnQualificationDtl.ClientID %>").value=NewStr;
                if(document.getElementById("<%= hdnQualificationDtl.ClientID %>").value=="")
                    document.getElementById("<%= hdnQualificationDtl.ClientID %>").value="¥-1µ-1µ";
                table_fill();
        }
         function DeleteRowExperience(id)
        {
           
            row = document.getElementById("<%= hdnExperience.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id!=n)                  
                     NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnExperience.ClientID %>").value=NewStr;
                if(document.getElementById("<%= hdnExperience.ClientID %>").value=="")
                    document.getElementById("<%= hdnExperience.ClientID %>").value="¥µµµ";
                table_fillExperience();
        }
         function DeleteRowFamily(id)
        {
           
            row = document.getElementById("<%= hdnFamily.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id!=n)                  
                     NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnFamily.ClientID %>").value=NewStr;
                if(document.getElementById("<%= hdnFamily.ClientID %>").value=="")
                    document.getElementById("<%= hdnFamily.ClientID %>").value="¥-1µµµ";
                table_fillFamily();
        }
         function DeleteRowAddressProof(id)
        {
           
            row = document.getElementById("<%= hdnAddressProof.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id!=n)                  
                     NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnAddressProof.ClientID %>").value=NewStr;
                if(document.getElementById("<%= hdnAddressProof.ClientID %>").value=="")
                    document.getElementById("<%= hdnAddressProof.ClientID %>").value="¥-1µµ";
                table_fillAddressProof();
        }
         function DeleteRowIDProof(id)
        {
           
            row = document.getElementById("<%= hdnIDProof.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id!=n)                  
                     NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnIDProof.ClientID %>").value=NewStr;
                if(document.getElementById("<%= hdnIDProof.ClientID %>").value=="")
                    document.getElementById("<%= hdnIDProof.ClientID %>").value="¥-1µµ";
                table_fillIDProof();
        }
        function CalculateTotalEarnings()
        {
            var BasicPay=document.getElementById("<%= txtBasicPay.ClientID %>").value;
            var DA=(document.getElementById("<%= txtDA.ClientID %>").value=="" || document.getElementById("<%= txtDA.ClientID %>").value=="0" ) ? 0 : document.getElementById("<%= txtDA.ClientID %>").value;
            var HRA=(document.getElementById("<%= txtHRA.ClientID %>").value=="" || document.getElementById("<%= txtHRA.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtHRA.ClientID %>").value;
            var Conveyance=(document.getElementById("<%= txtConveyance.ClientID %>").value=="" || document.getElementById("<%= txtConveyance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtConveyance.ClientID %>").value;
            var SA=(document.getElementById("<%= txtSpecialAllowance.ClientID %>").value=="" || document.getElementById("<%= txtSpecialAllowance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtSpecialAllowance.ClientID %>").value;
            var LA=(document.getElementById("<%= txtLocalAllowance.ClientID %>").value=="" || document.getElementById("<%= txtLocalAllowance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtLocalAllowance.ClientID %>").value;
            var MA=(document.getElementById("<%= txtMedicalAllowance.ClientID %>").value=="" || document.getElementById("<%= txtMedicalAllowance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtMedicalAllowance.ClientID %>").value;
            var HA=(document.getElementById("<%= txtHospitality.ClientID %>").value=="" || document.getElementById("<%= txtHospitality.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtHospitality.ClientID %>").value;
            var PA=(document.getElementById("<%= txtPerformance.ClientID %>").value=="" || document.getElementById("<%= txtPerformance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtPerformance.ClientID %>").value;
            var OA=(document.getElementById("<%= txtOtherAllowance.ClientID %>").value=="" || document.getElementById("<%= txtOtherAllowance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtOtherAllowance.ClientID %>").value;
            var FA=(document.getElementById("<%= txtFieldAllowance.ClientID %>").value=="" || document.getElementById("<%= txtFieldAllowance.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtFieldAllowance.ClientID %>").value;
            document.getElementById("<%= txtTotalEarnings.ClientID %>").value=Math.abs(BasicPay)+Math.abs(DA)+Math.abs(HRA)+Math.abs(Conveyance)+Math.abs(SA)+Math.abs(LA)+Math.abs(MA)+Math.abs(HA)+Math.abs(PA)+Math.abs(OA)+Math.abs(FA)
           
          
        }
        function CalculateTotalDeduction()
        {
            var ESI=(document.getElementById("<%= txtESI.ClientID %>").value=="" || document.getElementById("<%= txtESI.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtESI.ClientID %>").value;
            var PF=(document.getElementById("<%= txtPF.ClientID %>").value=="" || document.getElementById("<%= txtPF.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtPF.ClientID %>").value;
            var SWF=(document.getElementById("<%= txtSWF.ClientID %>").value=="" || document.getElementById("<%= txtSWF.ClientID %>").value=="0")? 0 : document.getElementById("<%= txtSWF.ClientID %>").value;
            var ESWT=(document.getElementById("<%= txtESWT.ClientID %>").value=="" || document.getElementById("<%= txtESWT.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtESWT.ClientID %>").value;
            var Charity=(document.getElementById("<%= txtCharity.ClientID %>").value=="" || document.getElementById("<%= txtCharity.ClientID %>").value=="0") ? 0 : document.getElementById("<%= txtCharity.ClientID %>").value;
            document.getElementById("<%= txtTotalDeductions.ClientID %>").value=Math.abs(ESI)+Math.abs(PF)+Math.abs(SWF)+Math.abs(ESWT)+Math.abs(Charity);
        }
        function TwoWheelerStatus(){
        if(document.getElementById("<%= chkTwoWheeler.ClientID %>").checked==true){
        document.getElementById("<%= txtDrivingNo.ClientID %>").disabled=false;
        }
        else{
         document.getElementById("<%= txtDrivingNo.ClientID %>").disabled=true;
         document.getElementById("<%= txtDrivingNo.ClientID %>").value="";
        }
        }
        function EmpCodeOnChange()
        {  
            var EmpCode=document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if(EmpCode!="")
            {
                var ToData = "3Ø" + EmpCode;          
                ToServer(ToData, 3);
            }
        }
               function FillData(Arg)
               {
                var Row=Arg.split("Ø");
                var Dtl=Row[0].split("µ");      
                ComboFill(Row[1],"<%= cmbDistrict.ClientID %>" )
                ComboFill(Row[2],"<%= cmbPost.ClientID %>" );  
                ComboFill(Row[9],"<%= cmbDesignation.ClientID %>" );   
                ComboFill(Row[10],"<%= cmbCaste.ClientID %>" );   
                ComboFill(Row[11],"<%= cmbReportingTo.ClientID %>" );  
              
                document.getElementById("<%= hdnQualificationDtl.ClientID %>").value=Row[3];
                document.getElementById("<%= hdnFamily.ClientID %>").value=Row[5];
                document.getElementById("<%= hdnExperience.ClientID %>").value=Row[6];
                document.getElementById("<%= hdnAddressProof.ClientID %>").value=Row[7];
                document.getElementById("<%= hdnIDProof.ClientID %>").value=Row[8];
                document.getElementById("ImgPhoto").setAttribute('src',Row[4]);   
                document.getElementById("<%= txtEmpName.ClientID %>").value=Dtl[1];
                 
                document.getElementById("<%= cmbBranch.ClientID %>").value=Dtl[3];
                document.getElementById("<%= cmbDepartment.ClientID %>").value=Dtl[2];
                document.getElementById("<%= cmbDesignation.ClientID %>").value=Dtl[4];
                document.getElementById("<%= cmbCadre.ClientID %>").value=Dtl[5];
                document.getElementById("<%= txtDateofJoin.ClientID %>").value=Dtl[6];
                document.getElementById("<%= txtHouseName.ClientID %>").value=Dtl[7];
                document.getElementById("<%= txtLocation.ClientID %>").value=Dtl[8];
                document.getElementById("<%= txtCity.ClientID %>").value=Dtl[9];
                document.getElementById("<%= txtMobile.ClientID %>").value=Dtl[10];
                document.getElementById("<%= cmbReportingTo.ClientID %>").value=Dtl[19];
                document.getElementById("<%= txtDOB.ClientID %>").value=Dtl[21];
                document.getElementById("<%= cmbMaritalStatus.ClientID %>").value=Dtl[20];
                MaritalStatusOnChange();
                document.getElementById("<%= txtCareOf.ClientID %>").value=Dtl[12];
                document.getElementById("<%= cmbGender.ClientID %>").value=Dtl[11];
                document.getElementById("<%= txtCareOfPhoneNo.ClientID %>").value=Dtl[22];
                document.getElementById("<%= cmbState.ClientID %>").value=Dtl[15];
                document.getElementById("<%= cmbDistrict.ClientID %>").value=Dtl[14];   
                document.getElementById("<%= cmbPost.ClientID %>").value=Dtl[13]+"~"+Dtl[17];    
                document.getElementById("<%= txtEmail.ClientID %>").value=Dtl[18];  
                document.getElementById("<%= txtLandline.ClientID %>").value=Dtl[16];  
                document.getElementById("<%= cmbEmpPost.ClientID %>").value=Dtl[63];  
                if (Dtl[64]==2)
                {
                    document.getElementById("<%= chkFieldStaff.ClientID %>").checked=true
                }
                else{
                    document.getElementById("<%= chkFieldStaff.ClientID %>").checked=false;
                }
        //        document.getElementById("<%= txtIDProof.ClientID %>").value=Dtl[24]; 
        //        document.getElementById("<%= cmbAddressProof.ClientID %>").value=Dtl[25];  
        //        document.getElementById("<%= txtAddressProof.ClientID %>").value=Dtl[26];  
                //SalaryDetails

                document.getElementById("<%= txtBasicPay.ClientID %>").value=parseFloat(Dtl[27]).toFixed(0); 
                document.getElementById("<%= txtDA.ClientID %>").value=parseFloat(Dtl[28]).toFixed(0); 
                document.getElementById("<%= txtHRA.ClientID %>").value=parseFloat(Dtl[29]).toFixed(0); 
                document.getElementById("<%= txtConveyance.ClientID %>").value=parseFloat(Dtl[30]).toFixed(0); 
                document.getElementById("<%= txtSpecialAllowance.ClientID %>").value=parseFloat(Dtl[31]).toFixed(0); 
                document.getElementById("<%= txtLocalAllowance.ClientID %>").value=parseFloat(Dtl[32]).toFixed(0); 
                document.getElementById("<%= txtMedicalAllowance.ClientID %>").value=parseFloat(Dtl[33]).toFixed(0); 
                document.getElementById("<%= txtHospitality.ClientID %>").value=parseFloat(Dtl[34]).toFixed(0); 
                document.getElementById("<%= txtPerformance.ClientID %>").value=parseFloat(Dtl[35]).toFixed(0); 
                document.getElementById("<%= txtOtherAllowance.ClientID %>").value=parseFloat(Dtl[36]).toFixed(0); 
                document.getElementById("<%= txtFieldAllowance.ClientID %>").value=parseFloat(Dtl[37]).toFixed(0); 
                  
                var TotalEarnings=parseFloat(Dtl[27])+parseFloat(Dtl[28])+parseFloat(Dtl[29])+parseFloat(Dtl[30])+parseFloat(Dtl[31])+parseFloat(Dtl[32])+parseFloat(Dtl[33])+parseFloat(Dtl[34])+parseFloat(Dtl[35])+parseFloat(Dtl[36])+parseFloat(Dtl[37])

                var TotalDeductions= parseFloat(Dtl[38])+parseFloat(Dtl[39])+parseFloat(Dtl[40])+parseFloat(Dtl[41])+parseFloat(Dtl[42]);
                document.getElementById("<%= txtESI.ClientID %>").value=parseFloat(Dtl[38]).toFixed(0); 
                document.getElementById("<%= txtPF.ClientID %>").value=parseFloat(Dtl[39]).toFixed(0); 
                document.getElementById("<%= txtSWF.ClientID %>").value=parseFloat(Dtl[40]).toFixed(0); 
                document.getElementById("<%= txtESWT.ClientID %>").value=parseFloat(Dtl[41]).toFixed(0); 
                document.getElementById("<%= txtCharity.ClientID %>").value=parseFloat(Dtl[42]).toFixed(0); 


                 document.getElementById("<%= txtTotalEarnings.ClientID %>").value=TotalEarnings;
                 document.getElementById("<%= txtTotalDeductions.ClientID %>").value=TotalDeductions;
                
                document.getElementById("<%= txtFather.ClientID %>").value=Dtl[43]; 
                document.getElementById("<%= txtMother.ClientID %>").value=Dtl[44]; 
                document.getElementById("<%= cmbCaste.ClientID %>").value=Dtl[45];
                
                document.getElementById("<%= cmbBloodGroup.ClientID %>").value=Dtl[46]; 
                document.getElementById("<%= cmbMotherTongue.ClientID %>").value=Dtl[47];
                 
                document.getElementById("<%= txtMarriageDate.ClientID %>").value=Dtl[48]; 
                document.getElementById("<%= chkTwoWheeler.ClientID %>").checked=(Dtl[49]==0)? false :true; 
                document.getElementById("<%= txtDrivingNo.ClientID %>").value=Dtl[50]; 
                document.getElementById("<%= txtCug.ClientID %>").value=Dtl[51]; 
                document.getElementById("<%= txtOfficialEmail.ClientID %>").value=Dtl[52]; 
                document.getElementById("<%= chkPanchayath.ClientID %>").value=(Dtl[53]==0)? false :true; 
                
                document.getElementById("<%= chkCommunity.ClientID %>").checked=(Dtl[54]==0)? false :true; 
                document.getElementById("<%= chkPolice.ClientID %>").checked=(Dtl[55]==0)? false :true; 
                document.getElementById("<%= chkSangam.ClientID %>").checked=(Dtl[56]==0)? false :true; 
                document.getElementById("<%= cmbSuretyType.ClientID %>").value=Dtl[57]; 
                document.getElementById("<%= txtSuretyName.ClientID %>").value=Dtl[58]; 
                document.getElementById("<%= txtSuretyAddr.ClientID %>").value=Dtl[59]; 
                document.getElementById("<%= txtSuretyPhone.ClientID %>").value=Dtl[60]; 
                document.getElementById("<%= cmbReligion.ClientID %>").value=Dtl[61]; 
                document.getElementById("<%= cmbEmpType.ClientID %>").value=Dtl[62]; 
          
                var newList ="";     
                var rrr = document.getElementById("<%= hdnLang.ClientID %>").value.split("Ñ");
                for (a = 2; a < rrr.length-1; a++) {
                    var cols = rrr[a].split("ÿ");
                    rows = Row[12].split("^");
                    var Val="";
                    for (n = 0; n <= rows.length - 1; n++) {
                        if(cols[0]==rows[n]){
                            Val="selected";
                            }
                    }
                    newList+="<option value="+ cols[0] +" "+ Val +" >"+ cols[1] +"</option>"  ;             
                    }
                 $('#chkveg2').html(''); // clear out old list
                 $('#chkveg2').multiselect('destroy');  // tell widget to clear itself
                 $('#chkveg2').html(newList); // add in the new list
                 $('#chkveg2').multiselect();
                 Fill();

               }
               function ChangePhoto()
                {    
                  var EmpCode=document.getElementById("<%= txtEmpCode.ClientID %>").value;
                  var EmpName=document.getElementById("<%= txtEmpName.ClientID %>").value;
                  if(EmpName=="")
                  {alert("Select an Employee"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false;}
                    if(EmpCode!="")
                    {   
                         window.showModalDialog("ChangePhoto.aspx?EmpCode="+btoa(EmpCode),"",'dialogHeight:200px;dialogWidth:500px; dialogLeft:300;dialogTop:300; center:yes');        
                         ToServer("2Ø"+EmpCode,2);      
                    }  
                }

    </script>
    <br />
    <table align="center" 
        style="width:80%; margin: 0px auto;">
           <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>APPOINTMENT DETAILS</strong><asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager></td>
           
        </tr>
        <tr>
            
            <td style="width:100% ;text-align:left;" colspan="4">
                
                <table align="left" style="width: 100%;" >

      
                <tr >
                    <td style="width:12%; text-align:left; " >
                        Employee Code&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:44% ;" >
                        &nbsp;&nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="NormalText"   MaxLength="6" onkeypress="return NumericCheck(event)"></asp:TextBox>
                    </td>
                    <td style="width:12% ;text-align:left;" >
                        Name&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:32% ;" class="Displays">
                        &nbsp;&nbsp;<asp:TextBox ID="txtEmpName" runat="server" Width="90%" readonly="true" 
                            class="ReadOnlyTextBox"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width:12% ;text-align:left;" >
                        Location&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:44% ;" class="Displays">
                        &nbsp;&nbsp;<asp:DropDownList 
                            ID="cmbBranch" runat="server" class="ReadOnlyTextBox" Width="50%" Enabled="false" > 
                        </asp:DropDownList>
                
                    </td>
                    <td style="width:12% ;text-align:left;" class="Displays">
                        Department&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:32% ;" class="Displays">
                        &nbsp;&nbsp;<asp:DropDownList ID="cmbDepartment" 
                            runat="server" class="ReadOnlyTextBox" Width="91%" Enabled="false" >
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width:12% ;text-align:left;">
                        Cadre&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:44% ;">
                        &nbsp;&nbsp;<asp:DropDownList ID="cmbCadre" runat="server" 
                            class="ReadOnlyTextBox" Width="50%" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td style="width:12% ;text-align:left;">
                        Designation&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:32% ;">
                        &nbsp;&nbsp;<asp:DropDownList ID="cmbDesignation" runat="server" 
                            class="ReadOnlyTextBox" Width="91%" Enabled="false">
                          <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width:12% ;text-align:left;">
                        Date Of Join&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:44% ;">
                        &nbsp;&nbsp;<asp:TextBox ID="txtDateofJoin" runat="server" class="ReadOnlyTextBox" Width="30%" Enabled="false" ></asp:TextBox> <asp:CalendarExtender ID="CE1" runat="server" TargetControlID="txtDateofJoin" Format="dd MMM yyyy" ></asp:CalendarExtender>
                        &nbsp;&nbsp;&nbsp;
                    </td>
                    <td style="width:12% ;text-align:left;">
                        Reporting To&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:32% ;">
                        &nbsp;&nbsp;<asp:DropDownList 
                            ID="cmbReportingTo" runat="server" class="ReadOnlyTextBox" Width="91%" Enabled="false" >
                            <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                  <tr>
                    <td style="width:12% ;text-align:left;">
                        Post&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:44% ;">
                        &nbsp;&nbsp;<asp:DropDownList 
                            ID="cmbEmpPost" runat="server" class="ReadOnlyTextBox" Width="50%" Enabled="false">
                            <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                        </asp:DropDownList>
                      </td>
                    <td style="width:12% ;text-align:left;">
                        Field Staff&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:32% ; text-align:left;">
                        &nbsp;&nbsp;<asp:CheckBox ID="chkFieldStaff" 
                            runat="server" Text=" "
                            TextAlign="Left"  />
                      </td>
                </tr>
                <tr>
                    <td style="width:12% ;text-align:left;">
                        Employee Status&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:44% ;">
                        &nbsp;&nbsp;<asp:DropDownList 
                            ID="cmbEmpType" runat="server" class="ReadOnlyTextBox" Width="50%" Enabled="false" >
                            <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                        </asp:DropDownList>
                      </td>
                    <td style="width:12% ;text-align:left;">
                        Surety Person&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:32% ; text-align:left;">
                        &nbsp;&nbsp;<asp:DropDownList 
                            ID="cmbSuretyType" runat="server" class="NormalText" Width="50%" >
                            <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                        </asp:DropDownList>
                      </td>
                </tr>
                <tr>
                    <td style="width:12% ;text-align:left;">
                        Surety Name&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:44% ;">
                        &nbsp;&nbsp;<asp:TextBox ID="txtSuretyName" runat="server" Width="90%" 
                            class="NormalText" ></asp:TextBox>
                      </td>
                    <td style="width:12% ;text-align:left;">
                        Surety Address&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:32% ; text-align:left;">
                        &nbsp;&nbsp;<asp:TextBox ID="txtSuretyAddr" runat="server" Width="90%" 
                            class="NormalText" ></asp:TextBox>
                      </td>
                </tr>
                <tr>
                    <td style="width:12% ;text-align:left;">
                        Surety Contact No&nbsp;&nbsp;<b style="color:red;">*</b></td>
                    <td style="width:44% ;">
                        &nbsp;&nbsp;<asp:TextBox ID="txtSuretyPhone" runat="server" Width="90%" MaxLength="11"
                            class="NormalText" onkeypress='return NumericCheck(event)' ></asp:TextBox>
                      </td>
                    <td style="width:12% ;text-align:left;">
                        &nbsp;&nbsp;&nbsp;</td>
                    <td style="width:32% ; text-align:left;">
                        &nbsp;&nbsp;
                      </td>
                </tr>

                </table></td>
        </tr>
         
        <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>PERSONAL DETAILS</strong></td>
           
        </tr>
        <tr>
            <td style="width:12% ;text-align:left;">
                &nbsp;</td>
            <td style="width:44% ;text-align:left;">
                &nbsp;</td>
            <td style="width:12% ;text-align:left;">
                &nbsp;</td>
            <td style="width:32% ;text-align:left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:100% ;text-align:left;" colspan="4">
                
                <table align="left" style="width: 80%">
                    <tr>
                        <td style="width:25%;">
                            Date Of Birth&nbsp;&nbsp;<b style="color:red;">*</b></td>
                        <td  style="width:25%;">
                            <asp:TextBox ID="txtDOB" 
                    runat="server" class="NormalText" Width="90%" ReadOnly="True" onkeypress="return false" ></asp:TextBox> 
                <asp:CalendarExtender ID="txtDOB_CalendarExtender" runat="server" 
                    TargetControlID="txtDOB" Format="dd MMM yyyy"></asp:CalendarExtender>
                        </td>
                        <td  style="width:25%;">
                            Gender&nbsp;&nbsp;<b style="color:red;">*</b></td>
                        <td  style="width:25%;">
                            <asp:DropDownList ID="cmbGender" runat="server" class="NormalText" Width="91%">
                </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:25% ;text-align:left;">
                Marital Status</td>
            <td style="width:25% ;text-align:left;">
                <asp:DropDownList ID="cmbMaritalStatus" runat="server" class="NormalText" 
                    Width="90%">
                </asp:DropDownList>
                &nbsp;</td>
            <td id="FatHus" style="width:25% ;text-align:left;">
                Guardian Name</td>
            <td style="width:25% ;text-align:left;">
                <asp:TextBox ID="txtCareOf" runat="server" 
                    Width="90%" class="NormalText" MaxLength="50" ></asp:TextBox> 
            </td>
                    </tr>
                    <tr>
                        <td style="width:25%;">
                            Guardian&#39;s Phone No</td>
                        <td  style="width:25%;">
                            <asp:TextBox ID="txtCareOfPhoneNo" runat="server" 
                    Width="90%" class="NormalText" MaxLength="11"  onkeypress='return NumericCheck(event)'></asp:TextBox> 
                        </td>
                        <td  style="width:25%;">
                House Name</td>
                        <td  style="width:25%;">
                            <asp:TextBox ID="txtHouseName" runat="server" 
                    Width="90%" class="NormalText" MaxLength="50" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:25%;">
                Location</td>
                        <td  style="width:25%;">
                            <asp:TextBox ID="txtLocation" runat="server" 
                    Width="90%" class="NormalText" MaxLength="50" ></asp:TextBox>
                        </td>
                        <td  style="width:25%;">
                City</td>
                        <td  style="width:25%;">
                            <asp:TextBox ID="txtCity" runat="server" 
                    Width="90%" class="NormalText" MaxLength="50" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:25%;">
                State</td>
                        <td  style="width:25%;">
                            <asp:DropDownList ID="cmbState" runat="server" class="NormalText" 
                    Width="91%">
                </asp:DropDownList>
                        </td>
                        <td  style="width:25%;">
                District</td>
                        <td  style="width:25%;">
                            <asp:DropDownList ID="cmbDistrict" runat="server" class="NormalText" 
                    Width="91%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:25%;">
                Post</td>
                        <td  style="width:25%;">
                            <asp:DropDownList ID="cmbPost" runat="server" class="NormalText" 
                    Width="91%">
                       <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
                        </td>
                        <td  style="width:25%;">
                Email </td>
                        <td  style="width:25%;">
                            <asp:TextBox ID="txtEmail" runat="server" 
                    Width="90%" class="NormalText" MaxLength="50" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:25%;">
                Land Line</td>
                        <td  style="width:25%;">
                            <asp:TextBox ID="txtLandline" runat="server" 
                    Width="90%" class="NormalText" MaxLength="15"  onkeypress='return NumericCheck(event)'></asp:TextBox>
                        </td>
                        <td  style="width:25%;">
                Mobile</td>
                        <td  style="width:25%;">
                            <asp:TextBox ID="txtMobile" runat="server" 
                    Width="90%" class="NormalText" MaxLength="10"  onkeypress='return NumericCheck(event)' ></asp:TextBox>
                        </td>
                    </tr>
                   
                    <tr style="display:none">
                        <td style="width:25%;">
                ID Proof </td>
                        <td  style="width:25%;">
                            <asp:DropDownList ID="cmbIDProof" runat="server" class="NormalText" 
                    Width="91%">
                       <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
                        </td>
                        <td  style="width:25%;">
                ID Proof No</td>
                        <td  style="width:25%;">
                            <asp:TextBox ID="txtIDProof" runat="server" 
                    Width="90%" class="NormalText" MaxLength="50" ></asp:TextBox> 
                        </td>
                    </tr>
                   
                    <tr style="display:none">
                        <td style="width:25%;">
                Address Proof</td>
                        <td  style="width:25%;">
                            <asp:DropDownList ID="cmbAddressProof" runat="server" class="NormalText" 
                    Width="91%">
                       <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
                        </td>
                        <td  style="width:25%;">
                Address Proof No</td>
                        <td  style="width:25%;">
                            <asp:TextBox ID="txtAddressProof" runat="server" 
                    Width="90%" class="NormalText" MaxLength="50" ></asp:TextBox> 
                        </td>
                    </tr>
                   <tr>
                        <td style="width:25% ;text-align:left;">
                            Fathers Name</td>
                        <td style="width:25% ;text-align:left;">
                           <asp:TextBox ID="txtFather" runat="server" 
                                Width="90%" class="NormalText" MaxLength="50" ></asp:TextBox></td>
                        <td style="width:25% ;text-align:left;">
                            &nbsp;Mothers Name</td>
                        <td style="width:25% ;text-align:left;">
                            <asp:TextBox ID="txtMother" runat="server" 
                                Width="90%" class="NormalText" MaxLength="50" ></asp:TextBox></td>
                    </tr> 

                    <tr>
            <td style="width:25% ;text-align:left;">
                Religion</td>
            <td style="width:25% ;text-align:left;">
                <asp:DropDownList ID="cmbReligion" runat="server" class="NormalText" 
                    Width="90%">
                       <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width:25% ;text-align:left;">
               Caste</td>
            <td style="width:25% ;text-align:left;">
                <asp:DropDownList ID="cmbCaste" runat="server" class="NormalText" 
                    Width="90%">
                       <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList></td>
        </tr> 
        <tr>
            <td style="width:25% ;text-align:left;">
                Blood Group</td>
            <td style="width:25% ;text-align:left;">
                <asp:DropDownList ID="cmbBloodGroup" runat="server" class="NormalText" 
                    Width="90%">
                       <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width:25% ;text-align:left;">
                Marriage Date</td>
            <td style="width:25% ;text-align:left;">
                <asp:TextBox ID="txtMarriageDate" runat="server" class="NormalText" Width="90%" ReadOnly="true"></asp:TextBox> 
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtMarriageDate" Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
        </tr> 
         <tr>
            <td style="width:25% ;text-align:left;">
                Two Wheeler Status</td>
            <td style="width:25% ;text-align:left;">
                <asp:CheckBox ID="chkTwoWheeler" runat="server" Text=" " 
                    TextAlign="Left" onclick="TwoWheelerStatus()"/></td>
            <td style="width:25% ;text-align:left;">
                Driving Liscence No</td>
            <td style="width:25% ;text-align:left;" >
                <asp:TextBox ID="txtDrivingNo" runat="server" class="NormalText" Width="90%" ></asp:TextBox> 
                </td>
        </tr>
         <tr>
            <td style="width:25% ;text-align:left;">
                CUG Mobile</td>
            <td style="width:25% ;text-align:left;">
                <asp:TextBox ID="txtCug" runat="server" 
                    Width="90%" class="NormalText" MaxLength="10" onkeypress='return NumericCheck(event)'  ></asp:TextBox></td>
            <td style="width:25% ;text-align:left;">
                Official e-Mail</td>
            <td style="width:25% ;text-align:left;">
                <asp:TextBox ID="txtOfficialEmail" runat="server" 
                    Width="90%" class="NormalText" MaxLength="50" ></asp:TextBox>
                </td>
        </tr>      
           <tr>
            <td style="width:25% ;text-align:left;">
                Reference Letter Details</td>
            <td style="width:25% ;text-align:left;" >
                <asp:CheckBox ID="chkPanchayath" runat="server" Text=" " 
                    TextAlign="Left" />Panchayath
                <asp:CheckBox ID="chkCommunity" runat="server" Text=" " 
                    TextAlign="Left" />Community
                <asp:CheckBox ID="chkPolice" runat="server" Text=" " 
                    TextAlign="Left" />Police station
                </td>
                <td style="width:25% ;text-align:left;">
                Mother Tongue</td>
            <td style="width:25% ;text-align:left;">
                <asp:DropDownList ID="cmbMotherTongue" runat="server" class="NormalText" 
                    Width="90%">
                       <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
                </td>
            
          
        </tr>
        <tr>
            <td style="width:25% ;text-align:left;">
                &nbsp;Languages Known</td>
            <td id="lang" style="width:25% ;text-align:left;" >
                &nbsp;<select id='chkveg2'  multiple='multiple' style='width:80%; '>
                      </select>
                </td>
               <td style="width:50% ;text-align:left;" colspan="2">
                <asp:CheckBox ID="chkSangam" runat="server" Text=" " 
                    TextAlign="Left" />
                  Whether the staff was earlier our Sangam member or not?
                </td>
            
             
        </tr> 
  </table>
                <div style="width:19%; height:124px; float:right; text-align:center;">
                <img id="ImgPhoto" alt="Photo" src="../Image/userimage.png" style="height:100px; width:100px;" title="Photo" />                   
                    <br/><a style="color:Blue; cursor:pointer;" onclick="ChangePhoto()"><u>Change Photo</u></a></div>
                </td>
            
             
        </tr>      
        
        <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>QUALIFICATION DETAILS</strong><img src="../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer;  padding-right:10px;" onclick="AddNewRow()" title="Add New" /></td>
           
        </tr> 
         <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnQualification" runat="server">
                </asp:Panel>
               </td>
        </tr>
        <tr>
            <td style="width:12%; height:20px; text-align:center;" colspan="4" class="mainhead" >
                <strong>EXPERIENCE DETAILS</strong><img src="../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer; padding-right:10px;" onclick="AddNewRowExperience()" title="Add New" /></td>
           
        </tr>
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnExperience" runat="server">
                </asp:Panel>
               </td>
        </tr>
        <tr>
            <td style="width:12%; height:20px; text-align:center;" colspan="4" class="mainhead" >
                <strong>ID PROOF DETAILS</strong><img src="../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer; padding-right:10px;" onclick="AddNewRowIDProof()" title="Add New" /></td>
           
        </tr>
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnIDProof" runat="server">
                </asp:Panel>
               </td>
        </tr>
        <tr>
            <td style="width:12%; height:20px; text-align:center;" colspan="4" class="mainhead" >
                <strong>ADDRESS PROOF DETAILS</strong><img src="../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer; padding-right:10px;" onclick="AddNewRowAddressProof()" title="Add New" /></td>
           
        </tr>
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnAddressProof" runat="server">
                </asp:Panel>
               </td>
        </tr>
         <tr>
            <td style="width:12%; height:20px; text-align:center;" colspan="4" class="mainhead" >
                <strong>FAMILY DETAILS</strong><img src="../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer; padding-right:10px;" onclick="AddNewRowFamily()" title="Add New" /></td>
           
        </tr>
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnFamily" runat="server">
                </asp:Panel>
               </td>
        </tr>
          <tr>
            <td style="width:12% ;text-align:left;">
                &nbsp;</td>
            <td style="width:44% ;">
                &nbsp;</td>
            <td style="width:12% ;text-align:left;">
                &nbsp;</td>
            <td style="width:32% ; text-align:left;">
                &nbsp;</td>
        </tr>
         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" hidden="true" >
                <strong>SALARY DETAILS</strong></td>
           
        </tr>
        <tr >
            <td style="width:12% ;text-align:left;" colspan="4" class="mainhead" hidden="true">            
                <table align="center" style="width: 100%;" hidden="true" >                                    
                    <tr>
                        <td style="width:12% ;text-align:center;" colspan="6" class="tblQal" >
                          EARNINGS </td>
                         <td style="width:12% ;text-align:center;"  colspan="2" class="tblQal">
                           DEDUCTIONS</td>
                    </tr>
                 
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                Basic Pay</td>
                       <td style="width:13% ;text-align:left;">
                             &nbsp;<asp:TextBox ID="txtBasicPay" runat="server" 
                    Width="80%" class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            DA</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtDA" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            HRA</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtHRA" runat="server" 
                    Width="80%" class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:13% ;text-align:left;">
                            ESI</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtESI" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            </td>
                    </tr>
                  
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            <span>Conveyance</span></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtConveyance" runat="server" 
                    Width="80%" class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            </td>
                        <td style="width:12% ;text-align:left;">
                            Special Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtSpecialAllowance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            Local Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtLocalAllowance" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            &nbsp;PF</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtPF" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            </td>
                    </tr>
                  
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            <span>Performance&nbsp;Allowance</span></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtPerformance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Other Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtOtherAllowance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Medical&nbsp;Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtMedicalAllowance" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Staff
                Welfare Fund</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtSWF" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                  
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            <span>Hospitality&nbsp;Allowance</span></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtHospitality" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Field Allowance</td>
                        <td style="width:13% ;text-align:left;">
                             &nbsp;<asp:TextBox ID="txtFieldAllowance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                             &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            &nbsp;</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            ESWT 1%</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtESWT" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                  
                   
                      <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;" colspan="6">
                            &nbsp;</td>                       
                        <td style="width:12% ;text-align:left;">
                            CharityFund 1%</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtCharity" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                   <tr class="tblQal">
                        <td style="width:12% ;text-align:left;" colspan="4">
                            &nbsp;</td>
                       
                        <td style="width:12% ;text-align:left;">
                           <strong>Total(+)</strong></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtTotalEarnings" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" 
                                ReadOnly="True" style="color:#CC0000; font-weight:bold;" ></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            <strong>Total(-)</strong></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtTotalDeductions" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" 
                                ReadOnly="True" style="color:#CC0000; font-weight:bold;" ></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                      </table>
               
                </td>
          
        </tr>
          <tr>
            <td style="width:12% ;text-align:left;">
                &nbsp;</td>
            <td style="width:44% ;">
                &nbsp;</td>
            <td style="width:12% ;text-align:left;">
                &nbsp;</td>
            <td style="width:32% ; text-align:left;">
                &nbsp;</td>
        </tr>
           <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <input id="btnClear" type="button" value="CLEAR" 
                    onclick="return btnClear_onclick()" 
                    style="font-family: cambria; width: 5%; cursor: pointer;" />
                <input id="btnSave" type="button" value="SAVE" 
                    onclick="return btnSave_onclick()" hidden="true"
                    style="font-family: cambria; width: 5%; cursor: pointer;  " />&nbsp;<input id="btnExit" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />
                    
                    <asp:HiddenField 
                    ID="hdnQualification" runat="server" />
                <asp:HiddenField 
                    ID="hdnFamilyVal" runat="server" />
                <asp:HiddenField 
                    ID="hdnFamily" runat="server" />
                <asp:HiddenField 
                    ID="hdnLang" runat="server" />
                <asp:HiddenField 
                    ID="hdnIDProofVal" runat="server" />
                <asp:HiddenField 
                    ID="hdnAddressProofVal" runat="server" />
                <asp:HiddenField 
                    ID="hdnAddressProof" runat="server" />
                <asp:HiddenField 
                    ID="hdnIDProof" runat="server" />
                <asp:HiddenField 
                    ID="hdnExperience" runat="server" />
                <asp:HiddenField 
                    ID="hdnQualificationDtl" runat="server" />
                <asp:HiddenField ID="hdnUniversity" runat="server" />
                  <asp:HiddenField ID="hdnWelfareAmount" runat="server" />
                 <asp:HiddenField ID="hdnESIRate" runat="server" />
                  <asp:HiddenField ID="hdnPFRate" runat="server" />
                   <asp:HiddenField ID="hdnSFWTRate" runat="server" />
                    <asp:HiddenField ID="hdnCharityRate" runat="server" />
<asp:HiddenField ID="hdnBtnEnable" runat="server" />
               </td>
        </tr>
    </table><br />


<script type="text/javascript">
    window.onload = function () {
        if (document.getElementById("<%= hdnBtnEnable.ClientID %>").value == 1) {
            document.getElementById("btnSave").disabled = false;
        }
        else {
            document.getElementById("btnSave").disabled = true;
        }
    }
</script>
</asp:Content>

