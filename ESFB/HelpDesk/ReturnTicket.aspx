﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="ReturnTicket.aspx.vb" Inherits="ReturnTicket" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">

<link href="../../Style/Style.css" rel="stylesheet" type="text/css" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
       <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    *                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
*                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
     
    </style>
    <script language="javascript" type="text/javascript">
     
     
        function FromServer(arg, context) {
            switch (context) {
                case 1:
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == "0") { window.close(); }
                    else if (Data[0] == "3") { document.getElementById("btnSave").style.display = 'none'; document.getElementById("btnResend").style.display = ''; document.getElementById("<%=lblMessage.ClientID %>").style.display = 'none'; }
                    break;
                case 2:
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == "0") { window.close(); }
                    else if (Data[0] == "3") { document.getElementById("btnSave").style.display = 'none'; document.getElementById("btnResend").style.display = ''; document.getElementById("<%=lblMessage.ClientID %>").style.display = 'none'; }
                    break;
               
            }
        }
        function btnSave_onclick() {
            document.getElementById("btnSave").style.display = 'none';
            if (document.getElementById("txtReason").value == "")
            { alert("Enter Reason"); return false; }
            if (document.getElementById("txtReason").value != "") {
                document.getElementById("<%=lblMessage.ClientID %>").style.display = ''; 
                var Reason = document.getElementById("txtReason").value;              
                var ToData = "1Ø" + document.getElementById("<%= hdnRequest.ClientID %>").value + Reason;
                ToServer(ToData, 1);
            }
        }

        function btnResend_onclick() {
            document.getElementById("btnResend").style.display = 'none';
            if (document.getElementById("txtReason").value == "")
            { alert("Enter Reason"); return false; }
            if (document.getElementById("txtReason").value != "") {
                document.getElementById("<%=lblMessage.ClientID %>").style.display = ''; 
                var Reason = document.getElementById("txtReason").value;
                var ToData = "2Ø" + document.getElementById("<%= hdnRequest.ClientID %>").value + Reason;
                ToServer(ToData, 2);
            }
        }

       
    </script>
</head>
<body>
    <form id="form1" runat="server"> 
    <div>
    
        <table align="center" class="style1">
            <tr>
                <td style="width:30%;">

<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
                </td>
                <td style="width:70%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width:30%; text-align:center;" colspan="2" class="mainhead">
                    &nbsp;</td>
             
            </tr>
            <tr>
                <td style="width:30%; text-align:right;">
                    &nbsp;Reason for Return</td>
                <td style="width:70%; text-align:left;">
            &nbsp;&nbsp;<textarea 
                        id="txtReason" cols="20" name="S1" rows="10" style="width: 90%" onkeypress='return TextAreaCheck(event)' ></textarea></td>
            </tr>
            <tr>
                <td style="width:30%;">
                    <asp:HiddenField ID="hdnRequest" runat="server" />
                <asp:HiddenField 
                    ID="hid_Value" runat="server" />
                </td>
                <td style="width:70%;">
                     &nbsp;&nbsp;<asp:Label 
                         ID="lblMessage" runat="server" Font-Italic="True" 
                        Font-Names="Cambria" ForeColor="Red" style="display:none"
                        Text="Sending Mail to User. Please Wait....."></asp:Label>
                </td>
            </tr>
                 <tr > 
            <td style="width:40%; text-align:right;" >
                &nbsp;</td>
            <td style="width:60%">
                &nbsp;</td>
     </tr>         
            <tr>
                <td style="width:30%; text-align:center;" colspan="2">
                 
                    <input id="btnSave" type="button" value="SAVE" 
                        style="font-family: Cambria; width: 9%" onclick="return btnSave_onclick()" /><input id="btnResend" 
                        type="button" value="RESEND" style="font-family: Cambria; width: 9%; display:none;" onclick="return btnResend_onclick()" /></td>
             
            </tr>
         
            </table>
    
    </div>
    </form>
</body>
</html>
</asp:Content>
