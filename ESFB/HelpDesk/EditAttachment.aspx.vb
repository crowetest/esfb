﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class EditAttachment
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim CallBackReturn As String = Nothing
    Dim RequestID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(GF.Decrypt(Request.QueryString.[Get]("RequestID")))
            Dim dt As DataTable = DB.ExecuteDataSet("SELECT PKID,ATTACHMENT,content_Type,attach_file_name FROM DMS_ESFB.dbo.HD_REQUEST_ATTACH where Request_Id=" & RequestID & " and status_id<>0").Tables(0)
            If dt IsNot Nothing Then
                Dim tb As New Table
                tb.Attributes.Add("width", "100%")
                For Each DR As DataRow In dt.Rows
                    Dim TRHead As New TableRow
                    Dim TRHead_00, TRHead_01 As New TableCell
                    RH.AddColumn(TRHead, TRHead_00, 100, 100, "l", "<a href='ShowAttachment.aspx?RequestID=" + GF.Encrypt(DR(0).ToString()) + " &RptID=2 ' style='cursor:pointer; font-size:12pt;'>" + DR(3) + "  </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick=EditAttachment(" + DR(0).ToString() + ") style='cursor:pointer; font-size:12pt;'>Remove")
                    tb.Controls.Add(TRHead)
                Next
                pnDisplay.Controls.Add(tb)
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim PKID As Integer = CInt(Data(1))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(3) As SqlParameter

                Params(0) = New SqlParameter("@PKID", SqlDbType.Int)
                Params(0).Value = PKID
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_HD_REMOVE_ATTACHMENT", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ContentType As String = ""
            Dim NoofAttachments As Integer = 0
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim Message As String = "Added Successfully"
            Try
                If hfc.Count > 0 Then
                    For i = 0 To hfc.Count - 1
                        Dim myFile As HttpPostedFile = hfc(i)
                        Dim nFileLen As Integer = myFile.ContentLength
                        Dim FileName As String = ""
                        If (nFileLen > 0) Then
                            ContentType = myFile.ContentType
                            FileName = myFile.FileName
                            AttachImg = New Byte(nFileLen - 1) {}
                            myFile.InputStream.Read(AttachImg, 0, nFileLen)
                            Dim Params(3) As SqlParameter
                            Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                            Params(0).Value = RequestID
                            Params(1) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                            Params(1).Value = AttachImg
                            Params(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                            Params(2).Value = ContentType
                            Params(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                            Params(3).Value = FileName
                            DB.ExecuteNonQuery("SP_HD_REQUEST_ATTACH", Params)
                        End If

                    Next
                End If
            Catch ex As Exception
                Message = ex.Message
                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');window.open('EditAttachment.aspx?RequestID=" + GF.Encrypt(RequestID.ToString()) + "','_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
