﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="AssignTicket.aspx.vb" Inherits="AssignTicket" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <style>
      #loadingmsg   
      {
      width:60%;
      background: #fff; 
      padding: 0px;
      position: fixed;     
      z-index: 100;
      margin-left: 20%;
      margin-bottom: -25%;   
       
      }
      #loadingover {
      background: black;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }

   </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <link href="../Style/ExportMenu.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript">return window_onload()</script>
        <script language="javascript" type="text/javascript">
       
        function window_onload() { 
            var ToData = "4Ø";                           
            ToServer(ToData, 4);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }     
        function FromServer(arg, context) {
            switch (context) {
                case 1:
                    var Data = arg.split("Ø");
                    alert(Data[1]);             
                    if (Data[0] == 0) { window.open("AssignTicket.aspx", "_self"); }
                    break; 
                case 2:
                    var Data = arg.split("Ø");
                    alert(Data[1]);             
                    if (Data[0] == 0) { 
                        if(document.getElementById("<%= ddlFilter.ClientID %>").value > 0 && document.getElementById("<%= ddlFilter.ClientID %>").value < 8)
                            window_onload();
                        else
                            window.open("AssignTicket.aspx", "_self"); 
                    }
                    break; 
                case 3:
                    var Data = arg.split("Ø");
                    alert(Data[1]);             
                    if (Data[0] == 0) { window.open("AssignTicket.aspx", "_self"); }
                    break;
                case 4:                    
                    document.getElementById("<%= hid_Items.ClientID %>").value = arg;
                    document.getElementById("<%= hid_ItemAll.ClientID %>").value = arg;
                    btnSearch_onclick();
                    break;  
                case  5:
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    window.open("AssignTicket.aspx", "_self");
                    break;
            }
        }
        function btnSave_onclick() {
            document.getElementById("<%= hid_Value.ClientID %>").value="";
            var row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");                                     
            for (n = 0; n <= row.length - 1; n++) {
                var col = row[n].split("µ");
                var i = n + 1;  
                if(document.getElementById("chkAttend"+i).checked==true &&  col[7]=="NA")
                {                         
                    document.getElementById("<%= hid_Value.ClientID %>").value+="¥"+col[0]+"µ0µ1µ";
                }                     
            }    
            if(document.getElementById("<%= hid_Value.ClientID %>").value=="")
            {
                alert("Attend atleast one Request"); return false;
            }
            var ToData = "2Ø" + document.getElementById("<%= hid_Value.ClientID %>").value;                           
            ToServer(ToData, 2);                
        }
        function AutoRefresh( t ) {
	        setTimeout("location.reload(true);", t);
        }
        function table_Fill()
        {
            var tab="";
            var row_bg = 0;
            tab += "<div style='width:100%;  height:auto; margin: 0px auto; overflow-y:scroll' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px;>";
            tab += "<td style='width:3%;text-align:center' ></td>";     
            tab += "<td style='width:3%;text-align:center' >#</td>";                
            tab += "<td style='width:10%;text-align:center' >Ticket No</td>";
            tab += "<td style='width:9%;text-align:center' >Branch</td>";
            tab += "<td style='width:6%;text-align:center' >Assign Dt</td>";
            tab += "<td style='width:8%;text-align:center' >Group</td>";
            tab += "<td style='width:8%;text-align:center' >Sub Group</td>";
            tab += "<td style='width:8%;text-align:center' >Findings</td>";
            tab += "<td style='width:15%;text-align:center' >Remarks</td>";  
            tab += "<td style='width:6%;text-align:center'>Corrrection Count</td>"; 
            tab += "<td style='width:4%;text-align:right'>Re-open Count</td>";                        
            tab += "<td style='width:3%;text-align:right'>Return</td>";
            tab += "<td style='width:3%;text-align:right'>Attend</td>";
            tab += "<td style='width:6%;text-align:center'>Atten. By</td>";  
            tab += "<td style='width:3%;text-align:left'>Update</td>";                  
            tab += "<td style='width:5%;text-align:center' ></td>";                
            tab += "</tr>";
            tab += "</table></div>";
            if(document.getElementById("<%= hid_Items.ClientID %>").value=="")
            {
                document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none'; 
                return;
            }         
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';  
            tab += "<div id='ScrollDiv' style='width:100%; height:274px;overflow: scroll;margin: 0px auto;' class=mainhead >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='left'>";

            var row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");                               
            for (n = 0; n <= row.length - 1; n++) 
            {
                var col = row[n].split("µ");                    
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr class='sub_first'; style='text-align:center;padding-left:20px;'>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class='sub_second'; style='text-align:center; padding-left:20px;'>";
                }     
                var i = n + 1; 
                if (col[8]==3) { 
                    tab += "<td style='width:3%;text-align:center'><img src='../image/Pause.png' title='On Hold' style='height:17px; width:17px;' /></td>";  
                }
                else {
                    if (col[9]==1){
                        tab += "<td style='width:3%;text-align:center'><img src='../image/Emergency.png' title='High Priority' style='height:20px; width:20px;' /></td>";  }
                    else
                        tab += "<td style='width:3%;text-align:center;'></td>"; 
                }                  
                tab += "<td style='width:3%;text-align:center;'>" + i + "</td>";
                tab += "<td style='width:10%;text-align:left'><a href='Reports/viewTicketDetails.aspx?RequestID="+ btoa(col[0]) +"' style='text-align:right;' target='_blank' >"  + col[1] + "</td>";
                tab += "<td style='width:9%;text-align:left'>" + col[10] + "</td>";
                tab += "<td style='width:6%;text-align:left'>" + col[11] + "</td>";
                tab += "<td style='width:8%;text-align:left'>" + col[4] + "</td>";
                tab += "<td style='width:8%;text-align:left '>" + col[5] + "</td>";
                tab += "<td style='width:8%;text-align:left'>" + col[3] + "</td>";
                tab += "<td style='width:15%;text-align:left'>" + col[2] + "</td>"; 
                tab += "<td style='width:6%;text-align:center'>" + col[15] + "</td>";

                tab += "<td style='width:4%;text-align:center'>" + col[16] + "</td>";
                if( col[7]!="NA")    
                {                 
                    if( col[8]==14 && col[12]==document.getElementById("<%= hid_User.ClientID %>").value)         
                    {
                        tab += "<td style='width:3%;text-align:center'><img src='../image/return2.png' title='Return to Branch' style='height:30px; width:30px; cursor:pointer;' disabled=true; onclick='RejectOnClick("+ col[0] +",2)' /></td>";              
                        tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkAttend" + i + "'  checked=true; onclick='DeAttend("+ col[0] + ","+ i +")' /></td>";
                        tab += "<td style='width:6%;text-align:left'>"+ col[7] +"</td>";
                    }
                    else
                    {
                        tab += "<td style='width:3%;text-align:center'><img src='../image/return2.png' title='Return to Branch' style='height:30px; width:30px; cursor:pointer;' disabled=true; onclick='RejectOnClick("+ col[0] +",2)' /></td>";              
                        tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkAttend" + i + "' disabled=true; checked=true; /></td>";
                        tab += "<td style='width:6%;text-align:left'>"+ col[7] +"</td>";
                    }
                }
                else
                {
                    tab += "<td style='width:3%;text-align:center'><img src='../image/return2.png' title='Return to Branch' style='height:30px; width:30px; cursor:pointer;' onclick='RejectOnClick("+ col[0] +",1)' /></td>";              
                    tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkAttend" + i + "'/></td>";
                    tab += "<td style='width:6%;text-align:left'></td>";
                }                                        
                if( col[7]!="NA")
                    tab += "<td style='width:3%;text-align:center'><img src='../image/update2.png' title='Update' style='height:22px; width:22px; cursor:pointer;' onclick='UpdateOnClick("+ col[0] +",1)' /></td>";
                else
                    tab += "<td style='width:3%;text-align:center'></td>";
                if(col[6]==0)
                    tab += "<td style='width:5%;text-align:center'></td>";                        
                else
                {  
                    tab+="<td  style='width:5%;text-align:center; '><img id='ViewReport' src='../Image/attchment2.png' onclick='viewReport("+ col[0] +","+ col[6]+")' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' >";			                       
                    tab+="</td>";   
                }
                var txtBox = "<input id='txtGroup" + col[0] + "' name='txtGroup" + col[0] + "' type='Text' style='width:99%;display:none;' value=" + col[13] + "></input>";
                tab += "<td style='width:0%;text-align:left;display:none;' >" + txtBox + "</td>";
                var txtBox1 = "<input id='txtSubGroup" + col[0] + "' name='txtSubGroup" + col[0] + "' type='Text' style='width:99%;display:none;' value=" + col[14] + "></input>";
                tab += "<td style='width:0%;text-align:left;display:none;' >" + txtBox1 + "</td>";
                tab += "</tr>";                
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;  
        }
        function DeAttend(ID,i)
        {
            if(confirm("Are you sure to De-attend this ticket?")==true)
            {
                var REASON=prompt("Enter Reason");
                if(REASON!=null)
                {
                    if(REASON!="")
                    {   
                        var ToData = "3Ø" +ID+"Ø"+REASON;
                        ToServer(ToData, 3); 
                    }
                    else{
                        alert("Please try again with a valid reason");document.getElementById("chkAttend"+i).checked=true;
                    }                      
                }
                else
                {
                    document.getElementById("chkAttend"+i).checked=true;
                }     
            }
            else{
                document.getElementById("chkAttend"+i).checked=true;
            }
        }
        function viewReport(ID,AttachCount)
        {
            if(AttachCount==1)
                window.open("Reports/ShowAttachment.aspx?RequestID=" + btoa(ID));
            else
                window.open("Reports/ShowMultipleAttachments.aspx?RequestID=" + btoa(ID),"_blank");
            return false;
        }
        function UpdateOnClick(RequestID,k)
        {
            if(k==1)       
            {            
                var FilterID=document.getElementById("<%= ddlFilter.ClientID %>").value;  
                var FilterText=document.getElementById("<%= txtFilter.ClientID %>").value; 
                var GroupID = document.getElementById("txtGroup" + RequestID).value;
                var SubGroupID = document.getElementById("txtSubGroup" + RequestID).value;            
                window.open("UpdateTicket.aspx?RequestID="+ btoa(RequestID) +"&FilterID="+ btoa(FilterID) +"&FilterText=" + btoa(FilterText) + "&GroupID="+ btoa(GroupID) + "&SubGroupID=" + btoa(SubGroupID) + "","_self");             
            }            
            else {
                alert("Only attended requests can be updated ! ");}        
        }
        function RejectOnClick(ID,Fl)
        {   
            document.getElementById("<%= hdnRequest.ClientID %>").value = ID;
            showLoading("RequestID="+btoa(ID));
           // window.showModalDialog("ReturnTicket.aspx?RequestID="+btoa(ID),"",'dialogHeight:400px;dialogWidth:800px; dialogLeft:300;dialogTop:300; center:yes');           
        }
        function btnRefresh_onclick() {
            window.open("AssignTicket.aspx", "_self");
        }
        function btnSearch_onclick() {     
            var row = document.getElementById("<%= hid_ItemAll.ClientID %>").value.split("¥"); 
            document.getElementById("<%= hid_Items.ClientID %>").value="";      
            var SearchText=document.getElementById("<%= txtFilter.ClientID %>").value;
            var rootString ="";
            var SearchTypeID=document.getElementById("<%= ddlFilter.ClientID %>").value;                      
            for (n = 0; n <= row.length - 1; n++) 
            {
                var col = row[n].split("µ");   
                switch (SearchTypeID) 
                {
                    case "1":                  
                        rootString=col[1].toUpperCase();
                        break; 
                    case "2":
                        rootString=col[10].toUpperCase();
                        break; 
                    case "3":
                        rootString=col[11].toUpperCase();
                        break; 
                    case "4":
                        rootString=col[4].toUpperCase();
                        break; 
                    case "5":
                        rootString=col[5].toUpperCase();
                        break; 
                    case "6":
                        rootString=col[3].toUpperCase();
                        break; 
                    case "7":
                        rootString=col[7].toUpperCase();
                        break; 
                    case "8":
                        rootString=col[7].toUpperCase();
                        break; 
                }     
                if(SearchTypeID!="8")       
                {      
                    if(rootString.indexOf(SearchText.toUpperCase()) > -1)
                    {
                        document.getElementById("<%= hid_Items.ClientID %>").value+="¥"+ row[n];              
                    }
                }
                else
                {
                    if(col[7]=="NA")
                        document.getElementById("<%= hid_Items.ClientID %>").value+="¥"+ row[n];
                }  
            } 
            document.getElementById("<%= hid_Items.ClientID %>").value=document.getElementById("<%= hid_Items.ClientID %>").value.substring(1); 
            table_Fill();
        }
        function FilterOnChange()
        {   
            var FilterID=document.getElementById("<%= ddlFilter.ClientID %>").value;    
            if(FilterID>0 && FilterID<8)
            {
                document.getElementById("<%= txtFilter.ClientID %>").disabled=false;
                document.getElementById("<%= txtFilter.ClientID %>").focus();
            }
            else
            {
                document.getElementById("<%= txtFilter.ClientID %>").disabled=true ;
                document.getElementById("<%= txtFilter.ClientID %>").value="";
            }
        }

        function showLoading(Msg) {
            document.getElementById('loadingmsg').style.display = 'block';
            document.getElementById('loadingover').style.display = 'block';        
        }
        function closeLoading() {
            document.getElementById('loadingmsg').style.display = 'none';
            document.getElementById('loadingover').style.display = 'none';         
        }
        function btnReturnOnClick()
        {
         if (document.getElementById("txtReason").value == "")
            { alert("Enter Reason"); return false; }
            if (document.getElementById("txtReason").value != "") {
                var Reason = document.getElementById("txtReason").value;              
                var ToData = "5Ø" + document.getElementById("<%= hdnRequest.ClientID %>").value + "µ0µ1µ" + Reason;
                ToServer(ToData, 5);
            }
        }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    </html>
    <div class="ScrollClass">
        <b>Help Desk Contact Numbers -</b> 08589975581, 08589975582, 08589975583, 08589975584<hr
            style="color: #E31E24; margin-top: 0px;" />
    </div>
    <br />
    <br />
    <div class="mainhead" style="width: 90%; text-align: center; margin: 0px auto; height: 35px;">
        <table style="width: 100%; margin: 0px auto;">
            <tr >
                <td style="width: 15%; text-align: Left;" colspan="4">
                  <div id='loadingmsg' style="text-align:center; display: none; ">
                    <table style="width:80%;text-align:center;margin:0px auto;" >
                     <tr style="height:150px;">
                        <td style="width:10%;text-align:left;">Reason</td><td style="width:90%;text-align:left;"><textarea  id="txtReason" maxlength="500" style="width:100%;" onkeypress='return TextAreaCheck(event)' ></textarea></td>
                    </tr>
                    <tr >
                        
                        <td colspan="2" style="text-align:center;"><input type="button" id="btnReturn" onclick="return btnReturnOnClick()"  value="RETURN" class="NormalText" style="width:15%;"/>&nbsp;&nbsp;<input type="button"  class="NormalText" id="btnClose" onclick="return closeLoading()" style="width:15%;" value="EXIT"/></td>
                    </tr>
                     <tr >
                        
                        <td colspan="2" style="text-align:center;"></td>
                    </tr>
                    </table>
                  </div>
                <div id='loadingover' style='display: none;'></div></td>
               
            </tr>
            <tr id="Tr1">
                <td style="width: 15%; text-align: Left;">
                    Filter&nbsp;By&nbsp;
                </td>
                <td style="width: 35%; text-align: left;">
                    <asp:DropDownList ID="ddlFilter" runat="server" Width="90%" class="NormalText">
                        <asp:ListItem Value="-1">ALL</asp:ListItem>
                        <asp:ListItem Value="1">TICKET NO</asp:ListItem>
                        <asp:ListItem Value="2">BRANCH</asp:ListItem>
                        <asp:ListItem Value="3">ASSIGN DATE</asp:ListItem>
                        <asp:ListItem Value="4">GROUP</asp:ListItem>
                        <asp:ListItem Value="5">SUB GROUP</asp:ListItem>
                        <asp:ListItem Value="6">FINDINGS</asp:ListItem>
                        <asp:ListItem Value="7">ATTENDED BY</asp:ListItem>
                        <asp:ListItem Value="8">UNATTENDED</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 15%; text-align: Left;">
                    Search Value
                </td>
                <td style="width: 35%; text-align: left;">
                    <asp:TextBox ID="txtFilter" runat="server" class="NormalText" Style="width: 60%;"></asp:TextBox>
                    &nbsp;&nbsp;<input id="btnSearch" style="font-family: cambria; cursor: pointer; width: 26%;
                        height: 25px;" type="button" value="SEARCH" onclick="return btnSearch_onclick()" />
                </td>
            </tr>
           
        </table>
    </div>
    <table class="style1" style="width: 90%; margin: 0px auto;">
        <tr>
            <td style="width: 15%; text-align: Left;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 15%; text-align: Left;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 15%;" colspan="4">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="4">
                <br />

                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="SAVE" onclick="return btnSave_onclick()" />&nbsp;<input id="btnRefresh" style="font-family: cambria;
                        cursor: pointer; width: 6%;" type="button" value="REFRESH" onclick="return btnRefresh_onclick()" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;<asp:HiddenField ID="hid_Items"
                        runat="server" />
                <asp:HiddenField ID="hid_Value" runat="server" />
                <asp:HiddenField ID="hid_EmpDetails" runat="server" />
                <asp:HiddenField ID="hid_ItemAll" runat="server" />
                <br />
                <asp:HiddenField ID="hid_User" runat="server" />
                <asp:HiddenField ID="hdnRequest" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="4">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
