﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class ReturnTicket
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim RequestID As Integer
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim SM As New SMTP
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(GF.Decrypt(Request.QueryString.Get("RequestID")))
            Me.hdnRequest.Value = "¥" + RequestID.ToString() + "µ0µ1µ"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim AssignDtl As String = Data(1)
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            Dim Arr() As String = AssignDtl.Substring(1).Split(CChar("µ"))
            Try
                Dim Params(3) As SqlParameter

                Params(0) = New SqlParameter("@AssignDtl", SqlDbType.VarChar, 5000)
                Params(0).Value = AssignDtl.Substring(1)
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_HD_RETURN_REQUEST", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
                System.Threading.Thread.Sleep(3000)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
          
        ElseIf CInt(Data(0)) = 2 Then
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim AssignDtl As String = Data(1)
            Dim Arr() As String = AssignDtl.Split(CChar("µ"))
            Try

                Dim QRY As String = ""
                QRY = "select a.TICKET_NO ,b.TEAM_NAME,isnull(a.EMAIL_ID,'') from  HD_REQUEST_MASTER  a,HD_TEAM_MASTER b where a.TEAM_ID =b.TEAM_ID  and a.request_id=" & Arr(0).ToString() & ""
                DT = DB.ExecuteDataSet(QRY).Tables(0)
                Dim Employee() = Split(CStr(DT.Rows(0).Item(0)), "|")
                Dim ToAddress As String = "hena.ek@esafmicrofin.com"
                Dim EmpName As String = ""
                Dim tt As String = DT.Rows(0)(2).ToString()
                If tt <> "" Then
                    ' ToAddress = tt
                    ToAddress = ToAddress
                End If
                Dim ccAddress As String = ""
                If ccAddress.Trim() <> "" Then
                    ccAddress += ","
                End If

                Dim Content As String = vbLf & vbLf & " Dear Sir/Madam, " & vbLf & vbLf

                Content += "Thanks for contacting ESAF IT Helpdesk." & vbLf & vbLf

                Content += "Your Ticket No " + DT.Rows(0)(0).ToString() + " has Returned by the " + DT.Rows(0)(1).ToString() + " team.The request will automatically close from the Helpdesk Tool if you are not responding within 3 days." & vbLf
                Content += "Request you to confirm/update the same in the Helpdesk tool immediately. You shall access the details of the Ticket in the following link." & vbLf & vbLf
                Content += "http://eweb.emfil.org -> Login -> Helpdesk Management -> Transaction -> Ticket Closure " & vbLf & vbLf

                Content = Content & Convert.ToString(vbLf & vbLf & " Thanks & Regards," & vbLf & vbLf & "IT Helpdesk" & vbLf & "Email: helpdesk@esafmicrofin.com" & vbLf & "Phone: 08589975581,82,83,84")
                Content += "For speedy resolution, please log a call in our online helpdesk tool http://eweb.emfil.org in future for any IT related issues" & vbLf & vbLf
                SM.SendMail(ToAddress, "Help Desk Ticket Return ", Content, ccAddress)
                CallBackReturn = ErrorFlag + "ØSend Mail To User"
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 3
               CallBackReturn = ErrorFlag.ToString + "ØAn error occured while sending the mail. Please click on Resend button if needed"
            End Try
        End If
    End Sub

End Class
