﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ReviewRootCause
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    '  Dim AST As New Asset
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 265) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Review Root Cause Analysis"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            DT = DB.ExecuteDataSet("Select '-1~~~~~~~' as ID,'-----Select-----' as Problem union all select problem_NO+'~'+problem+'~'+b.CATEGORY +'~'+c.PRIORITY_name " & _
                                   " +'~'+a.IMPACT +'~'+a.ROOT_CAUSE +'~'+a.WORK_AROUND +'~'+a.SOLUTION,a.problem_no   from HD_PROBLEM_MASTER a,HD_PROBLEM_CATEGORY b,HD_CHANGE_PRIORITY c " & _
                                   " where a.CATEGORY_ID=b.CATEGORY_ID and a.PRIORITY_ID=c.PRIORITY_ID and a.STATUS_ID=3").Tables(0)
            GN.ComboFill(cmbProblem, DT, 0, 1)

            Me.cmbProblem.Attributes.Add("onchange", "return ProblemOnChange()")
            Me.hlDetails.Attributes.Add("onclick", "DetailsOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then

            Dim ErrorFlag As Integer = 0
            Dim Message As String = ""

            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@ProblemNo", SqlDbType.VarChar, 50)
                Params(0).Value = CStr(Data(1))
                Params(1) = New SqlParameter("@Status", SqlDbType.Int)
                Params(1).Value = CInt(Data(2))
                Params(2) = New SqlParameter("@Description", SqlDbType.VarChar)
                Params(2).Value = CStr(Data(3))
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = CInt(Session("UserID"))
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_HD_PROBLEM_REVIEW", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If
    End Sub
#End Region


End Class
