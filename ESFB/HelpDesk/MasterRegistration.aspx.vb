﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class MasterRegistration
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 101) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Me.Master.subtitle = "Master Registration"

            If Not IsPostBack Then
                hid_tab.Value = CStr(1)

                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "CategoryFill();", True)
            End If
            Me.cmbApp.Attributes.Add("onchange", "ApprovalOnChange()")
            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            Me.cmbModule.Attributes.Add("onchange", "return ModuleOnChange()")
            hid_Edit.Value = CStr(0)
            hid_Id.Value = CStr(0)
            Me.btnSave.Attributes.Add("onclick", "return Saveonclick()")
            Me.btnView.Attributes.Add("onclick", "return ViewAttachment()")
            DT = DB.ExecuteDataSet("SELECT -1 AS ID,'-----Select-----' AS TYPE UNION ALL SELECT ID,ROOT_CAUSE FROM HD_PROBLEM_CAUSE WHERE STATUS_ID = 1").Tables(0)
            GF.ComboFill(cmbProbType, DT, 0, 1)
            DT = DB.ExecuteDataSet("SELECT -1 AS SEV_ID,'-----Select-----' AS SEVERITY UNION ALL SELECT SEV_ID,SEVERITY FROM HD_SEVERITY_MASTER WHERE STATUS_ID = 1").Tables(0)
            GF.ComboFill(cmbSeverity, DT, 0, 1)
            Me.cmbSeverity.Attributes.Add("onchange", "return SeverityOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim HDID As Integer = CInt(Data(0))
        Dim StatusID As Integer = 1
        Dim TabID As Integer = CInt(Data(1))

        If HDID = 2 Then 'fill category in panel
            Dim StrGrpnam As String = HD.GetGroupName()
            If StrGrpnam <> "" Then
                CallBackReturn = "1~" + CStr(StrGrpnam)
            Else
                CallBackReturn = "2~"
            End If
        ElseIf HDID = 3 Then 'fill category in combo
            DT = HD.GetCategory()
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next

            If TabID = 3 Then
                CallBackReturn += "~"
                DT = HD.GetDirectTeam()
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            End If

        ElseIf HDID = 4 Then 'fill sub category in panel
            If TabID <> -1 Then
                Dim StrModulenam As String = HD.GetModuleName(CInt(Data(2)))

                If StrModulenam = "" Then
                    CallBackReturn = "2~"

                Else
                    CallBackReturn = "1~" + CStr(StrModulenam)
                End If
            Else
                CallBackReturn = "2~"
            End If
        ElseIf HDID = 5 Then 'fill sub category in combo
            DT = HD.GetModule(CInt(Data(2)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf HDID = 6 Then 'fill findings in panel
            If TabID <> -1 Then
                Dim StrProblemnam As String = HD.GetProblemName(CInt(Data(2)))
                If StrProblemnam = "" Then
                    CallBackReturn = "2~"

                Else
                    CallBackReturn = "1~" + CStr(StrProblemnam)
                End If
            Else
                CallBackReturn = "2~"
            End If
        ElseIf HDID = 7 Then 'delete 
            If TabID = 1 Then 'category

                DT = HD.GetModule(CInt(Data(2)))
                If DT.Rows.Count = 1 Then
                    Dim RetVal As Integer = HD.DeleteGroup(TabID, CInt(Data(2)))
                    If RetVal = 1 Then
                        CallBackReturn = "1~Deleted Successfully~"
                    Else
                        CallBackReturn = "2~Error Occured"
                    End If
                Else
                    CallBackReturn = "2~Can't delete , Sub category registered in this category ."
                End If


            ElseIf TabID = 2 Then 'Sub category

                DT = HD.GetProblems(CInt(Data(2)))
                If DT.Rows.Count = 0 Then
                    Dim RetVal As Integer = HD.DeleteGroup(TabID, CInt(Data(2)))
                    If RetVal = 1 Then
                        CallBackReturn = "1~Deleted Successfully~"
                    Else
                        CallBackReturn = "2~Error Occured"
                    End If
                Else
                    CallBackReturn = "2~Can't delete , Findings registered in this sub category ."
                End If


            ElseIf TabID = 3 Then 'Finding

                DT = HD.GetRequestexits(CInt(Data(2)))
                If DT.Rows.Count = 0 Then
                    Dim RetVal As Integer = HD.DeleteGroup(TabID, CInt(Data(2)))
                    If RetVal = 1 Then
                        CallBackReturn = "1~Deleted Successfully"
                    Else
                        CallBackReturn = "2~Error Occured"
                    End If
                Else
                    CallBackReturn = "2~Can't delete ,Requests received under this finding ."
                End If
            End If
        ElseIf HDID = 8 Then 'edit, approval assign 'shima 17/10
            Dim StrEdit As String = ""
            StrEdit = HD.EditMasterData(TabID, CInt(Data(2)))
            If StrEdit <> "" Then
                CallBackReturn = "1~" + StrEdit
            Else
                CallBackReturn = "2~"
            End If
        ElseIf HDID = 9 Then
            Dim severity As String = Data(1)
            DT = DB.ExecuteDataSet("SELECT RESPONSE_TAT_TYPE, RESPONSE_TAT, RESOLUTION_TAT_TYPE, RESOLUTION_TAT FROM HD_SEVERITY_MASTER WHERE SEV_ID = " + severity + " AND STATUS_ID = 1").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += DR(0).ToString() + "Ø" + DR(1).ToString() + "Ø" + DR(2).ToString() + "Ø" + DR(3).ToString()
            Next
        End If

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim Data() As String = hid_Save.Value.Split(CChar("Ø"))
            Dim HDID As Integer = CInt(Data(0))
            Dim StatusID As Integer = 1
            Dim TabID As Integer = CInt(Data(1))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim Id As Integer = 0
            Try
                If TabID = 1 Then
                    Dim CateName As String = CStr(Data(2))
                    Id = CInt(Data(3))
                    Dim Params(4) As SqlParameter
                    Params(0) = New SqlParameter("@StatusID", SqlDbType.Int)
                    Params(0).Value = StatusID
                    Params(1) = New SqlParameter("@Category", SqlDbType.VarChar, 100)
                    Params(1).Value = CateName
                    Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(2).Direction = ParameterDirection.Output
                    Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@ID", SqlDbType.Int)
                    Params(4).Value = Id
                    DB.ExecuteNonQuery("SP_HD_GROUP_REG", Params)
                    ErrorFlag = CInt(Params(2).Value)
                    Message = CStr(Params(3).Value)
                ElseIf TabID = 2 Then
                    Dim SubGrpName As String = CStr(Data(3))
                    Dim IntGrpID As Integer = 0
                    IntGrpID = CInt(Data(2))
                    Id = CInt(Data(4))
                    Dim Params(5) As SqlParameter
                    Params(0) = New SqlParameter("@StatusID", SqlDbType.Int)
                    Params(0).Value = StatusID
                    Params(1) = New SqlParameter("@Module", SqlDbType.VarChar, 500)
                    Params(1).Value = SubGrpName
                    Params(2) = New SqlParameter("@GrpID", SqlDbType.Int)
                    Params(2).Value = IntGrpID
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@ID", SqlDbType.Int)
                    Params(5).Value = Id
                    DB.ExecuteNonQuery("SP_HD_SUBGROUP_REG", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                ElseIf TabID = 3 Then
                    Dim IntSubGrpID As Integer = 0
                    IntSubGrpID = CInt(Data(2))
                    Dim SubProblemName As String = CStr(Data(3))
                    Id = CInt(Data(4))
                    Dim App As String = CStr(Data(6))
                    Dim Appmode As Integer = 0
                    Appmode = CInt(Data(5))
                    Dim Description As String = CStr(Data(7))
                    Dim Critical As Integer = CInt(Data(8))
                    Dim DirectTeam As Integer = CInt(Data(9))
                    Dim RootCause As Integer = CInt(Data(10))
                    Dim Severity As Integer = CInt(Data(11))
                    Dim RespTatType As Integer = CInt(Data(12))
                    Dim RespTat As Integer = CInt(Data(13))
                    Dim ResolTatType As Integer = CInt(Data(14))
                    Dim ResolTat As Integer = CInt(Data(15))

                    Dim AttachImg As Byte() = Nothing
                    Dim ContentType As String = ""
                    AttachImg = Nothing
                    Dim FileName As String = ""
                    Dim myFile As HttpPostedFile = fupAnnexture.PostedFile
                    Dim nFileLen As Integer = myFile.ContentLength
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                        FileName = myFile.FileName
                    End If

                    Dim Params(19) As SqlParameter
                    Params(0) = New SqlParameter("@StatusID", SqlDbType.Int)
                    Params(0).Value = StatusID
                    Params(1) = New SqlParameter("@Problem", SqlDbType.VarChar, 500)
                    Params(1).Value = SubProblemName
                    Params(2) = New SqlParameter("@SubGrpID", SqlDbType.Int)
                    Params(2).Value = IntSubGrpID
                    Params(3) = New SqlParameter("@ID", SqlDbType.Int)
                    Params(3).Value = Id
                    Params(4) = New SqlParameter("@APP", SqlDbType.VarChar, 25)
                    Params(4).Value = App.Substring(1)
                    Params(5) = New SqlParameter("@APPMODE", SqlDbType.Int)
                    Params(5).Value = Appmode
                    Params(6) = New SqlParameter("@Descrpt", SqlDbType.VarChar, 500)
                    Params(6).Value = Description
                    Params(7) = New SqlParameter("@IsCritical", SqlDbType.Int)
                    Params(7).Value = Critical
                    Params(8) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
                    Params(8).Value = AttachImg
                    Params(9) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                    Params(9).Value = ContentType
                    Params(10) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                    Params(10).Value = FileName
                    Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(11).Direction = ParameterDirection.Output
                    Params(12) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(12).Direction = ParameterDirection.Output
                    Params(13) = New SqlParameter("@DirectTeam", SqlDbType.Int)
                    Params(13).Value = DirectTeam
                    Params(14) = New SqlParameter("@RootCause", SqlDbType.Int)
                    Params(14).Value = RootCause
                    Params(15) = New SqlParameter("@Severity", SqlDbType.Int)
                    Params(15).Value = Severity
                    Params(16) = New SqlParameter("@RespTatType", SqlDbType.Int)
                    Params(16).Value = RespTatType
                    Params(17) = New SqlParameter("@RespTat", SqlDbType.Int)
                    Params(17).Value = RespTat
                    Params(18) = New SqlParameter("@ResolTatType", SqlDbType.Int)
                    Params(18).Value = ResolTatType
                    Params(19) = New SqlParameter("@ResolTat", SqlDbType.Int)
                    Params(19).Value = ResolTat
                    DB.ExecuteNonQuery("SP_HD_PROBLEM", Params)
                    ErrorFlag = CInt(Params(11).Value)
                    Message = CStr(Params(12).Value)
                End If
                If ErrorFlag = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "IntialCtrl();", True)
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
