﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ChangeRequest.aspx.vb" Inherits="HelpDesk_ChangeRequest" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head>
<title></title>
<link href="../Style/Style.css" rel="stylesheet" type="text/css" />
<script src="../Script/Validations.js" type="text/javascript"></script>
  <link href="../Style/jquery-ui.css" rel="stylesheet" type="text/css" />
  <script src="../Script/jquery-1.10.2.js" type="text/javascript"></script>
  <script src="../Script/jquery-ui.js" type="text/javascript"></script>
  
  
<script language="javascript" type="text/javascript">

      $(function () {
       
          var availableTags = document.getElementById("<%= hdnTicketData.ClientID %>").value.split("~");
          $("#txtHdTicketNo").autocomplete({
              source: availableTags
          });
      });
  
    function DetailsOnClick()
    {
        var HdTicketNo = document.getElementById("txtHdTicketNo").value;
        if(HdTicketNo=="")
        {
            alert("Enter Ticket");
            document.getElementById("txtHdTicketNo").focus();
            return false;
        }
        else{
        var Data       = "2ʘ" +  document.getElementById("txtHdTicketNo").value;
            ToServer(Data, 2);
        }
       
    }

    function btnSave_onclick() 
    {
        var HdTicketNo = document.getElementById("txtHdTicketNo").value;
        var Change     = document.getElementById("<%= txtChange.ClientID %>").value;
        var Reason     = document.getElementById("<%= txtReason.ClientID %>").value;
        var CategoryID = document.getElementById("<%= cmbCategory.ClientID %>").value;
        var Data       = "1ʘ" + HdTicketNo + "ʘ" + Change + "ʘ" + Reason + "ʘ" + CategoryID;
        ToServer(Data, 1);
    }
    function FromServer(Arg, Context) 
    {
        switch(Context)
        {
            case 1: // Confirmation
            {
                var Data = Arg.split("ʘ");
                alert(Data[1]);
                if ( Data[0] == 0 )  
                {
                    alert("Your Ticket Number Is : " + Data[2]);
                    window.open("../Home.aspx","_self");
                }
                break;
            }
            case 2:
            {
                 if ( Arg == 0 )  
                {
                    alert("Invalid Ticket Number");
                    document.getElementById("txtHdTicketNo").value="";
                    document.getElementById("txtHdTicketNo").focus();
                    
                }
                else
                 window.open("Reports/viewTicketDetails.aspx?RequestID=" + btoa(Arg) + "");
                break;
            }
        }
    }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
    
</script>
</head>
<br />
    <br />
    <table align="center" style="width: 80%; margin: 0px auto; ">
        <tr>
            <td style="width: 27%; text-align:right;">Enter Helpdesk Ticket No</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;cursor:pointer;">
                    <div class="ui-widget">
                        <input id="txtHdTicketNo" class="NormalText"  style=" font-family:Cambria;font-size:10pt;resize:none;" width="30%" maxlength="20"/>
                                        &nbsp;&nbsp;<asp:HyperLink ID="hlDetails" runat="server" ForeColor="#0000CC" style="text-decoration: underline" ToolTip="View Details">View Details</asp:HyperLink>
                                        </div>
            </td>
        </tr>
        <tr>
            <td style="width: 27%; text-align:right;">Change Description<asp:HiddenField 
                    ID="hdnTicketData" runat="server" />
            </td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;"><asp:TextBox ID="txtChange" runat="server" class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" Width="100%" MaxLength="1000" Rows="8" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr>
            <td style="width: 27%; text-align:right;">Category</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                    <asp:DropDownList ID="cmbCategory" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="40%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
        </tr>
        <tr>
            <td style="width: 27%; text-align:right;">Reason For Change</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;"><asp:TextBox ID="txtReason" runat="server" class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" Width="100%" MaxLength="1000" Rows="8" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td style="text-align:center;"colspan="3" class="style2">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="SAVE" onclick="return btnSave_onclick()" />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" />
            </td>            
         </tr>
    </table>
    <br />
</asp:Content>


