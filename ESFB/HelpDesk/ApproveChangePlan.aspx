﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true"  CodeFile="ApproveChangePlan.aspx.vb" Inherits="ApproveChangePlan" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
         .fileUpload{
    width:255px;    
    font-size:11px;
    color:#000000;
    border:solid;
    border-width:1px;
    border-color:#7f9db9;    
    height:17px;
    }
            .ScrollClass
        {           
            text-align:center; color:#E31E24;
            background-color:#EBDDDD;
            height:20px;
           
        }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript"> 
        
        function ChangeOnChange(){
            var ChangeNo = document.getElementById("<%= cmbChange.ClientID %>").value;
            if(ChangeNo == "-1")
            {
                alert("Select Change Request");
                document.getElementById("<%= cmbChange.ClientID %>").focus();
                return false;
            }
            else
                ToServer ("1Ø" + ChangeNo , 1);
       }

       function DetailsOnClick()
        {
            var ChangeNo = document.getElementById("<%= cmbChange.ClientID %>").value;
            if(ChangeNo == "-1")
            {
                alert("Select Ticket");
                document.getElementById("<%= cmbChange.ClientID %>").focus();
                return false;
            }
            window.open("Reports/ChangeTicketInfo.aspx?ChangeNo=" + btoa(ChangeNo) + "");
        }

       function btnApprove_onclick() {
            var ChangeNo = document.getElementById("<%= cmbChange.ClientID %>").value;
            var Remarks  = document.getElementById("<%= txtRemarks.ClientID %>").value;
            var StatusID = 1;
            if(ChangeNo == "-1")
            {
                alert("Select Change Request");
                document.getElementById("<%= cmbChange.ClientID %>").focus();
                return false;
            }
            else
                ToServer ("2Ø" + ChangeNo + "Ø" + Remarks + "Ø" + StatusID , 2); 
       }

       function btnReject_onclick() {
            var ChangeNo = document.getElementById("<%= cmbChange.ClientID %>").value;
            var Remarks  = document.getElementById("<%= txtRemarks.ClientID %>").value;
            var StatusID = 2;
            if(ChangeNo == "-1")
            {
                alert("Select Change Request");
                document.getElementById("<%= cmbChange.ClientID %>").focus();
                return false;
            }
            if(Remarks == "")
            {
                alert("Enter Remarks for Rejection");
                document.getElementById("<%= txtRemarks.ClientID %>").focus();
                return false;
            }
            else
                ToServer ("2Ø" + ChangeNo + "Ø" + Remarks + "Ø" + StatusID , 2); 
       }

       function FromServer (arg,context) 
       {
             switch (context) 
             {
                 case 1:
                        {
                            var Data = arg.split("Ø");
                            document.getElementById("<%= txtDescription.ClientID %>").value = Data[0];
                            document.getElementById("<%= txtReason.ClientID %>").value = Data[1];
                            break;
                        }
                 case 2:
                        {
                            var Data = arg.split("Ø");
                            alert(Data[1]);             
                            if (Data[0] == 0) { window.open("../home.aspx", "_self"); }
                            break;
                        }
                 
            }
       }

       
       

       
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }





    </script>
   
</head>
</html>

<br /><br />

 <table class="style1" style="width:70%;margin: 0px auto;" >
           
        <tr> <td style="width:20%;"></td>
        <td style="width:15%; text-align:left;">Select Change Ticket</td>
            <td style="width:65%;cursor:pointer;">
                &nbsp;<asp:DropDownList ID="cmbChange" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList> &nbsp; &nbsp;
                <asp:HyperLink ID="hlDetails" runat="server" ForeColor="#0000CC" 
                        style="text-decoration: underline" ToolTip="View Details">View Details</asp:HyperLink>
                </td>
            
       </tr>
        <tr> <td style="width:20%;">&nbsp;</td>
        <td style="width:15%; text-align:left;">Change Description</td>
            <td style="width:65%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDescription" runat="server"  class="ReadOnlyTextBox"
                    TextMode="MultiLine" Width="100%" ReadOnly="True" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
                </td>
       </tr>
       <tr> <td style="width:20%;">&nbsp;</td>
        <td style="width:15%; text-align:left;">Reason for Change</td>
            <td style="width:65%">
                &nbsp; &nbsp;<asp:TextBox ID="txtReason" runat="server" class="ReadOnlyTextBox"
                    TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Width="100%" ReadOnly="True"></asp:TextBox>
                </td>
       </tr>
       
       <tr> <td style="width:20%;">&nbsp;</td>
        <td style="width:15%; text-align:left;">Remarks</td>
            <td style="width:65%">
                <asp:TextBox ID="txtRemarks" runat="server" class="NormalText" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Width="100%"></asp:TextBox>
                </td>
       </tr>
       
       <tr>
            <td style="text-align:center;" colspan="3">
                 
            <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="APPROVE"   onclick="return btnApprove_onclick()" />&nbsp;
                 
            <input id="btnReject" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="REJECT"   onclick="return btnReject_onclick()" />&nbsp; <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
        </tr>

       <tr id = "rowSub" style = "display:none;">
            <td style="text-align:center;" colspan="3">
                &nbsp;</td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

