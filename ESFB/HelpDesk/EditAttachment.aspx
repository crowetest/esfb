﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="EditAttachment.aspx.vb" Inherits="EditAttachment" %>
<%@ MasterType VirtualPath="~/REPORT.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript" >
        function EditAttachment(ID) {
            if (confirm("Are you sure to remove this attachment?") == 1) {
                var ToData = "1Ø" + ID;
                ToServer(ToData, 1);
                
            }
        }
        function FromServer(arg, context) {
            switch (context) {
                case 1:
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == 0) { window.location.reload(false); }
                    break;             
            }
        }
        function AddMoreImages() {
            if (!document.getElementById && !document.createElement)
                return false;
            var fileUploadarea = document.getElementById("fileUploadarea");
            if (!fileUploadarea)
                return false;
            var newLine = document.createElement("br");
            fileUploadarea.appendChild(newLine);
            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("class", "fileUpload");

            if (!AddMoreImages.lastAssignedId)
                AddMoreImages.lastAssignedId = 100;
            newFile.setAttribute("id", "FileUpload" + AddMoreImages.lastAssignedId);
            newFile.setAttribute("name", "FileUpload" + AddMoreImages.lastAssignedId);
            var div = document.createElement("div");
            div.appendChild(newFile);
            div.setAttribute("id", "div" + AddMoreImages.lastAssignedId);
            fileUploadarea.appendChild(div);
            AddMoreImages.lastAssignedId++;
        }



function btnExit_onclick() {
    window.close();
}

    </script>
    <style type="text/css">
        .style1
        {
            width: 50%;
        }
         .fileUpload{
    width:255px;    
    font-size:11px;
    color:#000000;
    border:solid;
    border-width:1px;
    border-color:#7f9db9;    
    height:17px;
    }
        *                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
*                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
            <asp:Panel ID="pnDisplay" runat="server" Width="100%">
            </asp:Panel>
    
    </div>
    <div>
        <table align="left" class="style1">
            <tr>
                <td style="width:40%; text-align:left;">
                    &nbsp;</td>
                <td style="width:60%; text-align:left;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width:40%; text-align:left;">
                    New Attachment If any</td>
                <td style="width:60%; text-align:left;">
                 </td>
            </tr>
            <tr>
                <td style="width:40%; text-align:left;">
                    &nbsp;</td>
                <td style="width:60%; text-align:left;">
                   <div id="fileUploadarea"><asp:FileUpload ID="FileUpload1" runat="server" CssClass="fileUpload" /><br /></div><br />
    <div><input style="display:block;" id="btnAddMoreFiles" type="button" value="Add more images" onclick="AddMoreImages();" /><br />        
        </div></td>
            </tr>
            <tr>
                <td style="text-align:center;" colspan="2">
            <asp:Button 
                ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" style="cursor:pointer;"
                 Width="8%" />
                 
            &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 8%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
</asp:Content>
