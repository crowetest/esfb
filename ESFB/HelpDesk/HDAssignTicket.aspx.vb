﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AssignTicket
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim HD As New HelpDesk
    Dim TeamID As Integer
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 104) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Assign Ticcket To Team"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            DT = HD.GetTeam(CInt(Session("UserID")))
            If DT.Rows.Count > 0 Then
                TeamID = CInt(DT.Rows(0)(0))
            Else
                TeamID = 0
            End If
            Dim FilterID As Integer = -1
            Dim FilterText As String = ""
            If Not IsNothing(Request.QueryString.Get("FilterID")) Then
                FilterID = CInt(Request.QueryString.Get("FilterID"))
            End If
            If Not IsNothing(Request.QueryString.Get("FilterText")) Then
                FilterText = Request.QueryString.Get("FilterText")
            End If
            'DT = DB.ExecuteDataSet("select * from (select top 10000 a.REQUEST_ID,a.TICKET_NO,a.REMARKS,c.PROBLEM,d.group_Name,e.Sub_Group_Name,NOOF_ATTACHMENTS,a.status_id,dbo.udfProperCase(br.Branch_Name)as Branch_Name,p.department_name,convert(varchar(19),a.REQUEST_DT)as Req_Dt,ISNULL(case when x.TICKET_TYPE=1 then 'New' else case when x.TICKET_TYPE=2 then 'Repair' else '' end end,'')AssetStatus,d.Group_ID,e.Sub_Group_ID,ISNULL((select e.Emp_Name from (select r.REQUEST_ID,r.USER_ID,r.Update_ID,MAX(r.ORDER_ID) ORDER_ID from HD_REQUEST_CYCLE r where r.Update_ID in(17,18) group by r.REQUEST_ID,r.USER_ID,r.Update_ID) am, EMP_MASTER e where am.USER_ID=e.Emp_Code and am.REQUEST_ID=a.request_id and am.Update_ID=a.STATUS_ID),'') UserName from HD_REQUEST_MASTER a left outer join HD_ASSET_TICKET x on a.REQUEST_ID=x.REQUEST_ID ,HD_TEAM_MASTER b,HD_PROBLEM_TYPE c,HD_GROUP_MASTER d,HD_SUB_GROUP e,BRANCH_MASTER br,DEPARTMENT_MASTER  p where a.TEAM_ID=b.TEAM_ID and a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and e.group_ID=d.group_ID and a.DEPARTMENT_ID=p.Department_ID and a.BRANCH_ID=br.Branch_ID and a.STATUS_ID in(1,17,18) and a.Team_ID=4 order by URGENT_REQ,REQUEST_DT)mm").Tables(0)
            DT = DB.ExecuteDataSet("select * from (select top 10000 a.REQUEST_ID,a.TICKET_NO,a.REMARKS,c.PROBLEM,d.group_Name,e.Sub_Group_Name,NOOF_ATTACHMENTS,a.status_id,dbo.udfProperCase(br.Branch_Name)as Branch_Name,p.department_name,convert(varchar(19),a.REQUEST_DT)as Req_Dt,ISNULL(case when x.TICKET_TYPE=1 then 'New' else case when x.TICKET_TYPE=2 then 'Repair' else '' end end,'')AssetStatus,d.Group_ID,e.Sub_Group_ID,ISNULL((select e.Emp_Name from (select h.USER_ID from (select request_id,MAX(ORDER_ID) as ORDER_ID from HD_REQUEST_CYCLE group by request_id)am ,HD_REQUEST_CYCLE h where am.request_id = h.request_id and am.REQUEST_ID=a.request_id and am.ORDER_ID = h.order_id) aa, EMP_MASTER e where aa.USER_ID=e.Emp_Code ) ,'') UserName from HD_REQUEST_MASTER a left outer join HD_ASSET_TICKET x on a.REQUEST_ID=x.REQUEST_ID ,HD_TEAM_MASTER b,HD_PROBLEM_TYPE c,HD_GROUP_MASTER d,HD_SUB_GROUP e,BRANCH_MASTER br,DEPARTMENT_MASTER  p where a.TEAM_ID=b.TEAM_ID and a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and e.group_ID=d.group_ID and a.DEPARTMENT_ID=p.Department_ID and a.BRANCH_ID=br.Branch_ID and a.STATUS_ID in(1,17,18) and a.Team_ID=4 order by URGENT_REQ,REQUEST_DT)mm").Tables(0)
            'ToDo//
            hid_Items.Value = ""
            hid_EmpDetails.Value = ""
            Dim i As Integer = 0
            For Each DR As DataRow In DT.Rows
                '                  RequestID 0               TicketNo1                 Branch 2                  Department 3              Request Date 4              Group 5                   Sub Group 6                Problem 7           Remarks 8           Asset Status 9              NoOfAttachments 10      GroupID  11               SubGroupID  12            StatusID  13             User   14
                hid_Items.Value += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(8).ToString() + "µ" + DR(9).ToString() + "µ" + DR(10).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(3).ToString() + "µ" + DR(2).ToString() + "µ" + DR(11).ToString() + "µ" + DR(6).ToString() + "µ" + DR(12).ToString() + "µ" + DR(13).ToString() + "µ" + DR(7).ToString() + "µ" + DR(14).ToString()
                If i <> DT.Rows.Count - 1 Then
                    hid_Items.Value += "¥"
                End If
                i = i + 1
            Next

            Me.ddlFilter.SelectedValue = CStr(FilterID)
            Me.txtFilter.Text = FilterText

            hid_ItemAll.Value = hid_Items.Value
            Me.hid_User.Value = Session("UserID").ToString()
            Me.ddlFilter.Attributes.Add("onchange", "FilterOnChange()")
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim AssignDtl As String = Data(1)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim Arr() As String = AssignDtl.Substring(1).Split(CChar("µ"))
            Try
                Dim Params(3) As SqlParameter

                Params(0) = New SqlParameter("@AssignDtl", SqlDbType.VarChar, 5000)
                Params(0).Value = AssignDtl.Substring(1)
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_HD_RETURN_REQUEST", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            Dim AssignDtl As String = Data(1)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(3) As SqlParameter

                Params(0) = New SqlParameter("@AssignDtl", SqlDbType.VarChar, 5000)
                Params(0).Value = AssignDtl.Substring(1)
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_HD_ASSIGN_REQUEST", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 3 Then
            Dim RequestID As Integer = CInt(Data(1))
            Dim Reason As String = Data(2).ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(4) As SqlParameter

                Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                Params(0).Value = RequestID
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@Reason", SqlDbType.VarChar, 5000)
                Params(2).Value = Reason
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_HD_DEATTEND_TICKETS", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If

    End Sub
#End Region

End Class
