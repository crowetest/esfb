﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Resolving
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim HD As New HelpDesk
    Dim CallBackReturn As String = Nothing
    Dim ReportID, RequestID, GroupID, SubGroupID As Integer
    Dim GF As New GeneralFunctions
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Assign Tickets To Team"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Me.cmd_view.Attributes.Add("onclick", "return viewattachment()")
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            DT = HD.GetTeam(CInt(Session("UserID")))
            If DT.Rows.Count > 0 Then
                hid_TeamID.Value = CStr(DT.Rows(0)(0))
            Else
                hid_TeamID.Value = CStr(0)
            End If
            ReportID = CInt(Request.QueryString.Get("RptID"))
            GroupID = CInt(GF.Decrypt(Request.QueryString.Get("GroupID")))
            SubGroupID = CInt(GF.Decrypt(Request.QueryString.Get("SubGroupID")))
            hid_ReportID.Value = ReportID.ToString()

            If Not IsPostBack Then
                GN.ComboFill(cmbItem, HD.GetSubGroup(GroupID), 0, 1)
                GN.ComboFill(cmbProblem, HD.GetPROBLEMToAssign(SubGroupID), 0, 1)
            End If

            If ReportID = 1 Then
                GN.ComboFill(cmbStatus, HD.GetStatus(2), 0, 1)
                GN.ComboFill(cmbRequest, HD.GetRequestDetails(CInt(Session("UserID"))), 0, 1)
                GN.ComboFill(cmbHover, HD.GetTeamResolving(1), 0, 1)
                lblStatus.Text = "Status"
            Else
                RequestID = CInt(GF.Decrypt(Request.QueryString.Get("RequestID")))
                GN.ComboFill(cmbRequest, HD.GetHDRequestDtl(CInt(hid_TeamID.Value), CInt(RequestID)), 0, 1)
                GN.ComboFill(cmbStatus, HD.GetStatus(3), 0, 1)
                GN.ComboFill(cmbHover, HD.GetTeam(), 0, 1)
                lblStatus.Text = "Action"
            End If

            GN.ComboFill(cmbCategory, HD.GetGroup(), 0, 1)
            GN.ComboFill(cmbClass, HD.GetClassification(), 0, 1)
            GN.ComboFill(cmbVendor, HD.GetVendor(CInt(hid_TeamID.Value)), 0, 1)
            Me.cmbRequest.Attributes.Add("onchange", "return RequestOnchange()")
            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            Me.cmbItem.Attributes.Add("onchange", "return ItemOnChange()")
            Me.cmbStatus.Attributes.Add("onchange", "return StatusOnChange()")
            Me.cmbHover.Attributes.Add("onchange", "return HandOverOnChange()")
            Me.cmbAssetStatus.Attributes.Add("onchange", "return AssetStatusOnChange()")
            Me.cmbProblem.Attributes.Add("onchange", "ProblemOnchange()")
            Me.txtAssetTAG.Attributes.Add("onchange", "return AssetTagOnChange()")
            Me.btnView.Attributes.Add("onclick", "return ViewAttachment()")
            Me.btnSave.Attributes.Add("onclick", "return btnSave_onclick()")
            Me.btnReturn.Attributes.Add("onclick", "return btnReturn_onclick()")
            Me.btnProgress.Attributes.Add("onclick", "return btnProgress_onclick()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            cmbRequest.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim EmpID As Integer = CInt(Data(1))
        ' "1Ø" + Branch + "Ø" + Dep + "Ø" + Category + "Ø" + Module + "Ø" + Problem + "Ø" + Class+ "Ø" +Status+ "Ø" +Hover+ "Ø" +Remarks+ "Ø" +document.getElementById("<%= hid_TeamID.ClientID %>").value;
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 2 Then
            If CInt(Data(1)) > 0 Then
                DT = HD.GetSubGroup(CInt(Data(1)))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            End If
        ElseIf CInt(Data(0)) = 3 Then
            If CInt(Data(1)) > 0 Then
                DT = HD.GetPROBLEMToAssign(CInt(Data(1)))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            End If
        ElseIf CInt(Data(0)) = 4 Then
            DT = HD.GetPreviousRemarks(CInt(Data(1)))
            Dim StrVal As String
            StrVal = HD.GetString(DT)
            CallBackReturn = StrVal.ToString

        ElseIf CInt(Data(0)) = 6 Then
            Dim AssignDtl As String = Data(1)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(3) As SqlParameter

                Params(0) = New SqlParameter("@AssignDtl", SqlDbType.VarChar, 5000)
                Params(0).Value = AssignDtl.Substring(1)
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_HD_CANCEL_REQUEST", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 7 Then
            Dim AssetTag As String = CStr(Data(1))
            Dim BranchID As Integer = CInt(Data(2))
            Dim Dept As Integer = CInt(Data(3))
            Dim ItemDtl() As String = Data(4).Split(CChar("~"))
            Dim ItemID As Integer = CInt(ItemDtl(0))
            DT = DB.ExecuteDataSet("Select COUNT(ASSET_ID) from FA_MASTER where STATUS_ID =1  AND LOCATION_ID=" & BranchID & " and DEPARTMENT_ID=" & Dept & " and ITEM_ID in (select item_id from hd_problem_type where problem_id =  " & ItemID & ") and ASSET_TAG='" & AssetTag & "'").Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn = CStr(DT.Rows(0)(0))
            Else
                CallBackReturn = "0"
            End If
        ElseIf CInt(Data(0)) = 8 Then
            Dim RequestID As Integer = CInt(Data(1))
            Dim Remark As String = CStr(Data(2))
            Dim Status As Integer = CInt(Data(3))

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(5) As SqlParameter

                Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                Params(0).Value = RequestID
                Params(1) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                Params(1).Value = Remark
                Params(2) = New SqlParameter("@Status", SqlDbType.Int)
                Params(2).Value = Status
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = UserID
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_HD_HOLD_PROGRESS_REQUEST", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If

    End Sub
#End Region

#Region "Save"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim Data() As String = Me.hdnSave.Value.Split(CChar("Ø"))
            Dim Branch As Integer = CInt(Data(1))
            Dim Dep As Integer = CInt(Data(2))
            Dim Category As Integer = CInt(Data(3))
            Dim Modules As Integer = CInt(Data(4))
            Dim Problem As Integer = CInt(Data(5))
            Dim Classi As Integer = CInt(Data(6))
            Dim Status As Integer = CInt(Data(7))
            Dim Hover As Integer = CInt(Data(8))
            Dim Remarks As String = CStr(Data(9))
            Dim User_Team_ID As Integer = CInt(Data(10))
            Dim REQUEST_ID As Integer = CInt(Data(11))
            Dim Vendor_ID As Integer = CInt(Data(12))
            Dim Emergency As Integer = CInt(Data(13))
            Dim ASSET_STATUS As Integer = CInt(Data(14))
            Dim ASSET_TAG As String = CStr(Data(15))
            Dim ASSET_COUNT As Integer = CInt(Data(16))

            Dim UserID As Integer = CInt(Session("UserID"))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim NoofAttachments As Integer = 0
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        NoofAttachments += 1
                    End If
                Next
            End If
            Try

                Dim Params(15) As SqlParameter

                Params(0) = New SqlParameter("@CLASSIFICATION_ID", SqlDbType.Int)
                Params(0).Value = Classi
                Params(1) = New SqlParameter("@REMARKS", SqlDbType.VarChar, 1000)
                Params(1).Value = Remarks
                Params(2) = New SqlParameter("@PROBLEM_ID", SqlDbType.Int)
                Params(2).Value = Problem
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = UserID
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@status", SqlDbType.Int)
                Params(6).Value = Status
                Params(7) = New SqlParameter("@TEAM_ID", SqlDbType.Int)
                Params(7).Value = Hover
                Params(8) = New SqlParameter("@USER_TEAM_ID", SqlDbType.Int)
                Params(8).Value = User_Team_ID
                Params(9) = New SqlParameter("@REQUEST_ID", SqlDbType.Int)
                Params(9).Value = REQUEST_ID
                Params(10) = New SqlParameter("@VENDORID", SqlDbType.Int)
                Params(10).Value = Vendor_ID
                Params(11) = New SqlParameter("@URGENT", SqlDbType.Int)
                Params(11).Value = Emergency
                Params(12) = New SqlParameter("@ASSET_STATUS", SqlDbType.Int)
                Params(12).Value = ASSET_STATUS
                Params(13) = New SqlParameter("@ASSET_TAG", SqlDbType.VarChar, 25)
                Params(13).Value = ASSET_TAG
                Params(14) = New SqlParameter("@ASSET_COUNT", SqlDbType.Int)
                Params(14).Value = ASSET_COUNT
                Params(15) = New SqlParameter("@BRANCH", SqlDbType.Int)
                Params(15).Value = Branch
                DB.ExecuteNonQuery("SP_HD_REQUEST_RESOLVING", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            If REQUEST_ID > 0 And hfc.Count > 0 And ErrorFlag = 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                        Dim Params(4) As SqlParameter
                        Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                        Params(0).Value = RequestID
                        Params(1) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                        Params(1).Value = AttachImg
                        Params(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                        Params(2).Value = ContentType
                        Params(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                        Params(3).Value = FileName
                        Params(4) = New SqlParameter("@AssetStatus", SqlDbType.VarChar, 500)
                        Params(4).Value = 0
                        DB.ExecuteNonQuery("SP_HD_REQUEST_ATTACH", Params)
                    End If
                Next
            End If
            Dim AttachementNo As Integer = 0
            DT = DB.ExecuteDataSet("select COUNT(PkId) from DMS_ESFB.dbo.HD_REQUEST_ATTACH where Request_ID=" & REQUEST_ID & "").Tables(0)
            If CInt(DT.Rows(0)(0)) >= 0 Then
                AttachementNo = CInt(DT.Rows(0)(0))
                Dim Err As Integer = DB.ExecuteNonQuery("update HD_REQUEST_MASTER set NOOF_ATTACHMENTS=" & AttachementNo & " where REQUEST_ID=" & REQUEST_ID & "")
            End If

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("         if (window.opener != null && !window.opener.closed);{window.opener.location.reload();window.close();}window.onbeforeunload = RefreshParent;")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Return"
    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Try
            Dim Data() As String = Me.hdnReturn.Value.Split(CChar("Ø")) '5Ø¥322256µ0µ1µpls contact regional HRØ156~0~0~~0
            Dim AssignDtl As String = Data(1)
            Dim RequestID As Integer = CInt(Data(2))
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim NoofAttachments As Integer = 0
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        NoofAttachments += 1
                    End If
                Next
            End If
            Try
                Dim Params(4) As SqlParameter

                Params(0) = New SqlParameter("@AssignDtl", SqlDbType.VarChar, 5000)
                Params(0).Value = AssignDtl.Substring(1)
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@HDFlag", SqlDbType.Int)
                Params(4).Value = 1
                DB.ExecuteNonQuery("SP_HD_RETURN_REQUEST", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            If RequestID > 0 And hfc.Count > 0 And ErrorFlag = 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                        Dim Params(4) As SqlParameter
                        Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                        Params(0).Value = RequestID
                        Params(1) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                        Params(1).Value = AttachImg
                        Params(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                        Params(2).Value = ContentType
                        Params(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                        Params(3).Value = FileName
                        Params(4) = New SqlParameter("@AssetStatus", SqlDbType.VarChar, 500)
                        Params(4).Value = 0
                        DB.ExecuteNonQuery("SP_HD_REQUEST_ATTACH", Params)
                    End If
                Next
            End If
            Dim AttachementNo As Integer = 0
            DT = DB.ExecuteDataSet("select COUNT(PkId) from DMS_ESFB.dbo.HD_REQUEST_ATTACH where Request_ID=" & RequestID & "").Tables(0)
            If CInt(DT.Rows(0)(0)) >= 0 Then
                AttachementNo = CInt(DT.Rows(0)(0))
                Dim Err As Integer = DB.ExecuteNonQuery("update HD_REQUEST_MASTER set NOOF_ATTACHMENTS=" & AttachementNo & " where REQUEST_ID=" & RequestID & "")
            End If

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("         if (window.opener != null && !window.opener.closed);{window.opener.location.reload();window.close();}window.onbeforeunload = RefreshParent;")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "In Progress"
    Protected Sub btnProgress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProgress.Click
        Try
            Dim Data() As String = Me.hdnProgress.Value.Split(CChar("Ø"))
            Dim RequestID As Integer = CInt(Data(1))
            Dim Remark As String = CStr(Data(2))
            Dim Status As Integer = CInt(Data(3))
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim NoofAttachments As Integer = 0
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        NoofAttachments += 1
                    End If
                Next
            End If
            Try
                Dim Params(5) As SqlParameter

                Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                Params(0).Value = RequestID
                Params(1) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                Params(1).Value = Remark
                Params(2) = New SqlParameter("@Status", SqlDbType.Int)
                Params(2).Value = Status
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = UserID
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_HD_HOLD_PROGRESS_REQUEST", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try

            If RequestID > 0 And hfc.Count > 0 And ErrorFlag = 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                        Dim Params(4) As SqlParameter
                        Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                        Params(0).Value = RequestID
                        Params(1) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                        Params(1).Value = AttachImg
                        Params(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                        Params(2).Value = ContentType
                        Params(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                        Params(3).Value = FileName
                        Params(4) = New SqlParameter("@AssetStatus", SqlDbType.VarChar, 500)
                        Params(4).Value = 0
                        DB.ExecuteNonQuery("SP_HD_REQUEST_ATTACH", Params)
                    End If
                Next
            End If
            Dim AttachementNo As Integer = 0
            DT = DB.ExecuteDataSet("select COUNT(PkId) from DMS_ESFB.dbo.HD_REQUEST_ATTACH where Request_ID=" & RequestID & "").Tables(0)
            If CInt(DT.Rows(0)(0)) >= 0 Then
                AttachementNo = CInt(DT.Rows(0)(0))
                Dim Err As Integer = DB.ExecuteNonQuery("update HD_REQUEST_MASTER set NOOF_ATTACHMENTS=" & AttachementNo & " where REQUEST_ID=" & RequestID & "")
            End If

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("         if (window.opener != null && !window.opener.closed);{window.opener.location.reload();window.close();}window.onbeforeunload = RefreshParent;")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
    
End Class
