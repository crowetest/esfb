﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ProblemRequest.aspx.vb" Inherits="HelpDesk_ProblemRequest" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head>
<title></title>
<link href="../Style/Style.css" rel="stylesheet" type="text/css" />
<script src="../Script/Validations.js" type="text/javascript"></script>
<link href="../Style/jquery-ui.css" rel="stylesheet" type="text/css" />
  <script src="../Script/jquery-1.10.2.js" type="text/javascript"></script>
  <script src="../Script/jquery-ui.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
 $(function () {
       
          var availableTags = document.getElementById("<%= hdnTicketData.ClientID %>").value.split("~");
          $("#txtHdTicketNo").autocomplete({
              source: availableTags
          });
      });
    function DetailsOnClick()
    {
        var HdTicketNo = document.getElementById("txtHdTicketNo").value;
        if(HdTicketNo=="")
        {
            alert("Enter Ticket");
            document.getElementById("txtHdTicketNo").focus();
            return false;
        }
        else
        {
            var Data       = "1ʘ" +  document.getElementById("txtHdTicketNo").value;
            ToServer(Data, 1);
        }
       
    }

    function btnSave_onclick() 
    {
        var HdTicketNo = document.getElementById("txtHdTicketNo").value;
        var Problem    = document.getElementById("<%= txtProblem.ClientID %>").value;
        var Category   = document.getElementById("<%= cmbCategory.ClientID %>").value;
        var Priority   = document.getElementById("<%= cmbPriority.ClientID %>").value;
        var Impact     = document.getElementById("<%= txtImpact.ClientID %>").value;
        var Data       = "9ʘ" + HdTicketNo + "ʘ" + Problem + "ʘ" + Category + "ʘ" + Priority + "ʘ" + Impact;
        ToServer(Data, 9);
    }
    function FromServer(Arg, Context) 
    {
        switch(Context)
        {
            case 1:
            {
                 if ( Arg == 0 )  
                {
                    alert("Invalid Ticket Number");
                    document.getElementById("txtHdTicketNo").value="";
                    document.getElementById("txtHdTicketNo").focus();
                    
                }
                else
                    window.open("Reports/viewTicketDetails.aspx?RequestID=" + btoa(Arg) + "");
                break;
            }
            case 9: // Confirmation
            {
                var Data = Arg.split("ʘ");
                alert(Data[1]);
                if ( Data[0] == 0 )  
                {
                    alert("Your Ticket Number Is : " + Data[2]);
                    window.open("../home.aspx","_self");
                }
                break;
            }
        }
    }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
</script>
</head>
<br />
    <br />
    <table align="center" style="width: 80%; margin: 0px auto; ">
        <tr>
            <td style="width: 27%; text-align:right;">Enter Helpdesk Ticket No</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;cursor:pointer;"><input id="txtHdTicketNo" class="NormalText"  style=" font-family:Cambria;font-size:10pt;resize:none;" width="30%" maxlength="20"/>
                                        &nbsp;&nbsp;
                    <asp:HyperLink ID="hlDetails" runat="server" ForeColor="#0000CC" 
                        style="text-decoration: underline" ToolTip="View Details">View Details</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td style="width: 27%; text-align:right;">Findings Description</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;"><asp:TextBox ID="txtProblem" 
                    runat="server" class="NormalText" 
                    style=" font-family:Cambria;font-size:10pt;resize:none;" Width="100%" 
                    MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr>
            <td style="width: 27%; text-align:right;">Category &amp; Priority</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                    <asp:DropDownList ID="cmbCategory" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="40%" ForeColor="Black">
                        </asp:DropDownList>
                    <asp:DropDownList ID="cmbPriority" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="40%" ForeColor="Black">
                        </asp:DropDownList>

                        </td>
        </tr>
        <tr>
            <td style="width: 27%; text-align:right;"><asp:HiddenField 
                    ID="hdnTicketData" runat="server" />
                Impact</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;"><asp:TextBox ID="txtImpact" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td style="text-align:center;"colspan="3" class="style2">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="SAVE" onclick="return btnSave_onclick()" />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" />
            </td>            
         </tr>
    </table>
    <br />
</asp:Content>


