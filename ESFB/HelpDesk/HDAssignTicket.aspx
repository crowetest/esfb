﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="HDAssignTicket.aspx.vb" Inherits="AssignTicket" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <link href="../Style/ExportMenu.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript">return window_onload()</script>
        <script language="javascript" type="text/javascript">
       
   
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
     
        function FromServer(arg, context) {
            switch (context) {
                case 1:
                    var Data = arg.split("Ø");
                    alert(Data[1]);             
                    if (Data[0] == 0) { window.open("HDAssignTicket.aspx", "_self"); }
                    break; 
                case 2:
                    var Data = arg.split("Ø");
                    alert(Data[1]);             
                    if (Data[0] == 0) { window.open("HDAssignTicket.aspx", "_self"); }
                    break; 
                case 3:
                    var Data = arg.split("Ø");
                    alert(Data[1]);             
                    if (Data[0] == 0) { window.open("HDAssignTicket.aspx", "_self"); }
                    break; 
            }
        }

        function window_onload() { 
            var FilterID =document.getElementById("<%= ddlFilter.ClientID %>").value;
            if(FilterID>0 && FilterID<8)
            {
                document.getElementById("<%= txtFilter.ClientID %>").disabled=false;
                document.getElementById("<%= txtFilter.ClientID %>").focus();
            }
            else
            {
                document.getElementById("<%= txtFilter.ClientID %>").disabled=true ;
                document.getElementById("<%= txtFilter.ClientID %>").value="";
            }     
            btnSearch_onclick();
        }
        function AutoRefresh( t ) {
	        setTimeout("location.reload(true);", t);
        }
        function table_Fill()
        {
            var tab="";
            var row_bg = 0;

            tab += "<div id='ScrollDiv' style='width:100%; height:310px;overflow: scroll;margin: 0px auto;' class=mainhead >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px;>";
                  
            tab += "<td style='width:3%;text-align:center' >#</td>";                
            tab += "<td style='width:11%;text-align:left' >Ticket No</td>";
            tab += "<td style='width:10%;text-align:left' >Branch</td>";
            tab += "<td style='width:10%;text-align:left' >Department</td>";
            tab += "<td style='width:6%;text-align:left' >Request Dt</td>";
            tab += "<td style='width:10%;text-align:left' >Group</td>";
            tab += "<td style='width:12%;text-align:left' >Sub Group</td>";
            tab += "<td style='width:10%;text-align:left' >Findings</td>";
            tab += "<td style='width:14%;text-align:left' >Remarks</td>";
            tab += "<td style='width:6%;text-align:left' >AssetStatus</td>";
            tab += "<td style='width:3%;text-align:left'>Assign</td>";                  
            tab += "<td style='width:5%;text-align:center' ></td>";                
            tab += "</tr>";
           // tab += "</table></div>";
            if(document.getElementById("<%= hid_Items.ClientID %>").value=="")
            {
                document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none'; 
                return;
            }            
         
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';  
            //tab += "<div id='ScrollDiv' style='width:100%; height:274px;overflow: scroll;margin: 0px auto;' class=mainhead >";
           // tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='left'>";     
            
            var row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");                               
            for (n = 0; n <= row.length - 1; n++) 
            {
                var col = row[n].split("µ");                    
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr class='sub_first'; style='text-align:center;padding-left:20px;'>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class='sub_second'; style='text-align:center; padding-left:20px;'>";
                }                    
                var i = n + 1; 

//                if (col[8]==3)
//                { tab += "<td style='width:3%;text-align:center'><img src='../image/Pause.png' title='On Hold' style='height:17px; width:17px;' /></td>";  }
//                else
//                {if (col[9]==1)  
//                {tab += "<td style='width:3%;text-align:center'><img src='../image/Emergency.png' title='High Priority' style='height:20px; width:20px;' /></td>";  }
//                else
//                tab += "<td style='width:3%;text-align:center;'></td>"; } 
                if(col[13]==1) {
                    tab += "<td style='width:3%;text-align:center;'>" + i + "</td>";
                }
                else {
                    var Title = "";
                    if(col[13]==17)
                    {  
                        Title = col[14] + " Hold"                      
                        tab += "<td style='width:3%;text-align:center;'>" + i + " <img id='ImgStatus' src='../Image/hold.png' title='" + Title + "'  style='height:10px; width:10px;  cursor:pointer;' ></td>";
                    }
                    else
                    {
                        Title = col[14] + " In Progress" 
                        tab += "<td style='width:3%;text-align:center;'>" + i + " <img id='ImgStatus' src='../Image/inprogress.png' title='" + Title + "'  style='height:10px; width:10px;  cursor:pointer;' ></td>";
                    }
                }
                tab += "<td style='width:11%;text-align:left'><a href='Reports/viewTicketDetails.aspx?RequestID="+btoa(col[0])+"' style='text-align:right;' target='_blank' >"  + col[1] + "</td>"; //TicketNo
                tab += "<td style='width:10%;text-align:left'>" + col[2] + "</td>"; //Branch
                tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>"; //Department
                tab += "<td style='width:6%;text-align:left'>" + col[4] + "</td>"; //Request Date
                tab += "<td style='width:10%;text-align:left '>" + col[5] + "</td>"; //Group
                tab += "<td style='width:12%;text-align:left'>" + col[6] + "</td>"; //SubGroup
                tab += "<td style='width:10%;text-align:left'>" + col[7] + "</td>"; //problem
                tab += "<td style='width:14%;text-align:left'>" + col[8] + "</td>"; //Remarks
                tab += "<td style='width:6%;text-align:left'>" + col[9] + "</td>"; //AssetStatus
                tab += "<td style='width:3%;text-align:center'><img src='../image/update2.png' title='Assign To Team' style='height:22px; width:22px; cursor:pointer;' onclick='UpdateOnClick("+ col[0] +",1)' /></td>";                                       
                if(col[10]==0)
                    tab += "<td style='width:5%;text-align:center'></td>";                        
                else
                {  
                    tab+="<td  style='width:5%;text-align:center; '><img id='ViewReport' src='../Image/attchment2.png' onclick='viewReport("+ col[0] +","+ col[10]+")' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' >";			               
                    tab+="</td>";                    
                }
                var txtBox = "<input id='txtGroup" + col[0] + "' name='txtGroup" + col[0] + "' type='Text' style='width:99%;display:none;' value=" + col[11] + "></input>";
                tab += "<td style='width:0%;text-align:left;display:none;' >" + txtBox + "</td>";
                var txtBox1 = "<input id='txtSubGroup" + col[0] + "' name='txtSubGroup" + col[0] + "' type='Text' style='width:99%;display:none;' value=" + col[12] + "></input>";
                tab += "<td style='width:0%;text-align:left;display:none;' >" + txtBox1 + "</td>";
                tab += "</tr>";                
            }
            tab += "</table></div></div></div>";            
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;  
        }
        function DeAttend(ID,i)
        {
            if(confirm("Are you sure to De-attend this ticket?")==true)
            {
                var REASON=prompt("Enter Reason");
                if(REASON!=null)
                {
                    if(REASON!="")
                    { 
                        var ToData = "3Ø" +ID+"Ø"+REASON;
                        ToServer(ToData, 3); 
                    }
                    else{
                        alert("Please try again with a valid reason");
                        document.getElementById("chkAttend"+i).checked=true;
                    }                      
                }
                else
                {
                    document.getElementById("chkAttend"+i).checked=true;
                }     
            }
            else{
                document.getElementById("chkAttend"+i).checked=true;
            }
        }
        function viewReport(ID,AttachCount)
        {
            if(AttachCount==1)
                window.open("Reports/ShowAttachment.aspx?RequestID=" + btoa(ID));
            else
                window.open("Reports/ShowMultipleAttachments.aspx?RequestID=" + btoa(ID),"_blank");
            return false;
        }
        function UpdateOnClick(RequestID,k)
        {
            if(k==1)       
            {
                var FilterID=document.getElementById("<%= ddlFilter.ClientID %>").value;  
                var FilterText=document.getElementById("<%= txtFilter.ClientID %>").value;  
                var GroupID = document.getElementById("txtGroup" + RequestID).value;
                var SubGroupID = document.getElementById("txtSubGroup" + RequestID).value; 
                window.open("Resolving.aspx?RequestID="+ btoa(RequestID) + " &RptID=2&GroupID="+ btoa(GroupID) + "&SubGroupID=" + btoa(SubGroupID) + ""); 
            }            
            else
            {
                alert("Only attended requests can be updated ! ");
            }        
        }
        function RejectOnClick(ID,Fl)
        {       
            window.showModalDialog("ReturnTicket.aspx?RequestID="+ID,"",'dialogHeight:400px;dialogWidth:800px; dialogLeft:300;dialogTop:300; center:yes'); 
            window.open("HDAssignTicket.aspx", "_self");              
        }
        function btnRefresh_onclick() {
            window.open("HDAssignTicket.aspx", "_self");
        }

        function btnSearch_onclick() {
        
            var row = document.getElementById("<%= hid_ItemAll.ClientID %>").value.split("¥"); 
            document.getElementById("<%= hid_Items.ClientID %>").value="";      
            var SearchText=document.getElementById("<%= txtFilter.ClientID %>").value;
            var rootString ="";
            var SearchTypeID=document.getElementById("<%= ddlFilter.ClientID %>").value;                      
            for (n = 0; n <= row.length - 1; n++) 
            {
                var col = row[n].split("µ");   
                switch (SearchTypeID) 
                {
//                    "1">TICKET NO
//                    "2">BRANCH
//                    "3">ASSIGN DATE
//                    "4">GROUP
//                    "5">SUB GROUP
//                    "6">PROBLEM

                    case "1":                  
                        rootString=col[1].toUpperCase(); //Ticket Number
                        break; 
                    case "2":
                        rootString=col[2].toUpperCase(); //Branch Name
                        break; 
                    case "3":
                        rootString=col[4].toUpperCase(); //Assign Date
                        break; 
                    case "4":
                        rootString=col[5].toUpperCase(); //Group
                        break; 
                    case "5":
                        rootString=col[6].toUpperCase(); //Sub Group
                        break; 
                    case "6":
                        rootString=col[7].toUpperCase(); //Problem
                        break; 
                }     
                if(SearchTypeID!="8")       
                {      
                    if(rootString.indexOf(SearchText.toUpperCase()) > -1)
                    {
                        document.getElementById("<%= hid_Items.ClientID %>").value+="¥"+ row[n];              
                    }
                }
                else
                {
                    if(col[7]=="NA")
                    document.getElementById("<%= hid_Items.ClientID %>").value+="¥"+ row[n];
                }  
            } 
            document.getElementById("<%= hid_Items.ClientID %>").value=document.getElementById("<%= hid_Items.ClientID %>").value.substring(1); 
            table_Fill();
        }

        function FilterOnChange()
        {
            var FilterID=document.getElementById("<%= ddlFilter.ClientID %>").value;    
            if(FilterID>0 && FilterID<8)
            {
                document.getElementById("<%= txtFilter.ClientID %>").disabled=false;
                document.getElementById("<%= txtFilter.ClientID %>").focus();
            }
            else
            {
                document.getElementById("<%= txtFilter.ClientID %>").disabled=true ;
                document.getElementById("<%= txtFilter.ClientID %>").value="";
            }
        }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    </html>
    <div class="ScrollClass">
        <b>Help Desk Contact Numbers -</b> 08589975581, 08589975582, 08589975583, 08589975584<hr
            style="color: #E31E24; margin-top: 0px;" />
    </div>
    <br />
    <br />
    <div class="mainhead" style="width: 90%; text-align: center; margin: 0px auto; height: 35px;">
        <table style="width: 100%; margin: 0px auto;">
            <tr id="Tr1">
                <td style="width: 15%; text-align: Left;">
                    Filter&nbsp;By&nbsp;
                </td>
                <td style="width: 35%; text-align: left;">
                    <asp:DropDownList ID="ddlFilter" runat="server" Width="90%" class="NormalText">
                        <asp:ListItem Value="-1">ALL</asp:ListItem>
                        <asp:ListItem Value="1">TICKET NO</asp:ListItem>
                        <asp:ListItem Value="2">BRANCH</asp:ListItem>
                        <asp:ListItem Value="3">ASSIGN DATE</asp:ListItem>
                        <asp:ListItem Value="4">GROUP</asp:ListItem>
                        <asp:ListItem Value="5">SUB GROUP</asp:ListItem>
                        <asp:ListItem Value="6">FINDINGS</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 15%; text-align: Left;">
                    Search Value
                </td>
                <td style="width: 35%; text-align: left;">
                    <asp:TextBox ID="txtFilter" runat="server" class="NormalText" Style="width: 60%;"></asp:TextBox>
                    &nbsp;&nbsp;<input id="btnSearch" style="font-family: cambria; cursor: pointer; width: 26%;
                        height: 25px;" type="button" value="SEARCH" onclick="return btnSearch_onclick()" />
                </td>
            </tr>
        </table>
    </div>
    <table class="style1" style="width: 90%; margin: 0px auto;">
        <tr>
            <td style="width: 15%; text-align: Left;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 15%; text-align: Left;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 15%;" colspan="4">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="4">
                <br />
                <input id="btnRefresh" style="font-family: cambria; cursor: pointer; width: 6%;"
                    type="button" value="REFRESH" onclick="return btnRefresh_onclick()" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;<asp:HiddenField ID="hid_Items"
                        runat="server" />
                <asp:HiddenField ID="hid_Value" runat="server" />
                <asp:HiddenField ID="hid_EmpDetails" runat="server" />
                <asp:HiddenField ID="hid_ItemAll" runat="server" />
                <br />
                <asp:HiddenField ID="hid_User" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="4">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <br />
</asp:Content>
