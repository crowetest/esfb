﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Change_FixCAB
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim AST As New HelpDesk
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim HD As New HelpDesk
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 258) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Fix Change Advisory Board"
            DT = DB.ExecuteDataSet("Select '-1~~' as ID,'-----Select-----' as Change union all select CHANGE_NO+'~'+change+'~'+REASON,CHANGE_NO  from hd_change_master where STATUS_ID=2").Tables(0)
            GN.ComboFill(cmbChange, DT, 0, 1)
            hdnValue.Value = "¥µµµ"

            '--//---------- Script Registrations -----------//--
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- For Call Back ---//
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)

            Me.cmbChange.Attributes.Add("onchange", "return ChangeOnChange()")
            Me.hlDetails.Attributes.Add("onclick", "DetailsOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim ChangeID As String = CStr(Data(1))
            Dim EmpDtl As String = Data(2)
            Dim ErrorFlag As Integer = 0
            Dim Message As String = ""
            Try
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@ChangeID", SqlDbType.VarChar, 20)
                Params(0).Value = ChangeID
                Params(1) = New SqlParameter("@EmpDtl", SqlDbType.VarChar)
                Params(1).Value = EmpDtl.Substring(1)
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = CInt(Session("UserID"))
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_HD_CHANGE_FIX_CAB", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            DT = DB.ExecuteDataSet("select Emp_Name,Department_Name,designation_name from Emp_List where Emp_Code=" & CInt(Data(1)) & "").Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn = CStr(Data(2)) + "~" + CStr(DT.Rows(0)(0)) + "Ø" + CStr(DT.Rows(0)(1)) + "Ø" + CStr(DT.Rows(0)(2))
            Else
                CallBackReturn = CStr(Data(2)) + "~" + "ØØ"
            End If
        End If
    End Sub
#End Region
End Class
