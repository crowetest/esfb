﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AssignTicket
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim HD As New HelpDesk
    Dim TeamID As Integer
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
    Dim GF As New GeneralFunctions
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 108) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Work Log Update"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            DT = HD.GetTeam(CInt(Session("UserID")))
            If DT.Rows.Count > 0 Then
                TeamID = CInt(DT.Rows(0)(0))
            Else
                TeamID = 0
            End If
            Dim FilterID As Integer = -1
            Dim FilterText As String = ""
            Dim ID As Integer = 0
            Dim TxtID As String = ""
            If Not Request.QueryString.Get("FilterID") = Nothing Then
                ID = CInt(GF.Decrypt(Request.QueryString.Get("FilterID")))
            End If
            If Not Request.QueryString.Get("FilterText") = Nothing Then
                TxtID = CStr(GF.Decrypt(Request.QueryString.Get("FilterText")))
            End If
            If Not IsNothing(Request.QueryString.Get("FilterID")) Then
                FilterID = CInt(GF.Decrypt(Request.QueryString.Get("FilterID")))
            End If
            If Not IsNothing(Request.QueryString.Get("FilterText")) Then
                FilterText = GF.Decrypt(Request.QueryString.Get("FilterText"))
            End If

            DT = HD.GetRequestForAssigning(TeamID, CInt(Session("UserID")))
            hid_Items.Value = ""
            hid_EmpDetails.Value = ""
            Dim i As Integer = 0
            For Each DR As DataRow In DT.Rows
                hid_Items.Value += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString() + "µ" + DR(7).ToString() + "µ" + DR(8).ToString() + "µ" + DR(9).ToString() + "µ" + DR(10).ToString() + "µ" + DR(11).ToString() + "µ" + DR(12).ToString() + "µ" + DR(13).ToString() + "µ" + DR(14).ToString() + "µ" + DR(15).ToString() + "µ" + DR(16).ToString()
                If i <> DT.Rows.Count - 1 Then
                    hid_Items.Value += "¥"
                End If
                i = i + 1
            Next
            Me.ddlFilter.SelectedValue = CStr(FilterID)
            Me.txtFilter.Text = FilterText

            hid_ItemAll.Value = hid_Items.Value
            Me.hid_User.Value = Session("UserID").ToString()
            Me.ddlFilter.Attributes.Add("onchange", "FilterOnChange()")

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
         
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim AssignDtl As String = Data(1)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim Arr() As String = AssignDtl.Substring(1).Split(CChar("µ"))
            Try
                Dim Params(3) As SqlParameter

                Params(0) = New SqlParameter("@AssignDtl", SqlDbType.VarChar, 5000)
                Params(0).Value = AssignDtl.Substring(1)
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_HD_RETURN_REQUEST", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            Dim AssignDtl As String = Data(1)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(3) As SqlParameter

                Params(0) = New SqlParameter("@AssignDtl", SqlDbType.VarChar, 5000)
                Params(0).Value = AssignDtl.Substring(1)
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_HD_ASSIGN_REQUEST", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 3 Then
            Dim RequestID As Integer = CInt(Data(1))
            Dim Reason As String = Data(2).ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(4) As SqlParameter

                Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                Params(0).Value = RequestID
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@Reason", SqlDbType.VarChar, 5000)
                Params(2).Value = Reason
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_HD_DEATTEND_TICKETS", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 4 Then
            DT = HD.GetTeam(CInt(Session("UserID")))
            If DT.Rows.Count > 0 Then
                TeamID = CInt(DT.Rows(0)(0))
            Else
                TeamID = 0
            End If
            DT = HD.GetRequestForAssigning(TeamID, CInt(Session("UserID")))
            hid_Items.Value = ""
            hid_EmpDetails.Value = ""
            Dim i As Integer = 0
            For Each DR As DataRow In DT.Rows
                CallBackReturn += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString() + "µ" + DR(7).ToString() + "µ" + DR(8).ToString() + "µ" + DR(9).ToString() + "µ" + DR(10).ToString() + "µ" + DR(11).ToString() + "µ" + DR(12).ToString() + "µ" + DR(13).ToString() + "µ" + DR(14).ToString() + "µ" + DR(15).ToString() + "µ" + DR(16).ToString()
                If i <> DT.Rows.Count - 1 Then
                    CallBackReturn += "¥"
                End If
                i = i + 1
            Next
        ElseIf CInt(Data(0)) = 5 Then
            Dim AssignDtl As String = Data(1)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@AssignDtl", SqlDbType.VarChar, 5000)
                Params(0).Value = AssignDtl
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_HD_RETURN_REQUEST", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
                System.Threading.Thread.Sleep(3000)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If
    End Sub
#End Region

End Class
