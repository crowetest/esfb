﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true"  CodeFile="ImplementChange.aspx.vb" Inherits="ImplementChange" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
         .fileUpload{
    width:255px;    
    font-size:11px;
    color:#000000;
    border:solid;
    border-width:1px;
    border-color:#7f9db9;    
    height:17px;
    }
            .ScrollClass
        {           
            text-align:center; color:#E31E24;
            background-color:#EBDDDD;
            height:20px;
           
        }
        </style>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
     <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.22/themes/redmond/jquery-ui.css" />
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <link href="../Style/jquery.ptTimeSelect.css" type="text/css" rel="Stylesheet"/>
   <script src="../Script/jquery.ptTimeSelect.js" type="text/javascript"></script>
   <script type="text/javascript">
       $(document).ready(function () {
           $('input[name="timeFrom"]')
            .ptTimeSelect({
                zIndex: 100,
                onBeforeShow: function (input, widget) {
                    // do something before the widget is made visible.
                }
            })
            .show();
        });
        $(document).ready(function () {
            $('input[name="timeTo"]')
            .ptTimeSelect({
                zIndex: 100,
                onBeforeShow: function (input, widget) {
                    // do something before the widget is made visible.
                }
            })
            .show();
        });
</script>  

    <script language="javascript" type="text/javascript"> 
      $('input[name="timeFrom"]')
    .ptTimeSelect({
        containerClass: undefined,
        containerWidth: undefined,
        hoursLabel:     'Hour',
        minutesLabel:   'Minutes',
        setButtonLabel: 'Set',
        popupImage:     undefined,
        onFocusDisplay: true,
        zIndex:         10,
        onBeforeShow:   undefined,
        onClose:        undefined
    });
    $('input[name="timeTo"]')
    .ptTimeSelect({
        containerClass: undefined,
        containerWidth: undefined,
        hoursLabel:     'Hour',
        minutesLabel:   'Minutes',
        setButtonLabel: 'Set',
        popupImage:     undefined,
        onFocusDisplay: true,
        zIndex:         10,
        onBeforeShow:   undefined,
        onClose:        undefined
    });


        function ChangeOnChange(){
            var ChangeNo = document.getElementById("<%= cmbChange.ClientID %>").value;
            if(ChangeNo == "-1")
            {
                alert("Select Change Request");
                document.getElementById("<%= cmbChange.ClientID %>").focus();
                return false;
            }
            else
                ToServer ("1Ø" + ChangeNo , 1);
       }

       function DetailsOnClick()
        {
            var ChangeNo = document.getElementById("<%= cmbChange.ClientID %>").value;
            if(ChangeNo == "-1")
            {
                alert("Select Ticket");
                document.getElementById("<%= cmbChange.ClientID %>").focus();
                return false;
            }
            window.open("Reports/ChangeTicketInfo.aspx?ChangeNo=" + btoa(ChangeNo) + "");
        }

       function StatusOnChange()
       {
            var Status    = document.getElementById("<%= cmbStatus.ClientID %>").value;
            if(Status == 6 || Status == 8)
            {
                document.getElementById("rowStart").style.display = "";
                document.getElementById("rowEnd").style.display = "";
            }
            else
            {
                document.getElementById("rowStart").style.display = "none";
                document.getElementById("rowEnd").style.display = "none";
            }
       }

       function btnSave_onclick() {
            var ChangeNo  = document.getElementById("<%= cmbChange.ClientID %>").value;
            var Status    = document.getElementById("<%= cmbStatus.ClientID %>").value;
            var FromDt    = document.getElementById("<%= txtFrom.ClientID %>").value;
            var ToDt      = document.getElementById("<%= txtTo.ClientID %>").value;
            var FromTime  = document.getElementById("timeFrom").value;
            var ToTime    = document.getElementById("timeTo").value;
            document.getElementById("<%= hdnTimeFrom.ClientID %>").value = FromTime;
            document.getElementById("<%= hdnTimeTo.ClientID %>").value   = ToTime;
            if(ChangeNo == "-1")
            {
                alert("Select Change Request");
                document.getElementById("<%= cmbChange.ClientID %>").focus();
                return false;
            }
            if(Status == "-1")
            {
                alert("Select Status");
                document.getElementById("<%= cmbStatus.ClientID %>").focus();
                return false;
            }
            if(Status == 6 || Status == 8)
            {
                if(FromDt == "")
                {
                    alert("Select Start Date");
                    document.getElementById("<%= txtFrom.ClientID %>").focus();
                    return false;
                }
                if(ToDt == "")
                {
                    alert("Select End Date");
                    document.getElementById("<%= txtTo.ClientID %>").focus();
                    return false;
                }
                if(FromTime == "")
                {
                    alert("Select Start Time");
                    document.getElementById("timeFrom").focus();
                    return false;
                }
                if(ToTime == "")
                {
                    alert("Select End Time");
                    document.getElementById("timeTo").focus();
                    return false;
                }
            }
            else
                return true;
       }

       function FromServer (arg,context) 
       {
             switch (context) 
             {
                 case 1:
                        {
                            var Data = arg.split("Ø");
                            document.getElementById("<%= txtDescription.ClientID %>").value = Data[0];
                            document.getElementById("<%= txtReason.ClientID %>").value = Data[1];
                            break;
                        }
               
                 
            }
       }

              
       function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
       }

       function setStartDate(sender, args) {
            var StartDt = AddDay(document.getElementById("<%= txtFrom.ClientID %>").value, 1);
            var EndDt = AddDay(document.getElementById("<%= txtTo.ClientID %>").value, 1);
            var Cnt = 1; 
            var OrgStartDt = AddDay(document.getElementById("<%= txtFrom.ClientID %>").value, Cnt);
            sender._startDate =  new Date(OrgStartDt);
            sender._endDate =  new Date(EndDt);
         }
         function AddDay(strDate, intNum) {
            sdate = new Date(strDate);
            sdate.setDate(sdate.getDate() + intNum);
            return sdate.getMonth() + 1 + " " + sdate.getDate() + " " + sdate.getFullYear();
        }
    </script>
   
</head>
</html>

<br /><br />

 <table class="style1" style="width:70%;margin: 0px auto;" >
           
        <tr> <td style="width:20%;">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager></td>
        <td style="width:15%; text-align:left;">Select Change Ticket</td>
            <td style="width:65%;cursor:pointer;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbChange" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList> &nbsp; &nbsp;
                <asp:HyperLink ID="hlDetails" runat="server" ForeColor="#0000CC" 
                        style="text-decoration: underline" ToolTip="View Details">View Details</asp:HyperLink>
                </td>
            
       </tr>
        <tr> <td style="width:20%;">&nbsp;</td>
        <td style="width:15%; text-align:left;">Change Description</td>
            <td style="width:65%">
               &nbsp; &nbsp;<asp:TextBox ID="txtDescription" runat="server"  class="ReadOnlyTextBox"
                    TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Width="80%" ReadOnly="True"></asp:TextBox>
                </td>
       </tr>
       <tr> <td style="width:20%;">&nbsp;</td>
        <td style="width:15%; text-align:left;">Reason for Change</td>
            <td style="width:65%">
                &nbsp; &nbsp;<asp:TextBox ID="txtReason" runat="server"  class="ReadOnlyTextBox"
                    TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Width="80%" ReadOnly="True"></asp:TextBox>
                </td>
       </tr>
       
       <tr> <td style="width:20%;">&nbsp;</td>
        <td style="width:15%; text-align:left;">Status</td>
            <td style="width:65%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbStatus" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList> 
                </td>
       </tr>
       
       <tr id="rowStart" style="display:none;"> <td style="width:20%;">&nbsp;</td>
        <td style="width:15%; text-align:left;">&nbsp;Start Time</td>
            &nbsp; &nbsp;<td style="width:65%">
                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtFrom" class="NormalText" runat="server" Width="27%" 
                MaxLength="100" Height="20px" onkeypress="return false" ></asp:TextBox>
             <asp:CalendarExtender ID="CalendarExtender1" runat="server"  TargetControlID="txtFrom" Format="dd MMM yyyy">
             </asp:CalendarExtender><input type="text" id="timeFrom" name="timeFrom" value="" onkeypress="return false" style="width:10%" />
                </td>
       </tr>
       <tr id="rowEnd" style="display:none;"> <td style="width:20%;">&nbsp;</td>
        <td style="width:15%; text-align:left;">End Time</td>
            &nbsp; &nbsp;<td style="width:65%">
                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtTo" class="NormalText" runat="server" Width="27%" 
                MaxLength="100" Height="20px" onkeypress="return false"></asp:TextBox>
             <asp:CalendarExtender ID="CEFromDate" runat="server" TargetControlID="txtTo" OnClientShowing="setStartDate" Format="dd MMM yyyy">
             </asp:CalendarExtender><input  type="text" id="timeTo" name="timeTo" value="" onkeypress="return false" style="width:10%"/>
          
                </td>
       </tr>
       <tr> <td style="width:20%;">&nbsp;</td>
        <td style="width:15%; text-align:left;">Attach Artifacts</td>
            <td style="width:65%">
                &nbsp; &nbsp;<asp:FileUpload ID="fupAttachment" Width="40%" runat="server" />
                </td>
       </tr>
       
       <tr>
            <td style="text-align:center;" colspan="3">
                 
                <asp:Button ID="btnSave" runat="server" Text="SAVE" style="width:7%;" Font-Names="Cambria" />
                &nbsp;&nbsp; <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 7%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
        </tr>

       <tr id = "rowSub" style = "display:none;">
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hdnTimeFrom" runat="server" />
                <asp:HiddenField ID="hdnTimeTo" runat="server" />
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

