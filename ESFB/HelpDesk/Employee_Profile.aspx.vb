﻿Imports System.Data
Partial Class Employee_Profile
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Public myVariable As String

    Dim DT As New DataTable

#Region "Page Load & Dispose"




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If GN.FormAccess(CInt(Session("UserID")), 1144) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            'If GN.AccessRole(CInt(Session("UserID"))) = False Then
            '    Me.hdnBtnEnable.Value = 1
            'Else
            '    Me.hdnBtnEnable.Value = 0
            'End If
            Me.Master.subtitle = "Employee Profile"
            GN.ComboFill(cmbBranch, EN.GetBranch(1), 0, 1)
            GN.ComboFill(cmbDepartment, EN.GetDepartment(), 0, 1)
            GN.ComboFill(cmbCadre, EN.GetCadre(), 0, 1)
            GN.ComboFill(cmbState, GN.GetState(), 0, 1)
            GN.ComboFill(cmbGender, EN.GetGender(), 0, 1)
            GN.ComboFill(cmbMaritalStatus, GN.GetMaritalStatus(), 0, 1)
            GN.ComboFill(cmbIDProof, GN.GetIDProof(), 0, 1)
            GN.ComboFill(cmbAddressProof, GN.GetAddressProof(), 0, 1)
            GN.ComboFill(cmbEmpPost, GN.GetEmpPost(), 0, 1)
            GN.ComboFill(cmbEmpType, GN.GetEmployeeStatus(), 0, 1)
            GN.ComboFill(cmbSuretyType, GN.GetFamily(), 0, 1)
            hdnCharityRate.Value = GN.GetParameterValue(8, 1)
            hdnESIRate.Value = GN.GetParameterValue(5, 1)
            hdnPFRate.Value = GN.GetParameterValue(6, 1)
            hdnSFWTRate.Value = GN.GetParameterValue(7, 1)
            hdnWelfareAmount.Value = GN.GetParameterValue(4, 1)

            GN.ComboFill(cmbBloodGroup, EN.Get_BloodGroup(), 0, 1)
            GN.ComboFill(cmbReligion, EN.Get_RELIGION(), 0, 1)
            GN.ComboFill(cmbMotherTongue, EN.Get_LANGUAGE(), 0, 1)
            DT = EN.Get_LANGUAGE()

            For Each DR As DataRow In DT.Rows
                hdnLang.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = GN.GetIDProof()
            For Each DR As DataRow In DT.Rows
                hdnIDProofVal.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = GN.GetAddressProof()
            For Each DR As DataRow In DT.Rows
                hdnAddressProofVal.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = GN.GetFamily()
            For Each DR As DataRow In DT.Rows
                hdnFamilyVal.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            txtSWF.Text = hdnWelfareAmount.Value
            DT = GN.GetQualification()
            hdnQualification.Value = ""
            hdnUniversity.Value = ""
            hdnQualificationDtl.Value = "¥-1µ-1µµ"
            hdnExperience.Value = "¥µµµ"
            hdnIDProof.Value = "¥-1µµ"
            hdnAddressProof.Value = "¥-1µµ"
            hdnFamily.Value = "¥-1µµµ"
            For Each DR As DataRow In DT.Rows
                hdnQualification.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = GN.GetUniversity()
            For Each DR As DataRow In DT.Rows
                hdnUniversity.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- Register Client Side Functions ---//
            Me.cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            Me.cmbState.Attributes.Add("onchange", "StateOnChange()")
            Me.cmbDistrict.Attributes.Add("onchange", "DistrictOnChange()")
            'Me.cmbPost.Attributes.Add("onchange", "PostOnChange()")
            Me.txtBasicPay.Attributes.Add("onchange", "BaicPayOnChange()")
            Me.cmbDepartment.Attributes.Add("onchange", "ChangeReportingOfficer()")
            Me.cmbDesignation.Attributes.Add("onchange", "ChangeReportingOfficer()")
            Me.cmbCadre.Attributes.Add("onchange", "return CadreOnChange()")
            Me.cmbMaritalStatus.Attributes.Add("onchange", "MaritalStatusOnChange()")
            Me.txtDA.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtHRA.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtConveyance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtSpecialAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtLocalAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtMedicalAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtHospitality.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtFieldAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtPerformance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtOtherAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtESI.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtPF.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtSWF.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtESWT.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtCharity.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.cmbReligion.Attributes.Add("onchange", "ReligionOnChange()")
            txtEmpCode.Attributes.Add("onchange", "EmpCodeOnChange()")
            txtEmpCode.Attributes.Add("onfocus", "EmpCodeOnFocus()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()

    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If Data(0) = 1 Then
            Dim EmpCode As Integer = CInt(Data(1))
            Dim EmpName As String = CStr(Data(2))
            Dim branch As Array = Split(Data(3), "|")
            Dim BranchID As Integer = CInt(branch(0))
            Dim DepartmentID As Integer = CInt(Data(4))
            Dim CadreID As Integer = CInt(Data(5))
            Dim DesignationID As Integer = CInt(Data(6))
            Dim DateOfJoin As Date = CDate(Data(7))
            Dim GenderID As Integer = CInt(Data(8))
            Dim ReportingTo As Integer = CInt(Data(9))
            Dim IsFieldStaff As Integer = CInt(Data(10))
            Dim CareOf As String = Data(11)
            Dim HouseName As String = Data(12)
            Dim Location As String = Data(13)
            Dim City As String = Data(14)
            Dim PostID As Integer = CInt(Data(15))
            Dim Email As String = Data(16)
            Dim Landline As String = Data(17)
            Dim Mobile As String = Data(18)
            Dim BasicPay As Double = CDbl(Data(19))
            Dim DA As Double = CDbl(Data(20))
            Dim HRA As Double = CDbl(Data(21))
            Dim Conveyance As Double = CDbl(Data(22))
            Dim SA As Double = CDbl(Data(23))
            Dim LA As Double = CDbl(Data(24))
            Dim MA As Double = CDbl(Data(25))
            Dim HA As Double = CDbl(Data(26))
            Dim PA As Double = CDbl(Data(27))
            Dim OA As Double = CDbl(Data(28))
            Dim FA As Double = CDbl(Data(29))
            Dim ESI As Double = CInt(Data(30))
            Dim PF As Double = CInt(Data(31))
            Dim SWF As Double = CInt(Data(32))
            Dim ESWT As Double = CInt(Data(33))
            Dim Charity As Double = CInt(Data(34))
            Dim DOB As Date = CDate(Data(35))
            Dim MStatusID As Integer = CInt(Data(36))
            Dim CareOfPhone As String = Data(37)
            Dim IDProofType As Integer = CInt(Data(38))
            Dim IDProofNumber As String = Data(39)
            Dim AddressProofType As Integer = CInt(Data(40))
            Dim AddressProofNumber As String = Data(41)
            Dim QualificationDtl As String = Data(42).ToString()
            If QualificationDtl <> "" Then
                QualificationDtl = QualificationDtl.Substring(1)
            End If '+Father,Mother,ReligionId,CasteId,IsTwowheeler,DrivingNo,IsSangamMember,MarriageDate+"Ø" +MotherTongueId,BloodGroupId,Cug,OfficialEmail,ChkPanch,chkComm,chkPolice,newstrExp,newstrID,newstrAddress;
            Dim EmpPost As Integer = CInt(Data(43))
            Dim Father As String = CStr(Data(44))
            Dim Mother As String = CStr(Data(45))
            Dim ReligionId As Integer = CInt(Data(46))
            Dim CasteId As Integer = CInt(Data(47))
            Dim IsTwowheeler As Integer = CInt(Data(48))
            Dim DrivingNo As String = CStr(Data(49))
            Dim IsSangamMember As Integer = CInt(Data(50))
            Dim MarriageDate As String = CStr(Data(51))
            Dim MotherTongueId As Integer = CInt(Data(52))
            Dim BloodGroupId As Integer = CInt(Data(53))
            Dim Cug As String = CStr(Data(54))
            Dim OfficialEmail As String = CStr(Data(55))
            Dim ChkPanch As Integer = CInt(Data(56))
            Dim chkComm As Integer = CInt(Data(57))
            Dim chkPolice As Integer = CInt(Data(58))
            Dim newstrExp As String = CStr(Data(59))
            If newstrExp <> "" Then
                newstrExp = newstrExp.Substring(1)
            End If
            Dim newstrID As String = CStr(Data(60))
            If newstrID <> "" Then
                newstrID = newstrID.Substring(1)
            End If
            Dim newstrAddress As String = CStr(Data(61))
            If newstrAddress <> "" Then
                newstrAddress = newstrAddress.Substring(1)
            End If
            Dim LangKnown As String = CStr(Data(62))
            Dim EmpType As Integer = CInt(Data(63))
            Dim SuretyType As Integer = CInt(Data(64))
            Dim SuretyName As String = CStr(Data(65))
            Dim SuretyAddr As String = CStr(Data(66))
            Dim SuretyPhone As String = CStr(Data(67))
            Dim newstrFamily As String = CStr(Data(68))
            If newstrFamily <> "" Then
                newstrFamily = newstrFamily.Substring(1)
            End If
            Dim UserID As Integer = CInt(Session("UserID"))
            CallBackReturn = EN.UpdateEmployee_HR(EmpCode, EmpName, BranchID, DepartmentID, CadreID, DesignationID, DateOfJoin, GenderID, ReportingTo, IsFieldStaff, UserID, CareOf, HouseName, Location, City, PostID, Email, Landline, Mobile, BasicPay, DA, HRA, Conveyance, SA, LA, MA, HA, PA, OA, FA, ESI, PF, SWF, ESWT, Charity, DOB, MStatusID, CareOfPhone, IDProofType, IDProofNumber, AddressProofType, AddressProofNumber, QualificationDtl, EmpPost, Father, Mother, ReligionId, CasteId, IsTwowheeler, DrivingNo, IsSangamMember, MarriageDate, MotherTongueId, BloodGroupId, Cug, OfficialEmail, ChkPanch, chkComm, chkPolice, newstrExp, newstrID, newstrAddress, LangKnown, EmpType, SuretyType, SuretyName, SuretyAddr, SuretyPhone, newstrFamily)
        ElseIf CInt(Data(0)) = 2 Then
            DT = GN.GetPhoto(CInt(Data(1)))
            If DT.Rows.Count > 0 Then
                Dim bytes As Byte() = DirectCast(DT.Rows(0)(0), Byte())
                Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                CallBackReturn = Convert.ToString("data:" + DT.Rows(0)(1) + ";base64,") & base64String
            Else
                CallBackReturn = "../Image/userimage.png"
            End If
        ElseIf CInt(Data(0)) = 3 Then
            Dim EmpCode = CInt(Data(1))
            DT = EN.GetEmployeeForUpdation(EmpCode)
            If DT.Rows.Count > 0 Then
                '10001µNeeradhµ4µ0µ89µ8µ12 Jun 2000µRATHNA VIHARµAVANooRµtcrµµ1µRATHNANµ37312µ390µ1µ3523546µ680547µnms@gmail.comµSoy K Eliasµ2µ13 Jun 1987µ5456789µ1µµ1µµ25000.0000µ10000.0000µ0.0000µ0.0000µ0.0000µ0.0000µ0.0000µ0.0000µ0.0000µ0.0000µ0.0000µ100.0000µ2000.0000µ20.0000µ250.0000µ250.0000µrathnan A Rµnishaµ24µ-1µ1µ01-01-1900 00:00:00µ1µhghjg µ859734567µnbnb@cesaf.comµ0µ1µ0µ1µ4µNIRMALµRATHNA VIHARµ2465767µ1
                CallBackReturn = DT.Rows(0)(0).ToString() + "µ" + DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + DT.Rows(0)(7).ToString() + "µ" + DT.Rows(0)(8).ToString() + "µ" + DT.Rows(0)(9).ToString() + "µ" + DT.Rows(0)(10).ToString() + "µ" + DT.Rows(0)(11).ToString() + "µ" + DT.Rows(0)(12).ToString() + "µ" + DT.Rows(0)(13).ToString() + "µ" + DT.Rows(0)(14).ToString() + "µ" + DT.Rows(0)(15).ToString() + "µ" + DT.Rows(0)(16).ToString() + "µ" + DT.Rows(0)(17).ToString() + "µ" + DT.Rows(0)(18).ToString() + "µ" + DT.Rows(0)(19).ToString() + "µ" + DT.Rows(0)(20).ToString() + "µ" + DT.Rows(0)(21).ToString() + "µ" + DT.Rows(0)(22).ToString() + "µ" + DT.Rows(0)(23).ToString() + "µ" + DT.Rows(0)(24).ToString() + "µ" + DT.Rows(0)(25).ToString() + "µ" + DT.Rows(0)(26).ToString() + "µ" + DT.Rows(0)(27).ToString() + "µ" + DT.Rows(0)(28).ToString() + "µ" + DT.Rows(0)(29).ToString() + "µ" + DT.Rows(0)(30).ToString() + "µ" + DT.Rows(0)(31).ToString() + "µ" + DT.Rows(0)(32).ToString() + "µ" + DT.Rows(0)(33).ToString() + "µ" + DT.Rows(0)(34).ToString() + "µ" + DT.Rows(0)(35).ToString() + "µ" + DT.Rows(0)(36).ToString() + "µ" + DT.Rows(0)(37).ToString() + "µ" + DT.Rows(0)(38).ToString() + "µ" + DT.Rows(0)(39).ToString() + "µ" + DT.Rows(0)(40).ToString() + "µ" + DT.Rows(0)(41).ToString() + "µ" + DT.Rows(0)(42).ToString() + "µ" + DT.Rows(0)(43).ToString() + "µ" + DT.Rows(0)(44).ToString() + "µ" + DT.Rows(0)(45).ToString() + "µ" + DT.Rows(0)(46).ToString() + "µ" + DT.Rows(0)(47).ToString() + "µ" + DT.Rows(0)(48).ToString() + "µ" + DT.Rows(0)(49).ToString() + "µ" + DT.Rows(0)(50).ToString() + "µ" + DT.Rows(0)(51).ToString() + "µ" + DT.Rows(0)(52).ToString() + "µ" + DT.Rows(0)(53).ToString() + "µ" + DT.Rows(0)(54).ToString() + "µ" + DT.Rows(0)(55).ToString() + "µ" + DT.Rows(0)(56).ToString() + "µ" + DT.Rows(0)(57).ToString() + "µ" + DT.Rows(0)(58).ToString() + "µ" + DT.Rows(0)(59).ToString() + "µ" + DT.Rows(0)(60).ToString() + "µ" + DT.Rows(0)(61).ToString() + "µ" + DT.Rows(0)(62).ToString() + "µ" + DT.Rows(0)(63).ToString() + "µ" + DT.Rows(0)(64).ToString()
                CallBackReturn += "Ø"
                Dim SateID As Integer = CInt(DT.Rows(0)(15))
                Dim DistrictID As Integer = CInt(DT.Rows(0)(14))
                Dim CadreID As Integer = CInt(DT.Rows(0)(5))
                Dim ReligionID As Integer = CInt(DT.Rows(0)(61))
                Dim BranchID As Array = Split((DT.Rows(0)(3)), "|")
                Dim DepID As Integer = CInt(DT.Rows(0)(2))
                Dim DesID As Integer = CInt(DT.Rows(0)(4))
                DT = GN.GetDistrict(SateID)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
                CallBackReturn += "Ø"
                DT = GN.GetPost(DistrictID)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
                CallBackReturn += "Ø"
                DT = EN.GetQualificationDtl(EmpCode)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString()
                Next
                CallBackReturn += "Ø"
                DT = GN.GetPhoto(EmpCode)
                If DT.Rows.Count > 0 Then
                    Dim bytes As Byte() = DirectCast(DT.Rows(0)(0), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                    CallBackReturn += Convert.ToString("data:" + DT.Rows(0)(1) + ";base64,") & base64String
                Else
                    CallBackReturn += "../Image/userimage.png"
                End If
                CallBackReturn += "Ø"
                DT = EN.GetFamilyDtl(EmpCode)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString()
                Next
                CallBackReturn += "Ø"
                DT = EN.GetExperienceDtl(EmpCode)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString()
                Next
                CallBackReturn += "Ø"
                DT = EN.GetAddressProofDtl(EmpCode)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
                Next
                CallBackReturn += "Ø"
                DT = EN.GetIDProofDtl(EmpCode)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
                Next
                CallBackReturn += "Ø"

                DT = EN.GetDesignation(CInt(CadreID))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
                CallBackReturn += "Ø"

                DT = EN.Get_CASTE(CInt(ReligionID))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
                CallBackReturn += "Ø"
                DT = EN.GetReportingOfficer(BranchID(0), DepID, DesID)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()

                Next
                CallBackReturn += "Ø"
                DT = EN.GetLangDtl(EmpCode)
                Dim LangVal As String = ""
                For Each DR As DataRow In DT.Rows
                    LangVal += DR(0).ToString() + "^"

                Next
                CallBackReturn += LangVal + "Ø"
            Else
                CallBackReturn = "ØØØØ"
            End If

        ElseIf CInt(Data(0)) = 4 Then
            DT = EN.GetDesignation(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 5 Then
            DT = GN.GetDistrict(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 6 Then
            DT = GN.GetPost(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 7 Then
            DT = EN.Get_CASTE(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub

#End Region



End Class
