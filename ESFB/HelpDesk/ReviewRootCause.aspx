﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true"  CodeFile="ReviewRootCause.aspx.vb" Inherits="ReviewRootCause" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
         .fileUpload{
    width:255px;    
    font-size:11px;
    color:#000000;
    border:solid;
    border-width:1px;
    border-color:#7f9db9;    
    height:17px;
    }
            .ScrollClass
        {           
            text-align:center; color:#E31E24;
            background-color:#EBDDDD;
            height:20px;
           
        }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }    

        
      function ProblemOnChange()
      {
            var Type = document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");
            if (Type[0] == "-1")
            {
                document.getElementById("txtProblem").value = "";
                document.getElementById("txtImpact").value = "";
                document.getElementById("txtRoot").value = "";
                document.getElementById("txtWork").value = "";
                document.getElementById("txtSolution").value = "";
                document.getElementById("txtRemarks").value = "";

                document.getElementById("<%= txtCategory.ClientID %>").value = "";
                document.getElementById("<%= txtPriority.ClientID %>").value = "";
            
            
                return false;
            }
            else
            {
                document.getElementById("txtProblem").value = Type[1];
                document.getElementById("txtImpact").value = Type[4];
                document.getElementById("txtRoot").value = Type[5];
                document.getElementById("txtWork").value = Type[6];
                document.getElementById("txtSolution").value = Type[7];
                document.getElementById("txtRemarks").value = "";

                document.getElementById("<%= txtCategory.ClientID %>").value = Type[2];
                document.getElementById("<%= txtPriority.ClientID %>").value = Type[3];
            }
       
        }

        function DetailsOnClick()
        {
            var Type = document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");
            var ProblemNo = Type[0];
            if(ProblemNo =="-1")
            {
                alert("Select Ticket");
                document.getElementById("<%= cmbProblem.ClientID %>").focus();
                return false;
            }
            window.open("Reports/ProblemTicketInfo.aspx?ProblemNo=" + btoa(ProblemNo) + "");
        }
        

        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function FromServer(arg, context) {

            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("../home.aspx", "_self");
            }
                     
        }    
       
        function btnSave_onclick() 
        {
             if(confirm("Are You Sure ") == true)
            {  
                var Detail = document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");
                if (Detail[0] == '-1')
                {
                    alert("Select Findings");
                    document.getElementById("<%= cmbProblem.ClientID %>").focus();
                    return false;
                }
                    
                var Descr  =  document.getElementById("txtRemarks").value;
                var ToData =  Detail[0] + "Ø" + 5 + "Ø" + Descr;
               
                ToServer ("1Ø" + ToData , 1);
            }
        }

function btnReject_onclick() {
    if(confirm("Are You Sure ") == true)
    {  
        var Detail = document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");
        if (Detail[0] == '-1')
        {
            alert("Select Findings");
            document.getElementById("<%= cmbProblem.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("txtRemarks").value == "")
        {
            alert("Enter Rejected Reason in Remarks");
            document.getElementById("txtRemarks").focus();
            return false;
        }      
        var Descr  =  document.getElementById("txtRemarks").value;
        var ToData =  Detail[0] + "Ø" + 4 + "Ø" + Descr;
               
        ToServer ("1Ø" + ToData , 1);
    }
}

    </script>
   
</head>
</html>

<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;" >
           
        <tr id="branch"> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Findings No</td>
            <td style="width:63%;cursor:pointer;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbProblem" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            &nbsp;
                    <asp:HyperLink ID="hlDetails" runat="server" ForeColor="#0000CC" 
                        style="text-decoration: underline" ToolTip="View Details">View Details</asp:HyperLink>
            </td>
            
       </tr>
       <tr> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Findings</td>
            <td style="width:63%">
                &nbsp; &nbsp;<textarea id="txtProblem" onkeypress='return TextAreaCheck(event)' 
                    class="ReadOnlyTextBox"  cols="20" name="S3" rows="2"   maxlength="1000" ReadOnly="true"
                    style="width: 80%;"></textarea></td>
            
       </tr>
       <tr> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Category</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCategory" runat="server"  
                    style=" font-family:Cambria;font-size:10pt;" Width="50%" class="ReadOnlyTextBox" 
                    MaxLength="50"  ReadOnly="true" />
            </td>
            
       </tr>
       <tr> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Priority</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtPriority" runat="server"  
                    style=" font-family:Cambria;font-size:10pt;" Width="50%" class="ReadOnlyTextBox" 
                    MaxLength="50"  ReadOnly="true" />
            </td>
            
       </tr>
       <tr> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Impact</td>
            <td style="width:63%">
                &nbsp; &nbsp;<textarea id="txtImpact" onkeypress='return TextAreaCheck(event)' 
                    class="ReadOnlyTextBox"  cols="20" name="S3" rows="3"   maxlength="1000" ReadOnly="true"
                    style="width: 80%;"></textarea></td>
            
       </tr>
       <tr> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">RootCause</td>
            <td style="width:63%">
                &nbsp; &nbsp;<textarea id="txtRoot" 
                    class="ReadOnlyTextBox"  cols="20" name="S3" rows="3"   maxlength="1000" ReadOnly="true"
                    style="width: 80%;" onkeypress='return TextAreaCheck(event)' ></textarea></td>
            
       </tr>
       <tr> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Work Around</td>
            <td style="width:63%">
                &nbsp; &nbsp;<textarea id="txtWork" onkeypress='return TextAreaCheck(event)' 
                    class="ReadOnlyTextBox"  cols="20" name="S3" rows="3"   maxlength="1000" ReadOnly="true"
                    style="width: 80%;"></textarea></td>
            
       </tr>
       <tr> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Solution</td>
            <td style="width:63%">
                &nbsp; &nbsp;<textarea id="txtSolution" onkeypress='return TextAreaCheck(event)' 
                    class="ReadOnlyTextBox"  cols="20" name="S3" rows="3"   maxlength="1000" ReadOnly="true"
                    style="width: 80%;"></textarea></td>
            
       </tr>
       <tr> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Remarks</td>
            <td style="width:63%">
                &nbsp; &nbsp;<textarea id="txtRemarks" onkeypress='return TextAreaCheck(event)' 
                    class="NormalText"  cols="20" name="S3" rows="3"   maxlength="1000" 
                    style="width: 80%;"></textarea></td>
            
       </tr>
       <tr>
            <td style="text-align:center;" colspan="3"><br />
            &nbsp;
                 <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="ACCEPT"  onclick="return btnSave_onclick()" />
                <input id="btnReject" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="REJECT"  onclick="return btnReject_onclick()" />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
                <br />
                <asp:HiddenField ID="hdnItemID" runat="server" />
                </td>
        </tr>

</table>    


</asp:Content>

