﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="BranchRequest.aspx.vb" Inherits="BranchRequest" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
<%--        <script type="text/javascript" src="Script/jquery-1.2.6.min.js"></script>--%>
        <script language="javascript" type="text/javascript">
    
    function RequestOnchange() {
        document.getElementById("Asset").style.display = "none"; 
        document.getElementById("AssetTag").style.display = "none";
        document.getElementById("AssetCount").style.display = "none";
        document.getElementById("<%= cmbDep.ClientID %>").disabled = true; 
        document.getElementById("<%= btnView.ClientID %>").style.display = "none";
        document.getElementById("<%= hdnAnnexture.ClientID %>").value = 0;

        if(document.getElementById("<%= cmbBranch.ClientID %>").value !=-1)
        {
            var ToData = "4Ø" + document.getElementById("<%= cmbBranch.ClientID %>").value; 
            ToServer(ToData, 4);
        }
        else
        {
            document.getElementById("<%= txtContactNo.ClientID %>").value="";
            document.getElementById("<%= txtEmailid.ClientID %>").value="";
        }   
    }
    function ViewAttachment() 
    {
        var ProblemDtl	= document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");
        var ProblemID=ProblemDtl[0];
        if (ProblemID > 0)
        alert("ShowFormat");
            window.open("ShowFormat.aspx?ProblemID=" + btoa(ProblemID) + "");
        return false;
    }
    function CategoryOnChange() {
        ClearCombo("<%= cmbItem.ClientID %>");
        ClearCombo("<%= cmbProblem.ClientID %>");
        var Groupid = document.getElementById("<%= cmbCategory.ClientID %>").value;
        if(Groupid == 1){
            document.getElementById("AcOrCif").style.display = "";
        }
        else{
            document.getElementById("AcOrCif").style.display = "none";
        }
        if (Groupid > 0) {
            var ToData = "2Ø" + Groupid; 
            ToServer(ToData, 2);
        }
    }
    function ItemOnChange() {
        var SubGroupid = document.getElementById("<%= cmbItem.ClientID %>").value;
        if (SubGroupid > 0) {
            var ToData = "3Ø" + SubGroupid;
            ToServer(ToData, 3);
        }
    } 
    function ProblemOnChange()
    {
        var ProblemDtl	= document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");
        var ProblemID=ProblemDtl[0];
        var AsssetStatus=ProblemDtl[2];
        var IsCritical=ProblemDtl[3];
        var Description=ProblemDtl[4];
        var Annexture=ProblemDtl[5];
        document.getElementById("<%= hdnAnnexture.ClientID %>").value = ProblemDtl[5];
        document.getElementById("<%= cmbProblem.ClientID %>").title = Description;
        if(IsCritical == 1)
            document.getElementById("<%= lblCritical.ClientID %>").innerHTML='Critical';
        else
            document.getElementById("<%= lblCritical.ClientID %>").innerHTML='';

        if(Annexture == 1)
            document.getElementById("<%= btnView.ClientID %>").style.display = '';
        else
            document.getElementById("<%= btnView.ClientID %>").style.display = "none";

        if(AsssetStatus == 1)
        {
            document.getElementById("Asset").style.display = "";
            document.getElementById("<%= cmbAssetStatus.ClientID %>").value = -1;
            document.getElementById("AssetTag").style.display = "none";
            document.getElementById("AssetCount").style.display = "none";
        }
        else
        {
            document.getElementById("Asset").style.display = "none";
            document.getElementById("AssetTag").style.display = "none";
            document.getElementById("AssetCount").style.display = "none";
        }
    }
    function AssetStatusOnChange()
    {
        var AssetStatus = document.getElementById("<%= cmbAssetStatus.ClientID %>").value;
        if (AssetStatus == 2) // Repaire
        {
            document.getElementById("AssetTag").style.display = "";
            document.getElementById("AssetCount").style.display = "none";
        }
        else // New
        {
            document.getElementById("AssetTag").style.display = "none";
            document.getElementById("AssetCount").style.display = "";
        }
    }
    function AcOrCif_OnChange()
    {
        var selectedNumber = document.getElementById("<%= cmbAcOrCif.ClientID %>").value;
        if(selectedNumber == 1)
        {
            document.getElementById("<%= AcNum.ClientID %>").style.display = "";
            document.getElementById("<%= CifNum.ClientID %>").style.display = "none";
        }
        else if(selectedNumber == 2)
        {
            document.getElementById("<%= CifNum.ClientID %>").style.display = "";
            document.getElementById("<%= AcNum.ClientID %>").style.display = "none";
        }
        else
        {
            document.getElementById("<%= CifNum.ClientID %>").style.display = "none";
            document.getElementById("<%= AcNum.ClientID %>").style.display = "none";
        }
    }
    function AssetTagOnChange()
    {
        var AssetTag = document.getElementById("<%= txtAssetTAG.ClientID %>").value;
        var ItemID   = document.getElementById("<%= cmbProblem.ClientID %>").value;
        if (AssetTag == "" || AssetTag == 0)
        {
            alert("Please Enter a Valid Asset Tag");
            document.getElementById("<%= txtAssetTAG.ClientID %>").focus();
            return false;
        } 
        var Branch	=document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");;
        var Dep	= document.getElementById("<%= cmbDep.ClientID %>").value;
        var ToData = "5Ø" + AssetTag + "Ø" + Branch + "Ø" + Dep + "Ø" + ItemID;
        ToServer(ToData, 5);
    }
        
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 1; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function ClearCombo(control) {
        document.getElementById(control).options.length = 0;
        var option1 = document.createElement("OPTION");
        option1.value = -1;
        option1.text = " -----Select-----";
        document.getElementById(control).add(option1);
    }
    function FromServer(arg, context) {
        if (context == 1) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("BranchRequest.aspx", "_self");
        }
        else if (context == 2) {
            ComboFill(arg, "<%= cmbItem.ClientID %>");
        }
        else if (context == 3) {
            ComboFill(arg, "<%= cmbProblem.ClientID %>");
        }
        else if (context == 5) {
            if(arg == 0)
            {
                alert("Please Enter a Valid Asset Tag");
                document.getElementById("<%= txtAssetTAG.ClientID %>").value = "";
                document.getElementById("<%= txtAssetTAG.ClientID %>").focus();
                return false;
            }
        }
    }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

    function RequestOnClick() {  
        if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1") 
        {
            alert("Select Branch");
            document.getElementById("<%= cmbBranch.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= cmbDep.ClientID %>").value == "-1") 
        {
            alert("Select Department");
            document.getElementById("<%= cmbDep.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= cmbCategory.ClientID %>").value == "-1") 
        {
            alert("Select Category");
            document.getElementById("<%= cmbCategory.ClientID %>").focus();
            return false;
        } 
        if (document.getElementById("<%= cmbItem.ClientID %>").value == "-1") 
        {
            alert("Select Sub Category");
            document.getElementById("<%= cmbItem.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= cmbProblem.ClientID %>").value == "-1") 
        {
            alert("Select Findings");
            document.getElementById("<%= cmbProblem.ClientID %>").focus();
            return false;
        }      
        if (document.getElementById("txtDetails").value == "") 
        {
            alert("Enter Description");
            document.getElementById("txtDetails").focus();
            return false;
        }     
        if (document.getElementById("<%= txtContactNo.ClientID %>").value== "") 
        {
            alert("Enter Contact Number");
            document.getElementById("<%= txtContactNo.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= txtEmailid.ClientID %>").value == "") 
        {
            alert("Enter Email Id ");
            document.getElementById("<%= txtEmailid.ClientID %>").focus();
            return false;
        }
        else{
            var ret= checkEmail(document.getElementById("<%= txtEmailid.ClientID %>")); if(ret==false) return false;
        }
        var AcOrCif = document.getElementById("<%= cmbAcOrCif.ClientID %>").value;
        var AcOrCif_Num;
        if((document.getElementById("<%= cmbAcOrCif.ClientID %>").value == -1) && (document.getElementById("<%= cmbCategory.ClientID %>").value == 1))
        {
            alert("Enter Account Number/CIF Number");
            return false;
        }
        else if(document.getElementById("<%= cmbAcOrCif.ClientID %>").value == 1)
        {
            AcOrCif_Num = document.getElementById("<%= AcNum.ClientID %>").value;
            if(document.getElementById("<%= AcNum.ClientID %>").value == '')
            {
                alert("Enter Account Number");
                return false;
            }
            else if(AcOrCif_Num.length < 14)
            {
                alert("Enter a valid Account Number");
                return false;
            }
        }
        else if(document.getElementById("<%= cmbAcOrCif.ClientID %>").value == 2)
        {
            AcOrCif_Num = document.getElementById("<%= CifNum.ClientID %>").value;
            if(document.getElementById("<%= CifNum.ClientID %>").value == '')
            {
                alert("Enter CIF Number");
                return false;
            }
            else if(AcOrCif_Num.length < 12)
            {
                alert("Enter a valid CIF Number");
                return false;
            }
        }
                   
//        if (document.getElementById("<%= txtCorrectionCnt.ClientID %>").value== "" || document.getElementById("<%= txtCorrectionCnt.ClientID %>").value== " " || document.getElementById("<%= txtCorrectionCnt.ClientID %>").value== 0) 
//        {
//            alert("Enter Correction Count");
//            document.getElementById("<%= txtCorrectionCnt.ClientID %>").focus();
//            return false;
//        }
//        var CorrCnt = document.getElementById("<%= txtCorrectionCnt.ClientID %>").value;
        var CorrCnt = 0;
        var Branch	= document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");;
        var Dep	= document.getElementById("<%= cmbDep.ClientID %>").value;
        var Category	= document.getElementById("<%= cmbCategory.ClientID %>").value;
 
        var Module	= document.getElementById("<%= cmbItem.ClientID %>").value;
        var ProblemDtl	= document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");
        var Problem=ProblemDtl[0];
        var AssetRelated = ProblemDtl[2];
        var AssetStatus = 0;
        var AssetTag = "";
        var AssetCount = 0;
        if (AssetRelated == 1)
        {
            AssetStatus = document.getElementById("<%= cmbAssetStatus.ClientID %>").value;
            if (AssetStatus == 2)
            { 
                AssetTag = document.getElementById("<%= txtAssetTAG.ClientID %>").value;
                if(AssetTag == "")
                {
                    alert("Enter Asset TAG");
                    document.getElementById("<%= txtAssetTAG.ClientID %>").focus();
                    return false;
                }
            }
            else
            {
                if(document.getElementById("<%= txtCount.ClientID %>").value == "" || document.getElementById("<%= txtCount.ClientID %>").value == 0)
                {
                    alert("Please Enter No Of Assets");
                    document.getElementById("<%= txtCount.ClientID %>").focus();
                    return false;
                }
                AssetCount = document.getElementById("<%= txtCount.ClientID %>").value;
            }
        }
        var Descr	= document.getElementById("txtDetails").value; 
        var contact=document.getElementById("<%= txtContactNo.ClientID %>").value;
        var emailid=document.getElementById("<%= txtEmailid.ClientID %>").value;
        var vendorTicket = document.getElementById("<%= vendorTicket.ClientID %>").value;
        document.getElementById("<%= hdnValue.ClientID %>").value = "1Ø" + Branch[0] + "Ø" + Dep + "Ø" + Category + "Ø" + Module + "Ø" + Problem + "Ø" + Descr + "Ø" +contact+ "Ø" +emailid+ "Ø" +AssetStatus+ "Ø" +AssetTag+ "Ø" +AssetCount + "Ø" + CorrCnt + "Ø" + AcOrCif + "Ø" + AcOrCif_Num + "Ø" + vendorTicket;       
    }

    function AddMoreImages() {
        if (!document.getElementById && !document.createElement)
            return false;
        var fileUploadarea = document.getElementById("fileUploadarea");
        if (!fileUploadarea)
            return false;
        var newLine = document.createElement("br");
        fileUploadarea.appendChild(newLine);
        var newFile = document.createElement("input");
        newFile.type = "file";
        newFile.setAttribute("class", "fileUpload");
            
        if (!AddMoreImages.lastAssignedId)
            AddMoreImages.lastAssignedId = 100;
        newFile.setAttribute("id", "FileUpload" + AddMoreImages.lastAssignedId);
        newFile.setAttribute("name", "FileUpload" + AddMoreImages.lastAssignedId);
        var div = document.createElement("div");
        div.appendChild(newFile);
        div.setAttribute("id", "div" + AddMoreImages.lastAssignedId);
        fileUploadarea.appendChild(div);
        AddMoreImages.lastAssignedId++;
    }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    </html>
    <div class="ScrollClass">
        <b>Help Desk Contact Numbers -</b> 08589975581, 08589975582, 08589975583, 08589975584<hr
            style="color: #E31E24; margin-top: 0px;" />
    </div>
    <br />
    <br />
    <table class="style1" style="width: 80%; margin: 0px auto;">
        <tr id="branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Department">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Department
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbDep" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Category">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Category
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbCategory" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Module">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Sub Category
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbItem" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Problem">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Findings
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbProblem" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                &nbsp; &nbsp;<asp:Label ID="lblCritical" runat="server" Font-Bold="True" 
                    ForeColor="#CC0000"></asp:Label>
                &nbsp; &nbsp;<asp:ImageButton ID="btnView" runat="server" Height="18px" Width="18px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Annexture" />
            </td>
        </tr>
        <tr id="Asset">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Asset Status
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbAssetStatus" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="2"> Repair </asp:ListItem>
                    <asp:ListItem Value="1"> New</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="AssetTag">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Asset TAG
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtAssetTAG" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="50"  />
            </td>
        </tr>
        <tr id="AssetCount">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Count
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCount" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="4" 
                    onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="AssetCount" style="display:none">
            <td style="width: 25%;">
                &nbsp;</td>
            <td style="width: 12%; text-align: left;">
                Correction Count</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCorrectionCnt" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;display:none;" Width="50%" class="NormalText" MaxLength="4" 
                    onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="Tr1">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_Post" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Description
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<textarea id="txtDetails" class="NormalText" cols="20" name="S1" rows="3" onkeypress='return TextAreaCheck(event)' 
                    maxlength="1000" style="width: 70%"></textarea>
            </td>
        </tr>
        <tr id="contactno">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Contact No.
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtContactNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="50" onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="Email">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Email Id
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmailid" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" MaxLength="50" />
            </td>
        </tr>
        <tr id="AcOrCif" style="display: none;">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Account No. / CIF No.
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbAcOrCif" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="20%" ForeColor="Black" onchange="AcOrCif_OnChange()">
                    <asp:ListItem Value="-1"> -------Select-------</asp:ListItem>
                    <asp:ListItem Value="1"> Account Number </asp:ListItem>
                    <asp:ListItem Value="2"> CIF Number </asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="AcNum" runat="server" Style="font-family: Cambria;
                    font-size: 10pt; display:none;" Width="29.69%" MaxLength="14" placeholder="Enter Account Number" onkeypress='return NumericCheck(event)'/>
                <asp:TextBox ID="CifNum" runat="server" Style="font-family: Cambria;
                    font-size: 10pt; display:none;" Width="29.69%" MaxLength="12"  placeholder="Enter CIF Number" onkeypress='return NumericCheck(event)'/>
            </td>
        </tr>
        <tr id="Tr2">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Vendor Ticket No.
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="vendorTicket" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" MaxLength="50" />
            </td>
        </tr>
        <tr id="Tr7">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Attachment If any
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;
                <div id="fileUploadarea">
                    <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" /><br />
                </div>
                <br />
                <div>
                    <input style="display: block;" id="btnAddMoreFiles" type="button" value="Add more images"
                        onclick="AddMoreImages();" /><br />
                </div>
            </td>
            <td style="width: 12%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 20%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:Button ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
                <asp:HiddenField ID="hdnAnnexture" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
