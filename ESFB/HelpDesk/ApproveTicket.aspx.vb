﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ApproveTicket
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim HD As New HelpDesk
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 107) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Ticket Approval"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            DT = HD.GetRequestForApproval(CInt(Session("UserID")), CInt(Session("Post_ID")))
            If Not IsNothing(DT) Then
                hid_Items.Value = ""
                hid_EmpDetails.Value = ""
                Dim i As Integer = 0
                For Each DR As DataRow In DT.Rows
                    hid_Items.Value += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString()
                    If i <> DT.Rows.Count - 1 Then
                        hid_Items.Value += "¥"
                    End If
                    i = i + 1
                Next
                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim RequestDtl As String = Data(1)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@RequestDtl", SqlDbType.VarChar, 5000)
                Params(0).Value = RequestDtl.Substring(1)
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_HD_APPROVE_REQUEST", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            If CInt(Data(1)) > 0 Then
                DT = HD.GetSubGroup(CInt(Data(1)))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()

                Next

            End If
        ElseIf CInt(Data(0)) = 3 Then
            If CInt(Data(1)) > 0 Then
                DT = HD.GetPROBLEM(CInt(Data(1)))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()

                Next

            End If
        ElseIf CInt(Data(0)) = 4 Then
            DT = HD.GetPreviousRemarks(CInt(Data(1)))
            Dim StrVal As String
            StrVal = HD.GetString(DT)
            CallBackReturn = StrVal.ToString
        End If

    End Sub
#End Region

End Class
