﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class CallCapture
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DB As New MS_SQL.Connect
    Dim DT, DT1, DT2 As New DataTable

#Region "Page Load & Dispose"




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'If GN.FormAccess(CInt(Session("UserID")), 84) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If

            Me.Master.subtitle = "Call Capture"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            txtEmpCode.Attributes.Add("onchange", "EmpCodeOnChange()")
            cmbRelation.Attributes.Add("onchange", "RelationOnChange()")

            txtBranchCode.Attributes.Add("onchange", "BranchOnChange()")
            cmbcategory.Attributes.Add("onchange", "CategoryOnChange()")
            cmbsubcategory.Attributes.Add("onchange", "SubCategoryOnChange()")
            txtBranchName.Attributes.Add("onchange", "BranchNameOnChange()")
            Me.rdbEmp.Attributes.Add("OnChange", "return SelectionOnChange()")
            Me.rdbBranch.Attributes.Add("OnChange", "return SelectionOnChange()")
            call_button.Attributes.Add("onclick", "return timeNow()")
            'cmbNeedGenerateTicket.Attributes.Add("onchange", "GenereateTicketOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()

    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If Data(0) = 1 Then
            Dim EmpCode As String
            Dim EmpDtl As String = ""
            Dim CallDtl As String = ""
            Dim BranchDtl As String = ""
            Dim branchId As String = ""
            EmpCode = CStr(Data(1))
            DT = DB.ExecuteDataSet("select A.emp_code,A.emp_name,A.branch_id,B.branch_name,c.designation_name from emp_master A " +
               " inner join Branch_master B on A.branch_id = B.Branch_id " +
               " inner join Designation_master c on A.designation_id = c.designation_id  " +
            " where(A.emp_code = " + EmpCode + " And A.Status_id = 1)").Tables(0)
            For Each DR As DataRow In DT.Rows
                branchId = DR(2).ToString()
                EmpDtl += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "¥"
            Next
            If branchId <> "" Then
                DT1 = DB.ExecuteDataSet("select Emp_code,Branch_code,case when Call_type=1 then 'Incoming' when Call_type=2 then 'Outgoing' else 'Not Mentioned' end," +
            " case when Relation_type=1 then 'Esaf Related' when Relation_type=2 then 'Fis Related' else 'No Ticket' end," +
            "startTIme,cast(cast(EndTime as datetime)-cast(startTime as datetime) as time) as call_duration,Remarks from HD_Call_Capture_Master" +
            " where branch_code=" + branchId).Tables(0)
                For Each DR As DataRow In DT1.Rows
                    CallDtl += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(5).ToString() + "¥"
                Next

                DT2 = DB.ExecuteDataSet("select a.emp_code,a.emp_name,b.mobile,c.designation_name  from branch_master bm " +
"inner join emp_master a on a.branch_id=bm.branch_id " +
"inner join emp_profile b on a.emp_code=b.emp_code " +
"inner join designation_master c on a.designation_id=c.designation_id " +
"where a.branch_id=" + branchId + " and a.emp_status_id not in(5,6) and a.status_id not in(3)").Tables(0)
                For Each DR As DataRow In DT2.Rows
                    BranchDtl += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "¥"
                Next
                CallBackReturn = EmpDtl + "Ø" + CallDtl + "Ø" + BranchDtl

            Else
                CallBackReturn = ""
            End If


        ElseIf CInt(Data(0)) = 2 Then

            Dim Relation As String
            Dim Branchcode As String
            Dim TicketDtl As String = ""
            Dim GroupDtl As String = ""
            

            Relation = CStr(Data(1))
            Branchcode = CStr(Data(2))
            Dim count As Integer
            Try

                If Data(3).ToString() <> "" Then
                    count = CInt(Data(3).ToString())
                Else
                    count = Nothing
                End If

            Catch ex As Exception

            End Try
           
            If Relation = 1 Then
                DT = DB.ExecuteDataSet("select top(5 + " + count.ToString() + ") request_id,ticket_no,Request_dt,branch_id,remarks  from HD_request_master where branch_id=" + Branchcode + " order by request_id desc").Tables(0)
                For Each DR As DataRow In DT.Rows
                    TicketDtl += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "¥"
                Next
                DT1 = DB.ExecuteDataSet("select -1 as call_group_id,'----select------' as call_group_name union all select call_group_id,call_group_name from hd_group_master_call_capture").Tables(0)
                For Each DR As DataRow In DT1.Rows
                    GroupDtl += DR(0).ToString() + "µ" + DR(1).ToString() + "¥"
                Next
                CallBackReturn = TicketDtl + "Ø" + GroupDtl + "Ø" + count.ToString()

            ElseIf Relation = 2 Then
                DT = DB.ExecuteDataSet("select -1 as call_group_id,'----select------' as call_group_name union all select call_group_id,call_group_name from hd_group_master_call_capture").Tables(0)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += DR(0).ToString() + "µ" + DR(1).ToString() + "¥"
                Next
                CallBackReturn = "" + "Ø" + CallBackReturn

            End If

        ElseIf CInt(Data(0)) = 3 Then

            Dim EmployeeDtl As String = ""
            Dim BranchDtl As String = ""
            Dim CallDtl As String = ""
            Dim BranchCode As String
            BranchCode = CStr(Data(1))
            DT = DB.ExecuteDataSet("select a.emp_code,a.emp_name,b.mobile,c.designation_name  from branch_master bm " +
 "inner join emp_master a on a.branch_id=bm.branch_id " +
"inner join emp_profile b on a.emp_code=b.emp_code " +
"inner join designation_master c on a.designation_id=c.designation_id " +
"where a.branch_id=" + BranchCode + " and a.emp_status_id not in(5,6) and a.status_id not in(3)").Tables(0)
            For Each DR As DataRow In DT.Rows
                EmployeeDtl += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "¥"
            Next
            DT1 = DB.ExecuteDataSet("select branch_id,branch_name from branch_master").Tables(0)
            For Each DR As DataRow In DT1.Rows
                BranchDtl += DR(0).ToString() + "µ" + DR(1).ToString() + "¥"
            Next
            DT2 = DB.ExecuteDataSet("select Emp_code,Branch_code,case when Call_type=1 then 'Incoming' when Call_type=2 then 'Outgoing' else 'Not Mentioned' end," +
            " case when Relation_type=1 then 'Esaf Related' when Relation_type=2 then 'Fis Related' else 'No Ticket' end," +
            "startTIme,cast(cast(EndTime as datetime)-cast(startTime as datetime) as time) as call_duration,Remarks from HD_Call_Capture_Master" +
            " where branch_code=" + BranchCode).Tables(0)
            For Each DR As DataRow In DT2.Rows
                CallDtl += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "¥"
            Next
            CallBackReturn = EmployeeDtl + "Ø" + BranchDtl + "Ø" + CallDtl

        ElseIf CInt(Data(0)) = 4 Then

            Dim Category As String
            Category = CStr(Data(1))
            DT = DB.ExecuteDataSet("select -1 as call_sub_group_id,'----select------' as call_sub_group_name union all select call_sub_group_id,call_sub_group_name from HD_SUB_GROUP_CALL_CAPTURE where call_group_id=" + Category).Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += DR(0).ToString() + "µ" + DR(1).ToString() + "¥"
            Next
            CallBackReturn += "Ø"
        ElseIf CInt(Data(0)) = 5 Then

            Dim finding As String
            finding = CStr(Data(1))
            DT = DB.ExecuteDataSet("select -1 as call_problem_id,'----select------' as call_problem union all select call_problem_id,call_problem from hd_problem_type_call_capture where call_sub_group_id=" + finding).Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += DR(0).ToString() + "µ" + DR(1).ToString() + "¥"
            Next
            CallBackReturn += "Ø"

        ElseIf CInt(Data(0)) = 6 Then
            Dim IncomeOrOutcome As Integer = CInt(Data(1))
            Dim EmpCode As Integer = CInt(Data(2))
            Dim BranchCode As Integer = CInt(Data(3))
            'Dim Branch As Integer = CInt(Data(1))
            Dim Relation As Integer = CInt(Data(4))
            Dim TicketNo As String = CStr(Data(5))
            'Dim TicketDate As DateTime = CDate(Data(6))
            Dim TicketDate As Nullable(Of DateTime)
            If Data(6).ToString = "" Then
                TicketDate = Nothing
            Else
                TicketDate = CDate(Data(6).ToString())
            End If

            Dim Category As Integer = CInt(Data(7))
            Dim SubCategory As Integer = CInt(Data(8))
            Dim Finding As Integer = CInt(Data(9))
            Dim Remark As String = Data(10).ToString()
            Dim StartTime As String = Data(11)
            Dim Type As Integer = CInt(Data(12))
            Dim Message As String = Nothing
            Dim callCaptureID As Integer = 0
            Dim UserID As String = Session("UserID").ToString()
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(14) As SqlParameter

                Params(0) = New SqlParameter("@IncomeOrOutcome", SqlDbType.Int)
                Params(0).Value = IncomeOrOutcome
                Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(1).Value = EmpCode
                Params(2) = New SqlParameter("@BranchCode", SqlDbType.Int)
                Params(2).Value = BranchCode
                Params(3) = New SqlParameter("@Relation", SqlDbType.Int)
                Params(3).Value = Relation
                Params(4) = New SqlParameter("@TicketNo", SqlDbType.VarChar, 100)
                Params(4).Value = TicketNo
                Params(5) = New SqlParameter("@TicketDate", SqlDbType.DateTime)
                Params(5).Value = TicketDate
                Params(6) = New SqlParameter("@Category", SqlDbType.Int)
                Params(6).Value = Category
                Params(7) = New SqlParameter("@SubCategory", SqlDbType.Int)
                Params(7).Value = SubCategory
                Params(8) = New SqlParameter("@Finding", SqlDbType.Int)
                Params(8).Value = Finding
                Params(9) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                Params(9).Value = Remark
                Params(10) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(10).Direction = ParameterDirection.Output
                Params(11) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(11).Direction = ParameterDirection.Output
                Params(12) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(12).Value = UserID
                Params(13) = New SqlParameter("@StartTime", SqlDbType.VarChar, 500)
                Params(13).Value = StartTime
                Params(14) = New SqlParameter("@Type", SqlDbType.Int)
                Params(14).Value = Type

                DB.ExecuteNonQuery("[SP_HD_CALL_CAPTURE]", Params)
                ErrorFlag = CInt(Params(10).Value)
                Message = CStr(Params(11).Value)
                If ErrorFlag = 0 Then
                    DT = DB.ExecuteDataSet("select max(call_capture_id) from HD_Call_Capture_Master").Tables(0)
                    For Each DR As DataRow In DT.Rows
                        callCaptureID = DR(0).ToString()
                    Next
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + callCaptureID.ToString
        ElseIf Data(0) = 7 Then
            Dim Branchcode As String
            Branchcode = CStr(Data(1))
            DT = DB.ExecuteDataSet("select branch_id,branch_name from branch_master where branch_id=" + Branchcode).Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += DR(0).ToString() + "µ" + DR(1).ToString() + "¥"
            Next
            CallBackReturn += "Ø"

        ElseIf Data(0) = 8 Then
            Dim BranchName As String
            BranchName = CStr(Data(1))
            DT = DB.ExecuteDataSet("select branch_id from branch_master where branch_name like'" & BranchName & "%' ").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString
            End If
        ElseIf Data(0) = 9 Then
            Dim BranchAll As String = ""
            Dim CallDtl As String = ""
            DT = DB.ExecuteDataSet("select -1 as  branch_id,'----select----' as branch_name union all  select branch_id,branch_name from branch_master where Status_ID=1").Tables(0)
            For Each DR As DataRow In DT.Rows
                BranchAll += DR(0).ToString() + "µ" + DR(1).ToString() + "¥"
            Next
           
            DT1 = DB.ExecuteDataSet("select  max(current_day) as t1,max(current_Month) as t2 from ( " +
" select 1 as id,concat(count(user_id),'/',(select count(call_capture_id)  from  HD_Call_Capture_Master " +
" where convert(date,tra_dt)=convert(date,getdate()))) as current_day , '' as current_Month from  HD_Call_Capture_Master " +
" where convert(date,tra_dt)=convert(date,getdate()) and user_id=" & Session("UserID") & " " +
" union all " +
" select 1 as id ,'' as current_day, concat(count(user_id),'/',(select count(call_capture_id) from  HD_Call_Capture_Master " +
" where datepart(mm,tra_dt) =month(getdate()))) as current_Month from  HD_Call_Capture_Master " +
" where datepart(mm,tra_dt) =month(getdate()) and user_id=" & Session("UserID") & " )a group by id").Tables(0)
            For Each DR As DataRow In DT1.Rows
                CallDtl = DR(0).ToString() + "µ" + DR(1).ToString()
            Next
            CallBackReturn = BranchAll + "Ø" + CallDtl


        End If

    End Sub

#End Region



End Class

