﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Partial Class ImplementChange
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim HD As New HelpDesk
    Dim CallBackReturn As String = Nothing
    Dim UserID As Integer
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 260) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Implementation of Change Plan"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- End ---//

            UserID = CInt(Session("UserID"))
            If Not IsPostBack Then
                DT = GN.GetQueryResult("SELECT '---SELECT---','-1' UNION ALL select CHANGE_NO,CHANGE_NO from HD_CHANGE_MASTER WHERE STATUS_ID in  (4,5) ")
                GN.ComboFill(cmbChange, DT, 1, 0)
                DT = GN.GetQueryResult("SELECT '---SELECT---',-1 UNION ALL select STATUS,STATUS_ID from HD_CHANGE_STATUS WHERE STATUS_ID IN (5,6,7,8) ")
                GN.ComboFill(cmbStatus, DT, 1, 0)
            End If

            Me.cmbChange.Attributes.Add("onchange", "return ChangeOnChange()")
            Me.cmbStatus.Attributes.Add("onchange", "return StatusOnChange()")
            Me.btnSave.Attributes.Add("onclick", "return btnSave_onclick()")
            Me.hlDetails.Attributes.Add("onclick", "DetailsOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim ChangeNo As String = CStr(Data(1))
            DT = GN.GetQueryResult("SELECT CHANGE,REASON FROM HD_CHANGE_MASTER WHERE CHANGE_NO = '" & ChangeNo & "'")
            CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString()
        End If
    End Sub
#End Region
#Region "Confirmation"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim myFile As HttpPostedFile = fupAttachment.PostedFile
            Dim FileLen As Integer = myFile.ContentLength
            If (FileLen > 0) Then
                ContentType = myFile.ContentType
                AttachImg = New Byte(FileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, FileLen)
            End If
            Dim fileName As String = Path.GetFileName(fupAttachment.PostedFile.FileName)
            Dim fileExtension As String = Path.GetExtension(fupAttachment.PostedFile.FileName)
            If (FileLen = 0) Or ((FileLen > 0) And (fileExtension = ".ZIP" Or fileExtension = ".zip" Or fileExtension = ".RAR" Or fileExtension = ".rar")) Then
                Dim ChangeNo As String = Me.cmbChange.SelectedValue.ToString()
                Dim StatusID As Integer = CInt(Me.cmbStatus.SelectedValue)
                Dim TimeFrom As String = Me.txtFrom.Text.ToString() + " " + Me.hdnTimeFrom.Value.ToString()
                Dim TimeTo As String = Me.txtTo.Text.ToString() + " " + Me.hdnTimeTo.Value.ToString()
                Dim ErrorFlag As Integer = 0
                Dim Message As String = ""
                Try
                    Dim Params(9) As SqlParameter
                    Params(0) = New SqlParameter("@ChangeNo", SqlDbType.VarChar, 20)
                    Params(0).Value = ChangeNo
                    Params(1) = New SqlParameter("@StatusID", SqlDbType.Int)
                    Params(1).Value = StatusID
                    Params(2) = New SqlParameter("@TimeFrameStart", SqlDbType.VarChar, 100)
                    Params(2).Value = TimeFrom
                    Params(3) = New SqlParameter("@TimeFrameEnd", SqlDbType.VarChar, 100)
                    Params(3).Value = TimeTo
                    Params(4) = New SqlParameter("@AttachDoc", SqlDbType.VarBinary)
                    Params(4).Value = AttachImg
                    Params(5) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                    Params(5).Value = ContentType
                    Params(6) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                    Params(6).Value = fileName
                    Params(7) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(7).Value = CInt(Session("UserID"))
                    Params(8) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(8).Direction = ParameterDirection.Output
                    Params(9) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(9).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_HD_CHANGE_IMPLEMENTATION", Params)
                    ErrorFlag = CInt(Params(8).Value)
                    Message = CStr(Params(9).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_script1.Append("         alert('" + Message + "');")
                cl_script1.Append("         window.open('../home.aspx','_self');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            Else
                Dim cl_script0 As New System.Text.StringBuilder
                cl_script0.Append("         alert('Uploaded Only ZIP/RAR file');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
                Exit Sub
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
End Class
