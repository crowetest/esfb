﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="UpdateTicket.aspx.vb" Inherits="UpdateTicket" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">return window_onload()</script>
        <script language="javascript" type="text/javascript">


            function RequestOnchange() {
                if (document.getElementById("<%= cmbRequest.ClientID %>").value != -1) {
                    var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                    document.getElementById("<%= txtBranch.ClientID %>").value = Dtl[2];
                    document.getElementById("<%= txtDep.ClientID %>").value = Dtl[5];
                    document.getElementById("<%= txtDetails.ClientID %>").innerHTML = Dtl[6] + "&nbsp;&nbsp;<a href='Reports/viewTicketDetails.aspx?RequestID=" + btoa(Dtl[0]) + "' style='text-align:right; float:right' target='_blank' >Details</a>&nbsp;&nbsp;&nbsp;&nbsp;";
                    if (document.getElementById("<%= hid_TeamID.ClientID %>").value == 4) {
                        document.getElementById("<%= cmbClass.ClientID %>").value = Dtl[3];
                    }
                    if (Dtl[12] == 1) {
                        document.getElementById("<%= cmbStatus.ClientID %>").value = 2;
                    }
                    else {
                        document.getElementById("<%= cmbStatus.ClientID %>").value = Dtl[12];
                    }

                    document.getElementById("<%= cmbHover.ClientID %>").value = Dtl[7];
                    document.getElementById("<%= txtStartDt.ClientID %>").value = Dtl[8];
                    document.getElementById("<%= hid_GroupID.ClientID %>").value = Dtl[11];
                    document.getElementById("<%= hid_SubID.ClientID %>").value = Dtl[10];
                    document.getElementById("<%= cmbItem.ClientID %>").value = Dtl[10];

                    document.getElementById("<%= hid_ProblemID.ClientID %>").value = Dtl[9];
                    document.getElementById("<%= cmbCategory.ClientID %>").value = Dtl[11];
                    if (Dtl[14] != 0) {
                        document.getElementById("<%= hid_ProblemID.ClientID %>").value = Dtl[9] + '~1~' + Dtl[20] + '~' + Dtl[21] + '~' + Dtl[22];
                        document.getElementById("<%= cmbProblem.ClientID %>").value = Dtl[9] + '~1~' + Dtl[20] + '~' + Dtl[21] + '~' + Dtl[22];
                    }
                    else {
                        document.getElementById("<%= hid_ProblemID.ClientID %>").value = Dtl[9] + '~0~' + Dtl[20] + '~' + Dtl[21] + '~' + Dtl[22];
                        document.getElementById("<%= cmbProblem.ClientID %>").value = Dtl[9] + '~0~' + Dtl[20] + '~' + Dtl[21] + '~' + Dtl[22];
                    }

                    if (Dtl[21] != "")
                        document.getElementById("<%= cmbProblem.ClientID %>").title = Dtl[21];

                    document.getElementById("<%= txtCorrectionCnt.ClientID %>").value = Dtl[23];
                    if (Dtl[23] == 1)
                        document.getElementById("<%= txtCorrectedCnt.ClientID %>").value = Dtl[23];

                    if (Dtl[20] == 1)
                        document.getElementById("<%= lblCritical.ClientID %>").innerHTML = 'Critical';
                    else
                        document.getElementById("<%= lblCritical.ClientID %>").innerHTML = '';

                    if (Dtl[22] == 1)
                        document.getElementById("<%= btnView.ClientID %>").style.display = '';
                    else
                        document.getElementById("<%= btnView.ClientID %>").style.display = "none";

                    if (Dtl[19] == "0") {
                        document.getElementById("<%= cmd_view.ClientID %>").style.display = 'none';
                        document.getElementById("<%= hlEditAttachment.ClientID %>").style.display = 'none';
                    }
                    else {
                        document.getElementById("<%= cmd_view.ClientID %>").style.display = '';
                        document.getElementById("<%= hlEditAttachment.ClientID %>").style.display = '';
                    }
                    if (Dtl[13] == "0") {
                        document.getElementById("TrVendor").style.display = "none";
                    }
                    else {
                        document.getElementById("<%= cmbHover.ClientID %>").value = 1000;
                        document.getElementById("TrVendor").style.display = '';
                        document.getElementById("<%= cmbVendor.ClientID %>").value = Dtl[13];
                    }
                    if (Dtl[14] != "0") {
                        document.getElementById("Asset").style.display = '';
                        document.getElementById("<%= cmbAssetStatus.ClientID %>").value = Dtl[14];
                        if (Dtl[14] == "1") { //new
                            document.getElementById("AssetCount").style.display = '';
                            document.getElementById("AssetTag").style.display = 'none';
                            document.getElementById("<%= txtCount.ClientID %>").value = Dtl[15];
                        }
                        else if (Dtl[14] == "2") { //repair
                            document.getElementById("AssetTag").style.display = '';
                            document.getElementById("AssetCount").style.display = 'none';
                            document.getElementById("<%= txtAssetTAG.ClientID %>").value = Dtl[18];
                        }
                    }
                    else {
                        document.getElementById("Asset").style.display = 'none';
                        document.getElementById("AssetTag").style.display = 'none';
                        document.getElementById("AssetCount").style.display = 'none';
                    }
                    document.getElementById("<%= pnlPreviousRemarks.ClientID %>").style.display = 'none';
                }
                else {
                    document.getElementById("<%= cmd_view.ClientID %>").style.display = "none";
                    document.getElementById("<%= hlEditAttachment.ClientID %>").style.display = "none";
                }
            }

            function CategoryOnChange() {
                ClearCombo("<%= cmbItem.ClientID %>");
                ClearCombo("<%= cmbProblem.ClientID %>");
                var Groupid = document.getElementById("<%= cmbCategory.ClientID %>").value;
                if (Groupid > 0) {
                    var ToData = "2Ø" + Groupid;
                    ToServer(ToData, 2);
                }
            }
            function ItemOnChange() {
                var SubGroupid = document.getElementById("<%= cmbItem.ClientID %>").value;
                if (SubGroupid > 0) {
                    var ToData = "3Ø" + SubGroupid;
                    ToServer(ToData, 3);
                }
            }
            function StatusOnChange() {
                if (document.getElementById("<%= cmbStatus.ClientID %>").value == 4) {
                    document.getElementById("<%= cmbHover.ClientID %>").value = -1;
                    document.getElementById("<%= cmbHover.ClientID %>").disabled = true;
                }
                else if (document.getElementById("<%= cmbStatus.ClientID %>").value == 19) {
                    document.getElementById("<%= cmbHover.ClientID %>").value = -1;
                    document.getElementById("<%= cmbHover.ClientID %>").disabled = true;
                }
                else {
                    document.getElementById("<%= cmbHover.ClientID %>").disabled = false;
                }
            }
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ñ");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function ClearCombo(control) {
                document.getElementById(control).options.length = 0;
                var option1 = document.createElement("OPTION");
                option1.value = -1;
                option1.text = " -----Select-----";
                document.getElementById(control).add(option1);
            }
            function FromServer(arg, context) {
                if (context == 1) {
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == 0) window.open("AssignTicket.aspx?FilterID=" + btoa(document.getElementById("<%= hdnFilterID.ClientID %>").value) + " &FilterText=" + btoa(document.getElementById("<%= hdnFilterText.ClientID %>").value) + "", "_self");
                }
                else if (context == 2) {
                    ComboFill(arg, "<%= cmbItem.ClientID %>");
                    if (document.getElementById("<%= cmbCategory.ClientID %>").value == document.getElementById("<%= hid_GroupID.ClientID %>").value) {
                        document.getElementById("<%= cmbItem.ClientID %>").value = document.getElementById("<%= hid_SubID.ClientID %>").value;
                        ItemOnChange();
                    }
                }
                else if (context == 3) {
                    ComboFill(arg, "<%= cmbProblem.ClientID %>");
                    if (document.getElementById("<%= cmbItem.ClientID %>").value == document.getElementById("<%= hid_SubID.ClientID %>").value) {
                        document.getElementById("<%= cmbProblem.ClientID %>").value = document.getElementById("<%= hid_ProblemID.ClientID %>").value;
                    }
                }
                else if (context == 4) {
                    document.getElementById("<%= hid_pnl.ClientID %>").value = arg;
                    FillData();
                }
                else if (context == 5) {
                    if (arg == 0) {
                        alert("Please Enter a Valid Asset Tag");
                        document.getElementById("<%= txtAssetTAG.ClientID %>").value = "";
                        document.getElementById("<%= txtAssetTAG.ClientID %>").focus();
                        return false;
                    }
                }
            }
            function ProblemOnChange() {
                var ProblemDtl = document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");
                var ProblemID = ProblemDtl[0];
                document.getElementById("<%= hdnProblemID.ClientID %>").value = ProblemID;
                var AsssetStatus = ProblemDtl[1];
                var IsCritical = ProblemDtl[2];
                var Description = ProblemDtl[3];
                var Annexture = ProblemDtl[4];

                document.getElementById("<%= cmbProblem.ClientID %>").title = Description;
                if (IsCritical == 1)
                    document.getElementById("<%= lblCritical.ClientID %>").innerHTML = 'Critical';
                else
                    document.getElementById("<%= lblCritical.ClientID %>").innerHTML = '';

                if (Annexture == 1)
                    document.getElementById("<%= btnView.ClientID %>").style.display = '';
                else
                    document.getElementById("<%= btnView.ClientID %>").style.display = "none";

                if (AsssetStatus == 1) {
                    document.getElementById("Asset").style.display = "";
                    document.getElementById("<%= cmbAssetStatus.ClientID %>").value = -1;
                    document.getElementById("AssetTag").style.display = "none";
                    document.getElementById("AssetCount").style.display = "none";
                }
                else {
                    document.getElementById("Asset").style.display = "none";
                    document.getElementById("AssetTag").style.display = "none";
                    document.getElementById("AssetCount").style.display = "none";
                }
            }
            function AssetStatusOnChange() {
                var AssetStatus = document.getElementById("<%= cmbAssetStatus.ClientID %>").value;
                if (AssetStatus == 2) // Repaire
                {
                    document.getElementById("AssetTag").style.display = "";
                    document.getElementById("AssetCount").style.display = "none";
                    document.getElementById("ImgSoft").style.display = "";
                }
                else // New
                {
                    document.getElementById("AssetTag").style.display = "none";
                    document.getElementById("AssetCount").style.display = "";
                    document.getElementById("ImgSoft").style.display = "none";
                }
            }
            function AssetTagOnChange() {
                var AssetTag = document.getElementById("<%= txtAssetTAG.ClientID %>").value;
                var ItemID = document.getElementById("<%= cmbProblem.ClientID %>").value;

                if (AssetTag == "" || AssetTag == 0) {
                    alert("Please Enter a Valid Asset Tag");
                    document.getElementById("<%= txtAssetTAG.ClientID %>").focus();
                    return false;
                }
                var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                var Branch = Dtl[1];
                var Dep = Dtl[4];
                var ToData = "5Ø" + AssetTag + "Ø" + Branch + "Ø" + Dep + "Ø" + ItemID;
                ToServer(ToData, 5);
            }
            function btnExit_onclick() {
                window.open("AssignTicket.aspx?FilterID=" + btoa(document.getElementById("<%= hdnFilterID.ClientID %>").value) + " &FilterText=" + btoa(document.getElementById("<%= hdnFilterText.ClientID %>").value) + "", "_self")
            }

            function btnSave_onclick() {
                if (document.getElementById("<%= cmbRequest.ClientID %>").value == "-1") {
                    alert("Select Request");
                    document.getElementById("<%= cmbRequest.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= cmbCategory.ClientID %>").value == "-1") {
                    alert("Select Category");
                    document.getElementById("<%= cmbCategory.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= cmbItem.ClientID %>").value == "-1") {
                    alert("Select Sub Category");
                    document.getElementById("<%= cmbItem.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= cmbProblem.ClientID %>").value == "-1") {
                    alert("Select Findings");
                    document.getElementById("<%= cmbProblem.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= hid_TeamID.ClientID %>").value == 4) {
                    if (document.getElementById("<%= cmbClass.ClientID %>").value == "-1") {
                        alert("Select Classification");
                        document.getElementById("<%= cmbClass.ClientID %>").focus();
                        return false;
                    }
                }
                if (document.getElementById("<%= cmbStatus.ClientID %>").value == "-1") {
                    alert("Select Status");
                    document.getElementById("<%= cmbStatus.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= cmbStatus.ClientID %>").value == 9) {


                    if (document.getElementById("<%= cmbHover.ClientID %>").value == "-1") {
                        alert("Select Hand over Team");
                        document.getElementById("<%= cmbHover.ClientID %>").focus();
                        return false;
                    }
                }
                if (document.getElementById("txtRemarks").value == "") {
                    alert("Enter Remarks");
                    document.getElementById("txtRemarks").focus();
                    return false;
                }
                if (document.getElementById("<%= txtCorrectionCnt.ClientID %>").value != 0) {
                    if (document.getElementById("<%= txtCorrectedCnt.ClientID %>").value == 0 || document.getElementById("<%= txtCorrectedCnt.ClientID %>").value == "" || document.getElementById("<%= txtCorrectedCnt.ClientID %>").value == " ") {
                        alert("Enter Corrected Count");
                        document.getElementById("txtCorrectedCnt").focus();
                        return false;
                    }
                    else if (document.getElementById("<%= txtCorrectedCnt.ClientID %>").value > document.getElementById("<%= txtCorrectionCnt.ClientID %>").value) {
                        alert("Enter Corrected Count");
                        document.getElementById("txtCorrectedCnt").focus();
                        return false;
                    }
                }
                if (document.getElementById("<%= cmbRoot.ClientID %>").value == -1) {
                    alert("Select Root Cause");
                    document.getElementById("cmbRoot").focus();
                    return false;
                }
                var RootCause = document.getElementById("<%= cmbRoot.ClientID %>").value;
                var Cnt = document.getElementById("<%= txtCorrectedCnt.ClientID %>").value;
                var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                var RequestID = Dtl[0];

                var Branch = Dtl[1];
                var Dep = Dtl[4];
                var Category = document.getElementById("<%= cmbCategory.ClientID %>").value;
                var Module = document.getElementById("<%= cmbItem.ClientID %>").value;

                Dtl = document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");
                var Problem = Dtl[0];
                if (document.getElementById("<%= hid_TeamID.ClientID %>").value == 4) {
                    var Class = document.getElementById("<%= cmbClass.ClientID %>").value;
                }
                else {
                    var Class = 0;
                }

                var Hover = document.getElementById("<%= cmbHover.ClientID %>").value;
                if (Hover == 4)
                    var Status = 1;
                else
                    var Status = document.getElementById("<%= cmbStatus.ClientID %>").value;

                var Remarks = document.getElementById("txtRemarks").value;
                var VendorID = 0
                if (Hover == 1000) {
                    if (document.getElementById("<%= cmbVendor.ClientID %>").value == -1)
                    { alert("Select Vendor"); document.getElementById("<%= cmbVendor.ClientID %>").focus(); return false; }
                    else {
                        VendorID = document.getElementById("<%= cmbVendor.ClientID %>").value;
                    }
                }

                var ProblemDtl = document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");

                var AssetRelated = ProblemDtl[1];
                var AssetStatus = 0;
                var AssetTag = "";
                var AssetCount = 0;

                if (document.getElementById("<%= cmbHover.ClientID %>").value == "9" && AssetRelated != 1) {
                    alert("Hand over team is not correct ");
                    document.getElementById("<%= cmbHover.ClientID %>").focus();
                    return false;
                }

                if (AssetRelated == 1) {
                    AssetStatus = document.getElementById("<%= cmbAssetStatus.ClientID %>").value;
                    if (AssetStatus == 2) {
                        AssetTag = document.getElementById("<%= txtAssetTAG.ClientID %>").value;
                        if (AssetTag == "") {
                            alert("Enter Asset TAG");
                            document.getElementById("<%= txtAssetTAG.ClientID %>").focus();
                            return false;
                        }
                    }
                    else {
                        if (document.getElementById("<%= txtCount.ClientID %>").value == "" || document.getElementById("<%= txtCount.ClientID %>").value == 0) {
                            alert("Please Enter No Of Assets");
                            document.getElementById("<%= txtCount.ClientID %>").focus();
                            return false;
                        }
                        AssetCount = document.getElementById("<%= txtCount.ClientID %>").value;
                    }
                }

                var ToData = "1Ø" + Branch + "Ø" + Dep + "Ø" + Category + "Ø" + Module + "Ø" + Problem + "Ø" + Class + "Ø" + Status + "Ø" + Hover + "Ø" + Remarks + "Ø" + document.getElementById("<%= hid_TeamID.ClientID %>").value + "Ø" + RequestID + "Ø" + VendorID + "Ø" + AssetStatus + "Ø" + AssetTag + "Ø" + AssetCount + "Ø" + Cnt + "Ø" + RootCause;

                ToServer(ToData, 1);
            }
            function FillData() {
                if (document.getElementById("<%= hid_pnl.ClientID %>").value == "") {
                    document.getElementById("<%= pnlPreviousRemarks.ClientID %>").innerHTML = "";
                    document.getElementById("<%= pnlPreviousRemarks.ClientID %>").style.display = "none";
                    return;
                }
                document.getElementById("<%= pnlPreviousRemarks.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";

                tab += "<div style='margin: 0px auto; width:80%;border-color:#C0D0E5; border-style:solid;'>";
                tab += "<div style='text-align:center;width:100%; height:18px; margin: 0px auto;'  align='center' class=mainhead><span style='color:#214263;'>PREVIOUS REMARKS</span></div>"
                tab += "<div style='width:100%; height:170px;background-color:#C0D0E5; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
                tab += "<tr class=mainhead>";
                tab += "<td style='width:5%;text-align:center'>Sl No</td>";
                tab += "<td style='width:10%;text-align:center' >Date</td>";
                tab += "<td style='width:15%;text-align:center' >Category</td>";
                tab += "<td style='width:15%;text-align:center'>Sub Category</td>";
                tab += "<td style='width:25%;text-align:center'>Findings</td>";
                tab += "<td style='width:10%;text-align:center'>Team</td>";
                tab += "<td style='width:20%;text-align:center'>Remarks</td>";

                tab += "</tr>";

                row = document.getElementById("<%= hid_pnl.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;

                    //                if (col[2]=='Branch/Department'){
                    //                    tab += "<td style='width:5%;text-align:center;color:Red'>" + i + "</td>";
                    //                    tab += "<td style='width:10%;text-align:center;color:Red' >" + col[6] + "</td>";
                    //                    tab += "<td style='width:15%;text-align:left;color:Red' >" + col[5] + "</td>";
                    //                    tab += "<td style='width:15%;text-align:center;color:Red'>" + col[4] + "</td>";
                    //                    tab += "<td style='width:25%;text-align:center;color:Red'>" + col[3] + "</td>";
                    //                    tab += "<td style='width:10%;text-align:center;color:Red'>" + col[2] + "</td>";
                    //                    tab += "<td style='width:20%;text-align:left;color:Red'>" + col[1] + "</td>";
                    //                }
                    //               
                    //                else {

                    tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                    tab += "<td style='width:10%;text-align:center' >" + col[6] + "</td>";
                    tab += "<td style='width:15%;text-align:center' >" + col[5] + "</td>";
                    tab += "<td style='width:15%;text-align:center'>" + col[4] + "</td>";
                    tab += "<td style='width:25%;text-align:center'>" + col[3] + "</td>";
                    tab += "<td style='width:10%;text-align:center'>" + col[2] + "</td>";
                    tab += "<td style='width:20%;text-align:left'>" + col[1] + "</td>";

                    //               }


                    tab += "</tr>";
                }
                tab += "</table><div><div>";
                //alert(tab);
                document.getElementById("<%= pnlPreviousRemarks.ClientID %>").innerHTML = tab;

            }
            function ViewRemarks() {

                if (document.getElementById("<%= cmbRequest.ClientID %>").value != "-1") {

                    var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                    var ToData = "4Ø" + Dtl[0];
                    ToServer(ToData, 4);


                }

            }
            function viewattachment() {

                var ReqDtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                var RequestID = ReqDtl[0];
                if (ReqDtl[14] == 1)
                    window.open("Reports/ShowAttachment.aspx?RequestID=" + btoa(RequestID), "_blank");
                else
                    window.open("Reports/ShowMultipleAttachments.aspx?RequestID=" + btoa(RequestID), "_blank");
                return false;
            }


            function window_onload() {
                document.getElementById("Asset").style.display = "none";
                document.getElementById("AssetTag").style.display = "none";
                document.getElementById("AssetCount").style.display = "none";
                document.getElementById("<%= cmd_view.ClientID %>").style.display = "none";
                document.getElementById("<%= hlEditAttachment.ClientID %>").style.display = "none";
                if (document.getElementById("<%= hid_TeamID.ClientID %>").value != 4) {
                    document.getElementById("Classi").style.display = 'none';
                }
                else {
                    document.getElementById("Classi").style.display = '';
                }
                RequestOnchange();
            }

            function HandOverOnChange() {
                if (document.getElementById("<%= cmbHover.ClientID %>").value == 1000) {
                    document.getElementById("TrVendor").style.display = ''
                }
                else {
                    document.getElementById("TrVendor").style.display = 'none'
                }

            }
            function EditOnClick() {
                var ReqDtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                var RequestID = ReqDtl[0];

                if (RequestID != -1) {
                    window.open("EditAttachment.aspx?RequestID=" + btoa(RequestID) + " ")
                }
            }
            function viewattachment() {
                var ReqDtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                var RequestID = ReqDtl[0];
                if (RequestID != -1) {
                    var AttachCount = ReqDtl[13];
                    if (AttachCount == "1")
                        window.open("Reports/ShowAttachment.aspx?RequestID=" + btoa(RequestID));
                    else
                        window.open("Reports/ShowMultipleAttachments.aspx?RequestID=" + btoa(RequestID) + " &RptID=2");
                }
                return false;
            }
            function ViewLicense() {

            }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    </html>
    <%--<div class="ScrollClass">
        <b>Help Desk Contact Numbers -</b> 08589975581, 08589975582, 08589975583, 08589975584<hr
            style="color: #E31E24; margin-top: 0px;" />
    </div>--%>
    <br />
    <br />
    <table class="style1" style="width: 80%; margin: 0px auto;">
        <tr id="branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 63%">
                <asp:HiddenField ID="cmbRequest" runat="server" />
            </td>
        </tr>
        <tr id="Tr3">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch
            </td>
            <td style="width: 63%">
                <asp:TextBox ID="txtBranch" runat="server" Width="50%" class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr id="Department">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Department
            </td>
            <td style="width: 63%">
                <asp:TextBox ID="txtDep" runat="server" Width="50%" class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Request Date
            </td>
            <td style="width: 63%">
                <asp:TextBox ID="txtStartDt" class="ReadOnlyTextBox" runat="server" Width="20%" onkeypress="return false"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="txtStartDt" Format="dd MMM yyyy">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
        <tr id="Category">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Category
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbCategory" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Module">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_TeamID" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Sub Category
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbItem" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Problem">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_ProblemID" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Findings
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbProblem" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:Label ID="lblCritical" runat="server" Font-Bold="True" ForeColor="#CC0000"></asp:Label>
                <asp:ImageButton ID="btnView" runat="server" Height="18px" Width="18px" ImageAlign="AbsMiddle"
                    ImageUrl="~/Image/attchment2.png" ToolTip="View Annexture" />
            </td>
        </tr>
        <tr id="Asset">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Asset Status
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbAssetStatus" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="2"> Repair </asp:ListItem>
                    <asp:ListItem Value="1"> New</asp:ListItem>
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="ImgSoft" runat="server" Height="23px" Width="23px" ImageAlign="AbsMiddle"
                    ImageUrl="~/Image/refresh1.png" Visible="false" ToolTip="Add/Remove Softwares" />
            </td>
        </tr>
        <tr id="AssetTag">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Asset TAG
            </td>
            <td style="width: 63%">
                <asp:TextBox ID="txtAssetTAG" runat="server" Style="font-family: Cambria; font-size: 10pt;"
                    Width="50%" class="NormalText" MaxLength="50" />
            </td>
        </tr>
        <tr id="AssetCount">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Count
            </td>
            <td style="width: 63%">
                <asp:TextBox ID="txtCount" runat="server" Style="font-family: Cambria; font-size: 10pt;"
                    Width="50%" class="NormalText" MaxLength="4" onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="Tr1">
            <td style="width: 25%;">
                &nbsp;</td>
            <td style="width: 12%; text-align: left;">
                Correction Count</td>
            <td style="width: 63%">
                <asp:TextBox ID="txtCorrectionCnt" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="4" readonly=true
                    onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="Tr1">
            <td style="width: 25%;">
                &nbsp;</td>
            <td style="width: 12%; text-align: left;">
                Corrected Count</td>
            <td style="width: 63%">
                <asp:TextBox ID="txtCorrectedCnt" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="4" 
                    onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="Tr1">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_SubID" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Previous Remarks
            </td>
            <td style="width: 63%">
                <asp:Panel ID="txtDetails" runat="server" Style="width: 80%; border: 1px solid silver;
                    min-height: 60px;">
                </asp:Panel>
            </td>
        </tr>
        <tr id="Classi">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_GroupID" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Classification
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbClass" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ----------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Tr5">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_pnl" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                <asp:Label ID="lblStatus" runat="server" Text="Label"></asp:Label>
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbStatus" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ----------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Tr4">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_show" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Hand Over To
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbHover" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ----------Select---------</asp:ListItem>
                </asp:DropDownList>
                &nbsp; &nbsp;&nbsp;
                <asp:ImageButton ID="cmd_view" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle"
                    ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" />
                &nbsp;&nbsp;<asp:HyperLink ID="hlEditAttachment" runat="server" onclick="EditOnClick()"
                    Style="color: Blue; cursor: pointer;" Font-Size="11pt" Font-Underline="True">Edit Attachment</asp:HyperLink>
            </td>
        </tr>
        <tr id="TrVendor" style="display: none;">
            <td style="width: 25%;">
                <asp:HiddenField ID="hdnFilterID" runat="server" />
                <asp:HiddenField ID="hdnFilterText" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Vendor
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbVendor" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ----------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                &nbsp;</td>
            <td style="width: 12%; text-align: left;">
                Root Cause
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbRoot" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ----------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Tr2">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_ReportID" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
            </td>
            <td style="width: 63%">
                <textarea id="txtRemarks" cols="20" name="S1" rows="3" maxlength="1000" style="width: 80%" onkeypress='return TextAreaCheck(event)' 
                    class="NormalText"></textarea><span style="color: Blue; cursor: pointer;" onclick="ViewRemarks()">
                    </span>
            </td>
        </tr>
        <tr id="Remarks">
            <td style="width: 25%;" colspan="3">
                <asp:Panel ID="pnlPreviousRemarks" runat="server" Style="display: none;">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="SAVE" onclick="return btnSave_onclick()" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnProblemID" runat="server" />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
