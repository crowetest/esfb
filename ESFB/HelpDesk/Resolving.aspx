﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="Resolving.aspx.vb" Inherits="Resolving" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">return window_onload()</script>
        <script language="javascript" type="text/javascript">

            function window_onload() {
                document.getElementById("Asset").style.display = "none";
                document.getElementById("AssetTag").style.display = "none";
                document.getElementById("AssetCount").style.display = "none";
                document.getElementById("<%= cmd_view.ClientID %>").style.display = "none";
                document.getElementById("<%= btnView.ClientID %>").style.display = "none";

                if (document.getElementById("<%= hid_TeamID.ClientID %>").value != 4) {
                    document.getElementById("Classi").style.display = 'none';
                }
                else {
                    document.getElementById("Classi").style.display = '';
                    RequestOnchange();
                }
            }
            function RequestOnchange() {
                if (document.getElementById("<%= cmbRequest.ClientID %>").value != -1) {
                    var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                    document.getElementById("<%= txtBranch.ClientID %>").value = Dtl[2];
                    document.getElementById("<%= txtDep.ClientID %>").value = Dtl[5];
                    document.getElementById("<%= txtDetails.ClientID %>").innerHTML = Dtl[6] + "&nbsp;&nbsp;<a href='Reports/viewTicketDetails.aspx?RequestID=" + btoa(Dtl[0]) + "' style='text-align:right; float:right' target='_blank' >Details</a>&nbsp;&nbsp;&nbsp;&nbsp;";
                    if (document.getElementById("<%= hid_TeamID.ClientID %>").value == 4) {
                        document.getElementById("<%= cmbClass.ClientID %>").value = Dtl[3];
                    }

                    if (Dtl[12] == 1)
                    { document.getElementById("<%= cmbStatus.ClientID %>").value = 2; }
                    else
                    { document.getElementById("<%= cmbStatus.ClientID %>").value = Dtl[12]; }

                    document.getElementById("<%= cmbHover.ClientID %>").value = Dtl[7];
                    document.getElementById("<%= txtStartDt.ClientID %>").value = Dtl[8];
                    document.getElementById("<%= hid_GroupID.ClientID %>").value = Dtl[11];
                    document.getElementById("<%= hid_SubID.ClientID %>").value = Dtl[10];
                    document.getElementById("<%= cmbItem.ClientID %>").value = Dtl[10];
                    if (Dtl[14] != 0) {
                        document.getElementById("<%= hid_ProblemID.ClientID %>").value = Dtl[9] + '~1~' + Dtl[17] + '~' + Dtl[18] + '~' + Dtl[19];
                        document.getElementById("<%= cmbProblem.ClientID %>").value = Dtl[9] + '~1~' + Dtl[17] + '~' + Dtl[18] + '~' + Dtl[19];
                    }
                    else {
                        document.getElementById("<%= hid_ProblemID.ClientID %>").value = Dtl[9] + '~0~' + Dtl[17] + '~' + Dtl[18] + '~' + Dtl[19];
                        document.getElementById("<%= cmbProblem.ClientID %>").value = Dtl[9] + '~0~' + Dtl[17] + '~' + Dtl[18] + '~' + Dtl[19];
                    }

                    if (Dtl[18] != "")
                        document.getElementById("<%= cmbProblem.ClientID %>").title = Dtl[18];

                    if (Dtl[17] == 1)
                        document.getElementById("<%= lblCritical.ClientID %>").innerHTML = 'Critical';
                    else
                        document.getElementById("<%= lblCritical.ClientID %>").innerHTML = '';

                    if (Dtl[19] == 1)
                        document.getElementById("<%= btnView.ClientID %>").style.display = '';
                    else
                        document.getElementById("<%= btnView.ClientID %>").style.display = "none";

                    document.getElementById("<%= cmbCategory.ClientID %>").value = Dtl[11];

                    if (Dtl[13] == "0")
                    { document.getElementById("<%= cmd_view.ClientID %>").style.display = "none"; }
                    else
                    { document.getElementById("<%= cmd_view.ClientID %>").style.display = ''; }


                    if (Dtl[14] != "0") {
                        document.getElementById("Asset").style.display = '';
                        document.getElementById("<%= cmbAssetStatus.ClientID %>").value = Dtl[14];
                        if (Dtl[14] == "1") { //new
                            document.getElementById("AssetCount").style.display = '';
                            document.getElementById("AssetTag").style.display = 'none';
                            document.getElementById("<%= txtCount.ClientID %>").value = Dtl[15];
                        }
                        else if (Dtl[14] == "2") { //repair
                            document.getElementById("AssetTag").style.display = '';
                            document.getElementById("AssetCount").style.display = 'none';
                            document.getElementById("<%= txtAssetTAG.ClientID %>").value = Dtl[16];
                        }
                    }
                    else {
                        document.getElementById("Asset").style.display = 'none';
                        document.getElementById("AssetTag").style.display = 'none';
                        document.getElementById("AssetCount").style.display = 'none';
                    }

                    document.getElementById("<%= pnlPreviousRemarks.ClientID %>").style.display = 'none';
                    //CategoryOnChange();
                }
                else
                    document.getElementById("<%= cmd_view.ClientID %>").style.display = "none";
            }

            function CategoryOnChange() {
                ClearCombo("<%= cmbItem.ClientID %>");
                ClearCombo("<%= cmbProblem.ClientID %>");
                var Groupid = document.getElementById("<%= cmbCategory.ClientID %>").value;
                if (Groupid > 0) {
                    var ToData = "2Ø" + Groupid;
                    ToServer(ToData, 2);
                }

            }
            function ItemOnChange() {
                var SubGroupid = document.getElementById("<%= cmbItem.ClientID %>").value;
                if (SubGroupid > 0) {
                    var ToData = "3Ø" + SubGroupid;
                    ToServer(ToData, 3);
                }
            }
            function StatusOnChange() {
                var Status = document.getElementById("<%= cmbStatus.ClientID %>").value;
                if (Status == 4 || Status == 12) {
                    document.getElementById("<%= cmbHover.ClientID %>").value = -1;
                    document.getElementById("<%= cmbHover.ClientID %>").disabled = true;
                }
                else {
                    document.getElementById("<%= cmbHover.ClientID %>").disabled = false;
                }
            }
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ñ");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function ClearCombo(control) {
                document.getElementById(control).options.length = 0;
                var option1 = document.createElement("OPTION");
                option1.value = -1;
                option1.text = " -----Select-----";
                document.getElementById(control).add(option1);
            }
            function FromServer(arg, context) {
                if (context == 1) {
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == 0) {
                        if (window.opener != null && !window.opener.closed) {
                            window.opener.location.reload();
                            window.close();
                        }
                        window.onbeforeunload = RefreshParent;
                    }
                }
                else if (context == 2) {
                    ComboFill(arg, "<%= cmbItem.ClientID %>");
                    if (document.getElementById("<%= cmbCategory.ClientID %>").value == document.getElementById("<%= hid_GroupID.ClientID %>").value) {
                        document.getElementById("<%= cmbItem.ClientID %>").value = document.getElementById("<%= hid_SubID.ClientID %>").value
                        ItemOnChange();
                    }

                }
                else if (context == 3) {
                    ComboFill(arg, "<%= cmbProblem.ClientID %>");
                    if (document.getElementById("<%= cmbItem.ClientID %>").value == document.getElementById("<%= hid_SubID.ClientID %>").value) {
                        document.getElementById("<%= cmbProblem.ClientID %>").value = document.getElementById("<%= hid_ProblemID.ClientID %>").value;
                    }

                }
                else if (context == 4) {
                    document.getElementById("<%= hid_pnl.ClientID %>").value = arg;
                    FillData();
                }                
                else if (context == 6) {
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == 0) {
                        if (window.opener != null && !window.opener.closed) {
                            window.opener.location.reload();
                            window.close();
                        }
                        window.onbeforeunload = RefreshParent;
                    }
                }
                else if (context == 8) {
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == 0) {
                        if (window.opener != null && !window.opener.closed) {
                            window.opener.location.reload();
                            window.close();
                        }
                        window.onbeforeunload = RefreshParent;
                    }
                }
            }
            function btnExit_onclick() {
                if (window.opener != null && !window.opener.closed) {
                    window.opener.location.reload();
                    window.close();
                }
                window.onbeforeunload = RefreshParent;
            }

            function btnSave_onclick() {
                if (document.getElementById("<%= cmbRequest.ClientID %>").value == "-1") {
                    alert("Select Request");
                    document.getElementById("<%= cmbRequest.ClientID %>").focus();
                    return false;
                }

                if (document.getElementById("<%= cmbCategory.ClientID %>").value == "-1") {
                    alert("Select Category");
                    document.getElementById("<%= cmbCategory.ClientID %>").focus();
                    return false;
                }

                if (document.getElementById("<%= cmbItem.ClientID %>").value == "-1") {
                    alert("Select Sub Category");
                    document.getElementById("<%= cmbItem.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= cmbProblem.ClientID %>").value == "-1") {
                    alert("Select Findings");
                    document.getElementById("<%= cmbProblem.ClientID %>").focus();
                    return false;
                }

                if (document.getElementById("<%= hid_TeamID.ClientID %>").value == 4) {
                    if (document.getElementById("<%= cmbClass.ClientID %>").value == "-1") {
                        alert("Select Classification");
                        document.getElementById("<%= cmbClass.ClientID %>").focus();
                        return false;
                    }
                }
                if (document.getElementById("<%= cmbStatus.ClientID %>").value == "-1") {
                    alert("Select Status");
                    document.getElementById("<%= cmbStatus.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= cmbStatus.ClientID %>").value == 9) {


                    if (document.getElementById("<%= cmbHover.ClientID %>").value == "-1") {
                        alert("Select Hand over Team");
                        document.getElementById("<%= cmbHover.ClientID %>").focus();
                        return false;
                    }
                }


                if (document.getElementById("txtRemarks").value == "") {

                    if (document.getElementById("<%= cmbStatus.ClientID %>").value == 4) {
                        alert("Enter Remarks");
                        document.getElementById("txtRemarks").focus();
                        return false;


                    }
                }

                var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                var RequestID = Dtl[0];

                var Branch = Dtl[1];
                var Dep = Dtl[4];
                var Category = document.getElementById("<%= cmbCategory.ClientID %>").value;
                var Module = document.getElementById("<%= cmbItem.ClientID %>").value;
                var Problem = document.getElementById("<%= cmbProblem.ClientID %>").value;
                if (document.getElementById("<%= hid_TeamID.ClientID %>").value == 4) {
                    var Class = document.getElementById("<%= cmbClass.ClientID %>").value;
                }
                else {
                    var Class = 0;
                }
                var Status = document.getElementById("<%= cmbStatus.ClientID %>").value;
                var Hover = document.getElementById("<%= cmbHover.ClientID %>").value;
                var Remarks = document.getElementById("txtRemarks").value;
                var Emergency = (document.getElementById("chkPriority").checked == true) ? 1 : 0;
                var VendorID = 0;
                if (Hover == 1000) {
                    if (document.getElementById("<%= cmbVendor.ClientID %>").value == -1)
                    { alert("Select Vendor"); document.getElementById("<%= cmbVendor.ClientID %>").focus(); return false; }
                    else {
                        VendorID = document.getElementById("<%= cmbVendor.ClientID %>").value;
                    }
                }



                var ProblemDtl = document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");

                var AssetRelated = ProblemDtl[1];
                var AssetStatus = 0;
                var AssetTag = "";
                var AssetCount = 0;

                if (document.getElementById("<%= cmbHover.ClientID %>").value == "9" && AssetRelated != 1) {
                    alert("Hand over team is not correct ");
                    document.getElementById("<%= cmbHover.ClientID %>").focus();
                    return false;
                }

                if (AssetRelated == 1) {
                    AssetStatus = document.getElementById("<%= cmbAssetStatus.ClientID %>").value;
                    if (AssetStatus == 2) {
                        AssetTag = document.getElementById("<%= txtAssetTAG.ClientID %>").value;
                        if (AssetTag == "") {
                            alert("Enter Asset TAG");
                            document.getElementById("<%= txtAssetTAG.ClientID %>").focus();
                            return false;
                        }
                    }
                    else {
                        if (document.getElementById("<%= txtCount.ClientID %>").value == "" || document.getElementById("<%= txtCount.ClientID %>").value == 0) {
                            alert("Please Enter No Of Assets");
                            document.getElementById("<%= txtCount.ClientID %>").focus();
                            return false;
                        }
                        AssetCount = document.getElementById("<%= txtCount.ClientID %>").value;
                    }
                }

                document.getElementById("<%= hdnSave.ClientID %>").value = "1Ø" + Branch + "Ø" + Dep + "Ø" + Category + "Ø" + Module + "Ø" + ProblemDtl[0] + "Ø" + Class + "Ø" + Status + "Ø" + Hover + "Ø" + Remarks + "Ø" + document.getElementById("<%= hid_TeamID.ClientID %>").value + "Ø" + RequestID + "Ø" + VendorID + "Ø" + Emergency + "Ø" + AssetStatus + "Ø" + AssetTag + "Ø" + AssetCount;

                //ToServer(ToData, 1);
            }


            function FillData() {
                if (document.getElementById("<%= hid_pnl.ClientID %>").value == "") {
                    document.getElementById("<%= pnlPreviousRemarks.ClientID %>").innerHTML = "";
                    document.getElementById("<%= pnlPreviousRemarks.ClientID %>").style.display = "none";
                    return;
                }
                document.getElementById("<%= pnlPreviousRemarks.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";

                tab += "<div style='margin: 0px auto; width:80%;border-color:#C0D0E5; border-style:solid;'>";
                tab += "<div style='text-align:center;width:100%; height:18px; margin: 0px auto;'  align='center' class=mainhead><span style='color:#214263;'>PREVIOUS REMARKS</span></div>"
                tab += "<div style='width:100%; height:170px;background-color:#C0D0E5; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
                tab += "<tr class=mainhead>";
                tab += "<td style='width:5%;text-align:center'>Sl No</td>";
                tab += "<td style='width:10%;text-align:center' >Date</td>";
                tab += "<td style='width:15%;text-align:center' >Category</td>";
                tab += "<td style='width:15%;text-align:center'>Sub Category</td>";
                tab += "<td style='width:25%;text-align:center'>Findings</td>";
                tab += "<td style='width:10%;text-align:center'>Team</td>";
                tab += "<td style='width:20%;text-align:center'>Remarks</td>";

                tab += "</tr>";

                row = document.getElementById("<%= hid_pnl.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;

                    //                if (col[2]=='Branch/Department'){
                    //                    tab += "<td style='width:5%;text-align:center;color:Red'>" + i + "</td>";
                    //                    tab += "<td style='width:10%;text-align:center;color:Red' >" + col[6] + "</td>";
                    //                    tab += "<td style='width:15%;text-align:left;color:Red' >" + col[5] + "</td>";
                    //                    tab += "<td style='width:15%;text-align:center;color:Red'>" + col[4] + "</td>";
                    //                    tab += "<td style='width:25%;text-align:center;color:Red'>" + col[3] + "</td>";
                    //                    tab += "<td style='width:10%;text-align:center;color:Red'>" + col[2] + "</td>";
                    //                    tab += "<td style='width:20%;text-align:left;color:Red'>" + col[1] + "</td>";
                    //                }
                    //               
                    //                else {

                    tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                    tab += "<td style='width:10%;text-align:center' >" + col[6] + "</td>";
                    tab += "<td style='width:15%;text-align:center' >" + col[5] + "</td>";
                    tab += "<td style='width:15%;text-align:center'>" + col[4] + "</td>";
                    tab += "<td style='width:25%;text-align:center'>" + col[3] + "</td>";
                    tab += "<td style='width:10%;text-align:center'>" + col[2] + "</td>";
                    tab += "<td style='width:20%;text-align:left'>" + col[1] + "</td>";

                    //               }


                    tab += "</tr>";
                }
                tab += "</table><div><div>";
                //alert(tab);
                document.getElementById("<%= pnlPreviousRemarks.ClientID %>").innerHTML = tab;

            }
            function ViewRemarks() {

                if (document.getElementById("<%= cmbRequest.ClientID %>").value != "-1") {

                    var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                    var ToData = "4Ø" + Dtl[0];
                    ToServer(ToData, 4);


                }

            }
            function viewattachment() {
                var ReqDtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                var RequestID = ReqDtl[0];
                if (RequestID != -1) {
                    var AttachCount = ReqDtl[13];
                    if (AttachCount == "1")
                        window.open("Reports/ShowAttachment.aspx?RequestID=" + btoa(RequestID));
                    else
                        window.open("Reports/ShowMultipleAttachments.aspx?RequestID=" + btoa(RequestID) + " &RptID=2");
                }
                return false;
            }


            function HandOverOnChange() {
                if (document.getElementById("<%= cmbHover.ClientID %>").value == 1000) {
                    document.getElementById("TrVendor").style.display = ''
                }
                else {
                    document.getElementById("TrVendor").style.display = 'none'
                }

            }
            function btnReturn_onclick() {
                var RejectReason = document.getElementById("txtRemarks").value;
                if (document.getElementById("<%= cmbRequest.ClientID %>").value == "-1") {
                    alert("Select Request");
                    document.getElementById("<%= cmbRequest.ClientID %>").focus();
                    return false;
                }
                if (RejectReason == "") {
                    alert("Enter Remarks"); document.getElementById("txtRemarks").focus(); return false;
                }
                else {
                    if (confirm("Are you sure to return this ticket?") == true) {
                        var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                        var RequestID = Dtl[0];
                        var Category = document.getElementById("<%= cmbCategory.ClientID %>").value;
                        var Module = document.getElementById("<%= cmbItem.ClientID %>").value;
                        var Problem = document.getElementById("<%= cmbProblem.ClientID %>").value;
                        var ToData = "5Ø" + "¥" + RequestID + "µ0µ1µ" + RejectReason + "µ" + Problem + "Ø" + RequestID;
                        document.getElementById("<%= hdnReturn.ClientID %>").value = ToData;
                    }
                }
            }
            function btnCancel_onclick() {
                var RejectReason = document.getElementById("txtRemarks").value;
                if (document.getElementById("<%= cmbRequest.ClientID %>").value == "-1") {
                    alert("Select Request"); document.getElementById("<%= cmbRequest.ClientID %>").focus(); return false;
                }
                if (RejectReason == "") {
                    alert("Enter Remarks"); document.getElementById("txtRemarks").focus(); return false;
                }
                else {
                    if (confirm("Are you sure to Cancel this ticket?") == true) {
                        var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                        var RequestID = Dtl[0];
                        var ToData = "6Ø" + "¥" + RequestID + "µ0µ1µ" + RejectReason;

                        ToServer(ToData, 6);
                    }
                }
            }

            function btnHold_onclick() {
                var RejectReason = document.getElementById("txtRemarks").value;
                if (document.getElementById("<%= cmbRequest.ClientID %>").value == "-1") {
                    alert("Select Request");
                    document.getElementById("<%= cmbRequest.ClientID %>").focus();
                    return false;
                }
                if (RejectReason == "") {
                    alert("Enter Remarks"); document.getElementById("txtRemarks").focus(); return false;
                }
                else {
                    if (confirm("Are you sure to Hold this ticket?") == true) {
                        var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                        var RequestID = Dtl[0];
                        var ToData = "8Ø" + RequestID + "Ø" + RejectReason + "Ø17";

                        ToServer(ToData, 8);
                    }
                }
            }

            function btnProgress_onclick() {
                var RejectReason = document.getElementById("txtRemarks").value;
                if (document.getElementById("<%= cmbRequest.ClientID %>").value == "-1") {
                    alert("Select Request");
                    document.getElementById("<%= cmbRequest.ClientID %>").focus();
                    return false;
                }
                if (RejectReason == "") {
                    alert("Enter Remarks"); document.getElementById("txtRemarks").focus(); return false;
                }
                else {
                    if (confirm("Are you sure to In Progress this ticket?") == true) {
                        var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                        var RequestID = Dtl[0];
                        var ToData = "8Ø" + RequestID + "Ø" + RejectReason + "Ø18";
                        document.getElementById("<%= hdnProgress.ClientID %>").value = ToData;
                    }
                }
            }
            function AssetStatusOnChange() {
                var AssetStatus = document.getElementById("<%= cmbAssetStatus.ClientID %>").value;
                if (AssetStatus == 2) // Repaire
                {
                    document.getElementById("<%= txtAssetTag.ClientID %>").value = "";
                    document.getElementById("AssetTag").style.display = "";
                    document.getElementById("AssetCount").style.display = "none";
                }
                else // New
                {
                    document.getElementById("<%= txtCount.ClientID %>").value = "";
                    document.getElementById("AssetTag").style.display = "none";
                    document.getElementById("AssetCount").style.display = "";
                }
            }



            function AssetTagOnChange() {
                var AssetTag = document.getElementById("<%= txtAssetTAG.ClientID %>").value;
                var ItemID = document.getElementById("<%= cmbProblem.ClientID %>").value;
                if (AssetTag == "" || AssetTag == 0) {
                    alert("Please Enter a Valid Asset Tag");
                    document.getElementById("<%= txtAssetTAG.ClientID %>").focus();
                    return false;
                }

                var Dtl = document.getElementById("<%= cmbRequest.ClientID %>").value.split("Ø");
                var Branch = Dtl[1];

                var Dep = Dtl[4];
                var ToData = "7Ø" + AssetTag + "Ø" + Branch + "Ø" + Dep + "Ø" + ItemID;
                ToServer(ToData, 7);
            }



            function ProblemOnchange() {
                var ProblemDtl = document.getElementById("<%= cmbProblem.ClientID %>").value.split("~");

                var ProblemID = ProblemDtl[0];
                document.getElementById("<%= hdnProblemID.ClientID %>").value = ProblemID;
                var AsssetStatus = ProblemDtl[1];
                var IsCritical = ProblemDtl[2];
                var Description = ProblemDtl[3];
                var Annexture = ProblemDtl[4];

                document.getElementById("<%= cmbProblem.ClientID %>").title = Description;
                if (IsCritical == 1)
                    document.getElementById("<%= lblCritical.ClientID %>").innerHTML = 'Critical';
                else
                    document.getElementById("<%= lblCritical.ClientID %>").innerHTML = '';

                if (Annexture == 1)
                    document.getElementById("<%= btnView.ClientID %>").style.display = '';
                else
                    document.getElementById("<%= btnView.ClientID %>").style.display = "none";

                if (AsssetStatus == 1) {
                    document.getElementById("Asset").style.display = "";
                    document.getElementById("<%= cmbAssetStatus.ClientID %>").value = -1;
                    document.getElementById("AssetTag").style.display = "none";
                    document.getElementById("AssetCount").style.display = "none";
                }
                else {
                    document.getElementById("Asset").style.display = "none";
                    document.getElementById("AssetTag").style.display = "none";
                    document.getElementById("AssetCount").style.display = "none";
                }
            }
            function ViewAttachment() {
                var ProblemID = document.getElementById("<%= hdnProblemID.ClientID %>").value;
                if (ProblemID > 0)
                    window.open("ShowFormat.aspx?ProblemID=" + btoa(ProblemID) + "");
                return false;
            }    

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    </html>
    <div class="ScrollClass">
        <b>Help Desk Contact Numbers -</b> 08589975581, 08589975582, 08589975583, 08589975584<hr
            style="color: #E31E24; margin-top: 0px;" />
    </div>
    <br />
    <br />
    <table class="style1" style="width: 80%; margin: 0px auto;">
        <tr id="branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Request
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbRequest" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="80%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Tr3">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch
            </td>
            <td style="width: 63%">
                <asp:TextBox ID="txtBranch" runat="server" Width="50%" class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr id="Department">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Department
            </td>
            <td style="width: 63%">
                <asp:TextBox ID="txtDep" runat="server" Width="50%" class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Request Date
            </td>
            <td style="width: 63%">
                <asp:TextBox ID="txtStartDt" class="ReadOnlyTextBox" runat="server" Width="20%" onkeypress="return false"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="txtStartDt" Format="dd MMM yyyy">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
        <tr id="Category">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Category
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbCategory" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Module">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_TeamID" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Sub Category
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbItem" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Problem">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_ProblemID" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Findings
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbProblem" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                &nbsp;&nbsp;<asp:Label ID="lblCritical" runat="server" Font-Bold="True" ForeColor="#CC0000"></asp:Label>
                &nbsp;&nbsp;<asp:ImageButton ID="btnView" runat="server" Height="18px" Width="18px"
                    ImageAlign="AbsMiddle" ImageUrl="~/Image/attchment2.png" ToolTip="View Annexture" />
            </td>
        </tr>
        <tr id="Asset">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Asset Status
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbAssetStatus" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="2"> Repair </asp:ListItem>
                    <asp:ListItem Value="1"> New</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="AssetTag">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Asset TAG
            </td>
            <td style="width: 63%">
                <asp:TextBox ID="txtAssetTAG" runat="server" Style="font-family: Cambria; font-size: 10pt;"
                    Width="50%" class="NormalText" MaxLength="50" />
            </td>
        </tr>
        <tr id="AssetCount">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Count
            </td>
            <td style="width: 63%">
                <asp:TextBox ID="txtCount" runat="server" Style="font-family: Cambria; font-size: 10pt;"
                    Width="50%" class="NormalText" MaxLength="4" onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="Tr1">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_SubID" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Previous Remarks
            </td>
            <td style="width: 63%">
                <asp:Panel ID="txtDetails" runat="server" Style="width: 80%; border: 1px solid silver;
                    min-height: 60px;">
                </asp:Panel>
            </td>
        </tr>
        <tr id="Classi">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_GroupID" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Classification
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbClass" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ----------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Tr5">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_pnl" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                <asp:Label ID="lblStatus" runat="server" Text="Label"></asp:Label>
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbStatus" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ----------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Tr4">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_show" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Hand Over To
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbHover" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ----------Select---------</asp:ListItem>
                </asp:DropDownList>
                &nbsp; &nbsp;&nbsp;
                <asp:ImageButton ID="cmd_view" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle"
                    ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" />
            </td>
        </tr>
        <tr id="TrVendor" style="display: none;">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Vendor
            </td>
            <td style="width: 63%">
                <asp:DropDownList ID="cmbVendor" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ----------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Tr2">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_ReportID" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Remarks
            </td>
            <td style="width: 63%">
                <textarea id="txtRemarks" cols="20" name="S1" rows="3" maxlength="1000" style="width: 80%"
                    class="NormalText" onkeypress='return TextAreaCheck(event)' ></textarea><span style="color: Blue; cursor: pointer;" onclick="ViewRemarks()">
                    </span>
            </td>
        </tr>
        <tr id="Tr2">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 63%">
                <input id="chkPriority" type="checkbox" />
                High Priority
            </td>
        </tr>
        <tr id="Tr2">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Attachment If any
            </td>
            <td style="width: 63%">
                <div id="fileUploadarea">
                    <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" /><br />
                </div>
                <br />
                <div>
                    <input id="btnAddMoreFiles" onclick="AddMoreImages();" style="display: block;" type="button"
                        value="Add more images" /><br />
                </div>
            </td>
        </tr>
        <tr id="Remarks">
            <td style="width: 25%;" colspan="3">
                <asp:Panel ID="pnlPreviousRemarks" runat="server" Style="display: none;">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:Button ID="btnSave" runat="server" Style="font-family: cambria; cursor: pointer;
                    width: 8%;" Text="SAVE" />
                &nbsp;<asp:Button ID="btnReturn" runat="server" Style="font-family: cambria; cursor: pointer;
                    width: 8%;" Text="RETURN" />
                &nbsp;<input id="btnCancel"
                        style="font-family: cambria; cursor: pointer; width: 8%;" type="button" value="CANCEL"
                        onclick="return btnCancel_onclick()" />
                <input id="btnHold" style="font-family: cambria; cursor: pointer; width: 8%;" type="button"
                    value="HOLD" onclick="return btnHold_onclick()" />
                <asp:Button ID="btnProgress" runat="server" Style="font-family: cambria; cursor: pointer;
                    width: 8%;" Text="IN PROGRESS" />
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 8%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnProblemID" runat="server" />
    <asp:HiddenField ID="hdnSave" runat="server" />
    <asp:HiddenField ID="hdnReturn" runat="server" />
    <asp:HiddenField ID="hdnProgress" runat="server" />
    <br />
    <br />
</asp:Content>
