﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class HelpDesk_ChangeReview
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
#Region "Page Load & Dispose"
    Protected Sub Compliance_NewCompliance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 261) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Review Change"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "ChangeOnChange();", True)

            DT = DB.ExecuteDataSet("select '-1','----------------Select----------------' union all SELECT A.CHANGE_NO+'^'+A.CHANGE+'^'+A.REASON+'^'+A.IMPLEMENT_TEAM+'^'+A.ASSET_DESCR+'^'+A.IMPLEMENTATION_PLAN+'^'+A.VERIFICATION_PLAN+'^'+A.ROLLBACK_PLAN+'^'+B.STATUS+'^'+CONVERT(VARCHAR,A.PROPOSED_STARTTIME) + ' - ' + CONVERT(VARCHAR,A.PROPOSED_ENDTIME) +'^'+CONVERT(VARCHAR,A.IMPLEMENTATION_START)+ ' - ' + CONVERT(VARCHAR,A.IMPLEMENTATION_END),A.CHANGE_NO FROM HD_CHANGE_MASTER A,HD_CHANGE_STATUS B WHERE A.STATUS_ID in (6,7,8) AND A.STATUS_ID = B.STATUS_ID ").Tables(0)
            GF.ComboFill(cmbChange, DT, 0, 1)
            Me.cmbChange.Attributes.Add("onchange", "ChangeOnChange()")
            Me.hlDetails.Attributes.Add("onclick", "DetailsOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Compliance_NewCompliance_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Select Case CInt(Data(0))
            Case 9 ' Confirmation
                ' "9ʘ"+ ChangeNo + "ʘ" + ReviewComments;
                Dim ChangeNo As String = CStr(Data(1))
                Dim ReviewComments As String = CStr(Data(2))
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim ErrorFlag As Integer = 0
                Dim Message As String = Nothing
                Try
                    Dim Params(4) As SqlParameter
                    Params(0) = New SqlParameter("@ChangeNo", SqlDbType.VarChar, 20)
                    Params(0).Value = ChangeNo
                    Params(1) = New SqlParameter("@ReviewComments", SqlDbType.VarChar, 1000)
                    Params(1).Value = ReviewComments
                    Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(2).Value = UserID
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 4000)
                    Params(4).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_HD_CHANGE_REVIEW", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region
End Class
