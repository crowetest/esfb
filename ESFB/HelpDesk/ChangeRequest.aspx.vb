﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class HelpDesk_ChangeRequest
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
#Region "Page Load & Dispose"
    Protected Sub Compliance_NewCompliance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 256) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "New Change Request"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "WindowOnload();", True)

            DT = DB.ExecuteDataSet("select CATEGORY_ID,CATEGORY from HD_PROBLEM_CATEGORY").Tables(0)
            GF.ComboFill(cmbCategory, DT, 0, 1)
            Dim StrData As String = ""
            DT = DB.ExecuteDataSet("select ticket_no from HD_request_master where  status_id not in (0,4)").Tables(0)
            For Each DR As DataRow In DT.Rows
                StrData += DR(0).ToString() + "~"

            Next
            hdnTicketData.Value = StrData.ToString()
            Me.hlDetails.Attributes.Add("onclick", "DetailsOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Compliance_NewCompliance_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Select Case CInt(Data(0))
            Case 1 ' Confirmation
                ' Data = "9ʘ" + HdTicketNo + "ʘ" + Change + "ʘ" + Reason;
                Dim HdTicketNo As String = CStr(Data(1))
                Dim Change As String = CStr(Data(2))
                Dim Reason As String = CStr(Data(3))
                Dim CategoryID As Integer = CInt(Data(4))
                Dim Message As String = Nothing
                Dim ChangeNo As String = Nothing
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(7) As SqlParameter
                    Params(0) = New SqlParameter("@HdTicketNo", SqlDbType.VarChar, 20)
                    Params(0).Value = HdTicketNo
                    Params(1) = New SqlParameter("@Change", SqlDbType.VarChar, 1000)
                    Params(1).Value = Change
                    Params(2) = New SqlParameter("@Reason", SqlDbType.VarChar, 1000)
                    Params(2).Value = Reason
                    Params(3) = New SqlParameter("@CategoryID", SqlDbType.Int)
                    Params(3).Value = CategoryID
                    Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(4).Value = UserID
                    Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@ChangeNo", SqlDbType.VarChar, 10)
                    Params(7).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_HD_CHANGE_REQUEST", Params)
                    ErrorFlag = CInt(Params(5).Value)
                    Message = CStr(Params(6).Value)
                    ChangeNo = CStr(Params(7).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message + "ʘ" + ChangeNo
            Case 2
                DT = DB.ExecuteDataSet("select REQUEST_ID from HD_request_master where ticket_no='" + Data(1) + "' and status_id not in (0,4)").Tables(0)
                If DT.Rows.Count > 0 Then
                    CallBackReturn = DT.Rows(0)(0).ToString
                Else
                    CallBackReturn = "0"
                End If
        End Select
    End Sub
#End Region
End Class
