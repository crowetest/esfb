﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ChangePlan.aspx.vb" Inherits="HelpDesk_ChangePlan" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server" >
    <head>
<title></title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.22/themes/redmond/jquery-ui.css" />
<link href="../Style/Style.css" rel="stylesheet" type="text/css" />
<script src="../Script/Validations.js" type="text/javascript"></script>
<link href="../Style/jquery.ptTimeSelect.css" type="text/css" rel="Stylesheet"/>
   <script src="../Script/jquery.ptTimeSelect.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" for="window" event="onload">return ChangeOnChange()</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('input[name="timeFrom"]')
            .ptTimeSelect({
                zIndex: 100,
                onBeforeShow: function (input, widget) {
                    // do something before the widget is made visible.
                }
            })
            .show();
    });
    $(document).ready(function () {
        $('input[name="timeTo"]')
            .ptTimeSelect({
                zIndex: 100,
                onBeforeShow: function (input, widget) {
                    // do something before the widget is made visible.
                }
            })
            .show();
        });
        $(document).ready(function () {
            $('input[name="timeDownFrom"]')
            .ptTimeSelect({
                zIndex: 100,
                onBeforeShow: function (input, widget) {
                    // do something before the widget is made visible.
                }
            })
            .show();
        });
        $(document).ready(function () {
            $('input[name="timeDownTo"]')
            .ptTimeSelect({
                zIndex: 100,
                onBeforeShow: function (input, widget) {
                    // do something before the widget is made visible.
                }
            })
            .show();
        });
</script>  
<script language="javascript" type="text/javascript">
     $('input[name="timeFrom"]')
    .ptTimeSelect({
        containerClass: undefined,
        containerWidth: undefined,
        hoursLabel:     'Hour',
        minutesLabel:   'Minutes',
        setButtonLabel: 'Set',
        popupImage:     undefined,
        onFocusDisplay: true,
        zIndex:         10,
        onBeforeShow:   undefined,
        onClose:        undefined
    });
    $('input[name="timeTo"]')
    .ptTimeSelect({
        containerClass: undefined,
        containerWidth: undefined,
        hoursLabel:     'Hour',
        minutesLabel:   'Minutes',
        setButtonLabel: 'Set',
        popupImage:     undefined,
        onFocusDisplay: true,
        zIndex:         10,
        onBeforeShow:   undefined,
        onClose:        undefined
    });
     $('input[name="timeDownFrom"]')
    .ptTimeSelect({
        containerClass: undefined,
        containerWidth: undefined,
        hoursLabel:     'Hour',
        minutesLabel:   'Minutes',
        setButtonLabel: 'Set',
        popupImage:     undefined,
        onFocusDisplay: true,
        zIndex:         10,
        onBeforeShow:   undefined,
        onClose:        undefined
    });
    $('input[name="timeDownTo"]')
    .ptTimeSelect({
        containerClass: undefined,
        containerWidth: undefined,
        hoursLabel:     'Hour',
        minutesLabel:   'Minutes',
        setButtonLabel: 'Set',
        popupImage:     undefined,
        onFocusDisplay: true,
        zIndex:         10,
        onBeforeShow:   undefined,
        onClose:        undefined
    });

    function ChangeOnChange()
    {
         if(document.getElementById("<%= cmbChange.ClientID %>").value =="-1")
         {
            document.getElementById("<%= txtChange.ClientID %>").value =  "";
            document.getElementById("<%= txtReason.ClientID %>").value = "" ;
            document.getElementById("<%= cmbType.ClientID %>").value=-1;
            document.getElementById("<%= cmbPriority.ClientID %>").value=-1;
            document.getElementById("<%= txtWhoWillDo.ClientID %>").value = "";
            document.getElementById("<%= txtAsset.ClientID %>").value = "";
            document.getElementById("<%= txtPrePlan.ClientID %>").value = "";
            document.getElementById("<%= txtImpPlan.ClientID %>").value = "";
            document.getElementById("<%= txtVerifyPlan.ClientID %>").value = "";
            document.getElementById("<%= txtRollbackPlan.ClientID %>").value = "";
            document.getElementById("<%= txtWhomAffect.ClientID %>").value = "";
            document.getElementById("<%= txtDownTimeStart.ClientID %>").value = "";
            document.getElementById("<%= txtDownTimeEnd.ClientID %>").value = "";
            document.getElementById("<%= txtFrom.ClientID %>").value = "";
            document.getElementById("<%= txtTo.ClientID %>").value = "";
            document.getElementById("timeFrom").value = "";
            document.getElementById("timeTo").value = "";
            document.getElementById("timeDownFrom").value = "";
            document.getElementById("timeDownTo").value = "";
        }
        else
        {
            var ChangeInfo = document.getElementById("<%= cmbChange.ClientID %>").value.split("^");
            document.getElementById("<%= txtChange.ClientID %>").value = ChangeInfo[1];
            document.getElementById("<%= txtReason.ClientID %>").value = ChangeInfo[2];
        }
    }

    function DetailsOnClick()
    {
        if(document.getElementById("<%= cmbChange.ClientID %>").value =="-1")
        {
            alert("Select Ticket");
            document.getElementById("<%= cmbChange.ClientID %>").focus();
            return false;
        }
        var ChangeInfo = document.getElementById("<%= cmbChange.ClientID %>").value.split("^");
        var ChangeNo   = ChangeInfo[0];
        window.open("Reports/ChangeTicketInfo.aspx?ChangeNo=" + btoa(ChangeNo) + "");
    }

    function btnSave_onclick() 
    {
           if(document.getElementById("<%= cmbType.ClientID %>").value == "-1") 
           {
                alert("Select Type"); return false;
           } 
           if(document.getElementById("<%= cmbPriority.ClientID %>").value == "-1") 
           {
                alert("Select Priority"); return false;
           } 
           if(document.getElementById("<%= txtWhoWillDo.ClientID %>").value == "") 
           {
                alert("Enter Implementor"); return false;
           }
           if(document.getElementById("<%= txtEmail.ClientID %>").value == "") 
           {
                alert("Enter Email"); return false;
           } 
           if(document.getElementById("<%= txtEmail.ClientID %>").value!="")
            { var ret= checkEmail(document.getElementById("<%= txtEmail.ClientID %>")); if(ret==false){ return false;}}
           if(document.getElementById("<%= txtAsset.ClientID %>").value == "") 
           {
                alert("Enter Asset Details"); return false;
           }
           if(document.getElementById("<%= txtPrePlan.ClientID %>").value == "") 
           {
                alert("Enter PrePlan Details"); return false;
           } 
           if(document.getElementById("<%= txtImpPlan.ClientID %>").value == "") 
           {
                alert("Enter Implementation Plan Details"); return false;
           }
           if(document.getElementById("<%= txtVerifyPlan.ClientID %>").value == "") 
           {
                alert("Enter Verify Plan Details"); return false;
           } 
           if(document.getElementById("<%= txtRollbackPlan.ClientID %>").value == "") 
           {
                alert("Enter Rollback Plan Details"); return false;
           } 
           if(document.getElementById("<%= txtWhomAffect.ClientID %>").value == "") 
           {
                alert("Enter Affected Persons"); return false;
           }
           if(document.getElementById("<%= txtDownTimeStart.ClientID %>").value == "") 
           {
                alert("Enter Down Time Start"); return false;
           }
           if(document.getElementById("<%= txtDownTimeEnd.ClientID %>").value == "") 
           {
                alert("Enter Down Time End"); return false;
           }
           if(document.getElementById("<%= txtFrom.ClientID %>").value == "") 
           {
                alert("Select From date"); return false;
           }
           if(document.getElementById("<%= txtTo.ClientID %>").value == "") 
           {
                alert("Select To date"); return false;
           }
           if(document.getElementById("timeFrom").value == "") 
           {
                alert("Select From Time"); return false;
           }
           if(document.getElementById("timeTo").value == "") 
           {
                alert("Select To Time"); return false;
           } 
           if(document.getElementById("timeDownFrom").value == "") 
           {
                alert("Select Down Time From"); return false;
           }
           if(document.getElementById("timeDownTo").value == "") 
           {
                alert("Select Down Time End"); return false;
           } 
        var ChangeInfo = document.getElementById("<%= cmbChange.ClientID %>").value.split("^");
        var ChangeNo   = ChangeInfo[0];
        var TypeID     = document.getElementById("<%= cmbType.ClientID %>").value;
        var PriorityID = document.getElementById("<%= cmbPriority.ClientID %>").value;
        var WhoWillDo  = document.getElementById("<%= txtWhoWillDo.ClientID %>").value;
        var Asset      = document.getElementById("<%= txtAsset.ClientID %>").value;
        var PrePlan    = document.getElementById("<%= txtPrePlan.ClientID %>").value;
        var ImpPlan    = document.getElementById("<%= txtImpPlan.ClientID %>").value;
        var VerifyPlan = document.getElementById("<%= txtVerifyPlan.ClientID %>").value;
        var RollPlan   = document.getElementById("<%= txtRollbackPlan.ClientID %>").value;
        var WhomAffect = document.getElementById("<%= txtWhomAffect.ClientID %>").value;
        var FromDownDt = document.getElementById("<%= txtDownTimeStart.ClientID %>").value;
        var ToDownDt   = document.getElementById("<%= txtDownTimeEnd.ClientID %>").value;
        var FrDownTime = document.getElementById("timeDownFrom").value;
        var ToDownTime = document.getElementById("timeDownTo").value;
        var FromDt     = document.getElementById("<%= txtFrom.ClientID %>").value;
        var ToDt       = document.getElementById("<%= txtTo.ClientID %>").value;
        var FromTime   = document.getElementById("timeFrom").value;
        var ToTime     = document.getElementById("timeTo").value;
        var StartTime  = FromDt + " " + FromTime;
        var EndTime    = ToDt + " " + ToTime;
        var StartDownTime = FromDownDt + " " + FrDownTime;
        var EndDownTime = ToDownDt + " " + ToDownTime;
        var Data       = "9ʘ"+ ChangeNo + "ʘ" + TypeID + "ʘ" + PriorityID + "ʘ" + WhoWillDo + "ʘ" + Asset + "ʘ" + PrePlan + "ʘ" + ImpPlan + "ʘ" + VerifyPlan + "ʘ" + RollPlan + "ʘ" + WhomAffect + "ʘ" + StartTime  + "ʘ" + EndTime + "ʘ" + StartDownTime  + "ʘ" + EndDownTime+ "ʘ" +document.getElementById("<%= txtEmail.ClientID %>").value;
        ToServer(Data,9)
       
    }

    function setStartDate(sender, args) {
            var StartDt = AddDay(document.getElementById("<%= txtFrom.ClientID %>").value, 1);
            var EndDt = AddDay(document.getElementById("<%= txtTo.ClientID %>").value, 1);
            var Cnt = 1; 
            var OrgStartDt = AddDay(document.getElementById("<%= txtFrom.ClientID %>").value, Cnt);
            sender._startDate =  new Date(OrgStartDt);
            sender._endDate =  new Date(EndDt);
    }

    function setDate(sender, args) {
            var StartDt = AddDay(document.getElementById("<%= txtFrom.ClientID %>").value, 1);
            var EndDt = AddDay(document.getElementById("<%= txtTo.ClientID %>").value, 1);
            var Cnt = 1; 
            var OrgStartDt = AddDay(document.getElementById("<%= txtFrom.ClientID %>").value, Cnt);
            sender._startDate =  new Date(OrgStartDt);
            //sender._endDate =  new Date(EndDt);
    }
    function setEndDate(sender, args) {
            var StartDt = AddDay(document.getElementById("<%= txtDownTimeStart.ClientID %>").value, 1);
            var EndDt = AddDay(document.getElementById("<%= txtDownTimeEnd.ClientID %>").value, 1);
            var Cnt = 1; 
            var OrgStartDt = AddDay(document.getElementById("<%= txtDownTimeStart.ClientID %>").value, Cnt);
            sender._startDate =  new Date(OrgStartDt);
            sender._endDate =  new Date(EndDt);
    }
    function AddDay(strDate, intNum) {
        sdate = new Date(strDate);
        sdate.setDate(sdate.getDate() + intNum);
        return sdate.getMonth() + 1 + " " + sdate.getDate() + " " + sdate.getFullYear();
    }

    function FromServer(Arg, Context) 
    {
        switch(Context)
        {
            case 9: // Confirmation
            {
                var Data = Arg.split("ʘ");
                alert(Data[1]);
                if ( Data[0] == 0 )  
                {
                    window.open("../home.aspx","_self");
                }
                break;
            }
        }
    }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
</script>
</head>
    <br />
           
    <table align="center" style="width: 90%; margin: 0px auto; ">
        <tr>
            <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;"><asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>Select Change Ticket</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;cursor:pointer">
                    <asp:DropDownList ID="cmbChange" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="60%" ForeColor="Black">
                        </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:HyperLink ID="hlDetails" runat="server" ForeColor="#0000CC" 
                        style="text-decoration: underline" ToolTip="View Details">View Details</asp:HyperLink>
            </td>
        </tr>
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left; height: 70px;">Change Description</td>
            <td style="width: 3%; height: 70px;"></td>
            <td style="width: 70%; text-align:left; height: 70px;">
                <asp:TextBox ID="txtChange" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)'  
                    ReadOnly="True"/></td>
        </tr>
        <tr style="display:none;">
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Reason For Change</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                <asp:TextBox ID="txtReason" runat="server" 
                    class="ReadOnlyTextBox" style=" font-family:Cambria;font-size:10pt;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' 
                    ReadOnly="True"/></td>
        </tr>
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Type of Change &amp; Priority</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                    <asp:DropDownList ID="cmbType" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="40%" ForeColor="Black">
                        </asp:DropDownList>
                    <asp:DropDownList ID="cmbPriority" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="40%" ForeColor="Black">
                        </asp:DropDownList>

                        </td>
        </tr>
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Proposed Start Time</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                <asp:TextBox ID="txtFrom" class="NormalText" runat="server" Width="27%" 
                MaxLength="100" Height="20px" onkeypress="return false" ></asp:TextBox>
             <asp:CalendarExtender ID="CalendarExtender1" runat="server"  TargetControlID="txtFrom" Format="dd MMM yyyy">
             </asp:CalendarExtender><input type="text" id="timeFrom" name="timeFrom" value="" onkeypress="return false" style="width:10%" /></td>
        </tr>
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Proposed End Time</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                <asp:TextBox ID="txtTo" class="NormalText" runat="server" Width="27%" 
                MaxLength="100" Height="20px" onkeypress="return false"></asp:TextBox>
             <asp:CalendarExtender ID="CEFromDate" runat="server" TargetControlID="txtTo" OnClientShowing="setStartDate" Format="dd MMM yyyy">
             </asp:CalendarExtender><input  type="text" id="timeTo" name="timeTo" value="" onkeypress="return false" style="width:10%"/></td>
        </tr>
         <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Impact- Whom Will Affect</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                <asp:TextBox ID="txtWhomAffect" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="1000" Rows="2" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
         <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Down Time Start</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
            <asp:TextBox ID="txtDownTimeStart" class="NormalText" runat="server" Width="27%" 
                MaxLength="100" Height="20px" onkeypress="return false" ></asp:TextBox>
             <asp:CalendarExtender ID="CalendarExtender4" runat="server" OnClientShowing="setDate" TargetControlID="txtDownTimeStart" Format="dd MMM yyyy">
             </asp:CalendarExtender>
                    <input type="text" id="timeDownFrom" 
                    name="timeDownFrom" value="" onkeypress="return false" style="width:10%" /></td>
        </tr>
        
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Down Time End</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
            <asp:TextBox ID="txtDownTimeEnd" class="NormalText" runat="server" Width="27%" 
                MaxLength="100" Height="20px" onkeypress="return false" ></asp:TextBox>
             <asp:CalendarExtender ID="CalendarExtender2" runat="server" OnClientShowing="setEndDate"  TargetControlID="txtDownTimeEnd" Format="dd MMM yyyy">
             </asp:CalendarExtender>
                    <input type="text" id="timeDownTo" 
                    name="timeDownTo" value="" onkeypress="return false" style="width:10%" /></td>
        </tr>
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Who Will Implement</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                <asp:TextBox ID="txtWhoWillDo" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="1000" Rows="2" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Implement Team E-mail </td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                <asp:TextBox ID="txtEmail" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="50" Rows="2" /></td>
        </tr>
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">On Which Asset(s)</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                <asp:TextBox ID="txtAsset" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="1000" Rows="2" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Pre Implementation Plan</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                <asp:TextBox ID="txtPrePlan" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Implementation Plan</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                <asp:TextBox ID="txtImpPlan" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Verification Plan</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                <asp:TextBox ID="txtVerifyPlan" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
        <tr>
        <td style="width: 10%">&nbsp;</td>
            <td style="width: 17%; text-align:left;">Rollback Plan</td>
            <td style="width: 3%">&nbsp;</td>
            <td style="width: 70%; text-align:left;">
                <asp:TextBox ID="txtRollbackPlan" runat="server" 
                    class="NormalText" style=" font-family:Cambria;font-size:10pt;resize:none;" 
                    Width="100%" MaxLength="1000" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' /></td>
        </tr>
       
       
        
        <tr>
            <td style="text-align:center;"colspan="4" class="style2">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="SAVE" onclick="return btnSave_onclick()" />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()"  />
            </td>            
         </tr>
    </table>
    <br />
</asp:Content>


