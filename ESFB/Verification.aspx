﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Verification.aspx.vb" Inherits="Verification" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<link href="Style/Style.css" rel="stylesheet" type="text/css" />
<script src="Script/Validations.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
    function btnConfirmClick() {
        var Status ;
        if (document.getElementById("rbtnYes").checked == false && document.getElementById("rbtnNo").checked == false) {
            alert("Please Check One Option");return false;
        }
        if (document.getElementById("rbtnYes").checked == true) {
            var Accno = document.getElementById("<%= txtAccNo.ClientID %>").value;
            if ((Accno.length) < 14) {
                alert("Enter Correct Account Number");
                document.getElementById("<%= txtAccNo.ClientID %>").value = "";
                document.getElementById("<%= txtAccNo.ClientID %>").focus();
                return false;
            }
            var CustID = document.getElementById("<%= txtCustID.ClientID %>").value;
            Status = 0;
        }
        if (document.getElementById("rbtnNo").checked == true) 
            Status = 1;
        document.getElementById("<%= hdnStatus.ClientID %>").value = Status;
    }
    function OptClick(ID) {
        if (ID == 1) {
            document.getElementById("rowAccNo").style.display = "";
            document.getElementById("rowMsg").style.display = "none";
            document.getElementById("<%= txtAccNo.ClientID %>").value = "";
            document.getElementById("<%= txtAccNo.ClientID %>").focus();
        }
        else {
            document.getElementById("rowAccNo").style.display = "none";
            document.getElementById("rowMsg").style.display = "";
            document.getElementById("<%= txtAccNo.ClientID %>").value = "";
        }
    }
</script>

   <div style="width:100%;">
        <table align="center" style="width: 100%;">
    
        <tr>
            <td style="width:20%; font-size: medium;">
                &nbsp;</td>
            <td  style="text-align:left;width:70%; font-size: medium;">
                &nbsp;</td>
            <td  style="width:10%; font-size: medium;">
                &nbsp;</td>
        </tr>
    
        <tr>
            <td style="width:20%; font-size: medium;">
                &nbsp;</td>
            <td  style="text-align:left;width:70%; font-size: medium;">
                &nbsp;</td>
            <td  style="width:10%; font-size: medium;">
                &nbsp;</td>
        </tr>
    
        <tr>
            <td style="width:20%; font-size: medium;">
                &nbsp;</td>
            <td  style="text-align:center; width:70%; font-size: medium;">
                Kindly update your ESAF SFB Account details for processing your monthly Salary 
                to proceed further</td>
            <td  style="width:10%; font-size: medium;">
                &nbsp;</td>
        </tr>
    
        <tr>
            <td style="width:20%; font-size: medium;">
                &nbsp;</td>
            <td  style="text-align:center; width:70%; font-size: medium;">
                &nbsp;</td>
            <td  style="width:10%; font-size: medium;">
                &nbsp;</td>
        </tr>
    
        <tr>
            <td style="width:20%; font-size: medium;">
                &nbsp;</td>
            <td  style="text-align:center; width:70%; font-size: medium;color: #0000FF;"> If the Account no is not activated/Account no is not available now, please 
                proceed further by clicking on &quot;No&quot;
                </td>
            <td  style="width:10%; font-size: medium;">
                &nbsp;</td>
        </tr>
    
        <tr>
            <td style="width:20%; font-size: medium;">
                &nbsp;</td>
            <td  style="text-align:center; width:70%; font-size: medium; color: #0000FF;">
                &nbsp;</td>
            <td  style="width:10%; font-size: medium;">
                &nbsp;</td>
        </tr>
    
        <tr>
            <td style="width:20%; font-size: medium;">
                &nbsp;</td>
            <td  style="text-align:center; width:70%; font-size: medium; ">
              Account Exist ?&nbsp;
                <input id="rbtnYes" name="aa" type="radio" onclick="OptClick(1)" />Yes&nbsp;&nbsp;
                <input id="rbtnNo" name="aa" type="radio" onclick="OptClick(2)"  />No </td>
            <td  style="width:10%; font-size: medium;">
                &nbsp;</td>
        </tr>
    
        <tr>
            <td style="width:20%; font-size: medium;">
                &nbsp;</td>
            <td  style="text-align:center; width:70%; font-size: medium; color: #0000FF;">
                &nbsp;</td>
            <td  style="width:10%; font-size: medium;">
                &nbsp;</td>
        </tr>
    
        <tr id="rowAccNo" style="display:none;height:40px;">
            <td style="width:20%;">
                &nbsp;</td>
            <td   style="text-align:center; width:70%;font-size: medium;">
                Enter Account No 
                <asp:TextBox ID="txtAccNo" MaxLength="14" onkeypress='return NumericCheck(event)' runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp; Enter CIF(if available) 
                <asp:TextBox ID="txtCustID" MaxLength="12"  onkeypress='return NumericCheck(event)' runat="server"></asp:TextBox></td>
            <td  style="width:10%;">
                &nbsp;</td>
        </tr>
    
        <tr id="rowMsg" style="display:none;height:40px;">
            <td style="width:20%;">
                &nbsp;</td>
            <td   style="text-align:center; width:70%;font-size: medium; color: #CC0000;">
                Dear Colleague, If your account is still not opened in ESAF SFB, please arrange to send the  complete set of filled up account opening forms duly certified by the BM/ABM and required self attested  KYC documents 
                and FATCA declaration to Mr Revi Kumar, ESAF SFB, Project Management Office, Shree Bhadram Complex, Mannuthy PO  680651, Thrissur, Kerala immediately.</td>
            <td  style="width:10%;">
                &nbsp;</td>
        </tr>
    
        <tr  style="display:none;height:40px;">
            <td style="width:20%;">
                &nbsp;</td>
            <td   style="text-align:left;width:70%;font-size: medium;">
                &nbsp;</td>
            <td  style="width:10%;">
                &nbsp;</td>
        </tr>
    
        <tr  style="height:40px;">
            <td style="width:20%;">
                &nbsp;</td>
            <td   style="text-align:left;width:70%;font-size: medium;">
                 
                 &nbsp;</td>
            <td  style="width:10%;">
                &nbsp;</td>
        </tr>
    
        <tr  style="height:40px;">
            <td style="width:20%;">
                &nbsp;</td>
            <td   style="text-align:center; width:70%;font-size: medium;">
                 
                 <asp:Button ID="btnConfirm" runat="server" Text="CONFIRM" class="NormalText" style="font-family: cambria; cursor: pointer; width: 10%; height:30px;"/>
                 </td>
            <td  style="width:10%;">
                &nbsp;</td>
        </tr>
    
        <tr style="height:40px;text-aign:center;">
            <td colspan="3">
                &nbsp;</td>
        </tr>
    
        <tr>
            <td style="width:20%;">
                &nbsp;</td>
            <td   style="text-align:left;width:70%;">
                &nbsp;</td>
            <td  style="width:10%;">
                &nbsp;</td>
        </tr>
    
        <tr>
            <td  style="text-align:center;width:100%;" colspan="3">
                 <asp:HiddenField ID="hdnStatus" runat="server" />
                 
                 &nbsp; 
            </td>
           
        </tr>
    </table>
    </div>
</asp:Content>

