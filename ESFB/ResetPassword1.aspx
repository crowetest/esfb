﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="ResetPassword1.aspx.vb" Inherits="ResetPassword1" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Change Password</title>
<link href="Style/Style.css" type="text/css" rel="Stylesheet"/>
<style type="text/css">
body 
{
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #476C91;
	background-color:#79A5C9;
	line-height: 18px;
	height: 100%;
	margin-top: 15px;
}
        .style1 
	{
            width: 100%;
        }
        .style2 
        {
        	width: 25%;
        }
        .style6
        {
            width: 25%;
            font-size: x-large;
            font-family: Cambria;
            color: #476C91;
        }
        .style17
        {
        	width: 25%;
            height: 30px;
            text-align: right;
            font-family: Cambria;
        }
        .style18
        {
            width: 25%;
            height: 30px;
            text-align: left;
        }
        VeryPoorStrength
        {
            background: Red;
            color:white;
            font-weight:bold;
        }
        .WeakStrength
        {
            background: Red;
            color:White;
            font-weight:bold;
        }
        .AverageStrength
        {
            background: orange;
            color:black;
            font-weight:bold;
        }
        .GoodStrength
        {
            background: blue;
            color:White;
            font-weight:bold;
        }
        .ExcellentStrength
        {
            background: Green;
            color:White;
            font-weight:bold;
        }
        .BarBorder
        {
            border-style: solid;
            border-width: 0px;
            width: 180px;
            padding:0px;
        }
        .BarIndicator
        {
	        height:1px;
        }
        .style27
        {
            width: 12%;
            height: 26px;
            text-align: right;
            font-family: Georgia;
        }
        .style28
        {
            width: 25%;
            height: 26px;
            text-align: left;
        }
        .style29
        {
            font-family: Georgia;
            font-size: small;
        }
        </style>
<script type="text/javascript" src="Script/jquery-1.2.6.min.js"></script>
<script src="Script/Validations.js" language="javascript" type="text/javascript"> </script>  
<script language="javascript" type="text/javascript">
    function btnExit_onclick() 
      {
          window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
      }
      
      function FromServer(arg, context) {

          switch (context) {

              case 1:
                  {                    
                      var Data = arg.split("Ø");
                      alert(Data[1]);
                      if (Data[0] == 0) window.open("ResetPassword1.aspx", "_self");
                      break;
                  }
             case 2:
                  {                    
                      var Data = arg.split("Ø");
                      alert(Data[1]);
                      if (Data[0] == 0) window.open("ResetPassword1.aspx", "_self");
                      break;
                  }

          }

      }
   
function btnSave_onclick() {alert(1);
 if (document.getElementById("<%= txtUserID.ClientID %>").value == "")
          { alert("Enter a Valid Employee Code"); document.getElementById("<%= txtUserID.ClientID %>").focus(); return false; }
          var EmpCode =document.getElementById("<%= txtUserID.ClientID %>").value;
          ToServer("1Ø" + EmpCode , 1);
}

function btnReset_onclick() {
if(confirm("Are You Sure to Reset the Password of all employees?")==true)
    ToServer("2Ø" , 2);
}

</script>
</head>
<body style="background-color:#79A5C9">
<form id="form1" runat="server">
<div style="height:150px;padding-left:10px;padding-top:15px;"><img src="Image/Logo.png" style="width:110px; height:110px;">
</div>
<div id="MainContents" style="background-color:White; height:370px; width:100%">
    <table align="center" class="style1">
        <tr>
            <td class="style6" colspan="2">
                &nbsp;</td>
            <td class="style2"> <asp:HiddenField ID="hdnValue" runat="server" />
                &nbsp;</td>
            <td class="style2">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style6" colspan="3">
                &nbsp;Reset Password</td>
            <td class="style2">
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2" colspan="2">
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style27">
                </td>
            <td class="style27">
                <span class="style29">User ID</span><span> :</span></td>
            <td class="style28">
                <asp:TextBox ID="txtUserID" runat="server" Width="90%" MaxLength="5" onkeypress="return NumericCheck();"></asp:TextBox>
            </td>
            <td class="style28">
                </td>
            <td class="style28">
                </td>
        </tr>
         <tr>
            <td class="style27">
                </td>
            <td class="style27">
                <span class="style29"></span><span></span></td>
            <td class="style28">
               
            </td>
            <td class="style28">
                </td>
            <td class="style28">
                </td>
        </tr>
        <tr>
         <td class="style18">
                </td>
            <td class="style17" colspan="2" style="text-align:center;">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="RESET"  onclick="return btnSave_onclick()" onclick="return btnSave_onclick()" />&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />&nbsp;</td>
            
            <td class="style18">
                <input id="btnReset" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="RESETALL"  onclick="return btnReset_onclick()" /></td>
            <td class="style18">
                </td>
        </tr>
    </table>
</div>
<div id="FooterContainer" style="float:right;">
</div>
</form>
</body>
</html>
</asp:Content>