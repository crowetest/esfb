﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AccessDenied.aspx.vb" Inherits="AccessDenied" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Access Denied !</title>
    <style type="text/css">
        .style1
        {
            color: #E31E24;
            text-align:center;
            height : auto;
            font-family:Cambria;
            font-size:xx-large;

        }
        .style2
        {
            color: #E31E24;
            text-align:center;
            height : auto;
            font-family:Cambria;
            font-size:medium;
            font-weight:bold;
        }
    </style>
</head>
<body onload="pageInit(); width:100%; height:100%"  class="style2">
    <form id="login_form" runat="server">
        <div style="height:200px; padding-top:0px; padding-top:10px; padding-bottom:10px;"></div>
        <div class="style1">Access Denied !<br /><br /><br /><br /></div>
        <div class="style2">Current ESFB IT Policy does not authorize you to visit this page<br /><br /></div>
        <div class="style2">Your IP is under surveillance</div>
    </form>
</body>
</html>
