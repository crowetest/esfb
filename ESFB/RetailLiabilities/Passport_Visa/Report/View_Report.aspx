﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false"
    CodeFile="View_Report.aspx.vb" Inherits="RetailLiabilities_Passport_Visa_Report_View_Report" %>

<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../../Style/ExportMenu.css" rel="stylesheet" type="text/css" />
        <script src="../../../Script/Validations.js" type="text/javascript"></script>
        <style type="text/css">
            * {
                padding: 0;
                margin-left: 0px;
                margin-right: 0px;
            }

            * {
                padding: 0;
                margin-left: 0px;
                margin-right: 0px;
            }

            #loadingmsg {
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                position: fixed;
                display: block;
                opacity: 0.7;
                background-color: #fff;
                z-index: 99;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <script language="javascript" type="text/javascript">
            function FromServer(Arg, Context) {
                var Branch = document.getElementById("<%= hdnBranch.ClientID %>").value;
                var BranchCode = document.getElementById("<%= hdnBrCode.ClientID %>").value;
                var SRType = document.getElementById("<%= hdnSRType.ClientID %>").value;
                var From_Date = document.getElementById("<%= hdnFromDT.ClientID %>").value;
                var To_Date = document.getElementById("<%= hdnToDT.ClientID %>").value;
                switch (Context) {
                    case 1:
                        {
                            var Data = Arg.split("Ø");
                            alert(Data[1]);
                            if (Data[0] == 0) window.open("View_Report.aspx?ViewType=" + btoa(-1) + "&Branch=" + btoa(Branch) + "&BranchCode=" + btoa(BranchCode) + "&From_Date=" + btoa(From_Date) + " &To_Date=" + btoa(To_Date) + " &SR_Type=" + btoa(SRType) + " ", "_self");
                        }
                    //case 2:
                    //    {
                    //        var Data = Arg.split("Ø");
                    //        if (Data[0] == 0) window.open("View_Report.aspx?ViewType=" + btoa(-1) + "&Branch=" + btoa(Branch) + "&BranchCode=" + btoa(BranchCode) + "&From_Date=" + btoa(From_Date) + " &To_Date=" + btoa(To_Date) + " &SR_Type=" + btoa(SRType) + " ", "_self");
                    //    }
                }
            }
            function PrintPanel() {
                var panel = document.getElementById("<%=pnDisplay.ClientID %>");
                var printWindow = window.open('', '', 'height=400,width=850,location=0,status=0');
                printWindow.document.write('<html><head><title>ESAF</title>');
                printWindow.document.write('</head><body >');
                printWindow.document.write(panel.innerHTML);
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                setTimeout(function () {
                    printWindow.print();
                }, 500);
                return false;
            }
            function Exitform() {
                var branch = document.getElementById("<%= hdnBranch.ClientID %>").value;
                if (branch == "0") {
                    window.open("../HO_Report.aspx", "_self");
                } else {
                    window.open("../BranchReport.aspx", "_self");
                }
                return false;
            }
            //function ImgBtnExport_Click() {
            //    var Data = "2Ø";
            //    ToServer(Data, 2);
            //}
            function SaveSRNo(CIF, Pass_SR, Visa_SR) {
                
                var txtPassSR = "txtPassportSR" + CIF;
                var txtVisaSR = "txtVisaSR" + CIF;
                var check = 0;

                var PassportSR = document.getElementById(txtPassSR).value.trim();
                var VisaSR = document.getElementById(txtVisaSR).value.trim();

                if (Pass_SR == 1) {
                    if (PassportSR.trim().length > 0) {
                        check += 1;
                    }
                    else {
                        check -= 1;
                    }
                }

                if (Visa_SR == 1) {
                    if (VisaSR.trim().length > 0) {
                        check += 1;
                    }
                    else {
                        check -= 1;
                    }
                }

                if (check < 0) {
                    alert("Please Provide Details !");
                    return false;
                }

                var Data = "1Ø" + CIF + "ʘ" + PassportSR + "ʘ" + VisaSR;
                ToServer(Data, 1);
                showLoading();

            }
            function showLoading() {
                document.getElementById('loadingmsg').style.display = 'block';
                document.getElementById('loadingover').style.display = 'block';
            }
            function closeLoading() {
                document.getElementById('loadingmsg').style.display = 'none';
                document.getElementById('loadingover').style.display = 'none';
            }
            function table_fill() {
                document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';

                var tab = "";

                tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
                tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'>";

                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                tab += "<tr style='background-color:WhiteSmoke;'>"
                tab += "<td width='100%' align='center' colspan='100'><font size='4'><font face='Cambria'><b>ESAF SMALL FINANCE BANK</b></font></font></td>"
                tab += "</tr>"

                tab += "<tr>"
                tab += "<td width='50%' align='left' colspan='50'><font size='2'><font face='Cambria'>" + document.getElementById("<%=hdnDate.ClientID %>").value + "</font></font></td><td width='50%' align='right' colspan='50'><font size='2'><font face='Cambria'>" + document.getElementById("<%=hdnTime.ClientID %>").value + "</font></font></td>"
                tab += "</tr>"

                tab += "<tr style='background-color:Silver;'>"
                tab += "<td width='100%' align='center' colspan='100'><font size='2'><font face='Cambria'><b>NRI ACCOUNTS - LIST OF PASSPORT/VISA EXPIRED CASES</b></font></font></td>"
                tab += "</tr>"

                tab += "<tr>"
                tab += "<td colspan='100'><hr></td>"
                tab += "</tr>"

                tab += "<tr></tr>"
                tab += "<tr></tr>"
                tab += "<tr></tr>"

                tab += "<tr style='background-color:WhiteSmoke;'>";
                tab += "<td style='width:2%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='2' rowspan='2'><b>Sl.No</b></td>";
                tab += "<td style='width:8%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='8' rowspan='2'><b>Branch Name</b></td>";
                tab += "<td style='width:6%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='6' rowspan='2'><b>CIF</b></td>";
                tab += "<td style='width:10%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='10' rowspan='2'><b>Customer Name</b></td>";
                tab += "<td style='width:32%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='32'><b>Communication Details</b></td>";
                tab += "<td style='width:10%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='10'><b>Passport Information</b></td>"
                tab += "<td style='width:10%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='10'><b>Visa Information</b></td>";
                tab += "<td style='width:8%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='8' rowspan='2'><b>Country of Residence</b></td>";
                tab += "<td style='width:10%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='10'><b>Action Taken</b></td>";
                tab += "<td style='width:4%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='4' rowspan='2'><b>Save</b></td>";
                tab += "</tr>"

                tab += "<tr>"
                tab += "<td style='width:16%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='16'><b>Communication Address</b></td>";
                tab += "<td style='width:10%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='10'><b>Email ID</b></td>";
                tab += "<td style='width:6%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='6'><b>Mobile No</b></td>";
                tab += "<td style='width:5%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='5'><b>Passport No</b></td>";
                tab += "<td style='width:5%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='5'><b>Passport Expiry Date</b></td>";
                tab += "<td style='width:5%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='5'><b>Visa No</b></td>";
                tab += "<td style='width:5%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='5'><b>Visa Expiry Date</b></td>";
                tab += "<td style='width:5%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='5'><b>Passport SR No</b></td>";
                tab += "<td style='width:5%;text-align:center;border-style:solid;border-color:Silver;border-width:1px' colspan='5'><b>Visa SR No</b></td>";
                tab += "</tr>"

                tab += "<tr></tr>"
                tab += "<tr></tr>"
                tab += "<tr></tr>"
                tab += "<tr></tr>"

                if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                    var row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("£");

                    for (n = 0; n <= row.length - 2; n++) {

                        col = row[n].split("µ");

                        i = n + 1;
                        tab += "<td style='width:2%;text-align:center;border-width:1px;border-style:Solid;' colspan='2'><font size='2'>" + i + "</td>";
                        tab += "<td style='width:8%;text-align:left;word-break: break-all;border-width:1px;border-style:Solid;' colspan='8'><font size='2'>" + col[0] + "</td>";
                        tab += "<td style='width:6%;text-align:left;word-break: break-all;border-width:1px;border-style:Solid;' colspan='6'><font size='2'>" + col[1] + "</td>";
                        tab += "<td style='width:10%;text-align:left;word-break: break-all;border-width:1px;border-style:Solid;' colspan='10'><font size='2'>" + col[2] + "</td>";
                        tab += "<td style='width:16%;text-align:left;word-break: break-all;border-width:1px;border-style:Solid;' colspan='16'><font size='2'>" + col[3] + "</td>";
                        tab += "<td style='width:10%;text-align:left;word-break: break-all;border-width:1px;border-style:Solid;'  colspan='10'><font size='2'>" + col[4] + "</td>";
                        tab += "<td style='width:6%;text-align:left;word-break: break-all;border-width:1px;border-style:Solid;' colspan='6'><font size='2'>" + col[5] + "</td>";
                        tab += "<td style='width:5%;text-align:center;word-break: break-all;border-width:1px;border-style:Solid;'  colspan='5'><font size='2'>" + col[6] + "</td>";
                        if (col[13] == 1)
                            tab += "<td style='width:5%;text-align:center;word-break: break-all;border-width:1px;border-style:Solid;border-color:red'  colspan='5'><font size='2' color='red'>" + col[7] + "</td>";
                        else if (col[13] == 2)
                            tab += "<td style='width:5%;text-align:center;word-break: break-all;border-width:1px;border-style:Solid;border-color:green'  colspan='5'><font size='2' color='green'>" + col[7] + "</td>";
                        else
                            tab += "<td style='width:5%;text-align:center;word-break: break-all;border-width:1px;border-style:Solid;'  colspan='5'><font size='2'>" + col[7] + "</td>";
                        tab += "<td style='width:5%;text-align:center;word-break: break-all;border-width:1px;border-style:Solid;'  colspan='5'><font size='2'>" + col[8] + "</td>";
                        if (col[14] == 1)
                            tab += "<td style='width:5%;text-align:center;word-break: break-all;border-width:1px;border-style:Solid;border-color:red'  colspan='5'><font size='2' color='red'>" + col[9] + "</td>";
                        else if (col[14] == 2)
                            tab += "<td style='width:5%;text-align:center;word-break: break-all;border-width:1px;border-style:Solid;border-color:green'  colspan='5'><font size='2' color='green'>" + col[9] + "</td>";
                        else
                            tab += "<td style='width:5%;text-align:center;word-break: break-all;border-width:1px;border-style:Solid;'  colspan='5'><font size='2'>" + col[9] + "</td>";
                        tab += "<td style='width:8%;text-align:left;word-break: break-all;border-width:1px;border-style:Solid;' colspan='8'><font size='2'>" + col[10] + "</td>";
                        var TxtBox1 = "";
                        if (col[13] == 1)
                            TxtBox1 = "<textarea id='txtPassportSR" + col[1] + "' name='txtPassportSR" + col[1] + "' style='width:99%;align:center;float:left;' maxlength='300' onkeypress='TextAreaCheck(event)'>" + col[11] + "</textarea>";
                        else
                            TxtBox1 = "<textarea id='txtPassportSR" + col[1] + "' name='txtPassportSR" + col[1] + "' style='width:99%;align:center;float:left;' maxlength='300' Disabled='True'>" + col[11] + "</textarea>";
                        tab += "<td style='width:5%;text-align:center;word-break: break-all;border-width:1px;border-style:Solid;' colspan='5'><font size='2'>" + TxtBox1 + "</td>";
                        var TxtBox2 = ""
                        if (col[14] == 1)
                            TxtBox2 = "<textarea id='txtVisaSR" + col[1] + "' name='txtVisaSR" + col[1] + "' style='width:99%;align:center;float:left;' maxlength='300' onkeypress='TextAreaCheck(event)'>" + col[12] + "</textarea>";
                        else
                            TxtBox2 = "<textarea id='txtVisaSR" + col[1] + "' name='txtVisaSR" + col[1] + "' style='width:99%;align:center;float:left;' maxlength='300' Disabled='True'>" + col[12] + "</textarea>";
                        tab += "<td style='width:5%;text-align:center;word-break: break-all;border-width:1px;border-style:Solid;' colspan='5'><font size='2'>" + TxtBox2 + "</td>";
                        if (col[13] == 1 || col[14] == 1)
                            tab += "<td  style='width:4%;text-align:center;border-width:1px;border-style:Solid;colspan:4;'><img id='ViewReport' src='../../../Image/save-button(5).png' onclick='SaveSRNo(" + col[1] + "," + col[13] + "," + col[14] + ")' title='Save'  style='height:30px; width:30px;  cursor:pointer;' >";
                        else
                            tab += "<td  style='width:4%;text-align:center;border-width:1px;border-style:Solid;colspan:4;'><img id='ViewReport' src='../../../Image/tick(4).png' title='Updated'  style='height:30px; width:30px;' >";
                        tab += "</tr>";
                    }
                }

                tab += "</table>";
                tab += "</div></div></div>";

                document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;

                //--------------------- Clearing Data ------------------------//

            }
        </script>

        <form id="form1" runat="server">
            <asp:HiddenField ID="hid_dtls" runat="server" />
            <div style="width: 100%; background-color: white; min-height: 750px; margin: 0px auto;">
                <div style="text-align: right; margin: 0px auto; width: 95%; background-color: white;">
                    <asp:ImageButton ID="cmd_Back" Style="text-align: left; float: left; padding-top: 12px;"
                        runat="server" Height="35px" Width="35px" ImageAlign="AbsMiddle" ImageUrl="~/Image/back.png"
                        ToolTip="Back" />
                    <asp:ImageButton ID="cmd_Print" Style="text-align: right; padding-top: 8px;" runat="server"
                        Height="40px" Width="40px" ImageAlign="AbsMiddle" ImageUrl="~/Image/print.png"
                        ToolTip="Click to Print" />
                    <%--<div id="MenuContainer" style="height: 40px; width: 40px; float: right;">
                        <ul class="menu">
                            <li><a href="#"><span>
                                <asp:ImageButton ID="ImgBtnExport" Style="text-align: right;" runat="server" Height="30px"
                                    Width="30px" ImageAlign="AbsMiddle" ImageUrl="~/Image/Export.png" ToolTip="Export"
                                    OnClientClick="ImgBtnExport_Click()" /></span></a>
                                <ul class="menu-hover">
                                </ul>
                            </li>
                        </ul>
                    </div>--%>
                </div>
                <div id='loadingmsg' style="text-align: center; display: none; margin: 0px auto;">
                    <img alt="progress" src="../../../image/Progress.gif"  style="width: 60px; height: 60px; text-align: center; background-color: #FFF;" />
                    <b>&nbsp;&nbsp;Updating Please wait....</b>                    
                        <br />
                    <asp:Label ID="lblWait" runat="server" Style="color: #FFF"></asp:Label>
                </div>
                <div id='loadingover' style='display: none;'></div>
                <br style="background-color: white" />
                <div style="text-align: center; margin: 0px auto; width: 95%; background-color: white; font-family: Cambria">
                    <asp:Panel ID="pnDisplay" runat="server" Width="100%">
                    </asp:Panel>
                </div>
            </div>
            <asp:HiddenField ID="hdnDate" runat="server" />
            <asp:HiddenField ID="hdnTime" runat="server" />
            <asp:HiddenField ID="hdnBranch" runat="server" />
            <asp:HiddenField ID="hdnBrCode" runat="server" />
            <asp:HiddenField ID="hdnSRType" runat="server" />
            <asp:HiddenField ID="hdnFromDT" runat="server" />
            <asp:HiddenField ID="hdnToDT" runat="server" />
        </form>
    </body>
    </html>
</asp:Content>
