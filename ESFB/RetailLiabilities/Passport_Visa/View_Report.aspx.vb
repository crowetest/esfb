﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports ClosedXML.Excel
Partial Class RetailLiabilities_Passport_Visa_Report_View_Report
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Dim ViewType As Integer
            Dim Branch As String
            Dim BranchCode As String
            Dim SRType As String
            Dim From_Dt As String
            Dim To_Dt As String

            ViewType = CInt(GN.Decrypt(Request.QueryString.Get("ViewType")))
            Branch = CStr(GN.Decrypt(Request.QueryString.Get("Branch")))
            BranchCode = CStr(GN.Decrypt(Request.QueryString.Get("BranchCode")))
            SRType = CStr(GN.Decrypt(Request.QueryString.Get("SR_Type")))
            From_Dt = CStr(GN.Decrypt(Request.QueryString.Get("From_Date")))
            To_Dt = CStr(GN.Decrypt(Request.QueryString.Get("To_Date")))

            hdnBranch.Value = Branch
            hdnBrCode.Value = BranchCode
            hdnSRType.Value = SRType
            hdnFromDT.Value = From_Dt
            hdnToDT.Value = To_Dt

            hdnDate.Value = DateTime.Today.ToString("dd-MMM-yyyy")
            hdnTime.Value = TimeOfDay.ToString("h:mm:ss tt")

            If ViewType = 1 Then
                Export_Excel_Click()
            End If

            If ViewType = -1 Then
                View_Details()
            End If

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)

        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Select Case CInt(Data(0))
            Case 1
                Try
                    Dim Data1() As String = Data(1).Split(CChar("ʘ"))
                    Dim Params(5) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.VarChar, 20)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@CIF", SqlDbType.VarChar, 50)
                    Params(1).Value = Data1(0).ToString()
                    Params(2) = New SqlParameter("@Passport_SR", SqlDbType.VarChar, 50)
                    Params(2).Value = Data1(1).ToString()
                    Params(3) = New SqlParameter("@Visa_SR", SqlDbType.VarChar, 50)
                    Params(3).Value = Data1(2).ToString()
                    Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 2000)
                    Params(5).Direction = ParameterDirection.Output

                    DB.ExecuteNonQuery("SP_PASSPORT_VISA_UPDATE", Params)
                    ErrorFlag = CInt(Params(4).Value)
                    Message = CStr(Params(5).Value)

                    If ErrorFlag = 1 Then
                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString(), False)
                    End If

                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString(), False)
                End Try

                CallBackReturn = ErrorFlag.ToString + "Ø" + Message
                'Case 2
                '    Export_Excel_Click()
        End Select
    End Sub
#End Region

#Region "Functions"

    Protected Sub View_Details()
        Dim BranchCode As String
        Dim SRType As String
        Dim From_Dt As String
        Dim To_Dt As String

        Try

            hid_dtls.Value = ""

            BranchCode = CStr(hdnBrCode.Value)
            SRType = CStr(hdnSRType.Value)
            From_Dt = If(hdnFromDT.Value.Trim().Length > 0, Convert.ToDateTime(hdnFromDT.Value).ToString("dd/MM/yyyy"), Nothing)
            To_Dt = If(hdnToDT.Value.Trim().Length > 0, Convert.ToDateTime(hdnToDT.Value).ToString("dd/MM/yyyy"), Nothing)

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim StrDtl As String = ""

            Dim Parameters1 As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@BranchCode", BranchCode),
                                                       New System.Data.SqlClient.SqlParameter("@SR_Type", SRType),
                                                       New System.Data.SqlClient.SqlParameter("@From_Dt", From_Dt),
                                                       New System.Data.SqlClient.SqlParameter("@To_Dt", To_Dt)}

            DT = DB.ExecuteDataSet("SP_PASSPORT_VISA_GET_EXPIRY_DATA", Parameters1).Tables(0)

            For n As Integer = 0 To DT.Rows.Count - 1

                StrDtl += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString

                StrDtl += "£"

            Next

            hid_dtls.Value = StrDtl

        Catch ex As Exception
            Response.Redirect("~/CatchException.aspx?ErrorNo=0", False)
        End Try

    End Sub

    Protected Sub Export_Excel_Click()
        Try

            Dim WorkBook As XLWorkbook = New XLWorkbook
            Dim WorkSheet = WorkBook.Worksheets.Add("Sheet 1")

            Dim httpResponse = Response

            Dim i As Integer = 0

            Dim BranchCode As String
            Dim SRType As String
            Dim From_Dt As String
            Dim To_Dt As String

            BranchCode = CStr(hdnBrCode.Value)
            SRType = CStr(hdnSRType.Value)
            From_Dt = If(hdnFromDT.Value.Trim().Length > 0, Convert.ToDateTime(hdnFromDT.Value).ToString("dd/MM/yyyy"), Nothing)
            To_Dt = If(hdnToDT.Value.Trim().Length > 0, Convert.ToDateTime(hdnToDT.Value).ToString("dd/MM/yyyy"), Nothing)

            WorkSheet.Cell("A1").SetValue("Branch Name")
            WorkSheet.Cell("B1").SetValue("CIF")
            WorkSheet.Cell("C1").SetValue("Customer Name")
            WorkSheet.Cell("D1").SetValue("Communication Details")
            WorkSheet.Cell("G1").SetValue("Passport Information")
            WorkSheet.Cell("I1").SetValue("Visa Information")
            WorkSheet.Cell("K1").SetValue("Country of Residence")
            WorkSheet.Cell("L1").SetValue("Action Taken")

            WorkSheet.Range("D1:F1").Merge()
            WorkSheet.Range("G1:H1").Merge()
            WorkSheet.Range("I1:J1").Merge()
            WorkSheet.Range("L1:M1").Merge()

            WorkSheet.Range("A1:A2").Merge()
            WorkSheet.Range("B1:B2").Merge()
            WorkSheet.Range("C1:C2").Merge()
            WorkSheet.Range("K1:K2").Merge()

            WorkSheet.Cell("D2").SetValue("Communication Address")
            WorkSheet.Cell("E2").SetValue("Email ID")
            WorkSheet.Cell("F2").SetValue("Mobile")
            WorkSheet.Cell("G2").SetValue("Passport Number")
            WorkSheet.Cell("H2").SetValue("Passport Expiry Date")
            WorkSheet.Cell("I2").SetValue("Visa Number")
            WorkSheet.Cell("J2").SetValue("Visa Expiry Date")
            WorkSheet.Cell("L2").SetValue("Passport SR No")
            WorkSheet.Cell("M2").SetValue("Visa SR No")

            WorkSheet.Range("A1:M1").Style.Font.Bold = True
            WorkSheet.Range("A2:M2").Style.Font.Bold = True

            WorkSheet.Range("A1:M1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
            'WorkSheet.Range("A1:M1").Style.Fill.BackgroundColor = XLColor.AshGrey
            'WorkSheet.Range("A2:M2").Style.Fill.BackgroundColor = XLColor.AshGrey

            'WorkSheet.Range("A1:M1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin
            'WorkSheet.Range("A2:M2").Style.Border.OutsideBorder = XLBorderStyleValues.Thin

            WorkSheet.Columns.AdjustToContents()

            Dim j As Integer = 3

            Dim Parameters1 As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@BranchCode", BranchCode),
                                                           New System.Data.SqlClient.SqlParameter("@SR_Type", SRType),
                                                           New System.Data.SqlClient.SqlParameter("@From_Dt", From_Dt),
                                                           New System.Data.SqlClient.SqlParameter("@To_Dt", To_Dt)}
            DT = DB.ExecuteDataSet("SP_PASSPORT_VISA_GET_EXPIRY_DATA", Parameters1).Tables(0)

            For m As Integer = 0 To DT.Rows.Count - 1
                Dim k As Integer = j
                WorkSheet.Cell("A" + k.ToString()).SetValue(DT(m)(0).ToString())
                WorkSheet.Cell("B" + k.ToString()).SetValue(DT(m)(1).ToString())
                WorkSheet.Cell("C" + k.ToString()).SetValue(DT(m)(2).ToString())
                WorkSheet.Cell("D" + k.ToString()).SetValue(DT(m)(3).ToString())
                WorkSheet.Cell("E" + k.ToString()).SetValue(DT(m)(4).ToString())
                WorkSheet.Cell("F" + k.ToString()).SetValue(DT(m)(5).ToString())
                WorkSheet.Cell("G" + k.ToString()).SetValue(DT(m)(6).ToString())
                WorkSheet.Cell("H" + k.ToString()).SetValue(DT(m)(7).ToString())
                WorkSheet.Cell("I" + k.ToString()).SetValue(DT(m)(8).ToString())
                WorkSheet.Cell("J" + k.ToString()).SetValue(DT(m)(9).ToString())
                WorkSheet.Cell("K" + k.ToString()).SetValue(DT(m)(10).ToString())
                WorkSheet.Cell("L" + k.ToString()).SetValue(DT(m)(11).ToString())
                WorkSheet.Cell("M" + k.ToString()).SetValue(DT(m)(12).ToString())

                j += 1

            Next

            Dim today As Date = Date.Today()
            Dim filename = "NRIReport" + today.ToString("ddMMMyyyy") + ".xls"

            httpResponse.Clear()
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            httpResponse.AddHeader("content-disposition", "attachment;filename=""" + filename + """")
            Using tmpMemoryStream As MemoryStream = New MemoryStream()
                WorkBook.SaveAs(tmpMemoryStream)
                tmpMemoryStream.WriteTo(httpResponse.OutputStream)
                tmpMemoryStream.Close()
            End Using
            HttpContext.Current.Response.Flush()
            HttpContext.Current.Response.SuppressContent = True
            HttpContext.Current.ApplicationInstance.CompleteRequest()

        Catch ex As Exception
            Dim i As Integer = 0
            i = i + 1
        End Try
    End Sub

#End Region

End Class
