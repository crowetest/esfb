﻿Imports System.Data
Imports System.IO
Imports System.Net
Imports System.Data.SqlClient
Partial Class RetailLiabilities_Passport_Visa_HO_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim GN As New GeneralFunctions
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1475) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "HO Report"
            If Not IsPostBack Then
                Dim UserID As String = CStr(Session("UserID"))
                DT = DB.ExecuteDataSet("SELECT -2 as branch_id,'--ALL--' as branch_name union all select branch_id,concat(upper(branch_name),'(',branch_id,')') as branch_name from branch_master where status_id = 1 order by branch_name").Tables(0)
                GF.ComboFill(cmbBranch, DT, 0, 1)
                DT = DB.ExecuteDataSet("select status_id SR_id,status_name SR_type from passport_visa_master where category_Flg=2 and status_Flg=1 order by custom_order").Tables(0)
                GF.ComboFill(cmbSRType, DT, 0, 1)
                Me.txt_From_Dt.Text = Date.Today.ToString("dd MMM yyyy")
                Me.txt_To_Dt.Text = Date.Today.ToString("dd MMM yyyy")
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
        DB.dispose()
    End Sub
End Class
