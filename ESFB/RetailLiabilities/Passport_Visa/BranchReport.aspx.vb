﻿Imports System.Data
Imports System.IO
Imports System.Net
Imports System.Data.SqlClient
Partial Class RetailLiabilities_Passport_Visa_BranchReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim GN As New GeneralFunctions
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1474) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Branch Report"
            If Not IsPostBack Then
                Dim UserID As String = CStr(Session("UserID"))

                Dim Params(0) As SqlParameter
                Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(0).Value = CInt(Session("UserID"))

                DT = DB.ExecuteDataSet("SELECT a.branch_id,concat(upper(b.branch_name),'(',a.branch_id,')') branch_name from emp_master a inner join branch_master b on b.branch_id = a.branch_id where emp_code = @EmpCode", Params, 1).Tables(0)
                If DT.Rows.Count > 0 Then
                    txtBranchCode.Text = DT.Rows(0)(0).ToString()
                    txtBranchName.Text = DT.Rows(0)(1).ToString()
                End If
                DT = DB.ExecuteDataSet("select status_id SR_id,status_name SR_type from passport_visa_master where category_Flg=1 and status_Flg=1 order by custom_order").Tables(0)
                GF.ComboFill(cmbSRType, DT, 0, 1)
                Me.txt_From_Dt.Text = Date.Today.ToString("dd MMM yyyy")
                Me.txt_To_Dt.Text = Date.Today.ToString("dd MMM yyyy")
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
        DB.dispose()
    End Sub
End Class
