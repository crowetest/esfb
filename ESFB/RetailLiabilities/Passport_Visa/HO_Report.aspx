﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="HO_Report.aspx.vb" Inherits="RetailLiabilities_Passport_Visa_HO_Report" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../../Style/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css" />
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:30%;">
                &nbsp;
            </td>
            <td  style="width:70%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                Branch</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" Width="81%" ForeColor="Black">
                </asp:DropDownList>               
            </td>           
        </tr>
        <tr>
            <td style="width:30%;">
                &nbsp;
            </td>
            <td  style="width:70%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                SR Number Type</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbSRType" class="NormalText" runat="server" Font-Names="Cambria" 
                onchange="return SRTypeOnChange()" Width="81%" ForeColor="Black">
                </asp:DropDownList>               
            </td>           
        </tr>
        <tr>
            <td style="width:30%;">
                &nbsp;
            </td>
            <td  style="width:70%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                Date From</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:TextBox ID="txt_From_Dt" class="NormalText" runat="server" 
                    Width="80%" onkeypress="return false" ReadOnly="true"></asp:TextBox>
                <asp:CalendarExtender ID="txt_From_Dt_CalendarExtender" runat="server" OnClientShowing="setStartDate"
                 Enabled="True" TargetControlID="txt_From_Dt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>           
        </tr>
        <tr>
            <td style="width:30%;">
                &nbsp;
            </td>
            <td  style="width:70%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                Date To</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:TextBox ID="txt_To_Dt" class="NormalText" runat="server"
                    Width="80%" onkeypress="return false" ReadOnly="true"></asp:TextBox>
                <asp:CalendarExtender ID="txt_To_Dt_CalendarExtender" runat="server"
                 Enabled="True" TargetControlID="txt_To_Dt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>           
        </tr>
            <td style="width:30%;">
                &nbsp;
            </td>
            <td  style="width:70%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:100%; height: 18px; text-align:center;" colspan="2">             
                <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />
                &nbsp;&nbsp;
                <input id="cmd_Export_Excel" style="font-family: Cambria; cursor: pointer; width: 87px;" 
                type="button" value="VIEW EXCEL" onclick="return btnExcelView_onclick()" />
                &nbsp;&nbsp;
                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 10%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
            </td>            
        </tr>
    <tr>
        <td style="width:30%;">
            &nbsp;</td>
         <td  style="width:70%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width:30%;">
            &nbsp;</td>
        <td  style="width:70%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width:30%;">
            &nbsp;</td>
         <td  style="width:70%;">
            <asp:HiddenField ID="hdnReportID" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="width:30%;">
            &nbsp;
        </td>
         <td  style="width:70%;">
            &nbsp;
        </td>
        <asp:HiddenField ID="hdnValue" runat="server" />
    </tr>
    </table>
    <script language="javascript" type="text/javascript">
     function window_onload()
     {
        SRTypeOnChange();
     }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }               
   function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 1; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }       
    function SRTypeOnChange(){
        var SR_Type = document.getElementById("<%= cmbSRType.ClientID %>").value;   
        if(SR_Type==2){   
            document.getElementById("<%= txt_From_Dt.ClientID %>").disabled=false;
            document.getElementById("<%= txt_To_Dt.ClientID %>").disabled=false;            

            document.getElementById("<%= txt_From_Dt.ClientID %>").value=GetTodaysDate();
            document.getElementById("<%= txt_To_Dt.ClientID %>").value=GetTodaysDate();
        }
        else{
            document.getElementById("<%= txt_From_Dt.ClientID %>").disabled=true;
            document.getElementById("<%= txt_To_Dt.ClientID %>").disabled=true;
            document.getElementById("<%= txt_From_Dt.ClientID %>").value='';
            document.getElementById("<%= txt_To_Dt.ClientID %>").value='';
        }
    }    
    function GetTodaysDate()
    {
        var today=new Date();
            var dd=String(today.getDate()).padStart(2,'0');
            var monthIndex=String(today.getMonth());
            var yyyy=today.getFullYear();

            var monthNames=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            var mm=monthNames[monthIndex];

            today=dd+' '+mm+' '+yyyy;

            return today;
    }
    //ViewType = -1 for normal report view
    //ViewType = 1 for excel report view
    function btnView_onclick(){
        var BranchCode=document.getElementById("<%= cmbBranch.ClientID %>").value;
        var SRType = document.getElementById("<%= cmbSRType.ClientID %>").value;
        var From_Date = document.getElementById("<%= txt_From_Dt.ClientID %>").value; 
        var To_Date = document.getElementById("<%= txt_To_Dt.ClientID %>").value;
        var From_Dt=new Date(From_Date).getTime()
        var To_Dt=new Date(To_Date).getTime()
        if(From_Dt>To_Dt)
        {
            alert("Please Check The Dates !")
            document.getElementById("<%= txt_To_Dt.ClientID %>").focus();
            return false
        }
        window.open("Report/View_Report.aspx?ViewType=" + btoa(-1) + "&Branch=" + btoa(0) + "&BranchCode=" + btoa(BranchCode) + "&From_Date=" + btoa(From_Date) + " &To_Date=" + btoa(To_Date) + " &SR_Type=" + btoa(SRType) +  " ", "_self");                   
    }    
   function btnExcelView_onclick(){
        var BranchCode=document.getElementById("<%= cmbBranch.ClientID %>").value;
        var SRType = document.getElementById("<%= cmbSRType.ClientID %>").value;
        var From_Date = document.getElementById("<%= txt_From_Dt.ClientID %>").value; 
        var To_Date = document.getElementById("<%= txt_To_Dt.ClientID %>").value;
        var From_Dt=new Date(From_Date).getTime()
        var To_Dt=new Date(To_Date).getTime()
        if(From_Dt>To_Dt)
        {
            alert("Please Check The Dates !")
            document.getElementById("<%= txt_To_Dt.ClientID %>").focus();
            return false
        }
        window.open("Report/View_Report.aspx?ViewType=" + btoa(1) + "&Branch=" + btoa(0) + "&BranchCode=" + btoa(BranchCode) + "&From_Date=" + btoa(From_Date) + " &To_Date=" + btoa(To_Date) + " &SR_Type=" + btoa(SRType) +  " ", "_self");                   
    }   
    function FromServer(arg, context) {
        switch (context) {
            case 1:
                {    
                    ComboFill(arg, "<%= cmbBranch.ClientID %>");               
                    ComboFill(arg, "<%= cmbSRType.ClientID %>");
                    break;    
                }               
            case 2:
                {    
                    
                }  
        }
    }
    function setStartDate(sender, args) {                    
        var Today = new Date();
        sender._endDate = Today;            
    }
    </script>
            <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            window_onload()
            // ]]>
        </script>

    </script>
</asp:Content>