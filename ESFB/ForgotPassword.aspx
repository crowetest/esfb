﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ForgotPassword.aspx.vb" Inherits="ForgotPassword" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
<title>Forgot Password</title>
<link href="Style/Style.css" type="text/css" rel="Stylesheet"/>
<style type="text/css">
body 
    {
	    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	    font-size: 13px;
	    color: #B21C32;
	    background-color:#b21c32;
	    line-height: 18px;
	    height: 100%;
	    margin-top: 15px;
    }
        .style1 
	{
            width: 100%;
        }
        .style2 
        {
        	width: 25%;
        }
        .style3
        {
            font-size: x-large;
            font-family: Cambria;
            color: #000066;
        }
        .style6
        {
            width: 25%;
            font-size: x-large;
            font-family: Cambria;
            color: #000066;
        }
        .style7
        {
            font-family: Cambria;
            font-size: medium;
            text-align: left;
        }
        .style12
        {
        	width: 25%;
            height: 23px;
            text-align: left;
            font-family: Georgia;
        }
        .style13
        {
            width: 12%;
            height: 23px;
            text-align: right;
            font-family: Georgia;
        }
        .style15
        {
            width: 25%;
            height: 30px;
            text-align: center;
            font-family: Georgia;
        }
        .style16
        {
            width: 12%;
            height: 29px;
            text-align: right;
            font-family: Cambria;
        }
        .style17
        {
        	width: 25%;
            height: 30px;
            text-align: right;
            font-family: Cambria;
        }
        .style18
        {
            width: 25%;
            height: 30px;
            text-align: left;
        }
        .style19
        {
            font-family: "Times New Roman", Times, serif;
            font-size: medium;
           
        }
        .style20
        {
            width: 25%;
            height: 29px;
            text-align: center;
            color: #999999;
            font-family: "Times New Roman", Times, serif;
            font-size: small;
        }
        VeryPoorStrength
        {
            background: Red;
            color:white;
            font-weight:bold;
        }
        .WeakStrength
        {
            background: Red;
            color:White;
            font-weight:bold;
        }
        .AverageStrength
        {
            background: orange;
            color:black;
            font-weight:bold;
        }
        .GoodStrength
        {
            background: blue;
            color:White;
            font-weight:bold;
        }
        .ExcellentStrength
        {
            background: Green;
            color:White;
            font-weight:bold;
        }
        .BarBorder
        {
            border-style: solid;
            border-width: 0px;
            width: 180px;
            padding:0px;
        }
        .BarIndicator
        {
	        height:1px;
        }
        .style21
        {
            width: 12%;
            height: 25px;
            text-align: right;
            font-family: Cambria;
        }
        .style22
        {
            width: 25%;
            height: 25px;
            text-align: left;
        }
        .style26
        {
            width: 25%;
            height: 29px;
            text-align: center;
        }
        .style27
        {
            width: 12%;
            height: 26px;
            text-align: right;
            font-family: Georgia;
        }
        .style28
        {
            width: 25%;
            height: 26px;
            text-align: left;
        }
        .style29
        {
            font-family: Georgia;
            font-size: small;
        }
        .style30
        {
            font-family: Georgia;
        }
    </style>
<script type="text/javascript" src="Script/jquery-1.2.6.min.js"></script>
<script src="Script/Validations.js" language="javascript" type="text/javascript"> </script>  

<script language="javascript">
    document.onmousedown = disableclick;
    status = "Right Click Disabled";
    function disableclick(event) {
        if (event.button == 2) {
            alert(status);
            return false;
        }
    }
</script>



  <script language="javascript" type="text/javascript">
      var Mobileno;


      var ip;
      var RTCPeerConnection = window.webkitRTCPeerConnection || window.mozRTCPeerConnection;

      if (RTCPeerConnection) (function () {
          var rtc = new RTCPeerConnection({ iceServers: [] });

          if (1 || window.mozRTCPeerConnection) {
              rtc.createDataChannel('', { reliable: false });
          };

          rtc.onicecandidate = function (evt) {

              if (evt.candidate) grepSDP("a=" + evt.candidate.candidate);
          };
          rtc.createOffer(function (offerDesc) {
              grepSDP(offerDesc.sdp);
              rtc.setLocalDescription(offerDesc);
          }, function (e) { console.warn("offer failed", e); });


          var addrs = Object.create(null);
          addrs["0.0.0.0"] = false;
          function updateDisplay(newAddr) {
              if (newAddr in addrs) return;
              else addrs[newAddr] = true;
              var displayAddrs = Object.keys(addrs).filter(function (k)
              { return addrs[k]; });
              ip = displayAddrs.join(" | ") || "n/a";


          }

          function grepSDP(sdp) {
              var hosts = [];
              sdp.split('\r\n').forEach(function (line) {
                  if (~line.indexOf("a=candidate")) {
                      var parts = line.split(' '),
                    addr = parts[4],
                    type = parts[7];
                      if (type === 'host') updateDisplay(addr);
                  } else if (~line.indexOf("c=")) {
                      var parts = line.split(' '),
                    addr = parts[2];
                      updateDisplay(addr);
                  }
              });
          }
      })(); else {


      }
      function RequestOnchange() {

          var Data = document.getElementById("<%= hid_data.ClientID %>").value.split("^");
          //          document.getElementById("<%= txtName.ClientID %>").value = Data[1];
          var name = Data[1];
          var length = name.length - 1;
          middle = '*'.repeat(length);
          var mask = name[0] + middle + name[name.length - 1];

          document.getElementById("<%= txtName.ClientID %>").value = mask;
          document.getElementById("<%= txtBranch.ClientID %>").value = Data[3];
          document.getElementById("<%= txtDepartment.ClientID %>").value = Data[5];
          document.getElementById("<%= txtDesignation.ClientID %>").value = Data[6];

      }
      function RequestID() {
          var EmpID = document.getElementById("<%= txtEmpcode.ClientID %>").value;
          if (EmpID > 0) {
              var ToData = "2Ø" + EmpID;
              ToServer(ToData, 2);
          }
      }

      function FromServer(arg, context) {

          if (context == 1) {
              var Data = arg.split("Ø");
              alert(Data[1]);
              if (Data[0] == 0) window.open("ChangePassword.aspx", "_self");
          }
          else if (context == 2) {
              var Data = arg.split("~");

              if (Data[0] == 1) {
                  document.getElementById("<%= hid_data.ClientID %>").value = Data[1];
                  RequestOnchange();
              }
              else if (Data[0] == 2) {
                  alert(Data[1]);
                  document.getElementById("<%= txtName.ClientID %>").value = "";
                  document.getElementById("<%= txtBranch.ClientID %>").value = "";
                  document.getElementById("<%= txtDepartment.ClientID %>").value = "";
                  document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                  document.getElementById("<%= txtEmpcode.ClientID %>").value = "";

                  document.getElementById("<%= txtEmpcode.ClientID %>").focus();

              }

          }
          else if (context == 3) {

              var Data = arg.split("Ø");
              document.getElementById("<%= hid_OTP.ClientID %>").value = "";

              
              if(Data=="NOMOBILE")
              {
                  alert("Your mobile number is not updated in the System.Please Contact HR.");
                  document.getElementById("<%= txtName.ClientID %>").value = "";
                  document.getElementById("<%= txtBranch.ClientID %>").value = "";
                  document.getElementById("<%= txtDepartment.ClientID %>").value = "";
                  document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                  document.getElementById("<%= txtEmpcode.ClientID %>").value = "";

                  document.getElementById("<%= txtEmpcode.ClientID %>").focus();
              }
              else if (Data[0] != 0) {
           
                  Mobileno = Data[1].toString();
                  alert("OTP send to your registered mobile XXXXXX" + Data[1].slice(-4) + ". Please enter OTP.");
                  document.getElementById("<%= hid_OTP.ClientID %>").value = Data[0];

              }
              else {
                  alert("Sending Failed.");
              }
          }
      }
      function btnExit_onclick() {
          window.open("Default.aspx", "_self");
      }
      function btnConfirm_onclick() {
          debugger;
          document.getElementById("btnOK").disabled = false;
          document.getElementById("btnConfirm").disabled = true;
          if (document.getElementById("<%= txtEmpcode.ClientID %>").value == "") {
              alert("Select Employee Code");
              document.getElementById("<%= txtEmpcode.ClientID %>").focus();
              return false;
          }
          var EmpID = document.getElementById("<%= txtEmpcode.ClientID %>").value;
          var ToData = "3Ø" + EmpID;
          ToServer(ToData, 3); // create OPT
          countdownTimer();
         
      }
      function btnOK_onclick() {


          if (document.getElementById("<%= txtEmpcode.ClientID %>").value == "") {
              alert("Select Employee Code");
              document.getElementById("<%= txtEmpcode.ClientID %>").focus();
              return false;
          }
          if (document.getElementById("<%= txtOTP.ClientID %>").value == "") {
              alert("Enter OTP");
              document.getElementById("<%= txtEmpcode.ClientID %>").focus();
              return false;
          }
          var EmpID = document.getElementById("<%= txtEmpcode.ClientID %>").value;
          var OTP = document.getElementById("<%= txtOTP.ClientID %>").value
          var HidOTP = document.getElementById("<%= hid_OTP.ClientID %>").value
          if (OTP != HidOTP) {
              alert("Invalid OTP");
              document.getElementById("<%= txtOTP.ClientID %>").focus();
              return false;
          }
          
          var ToData = "1Ø" + EmpID + "Ø" + OTP + "Ø" + Mobileno + "Ø" + ip;
          ToServer(ToData, 1);
      }
    </script>

       <script>
           debugger;
           var xx;
           function countdownTimer() {

               document.getElementById("lblmsg").innerHTML = "";
               document.getElementById("minutes").innerHTML = "00";
               document.getElementById("seconds").innerHTML = "00";
               var minutesLabel = document.getElementById("minutes");
               var secondsLabel = document.getElementById("seconds");
               var totalSeconds = 0;
               xx = setInterval(setTime, 1000);             
               function setTime() {

                   secondsLabel.innerHTML = pad(totalSeconds % 60);
                   minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
                   if (minutesLabel.innerHTML == "02") {
                       secondsLabel.innerHTML = "00";
                       minutesLabel.innerHTML = "00";
                       minutesLabel = "00";
                       minutesLabel = "00";
                       document.getElementById("lblmsg").innerHTML = "Time is Up";
                       document.getElementById("btnOK").disabled = true;
                       document.getElementById("btnConfirm").disabled = false;
                       alert("Time is Up, Please regenerate OTP");
                       clearInterval(xx);
                      
                   }
                   else {
                       ++totalSeconds;
                   }

               }

               function pad(val) {

                   var valString = val + "";
                   if (valString.length < 2) {
                       return "0" + valString;
                   } else {
                       return valString;
                   }

               }
           }

    </script>
</head>
<body style="background-color:#E31E24">
<form id="form1" runat="server">
<div style="height:150px; "><img src="Image/Logo.png" style="width:450px; height:150px;">
</div>
<div id="MainContents" style="background-color:White; height:370px; width:100%">
    <br />
    <asp:HiddenField ID="hid_data" runat="server" />
    <br />

 <table class="style1" style="width:80%;margin: 0px auto;">
    <tr> <td style="width:25%;"></td>
    <td style="width:12%; text-align:left;">Employee Code</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpcode" class="NormalText" runat="server" Width="15%" 
                MaxLength="5"  ></asp:TextBox>
            </td>
    </tr>
        <tr>
        <td style="width:25%;"></td>
         <td style="width:12%; text-align:left;">Name</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr style="display:none;"> 
       <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Location</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr style="display:none;"> 
       <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Department</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDepartment" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr style="display:none;"> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Designation</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesignation" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 100px;" 
                type="button" value="GENERATE OTP" onclick="return btnConfirm_onclick()"/>&nbsp;
              
                <label id="minutes"  style="font-size: medium; color: #FF0000;" >00</label>:<label id="seconds" style="font-size: medium; color: #FF0000;">00</label>&nbsp;&nbsp;
                <asp:Label ID="lblmsg" runat="server" Text="" ForeColor="#FF3300"></asp:Label> </td>
        </tr>
        <tr> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Enter OTP</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtOTP" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
<tr>
            <td style="text-align:center;" colspan="3"><br />
                 <input id="btnOK" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE" onclick="return btnOK_onclick()"/>&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />           
                 </td>
        </tr>
        <asp:HiddenField ID="hid_OTP" runat="server" />
</table>    
</div>
<div id="FooterContainer" style="float:right;">
</div>
</form>
</body>
</html>

