﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="CatchException.aspx.vb" Inherits="AccessDenied" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Access Denied !</title>
    <style type="text/css">
        .style1
        {
            color: #E31E24;
            text-align:center;
            height : auto;
            font-family:Cambria;
            font-size:xx-large;

        }
        .style2
        {
            color: #E31E24;
            text-align:center;
            height : auto;
            font-family:Cambria;
            font-size:medium;
            font-weight:bold;
        }
    </style>
    <script type="text/javascript" language="javascript" >
        function BackOnClick() {
        if(document.getElementById("<%= hdnFlg.ClientID %>").value==1)
        {window.open("Default.aspx","_self");return false;}
        else{
            window.open("Home.aspx", "_self"); return false;
        }

        }
    </script>
</head>
<body onload="pageInit(); width:100%; height:100%"  class="style2">
    <form id="login_form" runat="server">
        <div style="height:200px; padding-top:0px; padding-top:10px; padding-bottom:10px;"></div>
        <div class="style1">
            <asp:HiddenField ID="hdnFlg" runat="server" />
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <br /><br /><br /><br /></div>
        <div class="style2">Please Raise a HelpDesk Ticket with this Error Code&nbsp;For 
            Speedy Resolution.&nbsp;&nbsp;&nbsp;&nbsp;<asp:HyperLink 
                ID="hlBack" runat="server" href="" onclick="return BackOnClick()" >Back</asp:HyperLink>
            <br /><br /></div>
        <div class="style2"></div>
    </form>
</body>
</html>
</asp:Content>
