﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient

Partial Public Class ResetPassword1
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DB As New MS_SQL.Connect
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim LoginUSerID As Integer
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim StrValue As String
        Dim Type As Integer
        If CInt(Data(0)) = 1 Then
            StrValue = ""
            Type = 1
            LoginUSerID = CInt(Data(1))
        Else
            StrValue = GN.ResetPasswordString()
            Type = 2
        End If

        Dim StrPassword As String = EncryptDecrypt.Encrypt("Esaf123", LoginUSerID.ToString())
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try
            Dim Params(6) As SqlParameter
            Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(0).Value = LoginUSerID
            Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(1).Direction = ParameterDirection.Output
            Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(2).Direction = ParameterDirection.Output
            Params(3) = New SqlParameter("@Passwd", SqlDbType.VarChar, 5000)
            Params(3).Value = StrPassword
            Params(4) = New SqlParameter("@UserData", SqlDbType.VarChar, 5000)
            Params(4).Value = StrValue
            Params(5) = New SqlParameter("@Type", SqlDbType.Int)
            Params(5).Value = Type
            Params(6) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(6).Value = UserID
            DB.ExecuteNonQuery("SP_Reset_Password", Params)

            ErrorFlag = CInt(Params(1).Value)
            Message = CStr(Params(2).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message


    End Sub

    Protected Sub ResetPassword_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

    End Sub
End Class
