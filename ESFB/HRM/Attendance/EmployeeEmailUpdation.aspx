﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="EmployeeEmailUpdation.aspx.vb" Inherits="EmployeeEmailUpdation" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
 <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script> 
    <script language="javascript" type="text/javascript">
    
         function cursorwait(e) {
            document.body.className = 'wait';
        }
           function cursordefault(e) {
            document.body.className = 'default';
        }

        function btnUpdate_onclick() {

            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
         
        var EmpName =document.getElementById("<%= txtEmpName.ClientID %>").value;
       
        var OfficialEmail = document.getElementById("<%= txtOfficialEmail.ClientID %>").value;
        var ConfirmOfficialEmail = document.getElementById("<%= txtconfirmOfficialEmail.ClientID %>").value;
      
        var Cug =document.getElementById("<%= txtCug.ClientID %>").value;
        var Mobile =document.getElementById("<%= txtmobile.ClientID %>").value;
       
            if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "") { alert("Enter Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtEmpName.ClientID %>").value == "") { alert("Enter Employee Name"); document.getElementById("<%= txtEmpName.ClientID %>").focus(); return false; }
           if (document.getElementById("<%= txtOfficialEmail.ClientID %>").value == "") 
            {
                 alert("Enter Official e-mail ID"); 
                 document.getElementById("<%= txtOfficialEmail.ClientID %>").focus(); 
                 return false;
             }
             if (document.getElementById("<%= txtOfficialEmail.ClientID %>").value != "")
            { 
                var ret = checkEmail(document.getElementById("<%= txtOfficialEmail.ClientID %>"));
                if (ret == false) 
                return false;
            }
            if (document.getElementById("<%= txtconfirmOfficialEmail.ClientID %>").value == "") {
                alert("Enter Official e-mail ID");
                document.getElementById("<%= txtconfirmOfficialEmail.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtconfirmOfficialEmail.ClientID %>").value != "") {
                var ret = checkEmail(document.getElementById("<%= txtconfirmOfficialEmail.ClientID %>"));
                if (ret == false)
                    return false;
            }
            if (OfficialEmail != ConfirmOfficialEmail) {

                alert("Official E-mail and Confirm E-mail does not Match");
                document.getElementById("<%= txtconfirmOfficialEmail.ClientID %>").focus();
                return false;
            }
            
           

            if (document.getElementById("<%= txtmobile.ClientID %>").value == "") { alert("Enter Mobile Number"); document.getElementById("<%= txtmobile.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtmobile.ClientID %>").value != "") 
            {
                var ret1 = checkPhone(document.getElementById("<%= txtmobile.ClientID %>"));
                if (ret1 == false)
                 return false;
              }
          
            if (document.getElementById("<%= txtCug.ClientID %>").value != "") {
                var ret1 = checkPhone(document.getElementById("<%= txtCug.ClientID %>"));
                if (ret1 == false)
                    return false;
            }
          
            var ToData = "1|" + EmpCode + "|" + OfficialEmail  + "|" + Cug + "|" + Mobile ;
           
            ToServer(ToData, 1);

        }
         function btnExit_onclick() 
        {
             window.open("ESAFpunching.aspx", "_self");
         }

         function FromServer(arg, Context) {
            switch (Context) {
                case 1:
                    {
                       
                        var Data = arg.split("|");
                        alert(Data[2]);
                        var Msg = Data[3].split("¶");
                        document.getElementById("<%= lblMsg.ClientID %>").innerHTML = Msg[0];
                        if (Data[0] == 0) {
                            document.getElementById("<%= txtEmpCode.ClientID %>").value = "";
                            document.getElementById("<%= txtEmpName.ClientID %>").value = "";
                           
                            document.getElementById("<%= txtOfficialEmail.ClientID %>").value = "";
                            document.getElementById("<%= txtconfirmOfficialEmail.ClientID %>").value = "";
                            document.getElementById("<%= txtmobile.ClientID %>").value = "";
                          
                            document.getElementById("<%= txtCug.ClientID %>").value = "";
                            document.getElementById("btnExit").focus();
                        }
                        if (Msg[1] != "") alert(Msg[1].toString().split("$").join("\n"));
                        break;

                    }
            }
        }
    </script>
    </head>
    </html>
    <br />
    <table align="center" class="style1"
        style="width:60%; margin: 0px auto;">   
    
        <tr>
           <td style="width:15%;"></td>
           <td style="width:15%; text-align:left; "></td>
            <td style="width:15%; text-align:left; "></td>
            <td style="width:15% ;text-align:left;">
                &nbsp;</td>
        </tr>
            
        <tr>    
                <td style="width:15%; text-align:left; "></td>
                <td style="width:15%; text-align:left; ">
                Employee Code</td>
                <td style="width:15%; text-align:left; ">
                <asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="ReadOnlyTextBox"  disabled="true" MaxLength="6" onkeypress="return NumericCheck(event)"></asp:TextBox>
                </td>
                <td style="width:15% ;text-align:left;">
                &nbsp;</td>

         </tr>
       
         <tr>
                 <td style="width:15%; text-align:left; ">
                    &nbsp;</td>
                   <td style="width:15%; text-align:left; ">
                Name</td>
               <td style="width:15%; text-align:left; ">
                <asp:TextBox ID="txtEmpName" runat="server" Width="90%" disabled="true"
                class="ReadOnlyTextBox"></asp:TextBox>
                </td>
                 <td style="width:15%; text-align:left; ">
                &nbsp;</td>
        </tr>
       
            <tr>
                       <td style="width:15%; text-align:left; ">
                        &nbsp;</td>
                     <td style="width:15%; text-align:left; ">
                    Official E-Mail&nbsp;&nbsp;<b style="color:red;">*</b></td>
                      <td style="width:15%; text-align:left; ">
                    <asp:TextBox ID="txtOfficialEmail" oncopy="return false" onpaste="return false"  runat="server" 
                    Width="90%"  class="NormalText" MaxLength="50"  ></asp:TextBox>
                    </td>
                      <td style="width:15%; text-align:left; ">
                    &nbsp;</td>
             </tr>
             <tr>
                       <td style="width:15%; text-align:left; ">
                        &nbsp;</td>
                     <td style="width:15%; text-align:left; ">
                    Confirm Official E-Mail&nbsp;&nbsp;<b style="color:red;">*</b></td>
                      <td style="width:15%; text-align:left; ">
                    <asp:TextBox ID="txtconfirmOfficialEmail" oncopy="return false" onpaste="return false"  runat="server" 
                    Width="90%"  class="NormalText" MaxLength="50"  equalTo='#txtOfficialEmail'  equals="txtOfficialEmail" err="Confirm email must be the same as email"></asp:TextBox>
                    </td>
                      <td style="width:15%; text-align:left; ">
                    &nbsp;</td>
             </tr>
           
             <tr>
                      <td style="width:15%; text-align:left; ">
                        &nbsp;</td>
                     <td style="width:15%; text-align:left; ">
                    CUG Mobile&nbsp;&nbsp;&nbsp;<b style="color:red;">*</b></td>
                      <td style="width:15%; text-align:left; ">
                    <asp:TextBox ID="txtCug" oncopy="return false" onpaste="return false" runat="server" 
                    Width="90%" class="NormalText" MaxLength="10"  onkeypress='return NumericCheck(event)'></asp:TextBox>
                    </td>
                    <td style="width:15% ;text-align:left;">
                    &nbsp;</td>
              </tr>
              <tr>
                       <td style="width:15%; text-align:left; ">
                        &nbsp;</td>
                      <td style="width:15%; text-align:left; ">
                    Personal Mobile&nbsp;&nbsp;<b style="color:red;">*</b></td>
                     <td style="width:15%; text-align:left; ">
                    <asp:TextBox ID="txtmobile" oncopy="return false" onpaste="return false" runat="server" 
                    Width="90%" class="NormalText" MaxLength="10"  onkeypress='return NumericCheck(event)'></asp:TextBox>
                    </td>
                      <td style="width:15%; text-align:left; ">
                    &nbsp;</td>
               </tr>
               
           <tr>
           <td style="width:15%;"></td>
           <td style="width:15%; text-align:left; "></td>
            <td style="width:15%; text-align:left; "></td>
            <td style="width:15% ;text-align:left;">
                &nbsp;</td>
        </tr>
            <tr style="text-align:center;">
                    <td  colspan="4" style="text-align:center;">
                    <input id="btnUpdate" type="button" value="UPDATE" 
                    onclick="return btnUpdate_onclick()" 
                    style="font-family: cambria; width: 8%; cursor: pointer;" />&nbsp;                
                    
            
            
          
           <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
              
           
           
        </tr>
            <tr style="text-align:center;">
                    <td  colspan="4" style="text-align:center;">
                          <asp:Label ID="lblMsg" runat="server" Text=""  Font-Bold="True" ForeColor="Red" Font-Names="Cambria" style=" text-align:center; width=80%;font-size=30px;"></asp:Label>
                       <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
                    </td>
                    
</tr>
   
    </table>
  <br /> <br />
  <br /> <br />
  <br /> <br />
</asp:Content>

