﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="LeaveVerification.aspx.vb" Inherits="LeaveVerification" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       
        function table_fill() {

            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:60%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:5%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:30%;text-align:left' >Employee Code</td>";
            tab += "<td style='width:40%;text-align:left'>Name</td>";
            tab += "<td style='width:25%;text-align:left'>Status</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                    //EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time

                    tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                    tab += "<td style='width:30%;text-align:left' >" + col[0] + "</td>";
                    tab += "<td style='width:40%;text-align:left'>" + col[1] + "</td>";

                    var select = "<select id='cmb" + col[0] + "' class='NormalText' name='cmb" + col[0] + "' >";
                    select += "<option value='-1'>Select</option>";
                    select += "<option value='3'>Present</option>";
                    select += "<option value='1'>Absent</option>";
                    select += "<option value='2'>Not Informed</option>";                    
                    tab += "<td style='width:25%;text-align:left'>" + select + "</td>";
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }

        function UpdateValue() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("Staff does not exists for Verify");
                return 0;
            }
            
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");

                var txtName = "cmb" + col[0];
               
                if (document.getElementById("cmb" + col[0]).selectedIndex != 0) {

                    document.getElementById("<%= hid_temp.ClientID %>").value +=  col[0] + "µ" + document.getElementById("cmb" + col[0]).selectedIndex +"¥"  ;
                }
                
            }
           
            return 1;
        }
        function FromServer(arg, context) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("LeaveVerification.aspx", "_self");
        }
        function btnApprove_onclick() {
            var ret = UpdateValue(); 
            if (ret == 0) return false;


            if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                alert("Select Any staff for Verification entry");
                return false;
            }

            var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
            var Data = "1Ø" + strempcode;
            ToServer(Data, 1);
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        

    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="VERIFY" onclick="return btnApprove_onclick()" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

