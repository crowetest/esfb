﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class FSAttendanceApproval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim AT As New Attendance
    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 27) = False And GF.FormAccess(CInt(Session("UserID")), 173) = False And GF.FormAccess(CInt(Session("UserID")), 174) = False And GF.FormAccess(CInt(Session("UserID")), 175) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            TypeID = CInt(Request.QueryString.Get("TypeID"))
            'Response.Redirect("https://10.61.69.42/HRM/Attendance/FSAttendanceApproval.aspx?TypeID=" + TypeID.ToString() + "&EmpCode=" + Session("UserID").ToString() + "&BranchID=" + Session("BranchID").ToString() + "&UserName=" + Session("UserName").ToString() + "", False)
            If TypeID = 1 Then
                Me.Master.subtitle = "Field Staff Attendance Verification"
                DT = AT.Getexist_FS_Attendnce(CInt(Session("BranchID")), TypeID)
            ElseIf TypeID = 2 Then
                Me.Master.subtitle = "AM Attendance Verification"
                DT = AT.Getexist_FS_Attendnce(CInt(Session("UserID")), TypeID)
            ElseIf TypeID = 3 Then
                Me.Master.subtitle = "RM Attendance Verification"
                DT = AT.Getexist_FS_Attendnce(CInt(Session("UserID")), TypeID)
            Else
                Me.Master.subtitle = "Drivers Attendance Verification"
                DT = AT.Getexist_FS_Attendnce(CInt(Session("UserID")), TypeID)
            End If

            Dim Strattend As String
            Strattend = AT.GetFS_Attendance_Request(DT)
            hid_dtls.Value = Strattend
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim dataval As String = CStr(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))



        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try

            Dim Params(5) As SqlParameter
            Params(0) = New SqlParameter("@userID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(1).Value = BranchID
            Params(2) = New SqlParameter("@EMPDtl", SqlDbType.VarChar, 5000)
            Params(2).Value = dataval
            Params(3) = New SqlParameter("@TypeID", SqlDbType.Int)
            Params(3).Value = TypeID
            Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(4).Direction = ParameterDirection.Output
            Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(5).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_ATT_APPROVAL", Params)
            ErrorFlag = CInt(Params(4).Value)
            Message = CStr(Params(5).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString



    End Sub
#End Region

End Class
