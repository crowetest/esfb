﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient

Partial Class EmployeeEmailUpdation
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions

    Dim DT As New DataTable
    Dim emp_code As Integer
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing




#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Me.Master.subtitle = " Employee Profile Updation"

        Me.txtEmpCode.Text = CStr(Request.QueryString.Get("Emp_code"))
        Me.txtEmpName.Text = Request.QueryString.Get("Emp_name")


       
        DT = DB.ExecuteDataSet("SELECT a.emp_name,b.branch_id,b.branch_name,getdate(),emp_category,c.cug_no,c.mobile from EMP_MASTER a,BRANCH_MASTER b,emp_profile c where a.emp_code =c.emp_code and  a.emp_code = " & Me.txtEmpCode.Text & "  and a.branch_id = b.branch_id and a.status_id =1").Tables(0)
                If DT.Rows.Count > 0 Then

                    Session("UserID") = Me.txtEmpCode.Text
                    Session("UserName") = CStr(DT.Rows(0)(0))
                    Session("BranchID") = CStr(DT.Rows(0)(1))
                    Session("BranchName") = CStr(DT.Rows(0)(2))
                    Session("TraDt") = CDate(DT.Rows(0)(3)).ToString("dd MMM yyyy")
            Session("Post_ID") = CStr(DT.Rows(0)(4))
            txtCug.Text = DT.Rows(0)(5).ToString
            txtmobile.Text = DT.Rows(0)(6).ToString
                End If
           
        
        txtOfficialEmail.Focus()



        '          
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
        Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)




    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("|"))


        If (CInt(Data(0)) = 1) Then

            Dim EmpCode As Integer = CInt(Data(1))
          
            Dim OfficialEmail As String = CStr(Data(2))

            Dim Cug As String = CStr(Data(3))
            Dim Mobile As String = CStr(Data(4))
           
            Dim ErrorFlag As Integer = 0

            Dim Message As String = Nothing

            Dim ErrorFlag1 As Integer = 0

            Dim Message1 As String = Nothing
            Try


                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(0).Value = EmpCode
                Params(1) = New SqlParameter("@OfficialEmail", SqlDbType.VarChar, 500)
                Params(1).Value = OfficialEmail
                Params(2) = New SqlParameter("@Cug", SqlDbType.VarChar, 50)
                Params(2).Value = Cug
                Params(3) = New SqlParameter("@Mobile", SqlDbType.VarChar, 50)
                Params(3).Value = Mobile
                
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_EMP_MAIL_UPDATION", Params)
                ErrorFlag1 = CInt(Params(4).Value)
                Message1 = CStr(Params(5).Value)
                Dim Params_P(3) As SqlParameter
                If ErrorFlag1 = 0 Then
                    Params_P(0) = New SqlParameter("@Emp_Code", SqlDbType.Int)
                    Params_P(0).Value = Data(1)
                    Params_P(1) = New SqlParameter("@Settings", SqlDbType.VarChar, 30)
                    Params_P(1).Value = GeneralFunctions.getMACIPAddress
                    Params_P(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params_P(2).Direction = ParameterDirection.Output
                    Params_P(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params_P(3).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_MARKATTENDANCE", Params_P)
                    ErrorFlag = ErrorFlag1 + CInt(Params_P(2).Value)
                    Message = Message1 + "|" + CStr(Params_P(3).Value)
                Else
                    ErrorFlag = 1
                    Message = Message1 + "|"
                End If

            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag1.ToString + "|" + ErrorFlag.ToString + "|" + Message + "|" + CStr(EmpCode)

        End If

    End Sub

#End Region

    Private Function hid_User() As Object
        Throw New NotImplementedException
    End Function




End Class
