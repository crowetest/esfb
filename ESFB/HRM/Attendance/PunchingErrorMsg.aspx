﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="PunchingErrorMsg.aspx.vb" Inherits="PunchingErrorMsg" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>ESFB</title>
    <link href="../../Style/PortalStyle.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../../Image/favicon.ico" type="image/x-icon" />
<style type="text/css">
         .style4
        {
           
            width: 32%;
        }
        .style7
        {
            
            width: 19%;
            text-align:right;
        }
        .NormalText:focus
{
    border-style:solid;
    border-width:thin;    
    border-color:#CFEBFD;
}
.ReadOnlyTextBox
{
    font-family:Cambria;
    background-color:#FFF0E6;
    font-size:10pt;
    text-transform:uppercase;
    
}
        </style>
<style type="text/css">
body{
	 
	 font:bold 12px Arial, Helvetica, sans-serif;
	 margin:0;
	 padding:0;
	 min-width:960px;
	 color:#bbbbbb; 
}

a { 
	text-decoration:none; 
	color:#00c6ff;
}

h1 {
	font: 4em normal Arial, Helvetica, sans-serif;
	padding: 20px;	margin: 0;
	text-align:center;
}

h1 small{
	font: 0.2em normal  Arial, Helvetica, sans-serif;
	text-transform:uppercase; letter-spacing: 0.2em; line-height: 5em;
	display: block;
}

h2 {
    font-weight:700;
    color:#bbb;
    font-size:20px;
}

h2, p {
	margin-bottom:10px;
}

        .style7
        {
            
            width: 19%;
            text-align:right;
        }
         .style4
        {
           
            width: 32%;
             text-align:left;
        }
        .bgCol
        {
            height:650px;
            width:100%; height:650px;
            font-family:Trebuchet MS;
            font-size:16px;
            font-weight:bold;
            text-align:center;
            color:#DCDCDC; /* Color Equivalent to Gainsboro */
	        background:-moz-linear-gradient(bottom,#B21C32,#B21C32);
	        background:-o-linear-gradient(bottom,#B21C32,#B21C32);
	        background:-webkit-linear-gradient(bottom,#B21C32,#B21C32);
	        background:-ms-linear-gradient(bottom,#B21C32,#B21C32);
        }
          
          
  @-webkit-keyframes blink {
  0%     { opacity: 0 } 50% { opacity: 0 }
  50.01% { opacity: 1 } 100% { opacity: 1 }
}
 
   @-moz-keyframes blink {
  0%     { opacity: 0 } 50% { opacity: 0 }
  50.01% { opacity: 1 } 100% { opacity: 1 }
}

  @-o-keyframes blink {
  0%     { opacity: 0 } 50% { opacity: 0 }
  50.01% { opacity: 1 } 100% { opacity: 1 }
}
  @-ms-keyframes blink {
  0%     { opacity: 0 } 50% { opacity: 0 }
  50.01% { opacity: 1 } 100% { opacity: 1 }
}

.blink {
  -webkit-animation: blink 1.0s infinite linear alternate;
  -webkit-font-smoothing: antialiased;
    -moz-animation: blink 1.0s infinite linear alternate;
  -moz-font-smoothing: antialiased;
    -o-animation: blink 1.0s infinite linear alternate;
  -o-font-smoothing: antialiased;
    -ms-animation: blink 1.0s infinite linear alternate;
  -ms-font-smoothing: antialiased;
}

.BlinkMessage
{
    .addclass('blink').wait(1000).removeclass('blink');
}


</style>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
<script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript" for="window" event="onload">return window_onload()</script>    
            <script language="javascript" type="text/javascript">
                function window_onload() 
                {
                    document.getElementById("lblMessage").innerHTML = "You Can not Punch through this System.Please Contact Software Team";
                }

</script>

</head>
<body style=" width:100%; height:100%">
<form id="login_form" runat="server">
    <div id="HeadContents" style="width:100%;">
        <div style="height:130px; padding-top:10px; padding-bottom:10px; float:left; width:550px;"><img src="../../Image/Logo.png" style="width:390px; height:130px;"></div>
      
    </div>  <div style="clear:both;"></div>
  
    <div id="MainContents" style="background-color:White; height:350px;">
    <br /><br /><br /><br />
        <div id="lblMessage" class="BlinkMessage" style="color:Red;text-align:center; font-size:20px;">

         <asp:HiddenField ID="hid_value" runat="server" />
                           
                    </div>
    </div>
    <div style="clear:both;"></div>

    <div id="info" style="color:#B21C32; font-size:small; ">
        <asp:Label ID="lblMacAddress" runat="server"></asp:Label>
    </div>
      

</form>
</body>
</html>


</asp:Content>