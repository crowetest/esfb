﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="ESAFpunching.aspx.vb" Inherits="ESAFpunching" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">


<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>ESFB</title>
    <link href="../../Style/PortalStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Style/PunchingStyle.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../../Image/favicon.ico" type="image/x-icon" />
<style type="text/css">
         .style4
        {
           
            width: 32%;
        }
       

        .style7
        {
            
            width: 19%;
            text-align:right;
        }
        .NormalText:focus
{
    border-style:solid;
    border-width:thin;    
    border-color:#CFEBFD;
}
.ReadOnlyTextBox
{
    font-family:Cambria;
    background-color:#FFF0E6;
    font-size:10pt;
    text-transform:uppercase;
    
}
        </style>
<style type="text/css">
body{
	 
	 font:bold 12px Arial, Helvetica, sans-serif;
	 margin:0;
	 padding:0;
	 min-width:960px;
	 color:#bbbbbb; 
}

a { 
	text-decoration:none; 
	color:#00c6ff;
}

h1 {
	font: 4em normal Arial, Helvetica, sans-serif;
	padding: 20px;	margin: 0;
	text-align:center;
}

h1 small{
	font: 0.2em normal  Arial, Helvetica, sans-serif;
	text-transform:uppercase; letter-spacing: 0.2em; line-height: 5em;
	display: block;
}

h2 {
    font-weight:700;
    color:#bbb;
    font-size:20px;
}

h2, p {
	margin-bottom:10px;
}

@font-face {
    font-family: 'BebasNeueRegular';
    src: url('BebasNeue-webfont.eot');
    src: url('BebasNeue-webfont.eot?#iefix') format('embedded-opentype'),
         url('BebasNeue-webfont.woff') format('woff'),
         url('BebasNeue-webfont.ttf') format('truetype'),
         url('BebasNeue-webfont.svg#BebasNeueRegular') format('svg');
    font-weight: normal;
    font-style: normal;

}

.container {width: 960px; margin: 0 auto; overflow: hidden; background-color:transparent;}

.clock {width:800px; margin:0 auto; padding:30px;color:#fff;background-color:transparent; }

#Date { font-family:'BebasNeueRegular', Arial, Helvetica, sans-serif; font-size:25px; text-align:center; text-shadow:0 0 5px #00c6ff;background-color:transparent; }

ul { width:800px; margin:0 auto; padding:0px; list-style:none; text-align:center;background-color:transparent; }
ul li { display:inline; font-size:3em; text-align:center; font-family:'BebasNeueRegular', Arial, Helvetica, sans-serif; text-shadow:0 0 5px #00c6ff;color:White; background-color:transparent;}

#point { position:relative;  padding-left:10px; padding-right:10px; font-size:55px; background-color:transparent;}

@-webkit-keyframes mymove 
{
0% {opacity:1.0; text-shadow:0 0 20px white;}
50% {opacity:0; text-shadow:none; }
100% {opacity:1.0; text-shadow:0 0 20px white; }	
}


@-moz-keyframes mymove 
{
0% {opacity:1.0; text-shadow:0 0 20px white;}
50% {opacity:0; text-shadow:none; }
100% {opacity:1.0; text-shadow:0 0 20px white; }	
}

        .style7
        {
            
            width: 19%;
            text-align:right;
        }
         .style4
        {
           
            width: 32%;
             text-align:left;
        }
        .bgCol
        {
            height:650px;
            width:100%; height:650px;
font-family:Trebuchet MS;
font-size:16px;
font-weight:bold;
text-align:center;
color:#DCDCDC; /* Color Equivalent to Gainsboro */
	background:-moz-linear-gradient(bottom,#E31E24,#E31E24);
	background:-o-linear-gradient(bottom,#E31E24,#E31E24);
	background:-webkit-linear-gradient(bottom,#E31E24,#E31E24);
	background:-ms-linear-gradient(bottom,#E31E24,#E31E24);
        }
          
  @-webkit-keyframes blink {
  0%     { opacity: 0 } 50% { opacity: 0 }
  50.01% { opacity: 1 } 100% { opacity: 1 }
}
 
   @-moz-keyframes blink {
  0%     { opacity: 0 } 50% { opacity: 0 }
  50.01% { opacity: 1 } 100% { opacity: 1 }
}

  @-o-keyframes blink {
  0%     { opacity: 0 } 50% { opacity: 0 }
  50.01% { opacity: 1 } 100% { opacity: 1 }
}
  @-ms-keyframes blink {
  0%     { opacity: 0 } 50% { opacity: 0 }
  50.01% { opacity: 1 } 100% { opacity: 1 }
}

.blink {
  -webkit-animation: blink 1.0s infinite linear alternate;
  -webkit-font-smoothing: antialiased;
    -moz-animation: blink 1.0s infinite linear alternate;
  -moz-font-smoothing: antialiased;
    -o-animation: blink 1.0s infinite linear alternate;
  -o-font-smoothing: antialiased;
    -ms-animation: blink 1.0s infinite linear alternate;
  -ms-font-smoothing: antialiased;
}

.BlinkMessage
{
    .addclass('blink').wait(1000).removeclass('blink');
}


*                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
*                           {	padding:0; margin-left: 0px; margin-right: 0px;
}


    .style8
    {
        width: 19%;
        text-align: right;
        font-family: Cambria;
    }


</style>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
<script src="../../Script/Validations.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" for="window" event="onload">return window_onload()</script>        
            <script language="javascript" type="text/javascript">

                
                var ip;
                var UserID = 0;
                var EntryFlag = 0;
                $(document).ready(function () {


                    var RTCPeerConnection = window.webkitRTCPeerConnection || window.mozRTCPeerConnection;

                    if (RTCPeerConnection) (function () {
                        var rtc = new RTCPeerConnection({ iceServers: [] });

                        if (1 || window.mozRTCPeerConnection) {
                            rtc.createDataChannel('', { reliable: false });
                        };

                        rtc.onicecandidate = function (evt) {

                            if (evt.candidate) grepSDP("a=" + evt.candidate.candidate);
                        };
                        rtc.createOffer(function (offerDesc) {
                            grepSDP(offerDesc.sdp);
                            rtc.setLocalDescription(offerDesc);
                        }, function (e) { console.warn("offer failed", e); });


                        var addrs = Object.create(null);
                        addrs["0.0.0.0"] = false;
                        function updateDisplay(newAddr) {
                            if (newAddr in addrs) return;
                            else addrs[newAddr] = true;
                            var displayAddrs = Object.keys(addrs).filter(function (k) { return addrs[k]; });
                            ip = displayAddrs.join(" | ") || "n/a";
                        }

                        function grepSDP(sdp) {
                            var hosts = [];
                            sdp.split('\r\n').forEach(function (line) {
                                if (~line.indexOf("a=candidate")) {
                                    var parts = line.split(' '),
                    addr = parts[4],
                    type = parts[7];
                                    if (type === 'host') updateDisplay(addr);
                                } else if (~line.indexOf("c=")) {
                                    var parts = line.split(' '),
                    addr = parts[2];
                                    updateDisplay(addr);
                                }
                            });
                        }
                    })(); else {


                    }


                });


                $(document).ready(function () {
                    var is_mobile = false;

                    if ($('#some-element').css('display') == 'none') {
                        is_mobile = true;
                    }

                    // now i can use is_mobile to run javascript conditionally
                   // alert(isMobile());
                    if (isMobile() == true) {
                        //Conditional script here
                        window.open('../../BlockMobile.aspx', '_self');
                    }
                });
                function isMobile() {
                    try { document.createEvent("TouchEvent"); return true; }
                    catch (e) { return false; }
                }
                function GetCheckingEmployee(EmpCode) {
                    var ToData = "3|" + EmpCode;
                    ToServer(ToData, 3);
                }
                function FromServer(arg, context) {                    
                    switch (context) {
                    
                        case 1:
                        {
                            if (arg == "|") {

                                document.getElementById("lblMessage").innerHTML = "Invalid User";
                                document.getElementById("<%= txt_password.ClientID %>").value = "";
                                document.getElementById("<%= txt_user_id.ClientID %>").focus();
                            }
                            var Dtl = arg.split("|")
                            document.getElementById("<%= txt_emp_name.ClientID %>").value = Dtl[0];
                            document.getElementById("<%= txtShift.ClientID %>").value = Dtl[1];
                            EntryFlag = Dtl[2];
                            break;
                        }

                        case 2:
                        {

                            var Data = arg.split("|"); 
                            var Msg = Data[1].split("¶");
                            document.getElementById("lblMessage").innerHTML = Msg[0];
                            if (Data[0] == 0) {
                                document.getElementById("<%= txt_user_id.ClientID %>").value = "";
                                document.getElementById("<%= txt_password.ClientID %>").value = "";
                                document.getElementById("<%= txt_emp_name.ClientID %>").value = "";
                                document.getElementById("<%= txtShift.ClientID %>").value = "";
                                document.getElementById("<%= txt_user_id.ClientID %>").focus();
                                window.location.assign("http://10.65.0.72/AutoPunching.aspx?Empcode="+UserID+"&IP="+ip);
                            }
                            if (Msg[1] != "")
                                alert(Msg[1].toString().split("$").join("\n"));
                            break;
                        }           
                    }
                }
                function ValidateUser() {
                    UserID = document.getElementById("<%= txt_user_id.ClientID %>").value;
                    var Requestpasswd = document.getElementById("<%= txt_password.ClientID %>").value;
                    if (document.getElementById("<%= txt_user_id.ClientID %>").value != "" && document.getElementById("<%= txt_password.ClientID %>").value != "") {
                        var ToData = "1|" + UserID + "|" + Requestpasswd;
                       
                        ToServer(ToData, 1);
                    }
                }

                function btnConfirm_onclick() {

                    
                    var EmpCode = document.getElementById("<%= txt_user_id.ClientID %>").value;
                    var EmpNme = document.getElementById("<%= txt_emp_name.ClientID %>").value;
                   
                    if (document.getElementById("<%= txt_user_id.ClientID %>").value == "") {
                        alert("Enter the User ID");
                        document.getElementById("<%= txt_user_id.ClientID %>").focus(); return false;
                    }
                    if (document.getElementById("<%= txt_password.ClientID %>").value == "") {
                        alert("Enter the Password");
                        document.getElementById("<%= txt_password.ClientID %>").focus(); return false;
                    }
                    if (document.getElementById("<%= txt_emp_name.ClientID %>").value == "") {
                        alert("Invalid User Credential");
                        document.getElementById("<%= txt_user_id.ClientID %>").focus(); return false;
                    }
                    var userid = document.getElementById("<%= txt_user_id.ClientID %>").value;
                    var password = document.getElementById("<%= txt_password.ClientID %>").value;
                    
                if (EntryFlag == "0") {
                    
                     window.open("EmployeeEmailUpdation.aspx?Emp_code=" + EmpCode + "&Emp_name=" + EmpNme, "_self");
                     return false;
            }                 
                var ToData = "2|" + userid + "|" + password + "|" + ip;
                    ToServer(ToData, 2);
                }


                function ShowTime() 
                {
                 
                   
                    document.getElementById("info").value = document.getElementById("<%= hid_Mac.ClientID %>").value;
                        // Create two variable with the names of the months and days in an array
                        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                        var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

                        // Create a newDate() object
                        //var newDate = new Date();


                        var newDate = new Date(document.getElementById("<%= Hid_server_date.ClientID %>").value); //;
                        // Extract the current date from Date object
                        newDate.setDate(newDate.getDate());

                        if (document.getElementById("<%= hid_Date.ClientID %>").value == "22/May/2017") {
                            document.getElementById("imgLogo").src = "../../Image/BirthDay.png"
                            document.getElementById("imgLogo").style.width = "554px";
                            document.getElementById("imgLogo").style.height = "250px";
                        }
                       
                       // Output the day, date, month and year    
                        document.getElementById("Date").innerHTML=(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

                        setInterval(function () {
                            newDate.setSeconds(newDate.getSeconds() + 1)
                            // Create a newDate() object and extract the seconds of the current time on the visitor's
                            var seconds = newDate.getSeconds();
                            // Add a leading zero to seconds value
                            document.getElementById("sec").innerHTML=((seconds < 10 ? "0" : "") + seconds);
                        }, 1000);

                        setInterval(function () {
                            // Create a newDate() object and extract the minutes of the current time on the visitor's
                            var minutes = newDate.getMinutes();
                            // Add a leading zero to the minutes value
                            document.getElementById("min").innerHTML = ((minutes < 10 ? "0" : "") + minutes);
                        }, 1000);

                        setInterval(function () {
                            // Create a newDate() object and extract the hours of the current time on the visitor's
                            var hours = newDate.getHours();
                            // Add a leading zero to the hours value
                            document.getElementById("hours").innerHTML = ((hours < 10 ? "0" : "") + hours);
                        }, 1000);

                    }

                    function btnExit_onclick() {
                        window.open("../../Default.aspx","_self");
                    }
                  

    function UserIDOnKeyPress(e) {
        if (e.which == 13) {
            if (document.getElementById("<%= txt_user_id.ClientID %>").value != "")
                document.getElementById("<%= txt_password.ClientID %>").value = "";
                document.getElementById("<%= txtShift.ClientID %>").value = "";               
                document.getElementById("<%= txt_password.ClientID %>").focus();
        }
        else
            NumericCheck(e);      

    }

    function PasswordOnKeyPress(e) {
        if (e.which == 13) {
            if (document.getElementById("<%= txt_password.ClientID %>").value != "")
                document.getElementById("btnConfirm").focus();
        }

    }
    //Scrolling Photos
//////    
//////<div style="float:left; ">
//////	        <div class="BoxNew" style="width:100%;">
//////    		    <div id="NewsScroll" >
//////      			    <iframe  src="../../PortalSupports/esaf.html" frameborder="0"  width="300" height="400" style="display:none;"></iframe>
//////   		        </div>
//////	        </div>
//////        </div>
   

            </script>
<link rel="canonical" href="http://www.alessioatzeni.com/wp-content/tutorials/jquery/CSS3-digital-clock/index.html" />
</head>
<body>
    <form id="form1" runat="server">
<h1><img id="imgLogo" src="../../Image/logo.png" style="height:62px;width=242px;" " /></h1>
 
<div class="container" >

<asp:HiddenField ID="hidMacAddress" runat="server" />
<div class="clock">
<div id="Date"></div>

<ul>
	<li id="hours"> </li>
    <li id="point">:</li>
    <li id="min"> </li>
    <li id="point">:</li>
    <li id="sec"> </li>
</ul>

</div>

                                <asp:HiddenField ID="Hid_server_date" runat="server" />
                           
                                <asp:HiddenField ID="hid_Mac" runat="server" />
                           
                                <asp:HiddenField ID="hid_User" runat="server" />
                           
                                <asp:HiddenField ID="hid_Host" runat="server" />
                           
                                <asp:HiddenField ID="hid_IPAddress" runat="server" />
                           
                                <asp:HiddenField ID="hid_Date" runat="server" />
                           
                    <table border="0" align="center" style="width: 100%; " >
                        <tr >
                            <td class="style7" >
                                <span style="font-family: Cambria">User ID</span><span style="color: #ff0000">*&nbsp;
                                </span>
                            </td>
                            <td class="style4" >
                                <asp:TextBox ID="txt_user_id" class="NormalText"
                                    runat="server"  MaxLength="5" 
                                     Font-Names="Cambria" BorderStyle="Solid" BorderWidth="1px" 
                                    BorderColor="#CCCCCC" Width="30%" onkeypress="return UserIDOnKeyPress(event)"></asp:TextBox>
                                &nbsp;
                                                                
                                </td>
                               
                            
                               
                        </tr>
                        <tr>
                            <td  class="style7" >
                                <span style="font-family: Cambria">Password</span><span style="color: #ff0000">*&nbsp;
                                </span></td>
                            <td class="style4" >
                                <asp:TextBox 
                                    ID="txt_password" runat="server"  MaxLength="20" 
                                    Width="30%" TextMode="Password" Font-Names="Cambria" BorderStyle="Solid" class="NormalText"
                                    BorderWidth="1px" BorderColor="#CCCCCC" 
                                    onkeypress="return PasswordOnKeyPress(event)">admin</asp:TextBox>
                                
                                </td>

                            
                        </tr>
                        <tr>
                            <td  class="style8"  align="right">
                                Name&nbsp;&nbsp;&nbsp; </td>
                            <td class="style4" >
                                <asp:TextBox ID="txt_emp_name" class="ReadOnlyTextBox"
                                    runat="server"  MaxLength="20" 
                                    Width="30%" Font-Names="Cambria" BorderStyle="Solid" BorderWidth="1px" 
                                     BorderColor="#CCCCCC" ReadOnly="True"></asp:TextBox>
                                
                                </td>

                            
                        </tr>
                       
                        <tr>
                            <td  class="style8"  align="right">
                                Shift&nbsp;&nbsp;&nbsp;</td>
                            <td class="style4" >
                                <asp:TextBox ID="txtShift" class="ReadOnlyTextBox"
                                    runat="server"  MaxLength="20" 
                                    Width="30%" Font-Names="Cambria" BorderStyle="Solid" BorderWidth="1px" 
                                     BorderColor="#CCCCCC" ReadOnly="True"></asp:TextBox>
                                
                                </td>

                            
                        </tr>
                       
                        <tr >
                            <td align="center" colspan="2"  ><br />
                <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px; "
                    type="button" value="SAVE" onclick="return btnConfirm_onclick()" 
                                     />&nbsp;&nbsp;<input 
                                    id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                    type="button" value="EXIT"  onclick="return btnExit_onclick()" /><br />
                               
                            </td>
                        </tr>
                    </table>
                      <br /> <br />
                    <div id="lblMessage" class="BlinkMessage" style="color:Yellow; font-size:larger;"></div>

                    </div>
                    <div id="info" style="color:#B21C32; font-size:small; "></div>
                    </form>
</body>
</html>
</asp:Content>

