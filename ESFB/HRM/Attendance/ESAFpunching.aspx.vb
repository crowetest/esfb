﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ESAFpunching
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If isMobileBrowser() Then
                Response.Redirect("../../BlockMobile.aspx", False)
            Else
                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
                '/--- Register Client Side Functions ---//
                'Dim StrVal As String = GeneralFunctions.getMACIPAddress
                'Dim strHostName As String

                'Dim strIPAddress As String
                'hid_Host.Value = System.Net.Dns.GetHostName()
                'hid_IPAddress.Value = System.Net.Dns.GetHostByName(hid_Host.Value).AddressList(0).ToString()
                'hid_Mac.Value = StrVal
                DT = GF.Get_Server_Date()
                Hid_server_date.Value = CStr(DT.Rows(0).Item(0))
                Me.hid_Date.Value = CDate(DT.Rows(0).Item(0)).ToString("dd/MMM/yyyy")
                Me.txt_user_id.Attributes.Add("onblur", "return ValidateUser()")
                Me.txt_password.Attributes.Add("onblur", "return ValidateUser()")
                txt_user_id.Focus()
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "ShowTime();", True)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("|"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim Message As String = Nothing
        Dim EMP_Name As String
        Dim ErrorFlag As Integer = 0
        If (CInt(Data(0)) = 1) Then
            Dim RequestID As Integer = CInt(Data(1))
            Dim paasswd As String = CStr(Data(2))
            Dim ShiftName As String = ""
            Dim EntryFlag As Integer
            'If RequestID <> "" And passwd <> "" Then
            Dim Requestpassword As String = EncryptDecrypt.Encrypt(paasswd, CStr(RequestID))
            DTTS = GF.GetEmp_Attendance(RequestID, Requestpassword)
            If DTTS.Rows.Count = 1 Then
                EMP_Name = CStr(DTTS.Rows(0).Item(0))
                ShiftName = CStr(DTTS.Rows(0).Item(2))
            Else
                EMP_Name = ""

            End If
            'End If
            'DT = DB.ExecuteDataSet("select * from UPDATE_PROFILE_STATUS where Emp_code=" & RequestID & "").Tables(0)
            DT = DB.ExecuteDataSet("SELECT isnull(a.Official_mail_id,'') from EMP_profile a,emp_MASTER b where a.emp_code = " & RequestID.ToString & "  and a.emp_code = b.emp_code and b.status_id =1").Tables(0)
            If (DT.Rows(0)(0).ToString = "") Then
                EntryFlag = 0
            Else
                EntryFlag = 1
            End If
            CallBackReturn = CStr(EMP_Name) + "|" + ShiftName + "|" + EntryFlag.ToString
            'CallBackReturn = CStr(EMP_Name) + "|" + ShiftName

        ElseIf (CInt(Data(0)) = 2) Then
            Me.hid_User.Value = Data(1)
            Try

                Dim Params(4) As SqlParameter

                Params(0) = New SqlParameter("@Emp_Code", SqlDbType.Int)
                Params(0).Value = Data(1)
                Params(1) = New SqlParameter("@Settings", SqlDbType.VarChar, 30)
                Params(1).Value = GeneralFunctions.getMACIPAddress
                Params(2) = New SqlParameter("@IP", SqlDbType.VarChar, 30)
                Params(2).Value = Data(3)
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_MARKATTENDANCE", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "|" + Message + "|" + CStr(Me.hid_User.Value)

        ElseIf (CInt(Data(0)) = 3) Then
            Dim EmpCode As Integer = CInt(Data(1))
            DT = DB.ExecuteDataSet("select EmpName,InterDate,InterTime from interview_schedule where EmpCode=" & EmpCode & "").Tables(0)
            If (DT.Rows.Count > 0) Then
                Dim DateStr As String = Format(CDate(DT.Rows(0)(1)), "dd/MMM/yyyy").ToString()
                CallBackReturn += "Attention " & DT.Rows(0)(0).ToString() & " : You have been shortlisted for the Online test¶" & DateStr.ToString() & " ¶ " & DT.Rows(0)(2).ToString() & ""
            End If
        Else
            CallBackReturn = CStr(EN.Get_CheckMac_Address(CStr(Data(1))))
        End If
    End Sub
    Public Function ValidateUser(ByVal username As String, ByVal passwd As String) As Boolean
        Dim UserID As String
        Try
            UserID = CStr(Me.txt_user_id.Text)
        Catch ex As Exception
            Return False
        End Try
        Dim userExists As Boolean
        userExists = False
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("SELECT a.emp_name,b.branch_id,b.branch_name from EMP_MASTER a,BRANCH_MASTER b where a.emp_code = " & UserID & " and a.Pass_word = '" & Me.txt_password.Text & "' and a.branch_id = b.branch_id").Tables(0)
        If DT.Rows.Count = 0 Then
            userExists = True

        End If
        Return userExists
    End Function
    Public Shared Function isMobileBrowser() As Boolean
        'GETS THE CURRENT USER CONTEXT
        Dim context As HttpContext = HttpContext.Current

        'FIRST TRY BUILT IN ASP.NT CHECK
        If context.Request.Browser.IsMobileDevice Then
            Return True
        End If
        'THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
        If context.Request.ServerVariables("HTTP_X_WAP_PROFILE") IsNot Nothing Then
            Return True
        End If
        'THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
        If context.Request.ServerVariables("HTTP_ACCEPT") IsNot Nothing AndAlso context.Request.ServerVariables("HTTP_ACCEPT").ToLower().Contains("wap") Then
            Return True
        End If
        'AND FINALLY CHECK THE HTTP_USER_AGENT 
        'HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
        If context.Request.ServerVariables("HTTP_USER_AGENT") IsNot Nothing Then
            'Create a list of all mobile types
            Dim mobiles As String() = {"midp", "j2me", "avant", "docomo", "novarra", "palmos", "palmsource", "240x320", "opwv", "chtml", "pda", "windows ce", "mmp/", "blackberry", "mib/", "symbian", "wireless", "nokia", "hand", "mobi", "phone", "cdm", "up.b", "audio", "SIE-", "SEC-", "samsung", "HTC", "mot-", "mitsu", "sagem", "sony", "alcatel", "lg", "eric", "vx", "NEC", "philips", "mmm", "xx", "panasonic", "sharp", "wap", "sch", "rover", "pocket", "benq", "java", "pt", "pg", "vox", "amoi", "bird", "compal", "kg", "voda", "sany", "kdd", "dbt", "sendo", "sgh", "gradi", "jb", "dddi", "moto", "iphone", "midp", "java", "opera mini", "opera mobi", "mobi", "nokia",
                    "midp", "midp", "j2me", "avantg", "docomo", "novarra", "palmos",
                    "palmsource", "240x320", "opwv", "chtml", "pda", "windows ce",
                    "mmp/", "blackberry", "mib/", "symbian", "wireless", "nokia",
                    "hand", "mobi", "phone", "cdm", "up.b", "audio", "SIE-", "SEC-",
                    "samsung", "HTC", "mot-", "mitsu", "sagem", "sony", "alcatel",
                    "lg", "erics", "vx", "NEC", "philips", "mmm", "xx", "panasonic",
                    "sharp", "wap", "sch", "rover", "pocket", "benq", "java", "pt",
                    "pg", "vox", "amoi", "bird", "compal", "kg", "voda", "sany",
                    "kdd", "dbt", "sendo", "sgh", "jb", "dddi", "iphone", "ipad",
                    "android", "Android", "series 60", "s60"}

            'Loop through each item in the list created above 
            'and check if the header contains that text
            For Each s As String In mobiles
                If context.Request.ServerVariables("HTTP_USER_AGENT").ToLower().Contains(s.ToLower()) Then
                    Return True
                End If
            Next
        End If

        Return False
    End Function
End Class


