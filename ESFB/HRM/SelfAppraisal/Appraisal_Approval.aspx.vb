﻿Imports System.Data
Partial Class Appraisal_Approval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim ReportID As Integer = 1
    Dim LocationID As Integer = 0
    Dim DB As New MS_SQL.Connect
    Dim GMASTER As New Master
    Dim UserID As Integer
    Dim DTT As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1187) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
           
            Me.Master.subtitle = "Appraisal Approval"
            If Not IsPostBack Then
               
                
                'DT1 = DB.ExecuteDataSet("select Period_id,CONCAT(CONVERT(VARCHAR(11),period_from,106), ' - ', CONVERT(VARCHAR(11),period_to,106) ) AS Period from Appraisal_Period_Master ORDER BY period_from DESC").Tables(0)
                DT1 = DB.ExecuteDataSet("select period_id,Period from Appraisal_Period_Master order by Period_id desc ").Tables(0)

                GN.ComboFill(cmbPeriod, DT1, 0, 1)
                hdnPeriod.Value = DT1.Rows(0)(0).ToString()
                'DT = DB.ExecuteDataSet("select -1 as Currency_id,' ALL' as Currency union all select Currency_id,Currency from FCY_CURRENCY_MASTER order by Currency ").Tables(0)
                'GN.ComboFill(cmbCurrency, DT, 0, 1)
                'Me.CEFromDate.SelectedDate = CDate("01/09/2018")
                'CEToDate.SelectedDate = CDate(Session("TraDt"))
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.cmbRole.Attributes.Add("onchange", "return RoleOnChange()")
            Me.cmbEmp.Attributes.Add("onchange", "return EmployeeOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub


    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then


            Dim Role_ID As Integer = CInt(Data(1))
            If Role_ID = 1 Then
                DT = DB.ExecuteDataSet(" select -1 as APPRAISEE_ID,' -----Select-----' as Emp_name union all  SELECT APPRAISEE_ID,cast(B.emp_code as varchar(10)) + '|'+ B.Emp_name as Emp_name FROM Appraisal_Heads_Master A   INNER JOIN EMP_MASTER B ON A.APPRAISEE_ID=B.EMP_CODE WHERE  STATUS=1 AND REPORTING_OFFICER_ID =" & Session("USerID").ToString() & " and A.Period_id= " & hdnPeriod.Value & " ").Tables(0)
            ElseIf Role_ID = 2 Then
                DT = DB.ExecuteDataSet("select -1 as APPRAISEE_ID,' -----Select-----' as Emp_name union all  SELECT APPRAISEE_ID,cast(B.emp_code as varchar(10)) + '|'+ B.Emp_name as Emp_name FROM Appraisal_Heads_Master A INNER JOIN EMP_MASTER B ON A.APPRAISEE_ID=B.EMP_CODE where STATUS=2 AND REVIEWING_HEAD_ID=" + Session("USerID").ToString() & " and A.Period_id= " & hdnPeriod.Value & " ").Tables(0)
            ElseIf Role_ID = 3 Then
                DT = DB.ExecuteDataSet(" select -1 as APPRAISEE_ID,' -----Select-----' as Emp_name union all SELECT APPRAISEE_ID,cast(B.emp_code as varchar(10)) + '|'+ B.Emp_name as Emp_name FROM Appraisal_Heads_Master A INNER JOIN EMP_MASTER B ON A.APPRAISEE_ID=B.EMP_CODE WHERE  STATUS=3 AND FUNCTIONAL_HEAD_ID =" + Session("USerID").ToString() & " and A.Period_id= " & hdnPeriod.Value & " ").Tables(0)
            ElseIf Role_ID = 5 Then
                DT = DB.ExecuteDataSet(" select -1 as APPRAISEE_ID,' -----Select-----' as Emp_name union all  SELECT APPRAISEE_ID,cast(B.emp_code as varchar(10)) + '|'+ B.Emp_name as Emp_name FROM Appraisal_Heads_Master A INNER JOIN EMP_MASTER B ON A.APPRAISEE_ID=B.EMP_CODE WHERE STATUS =5 AND MD_CEO_EVP = " + Session("USerID").ToString() & " and A.Period_id= " & hdnPeriod.Value & " ").Tables(0)
            ElseIf Role_ID = 4 Then
                UserID = CInt(Session("UserID"))
                DTT = GMASTER.GetEmpRoleList(UserID)
                'DTT = DB.ExecuteDataSet("select emp_code from roles_assigned where role_id=49").Tables(0)
                For Each DR In DTT.Rows
                    If DR(2) = 49 Then
                        DT = DB.ExecuteDataSet(" select -1 as APPRAISEE_ID,' -----Select-----' as Emp_name union all  SELECT APPRAISEE_ID,cast(B.emp_code as varchar(10)) + '|'+ B.Emp_name as Emp_name FROM Appraisal_Heads_Master A INNER JOIN EMP_MASTER B ON A.APPRAISEE_ID=B.EMP_CODE WHERE STATUS =4  and A.Period_id= " & hdnPeriod.Value & " ").Tables(0)
                        Exit For
                    End If
                Next

            End If

            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next

        ElseIf CInt(Data(0)) = 2 Then
            Dim Emp_ID As Integer = CInt(Data(1))
            DT = DB.ExecuteDataSet("select  status from Appraisal_Heads_Master where appraisee_id=" & Emp_ID & "").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString()
            Next
        End If
    End Sub
End Class
