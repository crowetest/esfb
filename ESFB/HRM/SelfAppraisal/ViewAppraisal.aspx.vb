﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Web.Services
Imports System.Web.UI.WebControls
Imports System.Web.Script.Serialization
Imports System.Web
Imports System.Web.UI
Partial Class Appraisal
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim AdminFlag As Integer = 0
    Dim LabelOne As String = ""
    Dim GMASTER As New Master


#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GN.FormAccess(CInt(Session("UserID")), 398) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
               Me.Master.subtitle = "Employee Performance Evaluation"
         
           
            If Not IsPostBack Then
                Dim Role_ID As Integer = CInt(Request.QueryString.Get("Role_ID"))
                If Role_ID = 1 Then
                    Dim DTINIT As New DataTable
                    DTINIT = DB.ExecuteDataSet("select cast(end_date as date) from appraisal_settings_table where item='AssignBackToAppraisee'").Tables(0)
                    If (DTINIT.Rows.Count > 0) Then
                        Dim DateTime As DateTime = Today.Date()
                        If (DateTime > DTINIT.Rows(0).Item(0)) Then
                            hidassinback.Value = 0
                        Else
                            hidassinback.Value = 1
                        End If
                    End If
                ElseIf Role_ID = 2 Then
                    Dim DTINIT As New DataTable
                    DTINIT = DB.ExecuteDataSet("select cast(end_date as date) from appraisal_settings_table where item='AssignBacktoReportingofficer'").Tables(0)
                    If (DTINIT.Rows.Count > 0) Then
                        Dim DateTime As DateTime = Today.Date()
                        If (DateTime > DTINIT.Rows(0).Item(0)) Then
                            hidassinback.Value = 0
                        Else
                            hidassinback.Value = 1
                        End If
                    End If
                ElseIf Role_ID = 3 Then
                    Dim DTINIT As New DataTable
                    DTINIT = DB.ExecuteDataSet("select cast(end_date as date) from appraisal_settings_table where item='AssignBacktoReviewofficer'").Tables(0)
                    If (DTINIT.Rows.Count > 0) Then
                        Dim DateTime As DateTime = Today.Date()
                        If (DateTime > DTINIT.Rows(0).Item(0)) Then
                            hidassinback.Value = 0
                        Else
                            hidassinback.Value = 1
                        End If
                    End If
                ElseIf Role_ID = 4 Then
                    Dim DTINIT As New DataTable
                    DTINIT = DB.ExecuteDataSet("select cast(end_date as date) from appraisal_settings_table where item='AssignBacktofunctionalhead'").Tables(0)
                    If (DTINIT.Rows.Count > 0) Then
                        Dim DateTime As DateTime = Today.Date()
                        If (DateTime > DTINIT.Rows(0).Item(0)) Then
                            hidassinback.Value = 0
                        Else
                            hidassinback.Value = 1
                        End If
                    End If
                End If
                'Dim Role_ID As Integer
                Dim Emp_ID As Integer
                Dim Period_ID As Integer

                Emp_ID = CInt(Request.QueryString.Get("Emp_ID"))
                Period_ID = CInt(Request.QueryString.Get("Period_ID"))

                hdnRoleID.Value = Role_ID
                hdnEmp_ID.Value = Emp_ID
                hdnPeriod.Value = Period_ID
                Dim UserID As Integer = CInt(Session("UserID"))

                DT = DB.ExecuteDataSet("select count(*) from Appraisal_Heads_Master WHERE Period_id = " & hdnPeriod.Value & " ").Tables(0)
                If CInt(DT.Rows(0)(0)) <= 0 Then
                    Dim cl_script0 As New System.Text.StringBuilder
                    cl_script0.Append("         alert('Appraisal is not available for selected period ');window.open('../../Home.aspx','_self');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
                    Exit Sub
                End If

                GN.ComboFill(cmbLocationBranch, EN.GetBranches(), 0, 1)
                GN.ComboFill(cmbDesignationDoj, EN.GetDesignation(), 0, 1)
                GN.ComboFill(cmbDesignation, EN.GetDesignation(), 0, 1)
                GN.ComboFill(cmbDesignationreview, EN.GetDesignation(), 0, 1)
                GN.ComboFill(cmbReportingTo, EN.GetReportingTo(), 0, 1)
                GN.ComboFill(cmbReviewingHead, EN.GetReportingTo(), 0, 1)
                GN.ComboFill(cmbFunctionalHead, EN.GetReportingTo(), 0, 1)
                GN.ComboFill(cmbReviewingHead, EN.GetReportingTo(), 0, 1)
                GN.ComboFill(cmbReportingTo, EN.GetReportingTo(), 0, 1)
                GN.ComboFill(cmbDepartment, EN.GetDepartment(), 0, 1)


                'DT = DB.ExecuteDataSet("select count(*) from emp_master where reporting_to=" & Emp_ID).Tables(0)
                'txtReportedBy.Text = DT.Rows(0)(0).ToString()
                ''  DT = DB.ExecuteDataSet("select period_id,Period from Appraisal_Period_Master order by Period_id desc").Tables(0)
                'DT = DB.ExecuteDataSet("select period_id,Period from Appraisal_Period_Master where period_id=" & hdnPeriod.Value & " ").Tables(0)
                'txtPeriod.Text = DT.Rows(0)(1).ToString()
                'hdnPeriod.Value = DT.Rows(0)(0).ToString()
                DT = DB.ExecuteDataSet("select  count(Period_id) as  Period_id from  Appraisal_Period_Master APM where period_id in (select Period_id from Appraisal_Employee_Master AEM  where  emp_code=" & Emp_ID & " ) ").Tables(0)
                'DT = DB.ExecuteDataSet("select count(*) from Appraisal_Employee_Master where period_id=  " & hdnPeriod.Value & " and   emp_code=" & Emp_ID & "  ").Tables(0)
                If CInt(DT.Rows(0)(0)) > 0 Then
                    DT1 = DB.ExecuteDataSet("select Status from Appraisal_Heads_Master  where APPRAISEE_ID  =   " & Emp_ID & "  and period_id= " & hdnPeriod.Value & "").Tables(0)
                    hdnstatus.Value = CInt(DT1.Rows(0)(0))

                End If
                DT = GMASTER.GetEmpRoleList(UserID)
                For Each DR In DT.Rows
                    If DR(2) = 49 Then
                        AdminFlag = 1
                        Exit For
                    End If
                Next
                If AdminFlag = 1 Then
                    DT = DB.ExecuteDataSet("select HEAD_MASTER_ID from Appraisal_Heads_Master  where APPRAISEE_ID  =  " & Emp_ID & " AND PERIOD_ID=" & Period_ID & "  and status=4 ").Tables(0)
                    If DT.Rows.Count > 0 Then
                        txtHREmpCode.Text = UserID
                        DT = DB.ExecuteDataSet("select emp_name from emp_master where emp_code=" & UserID).Tables(0)
                        txtHRName.Text = DT.Rows(0)(0).ToString()
                    Else
                        DT = DB.ExecuteDataSet("select Appraisal_hr,emp_name from Appraisal_Employee_Master a inner join emp_master b on a.Appraisal_hr=b.emp_code where a.emp_code  =  " & Emp_ID & " AND PERIOD_ID=" & Period_ID & "   ").Tables(0)

                        If DT.Rows.Count > 0 Then
                            If DT.Rows(0)(0) = 0 Or IsDBNull(DT.Rows(0)(0)) Then
                                txtHREmpCode.Text = DT.Rows(0)(0).ToString()
                                txtHRName.Text = ""
                            Else
                                txtHREmpCode.Text = DT.Rows(0)(0).ToString()
                                txtHRName.Text = DT.Rows(0)(1).ToString()
                            End If
                        End If
                    End If

                End If



            End If
                    'hdnDutiesDtl.Value = "¥µµµµµ"
                    'hdnBeliefsDtl.Value = "¥µµµ"
                    '--//---------- Script Registrations -----------//--
                    '/--- For Call Back ---//
                    Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                    Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
                    '/--- Register Client Side Functions ---//


                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "EmpCodeOnChange(" & hdnEmp_ID.Value & ");", True)

                    ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "EmpCodeOnChange(" + Session("UserID") + ");", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        DT1.Dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If Data(0) = 1 Then
            Dim EmpCode As Integer = CInt(Data(1))
            Dim DesignationDojID As Integer = CInt(Data(2))
            Dim DesignationID As Integer = CInt(Data(3))
            Dim DesignationreviewID As Integer = CInt(Data(4))

            Dim DepartmentID As Integer = CInt(Data(5))
            Dim BranchID As Integer = CInt(Data(6))
            Dim ReportingTo As Integer = CInt(Data(7))
            Dim ReportedBy As Integer = CInt(Data(8))
            Dim ReviewHeadID As Integer = CInt(Data(9))
            Dim FunctionalHeadID As Integer = CInt(Data(10))
            Dim PeriodID As Integer = CInt(Data(11))
            Dim Evaluation As Date = CDate(Data(12))
            Dim Remarks As String = CStr(Data(13))
            Dim DutiesDtl As String = Data(14).ToString()
            If DutiesDtl <> "" Then
                DutiesDtl = DutiesDtl.Substring(1)
            End If
            Dim WeightedAvg As Double = CDbl(Data(15))
            Dim SelfRemarks As String = Data(16).ToString()
            Dim BeliefDtl As String = Data(17).ToString()
            If BeliefDtl <> "" Then
                BeliefDtl = BeliefDtl.Substring(1)
            End If
            Dim BeliefAvg As Double = CDbl(Data(18))
            Dim BeliefselfRemarks As String = Data(19).ToString()
            Dim Significant1 As String = Data(20).ToString()
            Dim Significant2 As String = Data(21).ToString()
            Dim Significant3 As String = Data(22).ToString()
            Dim Improve1 As String = Data(23).ToString()
            Dim Improve2 As String = Data(24).ToString()
            Dim Improve3 As String = Data(25).ToString()
            Dim plan1 As String = Data(26).ToString()
            Dim plan2 As String = Data(27).ToString()
            Dim plan3 As String = Data(28).ToString()
            Dim TraitsDtl As String = Data(29).ToString()
            If TraitsDtl <> "" Then
                TraitsDtl = TraitsDtl.Substring(1)
            End If
            Dim TraitsAvg As Double = CDbl(Data(30))
            Dim AppraiseeCommitments As String = Data(31).ToString()
            Dim SuperviorAvgDuties As Double = CDbl(Data(32))
            Dim SupervisorRemarksDuties As String = Data(33).ToString()
            Dim SupervisorAvgBeli As Double = CDbl(Data(34))
            Dim SupervisorRemarksBeli As String = Data(35).ToString()
            Dim SupervisorAvgTraits As Double = CDbl(Data(36))
            Dim RoleID As Integer = CInt(Data(37))

            Dim DisciplinaryActions As String = Data(38).ToString()
            Dim EffortDtl As String = Data(39).ToString()
            If EffortDtl <> "" Then
                EffortDtl = EffortDtl.Substring(1)
            End If
            Dim HODPerformanceRemarks As String = Data(40).ToString()
            Dim HODPerformanceScore As Double
            If Data(41) <> "" Then
                HODPerformanceScore = CDbl(Data(41))
            Else
                HODPerformanceScore = 0.0
            End If


            Dim HODPersonalityRemarks As String = Data(42).ToString()
            Dim HODPersonalityScore As Double
            If Data(43) <> "" Then
                HODPersonalityScore = CDbl(Data(43))
            Else
                HODPersonalityScore = 0.0
            End If
            'Dim HODPersonalityScore As Double = CDbl(Data(43))
            Dim HODAvgScore As Double = CDbl(Data(44))
            Dim EVPPerformanceRemarks As String = Data(45).ToString()
            Dim EVPPerformanceScore As Double
            If Data(46) <> "" Then
                EVPPerformanceScore = CDbl(Data(46))
            Else
                EVPPerformanceScore = 0.0
            End If
            'Dim EVPPerformanceScore As Double = CDbl(Data(46))
            Dim EVPPersonalityRemarks As String = Data(47).ToString()
            Dim EVPPersonalityScore As Double
            If Data(48) <> "" Then
                EVPPersonalityScore = CDbl(Data(48))
            Else
                EVPPersonalityScore = 0.0
            End If
            ' Dim EVPPersonalityScore As Double = CDbl(Data(48))
            Dim EVPAvgScore As Double = CDbl(Data(49))
            Dim EVPRecommend As String = Data(50).ToString()
            Dim HREmp_code As Integer
            If Data(51) <> "" Then
                HREmp_code = CInt(Data(51))
            Else
                HREmp_code = 0
            End If
            'Dim HREmp_code As Integer = CInt(Data(51))
            Dim MDRemarks As String = Data(52).ToString()

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(53) As SqlParameter
                Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(0).Value = EmpCode
                Params(1) = New SqlParameter("@DesignationDojID", SqlDbType.Int)
                Params(1).Value = DesignationDojID
                Params(2) = New SqlParameter("@DesignationID", SqlDbType.Int)
                Params(2).Value = DesignationID
                Params(3) = New SqlParameter("@DesignationreviewID", SqlDbType.Int)
                Params(3).Value = DesignationreviewID
                Params(4) = New SqlParameter("@DepartmentID", SqlDbType.Int)
                Params(4).Value = DepartmentID
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(7).Value = BranchID
                Params(8) = New SqlParameter("@ReportingTo", SqlDbType.Int)
                Params(8).Value = ReportingTo
                Params(9) = New SqlParameter("@ReportedBy", SqlDbType.Int)
                Params(9).Value = ReportedBy
                Params(10) = New SqlParameter("@ReviewHeadID", SqlDbType.Int)
                Params(10).Value = ReviewHeadID
                Params(11) = New SqlParameter("@FunctionalHeadID", SqlDbType.Int)
                Params(11).Value = FunctionalHeadID
                Params(12) = New SqlParameter("@PeriodID", SqlDbType.Int)
                Params(12).Value = PeriodID
                Params(13) = New SqlParameter("@Evaluation", SqlDbType.DateTime)
                Params(13).Value = Evaluation
                Params(14) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                Params(14).Value = Remarks
                Params(15) = New SqlParameter("@DutiesDtl", SqlDbType.VarChar, -1)
                Params(15).Value = DutiesDtl
                Params(16) = New SqlParameter("@WeightedAvg", SqlDbType.Money)
                Params(16).Value = WeightedAvg
                Params(17) = New SqlParameter("@SelfRemarks", SqlDbType.VarChar, 500)
                Params(17).Value = SelfRemarks
                Params(18) = New SqlParameter("@BeliefDtl", SqlDbType.VarChar, -1)
                Params(18).Value = BeliefDtl
                Params(19) = New SqlParameter("@BeliefAvg", SqlDbType.Money)
                Params(19).Value = BeliefAvg
                Params(20) = New SqlParameter("@BeliefselfRemarks", SqlDbType.VarChar, 500)
                Params(20).Value = BeliefselfRemarks
                Params(21) = New SqlParameter("@Significant1", SqlDbType.VarChar, 500)
                Params(21).Value = Significant1
                Params(22) = New SqlParameter("@Significant2", SqlDbType.VarChar, 500)
                Params(22).Value = Significant2
                Params(23) = New SqlParameter("@Significant3", SqlDbType.VarChar, 500)
                Params(23).Value = Significant3
                Params(24) = New SqlParameter("@Improve1", SqlDbType.VarChar, 500)
                Params(24).Value = Improve1
                Params(25) = New SqlParameter("@Improve2", SqlDbType.VarChar, 500)
                Params(25).Value = Improve2
                Params(26) = New SqlParameter("@Improve3", SqlDbType.VarChar, 500)
                Params(26).Value = Improve3
                Params(27) = New SqlParameter("@plan1", SqlDbType.VarChar, 500)
                Params(27).Value = plan1
                Params(28) = New SqlParameter("@plan2", SqlDbType.VarChar, 500)
                Params(28).Value = plan2
                Params(29) = New SqlParameter("@plan3", SqlDbType.VarChar, 500)
                Params(29).Value = plan3
                Params(30) = New SqlParameter("@TraitsDtl", SqlDbType.VarChar, -1)
                Params(30).Value = TraitsDtl
                Params(31) = New SqlParameter("@TraitsAvg", SqlDbType.Money)
                Params(31).Value = TraitsAvg
                Params(32) = New SqlParameter("@AppraiseeCommitments", SqlDbType.VarChar, -1)
                Params(32).Value = AppraiseeCommitments
                Params(33) = New SqlParameter("@SuperviorAvgDuties", SqlDbType.Money)
                Params(33).Value = SuperviorAvgDuties
                Params(34) = New SqlParameter("@SupervisorRemarksDuties", SqlDbType.VarChar, 500)
                Params(34).Value = SupervisorRemarksDuties
                Params(35) = New SqlParameter("@SupervisorAvgBeli", SqlDbType.Money)
                Params(35).Value = SupervisorAvgBeli
                Params(36) = New SqlParameter("@SupervisorRemarksBeli", SqlDbType.VarChar, 500)
                Params(36).Value = SupervisorRemarksBeli
                Params(37) = New SqlParameter("@SupervisorAvgTraits", SqlDbType.Money)
                Params(37).Value = SupervisorAvgTraits
                Params(38) = New SqlParameter("@RoleID", SqlDbType.Int)
                Params(38).Value = RoleID
                Params(39) = New SqlParameter("@DisciplinaryActions", SqlDbType.VarChar, 500)
                Params(39).Value = DisciplinaryActions
                Params(40) = New SqlParameter("@EffortsDtl", SqlDbType.VarChar, -1)
                Params(40).Value = EffortDtl
                Params(41) = New SqlParameter("@HODPerformanceRemarks", SqlDbType.VarChar, 500)
                Params(41).Value = HODPerformanceRemarks
                Params(42) = New SqlParameter("@HODPerformanceScore", SqlDbType.Money)
                Params(42).Value = HODPerformanceScore
                Params(43) = New SqlParameter("@HODPersonalityRemarks", SqlDbType.VarChar, 500)
                Params(43).Value = HODPersonalityRemarks
                Params(44) = New SqlParameter("@HODPersonalityScore", SqlDbType.Money)
                Params(44).Value = HODPersonalityScore
                Params(45) = New SqlParameter("@HODAvgScore", SqlDbType.Money)
                Params(45).Value = HODAvgScore
                Params(46) = New SqlParameter("@EVPPerformanceRemarks", SqlDbType.VarChar, 500)
                Params(46).Value = EVPPerformanceRemarks
                Params(47) = New SqlParameter("@EVPPerformanceScore", SqlDbType.Money)
                Params(47).Value = EVPPerformanceScore
                Params(48) = New SqlParameter("@EVPPersonalityRemarks", SqlDbType.VarChar, 500)
                Params(48).Value = EVPPersonalityRemarks
                Params(49) = New SqlParameter("@EVPPersonalityScore", SqlDbType.Money)
                Params(49).Value = EVPPersonalityScore
                Params(50) = New SqlParameter("@EVPAvgScore", SqlDbType.Money)
                Params(50).Value = EVPAvgScore
                Params(51) = New SqlParameter("@EVPRecommend", SqlDbType.VarChar, 200)
                Params(51).Value = EVPRecommend
                Params(52) = New SqlParameter("@HREmp_code", SqlDbType.Int)
                Params(52).Value = HREmp_code
                Params(53) = New SqlParameter("@MDRemarks", SqlDbType.VarChar, 500)
                Params(53).Value = MDRemarks

                DB.ExecuteNonQuery("SP_APPRAISAL_EMPLOYEE_DETAILS", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
          
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try

            CallBackReturn = ErrorFlag.ToString + "Ø" + Message

        ElseIf CInt(Data(0)) = 2 Then
            Try
                Dim EmpCode = CInt(Data(1))
                Dim Period_ID = CInt(Data(2))
                Dim Role_ID = CInt(Data(3))
                Dim StrWhr As String = ""
                Dim IntVal As Integer = 0
                Dim IntVals As Integer = 0
                If Role_ID = 1 Then
                    StrWhr = 2
                End If
                If Role_ID = 2 Then
                    StrWhr = 3
                End If
                If Role_ID = 3 Then

                    Dim Params(1) As SqlParameter
                    Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(0).Value = EmpCode
                    Params(1) = New SqlParameter("@Period_ID", SqlDbType.Int)
                    Params(1).Value = Period_ID
                    DB.ExecuteNonQuery("SP_APPRAISAL_Update_Total", Params)
                    StrWhr = 4
             
            End If
            If Role_ID = 4 Then
                IntVals = DB.ExecuteNonQuery("update Appraisal_Employee_Master set Appraisal_hr=" & CInt(Session("UserID")) & " where emp_code=" & CInt(Data(1)) & " and period_id=" & CInt(Data(2)) & "")
                StrWhr = 5
            End If
            IntVal = DB.ExecuteNonQuery("update Appraisal_Heads_Master set status=" & StrWhr & " where appraisee_id=" & CInt(Data(1)) & " and period_id=" & CInt(Data(2)) & "")
            CallBackReturn = CStr(IntVal)
            Catch ex As Exception

                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try
        ElseIf CInt(Data(0)) = 3 Then
            Dim EmpCode = CInt(Data(1))
            Dim Period_ID = CInt(Data(2))
            Dim Role_ID As Integer
            Role_ID = CInt(Request.QueryString.Get("Role_ID"))
            Dim Textname As String = ""
            If Role_ID = 1 Then
                Textname = "SEND BACK TO APPRAISEE"

            ElseIf Role_ID = 2 Then
                Textname = "SEND BACK TO REPORTING OFFICER"

            ElseIf Role_ID = 3 Then
                Textname = "SEND BACK TO REVIEWING OFFICER"

            ElseIf Role_ID = 4 Then
                Textname = "SEND BACK TO FUNCTIONAL HEAD"

            End If

            'DT2 = DB.ExecuteDataSet("select count(*) from appraisal_employee_master where emp_code= " & CInt(Data(1)) & " and period_id=" & CInt(Data(2)) & " ").Tables(0)
            'If CInt(DT2.Rows(0)(0)) > 0 Then


            DT = DB.ExecuteDataSet("select AEM.emp_code,em.emp_name,em.date_of_join,AEM.designation_id_doj,DM.DESIGNATION_NAME,AEM.designation_Id_current,DM1.DESIGNATION_NAME,DM.DESIGNATION_NAME,reported_by,AEM.period_id,APM.period " & _
                             " ,Evaluation_Date,AEM.Reviewing_Head_id,EM1.EMP_NAME,AEM.Functional_head_id, EM2.EMP_NAME, Remarks, AEM.Dept_id, DM2.DEPARTMENT_NAME, AEM.Reporting_to, em3.EMP_NAME,AEM.BRANCH_ID,BM.BRANCH_NAME,AEM.designation_id_last " & _
                            " from Appraisal_Employee_Master AEM INNER JOIN emp_master em on em.emp_code=AEM.emp_code INNER JOIN designation_master DM on AEM.designation_id_dOJ=DM.Designation_ID " & _
                            " INNER JOIN designation_master DM1 on AEM.designation_Id_last=DM1.Designation_ID INNER JOIN Appraisal_pERIOD_Master APM  ON AEM.PERIOD_ID=APM.PERIOD_ID INNER JOIN EMP_MASTER EM1 ON EM1.EMP_CODE=AEM.Reviewing_Head_id " & _
                            " INNER JOIN EMP_MASTER EM2 ON EM2.EMP_CODE=AEM.Functional_head_id INNER JOIN DEPARTMENT_MASTER DM2 ON DM2.DEPARTMENT_ID=AEM.DEPT_ID INNER JOIN EMP_MASTER em3 ON em3.EMP_CODE=AEM.Reporting_to INNER JOIN BRANCH_MASTER BM ON BM.BRANCH_ID= AEM.BRANCH_ID " & _
                            " where AEM.emp_code = " & CInt(Data(1)) & " And APM.Period_id = " & CInt(Data(2)) & " ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    ' CallBackReturn += "Ø" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ" + DR(2).ToString() + "ÿ" + DR(3).ToString() + "ÿ" + DR(4).ToString() + "ÿ" + DR(5).ToString() + "ÿ" + DR(6).ToString() + "ÿ" + DR(7).ToString() + "ÿ" + DR(8).ToString() + "ÿ" + DR(9).ToString() + "ÿ" + DR(10).ToString() + "ÿ" + DR(11).ToString() + "ÿ" + DR(12).ToString() + "ÿ" + DR(13).ToString() + "ÿ" + DR(14).ToString() + "ÿ" + DR(15).ToString() + "ÿ" + DR(16).ToString() + "ÿ" + DR(17).ToString() + "ÿ" + DR(18).ToString() + "ÿ" + DR(19).ToString() + "ÿ" + DR(20).ToString()
                    CallBackReturn = DT.Rows(0)(0).ToString() + "µ" + DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + DT.Rows(0)(7).ToString() + "µ" + DT.Rows(0)(8).ToString() + "µ" + DT.Rows(0)(9).ToString() + "µ" + DT.Rows(0)(10).ToString() + "µ" + DT.Rows(0)(11).ToString() + "µ" + DT.Rows(0)(12).ToString() + "µ" + DT.Rows(0)(13).ToString() + "µ" + DT.Rows(0)(14).ToString() + "µ" + DT.Rows(0)(15).ToString() + "µ" + DT.Rows(0)(16).ToString() + "µ" + DT.Rows(0)(17).ToString() + "µ" + DT.Rows(0)(18).ToString() + "µ" + DT.Rows(0)(19).ToString() + "µ" + DT.Rows(0)(20).ToString() + "µ" + DT.Rows(0)(21).ToString() + "µ" + DT.Rows(0)(22).ToString() + "µ" + DT.Rows(0)(23).ToString()
                Next
            Else

                CallBackReturn += "ØØØØ"

                GoTo LableOne

            End If
            CallBackReturn += "Ø"
            'Else
            '    DT = DB.ExecuteDataSet("select em.emp_code,em.emp_Name,CONVERT(VARCHAR(10),em.date_of_join,105) as Date_of_join,em.DESIGNATION_ID,designation_name ,em.DESIGNATION_ID,designation_name, designation_name ,'' as reported_by,'' as period_id,'' as period,CONVERT(VARCHAR(10),GETDATE(),105) as evaluation_date,AHM.reviewing_head_id,cast(AHM.reviewing_head_id as varchar(10))+ '|'+ RA.Emp_name as review_head_name,AHM.functional_head_id,cast(AHM.functional_head_id as varchar(10))+ '|'+ RA1.Emp_name as Functional_head_name,'' as remarks,em.DEPARTMENT_ID,department_name,em.reporting_to,'' as reporting_name,em.BRANCH_ID,branch_name from Emp_List  em left join emp_master a on a.emp_code=em.emp_code left join  Appraisal_Heads_Master AHM on em.emp_code=AHM.Appraisee_id  left join emp_master RA on RA.emp_code=AHM.reviewing_head_id left join emp_master RA1 on RA1.emp_code=AHM.Functional_head_id where em.emp_code=" + Session("USerID").ToString()).Tables(0)

            '    If DT.Rows.Count > 0 Then
            '        'CallBackReturn = DT.Rows(0)(0).ToString() + "µ" + DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + DT.Rows(0)(7).ToString()
            '        CallBackReturn = DT.Rows(0)(0).ToString() + "µ" + DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + DT.Rows(0)(7).ToString() + "µ" + DT.Rows(0)(8).ToString() + "µ" + DT.Rows(0)(9).ToString() + "µ" + DT.Rows(0)(10).ToString() + "µ" + DT.Rows(0)(11).ToString() + "µ" + DT.Rows(0)(12).ToString() + "µ" + DT.Rows(0)(13).ToString() + "µ" + DT.Rows(0)(14).ToString() + "µ" + DT.Rows(0)(15).ToString() + "µ" + DT.Rows(0)(16).ToString() + "µ" + DT.Rows(0)(17).ToString() + "µ" + DT.Rows(0)(18).ToString() + "µ" + DT.Rows(0)(19).ToString() + "µ" + DT.Rows(0)(20).ToString() + "µ" + DT.Rows(0)(21).ToString() + "µ" + DT.Rows(0)(22).ToString()

            '        CallBackReturn += "Ø"

            'Dim BranchID As Array = Split((DT.Rows(0)(2)), "|")
            'Dim DepID As Integer = CInt(DT.Rows(0)(3))
            'Dim DesID As Integer = CInt(DT.Rows(0)(4))





            '  DT = DB.ExecuteDataSet(" select A.parameters_id,A.category,A.parameters,A.measurement_points,B.ACTIVITIES,B.SELF_SCORE,B.SUPERVISOR_SCORE from Appraisal_ESAF_Beliefs_Parameter A left join ( select * from Appraisal_Esaf_Beliefs where emp_code =" + Session("USerID").ToString() + ") B on A.PARAMETERS_ID=B.PARAMETER_ID  ").Tables(0)
            DT = DB.ExecuteDataSet("select A.parameters_id,A.category,A.parameters,A.measurement_points,B.ACTIVITIES,B.SELF_SCORE,B.SUPERVISOR_SCORE from Appraisal_ESAF_Beliefs_Parameter A left join ( select * from Appraisal_Esaf_Beliefs where emp_code =" & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & ") B on A.PARAMETERS_ID=B.PARAMETER_ID   ").Tables(0)

            For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString()

            Next
            CallBackReturn += "Ø"

            DT = DB.ExecuteDataSet("select beliefs_id,average_score,SUPERVISOR_AVERAGE_SCORE,self_remarks,SUPERVISOR_REMARKS from Appraisal_Esaf_Beliefs where emp_code =" & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & " ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString()

                Next
            Else
                CallBackReturn += "¥µµµµ"
            End If
            CallBackReturn += "Ø"
            DT = DB.ExecuteDataSet("select A.SIPC_ID,A.significant1,A.significant2,A.significant3 from Appraisal_Signi_Improve_Plan_Commitment A where A.emp_code =" & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()

                Next
            Else
                CallBackReturn += "¥µµµ"
            End If


            CallBackReturn += "Ø"
            DT = DB.ExecuteDataSet("select A.SIPC_ID,A.improve1,A.improve2,A.improve3 from Appraisal_Signi_Improve_Plan_Commitment A  where a.emp_code =" & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & " ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()

                Next
            Else
                CallBackReturn += "¥µµµ"
            End If

            CallBackReturn += "Ø"

            DT = DB.ExecuteDataSet(" select SIPC_ID,plan1,plan2,plan3 from Appraisal_Signi_Improve_Plan_Commitment where emp_code =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
                Next
            Else
                CallBackReturn += "¥µµµ"
            End If
            CallBackReturn += "Ø"

            ' DT = DB.ExecuteDataSet("select A.Factors_id,A.Factors,excellent,A.good,A.fair,A.poor,B.SELF_SCORE from Appraisal_Traits_Behaviour_Factors A left join Appraisal_Traits_BehaviourS B ON A.Factors_id= B.Factors_id where B.emp_code=" + Session("USerID").ToString() + " ").Tables(0)
            DT = DB.ExecuteDataSet(" select A.Factors_id,A.Factors,excellent,A.good,A.fair,A.poor,B.SELF_SCORE,B.SUPERVISOR_SCORE from Appraisal_Traits_Behaviour_Factors A left join ( select * from Appraisal_Traits_BehaviourS where emp_code = " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & " )B ON A.Factors_id= B.Factors_id ").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString() + "µ" + DR(7).ToString()

            Next
            CallBackReturn += "Ø"
            DT = DB.ExecuteDataSet(" select Traits_id,avg_score,SUPERVISOR_AVG_SCORE from Appraisal_Traits_BehaviourS where emp_code = " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & " ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString()

                Next
            Else
                CallBackReturn += "¥µµ"
            End If
            CallBackReturn += "Ø"

            DT = DB.ExecuteDataSet(" select SIPC_ID,COMMITMENTS from Appraisal_Signi_Improve_Plan_Commitment where emp_code  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString()
                Next
            Else
                CallBackReturn += "¥µ"
            End If
            CallBackReturn += "Ø"

            DT = DB.ExecuteDataSet(" select DUTIES_RESPONSIBILITIES,WEIGHTAGE,KEY_PERFORMANCE,REVIEWING_STATUS,SELF_SCORE,SUPERVISOR_SCORE from Appraisal_Duties_Responsibilities_Master where emp_code  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString()
                Next
            Else
                CallBackReturn += "¥µµµµµ"
            End If
            CallBackReturn += "Ø"

            DT = DB.ExecuteDataSet("select distinct WEIGHTED_AVG_SCORE,SUPER_WEIGHTED_AVG_SCORE,SELF_REMARKS,SUPERVISOR_REMARKS from Appraisal_Duties_Responsibilities_Master where emp_code  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
                Next
            Else
                CallBackReturn += "¥µµµ"
            End If
            CallBackReturn += "Ø"

            DT = DB.ExecuteDataSet(" select DIS_ID,DISCIPLINARY_ACTIONS from RO_DISCIPLINARY_ACTIONS where emp_code  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString()
                Next
            Else
                CallBackReturn += "¥µ"
            End If
            CallBackReturn += "Ø"

            DT = DB.ExecuteDataSet(" select JOB_EFFORTS,CAREER_LEVEL_EFFORTS,TRAINING_EFFORTS from Appraisal_RO_Comments where APPRAISEE  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString()
                Next
            Else
                CallBackReturn += "¥µµ¥µµ"
            End If

            CallBackReturn += "Ø"

            DT = DB.ExecuteDataSet(" select REMARKS_ID,REVIEWING_PERFORMANCE,REVIEWING_PERFORM_SCORE,REVIEWING_PERSONALITY,REVIEWING_PERSONALITY_SCORE, REVIEWING_AVG_SCORE from Appraisal_Review_FnHead_Remarks where EMP_CODE  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString()
                Next
            Else
                CallBackReturn += "¥µµµµµ"
            End If
            CallBackReturn += "Ø"

            DT = DB.ExecuteDataSet(" select REMARKS_ID,FNHEAD_PERFORMANCE,FNHEAD_PERFORM_SCORE,FNHEAD_PERSONALITY,FNHEAD_PERSONALITY_SCORE,FNHEAD_AVG_SCORE,FNHEAD_RECOMMENDATION from  Appraisal_Review_FnHead_Remarks  where EMP_CODE  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString()
                Next
            Else
                CallBackReturn += "¥µµµµµµ"
            End If
            CallBackReturn += "Ø"
            DT = DB.ExecuteDataSet("select HEAD_MASTER_ID from Appraisal_Heads_Master  where APPRAISEE_ID  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  and status in (4,5) ").Tables(0)
            If DT.Rows.Count > 0 Then
                'CallBackReturn += "Ø"

                DT = DB.ExecuteDataSet(" select weightage_id,self_score,Reporting_officer_score,hod_score,fnhead_score,total_score from Appraisal_Weightage_Score where EMP_CODE  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString()
                    Next
                Else
                    CallBackReturn += "¥µµµµµ"
                End If
            Else

            End If
            CallBackReturn += "Ø"

            DT = DB.ExecuteDataSet(" select  MD_REMARKS_ID,MD_REMARKS FROM Appraisal_MD_EVP_Remarks  where EMP_CODE  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString()
                Next
            Else
                CallBackReturn += "¥µ"
            End If
            CallBackReturn += "¥" + Textname

        ElseIf CInt(Data(0)) = 4 Then
            Dim Role_ID As Integer
            Dim Emp_ID As Integer
            Role_ID = CInt(Request.QueryString.Get("Role_ID"))
            Emp_ID = CInt(Request.QueryString.Get("Emp_ID"))

            If Role_ID = 1 Then

                DB.ExecuteNonQuery("update appraisal_heads_master set status=0 where appraisee_id=" & Emp_ID & "")
            ElseIf Role_ID = 2 Then

                DB.ExecuteNonQuery("update appraisal_heads_master set status=1 where appraisee_id=" & Emp_ID & "")
            ElseIf Role_ID = 3 Then

                DB.ExecuteNonQuery("update appraisal_heads_master set status=2 where appraisee_id=" & Emp_ID & "")
            ElseIf Role_ID = 4 Then

                DB.ExecuteNonQuery("update appraisal_heads_master set status=3 where appraisee_id=" & Emp_ID & "")
            End If
            CallBackReturn += "Ø" + "Sucess"
        End If
LableOne:
    End Sub

#End Region


End Class
