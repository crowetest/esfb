﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Appraisal.aspx.vb" Inherits="Appraisal"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
		<link rel="stylesheet" href="../../Style/bootstrap-multiselect.css" type="text/css" />
		<script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-multiselect_EMP.js"></script>
        <script src="../../Script/sweetalert.min.js" type="text/javascript"></script>
        <link href="../../Style/sweetalert.css" rel="stylesheet" type="text/css" />

    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
        .style1
        {
            width: 38%;
        }
        .style2
        {
            width: 4%;
        }
        .style3
        {
            width: 23%;
        }
    </style>
    <script type="text/javascript">
    var finish_save_flag=0;
 
        $(document).ready(function () {
            $('#chkveg').multiselect();
            buttonWidth: '500px'
        });




        function EmpCodeOnChange(EmpID,Status_ID,PeriodID) {
            document.getElementById("<%= txtEmpCode.ClientID %>").value = EmpID;

            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            ///var PeriodID = document.getElementById("<%= hdnPeriod.ClientID %>").value;
            ////var Status_ID=document.getElementById("<%= hdnstatus.ClientID %>").value;
          
         
           
            if(Status_ID!=0)
            {
                    document.getElementById("btnSave").disabled=true;
                    document.getElementById("btnFinish").disabled=true;
                     if(Status_ID==1)
                        alert("Your self appraisal is already finish.Now it is in the stage of Reporting Officer");
                    else if(Status_ID==2)
                        alert("Your self appraisal is already finish.Now it is in the stage of Reviewing Head / HOD");
                    else if(Status_ID==3)
                        alert(" Your self appraisal is already finish.Now it is in the stage of Functional Head / EVP ");
                    else if(Status_ID==4)
                            alert("Your self appraisal is already finish.Now it is in the stage of HR ");
                        else 
                    alert("Your self appraisal is already finish.now it is in the stage of MD & CEO / EVP CS ");




            }
            else
            {
                document.getElementById("btnSave").disabled=false;
                document.getElementById("btnFinish").disabled=false;

            }
            if (EmpCode != "") {
                var ToData = "3Ø" + EmpCode+"Ø" + PeriodID;
               
                ToServer(ToData, 3);
            }

        }

         
         function FromServer(Arg, Context) {
           switch (Context) {

                    case 1:
                    {
                        var Data = Arg.split("Ø");
                        document.getElementById("btnSave").disabled=false;
                        if (Data[0] == 0)
                        { 

                            if(finish_save_flag==0)
                            {
                                alert(Data[1]);
                                /////window.open('Appraisal.aspx', "_self");

                            }
                            else
                            {
                             var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
                             var PeriodID = document.getElementById("<%= hdnPeriod.ClientID %>").value;
           
                                var ToData = "2Ø" + EmpCode+"Ø" + PeriodID ;
                                ToServer(ToData, 2);
                              
                            }
                            break;
                        }
                        else
                        {
                            alert(Data[1]);
                            document.getElementById("<%= cmbReportingTo.ClientID %>").value="-1";
                            document.getElementById("<%= txtEmpCode.ClientID %>").value="";
                            document.getElementById("<%= txtEmpName.ClientID %>").value="";
                            document.getElementById("<%= cmbDesignationreview.ClientID %>").value="-1";
                            document.getElementById("<%= cmbLocationBranch.ClientID %>").value="-1";
                            document.getElementById("<%= cmbDepartment.ClientID %>").value="-1";
                            document.getElementById("<%= cmbDesignation.ClientID %>").value="-1";
                            document.getElementById("<%= cmbDesignationDoj.ClientID %>").value="-1";
                            document.getElementById("<%= txtDoj.ClientID %>").value="";
                            document.getElementById("<%= txtEvaluation.ClientID %>").value="";
                            document.getElementById("<%= cmbReportingTo.ClientID %>").value="-1";
                            document.getElementById("<%= txtReportedBy.ClientID %>").value="";
                            document.getElementById("<%= cmbReviewingHead.ClientID %>").value="-1";
                            document.getElementById("<%= cmbFunctionalHead.ClientID %>").value="-1";
                            document.getElementById("<%= txtRemarks.ClientID %>").value="";
                            document.getElementById("<%= txtPeriod.ClientID %>").value="";  
                        }

                    }    
                    case 2:
                  {
                      
                 if (Arg==1 ){
               
                     alert("You have successfully completed the self appraisal");
                     window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self"); 
                   
                      
                    } 
                    else
                    return false;
                    break;
                    }       
                    case 3: {   
                   
                        var Row=Arg.split("Ø");
                     
                        document.getElementById("<%= hdnDutiesDtl.ClientID %>").value=Row[9];
                        document.getElementById("<%= hdnDutiesDtl1.ClientID %>").value=Row[10];
                        if(Arg=="ØØØØ")  
                        {alert("Invalid Employee Number");      } 
                        else{
                            FillData(Arg);
                            table_fillBeliefs(Arg);
                            table_Signifill(Arg);
                            table_ImprovementAreafill(Arg);
                            table_DevelopmentPlanfill(Arg);
                            table_fillTraits(Arg);
                            table_AppraiseeCommentsfill(Arg); 
                            table_fill(Arg);
                            break;   
                            }
                         }
                   
                    
                    }
        }

         function FillData(Arg)
               {


                var Row=Arg.split("Ø");
                
                var Dtl=Row[0].split("µ");   
                
               

                document.getElementById("<%= txtEmpCode.ClientID %>").value=Dtl[0];
                document.getElementById("<%= txtEmpName.ClientID %>").value=Dtl[1];
                document.getElementById("<%= txtDoj.ClientID %>").value=Dtl[2];
                document.getElementById("<%= cmbDesignationDoj.ClientID %>").value=Dtl[3];
                document.getElementById("<%= cmbDesignation.ClientID %>").value=Dtl[5];
                document.getElementById("<%= cmbDesignationreview.ClientID %>").value=Dtl[23];
                document.getElementById("<%= cmbReviewingHead.ClientID %>").value=Dtl[12];
                document.getElementById("<%= cmbFunctionalHead.ClientID %>").value=Dtl[14];                    
                document.getElementById("<%= cmbDepartment.ClientID %>").value=Dtl[17]; 
                document.getElementById("<%= txtRemarks.ClientID %>").value=Dtl[16];        
                document.getElementById("<%= cmbReportingTo.ClientID %>").value=Dtl[19];     
                document.getElementById("<%= cmbLocationBranch.ClientID %>").value=Dtl[21];     
                document.getElementById("<%= txtEvaluation.ClientID %>").value=Dtl[11];   
                
          
               

               }

            function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function check(len) 
         {

            return false;

         }       
         function btnSave_onclick() {
          
                var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
                var EmpName = document.getElementById("<%= txtEmpName.ClientID %>").value;
                var DateOfJoin = document.getElementById("<%= txtDoj.ClientID %>").value;
                var DesignationDojID = document.getElementById("<%= cmbDesignationDoj.ClientID %>").value;
                var DesignationID = document.getElementById("<%= cmbDesignation.ClientID %>").value;
                var DesignationreviewID = document.getElementById("<%= cmbDesignationreview.ClientID %>").value;

                var DepartmentID = document.getElementById("<%= cmbDepartment.ClientID %>").value;
                var BranchID = document.getElementById("<%= cmbLocationBranch.ClientID %>").value;
               
                var ReportingTo = document.getElementById("<%= cmbReportingTo.ClientID %>").value;
                var ReportedBy = document.getElementById("<%= txtReportedBy.ClientID %>").value;
                var ReviewHeadID = document.getElementById("<%= cmbReviewingHead.ClientID %>").value;
                var FunctionalHeadID = document.getElementById("<%= cmbFunctionalHead.ClientID %>").value;
                var PeriodID = document.getElementById("<%= hdnPeriod.ClientID %>").value;
              
                var Evaluation = document.getElementById("<%= txtEvaluation.ClientID %>").value;
                var Remarks = document.getElementById("<%= txtRemarks.ClientID %>").value;
               
                
                var row1=""
                if( DesignationDojID == -1 )
                {
                    alert("Please select designation during date of joining");
                    return false;
                }
                if( DesignationreviewID == -1 )
                {
                    alert("Please select designation during review");
                    return false;
                }
               row1 = document.getElementById("<%= hdnDutiesDtl.ClientID %>").value.split("¥");
          
                var NewDutiesStr=""
                
                var Count=0;
                for (n = 1; n <= row1.length-1 ; n++) {     
                    var Dutiescol=row1[n].split("µ");    
                           
                        Count += Math.abs(Dutiescol[1]);
                      if(document.getElementById("txtDuities1").value=="") 
                      {
                         alert("Please fill mandatory fields in Duties & Responsibilities tab");
                         
                       
                            document.getElementById("txtDuities"+n.toString()).focus();

                            return false;

                      }
                       else if(Dutiescol[0]!="" && Dutiescol[1]=="" )
                        {                   
                            alert("Please fill Weightage score in Duties & Responsibilities tab");
                             
                       
                            document.getElementById("txtWeightage"+n.toString()).focus();

                            return false;
                        }
                      else if(Dutiescol[0]!="" && Dutiescol[1]!=""  && Dutiescol[4]!="" )
                        {                   
                            NewDutiesStr+="¥"+Dutiescol[0]+"µ"+Dutiescol[1]+"µ"+Dutiescol[2]+ "µ" +Dutiescol[3]+ "µ" +Dutiescol[4];
                      
                           
                        } 
                        
                       else if(Dutiescol[0]=="" && Dutiescol[1]!=""  && Dutiescol[4]!="" )
                        {
                            alert("Please fill mandatory fields in Duties & Responsibilities tab");
                         
                       
                            document.getElementById("txtDuities"+n.toString()).focus();

                            return false;

                        }
                       else if (Dutiescol[0]=="" && Dutiescol[1]!="")
                        {
                            alert("Please fill mandatory fields in Duties & Responsibilities tab");
                             
                       
                            document.getElementById("txtDuities"+n.toString()).focus();

                            return false;



                        }
                        else if (Dutiescol[1]!="" && Dutiescol[4]==""  )
                        {
                            alert("Please fill self score in Duties & Responsibilities tab");
                             
                       
                            document.getElementById("txtSelfScore"+n.toString()).focus();

                            return false;



                        }
                      
                  if(Dutiescol[4]!="")
                  {
                        if((Dutiescol[4]<1.00) || (Dutiescol[4]>10.00))

                        {
                            alert("Choose Score from 1 to 10");
                            document.getElementById("txtSelfScore"+n.toString()).focus();
                            return false;

                        }
                       
                   }
                
                }
               
             if (Count!=100) 
             {
                alert("Weightage score in Duties and Responsibilities should be equal to 100");
                document.getElementById("txtPerentage").focus();
                return false;
                
             }
            

               row2 = document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value.split("¥");
            
                var NewBeliefsStr="";
                for (n = 1; n <= row2.length-1 ; n++) {     
                    var Beliefcol=row2[n].split("µ"); 
                   
                           
                    if(Beliefcol[4]!="" && Beliefcol[5]!="")
                    {                         
                        NewBeliefsStr+="¥"+Beliefcol[0]+"µ"+Beliefcol[4]+"µ"+Beliefcol[5];
                      
                    }
                    if(Beliefcol[4]!="" && Beliefcol[5]=="")
                    {                         
                       alert("Enter Self Score in ESAF Beliefs tab  ");  document.getElementById("txtSelfScoreBeli"+ n.toString()).focus(); return false;
                    }
                    if(Beliefcol[4]=="" && Beliefcol[5]!="")
                    {                         
                       alert("Enter Activities in ESAF Beliefs tab  ");  document.getElementById("txtActivities"+ n.toString()).focus(); return false;
                    }
                  if(Beliefcol[5]!="")
                  {
                         if((Beliefcol[5]<1.00) || (Beliefcol[5]>10.00))
                        {
                        

                                alert("Choose Score from 1 to 10");
                                document.getElementById("txtSelfScoreBeli"+n.toString()).focus();
                                return false;
                        }
                       
                   }
                
                }

                 
                row6 = document.getElementById("<%= hdnTraits.ClientID %>").value.split("¥");
               
                var NewTraitsStr="";
                for (n = 1; n <= row6.length-1 ; n++) {     
                    var Traitscol=row6[n].split("µ"); 
                   
                     if(Traitscol[6]=="" )
                    {
                        alert("Should fill all the fields of Self score column in Traits and Behaviours Tab");
                        document.getElementById("txtSelfScoreTraits"+ n.toString()).focus(); return false;
                    }
                    else
                    {                         
                                            
                        NewTraitsStr+="¥"+Traitscol[0]+"µ"+Traitscol[6];
                       
                    }
                   
                     if(Traitscol[6]!="")
                    {
                  
                         if((Traitscol[6]<1.00) || (Traitscol[6]>10.00))
                        {
                    
                                alert("Choose Score from 1 to 10");
                                document.getElementById("txtSelfScoreTraits"+n.toString()).focus();
                                return false;
                        }
                       
                   }
                    
                }
           
           
            var ToData = "1Ø" + EmpCode + "Ø" +  DesignationDojID  + "Ø" + DesignationID + "Ø" + DesignationreviewID  + "Ø" + DepartmentID  + "Ø" + BranchID + "Ø" + ReportingTo + "Ø" +  ReportedBy + "Ø" + ReviewHeadID + "Ø" + FunctionalHeadID + "Ø" + PeriodID + "Ø" + Evaluation + "Ø" + Remarks + "Ø" +NewDutiesStr + "Ø" + document.getElementById("txtAvgDuties").value + "Ø" + document.getElementById("txtselfDuties").value + "Ø" + NewBeliefsStr+ "Ø" + document.getElementById("txtAvgBeli").value + "Ø" + document.getElementById("txtBeliselfRemarks").value +"Ø" + document.getElementById("txtSignificant1").value + "Ø" + document.getElementById("txtSignificant2").value + "Ø" + document.getElementById("txtSignificant3").value + "Ø" + document.getElementById("txtImprove1").value+ "Ø" + document.getElementById("txtImprove2").value+ "Ø" + document.getElementById("txtImprove3").value+ "Ø" + document.getElementById("txtPlan1").value+ "Ø" + document.getElementById("txtPlan2").value+ "Ø" + document.getElementById("txtPlan3").value + "Ø" + NewTraitsStr + "Ø" +document.getElementById("txtAvgTraits").value+ "Ø" + document.getElementById("txtAppraiseeCommitments").value + "Ø" + 0  + "Ø" + ''  + "Ø" + 0  + "Ø" + '' + "Ø"+ 0 + "Ø" + 0 + "Ø" +'' + "Ø" +'' + "Ø" +''+ "Ø" + 0 + "Ø" +''+ "Ø" + 0 + "Ø" + 0 + "Ø" +''+ "Ø" + 0 + "Ø" +''+ "Ø" + 0 + "Ø" + 0 + "Ø" +''+ "Ø" + 0 + "Ø" +'' ;    

            ToServer(ToData, 1);
         
            
        }
        function Fill(){
                table_fill();
                table_Signifill();
                table_ImprovementAreafill();
                table_DevelopmentPlanfill();
                table_AppraiseeCommentsfill();
                }


        function table_fill() 
       {
                var dis;
        
   
                var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if(StatusID==0)
                {
                    DisFlag = '';
                    dis= 'visibility:visible;';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                     dis= 'visibility:hidden;';


                }
         if (document.getElementById("<%= hdnDutiesDtl.ClientID %>").value != "") 
         {
            document.getElementById("<%= pnDuties.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var TotWeightage=0;
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>";
             
            tab += "<td style='width:40%;text-align:center'><b>Duties,Responsibilities & Targets  <b style='color:red;'> * </b> </b></td>"; 
            tab += "<td style='width:10%;text-align:center'><b>Weightage in % <b style='color:red;'> * </b> </b></td>";   
            tab += "<td style='width:10%;text-align:center'><b>Key Performance Indicators</b></td>";
            tab += "<td style='width:10%;text-align:center'><b>Status while reviewing </b></td>";    
            tab += "<td style='width:10%;text-align:center'><b>Performance Score(Self) <b style='color:red;'> * </b> </b></td>";  
            tab += "<td style='width:10%;text-align:center'><b>Performance Score(Supervisor)</b></td>";   
                   
            tab +="<td style='width:5%;text-align:center'><b>Delete</b></td>";
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       

            row = document.getElementById("<%= hdnDutiesDtl.ClientID %>").value.split("¥");
       
            for (n = 1; n <= row.length - 1; n++) 
            {
                 col = row[n].split("µ");
                
                 if (row_bg == 0) 
                 {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  else 
                  {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";  
                 
                        var Duities="";
                        var Weightage=0;
                        var KeyPerformance;
                        var selfScore;
                        var SuperScore;
                   
                    if(col[0]!="")
                    {
                    //  tab += "<td style='width:10%;text-align:left'>" + col[0] + "</td>";
                      Duities = col[0];
                      
                       var txtDuities = "<input id='txtDuities" + n + "' name='txtDuities" + n + "' value='" + Duities + "'    type='Text'  'maxlength='500'  "+ DisFlag + " style='width:99%;' onchange='updateValue("+ n + ")' class='NormalText' />";
               
                    tab += "<td style='width:40%;text-align:left' >"+ txtDuities +"</td>";
                    }
                    else
                    {
                    var txtDuities = "<input id='txtDuities" + n + "' name='txtDuities" + n + "' type='Text' maxlength='500' style='width:99%;' class='NormalText' "+ DisFlag + " onchange='updateValue("+ n + ")'/>";
                    tab += "<td style='width:40%;text-align:left' >"+ txtDuities +"</td>";          
           
                       
                    }
                    if(col[1]!="")
                    {
                 //  tab += "<td style='width:15%;text-align:left'>" + col[1] + "</td>";
                      Weightage = col[1];

                      TotWeightage = parseInt(TotWeightage) + parseInt(Weightage) ;
                     
                       var txtWeightage = "<input id='txtWeightage" + n + "' name='txtWeightage" + n + "' value='" + Math.abs(Weightage).toFixed(2) + "'  type='Text'  onkeypress='return NumericCheck(event)' "+ DisFlag + " TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='3'  class='NormalText' onchange='updateValue("+n+"); CalculateTotal()' />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtWeightage +"</td>";
                      
                    }
                    else
                    {
                    var txtWeightage = "<input id='txtWeightage" + n + "' name='txtWeightage" + n + "' type='Text'  TextMode='MultiLine' Wrap='True' Rows='1'  onkeypress='return NumericCheck(event)' "+ DisFlag + " style='width:99%;' word-wrap:'break-word;' maxlength='3' style='width:99%;' class='NormalText' onchange='updateValue("+n+"); CalculateTotal()'  />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtWeightage +"</td>";
                       
                    }
                    if(col[2]!="")
                    {
                   //   tab += "<td style='width:10%;text-align:left'>" + col[2] + "</td>";
                      KeyPerformance = col[2];
                      var txtKeyPerformance = "<input id='txtKeyPerformance" + n + "' name='txtKeyPerformance" + n + "'  value='" + KeyPerformance + "' type='Text'  style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='100' onchange='updateValue("+n+")'    />"; 
                        tab += "<td style='width:10%;text-align:left' >"+ txtKeyPerformance +"</td>";
                    }
                    else
                    {
                    var txtKeyPerformance = "<input id='txtKeyPerformance" + n + "' name='txtKeyPerformance" + n + "'  type='Text'  style='width:99%;' class='NormalText' maxlength='100' "+ DisFlag + "  onchange='updateValue("+n+")'  onblur='return checkPhone(this.value,"+ n +")' />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtKeyPerformance +"</td>";
                       
                    }
                     if(col[3]!="")
                    {
                  
                      Status   = col[3];
                    var txtStatus = "<input id='txtStatus" + n + "' name='txtStatus" + n + "' value='" + Status + "' type='Text'  style='width:99%;' class='NormalText' maxlength='100' "+ DisFlag + " onchange='updateValue("+n+")' />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtStatus +"</td>";
                       
                    }
                    else
                    {
                    var txtStatus = "<input id='txtStatus" + n + "' name='txtStatus" + n + "' type='Text'  style='width:99%;' class='NormalText' maxlength='100' "+ DisFlag + " onchange='updateValue("+n+")' />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtStatus +"</td>";
                       
                    }
                    
                   
                     if(col[4]!="")
                    {
              
                        selfScore = col[4];
                        var txtSelfScore = "<input id='txtSelfScore" + n + "' name='txtSelfScore" + n + "' value='" + Math.abs(selfScore).toFixed(2) + "' type='Text'  style='width:99%;'  class='NormalText' "+ DisFlag + " maxlength='3'  onblur='return isNumberKey(this.value)'  onkeypress='return NumericWithDot(this,event)'    onchange='updateValue("+ n + ");CalculateTotal()' />";
                        tab += "<td style='width:10%;text-align:left' >"+ txtSelfScore +"</td>";
                      
                    }
                    else
                    {
                        var txtSelfScore = "<input id='txtSelfScore" + n + "' name='txtSelfScore" + n + "' type='Text' style='width:99%;'  "+ DisFlag + " class='NormalText'  maxlength='3'   onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)'  onchange='updateValue("+n+");CalculateTotal()'     />";
                        tab += "<td style='width:10%;text-align:left' >"+ txtSelfScore +"</td>";
                       
                    }
                    if(col[5]!="")
                    {
                      
                          SupervisorDutiesScore = col[5];
                          var txtSupervisorDutiesScore = "<input id='txtSupervisorDutiesScore" + n + "' name='txtSupervisorDutiesScore" + n + "' value='" + Math.abs(SupervisorDutiesScore).toFixed(2) + "' type='Text'  style='width:99%;'  disabled class='NormalText'  maxlength='3'    onkeypress='return NumericWithDot(this,event)'    onchange='updateValue("+ n + ");CalculateTotal()' />";
                          tab += "<td style='width:10%;text-align:left' >"+ txtSupervisorDutiesScore +"</td>";

                   
                 
                    }
                    else
                    {
                     var txtSupervisorDutiesScore = "<input id='txtSupervisorDutiesScore" + n + "' name='txtSupervisorDutiesScore" + n + "' type='Text' style='width:99%;'  class='NormalText'  maxlength='3'  disabled     onkeypress='return NumericWithDot(this,event)'  onchange='updateValue("+n+");CalculateTotal()'     />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtSupervisorDutiesScore +"</td>";
                  
                 

                    }
                    if(col[0] == "" && col[1] == "" && col[2] == "" && col[3] == "" && col[4] =="" && col[5] =="  ") 
                    {
                    
                    tab += "<td style='width:5%;text-align:center' onclick=DeleteRow('" + n + "')><img  src='../../Image/cross.png'; style='align:middle;cursor:pointer;  "+ dis + " height:15px; width:15px ;' title='Delete'/></td>";
                         
                  
                    
                    }
                    else
                    {
                       tab += "<td style='width:5%;text-align:center' onclick=DeleteRow('" + n + "')><img  src='../../Image/cross.png'; style='align:middle;cursor:pointer; "+ dis + "  height:15px; width:15px ;' title='Delete'/></td>";
                        
               
                    }
                    tab += "</tr>";
                    }
                    var self="";
                    var Avg="";
                    var Supervisor="";
                     
                     row1 = document.getElementById("<%= hdnDutiesDtl1.ClientID %>").value.split("¥");
                     
                     col = row1[1].split("µ");
                     
                    tab += "<tr height=20px; class='tblQal'>";
                    tab += "<td colspan=2 style='width:45%;text-align:center;font-weight:bold;'></td>";
                    
                    var txtPerentage = "<input id='txtPerentage' name='txtPerentage' value="+ TotWeightage +"  type='Text'   style='width:99%;'  class='ReadOnlyTextBox'  />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtPerentage +"</td>";

                    tab += "<td colspan=2 style='width:20%;text-align:center;font-weight:bold;'>Weighted Average Score</td>";
                
                   
                    if(col[1]!="")
                    {
                
                      AvgDuties = col[1];
                       var txtAvgDuties = "<input id='txtAvgDuties' name='txtAvgDuties' value='" + Math.abs(AvgDuties).toFixed(2) + "'  type='Text' onblur='return isNumberKey(this.value)'  style='width:99%;'  class='NormalText'  disabled=true   onchange='updateValue("+n+")'  />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtAvgDuties +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtAvgDuties = "<input id='txtAvgDuties' name='txtAvgDuties' type='Text' style='width:99%;'  onblur='return isNumberKey(this.value)' value='0' class='NormalText'  disabled=true    onchange='updateValue("+n+")' />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtAvgDuties +"</td>";
                       
                    }
                  if(col[1]!="")
                    {
                
                      AvgSuperDuties = col[1];
                       var txtAvgSuperDuties = "<input id='txtAvgSuperDuties' name='txtAvgSuperDuties' value='" + Math.abs(AvgSuperDuties).toFixed(2) + "'  type='Text'   style='width:99%;'  class='NormalText' disabled onchange='updateValue("+n+")'  />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtAvgSuperDuties +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtAvgSuperDuties = "<input id='txtAvgSuperDuties' name='txtAvgSuperDuties' type='Text' style='width:99%;'   value='0' class='NormalText'  disabled  onchange='updateValue("+n+")' />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtAvgSuperDuties +"</td>";
                       
                    }
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>"; 
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td colspan=2 style='width:20%;text-align:center' ><b>Remarks by Self </b></td>";
                    if(col[2]!="")
                    {
                
                        SelfRemarksDuties = col[2];
                        var txtselfDuties = "<input id='txtselfDuties' name='txtselfDuties' value='" + SelfRemarksDuties + "'  type='Text'  style='width:99%;'  class='NormalText'  maxlength='500'  "+ DisFlag + "  onchange='updateValue("+n+")'  />";
                        tab += "<td colspan=5 style='width:77%;text-align:left' >"+ txtselfDuties +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtselfDuties = "<input id='txtselfDuties' name='txtselfDuties' type='Text' style='width:99%;'  "+ DisFlag + "   class='NormalText'  maxlength='500'  onchange='updateValue("+n+")'' />";
                    tab += "<td colspan=5 style='width:77%;text-align:left' >"+ txtselfDuties +"</td>";
                       
                    }
                   // tab += "<td colspan=4 style='width:77%;text-align:left' ><input id='txtself' name='txtself' type='Text' maxlength='500' style='width:99%;' class='NormalText' /></td>";          
                   
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>";   
                       
                    tab += "<tr height=20px;  class='tblQal' >";
                    var SupervisorRemarksDuties="";
                    tab += "<td colspan=2 style='width:20%;text-align:center' ><b>Remarks by Supervisor </b></td>";
                     if(col[3]!="")
                    {
                
                      SupervisorRemarksDuties = col[3];
                       var txtSupervisorRemarksDuties = "<input id='txtSupervisorRemarksDuties' name='txtSupervisorRemarksDuties' value='" + SupervisorRemarksDuties + "'  type='Text'   style='width:99%;'  class='NormalText'  maxlength='500'   disabled onchange='updateValue("+n+")'  />";
                    tab += "<td colspan=5 style='width:77%;text-align:left' >"+ txtSupervisorRemarksDuties +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtSupervisorRemarksDuties = "<input id='txtSupervisorRemarksDuties' name='txtSupervisorRemarksDuties' type='Text' style='width:99%;'    class='NormalText'  maxlength='500'   disabled  onchange='updateValue("+n+")'' />";
                    tab += "<td colspan=5 style='width:77%;text-align:left' >"+ txtSupervisorRemarksDuties +"</td>";
                       
                    }
                
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>";         

                tab += "</table></div></div></div>"; 
                document.getElementById("<%= pnDuties.ClientID %>").innerHTML = tab;
                Calculate();
                }
                else
                document.getElementById("<%= pnDuties.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
                 
               
        }
      

     function isNumberKey(Val) {
                if (Val!="")
                {
                    if((Val<1) || (Val >10) ){
                        alert('choose number from 1-10');
                         return false;
                    }
       
                   else {
                        return true;
                    }
              }
    }
 
        function table_fillBeliefs(Arg) 
       {
            var Row=Arg.split("Ø");
           
            /////document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value=Row[3];
           var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if(StatusID==0)
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
          
         document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value=Row[1];
         if (document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value != "") 
         {
            document.getElementById("<%= pnBeliefsDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>";
             
            tab += "<td style='width:8%;text-align:center'><b></b></td>"; 
            tab += "<td style='width:15%;text-align:center'><b>Parameters</b></td>";   
            tab += "<td style='width:35%;text-align:center'><b>Suggestive Measurement Points</b></td>";
            tab += "<td style='width:18%;text-align:center'><b>Activities Done with Values/Timelines </b></td>";    
            tab += "<td style='width:8%;text-align:center'><b>Self score</b></td>";  
            tab += "<td style='width:8%;text-align:center'><b>Supervisor's Score</b></td>";   
                   
            tab +="<td style='width:3%;text-align:left'></td>";
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       

            row = document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value.split("¥");
            var m=0;

            for (n = 1; n <= row.length-1 ; n++) 
            {
                 col = row[n].split("µ");
                
                if (row_bg == 1) 
                 {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  else 
                  {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  m=n+1;
                  tab += "<td style='width:5%;text-align:center' >" + n + "</td>";  
                 
                        var Category="";
                        var Parameters="";
                        var MeasurementPoint="";
                        var selfScore="";
                        var SuperScore="";
                  
                    if(col[1]!="")
                    {
                    //  tab += "<td style='width:10%;text-align:left'>" + col[0] + "</td>";
                      Category = col[1];
                      
                       var txtCategory = "<input id='txtCategory" + n + "' name='txtCategory" + n + "' value='" + Category + "'    type='Text'  'maxlength='200' style='width:99%;'  disabled=true class='NormalText' onchange='updateScore("+ m +")' />";
               
                    tab += "<td style='width:8%;text-align:left' >"+ txtCategory +"</td>";
                    }
                    else
                    {
                    var txtCategory = "<input id='txtCategory" + n + "' name='txtCategory" + n + "' type='Text' maxlength='200' style='width:99%;' class='NormalText'  disabled=true onchange='updateScore("+ m +")'/>";
                    tab += "<td style='width:8%;text-align:left' >"+ txtCategory +"</td>";          
           
                       
                    }
                    if(col[2]!="")
                    {
                 //  tab += "<td style='width:15%;text-align:left'>" + col[1] + "</td>";
                      Parameters = col[2];
                       var txtParameters = "<input id='txtParameters" + n + "' name='txtParameters" + n + "' value='" + Parameters + "'  type='Text'  disabled=true  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='3'  class='NormalText' onchange='updateScore("+m+")' />";
                    tab += "<td style='width:15%;text-align:left' >"+ txtParameters +"</td>";
                      
                    }
                    else
                    {
                    var txtParameters = "<input id='txtParameters" + n + "' name='txtParameters" + n + "' type='Text'  TextMode='MultiLine' Wrap='True' Rows='1' disabled=true style='width:99%;' word-wrap:'break-word;' maxlength='3' style='width:99%;' class='NormalText' onchange='updateScore("+m+")' />";
                    tab += "<td style='width:15%;text-align:left' >"+ txtParameters +"</td>";
                       
                    }
                    if(col[3]!="")
                    {
                   //   tab += "<td style='width:10%;text-align:left'>" + col[2] + "</td>";
                      MeasurementPoint = col[3];
                    
                      var txtMeasurementPoint = "<input id='txtMeasurementPoint" + n + "' name='txtMeasurementPoint" + n + "'  value='" + MeasurementPoint + "' type='Text'  style='width:99%;' disabled=true class='NormalText' maxlength='10' onchange='updateScore("+m+")'    />"; 
                        tab += "<td style='width:35%;text-align:left' >"+ txtMeasurementPoint +"</td>";
                    }
                    else
                    {
                    var txtMeasurementPoint = "<input id='txtMeasurementPoint" + n + "' name='txtMeasurementPoint" + n + "'  type='Text'  style='width:99%;' class='NormalText' maxlength='10'  disabled=true onchange='updateScore("+m+")'   />";
                    tab += "<td style='width:35%;text-align:left' >"+ txtMeasurementPoint +"</td>";
                       
                    }
                     if(col[4]!="")
                    {
                
                      Activities   = col[4];
                    
                    var txtActivities = "<input id='txtActivities" + n + "' name='txtActivities" + n + "' value='" + Activities + "' type='Text'  style='width:99%;' class='NormalText' maxlength='500' "+ DisFlag + " onchange='updateValueBelief("+ n +")'  />";
                    tab += "<td style='width:18%;text-align:left' >"+ txtActivities +"</td>";
                       
                    }
                    else
                    {
                   
                    var txtActivities = "<input id='txtActivities" + n + "' name='txtActivities" + n + "' type='Text'  style='width:99%;' class='NormalText' maxlength='500' "+ DisFlag + "   onchange='updateValueBelief("+ n +")' />";
                    tab += "<td style='width:18%;text-align:left' >"+ txtActivities +"</td>";
                       
                    }
                    
                     if(col[5]!="")
                    {
                
                      selfScoreBelief = col[5];
                       var txtSelfScoreBeli = "<input id='txtSelfScoreBeli" + n + "' name='txtSelfScoreBeli" + n + "'  value='" + Math.abs(selfScoreBelief).toFixed(2) + "' type='Text' onkeypress='return NumericWithDot(this,event)' "+ DisFlag + " onblur='return isNumberKey(this.value)'  style='width:99%;'  class='NormalText'  maxlength='3'  onchange='updateValueBelief("+ n +");CalculateScoreBeli()'  />";
                    tab += "<td style='width:8%;text-align:left' >"+  txtSelfScoreBeli +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtSelfScoreBeli = "<input id='txtSelfScoreBeli" + n + "' name='txtSelfScoreBeli" + n + "' type='Text' style='width:99%;'  class='NormalText'  maxlength='3' "+ DisFlag + " onkeypress='return NumericWithDot(this,event)'  onblur='return isNumberKey(this.value)'   onchange='updateValueBelief("+ n +");CalculateScoreBeli()' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtSelfScoreBeli +"</td>";
                       
                    }
                  
                   if(col[6]!="")
                    {
                
                      SupervisorScoreBeli = col[6];
                       var txtSupervisorScoreBeli = "<input id='txtSupervisorScoreBeli" + n + "' name='txtSupervisorScoreBeli" + n + "'  value='" + Math.abs(SupervisorScoreBeli).toFixed(2)  + "' type='Text' disabled    style='width:99%;'  class='NormalText'  maxlength='3'  onchange='updateValueBelief("+ n +");CalculateScoreBeli()'  />";
                    tab += "<td style='width:8%;text-align:left' >"+  txtSupervisorScoreBeli +"</td>";
                      
                    }
                    else
                    {
                     var txtSupervisorScoreBeli = "<input id='txtSupervisorScoreBeli" + n + "'  name='txtSupervisorScoreBeli" + n + "'  type='Text'   disabled   style='width:99%;'  class='NormalText'  maxlength='3'  onchange='updateValueBelief("+ n +");CalculateScoreBeli()' />";
                    tab += "<td  style='width:8%;text-align:left' >"+ txtSupervisorScoreBeli +"</td>";
                 
                  

                    }
                    
              
                  
                    //tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "<td style='width:3%;text-align:center' ></td>";
                    tab += "</tr>";
                    }
                    var self="";
                    var Avg="";
                    var Supervisor="";
                    //PendingAdvance = Math.abs(col[4]);
                     document.getElementById("<%= hdnBeliefsDtl1.ClientID %>").value=Row[2];
                     row = document.getElementById("<%= hdnBeliefsDtl1.ClientID %>").value.split("¥");
                     
                     col = row[1].split("µ");
                    
                        tab += "<tr height=20px; class='tblQal'>";
                        tab += "<td colspan=5 style='width:77%;text-align:center;font-weight:bold;'> Average Score</td>";

                    var AvgBeli=""
                     if(col[1]!="")
                    {
                
                      AvgBeli = col[1];
                       var txtAvgBeli = "<input id='txtAvgBeli' name='txtAvgBeli' value='" + Math.abs(AvgBeli).toFixed(2) + "'  type='Text'   onblur='return isNumberKey(this.value)' disabled=true  style='width:99%;'  class='NormalText'   onkeypress='return NumericCheck(event)'  onblur='return checkvalue(this.value,"+ n +")' onchange='updateValueBelief("+ n +")'  />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtAvgBeli +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtAvgBeli = "<input id='txtAvgBeli' name='txtAvgBeli' type='Text' style='width:99%;'  onblur='return isNumberKey(this.value)'  value='0' class='NormalText'  disabled=true     onchange='updateValueBelief("+ n +")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtAvgBeli +"</td>";
                       
                    }
                   if(col[2]!="")
                    {
                
                      SupervisorAvgBeli = col[2];
                       var txtSupervisorAvgBeli = "<input id='txtSupervisorAvgBeli' name='txtSupervisorAvgBeli' value='" + Math.abs(SupervisorAvgBeli).toFixed(2) + "' type='Text'    disabled=true   style='width:99%;'  class='NormalText'  maxlength='3'     />";
                    tab += "<td style='width:8%;text-align:left' >"+  txtSupervisorAvgBeli +"</td>";
                      
                    }
                    else
                    {
                     var txtSupervisorAvgBeli = "<input id='txtSupervisorAvgBeli' name='txtSupervisorAvgBeli' type='Text' style='width:99%;'  value='0'  class='NormalText'  disabled=true    />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtSupervisorAvgBeli +"</td>";
                     

                    }
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>"; 
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td colspan=2 style='width:10%;text-align:center' ><b>Remarks by Self </b></td>";
                    var BeliselfRemarks=""
                     if(col[2]!="")
                    {
                
                      BeliselfRemarks = col[2];
                       var txtBeliselfRemarks = "<input id='txtBeliselfRemarks' name='txtBeliselfRemarks' value='" + BeliselfRemarks + "'  type='Text'  style='width:99%;'  class='NormalText' "+ DisFlag + "  maxlength='200'    onchange='updateValueBelief("+ n +")'  />";
                    tab += "<td colspan=5 style='width:87%;text-align:left' >"+ txtBeliselfRemarks +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtBeliselfRemarks = "<input id='txtBeliselfRemarks' name='txtBeliselfRemarks' type='Text' style='width:99%;'  class='NormalText'  maxlength='200'  "+ DisFlag + " onchange='updateValueBelief("+ n +")' />";
                    tab += "<td colspan=5 style='width:87%;text-align:left' >"+ txtBeliselfRemarks +"</td>";
                       
                    }
                   //// tab += "<td colspan=4 style='width:77%;text-align:left' ><input id='txtBeliselfRemarks' name='txtBeliselfRemarks' type='Text' maxlength='500' style='width:99%;' class='NormalText' /></td>";          
                  
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>";   
                    tab += "<tr height=20px;  class='tblQal' >";
                    tab += "<td colspan=2 style='width:10%;text-align:center' ><b>Remarks by Supervisor </b></td>";
                    if(col[4]!="")
                    {
                
                      BeliSupervisorRemarks = col[4];
                       var txtBeliSupervisorRemarks = "<input id='txtBeliSupervisorRemarks' name='txtBeliSupervisorRemarks' value='" + BeliSupervisorRemarks + "'  type='Text'  style='width:99%;' disabled class='NormalText'  maxlength='200'     />";
                    tab += "<td colspan=5 style='width:87%;text-align:left' >"+ txtBeliSupervisorRemarks +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtBeliSupervisorRemarks = "<input id='txtBeliSupervisorRemarks' name='txtBeliSupervisorRemarks' type='Text' style='width:99%;'  disabled  class='NormalText'      />";
                    tab +="<td colspan=5 style='width:87%;text-align:left' >"+ txtBeliSupervisorRemarks +"</td>";
                       
                    }
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>";         

                tab += "</table></div></div></div>"; 
                document.getElementById("<%= pnBeliefsDtl.ClientID %>").innerHTML = tab;
               
                
                }
                else
                document.getElementById("<%= pnBeliefsDtl.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
              
               
        }

        function updateValueBelief(n)
        {  
       
             row = document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value.split("¥");
            
             var newStr=""
              for (n = 1; n <= row.length - 1; n++) 
                {
                        var col = row[n].split("µ");
                   
                        var Activities=document.getElementById("txtActivities"+ n.toString()).value;
                           
                        var SelfScore = document.getElementById("txtSelfScoreBeli"+ n.toString()).value;
                       
                        newStr+="¥" +col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3]  + "µ" +Activities + "µ" + SelfScore ;  
                       
               
                }

                
            document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value=newStr;
          
        }
        
        function table_Signifill(Arg) 
       {
          var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if(StatusID==0)
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
             var Row=Arg.split("Ø");
            

            document.getElementById("<%= hdnSignificant.ClientID %>").value=Row[3];
        
           if (document.getElementById("<%= hdnSignificant.ClientID %>").value != "") 
           {
            document.getElementById("<%= pnSignificant.ClientID %>").style.display = '';
          
            var tab = "";
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            
            row = document.getElementById("<%= hdnSignificant.ClientID %>").value.split("¥");
            
            var Significant1="";
            var Significant2="";
            var Significant3="";
          
                 col = row[1].split("µ");
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 1 + "</td>";  
                
              



                  if(col[1]!="")
                  {
                     
                     
                        Significant1   = col[1];
                       
                        var txtSignificant1 = "<textarea id='txtSignificant1' name='txtSignificant1' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'>"+Significant1+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant1 +"</td>";
                       
                  }
                  else
                  {
                   
                        var txtSignificant1 ="<textarea id='txtSignificant1' name='txtSignificant1' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant1 +"</td>";
                       
                  }
                    
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 2 + "</td>";
                   if(col[2]!="")
                  {
                     
                     
                        Significant2   = col[2];
                        var txtSignificant2 = "<textarea id='txtSignificant2' name='txtSignificant2' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'>"+Significant2+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant2 +"</td>";
                       
                     
                  }
                  else
                  {
                   
                        var txtSignificant2 ="<textarea id='txtSignificant2' name='txtSignificant2' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant2 +"</td>";
                       
                       
                       
                  }
                    
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 3 + "</td>";
                     if(col[3]!="")
                  {
                     
                     
                        Significant3   = col[3];
                                                                  
                        var txtSignificant3 = "<textarea id='txtSignificant3' name='txtSignificant3' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'>"+Significant3+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant3 +"</td>";
                       


                  }
                  else
                  {
                        var txtSignificant3 ="<textarea id='txtSignificant3' name='txtSignificant3' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant3 +"</td>";
                       
                   
                      
                  }
                    
                  tab += "</tr>";
            
            
            tab += "</table></div>";
            document.getElementById("<%= pnSignificant.ClientID %>").innerHTML = tab;
             }
                else
                document.getElementById("<%= pnSignificant.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
              
               

        }

        function table_ImprovementAreafill(Arg) 
       {
            
            var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if(StatusID==0)
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
             var Row=Arg.split("Ø");
            

            document.getElementById("<%= hdnImprovementArea.ClientID %>").value=Row[4];
        
           if (document.getElementById("<%= hdnImprovementArea.ClientID %>").value != "") 
           {
            document.getElementById("<%= pnImprovementArea.ClientID %>").style.display = '';
          
            var tab = "";
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            
            row = document.getElementById("<%= hdnImprovementArea.ClientID %>").value.split("¥");
            
            var Improve1="";
            var Improve2="";
            var Improve3="";
          
                 col = row[1].split("µ");
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 1 + "</td>";  
                 
                 if(col[1]!="")
                  {
                     
                     
                        Improve1   = col[1];
                       
                        var txtImprove1 = "<textarea id='txtImprove1' name='txtImprove1' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'>"+Improve1+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove1 +"</td>";
                       
                  }
                  else
                  {
                   
                        var txtImprove1 ="<textarea id='txtImprove1' name='txtImprove1' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove1 +"</td>";
                       
                  }




                 
                    
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 2 + "</td>";

                   if(col[2]!="")
                  {
                     
                     
                        Improve2   = col[2];
                        var txtImprove2 = "<textarea id='txtImprove2' name='txtImprove2' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'>"+Improve2+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove2 +"</td>";
                       
                     
                  }
                  else
                  {
                   
                        var txtImprove2 ="<textarea id='txtImprove2' name='txtImprove2' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove2 +"</td>";
                       
                       
                       
                  }
                  
                    
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 3 + "</td>";

                  if(col[3]!="")
                  {
                     
                     
                        Improve3   = col[3];
                                                                  
                        var txtImprove3 = "<textarea id='txtImprove3' name='txtImprove3' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'>"+Improve3+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove3 +"</td>";
                       


                  }
                  else
                  {
                        var txtImprove3 ="<textarea id='txtImprove3' name='txtImprove3' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove3 +"</td>";
                       
                   
                      
                  }
                  
                    
                  tab += "</tr>";
            
            
            tab += "</table></div>";
            document.getElementById("<%= pnImprovementArea.ClientID %>").innerHTML = tab;
             }
                else
                document.getElementById("<%= pnImprovementArea.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
              

        }

         function table_DevelopmentPlanfill(Arg) 
        {
            var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if(StatusID==0)
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
             var Row=Arg.split("Ø");
            

            document.getElementById("<%= hdnDevelopmentPlan.ClientID %>").value=Row[5];
        
           if (document.getElementById("<%= hdnDevelopmentPlan.ClientID %>").value != "") 
           {
            document.getElementById("<%= pnDevelopmentPlan.ClientID %>").style.display = '';
          
            var tab = "";
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            
            row = document.getElementById("<%= hdnDevelopmentPlan.ClientID %>").value.split("¥");
           
            var Plan1="";
            var Plan2="";
            var Plan3="";
          
                 col = row[1].split("µ");
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 1 + "</td>";  
                 if(col[1]!="")
                  {
                     
                     
                        Plan1   = col[1];
                       
                        var txtPlan1 = "<textarea id='txtPlan1' name='txtPlan1' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'>"+Plan1+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan1 +"</td>";
                       
                  }
                  else
                  {
                   
                        var txtPlan1 ="<textarea id='txtPlan1' name='txtPlan1' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan1 +"</td>";
                       
                  }

                  
                    
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 2 + "</td>";
                  if(col[2]!="")
                  {
                     
                     
                        Plan2   = col[2];
                        var txtPlan2 = "<textarea id='txtPlan2' name='txtPlan2' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'>"+Plan2+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan2 +"</td>";
                       
                     
                  }
                  else
                  {
                   
                        var txtPlan2 ="<textarea id='txtPlan2' name='txtPlan2' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan2 +"</td>";
                       
                       
                       
                  }
                  
                  
                 
                    
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 3 + "</td>";
                     if(col[3]!="")
                  {
                     
                     
                        Plan3   = col[3];
                                                                  
                        var txtPlan3 = "<textarea id='txtPlan3' name='txtPlan3' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'>"+Plan3+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan3 +"</td>";
                       


                  }
                  else
                  {
                        var txtPlan3 ="<textarea id='txtPlan3' name='txtPlan3' style='width:100%;text-align:left;' rows=1; '  "+ DisFlag + " class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan3 +"</td>";
                       
                   
                      
                  }
                  
                 
                    
                  tab += "</tr>";
            
            
            tab += "</table></div>";
            document.getElementById("<%= pnDevelopmentPlan.ClientID %>").innerHTML = tab;
             }
                else
                document.getElementById("<%= pnDevelopmentPlan.ClientID %>").style.display = 'none';
               //--------------------- Clearing Data ------------------------//
      
            


        }

        function table_fillTraits(Arg) 
       {
            var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if(StatusID==0)
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
             var Row=Arg.split("Ø");
             document.getElementById("<%= hdnTraits.ClientID %>").value=Row[6];
   
       
         if (document.getElementById("<%= hdnTraits.ClientID %>").value != "") 
         {
            document.getElementById("<%= pnTraits.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>";
             
            tab += "<td style='width:13%;text-align:center'><b>Factors</b></td>"; 
            tab += "<td style='width:16%;text-align:center'><b>9-10 (Excellent)</b></td>";   
            tab += "<td style='width:17%;text-align:center'><b>6-8 (Good)</b></td>";
            tab += "<td style='width:17%;text-align:center'><b>3-5 (Fair)</b></td>";  
            tab += "<td style='width:14%;text-align:center'><b>1-2 (Poor)</b></td>";      
            tab += "<td style='width:7%;text-align:center'><b>Self score</b></td>";  
            tab += "<td style='width:7%;text-align:center'><b>Supervisor's Score</b></td>";        
            tab +="<td style='width:3%;text-align:left'></td>";
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       

            row = document.getElementById("<%= hdnTraits.ClientID %>").value.split("¥");
           
            var m=0;

            for (n = 1; n <= row.length-1 ; n++) 
            {
                 col = row[n].split("µ");
               
                if (row_bg == 1) 
                 {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  else 
                  {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  m=n+1;
                  tab += "<td style='width:5%;text-align:center' >" + n + "</td>";  
                 
                        var Category="";
                        var Parameters="";
                        var MeasurementPoint="";
                        var selfScore="";
                        var SuperScore="";
                  
                    if(col[1]!="")
                    {
                    //  tab += "<td style='width:10%;text-align:left'>" + col[0] + "</td>";
                      Factors = col[1];
                      
                      var txtFactors ="<textarea class='NormalText' id='txtFactors"+ n +"' name='txtFactors" + n + "'   "+ DisFlag + " style='width:100%;text-align:left;' rows=9; disabled=true >"+Factors+"</textarea>";
//////                       var txtCategory = "<input id='txtCategory" + m + "' name='txtCategory" + m + "' value='" + Category + "'    TextMode='MultiLine' Wrap='True' Rows='2'  type='Text'  'maxlength='200' style='width:99%;' class='NormalText' onchange='updateScore("+ m +")' />";
                        tab += "<td style='width:13%;text-align:left' >"+ txtFactors +"</td>";
                    }
                    else
                    {

                      var txtFactors ="<textarea class='NormalText' id='txtFactors"+ n +"' name='txtFactors" + n + "'  "+ DisFlag + " style='width:100%;text-align:left;' rows=9; disabled=true></textarea>";

////                    var txtCategory = "<input id='txtCategory" + m + "' name='txtCategory" + m + "' type='Text' maxlength='200' style='width:99%;' class='NormalText'   TextMode='MultiLine' Wrap='True' Rows='2' onchange='updateScore("+ m +")'/>";
                    tab += "<td style='width:13%;text-align:left' >"+ txtFactors +"</td>";          
           
                       
                    }
                    if(col[2]!="")
                    {
                 //  tab += "<td style='width:15%;text-align:left'>" + col[1] + "</td>";
                      Excellent = col[2];
                      var txtExcellent ="<textarea class='NormalText' id='txtExcellent"+ n +"' name='txtExcellent" + n + "'   "+ DisFlag + " style='width:100%;text-align:left;' rows=9; disabled=true>"+Excellent+"</textarea>";
//////                       var txtCategory = "<input id='txtCategory" + m + "' name='txtCategory" + m + "' value='" + Category + "'    TextMode='MultiLine' Wrap='True' Rows='2'  type='Text'  'maxlength='200' style='width:99%;' class='NormalText' onchange='updateScore("+ m +")' />";
                        tab += "<td style='width:16%;text-align:left' >"+ txtExcellent +"</td>";
                    }
                    else
                    {
                    var txtExcellent ="<textarea class='NormalText' id='txtExcellent"+ n +"' name='txtExcellent" + n + "'  "+ DisFlag + " style='width:100%;text-align:left;' rows=9; disabled=true></textarea>";

////                    var txtCategory = "<input id='txtCategory" + m + "' name='txtCategory" + m + "' type='Text' maxlength='200' style='width:99%;' class='NormalText'   TextMode='MultiLine' Wrap='True' Rows='2' onchange='updateScore("+ m +")'/>";
                    tab += "<td style='width:16%;text-align:left' >"+ txtExcellent +"</td>";          
             
                    }
                    if(col[3]!="")
                    {
                   //   tab += "<td style='width:10%;text-align:left'>" + col[2] + "</td>";
                      Good = col[3];
                     var txtGood ="<textarea class='NormalText' id='txtGood"+ n +"' name='txtGood" + n + "'   style='width:100%;text-align:left;' "+ DisFlag + " rows=9; disabled=true>"+Good+"</textarea>";
                     tab += "<td style='width:17%;text-align:left' >"+ txtGood +"</td>";
                    }
                    else
                    {
                    var txtGood ="<textarea class='NormalText' id='txtGood"+ n +"' name='txtGood" + n + "'  style='width:100%;text-align:left;' "+ DisFlag + " rows=9; disabled=true></textarea>";

////                    var txtCategory = "<input id='txtCategory" + m + "' name='txtCategory" + m + "' type='Text' maxlength='200' style='width:99%;' class='NormalText'   TextMode='MultiLine' Wrap='True' Rows='2' onchange='updateScore("+ m +")'/>";
                    tab += "<td style='width:17%;text-align:left' >"+ txtGood +"</td>";          
             
                    }
                     if(col[4]!="")
                    {
                     
                         Fair = col[4];
                         var txtFair ="<textarea class='NormalText' id='txtFair"+ n +"' name='txtFair" + n + "'   style='width:100%;text-align:left;' "+ DisFlag + " rows=9; disabled=true>"+Fair+"</textarea>";
                         tab += "<td style='width:17%;text-align:left' >"+ txtFair +"</td>";
                    
                    }
                    else
                    {
                   
                      var txtFair ="<textarea class='NormalText' id='txtFair"+ n +"' name='txtFair" + n + "'  style='width:100%;text-align:left;' "+ DisFlag + " rows=9; disabled=true></textarea>";
                      tab += "<td style='width:17%;text-align:left' >"+ txtFair +"</td>";          
              
                    }
                    if(col[5]!="")
                    {
                  
                         Poor = col[5];
                         var txtPoor ="<textarea class='NormalText' id='txtPoor"+ n +"' name='txtPoor" + n + "'  "+ DisFlag + "  style='width:100%;text-align:left;' rows=9; disabled=true>"+Poor+"</textarea>";
                         tab += "<td style='width:14%;text-align:left' >"+ txtPoor +"</td>";
                    }
                    else
                    {
                        var txtPoor ="<textarea class='NormalText' id='txtPoor"+ n +"' name='txtPoor" + n + "'  "+ DisFlag + " style='width:100%;text-align:left;' rows=9; disabled=true></textarea>";
                        tab += "<td style='width:14%;text-align:left' >"+ txtPoor +"</td>";          
              
                       
                    }
                    
                     if(col[6]!="")
                    {
               
                          SelfScoreTraits = col[6];
                     
                         var txtSelfScoreTraits ="<textarea class='NormalText' id='txtSelfScoreTraits"+ n +"' name='txtSelfScoreTraits" + n + "'   "+ DisFlag + " style='width:100%;text-align:left;' rows=9; onkeypress='return NumericWithDot(this,event)'  maxlength=3  onblur='return isNumberKey(this.value)' onchange='updateValueTraits("+n+");CalculateScoreTraits()' >"+Math.abs(SelfScoreTraits).toFixed(2)+"</textarea>";
                         tab += "<td style='width:7%;text-align:left' >"+ txtSelfScoreTraits +"</td>";

                      
                      
                    }
                    else
                    {
                   

                      var txtSelfScoreTraits ="<textarea class='NormalText' id='txtSelfScoreTraits"+ n +"' name='txtSelfScoreTraits" + n + "'  "+ DisFlag + " value='0' style='width:100%;text-align:left;'  rows=9; onkeypress='return NumericWithDot(this,event)' maxlength=3   onblur='return isNumberKey(this.value)' onchange='updateValueTraits("+n+");CalculateScoreTraits()' ></textarea>";
                        tab += "<td style='width:7%;text-align:left' >"+ txtSelfScoreTraits +"</td>";          
    
                    }
                  
                  if(col[7]!="")
                    {
               
                          SupervisorScoreTraits = col[7];
                     
                         var txtSupervisorScoreTraits ="<textarea class='NormalText' id='txtSupervisorScoreTraits"+ n +"' name='txtSupervisorScoreTraits" + n + "'  rows=9; style='width:100%;text-align:left;' maxlength=3  disabled onchange='updateValueTraits("+n+");CalculateScoreTraits()' >"+Math.abs(SupervisorScoreTraits).toFixed(2)+"</textarea>";
                         tab += "<td style='width:7%;text-align:left' >"+ txtSupervisorScoreTraits +"</td>";
  
                      
                    }
                    else
                    {
                   

                      var txtSupervisorScoreTraits ="<textarea class='NormalText' id='txtSupervisorScoreTraits"+ n +"' name='txtSupervisorScoreTraits" + n + "'   value='0' style='width:100%;text-align:left;'  rows=9;  maxlength=3   disabled=true onchange='updateValueTraits("+n+");CalculateScoreTraits()' ></textarea>";
                        tab += "<td style='width:7%;text-align:left' >"+ txtSupervisorScoreTraits +"</td>";          
    
                    }
                   
                    tab += "<td style='width:3%;text-align:center' ></td>";
                    tab += "</tr>";
                    }
                    var self="";
                    var Avg="";
                    var Supervisor="";

                     document.getElementById("<%= hdnTraits1.ClientID %>").value=Row[7];
                     var row1 = document.getElementById("<%= hdnTraits1.ClientID %>").value.split("¥");
                     col = row1[1].split("µ");
                    
                     var AvgTraits=""
                   
                    tab += "<tr height=20px; class='tblQal'>";
                    tab += "<td colspan=6 style='width:81%;text-align:center;font-weight:bold;'> Average Score</td>";
                    if(col[1]!="")
                    {

                        AvgTraits   = col[1];
                        var txtAvgTraits = "<input id='txtAvgTraits' name='txtAvgTraits' value='" + Math.abs(AvgTraits).toFixed(2) + "' type='Text'  style='width:99%;' onblur='return isNumberKey(this.value)'  "+ DisFlag + " class='NormalText' maxlength='10'  disabled=true  />";
                        tab += "<td style='width:8%;text-align:left' >"+ txtAvgTraits +"</td>";
                       
                      }
                      else
                      {
                   
                            var txtAvgTraits = "<input id='txtAvgTraits' name='txtAvgTraits' type='Text'  value='0' style='width:99%;' class='NormalText'  onblur='return isNumberKey(this.value)' "+ DisFlag + " maxlength='10' disabled=true  />";
                            tab += "<td style='width:8%;text-align:left' >"+ txtAvgTraits +"</td>";
                       
                      }
                    if(col[2]!="")
                    {

                        SupervisorAvgTraits   = col[2];
                        var txtSupervisorAvgTraits = "<input id='txtSupervisorAvgTraits' name='txtSupervisorAvgTraits' value='" + Math.abs(SupervisorAvgTraits).toFixed(2) + "' type='Text'  style='width:99%;'  value='0' class='NormalText' maxlength='3'  disabled=true  />";
                        tab += "<td style='width:8%;text-align:left' >"+ txtSupervisorAvgTraits +"</td>";
                       
                      }
                      else
                      {
                   
                            var txtSupervisorAvgTraits = "<input id='txtSupervisorAvgTraits' name='txtSupervisorAvgTraits' type='Text'  value='0' style='width:99%;' class='NormalText' maxlength='10' disabled=true  />";
                            tab += "<td style='width:8%;text-align:left' >"+ txtSupervisorAvgTraits +"</td>";
                       
                      }
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>"; 
                         

                tab += "</table></div></div></div>"; 
                document.getElementById("<%= pnTraits.ClientID %>").innerHTML = tab;
               
                
                }
                else
                document.getElementById("<%= pnTraits.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
              
               
        }

        function updateValueTraits(n)
        {  
       
             row3 = document.getElementById("<%= hdnTraits.ClientID %>").value.split("¥");
          
             var newStrTraits=""
             var SelfScoreTraits=""
              for (n = 1; n <= row3.length - 1; n++) 
                {
                        var col = row3[n].split("µ");
                   
                       
                           
                        var SelfScoreTraits = document.getElementById("txtSelfScoreTraits"+ n.toString()).value;
            
                       
                        newStrTraits+="¥" +col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3]  + "µ" + col[4] + "µ" + col[5] + "µ" + SelfScoreTraits ;  
                      
               
                }

                
            document.getElementById("<%= hdnTraits.ClientID %>").value=newStrTraits;
         
        }

       function table_AppraiseeCommentsfill(Arg) 
       {
            var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if(StatusID==0)
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
              var Row=Arg.split("Ø");
            

            document.getElementById("<%= hdnAppraiseeCommitments.ClientID %>").value=Row[8];
        
           if (document.getElementById("<%= hdnAppraiseeCommitments.ClientID %>").value != "") 
           {
            document.getElementById("<%= pnAppraiseeCommitments.ClientID %>").style.display = '';
          
            var tab = "";
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            
            row = document.getElementById("<%= hdnAppraiseeCommitments.ClientID %>").value.split("¥");
           
           
          
                 col = row[1].split("µ");
                  tab += "<tr height=20px;  class='tblQal'>";
               
                var AppraiseeCommitments=""
                  if(col[1]!="")
                  {

                         AppraiseeCommitments   = col[1];
                         var txtAppraiseeCommitments ="<textarea class='NormalText' id='txtAppraiseeCommitments' name='txtAppraiseeCommitments'  maxlength='200' "+ DisFlag + " style='width:100%;text-align:left;' rows=2;>"+AppraiseeCommitments+"</textarea>";
                         tab += "<td style='width:16%;text-align:left' >"+ txtAppraiseeCommitments +"</td>";
                    
                       
                  }
                  else
                  {
                       var txtAppraiseeCommitments ="<textarea class='NormalText' id='txtAppraiseeCommitments' name='txtAppraiseeCommitments'  maxlength='200' "+ DisFlag + " style='width:100%;text-align:left;' rows=2;></textarea>";
                       tab += "<td style='width:16%;text-align:left' >"+ txtAppraiseeCommitments +"</td>";
                  
                  }
                    
                  tab += "</tr>";

            tab += "</table></div>";
            document.getElementById("<%= pnAppraiseeCommitments.ClientID %>").innerHTML = tab;
             }
                else
                document.getElementById("<%= pnAppraiseeCommitments.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
              
               
            


        }
       
       
          function CalculateTotal()
       {
               
                var Data=document.getElementById("<%= hdnDutiesDtl.ClientID %>").value;
             
                var SubTotal=0;
                var SelfTotal=0;
                var row = Data.split("¥");
                for (n = 1; n <= row.length-1 ; n++) 
                {
                    var col = row[n].split("µ");
                 
                
           
                  SubTotal=Math.abs(SubTotal)+(Math.abs(col[1]));
              
                  SelfTotal=(Math.abs(SelfTotal)+((Math.abs(col[1])/100)*Math.abs(col[4]))).toFixed(2);
             
                     
                }
                document.getElementById("txtAvgDuties").value=SelfTotal;
                document.getElementById("txtPerentage").value=SubTotal;
                if(SubTotal!=100) 
                {
                    alert("Total Weightage should be equal to 100");
                    document.getElementById("txtPerentage").focus();
                    return false;
                  
                }
              
                 
       }

   function Calculate()
       {
               
                var Data=document.getElementById("<%= hdnDutiesDtl.ClientID %>").value;
             
                var SubTotal=0;
                var SelfTotal=0;
                var row = Data.split("¥");
                for (n = 1; n <= row.length-1 ; n++) 
                {
                    var col = row[n].split("µ");
                 
                
           
                  SubTotal=Math.abs(SubTotal)+(Math.abs(col[1]));
              
                  SelfTotal=(Math.abs(SelfTotal)+((Math.abs(col[1])/100)*Math.abs(col[4]))).toFixed(2);
             
                     
                }
                document.getElementById("txtAvgDuties").value=SelfTotal;
                document.getElementById("txtPerentage").value=SubTotal;
                
              
                 
       }
         function CalculateScoreBeli()
       {

             
                var SelfScore = 0;
                var SelfScoree=0;
               
                
                 var count=0;
                  for (n = 1; n <= 13 ; n++) 
                    {
                       SelfScore += Math.abs(document.getElementById("txtSelfScoreBeli"+n).value); 
                       
                      if(Math.abs(document.getElementById("txtSelfScoreBeli"+n).value)!="")
                      {

                        var count=count+1;
                        
                    
                      }
                    }
                  
                SelfScoree=Math.abs(SelfScore/Math.abs(count)).toFixed(2);
             
                document.getElementById("txtAvgBeli").value = SelfScoree;
               

         }       
       function CalculateScoreTraits()
       {
                var SelfScoreTrait=0;
                var SelfScoreTraits=0;
                        for (n = 1; n <= 10 ; n++) 
                            {
                               SelfScoreTraits += Math.abs(document.getElementById("txtSelfScoreTraits"+n).value); 
                               
                         
                            }

                        SelfScoreTrait=SelfScoreTraits/10;
              
                        document.getElementById("txtAvgTraits").value = SelfScoreTrait;
                           
               
       }
       





        function AddNewRow() {          
                      
                if (document.getElementById("<%= hdnDutiesDtl.ClientID %>").value != "") {      
                      
                    row = document.getElementById("<%= hdnDutiesDtl.ClientID %>").value.split("¥");
                   
                    var cnt=0;
                   
                    ///var Len = row.length - 1; 
                       for (n = 1; n <= row.length-1 ; n++) {     
                    
                             col = row[n].split("µ"); 
                              cnt += Math.abs(col[1]);
                        } 
                        if(cnt>=100)
                        {
                          alert("Your weightage is already 100.you cant add new row.If You want to add new row please edit weightage value");
                          return false;

                        }
                        else{
                         
                    
                            if (col[0] != "" && col[1] != ""  && col[4] != "")
                             {
                              
                                 document.getElementById("<%= hdnDutiesDtl.ClientID %>").value += "¥µµµµµ";      
                            
                       
                            }
                            else
                            {
                              alert("Please fill (Duties-Responsiblities-Targets),weightage and performance score column. Then only you can add new row");
                            }

                        }
                    }
                   
                table_fill();
            }
        function setFocus() {
      
            document.getElementById("txtDuities"+n).Focus();
        }
       function DeleteRow(id)
        {
           
                row = document.getElementById("<%= hdnDutiesDtl.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                    if(id!=n)                  
                        NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnDutiesDtl.ClientID %>").value=NewStr;
               
                if(document.getElementById("<%= hdnDutiesDtl.ClientID %>").value=="")
                document.getElementById("<%= hdnDutiesDtl.ClientID %>").value= "¥µµµµµ";
                table_fill();
        }
        
       function CreateNewRow(e, val) {
      
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValue(val);
                AddNewRow();                          
            }
         }

        

        function updateValue(id)
        {      
     
            row = document.getElementById("<%= hdnDutiesDtl.ClientID %>").value.split("¥");
           
             row1 = document.getElementById("<%= hdnDutiesDtl1.ClientID %>").value.split("¥");
           
                var NewStr=""
                var NewStr1=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if((id==n) ||(id==-1))
                   {
                        var Duities=document.getElementById("txtDuities"+n).value;                  
                        var Weightage=document.getElementById("txtWeightage"+n).value; 
                                  
                        var KeyPerformance=document.getElementById("txtKeyPerformance"+n).value;                    
                        var Status=document.getElementById("txtStatus"+n).value;                                          
                        var SelfScore=document.getElementById("txtSelfScore"+n).value;                           
                        var SupervisorDutiesScore=document.getElementById("txtSupervisorDutiesScore"+n).value;
                       
                        NewStr+="¥"+Duities+"µ"+Weightage+"µ"+KeyPerformance+"µ"+Status+"µ"+SelfScore+"µ"+SupervisorDutiesScore;
                          
                                                   
                   }
                   else
                   {
                        NewStr+="¥"+row[n];
                   }
                }
                   
                 

                document.getElementById("<%= hdnDutiesDtl.ClientID %>").value=NewStr;
               
              
        }


         function btnExit_onclick() {
             window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
         }
          function btnFinish_onclick() {

            finish_save_flag=1;
           
              
         
                var r = confirm("Do you want to finish? After pressing finish,You can't edit.");
               
                if (r) {
                     btnSave_onclick();
                     
                }
                else 
                {
                    finish_save_flag=0;
                  
                     return false;
                 ////  window.open('Appraisal.aspx', "_self");
                }    
            

         }
</script>
    <script language="javascript" type="text/javascript">
     
    </script>
    <br />
    <table align="center" 
        style="width:80%; margin: 0px auto;" >
           <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>EMPLOYEE DETAILS</strong></td>
           
        </tr>
        <tr>
            <td class="style2" style="width:12% ;">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            </td>
            <td class="style1" style="width:15%;">
                &nbsp;</td>
            <td class="style3">
                &nbsp;</td>
            <td style="width:15% ;">
                &nbsp;</td>
        </tr>
      
        <tr>
            <td style="text-align:left;width:12% ;" class="style2">
                Employee Code&nbsp;&nbsp; <b style="color:red;">*</b></td>
            <td class="style1" style="width:15% ;">
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="ReadOnlyTextBox"  MaxLength="6" onkeypress="return NumericCheck(event)"></asp:TextBox>
            </td>
            <td style="text-align:left;" class="style3">
                Name&nbsp;&nbsp;<b style="color:red;">*</b></td>
            <td style="width:15% ;">
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpName" runat="server" Width="90%" 
                    class="ReadOnlyTextBox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align:left; width:12% ;" class="style2">
                Date Of Join&nbsp;&nbsp;<b style="color:red;">*</b></td>
            <td class="style1" style="width:15% ;">
                &nbsp;&nbsp;<asp:TextBox 
                    ID="txtDoj" runat="server" Width="30%" 
                    class="ReadOnlyTextBox"></asp:TextBox>
                
            </td>
            <td style="text-align:left;" class="style3">
                Designation during the time of joining &nbsp;<b style="color:red;">*</b></td>
            <td style="width:15% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDesignationDoj" 
                    runat="server"  Width="91%" class="NormalText">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:12% ;" class="style2" >
                Current Designation&nbsp;<b style="color:red;">*</b></td>
            <td class="style1" style="width:15% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDesignation" runat="server" 
                    class="ReadOnlyTextBox" Enabled="false" Width="50%">
                </asp:DropDownList>
            </td>
            <td style="text-align:left;" class="style3">
                Designation during the last review &nbsp;<b style="color:red;">*</b></td>
            <td style="width:15% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDesignationreview" runat="server" 
                    class="NormalText" Width="91%">
                  <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
           <tr>
            <td style="text-align:left;width:12% ;" class="style2">
                Department&nbsp;<b style="color:red;">*</b></td>
            <td class="style1" style="width:12% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDepartment" runat="server" 
                    class="NormalText" Width="50%">
                </asp:DropDownList>
            </td>
            <td style="text-align:left;" class="style3">
                Location/Branch&nbsp;<b style="color:red;">*</b></td>
            <td style="width:15% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbLocationBranch" runat="server" 
                    class="NormalText" Width="91%">
                  <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
           </tr>
        <tr>
            <td style="text-align:left;" class="style2">
                Reporting To&nbsp;&nbsp;<b style="color:red;">*</b></td>
            <td class="style1">
                &nbsp;&nbsp;<asp:DropDownList 
                    ID="cmbReportingTo" runat="server" class="ReadOnlyTextBox" Enabled="false" Width="50%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="text-align:left;" class="style3">
                Reported By &nbsp; (No of Team members)
                &nbsp;<b style="color:red;">*</b>
               
                </td>
            <td style="width:32% ; text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtReportedBy" runat="server" Width="50%" 
                    class="NormalText" ReadOnly="true" ></asp:TextBox>
              </td>
        </tr>
          <tr>
            <td style="text-align:left;" class="style2">
                Reviewing Head&nbsp;&nbsp;<b style="color:red;">*</b></td>
            <td class="style1">
                &nbsp;&nbsp;<asp:DropDownList 
                    ID="cmbReviewingHead" runat="server" class="ReadOnlyTextBox" Enabled="false" Width="50%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
              </td>
            <td style="text-align:left;" class="style3">
                Functional Head&nbsp;&nbsp;<b style="color:red;">*</b></td>
            <td style="width:32% ; text-align:left;">&nbsp;&nbsp;<asp:DropDownList 
                    ID="cmbFunctionalHead" runat="server" class="ReadOnlyTextBox" Enabled="false"  Width="50%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
              </td>
        </tr>
        
        <tr>
            <td style="text-align:left;" class="style2">
                Period of Evaluation&nbsp;&nbsp;<b style="color:red;">*</b></td>
            <td class="style1">&nbsp;&nbsp;<asp:TextBox 
                    ID="txtPeriod" runat="server" Width="90%" 
                    class="ReadOnlyTextBox" ></asp:TextBox>
              </td>

            <td style="text-align:left;" class="style3">
                Date of Evaluation&nbsp;&nbsp;<b style="color:red;">*</b></td>
           <td style="width:32% ;text-align:left;">&nbsp;&nbsp;<asp:TextBox ID="txtEvaluation" 
                    runat="server" class="NormalText" Width="30%" ReadOnly="true"></asp:TextBox> 
                <asp:CalendarExtender ID="txtEvaluation_CalendarExtender" runat="server" 
                    TargetControlID="txtEvaluation" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;" class="style2">
                Remarks&nbsp;&nbsp;</td>
            <td class="style1">&nbsp;&nbsp;<asp:TextBox ID="txtRemarks" class="NormalText" 
                    runat="server" Width="91%"
                  onkeypress="return this.value.length < 499" onpaste="return check(this.value.length)" MaxLength="40" TextMode="MultiLine"></asp:TextBox>
              </td>
        </tr>
        
          
      
          <tr>
            <td style="text-align:left;" class="style2">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td style="text-align:left;" class="style3">
                &nbsp;</td>
            <td style="width:32% ; text-align:left;">
                &nbsp;</td>
        </tr>
         <tr>
            <td style="width:12% ;text-align:center;" colspan="3" class="mainhead" >
                <strong>A. DUTIES & RESPONSIBILITIES - PERFORMANCE INDICATORS</strong><b style="color:red;"> * </b></td>
                <td style="width:12% ;text-align:right;"  class="mainhead" >
                <strong><font size="1">Add New Row</font></strong>
                <img src="../../Image/Addd1.gif" style="height:18px; width:18px; float:center; z-index:1; cursor: pointer;  padding-right:10px;" onclick="AddNewRow()" title="Add New" /> 
                &nbsp;&nbsp;</td>
           
        </tr> 
         <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnDuties" runat="server">
                </asp:Panel>
               </td>
              
        </tr>
        <tr>
        <td style="width:12% ;text-align:left;" colspan="4"  >
                <strong>Notes : </strong>
                Summation of weightage shall be 100%,&nbsp;&nbsp;&nbsp;&nbsp;
                Rating Scale : 1 to 10 where 10 being Excellent
               
             </td>
        
        
        
        </tr>
        
         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>B. ESAF - BELIEFS </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnBeliefsDtl" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
          <tr>
            <td style="width:12% ;text-align:left;" colspan="4" class="mainhead" >
                <strong>List out your three significant contributions to the company (Explain Objectively) </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnSignificant" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
          <tr>
            <td style="width:12% ;text-align:left;" colspan="4" class="mainhead" >
                <strong>State the areas of improvement for self , so as to contribute better</strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnImprovementArea" runat="server">
                </asp:Panel>
               </td>
              

          </tr>
          <tr>
            <td style="width:12% ;text-align:left;" colspan="4" class="mainhead" >
                <strong>Individual Development Plan - so as to improve your skills/competency</strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnDevelopmentPlan" runat="server">
                </asp:Panel>
               </td>
         </tr>
         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>C. TRAITS AND BEHAVIOURS </strong> 
                &nbsp;&nbsp;<b style="color:red;">*</b></td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnTraits" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>D. COMMITMENTS / REMARKS BY THE APPRAISEE </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnAppraiseeCommitments" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
       
           <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <input id="btnSave" type="button" value="SAVE" 
                    onclick="return btnSave_onclick()" 
                    style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;<input id="btnExit" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />
                     <input id="btnFinish" type="button" value="FINISH" 
                    onclick="return btnFinish_onclick()" 
                    style="font-family: cambria; width: 5%; cursor: pointer;" /><asp:HiddenField 
                    ID="hdnDuties" runat="server" />
                <asp:HiddenField 
                    ID="hdnBeliefsDtl" runat="server" />
                    <asp:HiddenField 
                    ID="hdnBeliefsDtl1" runat="server" />
                <asp:HiddenField 
                    ID="hdnSignificant" runat="server" />
               
                <asp:HiddenField 
                    ID="hdnImprovementArea" runat="server" />
                <asp:HiddenField 
                    ID="hdnDevelopmentPlan" runat="server" />
                <asp:HiddenField 
                    ID="hdnTraits" runat="server" />
                     <asp:HiddenField 
                    ID="hdnTraits1" runat="server" />
                <asp:HiddenField 
                    ID="hdnAppraiseeCommitments" runat="server" />
                <asp:HiddenField 
                    ID="hdnExperience" runat="server" />
                <asp:HiddenField 
                    ID="hdnDutiesDtl" runat="server" />
            <asp:HiddenField 
                    ID="hdnDutiesDtl1" runat="server" />
                 <asp:HiddenField ID="hdnReportingTo" runat="server" />
                 <asp:HiddenField ID="hdnDepartment" runat="server" />
                 <asp:HiddenField ID="hdnDesignation" runat="server" />
                  <asp:HiddenField ID="hdnPeriod" runat="server" />
                  <asp:HiddenField ID="hdnPercentage" runat="server" />
                  <asp:HiddenField ID="hdnstatus" runat="server" />
               </td>
        </tr>
    </table></br>
</asp:Content>

