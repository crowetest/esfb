﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ExcelExport.aspx.vb" Inherits="ExcelExport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
<div>
    <table style="width: 50%; margin:0px auto; text-align:center;">
        <tr>
            <td style="width: 40%"
                &nbsp;</td>
            <td  style="width: 60%">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            </td>
        </tr>
        <tr>
            <td  style="width: 40%;text-align:right ;">
                Report&nbsp;&nbsp;</td>
            <td  style="width: 60%">
                <asp:DropDownList ID="cmbName" runat="server" Height="25px" Width="60%" class="NormalText">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                    <asp:ListItem Value="1">Overview Report</asp:ListItem>
                    <asp:ListItem Value="2">Detailed Report</asp:ListItem>
                    <asp:ListItem Value="3">Appraisal Not Attended Report</asp:ListItem>
                    <asp:ListItem Value="4">Consolidated Report</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
         <tr>
            <td  style="width: 40%;text-align:right ;">
                Period&nbsp;&nbsp;</td>
            <td  style="width: 60%">
                <asp:DropDownList ID="cmbPeriod" runat="server" Height="25px" Width="60%" class="NormalText">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td  style="width: 40%">
                &nbsp;</td>
            <td  style="width: 60%">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center;">
                <asp:HiddenField ID="hdnFromDt" runat="server" />
                <asp:HiddenField ID="hdnToDt" runat="server" />
                <asp:Button ID="btnExport" runat="server" Text="EXPORT" Font-Names="Cambria" 
                    Font-Size="10pt" Width="10%" />
&nbsp;
                <input id="btn_Exit" type="button" value="EXIT" 
                    style="font-family: Cambria; font-size: 10pt; width: 10%" onclick="return btn_Exit_onclick()" /></td>
        </tr>
    </table>
    </div> 
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btn_Exit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function NameOnChange()
        {
        
        }
        function ExportOnClick()
        {
          var Val=document.getElementById("<%= cmbName.ClientID %>").value;
         
        
        }

// ]]>
    </script>
</asp:Content>

