﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AppraisalReport_RO_RV_FH.aspx.vb" Inherits="AppraisalReport_RO_RV_FH" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br />
 <table align="center" style="width:40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                  <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager></td>
        </tr>
       <tr>
            <td style="width:30%;">
                Level</td>
            <td  style="width:70%;">
                <asp:DropDownList ID="cmbLevel" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ----------Select---------- </asp:ListItem>
                    <asp:ListItem Value="1">Reporting Officer</asp:ListItem>
                    <asp:ListItem Value="2">Reviewing Head / HOD </asp:ListItem>
                    <asp:ListItem Value="3">Functional Head / EVP</asp:ListItem>   
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:30%;">
                Employees to Review</td>
            <td  style="width:70%;">
                <asp:DropDownList ID="cmbEmp" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">----------Select----------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
         <tr>
            <td style="width:30%;">
                Period</td>
            <td  style="width:70%;">
                <asp:DropDownList ID="cmbPeriod" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">----------Select----------</asp:ListItem>
                   
                </asp:DropDownList></td>
        </tr>
         <tr>
            <td style="width:15%;">
               </td>
            <td  style="width:85%;"></td>
        </tr>
       
         <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">&nbsp;</td>
        </tr>
       
           <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()"  />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnStatus" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnPostID" runat="server" />
                 </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                 <asp:HiddenField ID="hdnLocationID" runat="server" />
            </td>
        </tr></table>
    <script language="javascript" type="text/javascript">
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function EmployeeOnChange() 
        {
              var Emp_ID = document.getElementById("<%= cmbEmp.ClientID %>").value;
              var ToData = "2Ø" + Emp_ID  ;
               ToServer(ToData, 2);

        }
        function LevelOnChange() 
        {
              var Level_ID = document.getElementById("<%= cmbLevel.ClientID %>").value;
              var ToData = "4Ø" + Level_ID  ;
               ToServer(ToData, 4);

        }
        function PeriodOnChange() 
        {
              var Period_ID = document.getElementById("<%= cmbPeriod.ClientID %>").value;
              var ToData = "3Ø" + Period_ID ;
               ToServer(ToData, 3);

        }
         function FromServer(Arg, Context) {
        
            switch (Context) {

                case 1:
                    {   
                        ComboFill(Arg, "<%= cmbEmp.ClientID %>");
                        break;
                    }  
                    case 2:
                    {   
                      
                        document.getElementById("<%= hdnStatus.ClientID %>").value=Arg[1];
                        break;
                    }
                    case 3:
                    {   
                      
                       ComboFill(Arg, "<%= cmbEmp.ClientID %>");
                        break;
                    } 
                    case 4:
                    {   
                      
                       ComboFill(Arg, "<%= cmbEmp.ClientID %>");
                        break;
                    }  
                     
            }
        }
      
        function btnView_onclick() 
        {
            var Emp_ID=document.getElementById("<%= cmbEmp.ClientID %>").value;
           
            var Period_ID=document.getElementById("<%= cmbPeriod.ClientID %>").value;
            var Role_ID = document.getElementById("<%= cmbLevel.ClientID %>").value;
            if (Emp_ID == -1) {
                  alert("Select Employee to Review");
                  return false;
            }
            if (Period_ID == -1) {
                  alert("Select Period");
                  return false;
            }
            else
            {
                if (Role_ID == 1)
                {
                    window.open("AppraisalReportRepOfficerView.aspx?Emp_ID="+ btoa(Emp_ID) +" &Period_ID="+ btoa(Period_ID),"_self");
                }
                if (Role_ID == 2)
                {
                    window.open("AppraisalReportRevOfficerView.aspx?Emp_ID="+ btoa(Emp_ID) +" &Period_ID="+ btoa(Period_ID),"_self");
                }
                if (Role_ID == 3)
                {
                    window.open("AppraisalReportFunOfficerView.aspx?Emp_ID="+ btoa(Emp_ID) +" &Period_ID="+ btoa(Period_ID),"_self");
                }

            }
        }

        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = "ALL";
            document.getElementById(control).add(option1);
        }
    </script>
</asp:Content>

