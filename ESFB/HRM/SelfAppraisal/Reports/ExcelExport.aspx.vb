﻿Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ExcelExport
    Inherits System.Web.UI.Page
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim Form_id As Integer
    Dim WebTools As New WebApp.Tools
    Dim AuditTypeID As Integer
    Dim ModuleID As Integer = 0
    Dim DB As New MS_SQL.Connect
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            Dim HeaderText As String
            Dim Report As Integer = CInt(Me.cmbName.SelectedValue)
            Dim Period As Integer = CInt(Me.cmbPeriod.SelectedValue)
            'If Report = 1 Then
            '    Dim sqlStr = "SELECT ROW_NUMBER() OVER(ORDER BY WEIGHTAGE_ID) AS [Sl No],a.Emp_code as [Employee code],b.Emp_name as [Name],d.Department_name as [Department],total_score as [Weighted Score] FROM Appraisal_Weightage_Score a inner join emp_master b on a.emp_code=b.emp_code inner join Appraisal_Employee_Master c on b.emp_code=c.emp_code inner join department_master d on d.department_id=c.dept_id where a.period_id= " & Period & " "
            '    DT = GF.GetQueryResult(sqlStr)

            'Else
            Dim Params(3) As SqlParameter
            Params(0) = New SqlParameter("@Period", SqlDbType.Int)
            Params(0).Value = Period
            Params(1) = New SqlParameter("@Report", SqlDbType.Int)
            Params(1).Value = Report
            Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(2).Direction = ParameterDirection.Output
            Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(3).Direction = ParameterDirection.Output
            DT = DB.ExecuteDataSet("SP_Appraisal_Report", Params).Tables(0)
            Dim ErrorFlag As Integer = CInt(Params(2).Value)
            Dim Message As String = CStr(Params(3).Value)

            'Dim sqlStr = "SELECT ROW_NUMBER() OVER(ORDER BY WEIGHTAGE_ID) AS [Sl No],a.Emp_code as [Employee code],b.Emp_name as [Name],d.Department_name as [Department],total_score as [Weighted Score] FROM Appraisal_Weightage_Score a inner join emp_master b on a.emp_code=b.emp_code inner join Appraisal_Employee_Master c on b.emp_code=c.emp_code inner join department_master d on d.department_id=c.dept_id"
            'DT = GF.GetQueryResult(sqlStr)
            'End If
            HeaderText = cmbName.SelectedItem.Text
            WebTools.ExporttoExcel(DT, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub ExcelExport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ModuleID = CInt(Request.QueryString.Get("ModuleID"))
            Me.cmbName.Attributes.Add("onchange", "NameOnChange()")
            Me.btnExport.Attributes.Add("onclick", "return ExportOnClick()")

            If Not IsPostBack Then
                DT = DB.ExecuteDataSet("select -1 as period_id,'----Select----' as Period union all SELECT period_id, Period FROM Appraisal_Period_Master ORDER BY period_id ").Tables(0)
                GF.ComboFill(cmbPeriod, DT, 0, 1)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
