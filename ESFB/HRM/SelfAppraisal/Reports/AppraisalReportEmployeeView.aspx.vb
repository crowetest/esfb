﻿Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.Drawing.Printing
Imports System.Drawing
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Diagnostics
Partial Class HRM_SelfAppraisal_Reports_Employee_Report_Viewer
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim pageIndex As Integer = 0
    Dim streams As IList(Of Stream)
    Dim GF As New GeneralFunctions
    Dim ProcessId As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ReportViewer1.ProcessingMode = ProcessingMode.Local
                'ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report.rdlc")
                Dim Period_ID As Integer
                Period_ID = CInt(GF.Decrypt(Request.QueryString.Get("Period_ID")))
                Dim Emp_Code As Integer
                Emp_Code = CInt(GF.Decrypt(Request.QueryString.Get("Emp_ID")))
                Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Empcode", Emp_Code),
                                                                          New System.Data.SqlClient.SqlParameter("@PeriodId", Period_ID)}
                'Dim Params(1) As SqlParameter
                'Params(0) = New SqlParameter("@Empcode", SqlDbType.Int)
                ' Params(0).Value = CInt(Session("UserID"))
                Dim ds As New DataTable()
                ds = DB.ExecuteDataSet("GetEmployeeAppraisalReport", Parameters).Tables(0)
                Dim rptds As ReportDataSource
                rptds = New ReportDataSource("ESFBDataSet", ds)
                rptds.Name = "DataSet1"

                Dim Parameters1 As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Empcode", Emp_Code)}
                Dim ds1 As New DataTable()
                ds1 = DB.ExecuteDataSet("GetDutiesAndResponsibilitiesMasterData", Parameters1).Tables(0)
                Dim rptds1 As ReportDataSource
                rptds1 = New ReportDataSource("ESFBDataSet", ds1)
                rptds1.Name = "DataSet2"

                Dim Parameters2 As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Empcode", Emp_Code),
                                                                           New System.Data.SqlClient.SqlParameter("@PeriodId", Period_ID)}
                Dim ds2 As New DataTable()
                ds2 = DB.ExecuteDataSet("GetDutiesAndResponsibilities", Parameters2).Tables(0)
                Dim rptds2 As ReportDataSource
                rptds2 = New ReportDataSource("ESFBDataSet", ds2)
                rptds2.Name = "DataSet3"

                Dim Parameters3 As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Empcode", Emp_Code),
                                                                           New System.Data.SqlClient.SqlParameter("@PeriodId", Period_ID)}
                Dim ds3 As New DataTable()
                ds3 = DB.ExecuteDataSet("GetSignificantData", Parameters3).Tables(0)
                Dim rptds3 As ReportDataSource
                rptds3 = New ReportDataSource("ESFBDataSet", ds3)
                rptds3.Name = "DataSet4"

                Dim Parameters4 As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Empcode", Emp_Code),
                                                                           New System.Data.SqlClient.SqlParameter("@PeriodId", Period_ID)}
                Dim ds4 As New DataTable()
                ds4 = DB.ExecuteDataSet("GetTraistsAndBehaviour", Parameters4).Tables(0)
                Dim rptds4 As ReportDataSource
                rptds4 = New ReportDataSource("ESFBDataSet", ds4)
                rptds4.Name = "DataSet5"

                Dim Parameters5 As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Empcode", Emp_Code),
                                                                           New System.Data.SqlClient.SqlParameter("@PeriodId", Period_ID)}
                Dim ds5 As New DataTable()
                ds5 = DB.ExecuteDataSet("GetReportingOfficer_Comment", Parameters5).Tables(0)
                Dim rptds5 As ReportDataSource
                rptds5 = New ReportDataSource("ESFBDataSet", ds5)
                rptds5.Name = "DataSet6"

                Dim Parameters6 As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Empcode", Emp_Code),
                                                                           New System.Data.SqlClient.SqlParameter("@PeriodId", Period_ID)}
                Dim ds6 As New DataTable()
                ds6 = DB.ExecuteDataSet("GetDisciplinaryAction", Parameters6).Tables(0)
                Dim rptds6 As ReportDataSource
                rptds6 = New ReportDataSource("ESFBDataSet", ds6)
                rptds6.Name = "DataSet7"
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/HRM/SelfAppraisal/Reports/Appraisal_Employee.rdlc")
                ReportViewer1.LocalReport.DataSources.Clear()
                ReportViewer1.LocalReport.DataSources.Add(rptds)
                ReportViewer1.LocalReport.DataSources.Add(rptds1)
                ReportViewer1.LocalReport.DataSources.Add(rptds2)
                ReportViewer1.LocalReport.DataSources.Add(rptds3)
                ReportViewer1.LocalReport.DataSources.Add(rptds4)
                ReportViewer1.LocalReport.DataSources.Add(rptds5)
                ReportViewer1.LocalReport.DataSources.Add(rptds6)
                ReportViewer1.LocalReport.Refresh()
                ReportViewer1.Visible = False
                Show_PDF()
                Response.Redirect("AppraisalReport_Employee.aspx", False)
            End If
        Catch ex As Exception
            
        End Try
    End Sub
    Private Sub Show_PDF()
        Try

            Dim deviceInfo As String = "<DeviceInfo>" + _
                                      "  <OutputFormat>EMF</OutputFormat>" + _
                                      "  <PageWidth>10.2in</PageWidth>" + _
                                      "  <PageHeight>11in</PageHeight>" + _
                                      "  <MarginTop>0.25in</MarginTop>" + _
                                      "  <MarginLeft>0.25in</MarginLeft>" + _
                                      "  <MarginRight>0.25in</MarginRight>" + _
                                      "  <MarginBottom>0.25in</MarginBottom>" + _
                                      "</DeviceInfo>"
            Dim warnings As Warning()
            Dim streamids As String()
            Dim mimeType As String = "application/Report.pdf"
            Dim encoding As String = String.Empty
            Dim filenameExtension As String = String.Empty
            Dim pdfContent As Byte() = ReportViewer1.LocalReport.Render("PDF", deviceInfo, mimeType, encoding, filenameExtension, streamids, warnings)
            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "Report.pdf")
            Response.BinaryWrite(pdfContent)
            Response.Flush()
        Catch ex As Exception
            MsgBox("Exception", MsgBoxStyle.Information, "")
        End Try
    End Sub
    Private Sub Printdoc()
        'ReportViewer1.print()
        Export(ReportViewer1.LocalReport)
        'pageIndex = 0
        'Print()
        Dim deviceInfo As String = "<DeviceInfo>" + _
                                  "  <OutputFormat>EMF</OutputFormat>" + _
                                  "  <PageWidth>8.5in</PageWidth>" + _
                                  "  <PageHeight>11in</PageHeight>" + _
                                  "  <MarginTop>0.25in</MarginTop>" + _
                                  "  <MarginLeft>0.25in</MarginLeft>" + _
                                  "  <MarginRight>0.25in</MarginRight>" + _
                                  "  <MarginBottom>0.25in</MarginBottom>" + _
                                  "</DeviceInfo>"
        Dim pdfContent As Byte() = ReportViewer1.LocalReport.Render("Image", deviceInfo, Nothing, Nothing, Nothing, Nothing, Nothing)

        'Creatr PDF file on disk
        Dim pdfPath As String = "C:\temp\report.pdf"
        Dim pdfFile As New System.IO.FileStream(pdfPath, System.IO.FileMode.Create)
        pdfFile.Write(pdfContent, 0, pdfContent.Length)
        pdfFile.Close()
        Process.Start(pdfPath)
    End Sub

    Private Sub Export(ByVal report As LocalReport)
        Dim deviceInfo As String = "<DeviceInfo>" + _
                                  "  <OutputFormat>PDF</OutputFormat>" + _
                                  "  <PageWidth>8.5in</PageWidth>" + _
                                  "  <PageHeight>11in</PageHeight>" + _
                                  "  <MarginTop>0.25in</MarginTop>" + _
                                  "  <MarginLeft>0.25in</MarginLeft>" + _
                                  "  <MarginRight>0.25in</MarginRight>" + _
                                  "  <MarginBottom>0.25in</MarginBottom>" + _
                                  "</DeviceInfo>"
        Dim warnings() As Warning
        streams = New List(Of Stream)
        report.Render("Image", deviceInfo, AddressOf CreateStream, warnings)
        For Each stream As Stream In streams
            stream.Position = 0
        Next
       
    End Sub

    Private Function CreateStream(ByVal name As String, ByVal fileNameExtension As String, ByVal encoding As Encoding, ByVal mimeType As String, ByVal willSeek As Boolean) As Stream
        Dim stream As Stream = New FileStream((Server.MapPath("~/Files/") _
                        + (name + ("." + fileNameExtension))), FileMode.OpenOrCreate)
        streams.Add(stream)
        Return stream
    End Function

    Private Sub PrintPage(ByVal sender As Object, ByVal ev As PrintPageEventArgs)
        Dim pageImage As Metafile = New Metafile(streams(pageIndex))
        ev.Graphics.DrawImage(pageImage, ev.PageBounds)
        pageIndex = (pageIndex + 1)
        ev.HasMorePages = (pageIndex < streams.Count)
    End Sub

    Private Overloads Sub Print()
        If ((streams Is Nothing) _
                    OrElse (streams.Count = 0)) Then
            Return
        End If

        Dim printDoc As PrintDocument = New PrintDocument
        AddHandler printDoc.PrintPage, AddressOf Me.PrintPage
        printDoc.Print()
    End Sub
End Class
