﻿<%@ Page Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="AppraisalReportHRView.aspx.vb" Inherits="HRM_SelfAppraisal_Reports_Employee_Report_Viewer" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ MasterType VirtualPath="~/REPORT.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="height: 571px; width: 766px">
    <form id="form1" runat="server">
    <div style="width: 736px; height: 539px">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <p style="margin-left: 520px">
            &nbsp;&nbsp
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
            WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Height="448px" 
            Width="726px" style="margin-right: 0px" SizeToReportContent="True"  
            ShowPrintButton="False" ShowBackButton="False" ShowCredentialPrompts="False" 
            ShowDocumentMapButton="False" ShowExportControls="False" 
            ShowFindControls="False" ShowPageNavigationControls="False" 
            ShowParameterPrompts="False" ShowPromptAreaButton="False" 
            ShowRefreshButton="False" ShowToolBar="False" 
            ShowWaitControlCancelLink="False" ShowZoomControl="False" 
            AsyncRendering="False">
        <LocalReport ReportPath="HRM\SelfAppraisal\Reports\Appraisal_HR.rdlc"> <DataSources>
        <rsweb:ReportDataSource Name="DataSet1" />
        <rsweb:ReportDataSource Name="DataSet2" />
        <rsweb:ReportDataSource Name="DataSet3" />
        <rsweb:ReportDataSource Name="DataSet4" />
        <rsweb:ReportDataSource Name="DataSet5" />
        <rsweb:ReportDataSource Name="DataSet6" />
        <rsweb:ReportDataSource Name="DataSet7" />
        <rsweb:ReportDataSource Name="DataSet8" />
        </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <script language="javascript" type="text/javascript">
        function btnPrint_onclick() {
            var report = document.getElementById("<%=ReportViewer1.ClientID %>");
            var div = report.getElementsByTagName("DIV");
            var reportContents;
            for (var i = 0; i < div.length; i++) {
                if (div[i].id.indexOf("VisibleReportContent") != -1) {
                    reportContents = div[i].innerHTML;
                    break;
                }
            }
            var frame1 = document.createElement('iframe');
            frame1.name = "frame1";
            frame1.style.position = "absolute";
            frame1.style.top = "-1000000px";
            document.body.appendChild(frame1);
            var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
            frameDoc.document.open();
            frameDoc.document.write('</head><body>');
            frameDoc.document.write(reportContents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                document.body.removeChild(frame1);
            }, 500);
        }
        function btnPrint_onclickb() {
            var divToPrint = document.getElementById("printTable");
            newWin = window.open("");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close();
        }
    </script>
    </div>
    </form>
</body>
</html>
</asp:Content>
