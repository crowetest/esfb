﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Appraisal_Approval.aspx.vb" Inherits="Appraisal_Approval" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br />
 <table align="center" style="width:40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                  <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager></td>
        </tr>
     
         
        <tr>
            <td style="width:30%;">
                Role</td>
            <td  style="width:70%;">
                <asp:DropDownList ID="cmbRole" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">----------Select---------- </asp:ListItem>
                    <asp:ListItem Value="1">Reporting Officer</asp:ListItem>
                    <asp:ListItem Value="2">Reviewing Head / HOD </asp:ListItem>
                    <asp:ListItem Value="3">Functional Head / EVP</asp:ListItem>
                    <asp:ListItem Value="4">HR</asp:ListItem>
                    <asp:ListItem Value="5">MD & CEO / EVP CS </asp:ListItem>
                   
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:30%;">
                Employees to Review</td>
            <td  style="width:70%;">
                <asp:DropDownList ID="cmbEmp" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">----------Select----------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
         <tr>
            <td style="width:30%;">
                Period</td>
            <td  style="width:70%;">
                <asp:DropDownList ID="cmbPeriod" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">----------Select----------</asp:ListItem>
                   
                </asp:DropDownList></td>
        </tr>
         <tr>
            <td style="width:15%;">
               </td>
            <td  style="width:85%;"></td>
        </tr>
       
         <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">&nbsp;</td>
        </tr>
       
           <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()"  />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnStatus" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnPostID" runat="server" />
                 </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                 <asp:HiddenField ID="hdnLocationID" runat="server" />
                 <asp:HiddenField ID="hdnPeriod" runat="server" />
               
            </td>
        </tr></table>
    <script language="javascript" type="text/javascript">
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
       
        function RoleOnChange() 
        {
              var Role_ID = document.getElementById("<%= cmbRole.ClientID %>").value;
              var ToData = "1Ø" + Role_ID  ;
               ToServer(ToData, 1);

        }
        function EmployeeOnChange() 
        {
              var Emp_ID = document.getElementById("<%= cmbEmp.ClientID %>").value;
              var ToData = "2Ø" + Emp_ID  ;
               ToServer(ToData, 2);

        }
        
         function FromServer(Arg, Context) {
        
            switch (Context) {

                case 1:
                    {   
                        ComboFill(Arg, "<%= cmbEmp.ClientID %>");
                        break;
                    }  
                    case 2:
                    {   
                      
                        document.getElementById("<%= hdnStatus.ClientID %>").value=Arg[1];
                        break;
                    }  
                     
            }
        }
      
        function btnView_onclick() 
        {
        var  Role_ID=document.getElementById("<%= cmbRole.ClientID %>").value;
        var Emp_ID=document.getElementById("<%= cmbEmp.ClientID %>").value;
        var Period_ID=document.getElementById("<%= cmbPeriod.ClientID %>").value;
        var Status_ID=document.getElementById("<%= hdnStatus.ClientID %>").value;
      
         if (Role_ID == -1) {

                alert("Select Any Role");            
                return false;

            }
             if (Emp_ID == -1) {
              alert("Select Employee to Review");
              return false;
        }
        if (Period_ID == -1) {
              alert("Select Period");
              return false;
        }
        if((Emp_ID!=-1)&& (Status_ID==0))
        {

            alert("Appraisal of "+Emp_ID+" Not Finished Yet");
        }
        else
        {
               
            window.open("ViewAppraisal.aspx?Role_ID=" + Role_ID + " &Emp_ID=" +  Emp_ID + "&Period_ID=" + Period_ID +  "", "_self");

        }
        }

        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = "ALL";
            document.getElementById(control).add(option1);
        }
    </script>
</asp:Content>

