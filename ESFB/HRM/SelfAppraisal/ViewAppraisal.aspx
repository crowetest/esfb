﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ViewAppraisal.aspx.vb" Inherits="Appraisal"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
		<link rel="stylesheet" href="../../Style/bootstrap-multiselect.css" type="text/css" />
		<script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-multiselect_EMP.js"></script>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
        .style1
        {
            width: 36%;
        }
        .style2
        {
            width: 4%;
        }
    </style>
    <script type="text/javascript">
     var finish_save_flag=0;
        $(document).ready(function () {
            $('#chkveg').multiselect();
            buttonWidth: '500px'
        });
       
       
        function EmpCodeOnChange(EmpID) {
       
            if(document.getElementById("<%= hidassinback.ClientID %>").value == 0)
            {
                document.getElementById("btnUndo").style.display = 'none';
            }

            document.getElementById("<%= txtEmpCode.ClientID %>").value = EmpID;
           
            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
           
            var PeriodID = document.getElementById("<%= hdnPeriod.ClientID %>").value;
           
           
            if (EmpCode != "") {
                var ToData = "3Ø" + EmpCode+"Ø" + PeriodID ;
                ToServer(ToData, 3);
            }

        }

        
         function FromServer(Arg, Context) {
           switch (Context) {

                    case 1:
                    {
                   
                        var Data = Arg.split("Ø");
                      
                        document.getElementById("btnSave").disabled=false;
                        if (Data[0] == 0)
                        { 
                            if(finish_save_flag==0){

                                      alert(Data[1]);
                            }
                            else
                            {
                            
                            var Period_ID = document.getElementById("<%= hdnPeriod.ClientID %>").value;
                            var Role_ID = document.getElementById("<%= hdnRoleID.ClientID %>").value;
                            var Emp_ID = document.getElementById("<%= hdnEmp_ID.ClientID %>").value;
                            var ToData = "2Ø" + Emp_ID+"Ø" + Period_ID +"Ø" + Role_ID;
                             ToServer(ToData, 2);
//                                alert("You have successfully completed the self appraisal");
//                               var Period_ID = document.getElementById("<%= hdnPeriod.ClientID %>").value;
//                               var Role_ID = document.getElementById("<%= hdnRoleID.ClientID %>").value;
//                               var Emp_ID = document.getElementById("<%= hdnEmp_ID.ClientID %>").value;
//                                
//                                window.open("ViewAppraisal.aspx?Role_ID=" + Role_ID + " &Emp_ID=" +  Emp_ID + "&Period_ID=" + Period_ID +  "", "_self");
                            }
                            break;
                        }
                        else
                        {

                            document.getElementById("<%= cmbReportingTo.ClientID %>").value="-1";
                            document.getElementById("<%= txtEmpCode.ClientID %>").value="";
                            document.getElementById("<%= txtEmpName.ClientID %>").value="";
                            document.getElementById("<%= cmbDesignationreview.ClientID %>").value="-1";
                            document.getElementById("<%= cmbLocationBranch.ClientID %>").value="-1";
                            document.getElementById("<%= cmbDepartment.ClientID %>").value="-1";
                            document.getElementById("<%= cmbDesignation.ClientID %>").value="-1";
                            document.getElementById("<%= cmbDesignationDoj.ClientID %>").value="-1";
                            document.getElementById("<%= txtDoj.ClientID %>").value="";
                            document.getElementById("<%= txtEvaluation.ClientID %>").value="";
                            document.getElementById("<%= cmbReportingTo.ClientID %>").value="-1";
                            document.getElementById("<%= txtReportedBy.ClientID %>").value="";
                            document.getElementById("<%= cmbReviewingHead.ClientID %>").value="-1";
                            document.getElementById("<%= cmbFunctionalHead.ClientID %>").value="-1";
                            document.getElementById("<%= txtRemarks.ClientID %>").value="";
                            document.getElementById("<%= txtPeriod.ClientID %>").value="";

                            
                        }

                    }    
                    case 2:
                    {
                         
                if (Arg==1 ){
                      alert("You have successfully completed the Approval Level");
                      window.open('Appraisal_Approval.aspx', "_self");
                      break;
                    } 
                    }      
                    case 3: {   
                   
                        var Row=Arg.split("Ø");

//                        document.getElementById("btnUndo").innerHTML=Row[16];
                        document.getElementById("<%= hdnDutiesDtl.ClientID %>").value=Row[9];
                        document.getElementById("<%= hdnDutiesDtl1.ClientID %>").value=Row[10];
                        document.getElementById("<%= hdnEfforts.ClientID %>").value=Row[12];
                        document.getElementById("<%= hdnHODComments.ClientID %>").value=Row[13];
                        document.getElementById("<%= hdnEVPComments.ClientID %>").value=Row[14];
                        document.getElementById("<%= hdnWeightage.ClientID %>").value=Row[15];
                    
                     
                        if(Arg=="ØØØØ")  
                        {alert("Data is not Available");      } 
                        else{
                            FillData(Arg);
                            table_fillBeliefs(Arg);
                            table_Signifill(Arg);
                            table_ImprovementAreafill(Arg);
                            table_DevelopmentPlanfill(Arg);
                            table_fillTraits(Arg);
                            table_fill() ;
                            table_RO_DisciplinaryActionsfill(Arg)
                            table_AppraiseeCommentsfill(Arg); 
                            table_RO_PerformEffortsfill();
                            table_HOD_Commentsfill();
                            table_EVP_Commentsfill();
                            table_Weightage_Score_fill();
                            table_AppraiseeCommentsfill(Arg); 
                            table_MDRemarksfill(Arg);
                            break;   
                            }
                         }
                   
                    case 4:
                    {
                         var Data = Arg.split("Ø");
                         alert(Data[1]);
                         
               
                    }   

                    }
        }

         function FillData(Arg)
         {
                var Row=Arg.split("Ø");               
                var Dtl=Row[0].split("µ");   
                document.getElementById("<%= txtEmpCode.ClientID %>").value=Dtl[0];
                document.getElementById("<%= txtEmpName.ClientID %>").value=Dtl[1];
                document.getElementById("<%= txtDoj.ClientID %>").value=Dtl[2];
                document.getElementById("<%= cmbDesignationDoj.ClientID %>").value=Dtl[3];
                document.getElementById("<%= cmbDesignation.ClientID %>").value=Dtl[5];
                document.getElementById("<%= cmbDesignationreview.ClientID %>").value=Dtl[23];
                document.getElementById("<%= cmbReviewingHead.ClientID %>").value=Dtl[12];
                document.getElementById("<%= cmbFunctionalHead.ClientID %>").value=Dtl[14];                    
                document.getElementById("<%= cmbDepartment.ClientID %>").value=Dtl[17]; 
                document.getElementById("<%= txtRemarks.ClientID %>").value=Dtl[16];        
                document.getElementById("<%= cmbReportingTo.ClientID %>").value=Dtl[19];     
                document.getElementById("<%= cmbLocationBranch.ClientID %>").value=Dtl[21];     
                document.getElementById("<%= txtEvaluation.ClientID %>").value=Dtl[11];
                document.getElementById("<%= txtReportedBy.ClientID %>").value=Dtl[8];    
                document.getElementById("<%= txtPeriod.ClientID %>").value=Dtl[10];   
                
                 
        }

            function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        

        function btnFinish_onclick() {
          finish_save_flag=1;
       

      
                var r = confirm("Do you want to finish? After pressing finish,You can't edit.");
               
               if (r) {
                    btnSave_onclick();
                    
                }
                else 
                {
                        finish_save_flag=0;

                         return false;

////                           var Period_ID = document.getElementById("<%= hdnPeriod.ClientID %>").value;
////                           var Role_ID = document.getElementById("<%= hdnRoleID.ClientID %>").value;
////                           var Emp_ID = document.getElementById("<%= hdnEmp_ID.ClientID %>").value;
////                           //// window.open('ViewAppraisal.aspx', "_self");
////                            window.open("ViewAppraisal.aspx?Role_ID=" + Role_ID + " &Emp_ID=" +  Emp_ID + "&Period_ID=" + Period_ID +  "", "_self");
////        
            }
           
          

         }
          function  btnUndo_onclick()
         
         {
         var ToData = "4Ø"
           ToServer(ToData, 4);
         
         }
         function btnSave_onclick() {
         
            
                var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
                var HRCode = document.getElementById("<%= txtHREmpCode.ClientID %>").value;

                var EmpName = document.getElementById("<%= txtEmpName.ClientID %>").value;
                var DateOfJoin = document.getElementById("<%= txtDoj.ClientID %>").value;
                var DesignationDojID = document.getElementById("<%= cmbDesignationDoj.ClientID %>").value;
                var DesignationID = document.getElementById("<%= cmbDesignation.ClientID %>").value;
                var DesignationreviewID = document.getElementById("<%= cmbDesignationreview.ClientID %>").value;

                var DepartmentID = document.getElementById("<%= cmbDepartment.ClientID %>").value;
                var BranchID = document.getElementById("<%= cmbLocationBranch.ClientID %>").value;
               
                var ReportingTo = document.getElementById("<%= cmbReportingTo.ClientID %>").value;
                var ReportedBy = document.getElementById("<%= txtReportedBy.ClientID %>").value;
                var ReviewHeadID = document.getElementById("<%= cmbReviewingHead.ClientID %>").value;
                var FunctionalHeadID = document.getElementById("<%= cmbFunctionalHead.ClientID %>").value;
                var PeriodID = document.getElementById("<%= hdnPeriod.ClientID %>").value;
              
                var Evaluation = document.getElementById("<%= txtEvaluation.ClientID %>").value;
                var Remarks = document.getElementById("<%= txtRemarks.ClientID %>").value;
                var RoleID=document.getElementById("<%= hdnRoleID.ClientID %>").value;

                var row1=""
               row1 = document.getElementById("<%= hdnDutiesDtl.ClientID %>").value.split("¥");
                var NewDutiesStr=""
                 var Count=0;
                for (n = 1; n <= row1.length-1 ; n++) {     
                    var Dutiescol=row1[n].split("µ");  
                      Count += Math.abs(Dutiescol[1]);          
                    if(Dutiescol[0]!="" && Dutiescol[1]!="" && Dutiescol[4]!="" && Dutiescol[5]!="")
                    {                         
                        NewDutiesStr+="¥"+Dutiescol[0]+"µ"+Dutiescol[1]+"µ"+Dutiescol[2]+ "µ" +Dutiescol[3]+ "µ" +Dutiescol[4]+ "µ" +Dutiescol[5];;
                         
                     } 
                    if(Dutiescol[4]!="" && Dutiescol[5]=="")
                    {                         
                       alert("Please enter Supervisor Score in 'Duties and Responsibilities' tab "); 
                       document.getElementById("txtSupervisorDutiesScore"+ n.toString()).focus(); 
                       return false;
                    }
                   
                     if (Dutiescol[5]!="")
                     {
                   
                        if((Dutiescol[5]<1.00 )|| (Dutiescol[5]>10.00))
                        {
                       
                            alert("Choose Score from 1 to 10");
                            document.getElementById("txtSupervisorDutiesScore"+n.toString()).focus();
                            return false;

                        }
                    }
                  
                    
                 }
             if (Count!=100)
             {
                alert("Weightage score in Duties and Responsibilities should be equal to 100");
                document.getElementById("txtPerentage").focus();
                return false;
                
             }
                
               
             
               row2 = document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value.split("¥");
               
                var NewBeliefsStr="";
                for (n = 1; n <= row2.length-1 ; n++) {     
                    var Beliefcol=row2[n].split("µ"); 

                    if(Beliefcol[5]=="" && Beliefcol[6]!="")
                    {
                        alert("In ESAF Beliefs, supervisor score can be marked only where self score is entered ");  document.getElementById("txtSupervisorScoreBeli"+ n.toString()).focus(); return false;
                    }
                    if(Beliefcol[5]!="" && Beliefcol[6]=="")
                    {
                        alert("Please enter Supervisor Score in ESAF Beliefs tab ");  document.getElementById("txtSupervisorScoreBeli"+ n.toString()).focus(); return false;
                    }
                    if(Beliefcol[6]!="" )
                    {                         
                        NewBeliefsStr+="¥"+Beliefcol[0]+"µ"+Beliefcol[4]+"µ"+Beliefcol[5]+"µ"+Beliefcol[6];
                    }

                 if (Beliefcol[6]!="")
                 {
                 
                        if((Beliefcol[6]<1.00 )|| (Beliefcol[6]>10.00))
                        {
                       
                            alert("Choose Score from 1 to 10 ");
                            document.getElementById("txtSupervisorScoreBeli"+n.toString()).focus();
                            return false;

                        }
                 }
                   
                }
                
               
                row6 = document.getElementById("<%= hdnTraits.ClientID %>").value.split("¥");
               
                var NewTraitsStr="";
                for (n = 1; n <= row6.length-1 ; n++) {     
                    var Traitscol=row6[n].split("µ"); 

                    if(Traitscol[6]=="" && Traitscol[7]!="")
                    {
                        alert(" In Traits and Behaviours, supervisor score can be marked only where self score is entered ");  document.getElementById("txtSupervisorScoreTraits"+ n.toString()).focus(); return false;
         

                    } 
                     if(Traitscol[6]!="" && Traitscol[7]=="")
                    {
                        alert("Please enter Supervisor Score in Traits and Behaviours tab ");  document.getElementById("txtSupervisorScoreTraits"+ n.toString()).focus(); return false;
         

                    } 
                    if(Traitscol[7]!="")
                    {
                  
                        if((Traitscol[7]<1.00 )|| (Traitscol[7]>10.00))
                        {
                               
                                alert("Choose Score from 1 to 10 ");
                                document.getElementById("txtSupervisorScoreTraits"+n.toString()).focus();
                                return false;

                        }   
                    }  
                    if(Traitscol[7]=="" )
                    {
                        alert("Please fill all the fields of Supervisor column in Traits and Behaviours Tab");
                        document.getElementById("txtSupervisorScoreTraits"+ n.toString()).focus(); return false;
                    }
                    else
                    {                         
                        NewTraitsStr+="¥"+Traitscol[0]+"µ"+Traitscol[6]+"µ"+Traitscol[7];
                    }                
                 
                }
                 
                updateValueEfforts(-1);
                 var row7 = document.getElementById("<%= hdnEfforts.ClientID %>").value.split("¥");
             
                var NewEffortsStr="";
                for (n = 1; n <= row7.length-1 ; n++) {     
                    var Effortscol=row7[n].split("µ"); 
                    if(Effortscol[0]!="" || Effortscol[1]!="" || Effortscol[2]!=""   )   
                    {                         
                        NewEffortsStr+="¥"+Effortscol[0]+"µ"+Effortscol[1]+"µ"+Effortscol[2];
                       
                    }                
                
                }
                 
                if ((RoleID ==2) && ((document.getElementById("txtPerformanceHODComments").value=="") || (document.getElementById("txtPerformanceHODScore").value=="")))
                {

                    alert("Please fill  all the fields of 'Comments and Remarks of the Reviewing Officer/HOD' Tab");
                    document.getElementById("txtPerformanceHODComments").focus();
                    return false;

                }
                if ((RoleID ==2)  && ((document.getElementById("txtPersonalityHODComments").value=="") || (document.getElementById("txtPersonalityHODScore").value=="") ))
                {

                    alert("Please fill  all the fields of 'Comments and Remarks of the Reviewing Officer/HOD' Tab ");
                    document.getElementById("txtPerformanceHODComments").focus();
                    return false;

                }
                 if ((RoleID ==3) && ((document.getElementById("txtPerformanceEVPComments").value=="") || (document.getElementById("txtPerformanceEVPScore").value=="")))
                {

                    alert("Please fill  all the fields of 'Comments and Remarks of the Head/EVP' Tab ");
                    document.getElementById("txtPerformanceEVPComments").focus();
                    return false;

                }
                if ((RoleID ==3)  && ((document.getElementById("txtPersonalityEVPComments").value=="") || (document.getElementById("txtPersonalityEVPScore").value=="") ))
                {

                    alert("Please fill  all the fields of 'Comments and Remarks of the Head/EVP' Tab ");
                    document.getElementById("txtPersonalityEVPComments").focus();
                    return false;

                }
                if(document.getElementById("txtPerformanceHODScore").value!="")
                {
                    if((document.getElementById("txtPerformanceHODScore").value<1.00 )|| (document.getElementById("txtPerformanceHODScore").value>10.00))   
                    {
                            alert("Choose Score from 1 to 10");
                            document.getElementById("txtPerformanceHODScore").focus();
                            return false;

                    } 
                }
                 if(document.getElementById("txtPersonalityHODScore").value!="")
                {

                    if((document.getElementById("txtPersonalityHODScore").value<1.00 )|| (document.getElementById("txtPersonalityHODScore").value>10.00))
                    {
                            alert("Choose Score from 1 to 10");
                            document.getElementById("txtPersonalityHODScore").focus();
                            return false;

                    }
                }
                if(document.getElementById("txtPerformanceEVPScore").value!="")
                {
                     if((document.getElementById("txtPerformanceEVPScore").value<1.00 )|| (document.getElementById("txtPerformanceEVPScore").value>10.00))
                
                    {
                            alert("Choose Score from 1 to 10");
                            document.getElementById("txtPerformanceEVPScore").focus();
                            return false;

                    }
                  } 

                if(document.getElementById("txtPersonalityEVPScore").value!="")
                {
                         if((document.getElementById("txtPersonalityEVPScore").value<1.00 )|| (document.getElementById("txtPersonalityEVPScore").value>10.00))
                        {
                                alert("Choose Score from 1 to 10");
                                document.getElementById("txtPersonalityEVPScore").focus();
                                return false;

                        }  

                }
            var ToData = "1Ø" + EmpCode + "Ø" +  DesignationDojID  + "Ø" + DesignationID + "Ø" + DesignationreviewID  + "Ø" + DepartmentID  + "Ø" + BranchID  + "Ø" + ReportingTo + "Ø" +  ReportedBy + "Ø" + ReviewHeadID + "Ø" + FunctionalHeadID + "Ø" + PeriodID + "Ø" + Evaluation + "Ø" + Remarks + "Ø" + NewDutiesStr + "Ø" + document.getElementById("txtAvgDuties").value + "Ø" + document.getElementById("txtselfDuties").value + "Ø" + NewBeliefsStr + "Ø" +  document.getElementById("txtAvgBeli").value + "Ø" + document.getElementById("txtBeliselfRemarks").value +"Ø" + document.getElementById("txtSignificant1").value + "Ø" + document.getElementById("txtSignificant2").value + "Ø" + document.getElementById("txtSignificant3").value + "Ø" + document.getElementById("txtImprove1").value + "Ø" + document.getElementById("txtImprove2").value+ "Ø" + document.getElementById("txtImprove3").value+ "Ø" + document.getElementById("txtPlan1").value+ "Ø" + document.getElementById("txtPlan2").value+ "Ø" + document.getElementById("txtPlan3").value + "Ø" + NewTraitsStr + "Ø" +document.getElementById("txtAvgTraits").value + "Ø" + document.getElementById("txtAppraiseeCommitments").value + "Ø" +  document.getElementById("txtAvgSuperDuties").value + "Ø" +document.getElementById("txtSupervisorRemarksDuties").value + "Ø" +document.getElementById("txtSupervisorAvgBeli").value + "Ø" + document.getElementById("txtBeliSupervisorRemarks").value + "Ø" + document.getElementById("txtSupervisorAvgTraits").value + "Ø" + document.getElementById("<%= hdnRoleID.ClientID %>").value + "Ø" + document.getElementById("txtDisciplinaryActions").value + "Ø" + NewEffortsStr + "Ø" +  document.getElementById("txtPerformanceHODComments").value + "Ø" +  document.getElementById("txtPerformanceHODScore").value+ "Ø" + document.getElementById("txtPersonalityHODComments").value + "Ø" +  document.getElementById("txtPersonalityHODScore").value + "Ø" + document.getElementById("txtReviewingAvg").value + "Ø" + document.getElementById("txtPerformanceEVPComments").value + "Ø" + document.getElementById("txtPerformanceEVPScore").value+ "Ø" + document.getElementById("txtPersonalityEVPComments").value+ "Ø" + document.getElementById("txtPersonalityEVPScore").value + "Ø" + document.getElementById("txtEVPAvg").value + "Ø" + document.getElementById("txtEVPRecommend").value+ "Ø" + HRCode + "Ø" +  document.getElementById("txtMDRemarks").value;
        
           ToServer(ToData, 1);
                     
           
        }
        function Fill(){
                table_fill();
                table_Signifill();
                table_ImprovementAreafill();
                table_DevelopmentPlanfill();
                table_AppraiseeCommentsfill();
                }





        function table_fill() 
       {

   
   
                var RoleID=document.getElementById("<%= hdnRoleID.ClientID %>").value;
               var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               
               if((StatusID==1) && (RoleID==1))
                {
                DisFlag = '';
                        
                }
                else
                {
                   
                    DisFlag = 'disabled';
                       

                }
      
         if (document.getElementById("<%= hdnDutiesDtl.ClientID %>").value != "") 
         {

           
            document.getElementById("<%= pnDuties.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var TotWeightage=0;
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>";
            tab += "<td style='width:42%;text-align:center'><b>Duties,Responsibilities & Targets</b></td>"; 
            tab += "<td style='width:10%;text-align:center'><b>Weightage in %<br>(Sum should be 100%)</b></td>";   
            tab += "<td style='width:10%;text-align:center'><b>Key Performance Indicators</b></td>";
            tab += "<td style='width:10%;text-align:center'><b>Status while reviewing </b></td>";    
            tab += "<td style='width:10%;text-align:center'><b>Performance Score(Self)</b></td>";  
            tab += "<td style='width:10%;text-align:center'><b>Performance Score(Supervisor)</b></td>";      
            tab +="<td style='width:3%;text-align:left'></td>";
            tab += "</tr>";     
            row = document.getElementById("<%= hdnDutiesDtl.ClientID %>").value.split("¥");
          
            for (n = 1; n <= row.length - 1; n++) 
            {
                 col = row[n].split("µ");
                
                 if (row_bg == 0) 
                 {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  else 
                  {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";  
                 
                        var Duities="";
                        var Weightage;
                        var KeyPerformance;
                        var selfScore;
                        var SuperScore;
                   
                    if(col[0]!="")
                    {
                  
                      Duities = col[0];
                      
                       var txtDuities = "<input id='txtDuities" + n + "' name='txtDuities" + n + "' value='" + Duities + "'    type='Text'  'maxlength='500'   disabled=true style='width:99%;' onchange='updateValue("+ n + ")' class='NormalText' />";
                       tab += "<td style='width:42%;text-align:left' >"+ txtDuities +"</td>";
                    }
                    else
                    {

                    var txtDuities = "<input id='txtDuities" + n + "' name='txtDuities" + n + "' type='Text' style='width:99%;'  class='NormalText'  'maxlength='500' disabled=true     onchange='updateValue("+n+");CalculateTotal()'     />";
                    tab += "<td style='width:42%;text-align:left' >"+ txtDuities +"</td>";
                       

                    }
                    if(col[1]!="")
                    {
               
                     Weightage = col[1];

                      TotWeightage = parseInt(TotWeightage) + parseInt(Weightage) ;
                     
                      var txtWeightage = "<input id='txtWeightage" + n + "' name='txtWeightage" + n + "' value='" + Math.abs(Weightage).toFixed(2) + "'  type='Text'  onkeypress='return NumericCheck(event)' "+ DisFlag + " TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='3'  class='NormalText' onchange='updateValue("+n+"); CalculateTotal()' />";
                      tab += "<td style='width:10%;text-align:left' >"+ txtWeightage +"</td>";
                      
                    }
                    else
                    {

                     var txtWeightage = "<input id='txtWeightage" + n + "' name='txtWeightage" + n + "' type='Text' style='width:99%;'  class='NormalText'  maxlength='3' "+ DisFlag + "     onkeypress='return NumericWithDot(this,event)'  onchange='updateValue("+n+");CalculateTotal()'     />";
                     tab += "<td style='width:10%;text-align:left' >"+ txtWeightage +"</td>";
                     

                    }
                    if(col[2]!="")
                    {
                  
                      KeyPerformance = col[2];
                      var txtKeyPerformance = "<input id='txtKeyPerformance" + n + "' name='txtKeyPerformance" + n + "'  value='" + KeyPerformance + "' type='Text'   disabled=true style='width:99%;' class='NormalText' maxlength='100' onchange='updateValue("+n+")'    />"; 
                        tab += "<td style='width:10%;text-align:left' >"+ txtKeyPerformance +"</td>";
                    }
                    else
                    {

                     var txtKeyPerformance = "<input id='txtKeyPerformance" + n + "' name='txtKeyPerformance" + n + "' type='Text' style='width:99%;'  class='NormalText'  maxlength='3'    disabled=true   onchange='updateValue("+n+")'     />";
                     tab += "<td style='width:10%;text-align:left' >"+ txtKeyPerformance +"</td>";
                     
                    }
                     if(col[3]!="")
                    {
                 
                      Status   = col[3];
                    var txtStatus = "<input id='txtStatus" + n + "' name='txtStatus" + n + "' value='" + Status + "' type='Text'  style='width:99%;' class='NormalText'  disabled=true maxlength='100' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtStatus +"</td>";
                       
                    }
                    else
                    {
                     var txtStatus = "<input id='txtStatus" + n + "' name='txtStatus" + n + "' type='Text' style='width:99%;'  class='NormalText'  maxlength='3'    disabled=true onchange='updateValue("+n+")'     />";
                     tab += "<td style='width:10%;text-align:left' >"+ txtStatus +"</td>";
                     
                    }

                     if(col[4]!="")
                    {
             
                      selfScore = col[4];
                       var txtSelfScore = "<input id='txtSelfScore" + n + "' name='txtSelfScore" + n + "' value='" + Math.abs(selfScore).toFixed(2) + "' type='Text'  style='width:99%;'  disabled=true class='NormalText'  maxlength='3' onchange='updateValue("+n+")'  />";
                        tab += "<td style='width:10%;text-align:left' >"+ txtSelfScore +"</td>";
                      
                    }
                   else
                    {
                     var txtSelfScore = "<input id='txtSelfScore" + n + "' name='txtSelfScore" + n + "' type='Text' style='width:99%;'  class='NormalText'  maxlength='3'  disabled=true  onchange='updateValue("+n+")'      />";
                     tab += "<td style='width:10%;text-align:left' >"+ txtSelfScore +"</td>";
                     
                    }

                    if(col[5]!="")
                    {
                  
                 
                     SupervisorDutiesScore = col[5];
                       var txtSupervisorDutiesScore = "<input id='txtSupervisorDutiesScore" + n + "' name='txtSupervisorDutiesScore" + n + "' value='" + Math.abs(SupervisorDutiesScore).toFixed(2) + "' type='Text'  style='width:99%;'  "+ DisFlag + " class='NormalText'  maxlength='3'  onblur='return isNumberKey(this.value)'  onkeypress='return NumericWithDot(this,event)'    onchange='updateValue("+ n + ");CalculateTotal()' />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtSupervisorDutiesScore +"</td>";
                    }
                     else
                    {
                    var txtSupervisorDutiesScore = "<input id='txtSupervisorDutiesScore" + n + "' name='txtSupervisorDutiesScore" + n + "' type='Text' style='width:99%;'  class='NormalText'  maxlength='3'  "+ DisFlag + "  onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)'  onchange='updateValue("+n+");CalculateTotal()'     />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtSupervisorDutiesScore +"</td>";
                       
                    }  
               
                    if(col[0] == "" && col[1] == "" && col[2] == "" && col[3] == "" && col[4] =="  " && col[5] =="  " ) 
                    {
                    
                 
                    tab += "<td style='width:3%;text-align:center' onclick=DeleteRow('" + n + "')></td>";
                        
                  
                    
                    }
                    else
                    {
                       tab += "<td style='width:3%;text-align:center' onclick=DeleteRow('" + n + "')></td>";
                        
               
                    }
                    tab += "</tr>";
                    }
                    var self="";
                    var Avg="";
                    var Supervisor="";
                     
                     row1 = document.getElementById("<%= hdnDutiesDtl1.ClientID %>").value.split("¥");
                    

                     col = row1[1].split("µ");
                    
                   tab += "<tr height=20px; class='tblQal'>";
                    tab += "<td colspan=2 style='width:45%;text-align:center;font-weight:bold;'></td>";
                    
                    var txtPerentage = "<input id='txtPerentage' name='txtPerentage' value="+ TotWeightage +"  type='Text'   style='width:99%;' "+ DisFlag + " class='ReadOnlyTextBox'      />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtPerentage +"</td>";

                    tab += "<td colspan=2 style='width:20%;text-align:center;font-weight:bold;'>Weighted Average Score</td>";
                
                    if(col[0]!="")
                    {
                
                      AvgDuties = col[0];
                       var txtAvgDuties = "<input id='txtAvgDuties' name='txtAvgDuties' value='" + Math.abs(AvgDuties).toFixed(2) + "'  type='Text'  disabled=true onblur='return isNumberKey(this.value)'  style='width:99%;'  class='NormalText'  disabled=true   onchange='updateValue("+n+")'  />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtAvgDuties +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtAvgDuties = "<input id='txtAvgDuties' name='txtAvgDuties' type='Text' style='width:99%;'  disabled=true  onblur='return isNumberKey(this.value)' value='0' class='NormalText'  disabled=true    onchange='updateValue("+n+")' />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtAvgDuties +"</td>";
                       
                    }
                     if(col[1]!="")
                    {
                
                      AvgSuperDuties = col[1];
                       var txtAvgSuperDuties = "<input id='txtAvgSuperDuties' name='txtAvgSuperDuties' value='" + Math.abs(AvgSuperDuties).toFixed(2) + "'  type='Text' onblur='return isNumberKey(this.value)'  style='width:99%;'  class='NormalText'  "+ DisFlag + " onchange='updateValue("+n+")'  />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtAvgSuperDuties +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtAvgSuperDuties = "<input id='txtAvgSuperDuties' name='txtAvgSuperDuties' type='Text' style='width:99%;'  onblur='return isNumberKey(this.value)' value='0' class='NormalText'   "+ DisFlag + "  onchange='updateValue("+n+")' />";
                    tab += "<td style='width:10%;text-align:left' >"+ txtAvgSuperDuties +"</td>";
                       
                    }
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>"; 
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td colspan=2 style='width:47%;text-align:center' ><b>Remarks by Self </b></td>";
                    if(col[2]!="")
                    {
                
                      SelfRemarksDuties = col[2];
                       var txtselfDuties = "<input id='txtselfDuties' name='txtselfDuties' value='" + SelfRemarksDuties + "'  disabled=true type='Text' disabled=true style='width:99%;'  class='NormalText'  maxlength='500'    onchange='updateValue("+n+")'  />";
                    tab += "<td colspan=5 style='width:50%;text-align:left' >"+ txtselfDuties +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtselfDuties = "<input id='txtselfDuties' name='txtselfDuties' type='Text' style='width:99%;'   disabled=true class='NormalText'  maxlength='500'    onchange='updateValue("+n+")'' />";
                    tab += "<td colspan=5 style='width:50%;text-align:left' >"+ txtselfDuties +"</td>";
                       
                    }
                    
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>";   
                    tab += "<tr height=20px;  class='tblQal' >";
                    tab += "<td colspan=2 style='width:47%;text-align:center' ><b>Remarks by Supervisor </b></td>";
                     if(col[3]!="")
                    {
                
                      SupervisorRemarksDuties = col[3];
                       var txtSupervisorRemarksDuties = "<input id='txtSupervisorRemarksDuties' name='txtSupervisorRemarksDuties' value='" + SupervisorRemarksDuties + "'  type='Text'   style='width:99%;'  class='NormalText'  maxlength='500'   "+ DisFlag + " onchange='updateValue("+n+")'  />";
                    tab += "<td colspan=5 style='width:50%;text-align:left' >"+ txtSupervisorRemarksDuties +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtSupervisorRemarksDuties = "<input id='txtSupervisorRemarksDuties' name='txtSupervisorRemarksDuties' type='Text' style='width:99%;'    class='NormalText'  maxlength='500'   "+ DisFlag + "  onchange='updateValue("+n+")'' />";
                    tab += "<td colspan=5 style='width:50%;text-align:left' >"+ txtSupervisorRemarksDuties +"</td>";
                       
                    }
                
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>";         

                tab += "</table></div></div></div>"; 
                document.getElementById("<%= pnDuties.ClientID %>").innerHTML = tab;
                
                }
                else
                document.getElementById("<%= pnDuties.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
                 
               
        }
      

     function isNumberKey(Val) {
    
                if (Val!="")
                {
                    if((Val<1) || (Val >10) ){
                        alert('choose number from 1-10');
                         return false;
                    }
       
                  
              }
    }
 
        function table_fillBeliefs(Arg) 
       {
       
            var Row=Arg.split("Ø");       
        
            var RoleID=document.getElementById("<%= hdnRoleID.ClientID %>").value;
               var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if((StatusID==1) && (RoleID==1))
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
         document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value=Row[1];
        
         if (document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value != "") 
         {
            document.getElementById("<%= pnBeliefsDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>";
             
            tab += "<td style='width:8%;text-align:center'><b></b></td>"; 
            tab += "<td style='width:15%;text-align:center'><b>Parameters</b></td>";   
            tab += "<td style='width:35%;text-align:center'><b>Suggestive Measurement Points</b></td>";
            tab += "<td style='width:18%;text-align:center'><b>Activities Done with Values/Timelines </b></td>";    
            tab += "<td style='width:8%;text-align:center'><b>Self score</b></td>";  
            tab += "<td style='width:8%;text-align:center'><b>Supervisor's Score</b></td>";   
                   
            tab +="<td style='width:3%;text-align:left'></td>";
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       

            row = document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value.split("¥");
            var m=0;

            for (n = 1; n <= row.length-1 ; n++) 
            {
                 col = row[n].split("µ");
                
                if (row_bg == 1) 
                 {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  else 
                  {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  m=n+1;
                  tab += "<td style='width:5%;text-align:center' >" + n + "</td>";  
                 
                        var Category="";
                        var Parameters="";
                        var MeasurementPoint="";
                        var selfScore="";
                        var SuperScore="";
                  
                    if(col[1]!="")
                    {
                    //  tab += "<td style='width:10%;text-align:left'>" + col[0] + "</td>";
                      Category = col[1];
                      
                       var txtCategory = "<input id='txtCategory" + n + "' name='txtCategory" + n + "' value='" + Category + "'    type='Text'  'maxlength='200' style='width:99%;'  disabled=true class='NormalText' onchange='updateScore("+ m +")' />";
               
                    tab += "<td style='width:8%;text-align:left' >"+ txtCategory +"</td>";
                    }
                    
                    if(col[2]!="")
                    {
                 //  tab += "<td style='width:15%;text-align:left'>" + col[1] + "</td>";
                      Parameters = col[2];
                       var txtParameters = "<input id='txtParameters" + n + "' name='txtParameters" + n + "' value='" + Parameters + "'  type='Text'  disabled=true  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='3'  class='NormalText' onchange='updateValueBelief("+ n +")' />";
                    tab += "<td style='width:15%;text-align:left' >"+ txtParameters +"</td>";
                      
                    }
                    
                    if(col[3]!="")
                    {
                   //   tab += "<td style='width:10%;text-align:left'>" + col[2] + "</td>";
                      MeasurementPoint = col[3];
                    
                      var txtMeasurementPoint = "<input id='txtMeasurementPoint" + n + "' name='txtMeasurementPoint" + n + "'  value='" + MeasurementPoint + "' type='Text'  style='width:99%;' disabled=true class='NormalText' maxlength='10' onchange='updateValueBelief("+ n +")'    />"; 
                        tab += "<td style='width:35%;text-align:left' >"+ txtMeasurementPoint +"</td>";
                    }
                    
                     if(col[4]!="")
                    {
                
                      Activities   = col[4];
                    
                    var txtActivities = "<input id='txtActivities" + n + "' name='txtActivities" + n + "' value='" + Activities + "' type='Text'  style='width:99%;' class='NormalText' maxlength='500' disabled=true onchange='updateValueBelief("+ n +")'  />";
                    tab += "<td style='width:18%;text-align:left' >"+ txtActivities +"</td>";
                       
                    }
                    else
                    {
                    var txtActivities = "<input id='txtActivities" + n + "' name='txtActivities" + n + "' type='Text'  style='width:99%;' class='NormalText' maxlength='500' disabled=true onchange='updateValueBelief("+ n +")'  />";
                    tab += "<td style='width:18%;text-align:left' >"+ txtActivities +"</td>";
                       
                    }
                     if(col[5]!="")
                    {
                
                      selfScoreBelief = col[5];
                       var txtSelfScoreBeli = "<input id='txtSelfScoreBeli" + n + "' name='txtSelfScoreBeli" + n + "' value='" + Math.abs(selfScoreBelief).toFixed(2)  + "' type='Text' onkeypress='return NumericWithDot(this,event)'  disabled=true onblur='return isNumberKey(this.value)'  style='width:99%;'  class='NormalText'  maxlength='3'  onchange='updateValueBelief("+ n +")'  />";
                    tab += "<td style='width:8%;text-align:left' >"+  txtSelfScoreBeli +"</td>";
                      
                    }
                     else
                    {
                
                   
                       var txtSelfScoreBeli = "<input id='txtSelfScoreBeli" + n + "' name='txtSelfScoreBeli" + n + "'  type='Text' onkeypress='return NumericWithDot(this,event)'  disabled=true onblur='return isNumberKey(this.value)'  style='width:99%;'  class='NormalText'  maxlength='3'  onchange='updateValueBelief("+ n +")'  />";
                    tab += "<td style='width:8%;text-align:left' >"+  txtSelfScoreBeli +"</td>";
                      
                    }
                    if(col[6]!="")
                    {
                
                      SupervisorScoreBeli = col[6];
                       var txtSupervisorScoreBeli = "<input id='txtSupervisorScoreBeli" + n + "' name='txtSupervisorScoreBeli" + n + "'  value='" + Math.abs(SupervisorScoreBeli).toFixed(2)  + "' type='Text' "+ DisFlag + " onkeypress='return NumericWithDot(this,event)'     onblur='return isNumberKey(this.value)'  style='width:99%;'  class='NormalText'  maxlength='3'  onchange='updateValueBelief("+ n +");CalculateScoreBeli()'  />";
                    tab += "<td style='width:8%;text-align:left' >"+  txtSupervisorScoreBeli +"</td>";
                      
                    }
                    else
                    {
                     var txtSupervisorScoreBeli = "<input id='txtSupervisorScoreBeli" + n + "'  name='txtSupervisorScoreBeli" + n + "'  type='Text' onkeypress='return NumericWithDot(this,event)'  "+ DisFlag + "  onblur='return isNumberKey(this.value)'  style='width:99%;'  class='NormalText'  maxlength='3'  onchange='updateValueBelief("+ n +");CalculateScoreBeli()' />";
                    tab += "<td  style='width:8%;text-align:left' >"+ txtSupervisorScoreBeli +"</td>";
                 
                  

                    }

                   
                    tab += "<td style='width:3%;text-align:center' ></td>";
                    tab += "</tr>";
                    }
                    var self="";
                    var Avg="";
                    var Supervisor="";
                    
                     document.getElementById("<%= hdnBeliefsDtl1.ClientID %>").value=Row[2];
                     row = document.getElementById("<%= hdnBeliefsDtl1.ClientID %>").value.split("¥");
                     
                     col = row[1].split("µ");
                 
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td colspan=5 style='width:81%;text-align:center;font-weight:bold;'>Average Score</td>";

                    var AvgBeli=""
                     if(col[1]!="")
                    {
                        
                        AvgBeli = col[1];
                        var txtAvgBeli = "<input id='txtAvgBeli' name='txtAvgBeli' value='" + Math.abs(AvgBeli).toFixed(2) + "'  type='Text'   onblur='return isNumberKey(this.value)' disabled=true  style='width:99%;'  class='NormalText'   onkeypress='return NumericCheck(event)'  onblur='return checkvalue(this.value,"+ n +")'  />";
                        tab += "<td style='width:8%;text-align:left' >"+ txtAvgBeli +"</td>";
                      
                    }
                    else
                    {
                       
                    var txtAvgBeli = "<input id='txtAvgBeli' name='txtAvgBeli' type='Text' style='width:99%;'  onblur='return isNumberKey(this.value)'  value='0' class='NormalText'  disabled=true      />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtAvgBeli +"</td>";
                       
                    }
                    if(col[2]!="")
                    {
                
                      SupervisorAvgBeli = col[2];
                       var txtSupervisorAvgBeli = "<input id='txtSupervisorAvgBeli' name='txtSupervisorAvgBeli' value='" + Math.abs(SupervisorAvgBeli).toFixed(2) + "' type='Text' onkeypress='return NumericWithDot(this,event)'   disabled=true onblur='return isNumberKey(this.value)'  style='width:99%;'  class='NormalText'  maxlength='3'     />";
                    tab += "<td style='width:8%;text-align:left' >"+  txtSupervisorAvgBeli +"</td>";
                      
                    }
                    else
                    {
                     var txtSupervisorAvgBeli = "<input id='txtSupervisorAvgBeli' name='txtSupervisorAvgBeli' type='Text' style='width:99%;'  onblur='return isNumberKey(this.value)'  value='0'  class='NormalText'  disabled=true    />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtSupervisorAvgBeli +"</td>";
                     

                    }
                   

                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>"; 
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td colspan=2 style='width:10%;text-align:center' ><b>Remarks by Self </b></td>";
                    var BeliselfRemarks=""
                     if(col[3]!="")
                    {
                
                      BeliselfRemarks = col[3];
                       var txtBeliselfRemarks = "<input id='txtBeliselfRemarks' name='txtBeliselfRemarks' value='" + BeliselfRemarks + "'  type='Text'  style='width:99%;'  class='NormalText'  maxlength='200'  disabled=true  />";
                    tab += "<td colspan=5 style='width:87%;text-align:left' >"+ txtBeliselfRemarks +"</td>";
                      
                    }
                    else
                    {
                     var txtBeliselfRemarks = "<input id='txtBeliselfRemarks' name='txtBeliselfRemarks' type='Text' style='width:99%;'    disabled=true  style='width:99%;'  class='NormalText'   maxlength='200'  />";
                    tab += "<td colspan=5  style='width:87%;text-align:left' >"+ txtBeliselfRemarks +"</td>";
                     

                    }
                                   
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>";   
                    tab += "<tr height=20px;  class='tblQal' >";
                    tab += "<td colspan=2 style='width:10%;text-align:center' ><b>Remarks by Supervisor </b></td>";
                    if(col[4]!="")
                    {
                
                      BeliSupervisorRemarks = col[4];
                       var txtBeliSupervisorRemarks = "<input id='txtBeliSupervisorRemarks' name='txtBeliSupervisorRemarks' value='" + BeliSupervisorRemarks + "'  type='Text'  style='width:99%;' "+ DisFlag + " class='NormalText'  maxlength='200'     />";
                    tab += "<td colspan=5 style='width:87%;text-align:left' >"+ txtBeliSupervisorRemarks +"</td>";
                      
                    }
                    else
                    {
                   
                    var txtBeliSupervisorRemarks = "<input id='txtBeliSupervisorRemarks' name='txtBeliSupervisorRemarks' type='Text' style='width:99%;'  "+ DisFlag + "  class='NormalText'      />";
                    tab +="<td colspan=5 style='width:87%;text-align:left' >"+ txtBeliSupervisorRemarks +"</td>";
                       
                    }
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>";         

                tab += "</table></div></div></div>"; 
                document.getElementById("<%= pnBeliefsDtl.ClientID %>").innerHTML = tab;
               
                
                }
                else
                document.getElementById("<%= pnBeliefsDtl.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
              
               
        }

        function updateValueBelief(id)
        {  
       
             row = document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value.split("¥");
           
             var newStr=""
              for (n = 1; n <= row.length - 1; n++) 
                {
                        var col = row[n].split("µ");
                        
                        var Activities=document.getElementById("txtActivities"+ n.toString()).value;
                        var SelfScoreBeli = document.getElementById("txtSelfScoreBeli"+ n.toString()).value;
                        var SupervisorScoreBeli = document.getElementById("txtSupervisorScoreBeli"+ n.toString()).value;
                    
                        newStr+="¥" +col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3]  + "µ" +Activities + "µ" + SelfScoreBeli + "µ" + SupervisorScoreBeli;  
                     
               
                }

                
            document.getElementById("<%= hdnBeliefsDtl.ClientID %>").value=newStr;
          
        }
        
        function table_Signifill(Arg) 
       {
          
             var Row=Arg.split("Ø");
            

            document.getElementById("<%= hdnSignificant.ClientID %>").value=Row[3];
        
           if (document.getElementById("<%= hdnSignificant.ClientID %>").value != "") 
           {
            document.getElementById("<%= pnSignificant.ClientID %>").style.display = '';
          
            var tab = "";
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            
            row = document.getElementById("<%= hdnSignificant.ClientID %>").value.split("¥");
            
            var Significant1="";
            var Significant2="";
            var Significant3="";
          
                 col = row[1].split("µ");
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 1 + "</td>";  
                 if(col[1]!="")
                  {
                     
                     
                        Significant1   = col[1];
                       
                        var txtSignificant1 = "<textarea id='txtSignificant1' name='txtSignificant1' style='width:100%;text-align:left;' rows=1; ' disabled=true class='NormalText' maxlength='500'>"+Significant1+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant1 +"</td>";
                       
                  }
                  else
                  {
                   
                        var txtSignificant1 ="<textarea id='txtSignificant1' name='txtSignificant1' style='width:100%;text-align:left;' rows=1; ' disabled=true class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant1 +"</td>";
                       
                  }
                 
                    
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 2 + "</td>";
                   if(col[2]!="")
                  {
                     
                     
                        Significant2   = col[2];
                        var txtSignificant2 = "<textarea id='txtSignificant2' name='txtSignificant2' style='width:100%;text-align:left;' rows=1; '  disabled=true  class='NormalText' maxlength='500'>"+Significant2+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant2 +"</td>";
                       
                     
                  }
                  else
                  {
                   
                        var txtSignificant2 ="<textarea id='txtSignificant2' name='txtSignificant2' style='width:100%;text-align:left;' rows=1; '  disabled=true  class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant2 +"</td>";
                       
                       
                       
                  }
                   
                    
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 3 + "</td>";
                  if(col[3]!="")
                  {
                     
                     
                        Significant3   = col[3];
                                                                  
                        var txtSignificant3 = "<textarea id='txtSignificant3' name='txtSignificant3' style='width:100%;text-align:left;' rows=1; '  disabled=true class='NormalText' maxlength='500'>"+Significant3+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant3 +"</td>";
                       


                  }
                  else
                  {
                        var txtSignificant3 ="<textarea id='txtSignificant3' name='txtSignificant3' style='width:100%;text-align:left;' rows=1; ' disabled=true class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtSignificant3 +"</td>";
                       
                   
                      
                  }
                     
  
                  tab += "</tr>";
            
            
            tab += "</table></div>";
            document.getElementById("<%= pnSignificant.ClientID %>").innerHTML = tab;
             }
                else
                document.getElementById("<%= pnSignificant.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
              
               

        }

        function table_ImprovementAreafill(Arg) 
       {
            

             var Row=Arg.split("Ø");
            

            document.getElementById("<%= hdnImprovementArea.ClientID %>").value=Row[4];
        
           if (document.getElementById("<%= hdnImprovementArea.ClientID %>").value != "") 
           {
            document.getElementById("<%= pnImprovementArea.ClientID %>").style.display = '';
          
            var tab = "";
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            
            row = document.getElementById("<%= hdnImprovementArea.ClientID %>").value.split("¥");
            
            var Improve1="";
            var Improve2="";
            var Improve3="";
          
                 col = row[1].split("µ");
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 1 + "</td>";  
                
                 if(col[1]!="")
                  {
                     
                     
                        Improve1   = col[1];
                       
                        var txtImprove1 = "<textarea id='txtImprove1' name='txtImprove1' style='width:100%;text-align:left;' rows=1; '  disabled=true  class='NormalText' maxlength='500'>"+Improve1+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove1 +"</td>";
                       
                  }
                  else
                  {
                   
                        var txtImprove1 ="<textarea id='txtImprove1' name='txtImprove1' style='width:100%;text-align:left;' rows=1; ' disabled=true  class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove1 +"</td>";
                       
                  }


 
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 2 + "</td>";
                   
                   if(col[2]!="")
                  {
                     
                     
                        Improve2   = col[2];
                        var txtImprove2 = "<textarea id='txtImprove2' name='txtImprove2' style='width:100%;text-align:left;' rows=1; '  disabled=true class='NormalText' maxlength='500'>"+Improve2+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove2 +"</td>";
                       
                     
                  }
                  else
                  {
                   
                        var txtImprove2 ="<textarea id='txtImprove2' name='txtImprove2' style='width:100%;text-align:left;' rows=1; '  disabled=true  class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove2 +"</td>";
                       
                       
                       
                  }
                   
 
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 3 + "</td>";
                  if(col[3]!="")
                  {
                     
                     
                        Improve3   = col[3];
                                                                  
                        var txtImprove3 = "<textarea id='txtImprove3' name='txtImprove3' style='width:100%;text-align:left;' rows=1; '   disabled=true class='NormalText' maxlength='500'>"+Improve3+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove3 +"</td>";
                       


                  }
                  else
                  {
                        var txtImprove3 ="<textarea id='txtImprove3' name='txtImprove3' style='width:100%;text-align:left;' rows=1; '  disabled=true class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtImprove3 +"</td>";
                       
                   
                      
                  }
                 
                 
                 
                    
                  tab += "</tr>";
            
            
            tab += "</table></div>";
            document.getElementById("<%= pnImprovementArea.ClientID %>").innerHTML = tab;
             }
                else
                document.getElementById("<%= pnImprovementArea.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
              

        }

         function table_DevelopmentPlanfill(Arg) 
        {
            
             var Row=Arg.split("Ø");
            

            document.getElementById("<%= hdnDevelopmentPlan.ClientID %>").value=Row[5];
        
           if (document.getElementById("<%= hdnDevelopmentPlan.ClientID %>").value != "") 
           {
            document.getElementById("<%= pnDevelopmentPlan.ClientID %>").style.display = '';
          
            var tab = "";
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            
            row = document.getElementById("<%= hdnDevelopmentPlan.ClientID %>").value.split("¥");
           
            var Plan1="";
            var Plan2="";
            var Plan3="";
          
                 col = row[1].split("µ");
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 1 + "</td>";  
                  
                  if(col[1]!="")
                  {
                     
                     
                        Plan1   = col[1];
                       
                        var txtPlan1 = "<textarea id='txtPlan1' name='txtPlan1' style='width:100%;text-align:left;' rows=1; '  disabled=true class='NormalText' maxlength='500'>"+Plan1+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan1 +"</td>";
                       
                  }
                  else
                  {
                   
                        var txtPlan1 ="<textarea id='txtPlan1' name='txtPlan1' style='width:100%;text-align:left;' rows=1; '  disabled=true class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan1 +"</td>";
                       
                  }
                 
                    
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 2 + "</td>";
                  if(col[2]!="")
                  {
                     
                     
                        Plan2   = col[2];
                        var txtPlan2 = "<textarea id='txtPlan2' name='txtPlan2' style='width:100%;text-align:left;' rows=1; '  disabled=true class='NormalText' maxlength='500'>"+Plan2+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan2 +"</td>";
                       
                     
                  }
                  else
                  {
                   
                        var txtPlan2 ="<textarea id='txtPlan2' name='txtPlan2' style='width:100%;text-align:left;' rows=1; '  disabled=true class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan2 +"</td>";
                       
                       
                       
                  }
                  
 
                  tab += "</tr>";
                  tab += "<tr height=20px;  class='tblQal'>";
                  tab += "<td style='width:5%;text-align:center' >" + 3 + "</td>";
                    if(col[3]!="")
                  {
                     
                     
                        Plan3   = col[3];
                                                                  
                        var txtPlan3 = "<textarea id='txtPlan3' name='txtPlan3' style='width:100%;text-align:left;' rows=1; ' disabled=true class='NormalText' maxlength='500'>"+Plan3+"</textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan3 +"</td>";
                       


                  }
                  else
                  {
                        var txtPlan3 ="<textarea id='txtPlan3' name='txtPlan3' style='width:100%;text-align:left;' rows=1; '  disabled=true class='NormalText' maxlength='500'></textarea>";
                        tab += "<td style='width:95%;text-align:left' >"+ txtPlan3 +"</td>";
                       
                   
                      
                  }
                  
                  
                  tab += "</tr>";
            
            
            tab += "</table></div>";
            document.getElementById("<%= pnDevelopmentPlan.ClientID %>").innerHTML = tab;
             }
                else
                document.getElementById("<%= pnDevelopmentPlan.ClientID %>").style.display = 'none';
               //--------------------- Clearing Data ------------------------//
      
            


        }

        function table_fillTraits(Arg) 
       {
              var RoleID=document.getElementById("<%= hdnRoleID.ClientID %>").value;
               var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if((StatusID==1) && (RoleID==1))
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
             var Row=Arg.split("Ø");
             document.getElementById("<%= hdnTraits.ClientID %>").value=Row[6];
   
       
         if (document.getElementById("<%= hdnTraits.ClientID %>").value != "") 
         {
            document.getElementById("<%= pnTraits.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:4%;text-align:center'>#</td>";
             
            tab += "<td style='width:14%;text-align:center'><b>Factors</b></td>"; 
            tab += "<td style='width:17%;text-align:center'><b>9-10 (Excellent)</b></td>";   
            tab += "<td style='width:18%;text-align:center'><b>6-8 (Good)</b></td>";
            tab += "<td style='width:16%;text-align:center'><b>3-5 (Fair)</b></td>";  
            tab += "<td style='width:14%;text-align:center'><b>1-2 (Poor)</b></td>";      
            tab += "<td style='width:7%;text-align:center'><b>Self score</b></td>";  
            tab += "<td style='width:7%;text-align:center'><b>Supervisor's Score</b></td>";        
            tab +="<td style='width:3%;text-align:left'></td>";
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       

            row = document.getElementById("<%= hdnTraits.ClientID %>").value.split("¥");
          
            var m=0;

            for (n = 1; n <= row.length-1 ; n++) 
            {
                 col = row[n].split("µ");
               
                if (row_bg == 1) 
                 {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  else 
                  {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  m=n+1;
                  tab += "<td style='width:4%;text-align:center' >" + n + "</td>";  
                 
                        var Category="";
                        var Parameters="";
                        var MeasurementPoint="";
                        var selfScore="";
                        var SuperScore="";
                  
                    if(col[1]!="")
                    {
                 
                      Factors = col[1];
                      
                      var txtFactors ="<textarea class='NormalText' id='txtFactors"+ n +"' name='txtFactors" + n + "'   style='width:100%;text-align:left;' rows=9; disabled=true >"+Factors+"</textarea>";
                      tab += "<td style='width:14%;text-align:left' >"+ txtFactors +"</td>";
                    }
                    else
                    {

                      var txtFactors ="<textarea class='NormalText' id='txtFactors"+ n +"' name='txtFactors" + n + "'  style='width:100%;text-align:left;' rows=9; disabled=true></textarea>";
                      tab += "<td style='width:14%;text-align:left' >"+ txtFactors +"</td>";          
           
                       
                    }
                    if(col[2]!="")
                    {
          
                      Excellent = col[2];
                      var txtExcellent ="<textarea class='NormalText' id='txtExcellent"+ n +"' name='txtExcellent" + n + "'   style='width:100%;text-align:left;' rows=9; disabled=true>"+Excellent+"</textarea>";
                      tab += "<td style='width:17%;text-align:left' >"+ txtExcellent +"</td>";
                    }
                    else
                    {
                    var txtExcellent ="<textarea class='NormalText' id='txtExcellent"+ n +"' name='txtExcellent" + n + "'  style='width:100%;text-align:left;' rows=9; disabled=true></textarea>";
                    tab += "<td style='width:17%;text-align:left' >"+ txtExcellent +"</td>";          
             
                    }
                    if(col[3]!="")
                    {
                     Good = col[3];
                     var txtGood ="<textarea class='NormalText' id='txtGood"+ n +"' name='txtGood" + n + "'   style='width:100%;text-align:left;' rows=9; disabled=true>"+Good+"</textarea>";
                     tab += "<td style='width:18%;text-align:left' >"+ txtGood +"</td>";
                    }
                    else
                    {
                    var txtGood ="<textarea class='NormalText' id='txtGood"+ n +"' name='txtGood" + n + "'  style='width:100%;text-align:left;' rows=9; disabled=true></textarea>";
                   tab += "<td style='width:18%;text-align:left' >"+ txtGood +"</td>";          
             
                    }
                     if(col[4]!="")
                    {
                     
                         Fair = col[4];
                         var txtFair ="<textarea class='NormalText' id='txtFair"+ n +"' name='txtFair" + n + "'   style='width:100%;text-align:left;' rows=9; disabled=true>"+Fair+"</textarea>";
                         tab += "<td style='width:16%;text-align:left' >"+ txtFair +"</td>";
                    
                    }
                    else
                    {
                   
                      var txtFair ="<textarea class='NormalText' id='txtFair"+ n +"' name='txtFair" + n + "'  style='width:100%;text-align:left;' rows=9; disabled=true></textarea>";
                      tab += "<td style='width:16%;text-align:left' >"+ txtFair +"</td>";          
              
                    }
                    if(col[5]!="")
                    {
                  
                         Poor = col[5];
                         var txtPoor ="<textarea class='NormalText' id='txtPoor"+ n +"' name='txtPoor" + n + "'   style='width:100%;text-align:left;' rows=9; disabled=true>"+Poor+"</textarea>";
                         tab += "<td style='width:14%;text-align:left' >"+ txtPoor +"</td>";
                    }
                    else
                    {
                        var txtPoor ="<textarea class='NormalText' id='txtPoor"+ n +"' name='txtPoor" + n + "'  style='width:100%;text-align:left;' rows=9; disabled=true></textarea>";
                        tab += "<td style='width:14%;text-align:left' >"+ txtPoor +"</td>";          
              
                       
                    }
                    
                     if(col[6]!="")
                    {
               
                         SelfScoreTraits = col[6];
                         var txtSelfScoreTraits ="<textarea class='NormalText' id='txtSelfScoreTraits"+ n +"' name='txtSelfScoreTraits" + n + "'  onkeypress='return NumericWithDot(this,event)' style='width:100%;text-align:left;' rows=9; maxlength=3  disabled=true onblur='return isNumberKey(this.value)'  >"+Math.abs(SelfScoreTraits).toFixed(2)+"</textarea>";
                         tab += "<td style='width:7%;text-align:left' >"+ txtSelfScoreTraits +"</td>";
                      
                    }
                    else
                    {
                   

                        var txtSelfScoreTraits ="<textarea class='NormalText' id='txtSelfScoreTraits"+ n +"' name='txtSelfScoreTraits" + n + "'  value='0' style='width:100%;text-align:left;'  rows=9; disabled=true onkeypress='return NumericWithDot(this,event)' maxlength=3   onblur='return isNumberKey(this.value)'  ></textarea>";
                        tab += "<td style='width:7%;text-align:left' >"+ txtSelfScoreTraits +"</td>";          
    
                    }
                    if(col[7]!="")
                    {
               
                          SupervisorScoreTraits = col[7];
                     
                         var txtSupervisorScoreTraits ="<textarea class='NormalText' id='txtSupervisorScoreTraits"+ n +"' name='txtSupervisorScoreTraits" + n + "' "+ DisFlag + " onkeypress='return NumericWithDot(this,event)' rows=9; style='width:100%;text-align:left;' maxlength=3   onblur='return isNumberKey(this.value)' onchange='updateValueTraits("+n+");CalculateScoreTraits()' >"+Math.abs(SupervisorScoreTraits).toFixed(2)+"</textarea>";
                         tab += "<td style='width:7%;text-align:left' >"+ txtSupervisorScoreTraits +"</td>";
  
                      
                    }
                    else
                    {
                   

                      var txtSupervisorScoreTraits ="<textarea class='NormalText' id='txtSupervisorScoreTraits"+ n +"' name='txtSupervisorScoreTraits" + n + "'  "+ DisFlag + " value='0' style='width:100%;text-align:left;'  rows=9; onkeypress='return NumericWithDot(this,event)' maxlength=3   onblur='return isNumberKey(this.value)' onchange='updateValueTraits("+n+");CalculateScoreTraits()' ></textarea>";
                        tab += "<td style='width:7%;text-align:left' >"+ txtSupervisorScoreTraits +"</td>";          
    
                    }
                   
                    tab += "<td style='width:3%;text-align:center' ></td>";
                    tab += "</tr>";
                    }
                    var self="";
                    var Avg="";
                    var Supervisor="";

                     document.getElementById("<%= hdnTraits1.ClientID %>").value=Row[7];
                   
                     var row1 = document.getElementById("<%= hdnTraits1.ClientID %>").value.split("¥");
                 
                     col = row1[1].split("µ");
                
                     var AvgTraits=""
                   
                    tab += "<tr height=20px; class='tblQal'>";
                    tab += "<td colspan=6 style='width:81%;text-align:center;font-weight:bold;'>Average Score</td>";
                    if(col[1]!="")
                    {

                        AvgTraits   = col[1];
                        var txtAvgTraits = "<input id='txtAvgTraits' name='txtAvgTraits' value='" + Math.abs(AvgTraits).toFixed(2) + "' type='Text'  style='width:99%;' onblur='return isNumberKey(this.value)' class='NormalText' maxlength='3'  disabled=true  />";
                        tab += "<td style='width:8%;text-align:left' >"+ txtAvgTraits +"</td>";
                       
                     }
                     else
                     {
                   
                            var txtAvgTraits = "<input id='txtAvgTraits' name='txtAvgTraits' type='Text'  value='0' style='width:99%;' class='NormalText'  onblur='return isNumberKey(this.value)' maxlength='3' disabled=true  />";
                            tab += "<td style='width:8%;text-align:left' >"+ txtAvgTraits +"</td>";
                       
                     }

                     if(col[2]!="")
                    {

                        SupervisorAvgTraits   = col[2];
                        var txtSupervisorAvgTraits = "<input id='txtSupervisorAvgTraits' name='txtSupervisorAvgTraits' value='" + Math.abs(SupervisorAvgTraits).toFixed(2) + "' type='Text'  style='width:99%;' onblur='return isNumberKey(this.value)' value='0' class='NormalText' maxlength='3'  disabled=true  />";
                        tab += "<td style='width:8%;text-align:left' >"+ txtSupervisorAvgTraits +"</td>";
                       
                      }
                      else
                      {
                   
                            var txtSupervisorAvgTraits = "<input id='txtSupervisorAvgTraits' name='txtSupervisorAvgTraits' type='Text'  value='0' style='width:99%;' class='NormalText' "+ DisFlag + " onblur='return isNumberKey(this.value)' maxlength='10' disabled=true  />";
                            tab += "<td style='width:8%;text-align:left' >"+ txtSupervisorAvgTraits +"</td>";
                       
                      }
                   
                    tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "</tr>"; 
                         

                tab += "</table></div></div></div>"; 
                document.getElementById("<%= pnTraits.ClientID %>").innerHTML = tab;
               
                
                }
                else
                document.getElementById("<%= pnTraits.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
              
               
        }

        function updateValueTraits(n)
        {  
       
          
            row3 = document.getElementById("<%= hdnTraits.ClientID %>").value.split("¥");
          
             var newStrTraits=""
             var SuperScoreTraits=""
              for (n = 1; n <= row3.length - 1; n++) 
                {
                        var col = row3[n].split("µ");
                   
                        var SuperScoreTraits = document.getElementById("txtSupervisorScoreTraits"+ n.toString()).value;
                       
                        newStrTraits+="¥" +col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3]  + "µ" + col[4] + "µ" + col[5] + "µ" +col[6]+ "µ" + SuperScoreTraits ;  
                     
               
                }

                
            document.getElementById("<%= hdnTraits.ClientID %>").value=newStrTraits;
         
        }

       function table_AppraiseeCommentsfill(Arg) 
       {
            

              var Row=Arg.split("Ø");
            

            document.getElementById("<%= hdnAppraiseeCommitments.ClientID %>").value=Row[8];
        
           if (document.getElementById("<%= hdnAppraiseeCommitments.ClientID %>").value != "") 
           {
            document.getElementById("<%= pnAppraiseeCommitments.ClientID %>").style.display = '';
          
            var tab = "";
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            
            row = document.getElementById("<%= hdnAppraiseeCommitments.ClientID %>").value.split("¥");
           
           
          
                 col = row[1].split("µ");
                  tab += "<tr height=20px;  class='tblQal'>";
               
                var AppraiseeCommitments=""
                  if(col[1]!="")
                  {

                        AppraiseeCommitments   = col[1];
                         var txtAppraiseeCommitments ="<textarea class='NormalText' id='txtAppraiseeCommitments' name='txtAppraiseeCommitments'  maxlength='200' disabled=true style='width:100%;text-align:left;' rows=2;>"+AppraiseeCommitments+"</textarea>";
                         tab += "<td style='width:16%;text-align:left' >"+ txtAppraiseeCommitments +"</td>";
                    
               }
                  else
                  {
                   var txtAppraiseeCommitments ="<textarea class='NormalText' id='txtAppraiseeCommitments' name='txtAppraiseeCommitments'  maxlength='200' style='width:100%;text-align:left;' "+ DisFlag + " rows=2;></textarea>";
                         tab += "<td style='width:16%;text-align:left' >"+ txtAppraiseeCommitments +"</td>";
                 }
                    
                  tab += "</tr>";

            tab += "</table></div>";
            document.getElementById("<%= pnAppraiseeCommitments.ClientID %>").innerHTML = tab;
             }
                else
                document.getElementById("<%= pnAppraiseeCommitments.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
              
               
            


        }
        function table_RO_DisciplinaryActionsfill(Arg) 
       {
            
              var Row=Arg.split("Ø");
               var RoleID=document.getElementById("<%= hdnRoleID.ClientID %>").value;
               var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if((StatusID==1) && (RoleID==1))
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
           
            document.getElementById("<%= hdnDisciplinary.ClientID %>").value=Row[11];
        
           if (document.getElementById("<%= hdnDisciplinary.ClientID %>").value != "") 
           {
            document.getElementById("<%= pnDisciplinary.ClientID %>").style.display = '';
          
            var tab = "";
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            
            row = document.getElementById("<%= hdnDisciplinary.ClientID %>").value.split("¥");
           
           
          
                 col = row[1].split("µ");
                 
                  tab += "<tr height=20px;  class='tblQal'>";
               
                var DisciplinaryActions=""
                  if(col[1]!="")
                  {

                        DisciplinaryActions   = col[1];
                         var txtDisciplinaryActions ="<textarea class='NormalText' id='txtDisciplinaryActions' name='txtDisciplinaryActions'  maxlength='200' "+ DisFlag + " style='width:100%;text-align:left;' rows=2;>"+DisciplinaryActions+"</textarea>";
                         tab += "<td style='width:16%;text-align:left' >"+ txtDisciplinaryActions +"</td>";
                    
                                          
                  }
                  else
                  {
                   var txtDisciplinaryActions ="<textarea class='NormalText' id='txtDisciplinaryActions' name='txtDisciplinaryActions'  maxlength='200' "+ DisFlag + " style='width:100%;text-align:left;' rows=2;></textarea>";
                         tab += "<td style='width:16%;text-align:left' >"+ txtDisciplinaryActions +"</td>";
                      
                  }
                    
                  tab += "</tr>";

            tab += "</table></div>";
            document.getElementById("<%= pnDisciplinary.ClientID %>").innerHTML = tab;
             }
                else
                document.getElementById("<%= pnDisciplinary.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//


        }

         function table_RO_PerformEffortsfill() 
        {
        var dis;
        
        var RoleID=document.getElementById("<%= hdnRoleID.ClientID %>").value;
               var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if((StatusID==1) && (RoleID==1))
                {
                    DisFlag = '';
                    dis= 'visibility:visible;';
                            
                }
                else
                {
                    DisFlag = 'disabled';
                    dis= 'visibility:hidden;';

                }
         if (document.getElementById("<%= hdnEfforts.ClientID %>").value != "") 
         {
            document.getElementById("<%= pnEfforts.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>";
            tab += "<td style='width:30%;text-align:center'><b>Improve in his / her current job</b></td>"; 
            tab += "<td style='width:30%;text-align:center'><b>For fast tracking to next level of career </b></td>";   
            tab += "<td style='width:30%;text-align:center'><b>Suggested Training</b></td>";
            tab +="<td style='width:5%;text-align:left'></td>";
            tab += "</tr>";     
            row = document.getElementById("<%= hdnEfforts.ClientID %>").value.split("¥");
            
            for (n = 1; n <= row.length - 1; n++) 
            {
                 col = row[n].split("µ");
                
                 if (row_bg == 0) 
                 {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  else 
                  {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";  
                 
                        var JobImprove="";
                        var CareerLevel="";
                        var SuggestedTraining="";
                        var selfScore;
                        var SuperScore;
                   
                    if(col[0]!="")
                    {
                  
                       JobImprove = col[0];
                       var txtJobImprove = "<input id='txtJobImprove" + n + "' name='txtJobImprove" + n + "' value='" + JobImprove + "'    type='Text'  'maxlength='200'   "+ DisFlag + " style='width:99%;' onchange='updateValueEfforts("+ n + ")' class='NormalText' />";
                       tab += "<td style='width:30%;text-align:left' >"+ txtJobImprove +"</td>";
                    }
                    else
                    {

                    var txtJobImprove = "<input id='txtJobImprove" + n + "' name='txtJobImprove" + n + "' type='Text' style='width:99%;'  class='NormalText'  'maxlength='200'   "+ DisFlag + "  onchange='updateValueEfforts("+n+")'     />";
                    tab += "<td style='width:30%;text-align:left' >"+ txtJobImprove +"</td>";
                       

                    }
                    if(col[1]!="")
                    {
               
                      CareerLevel = col[1];
                       var txtCareerLevel = "<input id='txtCareerLevel" + n + "' name='txtCareerLevel" + n + "' value='" + CareerLevel + "'  type='Text'  "+ DisFlag + "   TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='200'  class='NormalText' onchange='updateValueEfforts("+n+")' />";
                       tab += "<td style='width:30%;text-align:left' >"+ txtCareerLevel +"</td>";
                      
                    }
                    else
                    {

                     var txtCareerLevel = "<input id='txtCareerLevel" + n + "' name='txtCareerLevel" + n + "' type='Text' style='width:99%;'  class='NormalText' "+ DisFlag + "  maxlength='200'    onchange='updateValueEfforts("+n+")'     />";
                     tab += "<td style='width:30%;text-align:left' >"+ txtCareerLevel +"</td>";
                     

                    }
                    if(col[2]!="")
                    {
                  
                      SuggestedTraining = col[2];
                      var txtSuggestedTraining = "<input id='txtSuggestedTraining" + n + "' name='txtSuggestedTraining" + n + "'  value='" + SuggestedTraining + "' type='Text'   "+ DisFlag + "  style='width:99%;' class='NormalText' maxlength='200' onchange='updateValueEfforts("+n+")'    />"; 
                      tab += "<td style='width:30%;text-align:left' >"+ txtSuggestedTraining +"</td>";
                    }
                    else
                    {

                     var txtSuggestedTraining = "<input id='txtSuggestedTraining" + n + "' name='txtSuggestedTraining" + n + "' type='Text' style='width:99%;'  class='NormalText' "+ DisFlag + "  maxlength='200'    onchange='updateValueEfforts("+n+")'     />";
                     tab += "<td style='width:30%;text-align:left' >"+ txtSuggestedTraining +"</td>";
                     
                    }
               
                    if(col[0] == "" && col[1] == "" && col[2] == "" ) 
                    {
                    
                   
                     tab += "<td style='width:5%;text-align:center' onclick=DeleteRowEfforts('" + n + "')><img  src='../../Image/cross.png'; style='align:middle;cursor:pointer; "+dis+"  height:15px;  width:15px ;'  title='Delete'/></td>";
                    
                    }
                    else
                    {
                        
                         
                          tab += "<td style='width:5%;text-align:center' onclick=DeleteRowEfforts('" + n + "')><img id='img1' src='../../Image/cross.png'; style='align:middle;cursor:pointer; "+dis+"  height:15px; width:15px ;'  title='Delete'/></td>";
                     
                    }
                    tab += "</tr>";
                    }
                   
                tab += "</table></div></div></div>"; 
                document.getElementById("<%= pnEfforts.ClientID %>").innerHTML = tab;
                
                }
                else
                document.getElementById("<%= pnEfforts.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
                 
               
        }

       function table_HOD_Commentsfill() 
        {
        
        RoleID=document.getElementById("<%= hdnRoleID.ClientID %>").value;
          
               var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if((StatusID==2) && (RoleID==2))
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
          
         if (document.getElementById("<%= hdnHODComments.ClientID %>").value != "") 
         {
            document.getElementById("<%= pnHODComments.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:25%;text-align:center'><b></b></td>"; 
            tab += "<td style='width:60%;text-align:center'><b>Comments & Remarks </b></td>";   
            tab += "<td style='width:10%;text-align:center'><b>Score</b></td>";
            tab +="<td style='width:5%;text-align:left'></td>";
            tab += "</tr>";    
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
   
            row = document.getElementById("<%= hdnHODComments.ClientID %>").value.split("¥");
             col = row[1].split("µ");
            
                  tab += "<tr height=20px;  class='tblQal'>";
                  var PerformanceHODComments="";
                  var PerformanceHODScore="";
                 
                  tab += "<td style='width:25%;text-align:center;'> Performance Related </td>"; 
                   if(col[1]!="")
                    {
               

                         PerformanceHODComments = col[1];
                         var txtPerformanceHODComments ="<textarea class='NormalText' id='txtPerformanceHODComments' name='txtPerformanceHODComments'  maxlength='200'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;>"+PerformanceHODComments+"</textarea>";
                         tab += "<td style='width:60%;text-align:left' >"+ txtPerformanceHODComments +"</td>";
                    
                      
                    }
                    else
                    {

                          var txtPerformanceHODComments ="<textarea class='NormalText' id='txtPerformanceHODComments' name='txtPerformanceHODComments'  maxlength='200'  "+ DisFlag + " style='width:100%;text-align:left;' rows=1;></textarea>";
                         tab += "<td style='width:60%;text-align:left' >"+ txtPerformanceHODComments +"</td>";

                    }
                    if(col[2]!="")
                    {
                  

                        PerformanceHODScore = col[2];
                         var txtPerformanceHODScore ="<textarea class='NormalText' id='txtPerformanceHODScore' name='txtPerformanceHODScore'  maxlength='3' onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)'  onchange='CalculateAvgScoreHOD()'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;>"+ Math.abs(PerformanceHODScore).toFixed(2)  + "</textarea>";
                         tab += "<td style='width:10%;text-align:left' >"+ txtPerformanceHODScore +"</td>";
                    

                         
                          
                    }
                    else
                    {


                         var txtPerformanceHODScore ="<textarea class='NormalText' id='txtPerformanceHODScore' name='txtPerformanceHODScore'  maxlength='3' onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)'  onchange='CalculateAvgScoreHOD()'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;></textarea>";
                         tab += "<td style='width:10%;text-align:left' >"+ txtPerformanceHODScore +"</td>";
                    
                     
                    }
               
                    if(col[1] == "" && col[2] == "" ) 
                    {
                    
                 
                        tab +="<td style='width:5%;text-align:left'></td>";
                        
                    }
                    else
                    {
                        tab +="<td style='width:5%;text-align:left'></td>";

                    }
                    tab += "</tr>";
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td style='width:25%;text-align:center;'> Personality Related </td>"; 
                    if(col[3]!="")
                    {
               


                        PersonalityHODComments = col[3];
                         var txtPersonalityHODComments ="<textarea class='NormalText' id='txtPersonalityHODComments' name='txtPersonalityHODComments'  maxlength='200'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;>"+ PersonalityHODComments + "</textarea>";
                         tab += "<td style='width:60%;text-align:left' >"+ txtPersonalityHODComments +"</td>";
                    


                         
                    }
                    else
                    {

                         var txtPersonalityHODComments ="<textarea class='NormalText' id='txtPersonalityHODComments' name='txtPersonalityHODComments'  maxlength='200'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;></textarea>";
                         tab += "<td style='width:60%;text-align:left' >"+ txtPersonalityHODComments +"</td>";
                    

                    }
                    if(col[4]!="")
                    {
                  

                         PersonalityHODScore = col[4];
                         var txtPersonalityHODScore ="<textarea class='NormalText' id='txtPersonalityHODScore' name='txtPersonalityHODScore'  maxlength='3' onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)'  onchange='CalculateAvgScoreHOD()'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;>"+ Math.abs(PersonalityHODScore).toFixed(2)  + "</textarea>";
                         tab += "<td style='width:10%;text-align:left' >"+ txtPersonalityHODScore +"</td>";
                      
                  
                   }
                    else
                    {

                        var txtPersonalityHODScore ="<textarea class='NormalText' id='txtPersonalityHODScore' name='txtPersonalityHODScore'  maxlength='3' onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)'  onchange='CalculateAvgScoreHOD()'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;></textarea>";
                         tab += "<td style='width:10%;text-align:left' >"+ txtPersonalityHODScore +"</td>";
                      
                    }
                    if(col[3] == "" && col[4] == "" ) 
                    {
                    
                 
                        tab += "<td style='width:5%;text-align:center' ></td>";
                        
                    }
                    else
                    {
                        tab += "<td style='width:5%;text-align:center' ></td>";

                    }
                    tab += "</tr>";
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td colspan=2 style='width:65%;text-align:center;'> Average Score </td>"; 
                    if(col[5]!="")
                    {
               
                           ReviewingAvg = col[5];
                           var txtReviewingAvg = "<input id='txtReviewingAvg' name='txtReviewingAvg' value='" + Math.abs(ReviewingAvg).toFixed(2) + "'  type='Text'    onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)' disabled TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='3'  class='NormalText'  />";
                           tab += "<td style='width:10%;text-align:left' >"+ txtReviewingAvg +"</td>";
                      
                    }
                    else
                    {

                         var txtReviewingAvg = "<input id='txtReviewingAvg' name='txtReviewingAvg' type='Text' style='width:99%;' value='0' class='NormalText' disabled  maxlength='3' onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)'     />";
                         tab += "<td style='width:10%;text-align:left' >"+ txtReviewingAvg +"</td>";

                    }
                    tab += "<td style='width:5%;text-align:center' ></td>";     
                    tab += "</tr>";
                    tab += "</table></div></div></div>"; 
                    document.getElementById("<%= pnHODComments.ClientID %>").innerHTML = tab;
               
                
                }
                else
                document.getElementById("<%= pnHODComments.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//    
               
        }
         
         function table_EVP_Commentsfill() 
        {
        
           
             RoleID=document.getElementById("<%= hdnRoleID.ClientID %>").value;
          
               var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if((StatusID==3) && (RoleID==3))
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
          
             if (document.getElementById("<%= hdnEVPComments.ClientID %>").value != "") 
             {
                document.getElementById("<%= pnEVPComments.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr height=20px;  class='tblQal'>";      
                tab += "<td style='width:25%;text-align:center'><b></b></td>"; 
                tab += "<td style='width:60%;text-align:center'><b>Comments & Remarks </b></td>";   
                tab += "<td style='width:10%;text-align:center'><b>Score</b></td>";
                tab +="<td style='width:5%;text-align:left'></td>";
                tab += "</tr>";    
                tab += "</table></div>";  
                tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
                tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
   
                row = document.getElementById("<%= hdnEVPComments.ClientID %>").value.split("¥");
                 col = row[1].split("µ");
            
                  tab += "<tr height=20px;  class='tblQal'>";
                  var PerformanceEVPComments="";
                  var PerformanceEVPScore="";
                 
                  tab += "<td style='width:25%;text-align:center;'> Performance Related </td>"; 
                   if(col[1]!="")
                    {        
               
                         PerformanceEVPComments = col[1];
                         var txtPerformanceEVPComments ="<textarea class='NormalText' id='txtPerformanceEVPComments' name='txtPerformanceEVPComments'  maxlength='200'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;>"+PerformanceEVPComments+"</textarea>";
                         tab += "<td style='width:60%;text-align:left' >"+ txtPerformanceEVPComments +"</td>";
                    }
                    else
                    {

                        var txtPerformanceEVPComments ="<textarea class='NormalText' id='txtPerformanceEVPComments' name='txtPerformanceEVPComments'  maxlength='200'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;></textarea>";
                         tab += "<td style='width:60%;text-align:left' >"+ txtPerformanceEVPComments +"</td>";
                    
                   }
                    if(col[2]!="")
                    {
                  

                         PerformanceEVPScore = col[2];
                         var txtPerformanceEVPScore ="<textarea class='NormalText' id='txtPerformanceEVPScore' name='txtPerformanceEVPScore' maxlength='3' onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)' onchange='CalculateAvgScoreEVP()'   style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;>"+ Math.abs(PerformanceEVPScore).toFixed(2) +"</textarea>";
                         tab += "<td style='width:10%;text-align:left' >"+ txtPerformanceEVPScore +"</td>";

                    }
                    else
                    {

                        var txtPerformanceEVPScore ="<textarea class='NormalText' id='txtPerformanceEVPScore' name='txtPerformanceEVPScore' maxlength='3' onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)' onchange='CalculateAvgScoreEVP()'   style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;></textarea>";
                         tab += "<td style='width:10%;text-align:left' >"+ txtPerformanceEVPScore +"</td>";

                    }
               
                    
                    tab += "<td style='width:5%;text-align:center' ></td>";
                    tab += "</tr>";
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td style='width:25%;text-align:center;'> Personality Related </td>"; 
                    if(col[3]!="")
                    {
               
                        PersonalityEVPComments = col[3];
                         var txtPersonalityEVPComments ="<textarea class='NormalText' id='txtPersonalityEVPComments' name='txtPersonalityEVPComments'  maxlength='200'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;>"+ PersonalityEVPComments +"</textarea>";
                         tab += "<td style='width:60%;text-align:left' >"+ txtPersonalityEVPComments +"</td>";


  
                    }
                    else
                    {

                     var txtPersonalityEVPComments ="<textarea class='NormalText' id='txtPersonalityEVPComments' name='txtPersonalityEVPComments'  maxlength='200'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;></textarea>";
                     tab += "<td style='width:60%;text-align:left' >"+ txtPersonalityEVPComments +"</td>";


                    }
                    if(col[4]!="")
                    {
                  
                      PersonalityEVPScore = col[4];
                         var txtPersonalityEVPScore ="<textarea class='NormalText' id='txtPersonalityEVPScore' name='txtPersonalityEVPScore' maxlength='3' onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)' onchange='CalculateAvgScoreEVP()'   style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;>"+ Math.abs(PersonalityEVPScore).toFixed(2) +"</textarea>";
                         tab += "<td style='width:10%;text-align:left' >"+ txtPersonalityEVPScore +"</td>";



                       }
                    else
                    {
                        var txtPersonalityEVPScore ="<textarea class='NormalText' id='txtPersonalityEVPScore' name='txtPersonalityEVPScore' maxlength='3' onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)' onchange='CalculateAvgScoreEVP()'   style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;></textarea>";
                         tab += "<td style='width:10%;text-align:left' >"+ txtPersonalityEVPScore +"</td>";
 
                    }
                   
                       tab += "<td style='width:5%;text-align:center' ></td>";

                    
                    tab += "</tr>";
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td colspan=2 style='width:65%;text-align:center;'> Average Score </td>"; 
                    if(col[5]!="")
                    {
               


                      EVPAvg = col[5];
                       var txtEVPAvg = "<input id='txtEVPAvg' name='txtEVPAvg' value='" + Math.abs(EVPAvg).toFixed(2) + "'  type='Text'  disabled  onblur='return isNumberKey(this.value)'   onkeypress='return NumericWithDot(this,event)'  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='3'  class='NormalText'  />";
                       tab += "<td style='width:10%;text-align:left' >"+ txtEVPAvg +"</td>";
                      
                    }
                    else
                    {

                     var txtEVPAvg = "<input id='txtEVPAvg' name='txtEVPAvg' type='Text' style='width:99%;'  class='NormalText' disabled  maxlength='3' onblur='return isNumberKey(this.value)'  value='0' onkeypress='return NumericWithDot(this,event)'      />";
                     tab += "<td style='width:10%;text-align:left' >"+ txtEVPAvg +"</td>";
                     

                    }
                   
                    
                 
                    tab += "<td style='width:5%;text-align:center' ></td>";
                     tab += "</tr>";
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td  style='width:25%;text-align:center;'> Recommendation </td>"; 
                    if(col[6]!="")
                    {
               

                        EVPRecommend = col[6];
                         var txtEVPRecommend ="<textarea class='NormalText' id='txtEVPRecommend' name='txtEVPRecommend'  maxlength='200'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;>"+ EVPRecommend +"</textarea>";
                         tab += "<td colspan=2 style='width:70%;text-align:left' >"+ txtEVPRecommend +"</td>";

                    
                      }
                    else
                    {

                        var txtEVPRecommend ="<textarea class='NormalText' id='txtEVPRecommend' name='txtEVPRecommend'  maxlength='200'  style='width:100%;text-align:left;'  "+ DisFlag + " rows=1;></textarea>";
                         tab += "<td colspan=2 style='width:70%;text-align:left' >"+ txtEVPRecommend +"</td>";
 

                    }
                   
                    
                 
                    tab += "<td style='width:5%;text-align:center' ></td>";    
                   
                    tab += "</tr>";
                    tab += "</table></div></div></div>"; 
                    document.getElementById("<%= pnEVPComments.ClientID %>").innerHTML = tab;
               
                
                }
                else
                document.getElementById("<%= pnEVPComments.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//    
               
        }           
         
         function table_Weightage_Score_fill() 
        {
        
            RoleID=document.getElementById("<%= hdnRoleID.ClientID %>").value;
             
          
          
            if(RoleID==4)
            {
              
            
                DisFlag = 'disabled';
                       

            }
              if (document.getElementById("<%= hdnWeightage.ClientID %>").value != "") 
             {
                    document.getElementById("<%= pnWeightage.ClientID %>").style.display = '';
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div id='ScrollDiv' style='width:50%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
                    tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>"; 
                    row = document.getElementById("<%= hdnWeightage.ClientID %>").value.split("¥");
                    col = row[1].split("µ");      
                    tab += "<tr height=20px;  class='tblQal'>";

                    tab += "<td style='width:50%;text-align:center;'> Self(20%) </td>"; 
          
                    var txtselfWeight = "<input id='txtselfWeight' name='txtselfWeight' value='" + Math.abs(col[1]).toFixed(2) + "'  type='Text'  "+ DisFlag + "   TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='200'  class='NormalText' onchange='updateValueEfforts("+n+")' />";
                    tab += "<td style='width:50%;text-align:left' >"+ txtselfWeight +"</td>";
                    tab += "</tr>";
                    tab += "<tr height=20px;  class='tblQal'>";

                    tab += "<td style='width:50%;text-align:center;'> Reporting Officer(40%) </td>"; 
         
                    var txtReportingOfficerWeight = "<input id='txtReportingOfficerWeight' name='txtReportingOfficerWeight' value='" + Math.abs(col[2]).toFixed(2) + "'  type='Text'  "+ DisFlag + "   TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='200'  class='NormalText' onchange='updateValueEfforts("+n+")' />";
                    tab += "<td style='width:50%;text-align:left' >"+ txtReportingOfficerWeight +"</td>";
                    tab += "</tr>"; 
                     tab += "<tr height=20px;  class='tblQal'>";

                    tab += "<td style='width:50%;text-align:center;'> HOD(25%) </td>"; 
          
                    var txtHODWeight = "<input id='txtHODWeight' name='txtHODWeight' value='" + Math.abs(col[3]).toFixed(2) + "'  type='Text'  "+ DisFlag + "   TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='200'  class='NormalText' onchange='updateValueEfforts("+n+")' />";
                    tab += "<td style='width:50%;text-align:left' >"+txtHODWeight +"</td>";
                    tab += "</tr>";    
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td style='width:50%;text-align:center;'> Functional Head/EVP(15%) </td>"; 
                    var txtFNHeadWeight = "<input id='txtFNHeadWeight' name='txtFNHeadWeight' value='" + Math.abs(col[4]).toFixed(2) + "'  type='Text'  "+ DisFlag + "   TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='200'  class='NormalText' onchange='updateValueEfforts("+n+")' />";
                    tab += "<td style='width:50%;text-align:left' >"+ txtFNHeadWeight +"</td>";
                    tab += "</tr>"; 
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td style='width:50%;text-align:center;'> Total Score(100%) </td>"; 
                    var txtTotalWeight = "<input id='txtTotalWeight' name='txtTotalWeight' value='" + Math.abs(col[5]).toFixed(2) + "'  type='Text'  "+ DisFlag + "   TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='200'  class='NormalText' onchange='updateValueEfforts("+n+")' />";
                    tab += "<td style='width:50%;text-align:left' >"+ txtTotalWeight +"</td>";
                    tab += "</tr>";          
                    tab += "</table></div>"; 
                    document.getElementById("<%= pnWeightage.ClientID %>").innerHTML = tab;
               
                
        }
        else
        document.getElementById("<%= pnWeightage.ClientID %>").style.display = 'none';

        }
         
        function table_MDRemarksfill(Arg) 
       {
            RoleID=document.getElementById("<%= hdnRoleID.ClientID %>").value;
          
               var StatusID = document.getElementById("<%= hdnstatus.ClientID %>").value;
               if((StatusID==5) && (RoleID==5))
                {
                DisFlag = '';
                        
                }
                else
                {
                    DisFlag = 'disabled';
                       

                }
            
              var Row=Arg.split("Ø");
            

            document.getElementById("<%= hdnMD.ClientID %>").value=Row[16];
        
           if (document.getElementById("<%= hdnMD.ClientID %>").value != "") 
           {
            document.getElementById("<%= pnMD.ClientID %>").style.display = '';
          
            var tab = "";
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            
            row = document.getElementById("<%= hdnMD.ClientID %>").value.split("¥");
           
           
          
                 col = row[1].split("µ");
                  tab += "<tr height=20px;  class='tblQal'>";
               
                var MDRemarks=""
                  if(col[1]!="")
                  {

                        MDRemarks   = col[1];
                         var txtMDRemarks ="<textarea class='NormalText' id='txtMDRemarks' name='txtMDRemarks'  maxlength='200'  style='width:100%;text-align:left;' rows=2;>"+MDRemarks+"</textarea>";
                         tab += "<td style='width:16%;text-align:left' >"+ txtMDRemarks +"</td>";
                    
                                          
                  }
                  else
                  {
                   var txtMDRemarks ="<textarea class='NormalText' id='txtMDRemarks' name='txtMDRemarks'  maxlength='200' style='width:100%;text-align:left;' "+ DisFlag + " rows=2;></textarea>";
                         tab += "<td style='width:16%;text-align:left' >"+ txtMDRemarks +"</td>";
                                     
                  }
                    
                  tab += "</tr>";

            tab += "</table></div>";
            document.getElementById("<%= pnMD.ClientID %>").innerHTML = tab;
             }
                else
                document.getElementById("<%= pnMD.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//

        }

         function CalculateTotal()
       {

                var Data=document.getElementById("<%= hdnDutiesDtl.ClientID %>").value;
               
                var SuperTotal=0;
                var SubTotal=0;
                var SelfTotal=0;
                var row = Data.split("¥");
                for (n = 1; n <= row.length-1 ; n++) 
                {
                    var col = row[n].split("µ");
                  SubTotal=Math.abs(SubTotal)+(Math.abs(col[1]));
                  
                 SuperTotal=(Math.abs(SuperTotal)+((Math.abs(col[1])/100)*Math.abs(col[5]))).toFixed(2);
                 
                 SelfTotal=(Math.abs(SelfTotal)+((Math.abs(col[1])/100)*Math.abs(col[4]))).toFixed(2);
                 
                }
                  document.getElementById("txtPerentage").value=SubTotal;
                  document.getElementById("txtAvgDuties").value=SelfTotal;
                  document.getElementById("txtAvgSuperDuties").value=SuperTotal;
                if(SubTotal!=100) 
                {
                    alert("Total Weightage should be equal to 100");
                    document.getElementById("txtPerentage").focus();
                    return false;
                  
                }
                
                 
       }
         
         function CalculateScoreBeli()
       {

                var SupervisorScoreBeli = 0;
                var SupervisorScoreBelis=0;
                 var count=0;
                  for (n = 1; n <= 13 ; n++) 
                    {
                       SupervisorScoreBeli += Math.abs(document.getElementById("txtSupervisorScoreBeli"+n).value); 
                       
                     if(Math.abs(document.getElementById("txtSupervisorScoreBeli"+n).value)!="")
                      {

                        var count=count+1;
                        
                    
                      }
          
                    }

                SupervisorScoreBelis=Math.abs(SupervisorScoreBeli/Math.abs(count)).toFixed(2);
               
             
                document.getElementById("txtSupervisorAvgBeli").value = SupervisorScoreBelis;
                
         }
                
       function CalculateScoreTraits()
       {
                var SupervisorScoreTrait=0;
                var SupervisorScoreTraits=0;
                        for (n = 1; n <= 10 ; n++) 
                         {
                               SupervisorScoreTrait += Math.abs(document.getElementById("txtSupervisorScoreTraits"+n).value); 
                         
                          }

                        SupervisorScoreTraits=SupervisorScoreTrait/10;
                        document.getElementById("txtSupervisorAvgTraits").value = SupervisorScoreTraits;
       }
       
       function CalculateAvgScoreHOD()
       {
                var HODPerformanceScore=0;
                var HODPersonalityScore=0;
                var HODAvgScore=0;
                var HODAvg=0;
                HODAvgScore=Math.abs(document.getElementById("txtPerformanceHODScore").value)+Math.abs(document.getElementById("txtPersonalityHODScore").value);
               
                        HODAvg=HODAvgScore/2;
                      
                        document.getElementById("txtReviewingAvg").value = HODAvg;
       }
       function CalculateAvgScoreEVP()
       {
                var EVPPerformanceScore=0;
                var EVPPersonalityScore=0;
                var EVPAvgScore=0;
                var EVPAvg=0;
                EVPAvgScore=Math.abs(document.getElementById("txtPerformanceEVPScore").value)+Math.abs(document.getElementById("txtPersonalityEVPScore").value);
               
                        EVPAvg=EVPAvgScore/2;
                      
                        document.getElementById("txtEVPAvg").value = EVPAvg;
       }

        function AddNewRow() {          
                      
                if (document.getElementById("<%= hdnDutiesDtl.ClientID %>").value != "") {      
                    row = document.getElementById("<%= hdnDutiesDtl.ClientID %>").value.split("¥");
                    var Len = row.length - 1; 
                    col = row[Len].split("µ");   
                    if (col[0] != "" && col[1] != "" &&  col[2] != "" && col[3] != "" && col[4] != "") {
                        document.getElementById("<%= hdnDutiesDtl.ClientID %>").value += "¥µµµµ";      
                        }
                    }
                   else    
                   {  
                    document.getElementById("<%= hdnDutiesDtl.ClientID %>").value = "¥µµµµ";
                
                    }
                table_fill();
            }

                function AddNewRowEfforts() {          
                      
                if (document.getElementById("<%= hdnEfforts.ClientID %>").value != "")
                 {      
                    rowEfforts = document.getElementById("<%= hdnEfforts.ClientID %>").value.split("¥");
                 
                    var Len = rowEfforts.length - 1; 
                    col = rowEfforts[Len].split("µ");   
                    if (col[0] != "" || col[1] != "" ||  col[2] != "" ) {
                        document.getElementById("<%= hdnEfforts.ClientID %>").value += "¥µµ";      
                        }
                    }
                   else    
                   {  
                    document.getElementById("<%= hdnEfforts.ClientID %>").value = "¥µµ";
                
                    }
                table_RO_PerformEffortsfill();
            }


        function setFocus() {
            document.getElementById("txtDuities"+n).Focus();
        }

       function DeleteRow(id)
        {
                row = document.getElementById("<%= hdnDutiesDtl.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                    if(id!=n)                  
                        NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnDutiesDtl.ClientID %>").value=NewStr;
                if(document.getElementById("<%= hdnDutiesDtl.ClientID %>").value=="")
                document.getElementById("<%= hdnDutiesDtl.ClientID %>").value= "¥µµµµµ";
                table_fill();
        }
        
        function DeleteRowEfforts(id)
        {
                row = document.getElementById("<%= hdnEfforts.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                    if(id!=n)                  
                        NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnEfforts.ClientID %>").value=NewStr;
                if(document.getElementById("<%= hdnEfforts.ClientID %>").value=="")
                document.getElementById("<%= hdnEfforts.ClientID %>").value= "¥µµ";
                table_RO_PerformEffortsfill();
        }
       function CreateNewRow(e, val) {
      
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValue(val);
                AddNewRow();                          
            }
         }

         function CreateNewRow(e, val) {
      
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValueEfforts(val);
                AddNewRowEfforts();                          
            }
         }

        function updateValue(id)
        {      
     
            row = document.getElementById("<%= hdnDutiesDtl.ClientID %>").value.split("¥");
           
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id==n)
                   {
                        var Duities=document.getElementById("txtDuities"+n).value;                           
                        var Weightage=document.getElementById("txtWeightage"+n).value;                   
                        var KeyPerformance=document.getElementById("txtKeyPerformance"+n).value;            
                        var Status=document.getElementById("txtStatus"+n).value;                                      
                        var SelfScore=document.getElementById("txtSelfScore"+n).value;                    
                        var Superscore=document.getElementById("txtSupervisorDutiesScore"+n).value;
                        NewStr+="¥"+Duities+"µ"+Weightage+"µ"+KeyPerformance+"µ"+Status+"µ"+SelfScore+"µ"+Superscore ;
                          
                          
                   }
                   else
                   {
                        NewStr+="¥"+row[n];
                   }
                }
                document.getElementById("<%= hdnDutiesDtl.ClientID %>").value=NewStr;
               
              
        }

        function updateValueEfforts(id)
        {      
     
            var rowEfforts = document.getElementById("<%= hdnEfforts.ClientID %>").value.split("¥");
         
                var NewStrEfforts=""
                for (n = 1; n <= rowEfforts.length-1 ; n++) {
                   if((id==n) ||(id==-1))
                   {
                        var JobImprove=document.getElementById("txtJobImprove"+n).value;                           
                        var CareerLevel=document.getElementById("txtCareerLevel"+n).value;                   
                        var SuggestedTraining=document.getElementById("txtSuggestedTraining"+n).value;            
                        NewStrEfforts+="¥"+JobImprove+"µ"+CareerLevel+"µ"+SuggestedTraining; 
                   }
                   else
                   {
                        NewStrEfforts+="¥"+row[n];
                   }
                }
                document.getElementById("<%= hdnEfforts.ClientID %>").value=NewStrEfforts;
              
        }

        
         function btnExit_onclick() {
             window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
         }
</script>
    <script language="javascript" type="text/javascript">
     
    </script>
    <br />
    <table align="center" 
        style="width:80%; margin: 0px auto;" >
           <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>EMPLOYEE DETAILS</strong></td>
           
        </tr>
        <tr>
            <td class="style2" style="width:12% ;">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            </td>
            <td class="style1">
                &nbsp;</td>
            <td style="width:15% ;">
                &nbsp;</td>
            <td style="width:15% ;">
                &nbsp;</td>
        </tr>
      
        <tr>
            <td style="text-align:left;width:12% ;" class="style2">
                Employee Code&nbsp;&nbsp; </td>
            <td class="style1">
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="ReadOnlyTextBox"  MaxLength="6" onkeypress="return NumericCheck(event)"></asp:TextBox>
            </td>
            <td style="width:20% ;text-align:left;">
                Name&nbsp;&nbsp;</td>
            <td style="width:15% ;">
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpName" runat="server" Width="90%" 
                    class="ReadOnlyTextBox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align:left; width:12% ;" class="style2">
                Date Of Join&nbsp;&nbsp;</td>
            <td class="style1">
                &nbsp;&nbsp;<asp:TextBox 
                    ID="txtDoj" runat="server" Width="30%" 
                    class="ReadOnlyTextBox" Enabled="false"></asp:TextBox>
                
            </td>
            <td style="width:20% ;text-align:left;">
                Designation during the time of joining &nbsp;</td>
            <td style="width:15% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDesignationDoj" 
                    runat="server"  Width="91%" class="ReadOnlyTextBox" Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:12% ;" class="style2" >
                Current Designation&nbsp;</td>
            <td class="style1">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDesignation" runat="server" 
                    class="ReadOnlyTextBox" Width="50%" Enabled="false" >
                </asp:DropDownList>
            </td>
            <td style="width:20% ;text-align:left;">
                Designation during the last review &nbsp;</td>
            <td style="width:15% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDesignationreview" runat="server" 
                    class="ReadOnlyTextBox" Width="91%" Enabled="false" >
                  <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
           <tr>
            <td style="text-align:left;width:12% ;" class="style2">
                Department&nbsp;</td>
            <td class="style1">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDepartment" runat="server" 
                    class="ReadOnlyTextBox" Width="50%" Enabled="false" >
                </asp:DropDownList>
            </td>
            <td style="width:20% ;text-align:left;">
                Location/Branch&nbsp;</td>
            <td style="width:15% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbLocationBranch" runat="server" 
                    class="ReadOnlyTextBox" Width="91%" Enabled="false" >
                  <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
           </tr>
        <tr>
            <td style="text-align:left;" class="style2">
                Reporting To&nbsp;&nbsp;</td>
            <td class="style1">
                &nbsp;&nbsp;<asp:DropDownList 
                    ID="cmbReportingTo" runat="server" class="ReadOnlyTextBox" Width="50%" Enabled="false">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="width:20% ;text-align:left;">
                Reported By &nbsp; (No of Team members)
                &nbsp;</td>
            <td style="width:32% ; text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtReportedBy" runat="server" Width="50%" 
                    class="ReadOnlyTextBox" ReadOnly="true" ></asp:TextBox>
              </td>
        </tr>
          <tr>
            <td style="text-align:left;" class="style2">
                Reviewing Head&nbsp;&nbsp;</td>
            <td class="style1">
                &nbsp;&nbsp;<asp:DropDownList 
                    ID="cmbReviewingHead" runat="server" class="ReadOnlyTextBox" Width="50%" Enabled="false">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
              </td>
            <td style="width:20% ;text-align:left;">
                Functional Head&nbsp;&nbsp;</td>
            <td style="width:32% ; text-align:left;">&nbsp;&nbsp;<asp:DropDownList 
                    ID="cmbFunctionalHead" runat="server" class="ReadOnlyTextBox" Width="50%" Enabled="false">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
              </td>
        </tr>
        
        <tr>
            <td style="text-align:left;" class="style2">
                Period of Evaluation&nbsp;&nbsp;</td>
            <td class="style1">&nbsp;&nbsp;<asp:TextBox 
                    ID="txtPeriod" runat="server" Width="90%" 
                    class="ReadOnlyTextBox" Enabled="false" ></asp:TextBox>
              </td>

            <td style="width:20% ;text-align:left;">
                Date of Evaluation&nbsp;&nbsp;</td>
           <td style="width:32% ;text-align:left;">&nbsp;&nbsp;<asp:TextBox ID="txtEvaluation" 
                    runat="server" class="NormalText" Width="30%" ReadOnly="true" Enabled="false"></asp:TextBox> 
                <asp:CalendarExtender ID="txtEvaluation_CalendarExtender" runat="server" 
                    TargetControlID="txtEvaluation" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;" class="style2">
                Remarks&nbsp;&nbsp;</td>
            <td class="style1">&nbsp;&nbsp;<asp:TextBox ID="txtRemarks" class="ReadOnlyTextBox" 
                    runat="server" Width="91%" Enabled="false"
                    MaxLength="40" TextMode="MultiLine"></asp:TextBox>
              </td>
        </tr>
        
          
      
          <tr>
            <td style="text-align:left;" class="style2">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td style="width:12% ;text-align:left;">
                &nbsp;</td>
            <td style="width:32% ; text-align:left;">
                &nbsp;</td>
        </tr>
         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>A. DUTIES & RESPONSIBILITIES - PERFORMANCE INDICATORS</strong> 
                &nbsp;&nbsp;<b style="color:red;">*</b></td>
           
        </tr> 
         <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnDuties" runat="server">
                </asp:Panel>
               </td>
              
        </tr>
         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>B. ESAF - BELIEFS </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnBeliefsDtl" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
          <tr>
            <td style="width:12% ;text-align:left;" colspan="4" class="mainhead" >
                <strong>List out your three significant contributions to the company (Explain Objectively) </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnSignificant" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
          <tr>
            <td style="width:12% ;text-align:left;" colspan="4" class="mainhead" >
                <strong>State the areas of improvement for self , so as to contribute better</strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnImprovementArea" runat="server">
                </asp:Panel>
               </td>
              

          </tr>
          <tr>
            <td style="width:12% ;text-align:left;" colspan="4" class="mainhead" >
                <strong>Individual Development Plan - so as to improve your skills/competency</strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnDevelopmentPlan" runat="server">
                </asp:Panel>
               </td>
         </tr>
         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>C. TRAITS AND BEHAVIOURS </strong> 
                &nbsp;&nbsp;<b style="color:red;">*</b></td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnTraits" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>D. COMMITMENTS / REMARKS BY THE APPRAISEE </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnAppraiseeCommitments" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
        <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>E. GENERAL COMMENTS BY REPORTING OFFICER </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
         <tr>
            <td style="width:12% ;text-align:left;" colspan="4" class="mainhead" >
                <strong>Disciplinary actions/ Adverse remarks if any </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnDisciplinary" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
         <tr>
            <td style="width:12% ;text-align:left;" colspan="3" class="mainhead" >
                <strong>Efforts He / She has to perform </strong> <b style="color:red;">*</b></td>
                <td style="width:12% ;text-align:right;"  class="mainhead" >
                <strong><font size="1">Add New Row</font></strong>
                <img src="../../Image/Addd1.gif" style="height:18px; width:18px; float:center; z-index:1; cursor: pointer;  padding-right:10px;" onclick="AddNewRowEfforts()" title="Add New" /> 
                &nbsp;&nbsp;</td>
           
        </tr> 
            <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnEfforts" runat="server">
                </asp:Panel>
               </td>
              

        </tr>

         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>F. COMMENTS & REMARKS OF THE REVIEWING OFFICER / HOD </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
         <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnHODComments" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>G. COMMENTS & REMARKS OF THE FUNCTIONAL HEAD / EVP </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
         <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnEVPComments" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>H. WEIGHTAGE SCORE ( Derived out of all recommendation) </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
         <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnWeightage" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
            <tr>
            <td style="text-align:left;width:12% ;" class="style2">
                HR officer&nbsp;&nbsp; </td>
            <td class="style1">
                &nbsp;&nbsp;<asp:TextBox ID="txtHRName" runat="server" Width="30%" class="ReadOnlyTextBox"  MaxLength="6" onkeypress="return NumericCheck(event)"></asp:TextBox>
            </td>
            <td style="width:20% ;text-align:left;">
                Emp. Code&nbsp;&nbsp;</td>
            <td style="width:15% ;">
                &nbsp;&nbsp;<asp:TextBox ID="txtHREmpCode" runat="server"  Width="90%" 
                    class="ReadOnlyTextBox"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>I. APPROVAL / REMARKS BY MD & CEO /EVP CS </strong> 
                &nbsp;&nbsp;</td>
           
        </tr> 
         <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <asp:Panel ID="pnMD" runat="server">
                </asp:Panel>
               </td>
              

        </tr>
           <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
            <input id="btnUndo" type="button" value="ASSIGN BACK" 
                    onclick="return btnUndo_onclick()" 
                    style="font-family: cambria; width: 11%; cursor: pointer;" />&nbsp;
                <input id="btnSave" type="button" value="SAVE" 
                    onclick="return btnSave_onclick()" 
                    style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;<input id="btnExit" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />
                     <input id="btnFinish" type="button" value="FINISH" 
                    onclick="return btnFinish_onclick()" 
                    style="font-family: cambria; width: 6%; cursor: pointer;" />
                    
                    <asp:HiddenField 
                    ID="hdnDuties" runat="server" />
                <asp:HiddenField 
                    ID="hdnBeliefsDtl" runat="server" />
                    <asp:HiddenField 
                    ID="hdnBeliefsDtl1" runat="server" />
                <asp:HiddenField 
                    ID="hdnSignificant" runat="server" />
               
                <asp:HiddenField 
                    ID="hdnImprovementArea" runat="server" />
                <asp:HiddenField 
                    ID="hdnDevelopmentPlan" runat="server" />
                <asp:HiddenField 
                    ID="hdnTraits" runat="server" />
                     <asp:HiddenField 
                    ID="hdnTraits1" runat="server" />
                <asp:HiddenField 
                    ID="hdnAppraiseeCommitments" runat="server" />
                <asp:HiddenField 
                    ID="hdnExperience" runat="server" />
                <asp:HiddenField 
                    ID="hdnDutiesDtl" runat="server" />
            <asp:HiddenField 
                    ID="hdnDutiesDtl1" runat="server" />
                 <asp:HiddenField ID="hdnDisciplinary" runat="server" />
                 <asp:HiddenField ID="hdnEfforts" runat="server" />
                 <asp:HiddenField ID="hdnRoleID" runat="server" />
                 <asp:HiddenField ID="hdnPeriod" runat="server" />
                 <asp:HiddenField ID="hdnHODComments" runat="server" />
                 <asp:HiddenField ID="hdnEVPComments" runat="server" />
 <asp:HiddenField ID="hdnWeightage" runat="server" />
 <asp:HiddenField ID="hdnMD" runat="server" />
 <asp:HiddenField ID="hdnEmp_ID" runat="server" />
 <asp:HiddenField ID="hdnstatus" runat="server" />
  <asp:HiddenField ID="hidassinback" runat="server" />

               </td>
        </tr>
    </table></br>
</asp:Content>

