﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Web.Services
Imports System.Web.UI.WebControls
Imports System.Web.Script.Serialization
Imports System.Web
Imports System.Web.UI
Partial Class Appraisal
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim DT5 As New DataTable
    Dim DB As New MS_SQL.Connect
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 398) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Me.Master.subtitle = "Employee Performance Evaluation"
           
            If Not IsPostBack Then
                GN.ComboFill(cmbLocationBranch, EN.GetBranches(), 0, 1)
                GN.ComboFill(cmbDesignationDoj, EN.GetDesignation(), 0, 1)
                GN.ComboFill(cmbDesignation, EN.GetDesignation(), 0, 1)
                GN.ComboFill(cmbDesignationreview, EN.GetDesignation(), 0, 1)
                GN.ComboFill(cmbReportingTo, EN.GetReportingTo(), 0, 1)
                GN.ComboFill(cmbReviewingHead, EN.GetReportingTo(), 0, 1)
                GN.ComboFill(cmbFunctionalHead, EN.GetReportingTo(), 0, 1)
                GN.ComboFill(cmbReviewingHead, EN.GetReportingTo(), 0, 1)
                GN.ComboFill(cmbReportingTo, EN.GetReportingTo(), 0, 1)
                GN.ComboFill(cmbDepartment, EN.GetDepartment(), 0, 1)
                'DT = DB.ExecuteDataSet("SELECT TOP 1 period_id, Period FROM Appraisal_Period_Master ORDER BY period_id DESC").Tables(0)
                'hdnPeriod.Value = DT.Rows(0)(0).ToString()


                DT = DB.ExecuteDataSet("SELECT TOP 1 period_id, Period FROM Appraisal_Period_Master ORDER BY period_id DESC").Tables(0)
                txtPeriod.Text = DT.Rows(0)(1).ToString()
                hdnPeriod.Value = DT.Rows(0)(0).ToString()

                DT5 = DB.ExecuteDataSet("select count(*) from Appraisal_Heads_Master WHERE Appraisee_id = " & Session("UserID").ToString() & "  and period_id=" & hdnPeriod.Value & " ").Tables(0)
                If CInt(DT5.Rows(0)(0)) <= 0 Then
                    Dim cl_script0 As New System.Text.StringBuilder
                    cl_script0.Append("         alert('You are Not in List for Appraisal');window.open('../../Home.aspx','_self');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
                    Exit Sub
                End If

                '--//---------- Script Registrations -----------//--
                '/--- For Call Back ---//




                DT2 = DB.ExecuteDataSet("select count(*) from emp_master where reporting_to=" + Session("USerID").ToString()).Tables(0)
                txtReportedBy.Text = DT2.Rows(0)(0).ToString()

                DT3 = DB.ExecuteDataSet("select  count(Period_id) as  Period_id from  Appraisal_Period_Master APM where period_id in (select Period_id from Appraisal_Employee_Master AEM  where  emp_code=" + Session("USerID").ToString() + " ) ").Tables(0)
                If CInt(DT3.Rows(0)(0)) >= 0 Then
                    DT4 = DB.ExecuteDataSet("select Status from Appraisal_Heads_Master  where APPRAISEE_ID  =   " + Session("UserID").ToString() + " and period_id= " + hdnPeriod.Value + " ").Tables(0)
                    hdnstatus.Value = CInt(DT4.Rows(0)(0))


                End If

            End If

            hdnDutiesDtl.Value = "¥µµµµµ"
            hdnBeliefsDtl.Value = "¥µµµ"

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- Register Client Side Functions ---//

            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "EmpCodeOnChange(" + Session("UserID") + ");", True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "EmpCodeOnChange(" + Session("UserID") + "," + hdnstatus.Value + "," + hdnPeriod.Value + ");", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        DT1.Dispose()
        DT2.Dispose()
        DT3.Dispose()
        DT4.Dispose()
        DT5.Dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Try
            Dim Data() As String = eventArgument.Split(CChar("Ø"))
            If Data(0) = 1 Then
                Dim DesignationID As Integer = 0
                Dim DesignationDojID As Integer = 0
                Dim DesignationreviewID As Integer = 0
                If Data(3) <> "" Then
                    DesignationID = CInt(Data(3))
                End If
                If Data(2) <> "" Then
                    DesignationDojID = CInt(Data(2))
                End If
                If Data(4) <> "" Then
                    DesignationreviewID = CInt(Data(4))
                End If
                Dim EmpCode As Integer = CInt(Data(1))
                Dim DepartmentID As Integer = CInt(Data(5))
                Dim BranchID As Integer = CInt(Data(6))
                Dim ReportingTo As Integer = CInt(Data(7))
                Dim ReportedBy As Integer = CInt(Data(8))
                Dim ReviewHeadID As Integer = CInt(Data(9))
                Dim FunctionalHeadID As Integer = CInt(Data(10))
                Dim PeriodID As Integer = CInt(Data(11))

                Dim Evaluation As String = CStr(Data(12))
                Dim Remarks As String = CStr(Data(13))
                Dim DutiesDtl As String = Data(14).ToString()
                If DutiesDtl <> "" Then
                    DutiesDtl = DutiesDtl.Substring(1)
                End If
                Dim WeightedAvg As Double = CDbl(Data(15))
                Dim SelfRemarks As String = Data(16).ToString()
                Dim BeliefDtl As String = Data(17).ToString()
                If BeliefDtl <> "" Then
                    BeliefDtl = BeliefDtl.Substring(1)
                End If
                Dim BeliefAvg As Double = CDbl(Data(18))
                Dim BeliefselfRemarks As String = Data(19).ToString()
                Dim Significant1 As String = Data(20).ToString()
                Dim Significant2 As String = Data(21).ToString()
                Dim Significant3 As String = Data(22).ToString()
                Dim Improve1 As String = Data(23).ToString()
                Dim Improve2 As String = Data(24).ToString()
                Dim Improve3 As String = Data(25).ToString()
                Dim plan1 As String = Data(26).ToString()
                Dim plan2 As String = Data(27).ToString()
                Dim plan3 As String = Data(28).ToString()
                Dim TraitsDtl As String = Data(29).ToString()
                If TraitsDtl <> "" Then
                    TraitsDtl = TraitsDtl.Substring(1)
                End If
                Dim TraitsAvg As Double = CDbl(Data(30))
                Dim AppraiseeCommitments As String = Data(31).ToString()
                Dim SuperviorAvgDuties As Double = CDbl(Data(32))
                Dim SupervisorRemarksDuties As String = Data(33).ToString()
                Dim SupervisorAvgBeli As Double = CDbl(Data(34))
                Dim SupervisorRemarksBeli As String = Data(35).ToString()
                Dim SupervisorAvgTraits As Double = CDbl(Data(36))
                Dim RoleID As Integer = CInt(Data(37))
                Dim DisciplinaryActions As String = Data(38).ToString()
                Dim EffortDtl As String = Data(39).ToString()
                If EffortDtl <> "" Then
                    EffortDtl = EffortDtl.Substring(1)
                End If
                Dim HODPerformanceRemarks As String = Data(40).ToString()
                Dim HODPerformanceScore As Double = CDbl(Data(41))
                Dim HODPersonalityRemarks As String = Data(42).ToString()
                Dim HODPersonalityScore As Double = CDbl(Data(43))
                Dim HODAvgScore As Double = CDbl(Data(44))
                Dim EVPPerformanceRemarks As String = Data(45).ToString()
                Dim EVPPerformanceScore As Double = CDbl(Data(46))
                Dim EVPPersonalityRemarks As String = Data(47).ToString()
                Dim EVPPersonalityScore As Double = CDbl(Data(48))
                Dim EVPAvgScore As Double = CDbl(Data(49))
                Dim EVPRecommend As String = Data(50).ToString()
                Dim HREmp_code As Integer = CInt(Data(51))
                Dim MDRemarks As String = Data(52).ToString()
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(53) As SqlParameter
                    Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(0).Value = EmpCode
                    Params(1) = New SqlParameter("@DesignationDojID", SqlDbType.Int)
                    Params(1).Value = DesignationDojID
                    Params(2) = New SqlParameter("@DesignationID", SqlDbType.Int)
                    Params(2).Value = DesignationID
                    Params(3) = New SqlParameter("@DesignationreviewID", SqlDbType.Int)
                    Params(3).Value = DesignationreviewID
                    Params(4) = New SqlParameter("@DepartmentID", SqlDbType.Int)
                    Params(4).Value = DepartmentID
                    Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@BranchID", SqlDbType.Int)
                    Params(7).Value = BranchID
                    Params(8) = New SqlParameter("@ReportingTo", SqlDbType.Int)
                    Params(8).Value = ReportingTo
                    Params(9) = New SqlParameter("@ReportedBy", SqlDbType.Int)
                    Params(9).Value = ReportedBy
                    Params(10) = New SqlParameter("@ReviewHeadID", SqlDbType.Int)
                    Params(10).Value = ReviewHeadID
                    Params(11) = New SqlParameter("@FunctionalHeadID", SqlDbType.Int)
                    Params(11).Value = FunctionalHeadID
                    Params(12) = New SqlParameter("@PeriodID", SqlDbType.Int)
                    Params(12).Value = PeriodID
                    Params(13) = New SqlParameter("@Evaluation", SqlDbType.VarChar, 20)
                    Params(13).Value = Evaluation
                    Params(14) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                    Params(14).Value = Remarks
                    Params(15) = New SqlParameter("@DutiesDtl", SqlDbType.VarChar, -1)
                    Params(15).Value = DutiesDtl
                    Params(16) = New SqlParameter("@WeightedAvg", SqlDbType.Money)
                    Params(16).Value = WeightedAvg
                    Params(17) = New SqlParameter("@SelfRemarks", SqlDbType.VarChar, 500)
                    Params(17).Value = SelfRemarks
                    Params(18) = New SqlParameter("@BeliefDtl", SqlDbType.VarChar, -1)
                    Params(18).Value = BeliefDtl
                    Params(19) = New SqlParameter("@BeliefAvg", SqlDbType.Money)
                    Params(19).Value = BeliefAvg
                    Params(20) = New SqlParameter("@BeliefselfRemarks", SqlDbType.VarChar, 500)
                    Params(20).Value = BeliefselfRemarks
                    Params(21) = New SqlParameter("@Significant1", SqlDbType.VarChar, 500)
                    Params(21).Value = Significant1
                    Params(22) = New SqlParameter("@Significant2", SqlDbType.VarChar, 500)
                    Params(22).Value = Significant2
                    Params(23) = New SqlParameter("@Significant3", SqlDbType.VarChar, 500)
                    Params(23).Value = Significant3
                    Params(24) = New SqlParameter("@Improve1", SqlDbType.VarChar, 500)
                    Params(24).Value = Improve1
                    Params(25) = New SqlParameter("@Improve2", SqlDbType.VarChar, 500)
                    Params(25).Value = Improve2
                    Params(26) = New SqlParameter("@Improve3", SqlDbType.VarChar, 500)
                    Params(26).Value = Improve3
                    Params(27) = New SqlParameter("@plan1", SqlDbType.VarChar, 500)
                    Params(27).Value = plan1
                    Params(28) = New SqlParameter("@plan2", SqlDbType.VarChar, 500)
                    Params(28).Value = plan2
                    Params(29) = New SqlParameter("@plan3", SqlDbType.VarChar, 500)
                    Params(29).Value = plan3
                    Params(30) = New SqlParameter("@TraitsDtl", SqlDbType.VarChar, -1)
                    Params(30).Value = TraitsDtl
                    Params(31) = New SqlParameter("@TraitsAvg", SqlDbType.Money)
                    Params(31).Value = TraitsAvg
                    Params(32) = New SqlParameter("@AppraiseeCommitments", SqlDbType.VarChar, -1)
                    Params(32).Value = AppraiseeCommitments
                    Params(33) = New SqlParameter("@SuperviorAvgDuties", SqlDbType.Money)
                    Params(33).Value = SuperviorAvgDuties
                    Params(34) = New SqlParameter("@SupervisorRemarksDuties", SqlDbType.VarChar, 500)
                    Params(34).Value = SupervisorRemarksDuties
                    Params(35) = New SqlParameter("@SupervisorAvgBeli", SqlDbType.Money)
                    Params(35).Value = SupervisorAvgBeli
                    Params(36) = New SqlParameter("@SupervisorRemarksBeli", SqlDbType.VarChar, 500)
                    Params(36).Value = SupervisorRemarksBeli
                    Params(37) = New SqlParameter("@SupervisorAvgTraits", SqlDbType.Money)
                    Params(37).Value = SupervisorAvgTraits
                    Params(38) = New SqlParameter("@RoleID", SqlDbType.Int)
                    Params(38).Value = RoleID
                    Params(39) = New SqlParameter("@DisciplinaryActions", SqlDbType.VarChar, 500)
                    Params(39).Value = DisciplinaryActions
                    Params(40) = New SqlParameter("@EffortsDtl", SqlDbType.VarChar, -1)
                    Params(40).Value = EffortDtl
                    Params(41) = New SqlParameter("@HODPerformanceRemarks", SqlDbType.VarChar, 500)
                    Params(41).Value = HODPerformanceRemarks
                    Params(42) = New SqlParameter("@HODPerformanceScore", SqlDbType.Money)
                    Params(42).Value = HODPerformanceScore
                    Params(43) = New SqlParameter("@HODPersonalityRemarks", SqlDbType.VarChar, 500)
                    Params(43).Value = HODPersonalityRemarks
                    Params(44) = New SqlParameter("@HODPersonalityScore", SqlDbType.Money)
                    Params(44).Value = HODPersonalityScore
                    Params(45) = New SqlParameter("@HODAvgScore", SqlDbType.Money)
                    Params(45).Value = HODAvgScore
                    Params(46) = New SqlParameter("@EVPPerformanceRemarks", SqlDbType.VarChar, 500)
                    Params(46).Value = EVPPerformanceRemarks
                    Params(47) = New SqlParameter("@EVPPerformanceScore", SqlDbType.Money)
                    Params(47).Value = EVPPerformanceScore
                    Params(48) = New SqlParameter("@EVPPersonalityRemarks", SqlDbType.VarChar, 500)
                    Params(48).Value = EVPPersonalityRemarks
                    Params(49) = New SqlParameter("@EVPPersonalityScore", SqlDbType.Money)
                    Params(49).Value = EVPPersonalityScore
                    Params(50) = New SqlParameter("@EVPAvgScore", SqlDbType.Money)
                    Params(50).Value = EVPAvgScore
                    Params(51) = New SqlParameter("@EVPRecommend", SqlDbType.VarChar, 200)
                    Params(51).Value = EVPRecommend
                    Params(52) = New SqlParameter("@HREmp_code", SqlDbType.Int)
                    Params(52).Value = HREmp_code
                    Params(53) = New SqlParameter("@MDRemarks", SqlDbType.VarChar, 500)
                    Params(53).Value = MDRemarks

                    DB.ExecuteNonQuery("SP_APPRAISAL_EMPLOYEE_DETAILS", Params)
                    ErrorFlag = CInt(Params(5).Value)
                    Message = CStr(Params(6).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "Ø" + Message
            ElseIf CInt(Data(0)) = 2 Then

                'DT = DB.ExecuteDataSet("select AEM.emp_code,em.emp_name,em.date_of_join,AEM.designation_id_doj,DM.DESIGNATION_NAME,AEM.designation_Id_last,DM1.DESIGNATION_NAME,DM.DESIGNATION_NAME,reported_by,AEM.period_id,CONCAT(APM.period_from, ' - ', APM.period_to) " & _
                '                       " AS PERIOD,Evaluation_Date,AEM.Reviewing_Head_id,EM1.EMP_NAME,AEM.Functional_head_id, EM2.EMP_NAME, Remarks, AEM.Dept_id, DM2.DEPARTMENT_NAME, AEM.Reporting_to, em3.EMP_NAME,AEM.BRANCH_ID,BM.BRANCH_NAME " & _
                '                      " from Appraisal_Employee_Master AEM INNER JOIN emp_master em on em.emp_code=AEM.emp_code INNER JOIN designation_master DM on AEM.designation_id_dOJ=DM.Designation_ID " & _
                '                      " INNER JOIN designation_master DM1 on AEM.designation_Id_last=DM1.Designation_ID INNER JOIN Appraisal_pERIOD_Master APM  ON AEM.PERIOD_ID=APM.PERIOD_ID INNER JOIN EMP_MASTER EM1 ON EM1.EMP_CODE=AEM.Reviewing_Head_id " & _
                '                      " INNER JOIN EMP_MASTER EM2 ON EM2.EMP_CODE=AEM.Functional_head_id INNER JOIN DEPARTMENT_MASTER DM2 ON DM2.DEPARTMENT_ID=AEM.DEPT_ID INNER JOIN EMP_MASTER em3 ON em3.EMP_CODE=AEM.Reporting_to INNER JOIN BRANCH_MASTER BM ON BM.BRANCH_ID= AEM.BRANCH_ID " & _
                '                      " where AEM.emp_code = " & CInt(Data(1)) & " And APM.Period_id = " & CInt(Data(2)) & " ").Tables(0)
                'For Each DR As DataRow In DT.Rows
                '    ' CallBackReturn += "Ø" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ" + DR(2).ToString() + "ÿ" + DR(3).ToString() + "ÿ" + DR(4).ToString() + "ÿ" + DR(5).ToString() + "ÿ" + DR(6).ToString() + "ÿ" + DR(7).ToString() + "ÿ" + DR(8).ToString() + "ÿ" + DR(9).ToString() + "ÿ" + DR(10).ToString() + "ÿ" + DR(11).ToString() + "ÿ" + DR(12).ToString() + "ÿ" + DR(13).ToString() + "ÿ" + DR(14).ToString() + "ÿ" + DR(15).ToString() + "ÿ" + DR(16).ToString() + "ÿ" + DR(17).ToString() + "ÿ" + DR(18).ToString() + "ÿ" + DR(19).ToString() + "ÿ" + DR(20).ToString()
                '    CallBackReturn = DT.Rows(0)(0).ToString() + "µ" + DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + DT.Rows(0)(7).ToString() + "µ" + DT.Rows(0)(8).ToString() + "µ" + DT.Rows(0)(9).ToString() + "µ" + DT.Rows(0)(10).ToString() + "µ" + DT.Rows(0)(11).ToString() + "µ" + DT.Rows(0)(12).ToString() + "µ" + DT.Rows(0)(13).ToString() + "µ" + DT.Rows(0)(14).ToString() + "µ" + DT.Rows(0)(15).ToString() + "µ" + DT.Rows(0)(16).ToString() + "µ" + DT.Rows(0)(17).ToString() + "µ" + DT.Rows(0)(18).ToString() + "µ" + DT.Rows(0)(19).ToString() + "µ" + DT.Rows(0)(20).ToString() + "µ" + DT.Rows(0)(21).ToString() + "µ" + DT.Rows(0)(22).ToString()
                '    CallBackReturn += "Ø"
                'Next
                'CallBackReturn += "Ø"

                Dim EmpCode = CInt(Data(1))
                Dim Period_ID = CInt(Data(2))


                Dim IntVal As Integer = 0
                DT = DB.ExecuteDataSet("select count(*) from Appraisal_Employee_Master where emp_code = " & CInt(Data(1)) & " and Period_id =" & CInt(Data(2)) & " ").Tables(0)
                Dim Message As String = Nothing
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                If DT.Rows(0)(0) = 0 Then

                    Message = "Please Enter Minimum Data"
                    cl_script1.Append("         alert('" + Message + "');")
                    cl_script1.Append("          window.open('Appraisal.aspx','_self');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
                Else
                    IntVal = DB.ExecuteNonQuery("update Appraisal_Heads_Master set status=1 where appraisee_id=" & CInt(Data(1)) & " and period_id=" & CInt(Data(2)) & "")


                End If
                CallBackReturn = CStr(IntVal)
            ElseIf CInt(Data(0)) = 3 Then
                Dim EmpCode = CInt(Data(1))
                Dim Period_ID = CInt(Data(2))
                DT2 = DB.ExecuteDataSet("select count(*) from appraisal_employee_master where emp_code= " & CInt(Data(1)) & " and period_id=" & CInt(Data(2)) & " ").Tables(0)
                If CInt(DT2.Rows(0)(0)) > 0 Then

                    DT = DB.ExecuteDataSet("select AEM.emp_code,em.emp_name,convert(varchar(10),em.date_of_join,105) as date_of_join,AEM.designation_id_doj,DM.DESIGNATION_NAME,AEM.designation_Id_current,DM1.DESIGNATION_NAME,DM3.DESIGNATION_NAME,reported_by,AEM.period_id,APM.period " & _
                                     " AS PERIOD,convert(varchar(10),Evaluation_Date,105) as Evaluation_Date,AEM.Reviewing_Head_id,EM1.EMP_NAME,AEM.Functional_head_id, EM2.EMP_NAME, Remarks, AEM.Dept_id, DM2.DEPARTMENT_NAME, AEM.Reporting_to, em3.EMP_NAME,AEM.BRANCH_ID,BM.BRANCH_NAME,AEM.designation_id_last " & _
                                    " from Appraisal_Employee_Master AEM INNER JOIN emp_master em on em.emp_code=AEM.emp_code INNER JOIN designation_master DM on AEM.designation_id_dOJ=DM.Designation_ID " & _
                                    " INNER JOIN designation_master DM1 on AEM.designation_Id_last=DM1.Designation_ID INNER JOIN Appraisal_pERIOD_Master APM  ON AEM.PERIOD_ID=APM.PERIOD_ID INNER JOIN EMP_MASTER EM1 ON EM1.EMP_CODE=AEM.Reviewing_Head_id " & _
                                    " INNER JOIN EMP_MASTER EM2 ON EM2.EMP_CODE=AEM.Functional_head_id INNER JOIN DEPARTMENT_MASTER DM2 ON DM2.DEPARTMENT_ID=AEM.DEPT_ID INNER JOIN EMP_MASTER em3 ON em3.EMP_CODE=AEM.Reporting_to INNER JOIN BRANCH_MASTER BM ON BM.BRANCH_ID= AEM.BRANCH_ID INNER JOIN designation_master DM3 ON AEM.designation_Id_last=DM3.Designation_ID  " & _
                                    " where AEM.emp_code = " & CInt(Data(1)) & " And APM.Period_id = " & CInt(Data(2)) & " ").Tables(0)
                    For Each DR As DataRow In DT.Rows
                        ' CallBackReturn += "Ø" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ" + DR(2).ToString() + "ÿ" + DR(3).ToString() + "ÿ" + DR(4).ToString() + "ÿ" + DR(5).ToString() + "ÿ" + DR(6).ToString() + "ÿ" + DR(7).ToString() + "ÿ" + DR(8).ToString() + "ÿ" + DR(9).ToString() + "ÿ" + DR(10).ToString() + "ÿ" + DR(11).ToString() + "ÿ" + DR(12).ToString() + "ÿ" + DR(13).ToString() + "ÿ" + DR(14).ToString() + "ÿ" + DR(15).ToString() + "ÿ" + DR(16).ToString() + "ÿ" + DR(17).ToString() + "ÿ" + DR(18).ToString() + "ÿ" + DR(19).ToString() + "ÿ" + DR(20).ToString()
                        CallBackReturn = DT.Rows(0)(0).ToString() + "µ" + DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + DT.Rows(0)(7).ToString() + "µ" + DT.Rows(0)(8).ToString() + "µ" + DT.Rows(0)(9).ToString() + "µ" + DT.Rows(0)(10).ToString() + "µ" + DT.Rows(0)(11).ToString() + "µ" + DT.Rows(0)(12).ToString() + "µ" + DT.Rows(0)(13).ToString() + "µ" + DT.Rows(0)(14).ToString() + "µ" + DT.Rows(0)(15).ToString() + "µ" + DT.Rows(0)(16).ToString() + "µ" + DT.Rows(0)(17).ToString() + "µ" + DT.Rows(0)(18).ToString() + "µ" + DT.Rows(0)(19).ToString() + "µ" + DT.Rows(0)(20).ToString() + "µ" + DT.Rows(0)(21).ToString() + "µ" + DT.Rows(0)(22).ToString() + "µ" + DT.Rows(0)(23).ToString()
                        CallBackReturn += "Ø"
                    Next
                Else
                    '  DT = DB.ExecuteDataSet("select em.emp_code,em.emp_Name,CONVERT(VARCHAR(10),em.date_of_join,105) as Date_of_join,em.DESIGNATION_ID,designation_name ,em.DESIGNATION_ID,designation_name, designation_name ,'' as reported_by,'' as period_id,'' as period,CONVERT(VARCHAR(10),GETDATE(),105) as evaluation_date,AHM.reviewing_head_id,cast(AHM.reviewing_head_id as varchar(10))+ '|'+ RA.Emp_name as review_head_name,AHM.functional_head_id,cast(AHM.functional_head_id as varchar(10))+ '|'+ RA1.Emp_name as Functional_head_name,'' as remarks,em.DEPARTMENT_ID,department_name,em.reporting_to,'' as reporting_name,em.BRANCH_ID,branch_name from Emp_List  em left join emp_master a on a.emp_code=em.emp_code left join  Appraisal_Heads_Master AHM on em.emp_code=AHM.Appraisee_id  left join emp_master RA on RA.emp_code=AHM.reviewing_head_id left join emp_master RA1 on RA1.emp_code=AHM.Functional_head_id where em.emp_code=" + Session("USerID").ToString()).Tables(0)
                    DT = DB.ExecuteDataSet("select em.emp_code,em.emp_Name,CONVERT(VARCHAR(10),em.date_of_join,105) as Date_of_join,-1,NULL,em.DESIGNATION_ID,designation_name,null,'' as reported_by,'' as period_id,'' as period,CONVERT(VARCHAR(10),GETDATE(),105) as evaluation_date,AHM.reviewing_head_id,cast(AHM.reviewing_head_id as varchar(10))+ '|'+ RA.Emp_name as review_head_name,AHM.functional_head_id,cast(AHM.functional_head_id as varchar(10))+ '|'+ RA1.Emp_name as Functional_head_name,'' as remarks,em.DEPARTMENT_ID,department_name,AHM.reporting_officer_id,cast(AHM.reporting_officer_id as varchar(10))+ '|'+ RA2.Emp_name as  reporting_name,em.BRANCH_ID,branch_name,-1 from Emp_List  em left join emp_master a on a.emp_code=em.emp_code left join  Appraisal_Heads_Master AHM on em.emp_code=AHM.Appraisee_id  left join emp_master RA on RA.emp_code=AHM.reviewing_head_id left join emp_master RA1 on RA1.emp_code=AHM.Functional_head_id  left join emp_master RA2 on RA2.emp_code=AHM.reporting_officer_id where em.emp_code=" + Session("USerID").ToString()).Tables(0)

                    If DT.Rows.Count > 0 Then
                        'CallBackReturn = DT.Rows(0)(0).ToString() + "µ" + DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + DT.Rows(0)(7).ToString()
                        CallBackReturn = DT.Rows(0)(0).ToString() + "µ" + DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + DT.Rows(0)(7).ToString() + "µ" + DT.Rows(0)(8).ToString() + "µ" + DT.Rows(0)(9).ToString() + "µ" + DT.Rows(0)(10).ToString() + "µ" + DT.Rows(0)(11).ToString() + "µ" + DT.Rows(0)(12).ToString() + "µ" + DT.Rows(0)(13).ToString() + "µ" + DT.Rows(0)(14).ToString() + "µ" + DT.Rows(0)(15).ToString() + "µ" + DT.Rows(0)(16).ToString() + "µ" + DT.Rows(0)(17).ToString() + "µ" + DT.Rows(0)(18).ToString() + "µ" + DT.Rows(0)(19).ToString() + "µ" + DT.Rows(0)(20).ToString() + "µ" + DT.Rows(0)(21).ToString() + "µ" + DT.Rows(0)(22).ToString() + "µ" + DT.Rows(0)(23).ToString()

                        CallBackReturn += "Ø"

                        'Dim BranchID As Array = Split((DT.Rows(0)(2)), "|")
                        'Dim DepID As Integer = CInt(DT.Rows(0)(3))
                        'Dim DesID As Integer = CInt(DT.Rows(0)(4))

                        'DT = DB.ExecuteDataSet("select DISTINCT BM.BRANCH_ID,BM.BRANCH_NAME from branch_master BM inner join emp_master EM on EM.branch_id=BM.BRANCH_ID WHERE BM.STATUS_ID=1").Tables(0)
                        'For Each DR As DataRow In DT.Rows
                        '    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                        'Next
                        'CallBackReturn += "Ø"


                        'DT = EN.GetReportingOfficer(BranchID(0), DepID, DesID, EmpCode)
                        'For Each DR As DataRow In DT.Rows
                        '    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()

                        'Next
                        'CallBackReturn += "Ø"

                        'DT = DB.ExecuteDataSet("select DISTINCT DM.DEPARTMENT_ID,DM.DEPARTMENT_NAME from DEPARTMENT_MASTER DM inner join emp_master EM on EM.DEPARTMENT_ID=DM.DEPARTMENT_ID WHERE DM.STATUS_ID=1").Tables(0)
                        'For Each DR As DataRow In DT.Rows
                        '    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                        'Next
                        'CallBackReturn += "Ø"
                        'DT = DB.ExecuteDataSet("select '-1' as DESIGNATION_ID,' ------Select------' as DESIGNATION_NAME Union All select DISTINCT DM.DESIGNATION_ID,DM.DESIGNATION_NAME from DESIGNATION_MASTER DM inner join emp_master EM on EM.DESIGNATION_ID=DM.DESIGNATION_ID WHERE DM.STATUS_ID=1 order by designation_name ").Tables(0)
                        'For Each DR As DataRow In DT.Rows
                        '    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                        'Next
                        'CallBackReturn += "Ø"

                        'DT = DB.ExecuteDataSet("select AHM.reviewing_head_id,cast(AHM.reviewing_head_id as varchar(10))+ '|'+ EM.Emp_name as gg from Appraisal_Heads_Master AHM inner join emp_master EM on em.emp_code=AHM.reviewing_head_id where appraisee_id=" + Session("USerID").ToString()).Tables(0)
                        'For Each DR As DataRow In DT.Rows
                        '    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                        'Next
                        'CallBackReturn += "Ø"
                        'DT = DB.ExecuteDataSet("select AHM.functional_head_id,cast(AHM.functional_head_id as varchar(10))+ '|'+ EM.Emp_name as gg from Appraisal_Heads_Master AHM inner join emp_master EM on em.emp_code=AHM.functional_head_id where appraisee_id=" + Session("USerID").ToString()).Tables(0)
                        'For Each DR As DataRow In DT.Rows
                        '    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                        'Next
                        'CallBackReturn += "Ø"
                        'Else
                        '    CallBackReturn = "ØØØØ"
                    End If
                End If

                '  DT = DB.ExecuteDataSet(" select A.parameters_id,A.category,A.parameters,A.measurement_points,B.ACTIVITIES,B.SELF_SCORE,B.SUPERVISOR_SCORE from Appraisal_ESAF_Beliefs_Parameter A left join ( select * from Appraisal_Esaf_Beliefs where emp_code =" + Session("USerID").ToString() + ") B on A.PARAMETERS_ID=B.PARAMETER_ID  ").Tables(0)
                DT = DB.ExecuteDataSet("select A.parameters_id,A.category,A.parameters,A.measurement_points,B.ACTIVITIES,B.SELF_SCORE,B.SUPERVISOR_SCORE from Appraisal_ESAF_Beliefs_Parameter A left join ( select * from Appraisal_Esaf_Beliefs where emp_code =" & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & ") B on A.PARAMETERS_ID=B.PARAMETER_ID   ").Tables(0)

                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString()

                Next
                CallBackReturn += "Ø"
                DT = DB.ExecuteDataSet("select beliefs_id,average_score,SUPERVISOR_AVERAGE_SCORE,self_remarks,SUPERVISOR_REMARKS from Appraisal_Esaf_Beliefs where emp_code =" & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & " ").Tables(0)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString()

                    Next
                Else
                    CallBackReturn += "¥µµµµ"
                End If
                'DT = DB.ExecuteDataSet("select beliefs_id,average_score,self_remarks from Appraisal_Esaf_Beliefs where emp_code =" & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & " ").Tables(0)
                'If DT.Rows.Count > 0 Then
                '    For Each DR As DataRow In DT.Rows
                '        CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString()

                '    Next
                'Else
                '    CallBackReturn += "¥µµ"
                'End If
                CallBackReturn += "Ø"
                DT = DB.ExecuteDataSet("select A.SIPC_ID,A.significant1,A.significant2,A.significant3 from Appraisal_Signi_Improve_Plan_Commitment A where A.emp_code =" & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()

                    Next
                Else
                    CallBackReturn += "¥µµµ"
                End If


                CallBackReturn += "Ø"
                DT = DB.ExecuteDataSet("select A.SIPC_ID,A.improve1,A.improve2,A.improve3 from Appraisal_Signi_Improve_Plan_Commitment A  where a.emp_code =" & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & " ").Tables(0)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()

                    Next
                Else
                    CallBackReturn += "¥µµµ"
                End If

                CallBackReturn += "Ø"

                DT = DB.ExecuteDataSet(" select SIPC_ID,plan1,plan2,plan3 from Appraisal_Signi_Improve_Plan_Commitment where emp_code =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
                    Next
                Else
                    CallBackReturn += "¥µµµ"
                End If
                CallBackReturn += "Ø"
                DT = DB.ExecuteDataSet(" select A.Factors_id,A.Factors,excellent,A.good,A.fair,A.poor,B.SELF_SCORE,B.SUPERVISOR_SCORE from Appraisal_Traits_Behaviour_Factors A left join ( select * from Appraisal_Traits_BehaviourS where emp_code = " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & " )B ON A.Factors_id= B.Factors_id ").Tables(0)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString() + "µ" + DR(7).ToString()

                Next
                CallBackReturn += "Ø"
                DT = DB.ExecuteDataSet(" select Traits_id,avg_score,SUPERVISOR_AVG_SCORE from Appraisal_Traits_BehaviourS where emp_code = " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & " ").Tables(0)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString()

                    Next
                Else
                    CallBackReturn += "¥µµ"
                End If
                ' '' DT = DB.ExecuteDataSet("select A.Factors_id,A.Factors,excellent,A.good,A.fair,A.poor,B.SELF_SCORE from Appraisal_Traits_Behaviour_Factors A left join Appraisal_Traits_BehaviourS B ON A.Factors_id= B.Factors_id where B.emp_code=" + Session("USerID").ToString() + " ").Tables(0)
                ''DT = DB.ExecuteDataSet(" select A.Factors_id,A.Factors,excellent,A.good,A.fair,A.poor,B.SELF_SCORE from Appraisal_Traits_Behaviour_Factors A left join ( select * from Appraisal_Traits_BehaviourS where emp_code = " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & " )B ON A.Factors_id= B.Factors_id ").Tables(0)
                ''For Each DR As DataRow In DT.Rows
                ''    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString()

                ''Next
                ''CallBackReturn += "Ø"
                ''DT = DB.ExecuteDataSet(" select Traits_id,avg_score from Appraisal_Traits_BehaviourS where emp_code = " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & " ").Tables(0)
                ''If DT.Rows.Count > 0 Then
                ''    For Each DR As DataRow In DT.Rows
                ''        CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString()

                ''    Next
                ''Else
                ''    CallBackReturn += "¥µ"
                ''End If
                CallBackReturn += "Ø"

                DT = DB.ExecuteDataSet(" select SIPC_ID,COMMITMENTS from Appraisal_Signi_Improve_Plan_Commitment where emp_code  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString()
                    Next
                Else
                    CallBackReturn += "¥µ"
                End If
                CallBackReturn += "Ø"

                ' DT = DB.ExecuteDataSet(" select DUTIES_RESPONSIBILITIES,WEIGHTAGE,KEY_PERFORMANCE,REVIEWING_STATUS,SELF_SCORE from Appraisal_Duties_Responsibilities_Master where emp_code  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
                DT = DB.ExecuteDataSet(" select DUTIES_RESPONSIBILITIES,WEIGHTAGE,KEY_PERFORMANCE,REVIEWING_STATUS,SELF_SCORE,SUPERVISOR_SCORE from Appraisal_Duties_Responsibilities_Master where emp_code  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)

                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString()
                    Next
                Else
                    'CallBackReturn += "¥µµµµ"
                    CallBackReturn += "¥µµµµµ¥µµµµµ¥µµµµµ¥µµµµµ¥µµµµµ"
                End If
                CallBackReturn += "Ø"

                '  DT = DB.ExecuteDataSet("select duties_id,WEIGHTED_AVG_SCORE,SELF_REMARKS from Appraisal_Duties_Responsibilities_Master where emp_code  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)
                DT = DB.ExecuteDataSet("select distinct WEIGHTED_AVG_SCORE,SUPER_WEIGHTED_AVG_SCORE,SELF_REMARKS,SUPERVISOR_REMARKS from Appraisal_Duties_Responsibilities_Master where emp_code  =  " & CInt(Data(1)) & " AND PERIOD_ID=" & CInt(Data(2)) & "  ").Tables(0)

                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
                    Next
                Else
                    CallBackReturn += "¥µµµ"
                End If
            End If
        Catch ex As Exception
         
            Response.Redirect("~/CatchException.aspx?ErrorNo=1")
        End Try

    End Sub

#End Region


End Class
