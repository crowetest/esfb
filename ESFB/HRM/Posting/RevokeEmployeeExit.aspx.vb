﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class Posting_RevokeEmployeeExit
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim CallBackReturn As String
    Dim DB As New MS_SQL.Connect
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Select Case CInt(Data(0))
            Case 1
                Dim EmpCode As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select count(*) from emp_exit where emp_code = " & EmpCode & "")
                If DT.Rows(0)(0) > 0 Then
                    DT = GF.GetQueryResult("Select a.emp_name,Branch_name,Department_name,Designation_name,CONVERT(VARCHAR(11),a.Date_Of_Join,106),d.Status,b.Reason,CONVERT(VARCHAR(11),b.Relieving_Date,106),CONVERT(VARCHAR(11),b.Submited_Date,106),c.Emp_Name,b.User_ID,b.exit_type_id  from Emp_List a,EMP_EXIT b,EMP_MASTER c,EMP_STATUS d where a.Emp_Code = b.Emp_Code and b.User_ID = c.Emp_Code and a.Emp_Code = " & EmpCode & " and b.Exit_Type_ID = d.Status_ID and a.Status_ID = b.Exit_Type_ID and a.Releiving_Date = b.Relieving_Date AND A.STATUS_ID <> 1")
                    CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString() + "Ø" + DT.Rows(0)(6).ToString() + "Ø" + DT.Rows(0)(7).ToString() + "Ø" + DT.Rows(0)(8).ToString() + "Ø" + DT.Rows(0)(9).ToString() + "Ø" + DT.Rows(0)(10).ToString() + "Ø" + DT.Rows(0)(11).ToString()
                    If DT.Rows.Count > 0 Then
                    Else
                        CallBackReturn = "ØØ"
                    End If
                Else
                    CallBackReturn = "ØØ"
                End If
            Case 2
                Dim EmpCode As Integer = CInt(Data(1))
                Dim Remarks As String = Data(2)
                Dim ExitTypeID As Integer = CInt(Data(3))
                Dim EffDt As Date = Nothing
                If (ExitTypeID = 2) Then
                    EffDt = CDate(Data(4))
                Else
                    EffDt = "01/01/1990"
                End If
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim ErrorFlag As Integer = 0
                Dim Message As String = ""
                Try
                    Dim Params(6) As SqlParameter
                    Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(0).Value = EmpCode
                    Params(1) = New SqlParameter("@Remarks", SqlDbType.VarChar, 100)
                    Params(1).Value = Remarks
                    Params(2) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
                    Params(2).Value = EffDt
                    Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(3).Value = UserID
                    Params(4) = New SqlParameter("@ExitTypeID", SqlDbType.Int)
                    Params(4).Value = ExitTypeID
                    Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(6).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_EmployeeExitRevoke", Params)
                    ErrorFlag = CInt(Params(5).Value)
                    Message = CStr(Params(6).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
        End Select
    End Sub
#End Region
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 380) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Employee Exit Revoke"
            Me.txtEmpCode.Focus()
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- Register Client Side Functions ---//
            txtEmpCode.Attributes.Add("onchange", "EmployeeOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
    End Sub
#End Region

End Class
