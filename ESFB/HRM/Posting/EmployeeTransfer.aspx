﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="EmployeeTransfer.aspx.vb" Inherits="EmployeeTransfer" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 25%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
     function BranchOnchange() {

            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
//            if (BranchDtl[1] == 1) {
//                document.getElementById("<%= cmbDepartment.ClientID %>").value = 21;
//                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = true;
//                
//               
//            }
//            else {
//                document.getElementById("<%= cmbDepartment.ClientID %>").value = "-1";
//                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = false;
              
//            }
            ChangeReportingOfficer();
         }
        function RequestOnchange() {

            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
//            if (BranchDtl[1] == 1) {
//                document.getElementById("<%= cmbDepartment.ClientID %>").value = 21;
//                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = true;
//                
//               
//            }
//            else {
//                document.getElementById("<%= cmbDepartment.ClientID %>").value = "-1";
//                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = false;
//              
//            }
           var AllData =document.getElementById("<%= hid_data.ClientID %>").value.split("|");
            var Data = AllData[0].split("^");
            document.getElementById("<%= hid_Dtls.ClientID %>").value=AllData[1];
            document.getElementById("<%= txtName.ClientID %>").value = Data[1];
            document.getElementById("<%= hid_branchid.ClientID %>").value = Data[2];
            document.getElementById("<%= txtBranch.ClientID %>").value = Data[3];
            document.getElementById("<%= hid_depid.ClientID %>").value = Data[4];
            document.getElementById("<%= txtDepartment.ClientID %>").value = Data[5];
            document.getElementById("<%= txtDesignation.ClientID %>").value = Data[6];
            document.getElementById("<%= hid_ReportingTo.ClientID %>").value = Data[8];
           // document.getElementById("<%= cmbEmpPost.ClientID %>").value = Data[25]; 
            document.getElementById("<%= hdnPrevEmpPost.ClientID %>").value = Data[25];
            document.getElementById("<%= hid_DesignationID.ClientID %>").value = Data[7];
            document.getElementById("<%= txtCadre.ClientID %>").value=Data[26];
             //SALARY
                      document.getElementById("<%= txtBasicPay.ClientID %>").value = parseFloat(Data[9]).toFixed(0);
                      document.getElementById("<%= txtDA.ClientID %>").value = parseFloat(Data[10]).toFixed(0);
                      document.getElementById("<%= txtHRA.ClientID %>").value = parseFloat(Data[11]).toFixed(0);
                      document.getElementById("<%= txtConveyance.ClientID %>").value = parseFloat(Data[12]).toFixed(0);
                      document.getElementById("<%= txtSpecialAllowance.ClientID %>").value = parseFloat(Data[13]).toFixed(0);
                      document.getElementById("<%= txtLocalAllowance.ClientID %>").value = parseFloat(Data[14]).toFixed(0);
                      document.getElementById("<%= txtMedicalAllowance.ClientID %>").value = parseFloat(Data[15]).toFixed(0);
                      document.getElementById("<%= txtHospitality.ClientID %>").value = parseFloat(Data[16]).toFixed(0);
                      document.getElementById("<%= txtPerformance.ClientID %>").value = parseFloat(Data[17]).toFixed(0);
                      document.getElementById("<%= txtOtherAllowance.ClientID %>").value = parseFloat(Data[18]).toFixed(0);
                      document.getElementById("<%= txtFieldAllowance.ClientID %>").value = parseFloat(Data[19]).toFixed(0);


                      var TotalEarnings = parseFloat(Data[9]) + parseFloat(Data[10]) + parseFloat(Data[11]) + parseFloat(Data[12]) + parseFloat(Data[13]) + parseFloat(Data[14]) + parseFloat(Data[15]) + parseFloat(Data[16]) + parseFloat(Data[17]) + parseFloat(Data[18]) + parseFloat(Data[19])

                      var TotalDeductions = parseFloat(Data[20]) + parseFloat(Data[21]) + parseFloat(Data[22]) + parseFloat(Data[23]) + parseFloat(Data[24]);
                      document.getElementById("<%= txtESI.ClientID %>").value = parseFloat(Data[20]).toFixed(0);
                      document.getElementById("<%= txtPF.ClientID %>").value = parseFloat(Data[21]).toFixed(0);
                      document.getElementById("<%= txtSWF.ClientID %>").value = parseFloat(Data[22]).toFixed(0);
                      document.getElementById("<%= txtESWT.ClientID %>").value = parseFloat(Data[23]).toFixed(0);
                      document.getElementById("<%= txtCharity.ClientID %>").value = parseFloat(Data[24]).toFixed(0);
                     
                      

                      document.getElementById("<%= txtTotalEarnings.ClientID %>").value = TotalEarnings;
                      document.getElementById("<%= txtTotalDeductions.ClientID %>").value = TotalDeductions;

            table_fill();
            ChangeReportingOfficer();
            document.getElementById("<%= cmbBranch.ClientID %>").focus();
          }
          function RequestID() {
              var EmpID = document.getElementById("<%= txtEmpcode.ClientID %>").value;
              if (EmpID > 0) {
                  var ToData = "2Ø" + EmpID;
                  ToServer(ToData,2);
              }
          }

          function FromServer(arg, context) {
             
              if (context == 1) {
                var Data = arg.split("Ø");
                  alert(Data[1]);
                  if (Data[0] == 0) window.open("EmployeeTransfer.aspx", "_self");
              }
              else if (context == 2) {
                  var Data = arg.split("~");
                 if (Data[0] == 1) {
                      document.getElementById("<%= hid_data.ClientID %>").value = Data[1];
                      RequestOnchange();
                  }
                  else if (Data[0] == 2)
                  {
                      alert(Data[1]);
                      document.getElementById("<%= txtName.ClientID %>").value = "";
                      document.getElementById("<%= hid_branchid.ClientID %>").value ="";
                      document.getElementById("<%= txtBranch.ClientID %>").value = "";
                      document.getElementById("<%= hid_depid.ClientID %>").value = "";
                      document.getElementById("<%= txtDepartment.ClientID %>").value = "";
                      document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                      document.getElementById("<%= txtEmpcode.ClientID %>").value = "";
                      document.getElementById("<%= hid_ReportingTo.ClientID %>").value ="";
                      document.getElementById("<%= txtCadre.ClientID %>").value="";
                      document.getElementById("<%= hid_Dtls.ClientID %>").value = "";
                      table_fill();
                      document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                  }
             
              }
               else if (context == 3)
                    {
                        ComboFill(arg, "<%= cmbReportingTo.ClientID %>");
                        
                    }
             else if (context == 4) {                   
                            ComboFill(arg, "<%= cmbDesignation.ClientID %>");                           
                         }
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnConfirm_onclick() {
           
           
            if (document.getElementById("<%= txtEmpcode.ClientID %>").value == "") 
            {
                alert("Select Employee Code");
                document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                return false;
            }
           
              if (document.getElementById("<%= txtStartDt.ClientID %>").value == "") 
            {
                alert("Select Effective Date");
                document.getElementById("<%= txtStartDt.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1") 
            {
                alert("Select Branch");
                document.getElementById("<%= cmbBranch.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbDepartment.ClientID %>").value == "-1") 
            {
                alert("Select Department");
                document.getElementById("<%= cmbDepartment.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= cmbCadre.ClientID %>").value == "-1") {
                alert("Select Cadre");
                document.getElementById("<%= cmbCadre.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= cmbDesignation.ClientID %>").value == "-1") {
                alert("Select Designation");
                document.getElementById("<%= cmbDesignation.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= cmbEmpPost.ClientID %>").value == "-1") {
                alert("Select Post");
                document.getElementById("<%= cmbEmpPost.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= cmbReportingTo.ClientID %>").value == "-1") 
            {
                alert("Select Reporting Officer");
                document.getElementById("<%= cmbReportingTo.ClientID %>").focus();
                return false;
            }
            
           
            var EmpID	= document.getElementById("<%= txtEmpcode.ClientID %>").value;
	        var DepartmentID	= document.getElementById("<%= cmbDepartment.ClientID %>").value;
	        var PrevBranchID	= document.getElementById("<%= hid_branchid.ClientID %>").value;
	        var PrevDepartmentID	= document.getElementById("<%= hid_depid.ClientID %>").value;
	        var EffectiveDate	= document.getElementById("<%= txtStartDt.ClientID %>").value;
	        var Branch = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
            var ReportingTo = document.getElementById("<%= cmbReportingTo.ClientID %>").value;
            var PrevReportingTo = document.getElementById("<%= hid_ReportingTo.ClientID %>").value;
            var CadreID =document.getElementById("<%= cmbCadre.ClientID %>").value;
            var DesignationID=document.getElementById("<%= cmbDesignation.ClientID %>").value;
	        var BranchID = Branch[0];
            var Post = document.getElementById("<%= cmbEmpPost.ClientID %>").value;

            
                     
	        var BasicPay = document.getElementById("<%= txtBasicPay.ClientID %>").value;
	        var DA = (document.getElementById("<%= txtDA.ClientID %>").value == "" || document.getElementById("<%= txtDA.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtDA.ClientID %>").value;
	        var HRA = (document.getElementById("<%= txtHRA.ClientID %>").value == "" || document.getElementById("<%= txtHRA.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtHRA.ClientID %>").value;
	        var Conveyance = (document.getElementById("<%= txtConveyance.ClientID %>").value == "" || document.getElementById("<%= txtConveyance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtConveyance.ClientID %>").value;
	        var SA = (document.getElementById("<%= txtSpecialAllowance.ClientID %>").value == "" || document.getElementById("<%= txtSpecialAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtSpecialAllowance.ClientID %>").value;
	        var LA = (document.getElementById("<%= txtLocalAllowance.ClientID %>").value == "" || document.getElementById("<%= txtLocalAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtLocalAllowance.ClientID %>").value;
	        var MA = (document.getElementById("<%= txtMedicalAllowance.ClientID %>").value == "" || document.getElementById("<%= txtMedicalAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtMedicalAllowance.ClientID %>").value;
	        var HA = (document.getElementById("<%= txtHospitality.ClientID %>").value == "" || document.getElementById("<%= txtHospitality.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtHospitality.ClientID %>").value;
	        var PA = (document.getElementById("<%= txtPerformance.ClientID %>").value == "" || document.getElementById("<%= txtPerformance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtPerformance.ClientID %>").value;
	        var OA = (document.getElementById("<%= txtOtherAllowance.ClientID %>").value == "" || document.getElementById("<%= txtOtherAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtOtherAllowance.ClientID %>").value;
	        var FA = (document.getElementById("<%= txtFieldAllowance.ClientID %>").value == "" || document.getElementById("<%= txtFieldAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtFieldAllowance.ClientID %>").value;
	        var ESI = (document.getElementById("<%= txtESI.ClientID %>").value == "" || document.getElementById("<%= txtESI.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtESI.ClientID %>").value;
	        var PF = (document.getElementById("<%= txtPF.ClientID %>").value == "" || document.getElementById("<%= txtPF.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtPF.ClientID %>").value;
	        var SWF = (document.getElementById("<%= txtSWF.ClientID %>").value == "" || document.getElementById("<%= txtSWF.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtSWF.ClientID %>").value;
	        var ESWT = (document.getElementById("<%= txtESWT.ClientID %>").value == "" || document.getElementById("<%= txtESWT.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtESWT.ClientID %>").value;
	        var Charity = (document.getElementById("<%= txtCharity.ClientID %>").value == "" || document.getElementById("<%= txtCharity.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtCharity.ClientID %>").value;

            var ToData = "1Ø" + EmpID + "Ø" + BranchID + "Ø" + DepartmentID + "Ø" + PrevBranchID + "Ø" + PrevDepartmentID + "Ø" + EffectiveDate+ "Ø" +ReportingTo+ "Ø" +PrevReportingTo + "Ø" + Post + "Ø" + document.getElementById("<%= hdnPrevEmpPost.ClientID %>").value+ "Ø" + BasicPay + "Ø" + DA + "Ø" + HRA + "Ø" + Conveyance + "Ø" + SA + "Ø" + LA + "Ø" + MA + "Ø" + HA + "Ø" + PA + "Ø" + OA + "Ø" + FA + "Ø" + ESI + "Ø" + PF + "Ø" + SWF + "Ø" + ESWT + "Ø" + Charity+"Ø"+CadreID+"Ø"+DesignationID ;
	       
            ToServer(ToData, 1);
        }
        function BaicPayOnChange() {
            var BasicPay = parseFloat(document.getElementById("<%= txtBasicPay.ClientID %>").value);
            if (BasicPay > 0) {
               
                var Perc1Amount = parseFloat(BasicPay * (parseFloat(document.getElementById("<%= hdnSFWTRate.ClientID %>").value) / 100));
                
                document.getElementById("<%= txtESWT.ClientID %>").value = Perc1Amount;
                Perc1Amount = Math.round(BasicPay * (parseFloat(document.getElementById("<%= hdnCharityRate.ClientID %>").value / 100)));
                document.getElementById("<%= txtCharity.ClientID %>").value = Perc1Amount;
            }
            CalculateTotalEarnings();
            CalculateTotalDeduction();
        }
        function CalculateTotalEarnings() {
            var BasicPay = document.getElementById("<%= txtBasicPay.ClientID %>").value;
            var DA = (document.getElementById("<%= txtDA.ClientID %>").value == "" || document.getElementById("<%= txtDA.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtDA.ClientID %>").value;
            var HRA = (document.getElementById("<%= txtHRA.ClientID %>").value == "" || document.getElementById("<%= txtHRA.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtHRA.ClientID %>").value;
            var Conveyance = (document.getElementById("<%= txtConveyance.ClientID %>").value == "" || document.getElementById("<%= txtConveyance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtConveyance.ClientID %>").value;
            var SA = (document.getElementById("<%= txtSpecialAllowance.ClientID %>").value == "" || document.getElementById("<%= txtSpecialAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtSpecialAllowance.ClientID %>").value;
            var LA = (document.getElementById("<%= txtLocalAllowance.ClientID %>").value == "" || document.getElementById("<%= txtLocalAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtLocalAllowance.ClientID %>").value;
            var MA = (document.getElementById("<%= txtMedicalAllowance.ClientID %>").value == "" || document.getElementById("<%= txtMedicalAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtMedicalAllowance.ClientID %>").value;
            var HA = (document.getElementById("<%= txtHospitality.ClientID %>").value == "" || document.getElementById("<%= txtHospitality.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtHospitality.ClientID %>").value;
            var PA = (document.getElementById("<%= txtPerformance.ClientID %>").value == "" || document.getElementById("<%= txtPerformance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtPerformance.ClientID %>").value;
            var OA = (document.getElementById("<%= txtOtherAllowance.ClientID %>").value == "" || document.getElementById("<%= txtOtherAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtOtherAllowance.ClientID %>").value;
            var FA = (document.getElementById("<%= txtFieldAllowance.ClientID %>").value == "" || document.getElementById("<%= txtFieldAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtFieldAllowance.ClientID %>").value;
            document.getElementById("<%= txtTotalEarnings.ClientID %>").value = Math.abs(BasicPay) + Math.abs(DA) + Math.abs(HRA) + Math.abs(Conveyance) + Math.abs(SA) + Math.abs(LA) + Math.abs(MA) + Math.abs(HA) + Math.abs(PA) + Math.abs(OA) + Math.abs(FA)


        }
        function CalculateTotalDeduction() {
            var ESI = (document.getElementById("<%= txtESI.ClientID %>").value == "" || document.getElementById("<%= txtESI.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtESI.ClientID %>").value;
            var PF = (document.getElementById("<%= txtPF.ClientID %>").value == "" || document.getElementById("<%= txtPF.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtPF.ClientID %>").value;
            var SWF = (document.getElementById("<%= txtSWF.ClientID %>").value == "" || document.getElementById("<%= txtSWF.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtSWF.ClientID %>").value;
            var ESWT = (document.getElementById("<%= txtESWT.ClientID %>").value == "" || document.getElementById("<%= txtESWT.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtESWT.ClientID %>").value;
            var Charity = (document.getElementById("<%= txtCharity.ClientID %>").value == "" || document.getElementById("<%= txtCharity.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtCharity.ClientID %>").value;
            document.getElementById("<%= txtTotalDeductions.ClientID %>").value = Math.abs(ESI) + Math.abs(PF) + Math.abs(SWF) + Math.abs(ESWT) + Math.abs(Charity);
        }

        function ChangeReportingOfficer() {
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
            if(BranchDtl!="-1")
            {
                var BranchID = BranchDtl[0];
                var DepartmentID = document.getElementById("<%= cmbDepartment.ClientID %>").value;
                //alert(document.getElementById("<%= txtEmpcode.ClientID %>").value);
                if(DepartmentID>0 && BranchID>=0 )
                    ToServer("3Ø" + BranchID + "Ø" + DepartmentID + "Ø" + document.getElementById("<%= txtEmpcode.ClientID %>").value+ "Ø" +DepartmentID , 3);
            }
        }
         function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }

        function table_fill() {

//            if (document.getElementById("<%= hid_Dtls.ClientID %>").value == "") {
//                document.getElementById("<%= pnHistory.ClientID %>").innerHTML = "";
//                document.getElementById("<%= pnHistory.ClientID %>").style.display = "none";
//               
//                return;
//            }
            document.getElementById("<%= pnHistory.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab +="<div style='width:100%; background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto; align=center;color:#FFF;font-family:cambria; font-size:10pt;'>TRANSFER HISTORY</div>"
            tab += "<div style='width:100%; height:170px;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
            tab += "<tr class=mainhead>";
            tab += "<td style='width:2%;text-align:center'>No</td>";
            tab += "<td style='width:8%;text-align:center' >Date</td>";
            tab += "<td style='width:10%;text-align:center;'colspan='2' >Location</td>";
            tab += "<td style='width:10%;text-align:center'colspan='2'>Department</td>";
            tab += "<td style='width:5%;text-align:center'colspan='2'>Cadre</td>";
            tab += "<td style='width:10%;text-align:center'colspan='2'>Designation</td>";
            tab += "<td style='width:10%;text-align:center'colspan='2'>Reporting To</td>";          
            tab += "</tr>";
            tab += "<tr class=mainhead>";
            tab += "<td style='width:2%;text-align:center'></td>";
            tab += "<td style='width:8%;text-align:center' ></td>";
            tab += "<td style='width:10%;text-align:center' >From</td>";
            tab += "<td style='width:10%;text-align:center' >To</td>";
            tab += "<td style='width:10%;text-align:center'>From</td>";
            tab += "<td style='width:10%;text-align:center'>To</td>";
            tab += "<td style='width:5%;text-align:center'>From</td>";
            tab += "<td style='width:5%;text-align:center'>To</td>";
            tab += "<td style='width:10%;text-align:center'>From</td>";
            tab += "<td style='width:10%;text-align:center'>To</td>";
            tab += "<td style='width:10%;text-align:center'>From</td>"; 
            tab += "<td style='width:10%;text-align:center'>To</td>";          
            tab += "</tr>";
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
           
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                if (document.getElementById("<%= hid_Dtls.ClientID %>").value != "") {
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr class=sub_first>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class=sub_second>";
                }
                i = n + 1;
             
             
                tab += "<td style='width:2%;text-align:center'>" + i + "</td>";
                tab += "<td style='width:8%;text-align:center' >" + col[8] + "</td>";
                tab += "<td style='width:10%;text-align:center' >" + col[0] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[1] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[2] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[3] + "</td>";
                 tab += "<td style='width:5%;text-align:center' >" + col[11] + "</td>";
                tab += "<td style='width:5%;text-align:center'>" + col[12] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[9] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[10] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[4] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[5] + "</td>";             
                tab += "</tr>";
                }
            }
            tab += "</table><div><div>";
            //alert(tab);
            document.getElementById("<%= pnHistory.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }
         function CadreOnChange() {
            var CadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;           
            var ToData = "4Ø" + CadreID;          
            ToServer(ToData, 4);

        }
    </script>
   
</head>
</html>
    <asp:HiddenField ID="hid_depid" runat="server" />
    <asp:HiddenField ID="hid_data" runat="server" />
                 <asp:HiddenField ID="hid_ReportingTo" runat="server" />
<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;">
    <tr> <td style="width:25%;">
                   <asp:HiddenField ID="hdnESI" runat="server" />
                   <asp:HiddenField ID="hdnSFWTRate" runat="server" />
                    </td>
    <td style="width:25%; text-align:left;">
                   <asp:HiddenField ID="hdnPrevEmpPost" runat="server" />
                    <asp:HiddenField ID="hdnCharityRate" runat="server" />
                   </td>
            <td style="width:25%">
                   <asp:HiddenField ID="hid_Dtls" runat="server" />
                   <asp:HiddenField ID="hdnPF" runat="server" />
            </td>
            <td style="width:25%">
                   <asp:HiddenField ID="hdnESWT" runat="server" />
                    </td>
    </tr>
    <tr> <td style="width:25%;">
                   Employee Code</td>
    <td style="width:25%; text-align:left;"><asp:TextBox ID="txtEmpcode" class="NormalText" runat="server" Width="45%" 
                MaxLength="5"  ></asp:TextBox>
            </td>
            <td style="width:25%">
                &nbsp; &nbsp;Name</td>
            <td style="width:25%">
                <asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="90%" 
                MaxLength="100"></asp:TextBox>
            </td>
    </tr>
        <tr>
        <td style="width:25%;">
                   Location</td>
         <td style="width:25%; text-align:left;"><asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" runat="server" Width="90%" 
                MaxLength="100"></asp:TextBox>
            </td>
            <td style="width:25%">
                &nbsp; &nbsp;Department</td>


            <td style="width:25%">
                <asp:TextBox ID="txtDepartment" class="ReadOnlyTextBox" runat="server" Width="90%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:25%;">
                   Cadre</td>
       <td style="width:25%; text-align:left;"><asp:TextBox ID="txtCadre" 
               class="ReadOnlyTextBox" runat="server" Width="90%" 
                MaxLength="100"></asp:TextBox>
            </td>
            <td style="width:25%">
                &nbsp; &nbsp;Designation</td>


            <td style="width:25%">
                <asp:TextBox ID="txtDesignation" class="ReadOnlyTextBox" runat="server" Width="90%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td colspan="4" style="text-align:center; font-family:Cambria;" class="tblQal">&nbsp; &nbsp;TRANSFER DETAILS</td>


       </tr>
       <tr> <td style="width:25%;">Location</td>
       <td style="width:25%; text-align:left;"><asp:DropDownList ID="cmbBranch" class="NormalText" style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="90%" ForeColor="Black"><asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
            </asp:DropDownList>
            </td>
            <td style="width:25%">
                &nbsp; &nbsp;Department</td>


            <td style="width:25%">
                <asp:DropDownList ID="cmbDepartment" class="NormalText" style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="90%" ForeColor="Black"><asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
            </asp:DropDownList>
            </td>


       </tr>
       
        <tr>
            <td style="width:25%;">Cadre</td>
            <td style="width:25%; text-align:left;"><asp:DropDownList ID="cmbCadre" runat="server" 
                    class="NormalText" Width="90%">
                </asp:DropDownList>
            </td>
            <td style="width:25%">
                &nbsp; &nbsp;Designation</td>
            
            <td style="width:25%">
                <asp:DropDownList ID="cmbDesignation" runat="server" 
                    class="NormalText" Width="90%">
                  <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
            
       </tr>
       <tr><td class="style3">Post</td>
       <td style="text-align:left;" class="style3"><asp:DropDownList 
                    ID="cmbEmpPost" runat="server" class="NormalText" Width="90%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="style3">
                &nbsp; &nbsp;Reporting To</td>
            
            <td class="style3">
                <asp:DropDownList ID="cmbReportingTo" runat="server" class="NormalText" Width="90%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
            
       </tr>
       <tr><td style="width:25%;">EffectiveDate</td>
       <td style="width:25%; text-align:left;"><asp:TextBox ID="txtStartDt" class="NormalText" runat="server" 
                    Width="65%"  ReadOnly="true" ></asp:TextBox>
             <asp:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtStartDt" Format="dd MMM yyyy">
             </asp:CalendarExtender>
            </td>
            <td style="width:25%">
                &nbsp; &nbsp;</td>
            
            <td style="width:25%">
                &nbsp;</td>
            
       </tr>
       <tr> 
       <td colspan="4" style="text-align:center; font-family:Cambria;" class="tblQal">&nbsp; &nbsp;SALARY DETAILS</td>


       </tr>
       <tr>
       <td style="width:100%;" colspan="4">
       <table align="center" style="width: 100%" >                                    
                    <tr>
                        <td style="width:12% ;text-align:center;" colspan="6" class="mainhead" >
                          EARNINGS </td>
                         <td style="width:12% ;text-align:center;"  colspan="2" class="mainhead">
                           DEDUCTIONS</td>
                    </tr>
                 
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                Basic Pay</td>
                       <td style="width:13% ;text-align:left;">
                             &nbsp;<asp:TextBox ID="txtBasicPay" runat="server" 
                    Width="80%" class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            DA</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtDA" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            HRA</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtHRA" runat="server" 
                    Width="80%" class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:13% ;text-align:left;">
                            ESI</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtESI" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            </td>
                    </tr>
                  
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            <span>Conveyance</span></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtConveyance" runat="server" 
                    Width="80%" class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            </td>
                        <td style="width:12% ;text-align:left;">
                            Special Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtSpecialAllowance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            Local Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtLocalAllowance" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            &nbsp;PF</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtPF" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            </td>
                    </tr>
                  
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            <span>Performance&nbsp;Allowance</span></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtPerformance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Other Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtOtherAllowance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Medical&nbsp;Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtMedicalAllowance" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Staff
                Welfare Fund</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtSWF" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                  
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            <span>Hospitality&nbsp;Allowance</span></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtHospitality" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Field Allowance</td>
                        <td style="width:13% ;text-align:left;">
                             &nbsp;<asp:TextBox ID="txtFieldAllowance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                             &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            &nbsp;</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            ESWT 1%</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtESWT" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                  
                   
                      <tr class="tblQalBody">
                        <td style="text-align:left;" colspan="6" class="style4">
                            </td>                       
                        <td style="text-align:left;" class="style4">
                            CharityFund 1%</td>
                        <td style="text-align:left;" class="style5">
                            &nbsp;<asp:TextBox ID="txtCharity" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                   <tr class="tblQal">
                        <td style="width:12% ;text-align:left;" colspan="4">
                            &nbsp;</td>
                       
                        <td style="width:12% ;text-align:left;color:White;">
                           <strong>Total(+)</strong></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtTotalEarnings" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" 
                                ReadOnly="True" style="color:#CC0000; font-weight:bold;"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;color:White;">
                            <strong>Total(-)</strong></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtTotalDeductions" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" 
                                ReadOnly="True" style="color:#CC0000; font-weight:bold;"></asp:TextBox>
                            &nbsp;</td>
                    </tr>

                      </table>
       </td>
       </tr>
       <tr>
       <td style="width:100%;" colspan="4">
       <asp:Panel ID="pnHistory" runat="server">
            </asp:Panel>
       </td>
       </tr>

       <tr>
            <td style="text-align:center;" colspan="4"><br />
                 <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE" onclick="return btnConfirm_onclick()"/>&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /><asp:HiddenField 
                    ID="hid_DesignationID" runat="server" />
            &nbsp;<asp:HiddenField ID="hid_branchid" runat="server" />
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

