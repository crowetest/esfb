﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="HRProfileUpdationTemp.aspx.vb" Inherits="HRProfileUpdationTemp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
    <link rel="stylesheet" href="../../Style/bootstrap-multiselect.css" type="text/css" />
    <script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
    <script type="text/javascript" src="../../Script/bootstrap-multiselect_EMP.js"></script>
    <style>
        .tblQal
        {
            border: 9px;
            background-color: #FBEFFB;
            height: 25px;
            font-size: 10;
            color: #476C91;
        }
        .tblQalBody
        {
            border: 9px;
            background-color: #FFF;
            height: 25px;
            font-size: 10;
            color: #476C91;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#chkveg').multiselect();
            buttonWidth: '500px'
        }); 0
    </script>
    <script language="javascript" type="text/javascript">
      $(function() {
			        $('#chkveg').multiselect({                                            
			            includeSelectAllOption: false
                                        
			        });
                     $('#chkveg2').multiselect({                                            
			            includeSelectAllOption: false
                                        
			        });
                      $('#chkveg1').multiselect({                                            
			            includeSelectAllOption: false
                                        
			        });
			      
			    });
     

        function cursorwait(e) {
            document.body.className = 'wait';
        }
        function cursordefault(e) {
            document.body.className = 'default';
        }
        function ReligionOnChange(){
            if(document.getElementById("<%= cmbReligion.ClientID %>").value!="-1")
            {   ClearCombo("<%= cmbCaste.ClientID %>"); 
                var Data = "7Ø" +document.getElementById("<%= cmbReligion.ClientID %>").value;
                ToServer(Data, 7);
            }
        }
        function window_onload() {
            document.getElementById("btnSave").disabled=true;
        }
        function EmpCodeOnFocus()
        {
            document.getElementById("btnSave").disabled=true;
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
        }
        function btnSave_onclick() {                    
            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            var EmpName = document.getElementById("<%= txtEmpName.ClientID %>").value;
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
            var BranchID = BranchDtl[0];
            var DepartmentID = document.getElementById("<%= cmbDepartment.ClientID %>").value;
            var CadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;
            var DesignationID = document.getElementById("<%= cmbDesignation.ClientID %>").value;
            var DateOfJoin = document.getElementById("<%= txtDateOfJoin.ClientID %>").value;
            var GenderID = document.getElementById("<%= cmbGender.ClientID %>").value;
            var ReportingTo = document.getElementById("<%= cmbReportingTo.ClientID %>").value;
            var empPost = document.getElementById("<%= cmbEmpPost.ClientID %>").value;            
            var isFieldStaff = (document.getElementById("<%= chkFieldStaff.ClientID %>").checked) ? 1 : 0;   
            var CareOf= document.getElementById("<%= txtCareOf.ClientID %>").value;    
            var CareOfPhone= document.getElementById("<%= txtCareOfPhoneNo.ClientID %>").value;  
            var HouseName= document.getElementById("<%= txtHouseName.ClientID %>").value; 
            var Location=  document.getElementById("<%= txtLocation.ClientID %>").value;
            var City=  document.getElementById("<%= txtCity.ClientID %>").value;
            if(document.getElementById("<%= cmbPost.ClientID %>").value == "")
                var PostID=0;
            else{
                var PostDtl=document.getElementById("<%= cmbPost.ClientID %>").value.split("~");
                var PostID=PostDtl[0];
            }
            var Email=document.getElementById("<%= txtEmail.ClientID %>").value;
            var Landline=document.getElementById("<%= txtLandLine.ClientID %>").value;
            var IDProofType =(document.getElementById("<%= cmbIDProof.ClientID %>").value==-1) ? 1 : document.getElementById("<%= cmbIDProof.ClientID %>").value;
            if (IDProofType == "")
                IDProofType = 1;
            var AddressProofType=(document.getElementById("<%= cmbAddressProof.ClientID %>").value==-1) ? 1 : document.getElementById("<%= cmbAddressProof.ClientID %>").value;
            if (AddressProofType == "")
                AddressProofType = 1;
            var IDProofNumber=document.getElementById("<%= txtIDProof.ClientID %>").value;
            var AddressProofNumber=document.getElementById("<%= txtAddressProof.ClientID %>").value;
            var Mobile=document.getElementById("<%= txtMobile.ClientID %>").value;    
            var DOB = "01/01/1900";
            if(document.getElementById("<%= txtDOB.ClientID %>").value != "")
                DOB=document.getElementById("<%= txtDOB.ClientID %>").value;

            var MStatusID=(document.getElementById("<%= cmbMaritalStatus.ClientID %>").value==-1) ? 2 :document.getElementById("<%= cmbMaritalStatus.ClientID %>").value;
            if(MStatusID == "")
                MStatusID = 2;
            var IsTwowheeler = (document.getElementById("<%= chkTwoWheeler.ClientID %>").checked) ? 1 : 0; 
            var IsSangamMember = (document.getElementById("<%= chkSangam.ClientID %>").checked) ? 1 : 0; 
            var MarriageDate=document.getElementById("<%= txtMarriageDate.ClientID %>").value;
            var DrivingNo=document.getElementById("<%= txtDrivingNo.ClientID %>").value;
            var Father=document.getElementById("<%= txtFather.ClientID %>").value;
            var Mother=document.getElementById("<%= txtMother.ClientID %>").value;
            var ReligionId=document.getElementById("<%= cmbReligion.ClientID %>").value;
            if (ReligionId == "")
                ReligionId = -1;
            var CasteId=document.getElementById("<%= cmbCaste.ClientID %>").value;
            if (CasteId == "")
                CasteId = -1;
            var MotherTongueId=document.getElementById("<%= cmbMotherTongue.ClientID %>").value;
            if (MotherTongueId == "")
                MotherTongueId = -1;
            var BloodGroupId=document.getElementById("<%= cmbBloodGroup.ClientID %>").value;
            if(BloodGroupId == "")
                BloodGroupId = -1;
            var Cug=document.getElementById("<%= txtCug.ClientID %>").value;
            var OfficialEmail=document.getElementById("<%= txtOfficialEmail.ClientID %>").value;
            var ChkPanch = (document.getElementById("<%= chkPanchayath.ClientID %>").checked) ? 1 : 0; 
            var chkComm = (document.getElementById("<%= chkCommunity.ClientID %>").checked) ? 1 : 0; 
            var chkPolice = (document.getElementById("<%= chkPolice.ClientID %>").checked) ? 1 : 0; 
             
            var SelectedItems =$('#chkveg2').val(); 
            if(SelectedItems == null){
            SelectedItems="";
            }
                        
            var EmpType=document.getElementById("<%= cmbEmpType.ClientID %>").value;
            if (EmpType == "")
                EmpType = -1;
            var SuretyType=document.getElementById("<%= cmbSuretyType.ClientID %>").value;
            if (SuretyType == "")
                SuretyType = -1;
            var SuretyName=document.getElementById("<%= txtSuretyName.ClientID %>").value;
            var SuretyAddr=document.getElementById("<%= txtSuretyAddr.ClientID %>").value;
            var SuretyPhone=document.getElementById("<%= txtSuretyPhone.ClientID %>").value;

            var Data = "1Ø" + EmpCode + "Ø" + EmpName + "Ø" + BranchID + "Ø" + DepartmentID + "Ø" + CadreID + "Ø" + DesignationID + "Ø" + DateOfJoin + "Ø" + GenderID + "Ø" + ReportingTo + "Ø" + isFieldStaff + "Ø" + CareOf+ "Ø" + HouseName + "Ø" + Location+"Ø"+City+"Ø"+PostID+"Ø"+Email+"Ø"+Landline+"Ø"+Mobile+"Ø"+DOB+"Ø"+MStatusID+"Ø"+CareOfPhone+"Ø"+IDProofType+"Ø"+IDProofNumber+"Ø"+AddressProofType+"Ø"+AddressProofNumber+"Ø"+empPost+"Ø"+Father+"Ø"+Mother+ "Ø" +ReligionId+ "Ø" +CasteId+ "Ø" +IsTwowheeler+ "Ø" +DrivingNo+ "Ø" +IsSangamMember+ "Ø" +MarriageDate+ "Ø" +MotherTongueId+ "Ø" +BloodGroupId+ "Ø" +Cug+ "Ø" +OfficialEmail+ "Ø" +ChkPanch+ "Ø" +chkComm+ "Ø" +chkPolice+ "Ø" +SelectedItems+ "Ø" +EmpType+ "Ø" +SuretyType+ "Ø" +SuretyName+ "Ø" +SuretyAddr+ "Ø" +SuretyPhone;
            
            cursorwait();
            document.getElementById("btnSave").disabled=true;
            ToServer(Data, 1);            
        }

        function FromServer(Arg, Context) {
            switch (Context) {
                case 1:
                {
                    var Data = Arg.split("Ø");
                    cursordefault();
                    alert(Data[1]);
                    if (Data[0] == 0) window.open("HRProfileUpdationTemp.aspx", "_self");
                    else
                    { document.getElementById("btnSave").disabled=false;}
                    break;
                }    
                case 2:
                {
                    document.getElementById("ImgPhoto").setAttribute('src',Arg); 
                    break;
                }       
                case 3: 
                {    
                    if(Arg=="ØØØØ")  
                    {
                        alert("Invalid Employee Number");  document.getElementById("<%= cmbState.ClientID %>").focus();    break;      
                    } 
                    else
                    {
                        FillData(Arg);    
                        document.getElementById("btnSave").disabled=false;                           
                    }
                    break;
                }
                case 4: {                   
                    ComboFill(Arg, "<%= cmbDesignation.ClientID %>");
                    break;
                }
                case 5: {                   
                    ComboFill(Arg, "<%= cmbDistrict.ClientID %>");
                    break;
                }
                case 6: {                   
                    ComboFill(Arg, "<%= cmbPost.ClientID %>");
                    break;
                }
                case 7: {                  
                    ComboFill(Arg, "<%= cmbCaste.ClientID %>");
                    break;
                }                    
            }
        }
        function BranchOnChange() {
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
//            if (BranchDtl[1] == 1 || BranchDtl[1]==3) {
//                document.getElementById("<%= cmbDepartment.ClientID %>").value = 21;
//                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = true ;               
//            }
//            else {
//                document.getElementById("<%= cmbDepartment.ClientID %>").value = "-1";
//                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = false;              
//            }
            ChangeReportingOfficer();
        }
        function ChangeReportingOfficer() {
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
            if(BranchDtl!="-1")
            {
                var BranchID = BranchDtl[0];
                var DepartmentID = document.getElementById("<%= cmbDepartment.ClientID %>").value;
                var DesignationID = document.getElementById("<%= cmbDesignation.ClientID %>").value;
                if(DepartmentID>0 && BranchID>=0 && DesignationID>0)
                    ToServer("2Ø" + BranchID + "Ø" + DepartmentID + "Ø" + DesignationID + "Ø", 2);
            }
        }
        function CadreOnChange() {
            var CadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;           
            var ToData = "4Ø" + CadreID;          
            ToServer(ToData, 4);

        }
        function StateOnChange() {
            ClearCombo("<%= cmbPost.ClientID %>");
            var StateID = document.getElementById("<%= cmbState.ClientID %>").value;           
            var ToData = "5Ø" + StateID;          
            ToServer(ToData, 5);

        }
        function DistrictOnChange() {
            var DistrictID = document.getElementById("<%= cmbDistrict.ClientID %>").value;           
            var ToData = "6Ø" + DistrictID;          
            ToServer(ToData, 6);
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function MaritalStatusOnChange()
        {         
            if(document.getElementById("<%= cmbMaritalStatus.ClientID %>").value==-1)
            {
                document.getElementById("FatHus").innerHTML ="Guardian";           
            }
            else if(document.getElementById("<%= cmbMaritalStatus.ClientID %>").value==1 || document.getElementById("<%= cmbMaritalStatus.ClientID %>").value==3 )
            {
                document.getElementById("FatHus").innerHTML ="Spouse";          
            }
            else
            {
                document.getElementById("FatHus").innerHTML ="Father";           
            }
        }                       
        function CreateNewRow(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValue(val);
                AddNewRow();                          
            }
        }
        function CreateNewRowExperience(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValueExperience(val);
                AddNewRowExperience();                          
            }
        }
        function CreateNewRowFamily(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValueFamily(val);
                AddNewRowFamily();                          
            }
        }
        function CreateNewRowAddressProof(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            
            if (n == 13 || n == 9) {
                updateValueAddressProof(val);
                AddNewRowAddressProof();                          
            }
        }
        function CreateNewRowIDProof(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValueIDProof(val);
                AddNewRowIDProof();                          
            }
        } 
        function TwoWheelerStatus(){
            if(document.getElementById("<%= chkTwoWheeler.ClientID %>").checked==true){
                document.getElementById("<%= txtDrivingNo.ClientID %>").disabled=false;
            }
            else{
                document.getElementById("<%= txtDrivingNo.ClientID %>").disabled=true;
                document.getElementById("<%= txtDrivingNo.ClientID %>").value="";
            }
        }
        function EmpCodeOnChange()
        {  
            var EmpCode=document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if(EmpCode!="")
            {
                var ToData = "3Ø" + EmpCode;          
                ToServer(ToData, 3);
            }
        }
        function FillData(Arg)
        {
            var Row=Arg.split("Ø");
            var Dtl=Row[0].split("µ");      
            ComboFill(Row[1],"<%= cmbDistrict.ClientID %>" )
            ComboFill(Row[2],"<%= cmbPost.ClientID %>" );  
            ComboFill(Row[9],"<%= cmbDesignation.ClientID %>" );   
            ComboFill(Row[10],"<%= cmbCaste.ClientID %>" );   
            ComboFill(Row[11],"<%= cmbReportingTo.ClientID %>" );                 
                
            document.getElementById("ImgPhoto").setAttribute('src',Row[4]);   
            document.getElementById("<%= txtEmpName.ClientID %>").value=Dtl[1];
                 
            document.getElementById("<%= cmbBranch.ClientID %>").value=Dtl[3];
            document.getElementById("<%= cmbDepartment.ClientID %>").value=Dtl[2];
            document.getElementById("<%= cmbDesignation.ClientID %>").value=Dtl[4];
            document.getElementById("<%= cmbCadre.ClientID %>").value=Dtl[5];
            document.getElementById("<%= txtDateofJoin.ClientID %>").value=Dtl[6];
            document.getElementById("<%= txtHouseName.ClientID %>").value=Dtl[7];
            document.getElementById("<%= txtLocation.ClientID %>").value=Dtl[8];
            document.getElementById("<%= txtCity.ClientID %>").value=Dtl[9];
            document.getElementById("<%= txtMobile.ClientID %>").value=Dtl[10];
            document.getElementById("<%= cmbReportingTo.ClientID %>").value=Dtl[19];
            document.getElementById("<%= txtDOB.ClientID %>").value=Dtl[21];
            document.getElementById("<%= cmbMaritalStatus.ClientID %>").value=Dtl[20];
            MaritalStatusOnChange();
            document.getElementById("<%= txtCareOf.ClientID %>").value=Dtl[12];
            document.getElementById("<%= cmbGender.ClientID %>").value=Dtl[11];
            document.getElementById("<%= txtCareOfPhoneNo.ClientID %>").value=Dtl[22];
            document.getElementById("<%= cmbState.ClientID %>").value=Dtl[15];
            document.getElementById("<%= cmbDistrict.ClientID %>").value=Dtl[14];   
            document.getElementById("<%= cmbPost.ClientID %>").value=Dtl[13]+"~"+Dtl[17];    
            document.getElementById("<%= txtEmail.ClientID %>").value=Dtl[18];  
            document.getElementById("<%= txtLandline.ClientID %>").value=Dtl[16];  
            document.getElementById("<%= cmbEmpPost.ClientID %>").value=Dtl[63];  
            if (Dtl[64]==2)
            {
                document.getElementById("<%= chkFieldStaff.ClientID %>").checked=true
            }
            else{
                document.getElementById("<%= chkFieldStaff.ClientID %>").checked=false;
            }                               
            document.getElementById("<%= txtFather.ClientID %>").value=Dtl[43]; 
            document.getElementById("<%= txtMother.ClientID %>").value=Dtl[44]; 
            document.getElementById("<%= cmbCaste.ClientID %>").value=Dtl[45];
                
            document.getElementById("<%= cmbBloodGroup.ClientID %>").value=Dtl[46]; 
            document.getElementById("<%= cmbMotherTongue.ClientID %>").value=Dtl[47];
                 
            document.getElementById("<%= txtMarriageDate.ClientID %>").value=Dtl[48]; 
            document.getElementById("<%= chkTwoWheeler.ClientID %>").checked=(Dtl[49]==0)? false :true; 
            document.getElementById("<%= txtDrivingNo.ClientID %>").value=Dtl[50]; 
            document.getElementById("<%= txtCug.ClientID %>").value=Dtl[51]; 
            document.getElementById("<%= txtOfficialEmail.ClientID %>").value=Dtl[52]; 
            document.getElementById("<%= chkPanchayath.ClientID %>").value=(Dtl[53]==0)? false :true; 
                
            document.getElementById("<%= chkCommunity.ClientID %>").checked=(Dtl[54]==0)? false :true; 
            document.getElementById("<%= chkPolice.ClientID %>").checked=(Dtl[55]==0)? false :true; 
            document.getElementById("<%= chkSangam.ClientID %>").checked=(Dtl[56]==0)? false :true; 
            document.getElementById("<%= cmbSuretyType.ClientID %>").value=Dtl[57]; 
            document.getElementById("<%= txtSuretyName.ClientID %>").value=Dtl[58]; 
            document.getElementById("<%= txtSuretyAddr.ClientID %>").value=Dtl[59]; 
            document.getElementById("<%= txtSuretyPhone.ClientID %>").value=Dtl[60]; 
            document.getElementById("<%= cmbReligion.ClientID %>").value=Dtl[61]; 
            document.getElementById("<%= cmbEmpType.ClientID %>").value=Dtl[62];                 
                             
            var newList ="";     
            var rrr = document.getElementById("<%= hdnLang.ClientID %>").value.split("Ñ");
            for (a = 2; a < rrr.length-1; a++) {
                var cols = rrr[a].split("ÿ");
                rows = Row[12].split("^");
                var Val="";
                          
                for (n = 0; n <= rows.length - 1; n++) {
                    if(cols[0]==rows[n]){
                        Val="selected";
                    }                            
                }
                newList+="<option value="+ cols[0] +" "+ Val +" >"+ cols[1] +"</option>"  ;             
            }               
             
            $('#chkveg2').html(''); // clear out old list
            $('#chkveg2').multiselect('destroy');  // tell widget to clear itself
            $('#chkveg2').html(newList); // add in the new list
            $('#chkveg2').multiselect();
        }
        function ChangePhoto()
        {    
            var EmpCode=document.getElementById("<%= txtEmpCode.ClientID %>").value;
            var EmpName=document.getElementById("<%= txtEmpName.ClientID %>").value;
            if(EmpName=="")
            {alert("Select an Employee"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false;}
            if(EmpCode!="")
            {   
                window.showModalDialog("ChangePhoto.aspx?EmpCode="+EmpCode,"",'dialogHeight:200px;dialogWidth:500px; dialogLeft:300;dialogTop:300; center:yes');        
                ToServer("2Ø"+EmpCode,2);      
            }  
        }

    </script>
    <br />
    <table align="center" style="width: 80%; margin: 0px auto;">
        <tr>
            <td style="width: 12%; text-align: center;" colspan="4" class="mainhead">
                <strong>APPOINTMENT DETAILS</strong><asp:ToolkitScriptManager ID="ToolkitScriptManager1"
                    runat="server">
                </asp:ToolkitScriptManager>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; text-align: left;" colspan="4">
                <table align="left" style="width: 100%;">
                    <tr>
                        <td style="width: 12%; text-align: left;">
                            Employee Code&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 44%;">
                            &nbsp;&nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="NormalText"
                                MaxLength="6" onkeypress="return NumericCheck(event)"></asp:TextBox>
                        </td>
                        <td style="width: 12%; text-align: left;">
                            Name&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 32%;" class="Displays">
                            &nbsp;&nbsp;<asp:TextBox ID="txtEmpName" runat="server" Width="90%" ReadOnly="true"
                                class="ReadOnlyTextBox"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 12%; text-align: left;">
                            Location&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 44%;" class="Displays">
                            &nbsp;&nbsp;<asp:DropDownList ID="cmbBranch" runat="server" class="ReadOnlyTextBox"
                                Width="50%" Enabled="false">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 12%; text-align: left;" class="Displays">
                            Department&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 32%;" class="Displays">
                            &nbsp;&nbsp;<asp:DropDownList ID="cmbDepartment" runat="server" class="ReadOnlyTextBox"
                                Width="91%" Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 12%; text-align: left;">
                            Cadre&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 44%;">
                            &nbsp;&nbsp;<asp:DropDownList ID="cmbCadre" runat="server" class="ReadOnlyTextBox"
                                Width="50%" Enabled="false">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 12%; text-align: left;">
                            Designation&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 32%;">
                            &nbsp;&nbsp;<asp:DropDownList ID="cmbDesignation" runat="server" class="ReadOnlyTextBox"
                                Width="91%" Enabled="false">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 12%; text-align: left;">
                            Date Of Join&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 44%;">
                            &nbsp;&nbsp;<asp:TextBox ID="txtDateofJoin" runat="server" class="ReadOnlyTextBox"
                                Width="30%" ReadOnly="true"></asp:TextBox>
                            <asp:CalendarExtender ID="CE1" runat="server" TargetControlID="txtDateofJoin" Format="dd MMM yyyy">
                            </asp:CalendarExtender>
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td style="width: 12%; text-align: left;">
                            Reporting To&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 32%;">
                            &nbsp;&nbsp;<asp:DropDownList ID="cmbReportingTo" runat="server" class="NormalText"
                                Width="91%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 12%; text-align: left;">
                            Post&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 44%;">
                            &nbsp;&nbsp;<asp:DropDownList ID="cmbEmpPost" runat="server" class="ReadOnlyTextBox"
                                Width="50%" readonly="true">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 12%; text-align: left;">
                            Field Staff&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 32%; text-align: left;">
                            &nbsp;&nbsp;<asp:CheckBox ID="chkFieldStaff" runat="server" Text=" " TextAlign="Left" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 12%; text-align: left;">
                            Employee Status&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 44%;">
                            &nbsp;&nbsp;<asp:DropDownList ID="cmbEmpType" runat="server" class="ReadOnlyTextBox"
                                Width="50%" Enabled="false">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 12%; text-align: left;">
                            Surety Person&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 32%; text-align: left;">
                            &nbsp;&nbsp;<asp:DropDownList ID="cmbSuretyType" runat="server" class="NormalText"
                                Width="50%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 12%; text-align: left;">
                            Surety Name&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 44%;">
                            &nbsp;&nbsp;<asp:TextBox ID="txtSuretyName" runat="server" Width="90%" class="NormalText"></asp:TextBox>
                        </td>
                        <td style="width: 12%; text-align: left;">
                            Surety Address&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 32%; text-align: left;">
                            &nbsp;&nbsp;<asp:TextBox ID="txtSuretyAddr" runat="server" Width="90%" class="NormalText"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 12%; text-align: left;">
                            Surety Contact No&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 44%;">
                            &nbsp;&nbsp;<asp:TextBox ID="txtSuretyPhone" runat="server" Width="90%" MaxLength="11"
                                class="NormalText" onkeypress='return NumericCheck(event)'></asp:TextBox>
                        </td>
                        <td style="width: 12%; text-align: left;">
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td style="width: 32%; text-align: left;">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 12%; text-align: center;" colspan="4" class="mainhead">
                <strong>PERSONAL DETAILS</strong>
            </td>
        </tr>
        <tr>
            <td style="width: 12%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 44%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 32%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 100%; text-align: left;" colspan="4">
                <table align="left" style="width: 80%">
                    <tr>
                        <td style="width: 25%;">
                            Date Of Birth&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="txtDOB" runat="server" class="NormalText" Width="90%" ReadOnly="True"
                                onkeypress="return false"></asp:TextBox>
                            <asp:CalendarExtender ID="txtDOB_CalendarExtender" runat="server" TargetControlID="txtDOB"
                                Format="dd MMM yyyy">
                            </asp:CalendarExtender>
                        </td>
                        <td style="width: 25%;">
                            Gender&nbsp;&nbsp;<b style="color: red;">*</b>
                        </td>
                        <td style="width: 25%;">
                            <asp:DropDownList ID="cmbGender" runat="server" class="NormalText" Width="91%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; text-align: left;">
                            Marital Status
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:DropDownList ID="cmbMaritalStatus" runat="server" class="NormalText" Width="90%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                        </td>
                        <td id="FatHus" style="width: 25%; text-align: left;">
                            Guardian Name
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:TextBox ID="txtCareOf" runat="server" Width="90%" class="NormalText" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            Guardian&#39;s Phone No
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="txtCareOfPhoneNo" runat="server" Width="90%" class="NormalText"
                                MaxLength="11" onkeypress='return NumericCheck(event)'></asp:TextBox>
                        </td>
                        <td style="width: 25%;">
                            House Name
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="txtHouseName" runat="server" Width="90%" class="NormalText" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            Location
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="txtLocation" runat="server" Width="90%" class="NormalText" MaxLength="50"></asp:TextBox>
                        </td>
                        <td style="width: 25%;">
                            City
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="txtCity" runat="server" Width="90%" class="NormalText" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            State
                        </td>
                        <td style="width: 25%;">
                            <asp:DropDownList ID="cmbState" runat="server" class="NormalText" Width="91%">
                            <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 25%;">
                            District
                        </td>
                        <td style="width: 25%;">
                            <asp:DropDownList ID="cmbDistrict" runat="server" class="NormalText" Width="91%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            Post
                        </td>
                        <td style="width: 25%;">
                            <asp:DropDownList ID="cmbPost" runat="server" class="NormalText" Width="91%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 25%;">
                            Email
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="txtEmail" runat="server" Width="90%" class="NormalText" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            Land Line
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="txtLandline" runat="server" Width="90%" class="NormalText" MaxLength="15"
                                onkeypress='return NumericCheck(event)'></asp:TextBox>
                        </td>
                        <td style="width: 25%;">
                            Mobile
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="txtMobile" runat="server" Width="90%" class="NormalText" MaxLength="10"
                                onkeypress='return NumericCheck(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td style="width: 25%;">
                            ID Proof
                        </td>
                        <td style="width: 25%;">
                            <asp:DropDownList ID="cmbIDProof" runat="server" class="NormalText" Width="91%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 25%;">
                            ID Proof No
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="txtIDProof" runat="server" Width="90%" class="NormalText" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td style="width: 25%;">
                            Address Proof
                        </td>
                        <td style="width: 25%;">
                            <asp:DropDownList ID="cmbAddressProof" runat="server" class="NormalText" Width="91%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 25%;">
                            Address Proof No
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="txtAddressProof" runat="server" Width="90%" class="NormalText" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; text-align: left;">
                            Fathers Name
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:TextBox ID="txtFather" runat="server" Width="90%" class="NormalText" MaxLength="50"></asp:TextBox>
                        </td>
                        <td style="width: 25%; text-align: left;">
                            &nbsp;Mothers Name
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:TextBox ID="txtMother" runat="server" Width="90%" class="NormalText" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; text-align: left;">
                            Religion
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:DropDownList ID="cmbReligion" runat="server" class="NormalText" Width="90%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 25%; text-align: left;">
                            Caste
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:DropDownList ID="cmbCaste" runat="server" class="NormalText" Width="90%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; text-align: left;">
                            Blood Group
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:DropDownList ID="cmbBloodGroup" runat="server" class="NormalText" Width="90%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 25%; text-align: left;">
                            Marriage Date
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:TextBox ID="txtMarriageDate" runat="server" class="NormalText" Width="90%" ReadOnly="true"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtMarriageDate"
                                Format="dd MMM yyyy">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; text-align: left;">
                            Two Wheeler Status
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:CheckBox ID="chkTwoWheeler" runat="server" Text=" " TextAlign="Left" onclick="TwoWheelerStatus()" />
                        </td>
                        <td style="width: 25%; text-align: left;">
                            Driving Liscence No
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:TextBox ID="txtDrivingNo" runat="server" class="NormalText" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; text-align: left;">
                            CUG Mobile
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:TextBox ID="txtCug" runat="server" Width="90%" class="NormalText" MaxLength="10"
                                onkeypress='return NumericCheck(event)'></asp:TextBox>
                        </td>
                        <td style="width: 25%; text-align: left;">
                            Official e-Mail
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:TextBox ID="txtOfficialEmail" runat="server" Width="90%" class="NormalText"
                                MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; text-align: left;">
                            Reference Letter Details
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:CheckBox ID="chkPanchayath" runat="server" Text=" " TextAlign="Left" />Panchayath
                            <asp:CheckBox ID="chkCommunity" runat="server" Text=" " TextAlign="Left" />Community
                            <asp:CheckBox ID="chkPolice" runat="server" Text=" " TextAlign="Left" />Police station
                        </td>
                        <td style="width: 25%; text-align: left;">
                            Mother Tongue
                        </td>
                        <td style="width: 25%; text-align: left;">
                            <asp:DropDownList ID="cmbMotherTongue" runat="server" class="NormalText" Width="90%">
                                <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; text-align: left;">
                            &nbsp;Languages Known
                        </td>
                        <td id="lang" style="width: 25%; text-align: left;">
                            &nbsp;<select id='chkveg2' multiple='multiple' style='width: 80%;'>
                            </select>
                        </td>
                        <td style="width: 50%; text-align: left;" colspan="2">
                            <asp:CheckBox ID="chkSangam" runat="server" Text=" " TextAlign="Left" />
                            Whether the staff was earlier our Sangam member or not?
                        </td>
                    </tr>
                </table>
                <div style="width: 19%; height: 124px; float: right; text-align: center;">
                    <img id="ImgPhoto" alt="Photo" src="../../Image/userimage.png" style="height: 100px;
                        width: 100px;" title="Photo" />
                    <br />
                    <a style="color: Blue; cursor: pointer;" onclick="ChangePhoto()"><u>Change Photo</u></a></div>
            </td>
        </tr>
        <tr>
            <td style="width: 12%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 44%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 32%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr style="text-align: center;">
            <td colspan="4" style="text-align: center;">
                <input id="btnSave" type="button" value="SAVE" onclick="return btnSave_onclick()"
                    style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;<input id="btnExit"
                        type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria;
                        width: 5%; cursor: pointer;" /><asp:HiddenField ID="hdnQualification" runat="server" />
                <asp:HiddenField ID="hdnLang" runat="server" />
                <asp:HiddenField ID="hdnUniversity" runat="server" />
                <asp:HiddenField ID="hdnWelfareAmount" runat="server" />
                <asp:HiddenField ID="hdnESIRate" runat="server" />
                <asp:HiddenField ID="hdnPFRate" runat="server" />
                <asp:HiddenField ID="hdnSFWTRate" runat="server" />
                <asp:HiddenField ID="hdnCharityRate" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
    </script>
</asp:Content>
