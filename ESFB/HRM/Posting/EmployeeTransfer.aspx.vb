﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class EmployeeTransfer
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 18) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Employee Transfer"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            GF.ComboFill(cmbEmpPost, GF.GetEmpPost(), 0, 1)
            GF.ComboFill(cmbCadre, EN.GetCadre(), 0, 1)
            DT = EN.GetBranch()
            GF.ComboFill(cmbBranch, DT, 0, 1)
            DTTS = EN.GetDepartment()
            GF.ComboFill(cmbDepartment, DTTS, 0, 1)

            Me.cmbBranch.Attributes.Add("onchange", "return BranchOnchange()")
            Me.txtEmpcode.Attributes.Add("onchange", "return RequestID()")
            Me.cmbDepartment.Attributes.Add("onchange", "ChangeReportingOfficer()")

            Me.txtBasicPay.Attributes.Add("onchange", "BaicPayOnChange()")
            Me.txtDA.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtHRA.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtConveyance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtSpecialAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtLocalAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtMedicalAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtHospitality.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtFieldAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtPerformance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtOtherAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            'General Parameters 4-ESI,5-PF,6-SWF,7-ESWT,8-Charity
            'hdnESI.Value = GF.GetParameterValue(4, 1)
            'hdnPF.Value = GF.GetParameterValue(5, 1)
            hdnSFWTRate.Value = GF.GetParameterValue(7, 1)
            'hdnESWT.Value = GF.GetParameterValue(7, 1)
            hdnCharityRate.Value = GF.GetParameterValue(8, 1)

            Me.txtESI.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtPF.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtSWF.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtESWT.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtCharity.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.cmbCadre.Attributes.Add("onchange", "return CadreOnChange()")
            txtEmpcode.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EmpID As Integer = CInt(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim BranchID As Integer = CInt(Data(2))
            Dim DepartmentID As Integer = CInt(Data(3))
            Dim PrevBranchID As Integer = CInt(Data(4))
            Dim PrevDepartmentID As Integer = CInt(Data(5))
            Dim statusID As Integer = 0
            If CDate(Data(6)) > CDate(Session("TraDt")) Then
                statusID = 1
            End If
            'Dim statusID          INT,--0-update emp_master,otherwise-1
            Dim EffectiveDate As Date = CDate(Data(6))
            Dim Reporting_To As Integer = CInt(Data(7))
            Dim Prev_Reporting_To As Integer = CInt(Data(8))
            Dim EmpPost As Integer = CInt(Data(9))
            Dim PrevEmpPost As Integer = CInt(Data(10))

            Dim BasicPay As Double = CDbl(Data(11))
            Dim DA As Double = CDbl(Data(12))
            Dim HRA As Double = CDbl(Data(13))
            Dim Conveyance As Double = CDbl(Data(14))
            Dim SA As Double = CDbl(Data(15))
            Dim LA As Double = CDbl(Data(16))
            Dim MA As Double = CDbl(Data(17))
            Dim HA As Double = CDbl(Data(18))
            Dim PA As Double = CDbl(Data(19))
            Dim OA As Double = CDbl(Data(20))
            Dim FA As Double = CDbl(Data(21))
            Dim ESI As Double = CInt(Data(22))
            Dim PF As Double = CInt(Data(23))
            Dim SWF As Double = CInt(Data(24))
            Dim ESWT As Double = CInt(Data(25))
            Dim Charity As Double = CInt(Data(26))
            Dim CadreID As Integer = CInt(Data(27))
            Dim DesignationID As Integer = CInt(Data(28))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(31) As SqlParameter
                Params(0) = New SqlParameter("@EmpID", SqlDbType.Int)
                Params(0).Value = EmpID
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(2).Value = BranchID
                Params(3) = New SqlParameter("@DepartmentID", SqlDbType.Int)
                Params(3).Value = DepartmentID
                Params(4) = New SqlParameter("@PrevBranchID", SqlDbType.Int)
                Params(4).Value = PrevBranchID
                Params(5) = New SqlParameter("@PrevDepartmentID", SqlDbType.Int)
                Params(5).Value = PrevDepartmentID
                Params(6) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
                Params(6).Value = EffectiveDate
                Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(8).Direction = ParameterDirection.Output
                Params(9) = New SqlParameter("@statusID", SqlDbType.Int)
                Params(9).Value = statusID
                Params(10) = New SqlParameter("@Reporting_To", SqlDbType.Int)
                Params(10).Value = Reporting_To
                Params(11) = New SqlParameter("@Prev_Reporting_To", SqlDbType.Int)
                Params(11).Value = Prev_Reporting_To
                Params(12) = New SqlParameter("@EmpPost", SqlDbType.Int)
                Params(12).Value = EmpPost
                Params(13) = New SqlParameter("@PrevEmpPost", SqlDbType.Int)
                Params(13).Value = PrevEmpPost
                Params(14) = New SqlParameter("@BasicPay", SqlDbType.Money)
                Params(14).Value = BasicPay
                Params(15) = New SqlParameter("@DA", SqlDbType.Money)
                Params(15).Value = DA
                Params(16) = New SqlParameter("@HRA", SqlDbType.Money)
                Params(16).Value = HRA
                Params(17) = New SqlParameter("@Conveyance", SqlDbType.Money)
                Params(17).Value = Conveyance
                Params(18) = New SqlParameter("@SpecialAllowance", SqlDbType.Money)
                Params(18).Value = SA
                Params(19) = New SqlParameter("@LocalAllowance", SqlDbType.Money)
                Params(19).Value = LA
                Params(20) = New SqlParameter("@MedicalAllowance", SqlDbType.Money)
                Params(20).Value = MA
                Params(21) = New SqlParameter("@HospitalityAllowance", SqlDbType.Money)
                Params(21).Value = HA
                Params(22) = New SqlParameter("@PerformanceAllowance", SqlDbType.Money)
                Params(22).Value = PA
                Params(23) = New SqlParameter("@OtherAllowance", SqlDbType.Money)
                Params(23).Value = OA
                Params(24) = New SqlParameter("@FieldAllowance", SqlDbType.Money)
                Params(24).Value = FA
                Params(25) = New SqlParameter("@ESI", SqlDbType.Money)
                Params(25).Value = ESI
                Params(26) = New SqlParameter("@PF", SqlDbType.Money)
                Params(26).Value = PF
                Params(27) = New SqlParameter("@SWF", SqlDbType.Money)
                Params(27).Value = SWF
                Params(28) = New SqlParameter("@ESWT", SqlDbType.Money)
                Params(28).Value = ESWT
                Params(29) = New SqlParameter("@Charity", SqlDbType.Money)
                Params(29).Value = Charity
                Params(30) = New SqlParameter("@CadreID", SqlDbType.Int)
                Params(30).Value = CadreID
                Params(31) = New SqlParameter("@DesignationID", SqlDbType.Int)
                Params(31).Value = DesignationID
                DB.ExecuteNonQuery("SP_EMP_TRANSFER", Params)
                ErrorFlag = CInt(Params(7).Value)
                Message = CStr(Params(8).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            If CInt(Data(1)) > 0 Then
                DT_EMP = GF.GET_EMP_DEATILS(CInt(Data(1)))
                hid_data.Value = ""
                If DT_EMP.Rows.Count > 0 Then
                    DTTS = EN.Get_Transfer_Dtls(CInt(Data(1)))
                    hid_Dtls.Value = ""


                    Dim strTS As New StringBuilder

                    Dim dr As DataRow

                    For Each dr In DTTS.Rows
                        strTS.Append("¥")
                        strTS.Append(dr.Item(0).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(1).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(2).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(3).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(4).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(5).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(6).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(7).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(8).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(9).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(10).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(11).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(12).ToString())
                        strTS.Append("µ")
                    Next
                    If DTTS.Rows.Count > 0 Then
                        CallBackReturn = "1~" + CStr(DT_EMP.Rows(0).Item(0)) + "|" + CStr(strTS.ToString.Substring(1))
                    Else
                        CallBackReturn = "1~" + CStr(DT_EMP.Rows(0).Item(0)) + "|"
                    End If
                Else
                    CallBackReturn = "2~" + "Invalid Employee Code!"
                End If

            Else

            End If
        ElseIf CInt(Data(0)) = 3 Then
            DT = EN.GetReportingOfficer(CInt(Data(1)), CInt(Data(2)), CInt(Data(4)), CInt(Data(3)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()

            Next
        ElseIf CInt(Data(0)) = 4 Then
            DT = EN.GetDesignation(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
            

    End Sub
#End Region
End Class
