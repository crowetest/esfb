﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.IO
Partial Class GroupEnrollment
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
    Dim UserID As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            UserID = CInt(Session("UserID"))
            Me.Master.subtitle = "Group Enrollment"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", " window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim NewGroupID As Integer = 0
        Dim Mobileno As String
        Dim EmpCode As Integer = 0
        Dim EmpName As String = ""
        Dim CreatedOTP As String = ""
        Dim Sender As String = ""
        Dim UserName As String = ""
        Dim PassWord As String = ""
        Dim StrUrl As String
        Dim SMS As String = ""
        Dim RetStr() As String
        Dim OTPSEND As String = "0"
        Try
     
            Dim IP As String = CStr(Data(0))
            DT = DB.ExecuteDataSet("select isnull( Max(GROUPID),0) from  Emp_Group_Enrollment").Tables(0)
            If DT.Rows.Count > 0 Then
                NewGroupID = CInt(DT.Rows(0).Item(0)) + 1
            End If
            Dim Params(2) As SqlParameter
            Params(0) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(0).Direction = ParameterDirection.Output
            Params(1) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(1).Direction = ParameterDirection.Output
            Params(2) = New SqlParameter("@GroupID", SqlDbType.Int)
            Params(2).Value = NewGroupID

            DB.ExecuteNonQuery("SP_Enrollment_Group", Params)

            ErrorFlag = CInt(Params(0).Value)
            Message = CStr(Params(1).Value)
            If ErrorFlag = 0 Then

                DT = DB.ExecuteDataSet("SELECT sender,username,password FROM SMS_PORTAL WHERE STATUS_ID =1").Tables(0)
                If DT.Rows.Count > 0 Then

                    Sender = DT.Rows(0).Item(0).ToString()
                    UserName = DT.Rows(0).Item(1).ToString()
                    PassWord = DT.Rows(0).Item(2).ToString()
                End If
                DT = DB.ExecuteDataSet("select [Employee number],[Employee Name],[Personal Mobile Number] from  Emp_Group_Enrollment WHERE GroupID = " & NewGroupID & " ").Tables(0)
                For Each DR As DataRow In DT.Rows

                    EmpCode = CInt(DR(0))
                    EmpName = DR(1).ToString()
                    Mobileno = "91" + DR(2).ToString()
                    CreatedOTP = GF.GenerateOTP(5)

                    '' Send OTP
                    SMS = EmpName & ", Welcome to SFB. Your eweb Username is " & EmpCode & " And Password generated is " & CreatedOTP & "Please login into http://eweb.emfil.org/esfb/ using this credentials."
                    StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & SMS & "&GSM=" & Mobileno & ""

                    RetStr = WEB_Request_Response(StrUrl, 1)

                    If RetStr(6) = "0" Then ' Delivered
                        OTPSEND = CreatedOTP
                        DB.ExecuteNonQuery("UPDATE EMP_MASTER SET PASS_WORD = '" & EncryptDecrypt.Encrypt(OTPSEND, EmpCode.ToString()) & "' WHERE EMP_CODE = " & EmpCode & "")

                        DB.ExecuteNonQuery("INSERT INTO OTP_LOG (EMP_CODE, USER_ID, CHANGE_DT,SMS,MOBILE_NO,IPAddress) VALUES ( " & EmpCode & ", " & UserID & ", getdate(),'" & SMS & "','" & DR(2).ToString() & "','" & IP & "')")
                    End If
                    CreatedOTP = "0"
                Next
            End If
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message
    End Sub
#Region "Function"

    Public Shared Function WEB_Request_Response(Request As String, Request_type As Integer) As String()
        'Added On 22-10-2020 by 40013 - Changed request from IT
        Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
        Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString

        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        If Request_type = 1 Then
            delimiters = {"<", ">", ">/", "</", "/"}
        Else
            'delimiters = {"<", ">", ">/", "</"}
            delimiters = {"=", " "}
        End If
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)


        Return parts.ToArray()


    End Function
#End Region

End Class
