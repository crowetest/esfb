﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="EmployeePromotion.aspx.vb" Inherits="EmployeePromotion" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .tblQal
        {
            border:9px;background-color:#EEB8A6; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style2
     {
         color: #000099;
     }
    </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">


        function RequestID() {
            var type = 1;
            if (document.getElementById("<%= radDemotion.ClientID %>").checked == true)
                type = 0;
              var EmpID = document.getElementById("<%= txtEmpcode.ClientID %>").value;
              if (EmpID > 0) {
                  var ToData = "2Ø" + EmpID + "Ø"+type;
                  ToServer(ToData,2);
              }
          }
         
          function FromServer(arg, context) {

              if (context == 1) {
                  var Data = arg.split("Ø");
                  alert(Data[1]);
                  if (Data[0] == 0) window.open("EmployeePromotion.aspx", "_self");
              }
              else if (context == 2) {
                  var Data = arg.split("~");
                  if (Data[0] == 1) {
                      document.getElementById("<%= hid_data.ClientID %>").value = Data[1];
                      var AllData = document.getElementById("<%= hid_data.ClientID %>").value.split("|");
                      var Dtl = AllData[0].split("^");
                      document.getElementById("<%= hid_Dtls.ClientID %>").value = AllData[1];
                      
                      document.getElementById("<%= txtName.ClientID %>").value = Dtl[1];
                      document.getElementById("<%= hid_branchid.ClientID %>").value = Dtl[2];
                      document.getElementById("<%= txtBranch.ClientID %>").value = Dtl[3];
                      document.getElementById("<%= hid_depid.ClientID %>").value = Dtl[4];
                      document.getElementById("<%= txtDepartment.ClientID %>").value = Dtl[5];
                      document.getElementById("<%= txtDesignation.ClientID %>").value = Dtl[6];
                      document.getElementById("<%= hid_desid.ClientID %>").value = Dtl[7];
                      document.getElementById("<%= hid_ReportingTo.ClientID %>").value = Dtl[8];
                     

                      //SALARY
                      document.getElementById("<%= txtBasicPay.ClientID %>").value = parseFloat(Dtl[9]).toFixed(0);
                      document.getElementById("<%= txtDA.ClientID %>").value = parseFloat(Dtl[10]).toFixed(0);
                      document.getElementById("<%= txtHRA.ClientID %>").value = parseFloat(Dtl[11]).toFixed(0);
                      document.getElementById("<%= txtConveyance.ClientID %>").value = parseFloat(Dtl[12]).toFixed(0);
                      document.getElementById("<%= txtSpecialAllowance.ClientID %>").value = parseFloat(Dtl[13]).toFixed(0);
                      document.getElementById("<%= txtLocalAllowance.ClientID %>").value = parseFloat(Dtl[14]).toFixed(0);
                      document.getElementById("<%= txtMedicalAllowance.ClientID %>").value = parseFloat(Dtl[15]).toFixed(0);
                      document.getElementById("<%= txtHospitality.ClientID %>").value = parseFloat(Dtl[16]).toFixed(0);
                      document.getElementById("<%= txtPerformance.ClientID %>").value = parseFloat(Dtl[17]).toFixed(0);
                      document.getElementById("<%= txtOtherAllowance.ClientID %>").value = parseFloat(Dtl[18]).toFixed(0);
                      document.getElementById("<%= txtFieldAllowance.ClientID %>").value = parseFloat(Dtl[19]).toFixed(0);


                      var TotalEarnings = parseFloat(Dtl[9]) + parseFloat(Dtl[10]) + parseFloat(Dtl[11]) + parseFloat(Dtl[12]) + parseFloat(Dtl[13]) + parseFloat(Dtl[14]) + parseFloat(Dtl[15]) + parseFloat(Dtl[16]) + parseFloat(Dtl[17]) + parseFloat(Dtl[18]) + parseFloat(Dtl[19])

                      var TotalDeductions = parseFloat(Dtl[20]) + parseFloat(Dtl[21]) + parseFloat(Dtl[22]) + parseFloat(Dtl[23]) + parseFloat(Dtl[24]);
                      document.getElementById("<%= txtESI.ClientID %>").value = parseFloat(Dtl[20]).toFixed(0);
                      document.getElementById("<%= txtPF.ClientID %>").value = parseFloat(Dtl[21]).toFixed(0);
                      document.getElementById("<%= txtSWF.ClientID %>").value = parseFloat(Dtl[22]).toFixed(0);
                      document.getElementById("<%= txtESWT.ClientID %>").value = parseFloat(Dtl[23]).toFixed(0);
                      document.getElementById("<%= txtCharity.ClientID %>").value = parseFloat(Dtl[24]).toFixed(0);
                      document.getElementById("<%= cmbEmpPost.ClientID %>").value = Dtl[25];
                      document.getElementById("<%= hdnPrevEmpPost.ClientID %>").value = Dtl[25];
                      

                      document.getElementById("<%= txtTotalEarnings.ClientID %>").value = TotalEarnings;
                      document.getElementById("<%= txtTotalDeductions.ClientID %>").value = TotalDeductions;
           
                      //

                      ComboFill(Data[2], "<%= cmbCadre.ClientID %>");
                      table_fill();
                      document.getElementById("<%= cmbCadre.ClientID %>").focus();
                      var ToData = "5Ø" + Dtl[2] + "Ø" + Dtl[4] + "Ø" + document.getElementById("<%= txtEmpcode.ClientID %>").value + "Ø" + document.getElementById("<%= cmbDesignation.ClientID %>").value;
                      ToServer(ToData, 5);
                  }
                  else if (Data[0] == 2) {
                      
                      document.getElementById("<%= txtName.ClientID %>").value = "";
                      document.getElementById("<%= hid_branchid.ClientID %>").value = "";
                      document.getElementById("<%= txtBranch.ClientID %>").value = "";
                      document.getElementById("<%= hid_depid.ClientID %>").value = "";
                      document.getElementById("<%= txtDepartment.ClientID %>").value = "";
                      document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                      document.getElementById("<%= txtEmpcode.ClientID %>").value = "";
                      document.getElementById("<%= hid_desid.ClientID %>").value = "";
                      document.getElementById("<%= hid_ReportingTo.ClientID %>").value = "";
                      document.getElementById("<%= hid_Dtls.ClientID %>").value = "";

                      document.getElementById("<%= txtBasicPay.ClientID %>").value = 0;
                      document.getElementById("<%= txtDA.ClientID %>").value =0;
                      document.getElementById("<%= txtHRA.ClientID %>").value = 0;
                      document.getElementById("<%= txtConveyance.ClientID %>").value = 0;
                      document.getElementById("<%= txtSpecialAllowance.ClientID %>").value = 0;
                      document.getElementById("<%= txtLocalAllowance.ClientID %>").value = 0;
                      document.getElementById("<%= txtMedicalAllowance.ClientID %>").value = 0;
                      document.getElementById("<%= txtHospitality.ClientID %>").value = 0;
                      document.getElementById("<%= txtPerformance.ClientID %>").value = 0;
                      document.getElementById("<%= txtOtherAllowance.ClientID %>").value = 0;
                      document.getElementById("<%= txtFieldAllowance.ClientID %>").value = 0;

                      
                      document.getElementById("<%= txtESI.ClientID %>").value =0;
                      document.getElementById("<%= txtPF.ClientID %>").value = 0;
                      document.getElementById("<%= txtSWF.ClientID %>").value = 0;
                      document.getElementById("<%= txtESWT.ClientID %>").value = 0;
                      document.getElementById("<%= txtCharity.ClientID %>").value =0;
                      document.getElementById("<%= txtTotalEarnings.ClientID %>").value = 0;
                      document.getElementById("<%= txtTotalDeductions.ClientID %>").value = 0;

                      table_fill();
                      document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                  }
              }
              else if (context == 3) {                 
              ComboFill(arg, "<%= cmbCadre.ClientID %>");
          }
          else if (context == 4) {
              ComboFill(arg, "<%= cmbDesignation.ClientID %>");
          }
          else if (context == 5) {
              ComboFill(arg, "<%= cmbReportingTo.ClientID %>");
          }
        }
        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }
        function btnConfirm_onclick() {
           
           
            if (document.getElementById("<%= txtEmpcode.ClientID %>").value == "") 
            {
                alert("Select Employee Code");
                document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                return false;
            }
           
              if (document.getElementById("<%= txtStartDt.ClientID %>").value == "") 
            {
                alert("Select Effective Date");
                document.getElementById("<%= txtStartDt.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbCadre.ClientID %>").value == "-1") {
                alert("Select Cadre");
                document.getElementById("<%= cmbCadre.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbDesignation.ClientID %>").value == "-1") 
            {
                alert("Select Designation");
                document.getElementById("<%= cmbDesignation.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbReportingTo.ClientID %>").value == "-1") {
                alert("Select Reporting Person");
                document.getElementById("<%= cmbReportingTo.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbEmpPost.ClientID %>").value == "-1") {
                alert("Select Post");
                document.getElementById("<%= cmbEmpPost.ClientID %>").focus();
                return false;
            }
            var EmpID	= document.getElementById("<%= txtEmpcode.ClientID %>").value;	      
	        var EffectiveDate = document.getElementById("<%= txtStartDt.ClientID %>").value;
	        var cadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;
	        var Designationid = document.getElementById("<%= cmbDesignation.ClientID %>").value;	       
            if (document.getElementById("<%= radPromotion.ClientID %>").checked==true){
	            var type = 1;
            }
	        else if (document.getElementById("<%= radDemotion.ClientID %>").checked == true) {
	            var type = 0;
	        }
	        var ReportingTo = document.getElementById("<%= cmbReportingTo.ClientID %>").value;
	        var PrevReportingTo = document.getElementById("<%= hid_ReportingTo.ClientID %>").value;


	        var BasicPay = document.getElementById("<%= txtBasicPay.ClientID %>").value;
	        var DA = (document.getElementById("<%= txtDA.ClientID %>").value == "" || document.getElementById("<%= txtDA.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtDA.ClientID %>").value;
	        var HRA = (document.getElementById("<%= txtHRA.ClientID %>").value == "" || document.getElementById("<%= txtHRA.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtHRA.ClientID %>").value;
	        var Conveyance = (document.getElementById("<%= txtConveyance.ClientID %>").value == "" || document.getElementById("<%= txtConveyance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtConveyance.ClientID %>").value;
	        var SA = (document.getElementById("<%= txtSpecialAllowance.ClientID %>").value == "" || document.getElementById("<%= txtSpecialAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtSpecialAllowance.ClientID %>").value;
	        var LA = (document.getElementById("<%= txtLocalAllowance.ClientID %>").value == "" || document.getElementById("<%= txtLocalAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtLocalAllowance.ClientID %>").value;
	        var MA = (document.getElementById("<%= txtMedicalAllowance.ClientID %>").value == "" || document.getElementById("<%= txtMedicalAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtMedicalAllowance.ClientID %>").value;
	        var HA = (document.getElementById("<%= txtHospitality.ClientID %>").value == "" || document.getElementById("<%= txtHospitality.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtHospitality.ClientID %>").value;
	        var PA = (document.getElementById("<%= txtPerformance.ClientID %>").value == "" || document.getElementById("<%= txtPerformance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtPerformance.ClientID %>").value;
	        var OA = (document.getElementById("<%= txtOtherAllowance.ClientID %>").value == "" || document.getElementById("<%= txtOtherAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtOtherAllowance.ClientID %>").value;
	        var FA = (document.getElementById("<%= txtFieldAllowance.ClientID %>").value == "" || document.getElementById("<%= txtFieldAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtFieldAllowance.ClientID %>").value;
	        var ESI = (document.getElementById("<%= txtESI.ClientID %>").value == "" || document.getElementById("<%= txtESI.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtESI.ClientID %>").value;
	        var PF = (document.getElementById("<%= txtPF.ClientID %>").value == "" || document.getElementById("<%= txtPF.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtPF.ClientID %>").value;
	        var SWF = (document.getElementById("<%= txtSWF.ClientID %>").value == "" || document.getElementById("<%= txtSWF.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtSWF.ClientID %>").value;
	        var ESWT = (document.getElementById("<%= txtESWT.ClientID %>").value == "" || document.getElementById("<%= txtESWT.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtESWT.ClientID %>").value;
	        var Charity = (document.getElementById("<%= txtCharity.ClientID %>").value == "" || document.getElementById("<%= txtCharity.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtCharity.ClientID %>").value;

	        var Post = document.getElementById("<%= cmbEmpPost.ClientID %>").value;

	        var ToData = "1Ø" + EmpID + "Ø" + EffectiveDate + "Ø" + type + "Ø" + Designationid + "Ø" + cadreID + "Ø" + ReportingTo + "Ø" + PrevReportingTo + "Ø" + BasicPay + "Ø" + DA + "Ø" + HRA + "Ø" + Conveyance + "Ø" + SA + "Ø" + LA + "Ø" + MA + "Ø" + HA + "Ø" + PA + "Ø" + OA + "Ø" + FA + "Ø" + ESI + "Ø" + PF + "Ø" + SWF + "Ø" + ESWT + "Ø" + Charity + "Ø" + Post + "Ø" + document.getElementById("<%= hid_branchid.ClientID %>").value + document.getElementById("<%= hid_depid.ClientID %>").value + "Ø" + document.getElementById("<%= hdnPrevEmpPost.ClientID %>").value;
            ToServer(ToData, 1);
        }

        function FillData(Arg) {
            var Row = Arg.split("Ø");
            var Dtl = Row[0].split("µ");
           
            
            //SalaryDetails

            
        }
        function BaicPayOnChange() {
            var BasicPay = parseFloat(document.getElementById("<%= txtBasicPay.ClientID %>").value);
            if (BasicPay > 0) {
               
                var Perc1Amount = parseFloat(BasicPay * (parseFloat(document.getElementById("<%= hdnSFWTRate.ClientID %>").value) / 100));
                
                document.getElementById("<%= txtESWT.ClientID %>").value = Perc1Amount;
                Perc1Amount = Math.round(BasicPay * (parseFloat(document.getElementById("<%= hdnCharityRate.ClientID %>").value / 100)));
                document.getElementById("<%= txtCharity.ClientID %>").value = Perc1Amount;
            }
            CalculateTotalEarnings();
            CalculateTotalDeduction();
        }
        function CalculateTotalEarnings() {
            var BasicPay = document.getElementById("<%= txtBasicPay.ClientID %>").value;
            var DA = (document.getElementById("<%= txtDA.ClientID %>").value == "" || document.getElementById("<%= txtDA.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtDA.ClientID %>").value;
            var HRA = (document.getElementById("<%= txtHRA.ClientID %>").value == "" || document.getElementById("<%= txtHRA.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtHRA.ClientID %>").value;
            var Conveyance = (document.getElementById("<%= txtConveyance.ClientID %>").value == "" || document.getElementById("<%= txtConveyance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtConveyance.ClientID %>").value;
            var SA = (document.getElementById("<%= txtSpecialAllowance.ClientID %>").value == "" || document.getElementById("<%= txtSpecialAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtSpecialAllowance.ClientID %>").value;
            var LA = (document.getElementById("<%= txtLocalAllowance.ClientID %>").value == "" || document.getElementById("<%= txtLocalAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtLocalAllowance.ClientID %>").value;
            var MA = (document.getElementById("<%= txtMedicalAllowance.ClientID %>").value == "" || document.getElementById("<%= txtMedicalAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtMedicalAllowance.ClientID %>").value;
            var HA = (document.getElementById("<%= txtHospitality.ClientID %>").value == "" || document.getElementById("<%= txtHospitality.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtHospitality.ClientID %>").value;
            var PA = (document.getElementById("<%= txtPerformance.ClientID %>").value == "" || document.getElementById("<%= txtPerformance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtPerformance.ClientID %>").value;
            var OA = (document.getElementById("<%= txtOtherAllowance.ClientID %>").value == "" || document.getElementById("<%= txtOtherAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtOtherAllowance.ClientID %>").value;
            var FA = (document.getElementById("<%= txtFieldAllowance.ClientID %>").value == "" || document.getElementById("<%= txtFieldAllowance.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtFieldAllowance.ClientID %>").value;
            document.getElementById("<%= txtTotalEarnings.ClientID %>").value = Math.abs(BasicPay) + Math.abs(DA) + Math.abs(HRA) + Math.abs(Conveyance) + Math.abs(SA) + Math.abs(LA) + Math.abs(MA) + Math.abs(HA) + Math.abs(PA) + Math.abs(OA) + Math.abs(FA)


        }
        function CalculateTotalDeduction() {
            var ESI = (document.getElementById("<%= txtESI.ClientID %>").value == "" || document.getElementById("<%= txtESI.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtESI.ClientID %>").value;
            var PF = (document.getElementById("<%= txtPF.ClientID %>").value == "" || document.getElementById("<%= txtPF.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtPF.ClientID %>").value;
            var SWF = (document.getElementById("<%= txtSWF.ClientID %>").value == "" || document.getElementById("<%= txtSWF.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtSWF.ClientID %>").value;
            var ESWT = (document.getElementById("<%= txtESWT.ClientID %>").value == "" || document.getElementById("<%= txtESWT.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtESWT.ClientID %>").value;
            var Charity = (document.getElementById("<%= txtCharity.ClientID %>").value == "" || document.getElementById("<%= txtCharity.ClientID %>").value == "0") ? 0 : document.getElementById("<%= txtCharity.ClientID %>").value;
            document.getElementById("<%= txtTotalDeductions.ClientID %>").value = Math.abs(ESI) + Math.abs(PF) + Math.abs(SWF) + Math.abs(ESWT) + Math.abs(Charity);
        }


        function TypeOnChange() {

            var type = 1;
            ClearCombo("<%= cmbDesignation.ClientID %>")
            if (document.getElementById("<%= radDemotion.ClientID %>").checked == true)
                type = 0;
            var EmpID = document.getElementById("<%= txtEmpcode.ClientID %>").value;
            if (document.getElementById("<%= txtName.ClientID %>").value != "") {
                var ToData = "3Ø" + EmpID + "Ø" + type;                
                ToServer(ToData, 3);
            }
        }
        function CadreOnChange() {
            var CadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;
            var ToData = "4Ø" + CadreID;          
            ToServer(ToData, 4);

        }
        function DesgOnChange() {
            var CadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;
            var ToData = "5Ø" + document.getElementById("<%= hid_desid.ClientID %>").value + "Ø" + document.getElementById("<%= hid_branchid.ClientID %>").value + "Ø" + document.getElementById("<%= txtEmpcode.ClientID %>").value + "Ø" + document.getElementById("<%= cmbDesignation.ClientID %>").value;
            ToServer(ToData, 5);

        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function table_fill() {

            //            if (document.getElementById("<%= hid_Dtls.ClientID %>").value == "") {
            //                document.getElementById("<%= pnHistory.ClientID %>").innerHTML = "";
            //                document.getElementById("<%= pnHistory.ClientID %>").style.display = "none";
            //               
            //                return;
            //            }
            document.getElementById("<%= pnHistory.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";

            tab += "<div style='width:100%; height:170px;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
            tab += "<tr class=mainhead>";
            tab += "<td style='width:5%;text-align:center'>Sl No</td>";
            tab += "<td style='width:14%;text-align:center' >Date</td>";
            tab += "<td style='width:13%;text-align:left' >Previous Designation</td>";
            tab += "<td style='width:13%;text-align:center'>Current Designation</td>";
            tab += "<td style='width:13%;text-align:center'>Previous Reporting</td>";
            tab += "<td style='width:13%;text-align:center'>Current Reporting</td>";
            tab += "<td style='width:13%;text-align:center'>Previous Post</td>";
            tab += "<td style='width:13%;text-align:center'>Current Post</td>";
            tab += "</tr>";

            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                if (document.getElementById("<%= hid_Dtls.ClientID %>").value != "") {
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;


                    tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                    tab += "<td style='width:14%;text-align:center' ><a href='../Reports/viewSalary.aspx?Trans_id=" + btoa(col[7]) + "' target=_blank>" + col[0] + "</a></td>";
                    tab += "<td style='width:13%;text-align:left' >" + col[1] + "</td>";
                    tab += "<td style='width:13%;text-align:center'>" + col[2] + "</td>";
                    tab += "<td style='width:13%;text-align:center'>" + col[3] + "</td>";
                    tab += "<td style='width:13%;text-align:center'>" + col[4] + "</td>";
                    tab += "<td style='width:13%;text-align:left' >" + col[5] + "</td>";
                    tab += "<td style='width:13%;text-align:center'>" + col[6] + "</td>";
                  


                    tab += "</tr>";
                }
            }
            tab += "</table><div><div>";
            //alert(tab);
            document.getElementById("<%= pnHistory.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//


        }
    </script>
   
</head>
</html>
    <asp:HiddenField ID="hid_depid" runat="server" />
    <asp:HiddenField ID="hid_data" runat="server" />
     <asp:HiddenField ID="hid_desid" runat="server" />
<br />
<br />
 <table class="style1" style="width:80%;margin: 0px auto;">
 <tr> 
 <td style="width:25%;">
                   <asp:HiddenField ID="hdnESI" runat="server" />
                    </td>
 <td style="width:12%; text-align:left;"></td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:RadioButton ID="radPromotion" 
                 runat="server" Text="Promotion" GroupName="rad" />&nbsp; &nbsp;
            <asp:RadioButton ID="radDemotion" runat="server" Text="Demotion" GroupName="rad" />
            </td>
    </tr>
    <tr> <td style="width:25%;">
                   <asp:HiddenField ID="hdnPF" runat="server" />
                    </td>
    <td style="width:12%; text-align:left;">Employee Code</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpcode" class="NormalText" runat="server" Width="15%" 
                MaxLength="5"></asp:TextBox>
            </td>
    </tr>
        <tr> <td style="width:25%;">
                   <asp:HiddenField ID="hdnSFWTRate" runat="server" />
                    </td>
        <td style="width:12%; text-align:left;">Name</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:25%;">
                    <asp:HiddenField ID="hdnCharityRate" runat="server" />
               </td>
       <td style="width:12%; text-align:left;">Location</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> <td style="width:25%;">
                   <asp:HiddenField ID="hdnESWT" runat="server" />
                    </td>
       <td style="width:12%; text-align:left;">Department</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDepartment" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> <td style="width:25%;">
                   <asp:HiddenField ID="hdnPrevEmpPost" runat="server" />
                    </td>
       <td style="width:12%; text-align:left;">Designation</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesignation" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
        <tr><td style="width:25%;text-align:center;" colspan="3" class="style2" ><strong>
            Promotion/Demotion Details</strong></td>
       
            
       </tr>
       
        <tr><td style="width:25%;">
                   <asp:HiddenField ID="hid_Dtls" runat="server" />
                    </td>
        <td style="width:12%; text-align:left;">Cadre</td>
            <td style="width:63%">
                 &nbsp; &nbsp;<asp:DropDownList ID="cmbCadre" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="60%" ForeColor="Black">
                     <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
            </asp:DropDownList>
            </td>
            
       </tr>
       
        <tr><td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Designation</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbDesignation" class="NormalText" style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="60%" ForeColor="Black">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
            </asp:DropDownList>
            </td>
            
       </tr>
        <tr><td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Post</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:DropDownList 
                    ID="cmbEmpPost" runat="server" class="NormalText" Width="60%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
            
       </tr>
        <tr><td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Reporting To</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbReportingTo" runat="server" class="NormalText" Width="60%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
            
       </tr> 
      
        <tr> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">EffectiveDate</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtStartDt" class="NormalText" runat="server" 
                    Width="15%"  ReadOnly="true" ></asp:TextBox>
             <asp:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtStartDt" Format="dd MMM yyyy">
             </asp:CalendarExtender>
            </td>
       </tr>
       <tr> 
     
            <td style="width:63%" colspan="3">
               <table align="center" style="width: 100%" >                                    
                    <tr>
                        <td style="width:12% ;text-align:center;" colspan="6" class="tblQal" >
                          EARNINGS </td>
                         <td style="width:12% ;text-align:center;"  colspan="2" class="tblQal">
                           DEDUCTIONS</td>
                    </tr>
                 
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                Basic Pay</td>
                       <td style="width:13% ;text-align:left;">
                             &nbsp;<asp:TextBox ID="txtBasicPay" runat="server" 
                    Width="80%" class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            DA</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtDA" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            HRA</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtHRA" runat="server" 
                    Width="80%" class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:13% ;text-align:left;">
                            ESI</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtESI" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            </td>
                    </tr>
                  
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            <span>Conveyance</span></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtConveyance" runat="server" 
                    Width="80%" class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            </td>
                        <td style="width:12% ;text-align:left;">
                            Special Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtSpecialAllowance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            Local Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtLocalAllowance" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            &nbsp;PF</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtPF" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            </td>
                    </tr>
                  
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            <span>Performance&nbsp;Allowance</span></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtPerformance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Other Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtOtherAllowance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Medical&nbsp;Allowance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtMedicalAllowance" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Staff
                Welfare Fund</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtSWF" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                  
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            <span>Hospitality&nbsp;Allowance</span></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtHospitality" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            Field Allowance</td>
                        <td style="width:13% ;text-align:left;">
                             &nbsp;<asp:TextBox ID="txtFieldAllowance" runat="server" Width="80%" 
                                class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                             &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            &nbsp;</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            ESWT 1%</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtESWT" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                  
                   
                      <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;" colspan="6">
                            &nbsp;</td>                       
                        <td style="width:12% ;text-align:left;">
                            CharityFund 1%</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtCharity" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                   <tr class="tblQal">
                        <td style="width:12% ;text-align:left;" colspan="4">
                            &nbsp;</td>
                       
                        <td style="width:12% ;text-align:left;">
                           <strong>Total(+)</strong></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtTotalEarnings" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" 
                                ReadOnly="True" style="color:#CC0000; font-weight:bold;"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:12% ;text-align:left;">
                            <strong>Total(-)</strong></td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtTotalDeductions" runat="server" Width="80%" 
                    class="NormalText" MaxLength="6" onkeypress="return NumericCheck(event);" 
                                ReadOnly="True" style="color:#CC0000; font-weight:bold;"></asp:TextBox>
                            &nbsp;</td>
                    </tr>

                      </table>
            </td>


       </tr>
      <tr>
       <td style="width:100%;" colspan="3">
       <asp:Panel ID="pnHistory" runat="server">
            </asp:Panel>
       </td>
       </tr>
       
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE" onclick="return btnConfirm_onclick()"/>&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()"/><asp:HiddenField 
                    ID="hid_ReportingTo" runat="server" />
            &nbsp;<asp:HiddenField ID="hid_branchid" runat="server" />
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br />
</asp:Content>

