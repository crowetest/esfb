﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Web.Services
Imports System.Web.UI.WebControls
Imports System.Web.Script.Serialization
Imports System.Web
Imports System.Web.UI
Partial Class Enrollment_Enrollment
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DT As New DataTable
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 20) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Employee Enrollment"
            GN.ComboFill(cmbBranch, EN.GetBranch(), 0, 1)
            GN.ComboFill(cmbDepartment, EN.GetDepartment(), 0, 1)
            GN.ComboFill(cmbCadre, EN.GetCadre(), 0, 1)
            GN.ComboFill(cmbState, GN.GetState(), 0, 1)
            GN.ComboFill(cmbGender, EN.GetGender(), 0, 1)
            GN.ComboFill(cmbMaritalStatus, GN.GetMaritalStatus(), 0, 1)
            GN.ComboFill(cmbIDProof, GN.GetIDProof(), 0, 1)
            GN.ComboFill(cmbAddressProof, GN.GetAddressProof(), 0, 1)
            GN.ComboFill(cmbEmpPost, GN.GetEmpPost(), 0, 1)
            GN.ComboFill(cmbEmpType, GN.GetEmployeeStatus(), 0, 1)
            GN.ComboFill(cmbSuretyType, GN.GetFamily(), 0, 1)
            GN.ComboFill(cmbEntity, GN.GetEntity(), 0, 1)
            cmbEntity.SelectedValue = 2
            hdnCharityRate.Value = GN.GetParameterValue(8, 1)
            hdnESIRate.Value = GN.GetParameterValue(5, 1)
            hdnPFRate.Value = GN.GetParameterValue(6, 1)
            hdnSFWTRate.Value = GN.GetParameterValue(7, 1)
            hdnWelfareAmount.Value = GN.GetParameterValue(4, 1)
            GN.ComboFill(cmbBloodGroup, EN.Get_BloodGroup(), 0, 1)
            GN.ComboFill(cmbReligion, EN.Get_RELIGION(), 0, 1)
            GN.ComboFill(cmbMotherTongue, EN.Get_LANGUAGE(), 0, 1)
            DT = EN.Get_LANGUAGE()
            For Each DR As DataRow In DT.Rows
                hdnLang.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = GN.GetIDProof()
            For Each DR As DataRow In DT.Rows
                hdnIDProofVal.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = GN.GetAddressProof()
            For Each DR As DataRow In DT.Rows
                hdnAddressProofVal.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = GN.GetFamily()
            For Each DR As DataRow In DT.Rows
                hdnFamilyVal.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            txtSWF.Text = hdnWelfareAmount.Value
            DT = GN.GetQualification()
            hdnQualification.Value = ""
            hdnUniversity.Value = ""
            hdnQualificationDtl.Value = "¥-1µ-1µµ"
            hdnExperience.Value = "¥µµµ"
            hdnIDProof.Value = "¥-1µµ"
            hdnAddressProof.Value = "¥-1µµ"
            hdnFamily.Value = "¥-1µµµ"
            For Each DR As DataRow In DT.Rows
                hdnQualification.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = GN.GetUniversity()
            For Each DR As DataRow In DT.Rows
                hdnUniversity.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- Register Client Side Functions ---//
            Me.cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            Me.cmbState.Attributes.Add("onchange", "StateOnChange()")
            Me.cmbDistrict.Attributes.Add("onchange", "DistrictOnChange()")
            ' Me.cmbPost.Attributes.Add("onchange", "PostOnChange()")
            Me.txtBasicPay.Attributes.Add("onchange", "BaicPayOnChange()")
            Me.cmbDepartment.Attributes.Add("onchange", "ChangeReportingOfficer()")
            Me.cmbDesignation.Attributes.Add("onchange", "ChangeReportingOfficer()")
            Me.cmbCadre.Attributes.Add("onchange", "return CadreOnChange()")
            Me.cmbMaritalStatus.Attributes.Add("onchange", "MaritalStatusOnChange()")
            Me.txtDA.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtHRA.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtConveyance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtSpecialAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtLocalAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtMedicalAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtHospitality.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtFieldAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtPerformance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtOtherAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtESI.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtPF.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtSWF.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtESWT.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtCharity.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.cmbReligion.Attributes.Add("onchange", "ReligionOnChange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "Fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If Data(0) = 1 Then
            Dim EmpCode As Integer = CInt(Data(1))
            Dim EmpName As String = CStr(Data(2))
            Dim BranchID As Integer = CInt(Data(3))
            Dim DepartmentID As Integer = CInt(Data(4))
            Dim CadreID As Integer = CInt(Data(5))
            Dim DesignationID As Integer = CInt(Data(6))
            Dim DateOfJoin As Date = CDate(Data(7))
            Dim GenderID As Integer = CInt(Data(8))
            Dim ReportingTo As Integer = CInt(Data(9))
            Dim IsFieldStaff As Integer = CInt(Data(10))
            Dim CareOf As String = Data(11)
            Dim HouseName As String = Data(12)
            Dim Location As String = Data(13)
            Dim City As String = Data(14)
            Dim PostID As Integer = CInt(Data(15))
            Dim Email As String = Data(16)
            Dim Landline As String = Data(17)
            Dim Mobile As String = Data(18)
            Dim BasicPay As Double = CDbl(Data(19))
            Dim DA As Double = CDbl(Data(20))
            Dim HRA As Double = CDbl(Data(21))
            Dim Conveyance As Double = CDbl(Data(22))
            Dim SA As Double = CDbl(Data(23))
            Dim LA As Double = CDbl(Data(24))
            Dim MA As Double = CDbl(Data(25))
            Dim HA As Double = CDbl(Data(26))
            Dim PA As Double = CDbl(Data(27))
            Dim OA As Double = CDbl(Data(28))
            Dim FA As Double = CDbl(Data(29))
            Dim ESI As Double = CInt(Data(30))
            Dim PF As Double = CInt(Data(31))
            Dim SWF As Double = CInt(Data(32))
            Dim ESWT As Double = CInt(Data(33))
            Dim Charity As Double = CInt(Data(34))
            Dim DOB As Date = CDate(Data(35))
            Dim MStatusID As Integer = CInt(Data(36))
            Dim CareOfPhone As String = Data(37)
            Dim IDProofType As Integer = CInt(Data(38))
            Dim IDProofNumber As String = Data(39)
            Dim AddressProofType As Integer = CInt(Data(40))
            Dim AddressProofNumber As String = Data(41)
            Dim QualificationDtl As String = Data(42).ToString()
            If QualificationDtl <> "" Then
                QualificationDtl = QualificationDtl.Substring(1)
            End If '+Father,Mother,ReligionId,CasteId,IsTwowheeler,DrivingNo,IsSangamMember,MarriageDate+"Ø" +MotherTongueId,BloodGroupId,Cug,OfficialEmail,ChkPanch,chkComm,chkPolice,newstrExp,newstrID,newstrAddress;
            Dim EmpPost As Integer = CInt(Data(43))
            Dim Father As String = CStr(Data(44))
            Dim Mother As String = CStr(Data(45))
            Dim ReligionId As Integer = CInt(Data(46))
            Dim CasteId As Integer = CInt(Data(47))
            Dim IsTwowheeler As Integer = CInt(Data(48))
            Dim DrivingNo As String = CStr(Data(49))
            Dim IsSangamMember As Integer = CInt(Data(50))
            Dim MarriageDate As String = CStr(Data(51))
            Dim MotherTongueId As Integer = CInt(Data(52))
            Dim BloodGroupId As Integer = CInt(Data(53))
            Dim Cug As String = CStr(Data(54))
            Dim OfficialEmail As String = CStr(Data(55))
            Dim ChkPanch As Integer = CInt(Data(56))
            Dim chkComm As Integer = CInt(Data(57))
            Dim chkPolice As Integer = CInt(Data(58))
            Dim newstrExp As String = CStr(Data(59))
            If newstrExp <> "" Then
                newstrExp = newstrExp.Substring(1)
            End If
            Dim newstrID As String = CStr(Data(60))
            If newstrID <> "" Then
                newstrID = newstrID.Substring(1)
            End If
            Dim newstrAddress As String = CStr(Data(61))
            If newstrAddress <> "" Then
                newstrAddress = newstrAddress.Substring(1)
            End If
            Dim LangKnown As String = CStr(Data(62))
            Dim EmpType As Integer = CInt(Data(63))
            Dim SuretyType As Integer = CInt(Data(64))
            Dim SuretyName As String = CStr(Data(65))
            Dim SuretyAddr As String = CStr(Data(66))
            Dim SuretyPhone As String = CStr(Data(67))
            Dim Entity As Integer = CInt(Data(68))
            Dim newstrFamily As String = CStr(Data(69))
            If newstrFamily <> "" Then
                newstrFamily = newstrFamily.Substring(1)
            End If
            Dim ip As String = CStr(Data(70))
            Dim UserID As Integer = CInt(Session("UserID"))

            'Modified for OTP sending ----------------
            Dim DB As New MS_SQL.Connect
            Dim RetStr() As String

            Dim CreatedOTP As String = GN.GenerateOTP(5)

            Dim OTPSEND As String = "0"
            Dim Count As Integer = 0
            Dim StrUrl As String

            Dim Mobileno As String = ""
            Dim SMS As String = ""
            Dim DT_EMP As New DataTable
            If Cug <> "" Then
                Mobileno = Cug
            ElseIf Mobile <> "" Then
                Mobileno = Mobile
            End If
            If Mobileno <> "" Then
                Mobileno = "91" + Mobileno.ToString()
                DT = DB.ExecuteDataSet("SELECT sender,username,password FROM SMS_PORTAL WHERE STATUS_ID =1").Tables(0)
                If DT.Rows.Count > 0 Then
                    Dim Sender As String = ""
                    Dim UserName As String = ""
                    Dim PassWord As String = ""
                    Sender = DT.Rows(0).Item(0).ToString()
                    UserName = DT.Rows(0).Item(1).ToString()
                    PassWord = DT.Rows(0).Item(2).ToString()
                    '' Send OTP
                    SMS = EmpName & ", Welcome to SFB. Your eweb Username is " & EmpCode & " And Password generated is " & CreatedOTP
                    StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & SMS & "&GSM=" & Mobileno & ""

                    RetStr = WEB_Request_Response(StrUrl, 1)

                    If RetStr(6) = "0" Then ' Delivered

                        OTPSEND = CreatedOTP

                    End If
                End If
            End If
            CallBackReturn = EN.EnrollEmployee(EmpCode, EmpName, BranchID, DepartmentID, CadreID, DesignationID, DateOfJoin, GenderID, ReportingTo, IsFieldStaff, UserID, CareOf, HouseName, Location, City, PostID, Email, Landline, Mobile, BasicPay, DA, HRA, Conveyance, SA, LA, MA, HA, PA, OA, FA, ESI, PF, SWF, ESWT, Charity, DOB, MStatusID, CareOfPhone, IDProofType, IDProofNumber, AddressProofType, AddressProofNumber, QualificationDtl, EmpPost, Father, Mother, ReligionId, CasteId, IsTwowheeler, DrivingNo, IsSangamMember, MarriageDate, MotherTongueId, BloodGroupId, Cug, OfficialEmail, ChkPanch, chkComm, chkPolice, newstrExp, newstrID, newstrAddress, LangKnown, EmpType, SuretyType, SuretyName, SuretyAddr, SuretyPhone, Entity, newstrFamily, OTPSEND, SMS, ip)
        ElseIf Data(0) = 2 Then
            DT = EN.GetReportingOfficer(CInt(Data(1)), CInt(Data(2)), CInt(Data(3)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 4 Then
            DT = EN.GetDesignation(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 5 Then
            DT = GN.GetDistrict(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 6 Then
            DT = GN.GetPost(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 7 Then
            DT = EN.Get_CASTE(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub
#End Region

#Region "Function"

    Public Shared Function WEB_Request_Response(Request As String, Request_type As Integer) As String()

        'Added On 22-10-2020 by 40013 - Changed request from IT
        Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
        Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString

        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        If Request_type = 1 Then
            delimiters = {"<", ">", ">/", "</", "/"}
        Else
            'delimiters = {"<", ">", ">/", "</"}
            delimiters = {"=", " "}
        End If
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)


        Return parts.ToArray()


    End Function
#End Region
End Class
