﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class EmployeePromotion
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 19) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Employee Promotion/Demotion"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            GF.ComboFill(cmbEmpPost, GF.GetEmpPost(), 0, 1)
            Me.txtEmpcode.Attributes.Add("onchange", "return RequestID()")
            Me.radDemotion.Attributes.Add("onclick", "return TypeOnChange()")
            Me.radPromotion.Attributes.Add("onclick", "return TypeOnChange()")
            Me.cmbCadre.Attributes.Add("onchange", "return CadreOnChange()")
            Me.cmbDesignation.Attributes.Add("onchange", "return DesgOnChange()")
            Me.txtBasicPay.Attributes.Add("onchange", "BaicPayOnChange()")
            Me.txtDA.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtHRA.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtConveyance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtSpecialAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtLocalAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtMedicalAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtHospitality.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtFieldAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtPerformance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            Me.txtOtherAllowance.Attributes.Add("onchange", "CalculateTotalEarnings()")
            'General Parameters 4-ESI,5-PF,6-SWF,7-ESWT,8-Charity
            'hdnESI.Value = GF.GetParameterValue(4, 1)
            'hdnPF.Value = GF.GetParameterValue(5, 1)
            hdnSFWTRate.Value = GF.GetParameterValue(7, 1)
            'hdnESWT.Value = GF.GetParameterValue(7, 1)
            hdnCharityRate.Value = GF.GetParameterValue(8, 1)

            Me.txtESI.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtPF.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtSWF.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtESWT.Attributes.Add("onchange", "CalculateTotalDeduction()")
            Me.txtCharity.Attributes.Add("onchange", "CalculateTotalDeduction()")
            radPromotion.Checked = True
            txtEmpcode.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EmpID As Integer = CInt(Data(1))
        Dim EN As New Enrollment
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim Type As Integer = CInt(Data(3))
            Dim Designation_ID As Integer = CInt(Data(4))
            Dim CadreID As Integer = CInt(Data(5))
            Dim EffectiveDate As Date = CDate(Data(2))
            Dim ReportingTo As Integer = CInt(Data(6))
            Dim PrevReportingTo As Integer = CInt(Data(7))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim BasicPay As Double = CDbl(Data(8))
            Dim DA As Double = CDbl(Data(9))
            Dim HRA As Double = CDbl(Data(10))
            Dim Conveyance As Double = CDbl(Data(11))
            Dim SA As Double = CDbl(Data(12))
            Dim LA As Double = CDbl(Data(13))
            Dim MA As Double = CDbl(Data(14))
            Dim HA As Double = CDbl(Data(15))
            Dim PA As Double = CDbl(Data(16))
            Dim OA As Double = CDbl(Data(17))
            Dim FA As Double = CDbl(Data(18))
            Dim ESI As Double = CInt(Data(19))
            Dim PF As Double = CInt(Data(20))
            Dim SWF As Double = CInt(Data(21))
            Dim ESWT As Double = CInt(Data(22))
            Dim Charity As Double = CInt(Data(23))
            Dim EmpPost As Integer = CInt(Data(24))
            Dim PrevEmpPost As Integer = CInt(Data(26))
            Try
                Dim Params(29) As SqlParameter
                Params(0) = New SqlParameter("@EmpID", SqlDbType.Int)
                Params(0).Value = EmpID
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@type", SqlDbType.Int)
                Params(2).Value = Type
                Params(3) = New SqlParameter("@CadreID", SqlDbType.Int)
                Params(3).Value = CadreID
                Params(4) = New SqlParameter("@DesignationID", SqlDbType.Int)
                Params(4).Value = Designation_ID
                Params(5) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
                Params(5).Value = EffectiveDate
                Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@Reporting_To", SqlDbType.Int)
                Params(8).Value = ReportingTo
                Params(9) = New SqlParameter("@Prev_Reporting_To", SqlDbType.Int)
                Params(9).Value = PrevReportingTo
                Params(10) = New SqlParameter("@BasicPay", SqlDbType.Money)
                Params(10).Value = BasicPay
                Params(11) = New SqlParameter("@DA", SqlDbType.Money)
                Params(11).Value = DA
                Params(12) = New SqlParameter("@HRA", SqlDbType.Money)
                Params(12).Value = HRA
                Params(13) = New SqlParameter("@Conveyance", SqlDbType.Money)
                Params(13).Value = Conveyance
                Params(14) = New SqlParameter("@SpecialAllowance", SqlDbType.Money)
                Params(14).Value = SA
                Params(15) = New SqlParameter("@LocalAllowance", SqlDbType.Money)
                Params(15).Value = LA
                Params(16) = New SqlParameter("@MedicalAllowance", SqlDbType.Money)
                Params(16).Value = MA
                Params(17) = New SqlParameter("@HospitalityAllowance", SqlDbType.Money)
                Params(17).Value = HA
                Params(18) = New SqlParameter("@PerformanceAllowance", SqlDbType.Money)
                Params(18).Value = PA
                Params(19) = New SqlParameter("@OtherAllowance", SqlDbType.Money)
                Params(19).Value = OA
                Params(20) = New SqlParameter("@FieldAllowance", SqlDbType.Money)
                Params(20).Value = FA
                Params(21) = New SqlParameter("@ESI", SqlDbType.Money)
                Params(21).Value = ESI
                Params(22) = New SqlParameter("@PF", SqlDbType.Money)
                Params(22).Value = PF
                Params(23) = New SqlParameter("@SWF", SqlDbType.Money)
                Params(23).Value = SWF
                Params(24) = New SqlParameter("@ESWT", SqlDbType.Money)
                Params(24).Value = ESWT
                Params(25) = New SqlParameter("@Charity", SqlDbType.Money)
                Params(25).Value = Charity
                Params(26) = New SqlParameter("@EmpPost", SqlDbType.Int)
                Params(26).Value = EmpPost
                Params(27) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(27).Value = CInt(Data(25))
                Params(28) = New SqlParameter("@DepartmentID", SqlDbType.Int)
                Params(28).Value = CInt(Data(26))
                Params(29) = New SqlParameter("@PrevEmpPost", SqlDbType.Int)
                Params(29).Value = PrevEmpPost
                DB.ExecuteNonQuery("SP_EMP_PROMOTION", Params)
                ErrorFlag = CInt(Params(6).Value)
                Message = CStr(Params(7).Value)
                If ErrorFlag = 0 Then
                    Dim ToAddress As String = "hena.ek@esafmicrofin.com"
                    Dim EmpName As String = ""
                    Dim tt As String = SM.GetEmailAddress(EmpID, 1)
                    Dim Val = Split(tt, "^")
                    If Val(0).ToString <> "" Then
                        ToAddress = Val(0)
                    End If
                    If Val(1).ToString <> "" Then
                        EmpName = Val(1).ToString
                    End If
                    Dim ccAddress As String = ""
                    Dim tt1 As String = SM.GetEmailAddress(EmpID, 2)
                    Dim Val1 = Split(tt, "^")
                    If Val1(0).ToString <> "" Then
                        ccAddress = Val1(0)
                    End If
                    If ccAddress.Trim() <> "" Then
                        ccAddress += ","
                    End If
                    Dim StrVal As String = ""
                    If Type = 1 Then
                        StrVal = "Promotion"
                    Else
                        StrVal = "Demotion"
                    End If
                    Dim Content As String = vbLf & vbLf & " Dear " + EmpName + ", " & vbLf & vbLf
                    If Type = 1 Then
                        Content += "  With profound happiness, we wish to inform you that it has been decided to promote you into the next Grade With Effect From " + EffectiveDate.ToString("dd MMM yyyy") + "."
                    Else
                        Content += "  We wish to inform you that it has been decided to demote you With Effect From " + EffectiveDate.ToString + "."
                        Content = Content & Convert.ToString(vbLf & vbLf & " With Regards" & vbLf & vbLf & "HR Team" & vbLf & "Head, HR")
                        SM.SendMail(ToAddress, StrVal, Content, ccAddress & Convert.ToString("nisha.rathnan@esafmicrofin.com"))
                    End If
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            If CInt(Data(1)) > 0 Then
                DT_EMP = GF.GET_EMP_DEATILS(CInt(Data(1)))
                hid_data.Value = ""
                If DT_EMP.Rows.Count > 0 Then
                    DTTS = EN.Get_Promotion_Dtls(CInt(Data(1)))
                    hid_Dtls.Value = ""
                    Dim strTS As New StringBuilder
                    Dim dr As DataRow
                    For Each dr In DTTS.Rows
                        strTS.Append("¥")
                        strTS.Append(dr.Item(0).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(1).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(2).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(3).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(4).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(5).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(6).ToString())
                        strTS.Append("µ")
                        strTS.Append(dr.Item(7).ToString())
                        strTS.Append("µ")
                    Next
                    If DTTS.Rows.Count > 0 Then
                        CallBackReturn = "1~" + CStr(DT_EMP.Rows(0).Item(0)) + "|" + CStr(strTS.ToString.Substring(1)) + "~"
                    Else
                        CallBackReturn = "1~" + CStr(DT_EMP.Rows(0).Item(0)) + "|" + "~"
                    End If
                Else
                    CallBackReturn = "2~" + "Invalid Employee Code!" + "~"
                End If
                DT = EN.GetCadreForPromotion(CInt(Data(1)), CInt(Data(2)))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            End If
        ElseIf CInt(Data(0)) = 3 Then
            DT = EN.GetCadreForPromotion(CInt(Data(1)), CInt(Data(2)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 4 Then
            DT = EN.GetDesignation(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 5 Then
            DT = EN.GetReportingOfficer(CInt(Data(1)), CInt(Data(2)), CInt(Data(4)), CInt(Data(3)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub
#End Region
End Class
