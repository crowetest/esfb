﻿Partial Class HRM_Posting_ChangePhoto
    Inherits System.Web.UI.Page
    Dim EN As New Enrollment
    Dim EmpCode As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            EmpCode = CInt(GF.Decrypt(Request.QueryString.Get("EmpCode")))
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim myFile As HttpPostedFile = fup1.PostedFile
            Dim nFileLen As Integer = myFile.ContentLength
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                AttachImg = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, nFileLen)
            End If
            Dim Result As String
            Try
                Result = EN.UpdateEmployeePhoto(EmpCode, AttachImg, ContentType)
            Catch
                Result = "1ØError"
            End Try
            Dim arr() = Result.Split(CChar("Ø"))
            If CDbl(arr(0)) = 0 Then
                cl_script1.Append("         alert('" + arr(1) + "');window.close();")
            Else
                cl_script1.Append("         alert('" + arr(1) + "');")
            End If
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
