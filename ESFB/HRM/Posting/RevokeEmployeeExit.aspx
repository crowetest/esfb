﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="RevokeEmployeeExit.aspx.vb" Inherits="Posting_RevokeEmployeeExit" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
    <script src="../../Script/Calculations.js" language="javascript" type="text/javascript"> </script> 
  <script language="javascript" type="text/javascript">
      function btnExit_onclick() 
      {
          window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
      }

      function EmployeeOnChange() 
      {
          var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
          if (EmpCode != "")
              ToServer("1Ø" + EmpCode, 1);
      }

      function FromServer(Arg, Context) {

          switch (Context) {

              case 1:
                  {
                      if (Arg == "ØØ") {
                            alert("Check Employee Code "); 
                            document.getElementById("<%= txtName.ClientID %>").value        = "";
                            document.getElementById("<%= txtBranch.ClientID %>").value      = "";
                            document.getElementById("<%= txtDepartment.ClientID %>").value  = "";
                            document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                            document.getElementById("<%= txtDateOfJoin.ClientID %>").value  = "";
                            document.getElementById("<%= txtType.ClientID %>").value        = "";
                            document.getElementById("<%= txtReason.ClientID %>").value      = "";
                            document.getElementById("<%= txtEffDt.ClientID %>").value       = "";
                            document.getElementById("<%= txtEnteredDt.ClientID %>").value   = "";
                            document.getElementById("<%= txtEnteredBy.ClientID %>").value   = "";
                            document.getElementById("<%= txtEmpCode.ClientID %>").value     = "";
                            document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                            return false;
                        }
                        var Data = Arg.split("Ø");
                        document.getElementById("<%= txtName.ClientID %>").value        = Data[0];
                        document.getElementById("<%= txtBranch.ClientID %>").value      = Data[1];
                        document.getElementById("<%= txtDepartment.ClientID %>").value  = Data[2];
                        document.getElementById("<%= txtDesignation.ClientID %>").value = Data[3];
                        document.getElementById("<%= txtDateOfJoin.ClientID %>").value  = Data[4];
                        document.getElementById("<%= txtType.ClientID %>").value        = Data[5];
                        document.getElementById("<%= txtReason.ClientID %>").value      = Data[6];
                        document.getElementById("<%= txtEffDt.ClientID %>").value       = Data[7];
                        document.getElementById("<%= txtEnteredDt.ClientID %>").value   = Data[8];
                        document.getElementById("<%= txtEnteredBy.ClientID %>").value   = Data[9];
                        document.getElementById("<%= hdnStatus.ClientID %>").value      = Data[11];
                        if(Data[11]==2)
                            document.getElementById("rowSuspension").style.display      = '';
                        else
                            document.getElementById("rowSuspension").style.display      = 'none';
                      break;
                  }
              case 2:
                  {
                      var Data = Arg.split("Ø");
                      alert(Data[1]);
                      if (Data[0] == 0) window.open("RevokeEmployeeExit.aspx", "_self");
                      break;
                  }

          }

      }
      function btnSave_onclick() {
          if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "")
          { alert("Enter a Valid Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false; }
          if (document.getElementById("<%= txtRemarks.ClientID %>").value == "")
          { alert("Enter Remarks"); document.getElementById("<%= txtReason.ClientID %>").focus(); return false; }
          var Status = document.getElementById("<%= hdnStatus.ClientID %>").value;
          if (Status == 2)
          {
                if(document.getElementById("<%= txtRevokeDt.ClientID %>").value == "")
                {alert("Select Suspension Revoke Date");document.getElementById("<%= txtRevokeDt.ClientID %>").focus();return false;}
          }
          var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
          var Remarks = document.getElementById("<%= txtRemarks.ClientID %>").value;
          var EffectiveDt = document.getElementById("<%= txtRevokeDt.ClientID %>").value;
          ToServer("2Ø" + EmpCode + "Ø" + Remarks + "Ø" + Status + "Ø" + EffectiveDt, 2);
      }



// ]]>
  </script><br />
    <table align="center" style="width:80%;margin: 0px auto;">
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
                </td>1

            <td style="width:63% ;text-align:left;">
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Employee Code&nbsp;
                </td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Width="20%" class="NormalText" 
                    MaxLength="5" ></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Name
                </td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtName" runat="server" Width="60%" 
                    class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Branch</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtBranch" runat="server" Width="60%" 
                    class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Department</td>
            <td style="width:63% ;text-align:left;">
               &nbsp;&nbsp;<asp:TextBox ID="txtDepartment" runat="server" Width="60%" 
                    class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Designation</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtDesignation" runat="server" Width="60%" 
                    class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Date of Join</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtDateOfJoin" runat="server" Width="60%" 
                    class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
                </td>
        </tr>
        <tr style="text-align:center;">
        <td colspan="3" style="text-align:center;">
               Exit Details</td>
        </tr>
        <tr>
        <td style="width:25%;">&nbsp;</td>
            <td style="width:12% ; text-align:left;">
                Type</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtType" runat="server" Width="60%" 
                    class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;">&nbsp;</td>
            <td style="width:12% ; text-align:left;">
                Reason</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; 
                <asp:TextBox ID="txtReason" runat="server" Width="60%" 
                    class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
                &nbsp;</td>
        </tr>
        <tr>
        <td style="width:25%;">&nbsp;</td>
            <td style="width:12% ; text-align:left;">
                Effective Date</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; 
                <asp:TextBox ID="txtEffDt" runat="server" Width="60%" 
                    class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
                &nbsp;</td>
        </tr>
        <tr>
        <td style="width:25%;">&nbsp;</td>
            <td style="width:12% ; text-align:left;">
                Entered Date</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; 
                <asp:TextBox ID="txtEnteredDt" runat="server" Width="60%" 
                    class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
                &nbsp;</td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Entered By</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtEnteredBy" runat="server" Width="60%" 
                    class="ReadOnlyTextBox" ReadOnly="True"></asp:TextBox>
                </td>
        </tr>
        <tr id="rowSuspension" style="display:none;">
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Suspension Revoke Date</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtRevokeDt" runat="server" class="NormalText" Width="40%" ReadOnly="true" ></asp:TextBox> 
                <asp:CalendarExtender ID="CE1" runat="server" TargetControlID="txtRevokeDt" 
                    Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Remarks</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtRemarks" runat="server" Width="60%" 
                    class="NormalText" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' MaxLength="100"></asp:TextBox>
                </td>
        </tr>
       <br /><br />
        <tr>
        
         <td style="text-align:center;" colspan="3"> <br /><br /> <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE"  onclick="return btnSave_onclick()" />&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
        
    </table>
                <asp:HiddenField ID="hdnStatus" runat="server" />
                </asp:Content>

