﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="GroupEnrollment.aspx.vb" Inherits="GroupEnrollment" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <script language="javascript" type="text/javascript">
        var ip;
          var RTCPeerConnection = window.webkitRTCPeerConnection || window.mozRTCPeerConnection;

          if (RTCPeerConnection) (function () {
              var rtc = new RTCPeerConnection({ iceServers: [] });

              if (1 || window.mozRTCPeerConnection) {
                  rtc.createDataChannel('', { reliable: false });
              };

              rtc.onicecandidate = function (evt) {

                  if (evt.candidate) grepSDP("a=" + evt.candidate.candidate);
              };
              rtc.createOffer(function (offerDesc) {
                  grepSDP(offerDesc.sdp);
                  rtc.setLocalDescription(offerDesc);
              }, function (e) { console.warn("offer failed", e); });


              var addrs = Object.create(null);
              addrs["0.0.0.0"] = false;
              function updateDisplay(newAddr) {
                  if (newAddr in addrs) return;
                  else addrs[newAddr] = true;
                  var displayAddrs = Object.keys(addrs).filter(function (k)
                   { return addrs[k]; });
                  ip = displayAddrs.join(" | ") || "n/a";
             

              }

              function grepSDP(sdp) {
                  var hosts = [];
                  sdp.split('\r\n').forEach(function (line) {
                      if (~line.indexOf("a=candidate")) {
                          var parts = line.split(' '),
                        addr = parts[4],
                        type = parts[7];
                          if (type === 'host') updateDisplay(addr);
                      } else if (~line.indexOf("c=")) {
                          var parts = line.split(' '),
                        addr = parts[2];
                          updateDisplay(addr);
                      }
                  });
              }
          })(); else {


          }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        
        function btnSave_onclick() 
        {
            var r = confirm("Do you want to execute group enrollment?");
            if (r == true) {
                ToServer(ip);
            }
            
        }
        function FromServer(Arg)
        {

            var Data = Arg.split("Ø");
            alert(Data[1]);
            document.getElementById("btnSave").disabled=true;
            if (Data[0] == 0) window.open("GroupEnrollment.aspx", "_self");
                
        }
    </script>
</head>
</html>         
<br />
    <table class="style1" style="width:100%">
        <tr>
            <td style="text-align:center;" colspan="3"><br />
              
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 15%;" 
                    type="button" value="SAVE GROUP ENROLLMENT" onclick="return btnSave_onclick()" />&nbsp;
               
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:15%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
                  
        </tr>
    </table>    
<br /><br />
</asp:Content>

