﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class HRProfileUpdationTemp
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DB As New MS_SQL.Connect
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DT As New DataTable

#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 343) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Profile Updation"
            GN.ComboFill(cmbBranch, EN.GetBranch(1), 0, 1)
            GN.ComboFill(cmbDepartment, EN.GetDepartment(), 0, 1)
            GN.ComboFill(cmbCadre, EN.GetCadre(), 0, 1)
            GN.ComboFill(cmbState, GN.GetState(), 0, 1)
            GN.ComboFill(cmbGender, EN.GetGender(), 0, 1)
            GN.ComboFill(cmbMaritalStatus, GN.GetMaritalStatus(), 0, 1)
            GN.ComboFill(cmbIDProof, GN.GetIDProof(), 0, 1)
            GN.ComboFill(cmbAddressProof, GN.GetAddressProof(), 0, 1)
            GN.ComboFill(cmbEmpPost, GN.GetEmpPost(), 0, 1)
            GN.ComboFill(cmbEmpType, GN.GetEmployeeStatus(), 0, 1)
            GN.ComboFill(cmbSuretyType, GN.GetFamily(), 0, 1)
            hdnCharityRate.Value = GN.GetParameterValue(8, 1)
            hdnESIRate.Value = GN.GetParameterValue(5, 1)
            hdnPFRate.Value = GN.GetParameterValue(6, 1)
            hdnSFWTRate.Value = GN.GetParameterValue(7, 1)
            hdnWelfareAmount.Value = GN.GetParameterValue(4, 1)

            GN.ComboFill(cmbBloodGroup, EN.Get_BloodGroup(), 0, 1)
            GN.ComboFill(cmbReligion, EN.Get_RELIGION(), 0, 1)
            GN.ComboFill(cmbMotherTongue, EN.Get_LANGUAGE(), 0, 1)
            DT = EN.Get_LANGUAGE()

            For Each DR As DataRow In DT.Rows
                hdnLang.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next


            DT = GN.GetQualification()
            hdnQualification.Value = ""
            hdnUniversity.Value = ""


            For Each DR As DataRow In DT.Rows
                hdnQualification.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = GN.GetUniversity()
            For Each DR As DataRow In DT.Rows
                hdnUniversity.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- Register Client Side Functions ---//
            Me.cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            Me.cmbState.Attributes.Add("onchange", "StateOnChange()")
            Me.cmbDistrict.Attributes.Add("onchange", "DistrictOnChange()")
            Me.cmbDepartment.Attributes.Add("onchange", "ChangeReportingOfficer()")
            Me.cmbDesignation.Attributes.Add("onchange", "ChangeReportingOfficer()")
            Me.cmbCadre.Attributes.Add("onchange", "return CadreOnChange()")
            Me.cmbMaritalStatus.Attributes.Add("onchange", "MaritalStatusOnChange()")

            Me.cmbReligion.Attributes.Add("onchange", "ReligionOnChange()")
            txtEmpCode.Attributes.Add("onchange", "EmpCodeOnChange()")
            txtEmpCode.Attributes.Add("onfocus", "EmpCodeOnFocus()")

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If Data(0) = 1 Then
            '1               2                 3                4                  5               6                     7                    8              9
            'EmpCode(+"Ø" + EmpName + "Ø" + BranchID + "Ø" + DepartmentID + "Ø" + CadreID + "Ø" + DesignationID + "Ø" + DateOfJoin + "Ø" + GenderID + "Ø" + ReportingTo) 
            '         10                   11               12             13           14      15          16         17          18         19
            '+ "Ø" + isFieldStaff + "Ø" + CareOf+ "Ø" + HouseName + "Ø" + Location+"Ø"+City+"Ø"+PostID+"Ø"+Email+"Ø"+Landline+"Ø"+Mobile +"Ø"+DOB+"Ø"+
            '   20               21                22                     23                     24                         25                    26
            'MStatusID(+"Ø" + CareOfPhone + "Ø" + IDProofType + "Ø" + IDProofNumber + "Ø" + AddressProofType + "Ø" + AddressProofNumber + "Ø" + empPost + "Ø" +
            '27              28               29                30                    31              32                  33                 34                   35
            'Father + "Ø" + Mother + "Ø" + ReligionId + "Ø" + CasteId + "Ø" + IsTwowheeler(+"Ø" + DrivingNo + "Ø" + IsSangamMember + "Ø" + MarriageDate + "Ø" + MotherTongueId + "Ø" + 
            ' 36                  37              38                  39               40              41                 42                  43               44                  45                46                  47
            'BloodGroupId + "Ø" + Cug + "Ø" + OfficialEmail + "Ø" + ChkPanch + "Ø" + chkComm + "Ø" + chkPolice + "Ø" + SelectedItems + "Ø" + EmpType + "Ø" + SuretyType + "Ø" + SuretyName + "Ø" + SuretyAddr + "Ø" + SuretyPhone))
            Dim EmpCode As Integer = CInt(Data(1))
            Dim EmpName As String = CStr(Data(2))
            Dim branch As Array = Split(Data(3), "|")
            Dim BranchID As Integer = CInt(branch(0))
            Dim DepartmentID As Integer = CInt(Data(4))
            Dim CadreID As Integer = CInt(Data(5))
            Dim DesignationID As Integer = CInt(Data(6))
            Dim DateOfJoin As Date = CDate(Data(7))
            Dim GenderID As Integer = CInt(Data(8))
            Dim ReportingTo As Integer = CInt(Data(9))
            Dim IsFieldStaff As Integer = CInt(Data(10))
            Dim CareOf As String = Data(11)
            Dim HouseName As String = Data(12)
            Dim Location As String = Data(13)
            Dim City As String = Data(14)
            Dim PostID As Integer = CInt(Data(15))
            Dim Email As String = Data(16)
            Dim Landline As String = Data(17)
            Dim Mobile As String = Data(18)
            Dim DOB As Date = CDate(Data(19))
            Dim MStatusID As Integer = CInt(Data(20))
            Dim CareOfPhone As String = Data(21)
            Dim IDProofType As Integer = CInt(Data(22))
            Dim IDProofNumber As String = Data(23)
            Dim AddressProofType As Integer = CInt(Data(24))
            Dim AddressProofNumber As String = Data(25)
            Dim EmpPost As Integer = CInt(Data(26))
            Dim Father As String = CStr(Data(27))
            Dim Mother As String = CStr(Data(28))
            Dim ReligionId As Integer = CInt(Data(29))
            Dim CasteId As Integer = CInt(Data(30))
            Dim IsTwowheeler As Integer = CInt(Data(31))
            Dim DrivingNo As String = CStr(Data(32))
            Dim IsSangamMember As Integer = CInt(Data(33))
            Dim MarriageDate As String = CStr(Data(34))
            Dim MotherTongueId As Integer = CInt(Data(35))
            Dim BloodGroupId As Integer = CInt(Data(36))
            Dim Cug As String = CStr(Data(37))
            Dim OfficialEmail As String = CStr(Data(38))
            Dim ChkPanch As Integer = CInt(Data(39))
            Dim chkComm As Integer = CInt(Data(40))
            Dim chkPolice As Integer = CInt(Data(41))
            Dim LangKnown As String = CStr(Data(42)) 'SelectedItems
            Dim EmpType As Integer = CInt(Data(43))
            Dim SuretyType As Integer = CInt(Data(44))
            Dim SuretyName As String = CStr(Data(45))
            Dim SuretyAddr As String = CStr(Data(46))
            Dim SuretyPhone As String = CStr(Data(47))

            Dim UserID As Integer = CInt(Session("UserID"))
            'CallBackReturn = EN.UpdateEmployee_HR(EmpCode, EmpName, BranchID, DepartmentID, CadreID, DesignationID, DateOfJoin, GenderID, ReportingTo, IsFieldStaff, UserID, CareOf, HouseName, Location, City, PostID, Email, Landline, Mobile, DOB, MStatusID, CareOfPhone, IDProofType, IDProofNumber, AddressProofType, AddressProofNumber, EmpPost, Father, Mother, ReligionId, CasteId, IsTwowheeler, DrivingNo, IsSangamMember, MarriageDate, MotherTongueId, BloodGroupId, Cug, OfficialEmail, ChkPanch, chkComm, chkPolice, LangKnown, EmpType, SuretyType, SuretyName, SuretyAddr, SuretyPhone)
            Dim ErrorFlag As Integer = 0
            Dim Message As String = ""
            Try
                Dim Params(51) As SqlParameter
                Params(0) = New SqlParameter("@TrNo", SqlDbType.Int)
                Params(0).Value = 1
                Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(1).Value = EmpCode
                Params(2) = New SqlParameter("@EmpName", SqlDbType.VarChar, 50)
                Params(2).Value = EmpName
                Params(3) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(3).Value = BranchID
                Params(4) = New SqlParameter("@DepartmentID", SqlDbType.Int)
                Params(4).Value = DepartmentID
                Params(5) = New SqlParameter("@CadreID", SqlDbType.Int)
                Params(5).Value = CadreID
                Params(6) = New SqlParameter("@DesignationID", SqlDbType.Int)
                Params(6).Value = DesignationID
                Params(7) = New SqlParameter("@GenderID", SqlDbType.Int)
                Params(7).Value = GenderID
                Params(8) = New SqlParameter("@DateOfJoin", SqlDbType.Date)
                Params(8).Value = DateOfJoin
                Params(9) = New SqlParameter("@EmpPassword", SqlDbType.VarChar, 50)
                Params(9).Value = EncryptDecrypt.Encrypt("Esaf123", EmpCode.ToString())
                Params(10) = New SqlParameter("@ReportingTo", SqlDbType.Int)
                Params(10).Value = ReportingTo
                Params(11) = New SqlParameter("@IsFieldStaff", SqlDbType.Int)
                Params(11).Value = IsFieldStaff
                Params(12) = New SqlParameter("@CareOF", SqlDbType.VarChar, 50)
                Params(12).Value = CareOf
                Params(13) = New SqlParameter("@HouseName", SqlDbType.VarChar, 50)
                Params(13).Value = HouseName
                Params(14) = New SqlParameter("@Location", SqlDbType.VarChar, 50)
                Params(14).Value = Location
                Params(15) = New SqlParameter("@City", SqlDbType.VarChar, 50)
                Params(15).Value = City
                Params(16) = New SqlParameter("@PostID", SqlDbType.Int)
                Params(16).Value = PostID
                Params(17) = New SqlParameter("@Email", SqlDbType.VarChar, 50)
                Params(17).Value = Email
                Params(18) = New SqlParameter("@Landline", SqlDbType.VarChar, 50)
                Params(18).Value = Landline
                Params(19) = New SqlParameter("@Mobile", SqlDbType.VarChar, 50)
                Params(19).Value = Mobile
                Params(20) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(20).Value = UserID
                Params(21) = New SqlParameter("@DOB", SqlDbType.Date)
                Params(21).Value = DOB
                Params(22) = New SqlParameter("@MStatusID", SqlDbType.Int)
                Params(22).Value = MStatusID
                Params(23) = New SqlParameter("@CareOfPhone", SqlDbType.VarChar, 50)
                Params(23).Value = CareOfPhone
                Params(24) = New SqlParameter("@IDProofType", SqlDbType.Int)
                Params(24).Value = IDProofType
                Params(25) = New SqlParameter("@IDProofNumber", SqlDbType.VarChar, 50)
                Params(25).Value = IDProofNumber
                Params(26) = New SqlParameter("@AddressProofType", SqlDbType.Int)
                Params(26).Value = AddressProofType
                Params(27) = New SqlParameter("@AddressProofNumber", SqlDbType.VarChar, 50)
                Params(27).Value = AddressProofNumber
                Params(28) = New SqlParameter("@EmpPost", SqlDbType.VarChar, 500)
                Params(28).Value = EmpPost
                Params(29) = New SqlParameter("@Father_Name", SqlDbType.VarChar, 500)
                Params(29).Value = Father
                Params(30) = New SqlParameter("@Mother_Name", SqlDbType.VarChar, 500)
                Params(30).Value = Mother
                Params(31) = New SqlParameter("@ReligionId", SqlDbType.Int)
                Params(31).Value = ReligionId
                Params(32) = New SqlParameter("@Caste_id", SqlDbType.Int)
                Params(32).Value = CasteId
                Params(33) = New SqlParameter("@Twowheeler_Status", SqlDbType.Int)
                Params(33).Value = IsTwowheeler
                Params(34) = New SqlParameter("@Driving_License_No", SqlDbType.VarChar, 500)
                Params(34).Value = DrivingNo
                Params(35) = New SqlParameter("@Is_Sangam_member", SqlDbType.Int)
                Params(35).Value = IsSangamMember
                Params(36) = New SqlParameter("@Marriage_Date", SqlDbType.Date)
                If MarriageDate = "" Then
                    Params(36).Value = CDate("01/01/1900")
                Else
                    Params(36).Value = CDate(MarriageDate)
                End If
                Params(37) = New SqlParameter("@Mother_tongue", SqlDbType.Int)
                Params(37).Value = MotherTongueId
                Params(38) = New SqlParameter("@Blood_Group", SqlDbType.Int)
                Params(38).Value = BloodGroupId
                Params(39) = New SqlParameter("@Cug_No", SqlDbType.VarChar, 500)
                Params(39).Value = Cug
                Params(40) = New SqlParameter("@Official_mail_id", SqlDbType.VarChar, 500)
                Params(40).Value = OfficialEmail
                Params(41) = New SqlParameter("@Ref_Letter_Panchayath", SqlDbType.Int)
                Params(41).Value = ChkPanch
                Params(42) = New SqlParameter("@Ref_Letter_Community", SqlDbType.Int)
                Params(42).Value = chkComm
                Params(43) = New SqlParameter("@Ref_Letter_PoliceStation", SqlDbType.Int)
                Params(43).Value = chkPolice
                Params(44) = New SqlParameter("@LangKnown", SqlDbType.VarChar, 5000)
                Params(44).Value = LangKnown
                Params(45) = New SqlParameter("@EmpType", SqlDbType.Int)
                Params(45).Value = EmpType
                Params(46) = New SqlParameter("@SuretyType", SqlDbType.Int)
                Params(46).Value = SuretyType
                Params(47) = New SqlParameter("@SuretyName", SqlDbType.VarChar, 500)
                Params(47).Value = SuretyName
                Params(48) = New SqlParameter("@SuretyAddr", SqlDbType.VarChar, 500)
                Params(48).Value = SuretyAddr
                Params(49) = New SqlParameter("@SuretyPhone", SqlDbType.VarChar, 500)
                Params(49).Value = SuretyPhone
                Params(50) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(50).Direction = ParameterDirection.Output
                Params(51) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(51).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_Update_Profile_New", Params)
                ErrorFlag = CInt(Params(50).Value)
                Message = CStr(Params(51).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn += ErrorFlag.ToString() + "Ø" + Message

        ElseIf CInt(Data(0)) = 2 Then
            DT = GN.GetPhoto(CInt(Data(1)))
            If DT.Rows.Count > 0 Then
                Dim bytes As Byte() = DirectCast(DT.Rows(0)(0), Byte())
                Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                CallBackReturn = Convert.ToString("data:" + DT.Rows(0)(1) + ";base64,") & base64String
            Else
                CallBackReturn = "../../Image/userimage.png"
            End If
        ElseIf CInt(Data(0)) = 3 Then
            Dim EmpCode = CInt(Data(1))
            DT = EN.GetEmployeeForUpdation(EmpCode)
            If DT.Rows.Count > 0 Then
                '10001µNeeradhµ4µ0µ89µ8µ12 Jun 2000µRATHNA VIHARµAVANooRµtcrµµ1µRATHNANµ37312µ390µ1µ3523546µ680547µnms@gmail.comµSoy K Eliasµ2µ13 Jun 1987µ5456789µ1µµ1µµ25000.0000µ10000.0000µ0.0000µ0.0000µ0.0000µ0.0000µ0.0000µ0.0000µ0.0000µ0.0000µ0.0000µ100.0000µ2000.0000µ20.0000µ250.0000µ250.0000µrathnan A Rµnishaµ24µ-1µ1µ01-01-1900 00:00:00µ1µhghjg µ859734567µnbnb@cesaf.comµ0µ1µ0µ1µ4µNIRMALµRATHNA VIHARµ2465767µ1
                CallBackReturn = DT.Rows(0)(0).ToString() + "µ" + DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + DT.Rows(0)(7).ToString() + "µ" + DT.Rows(0)(8).ToString() + "µ" + DT.Rows(0)(9).ToString() + "µ" + DT.Rows(0)(10).ToString() + "µ" + DT.Rows(0)(11).ToString() + "µ" + DT.Rows(0)(12).ToString() + "µ" + DT.Rows(0)(13).ToString() + "µ" + DT.Rows(0)(14).ToString() + "µ" + DT.Rows(0)(15).ToString() + "µ" + DT.Rows(0)(16).ToString() + "µ" + DT.Rows(0)(17).ToString() + "µ" + DT.Rows(0)(18).ToString() + "µ" + DT.Rows(0)(19).ToString() + "µ" + DT.Rows(0)(20).ToString() + "µ" + DT.Rows(0)(21).ToString() + "µ" + DT.Rows(0)(22).ToString() + "µ" + DT.Rows(0)(23).ToString() + "µ" + DT.Rows(0)(24).ToString() + "µ" + DT.Rows(0)(25).ToString() + "µ" + DT.Rows(0)(26).ToString() + "µ" + DT.Rows(0)(27).ToString() + "µ" + DT.Rows(0)(28).ToString() + "µ" + DT.Rows(0)(29).ToString() + "µ" + DT.Rows(0)(30).ToString() + "µ" + DT.Rows(0)(31).ToString() + "µ" + DT.Rows(0)(32).ToString() + "µ" + DT.Rows(0)(33).ToString() + "µ" + DT.Rows(0)(34).ToString() + "µ" + DT.Rows(0)(35).ToString() + "µ" + DT.Rows(0)(36).ToString() + "µ" + DT.Rows(0)(37).ToString() + "µ" + DT.Rows(0)(38).ToString() + "µ" + DT.Rows(0)(39).ToString() + "µ" + DT.Rows(0)(40).ToString() + "µ" + DT.Rows(0)(41).ToString() + "µ" + DT.Rows(0)(42).ToString() + "µ" + DT.Rows(0)(43).ToString() + "µ" + DT.Rows(0)(44).ToString() + "µ" + DT.Rows(0)(45).ToString() + "µ" + DT.Rows(0)(46).ToString() + "µ" + DT.Rows(0)(47).ToString() + "µ" + DT.Rows(0)(48).ToString() + "µ" + DT.Rows(0)(49).ToString() + "µ" + DT.Rows(0)(50).ToString() + "µ" + DT.Rows(0)(51).ToString() + "µ" + DT.Rows(0)(52).ToString() + "µ" + DT.Rows(0)(53).ToString() + "µ" + DT.Rows(0)(54).ToString() + "µ" + DT.Rows(0)(55).ToString() + "µ" + DT.Rows(0)(56).ToString() + "µ" + DT.Rows(0)(57).ToString() + "µ" + DT.Rows(0)(58).ToString() + "µ" + DT.Rows(0)(59).ToString() + "µ" + DT.Rows(0)(60).ToString() + "µ" + DT.Rows(0)(61).ToString() + "µ" + DT.Rows(0)(62).ToString() + "µ" + DT.Rows(0)(63).ToString() + "µ" + DT.Rows(0)(64).ToString()
                CallBackReturn += "Ø"
                Dim SateID As Integer = CInt(DT.Rows(0)(15))
                Dim DistrictID As Integer = CInt(DT.Rows(0)(14))
                Dim CadreID As Integer = CInt(DT.Rows(0)(5))
                Dim ReligionID As Integer = CInt(DT.Rows(0)(61))
                Dim BranchID As Array = Split((DT.Rows(0)(3)), "|")
                Dim DepID As Integer = CInt(DT.Rows(0)(2))
                Dim DesID As Integer = CInt(DT.Rows(0)(4))
                DT = GN.GetDistrict(SateID)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
                CallBackReturn += "Ø"
                DT = GN.GetPost(DistrictID)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
                CallBackReturn += "Ø"
                DT = EN.GetQualificationDtl(EmpCode)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString()
                Next
                CallBackReturn += "Ø"
                DT = GN.GetPhoto(EmpCode)
                If DT.Rows.Count > 0 Then
                    Dim bytes As Byte() = DirectCast(DT.Rows(0)(0), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                    CallBackReturn += Convert.ToString("data:" + DT.Rows(0)(1) + ";base64,") & base64String
                Else
                    CallBackReturn += "../../Image/userimage.png"
                End If
                CallBackReturn += "Ø"
                DT = EN.GetFamilyDtl(EmpCode)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString()
                Next
                CallBackReturn += "Ø"
                DT = EN.GetExperienceDtl(EmpCode)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString()
                Next
                CallBackReturn += "Ø"
                DT = EN.GetAddressProofDtl(EmpCode)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
                Next
                CallBackReturn += "Ø"
                DT = EN.GetIDProofDtl(EmpCode)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
                Next
                CallBackReturn += "Ø"

                DT = EN.GetDesignation(CInt(CadreID))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
                CallBackReturn += "Ø"

                DT = EN.Get_CASTE(CInt(ReligionID))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
                CallBackReturn += "Ø"
                DT = EN.GetReportingOfficer(BranchID(0), DepID, DesID)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()

                Next
                CallBackReturn += "Ø"
                DT = EN.GetLangDtl(EmpCode)
                Dim LangVal As String = ""

                For Each DR As DataRow In DT.Rows
                    LangVal += DR(0).ToString() + "^"

                Next
                CallBackReturn += LangVal + "Ø"
            Else
                CallBackReturn = "ØØØØ"
            End If

        ElseIf CInt(Data(0)) = 4 Then
            DT = EN.GetDesignation(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 5 Then
            DT = GN.GetDistrict(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 6 Then
            DT = GN.GetPost(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 7 Then
            DT = EN.Get_CASTE(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub

#End Region

End Class
