﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewCompensatoryReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim rptID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 320) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            rptID = CInt(GF.Decrypt(Request.QueryString.Get("rptID")))
            hdnReportID.Value = rptID.ToString()
            Dim FromDt As Date = CDate(Request.QueryString.Get("FromDt"))
            Dim ToDt As Date = CDate(Request.QueryString.Get("ToDt"))
            Dim EmpCode As Integer = CInt(GF.Decrypt(Request.QueryString.Get("EmpCode")))
            Dim EmpName As String = GF.Decrypt(Request.QueryString.Get("EmpName"))

            Dim DT As New DataTable
            Dim DT_Sum As New DataTable
            Dim TR22 As New TableRow
            Dim TR55 As New TableRow
            Dim TR66 As New TableRow
            Dim TR77 As New TableRow
            Dim TR88 As New TableRow
            Dim TR99 As New TableRow
            DT_Sum = DB.ExecuteDataSet("SELECT AA.EMP_CODE,UPPER(AA.EMP_NAME) as EMP_NAME,AA.CL,SUM(AA.CLcount) as CLcnt,AA.SL,SUM(AA.SLcount) as SLcnt " & _
                    " ,AA.PL,SUM(AA.PLcount) as PLcnt,AA.OTHER,SUM(AA.OTHERcount) as OTHERcnt FROM ( " & _
                    " select L.EMP_CODE,E.EMP_name,'CASUAL LEAVE' AS CL,case when T.Type_ID=1 then SUM(leave_days) ELSE 0 END as CLcount " & _
                    " ,'SICK LEAVE'  AS SL,case when T.Type_ID=2 then SUM(leave_days) ELSE 0 END as SLcount, " & _
                    " 'PRIVILEGE LEAVE'  AS PL,case when T.Type_ID=3 then SUM(leave_days) ELSE 0 END as PLcount, " & _
                    " 'OTHERS'  AS OTHER,case when T.Type_ID>3 then SUM(leave_days) ELSE 0 END as OTHERcount " & _
                    " from EMP_LEAVE_REQUEST L INNER JOIN EMP_LEAVE_REASONS R ON L.Reason_ID=R.Reason_ID  " & _
                    " INNER JOIN Emp_Leave_Type T ON L.Leave_Type=T.Type_ID INNER JOIN EMP_MASTER E ON L.Emp_Code=E.Emp_Code  WHERE L.emp_code=" & EmpCode.ToString() & " and L.Leave_From Between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "'  AND L.Status_ID<5 " & _
                    " GROUP BY L.EMP_CODE,E.EMP_name,Type_ID,TYPE_NAME) AA GROUP BY AA.EMP_CODE,AA.EMP_NAME,AA.CL,AA.SL,AA.PL,AA.OTHER").Tables(0)

            If DT_Sum.Rows.Count = 0 Then
                DT_Sum = DB.ExecuteDataSet("select e.Emp_Code,UPPER(e.Emp_Name),'CASUAL LEAVE' AS CL,0 as CLcount,'SICK LEAVE'  AS SL,0 as SLcount, 'PRIVILEGE LEAVE'  AS PL,0 as PLcount, 'OTHERS'  AS OTHER,0 as OTHERcount  from EMP_MASTER e where e.Emp_Code=" & EmpCode.ToString() & "").Tables(0)
            End If
            DT = DB.ExecuteDataSet("select a.Request_dt,b.Comp_Date,a.Remarks Reason,case when a.Status_ID=1 then 'Requested (HOD Recommendation Pending)' else case when a.Status_ID=2 then 'Recommented (HR Approval Pending)' else case when a.Status_ID=3 then 'HOD Rejected' else case when a.Status_ID=4 then 'HR Rejected' else case when a.Status_ID=0 then 'HR Approved' else case when a.Status_ID=5 then 'Expired' end end end end end end Status from Compensatory_Eligibility a, Compensatory_Master b where a.Comp_ID=b.Comp_ID and a.Emp_Code=" & EmpCode.ToString() & " and Request_dt between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "' order by 1").Tables(0)
            RH.Heading(CStr(Session("FirmName")), tb, "COMPENSATORY LEAVE REPORT", 100)
            Dim DR As DataRow
            Dim DR_sum As DataRow
            If DT_Sum.Rows.Count > 0 Then
                DR_sum = DT_Sum.Rows(0)
                Dim TR22_00, TR22_01, TR22_02, TR22_03 As New TableCell
                RH.AddColumn(TR22, TR22_00, 47, 47, "l", "&nbsp;&nbsp;<u>REPORT INFORMATION</u>")
                RH.AddColumn(TR22, TR22_01, 3, 3, "l", "")
                RH.AddColumn(TR22, TR22_02, 3, 3, "l", "")
                RH.AddColumn(TR22, TR22_03, 47, 47, "l", "&nbsp;&nbsp;<u>AVAILED LEAVE DETAILS</u>")
                tb.Controls.Add(TR22)
                TR22_00.BackColor = Drawing.Color.LightSteelBlue
                TR22_03.BackColor = Drawing.Color.LightSteelBlue
                TR22.Font.Bold = True
                TR22_00.Font.Size = 20
                TR22_03.Font.Size = 20
                Dim TR55_00, TR55_01, TR55_02, TR55_03, TR55_04, TR55_05, TR55_06, TR55_07 As New TableCell
                RH.AddColumn(TR55, TR55_00, 23, 23, "l", "&nbsp;&nbsp;Emp Code")
                RH.AddColumn(TR55, TR55_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR55, TR55_02, 23, 23, "l", EmpCode.ToString())
                RH.AddColumn(TR55, TR55_03, 3, 3, "l", "")
                RH.AddColumn(TR55, TR55_04, 3, 3, "l", "")
                RH.AddColumn(TR55, TR55_05, 23, 23, "l", "&nbsp;&nbsp;Casual Leave")
                RH.AddColumn(TR55, TR55_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR55, TR55_07, 23, 23, "l", DR_sum(3).ToString())
                tb.Controls.Add(TR55)
                TR55_00.BackColor = Drawing.Color.WhiteSmoke
                TR55_01.BackColor = Drawing.Color.WhiteSmoke
                TR55_02.BackColor = Drawing.Color.WhiteSmoke
                TR55_05.BackColor = Drawing.Color.WhiteSmoke
                TR55_06.BackColor = Drawing.Color.WhiteSmoke
                TR55_07.BackColor = Drawing.Color.WhiteSmoke
                RH.BlankRow(tb, 1)
                Dim TR66_00, TR66_01, TR66_02, TR66_03, TR66_04, TR66_05, TR66_06, TR66_07 As New TableCell
                RH.AddColumn(TR66, TR66_00, 23, 23, "l", "&nbsp;&nbsp;Name")
                RH.AddColumn(TR66, TR66_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR66, TR66_02, 23, 23, "l", EmpName)
                RH.AddColumn(TR66, TR66_03, 3, 3, "l", "")
                RH.AddColumn(TR66, TR66_04, 3, 3, "l", "")
                RH.AddColumn(TR66, TR66_05, 23, 23, "l", "&nbsp;&nbsp;Sick Leave")
                RH.AddColumn(TR66, TR66_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR66, TR66_07, 23, 23, "l", DR_sum(5).ToString())
                TR66_00.BackColor = Drawing.Color.FloralWhite
                TR66_01.BackColor = Drawing.Color.FloralWhite
                TR66_02.BackColor = Drawing.Color.FloralWhite
                TR66_05.BackColor = Drawing.Color.FloralWhite
                TR66_06.BackColor = Drawing.Color.FloralWhite
                TR66_07.BackColor = Drawing.Color.FloralWhite
                tb.Controls.Add(TR66)
                Dim TR77_00, TR77_01, TR77_02, TR77_03, TR77_04, TR77_05, TR77_06, TR77_07 As New TableCell
                RH.AddColumn(TR77, TR77_00, 23, 23, "l", "&nbsp;&nbsp;From Date")
                RH.AddColumn(TR77, TR77_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR77, TR77_02, 23, 23, "l", FromDt.ToString("dd MMM yyyy"))
                RH.AddColumn(TR77, TR77_03, 3, 3, "l", "")
                RH.AddColumn(TR77, TR77_04, 3, 3, "l", "")
                RH.AddColumn(TR77, TR77_05, 23, 23, "l", "&nbsp;&nbsp;Privilege Leave")
                RH.AddColumn(TR77, TR77_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR77, TR77_07, 23, 23, "l", DR_sum(7).ToString())
                TR77_00.BackColor = Drawing.Color.WhiteSmoke
                TR77_01.BackColor = Drawing.Color.WhiteSmoke
                TR77_02.BackColor = Drawing.Color.WhiteSmoke
                TR77_05.BackColor = Drawing.Color.WhiteSmoke
                TR77_06.BackColor = Drawing.Color.WhiteSmoke
                TR77_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR77)
                Dim TR88_00, TR88_01, TR88_02, TR88_03, TR88_04, TR88_05, TR88_06, TR88_07 As New TableCell
                RH.AddColumn(TR88, TR88_00, 23, 23, "l", "&nbsp;&nbsp;To Date")
                RH.AddColumn(TR88, TR88_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR88, TR88_02, 23, 23, "l", ToDt.ToString("dd MMM yyyy"))
                RH.AddColumn(TR88, TR88_03, 3, 3, "l", "")
                RH.AddColumn(TR88, TR88_04, 3, 3, "l", "")
                RH.AddColumn(TR88, TR88_05, 23, 23, "l", "&nbsp;&nbsp;Others")
                RH.AddColumn(TR88, TR88_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR88, TR88_07, 23, 23, "l", DR_sum(9).ToString())
                TR88_00.BackColor = Drawing.Color.FloralWhite
                TR88_01.BackColor = Drawing.Color.FloralWhite
                TR88_02.BackColor = Drawing.Color.FloralWhite
                TR88_05.BackColor = Drawing.Color.FloralWhite
                TR88_06.BackColor = Drawing.Color.FloralWhite
                TR88_07.BackColor = Drawing.Color.FloralWhite
                tb.Controls.Add(TR88)
                RH.DrawLine(tb, 100)
                Dim RowBG As Integer = 0
                Dim TR0 As New TableRow
                TR0.BackColor = Drawing.Color.WhiteSmoke
                Dim TR0_00, TR0_01, TR0_02, TR0_03 As New TableCell
                RH.AddColumn(TR0, TR0_00, 12, 12, "c", "Request Date")
                RH.AddColumn(TR0, TR0_01, 14, 14, "c", "Compensatory Date")
                RH.AddColumn(TR0, TR0_02, 50, 50, "l", "Reason")
                RH.AddColumn(TR0, TR0_03, 24, 24, "l", "Status")
                tb.Controls.Add(TR0)
                TR0.Font.Bold = True
                RH.DrawLine(tb, 100)
                Dim TOT As Double = 0
                Dim I As Integer = 1
                For Each DR In DT.Rows
                    Dim TR1 As New TableRow
                    If RowBG = 0 Then
                        RowBG = 1
                        TR1.BackColor = Drawing.Color.WhiteSmoke
                    Else
                        RowBG = 0
                    End If
                    Dim TR1_00, TR1_01, TR1_02, TR1_03 As New TableCell
                    RH.AddColumn(TR1, TR1_00, 12, 12, "c", DR(0))
                    RH.AddColumn(TR1, TR1_01, 14, 14, "c", DR(1))
                    RH.AddColumn(TR1, TR1_02, 50, 50, "l", DR(2))
                    RH.AddColumn(TR1, TR1_03, 24, 24, "l", DR(3))
                    tb.Controls.Add(TR1)
                Next
                RH.DrawLine(tb, 100)
            End If
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
    End Sub
    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
    End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
