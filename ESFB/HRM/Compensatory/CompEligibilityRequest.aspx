﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CompEligibilityRequest.aspx.vb" Inherits="HRM_Compensatory_CompEligibilityRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script src="../../Script/Calculations.js" language="javascript" type="text/javascript"> </script>
        <script language="javascript" type="text/javascript">
            function window_onload() {
                ToServer("1ʘ", 1);
            }

            function DateOnChange() {  
                var CompDate = document.getElementById("<%= txtCompDt.ClientID %>").value;
                ToServer("2ʘ" + CompDate, 2);
            }

            function setStartDate(sender, args) 
            {
                var today = new Date();
                today.setDate(today.getDate() - 90)
                sender._startDate = today;
                sender._endDate = new Date();
            }

            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {
                        var Data = Arg.split("Æ");
                        document.getElementById("<%= txtEmpCode.ClientID %>").value = Data[0];
                        document.getElementById("<%= txtName.ClientID %>").value = Data[1];
                        break;
                    }
                    case 2:
                    {
                        var Data = Arg.split("Æ");
                        document.getElementById("<%= txtMorning.ClientID %>").value = Data[1];
                        document.getElementById("<%= txtEvening.ClientID %>").value = Data[2];
                        if (Data[0] == 0)
                        {
                            alert("Selected Date Is Not a Holiday");    
                            document.getElementById("btnSave").disabled = true;                        
                            return false;
                        }
                        else
                        {
                            document.getElementById("btnSave").disabled = false;
                        }
                        break;
                    }
                    case 3:
                    {
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        if (Data[0] == 0) {      
                            window.open("CompEligibilityRequest.aspx","_self");
                        }
                        break;
                    }
                }
            }

            function btnSave_onclick() {
                var CompDate = document.getElementById("<%= txtCompDt.ClientID %>").value;
                var Remark = document.getElementById("<%= txtRemarks.ClientID %>").value;
                ToServer("3ʘ" + CompDate + "ʘ" + Remark, 3);
            }

            function btnExit_onclick() {
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }        
            
// ]]>
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <table style="text-align: center; width: 60%; margin: 0px auto;">
        <tr>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            </br>
            <td style="width: 25%; text-align: left;">
                Employee Code
            </td>
            <td style="width: 25%; text-align: left;">
                <asp:TextBox ID="txtEmpCode" class="ReadOnlyTextBox" runat="server" Width="98%"
                    MaxLength="5" ReadOnly="True"></asp:TextBox>
            </td>
            <td style="width: 25%; text-align: left;">
                Name
            </td>
            <td style="width: 25%; text-align: left;">
                <asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="100%" MaxLength="50"
                    ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%; text-align: left;">
                Compensatory Date
            </td>
            <td style="width: 25%; text-align: left;">
                <asp:TextBox ID="txtCompDt" class="NormalText" runat="server" Width="98%"></asp:TextBox>
                <asp:CalendarExtender ID="txtCompDt_CalendarExtender" runat="server" TargetControlID="txtCompDt" OnClientShowing="setStartDate"
                    Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 25%; text-align: left;">
                Morning Time
            </td>
            <td style="width: 25%; text-align: left;">
                <asp:TextBox ID="txtMorning" class="ReadOnlyTextBox" runat="server" Width="98%"
                    ReadOnly="True"></asp:TextBox>
            </td>
            <td style="width: 25%; text-align: left;">
                Evening Time
            </td>
            <td style="width: 25%; text-align: left;">
                <asp:TextBox ID="txtEvening" class="ReadOnlyTextBox" runat="server" Width="100%"
                    ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%; text-align: left;">
                Remarks
            </td>
            <td style="text-align: left;" colspan="2">
                <asp:TextBox ID="txtRemarks" class="NormalText" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
            <td style="text-align: center;" colspan="2">
                &nbsp; &nbsp;
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 15%;" type="button"
                    value="SAVE" onclick="return btnSave_onclick()">
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 15%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
