﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CompensatoryHRApproval.aspx.vb" Inherits="CompensatoryHRApproval" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        
        function RequestOnchange() {
            if (document.getElementById("<%= cmbCompensatory.ClientID %>").value != "-1") {
                var Data = document.getElementById("<%= cmbCompensatory.ClientID %>").value.split("Ø");
                document.getElementById("<%= txtEmpCode.ClientID %>").value = Data[1];
                document.getElementById("<%= txtEmpName.ClientID %>").value = Data[2];
                document.getElementById("<%= txtCompDate.ClientID %>").value = Data[3];
                document.getElementById("<%= txtReason.ClientID %>").value = Data[4];
                document.getElementById("<%= txtBranch.ClientID %>").value = Data[5];
                document.getElementById("<%= txtDept.ClientID %>").value = Data[6];
                document.getElementById("<%= txtRemarks.ClientID %>").focus();
                
            }
            else {
                document.getElementById("<%= txtEmpCode.ClientID %>").value = "";
                document.getElementById("<%= txtEmpName.ClientID %>").value = "";
                document.getElementById("<%= txtCompDate.ClientID %>").value = "";
                document.getElementById("<%= txtReason.ClientID %>").value = "";
                document.getElementById("<%= txtBranch.ClientID %>").value ="";
                document.getElementById("<%= txtDept.ClientID %>").value = "";
            }
           
        }



        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        function btnReject_onclick() {
            if (document.getElementById("<%= cmbCompensatory.ClientID %>").value == "-1") {
                alert("Select Compensatory Request");
                document.getElementById("<%= cmbCompensatory.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtremarks.ClientID %>").value == "") {
                alert("Enter Remarks");
                document.getElementById("<%= txtremarks.ClientID %>").focus();
                return false;
            }
            var ReqDtl    = document.getElementById("<%= cmbCompensatory.ClientID %>").value.split("Ø");
            var RequestID = ReqDtl[0];
            var remarks   = document.getElementById("<%= txtremarks.ClientID %>").value;
            var Status    = 4;
            var Data      = "1Ø" + RequestID + "Ø" + remarks + "Ø" + Status;
            ToServer(Data, 1);
        }

        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("CompensatoryHRApproval.aspx", "_self");
            }
            else if (context == 2) {
               
            }

        }
       
        function btnApprove_onclick() {
            if (document.getElementById("<%= cmbCompensatory.ClientID %>").value == "-1") {
                alert("Select Compensatory Request");
                document.getElementById("<%= cmbCompensatory.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtremarks.ClientID %>").value == "") {
                alert("Enter Remarks");
                document.getElementById("<%= txtremarks.ClientID %>").focus();
                return false;
            }
            var ReqDtl    = document.getElementById("<%= cmbCompensatory.ClientID %>").value.split("Ø");
            var RequestID = ReqDtl[0];
            var remarks   = document.getElementById("<%= txtremarks.ClientID %>").value;
            var Status    = 0;
            var Data      = "1Ø" + RequestID + "Ø" + remarks + "Ø" + Status;
            ToServer(Data, 1);    
        }
        
        

    </script>

</head>
</html>
               
<br />

    <table class="style1" style="width:100%">
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12%; text-align:left; ">
                Select&nbsp; Compensatory</td>
            <td style="width:63%;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbCompensatory" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
        <td style="width:25%;">&nbsp;</td>
            <td style="width:12%; text-align:left; ">
                Branch Name</td>
            <td style="width:63%;">
                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" readonly="True" 
                    runat="server" Width="20%" 
                    MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
        <td style="width:25%;">&nbsp;</td>
            <td style="width:12%; text-align:left; ">
                Department</td>
            <td style="width:63%;">
                &nbsp;&nbsp; <asp:TextBox ID="txtDept" class="ReadOnlyTextBox" readonly="True" 
                    runat="server" Width="20%" 
                    MaxLength="100"></asp:TextBox>
                &nbsp;</td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12%; text-align:left; ">
                Emp Code</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpCode" class="ReadOnlyTextBox" readonly="True" runat="server" Width="20%" 
                    MaxLength="100"></asp:TextBox>
            </td>
          </tr>
          <tr>
          <td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">
                Emp Name</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpName" class="ReadOnlyTextBox" readonly="True" runat="server" Width="20%" 
                    MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">
                Compensatory Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCompDate" class="ReadOnlyTextBox" readonly="True" runat="server" Width="20%" 
                    MaxLength="100"></asp:TextBox>
            </td>
         </tr>
         <tr>
         <td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">
                Reason</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtReason" class="ReadOnlyTextBox" readonly="True" runat="server" Width="60%" 
                    MaxLength="100"></asp:TextBox>  
            </td>
        </tr>
        <tr> 
        <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Remarks</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtRemarks" class="NormalText"
                runat="server" Width="60%" 
                MaxLength="100" ></asp:TextBox>
            
            </td>
       </tr>
        <tr>
            <td style="text-align:center;" colspan="3"><br />
                <asp:HiddenField ID="hid_approve" runat="server" />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 8%;" 
                    type="button" value="APPROVE" onclick="return btnApprove_onclick()" />&nbsp;
                <input id="btnReject" style="font-family: cambria; cursor: pointer; width:8%;" 
                    type="button" value="REJECT" onclick="return btnReject_onclick()" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

