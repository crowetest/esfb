﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CompensatoryHRApproval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim LV As New Leave
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 210) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Compensatory HR Approval"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Dim UserID As Integer = CInt(Session("UserID"))
            DT = GF.GetQueryResult("select '---SELECT---','-1' union all select convert (varchar,a.emp_code) + '--' + b.Emp_Name + '-- ' + convert (varchar,c.comp_date),convert (varchar,a.request_id) + 'Ø' + convert (varchar,a.emp_code) + 'Ø' + convert (varchar,b.Emp_Name) + 'Ø' +convert (varchar,c.Comp_Date) + 'Ø' + a.remarks  + 'Ø' + b.Branch_Name  + 'Ø' + b.Department_Name  from Compensatory_Eligibility  a,EMP_LIST b,Compensatory_Master c  where a.emp_code = b.Emp_Code and a.status_id = 2 and a.comp_id = c.comp_id ")
            If DT Is Nothing Then
            Else
                GF.ComboFill(cmbCompensatory, DT, 1, 0)
            End If
            Me.cmbCompensatory.Attributes.Add("onchange", "return RequestOnchange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        ' RequestID + "Ø" + status + "Ø" + remarks + "Ø" + days + "Ø" + approvestatus;
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim SelectedUserID As Integer = CInt(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        If RequestID = 1 Then
            Dim ReqID As Integer = CInt(Data(1))
            Dim status As Integer = CInt(Data(3))
            Dim remarks As String = CStr(Data(2))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                Params(0).Value = ReqID
                Params(1) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                Params(1).Value = remarks
                Params(2) = New SqlParameter("@StatusID", SqlDbType.Int)
                Params(2).Value = status
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = UserID
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_COMPENSATORY_FINAL_APPROVAL", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If
    End Sub
#End Region
End Class
