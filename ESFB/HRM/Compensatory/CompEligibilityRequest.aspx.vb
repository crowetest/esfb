﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class HRM_Compensatory_CompEligibilityRequest
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
#Region "Page Load & Dispose"
    Protected Sub HRM_Leave_CompEligibilityRequest_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub
    Protected Sub HRM_Leave_CompEligibilityRequest_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 202) = False Then
            '    Response.redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Me.Master.subtitle = "Compensatory Eligibility Request"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.txtCompDt.Attributes.Add("onchange", "return DateOnChange();")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1
                DT = GF.GetQueryResult("select Emp_Name from EMP_MASTER where Emp_Code=" & UserID & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn = CStr(UserID) + "Æ" + DT.Rows(0)(0).ToString()
                End If
            Case 2
                'DT = GF.GetQueryResult("select b.HolidayZoneID from branch_master b, EMP_MASTER e where b.Branch_ID=e.Branch_ID and e.Emp_Code=" & UserID & "")
                'Dim ZoneID As Integer = CInt(DT.Rows(0)(0))
                DT = GF.GetQueryResult("select COUNT(HolidayID) from HOLIDAY_MASTER where DATEADD(day,DATEDIFF(day, 0, Tra_Dt ),0)='" & CDate(Data(1)) & "' and branch_id=" & CInt(Me.Session("BranchID")) & " and Status_ID=1")
                If DT.Rows.Count > 0 Then
                    CallBackReturn = CStr(DT.Rows(0)(0))
                    DT = GF.GetQueryResult("select isnull(M_Time,0),isnull(E_Time,0) from EMP_ATTEND_HIS where Emp_Code=" & UserID & " and DATEADD(day,DATEDIFF(day, 0, Tra_Dt ),0)='" & CDate(Data(1)) & "'")
                    If DT.Rows.Count > 0 Then
                        CallBackReturn += "Æ" + DT.Rows(0)(0).ToString() + "Æ" + DT.Rows(0)(1).ToString()
                    Else
                        CallBackReturn += "ÆÆ"
                    End If
                End If
            Case 3
                Dim CompDate As Date = CDate(Data(1))
                Dim Remark As String = CStr(Data(2))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(4) As SqlParameter
                    Params(0) = New SqlParameter("@CompenDate", SqlDbType.Date)
                    Params(0).Value = CompDate
                    Params(1) = New SqlParameter("@Remark", SqlDbType.VarChar, 1000)
                    Params(1).Value = Remark
                    Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(2).Value = UserID
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_ELIGIBILITY_REQUEST", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region
End Class
