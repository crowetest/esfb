﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewESI_Employee
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb, tb1 As New Table
    Dim WebTools As New WebApp.Tools
    Dim DT As New DataTable
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 348) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DTHead As New DataTable


            RH.Heading(CStr(Session("FirmName")), tb, "ESI Employee Details", 100)
            tb.Attributes.Add("width", "100%")
            RH.BlankRow(tb, 6)


            tb1.Attributes.Add("width", "100%")
            tb1.Attributes.Add("border", "1")
            Dim TRHead4 As New TableRow
            Dim TRHead4_00, TRHead4_01, TRHead4_02, TRHead4_03, TRHead4_04, TRHead4_05 As New TableCell
            TRHead4.Attributes.Add("height", "40px")
            TRHead4.Style.Add("font-weight", "bold")
            TRHead4.BackColor = Drawing.Color.Silver
            RH.InsertColumn(TRHead4, TRHead4_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead4, TRHead4_05, 30, 2, "Branch Name")
            RH.InsertColumn(TRHead4, TRHead4_01, 10, 2, "Employee Code")
            RH.InsertColumn(TRHead4, TRHead4_02, 30, 2, "Employee Name")
            RH.InsertColumn(TRHead4, TRHead4_03, 25, 2, "ESI No.")
            tb1.Controls.Add(TRHead4)
            RH.BlankRow(tb1, 6)

            DT = DB.ExecuteDataSet("select b.branch_name,e.emp_code,e.emp_name,d.esi_no from emp_master e, emp_details d , brmaster b  where e.emp_code=d.emp_code and e.branch_id=b.branch_id and e.esi_flag=1 order by 1,2").Tables(0)
            Dim dr As DataRow
            Dim n As Integer = 1
            For Each dr In DT.Rows
                Dim TRHead5 As New TableRow
                Dim TRHead5_00, TRHead5_01, TRHead5_02, TRHead5_03, TRHead5_04, TRHead5_05 As New TableCell
                RH.InsertColumn(TRHead5, TRHead5_00, 5, 0, n)
                RH.InsertColumn(TRHead5, TRHead5_01, 30, 0, dr(0))
                RH.InsertColumn(TRHead5, TRHead5_02, 10, 0, dr(1))
                RH.InsertColumn(TRHead5, TRHead5_03, 30, 0, dr(2))
                RH.InsertColumn(TRHead5, TRHead5_04, 25, 0, dr(3))
                n = n + 1
                tb1.Controls.Add(TRHead5)
            Next
            pnDisplay.Controls.Add(tb)
            pnDisplay.Controls.Add(tb1)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    'Protected Sub cmd_Export_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
    '    Response.ContentType = "application/pdf"
    '    Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
    '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '    Dim sw As New StringWriter()
    '    Dim hw As New HtmlTextWriter(sw)
    '    pnDisplay.RenderControl(hw)
    '    Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
    '    PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
    '    pdfDoc.Open()
    '    Dim sr As New StringReader(sw.ToString())
    '    Dim htmlparser As New HTMLWorker(pdfDoc)
    '    htmlparser.Parse(sr)
    '    pdfDoc.Close()
    '    Response.Write(pdfDoc)
    '    Response.[End]()
    'End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            WebTools.ExporttoExcel(DT, "ESI Data")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
