﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="viewConsolidatedReport_Department.aspx.vb" Inherits="Reports_viewConsolidatedReport_Department" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">

<body style="background-color:Black;">
 <script language="javascript" type="text/javascript">
     function PrintPanel() {
         var panel = document.getElementById("<%=pnDisplay.ClientID %>");
         var printWindow = window.open('', '', 'height=400,width=800');
         printWindow.document.write('<html><head><title>ESAF</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () {
             printWindow.print();
         }, 500);
         return false;
     }
     function Exitform() {
         window.close();
         return false;
     }
     function Backform() {
         window.open("../../home.aspx", "_self");
         return false;
     }
     function window_onload() {
         if (document.getElementById("<%= hdn_PostID.ClientID %>").value == 5) {
             document.getElementById("<%= cmd_Back.ClientID %>").style.display = '';
             document.getElementById("<%= cmd_Close.ClientID %>").style.display = 'none';
         }
         else {
             document.getElementById("<%= cmd_Back.ClientID %>").style.display = 'none';
             document.getElementById("<%= cmd_Close.ClientID %>").style.display = '';
         }
     }
     </script>
    <form id="form1" runat="server">
    <div style="width:60%; background-color:white; min-height:750px; margin:0px auto;">
        <div style="text-align:right ;margin:0px auto; width:95%; background-color:white; ">
            <asp:HiddenField ID="hdnReportID" runat="server" />
            <asp:ImageButton ID="cmd_Close" style="text-align:left ;float:left; padding-top:5px;" runat="server" Height="35px" Width="35px" ImageAlign="AbsMiddle" ImageUrl="~/Image/close.png" ToolTip="Close"/>
            <asp:ImageButton ID="cmd_Back" style="text-align:left ;float:left; padding-top:5px;" runat="server" Height="35px" Width="35px" ImageAlign="AbsMiddle" ImageUrl="~/Image/back.png" ToolTip="Back"/>
            <asp:HiddenField ID="hdn_PostID" runat="server" />
            <asp:ImageButton ID="cmd_Print" style="text-align:right ;" runat="server" Height="40px" Width="40px" ImageAlign="AbsMiddle" ImageUrl="~/Image/print.png" ToolTip="Click to Print"/>
            &nbsp;&nbsp;<asp:ImageButton ID="cmd_Export" style="text-align:right ;" runat="server" Height="30px" Width="30px" ImageAlign="AbsMiddle" ImageUrl="~/Image/Export.png" ToolTip="Click to Export"/>
        </div>
        <br style="background-color:white"/>
        <div style="text-align:center;margin:0px auto; width:95%; background-color:white;">   
            <asp:Panel ID="pnDisplay" runat="server" Font-Names="Cambria,Franklin Gothic Medium" Width="100%">
            </asp:Panel>
        </div>
    </div>
    </form>
</body>
</html>
</asp:Content>