﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="viewEmployeeHRDetails.aspx.vb" Inherits="viewEmployeeHRDetails" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
      <script src="../../Script/Validations.js" type="text/javascript"></script>
</head>
<body style="background-color:Black;">
 <script language="javascript" type="text/javascript">
     function EmpCodeOnChange() {
         var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
         if (EmpCode == "")
            { document.getElementById("<%= txtEmpName.ClientID %>").value = ""; return false; }
         else
            ToServer("1ʘ" + EmpCode, 1);
     }

     function FromServer(Arg, Context) {
         switch (Context) {
             case 1:
                 {
                     var Data = Arg.split("Æ");
                     document.getElementById("<%= txtEmpName.ClientID %>").value = Data[0];
                     break;
                 }
         }
     }

     function btnExit_onclick() {
         window.open('../../home.aspx', '_self');
     }
     </script>
    <form id="form1" runat="server"> 
     <div style="width:95%; background-color:white; min-height:750px; margin:0px auto;">
    <div>
     <br /> 
   <table  style="width:75%;margin: 0px auto;font-family:Cambria;" border="1" >
    <tr style="background-color:lightsteelblue;">
    <td style="text-align: center;" colspan="4">Employee Details</td>
    </tr>
    <tr style="background-color:lightsteelblue;">
    <td style="width:25%; text-align: center;">EmpCode</td>
    <td style="width:25%;">
                 <asp:TextBox ID="txtEmpCode" class="NormalText" runat="server" Width="95%" 
                MaxLength="100" Height="20px" ></asp:TextBox> 
                </td>
    <td style="width:25%; text-align: center;">EmpName</td>
    <td style="width:25%;">
                 <asp:TextBox ID="txtEmpName" class="ReadOnlyTextBox" runat="server" Width="95%" 
                MaxLength="100" Height="20px" onkeypress="return false" ></asp:TextBox>
     </td>
    </tr>
    <tr style="background-color:lightsteelblue;">
    <td style="text-align: center;" colspan="4">
                <asp:Button ID="btnView" runat="server" Text="VIEW" Font-Names="Cambria" 
                    style="margin-bottom: 0px" Width="8%"/>
                &nbsp;&nbsp; 
                <input id="btnExit" style="font-family: cambria; cursor: pointer;width:8%; " 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
    <tr><td colspan="4"><asp:Panel ID="pnDisplay"  style="text-align:center;"  runat="server" Width="100%">
            </asp:Panel></td></tr>
    </table>
    </div>   
    </div>
    </form>
</body>
</html>
</asp:Content>