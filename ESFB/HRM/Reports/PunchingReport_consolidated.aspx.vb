﻿
Partial Class PunchingReport_consolidated
    Inherits System.Web.UI.Page
    Dim GN As New GeneralFunctions
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try
            Response.Redirect("viewConsolidatedReport_Reporting.aspx?TraDt=" & CDate(txtFromDt.Text).ToString("dd/MMM/yyyy") & "", False)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub HRM_Reports_viewConsolidatedPunching_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1108) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.txtFromDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            Me.txtFromDt_CalendarExtender.EndDate = CDate(Session("TraDt"))
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
