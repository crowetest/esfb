﻿
Partial Class HRM_Reports_viewConsolidatedPunching
    Inherits System.Web.UI.Page
    Dim GN As New GeneralFunctions
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try
            Dim PostID As Integer = CInt(Session("Post_ID"))
            If PostID = 5 Then
                Response.Redirect("viewconsolidatedReport.aspx?rptID=" + GN.Encrypt("2") + " &RegionID=" + GN.Encrypt("-1") + "  &AreaID=" + GN.Encrypt("-1") + "  &BranchID=" + GN.Encrypt(GN.GetBranch_ID(Session("UserID")).ToString()) + " &PostID=" & PostID & " &TraDt=" & CDate(txtFromDt.Text).ToString("dd/MMM/yyyy") & "", False)
            ElseIf PostID = 6 Then
                Response.Redirect("viewconsolidatedPunchingReport_Branch.aspx?rptID=" + GN.Encrypt("2") + "  &RegionID=" + GN.Encrypt("1") + "  &AreaID=" + GN.Encrypt(GN.GetArea_ID(Session("UserID")).ToString()) + " &BranchID=-1 &PostID=" & PostID & " &TraDt=" & CDate(txtFromDt.Text).ToString("dd/MMM/yyyy") & "", False)
            ElseIf PostID = 7 Then
                Response.Redirect("viewconsolidatedPunchingReport_Area.aspx?rptID=" + GN.Encrypt("2") + "  &RegionID=" + GN.Encrypt(GN.GetRegion_ID(Session("UserID")).ToString()) + " &AreaID=" + GN.Encrypt("-1") + "  &BranchID=" + GN.Encrypt("-1") + "  &PostID=" & PostID & " &TraDt=" & CDate(txtFromDt.Text).ToString("dd/MMM/yyyy") & "", False)
            Else
                Response.Redirect("viewconsolidatedPunchingReport_REgion.aspx?rptID=" + GN.Encrypt("2") + "  &RegionID=" + GN.Encrypt("1") + "  &AreaID=" + GN.Encrypt("-1") + "  &BranchID=" + GN.Encrypt("-1") + "  &PostID=" & PostID & " &TraDt=" & CDate(txtFromDt.Text).ToString("dd/MMM/yyyy") & "", False)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub HRM_Reports_viewConsolidatedPunching_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 115) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.txtFromDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            Me.txtFromDt_CalendarExtender.EndDate = CDate(Session("TraDt"))
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
