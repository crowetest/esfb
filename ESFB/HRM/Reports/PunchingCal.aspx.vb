﻿Imports System.Data

Partial Class Reports_PunchingReport
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim rptID As Integer
    Dim CallBackReturn As String
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            rptID = CInt(Request.QueryString.Get("ID"))
            txtEmpCode.Text = Session("UserID").ToString()
            txtName.Text = Session("UserName").ToString()

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim SubHead As String = ""
            If rptID = 1 Then
                SubHead = "Punching Report"
            ElseIf rptID = 2 Then
                SubHead = "Leave Report"
            ElseIf rptID = 3 Then
                SubHead = "Travel Report"
            ElseIf rptID = 4 Then
                SubHead = "Leave verification Report"
            ElseIf rptID = 5 Then
                SubHead = "Punching Report"
                Me.txtEmpCode.ReadOnly = False
                Me.txtEmpCode.CssClass = "NormalText"
                Me.txtName.Text = ""
                txtEmpCode.Text = ""
                txtEmpCode.Focus()
            End If
            Me.Master.subtitle = SubHead

            btnGenerate.Attributes.Add("onclick", "return GenerateOnClick()")
            txtEmpCode.Attributes.Add("onchange", "return EmpCodeOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try
            Dim Data() As String = Me.hdnValue.Value.Split("Ø")
            Dim CurrentDay As Date = Date.Now().ToString("d")
            Dim CurrentMonthFirstDay As Date = CurrentDay.AddDays(-(CurrentDay.Day - 1))
            Dim CurrentMonthLastDay As Date = CurrentDay.AddMonths(1).AddDays(-(CurrentDay.AddMonths(1).Day))
            Dim Emp_Code As Integer = CInt(Data(0))
            Dim Emp_Name As String = CStr(Data(1))
            Response.Redirect("PunchingCalendar.aspx?FromDt=" & CurrentMonthFirstDay & ",&ToDt=" & CurrentMonthLastDay & ",&EmpCode=" & Emp_Code & ",&EmpName=" & Emp_Name, False)
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If Data(0) = 1 Then
            Dim EmpCode As Integer = CInt(Data(1))
            Dim EN As New Enrollment
            Dim DT As New DataTable
            DT = EN.GetEmployee(EmpCode)
            If DT.Rows.Count > 0 Then
                CallBackReturn = "1Ø" + DT.Rows(0)(1)
            Else
                CallBackReturn = "-1Ø-1"
            End If

        End If
    End Sub
End Class
