﻿Imports System.Data
Partial Class Audit_Leave_Travel_Report
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim rptID As Integer
    Dim CallBackReturn As String
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1120) = False And GF.FormAccess(CInt(Session("UserID")), 1121) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            rptID = CInt(Request.QueryString.Get("ID"))
            txtFromDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            txttODt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim SubHead As String = ""
            If rptID = 1 Then
                SubHead = "BM/AM/RM - Leave Report"
            ElseIf rptID = 2 Then
                SubHead = "BM/AM/RM - Travel Report"
            End If
            Me.Master.subtitle = SubHead
            txtFromDt.Attributes.Add("onchange", "return DateOnChange();")
            txtToDt.Attributes.Add("onchange", "return DateOnChange();")
            btnGenerate.Attributes.Add("onclick", "return GenerateOnClick()")
            txtEmpCode.Attributes.Add("onchange", "return EmpCodeOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Dim Data() As String = Me.hdnValue.Value.Split("Ø")
        Response.Redirect("viewAudit_Leave_Travel_Report.aspx?rptID=" + GF.Encrypt(rptID.ToString()) + "&EmpCode=" + GF.Encrypt(Data(0)) + " &FromDt=" + Data(1) + " &ToDt=" + Data(2) + " &EmpName=" + GF.Encrypt(Data(3)) + "", False)
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If Data(0) = 1 Then
            Dim EmpCode As Integer = CInt(Data(1))
            Dim EN As New Enrollment
            Dim DT As New DataTable
            DT = DB.ExecuteDataSet("select a.Emp_Code,a.Emp_Name,e.Department_ID,cast(d.Branch_id as varchar(8))+'Ø'+cast(d.status_ID as varchar(2)),g.Designation_id,f.Cadre_id,convert(varchar(11),a.Date_Of_Join,106), " & _
            " isnull(b.HouseName,'') as House,ISNULL(b.Location,'') as location,ISNULL(b.city,'') as city,isnull(b.mobile,''), " & _
            " a.Gender_ID,isnull(b.CareOf,''),k.Post_ID,j.District_ID,i.State_ID,isnull(b.Landline,''),k.pin_Code,isnull(b.email,''), " & _
            " l.emp_name as ReportingTo,b.Marital_Status,convert(varchar(11),b.DOB,106),b.CareOf_Phone,b.IDProof_Type,b.IDProof_Number,b.AddressProof_Type,b.AddressProof_Number,c.Basic_Pay,c.DA,c.HRA,c.Conveyance,c.SpecialAllowance,c.LocalAllowance,c.MedicalAllowance,c.HospitalityAllowance,c.PerformanceAllowance,c.OtherAllowance,c.FieldAllowance,c.ESI,c.PF,c.SWF,c.ESWT,c.Charity " & _
            ",b.Fathers_name,b.Mothers_name,b.Caste_id,b.Blood_Group,b.Mother_tongue,case when b.Marriage_Date='1900-01-01' then '' else b.Marriage_Date end as Marriage_Date ,b.Twowheeler_Status, b.Driving_License_No,b.Cug_No,b.Official_mail_id,b.Ref_Letter_Panchayath " & _
            " ,b.Ref_Letter_Community,b.Ref_Letter_PoliceStation,b.Is_Sangam_member,b.surety_id, b.Surety_person_name, b.Surety_Address, b.Surety_Phone,m.Religion_Id  " & _
            " from EMP_MASTER a , Emp_Profile b LEFT JOIN CASTE_MASTER m ON  b.Caste_id =m.caste_id,EMP_SALARY_DTL c,BRANCH_MASTER d,DEPARTMENT_MASTER e, " & _
            " CADRE_MASTER f,DESIGNATION_MASTER g,State_Master i,District_Master j,Post_Master k,emp_master l where a.Emp_Code=b.Emp_Code and b.Emp_Code=c.Emp_Code and a.Branch_ID=d.Branch_ID and a.Department_ID=e.Department_ID and b.Post_ID=k.Post_ID and k.District_ID=j.District_ID  and j.State_ID =i.State_ID and a.Reporting_to=l.Emp_Code and " & _
            " a.Cadre_ID=f.Cadre_ID and a.Designation_ID=g.Designation_ID  and a.emp_code=" & EmpCode.ToString() & " and a.post_id in(5,6,7) ").Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn = "1Ø" + DT.Rows(0)(1)
            Else
                CallBackReturn = "-1Ø-1"
            End If

        End If
    End Sub
End Class
