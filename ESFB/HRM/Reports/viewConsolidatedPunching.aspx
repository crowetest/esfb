﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="viewConsolidatedPunching.aspx.vb" Inherits="HRM_Reports_viewConsolidatedPunching" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <table align="center" style="width: 50%; margin:0px auto;">
        <tr>
                <td style="width:50%">
                    &nbsp;</td>
                <td style="width:50%">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
                </td>
            </tr>
            <tr>
                <td style="width:50%; text-align:right;">
                    Date&nbsp;&nbsp;&nbsp;</td>
                <td style="width:50%">
                    <asp:TextBox ID="txtFromDt" class="NormalText" runat="server" 
                    Width="40%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txtFromDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtFromDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
                </td>
            </tr>
            <tr>  <td style="width:50%">
                    &nbsp;</td>
                <td style="text-align:left;width:50%">
                    &nbsp;</td>
            </tr>
            <tr>  <td style="width:50%">
                    </td>
                <td style="text-align:left;width:50%">
                    <asp:Button ID="btnGenerate" runat="server" Font-Names="Cambria" 
                        Font-Size="12pt" Text="Generate" style="width:30%" />
                    <input id="btnExit" type="button" value="Exit" style="font-family:Cambria; font-size:12pt; width:30%" onclick="return btnExit_onclick()" /></td>
            </tr>
            <tr>  <td style="width:50%">
                    &nbsp;</td>
                <td style="text-align:left;width:50%">
                    &nbsp;</td>
            </tr>
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

// ]]>
    </script>
</asp:Content>

