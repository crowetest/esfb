﻿
Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewEmployeeReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim rptID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 71) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim DT As New DataTable
            tb.Attributes.Add("width", "100%")
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            DT = DB.ExecuteDataSet("select a.Emp_Code,dbo.udfPropercase(a.Emp_Name),convert(varchar,a.Date_Of_Join,106),f.Cadre_name,c.Department_Name,d.Designation_Name,b.Branch_Name," & _
                                   " case when a.Last_Transfer_Dt is null then convert(varchar,a.Date_Of_Join,106) else convert(varchar,a.Last_Transfer_Dt,106) END,e.Emp_Name " & _
                    " from emp_master a,BRANCH_MASTER b,DEPARTMENT_MASTER c,DESIGNATION_MASTER d,EMP_MASTER e,CADRE_MASTER f where " & _
                    " a.Branch_ID = b.Branch_ID And a.Department_ID = c.Department_ID And a.Designation_ID = d.Designation_ID And a.Reporting_To = e.Emp_Code and a.Cadre_ID=f.Cadre_ID " & _
                    " and a.Status_ID=1 and a.Emp_Type_ID <>4 AND a.EMP_STATUS_ID<>6").Tables(0)
            RH.Heading(CStr(Session("FirmName")), tb, "EMPLOYEE LIST", 100)

            Dim TR0 As New TableRow
            TR0.BackColor = Drawing.Color.WhiteSmoke
            Dim TR0_00, TR0_01, TR0_02, TR0_03, TR0_04, TR0_05, TR0_06, TR0_07, TR0_08 As New TableCell
            RH.AddColumn(TR0, TR0_00, 5, 5, "l", "#")
            RH.AddColumn(TR0, TR0_01, 10, 10, "l", "Emp.Code")
            RH.AddColumn(TR0, TR0_02, 20, 20, "l", "Name")
            RH.AddColumn(TR0, TR0_03, 12, 12, "c", "Date Of Joining")
            RH.AddColumn(TR0, TR0_04, 5, 5, "l", "Cadre")
            RH.AddColumn(TR0, TR0_05, 12, 12, "l", "Department")
            RH.AddColumn(TR0, TR0_06, 12, 12, "l", "Designation")
            RH.AddColumn(TR0, TR0_07, 12, 12, "l", "Branch")
            RH.AddColumn(TR0, TR0_08, 12, 12, "c", "DOJ in the present office")
            Dim i As Integer = 0
            tb.Controls.Add(TR0)

            RH.DrawLine(tb, 100)
            RH.BlankRow(tb, 1)
            If DT.Rows.Count > 0 Then
                For Each DR In DT.Rows
                    Dim TR55 As New TableRow
                    i = i + 1
                    Dim TR55_00, TR55_01, TR55_02, TR55_03, TR55_04, TR55_05, TR55_06, TR55_07, TR55_08 As New TableCell
                    RH.AddColumn(TR55, TR55_00, 5, 5, "l", i)
                    RH.AddColumn(TR55, TR55_01, 10, 10, "l", " <a href='viewEmployeeDetails.aspx?EmpCode=" + GF.Encrypt(DR(0).ToString) + "&Name=" + GF.Encrypt(DR(1).ToString) + "&DOJ=" + GF.Encrypt(DR(2).ToString) + "&Cadre=" + GF.Encrypt(DR(3).ToString) + "&Dep=" + GF.Encrypt(DR(4).ToString) + "&Des=" + GF.Encrypt(DR(5).ToString) + "&Branch=" + GF.Encrypt(DR(6).ToString) + " '>" + DR(0).ToString())
                    RH.AddColumn(TR55, TR55_02, 20, 20, "l", DR(1))
                    RH.AddColumn(TR55, TR55_03, 12, 12, "c", CDate(DR(2)).ToString("dd-MMM-yyyy"))
                    RH.AddColumn(TR55, TR55_04, 5, 5, "l", DR(3))
                    RH.AddColumn(TR55, TR55_05, 12, 12, "l", DR(4))
                    RH.AddColumn(TR55, TR55_06, 12, 12, "l", DR(5))
                    RH.AddColumn(TR55, TR55_07, 12, 12, "l", DR(6))
                    RH.AddColumn(TR55, TR55_08, 12, 12, "c", CDate(DR(7)).ToString("dd-MMM-yyyy"))


                    tb.Controls.Add(TR55)
                Next
                RH.DrawLine(tb, 100)
            End If
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
