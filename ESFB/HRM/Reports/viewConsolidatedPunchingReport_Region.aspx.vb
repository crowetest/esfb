﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewConsolidatedPunchingReport_Region
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim rptID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 115) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            rptID = 1 'CInt(Request.QueryString.Get("rptID"))
            hdnReportID.Value = 1 'rptID.ToString()
            ' Dim FromDt As Date = CDate(Request.QueryString.Get("FromDt"))
            ' Dim EmpCode As Integer = CInt(Request.QueryString.Get("UserID"))
            Dim BranchName As String = ""
            If rptID = 1 Then
                Dim DT As New DataTable
                Dim TraDt As DateTime = CDate(Request.QueryString.Get("TraDt"))
                Dim Sql As String = "select a.Region_ID,a.Region_Name,sum(case when b.Emp_Type_ID=2 then 1 else 0 end)as FieldStaff,sum(case when ((c.M_Time is not null or c.e_time is not null ) and b.Emp_Type_ID=2 and c.FS_Approved_by is not null) then 1 else 0 end) as FS_Actual_Present,sum(case when ((c.M_Time is  null and c.e_time is  null ) and b.Emp_Type_ID=2 and c.FS_Approved_by is not null) then 1 else 0 end) as FS_Approve_Present,sum(case when ((c.M_Time is not  null or c.e_time is not null) and b.Emp_Type_ID=2 and c.FS_Approved_by is  null) then 1 else 0 end) as FS_Present,sum(case when b.Emp_Type_ID<>2 then 1 else 0 end)as NormalStaff,sum(case when ((c.M_Time is not null or c.e_time IS not null) and b.Emp_Type_ID <>2) then 1 else 0 end) as present from BrMaster a,EMP_MASTER b,Attend_All c where a.Branch_ID=b.Branch_ID and b.Emp_Code=c.Emp_Code and b.Emp_Category=1 and Tra_Dt ='" + TraDt.ToString("MM/dd/yyyy") + "' "
               
                Sql += " group by a.Region_ID,a.Region_Name"
                DT = DB.ExecuteDataSet(Sql).Tables(0)
                RH.Heading(Session("FirmName"), tb, "CONSOLIDATED PUNCHING REPORT OF " + TraDt.ToString("dd/MMM/yyyy") + "", 100)
                Dim RowBG As Integer = 0
                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")
                ' tb.Attributes.Add("border", "1")
                Dim TR0 As New TableRow

                Dim TR0_00, TR0_01, TR0_02, TR0_03 As New TableCell
                TR0_00.BackColor = Drawing.Color.WhiteSmoke
                TR0_01.BackColor = Drawing.Color.WhiteSmoke
                TR0_02.BackColor = Drawing.Color.LightBlue
                TR0_03.BackColor = Drawing.Color.LightPink
                RH.AddColumn(TR0, TR0_00, 5, 5, "c", "SI No")
                RH.AddColumn(TR0, TR0_01, 15, 15, "l", "Region")
                RH.AddColumn(TR0, TR0_02, 40, 40, "c", "Normal Staff")
                RH.AddColumn(TR0, TR0_03, 40, 40, "c", "Field Staff")
                tb.Controls.Add(TR0)
                Dim TR01 As New TableRow
                TR01.BackColor = Drawing.Color.WhiteSmoke
                Dim TR01_00, TR01_01, TR01_02, TR01_03, TR01_04, TR01_05, TR01_06, TR01_07, TR01_08 As New TableCell
                TR01_00.BackColor = Drawing.Color.WhiteSmoke
                TR01_01.BackColor = Drawing.Color.LightBlue
                TR01_02.BackColor = Drawing.Color.LightBlue
                TR01_03.BackColor = Drawing.Color.LightBlue
                TR01_04.BackColor = Drawing.Color.LightBlue
                TR01_05.BackColor = Drawing.Color.LightPink
                TR01_06.BackColor = Drawing.Color.LightPink
                TR01_07.BackColor = Drawing.Color.LightPink
                TR01_08.BackColor = Drawing.Color.LightPink

                RH.AddColumn(TR01, TR01_00, 20, 20, "c", "")
                RH.AddColumn(TR01, TR01_01, 10, 10, "c", "Strength")
                RH.AddColumn(TR01, TR01_02, 10, 10, "c", "Present")
                RH.AddColumn(TR01, TR01_03, 10, 10, "c", "Absent")
                RH.AddColumn(TR01, TR01_04, 10, 10, "c", "Present %")
                RH.AddColumn(TR01, TR01_05, 10, 10, "c", "Strength")
                RH.AddColumn(TR01, TR01_06, 10, 10, "c", "Present")
                RH.AddColumn(TR01, TR01_07, 10, 10, "c", "Absent")
                RH.AddColumn(TR01, TR01_08, 10, 10, "c", "Present %")

                tb.Controls.Add(TR01)
                RH.DrawLine(tb, 100)
                Dim I As Integer = 1
                Dim TotalNormalStaff As Double = 0
                Dim TotalNormalPresent As Double = 0
                Dim TotalMarked As Double = 0
                Dim TotalFieldStaff As Double = 0
                Dim TotalApproved As Double = 0
                Dim TotalBoth As Double = 0
                Dim PresentPerc As Double = 0
                For Each DR In DT.Rows
                    Dim TR1 As New TableRow
                    Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07, TR1_08, TR1_09 As New TableCell
                    If RowBG = 0 Then
                        RowBG = 1
                        TR1_00.BackColor = Drawing.Color.White
                        TR1_01.BackColor = Drawing.Color.White
                        TR1_02.BackColor = Drawing.Color.Lavender
                        TR1_03.BackColor = Drawing.Color.Lavender
                        TR1_04.BackColor = Drawing.Color.Lavender
                        TR1_05.BackColor = Drawing.Color.Lavender
                        TR1_06.BackColor = Drawing.Color.LavenderBlush
                        TR1_07.BackColor = Drawing.Color.LavenderBlush
                        TR1_08.BackColor = Drawing.Color.LavenderBlush
                        TR1_09.BackColor = Drawing.Color.LavenderBlush
                    Else
                        RowBG = 0
                        TR1_00.BackColor = Drawing.Color.WhiteSmoke
                        TR1_01.BackColor = Drawing.Color.WhiteSmoke
                        TR1_02.BackColor = Drawing.Color.LightBlue
                        TR1_03.BackColor = Drawing.Color.LightBlue
                        TR1_04.BackColor = Drawing.Color.LightBlue
                        TR1_05.BackColor = Drawing.Color.LightBlue
                        TR1_06.BackColor = Drawing.Color.LightPink
                        TR1_07.BackColor = Drawing.Color.LightPink
                        TR1_08.BackColor = Drawing.Color.LightPink
                        TR1_09.BackColor = Drawing.Color.LightPink
                    End If

                    RH.AddColumn(TR1, TR1_00, 5, 5, "c", I.ToString())
                    RH.AddColumn(TR1, TR1_01, 15, 15, "l", "<a href='viewConsolidatedPunchingReport_Area.aspx?rptID=2 &PresentFlag=-1 &RegionID=" & GF.Encrypt(DR(0).ToString()) & " &AreaID=-1 &BRanchID=-1 &Tradt=" + TraDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(1).ToString() + "</a>")
                    RH.AddColumn(TR1, TR1_02, 10, 10, "c", DR(6))
                    RH.AddColumn(TR1, TR1_03, 10, 10, "c", DR(7))
                    If CDbl(DR(6)) = 0 Then
                        PresentPerc = 0
                    Else
                        PresentPerc = CDbl(DR(7)) * 100 / CDbl(DR(6))
                    End If

                    RH.AddColumn(TR1, TR1_04, 10, 10, "c", CDbl(DR(6)) - CDbl(DR(7)))
                    RH.AddColumn(TR1, TR1_05, 10, 10, "c", PresentPerc.ToString("###0.00"))
                    RH.AddColumn(TR1, TR1_06, 10, 10, "c", DR(2))
                    RH.AddColumn(TR1, TR1_07, 10, 10, "c", DR(3))
                    RH.AddColumn(TR1, TR1_08, 10, 10, "c", CDbl(DR(2)) - CDbl(DR(3)))
                    If CDbl(DR(2)) = 0 Then
                        PresentPerc = 0
                    Else
                        PresentPerc = CDbl(DR(3)) * 100 / CDbl(DR(2))
                    End If

                    RH.AddColumn(TR1, TR1_09, 10, 10, "c", PresentPerc.ToString("###0.00"))
                    tb.Controls.Add(TR1)
                    I = I + 1
                    TotalNormalStaff += CDbl(DR(6))
                    TotalNormalPresent += CDbl(DR(7))
                    TotalFieldStaff += CDbl(DR(2))
                    TotalMarked += CDbl(DR(5))
                    TotalApproved += CDbl(DR(4))
                    TotalBoth += CDbl(DR(3))
                Next
                RH.DrawLine(tb, 100)

                Dim TRTotal As New TableRow
                TRTotal.BackColor = Drawing.Color.WhiteSmoke
                Dim TRTotal_00, TRTotal_01, TRTotal_02, TRTotal_03, TRTotal_04, TRTotal_05, TRTotal_06, TRTotal_07, TRTotal_08, TRTotal_09 As New TableCell
                TRTotal_00.BackColor = Drawing.Color.WhiteSmoke
                TRTotal_01.BackColor = Drawing.Color.WhiteSmoke
                TRTotal_02.BackColor = Drawing.Color.LightBlue
                TRTotal_03.BackColor = Drawing.Color.LightBlue
                TRTotal_04.BackColor = Drawing.Color.LightBlue
                TRTotal_05.BackColor = Drawing.Color.LightBlue
                TRTotal_06.BackColor = Drawing.Color.LightPink
                TRTotal_07.BackColor = Drawing.Color.LightPink
                TRTotal_08.BackColor = Drawing.Color.LightPink
                TRTotal_09.BackColor = Drawing.Color.LightPink

                RH.AddColumn(TRTotal, TRTotal_00, 5, 5, "c", "")
                RH.AddColumn(TRTotal, TRTotal_01, 15, 15, "l", "TOTAL")
                RH.AddColumn(TRTotal, TRTotal_02, 10, 10, "c", "<a href='viewConsolidatedReport.aspx?rptID=" + GF.Encrypt("2") + " &PresentFlag=1 &RegionID=" + GF.Encrypt("-1") + " &AreaID=" + GF.Encrypt("-1") + " &BRanchID=" + GF.Encrypt("-1") + " &Tradt=" + TraDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + TotalNormalStaff.ToString() + "</a>")
                If TotalNormalPresent > 0 Then
                    RH.AddColumn(TRTotal, TRTotal_03, 10, 10, "c", "<a href='viewConsolidatedReport.aspx?rptID=" + GF.Encrypt("2") + " &PresentFlag=2  &RegionID=" + GF.Encrypt("-1") + " &AreaID=" + GF.Encrypt("-1") + " &BRanchID=" + GF.Encrypt("-1") + " &Tradt=" + TraDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + TotalNormalPresent.ToString() + "</a>")
                Else
                    RH.AddColumn(TRTotal, TRTotal_03, 10, 10, "c", TotalNormalPresent.ToString())
                End If
                If (TotalNormalStaff - TotalNormalPresent) > 0 Then
                    RH.AddColumn(TRTotal, TRTotal_04, 10, 10, "c", "<a href='viewConsolidatedReport.aspx?rptID=" + GF.Encrypt("2") + " &PresentFlag=3  &RegionID=" + GF.Encrypt("-1") + " &AreaID=" + GF.Encrypt("-1") + " &BRanchID=" + GF.Encrypt("-1") + " &Tradt=" + TraDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + (TotalNormalStaff - TotalNormalPresent).ToString() + "</a>")
                Else
                    RH.AddColumn(TRTotal, TRTotal_04, 10, 10, "c", (TotalNormalStaff - TotalNormalPresent).ToString())
                End If
                If TotalNormalStaff > 0 Then
                    RH.AddColumn(TRTotal, TRTotal_05, 10, 10, "c", (CDbl((TotalNormalPresent * 100) / TotalNormalStaff)).ToString("###0.00"))
                Else
                    RH.AddColumn(TRTotal, TRTotal_05, 10, 10, "c", "0.00")
                End If

                If TotalFieldStaff > 0 Then
                    RH.AddColumn(TRTotal, TRTotal_06, 10, 10, "c", "<a href='viewConsolidatedReport.aspx?rptID=" + GF.Encrypt("2") + " &PresentFlag=4  &RegionID=" + GF.Encrypt("-1") + " &AreaID=" + GF.Encrypt("-1") + " &BRanchID=" + GF.Encrypt("-1") + " &Tradt=" + TraDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + TotalFieldStaff.ToString() + "</a>")
                Else
                    RH.AddColumn(TRTotal, TRTotal_06, 10, 10, "c", TotalFieldStaff.ToString())
                End If
                If TotalBoth > 0 Then
                    RH.AddColumn(TRTotal, TRTotal_07, 10, 10, "c", "<a href='viewConsolidatedReport.aspx?rptID=" + GF.Encrypt("2") + " &PresentFlag=5  &RegionID=" + GF.Encrypt("-1") + " &AreaID=" + GF.Encrypt("-1") + " &BRanchID=" + GF.Encrypt("-1") + " &Tradt=" + TraDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + TotalBoth.ToString() + "</a>")
                Else
                    RH.AddColumn(TRTotal, TRTotal_07, 10, 10, "c", TotalBoth.ToString())
                End If
                If ((TotalFieldStaff - TotalBoth) > 0) Then
                    RH.AddColumn(TRTotal, TRTotal_08, 10, 10, "c", "<a href='viewConsolidatedReport.aspx?rptID=" + GF.Encrypt("2") + " &PresentFlag=6  &RegionID=" + GF.Encrypt("-1") + " &AreaID=" + GF.Encrypt("-1") + " &BRanchID=" + GF.Encrypt("-1") + " &Tradt=" + TraDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + (TotalFieldStaff - TotalBoth).ToString() + "</a>")
                Else
                    RH.AddColumn(TRTotal, TRTotal_08, 10, 10, "c", (TotalFieldStaff - TotalBoth).ToString())
                End If
                If TotalFieldStaff > 0 Then
                    RH.AddColumn(TRTotal, TRTotal_09, 10, 10, "c", (CDbl((TotalBoth * 100) / TotalFieldStaff)).ToString("###0.00"))
                Else
                    RH.AddColumn(TRTotal, TRTotal_09, 10, 10, "c", "0.00")
                End If

                tb.Controls.Add(TRTotal)
                RH.DrawLine(tb, 100)
            End If
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
