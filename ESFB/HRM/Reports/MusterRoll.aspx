﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="MusterRoll.aspx.vb" Inherits="Reports_MusterRoll" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Calculations.js" type="text/javascript"></script>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function TableFill() {
          
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:50%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:left;margin: 0px auto;' align='left'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='left'>";
            tab += "<tr >";
            tab += "<td style='width:5%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:5%;text-align:left' >Type Abbr.</td>";
            tab += "<td style='width:15%;text-align:left'>Desription</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hdnDisp.ClientID %>").value != "") 
            {
                var row = document.getElementById("<%= hdnDisp.ClientID %>").value.split("Ñ");
                for (n = 1; n <= row.length - 1; n++) 
                {
                    col = row[n].split("ÿ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    tab += "<td style='width:5%;text-align:center'>" + col[0] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:15%;text-align:left'>" + col[2] + "</td>";
                    tab += "</tr>";
                }
                tab += "</table></div></div></div>";
               
            }
        }

        function GenerateOnClick() {
            var MonthID = document.getElementById("<%= cmbMonth.ClientID %>").value;
            if (MonthID == "-1")
            { alert("Select Month"); document.getElementById("<%= cmbMonth.ClientID %>").focus(); return false; }
           
        }


        function MonthOnChange()
                {
                    var MonthID= document.getElementById("<%= cmbMonth.ClientID %>").value;
                    if (MonthID != "-1")
                    { document.getElementById("<%= lblYear.ClientID %>").innerHTML = document.getElementById("<%= hdnDate.ClientID %>").value; }
                    else
                    {document.getElementById("<%= lblYear.ClientID %>").innerHTML = ""; }
                }
                

    </script>
    <table style="width:80%;margin: 0px auto;" >
        <tr>
            <td style="width:25%;">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            </td>
            <td style="width:12%; text-align:left;">
                &nbsp;</td>
            <td style="width:63%">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:25%;">
                &nbsp;</td>
            <td style="width:12%; text-align:left;">
                Month</td>
            <td style="width:63%">
                <asp:DropDownList ID="cmbMonth" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="60%" ForeColor="Black">
                     <asp:ListItem Value="-1"> -----SELECT-----</asp:ListItem>
                     <asp:ListItem Value="1">JANUARY</asp:ListItem>
                     <asp:ListItem Value="2">FEBRUARY</asp:ListItem>
                     <asp:ListItem Value="3">MARCH</asp:ListItem>
                     <asp:ListItem Value="4">APRIL</asp:ListItem>
                     <asp:ListItem Value="5">MAY</asp:ListItem>
                     <asp:ListItem Value="6">JUNE</asp:ListItem>
                     <asp:ListItem Value="7">JULY</asp:ListItem>
                     <asp:ListItem Value="8">AUGUST</asp:ListItem>
                     <asp:ListItem Value="9">SEPTEMBER</asp:ListItem>
                     <asp:ListItem Value="10">OCTOBER</asp:ListItem>
                     <asp:ListItem Value="11">NOVEMBER</asp:ListItem>
                     <asp:ListItem Value="12">DECEMBER</asp:ListItem>
            </asp:DropDownList>
               &nbsp;&nbsp;&nbsp;<asp:Label ID="lblYear" runat="server" Font-Names="Cambria" Font-Size="10pt" 
                    Text="" ></asp:Label>
            </td>
        </tr>
     
        <tr>
            <td style="text-align:center;"  colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center; height: 30px;"  colspan="3">
                &nbsp; 
                <asp:Button ID="btnGenerate" runat="server" Text="GENERATE" Width="10%" style="font-family: cambria; cursor:pointer"  />
                &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" 
                type="button" value="EXIT" onclick="window.open('../../Home.aspx','_self')"  /></td>
        </tr>
        <tr >
            <td style="text-align:center; height: 68px;" colspan="3">
                <br />
                &nbsp;&nbsp;
                <asp:HiddenField ID="hdnDate" runat="server" />
                <asp:HiddenField ID="hdnValue" runat="server" />
                <asp:HiddenField ID="hdnDisp" runat="server" />
            </td>
        </tr>
    </table>
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return TableFill()
// ]]>
</script>
</asp:Content>
