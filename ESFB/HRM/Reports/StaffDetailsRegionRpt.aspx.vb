﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class NewCompliance_StaffDetailsRegionRpt
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim TB As New Table
    Dim AsOnDt As String
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 167) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT, DT_DATE As New DataTable
            TB.Attributes.Add("width", "100%")
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            DT_DATE = DB.ExecuteDataSet("select distinct PROCESS_DT from BRANCH_STAFF_DETAIL").Tables(0)
            If (DT_DATE.Rows.Count > 0) Then
                AsOnDt = CStr(Format(DT_DATE.Rows(0)(0), "dd/MMM/yyyy"))
            End If

            RH.Heading(CStr(Session("FirmName")), TB, "STAFF DETAILS AS ON " & AsOnDt & "", 100)

            RH.BlankRow(TB, 5)

            '---------------
            DT = DB.ExecuteDataSet("SELECT r.Region_ID,a.Area_ID,b.Branch_ID,r.Region_Name,a.Area_Name,b.Branch_Name,s.SBMC,s.SBMT,s.BMC,s.BMT,s.ABMC,s.ABMT,s.CCAC,s.CCAT,s.JCOC,s.JCOT,s.ACOC,s.ACOT,s.COC,s.COTT FROM BRANCH_STAFF_DETAIL s, BRANCH_MASTER b, AREA_MASTER a, REGION_MASTER r where s.BRANCH_ID=b.Branch_ID and b.Area_ID=a.Area_ID and a.Region_ID=r.Region_ID and r.Region_ID<>0 order by r.Region_ID,a.Area_ID,b.Branch_ID").Tables(0)

            If DT.Rows.Count > 0 Then
                Dim TR0 As New TableRow
                TR0.BackColor = Drawing.Color.WhiteSmoke
                Dim TR0_00, TR0_01, TR0_02, TR0_03, TR0_04 As New TableCell

                TR0_00.BorderWidth = "1"
                TR0_01.BorderWidth = "1"
                TR0_02.BorderWidth = "1"
                TR0_03.BorderWidth = "1"
                TR0_04.BorderWidth = "1"

                TR0_00.BorderStyle = BorderStyle.Solid
                TR0_01.BorderStyle = BorderStyle.Solid
                TR0_02.BorderStyle = BorderStyle.Solid
                TR0_03.BorderStyle = BorderStyle.Solid
                TR0_04.BorderStyle = BorderStyle.Solid

                TR0_00.BorderColor = Drawing.Color.Silver
                TR0_01.BorderColor = Drawing.Color.Silver
                TR0_02.BorderColor = Drawing.Color.Silver
                TR0_03.BorderColor = Drawing.Color.Silver
                TR0_04.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TR0, TR0_00, 10, 10, "c", "Region")
                RH.AddColumn(TR0, TR0_01, 10, 10, "c", "Area")
                RH.AddColumn(TR0, TR0_02, 5, 5, "c", "Sl No")
                RH.AddColumn(TR0, TR0_03, 10, 10, "c", "Branch")
                RH.AddColumn(TR0, TR0_04, 65, 65, "c", "Actual")

                TB.Controls.Add(TR0)

                Dim TRSec0 As New TableRow
                TRSec0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRSec0_00, TRSec0_01, TRSec0_02, TRSec0_03, TRSec0_04, TRSec0_05, TRSec0_06, TRSec0_07, TRSec0_08, TRSec0_09, TRSec0_10, TRSec0_11, TRSec0_12 As New TableCell

                TRSec0_00.BorderWidth = "1"
                TRSec0_01.BorderWidth = "1"
                TRSec0_02.BorderWidth = "1"
                TRSec0_03.BorderWidth = "1"
                TRSec0_04.BorderWidth = "1"
                TRSec0_05.BorderWidth = "1"
                TRSec0_06.BorderWidth = "1"
                TRSec0_07.BorderWidth = "1"
                TRSec0_08.BorderWidth = "1"
                TRSec0_09.BorderWidth = "1"
                TRSec0_10.BorderWidth = "1"
                TRSec0_11.BorderWidth = "1"
                TRSec0_12.BorderWidth = "1"

                TRSec0_00.BorderStyle = BorderStyle.Solid
                TRSec0_01.BorderStyle = BorderStyle.Solid
                TRSec0_02.BorderStyle = BorderStyle.Solid
                TRSec0_03.BorderStyle = BorderStyle.Solid
                TRSec0_04.BorderStyle = BorderStyle.Solid
                TRSec0_05.BorderStyle = BorderStyle.Solid
                TRSec0_06.BorderStyle = BorderStyle.Solid
                TRSec0_07.BorderStyle = BorderStyle.Solid
                TRSec0_08.BorderStyle = BorderStyle.Solid
                TRSec0_09.BorderStyle = BorderStyle.Solid
                TRSec0_10.BorderStyle = BorderStyle.Solid
                TRSec0_11.BorderStyle = BorderStyle.Solid
                TRSec0_12.BorderStyle = BorderStyle.Solid

                TRSec0_00.BorderColor = Drawing.Color.Silver
                TRSec0_01.BorderColor = Drawing.Color.Silver
                TRSec0_02.BorderColor = Drawing.Color.Silver
                TRSec0_03.BorderColor = Drawing.Color.Silver
                TRSec0_04.BorderColor = Drawing.Color.Silver
                TRSec0_05.BorderColor = Drawing.Color.Silver
                TRSec0_06.BorderColor = Drawing.Color.Silver
                TRSec0_07.BorderColor = Drawing.Color.Silver
                TRSec0_08.BorderColor = Drawing.Color.Silver
                TRSec0_09.BorderColor = Drawing.Color.Silver
                TRSec0_10.BorderColor = Drawing.Color.Silver
                TRSec0_11.BorderColor = Drawing.Color.Silver
                TRSec0_12.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRSec0, TRSec0_00, 10, 10, "c", "")
                RH.AddColumn(TRSec0, TRSec0_01, 10, 10, "c", "")
                RH.AddColumn(TRSec0, TRSec0_02, 5, 5, "c", "")
                RH.AddColumn(TRSec0, TRSec0_03, 10, 10, "c", "")
                RH.AddColumn(TRSec0, TRSec0_04, 7, 7, "c", "SBM")
                RH.AddColumn(TRSec0, TRSec0_05, 7, 7, "c", "BM")
                RH.AddColumn(TRSec0, TRSec0_06, 7, 7, "c", "ABM")
                RH.AddColumn(TRSec0, TRSec0_07, 7, 7, "c", "CCA")
                RH.AddColumn(TRSec0, TRSec0_08, 7, 7, "c", "JCO")
                RH.AddColumn(TRSec0, TRSec0_09, 7, 7, "c", "ACO")
                RH.AddColumn(TRSec0, TRSec0_10, 7, 7, "c", "CO")
                RH.AddColumn(TRSec0, TRSec0_11, 7, 7, "c", "Total")
                RH.AddColumn(TRSec0, TRSec0_12, 9, 9, "c", "Total Staff")

                TB.Controls.Add(TRSec0)


                Dim TRSec220 As New TableRow
                TRSec220.BackColor = Drawing.Color.WhiteSmoke
                Dim TRSec220_00, TRSec220_01, TRSec220_02, TRSec220_03, TRSec220_04, TRSec220_05, TRSec220_06, TRSec220_07, TRSec220_08, TRSec220_09, TRSec220_10, TRSec220_11, TRSec220_12, TRSec220_13, TRSec220_14, TRSec220_15, TRSec220_16, TRSec220_17, TRSec220_18, TRSec220_19, TRSec220_20 As New TableCell

                TRSec220_00.BorderWidth = "1"
                TRSec220_01.BorderWidth = "1"
                TRSec220_02.BorderWidth = "1"
                TRSec220_03.BorderWidth = "1"
                TRSec220_04.BorderWidth = "1"
                TRSec220_05.BorderWidth = "1"
                TRSec220_06.BorderWidth = "1"
                TRSec220_07.BorderWidth = "1"
                TRSec220_08.BorderWidth = "1"
                TRSec220_09.BorderWidth = "1"
                TRSec220_10.BorderWidth = "1"
                TRSec220_11.BorderWidth = "1"
                TRSec220_12.BorderWidth = "1"
                TRSec220_13.BorderWidth = "1"
                TRSec220_14.BorderWidth = "1"
                TRSec220_15.BorderWidth = "1"
                TRSec220_16.BorderWidth = "1"
                TRSec220_17.BorderWidth = "1"
                TRSec220_18.BorderWidth = "1"
                TRSec220_19.BorderWidth = "1"
                TRSec220_20.BorderWidth = "1"

                TRSec220_00.BorderStyle = BorderStyle.Solid
                TRSec220_01.BorderStyle = BorderStyle.Solid
                TRSec220_02.BorderStyle = BorderStyle.Solid
                TRSec220_03.BorderStyle = BorderStyle.Solid
                TRSec220_04.BorderStyle = BorderStyle.Solid
                TRSec220_05.BorderStyle = BorderStyle.Solid
                TRSec220_06.BorderStyle = BorderStyle.Solid
                TRSec220_07.BorderStyle = BorderStyle.Solid
                TRSec220_08.BorderStyle = BorderStyle.Solid
                TRSec220_09.BorderStyle = BorderStyle.Solid
                TRSec220_10.BorderStyle = BorderStyle.Solid
                TRSec220_11.BorderStyle = BorderStyle.Solid
                TRSec220_12.BorderStyle = BorderStyle.Solid
                TRSec220_13.BorderStyle = BorderStyle.Solid
                TRSec220_14.BorderStyle = BorderStyle.Solid
                TRSec220_15.BorderStyle = BorderStyle.Solid
                TRSec220_16.BorderStyle = BorderStyle.Solid
                TRSec220_17.BorderStyle = BorderStyle.Solid
                TRSec220_18.BorderStyle = BorderStyle.Solid
                TRSec220_19.BorderStyle = BorderStyle.Solid
                TRSec220_20.BorderStyle = BorderStyle.Solid

                TRSec220_00.BorderColor = Drawing.Color.Silver
                TRSec220_01.BorderColor = Drawing.Color.Silver
                TRSec220_02.BorderColor = Drawing.Color.Silver
                TRSec220_03.BorderColor = Drawing.Color.Silver
                TRSec220_04.BorderColor = Drawing.Color.Silver
                TRSec220_05.BorderColor = Drawing.Color.Silver
                TRSec220_06.BorderColor = Drawing.Color.Silver
                TRSec220_07.BorderColor = Drawing.Color.Silver
                TRSec220_08.BorderColor = Drawing.Color.Silver
                TRSec220_09.BorderColor = Drawing.Color.Silver
                TRSec220_10.BorderColor = Drawing.Color.Silver
                TRSec220_11.BorderColor = Drawing.Color.Silver
                TRSec220_12.BorderColor = Drawing.Color.Silver
                TRSec220_13.BorderColor = Drawing.Color.Silver
                TRSec220_14.BorderColor = Drawing.Color.Silver
                TRSec220_15.BorderColor = Drawing.Color.Silver
                TRSec220_16.BorderColor = Drawing.Color.Silver
                TRSec220_17.BorderColor = Drawing.Color.Silver
                TRSec220_18.BorderColor = Drawing.Color.Silver
                TRSec220_19.BorderColor = Drawing.Color.Silver
                TRSec220_20.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRSec220, TRSec220_00, 10, 10, "c", "")
                RH.AddColumn(TRSec220, TRSec220_01, 10, 10, "c", "")
                RH.AddColumn(TRSec220, TRSec220_02, 5, 5, "c", "")
                RH.AddColumn(TRSec220, TRSec220_03, 10, 10, "c", "")
                RH.AddColumn(TRSec220, TRSec220_04, 3, 3, "c", "C")
                RH.AddColumn(TRSec220, TRSec220_05, 4, 4, "c", "T")
                RH.AddColumn(TRSec220, TRSec220_06, 3, 3, "c", "C")
                RH.AddColumn(TRSec220, TRSec220_07, 4, 4, "c", "T")
                RH.AddColumn(TRSec220, TRSec220_08, 3, 3, "c", "C")
                RH.AddColumn(TRSec220, TRSec220_09, 4, 4, "c", "T")
                RH.AddColumn(TRSec220, TRSec220_10, 3, 3, "c", "C")
                RH.AddColumn(TRSec220, TRSec220_11, 4, 4, "c", "T")
                RH.AddColumn(TRSec220, TRSec220_12, 3, 3, "c", "C")
                RH.AddColumn(TRSec220, TRSec220_13, 4, 4, "c", "T")
                RH.AddColumn(TRSec220, TRSec220_14, 3, 3, "c", "C")
                RH.AddColumn(TRSec220, TRSec220_15, 4, 4, "c", "T")
                RH.AddColumn(TRSec220, TRSec220_16, 3, 3, "c", "C")
                RH.AddColumn(TRSec220, TRSec220_17, 4, 4, "c", "T")
                RH.AddColumn(TRSec220, TRSec220_18, 3, 3, "c", "C")
                RH.AddColumn(TRSec220, TRSec220_19, 4, 4, "c", "T")
                RH.AddColumn(TRSec220, TRSec220_20, 9, 9, "c", "")

                TB.Controls.Add(TRSec220)

                Dim i As Integer = 0
                'RH.BlankRow(TB, 1)

                Dim RegionID As Integer = 1
                Dim NextRegion As Integer = 0

                Dim AreaID As Integer = 1
                Dim NextArea As Integer = 0

                For Each DR In DT.Rows
                    Dim TR55 As New TableRow
                    i = i + 1
                    Dim TR55_00, TR55_01, TR55_02, TR55_03, TR55_04, TR55_05, TR55_06, TR55_07, TR55_08, TR55_09, TR55_10, TR55_11, TR55_12, TR55_13, TR55_14, TR55_15, TR55_16, TR55_17, TR55_18, TR55_19, TR55_20 As New TableCell

                    TR55_00.BorderWidth = "1"
                    TR55_01.BorderWidth = "1"
                    TR55_02.BorderWidth = "1"
                    TR55_03.BorderWidth = "1"
                    TR55_04.BorderWidth = "1"
                    TR55_05.BorderWidth = "1"
                    TR55_06.BorderWidth = "1"
                    TR55_07.BorderWidth = "1"
                    TR55_08.BorderWidth = "1"
                    TR55_09.BorderWidth = "1"
                    TR55_10.BorderWidth = "1"
                    TR55_11.BorderWidth = "1"
                    TR55_12.BorderWidth = "1"
                    TR55_13.BorderWidth = "1"
                    TR55_14.BorderWidth = "1"
                    TR55_15.BorderWidth = "1"
                    TR55_16.BorderWidth = "1"
                    TR55_17.BorderWidth = "1"
                    TR55_18.BorderWidth = "1"
                    TR55_19.BorderWidth = "1"
                    TR55_20.BorderWidth = "1"

                    TR55_00.BorderStyle = BorderStyle.Solid
                    TR55_01.BorderStyle = BorderStyle.Solid
                    TR55_02.BorderStyle = BorderStyle.Solid
                    TR55_03.BorderStyle = BorderStyle.Solid
                    TR55_04.BorderStyle = BorderStyle.Solid
                    TR55_05.BorderStyle = BorderStyle.Solid
                    TR55_06.BorderStyle = BorderStyle.Solid
                    TR55_07.BorderStyle = BorderStyle.Solid
                    TR55_08.BorderStyle = BorderStyle.Solid
                    TR55_09.BorderStyle = BorderStyle.Solid
                    TR55_10.BorderStyle = BorderStyle.Solid
                    TR55_11.BorderStyle = BorderStyle.Solid
                    TR55_12.BorderStyle = BorderStyle.Solid
                    TR55_13.BorderStyle = BorderStyle.Solid
                    TR55_14.BorderStyle = BorderStyle.Solid
                    TR55_15.BorderStyle = BorderStyle.Solid
                    TR55_16.BorderStyle = BorderStyle.Solid
                    TR55_17.BorderStyle = BorderStyle.Solid
                    TR55_18.BorderStyle = BorderStyle.Solid
                    TR55_19.BorderStyle = BorderStyle.Solid
                    TR55_20.BorderStyle = BorderStyle.Solid

                    TR55_00.BorderColor = Drawing.Color.Silver
                    TR55_01.BorderColor = Drawing.Color.Silver
                    TR55_02.BorderColor = Drawing.Color.Silver
                    TR55_03.BorderColor = Drawing.Color.Silver
                    TR55_04.BorderColor = Drawing.Color.Silver
                    TR55_05.BorderColor = Drawing.Color.Silver
                    TR55_06.BorderColor = Drawing.Color.Silver
                    TR55_07.BorderColor = Drawing.Color.Silver
                    TR55_08.BorderColor = Drawing.Color.Silver
                    TR55_09.BorderColor = Drawing.Color.Silver
                    TR55_10.BorderColor = Drawing.Color.Silver
                    TR55_11.BorderColor = Drawing.Color.Silver
                    TR55_12.BorderColor = Drawing.Color.Silver
                    TR55_13.BorderColor = Drawing.Color.Silver
                    TR55_14.BorderColor = Drawing.Color.Silver
                    TR55_15.BorderColor = Drawing.Color.Silver
                    TR55_16.BorderColor = Drawing.Color.Silver
                    TR55_17.BorderColor = Drawing.Color.Silver
                    TR55_18.BorderColor = Drawing.Color.Silver
                    TR55_19.BorderColor = Drawing.Color.Silver
                    TR55_20.BorderColor = Drawing.Color.Silver

                    RegionID = DR(0)
                    AreaID = DR(1)
                    If (RegionID = NextRegion) Then
                        RH.AddColumn(TR55, TR55_00, 10, 10, "l", "")
                    Else
                        RH.AddColumn(TR55, TR55_00, 10, 10, "l", DR(3))
                    End If
                    If (AreaID = NextArea) Then
                        RH.AddColumn(TR55, TR55_01, 10, 10, "l", "")
                    Else
                        RH.AddColumn(TR55, TR55_01, 10, 10, "l", DR(4))
                    End If
                    RH.AddColumn(TR55, TR55_02, 5, 5, "c", i)
                    RH.AddColumn(TR55, TR55_03, 10, 10, "l", DR(5))
                    RH.AddColumn(TR55, TR55_04, 3, 3, "c", DR(6))
                    RH.AddColumn(TR55, TR55_05, 4, 4, "c", DR(7))
                    RH.AddColumn(TR55, TR55_06, 3, 3, "c", DR(8))
                    RH.AddColumn(TR55, TR55_07, 4, 4, "c", DR(9))
                    RH.AddColumn(TR55, TR55_08, 3, 3, "c", DR(10))
                    RH.AddColumn(TR55, TR55_09, 4, 4, "c", DR(11))
                    RH.AddColumn(TR55, TR55_10, 3, 3, "c", DR(12))
                    RH.AddColumn(TR55, TR55_11, 4, 4, "c", DR(13))
                    RH.AddColumn(TR55, TR55_12, 3, 3, "c", DR(14))
                    RH.AddColumn(TR55, TR55_13, 4, 4, "c", DR(15))
                    RH.AddColumn(TR55, TR55_14, 3, 3, "c", DR(16))
                    RH.AddColumn(TR55, TR55_15, 4, 4, "c", DR(17))
                    RH.AddColumn(TR55, TR55_16, 3, 3, "c", DR(18))
                    RH.AddColumn(TR55, TR55_17, 4, 4, "c", DR(19))

                    Dim TotC As Integer = DR(6) + DR(8) + DR(10) + DR(12) + DR(14) + DR(16) + DR(18)
                    Dim TotT As Integer = DR(7) + DR(9) + DR(11) + DR(13) + DR(15) + DR(17) + DR(19)

                    RH.AddColumn(TR55, TR55_18, 3, 3, "c", TotC)
                    RH.AddColumn(TR55, TR55_19, 4, 4, "c", TotT)

                    Dim TotStaff As Integer = TotC + TotT

                    RH.AddColumn(TR55, TR55_20, 9, 9, "c", TotStaff)

                    TB.Controls.Add(TR55)
                    NextRegion = RegionID
                    NextArea = AreaID
                Next
            End If
            pnDisplay.Controls.Add(TB)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
