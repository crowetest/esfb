﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_MusterRoll
    Inherits System.Web.UI.Page
    Dim rptID As Integer
    Dim CallBackReturn As String
    Dim WebTools As New WebApp.Tools
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 194) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Muster Roll"
            Me.hdnDate.Value = CDate(Session("TraDt")).Year.ToString()
            DT = GF.GetQueryResult("SELECT ID,TYPE_ABBR,TYPE_DESCRIPTION  FROM EXP_REMARKS_MASTER ORDER BY  ID")
            For Each DR As DataRow In DT.Rows
                Me.hdnDisp.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ" + DR(2).ToString()
            Next
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "TableFill();", True)
            Me.cmbMonth.Attributes.Add("onchange", "MonthOnChange()")
            Me.btnGenerate.Attributes.Add("onclick", "return GenerateOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try
            Dim HeaderText As String
            Dim MonthID As Integer = CInt(cmbMonth.SelectedValue)
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@MonthID", MonthID)}
            Dim ds As New DataSet
            ds = DB.ExecuteDataSet("SP_HR_ATTENDANCE_REPORT", Parameters)
            ds.Tables(0).TableName = "MusterRoll"
            ds.Tables(1).TableName = "Abbreviations"
            Dim DT As DataTable = ds.Tables(0)
            HeaderText = "MusterRoll-" + cmbMonth.SelectedItem.Text + "-" + lblYear.Text
            WebTools.ExporttoExcel(ds, HeaderText)
            '   ExporttoExcel(ds, HeaderText, 1)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
   
End Class
