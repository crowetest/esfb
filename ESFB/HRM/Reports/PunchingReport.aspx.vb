﻿Imports System.Data
Partial Class Reports_PunchingReport
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim rptID As Integer
    Dim CallBackReturn As String
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 26) = False And GF.FormAccess(CInt(Session("UserID")), 28) = False And GF.FormAccess(CInt(Session("UserID")), 29) = False And GF.FormAccess(CInt(Session("UserID")), 33) = False And GF.FormAccess(CInt(Session("UserID")), 172) = False And GF.FormAccess(CInt(Session("UserID")), 368) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            rptID = CInt(Request.QueryString.Get("ID"))
            txtEmpCode.Text = Session("UserID").ToString()
            txtName.Text = Session("UserName").ToString()
            txtFromDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            txttODt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim SubHead As String = ""
            If rptID = 1 Then
                SubHead = "Punching Report"
                'Response.Redirect("https://10.61.69.42/HRM/Reports/PunchingReport.aspx?ID=1 &TraDt=" + Session("TraDt") + " &EmpCode=" + Session("UserID").ToString() + " &EmpName=" + Session("UserName") + "", False)
            ElseIf rptID = 2 Then
                SubHead = "Leave Report"
            ElseIf rptID = 3 Then
                SubHead = "Travel Report"
            ElseIf rptID = 4 Then
                SubHead = "Leave verification Report"
            ElseIf rptID = 5 Then
                SubHead = "Punching Report"
                Me.txtEmpCode.ReadOnly = False
                Me.txtEmpCode.CssClass = "NormalText"
                Me.txtName.Text = ""
                txtEmpCode.Text = ""
                txtEmpCode.Focus()
            ElseIf rptID = 6 Then
                SubHead = "Regularization Report"
            ElseIf rptID = 7 Then
                SubHead = "Regularization Report"
                Me.txtEmpCode.ReadOnly = False
                Me.txtEmpCode.CssClass = "NormalText"
                Me.txtName.Text = ""
                txtEmpCode.Text = ""
                txtEmpCode.Focus()
            End If
            Me.Master.subtitle = SubHead
            txtFromDt.Attributes.Add("onchange", "return DateOnChange();")
            txtToDt.Attributes.Add("onchange", "return DateOnChange();")
            btnGenerate.Attributes.Add("onclick", "return GenerateOnClick()")
            txtEmpCode.Attributes.Add("onchange", "return EmpCodeOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try
            Dim Data() As String = Me.hdnValue.Value.Split("Ø")
            Response.Redirect("viewPunchingReport.aspx?rptID=" + GF.Encrypt(rptID.ToString()) + "&EmpCode=" + GF.Encrypt(Data(0)) + " &FromDt=" + Data(1) + " &ToDt=" + Data(2) + " &EmpName=" + GF.Encrypt(Data(3)) + "", False)
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If Data(0) = 1 Then
            Dim EmpCode As Integer = CInt(Data(1))
            Dim EN As New Enrollment
            Dim DT As New DataTable
            DT = EN.GetEmployee(EmpCode)
            If DT.Rows.Count > 0 Then
                CallBackReturn = "1Ø" + DT.Rows(0)(1)
            Else
                CallBackReturn = "-1Ø-1"
            End If

        End If
    End Sub
End Class
