﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf

Partial Class PunchingCalendar
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim rptID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return BackForm()")
            cmd_Next.Attributes.Add("onclick", "return NextForm()")
            cmd_Close.Attributes.Add("onclick", "return CloseForm()")

            rptID = CInt(Request.QueryString.Get("rptID"))
            hdnReportID.Value = rptID.ToString()

            Dim FromDt As Date = CDate(Request.QueryString.Get("FromDt"))
            Dim ToDt As Date = CDate(Request.QueryString.Get("ToDt"))
            Dim EmpCode As Integer = CInt(Request.QueryString.Get("EmpCode"))
            Dim EmpName As String = Request.QueryString.Get("EmpName")

            Me.hdnEmpCode.Value = EmpCode
            Me.hdnEmpName.Value = EmpName

            Dim MonthID As Integer = FromDt.Month
            Dim CurrentDay As Date = Date.Now().ToString("d")
            Dim CurrMonthID As Integer = CurrentDay.Month

            Dim MName As String = FromDt.ToString("MMMM")
            Dim CurYear As Integer = FromDt.Year

            Dim PreviousMonthFirstDay As Date = DateSerial(Year(FromDt), Month(FromDt) - 1, 1)
            Dim PreviousMonthLastDay As Date = DateSerial(Year(FromDt), Month(FromDt), 0)

            Me.hdnPrevFirst.Value = CStr(PreviousMonthFirstDay)
            Me.hdnPrevLast.Value = CStr(PreviousMonthLastDay)

            Dim NextMonthFirstDay As Date = DateSerial(Year(FromDt), Month(FromDt) + 1, 1)
            Dim NextMonthLastDay As Date = FromDt.AddMonths(2).AddDays(-(FromDt.AddMonths(2).Day))

            Me.hdnNextFirst.Value = CStr(NextMonthFirstDay)
            Me.hdnNextLast.Value = CStr(NextMonthLastDay)

            RH.Heading(Session("FirmName"), tb, CurYear & " " & MName & " Month Punching Report of " + EmpName + " (" + EmpCode.ToString() + ") ", 100)
            'RH.SubHeading(tb, 100, "l", "Employee     :&nbsp;" + EmpName + "&nbsp;(" + EmpCode.ToString() + ")")

            'RH.DrawLine(tb, 100)
            Dim TR7 As New TableRow
            Dim TR7_00, TR7_01, TR7_02, TR7_03, TR7_04, TR7_05, TR7_06 As New TableCell
            RH.InsertColumn(TR7, TR7_00, 15, 2, "<font size=4; ><b>Sunday</b></font>")
            TR7_00.ForeColor = Drawing.Color.Red
            RH.InsertColumn(TR7, TR7_01, 14, 2, "<font size=4; ><b>Monday</b></font>")
            RH.InsertColumn(TR7, TR7_02, 14, 2, "<font size=4; ><b>Tuesday</b></font>")
            RH.InsertColumn(TR7, TR7_03, 14, 2, "<font size=4; ><b>Wednesday</b></font>")
            RH.InsertColumn(TR7, TR7_04, 14, 2, "<font size=4; ><b>Thurseday</b></font>")
            RH.InsertColumn(TR7, TR7_05, 14, 2, "<font size=4; ><b>Friday</b></font>")
            RH.InsertColumn(TR7, TR7_06, 15, 2, "<font size=4; ><b>Saturday</b></font>")
            tb.Controls.Add(TR7)
            TR7.BackColor = Drawing.Color.OldLace

            RH.DrawLine(tb, 100)

            Dim DT As New DataTable
            If MonthID = CurrMonthID Then
                DT = DB.ExecuteDataSet("select a.tra_dt,DATEPART(dw,a.Tra_Dt),DATEPART(DD,a.Tra_Dt),case when a.M_Time = 'NM-REG' then 'NM-REG' when a.M_Time = 'Travel' then 'Travel'  else isnull(FORMAT(CAST(a.M_Time AS DATETIME),'hh:mm tt'),'') end Moring,case when a.E_Time = 'NM-REG' then 'NM-REG' when a.E_Time = 'Travel' then 'Travel' else isnull(FORMAT(CAST(a.E_Time AS DATETIME),'hh:mm tt'),'') end Evening,(select isnull(COUNT(*),0) from HOLIDAY_MASTER h, branch_master b where h.Tra_Dt=a.Tra_Dt and h.Branch_ID=b.Branch_ID and b.Branch_ID=a.Branch_ID and h.Status_ID=1 and h.Branch_ID=b.Branch_ID),(select h.Description from HOLIDAY_MASTER h, branch_master b where h.Tra_Dt=a.Tra_Dt and h.branch_id=b.branch_id and b.Branch_ID=a.Branch_ID and h.Status_ID=1 and h.Branch_ID=b.Branch_ID) holidayDes,case when DATEPART(dw,a.Tra_Dt) = 7 AND DATEPART(dd, a.Tra_Dt) BETWEEN 8 AND 14 then 1 else 0 end SecondSat,case when DATEPART(dw,a.Tra_Dt) = 7 AND DATEPART(dd, a.Tra_Dt) BETWEEN 22 AND 28 then 1 else 0 end FourthSat,case when a.M_Time is null AND a.E_Time is null then isnull(isnull((select dbo.udfProperCase(y.Type_Name)+'Œ'+CONVERT(varchar,x.Status_ID) from EMP_LEAVE_REQUEST x, EMP_LEAVE_TYPE y where x.emp_code=a.Emp_Code and a.Tra_Dt between x.Leave_From and x.Leave_To and x.Leave_Type=y.Type_ID and x.Status_ID not in(5,6)),(select 'UnApprovedTravelŒ'+CONVERT(varchar,t.Status_ID) from EMP_TOUR_MASTER t where t.userid=a.Emp_Code and a.Tra_Dt between t.Tour_From and t.Tour_To and t.status_id=0)),'Unauthorised AbsenceŒ0') else '' end Leave,isnull(a.att_remarks,'') from ATTEND_ALL a where a.Emp_Code=" + EmpCode.ToString() + " and a.Tra_Dt between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "' union all select datetable.Date,DATEPART(dw,datetable.Date),DATEPART(DD,datetable.Date),'' Morning,'' Evening,(select isnull(COUNT(*),0) from HOLIDAY_MASTER h, branch_master b, EMP_MASTER e where h.Tra_Dt=datetable.Date and h.branch_id=b.branch_id and b.Branch_ID=e.Branch_ID and e.Emp_Code=" + EmpCode.ToString() + " and h.Status_ID=1 and h.Branch_ID=b.Branch_ID) Holiday,(select h.Description from HOLIDAY_MASTER h, branch_master b, EMP_MASTER e where h.Tra_Dt=datetable.Date and h.branch_id=b.branch_id and b.Branch_ID=e.Branch_ID and e.Emp_Code=" + EmpCode.ToString() + " and h.Status_ID=1 and h.Branch_ID=b.Branch_ID) HolidayDes,case when DATEPART(dw,datetable.Date) = 7 AND DATEPART(dd, datetable.Date) BETWEEN 8 AND 14 then 1 else 0 end SecondSat,case when DATEPART(dw,datetable.Date) = 7 AND DATEPART(dd, datetable.Date) BETWEEN 22 AND 28 then 1 else 0 end FourthSat,'' leave,'' br from (select DATEADD(day,-(a.a + (10 * b.a) + (100 * c.a)),'" + ToDt.ToString("MM/dd/yyyy") + "') AS Date from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c) datetable where datetable.Date between GETDATE() and '" + ToDt.ToString("MM/dd/yyyy") + "' order by 1").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select a.tra_dt,DATEPART(dw,a.Tra_Dt),DATEPART(DD,a.Tra_Dt),case when a.M_Time = 'NM-REG' then 'NM-REG' when a.M_Time = 'Travel' then 'Travel' else isnull(FORMAT(CAST(a.M_Time AS DATETIME),'hh:mm tt'),'') end Moring,case when a.E_Time = 'NM-REG' then 'NM-REG' when a.e_Time = 'Travel' then 'Travel' else isnull(FORMAT(CAST(a.E_Time AS DATETIME),'hh:mm tt'),'') end Evening,(select isnull(COUNT(*),0) from HOLIDAY_MASTER h, branch_master b where h.Tra_Dt=a.Tra_Dt and h.Branch_ID=b.Branch_ID and b.Branch_ID=a.Branch_ID and h.Status_ID=1 and h.Branch_ID=b.Branch_ID),(select h.Description from HOLIDAY_MASTER h, branch_master b where h.Tra_Dt=a.Tra_Dt and h.branch_id=b.branch_id and b.Branch_ID=a.Branch_ID and h.Status_ID=1 and h.Branch_ID=b.Branch_ID) holidayDes,case when DATEPART(dw,a.Tra_Dt) = 7 AND DATEPART(dd, a.Tra_Dt) BETWEEN 8 AND 14 then 1 else 0 end SecondSat,case when DATEPART(dw,a.Tra_Dt) = 7 AND DATEPART(dd, a.Tra_Dt) BETWEEN 22 AND 28 then 1 else 0 end FourthSat,case when a.M_Time is null AND a.E_Time is null then isnull(isnull((select dbo.udfProperCase(y.Type_Name)+'Œ'+CONVERT(varchar,x.Status_ID) from EMP_LEAVE_REQUEST x, EMP_LEAVE_TYPE y where x.emp_code=a.Emp_Code and a.Tra_Dt between x.Leave_From and x.Leave_To and x.Leave_Type=y.Type_ID and x.Status_ID not in(5,6)),(select 'UnApprovedTravelŒ'+CONVERT(varchar,t.Status_ID) from EMP_TOUR_MASTER t where t.userid=a.Emp_Code and a.Tra_Dt between t.Tour_From and t.Tour_To and t.status_id=0)),'Unauthorised AbsenceŒ0') else '' end Leave,isnull(a.att_remarks,'') from ATTEND_ALL a where a.Emp_Code=" + EmpCode.ToString() + " and a.Tra_Dt between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "' order by tra_dt").Tables(0)
            End If

            If DT.Rows.Count > 0 Then
                Dim RowVal As Integer = 0
                Dim i As Integer = 0
                While i < DT.Rows.Count
                    Dim TR4 As New TableRow
                    Dim TR4_00, TR4_01, TR4_02, TR4_03, TR4_04, TR4_05, TR4_06 As New TableCell
                    Dim j As Integer = 1
                    If i = 0 Then 'First Time
                        j = DT.Rows(i)(1) 'Week 1/Sunday Check
                        If (DT.Rows(i)(1) = 1) Then '1st day
                            If (i < DT.Rows.Count) Then
                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then 'If Not Punch
                                    RH.InsertColumn(TR4, TR4_00, 15, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Weekly Off</font>") '1-Sunday 
                                Else 'If Punched
                                    RH.InsertColumn(TR4, TR4_00, 15, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                End If

                                If (DT.Rows(i)(5) > 0) Then
                                    TR4_00.ForeColor = Drawing.Color.Red
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_00, 15, 2, "")
                            End If
                        ElseIf (DT.Rows(i)(1) = 2) Then 'Monday
                            If (i < DT.Rows.Count) Then 'Total days Count Check
                                RH.InsertColumn(TR4, TR4_00, 15, 2, "") 'One Colum Blank

                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then 'If Not Punch
                                    If (DT.Rows(i)(5) > 0) Then 'If Monday Holiday
                                        RH.InsertColumn(TR4, TR4_01, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                        TR4_01.ForeColor = Drawing.Color.Red
                                    Else 'Not Holiday
                                        If DT.Rows(i)(9) = "" Then 'Leave Description Is Blank
                                            RH.InsertColumn(TR4, TR4_01, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then 'Approved Leave
                                                RH.InsertColumn(TR4, TR4_01, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else 'Not Approved Leave
                                                RH.InsertColumn(TR4, TR4_01, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else 'Puched Successfully
                                    RH.InsertColumn(TR4, TR4_01, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        TR4_01.ForeColor = Drawing.Color.Red
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_01, 14, 2, "")
                            End If
                        ElseIf (DT.Rows(i)(1) = 3) Then 'Tuesday
                            If (i < DT.Rows.Count) Then
                                RH.InsertColumn(TR4, TR4_00, 15, 2, "") 'Two Colum Blank
                                RH.InsertColumn(TR4, TR4_01, 14, 2, "")

                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then
                                    If (DT.Rows(i)(5) > 0) Then 'If Tuesday Holiday
                                        RH.InsertColumn(TR4, TR4_02, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                        TR4_02.ForeColor = Drawing.Color.Red
                                    Else
                                        If DT.Rows(i)(9) = "" Then
                                            RH.InsertColumn(TR4, TR4_02, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then
                                                RH.InsertColumn(TR4, TR4_02, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else
                                                RH.InsertColumn(TR4, TR4_02, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else
                                    RH.InsertColumn(TR4, TR4_02, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        TR4_02.ForeColor = Drawing.Color.Red
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_02, 14, 2, "")
                            End If
                        ElseIf (DT.Rows(i)(1) = 4) Then 'Wednesday
                            If (i < DT.Rows.Count) Then
                                RH.InsertColumn(TR4, TR4_00, 15, 2, "")
                                RH.InsertColumn(TR4, TR4_01, 14, 2, "")
                                RH.InsertColumn(TR4, TR4_02, 14, 2, "")

                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then
                                    If (DT.Rows(i)(5) > 0) Then
                                        RH.InsertColumn(TR4, TR4_03, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                        TR4_03.ForeColor = Drawing.Color.Red
                                    Else
                                        If DT.Rows(i)(9) = "" Then
                                            RH.InsertColumn(TR4, TR4_03, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then
                                                RH.InsertColumn(TR4, TR4_03, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else
                                                RH.InsertColumn(TR4, TR4_03, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else
                                    RH.InsertColumn(TR4, TR4_03, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        TR4_03.ForeColor = Drawing.Color.Red
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_03, 14, 2, "")
                            End If
                        ElseIf (DT.Rows(i)(1) = 5) Then 'Thurseday
                            If (i < DT.Rows.Count) Then
                                RH.InsertColumn(TR4, TR4_00, 15, 2, "")
                                RH.InsertColumn(TR4, TR4_01, 14, 2, "")
                                RH.InsertColumn(TR4, TR4_02, 14, 2, "")
                                RH.InsertColumn(TR4, TR4_03, 14, 2, "")

                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then
                                    If (DT.Rows(i)(5) > 0) Then
                                        RH.InsertColumn(TR4, TR4_04, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                        TR4_04.ForeColor = Drawing.Color.Red
                                    Else
                                        If DT.Rows(i)(9) = "" Then
                                            RH.InsertColumn(TR4, TR4_04, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then
                                                RH.InsertColumn(TR4, TR4_04, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else
                                                RH.InsertColumn(TR4, TR4_04, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else
                                    RH.InsertColumn(TR4, TR4_04, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        TR4_04.ForeColor = Drawing.Color.Red
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_04, 14, 2, "")
                            End If
                        ElseIf (DT.Rows(i)(1) = 6) Then 'Friday
                            If (i < DT.Rows.Count) Then
                                RH.InsertColumn(TR4, TR4_00, 15, 2, "")
                                RH.InsertColumn(TR4, TR4_01, 14, 2, "")
                                RH.InsertColumn(TR4, TR4_02, 14, 2, "")
                                RH.InsertColumn(TR4, TR4_03, 14, 2, "")
                                RH.InsertColumn(TR4, TR4_04, 14, 2, "")

                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then
                                    If (DT.Rows(i)(5) > 0) Then
                                        RH.InsertColumn(TR4, TR4_05, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                        TR4_05.ForeColor = Drawing.Color.Red
                                    Else
                                        If DT.Rows(i)(9) = "" Then
                                            RH.InsertColumn(TR4, TR4_05, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then
                                                RH.InsertColumn(TR4, TR4_05, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else
                                                RH.InsertColumn(TR4, TR4_05, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else
                                    RH.InsertColumn(TR4, TR4_05, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        TR4_05.ForeColor = Drawing.Color.Red
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_05, 14, 2, "")
                            End If
                        ElseIf (DT.Rows(i)(1) = 7) Then 'Saturday
                            If (i < DT.Rows.Count) Then
                                RH.InsertColumn(TR4, TR4_00, 15, 2, "")
                                RH.InsertColumn(TR4, TR4_01, 14, 2, "")
                                RH.InsertColumn(TR4, TR4_02, 14, 2, "")
                                RH.InsertColumn(TR4, TR4_03, 14, 2, "")
                                RH.InsertColumn(TR4, TR4_04, 14, 2, "")
                                RH.InsertColumn(TR4, TR4_05, 14, 2, "")

                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then 'Not Punched 
                                    If (DT.Rows(i)(5) > 0) Then 'Holiday
                                        If (DT.Rows(i)(7) > 0) Then 'If Saturday Is Holiday
                                            RH.InsertColumn(TR4, TR4_06, 15, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>2nd Saturday</font>") 'Second Saturday
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        ElseIf (DT.Rows(i)(8) > 0) Then
                                            RH.InsertColumn(TR4, TR4_06, 15, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>4th Saturday</font>") 'Fourth Saturday
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        Else
                                            RH.InsertColumn(TR4, TR4_06, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        End If
                                    Else
                                        If DT.Rows(i)(9) = "" Then 'Leave description is null
                                            RH.InsertColumn(TR4, TR4_06, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then 'Approved Leave
                                                RH.InsertColumn(TR4, TR4_06, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else 'Not Approved Leave
                                                RH.InsertColumn(TR4, TR4_06, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else
                                    RH.InsertColumn(TR4, TR4_06, 15, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        If (DT.Rows(i)(7) > 0) Then
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        ElseIf (DT.Rows(i)(8) > 0) Then
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        Else
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        End If
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_06, 15, 2, "")
                            End If
                        End If
                        i = i + 1
                        j = j + 1
                    End If
                    If i > 0 Then 'Not First Time
                        Dim k As Integer = j
                        If k = 1 Then 'Sunday
                            k = k + 1
                            If (i < DT.Rows.Count) Then

                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then 'If Not Punch
                                    RH.InsertColumn(TR4, TR4_00, 15, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Weekly Off</font>") '1-Sunday 
                                Else 'If Punched
                                    RH.InsertColumn(TR4, TR4_00, 15, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                End If

                                If (DT.Rows(i)(5) > 0) Then
                                    TR4_00.ForeColor = Drawing.Color.Red
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_00, 15, 2, "")
                            End If
                            i = i + 1
                        End If
                        If k = 2 Then 'Monday
                            k = k + 1
                            If (i < DT.Rows.Count) Then
                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then 'If Not Punch
                                    If (DT.Rows(i)(5) > 0) Then 'If Monday Holiday
                                        RH.InsertColumn(TR4, TR4_01, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                        TR4_01.ForeColor = Drawing.Color.Red
                                    Else 'Not Holiday
                                        If DT.Rows(i)(9) = "" Then 'Leave Description Is Blank
                                            RH.InsertColumn(TR4, TR4_01, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then 'Approved Leave
                                                RH.InsertColumn(TR4, TR4_01, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else 'Not Approved Leave
                                                RH.InsertColumn(TR4, TR4_01, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else 'Puched Successfully
                                    RH.InsertColumn(TR4, TR4_01, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        TR4_01.ForeColor = Drawing.Color.Red
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_01, 14, 2, "")
                            End If
                            i = i + 1
                        End If
                        If k = 3 Then 'Tuesday
                            k = k + 1
                            If (i < DT.Rows.Count) Then
                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then
                                    If (DT.Rows(i)(5) > 0) Then 'If Tuesday Holiday
                                        RH.InsertColumn(TR4, TR4_02, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                        TR4_02.ForeColor = Drawing.Color.Red
                                    Else
                                        If DT.Rows(i)(9) = "" Then
                                            RH.InsertColumn(TR4, TR4_02, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then
                                                RH.InsertColumn(TR4, TR4_02, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else
                                                RH.InsertColumn(TR4, TR4_02, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else
                                    RH.InsertColumn(TR4, TR4_02, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        TR4_02.ForeColor = Drawing.Color.Red
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_02, 14, 2, "")
                            End If
                            i = i + 1
                        End If
                        If k = 4 Then 'Wednesday
                            k = k + 1
                            If (i < DT.Rows.Count) Then
                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then
                                    If (DT.Rows(i)(5) > 0) Then
                                        RH.InsertColumn(TR4, TR4_03, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                        TR4_03.ForeColor = Drawing.Color.Red
                                    Else
                                        If DT.Rows(i)(9) = "" Then
                                            RH.InsertColumn(TR4, TR4_03, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then
                                                RH.InsertColumn(TR4, TR4_03, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else
                                                RH.InsertColumn(TR4, TR4_03, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else
                                    RH.InsertColumn(TR4, TR4_03, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        TR4_03.ForeColor = Drawing.Color.Red
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_03, 14, 2, "")
                            End If
                            i = i + 1
                        End If
                        If k = 5 Then 'Thurseday
                            k = k + 1
                            If (i < DT.Rows.Count) Then
                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then
                                    If (DT.Rows(i)(5) > 0) Then
                                        RH.InsertColumn(TR4, TR4_04, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                        TR4_04.ForeColor = Drawing.Color.Red
                                    Else
                                        If DT.Rows(i)(9) = "" Then
                                            RH.InsertColumn(TR4, TR4_04, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then
                                                RH.InsertColumn(TR4, TR4_04, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else
                                                RH.InsertColumn(TR4, TR4_04, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else
                                    RH.InsertColumn(TR4, TR4_04, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        TR4_04.ForeColor = Drawing.Color.Red
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_04, 14, 2, "")
                            End If
                            i = i + 1
                        End If
                        If k = 6 Then 'Friday
                            k = k + 1
                            If (i < DT.Rows.Count) Then
                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then
                                    If (DT.Rows(i)(5) > 0) Then
                                        RH.InsertColumn(TR4, TR4_05, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                        TR4_05.ForeColor = Drawing.Color.Red
                                    Else
                                        If DT.Rows(i)(9) = "" Then
                                            RH.InsertColumn(TR4, TR4_05, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then
                                                RH.InsertColumn(TR4, TR4_05, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else
                                                RH.InsertColumn(TR4, TR4_05, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else
                                    RH.InsertColumn(TR4, TR4_05, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        TR4_05.ForeColor = Drawing.Color.Red
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_05, 14, 2, "")
                            End If
                            i = i + 1
                        End If
                        If k = 7 Then 'Saturday
                            k = k + 1
                            If (i < DT.Rows.Count) Then
                                If (DT.Rows(i)(3) = "" And DT.Rows(i)(4) = "") Then 'Not Punched 
                                    If (DT.Rows(i)(5) > 0) Then 'Holiday
                                        If (DT.Rows(i)(7) > 0) Then 'If Saturday Is Holiday
                                            RH.InsertColumn(TR4, TR4_06, 15, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>2nd Saturday</font>") 'Second Saturday
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        ElseIf (DT.Rows(i)(8) > 0) Then
                                            RH.InsertColumn(TR4, TR4_06, 15, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>4th Saturday</font>") 'Fourth Saturday
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        Else
                                            RH.InsertColumn(TR4, TR4_06, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>" & DT.Rows(i)(6) & "</font>")
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        End If
                                    Else
                                        If DT.Rows(i)(9) = "" Then 'Leave description is null
                                            RH.InsertColumn(TR4, TR4_06, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & DT.Rows(i)(9) & "</font>")
                                        Else
                                            Dim Leave() As String = DT.Rows(i)(9).Split("Œ")
                                            If Leave(1) = 1 Then 'Approved Leave
                                                RH.InsertColumn(TR4, TR4_06, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2714;</span></font>")
                                            Else 'Not Approved Leave
                                                RH.InsertColumn(TR4, TR4_06, 14, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2; color='DeepSkyBlue';>" & Leave(0) & "&nbsp;<span style='font-family: wingdings; font-size: 150%;'>&#x2718;</span></font>")
                                            End If
                                        End If
                                    End If
                                Else
                                    RH.InsertColumn(TR4, TR4_06, 15, 2, "<font size=6; ><b>" & DT.Rows(i)(2) & "</b></font><br/><font size=2;>Check&nbsp;In&nbsp;:&nbsp;" & DT.Rows(i)(3) & "<br/>Check&nbsp;Out&nbsp;:&nbsp;" & DT.Rows(i)(4) & "</font>")
                                    If (DT.Rows(i)(5) > 0) Then
                                        If (DT.Rows(i)(7) > 0) Then
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        ElseIf (DT.Rows(i)(8) > 0) Then
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        Else
                                            TR4_06.ForeColor = Drawing.Color.Red
                                        End If
                                    End If
                                End If
                            Else
                                RH.InsertColumn(TR4, TR4_06, 15, 2, "")
                            End If
                            i = i + 1
                        End If
                    End If
                    tb.Controls.Add(TR4)
                    TR4_00.BorderWidth = "1"
                    TR4_01.BorderWidth = "1"
                    TR4_02.BorderWidth = "1"
                    TR4_03.BorderWidth = "1"
                    TR4_04.BorderWidth = "1"
                    TR4_05.BorderWidth = "1"
                    TR4_06.BorderWidth = "1"

                    TR4_00.BorderColor = Drawing.Color.Black
                    TR4_01.BorderColor = Drawing.Color.Black
                    TR4_02.BorderColor = Drawing.Color.Black
                    TR4_03.BorderColor = Drawing.Color.Black
                    TR4_04.BorderColor = Drawing.Color.Black
                    TR4_05.BorderColor = Drawing.Color.Black
                    TR4_06.BorderColor = Drawing.Color.Black

                    TR4_00.BorderStyle = BorderStyle.Dotted
                    TR4_01.BorderStyle = BorderStyle.Dotted
                    TR4_02.BorderStyle = BorderStyle.Dotted
                    TR4_03.BorderStyle = BorderStyle.Dotted
                    TR4_04.BorderStyle = BorderStyle.Dotted
                    TR4_05.BorderStyle = BorderStyle.Dotted
                    TR4_06.BorderStyle = BorderStyle.Dotted

                    TR4_00.BackColor = Drawing.Color.OldLace
                    TR4_01.BackColor = Drawing.Color.OldLace
                    TR4_02.BackColor = Drawing.Color.OldLace
                    TR4_03.BackColor = Drawing.Color.OldLace
                    TR4_04.BackColor = Drawing.Color.OldLace
                    TR4_05.BackColor = Drawing.Color.OldLace
                    TR4_06.BackColor = Drawing.Color.OldLace

                    TR4.Height = 30

                    RH.BlankRow(tb, 3)
                End While
                'RH.DrawLine(tb, 100)
                pnDisplay.Controls.Add(tb)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
