﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewPunchingReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim rptID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            rptID = CInt(GF.Decrypt(Request.QueryString.Get("rptID")))
            hdnReportID.Value = rptID.ToString()
            Dim FromDt As Date = CDate(Request.QueryString.Get("FromDt"))
            Dim ToDt As Date = CDate(Request.QueryString.Get("ToDt"))
            Dim EmpCode As Integer = CInt(GF.Decrypt(Request.QueryString.Get("EmpCode")))
            Dim EmpName As String = GF.Decrypt(Request.QueryString.Get("EmpName"))
            If rptID = 1 Or rptID = 11 Then
                Dim DT As New DataTable
                DT = DB.ExecuteDataSet("select Emp_Code,tra_dt,isnull(M_Time,''),isnull(E_Time,''),case when (isnull(M_Time,'') <>'NM-REG' and isnull(E_Time,'')='NM-REG') then 'Evening- Non Marking Regularized' " & _
                        " when (isnull(M_Time,'') ='NM-REG' and isnull(E_Time,'') <>'NM-REG') then 'Morning- Non Marking Regularized'  " & _
                        " when (isnull(M_Time,'') ='NM-REG' and isnull(E_Time,'') ='NM-REG') then 'Absent Regularized'  " & _
                        " else isnull(att_remarks,'') end  from ATTEND_ALL where Emp_Code=" + EmpCode.ToString() + " and Tra_Dt between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "' order by tra_dt").Tables(0)
                RH.Heading(Session("FirmName"), tb, "PUNCHING REPORT", 100)
                RH.SubHeading(tb, 100, "l", "Emp Code :" + EmpCode.ToString())
                RH.SubHeading(tb, 100, "l", "Name     :" + EmpName)
                RH.DrawLine(tb, 100)
                Dim RowBG As Integer = 0
                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")
                ' tb.Attributes.Add("border", "1")
                Dim TR0 As New TableRow
                TR0.BackColor = Drawing.Color.WhiteSmoke
                Dim TR0_00, TR0_01, TR0_02, TR0_03 As New TableCell
                RH.AddColumn(TR0, TR0_00, 25, 25, "l", "DATE")
                RH.AddColumn(TR0, TR0_01, 25, 25, "l", "MORNING")
                RH.AddColumn(TR0, TR0_02, 25, 25, "l", "EVENING")
                RH.AddColumn(TR0, TR0_03, 25, 25, "l", "REMARKS")
                tb.Controls.Add(TR0)
                RH.DrawLine(tb, 100)
                Dim I As Integer = 1
                For Each DR In DT.Rows
                    Dim TR1 As New TableRow
                    If RowBG = 0 Then
                        RowBG = 1
                        TR1.BackColor = Drawing.Color.WhiteSmoke
                    Else
                        RowBG = 0
                    End If
                    Dim TR1_00, TR1_01, TR1_02, TR1_03 As New TableCell
                    RH.AddColumn(TR1, TR1_00, 25, 25, "l", CDate(DR(1)).ToString("dd MMM yyyy"))
                    RH.AddColumn(TR1, TR1_01, 25, 25, "l", DR(2))
                    RH.AddColumn(TR1, TR1_02, 25, 25, "l", DR(3))
                    RH.AddColumn(TR1, TR1_03, 25, 25, "l", DR(4))
                    tb.Controls.Add(TR1)
                Next
                RH.DrawLine(tb, 100)
            ElseIf rptID = 2 Or rptID = 5 Then
                Dim DT As New DataTable
                Dim DT_Sum As New DataTable
                'Session("GraphType") = "RangeColumn"
                Dim TR22 As New TableRow
                Dim TR55 As New TableRow
                Dim TR66 As New TableRow
                Dim TR77 As New TableRow
                Dim TR88 As New TableRow
                Dim TR99 As New TableRow
                DT_Sum = DB.ExecuteDataSet("SELECT AA.EMP_CODE,UPPER(AA.EMP_NAME) as EMP_NAME,AA.CL,SUM(AA.CLcount) as CLcnt,AA.SL,SUM(AA.SLcount) as SLcnt " & _
                        " ,AA.PL,SUM(AA.PLcount) as PLcnt,AA.OTHER,SUM(AA.OTHERcount) as OTHERcnt FROM ( " & _
                        " select L.EMP_CODE,E.EMP_name,'CASUAL LEAVE' AS CL,case when T.Type_ID=1 then SUM(leave_days) ELSE 0 END as CLcount " & _
                        " ,'SICK LEAVE'  AS SL,case when T.Type_ID=2 then SUM(leave_days) ELSE 0 END as SLcount, " & _
                        " 'PRIVILEGE LEAVE'  AS PL,case when T.Type_ID=3 then SUM(leave_days) ELSE 0 END as PLcount, " & _
                        " 'OTHERS'  AS OTHER,case when T.Type_ID>3 then SUM(leave_days) ELSE 0 END as OTHERcount " & _
                        " from EMP_LEAVE_REQUEST L INNER JOIN EMP_LEAVE_REASONS R ON L.Reason_ID=R.Reason_ID  " & _
                        " INNER JOIN Emp_Leave_Type T ON L.Leave_Type=T.Type_ID INNER JOIN EMP_MASTER E ON L.Emp_Code=E.Emp_Code  WHERE L.emp_code=" & EmpCode.ToString() & " and DATEADD(day, DATEDIFF(day, 0, Leave_From), 0) Between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "'  AND L.Status_ID<5 " & _
                        " GROUP BY L.EMP_CODE,E.EMP_name,Type_ID,TYPE_NAME) AA GROUP BY AA.EMP_CODE,AA.EMP_NAME,AA.CL,AA.SL,AA.PL,AA.OTHER").Tables(0)




                DT = DB.ExecuteDataSet("select  CONVERT(VARCHAR(11),L.request_date,106) as reqdate,T.Type_Name, " & _
                    " CONVERT(VARCHAR(11),L.Leave_From,106) as leavefrom,CONVERT(VARCHAR(11),L.Leave_To,106) as leaveto,L.Leave_Days,R.Reason, " & _
                    " case when L.Status_ID=0 then 'Requested' WHEN L.Status_ID=1 then 'Approved' WHEN L.Status_ID=2 then 'Recommended' WHEN L.Status_ID=5 then 'Rejected' WHEN L.Status_ID=6 then 'Cancelled' ELSE '' END AS status from EMP_LEAVE_REQUEST L INNER JOIN EMP_LEAVE_REASONS R ON L.Reason_ID=R.Reason_ID  " & _
                    " INNER JOIN Emp_Leave_Type T ON L.Leave_Type=T.Type_ID INNER JOIN EMP_MASTER E ON L.Emp_Code=E.Emp_Code WHERE L.emp_code=" & EmpCode.ToString() & " and DATEADD(day, DATEDIFF(day, 0, Leave_From), 0) Between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "'  AND L.Status_ID<5 order by L.request_date").Tables(0)
                RH.Heading(CStr(Session("FirmName")), tb, "LEAVE REPORT", 100)
                Dim DR As DataRow
                Dim DR_sum As DataRow
                If DT_Sum.Rows.Count > 0 Then
                    DR_sum = DT_Sum.Rows(0)
                    'TR55.Font.Bold = True
                    'TR66.Font.Bold = True
                    'TR77.Font.Bold = True
                    'TR88.Font.Bold = True
                    Dim TR22_00, TR22_01, TR22_02, TR22_03 As New TableCell

                    RH.AddColumn(TR22, TR22_00, 47, 47, "l", "&nbsp;&nbsp;<u>REPORT INFORMATION</u>")
                    RH.AddColumn(TR22, TR22_01, 3, 3, "l", "")
                    RH.AddColumn(TR22, TR22_02, 3, 3, "l", "")
                    RH.AddColumn(TR22, TR22_03, 47, 47, "l", "&nbsp;&nbsp;<u>AVAILED LEAVE DETAILS</u>")
                    tb.Controls.Add(TR22)
                    TR22_00.BackColor = Drawing.Color.LightSteelBlue
                    TR22_03.BackColor = Drawing.Color.LightSteelBlue
                    TR22.Font.Bold = True
                    TR22_00.Font.Size = 20
                    TR22_03.Font.Size = 20
                    Dim TR55_00, TR55_01, TR55_02, TR55_03, TR55_04, TR55_05, TR55_06, TR55_07 As New TableCell
                    RH.AddColumn(TR55, TR55_00, 23, 23, "l", "&nbsp;&nbsp;Emp Code")
                    RH.AddColumn(TR55, TR55_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR55, TR55_02, 23, 23, "l", EmpCode.ToString())
                    RH.AddColumn(TR55, TR55_03, 3, 3, "l", "")
                    RH.AddColumn(TR55, TR55_04, 3, 3, "l", "")
                    RH.AddColumn(TR55, TR55_05, 23, 23, "l", "&nbsp;&nbsp;Casual Leave")
                    RH.AddColumn(TR55, TR55_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR55, TR55_07, 23, 23, "l", DR_sum(3).ToString())
                    tb.Controls.Add(TR55)
                    TR55_00.BackColor = Drawing.Color.WhiteSmoke
                    TR55_01.BackColor = Drawing.Color.WhiteSmoke
                    TR55_02.BackColor = Drawing.Color.WhiteSmoke
                    TR55_05.BackColor = Drawing.Color.WhiteSmoke
                    TR55_06.BackColor = Drawing.Color.WhiteSmoke
                    TR55_07.BackColor = Drawing.Color.WhiteSmoke
                    RH.BlankRow(tb, 1)
                    Dim TR66_00, TR66_01, TR66_02, TR66_03, TR66_04, TR66_05, TR66_06, TR66_07 As New TableCell
                    RH.AddColumn(TR66, TR66_00, 23, 23, "l", "&nbsp;&nbsp;Name")
                    RH.AddColumn(TR66, TR66_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR66, TR66_02, 23, 23, "l", EmpName)
                    RH.AddColumn(TR66, TR66_03, 3, 3, "l", "")
                    RH.AddColumn(TR66, TR66_04, 3, 3, "l", "")
                    RH.AddColumn(TR66, TR66_05, 23, 23, "l", "&nbsp;&nbsp;Sick Leave")
                    RH.AddColumn(TR66, TR66_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR66, TR66_07, 23, 23, "l", DR_sum(5).ToString())
                    TR66_00.BackColor = Drawing.Color.FloralWhite
                    TR66_01.BackColor = Drawing.Color.FloralWhite
                    TR66_02.BackColor = Drawing.Color.FloralWhite
                    TR66_05.BackColor = Drawing.Color.FloralWhite
                    TR66_06.BackColor = Drawing.Color.FloralWhite
                    TR66_07.BackColor = Drawing.Color.FloralWhite
                    tb.Controls.Add(TR66)

                    Dim TR77_00, TR77_01, TR77_02, TR77_03, TR77_04, TR77_05, TR77_06, TR77_07 As New TableCell
                    RH.AddColumn(TR77, TR77_00, 23, 23, "l", "&nbsp;&nbsp;From Date")
                    RH.AddColumn(TR77, TR77_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR77, TR77_02, 23, 23, "l", FromDt.ToString("dd MMM yyyy"))
                    RH.AddColumn(TR77, TR77_03, 3, 3, "l", "")
                    RH.AddColumn(TR77, TR77_04, 3, 3, "l", "")
                    RH.AddColumn(TR77, TR77_05, 23, 23, "l", "&nbsp;&nbsp;Privilege Leave")
                    RH.AddColumn(TR77, TR77_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR77, TR77_07, 23, 23, "l", DR_sum(7).ToString())
                    TR77_00.BackColor = Drawing.Color.WhiteSmoke
                    TR77_01.BackColor = Drawing.Color.WhiteSmoke
                    TR77_02.BackColor = Drawing.Color.WhiteSmoke
                    TR77_05.BackColor = Drawing.Color.WhiteSmoke
                    TR77_06.BackColor = Drawing.Color.WhiteSmoke
                    TR77_07.BackColor = Drawing.Color.WhiteSmoke
                    tb.Controls.Add(TR77)

                    Dim TR88_00, TR88_01, TR88_02, TR88_03, TR88_04, TR88_05, TR88_06, TR88_07 As New TableCell
                    RH.AddColumn(TR88, TR88_00, 23, 23, "l", "&nbsp;&nbsp;To Date")
                    RH.AddColumn(TR88, TR88_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR88, TR88_02, 23, 23, "l", ToDt.ToString("dd MMM yyyy"))
                    RH.AddColumn(TR88, TR88_03, 3, 3, "l", "")
                    RH.AddColumn(TR88, TR88_04, 3, 3, "l", "")
                    RH.AddColumn(TR88, TR88_05, 23, 23, "l", "&nbsp;&nbsp;Others")
                    RH.AddColumn(TR88, TR88_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR88, TR88_07, 23, 23, "l", DR_sum(9).ToString())
                    TR88_00.BackColor = Drawing.Color.FloralWhite
                    TR88_01.BackColor = Drawing.Color.FloralWhite
                    TR88_02.BackColor = Drawing.Color.FloralWhite
                    TR88_05.BackColor = Drawing.Color.FloralWhite
                    TR88_06.BackColor = Drawing.Color.FloralWhite
                    TR88_07.BackColor = Drawing.Color.FloralWhite
                    tb.Controls.Add(TR88)

                    RH.DrawLine(tb, 100)

                    Dim RowBG As Integer = 0
                    Dim TR0 As New TableRow
                    TR0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TR0_00, TR0_01, TR0_02, TR0_03, TR0_04, TR0_05, TR0_06 As New TableCell
                    RH.AddColumn(TR0, TR0_00, 15, 15, "l", "Request Date")
                    RH.AddColumn(TR0, TR0_01, 24, 24, "l", "Leave Type")
                    RH.AddColumn(TR0, TR0_02, 12, 12, "l", "From")
                    RH.AddColumn(TR0, TR0_03, 12, 12, "l", "To")
                    RH.AddColumn(TR0, TR0_04, 7, 7, "c", "Days")
                    RH.AddColumn(TR0, TR0_05, 15, 15, "l", "Status")
                    RH.AddColumn(TR0, TR0_06, 15, 15, "l", "Reason")
                    tb.Controls.Add(TR0)
                    TR0.Font.Bold = True
                    RH.DrawLine(tb, 100)
                    Dim TOT As Double = 0
                    Dim I As Integer = 1
                    For Each DR In DT.Rows
                        Dim TR1 As New TableRow
                        If RowBG = 0 Then
                            RowBG = 1
                            TR1.BackColor = Drawing.Color.WhiteSmoke
                        Else
                            RowBG = 0
                        End If
                        Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06 As New TableCell

                        RH.AddColumn(TR1, TR1_00, 15, 15, "l", DR(0))
                        RH.AddColumn(TR1, TR1_01, 24, 24, "l", DR(1))
                        RH.AddColumn(TR1, TR1_02, 12, 12, "l", DR(2))
                        RH.AddColumn(TR1, TR1_03, 12, 12, "l", DR(3))
                        RH.AddColumn(TR1, TR1_04, 7, 7, "c", DR(4))
                        RH.AddColumn(TR1, TR1_05, 15, 15, "l", DR(6))
                        RH.AddColumn(TR1, TR1_06, 15, 15, "l", DR(5))
                        tb.Controls.Add(TR1)
                        TOT += CDbl(DR(4))
                    Next
                    RH.DrawLine(tb, 100)
                    Dim TR99_00, TR99_01, TR99_02, TR99_03, TR99_04, TR99_05, TR99_06 As New TableCell
                    RH.AddColumn(TR99, TR99_00, 15, 15, "l", "<b>TOTAL</b>")
                    RH.AddColumn(TR99, TR99_01, 24, 24, "l", "")
                    RH.AddColumn(TR99, TR99_02, 12, 12, "l", "")
                    RH.AddColumn(TR99, TR99_03, 12, 12, "l", "")
                    RH.AddColumn(TR99, TR99_04, 7, 7, "c", "<b>" + TOT.ToString() + "</b>")
                    RH.AddColumn(TR99, TR99_05, 15, 15, "l", "")
                    RH.AddColumn(TR99, TR99_06, 15, 15, "l", "")
                    tb.Controls.Add(TR99)
                    RH.DrawLine(tb, 100)
                End If
            ElseIf rptID = 3 Then 'TOUR REPORT
                Dim DT As New DataTable

                DT = DB.ExecuteDataSet("select D.location,D.tour_start_date,D.tour_end_date,D.purpose,case when T.status_id=0 then 'Requested'  " & _
                    " when T.status_id=1 then 'Approved' when T.status_id=2 then 'Rejected' ELSE 'Cancelled' END AS tourstatus " & _
                    " from [EMP_TOUR_MASTER] T,[EMP_TOUR] D,EMP_MASTER E where  T.M_request_id=D.M_request_id and T.userid=E.emp_code and userid=" + EmpCode.ToString() + " " & _
                    " and ((tour_from between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "') or (tour_to between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "')) order by tour_from").Tables(0)
                RH.Heading(Session("FirmName"), tb, "TRAVEL REPORT", 100)
                RH.SubHeading(tb, 100, "l", "Emp Code :" + EmpCode.ToString())
                RH.SubHeading(tb, 100, "l", "Name     :" + EmpName)
                RH.DrawLine(tb, 100)
                Dim RowBG As Integer = 0
                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")
                ' tb.Attributes.Add("border", "1")
                Dim TR0 As New TableRow
                TR0.BackColor = Drawing.Color.WhiteSmoke
                Dim TR0_00, TR0_01, TR0_02, TR0_03, TR0_04 As New TableCell
                RH.AddColumn(TR0, TR0_00, 20, 20, "l", "From")
                RH.AddColumn(TR0, TR0_01, 20, 20, "l", "To")
                RH.AddColumn(TR0, TR0_02, 20, 20, "l", "Location")
                RH.AddColumn(TR0, TR0_03, 20, 20, "l", "Purpose")
                RH.AddColumn(TR0, TR0_04, 20, 20, "l", "Status")
                tb.Controls.Add(TR0)
                RH.DrawLine(tb, 100)
                Dim I As Integer = 1
                For Each DR In DT.Rows
                    Dim TR1 As New TableRow
                    If RowBG = 0 Then
                        RowBG = 1
                        TR1.BackColor = Drawing.Color.WhiteSmoke
                    Else
                        RowBG = 0
                    End If
                    Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04 As New TableCell
                    RH.AddColumn(TR1, TR1_00, 20, 20, "l", CDate(DR(1)).ToString("dd MMM yyyy"))
                    RH.AddColumn(TR1, TR1_01, 20, 20, "l", CDate(DR(2)).ToString("dd MMM yyyy"))
                    RH.AddColumn(TR1, TR1_02, 20, 20, "l", DR(0))
                    RH.AddColumn(TR1, TR1_03, 20, 20, "l", DR(3))
                    RH.AddColumn(TR1, TR1_04, 20, 20, "l", DR(4))
                    tb.Controls.Add(TR1)
                Next
                RH.DrawLine(tb, 100)
            ElseIf rptID = 4 Then
                Dim DT As New DataTable
                Dim BranchName As String = ""
                DT = DB.ExecuteDataSet("select b.emp_code,c.Emp_Name,a.verify_date,case when b.status_id=1 then 'Absent but Informed' when  b.status_id=2 then 'Absent but Not Informed'  " & _
                    " when  b.status_id=3 then 'Present' else '' END AS status  from EMP_LEAVE_VERIFY_MASTER  a,EMP_LEAVE_VERIFY b,EMP_MASTER c where " & _
                    " a.verify_id=b.verify_id and b.Emp_code = c.Emp_Code  and a.emp_code=" + EmpCode.ToString() + " and " & _
                    " dateadd(ss,1,dateadd(dd,-1,dateadd(ss,-1,dateadd(dd,1,convert(datetime,convert(varchar(10),a.verify_date,101)))))) between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "' order by verify_date").Tables(0)
                RH.Heading(Session("FirmName"), tb, "LEAVE VERIFICATION REPORT", 100)
                RH.SubHeading(tb, 100, "l", "Emp Code :" + EmpCode.ToString())
                RH.SubHeading(tb, 100, "l", "Name     :" + EmpName)
                Dim DTsub As New DataTable
                DTsub = DB.ExecuteDataSet("select (case when Branch_ID<100 then Department_Name else Branch_Name end)as Branch from Emp_List where Emp_Code=" + EmpCode.ToString()).Tables(0)
                If DTsub.Rows.Count > 0 Then

                    BranchName = DTsub.Rows(0)(0)
                End If
                RH.SubHeading(tb, 100, "l", "Location/Department:" + BranchName)
                RH.DrawLine(tb, 100)
                Dim RowBG As Integer = 0
                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")
                ' tb.Attributes.Add("border", "1")
                Dim TR0 As New TableRow
                TR0.BackColor = Drawing.Color.WhiteSmoke
                Dim TR0_00, TR0_01, TR0_02, TR0_03 As New TableCell
                RH.AddColumn(TR0, TR0_00, 25, 25, "l", "Employee Code")
                RH.AddColumn(TR0, TR0_01, 25, 25, "l", "Name")
                RH.AddColumn(TR0, TR0_02, 25, 25, "l", "Date")
                RH.AddColumn(TR0, TR0_03, 25, 25, "l", "Status")
                tb.Controls.Add(TR0)
                RH.DrawLine(tb, 100)
                Dim I As Integer = 1
                For Each DR In DT.Rows
                    Dim TR1 As New TableRow
                    If RowBG = 0 Then
                        RowBG = 1
                        TR1.BackColor = Drawing.Color.WhiteSmoke
                    Else
                        RowBG = 0
                    End If
                    Dim TR1_00, TR1_01, TR1_02, TR1_03 As New TableCell
                    RH.AddColumn(TR1, TR1_00, 25, 25, "l", DR(0))
                    RH.AddColumn(TR1, TR1_01, 25, 25, "l", DR(1))
                    RH.AddColumn(TR1, TR1_02, 25, 25, "l", CDate(DR(2)).ToString("dd MMM yyyy"))
                    RH.AddColumn(TR1, TR1_03, 25, 25, "l", DR(3))
                    tb.Controls.Add(TR1)
                Next
                RH.DrawLine(tb, 100)
            ElseIf rptID = 6 Or rptID = 7 Then
                Dim DT As New DataTable
                Dim BranchName As String = ""
                DT = DB.ExecuteDataSet("select a.emp_code,b.Emp_Name,a.NM_Date ,case when a.requesttype=1 then 'Non Marking' when a.requesttype=4 then 'Absent'  else '' end as Category,case when a.status_id=0 then 'Requested' when  a.status_id=1 then 'Approved' when  a.status_id=2 then 'Rejected' else '' END AS status  from EMP_REGULARIZE   a,EMP_MASTER b where a.Emp_code = b.Emp_Code  and a.emp_code=" + EmpCode.ToString() + " and dateadd(ss,1,dateadd(dd,-1,dateadd(ss,-1,dateadd(dd,1,convert(datetime,convert(varchar(10),a.NM_Date ,101)))))) between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "' union all select a.Emp_Code,b.Emp_Name,a.Early_Date,'Early Going' as Category, case when a.status_id=0 then 'Requested' when  a.status_id=1 then 'Approved' when  a.status_id=2 then 'Rejected' else '' END AS status  from EMP_EARLY_GOING a,EMP_MASTER  b where a.Emp_Code =b.Emp_Code and a.Emp_Code=" + EmpCode.ToString() + "  and dateadd(ss,1,dateadd(dd,-1,dateadd(ss,-1,dateadd(dd,1,convert(datetime,convert(varchar(10),a.Early_Date  ,101)))))) between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "' order by 3 ").Tables(0)
                RH.Heading(Session("FirmName"), tb, "REGULARIZATION REPORT FROM " + FromDt.ToString("dd MMM yyyy") + " " + " TO " + ToDt.ToString("dd MMM yyyy"), 100)
                RH.SubHeading(tb, 100, "l", "Emp Code :" + EmpCode.ToString())
                RH.SubHeading(tb, 100, "l", "Name     :" + EmpName)
                Dim DTsub As New DataTable
                DTsub = DB.ExecuteDataSet("select (case when Branch_ID<100 then Department_Name else Branch_Name end)as Branch from Emp_List where Emp_Code=" + EmpCode.ToString()).Tables(0)
                If DTsub.Rows.Count > 0 Then

                    BranchName = DTsub.Rows(0)(0)
                End If
                RH.SubHeading(tb, 100, "l", "Location/Department:" + BranchName)
                RH.DrawLine(tb, 100)
                Dim RowBG As Integer = 0
                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")
                ' tb.Attributes.Add("border", "1")
                Dim TR0 As New TableRow
                TR0.BackColor = Drawing.Color.WhiteSmoke
                Dim TR0_00, TR0_01, TR0_02, TR0_03, TR0_04 As New TableCell
                RH.AddColumn(TR0, TR0_00, 5, 5, "l", "Emp Code")
                RH.AddColumn(TR0, TR0_01, 25, 25, "l", "Name")
                RH.AddColumn(TR0, TR0_02, 25, 25, "l", "Category")
                RH.AddColumn(TR0, TR0_03, 20, 20, "l", "Date")
                RH.AddColumn(TR0, TR0_04, 25, 25, "l", "Status")
                tb.Controls.Add(TR0)
                RH.DrawLine(tb, 100)
                Dim I As Integer = 1
                For Each DR In DT.Rows
                    Dim TR1 As New TableRow
                    If RowBG = 0 Then
                        RowBG = 1
                        TR1.BackColor = Drawing.Color.WhiteSmoke
                    Else
                        RowBG = 0
                    End If
                    Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04 As New TableCell
                    RH.AddColumn(TR1, TR1_00, 5, 5, "l", DR(0))
                    RH.AddColumn(TR1, TR1_01, 25, 25, "l", DR(1))
                    RH.AddColumn(TR1, TR1_02, 25, 25, "l", DR(3))
                    RH.AddColumn(TR1, TR1_03, 20, 20, "l", CDate(DR(2)).ToString("dd MMM yyyy"))
                    RH.AddColumn(TR1, TR1_04, 25, 25, "l", DR(4))
                    tb.Controls.Add(TR1)
                Next
                RH.DrawLine(tb, 100)
            End If
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
