﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewConsolidatedBtwnDateReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim rptID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            rptID = CInt(GF.Decrypt(Request.QueryString.Get("rptID")))
            hdnReportID.Value = rptID.ToString()
            Dim FromDt As Date = CDate(Request.QueryString.Get("FromDt"))
            Dim ToDt As Date = CDate(Request.QueryString.Get("ToDt"))
            Dim EmpCode As Integer = CInt(GF.Decrypt(Request.QueryString.Get("UserID")))
            Dim BranchName As String = ""
            If rptID = 1 Then '------------------CONSOLIDATED TOUR REPORT
                Dim DT As New DataTable
                Dim DT_Dep As New DataTable
                DT = DB.ExecuteDataSet("select  T.userid,E.Emp_Name,D.location,D.tour_start_date,D.tour_end_date,D.purpose,case when T.status_id=0 then 'Requested'  " & _
                    " when T.status_id=1 then 'Approved' when T.status_id=2 then 'Rejected' ELSE 'Cancelled' END AS tourstatus,ROW_NUMBER() OVER (ORDER BY D.request_id) AS Row " & _
                    " from [EMP_TOUR_MASTER] T,[EMP_TOUR] D,EMP_MASTER E where  T.M_request_id=D.M_request_id and T.userid=E.emp_code and  T.userid in(Select Emp_Code from EMP_MASTER where Reporting_To=" + EmpCode.ToString() + ")  " & _
                    " and (tour_from between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "') and (tour_to between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "') order by tour_from").Tables(0)

                RH.Heading(Session("FirmName"), tb, "CONSOLIDATED TRAVEL REPORT", 100)
                DT_Dep = DB.ExecuteDataSet("select case when Branch_ID<100 then Department_Name else Branch_Name end as department from emp_list where Emp_Code=" + EmpCode.ToString() + "").Tables(0)
                Dim strDepartment As String
                If DT_Dep.Rows.Count > 0 Then
                    strDepartment = DT_Dep.Rows(0).Item(0)
                Else
                    strDepartment = ""
                End If
                RH.SubHeading(tb, 100, "l", "Date :" + CDate(FromDt).ToString("dd-MMM-yyyy"))
                RH.SubHeading(tb, 100, "l", "Location/Department     :" + strDepartment)
                RH.DrawLine(tb, 100)
                Dim RowBG As Integer = 0
                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")
                ' tb.Attributes.Add("border", "1")
                Dim TR0 As New TableRow
                TR0.BackColor = Drawing.Color.WhiteSmoke
                Dim TR0_00, TR0_01, TR0_02, TR0_03, TR0_04, TR0_05, TR0_06, TR0_07 As New TableCell
                RH.AddColumn(TR0, TR0_00, 10, 10, "c", "Sl No")
                RH.AddColumn(TR0, TR0_01, 10, 10, "l", "Emp Code")
                RH.AddColumn(TR0, TR0_02, 20, 20, "l", "Name")
                RH.AddColumn(TR0, TR0_03, 5, 5, "l", "From")
                RH.AddColumn(TR0, TR0_04, 5, 5, "l", "To")
                RH.AddColumn(TR0, TR0_05, 20, 20, "l", "Location")
                RH.AddColumn(TR0, TR0_06, 20, 20, "l", "Purpose")
                RH.AddColumn(TR0, TR0_07, 10, 10, "l", "Status")
                tb.Controls.Add(TR0)
                RH.DrawLine(tb, 100)
                Dim I As Integer = 1
                For Each DR In DT.Rows
                    Dim TR1 As New TableRow
                    If RowBG = 0 Then
                        RowBG = 1
                        TR1.BackColor = Drawing.Color.WhiteSmoke
                    Else
                        RowBG = 0
                    End If
                    Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07 As New TableCell
                    RH.AddColumn(TR1, TR1_00, 10, 10, "c", DR(7))
                    RH.AddColumn(TR1, TR1_01, 10, 10, "l", DR(0))
                    RH.AddColumn(TR1, TR1_02, 20, 20, "l", DR(1))
                    RH.AddColumn(TR1, TR1_03, 5, 5, "l", DR(3))
                    RH.AddColumn(TR1, TR1_04, 5, 5, "l", DR(4))
                    RH.AddColumn(TR1, TR1_05, 20, 20, "l", DR(2))
                    RH.AddColumn(TR1, TR1_06, 20, 20, "l", DR(5))
                    RH.AddColumn(TR1, TR1_07, 10, 10, "l", DR(6))
                    tb.Controls.Add(TR1)
                Next
                RH.DrawLine(tb, 100)

            End If
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
