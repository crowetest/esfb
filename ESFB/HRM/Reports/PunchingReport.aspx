﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="PunchingReport.aspx.vb" Inherits="Reports_PunchingReport" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Calculations.js" type="text/javascript"></script>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function DateOnChange() {
            var frmDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDt.ClientID %>").value;
            if (frmDt != "" && ToDt != "") {
                var days = getDateDiff(frmDt, ToDt, "days");
                if (days < 0) {
                    document.getElementById("<%= txtToDt.ClientID %>").value = "";
                    alert("Check Dates");
                    document.getElementById("<%= txtToDt.ClientID %>").focus();
                    return false;
                }
            }

        }
        function GenerateOnClick() {
            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if (EmpCode == "")
            { alert("Enter Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false; }
            var frmDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDt.ClientID %>").value;
            if (frmDt == "")
            { alert("Select From Date"); document.getElementById("<%= txtFromDt.ClientID %>").focus(); return false; }
            if (ToDt == "")
            { alert("Select To Date"); document.getElementById("<%= txtToDt.ClientID %>").focus(); return false; }
            document.getElementById("<%= hdnValue.ClientID %>").value = EmpCode + "Ø" + frmDt + "Ø" + ToDt + "Ø" + document.getElementById("<%= txtName.ClientID %>").value
        }

        function FromServer(Arg, Context) {
            switch (Context) {

                case 1:
                    {
                        var Data = Arg.split("Ø");
                        if (Data[1] == "-1") {
                            alert("Invalid Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").value = ""; document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                        }
                        else {
                            document.getElementById("<%= txtName.ClientID %>").value = Data[1];

                        }
                        break;
                    }
            }
        }
                function EmpCodeOnChange()
                {
                    var Emp_Code= document.getElementById("<%= txtEmpCode.ClientID %>").value;
                    if(Emp_Code!="")
                    { ToServer("1Ø" + Emp_Code, 1);}
                }
    </script>
    <table style="width:80%;margin: 0px auto;" >
        <tr>
            <td style="width:25%;">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            </td>
            <td style="width:12%; text-align:left;">
                &nbsp;</td>
            <td style="width:63%">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:25%;">
                &nbsp;</td>
            <td style="width:12%; text-align:left;">
                Emp Code</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpCode" 
                    class="ReadOnlyTextBox" runat="server" 
                    Width="40%" ReadOnly="True" MaxLength="5" onkeypress="return NumericCheck()"  ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Name</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtName" 
                    class="ReadOnlyTextBox" runat="server" 
                    Width="40%" ReadOnly="True"  ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Date From</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtFromDt" class="NormalText" runat="server" 
                    Width="40%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txtFromDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtFromDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>
        </tr>
        <tr >
            <td style="width:25%;">
                &nbsp;</td>
            <td style="width:12%; text-align:left;">
                Date To</td>
            <td style="width:63%">
                 &nbsp; &nbsp;<asp:TextBox ID="txtToDt" class="NormalText" runat="server" 
                    Width="40%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txttODt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txttODt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="text-align:center;"  colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;"  colspan="3">
                &nbsp; 
                <asp:Button ID="btnGenerate" runat="server" Text="GENERATE" Width="10%" style="font-family: cambria; cursor:pointer"  />
                &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" 
                type="button" value="EXIT" onclick="window.open('../../Home.aspx','_self')"  /></td>
        </tr>
        <tr >
            <td style="text-align:center; " colspan="3">
                <br />
                &nbsp;&nbsp;
                <asp:HiddenField ID="hdnDate" runat="server" />
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
