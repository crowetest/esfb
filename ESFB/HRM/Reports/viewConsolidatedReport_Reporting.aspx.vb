﻿
Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf

Partial Class Reports_viewConsolidatedReport_Reporting
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim rptID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim DT As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            hdn_PostID.Value = CInt(Request.QueryString.Get("PostID"))
            Dim TraDt As DateTime = CDate(Request.QueryString.Get("TraDt"))
            cmd_Back.Attributes.Add("onclick", "return Backform()")

            hdnReportID.Value = rptID.ToString()
            Dim BranchName As String = ""
            Dim RowBG As Integer = 0
            Dim I As Integer = 1

            Dim StrSubHD As String = ""
            RH.DrawLine(tb, 100)
            Dim SqlStr As String = "select b.Branch_Name as [Location], a.Emp_Code as [Emp Code],c.Emp_Name as [Emp Name], tra_dt as [Date],isnull(M_Time,'') as [Morning],isnull(E_Time,'') as [Evening],isnull(att_remarks,'') as [Remarks] FROM ATTEND_ALL a,brmaster b,EMP_MASTER c  where a.Branch_ID=b.Branch_ID and a.Emp_Code=c.Emp_Code and c.Emp_Category=1  and c.Emp_Status_ID<>6 and Tra_Dt ='" + TraDt.ToString("MM/dd/yyyy") + "'"
            SqlStr += " and c.Reporting_to=" + Session("UserID").ToString()
            Dim DepartmentName As String = ""

            StrSubHD = " For Employees Reporting to " + Session("UserName")

            SqlStr += "  order by   b.Branch_Name, a.Emp_Code,c.Emp_Name,tra_dt"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            RH.Heading(Session("FirmName"), tb, "Punching Report of  " + TraDt.ToString("dd/MMM/yyyy") + " " + StrSubHD, 100)



            'Dim TR02 As New TableRow

            'Dim TR02_00, TR02_01, TR02_02, TR02_03 As New TableCell
            'RH.AddColumn(TR02, TR02_00, 10, 10, "l", "Date         :")
            'RH.AddColumn(TR02, TR02_01, 50, 50, "l", CDate(Session("TraDt")).ToString("dd MMM yyyy"))
            'RH.AddColumn(TR02, TR02_02, 20, 20, "l", "")
            'RH.AddColumn(TR02, TR02_03, 20, 20, "l", "")

            'tb.Controls.Add(TR02)
            Dim DR1 As DataRow
            tb.Attributes.Add("width", "100%")
            ' tb.Attributes.Add("border", "1")
            '  RH.DrawLine(tb, 100)
            Dim TR1 As New TableRow
            TR1.Style.Add("Font-weight", "bold")
            TR1.BackColor = Drawing.Color.WhiteSmoke
            Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07 As New TableCell
            RH.AddColumn(TR1, TR1_00, 5, 5, "c", "SI No")
            RH.AddColumn(TR1, TR1_01, 15, 15, "l", "Location")
            RH.AddColumn(TR1, TR1_02, 10, 10, "l", "Emp Code")
            RH.AddColumn(TR1, TR1_03, 20, 20, "l", "Name")
            RH.AddColumn(TR1, TR1_04, 15, 15, "l", "Date")
            RH.AddColumn(TR1, TR1_05, 15, 15, "l", "Morning")
            RH.AddColumn(TR1, TR1_06, 10, 10, "l", "Evening")
            RH.AddColumn(TR1, TR1_07, 10, 10, "l", "Remarks")
            tb.Controls.Add(TR1)
            RH.DrawLine(tb, 100)

            For Each DR1 In DT.Rows
                Dim TR2 As New TableRow
                If RowBG = 0 Then
                    RowBG = 1
                    TR2.BackColor = Drawing.Color.WhiteSmoke
                Else
                    RowBG = 0
                End If
                Dim TR2_00, TR2_01, TR2_02, TR2_03, TR2_04, TR2_05, TR2_06, TR2_07 As New TableCell
                RH.AddColumn(TR2, TR2_00, 5, 5, "c", I.ToString())
                RH.AddColumn(TR2, TR2_01, 15, 15, "l", DR1(0))
                RH.AddColumn(TR2, TR2_02, 10, 10, "l", DR1(1))
                RH.AddColumn(TR2, TR2_03, 20, 20, "l", DR1(2).ToString().ToUpper())
                RH.AddColumn(TR2, TR2_04, 15, 15, "l", CDate(DR1(3)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR2, TR2_05, 15, 15, "l", DR1(4))
                RH.AddColumn(TR2, TR2_06, 10, 10, "l", DR1(5))
                RH.AddColumn(TR2, TR2_07, 10, 10, "l", DR1(6))
                tb.Controls.Add(TR2)
                I = I + 1
            Next
            RH.DrawLine(tb, 100)
            RH.BlankRow(tb, 30)

            pnDisplay.Controls.Add(tb)
        
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Dim HeaderText As String = "PunchingReport"
            WebTools.ExporttoExcel(DT, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
