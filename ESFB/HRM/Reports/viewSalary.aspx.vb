﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewEmployeeDetails
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim Trans_id As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Trans_id = CInt(GF.Decrypt(Request.QueryString.Get("Trans_id")))
            Dim DT As New DataTable
            tb.Attributes.Add("width", "100%")
            Dim RowBG As Integer = 0
            Dim GrossSalary As Double
            Dim Deductions As Double
            Dim ESI As Double
            Dim PF As Double
            Dim StaffWelfare As Double
            DT = DB.ExecuteDataSet(" select j.emp_code,j.emp_name, convert(varchar,a.Effective_Date,106) as edate,e.Designation_Name as fromdesg,f.Designation_Name as todesg,b.Emp_Name as fromReporting,h.Emp_Name as ToReporting   " & _
                " ,c.Post_Name as fromPost,i.Post_Name as toPost, convert(numeric(16,0),isnull(a.Basic_Pay,0)),convert(numeric(16,0),isnull(a.DA,0)), convert(numeric(16,0),isnull(a.HRA,0)), " & _
                " convert(numeric(16,0),isnull(a.Conveyance,0)), convert(numeric(16,0),isnull(a.SpecialAllowance,0)), convert(numeric(16,0),isnull(a.LocalAllowance,0)),convert(numeric(16,0),isnull(a.PerformanceAllowance,0)), convert(numeric(16,0),isnull(a.OtherAllowance,0)), convert(numeric(16,0),isnull(a.MedicalAllowance,0)),  " & _
                " convert(numeric(16,0),isnull(a.HospitalityAllowance,0)), convert(numeric(16,0),isnull(a.FieldAllowance,0)), isnull(a.ESI,0), isnull(a.PF,0), isnull(a.SWF,0), " & _
                " isnull(a.Charity,0),isnull(a.ESWT,0) as ESWT  from EMP_PROMOTION  a,EMP_MASTER b,EMP_MASTER j,EMP_POST_MASTER c,BRANCH_MASTER d,DESIGNATION_MASTER  e,DESIGNATION_MASTER  f,EMP_MASTER h,EMP_POST_MASTER i " & _
                " where(a.Reporting_To_From = b.Emp_Code And a.EmpPost = c.Post_ID And a.Branch_ID = d.Branch_ID And a.Designation_ID_From = e.Designation_ID) " & _
                " and a.Designation_ID_to =f.Designation_ID and a.Reporting_To_To=h.emp_code and a.EmpPost=i.Post_ID " & _
                " and a.Emp_code =j.Emp_Code  and a.trans_id=" & Trans_id & "").Tables(0)
            GrossSalary = DT.Rows(0)(9) + DT.Rows(0)(10) + DT.Rows(0)(11) + DT.Rows(0)(12) + DT.Rows(0)(13) + DT.Rows(0)(14) + DT.Rows(0)(15) + DT.Rows(0)(16) + DT.Rows(0)(17) + DT.Rows(0)(18) + DT.Rows(0)(19)

            ESI = CInt(DT.Rows(0)(20))
            PF = CInt(DT.Rows(0)(21))
            StaffWelfare = CInt(DT.Rows(0)(22))
            Deductions = ESI + PF + StaffWelfare + CInt(DT.Rows(0)(23)) + CInt(DT.Rows(0)(24))

            RH.Heading(CStr(Session("FirmName")), tb, "SALARY DETAILS", 100)
            Dim TRItemHead As New TableRow
            TRItemHead.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead_00 As New TableCell
            RH.BlankRow(tb, 5)


            Dim TR18 As New TableRow
            'TR18.BackColor = Drawing.Color.WhiteSmoke
            Dim TR18_01, TR18_02, TR18_03, TR18_04, TR18_05, TR18_06, TR18_07, TR18_08 As New TableCell

            TR18_01.BackColor = Drawing.Color.WhiteSmoke
            TR18_07.BackColor = Drawing.Color.WhiteSmoke
            TR18_04.BackColor = Drawing.Color.WhiteSmoke
            TR18_01.BorderWidth = "1"
            TR18_02.BorderWidth = "1"
            TR18_04.BorderWidth = "1"
            TR18_05.BorderWidth = "1"
            TR18_07.BorderWidth = "1"
            TR18_08.BorderWidth = "1"

            TR18_01.BorderColor = Drawing.Color.Silver
            TR18_02.BorderColor = Drawing.Color.Silver
            TR18_04.BorderColor = Drawing.Color.Silver
            TR18_05.BorderColor = Drawing.Color.Silver
            TR18_07.BorderColor = Drawing.Color.Silver
            TR18_08.BorderColor = Drawing.Color.Silver



            TR18_01.BorderStyle = BorderStyle.Solid
            TR18_02.BorderStyle = BorderStyle.Solid
            TR18_04.BorderStyle = BorderStyle.Solid
            TR18_05.BorderStyle = BorderStyle.Solid
            TR18_07.BorderStyle = BorderStyle.Solid
            TR18_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR18, TR18_01, 16, 16, "l", "Basic Pay")
            RH.AddColumn(TR18, TR18_02, 16, 16, "r", DT.Rows(0)(9))
            RH.AddColumn(TR18, TR18_03, 2, 2, "c", "")
            RH.AddColumn(TR18, TR18_04, 16, 16, "l", "DA")
            RH.AddColumn(TR18, TR18_05, 16, 16, "r", DT.Rows(0)(10))
            RH.AddColumn(TR18, TR18_06, 2, 2, "l", "")
            RH.AddColumn(TR18, TR18_07, 16, 16, "l", "ESI")
            RH.AddColumn(TR18, TR18_08, 16, 16, "r", ESI)


            tb.Controls.Add(TR18)


            Dim TR19 As New TableRow
            'TR19.BackColor = Drawing.Color.WhiteSmoke
            Dim TR19_01, TR19_02, TR19_03, TR19_04, TR19_05, TR19_06, TR19_07, TR19_08 As New TableCell

            TR19_01.BackColor = Drawing.Color.WhiteSmoke
            TR19_07.BackColor = Drawing.Color.WhiteSmoke
            TR19_04.BackColor = Drawing.Color.WhiteSmoke
            TR19_01.BorderWidth = "1"
            TR19_02.BorderWidth = "1"
            TR19_04.BorderWidth = "1"
            TR19_05.BorderWidth = "1"
            TR19_07.BorderWidth = "1"
            TR19_08.BorderWidth = "1"

            TR19_01.BorderColor = Drawing.Color.Silver
            TR19_02.BorderColor = Drawing.Color.Silver
            TR19_04.BorderColor = Drawing.Color.Silver
            TR19_05.BorderColor = Drawing.Color.Silver
            TR19_07.BorderColor = Drawing.Color.Silver
            TR19_08.BorderColor = Drawing.Color.Silver



            TR19_01.BorderStyle = BorderStyle.Solid
            TR19_02.BorderStyle = BorderStyle.Solid
            TR19_04.BorderStyle = BorderStyle.Solid
            TR19_05.BorderStyle = BorderStyle.Solid
            TR19_07.BorderStyle = BorderStyle.Solid
            TR19_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR19, TR19_01, 16, 16, "l", "Conveyance")
            RH.AddColumn(TR19, TR19_02, 16, 16, "r", DT.Rows(0)(12))
            RH.AddColumn(TR19, TR19_03, 2, 2, "c", "")
            RH.AddColumn(TR19, TR19_04, 16, 16, "l", "Special Allwnce")
            RH.AddColumn(TR19, TR19_05, 16, 16, "r", DT.Rows(0)(13))
            RH.AddColumn(TR19, TR19_06, 2, 2, "l", "")
            RH.AddColumn(TR19, TR19_07, 16, 16, "l", "PF")
            RH.AddColumn(TR19, TR19_08, 16, 16, "r", PF)


            tb.Controls.Add(TR19)

            Dim TR20 As New TableRow
            'TR20.BackColor = Drawing.Color.WhiteSmoke
            Dim TR20_01, TR20_02, TR20_03, TR20_04, TR20_05, TR20_06, TR20_07, TR20_08 As New TableCell

            TR20_01.BackColor = Drawing.Color.WhiteSmoke
            TR20_07.BackColor = Drawing.Color.WhiteSmoke
            TR20_04.BackColor = Drawing.Color.WhiteSmoke
            TR20_01.BorderWidth = "1"
            TR20_02.BorderWidth = "1"
            TR20_04.BorderWidth = "1"
            TR20_05.BorderWidth = "1"
            TR20_07.BorderWidth = "1"
            TR20_08.BorderWidth = "1"

            TR20_01.BorderColor = Drawing.Color.Silver
            TR20_02.BorderColor = Drawing.Color.Silver
            TR20_04.BorderColor = Drawing.Color.Silver
            TR20_05.BorderColor = Drawing.Color.Silver
            TR20_07.BorderColor = Drawing.Color.Silver
            TR20_08.BorderColor = Drawing.Color.Silver



            TR20_01.BorderStyle = BorderStyle.Solid
            TR20_02.BorderStyle = BorderStyle.Solid
            TR20_04.BorderStyle = BorderStyle.Solid
            TR20_05.BorderStyle = BorderStyle.Solid
            TR20_07.BorderStyle = BorderStyle.Solid
            TR20_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR20, TR20_01, 16, 16, "l", "Performance Allwnce")
            RH.AddColumn(TR20, TR20_02, 16, 16, "r", DT.Rows(0)(15))
            RH.AddColumn(TR20, TR20_03, 2, 2, "c", "")
            RH.AddColumn(TR20, TR20_04, 16, 16, "l", "Other Allwnce")
            RH.AddColumn(TR20, TR20_05, 16, 16, "r", DT.Rows(0)(16))
            RH.AddColumn(TR20, TR20_06, 2, 2, "l", "")
            RH.AddColumn(TR20, TR20_07, 16, 16, "l", "Staff Welfare")
            RH.AddColumn(TR20, TR20_08, 16, 16, "r", StaffWelfare)


            tb.Controls.Add(TR20)
            Dim TR21 As New TableRow
            'TR21.BackColor = Drawing.Color.WhiteSmoke
            Dim TR21_01, TR21_02, TR21_03, TR21_04, TR21_05, TR21_06, TR21_07, TR21_08 As New TableCell

            TR21_01.BackColor = Drawing.Color.WhiteSmoke
            TR21_07.BackColor = Drawing.Color.WhiteSmoke
            TR21_04.BackColor = Drawing.Color.WhiteSmoke
            TR21_01.BorderWidth = "1"
            TR21_02.BorderWidth = "1"
            TR21_04.BorderWidth = "1"
            TR21_05.BorderWidth = "1"
            TR21_07.BorderWidth = "1"
            TR21_08.BorderWidth = "1"

            TR21_01.BorderColor = Drawing.Color.Silver
            TR21_02.BorderColor = Drawing.Color.Silver
            TR21_04.BorderColor = Drawing.Color.Silver
            TR21_05.BorderColor = Drawing.Color.Silver
            TR21_07.BorderColor = Drawing.Color.Silver
            TR21_08.BorderColor = Drawing.Color.Silver



            TR21_01.BorderStyle = BorderStyle.Solid
            TR21_02.BorderStyle = BorderStyle.Solid
            TR21_04.BorderStyle = BorderStyle.Solid
            TR21_05.BorderStyle = BorderStyle.Solid
            TR21_07.BorderStyle = BorderStyle.Solid
            TR21_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR21, TR21_01, 16, 16, "l", "Hospital Allwnce")
            RH.AddColumn(TR21, TR21_02, 16, 16, "r", DT.Rows(0)(18))
            RH.AddColumn(TR21, TR21_03, 2, 2, "c", "")
            RH.AddColumn(TR21, TR21_04, 16, 16, "l", "Field Allwnce")
            RH.AddColumn(TR21, TR21_05, 16, 16, "r", DT.Rows(0)(19))
            RH.AddColumn(TR21, TR21_06, 2, 2, "l", "")
            RH.AddColumn(TR21, TR21_07, 16, 16, "l", "ESWT")
            RH.AddColumn(TR21, TR21_08, 16, 16, "r", CInt(DT.Rows(0)(24)))


            tb.Controls.Add(TR21)
            Dim TR12 As New TableRow
            'TR12.BackColor = Drawing.Color.WhiteSmoke
            Dim TR12_01, TR12_02, TR12_03, TR12_04, TR12_05, TR12_06, TR12_07, TR12_08 As New TableCell

            TR12_01.BackColor = Drawing.Color.WhiteSmoke
            TR12_07.BackColor = Drawing.Color.WhiteSmoke
            TR12_04.BackColor = Drawing.Color.WhiteSmoke
            TR12_01.BorderWidth = "1"
            TR12_02.BorderWidth = "1"
            TR12_04.BorderWidth = "1"
            TR12_05.BorderWidth = "1"
            TR12_07.BorderWidth = "1"
            TR12_08.BorderWidth = "1"

            TR12_01.BorderColor = Drawing.Color.Silver
            TR12_02.BorderColor = Drawing.Color.Silver
            TR12_04.BorderColor = Drawing.Color.Silver
            TR12_05.BorderColor = Drawing.Color.Silver
            TR12_07.BorderColor = Drawing.Color.Silver
            TR12_08.BorderColor = Drawing.Color.Silver



            TR12_01.BorderStyle = BorderStyle.Solid
            TR12_02.BorderStyle = BorderStyle.Solid
            TR12_04.BorderStyle = BorderStyle.Solid
            TR12_05.BorderStyle = BorderStyle.Solid
            TR12_07.BorderStyle = BorderStyle.Solid
            TR12_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR12, TR12_01, 16, 16, "l", "HRA")
            RH.AddColumn(TR12, TR12_02, 16, 16, "r", DT.Rows(0)(11))
            RH.AddColumn(TR12, TR12_03, 2, 2, "c", "")
            RH.AddColumn(TR12, TR12_04, 16, 16, "l", "Local Allwnce")
            RH.AddColumn(TR12, TR12_05, 16, 16, "r", DT.Rows(0)(14))
            RH.AddColumn(TR12, TR12_06, 2, 2, "l", "")
            RH.AddColumn(TR12, TR12_07, 16, 16, "l", "Charity Fund")
            RH.AddColumn(TR12, TR12_08, 16, 16, "r", CInt(DT.Rows(0)(23)))


            tb.Controls.Add(TR12)
            Dim TR15 As New TableRow
            'TR15.BackColor = Drawing.Color.WhiteSmoke
            Dim TR15_01, TR15_02, TR15_03, TR15_04, TR15_05, TR15_06, TR15_07, TR15_08 As New TableCell

            TR15_01.BackColor = Drawing.Color.WhiteSmoke
            TR15_07.BackColor = Drawing.Color.WhiteSmoke
            TR15_04.BackColor = Drawing.Color.WhiteSmoke
            TR15_01.BorderWidth = "1"
            TR15_02.BorderWidth = "1"
            TR15_04.BorderWidth = "1"
            TR15_05.BorderWidth = "1"
            TR15_07.BorderWidth = "1"
            TR15_08.BorderWidth = "1"

            TR15_01.BorderColor = Drawing.Color.Silver
            TR15_02.BorderColor = Drawing.Color.Silver
            TR15_04.BorderColor = Drawing.Color.Silver
            TR15_05.BorderColor = Drawing.Color.Silver
            TR15_07.BorderColor = Drawing.Color.Silver
            TR15_08.BorderColor = Drawing.Color.Silver



            TR15_01.BorderStyle = BorderStyle.Solid
            TR15_02.BorderStyle = BorderStyle.Solid
            TR15_04.BorderStyle = BorderStyle.Solid
            TR15_05.BorderStyle = BorderStyle.Solid
            TR15_07.BorderStyle = BorderStyle.Solid
            TR15_08.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TR15, TR15_01, 16, 16, "l", "Medical Allowance")
            RH.AddColumn(TR15, TR15_02, 16, 16, "r", DT.Rows(0)(17))
            RH.AddColumn(TR15, TR15_03, 2, 2, "c", "")
            RH.AddColumn(TR15, TR15_04, 16, 16, "l", "")
            RH.AddColumn(TR15, TR15_05, 16, 16, "r", "")
            RH.AddColumn(TR15, TR15_06, 2, 2, "l", "")
            RH.AddColumn(TR15, TR15_07, 16, 16, "r", "")
            RH.AddColumn(TR15, TR15_08, 16, 16, "r", "")


            tb.Controls.Add(TR15)
            'RH.BlankRow(tb, 5)

            Dim TR13 As New TableRow
            'TR13.BackColor = Drawing.Color.WhiteSmoke
            Dim TR13_01, TR13_02, TR13_03, TR13_04, TR13_05, TR13_06, TR13_07, TR13_08 As New TableCell


            TR13_07.BackColor = Drawing.Color.WhiteSmoke
            TR13_04.BackColor = Drawing.Color.WhiteSmoke

            TR13_04.BorderWidth = "1"
            TR13_05.BorderWidth = "1"
            TR13_07.BorderWidth = "1"
            TR13_08.BorderWidth = "1"


            TR13_04.BorderColor = Drawing.Color.Silver
            TR13_05.BorderColor = Drawing.Color.Silver
            TR13_07.BorderColor = Drawing.Color.Silver
            TR13_08.BorderColor = Drawing.Color.Silver




            TR13_04.BorderStyle = BorderStyle.Solid
            TR13_05.BorderStyle = BorderStyle.Solid
            TR13_07.BorderStyle = BorderStyle.Solid
            TR13_08.BorderStyle = BorderStyle.Solid

            TR13_05.ForeColor = Drawing.Color.DarkRed
            TR13_08.ForeColor = Drawing.Color.DarkRed
            TR13_05.Font.Bold = True
            TR13_08.Font.Bold = True
            TR13_04.Font.Bold = True
            TR13_07.Font.Bold = True
            RH.AddColumn(TR13, TR13_01, 16, 16, "l", "")
            RH.AddColumn(TR13, TR13_02, 16, 16, "l", "")
            RH.AddColumn(TR13, TR13_03, 2, 2, "c", "")
            RH.AddColumn(TR13, TR13_04, 16, 16, "r", "Total [+]")
            RH.AddColumn(TR13, TR13_05, 16, 16, "r", GrossSalary)
            RH.AddColumn(TR13, TR13_06, 2, 2, "l", "")
            RH.AddColumn(TR13, TR13_07, 16, 16, "r", "Total [-]")
            RH.AddColumn(TR13, TR13_08, 16, 16, "r", Deductions)


            tb.Controls.Add(TR13)
            RH.BlankRow(tb, 5)
            RH.BlankRow(tb, 5)
            RH.BlankRow(tb, 5)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
