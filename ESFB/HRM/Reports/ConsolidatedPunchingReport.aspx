﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ConsolidatedPunchingReport.aspx.vb" Inherits="Reports_ConsolidatedPunchingReport" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Calculations.js" type="text/javascript"></script>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        
        function GenerateOnClick() {
            var frmDt = document.getElementById("<%= txtFromDt.ClientID %>").value;            
            if (frmDt == "")
            { alert("Select From Date"); document.getElementById("<%= txtToDt.ClientID %>").focus(); return false; }
            var toDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            if (toDt == "")
            { alert("Select To Date"); document.getElementById("<%= txtToDt.ClientID %>").focus(); return false; }
        }
    </script>
    <table style="width:80%;margin: 0px auto;" >
        <tr>
            <td style="width:25%;">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            </td>
            <td style="width:12%; text-align:left;">
                &nbsp;</td>
            <td style="width:63%">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                From
                Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtFromDt" class="NormalText" runat="server" 
                    Width="40%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txtFromDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtFromDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
                &nbsp;</td>
            <td style="width:12%; text-align:left;">
                To Date</td>
            <td style="width:63%">
                &nbsp;&nbsp;
                <asp:TextBox ID="txtToDt" class="NormalText" runat="server" 
                    Width="40%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtToDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="text-align:center;"  colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;"  colspan="3">
                &nbsp; 
                <asp:Button ID="btnGenerate" runat="server" Text="GENERATE" Width="10%" 
                    style="cursor:pointer" Font-Names="Cambria"  />
                &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" 
                type="button" value="EXIT" onclick="window.open('../../Home.aspx','_self')"  /></td>
        </tr>
        <tr >
            <td style="text-align:center; " colspan="3">
                <br />
                &nbsp;&nbsp;
                <asp:HiddenField ID="hdnDate" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
