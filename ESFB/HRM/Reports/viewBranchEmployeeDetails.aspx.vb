﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewEmployeeDetails
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim EMP_Code As Integer
    Dim Name As String
    Dim Doj As String
    Dim cadre As String
    Dim dep As String
    Dim des As String
    Dim Branch As String
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            EMP_Code = CInt(GF.Decrypt(Request.QueryString.Get("EmpCode")))
            Name = CStr(GF.Decrypt(Request.QueryString.Get("Name")))
            Doj = CStr(GF.Decrypt(Request.QueryString.Get("DOJ")))
            cadre = CStr(GF.Decrypt(Request.QueryString.Get("Cadre")))
            dep = CStr(GF.Decrypt(Request.QueryString.Get("Dep")))
            des = CStr(GF.Decrypt(Request.QueryString.Get("Des")))
            Branch = CStr(GF.Decrypt(Request.QueryString.Get("Branch")))
            hdnReportID.Value = CStr(GF.Decrypt(Request.QueryString.Get("BranchID")))

            Dim DT As New DataTable
            Dim DT_Quali As New DataTable
            Dim DT_Exp As New DataTable
            Dim DT_ID As New DataTable
            Dim DT_ADDR As New DataTable
            Dim DT_FAM As New DataTable
            Dim DT_LANG As New DataTable
            tb.Attributes.Add("width", "100%")




            Dim RowBG As Integer = 0

            Dim GrossSalary As Double
            Dim Deductions As Double
            Dim ESI As Double
            Dim PF As Double
            Dim StaffWelfare As Double
            DT = DB.ExecuteDataSet("select a.emp_code,dbo.udfPropercase(a.emp_name), dbo.udfPropercase(isnull(d.branch_Name ,'')),dbo.udfPropercase(isnull(f.Cadre_name  ,'')), dbo.udfPropercase(isnull(g.Designation_Name ,'')), dbo.udfPropercase(isnull(e.Department_Name ,'')), dbo.udfPropercase(isnull(convert(varchar,a.Date_Of_Join ,106),'')), dbo.udfPropercase(isnull(b.DOB,'')),isnull(b.Email,''),dbo.udfPropercase(isnull(h.Gender_Name,'')),isnull(b.Mobile,''),dbo.udfPropercase(isnull(b.HouseName,'')) , isnull(b.LandLine,''),dbo.udfPropercase(b.CareOf),isnull(b.CareOf_Phone,''), dbo.udfPropercase(b.City),dbo.udfPropercase(b.location)," & _
            " dbo.udfPropercase(isnull(v.MStatus,'')), dbo.udfPropercase(isnull(m.Caste_name,'')), dbo.udfPropercase(isnull(k.Pin_Code,'')), dbo.udfPropercase(isnull(k.Post_Office,'')), convert(numeric(16,0),isnull(c.Basic_Pay,0)),convert(numeric(16,0),isnull(c.DA,0)), convert(numeric(16,0),isnull(c.HRA,0)), " & _
            " convert(numeric(16,0),isnull(c.Conveyance,0)), convert(numeric(16,0),isnull(c.SpecialAllowance,0)), convert(numeric(16,0),isnull(c.LocalAllowance,0)),convert(numeric(16,0),isnull(c.PerformanceAllowance,0)), convert(numeric(16,0),isnull(c.OtherAllowance,0)), convert(numeric(16,0),isnull(c.MedicalAllowance,0))," & _
            " convert(numeric(16,0),isnull(c.HospitalityAllowance,0)), convert(numeric(16,0),isnull(c.FieldAllowance,0)),dbo.udfPropercase(isnull(j.District_Name,'')),dbo.udfPropercase(isnull(i.state_name,'')), isnull(c.ESI,0), isnull(c.PF,0), isnull(c.SWF,0)," & _
            " isnull(c.Charity,0),isnull(c.ESWT,0) as ESWT ,dbo.udfPropercase(isnull(b.Fathers_name,'')), dbo.udfPropercase(isnull(b.Mothers_name,'')) ,dbo.udfPropercase(isnull(t.Blood_Name,'')) ,dbo.udfPropercase(isnull(u.Language,'')) ,isnull(case when convert(varchar,b.Marriage_Date,106)='01-jan-1900' then '' else convert(varchar,b.Marriage_Date,106) end,'') as Marriage_Date,CASE when b.Twowheeler_Status=1 then 'YES' else 'NO' end, dbo.udfPropercase(isnull(b.Driving_License_No,'')) " & _
            " ,dbo.udfPropercase(isnull(b.Cug_No,'')),dbo.udfPropercase(isnull(b.Official_mail_id,'')),case when b.Ref_Letter_Panchayath=1 then 'YES' else 'NO' END  ,case when b.Ref_Letter_Community=1 then 'YES' else 'NO' END ," & _
            " CASE when b.Ref_Letter_PoliceStation=1 then 'YES' ELSE 'NO' END ,case when b.Is_Sangam_member=1 then 'YES' else 'NO' END,dbo.udfPropercase(isnull(r.Family_person,'')) , dbo.udfPropercase(isnull(b.Surety_person_name,'')),dbo.udfPropercase(isnull(b.Surety_Address,'')), dbo.udfPropercase(isnull(b.Surety_Phone,'')),dbo.udfPropercase(isnull(s.Religion_name,'')) ,w.Emp_Status  " & _
            " from EMP_MASTER a LEFT JOIN  Emp_Profile b ON a.Emp_Code=b.Emp_Code   LEFT JOIN CASTE_MASTER m ON  b.Caste_id =m.caste_id " & _
            " LEFT JOIN EMP_SALARY_DTL c ON b.Emp_Code=c.Emp_Code LEFT JOIN BRANCH_MASTER d ON a.Branch_ID=d.Branch_ID LEFT JOIN DEPARTMENT_MASTER e ON a.Department_ID=e.Department_ID LEFT JOIN   CADRE_MASTER f ON  a.Cadre_ID=f.Cadre_ID LEFT JOIN DESIGNATION_MASTER g ON  a.Designation_ID=g.Designation_ID " & _
            " LEFT JOIN Post_Master k ON b.Post_ID=k.Post_ID LEFT JOIN District_Master j ON k.District_ID=j.District_ID LEFT JOIN State_Master i ON j.State_ID =i.State_ID LEFT JOIN emp_master l ON a.Reporting_to=l.Emp_Code LEFT JOIN GENDER_MASTER h ON a.gender_id=h.gender_id  " & _
             " LEFT JOIN FAMILY_MASTER r ON b.Surety_id=r.Family_id LEFT JOIN RELIGION_MASTER s ON m.Religion_Id=s.Religion_id LEFT JOIN BLOOD_GROUP t ON b.Blood_Group=t.Blood_Id LEFT JOIN LANGUAGE_MASTER u ON b.Mother_tongue=u.Language_id LEFT JOIN MARITAL_STATUS v ON v.MStatus_ID=b.Marital_Status " & _
             " LEFT JOIN employee_status w ON a.emp_status_id =w.emp_status_id where a.Emp_Code = " & EMP_Code & "").Tables(0)
            DT_Quali = DB.ExecuteDataSet("select b.QUALIFICATION_NAME,a.Qualification,c.UNIVERSITY_NAME,a.YearOFPassing from EMP_QUALIFICATION a,QUALIFICATION_MASTER b,UNIVERSITY_MASTER c where a.Qualification_ID=b.QUALIFICATION_ID and a.University_ID =c.UNIVERSITY_ID  and a.Emp_Code=" & EMP_Code & "").Tables(0)
            DT_Exp = DB.ExecuteDataSet("select Company,Address,Phone,No_of_years  from EMP_EXPERIENCE_DETAILS  where Emp_Code=" & EMP_Code & "").Tables(0)
            'To Be modified
            Dim LangVal As String = ""

            DT_LANG = DB.ExecuteDataSet("select b.Language from EMP_LANGUAGES_KNOWN a,LANGUAGE_MASTER b WHERE a.Language_id=b.Language_id and a.emp_code=" & EMP_Code & "").Tables(0)
            For Each DR As DataRow In DT_LANG.Rows
                LangVal += "," + DR(0).ToString()

            Next
            If (LangVal.Length > 0) Then
                LangVal = LangVal.Substring(1)
            End If
            DT_FAM = DB.ExecuteDataSet("select b.Family_person,a.Person_Name,a.Age,a.Occupation  from EMP_FAMILY_DETAILS a,FAMILY_MASTER b where a.Family_id=b.Family_id and a.Emp_Code=" & EMP_Code & "").Tables(0)
            DT_ADDR = DB.ExecuteDataSet("select b.Type_Name,a.Address_proof_Name  from EMP_ADDRESS_PROOF_DETAILS a,ID_Proof_Type b where a.Address_Proof_Id=b.Type_ID and a.Emp_Code=" & EMP_Code & "").Tables(0)
            DT_ID = DB.ExecuteDataSet("select b.Type_Name,a.Proof_Name  from EMP_ID_PROOF_DETAILS a,ID_Proof_Type b where a.ID_Proof_Id=b.Type_ID  and a.Emp_Code=" & EMP_Code & "").Tables(0)
            GrossSalary = DT.Rows(0)(21) + DT.Rows(0)(22) + DT.Rows(0)(23) + DT.Rows(0)(24) + DT.Rows(0)(25) + DT.Rows(0)(26) + DT.Rows(0)(27) + DT.Rows(0)(28) + DT.Rows(0)(29) + DT.Rows(0)(30) + DT.Rows(0)(31)


            ESI = CInt(DT.Rows(0)(34))
            PF = CInt(DT.Rows(0)(35))
            StaffWelfare = CInt(DT.Rows(0)(36))
            Deductions = ESI + PF + StaffWelfare + CInt(DT.Rows(0)(37)) + CInt(DT.Rows(0)(38))

            RH.Heading(CStr(Session("FirmName")), tb, "EMPLOYEE DETAILS", 100)
            Dim TRItemHead As New TableRow
            TRItemHead.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead_00 As New TableCell
            RH.BlankRow(tb, 5)
            RH.AddColumn(TRItemHead, TRItemHead_00, 100, 100, "c", "<b>APPOINTMENT DETAILS</b>")
            TRItemHead.BackColor = Drawing.Color.WhiteSmoke
            tb.Controls.Add(TRItemHead)
            RH.BlankRow(tb, 5)


            Dim TR62 As New TableRow
            'TR62.BackColor = Drawing.Color.WhiteSmoke
            Dim TR62_01, TR62_02, TR62_03, TR62_04, TR62_05, TR62_06, TR62_07, TR62_08 As New TableCell
            TR62_01.BackColor = Drawing.Color.WhiteSmoke
            TR62_07.BackColor = Drawing.Color.WhiteSmoke
            TR62_04.BackColor = Drawing.Color.WhiteSmoke
            TR62_01.BorderWidth = "1"
            TR62_02.BorderWidth = "1"
            TR62_04.BorderWidth = "1"
            TR62_05.BorderWidth = "1"
            TR62_07.BorderWidth = "1"
            TR62_08.BorderWidth = "1"

            TR62_01.BorderColor = Drawing.Color.Silver
            TR62_02.BorderColor = Drawing.Color.Silver
            TR62_04.BorderColor = Drawing.Color.Silver
            TR62_05.BorderColor = Drawing.Color.Silver
            TR62_07.BorderColor = Drawing.Color.Silver
            TR62_08.BorderColor = Drawing.Color.Silver



            TR62_01.BorderStyle = BorderStyle.Solid
            TR62_02.BorderStyle = BorderStyle.Solid
            TR62_04.BorderStyle = BorderStyle.Solid
            TR62_05.BorderStyle = BorderStyle.Solid
            TR62_07.BorderStyle = BorderStyle.Solid
            TR62_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR62, TR62_01, 16, 16, "l", "Employee Code")
            RH.AddColumn(TR62, TR62_02, 16, 16, "l", DT.Rows(0)(0))
            RH.AddColumn(TR62, TR62_03, 2, 2, "c", "")
            RH.AddColumn(TR62, TR62_04, 16, 16, "l", "Name")
            RH.AddColumn(TR62, TR62_05, 16, 16, "l", DT.Rows(0)(1))
            RH.AddColumn(TR62, TR62_06, 2, 2, "l", "")
            RH.AddColumn(TR62, TR62_07, 16, 16, "l", "Branch")
            RH.AddColumn(TR62, TR62_08, 16, 16, "l", DT.Rows(0)(2))


            tb.Controls.Add(TR62)

            Dim TR63 As New TableRow
            'TR63.BackColor = Drawing.Color.WhiteSmoke
            Dim TR63_01, TR63_02, TR63_03, TR63_04, TR63_05, TR63_06, TR63_07, TR63_08 As New TableCell
            TR63_01.BackColor = Drawing.Color.WhiteSmoke
            TR63_07.BackColor = Drawing.Color.WhiteSmoke
            TR63_04.BackColor = Drawing.Color.WhiteSmoke
            TR63_01.BorderWidth = "1"
            TR63_02.BorderWidth = "1"
            TR63_04.BorderWidth = "1"
            TR63_05.BorderWidth = "1"
            TR63_07.BorderWidth = "1"
            TR63_08.BorderWidth = "1"

            TR63_01.BorderColor = Drawing.Color.Silver
            TR63_02.BorderColor = Drawing.Color.Silver
            TR63_04.BorderColor = Drawing.Color.Silver
            TR63_05.BorderColor = Drawing.Color.Silver
            TR63_07.BorderColor = Drawing.Color.Silver
            TR63_08.BorderColor = Drawing.Color.Silver



            TR63_01.BorderStyle = BorderStyle.Solid
            TR63_02.BorderStyle = BorderStyle.Solid
            TR63_04.BorderStyle = BorderStyle.Solid
            TR63_05.BorderStyle = BorderStyle.Solid
            TR63_07.BorderStyle = BorderStyle.Solid
            TR63_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR63, TR63_01, 16, 16, "l", "Cadre")
            RH.AddColumn(TR63, TR63_02, 16, 16, "l", DT.Rows(0)(3))
            RH.AddColumn(TR63, TR63_03, 2, 2, "c", "")
            RH.AddColumn(TR63, TR63_04, 16, 16, "l", "Department")
            RH.AddColumn(TR63, TR63_05, 16, 16, "l", DT.Rows(0)(4))
            RH.AddColumn(TR63, TR63_06, 2, 2, "l", "")
            RH.AddColumn(TR63, TR63_07, 16, 16, "l", "Designation")
            RH.AddColumn(TR63, TR63_08, 16, 16, "l", DT.Rows(0)(5))


            tb.Controls.Add(TR63)

            Dim TR64 As New TableRow
            'TR64.BackColor = Drawing.Color.WhiteSmoke
            Dim TR64_01, TR64_02, TR64_03, TR64_04, TR64_05, TR64_06, TR64_07, TR64_08 As New TableCell
            TR64_01.BackColor = Drawing.Color.WhiteSmoke
            TR64_07.BackColor = Drawing.Color.WhiteSmoke
            TR64_04.BackColor = Drawing.Color.WhiteSmoke
            TR64_01.BorderWidth = "1"
            TR64_02.BorderWidth = "1"
            TR64_04.BorderWidth = "1"
            TR64_05.BorderWidth = "1"
            TR64_07.BorderWidth = "1"
            TR64_08.BorderWidth = "1"

            TR64_01.BorderColor = Drawing.Color.Silver
            TR64_02.BorderColor = Drawing.Color.Silver
            TR64_04.BorderColor = Drawing.Color.Silver
            TR64_05.BorderColor = Drawing.Color.Silver
            TR64_07.BorderColor = Drawing.Color.Silver
            TR64_08.BorderColor = Drawing.Color.Silver



            TR64_01.BorderStyle = BorderStyle.Solid
            TR64_02.BorderStyle = BorderStyle.Solid
            TR64_04.BorderStyle = BorderStyle.Solid
            TR64_05.BorderStyle = BorderStyle.Solid
            TR64_07.BorderStyle = BorderStyle.Solid
            TR64_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR64, TR64_01, 16, 16, "l", "Date Of Joining")
            RH.AddColumn(TR64, TR64_02, 16, 16, "l", DT.Rows(0)(6))
            RH.AddColumn(TR64, TR64_03, 2, 2, "c", "")
            RH.AddColumn(TR64, TR64_04, 16, 16, "l", "Status")
            RH.AddColumn(TR64, TR64_05, 16, 16, "l", DT.Rows(0)(57))
            RH.AddColumn(TR64, TR64_06, 2, 2, "l", "")
            RH.AddColumn(TR64, TR64_07, 16, 16, "l", "Surety Type")
            RH.AddColumn(TR64, TR64_08, 16, 16, "l", DT.Rows(0)(52))


            tb.Controls.Add(TR64)

            Dim TR65 As New TableRow
            'TR65.BackColor = Drawing.Color.WhiteSmoke
            Dim TR65_01, TR65_02, TR65_03, TR65_04, TR65_05, TR65_06, TR65_07, TR65_08 As New TableCell
            TR65_01.BackColor = Drawing.Color.WhiteSmoke
            TR65_07.BackColor = Drawing.Color.WhiteSmoke
            TR65_04.BackColor = Drawing.Color.WhiteSmoke
            TR65_01.BorderWidth = "1"
            TR65_02.BorderWidth = "1"
            TR65_04.BorderWidth = "1"
            TR65_05.BorderWidth = "1"
            TR65_07.BorderWidth = "1"
            TR65_08.BorderWidth = "1"

            TR65_01.BorderColor = Drawing.Color.Silver
            TR65_02.BorderColor = Drawing.Color.Silver
            TR65_04.BorderColor = Drawing.Color.Silver
            TR65_05.BorderColor = Drawing.Color.Silver
            TR65_07.BorderColor = Drawing.Color.Silver
            TR65_08.BorderColor = Drawing.Color.Silver



            TR65_01.BorderStyle = BorderStyle.Solid
            TR65_02.BorderStyle = BorderStyle.Solid
            TR65_04.BorderStyle = BorderStyle.Solid
            TR65_05.BorderStyle = BorderStyle.Solid
            TR65_07.BorderStyle = BorderStyle.Solid
            TR65_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR65, TR65_01, 16, 16, "l", "Surety Name")
            RH.AddColumn(TR65, TR65_02, 16, 16, "l", DT.Rows(0)(53))
            RH.AddColumn(TR65, TR65_03, 2, 2, "c", "")
            RH.AddColumn(TR65, TR65_04, 16, 16, "l", "Surety Address")
            RH.AddColumn(TR65, TR65_05, 16, 16, "l", DT.Rows(0)(54))
            RH.AddColumn(TR65, TR65_06, 2, 2, "l", "")
            RH.AddColumn(TR65, TR65_07, 16, 16, "l", "Surety Phone")
            RH.AddColumn(TR65, TR65_08, 16, 16, "l", DT.Rows(0)(55))


            tb.Controls.Add(TR65)

            Dim TRItemHead1 As New TableRow
            TRItemHead1.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead1_00 As New TableCell
            RH.BlankRow(tb, 5)
            RH.AddColumn(TRItemHead1, TRItemHead1_00, 100, 100, "c", "<b>PERSONAL DETAILS</b>")
            TRItemHead1.BackColor = Drawing.Color.WhiteSmoke
            tb.Controls.Add(TRItemHead1)
            RH.BlankRow(tb, 5)

            Dim TR2 As New TableRow
            'TR2.BackColor = Drawing.Color.WhiteSmoke
            Dim TR2_01, TR2_02, TR2_03, TR2_04, TR2_05, TR2_06, TR2_07, TR2_08 As New TableCell
            TR2_01.BackColor = Drawing.Color.WhiteSmoke
            TR2_07.BackColor = Drawing.Color.WhiteSmoke
            TR2_04.BackColor = Drawing.Color.WhiteSmoke
            TR2_01.BorderWidth = "1"
            TR2_02.BorderWidth = "1"
            TR2_04.BorderWidth = "1"
            TR2_05.BorderWidth = "1"
            TR2_07.BorderWidth = "1"
            TR2_08.BorderWidth = "1"

            TR2_01.BorderColor = Drawing.Color.Silver
            TR2_02.BorderColor = Drawing.Color.Silver
            TR2_04.BorderColor = Drawing.Color.Silver
            TR2_05.BorderColor = Drawing.Color.Silver
            TR2_07.BorderColor = Drawing.Color.Silver
            TR2_08.BorderColor = Drawing.Color.Silver



            TR2_01.BorderStyle = BorderStyle.Solid
            TR2_02.BorderStyle = BorderStyle.Solid
            TR2_04.BorderStyle = BorderStyle.Solid
            TR2_05.BorderStyle = BorderStyle.Solid
            TR2_07.BorderStyle = BorderStyle.Solid
            TR2_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR2, TR2_01, 16, 16, "l", "Date Of Birth")
            RH.AddColumn(TR2, TR2_02, 16, 16, "l", DT.Rows(0)(7))
            RH.AddColumn(TR2, TR2_03, 2, 2, "c", "")
            RH.AddColumn(TR2, TR2_04, 16, 16, "l", "Marital Status")
            RH.AddColumn(TR2, TR2_05, 16, 16, "l", DT.Rows(0)(17))
            RH.AddColumn(TR2, TR2_06, 2, 2, "l", "")
            RH.AddColumn(TR2, TR2_07, 16, 16, "l", "Gender")
            RH.AddColumn(TR2, TR2_08, 16, 16, "l", DT.Rows(0)(9))


            tb.Controls.Add(TR2)


            Dim TR3 As New TableRow
            'TR3.BackColor = Drawing.Color.WhiteSmoke
            Dim TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08 As New TableCell

            TR3_01.BackColor = Drawing.Color.WhiteSmoke
            TR3_07.BackColor = Drawing.Color.WhiteSmoke
            TR3_04.BackColor = Drawing.Color.WhiteSmoke
            TR3_01.BorderWidth = "1"
            TR3_02.BorderWidth = "1"
            TR3_04.BorderWidth = "1"
            TR3_05.BorderWidth = "1"
            TR3_07.BorderWidth = "1"
            TR3_08.BorderWidth = "1"

            TR3_01.BorderColor = Drawing.Color.Silver
            TR3_02.BorderColor = Drawing.Color.Silver
            TR3_04.BorderColor = Drawing.Color.Silver
            TR3_05.BorderColor = Drawing.Color.Silver
            TR3_07.BorderColor = Drawing.Color.Silver
            TR3_08.BorderColor = Drawing.Color.Silver



            TR3_01.BorderStyle = BorderStyle.Solid
            TR3_02.BorderStyle = BorderStyle.Solid
            TR3_04.BorderStyle = BorderStyle.Solid
            TR3_05.BorderStyle = BorderStyle.Solid
            TR3_07.BorderStyle = BorderStyle.Solid
            TR3_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR3, TR3_01, 16, 16, "l", "Contact Person")
            RH.AddColumn(TR3, TR3_02, 16, 16, "l", DT.Rows(0)(13))
            RH.AddColumn(TR3, TR3_03, 2, 2, "c", "")
            RH.AddColumn(TR3, TR3_04, 16, 16, "l", "Contact No")
            RH.AddColumn(TR3, TR3_05, 16, 16, "l", DT.Rows(0)(14))
            RH.AddColumn(TR3, TR3_06, 2, 2, "l", "")
            RH.AddColumn(TR3, TR3_07, 16, 16, "l", "House Name")
            RH.AddColumn(TR3, TR3_08, 16, 16, "l", DT.Rows(0)(11))


            tb.Controls.Add(TR3)
            Dim TR4 As New TableRow
            'TR4.BackColor = Drawing.Color.WhiteSmoke
            Dim TR4_01, TR4_02, TR4_03, TR4_04, TR4_05, TR4_06, TR4_07, TR4_08 As New TableCell

            TR4_01.BackColor = Drawing.Color.WhiteSmoke
            TR4_07.BackColor = Drawing.Color.WhiteSmoke
            TR4_04.BackColor = Drawing.Color.WhiteSmoke
            TR4_01.BorderWidth = "1"
            TR4_02.BorderWidth = "1"
            TR4_04.BorderWidth = "1"
            TR4_05.BorderWidth = "1"
            TR4_07.BorderWidth = "1"
            TR4_08.BorderWidth = "1"

            TR4_01.BorderColor = Drawing.Color.Silver
            TR4_02.BorderColor = Drawing.Color.Silver
            TR4_04.BorderColor = Drawing.Color.Silver
            TR4_05.BorderColor = Drawing.Color.Silver
            TR4_07.BorderColor = Drawing.Color.Silver
            TR4_08.BorderColor = Drawing.Color.Silver



            TR4_01.BorderStyle = BorderStyle.Solid
            TR4_02.BorderStyle = BorderStyle.Solid
            TR4_04.BorderStyle = BorderStyle.Solid
            TR4_05.BorderStyle = BorderStyle.Solid
            TR4_07.BorderStyle = BorderStyle.Solid
            TR4_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR4, TR4_01, 16, 16, "l", "Location")
            RH.AddColumn(TR4, TR4_02, 16, 16, "l", DT.Rows(0)(16))
            RH.AddColumn(TR4, TR4_03, 2, 2, "c", "")
            RH.AddColumn(TR4, TR4_04, 16, 16, "l", "City")
            RH.AddColumn(TR4, TR4_05, 16, 16, "l", DT.Rows(0)(15))
            RH.AddColumn(TR4, TR4_06, 2, 2, "l", "")
            RH.AddColumn(TR4, TR4_07, 16, 16, "l", "State")
            RH.AddColumn(TR4, TR4_08, 16, 16, "l", DT.Rows(0)(33))


            tb.Controls.Add(TR4)

            Dim TR5 As New TableRow
            'TR5.BackColor = Drawing.Color.WhiteSmoke
            Dim TR5_01, TR5_02, TR5_03, TR5_04, TR5_05, TR5_06, TR5_07, TR5_08 As New TableCell

            TR5_01.BackColor = Drawing.Color.WhiteSmoke
            TR5_07.BackColor = Drawing.Color.WhiteSmoke
            TR5_04.BackColor = Drawing.Color.WhiteSmoke
            TR5_01.BorderWidth = "1"
            TR5_02.BorderWidth = "1"
            TR5_04.BorderWidth = "1"
            TR5_05.BorderWidth = "1"
            TR5_07.BorderWidth = "1"
            TR5_08.BorderWidth = "1"

            TR5_01.BorderColor = Drawing.Color.Silver
            TR5_02.BorderColor = Drawing.Color.Silver
            TR5_04.BorderColor = Drawing.Color.Silver
            TR5_05.BorderColor = Drawing.Color.Silver
            TR5_07.BorderColor = Drawing.Color.Silver
            TR5_08.BorderColor = Drawing.Color.Silver



            TR5_01.BorderStyle = BorderStyle.Solid
            TR5_02.BorderStyle = BorderStyle.Solid
            TR5_04.BorderStyle = BorderStyle.Solid
            TR5_05.BorderStyle = BorderStyle.Solid
            TR5_07.BorderStyle = BorderStyle.Solid
            TR5_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR5, TR5_01, 16, 16, "l", "District")
            RH.AddColumn(TR5, TR5_02, 16, 16, "l", DT.Rows(0)(32))
            RH.AddColumn(TR5, TR5_03, 2, 2, "c", "")
            RH.AddColumn(TR5, TR5_04, 16, 16, "l", "Post & Pin")
            RH.AddColumn(TR5, TR5_05, 16, 16, "l", DT.Rows(0)(20) + "-" + DT.Rows(0)(19).ToString)
            RH.AddColumn(TR5, TR5_06, 2, 2, "l", "")
            RH.AddColumn(TR5, TR5_07, 16, 16, "l", "E-mail")
            RH.AddColumn(TR5, TR5_08, 16, 16, "l", DT.Rows(0)(8))


            tb.Controls.Add(TR5)
            Dim TR6 As New TableRow
            'TR6.BackColor = Drawing.Color.WhiteSmoke
            Dim TR6_01, TR6_02, TR6_03, TR6_04, TR6_05, TR6_06, TR6_07, TR6_08 As New TableCell

            TR6_01.BackColor = Drawing.Color.WhiteSmoke
            TR6_07.BackColor = Drawing.Color.WhiteSmoke
            TR6_04.BackColor = Drawing.Color.WhiteSmoke
            TR6_01.BorderWidth = "1"
            TR6_02.BorderWidth = "1"
            TR6_04.BorderWidth = "1"
            TR6_05.BorderWidth = "1"
            TR6_07.BorderWidth = "1"
            TR6_08.BorderWidth = "1"

            TR6_01.BorderColor = Drawing.Color.Silver
            TR6_02.BorderColor = Drawing.Color.Silver
            TR6_04.BorderColor = Drawing.Color.Silver
            TR6_05.BorderColor = Drawing.Color.Silver
            TR6_07.BorderColor = Drawing.Color.Silver
            TR6_08.BorderColor = Drawing.Color.Silver



            TR6_01.BorderStyle = BorderStyle.Solid
            TR6_02.BorderStyle = BorderStyle.Solid
            TR6_04.BorderStyle = BorderStyle.Solid
            TR6_05.BorderStyle = BorderStyle.Solid
            TR6_07.BorderStyle = BorderStyle.Solid
            TR6_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR6, TR6_01, 16, 16, "l", "Land Line")
            RH.AddColumn(TR6, TR6_02, 16, 16, "l", DT.Rows(0)(12))
            RH.AddColumn(TR6, TR6_03, 2, 2, "c", "")
            RH.AddColumn(TR6, TR6_04, 16, 16, "l", "Mobile")
            RH.AddColumn(TR6, TR6_05, 16, 16, "l", DT.Rows(0)(10))
            RH.AddColumn(TR6, TR6_06, 2, 2, "l", "")
            RH.AddColumn(TR6, TR6_07, 16, 16, "l", "Fathers Name")
            RH.AddColumn(TR6, TR6_08, 16, 16, "l", DT.Rows(0)(39))


            tb.Controls.Add(TR6)

            Dim TR7 As New TableRow
            'TR7.BackColor = Drawing.Color.WhiteSmoke
            Dim TR7_01, TR7_02, TR7_03, TR7_04, TR7_05, TR7_06, TR7_07, TR7_08 As New TableCell

            TR7_01.BackColor = Drawing.Color.WhiteSmoke
            TR7_07.BackColor = Drawing.Color.WhiteSmoke
            TR7_04.BackColor = Drawing.Color.WhiteSmoke
            TR7_01.BorderWidth = "1"
            TR7_02.BorderWidth = "1"
            TR7_04.BorderWidth = "1"
            TR7_05.BorderWidth = "1"
            TR7_07.BorderWidth = "1"
            TR7_08.BorderWidth = "1"

            TR7_01.BorderColor = Drawing.Color.Silver
            TR7_02.BorderColor = Drawing.Color.Silver
            TR7_04.BorderColor = Drawing.Color.Silver
            TR7_05.BorderColor = Drawing.Color.Silver
            TR7_07.BorderColor = Drawing.Color.Silver
            TR7_08.BorderColor = Drawing.Color.Silver



            TR7_01.BorderStyle = BorderStyle.Solid
            TR7_02.BorderStyle = BorderStyle.Solid
            TR7_04.BorderStyle = BorderStyle.Solid
            TR7_05.BorderStyle = BorderStyle.Solid
            TR7_07.BorderStyle = BorderStyle.Solid
            TR7_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR7, TR7_01, 16, 16, "l", "Mothers Name")
            RH.AddColumn(TR7, TR7_02, 16, 16, "l", DT.Rows(0)(40))
            RH.AddColumn(TR7, TR7_03, 2, 2, "c", "")
            RH.AddColumn(TR7, TR7_04, 16, 16, "l", "Religion")
            RH.AddColumn(TR7, TR7_05, 16, 16, "l", DT.Rows(0)(56))
            RH.AddColumn(TR7, TR7_06, 2, 2, "l", "")
            RH.AddColumn(TR7, TR7_07, 16, 16, "l", "Caste")
            RH.AddColumn(TR7, TR7_08, 16, 16, "l", DT.Rows(0)(18))


            tb.Controls.Add(TR7)

            Dim TR8 As New TableRow
            'TR7.BackColor = Drawing.Color.WhiteSmoke
            Dim TR8_01, TR8_02, TR8_03, TR8_04, TR8_05, TR8_06, TR8_07, TR8_08 As New TableCell

            TR8_01.BackColor = Drawing.Color.WhiteSmoke
            TR8_07.BackColor = Drawing.Color.WhiteSmoke
            TR8_04.BackColor = Drawing.Color.WhiteSmoke
            TR8_01.BorderWidth = "1"
            TR8_02.BorderWidth = "1"
            TR8_04.BorderWidth = "1"
            TR8_05.BorderWidth = "1"
            TR8_07.BorderWidth = "1"
            TR8_08.BorderWidth = "1"

            TR8_01.BorderColor = Drawing.Color.Silver
            TR8_02.BorderColor = Drawing.Color.Silver
            TR8_04.BorderColor = Drawing.Color.Silver
            TR8_05.BorderColor = Drawing.Color.Silver
            TR8_07.BorderColor = Drawing.Color.Silver
            TR8_08.BorderColor = Drawing.Color.Silver



            TR8_01.BorderStyle = BorderStyle.Solid
            TR8_02.BorderStyle = BorderStyle.Solid
            TR8_04.BorderStyle = BorderStyle.Solid
            TR8_05.BorderStyle = BorderStyle.Solid
            TR8_07.BorderStyle = BorderStyle.Solid
            TR8_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR8, TR8_01, 16, 16, "l", "Blood Group")
            RH.AddColumn(TR8, TR8_02, 16, 16, "l", DT.Rows(0)(41))
            RH.AddColumn(TR8, TR8_03, 2, 2, "c", "")
            RH.AddColumn(TR8, TR8_04, 16, 16, "l", "Marriage Date")
            RH.AddColumn(TR8, TR8_05, 16, 16, "l", DT.Rows(0)(43))
            RH.AddColumn(TR8, TR8_06, 2, 2, "l", "")
            RH.AddColumn(TR8, TR8_07, 16, 16, "l", "Two Wheeler Status")
            RH.AddColumn(TR8, TR8_08, 16, 16, "l", DT.Rows(0)(44))


            tb.Controls.Add(TR8)
            Dim TR9 As New TableRow
            'TR7.BackColor = Drawing.Color.WhiteSmoke
            Dim TR9_01, TR9_02, TR9_03, TR9_04, TR9_05, TR9_06, TR9_07, TR9_08 As New TableCell

            TR9_01.BackColor = Drawing.Color.WhiteSmoke
            TR9_07.BackColor = Drawing.Color.WhiteSmoke
            TR9_04.BackColor = Drawing.Color.WhiteSmoke
            TR9_01.BorderWidth = "1"
            TR9_02.BorderWidth = "1"
            TR9_04.BorderWidth = "1"
            TR9_05.BorderWidth = "1"
            TR9_07.BorderWidth = "1"
            TR9_08.BorderWidth = "1"

            TR9_01.BorderColor = Drawing.Color.Silver
            TR9_02.BorderColor = Drawing.Color.Silver
            TR9_04.BorderColor = Drawing.Color.Silver
            TR9_05.BorderColor = Drawing.Color.Silver
            TR9_07.BorderColor = Drawing.Color.Silver
            TR9_08.BorderColor = Drawing.Color.Silver



            TR9_01.BorderStyle = BorderStyle.Solid
            TR9_02.BorderStyle = BorderStyle.Solid
            TR9_04.BorderStyle = BorderStyle.Solid
            TR9_05.BorderStyle = BorderStyle.Solid
            TR9_07.BorderStyle = BorderStyle.Solid
            TR9_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR9, TR9_01, 16, 16, "l", "Driving Liscence No")
            RH.AddColumn(TR9, TR9_02, 16, 16, "l", DT.Rows(0)(45))
            RH.AddColumn(TR9, TR9_03, 2, 2, "c", "")
            RH.AddColumn(TR9, TR9_04, 16, 16, "l", "CUG No")
            RH.AddColumn(TR9, TR9_05, 16, 16, "l", DT.Rows(0)(46))
            RH.AddColumn(TR9, TR9_06, 2, 2, "l", "")
            RH.AddColumn(TR9, TR9_07, 16, 16, "l", "Official e-Mail")
            RH.AddColumn(TR9, TR9_08, 16, 16, "l", DT.Rows(0)(47))


            tb.Controls.Add(TR9)
            Dim TR10 As New TableRow
            'TR7.BackColor = Drawing.Color.WhiteSmoke
            Dim TR10_01, TR10_02, TR10_03, TR10_04, TR10_05, TR10_06, TR10_07, TR10_08 As New TableCell

            TR10_01.BackColor = Drawing.Color.WhiteSmoke
            TR10_07.BackColor = Drawing.Color.WhiteSmoke
            TR10_04.BackColor = Drawing.Color.WhiteSmoke
            TR10_01.BorderWidth = "1"
            TR10_02.BorderWidth = "1"
            TR10_04.BorderWidth = "1"
            TR10_05.BorderWidth = "1"
            TR10_07.BorderWidth = "1"
            TR10_08.BorderWidth = "1"

            TR10_01.BorderColor = Drawing.Color.Silver
            TR10_02.BorderColor = Drawing.Color.Silver
            TR10_04.BorderColor = Drawing.Color.Silver
            TR10_05.BorderColor = Drawing.Color.Silver
            TR10_07.BorderColor = Drawing.Color.Silver
            TR10_08.BorderColor = Drawing.Color.Silver



            TR10_01.BorderStyle = BorderStyle.Solid
            TR10_02.BorderStyle = BorderStyle.Solid
            TR10_04.BorderStyle = BorderStyle.Solid
            TR10_05.BorderStyle = BorderStyle.Solid
            TR10_07.BorderStyle = BorderStyle.Solid
            TR10_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR10, TR10_01, 16, 16, "l", "Panchayath Ref. Letter")
            RH.AddColumn(TR10, TR10_02, 16, 16, "l", DT.Rows(0)(48))
            RH.AddColumn(TR10, TR10_03, 2, 2, "c", "")
            RH.AddColumn(TR10, TR10_04, 16, 16, "l", "Community Ref. Letter")
            RH.AddColumn(TR10, TR10_05, 16, 16, "l", DT.Rows(0)(49))
            RH.AddColumn(TR10, TR10_06, 2, 2, "l", "")
            RH.AddColumn(TR10, TR10_07, 16, 16, "l", "Police Ref. Letter")
            RH.AddColumn(TR10, TR10_08, 16, 16, "l", DT.Rows(0)(50))


            tb.Controls.Add(TR10)
            Dim TR11 As New TableRow
            'TR7.BackColor = Drawing.Color.WhiteSmoke
            Dim TR11_01, TR11_02, TR11_03, TR11_04, TR11_05, TR11_06, TR11_07, TR11_08 As New TableCell

            TR11_01.BackColor = Drawing.Color.WhiteSmoke
            TR11_07.BackColor = Drawing.Color.WhiteSmoke
            TR11_04.BackColor = Drawing.Color.WhiteSmoke
            TR11_01.BorderWidth = "1"
            TR11_02.BorderWidth = "1"
            TR11_05.BorderWidth = "1"
            TR11_07.BorderWidth = "1"
            TR11_08.BorderWidth = "1"
            TR11_04.BorderWidth = "1"

            TR11_01.BorderColor = Drawing.Color.Silver
            TR11_02.BorderColor = Drawing.Color.Silver
            TR11_05.BorderColor = Drawing.Color.Silver
            TR11_07.BorderColor = Drawing.Color.Silver
            TR11_08.BorderColor = Drawing.Color.Silver
            TR11_04.BorderColor = Drawing.Color.Silver
            TR11_05.BorderColor = Drawing.Color.Silver


            TR11_01.BorderStyle = BorderStyle.Solid
            TR11_02.BorderStyle = BorderStyle.Solid
            TR11_05.BorderStyle = BorderStyle.Solid
            TR11_07.BorderStyle = BorderStyle.Solid
            TR11_08.BorderStyle = BorderStyle.Solid
            TR11_04.BorderStyle = BorderStyle.Solid
            TR11_05.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TR11, TR11_01, 16, 16, "l", "Whether the staff was earlier our Sangam member or not? ")
            RH.AddColumn(TR11, TR11_02, 16, 16, "l", DT.Rows(0)(51))
            RH.AddColumn(TR11, TR11_03, 2, 2, "c", "")
            RH.AddColumn(TR11, TR11_07, 16, 16, "l", "Languages Known")
            RH.AddColumn(TR11, TR11_08, 16, 16, "l", LangVal)
            RH.AddColumn(TR11, TR11_06, 2, 2, "l", "")
            RH.AddColumn(TR11, TR11_04, 16, 16, "l", "Mother Tongue")
            RH.AddColumn(TR11, TR11_05, 16, 16, "l", DT.Rows(0)(42))


            tb.Controls.Add(TR11)
            Dim TRItemHead3 As New TableRow
            TRItemHead3.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead3_00 As New TableCell
            RH.BlankRow(tb, 5)
            RH.AddColumn(TRItemHead3, TRItemHead3_00, 100, 100, "c", "<b>QUALIFICATION DETAILS</b>")
            TRItemHead3.BackColor = Drawing.Color.WhiteSmoke
            tb.Controls.Add(TRItemHead3)
            RH.BlankRow(tb, 5)

            Dim TR33 As New TableRow
            TR33.BackColor = Drawing.Color.WhiteSmoke
            Dim TR33_01, TR33_02, TR33_03, TR33_04, TR33_05, TR33_06, TR33_07 As New TableCell

            TR33_01.BorderWidth = "1"
            TR33_02.BorderWidth = "1"
            TR33_03.BorderWidth = "1"
            TR33_04.BorderWidth = "1"
            TR33_05.BorderWidth = "1"
            TR33_06.BorderWidth = "1"
            TR33_07.BorderWidth = "1"



            TR33_01.BorderColor = Drawing.Color.Silver
            TR33_02.BorderColor = Drawing.Color.Silver
            TR33_03.BorderColor = Drawing.Color.Silver
            TR33_04.BorderColor = Drawing.Color.Silver
            TR33_05.BorderColor = Drawing.Color.Silver
            TR33_06.BorderColor = Drawing.Color.Silver
            TR33_07.BorderColor = Drawing.Color.Silver



            TR33_01.BorderStyle = BorderStyle.Solid
            TR33_02.BorderStyle = BorderStyle.Solid
            TR33_03.BorderStyle = BorderStyle.Solid
            TR33_04.BorderStyle = BorderStyle.Solid
            TR33_05.BorderStyle = BorderStyle.Solid
            TR33_06.BorderStyle = BorderStyle.Solid
            TR33_07.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR33, TR33_01, 27, 27, "l", "Level")
            RH.AddColumn(TR33, TR33_02, 32, 32, "l", "Qualification")
            RH.AddColumn(TR33, TR33_03, 24, 24, "l", "University")
            RH.AddColumn(TR33, TR33_04, 17, 17, "l", "Year of Passing")



            tb.Controls.Add(TR33)

            For Each DR In DT_Quali.Rows
                Dim TR44 As New TableRow
                TR44.BorderWidth = "1"
                TR44.BorderStyle = BorderStyle.Solid
                Dim TR44_01, TR44_02, TR44_03, TR44_04 As New TableCell

                TR44_01.BorderWidth = "1"
                TR44_02.BorderWidth = "1"
                TR44_03.BorderWidth = "1"
                TR44_04.BorderWidth = "1"




                TR44_01.BorderColor = Drawing.Color.Silver
                TR44_02.BorderColor = Drawing.Color.Silver
                TR44_03.BorderColor = Drawing.Color.Silver
                TR44_04.BorderColor = Drawing.Color.Silver




                TR44_01.BorderStyle = BorderStyle.Solid
                TR44_02.BorderStyle = BorderStyle.Solid
                TR44_03.BorderStyle = BorderStyle.Solid
                TR44_04.BorderStyle = BorderStyle.Solid


                RH.AddColumn(TR44, TR44_01, 27, 27, "l", DR(0))
                RH.AddColumn(TR44, TR44_02, 32, 32, "l", DR(1))
                RH.AddColumn(TR44, TR44_03, 24, 24, "l", DR(2))
                RH.AddColumn(TR44, TR44_04, 17, 17, "l", DR(3))



                tb.Controls.Add(TR44)

            Next

            Dim TRItemHead4 As New TableRow
            TRItemHead4.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead4_00 As New TableCell
            RH.BlankRow(tb, 5)
            RH.AddColumn(TRItemHead4, TRItemHead4_00, 100, 100, "c", "<b>EXPERIENCE DETAILS</b>")
            TRItemHead4.BackColor = Drawing.Color.WhiteSmoke
            tb.Controls.Add(TRItemHead4)
            RH.BlankRow(tb, 5)

            Dim TR43 As New TableRow
            TR43.BackColor = Drawing.Color.WhiteSmoke
            Dim TR43_01, TR43_02, TR43_03, TR43_04 As New TableCell

            TR43_01.BorderWidth = "1"
            TR43_02.BorderWidth = "1"
            TR43_03.BorderWidth = "1"
            TR43_04.BorderWidth = "1"




            TR43_01.BorderColor = Drawing.Color.Silver
            TR43_02.BorderColor = Drawing.Color.Silver
            TR43_03.BorderColor = Drawing.Color.Silver
            TR43_04.BorderColor = Drawing.Color.Silver




            TR43_01.BorderStyle = BorderStyle.Solid
            TR43_02.BorderStyle = BorderStyle.Solid
            TR43_03.BorderStyle = BorderStyle.Solid
            TR43_04.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TR43, TR43_01, 27, 27, "l", "Company")
            RH.AddColumn(TR43, TR43_02, 32, 32, "l", "Address")
            RH.AddColumn(TR43, TR43_03, 24, 24, "l", "Contact No")
            RH.AddColumn(TR43, TR43_04, 17, 17, "l", "No of Years")



            tb.Controls.Add(TR43)

            For Each DR In DT_Exp.Rows
                Dim TR44 As New TableRow
                TR44.BorderWidth = "1"
                TR44.BorderStyle = BorderStyle.Solid
                Dim TR44_01, TR44_02, TR44_03, TR44_04 As New TableCell

                TR44_01.BorderWidth = "1"
                TR44_02.BorderWidth = "1"
                TR44_03.BorderWidth = "1"
                TR44_04.BorderWidth = "1"




                TR44_01.BorderColor = Drawing.Color.Silver
                TR44_02.BorderColor = Drawing.Color.Silver
                TR44_03.BorderColor = Drawing.Color.Silver
                TR44_04.BorderColor = Drawing.Color.Silver




                TR44_01.BorderStyle = BorderStyle.Solid
                TR44_02.BorderStyle = BorderStyle.Solid
                TR44_03.BorderStyle = BorderStyle.Solid
                TR44_04.BorderStyle = BorderStyle.Solid


                RH.AddColumn(TR44, TR44_01, 27, 27, "l", DR(0))
                RH.AddColumn(TR44, TR44_02, 32, 32, "l", DR(1))
                RH.AddColumn(TR44, TR44_03, 24, 24, "l", DR(2))
                RH.AddColumn(TR44, TR44_04, 17, 17, "l", DR(3))



                tb.Controls.Add(TR44)

            Next



            Dim TRItemHead6 As New TableRow
            TRItemHead6.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead6_00 As New TableCell
            RH.BlankRow(tb, 5)
            RH.AddColumn(TRItemHead6, TRItemHead6_00, 100, 100, "c", "<b>ADDRESS PROOF DETAILS</b>")
            TRItemHead6.BackColor = Drawing.Color.WhiteSmoke
            tb.Controls.Add(TRItemHead6)
            RH.BlankRow(tb, 5)

            Dim TR42 As New TableRow
            TR42.BackColor = Drawing.Color.WhiteSmoke
            Dim TR42_01, TR42_02 As New TableCell

            TR42_01.BorderWidth = "1"
            TR42_02.BorderWidth = "1"

            TR42_01.BorderColor = Drawing.Color.Silver
            TR42_02.BorderColor = Drawing.Color.Silver


            TR42_01.BorderStyle = BorderStyle.Solid
            TR42_02.BorderStyle = BorderStyle.Solid



            RH.AddColumn(TR42, TR42_01, 51, 51, "l", "Address Proof Type")
            RH.AddColumn(TR42, TR42_02, 49, 49, "l", "Address Proof Number")


            tb.Controls.Add(TR42)

            For Each DR In DT_ADDR.Rows
                Dim TR44 As New TableRow
                TR44.BorderWidth = "1"
                TR44.BorderStyle = BorderStyle.Solid
                Dim TR44_01, TR44_02 As New TableCell

                TR44_01.BorderWidth = "1"
                TR44_02.BorderWidth = "1"



                TR44_01.BorderColor = Drawing.Color.Silver
                TR44_02.BorderColor = Drawing.Color.Silver


                TR44_01.BorderStyle = BorderStyle.Solid
                TR44_02.BorderStyle = BorderStyle.Solid



                RH.AddColumn(TR44, TR44_01, 51, 51, "l", DR(0))
                RH.AddColumn(TR44, TR44_02, 49, 49, "l", DR(1))




                tb.Controls.Add(TR44)

            Next
            Dim TRItemHead9 As New TableRow
            TRItemHead9.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead9_00 As New TableCell
            RH.BlankRow(tb, 5)
            RH.AddColumn(TRItemHead9, TRItemHead6_00, 100, 100, "c", "<b>ID PROOF DETAILS</b>")
            TRItemHead9.BackColor = Drawing.Color.WhiteSmoke
            tb.Controls.Add(TRItemHead9)
            RH.BlankRow(tb, 5)

            Dim TR52 As New TableRow
            TR52.BackColor = Drawing.Color.WhiteSmoke
            Dim TR52_01, TR52_02 As New TableCell

            TR52_01.BorderWidth = "1"
            TR52_02.BorderWidth = "1"

            TR52_01.BorderColor = Drawing.Color.Silver
            TR52_02.BorderColor = Drawing.Color.Silver


            TR52_01.BorderStyle = BorderStyle.Solid
            TR52_02.BorderStyle = BorderStyle.Solid



            RH.AddColumn(TR52, TR52_01, 51, 51, "l", "Address Proof Type")
            RH.AddColumn(TR52, TR52_02, 49, 49, "l", "Address Proof Number")


            tb.Controls.Add(TR52)

            For Each DR In DT_ID.Rows
                Dim TR44 As New TableRow
                TR44.BorderWidth = "1"
                TR44.BorderStyle = BorderStyle.Solid
                Dim TR44_01, TR44_02 As New TableCell

                TR44_01.BorderWidth = "1"
                TR44_02.BorderWidth = "1"



                TR44_01.BorderColor = Drawing.Color.Silver
                TR44_02.BorderColor = Drawing.Color.Silver


                TR44_01.BorderStyle = BorderStyle.Solid
                TR44_02.BorderStyle = BorderStyle.Solid



                RH.AddColumn(TR44, TR44_01, 51, 51, "l", DR(0))
                RH.AddColumn(TR44, TR44_02, 49, 49, "l", DR(1))




                tb.Controls.Add(TR44)

            Next
            Dim TRItemHead7 As New TableRow
            TRItemHead7.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead7_00 As New TableCell
            RH.BlankRow(tb, 5)
            RH.AddColumn(TRItemHead7, TRItemHead7_00, 100, 100, "c", "<b>FAMILY DETAILS</b>")
            TRItemHead7.BackColor = Drawing.Color.WhiteSmoke
            tb.Controls.Add(TRItemHead7)
            RH.BlankRow(tb, 5)

            Dim TR53 As New TableRow
            TR53.BackColor = Drawing.Color.WhiteSmoke
            Dim TR53_01, TR53_02, TR53_03, TR53_04 As New TableCell

            TR53_01.BorderWidth = "1"
            TR53_02.BorderWidth = "1"
            TR53_03.BorderWidth = "1"
            TR53_04.BorderWidth = "1"




            TR53_01.BorderColor = Drawing.Color.Silver
            TR53_02.BorderColor = Drawing.Color.Silver
            TR53_03.BorderColor = Drawing.Color.Silver
            TR53_04.BorderColor = Drawing.Color.Silver




            TR53_01.BorderStyle = BorderStyle.Solid
            TR53_02.BorderStyle = BorderStyle.Solid
            TR53_03.BorderStyle = BorderStyle.Solid
            TR53_04.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TR53, TR53_01, 27, 27, "l", "Family Member")
            RH.AddColumn(TR53, TR53_02, 32, 32, "l", "Name")
            RH.AddColumn(TR53, TR53_03, 24, 24, "l", "Age")
            RH.AddColumn(TR53, TR53_04, 17, 17, "l", "Occupation")



            tb.Controls.Add(TR53)

            For Each DR In DT_FAM.Rows
                Dim TR44 As New TableRow
                TR44.BorderWidth = "1"
                TR44.BorderStyle = BorderStyle.Solid
                Dim TR44_01, TR44_02, TR44_03, TR44_04 As New TableCell

                TR44_01.BorderWidth = "1"
                TR44_02.BorderWidth = "1"
                TR44_03.BorderWidth = "1"
                TR44_04.BorderWidth = "1"




                TR44_01.BorderColor = Drawing.Color.Silver
                TR44_02.BorderColor = Drawing.Color.Silver
                TR44_03.BorderColor = Drawing.Color.Silver
                TR44_04.BorderColor = Drawing.Color.Silver




                TR44_01.BorderStyle = BorderStyle.Solid
                TR44_02.BorderStyle = BorderStyle.Solid
                TR44_03.BorderStyle = BorderStyle.Solid
                TR44_04.BorderStyle = BorderStyle.Solid


                RH.AddColumn(TR44, TR44_01, 27, 27, "l", DR(0))
                RH.AddColumn(TR44, TR44_02, 32, 32, "l", DR(1))
                RH.AddColumn(TR44, TR44_03, 24, 24, "l", DR(2))
                RH.AddColumn(TR44, TR44_04, 17, 17, "l", DR(3))



                tb.Controls.Add(TR44)

            Next
            Dim TRItemHead5 As New TableRow
            TRItemHead5.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead5_00 As New TableCell
            RH.BlankRow(tb, 5)
            RH.AddColumn(TRItemHead5, TRItemHead5_00, 100, 100, "c", "<b>SALARY DETAILS</b>")
            TRItemHead5.BackColor = Drawing.Color.WhiteSmoke
            tb.Controls.Add(TRItemHead5)
            RH.BlankRow(tb, 5)

            Dim TR18 As New TableRow
            'TR18.BackColor = Drawing.Color.WhiteSmoke
            Dim TR18_01, TR18_02, TR18_03, TR18_04, TR18_05, TR18_06, TR18_07, TR18_08 As New TableCell

            TR18_01.BackColor = Drawing.Color.WhiteSmoke
            TR18_07.BackColor = Drawing.Color.WhiteSmoke
            TR18_04.BackColor = Drawing.Color.WhiteSmoke
            TR18_01.BorderWidth = "1"
            TR18_02.BorderWidth = "1"
            TR18_04.BorderWidth = "1"
            TR18_05.BorderWidth = "1"
            TR18_07.BorderWidth = "1"
            TR18_08.BorderWidth = "1"

            TR18_01.BorderColor = Drawing.Color.Silver
            TR18_02.BorderColor = Drawing.Color.Silver
            TR18_04.BorderColor = Drawing.Color.Silver
            TR18_05.BorderColor = Drawing.Color.Silver
            TR18_07.BorderColor = Drawing.Color.Silver
            TR18_08.BorderColor = Drawing.Color.Silver



            TR18_01.BorderStyle = BorderStyle.Solid
            TR18_02.BorderStyle = BorderStyle.Solid
            TR18_04.BorderStyle = BorderStyle.Solid
            TR18_05.BorderStyle = BorderStyle.Solid
            TR18_07.BorderStyle = BorderStyle.Solid
            TR18_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR18, TR18_01, 16, 16, "l", "Basic Pay")
            RH.AddColumn(TR18, TR18_02, 16, 16, "r", DT.Rows(0)(21))
            RH.AddColumn(TR18, TR18_03, 2, 2, "c", "")
            RH.AddColumn(TR18, TR18_04, 16, 16, "l", "DA")
            RH.AddColumn(TR18, TR18_05, 16, 16, "r", DT.Rows(0)(22))
            RH.AddColumn(TR18, TR18_06, 2, 2, "l", "")
            RH.AddColumn(TR18, TR18_07, 16, 16, "l", "ESI")
            RH.AddColumn(TR18, TR18_08, 16, 16, "r", ESI)


            tb.Controls.Add(TR18)


            Dim TR19 As New TableRow
            'TR19.BackColor = Drawing.Color.WhiteSmoke
            Dim TR19_01, TR19_02, TR19_03, TR19_04, TR19_05, TR19_06, TR19_07, TR19_08 As New TableCell

            TR19_01.BackColor = Drawing.Color.WhiteSmoke
            TR19_07.BackColor = Drawing.Color.WhiteSmoke
            TR19_04.BackColor = Drawing.Color.WhiteSmoke
            TR19_01.BorderWidth = "1"
            TR19_02.BorderWidth = "1"
            TR19_04.BorderWidth = "1"
            TR19_05.BorderWidth = "1"
            TR19_07.BorderWidth = "1"
            TR19_08.BorderWidth = "1"

            TR19_01.BorderColor = Drawing.Color.Silver
            TR19_02.BorderColor = Drawing.Color.Silver
            TR19_04.BorderColor = Drawing.Color.Silver
            TR19_05.BorderColor = Drawing.Color.Silver
            TR19_07.BorderColor = Drawing.Color.Silver
            TR19_08.BorderColor = Drawing.Color.Silver



            TR19_01.BorderStyle = BorderStyle.Solid
            TR19_02.BorderStyle = BorderStyle.Solid
            TR19_04.BorderStyle = BorderStyle.Solid
            TR19_05.BorderStyle = BorderStyle.Solid
            TR19_07.BorderStyle = BorderStyle.Solid
            TR19_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR19, TR19_01, 16, 16, "l", "Conveyance")
            RH.AddColumn(TR19, TR19_02, 16, 16, "r", DT.Rows(0)(24))
            RH.AddColumn(TR19, TR19_03, 2, 2, "c", "")
            RH.AddColumn(TR19, TR19_04, 16, 16, "l", "Special Allwnce")
            RH.AddColumn(TR19, TR19_05, 16, 16, "r", DT.Rows(0)(25))
            RH.AddColumn(TR19, TR19_06, 2, 2, "l", "")
            RH.AddColumn(TR19, TR19_07, 16, 16, "l", "PF")
            RH.AddColumn(TR19, TR19_08, 16, 16, "r", PF)


            tb.Controls.Add(TR19)

            Dim TR20 As New TableRow
            'TR20.BackColor = Drawing.Color.WhiteSmoke
            Dim TR20_01, TR20_02, TR20_03, TR20_04, TR20_05, TR20_06, TR20_07, TR20_08 As New TableCell

            TR20_01.BackColor = Drawing.Color.WhiteSmoke
            TR20_07.BackColor = Drawing.Color.WhiteSmoke
            TR20_04.BackColor = Drawing.Color.WhiteSmoke
            TR20_01.BorderWidth = "1"
            TR20_02.BorderWidth = "1"
            TR20_04.BorderWidth = "1"
            TR20_05.BorderWidth = "1"
            TR20_07.BorderWidth = "1"
            TR20_08.BorderWidth = "1"

            TR20_01.BorderColor = Drawing.Color.Silver
            TR20_02.BorderColor = Drawing.Color.Silver
            TR20_04.BorderColor = Drawing.Color.Silver
            TR20_05.BorderColor = Drawing.Color.Silver
            TR20_07.BorderColor = Drawing.Color.Silver
            TR20_08.BorderColor = Drawing.Color.Silver



            TR20_01.BorderStyle = BorderStyle.Solid
            TR20_02.BorderStyle = BorderStyle.Solid
            TR20_04.BorderStyle = BorderStyle.Solid
            TR20_05.BorderStyle = BorderStyle.Solid
            TR20_07.BorderStyle = BorderStyle.Solid
            TR20_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR20, TR20_01, 16, 16, "l", "Performance Allwnce")
            RH.AddColumn(TR20, TR20_02, 16, 16, "r", DT.Rows(0)(27))
            RH.AddColumn(TR20, TR20_03, 2, 2, "c", "")
            RH.AddColumn(TR20, TR20_04, 16, 16, "l", "Other Allwnce")
            RH.AddColumn(TR20, TR20_05, 16, 16, "r", DT.Rows(0)(28))
            RH.AddColumn(TR20, TR20_06, 2, 2, "l", "")
            RH.AddColumn(TR20, TR20_07, 16, 16, "l", "Staff Welfare")
            RH.AddColumn(TR20, TR20_08, 16, 16, "r", StaffWelfare)


            tb.Controls.Add(TR20)
            Dim TR21 As New TableRow
            'TR21.BackColor = Drawing.Color.WhiteSmoke
            Dim TR21_01, TR21_02, TR21_03, TR21_04, TR21_05, TR21_06, TR21_07, TR21_08 As New TableCell

            TR21_01.BackColor = Drawing.Color.WhiteSmoke
            TR21_07.BackColor = Drawing.Color.WhiteSmoke
            TR21_04.BackColor = Drawing.Color.WhiteSmoke
            TR21_01.BorderWidth = "1"
            TR21_02.BorderWidth = "1"
            TR21_04.BorderWidth = "1"
            TR21_05.BorderWidth = "1"
            TR21_07.BorderWidth = "1"
            TR21_08.BorderWidth = "1"

            TR21_01.BorderColor = Drawing.Color.Silver
            TR21_02.BorderColor = Drawing.Color.Silver
            TR21_04.BorderColor = Drawing.Color.Silver
            TR21_05.BorderColor = Drawing.Color.Silver
            TR21_07.BorderColor = Drawing.Color.Silver
            TR21_08.BorderColor = Drawing.Color.Silver



            TR21_01.BorderStyle = BorderStyle.Solid
            TR21_02.BorderStyle = BorderStyle.Solid
            TR21_04.BorderStyle = BorderStyle.Solid
            TR21_05.BorderStyle = BorderStyle.Solid
            TR21_07.BorderStyle = BorderStyle.Solid
            TR21_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR21, TR21_01, 16, 16, "l", "Hospital Allwnce")
            RH.AddColumn(TR21, TR21_02, 16, 16, "r", DT.Rows(0)(30))
            RH.AddColumn(TR21, TR21_03, 2, 2, "c", "")
            RH.AddColumn(TR21, TR21_04, 16, 16, "l", "Field Allwnce")
            RH.AddColumn(TR21, TR21_05, 16, 16, "r", DT.Rows(0)(31))
            RH.AddColumn(TR21, TR21_06, 2, 2, "l", "")
            RH.AddColumn(TR21, TR21_07, 16, 16, "l", "ESWT")
            RH.AddColumn(TR21, TR21_08, 16, 16, "r", CInt(DT.Rows(0)(38)))


            tb.Controls.Add(TR21)
            Dim TR12 As New TableRow
            'TR12.BackColor = Drawing.Color.WhiteSmoke
            Dim TR12_01, TR12_02, TR12_03, TR12_04, TR12_05, TR12_06, TR12_07, TR12_08 As New TableCell

            TR12_01.BackColor = Drawing.Color.WhiteSmoke
            TR12_07.BackColor = Drawing.Color.WhiteSmoke
            TR12_04.BackColor = Drawing.Color.WhiteSmoke
            TR12_01.BorderWidth = "1"
            TR12_02.BorderWidth = "1"
            TR12_04.BorderWidth = "1"
            TR12_05.BorderWidth = "1"
            TR12_07.BorderWidth = "1"
            TR12_08.BorderWidth = "1"

            TR12_01.BorderColor = Drawing.Color.Silver
            TR12_02.BorderColor = Drawing.Color.Silver
            TR12_04.BorderColor = Drawing.Color.Silver
            TR12_05.BorderColor = Drawing.Color.Silver
            TR12_07.BorderColor = Drawing.Color.Silver
            TR12_08.BorderColor = Drawing.Color.Silver



            TR12_01.BorderStyle = BorderStyle.Solid
            TR12_02.BorderStyle = BorderStyle.Solid
            TR12_04.BorderStyle = BorderStyle.Solid
            TR12_05.BorderStyle = BorderStyle.Solid
            TR12_07.BorderStyle = BorderStyle.Solid
            TR12_08.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TR12, TR12_01, 16, 16, "l", "HRA")
            RH.AddColumn(TR12, TR12_02, 16, 16, "r", DT.Rows(0)(23))
            RH.AddColumn(TR12, TR12_03, 2, 2, "c", "")
            RH.AddColumn(TR12, TR12_04, 16, 16, "l", "Local Allwnce")
            RH.AddColumn(TR12, TR12_05, 16, 16, "r", DT.Rows(0)(26))
            RH.AddColumn(TR12, TR12_06, 2, 2, "l", "")
            RH.AddColumn(TR12, TR12_07, 16, 16, "l", "Charity Fund")
            RH.AddColumn(TR12, TR12_08, 16, 16, "r", CInt(DT.Rows(0)(37)))


            tb.Controls.Add(TR12)
            Dim TR13 As New TableRow
            'TR13.BackColor = Drawing.Color.WhiteSmoke
            Dim TR13_01, TR13_02, TR13_03, TR13_04, TR13_05, TR13_06, TR13_07, TR13_08 As New TableCell


            TR13_07.BackColor = Drawing.Color.WhiteSmoke
            TR13_04.BackColor = Drawing.Color.WhiteSmoke

            TR13_04.BorderWidth = "1"
            TR13_05.BorderWidth = "1"
            TR13_07.BorderWidth = "1"
            TR13_08.BorderWidth = "1"


            TR13_04.BorderColor = Drawing.Color.Silver
            TR13_05.BorderColor = Drawing.Color.Silver
            TR13_07.BorderColor = Drawing.Color.Silver
            TR13_08.BorderColor = Drawing.Color.Silver




            TR13_04.BorderStyle = BorderStyle.Solid
            TR13_05.BorderStyle = BorderStyle.Solid
            TR13_07.BorderStyle = BorderStyle.Solid
            TR13_08.BorderStyle = BorderStyle.Solid

            TR13_05.ForeColor = Drawing.Color.DarkRed
            TR13_08.ForeColor = Drawing.Color.DarkRed
            TR13_05.Font.Bold = True
            TR13_08.Font.Bold = True
            TR13_04.Font.Bold = True
            TR13_07.Font.Bold = True
            RH.AddColumn(TR13, TR13_01, 16, 16, "l", "")
            RH.AddColumn(TR13, TR13_02, 16, 16, "l", "")
            RH.AddColumn(TR13, TR13_03, 2, 2, "c", "")
            RH.AddColumn(TR13, TR13_04, 16, 16, "r", "Total [+]")
            RH.AddColumn(TR13, TR13_05, 16, 16, "r", GrossSalary)
            RH.AddColumn(TR13, TR13_06, 2, 2, "l", "")
            RH.AddColumn(TR13, TR13_07, 16, 16, "r", "Total [-]")
            RH.AddColumn(TR13, TR13_08, 16, 16, "r", Deductions)


            tb.Controls.Add(TR13)
            RH.BlankRow(tb, 5)
            RH.BlankRow(tb, 5)
            RH.BlankRow(tb, 5)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
