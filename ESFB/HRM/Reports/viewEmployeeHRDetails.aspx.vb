﻿
Imports System.Data
Imports System.IO
Partial Class viewEmployeeHRDetails
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GN As New GeneralFunctions
#Region "Page Load & Disposed"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 272) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '//-------------Call Back------------------//

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Me.txtEmpCode.Attributes.Add("onchange", "return EmpCodeOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CStr("ʘ"))
        Dim DT As New DataTable
        Dim SQL As String
        If Data(0) = 1 Then
            SQL = "select count(*) from emp_master where emp_code = " & CInt(Data(1)) & ""
            DT = DB.ExecuteDataSet(SQL).Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn = DT.Rows.Count
                CallBackReturn += "Æ"
                SQL = "select emp_name from emp_master where emp_code = " & CInt(Data(1)) & ""
                DT = DB.ExecuteDataSet(SQL).Tables(0)
                CallBackReturn = DT.Rows(0)(0)
            Else
                CallBackReturn = "0Æ"
            End If
        End If
    End Sub
#End Region
#Region "Confirm"
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            Dim EmpCode As Integer = CInt(Me.txtEmpCode.Text)
            ViewReport(EmpCode)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub

    Sub ViewReport(ByVal EmpCode As Integer)
        Dim EMP_Code As Integer
        EMP_Code = EmpCode

        Dim DT As New DataTable
        Dim DT_Prom As New DataTable
        Dim DT_Transfer As New DataTable
        Dim DT_ID As New DataTable
        Dim DT_ADDR As New DataTable
        Dim DT_FAM As New DataTable
        Dim DT_LANG As New DataTable
        tb.Attributes.Add("width", "100%")

        Dim RowBG As Integer = 0
        DT = DB.ExecuteDataSet("select a.emp_code,dbo.udfPropercase(a.emp_name), dbo.udfPropercase(isnull(d.branch_Name ,'')),dbo.udfPropercase(isnull(f.Cadre_name  ,'')),  " & _
                             " dbo.udfPropercase(isnull(g.Designation_Name ,'')), dbo.udfPropercase(isnull(e.Department_Name ,'')),  " & _
                             " dbo.udfPropercase(isnull(convert(varchar,a.Date_Of_Join ,106),'')), dbo.udfPropercase(isnull(b.DOB,'')),isnull(b.Email,''),  " & _
                             " dbo.udfPropercase(isnull(h.Gender_Name,'')),isnull(b.Mobile,''),dbo.udfPropercase(isnull(b.HouseName,'')) , isnull(b.LandLine,''),  " & _
                             " dbo.udfPropercase(b.CareOf),isnull(b.CareOf_Phone,''), dbo.udfPropercase(b.City),dbo.udfPropercase(b.location),  " & _
                             " dbo.udfPropercase(isnull(v.MStatus,'')),  dbo.udfPropercase(isnull(k.Pin_Code,'')),  " & _
                             " dbo.udfPropercase(isnull(k.Post_Office,'')), dbo.udfPropercase(isnull(j.District_Name,'')),  " & _
                             " dbo.udfPropercase(isnull(i.state_name,'')), dbo.udfPropercase(isnull(b.Fathers_name,'')), dbo.udfPropercase(isnull(b.Mothers_name,'')) ,  " & _
                             " dbo.udfPropercase(isnull(b.Cug_No,'')),dbo.udfPropercase(isnull(b.Official_mail_id,'')),w.Status,a.Last_modified_Dt,r.Post_Name,  " & _
                             " dbo.udfPropercase(isnull(p.Emp_Name,''))+'('+ convert(varchar,a.Reporting_To) + ')',dbo.udfPropercase(isnull(q.Emp_Name,''))+'('+ convert(varchar,a.User_ID) + ')'   " & _
                             " from EMP_MASTER a LEFT JOIN  Emp_Profile b ON a.Emp_Code=b.Emp_Code   LEFT JOIN BRANCH_MASTER d ON a.Branch_ID=d.Branch_ID LEFT JOIN DEPARTMENT_MASTER e  " & _
                             " ON a.Department_ID=e.Department_ID LEFT JOIN   CADRE_MASTER f ON  a.Cadre_ID=f.Cadre_ID LEFT JOIN DESIGNATION_MASTER g ON  a.Designation_ID=g.Designation_ID  " & _
                             " LEFT JOIN Post_Master k ON b.Post_ID=k.Post_ID LEFT JOIN District_Master j ON k.District_ID=j.District_ID LEFT JOIN State_Master i ON  " & _
                             " j.State_ID =i.State_ID LEFT JOIN emp_master l ON a.Reporting_to=l.Emp_Code LEFT JOIN GENDER_MASTER h ON a.gender_id=h.gender_id   " & _
                             " LEFT JOIN MARITAL_STATUS v ON v.MStatus_ID=b.Marital_Status LEFT JOIN emp_status w ON a.Status_ID=w.Status_ID LEFT JOIN EMP_MASTER p ON p.emp_code = a.Reporting_To  " & _
                             " LEFT JOIN EMP_POST_MASTER r ON r.Post_ID = b.Post_ID LEFT JOIN EMP_MASTER q ON q.emp_code = a.User_ID where a.Emp_Code =" & EMP_Code & "").Tables(0)

        DT_Transfer = DB.ExecuteDataSet("select f.Branch_Name as prevBranch,d.Branch_Name as Branch,g.Department_Name as prevDep ,e.Department_Name as Dep ,  " & _
                        "	h.Emp_Name as PrevReporting,b.Emp_Name as Reporting,i.Post_Name as prevPost ,c.Post_Name as Post,convert(varchar,a.Effective_Date,106) as Edate,  " & _
                        "	j.designation_Name as Prev_Desig,k.designation_Name,l.Cadre_Name as Prev_Cadre,m.Cadre_Name   from EMP_TRANSFER  a , EMP_MASTER b , EMP_POST_MASTER c,  " & _
                        "	BRANCH_MASTER d,DEPARTMENT_MASTER e,BRANCH_MASTER f,DEPARTMENT_MASTER g,EMP_MASTER h,EMP_POST_MASTER i,Designation_master j,designation_master k,cadre_master l,cadre_master m where(a.Reporting_To = b.Emp_Code And a.EmpPost =   " & _
                        "	c.Post_ID And a.Branch_ID = d.Branch_ID And a.Department_ID = e.Department_ID) and a.Prev_Branch_ID=f.Branch_ID and a.Prev_Department_ID =g.Department_ID   " & _
                        "	and a.Prev_Reporting_To=h.emp_code and a.Prev_EmpPost=i.Post_ID and a.Prev_Designation_id=j.designation_id and a.Designation_id=k.designation_id   " & _
                        "	and a.Prev_CadreID=l.Cadre_id and a.Cadreid=m.Cadre_id  and a.Emp_code=" & EMP_Code & "  order by a.Effective_Date").Tables(0)

        DT_Prom = DB.ExecuteDataSet("select convert(varchar,a.Effective_Date,106) as edate,e.Designation_Name as fromdesg,f.Designation_Name as todesg,b.Emp_Name as fromReporting,h.Emp_Name as ToReporting  " & _
            " ,c.Post_Name as fromPost,i.Post_Name as toPost,a.trans_id from EMP_PROMOTION  a,EMP_MASTER b,EMP_POST_MASTER c,BRANCH_MASTER d,DESIGNATION_MASTER  e,DESIGNATION_MASTER  f,EMP_MASTER h,EMP_POST_MASTER i " & _
            " where(a.Reporting_To_From = b.Emp_Code And a.EmpPost = c.Post_ID And a.Branch_ID = d.Branch_ID And a.Designation_ID_From = e.Designation_ID) " & _
            " and a.Designation_ID_to =f.Designation_ID and a.Reporting_To_To=h.emp_code and a.EmpPost=i.Post_ID and a.Emp_code=" + EMP_Code.ToString() + "  order by a.Effective_Date").Tables(0)

        RH.BlankRow(tb, 5)
        'Dim TRItemHead As New TableRow
        'TRItemHead.ForeColor = Drawing.Color.DarkBlue
        'Dim TRItemHead_00 As New TableCell
        'RH.BlankRow(tb, 5)
        'RH.InsertColumn(TRItemHead, TRItemHead_00, 100, 2, "<b>APPOINTMENT DETAILS</b>")
        'TRItemHead.BackColor = Drawing.Color.WhiteSmoke
        'tb.Controls.Add(TRItemHead)
        'RH.BlankRow(tb, 5)


        Dim TR62 As New TableRow
        Dim TR62_01, TR62_02, TR62_03, TR62_04, TR62_05, TR62_06, TR62_07, TR62_08 As New TableCell
        RH.InsertColumn(TR62, TR62_01, 16, 0, "Employee Code")
        RH.InsertColumn(TR62, TR62_02, 16, 0, DT.Rows(0)(0))
        RH.AddColumn(TR62, TR62_03, 2, 2, "l", "")
        RH.InsertColumn(TR62, TR62_04, 16, 0, "Name")
        RH.InsertColumn(TR62, TR62_05, 16, 0, DT.Rows(0)(1))
        RH.AddColumn(TR62, TR62_06, 2, 2, "l", "")
        RH.InsertColumn(TR62, TR62_07, 16, 0, "Branch")
        RH.InsertColumn(TR62, TR62_08, 16, 0, DT.Rows(0)(2))
        tb.Controls.Add(TR62)

        Dim TR63 As New TableRow
        Dim TR63_01, TR63_02, TR63_03, TR63_04, TR63_05, TR63_06, TR63_07, TR63_08 As New TableCell
        RH.InsertColumn(TR63, TR63_01, 16, 0, "Cadre")
        RH.InsertColumn(TR63, TR63_02, 16, 0, DT.Rows(0)(3))
        RH.AddColumn(TR63, TR63_03, 2, 2, "l", "")
        RH.InsertColumn(TR63, TR63_04, 16, 0, "Department")
        RH.InsertColumn(TR63, TR63_05, 16, 0, DT.Rows(0)(5).ToString.ToUpper())
        RH.AddColumn(TR63, TR63_06, 2, 2, "l", "")
        RH.InsertColumn(TR63, TR63_07, 16, 0, "Designation")
        RH.InsertColumn(TR63, TR63_08, 16, 0, DT.Rows(0)(4))
        tb.Controls.Add(TR63)

        Dim TR64 As New TableRow
        Dim TR64_01, TR64_02, TR64_03, TR64_04, TR64_05, TR64_06, TR64_07, TR64_08 As New TableCell
        RH.InsertColumn(TR64, TR64_01, 16, 0, "Date Of Joining")
        RH.InsertColumn(TR64, TR64_02, 16, 0, DT.Rows(0)(6))
        RH.AddColumn(TR64, TR64_03, 2, 2, "c", "")
        RH.InsertColumn(TR64, TR64_04, 16, 0, "Status")
        RH.InsertColumn(TR64, TR64_05, 16, 0, DT.Rows(0)(26))
        RH.AddColumn(TR64, TR64_06, 2, 2, "l", "")
        RH.InsertColumn(TR64, TR64_07, 16, 0, "Post")
        If IsDBNull(DT.Rows(0)(28)) Then
            RH.InsertColumn(TR64, TR64_08, 16, 0, "")
        Else
            RH.InsertColumn(TR64, TR64_08, 16, 0, DT.Rows(0)(28))
        End If

        tb.Controls.Add(TR64)

        Dim TR65 As New TableRow
        Dim TR65_01, TR65_02, TR65_03, TR65_04, TR65_05, TR65_06, TR65_07, TR65_08 As New TableCell
        RH.InsertColumn(TR65, TR65_01, 16, 0, "Reporting To")
        RH.InsertColumn(TR65, TR65_02, 16, 0, DT.Rows(0)(29))
        RH.AddColumn(TR65, TR65_03, 2, 2, "c", "")
        RH.InsertColumn(TR65, TR65_04, 16, 0, "Last Modified By")
        RH.InsertColumn(TR65, TR65_05, 16, 0, DT.Rows(0)(30))
        RH.AddColumn(TR65, TR65_06, 2, 2, "l", "")
        RH.InsertColumn(TR65, TR65_07, 16, 0, "Last Modified Date")
        RH.InsertColumn(TR65, TR65_08, 16, 0, Format(CDate(DT.Rows(0)(27)), "dd MMM yyyy"))
        tb.Controls.Add(TR65)

        Dim TR2 As New TableRow
        Dim TR2_01, TR2_02, TR2_03, TR2_04, TR2_05, TR2_06, TR2_07, TR2_08 As New TableCell
        RH.InsertColumn(TR2, TR2_01, 16, 0, "Date Of Birth")
        RH.InsertColumn(TR2, TR2_02, 16, 0, Format(CDate(DT.Rows(0)(7)), "dd MMM yyyy"))
        RH.AddColumn(TR2, TR2_03, 2, 2, "c", "")
        RH.InsertColumn(TR2, TR2_04, 16, 0, "Marital Status")
        RH.InsertColumn(TR2, TR2_05, 16, 0, DT.Rows(0)(17))
        RH.AddColumn(TR2, TR2_06, 2, 2, "l", "")
        RH.InsertColumn(TR2, TR2_07, 16, 0, "Gender")
        RH.InsertColumn(TR2, TR2_08, 16, 0, DT.Rows(0)(9))
        tb.Controls.Add(TR2)


        Dim TR5 As New TableRow
        Dim TR5_01, TR5_02, TR5_03, TR5_04, TR5_05, TR5_06, TR5_07, TR5_08 As New TableCell
        RH.InsertColumn(TR5, TR5_01, 16, 0, "Official Mail")
        RH.InsertColumn(TR5, TR5_02, 16, 0, DT.Rows(0)(25))
        RH.AddColumn(TR5, TR5_03, 2, 2, "c", "")
        RH.InsertColumn(TR5, TR5_04, 16, 0, "E-Mail")
        RH.InsertColumn(TR5, TR5_05, 16, 0, DT.Rows(0)(8))
        RH.AddColumn(TR5, TR5_06, 2, 2, "l", "")
        RH.InsertColumn(TR5, TR5_07, 16, 0, "CUG")
        RH.InsertColumn(TR5, TR5_08, 16, 0, DT.Rows(0)(24))
        tb.Controls.Add(TR5)

        'Transfer Details

        Dim TRItemHead3 As New TableRow
        TRItemHead3.ForeColor = Drawing.Color.DarkBlue
        Dim TRItemHead3_00 As New TableCell
        RH.BlankRow(tb, 5)
        RH.InsertColumn(TRItemHead3, TRItemHead3_00, 100, 2, "<b>TRANSFER DETAILS</b>")
        TRItemHead3.BackColor = Drawing.Color.WhiteSmoke
        tb.Controls.Add(TRItemHead3)


        Dim TrID As Integer = 0
        For Each DR In DT_Transfer.Rows
            If TrID = 0 Then
                RH.BlankRow(tb, 5)
                Dim TRItemSubHead3 As New TableRow
                TRItemSubHead3.ForeColor = Drawing.Color.DarkBlue
                Dim TRItemSubHead3_00 As New TableCell
                RH.AddColumn(TRItemSubHead3, TRItemSubHead3_00, 100, 100, "l", "JOINING DETAILS")
                TRItemSubHead3.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TRItemSubHead3)
                RH.BlankRow(tb, 5)

                Dim TR33 As New TableRow
                TR33.BackColor = Drawing.Color.WhiteSmoke
                Dim TR33_01, TR33_02, TR33_03, TR33_04, TR33_05, TR33_06, TR33_07 As New TableCell

                RH.InsertColumn(TR33, TR33_01, 25, 0, "Location")
                RH.InsertColumn(TR33, TR33_02, 15, 0, "Department")
                RH.InsertColumn(TR33, TR33_03, 10, 0, "Cadre")
                RH.InsertColumn(TR33, TR33_04, 20, 0, "Designation")
                RH.InsertColumn(TR33, TR33_05, 30, 0, "Reporting&nbsp;Officer")
                tb.Controls.Add(TR33)

                Dim TR44 As New TableRow
                TR44.BorderWidth = "1"
                TR44.BorderStyle = BorderStyle.Solid
                Dim TR44_01, TR44_02, TR44_03, TR44_04, TR44_05 As New TableCell
                RH.InsertColumn(TR44, TR44_01, 25, 0, DR(0).ToString())
                RH.InsertColumn(TR44, TR44_02, 15, 0, DR(2).ToString())
                RH.InsertColumn(TR44, TR44_03, 10, 0, DR(11).ToString())
                RH.InsertColumn(TR44, TR44_04, 20, 0, DR(9).ToString())
                RH.InsertColumn(TR44, TR44_05, 30, 0, DR(4).ToString())
                tb.Controls.Add(TR44)
                TrID = 1
            Else
                Dim TRItemSubHead4 As New TableRow
                TRItemSubHead4.ForeColor = Drawing.Color.DarkBlue
                Dim TRItemSubHead4_00 As New TableCell
                RH.BlankRow(tb, 5)
                RH.InsertColumn(TRItemSubHead4, TRItemSubHead4_00, 100, 0, "TRANSFER DETAILS")
                TRItemSubHead4.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TRItemSubHead4)
                RH.BlankRow(tb, 5)

                Dim TR34 As New TableRow
                TR34.BackColor = Drawing.Color.WhiteSmoke
                Dim TR34_01, TR34_02, TR34_03, TR34_04, TR34_05, TR34_06, TR34_07 As New TableCell

                RH.InsertColumn(TR34, TR34_01, 25, 0, "Location")
                RH.InsertColumn(TR34, TR34_02, 15, 0, "Department")
                RH.InsertColumn(TR34, TR34_03, 10, 0, "Cadre")
                RH.InsertColumn(TR34, TR34_04, 20, 0, "Designation")
                RH.InsertColumn(TR34, TR34_05, 30, 0, "Reporting&nbsp;Officer")
                tb.Controls.Add(TR34)

                Dim TR45 As New TableRow
                TR45.BorderWidth = "1"
                TR45.BorderStyle = BorderStyle.Solid
                Dim TR45_01, TR45_02, TR45_03, TR45_04, TR45_05 As New TableCell
                RH.InsertColumn(TR45, TR45_01, 25, 0, DR(0).ToString())
                RH.InsertColumn(TR45, TR45_02, 15, 0, DR(2).ToString())
                RH.InsertColumn(TR45, TR45_03, 10, 0, DR(11).ToString())
                RH.InsertColumn(TR45, TR45_04, 20, 0, DR(9).ToString())
                RH.InsertColumn(TR45, TR45_05, 30, 0, DR(4).ToString())
                tb.Controls.Add(TR45)
            End If
        Next
        RH.BlankRow(tb, 5)
        'Promotion Details

        Dim TRItemHead4 As New TableRow
        TRItemHead4.ForeColor = Drawing.Color.DarkBlue
        Dim TRItemHead4_00 As New TableCell
        RH.BlankRow(tb, 5)
        RH.InsertColumn(TRItemHead4, TRItemHead4_00, 100, 2, "<b>PROMOTION DETAILS</b>")
        TRItemHead4.BackColor = Drawing.Color.WhiteSmoke
        tb.Controls.Add(TRItemHead4)

        Dim TR344 As New TableRow
        TR344.BackColor = Drawing.Color.WhiteSmoke
        Dim TR344_01, TR344_02, TR344_03, TR344_04, TR344_05, TR344_06, TR344_07 As New TableCell

        RH.InsertColumn(TR344, TR344_01, 10, 0, "Date")
        RH.InsertColumn(TR344, TR344_02, 15, 0, "Previous Designation")
        RH.InsertColumn(TR344, TR344_03, 15, 0, "Current Designation")
        RH.InsertColumn(TR344, TR344_04, 15, 0, "Previous Reporting")
        RH.InsertColumn(TR344, TR344_05, 15, 0, "Current Reporting")
        RH.InsertColumn(TR344, TR344_06, 15, 0, "Previous Post")
        RH.InsertColumn(TR344, TR344_07, 15, 0, "Current Post")
        tb.Controls.Add(TR344)

        Dim PrID As Integer = 0
        For Each DR In DT_Prom.Rows
            Dim TR455 As New TableRow
            TR455.BorderWidth = "1"
            TR455.BorderStyle = BorderStyle.Solid
            Dim TR455_01, TR455_02, TR455_03, TR455_04, TR455_05, TR455_06, TR455_07 As New TableCell
            RH.InsertColumn(TR455, TR455_01, 10, 0, Format(CDate(DR(0)), "dd MMM yyyy"))
            RH.InsertColumn(TR455, TR455_02, 15, 0, DR(1).ToString())
            RH.InsertColumn(TR455, TR455_03, 15, 0, DR(2).ToString())
            RH.InsertColumn(TR455, TR455_04, 15, 0, DR(3).ToString())
            RH.InsertColumn(TR455, TR455_05, 15, 0, DR(4).ToString())
            RH.InsertColumn(TR455, TR455_06, 15, 0, DR(5).ToString())
            RH.InsertColumn(TR455, TR455_07, 15, 0, DR(6).ToString())
            tb.Controls.Add(TR455)
        Next

        RH.BlankRow(tb, 5)
        RH.BlankRow(tb, 5)
        pnDisplay.Controls.Add(tb)

    End Sub
#End Region

End Class
