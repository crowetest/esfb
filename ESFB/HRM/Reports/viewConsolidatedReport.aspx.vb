﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewConsolidatedReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim rptID As Integer
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 115) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            hdn_PostID.Value = CInt(Request.QueryString.Get("PostID"))
            Dim PostID As Integer = CInt(Request.QueryString.Get("PostID"))
            rptID = CInt(GN.Decrypt(Request.QueryString.Get("rptID")))
            hdnReportID.Value = rptID.ToString()
            cmd_Back.Attributes.Add("onclick", "return Backform(" & PostID & ")")
            cmd_Close.Attributes.Add("onclick", "return Exitform(" & PostID & ")")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            Dim BranchName As String = ""
            Dim RowBG As Integer = 0
            Dim I As Integer = 1
            Dim DT As New DataTable
            If rptID = 1 Then
                Dim FromDt As Date = CDate(Request.QueryString.Get("FromDt"))
                Dim ToDt As Date = CDate(Request.QueryString.Get("ToDt"))
                Dim EmpCode As Integer = CInt(GN.Decrypt(Request.QueryString.Get("UserID")))
                DT = DB.ExecuteDataSet("select (case when Branch_ID<100 then Department_Name else Branch_Name end)as Branch from Emp_List where Emp_Code=" + EmpCode.ToString()).Tables(0)
                If DT.Rows.Count > 0 Then
                    BranchName = DT.Rows(0)(0)
                End If
                DT = DB.ExecuteDataSet("select L.Emp_code,E.Emp_Name,T.Type_Name,l.Leave_From,l.Leave_To,l.Leave_Days,R.Reason, case when L.Status_ID=0 then 'Requested' WHEN L.Status_ID=1 then 'Approved' WHEN L.Status_ID=2 then 'Recommended' WHEN L.Status_ID=5 then 'Rejected' WHEN L.Status_ID=6 then 'Cancelled' ELSE '' END AS status from EMP_LEAVE_REQUEST L INNER JOIN EMP_LEAVE_REASONS R ON L.Reason_ID=R.Reason_ID  INNER JOIN Emp_Leave_Type T ON L.Leave_Type=T.Type_ID INNER JOIN EMP_MASTER E ON L.Emp_Code=E.Emp_Code WHERE L.emp_code in(Select Emp_Code from EMP_MASTER where Reporting_To=" + EmpCode.ToString() + ") and  L.Leave_From <= '" + ToDt.ToString("MM/dd/yyyy") + "'  and L.Leave_To >= '" + FromDt.ToString("MM/dd/yyyy") + "'  AND L.Status_ID<5 order by L.Emp_Code").Tables(0)
                RH.Heading(Session("FirmName"), tb, "CONSOLIDATED LEAVE REPORT", 100)
                RH.SubHeading(tb, 100, "l", "Date               :" + FromDt.ToString("dd MMM yyyy"))
                RH.SubHeading(tb, 100, "l", "Location/Department:" + BranchName)
                RH.DrawLine(tb, 100)

                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")
                ' tb.Attributes.Add("border", "1")
                Dim TR0 As New TableRow
                TR0.BackColor = Drawing.Color.WhiteSmoke
                Dim TR0_00, TR0_01, TR0_02, TR0_03, TR0_04, TR0_05, TR0_06, TR0_07, TR0_08 As New TableCell
                RH.AddColumn(TR0, TR0_00, 5, 5, "c", "SI No")
                RH.AddColumn(TR0, TR0_01, 10, 10, "l", "Emp Code")
                RH.AddColumn(TR0, TR0_02, 15, 15, "l", "Name")
                RH.AddColumn(TR0, TR0_03, 15, 15, "l", "Leave Type")
                RH.AddColumn(TR0, TR0_04, 10, 10, "l", "From Date")
                RH.AddColumn(TR0, TR0_05, 10, 10, "l", "To Date")
                RH.AddColumn(TR0, TR0_06, 10, 10, "l", "Leave Days")
                RH.AddColumn(TR0, TR0_07, 10, 10, "l", "Status")
                RH.AddColumn(TR0, TR0_08, 15, 15, "l", "Reason")
                tb.Controls.Add(TR0)
                RH.DrawLine(tb, 100)

                For Each DR In DT.Rows
                    Dim TR1 As New TableRow
                    If RowBG = 0 Then
                        RowBG = 1
                        TR1.BackColor = Drawing.Color.WhiteSmoke
                    Else
                        RowBG = 0
                    End If
                    Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07, TR1_08 As New TableCell
                    RH.AddColumn(TR1, TR1_00, 5, 5, "c", I.ToString())
                    RH.AddColumn(TR1, TR1_01, 10, 10, "l", DR(0))
                    RH.AddColumn(TR1, TR1_02, 15, 15, "l", DR(1))
                    RH.AddColumn(TR1, TR1_03, 15, 15, "l", DR(2))
                    RH.AddColumn(TR1, TR1_04, 10, 10, "l", Format(DR(3), "dd-MMM-yyyy"))
                    RH.AddColumn(TR1, TR1_05, 10, 10, "l", Format(DR(4), "dd-MMM-yyyy"))
                    RH.AddColumn(TR1, TR1_06, 10, 10, "c", DR(5))
                    RH.AddColumn(TR1, TR1_07, 10, 10, "l", DR(7))
                    RH.AddColumn(TR1, TR1_08, 15, 15, "l", DR(6))
                    tb.Controls.Add(TR1)

                    I = I + 1
                Next
                RH.DrawLine(tb, 100)
            Else

                Dim PrFlag = Request.QueryString.Get("PresentFlag")
                Dim RegionID As Integer = GN.Decrypt(Request.QueryString.Get("RegionID"))
                Dim AreaID As Integer = GN.Decrypt(Request.QueryString.Get("AreaID"))
                Dim BranchID As Integer = GN.Decrypt(Request.QueryString.Get("BranchID"))
                Dim TraDt As DateTime = CDate(Request.QueryString.Get("TraDt"))
                Dim StrSubHD As String = ""
                RH.DrawLine(tb, 100)
                Dim SqlStr As String = "select b.Branch_Name, a.Emp_Code,c.Emp_Name, tra_dt,isnull(M_Time,''),isnull(E_Time,''),isnull(att_remarks,'') FROM ATTEND_ALL a,brmaster b,EMP_MASTER c  where a.Branch_ID=b.Branch_ID and a.Emp_Code=c.Emp_Code and c.Emp_Category=1 and Tra_Dt ='" + TraDt.ToString("MM/dd/yyyy") + "'"

                If Not RegionID < 0 Then
                    SqlStr += " and b.region_id=" + RegionID.ToString()
                    StrSubHD = GN.GetRegion_Name(RegionID) + " Region"
                End If

                If Not AreaID < 0 Then
                    SqlStr += " and b.area_id=" + AreaID.ToString()
                    StrSubHD = GN.GetArea_Name(AreaID) + " Area"
                End If
                If Not BranchID < 0 Then
                    SqlStr += " and b.branch_id=" + BranchID.ToString()
                    StrSubHD = GN.GetBranch_Name(BranchID) + " Branch"
                End If

                If PrFlag = 1 Then
                    SqlStr += " and  c.Emp_Type_ID<>2"
                    StrSubHD += " Staus - Total Normal Staff "
                ElseIf PrFlag = 2 Then
                    SqlStr += " and (a.M_Time is not null or a.e_time IS not null) and c.Emp_Type_ID <>2"
                    StrSubHD += " Staus - Normal Staff Present"
                ElseIf PrFlag = 3 Then
                    SqlStr += " and (a.M_Time is  null and a.e_time IS null) and c.Emp_Type_ID <>2"
                    StrSubHD += " Staus - Normal Staff Absent"
                ElseIf PrFlag = 4 Then
                    SqlStr += " and  c.Emp_Type_ID=2"
                    StrSubHD += " Staus - Total Field Staff"
                ElseIf PrFlag = 5 Then
                    SqlStr += " and (a.M_Time is not  null or a.e_time is not null) and c.Emp_Type_ID=2 and a.FS_Approved_by is not null"
                    StrSubHD += " Staus - Field Staff Present"
                ElseIf PrFlag = 6 Then
                    SqlStr += " and ((a.M_Time is  null and a.e_time is null  and a.FS_Approved_by is not null ) or a.FS_Approved_by is null )and c.Emp_Type_ID=2 "
                    StrSubHD += " Staus - Field Staff Absent"
                ElseIf PrFlag = 7 Then
                    SqlStr += " and (a.M_Time is not null or a.e_time is not null ) and c.Emp_Type_ID=2 and a.FS_Approved_by is not null"
                    StrSubHD += " Staus - Field Staff Both"
                End If
                SqlStr += " and c.Emp_Category=1  order by   b.Branch_Name, a.Emp_Code,c.Emp_Name,tra_dt"
                DT = DB.ExecuteDataSet(SqlStr).Tables(0)
                RH.Heading(Session("FirmName"), tb, "CONSOLIDATED PUNCHING REPORT OF " + TraDt.ToString("dd/MMM/yyyy") + " " + StrSubHD, 100)



                Dim TR02 As New TableRow

                Dim TR02_00, TR02_01, TR02_02, TR02_03 As New TableCell
                RH.AddColumn(TR02, TR02_00, 10, 10, "l", "Date         :")
                RH.AddColumn(TR02, TR02_01, 50, 50, "l", TraDt.ToString("dd MMM yyyy"))
                RH.AddColumn(TR02, TR02_02, 20, 20, "l", "")
                RH.AddColumn(TR02, TR02_03, 20, 20, "l", "")

                tb.Controls.Add(TR02)
                Dim DR1 As DataRow
                tb.Attributes.Add("width", "100%")
                ' tb.Attributes.Add("border", "1")
                Dim TR1 As New TableRow
                TR1.BackColor = Drawing.Color.WhiteSmoke
                Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07 As New TableCell
                RH.AddColumn(TR1, TR1_00, 10, 10, "c", "SI No")
                RH.AddColumn(TR1, TR1_01, 10, 10, "l", "Location")
                RH.AddColumn(TR1, TR1_02, 10, 10, "l", "Emp Code")
                RH.AddColumn(TR1, TR1_03, 20, 20, "l", "Name")
                RH.AddColumn(TR1, TR1_04, 15, 15, "l", "Date")
                RH.AddColumn(TR1, TR1_05, 15, 15, "l", "Morning")
                RH.AddColumn(TR1, TR1_06, 10, 10, "l", "Evening")
                RH.AddColumn(TR1, TR1_07, 10, 10, "l", "Remarks")
                tb.Controls.Add(TR1)
                RH.DrawLine(tb, 100)

                For Each DR1 In DT.Rows
                    Dim TR2 As New TableRow
                    If RowBG = 0 Then
                        RowBG = 1
                        TR2.BackColor = Drawing.Color.WhiteSmoke
                    Else
                        RowBG = 0
                    End If
                    Dim TR2_00, TR2_01, TR2_02, TR2_03, TR2_04, TR2_05, TR2_06, TR2_07 As New TableCell
                    RH.AddColumn(TR2, TR2_00, 10, 10, "c", I.ToString())
                    RH.AddColumn(TR2, TR2_01, 10, 10, "l", DR1(0))
                    RH.AddColumn(TR2, TR2_02, 10, 10, "l", DR1(1))
                    RH.AddColumn(TR2, TR2_03, 20, 20, "l", DR1(2))
                    RH.AddColumn(TR2, TR2_04, 15, 15, "l", DR1(3))
                    RH.AddColumn(TR2, TR2_05, 15, 15, "l", DR1(4))
                    RH.AddColumn(TR2, TR2_06, 10, 10, "l", DR1(5))
                    RH.AddColumn(TR2, TR2_07, 10, 10, "l", DR1(6))
                    tb.Controls.Add(TR2)
                    I = I + 1
                Next
                RH.DrawLine(tb, 100)
                RH.BlankRow(tb, 30)
            End If
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
