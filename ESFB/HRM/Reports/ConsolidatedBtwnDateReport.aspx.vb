﻿Imports System.Data
Partial Class ConsolidatedBtwnDateReport
    Inherits System.Web.UI.Page
    Dim rptID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 30) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            rptID = CInt(Request.QueryString.Get("ID"))
            Dim SubHead As String = ""
            If rptID = 1 Then
                SubHead = "Consolidated Travel Report"
            End If
            Me.Master.subtitle = SubHead
            txtFromDt.Attributes.Add("onchange", "return DateOnChange();")
            txtToDt.Attributes.Add("onchange", "return DateOnChange();")
            btnGenerate.Attributes.Add("onclick", "return GenerateOnClick()")

            txtFromDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            txttODt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try
            Response.Redirect("viewConsolidatedBtwnDateReport.aspx?rptID=" + GF.Encrypt(rptID.ToString()) + "&UserID=" + GF.Encrypt(Session("UserID")) + " &FromDt=" + txtFromDt.Text + " &ToDt=" + txtToDt.Text + "", False)
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
