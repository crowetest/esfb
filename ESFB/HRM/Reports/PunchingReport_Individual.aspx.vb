﻿Imports System.Data
Partial Class Reports_PunchingReport_Individual
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim rptID As Integer
    Dim CallBackReturn As String
    Dim GF As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1107) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            If Not IsPostBack Then
                Dim DT As New DataTable
                DT = DB.ExecuteDataSet("select * from roles_assigned where role_id=11 and emp_code= " + Session("UserID").ToString()).Tables(0)
                If DT.Rows.Count > 0 Then
                    DT = DB.ExecuteDataSet("select a.emp_code,a.emp_name from EMP_MASTER a where emp_status_id<>6 and emp_Type_id=1 and a.emp_code>100").Tables(0)
                    GF.ComboFill(cmbEmployee, DT, 0, 1)
                Else
                    GF.ComboFill(cmbEmployee, GF.getReportingEmployees(CInt(Session("UserID"))), 0, 1)
                End If

                txtFromDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
                txttODt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            End If
            Dim SubHead As String = "Punching Report"
         
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = SubHead
            txtFromDt.Attributes.Add("onchange", "return DateOnChange();")
            txtToDt.Attributes.Add("onchange", "return DateOnChange();")
            btnGenerate.Attributes.Add("onclick", "return GenerateOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try
            Dim Data() As String = Me.hdnValue.Value.Split("Ø")
            Response.Redirect("viewPunchingReport.aspx?rptID=" + GF.Encrypt(11) + " &EmpCode=" + GF.Encrypt(Data(0)) + " &FromDt=" + Data(1) + " &ToDt=" + Data(2) + " &EmpName=" + GF.Encrypt(Data(3)) + "", False)
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If Data(0) = 1 Then
            Dim EmpCode As Integer = CInt(Data(1))
            Dim EN As New Enrollment
            Dim DT As New DataTable
            DT = EN.GetEmployee(EmpCode)
            If DT.Rows.Count > 0 Then
                CallBackReturn = "1Ø" + DT.Rows(0)(1)
            Else
                CallBackReturn = "-1Ø-1"
            End If

        End If
    End Sub
End Class
