﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AreaMaster
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim MAST As New Master

#Region "Page Load & Dispose"
    Protected Sub AreaMaster_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub AreaMaster_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1182) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "AREA MASTER"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            If Not IsPostBack Then
                DT = GF.GetQueryResult("select -1 as Region_ID,' -----SELECT-----' as Region_name UNION ALL select Region_id,Region_name from Region_Master where Status_ID=1 ")
                GF.ComboFill(cmbRegion, DT, 0, 1)

                DT = GF.GetQueryResult("SELECT -1 as Emp_Code,' -----Select-----' as Emp_Name Union all select Emp_code,CAST(Emp_Code as varchar(5))+'   |   ' +emp_Name from EMP_MASTER  where Status_ID=1 ")
                GF.ComboFill(cmbAreaHead, DT, 0, 1)
            End If
            Me.cmbRegion.Attributes.Add("onchange", "RegionOnChange()")
            Me.txt_AreaName.Attributes.Add("onblur", "AreaNameOnChange()")
            Me.cmbAreaHead.Attributes.Add("onchange", "AreaHeadOnChange()")

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "windows_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        If CInt(Data(0)) = 2 Then
            DT = MAST.GetEmail(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "ʘ" + DR(0).ToString()
            Next
        End If

        If CInt(Data(0)) = 1 Then
            Dim Region_id As Integer = CInt(Data(1))
            Dim Area_name As String = CStr(Data(2))
            Dim Area_Head As Integer = CInt(Data(3))
            Dim email As String = CStr(Data(4))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@Region_id", SqlDbType.Int)
                Params(0).Value = Region_id
                Params(1) = New SqlParameter("@Area_name", SqlDbType.VarChar, 50)
                Params(1).Value = Area_name
                Params(2) = New SqlParameter("@Area_Head", SqlDbType.Int)
                Params(2).Value = Area_Head
                Params(3) = New SqlParameter("@email", SqlDbType.VarChar, 50)
                Params(3).Value = email
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_Area_Reg", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        ElseIf CInt(Data(0)) = 3 Then
            DT = MAST.GET_AREA(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString()
            Next
        ElseIf CInt(Data(0)) = 4 Then
            DT = DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY a.area_name ) as row,upper(b.Region_name) as Region_name,upper(a.area_name) as Area_name,upper(s.Emp_Name) as Emp_name  from  area_master a left join Region_master b on b.Region_ID=a.Region_ID inner join EMP_MASTER s on a.Area_Head=s.emp_code where a.Status_ID=1  and a.Area_Name like '" & CStr(Data(1)) & "%' order by a.area_name").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()

            Next
        End If
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
End Class
