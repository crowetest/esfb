﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class ChangeEmployDetails
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DB As New MS_SQL.Connect
    Dim DT, DT1 As New DataTable
    Dim UserID As Integer
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.txtEmpCode.Focus()
            Me.Master.subtitle = "Change Employee Details"
            UserID = CInt(Session("UserID"))
            If GN.FormAccess(UserID, 378) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            DT = DB.ExecuteDataSet("select '-1' as Branch_id,' ---------Select---------' as Branch_Name,''  union all " & _
                "select cast(Branch_id as varchar(5))+'|'+cast(status_ID as varchar(2)) as Branch_ID,Branch_Name,CAST(status_id AS VARCHAR(3) )  from Branch_MASTER where status_id >1   " & _
                "union all " & _
                "select '-1' as Branch_id,' ----------------','BB' as Branch_Name union all " & _
                " select cast(Branch_id as varchar(5))+'|'+cast(status_ID as varchar(2)) as Branch_ID,Branch_Name,(case when status_id = 1 then 'Branch'+SPACE(6) else 'Admin Centre' end) +' | '+branch_name from Branch_MASTER where status_id=1  order by 3").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
            GN.ComboFill(cmbDepartment, EN.GetDepartment(), 0, 1)
            GN.ComboFill(cmbDesignation, EN.GetDesignation(), 0, 1)
            GN.ComboFill(cmbCadre, EN.GetCadre(), 0, 1)
            GN.ComboFill(cmbEmpPost, GN.GetEmpPost(), 0, 1)
            GN.ComboFill(cmbEmpType, GN.GetEmployeeStatus(), 0, 1)
                        '--//---------- Script Registrations -----------//--
                        '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
                        '/--- Register Client Side Functions ---//
            txtEmpCode.Attributes.Add("onchange", "EmpCodeOnChange()")
            Me.cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            Me.cmbCadre.Attributes.Add("onchange", "return CadreOnChange()")
            Me.cmbDesignation.Attributes.Add("onchange", "ChangeReportingOfficer()")
            Me.cmbDepartment.Attributes.Add("onchange", "ChangeReportingOfficer()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Select Case CInt(Data(0))
            Case 1
                Dim EmpCode As Integer = CInt(Data(1))
                DT = DB.ExecuteDataSet("select count(*) from emp_master where status_id = 1 and emp_code = " & EmpCode & "").Tables(0)
                If (DT.Rows(0)(0) > 0) Then
                    DT = DB.ExecuteDataSet("select Emp_Name,cast(a.Branch_ID as varchar(8)) + '|' + cast(b.status_id as varchar(4)),Department_ID,Cadre_ID,Designation_ID,Date_Of_Join,Reporting_To,Post_ID,EMP_STATUS_id from emp_master a,branch_master b where emp_code = " & EmpCode & " and a.status_id = 1 and a.branch_id = b.branch_id").Tables(0)
                    CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + Format(DT.Rows(0)(5), "dd-MMM-yyyy").ToString() + "Ø" + DT.Rows(0)(6).ToString() + "Ø" + DT.Rows(0)(7).ToString() + "Ø" + DT.Rows(0)(8).ToString()
                    CallBackReturn += "Ø"
                    Dim Branch() As String = DT.Rows(0)(1).split("|")
                    DT1 = EN.GetReportingOfficer(Branch(0), DT.Rows(0)(2), DT.Rows(0)(3))
                    For Each DR As DataRow In DT1.Rows
                        CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                    Next
                Else
                    CallBackReturn = "-1Ø-1"
                End If
            Case 2
                DT = EN.GetDesignation(CInt(Data(1)))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            Case 3
                DT = EN.GetReportingOfficer(CInt(Data(1)), CInt(Data(2)), CInt(Data(3)))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            Case 4
                '"4Ø" + EmpCode + "Ø" + EmpName + "Ø" + BranchID + "Ø" + DepartmentID + "Ø" + CadreID + "Ø" + DesignationID + "Ø" + DateOfJoin + "Ø" + ReportingTo + "Ø" + EmpPost + "Ø" + EmpStatus;
                Dim EmpCode As Integer = CInt(Data(1))
                Dim EmpName As String = Data(2)
                Dim BranchID As Integer = CInt(Data(3))
                Dim DepartmentID As Integer = CInt(Data(4))
                Dim CadreID As Integer = CInt(Data(5))
                Dim DesignationID As Integer = CInt(Data(6))
                Dim DateOfJoin As Date = CDate(Data(7))
                Dim ReportingTo As Integer = CInt(Data(8))
                Dim PostID As Integer = CInt(Data(9))
                Dim EmpStatus As Integer = CInt(Data(10))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(12) As SqlParameter
                    Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(0).Value = EmpCode
                    Params(1) = New SqlParameter("@EmpName", SqlDbType.VarChar, 50)
                    Params(1).Value = EmpName
                    Params(2) = New SqlParameter("@BranchID", SqlDbType.Int)
                    Params(2).Value = BranchID
                    Params(3) = New SqlParameter("@DepartmentID", SqlDbType.Int)
                    Params(3).Value = DepartmentID
                    Params(4) = New SqlParameter("@CadreID", SqlDbType.Int)
                    Params(4).Value = CadreID
                    Params(5) = New SqlParameter("@DesignationID", SqlDbType.Int)
                    Params(5).Value = DesignationID
                    Params(6) = New SqlParameter("@DateOfJoin", SqlDbType.Date)
                    Params(6).Value = DateOfJoin
                    Params(7) = New SqlParameter("@ReportingTo", SqlDbType.Int)
                    Params(7).Value = ReportingTo
                    Params(8) = New SqlParameter("@PostID", SqlDbType.Int)
                    Params(8).Value = PostID
                    Params(9) = New SqlParameter("@EmpStatus", SqlDbType.Int)
                    Params(9).Value = EmpStatus
                    Params(10) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(10).Value = UserID
                    Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(11).Direction = ParameterDirection.Output
                    Params(12) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(12).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_EMP_DETAILS_CHANGE", Params)
                    ErrorFlag = CInt(Params(11).Value)
                    Message = CStr(Params(12).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region
End Class
