﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class StateMaster
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim MAST As New Master
    Public Event TextChanged As EventHandler



#Region "Page Load & Dispose"
    Protected Sub StateMaster_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub StateMaster_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1179) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "STATE MASTER"
            'DT = DB.ExecuteDataSet("select State_Name,State_Abbr from state_master where Status_ID=1 ").Tables(0)
            'Me.hdnData.Value = ""
            'If DT.Rows.Count > 0 Then
            '    For Each DR As DataRow In DT.Rows
            '        Me.hdnData.Value += "Ñ" & DR(0).ToString() & "ÿ" & DR(1).ToString()
            '    Next
            'End If

            Me.txt_state_name.Attributes.Add("onblur", "return StateOnchange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "State_table_fill();", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))

       
        If CInt(Data(0)) = 1 Then
            Dim Loc_Name As String = CStr(Data(1))
            Dim Abbr_Code As String = CStr(Data(2))
            Dim Type As String = "s"
            Dim PK_Id = 0
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@Loc_Name", SqlDbType.VarChar, 50)
                Params(0).Value = Loc_Name
                Params(1) = New SqlParameter("@Abbr_Code", SqlDbType.VarChar, 5)
                Params(1).Value = Abbr_Code
                Params(2) = New SqlParameter("@Type", SqlDbType.VarChar, 10)
                Params(2).Value = Type
                Params(3) = New SqlParameter("@PK_Id", SqlDbType.Int)
                Params(3).Value = PK_Id
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_Location_Reg", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "ʘ" + Message

        ElseIf CInt(Data(0)) = 2 Then

            DT = DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY state_name ) as row,upper(state_name) ,State_Abbr from state_master where Status_ID=1 order by state_name").Tables(0)

              For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString()

            Next
        ElseIf CInt(Data(0)) = 3 Then
            DT = DB.ExecuteDataSet("Select ROW_NUMBER() OVER (ORDER BY state_name ) as row,upper(state_name) ,State_Abbr from state_master where state_name like'" & CStr(Data(1)) & "%' and Status_ID=1").Tables(0)

            For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString()

            Next
           

        End If

    End Sub
#End Region

   
   
End Class
