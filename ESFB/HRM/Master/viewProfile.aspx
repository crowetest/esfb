﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="viewProfile.aspx.vb" Inherits="viewProfile" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       

        function FromServer(arg, context) {

            switch (context) {

                case 1:
                    {
                        var Data = arg.split("~");
                        if (Data[0] != "2") 
                        {
                            var empDtls = Data[1].split("Ø");
                            document.getElementById("<%= txtName.ClientID %>").value = empDtls[0];
                            document.getElementById("<%= txtLocation.ClientID %>").value = empDtls[1];
                            document.getElementById("<%= txtDept.ClientID %>").value = empDtls[2];
                            document.getElementById("<%= txtDesig.ClientID %>").value = empDtls[3];
                            document.getElementById("<%= txtPost.ClientID %>").value = empDtls[4];
                            document.getElementById("<%= txtJoinDt.ClientID %>").value = empDtls[5];                          
                        
                        }
                        else 
                        {
                            alert(Data[1]);
                            document.getElementById("<%= txtName.ClientID %>").value = "";
                            document.getElementById("<%= txtLocation.ClientID %>").value = "";
                            document.getElementById("<%= txtDept.ClientID %>").value = "";
                            document.getElementById("<%= txtDesig.ClientID %>").value = "";
                            document.getElementById("<%= txtPost.ClientID %>").value = "";
                            document.getElementById("<%= txtJoinDt.ClientID %>").value = "";                          
                            document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                        }
                        break;
                    }               
            }
        }
      
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
      
          function GetEmployeeDtls() {               
             EmpID= document.getElementById("<%= txtEmpCode.ClientID %>").value;     
            if (EmpID !="") {
                var ToData = "1Ø" + EmpID;
                ToServer(ToData, 1);
            }
        }

function btnView_onclick() {
 EmpID= document.getElementById("<%= txtEmpCode.ClientID %>").value; 
 EmpName=   document.getElementById("<%= txtName.ClientID %>").value; 
 if(EmpID=="" || EmpName=="") 
 {alert("Enter a valid Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus();return false; }
  window.open("../../ProfileIndex.aspx?TypeID=2 &EmpCode="+btoa(EmpID),"_self");
}

    </script>
  
</head>
</html>
<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;" >
           <tr id="rowText">
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Employee Code</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpCode" class="NormalText" runat="server" Width="15%" 
                MaxLength="5" onkeypress="return NumericCheck(event)" ></asp:TextBox>
            </td>
        </tr>   
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Name</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Location</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtLocation" class="ReadOnlyTextBox" runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Department</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDept" class="ReadOnlyTextBox" runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Designation</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesig" class="ReadOnlyTextBox" runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Post</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtPost" class="ReadOnlyTextBox" 
                    runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Join Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtJoinDt" class="ReadOnlyTextBox" 
                    runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
       
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 <input id="btnView" 
                    style="width: 8%; font-family: Cambria; cursor: pointer;" type="button" 
                    value="VIEW" onclick="return btnView_onclick()" />&nbsp;&nbsp;
            &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 8%;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
                 
                <asp:HiddenField ID="hdnRptID" runat="server" />
                 
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

