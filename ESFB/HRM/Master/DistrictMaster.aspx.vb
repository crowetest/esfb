﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class DistrictMaster
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim MAST As New Master



#Region "Page Load & Dispose"
    Protected Sub DistrictMaster_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub DistrictMaster_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1180) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "DISTRICT MASTER"

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            If Not IsPostBack Then
                DT = GF.GetQueryResult("select -1 as State_id,' -----SELECT-----' as State_Name UNION ALL select State_id,state_name from STATE_MASTER where Status_ID=1 ")
                GF.ComboFill(cmbState, DT, 0, 1)
            End If

            Me.cmbState.Attributes.Add("onchange", "return StateOnchange()")

            Me.txt_District.Attributes.Add("onblur", "return DistrictOnchange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "windows_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))


        If CInt(Data(0)) = 1 Then
            Dim PK_Id As Integer = CInt(Data(1))
            Dim District As String = CStr(Data(2))
            Dim Abbr_Code As String = ""
            Dim Type As String = "D"
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@PK_Id", SqlDbType.Int)
                Params(0).Value = PK_Id
                Params(1) = New SqlParameter("@Loc_Name", SqlDbType.VarChar, 50)
                Params(1).Value = District
                Params(2) = New SqlParameter("@Abbr_Code", SqlDbType.VarChar, 50)
                Params(2).Value = Abbr_Code
                Params(3) = New SqlParameter("@Type", SqlDbType.VarChar, 10)
                Params(3).Value = Type
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_Location_Reg", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "ʘ" + Message

        ElseIf CInt(Data(0)) = 2 Then
            DT = DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY District_name ) as row,upper(District_Name) as District_Name  from DISTRICT_MASTER where Status_ID=1 and  state_id=" & CInt(Data(1)) & " order by district_name").Tables(0)

            For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 3 Then
            DT = DB.ExecuteDataSet("Select ROW_NUMBER() OVER (ORDER BY District_name ) as row,upper(District_Name) as District_Name from DISTRICT_MASTER where state_id =" & CInt(Data(1)) & " and District_name like'" & CStr(Data(2)) & "%' and Status_ID=1").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString()

            Next
        End If





    End Sub
#End Region

  
End Class
