﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AddNewDesignation.aspx.vb" Inherits="HRM_Master_AddNewDesignation" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <table align="center" style="width: 80%; margin:0px auto;">
        <tr>
            <td style="width:30%;text-align:left;">
                &nbsp;</td>
            <td style="width:70%;text-align:left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%;text-align:left;">
                &nbsp;</td>
            <td style="width:70%;text-align:left;">
                </td>
        </tr>
        <tr id="rowList">
            <td style="text-align:left;" colspan="2">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr id="rowReport" style="display:none;">
            <td style="text-align:left;" colspan="2">
                <asp:Panel ID="pnReport" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr id="rowAdd" style="display:none;" >
            <td style="text-align:left;" colspan="2">
            <div style="width:50%; height:150px;  margin: 0px auto; " class="sub_first">
                <table align="center" style="margin:0px auto;width:100%;">
                    <tr>
                        <td id="subHd" colspan="2"  style="width:20%;text-align:center; color:maroon;height:30px; font-weight:bold;" class="mainhead">Add New Department</td>
                            
                    </tr>
                    <tr class="sub_second" style="height:28px;">
                        <td style="width:30%;">
                            Cadre</td>
                        <td  style="width:70%;">
                            <asp:DropDownList ID="cmbCadre" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="68%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="sub_second" style="height:28px;">
                        <td style="width:30%;">
                            Designation</td>
                        <td  style="width:70%;">
             
                <asp:TextBox ID="txtDesignation" runat="server" Width="68%" 
                    class="NormalText" MaxLength="100"  style="text-transform:uppercase;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="sub_second" style="height:40px;">
                        <td colspan="2" style="text-align:center;">
                <input id="btnSave" 
                    style="font-family: Cambria; font-size: 10pt; font-weight: normal; width: 20%;height:30px;" 
                    type="button" value="SAVE" 
                     onclick="return btnSave_onclick()" />&nbsp;&nbsp;<input id="btnExitAdd" 
                    style="font-family: Cambria; font-size: 10pt; font-weight: normal; width: 20%;height:30px;" 
                    type="button" value="EXIT"    onclick="return btnExitAdd_onclick()" /></td>
                    </tr>
                </table></div></td>
        </tr>     
        <tr>
            <td style="text-align:left;" colspan="2">
                &nbsp;</td>
        </tr>
        <tr id="rowExit">
            <td style="text-align:center;" colspan="2">
                <input id="btnExit" 
                    style="font-family: Cambria; font-size: 10pt; font-weight: normal; width: 10%; " 
                    type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
        </tr>
        <tr>
            <td style="width:30%;text-align:left;">
                <asp:HiddenField ID="hdnData" runat="server" />
            </td>
            <td style="width:70%;text-align:left;">
                &nbsp;</td>
        </tr>
    </table>
    <style type="text/css">
          #Button
        {
            width:90%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
           color:#E0E0E0;
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
            color:#036;
        }        
    
      #loadingover {
      background: black;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }
     #loadingmsg {           
      z-index: 100;  
      position:fixed; 
      top:50px;
      left:50px;
      }
        
    </style>
     <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var DesignationID = 0;
        function btnExit_onclick() {
            window.open("../../home.aspx","_self");
        }
        function OnLoad() {
            FillDesignation();
        }
        function displayChange(ID) {
            document.getElementById("rowAdd").style.display = "none";
            document.getElementById("rowList").style.display = "none";
            document.getElementById("rowExit").style.display = "none";
            document.getElementById("rowReport").style.display = "none";
            if (ID == 1) {
                document.getElementById("rowList").style.display = "";
                document.getElementById("rowExit").style.display = "";
            }
            else if (ID == 2) {
                if (DesignationID == 0) {
                    document.getElementById("btnSave").value = "ADD";
                    document.getElementById("subHd").innerHTML = "Add New Designation";
                    document.getElementById("<%= cmbCadre.ClientID %>").value = -1;
                    document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                  
                }
                else {
                    document.getElementById("btnSave").value = "UPDATE";
                    document.getElementById("subHd").innerHTML = "Edit Designation Details";
                    var rowval = document.getElementById("<%= hdnData.ClientID %>").value.split("Ñ");
                    for (n = 0; n <= rowval.length - 1; n++) {
                        colval = rowval[n].split("ÿ");
                        if (DesignationID == colval[0]) {
                            document.getElementById("<%= cmbCadre.ClientID %>").value = colval[3];
                            document.getElementById("<%= txtDesignation.ClientID %>").value = colval[1];                           
                            break;
                        }
                    }
                }
                document.getElementById("rowAdd").style.display = "";
            }
            else if (ID == 3) {
                document.getElementById("rowReport").style.display = "";
            }
        }
        function AddDesignation() {
            DesignationID = 0;
            displayChange(2);
        }
        function FillDesignation() {           
            var row_bg = 0;
            var tab = "";
            var cnt = 0;
            tab += "<div style='width:70%; height:auto; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria'; ' align='center'>";
            tab += "<tr style='height:30px;' >";
            tab += "<td style='width:5%;text-align:left ; color:#FFF' ><div id='Button' onClick='AddDesignation()' >New</div></td>";
            tab += "<td style='width:20%;text-align:center; color:Maroon' colspan=4><b>Designation List</b></td>";
            tab += "<td style='width:3%;text-align:right; color:#FFF' colspan=2> <img id='imgClose' src='../../image/close.png' onclick='return btnExit_onclick()' style='height:25px;width:25px; float:right; padding-right:5px;'></td>";
           
            tab += "</tr>";
            tab += "<tr style='height:30px; background-color:#B21C32; ' >";
            tab += "<td style='width:10%;text-align:center ; color:#FFF' ><b>#</b></td>";
            tab += "<td style='width:20%;text-align:left; color:#FFF'><b>Cadre</b></td>";
            tab += "<td style='width:40%;text-align:left; color:#FFF'><b>Designation</b></td>";
            tab += "<td style='width:20%;text-align:center; color:#FFF'><b>No Of Staff</b></td>";
            tab += "<td style='width:5%;text-align:left; color:#FFF'></td>";
            tab += "<td style='width:5%;text-align:left; color:#FFF'></td>";
            tab += "</tr>";
            tab += "</table></div>";
            tab += "<div style='width:70%; height:425px; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria'; ' align='center'>";
            if (document.getElementById("<%= hdnData.ClientID %>").value != "") {
                rowval = document.getElementById("<%= hdnData.ClientID %>").value.split("Ñ");
                for (n = 0; n <= rowval.length - 1; n++) {
                    colval = rowval[n].split("ÿ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first' style='height:28px;'  >";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second' style='height:28px;' >";
                    }
                    i = n + 1;

                    tab += "<td style='width:10%;text-align:center; '>" + i + "</td>";
                    tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" + colval[2] + "</td>";
                    tab += "<td style='width:40%;text-align:left;font-size:9pt;'>" + colval[1] + "</td>";
                    if (colval[4] == 0)
                        tab += "<td style='width:20%;text-align:center;font-size:9pt;'></td>";
                    else
                        tab += "<td style='width:20%;text-align:center;font-size:9pt;cursor:pointer;text-decoration:underline;color:blue'  title='Click to View Employee Details' onclick='RowOnClick(" + colval[0] + ")'>" + colval[4] + "</td>";  
                   
                    tab += "<td  style='width:5%;text-align:center' ><img src='../../image/edit1.png' title='Edit'  style='height:18px;width:18px;cursor:pointer'  onclick=EditOnClick(" + colval[0] + ") /></td>";
                    tab += "<td  style='width:5%;text-align:center' ><img src='../../image/delete.png' title='Delete'  style='height:18px;width:18px;cursor:pointer'  onclick=DeleteOnClick(" + colval[0] + "," + colval[4]  + ") /></td>";
                    tab += "</tr>";
                    cnt +=parseInt(colval[4]);
                }
            }
            tab += "</table></div>";            
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;

        }

        function fillReport(FillData) {
            var row_bg = 0;
            var tab = "";
            var Dtl = FillData.split("ʘ");
            var Data = Dtl[0];
            tab += "<div id='loadingmsg' style='width:90%; height:auto; margin: 0px auto;background-color:silver; ' >";
            tab += "<div  style='width:100%; height:auto; overflow-y: scroll; margin: 0px auto;border-left:2px solid silver;border-top:2px solid silver;' class='sub_first'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria'; ' align='center'>";
            tab += "<tr style='height:30px;background-color:silver;' >";
            tab += "<td style='width:5%;text-align:center;' colspan=5>Employee Details (" + Dtl[1] + ")<img id='imgClose' src='../../image/close.png' onclick='return displayChange(1)' style='height:25px;width:25px; float:right; padding-right:5px;'></td>";
            tab += "</tr>";
            tab += "<tr style='height:30px;background-color:silver; '  >";
            tab += "<td style='width:5%;text-align:center; ' ><b>#</b></td>";
            tab += "<td style='width:10%;text-align:left; '><b>Emp Code</b></td>";
            tab += "<td style='width:30%;text-align:left; '><b>Name</b></td>";
            tab += "<td style='width:30%;text-align:left;'><b>Branch</b></td>";
            tab += "<td style='width:25%;text-align:left; '><b>Department</b></td>";            
            tab += "</tr>";
            tab += "</table></div>";
            tab += "<div style='width:100%; height:auto; max-height:425px; overflow-y: scroll; margin: 0px auto;background-color:silver; '>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria'; ' align='center'>";
            if (Data != "") {
                rowval = Data.split("Ñ");
                for (n = 1; n <= rowval.length-1 ; n++) {
                    colval = rowval[n].split("ÿ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr  style='height:28px;' class='sub_first' >";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr style='height:28px;' class='sub_first'>";
                    }
                    
                    tab += "<td style='width:5%;text-align:center; '>" + n + "</td>";
                    tab += "<td style='width:10%;text-align:left;font-size:9pt;'>" + colval[0] + "</td>";
                    tab += "<td style='width:30%;text-align:left;font-size:9pt; '>" + colval[1]  + "</td>";
                    tab += "<td style='width:30%;text-align:left;font-size:9pt;'>" + colval[2] + "</td>";
                    tab += "<td style='width:25%;text-align:left;font-size:9pt;'>" + colval[3] + "</td>";                   
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div>";
            tab += "<div id='loadingover' ></div>";
            document.getElementById("<%= pnReport.ClientID %>").innerHTML = tab;
            displayChange(3);
            document.getElementById('loadingmsg').style.display = 'block';
            document.getElementById('loadingover').style.display = 'block';
        }
        function RowOnClick(ID) {
            ToServer("4ʘ" + ID, 4);      
          //  window.open("ShowEmpDetails.aspx?DepartmentID=" + btoa(ID) + "", "_blank", "toolbar=0,top=" + getOffset(document.getElementById("<%= pnDisplay.ClientID %>")).top + "px,left=" + getOffset(document.getElementById("<%= pnDisplay.ClientID %>")).left + "px,width=900,height=400,,menubar=0,center=1,channelmode =1,scrollbars=2,status=0,titlebar=0");
        }
        function EditOnClick(ID) {
            DesignationID = ID;
            displayChange(2);
        }
        function DeleteOnClick(ID, NoOfStaff) {
            if (NoOfStaff == 0) {
                if(confirm("Are you sure to delete this Designation?")==true)
                ToServer("1ʘ"+ID, 1);
            }
            else {
                // alert("There are <span style='color:red; font-weight:bold;'> live employees </span>in this department.Hence <span style='color:red; font-weight:bold;'>Deletion is not possible.</span>"); return false;
                alert("There are  live employees with this Designation.Hence Deletion is not possible."); return false;
            }
        }
        function FromServer(Arg, Context) {
            switch (Context) {
                case 1:
                    {
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        document.getElementById("<%= hdnData.ClientID %>").value = Data[2];
                        FillDesignation();
                        break;
                    }
                case 2:
                    {
                        var Dtl = Arg.split("ʘ");
                        alert(Dtl[1]);
                        if (Dtl[0] == 0) {
                            document.getElementById("<%= hdnData.ClientID %>").value = Dtl[2];
                            FillDesignation();
                            displayChange(1);
                        }
                        break;
                    }
            
                case 4:
                    {
                        fillReport(Arg);
                        break;
                    }
            }
        }
        function btnExitAdd_onclick() {
            displayChange(1);
        }



        function btnSave_onclick() {
            var CadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;
            if (CadreID == -1)
            { alert("Select Cadre"); document.getElementById("<%= cmbCadre.ClientID %>").focus(); return false; }           
            var DesignationName = document.getElementById("<%= txtDesignation.ClientID %>").value;
            if (DesignationName == "")
            { alert("Enter Designation Name"); document.getElementById("<%= txtDesignation.ClientID %>").focus(); return false; }           
            ToServer("2ʘ" + DesignationID + "ʘ" + DesignationName + "ʘ" + CadreID, 2);          
        }


    </script>
</asp:Content>

