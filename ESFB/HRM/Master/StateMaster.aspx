﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="StateMaster.aspx.vb" Inherits="StateMaster" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content3" ContentPlaceHolderID="CPH" runat="Server">

    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       
        $('input').on('keydown', function() {
     if (this.value.length > 1) {
          var ToData = "3ʘ" + document.getElementById("<%= txt_state_name.ClientID %>").value;
       
        ToServer(ToData, 3);
     }
}); 



       function StateOnchange()
        {
       
         var ToData = "3ʘ" + document.getElementById("<%= txt_state_name.ClientID %>").value;
       
        ToServer(ToData, 3);
       

        }
            function table_fill() {
       
           
            document.getElementById("<%= pnl_State.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div  style='width:50%; height:25px;padding-top:0px;margin: 0px auto; background-color:#F8A0AA ' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr style='height:30px; background-color:Maroon; color:#FFF; font-weight:bold;'>";
            tab += "<td style='width:10%;text-align:center;color:#FFF;' >#</td>";
            tab += "<td style='width:60%;text-align:left;color:#FFF;' >State Name</td>";
            tab += "<td style='width:28%;text-align:left;color:#FFF;' >State Abbreviation</td>";
            tab += "<td style='width:2%;text-align:center;color:#FFF;' ></td>";
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div style='width:50%; height:128px; overflow:auto; margin:0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";  
            if (document.getElementById("<%= hdnData.ClientID %>").value != "") {
                row = document.getElementById("<%= hdnData.ClientID %>").value.split("¥");
               
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                 
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr  style='height:30px;text-align:center; background-color:#f1f1f1; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr  style='height:30px;text-align:center;background-color:#f1f1f1; padding-left:20px;'>";
                    }
                    
                    tab += "<td style='width:10%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:60%;text-align:left'>" + col[1] + "</td>";
                     tab += "<td style='width:28%;text-align:left'>" + col[2] + "</td>";
                    tab += "</tr>";
                 
                }
            }           
            tab += "</table></div>";
            document.getElementById("<%= pnl_State.ClientID %>").innerHTML = tab;           

        }
         function State_table_fill() {

                var ToData = "2ʘ" + document.getElementById("<%= hdnData.ClientID %>").value;

                
                ToServer(ToData, 2);
       

        }
         
        function btnExit_onclick() {

            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
       
         }

       
         function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("ʘ");
                alert(Data[1]);
                if (Data[0] == 0) window.open("StateMaster.aspx", "_self");
           }
            else if (context == 2) {
               
                   document.getElementById("<%= hdnData.ClientID %>").value =arg;
             
                   table_fill();
                   
             return false;
             }
              else if (context == 3) {
               
                        
                         document.getElementById("<%= hdnData.ClientID %>").value =arg;
                     
                      
                        table_fill();
                         return false;
                    }
           else{
           }

        }
     
        
        
       function btnSave_onclick() {

                     var state_name  = document.getElementById("<%= txt_state_name.ClientID %>").value;
                     var Abb_code = document.getElementById("<%= txt_abb_no.ClientID %>").value;
                     if (state_name == "") 
                    {
                        alert("Enter State Name");
                        document.getElementById("<%= txt_state_name.ClientID %>").focus();
                        return false;
                    }
                     if (Abb_code == "") 
                    {
                        alert("Enter Abbreviation Code");
                        document.getElementById("<%= txt_abb_no.ClientID %>").focus();
                        return false;
                    }
                      
                          var ToData = "1ʘ" + state_name+ "ʘ" + Abb_code;
                         
                            ToServer(ToData, 1);
}

        
        
    </script>
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
     </script>
      
</head>
</html>

  <table align="center" style="width: 100%; margin: 0px auto;">
                <tr>
                    <td style="width: 27%; text-align: right;">
                      
                    </td>
                    
                    <td style="width: 32%; text-align: center; cursor:pointer;">
                        &nbsp;</td>
                   
                </tr>
               
                <tr id="Tr1">
                    <td style="width: 27%; text-align: right;">
                         State Name&nbsp;&nbsp;
                     </td>
                    
                    <td style="width: 32%; text-align: left;">
                        <asp:TextBox ID="txt_state_name" runat="server" class="NormalText" 
                            Height="20px" Width="286px"></asp:TextBox>
                    </td>
                    
                </tr>
                <tr id="Tr2">
                    <td style="width: 27%; text-align: right;">
                        State Abbreviation&nbsp;&nbsp;</td>
                    
                    <td style="width: 32%; text-align: left;">
                        <asp:TextBox ID="txt_abb_no" runat="server" height="20px" width="286px"></asp:TextBox>
                    </td>
                 
                </tr>
               
               <tr>
               <td style="width: 27%; text-align: right;"> </td>
               <td style="width: 32%; text-align: left;"></td>
                 
               </tr> 
                <tr>
                     <td style="width:27%; text-align: center;" colspan="2" align='center'>
                         <asp:Panel ID="pnl_State" style="width:100%; text-align:right;float:right; " runat="server">
                         </asp:Panel>
                         <asp:HiddenField ID="hdnData" runat="server" />
                     </td>
                     
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="2">
                        &nbsp;&nbsp;<input
                        id="btnSave" type="button" value="SAVE"  style="font-family: cambria;
                        width: 6%; cursor: pointer;"   onclick="return btnSave_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input
                        id="btnExit" type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria;
                        width: 6%; cursor: pointer; height: 26px;" onclick="return btnExit_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   
                    </td>
                   
                </tr>
               
            </table>
   
<br /><br />
</asp:Content>

















