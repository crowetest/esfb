﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="MasterData.aspx.vb" Inherits="MasterData" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

       function TypeOnChange() {
            var SubGroupid = document.getElementById("<%= cmbModule.ClientID %>").value;
            if (SubGroupid==3) {
                document.getElementById("txtType").value="";
            }
            else
            {
             document.getElementById("txtType").value="";
            }
        } 
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("MasterData.aspx", "_self");
           }
           else{
           }

        }
        
        function btnApprove_onclick() {
            if (document.getElementById("<%= cmbModule.ClientID %>").value == "-1") {
                alert("Select Type");
                document.getElementById("<%= cmbModule.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtType.ClientID %>").value == "-1") {
                alert("Enter New Value");
                document.getElementById("<%= txtType.ClientID %>").focus();
                return false;
            }
            var TypeVal = document.getElementById("<%= cmbModule.ClientID %>").value;
            var NewValue = document.getElementById("<%= txtType.ClientID %>").value;
            var Data = "1Ø" + TypeVal + "Ø" + NewValue+"Ø"+document.getElementById("<%= cmbRole.ClientID %>").value;
            ToServer(Data, 1);
        }
        
        
    </script>
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
</script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_sum" runat="server" />
<br />

    <table class="style1" style="width:100%">
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12%; text-align:left; ">
                Select&nbsp; Type</td>
            <td style="width:63%;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbModule" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black">
                    <asp:ListItem Value="-1">---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1">Qualification</asp:ListItem>
                    <asp:ListItem Value="2">University</asp:ListItem>
                    <asp:ListItem Value="3">ID Proof</asp:ListItem>
                    <asp:ListItem Value="4">Address Proof</asp:ListItem>
                    <asp:ListItem Value="5">Languages</asp:ListItem>
                    <asp:ListItem Value="6">Family Type</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
       
        <tr> 
        <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">New Value</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtType" class="NormalText"
                runat="server" Width="60%" 
                MaxLength="100" ></asp:TextBox>
            
            </td>
       </tr>
       <tr id="Role" style="display:none;">
        <td style="width:25%;"></td>
            <td style="width:12%; text-align:left; ">
                Select&nbsp; Role</td>
            <td style="width:63%;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbRole" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black">
                    <asp:ListItem Value="-1">---------Select---------</asp:ListItem>
                   </asp:DropDownList>
            </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">
                &nbsp;</td>
            <td style="width:63%;text-align:left; ">
                
            </td>
        </tr>
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3"><br />
                <asp:HiddenField ID="hid_approve" runat="server" />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 8%;" 
                    type="button" value="SAVE" onclick="return btnApprove_onclick()" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

