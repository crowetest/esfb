﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class ShowEmpDetails
    Inherits System.Web.UI.Page
    Dim DepartmentID As Integer
    Dim DepartmentName As String
    Dim GN As New GeneralFunctions
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Protected Sub ShowClientDetails_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DepartmentID = CInt(GN.Decrypt(Request.QueryString.Get("DepartmentID")))
            DepartmentName = GN.GetDepartment_Name(DepartmentID)
            Dim Sql As String
            Sql = "select emp_code,upper(emp_name),upper(branch_name),upper(designation_name) from Emp_List  where department_id=" + DepartmentID.ToString() + " and status_id=1"
            DT = DB.ExecuteDataSet(Sql).Tables(0)
            If DT.Rows.Count = 0 Then
                Return
            End If
            tb.Attributes.Add("style", "font-family:Cambria")
            RH.Heading(Session("FirmName"), tb, "EMPLOYEES OF " + DepartmentName.ToUpper() + " DEPARTMENT", 100)
            Dim RowHeight As Integer = 25
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.White
            TRHead.Height = RowHeight
            Dim TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell
            RH.InsertColumn(TRHead, TRHead_01, 5, WholeHelper.ClsRepCtrl.Allign.Centre, "#")
            RH.InsertColumn(TRHead, TRHead_02, 11, WholeHelper.ClsRepCtrl.Allign.Left, "Emp Code")
            RH.InsertColumn(TRHead, TRHead_03, 28, WholeHelper.ClsRepCtrl.Allign.Left, "Name")
            RH.InsertColumn(TRHead, TRHead_04, 28, WholeHelper.ClsRepCtrl.Allign.Left, "Branch")
            RH.InsertColumn(TRHead, TRHead_05, 28, WholeHelper.ClsRepCtrl.Allign.Left, "Designation")
            tb.Controls.Add(TRHead)
            Dim i As Integer = 0
            For Each DR As DataRow In DT.Rows
                Dim TR01 As New TableRow
                TR01.BackColor = Drawing.Color.White
                TR01.Height = RowHeight
                i += 1
                Dim TR01_01, TR01_02, TR01_03, TR01_04, TR01_05 As New TableCell
                RH.InsertColumn(TR01, TR01_01, 5, WholeHelper.ClsRepCtrl.Allign.Centre, i.ToString())
                RH.InsertColumn(TR01, TR01_02, 11, WholeHelper.ClsRepCtrl.Allign.Left, DR(0))
                RH.InsertColumn(TR01, TR01_03, 28, WholeHelper.ClsRepCtrl.Allign.Left, DR(1))
                RH.InsertColumn(TR01, TR01_04, 28, WholeHelper.ClsRepCtrl.Allign.Left, DR(2))
                RH.InsertColumn(TR01, TR01_05, 28, WholeHelper.ClsRepCtrl.Allign.Left, DR(3))
                tb.Controls.Add(TR01)
            Next
            pnDisplay.Controls.Add(tb)
            RH.BlankRow(tb, 20)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub
End Class
