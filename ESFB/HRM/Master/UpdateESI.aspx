﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="UpdateESI.aspx.vb" Inherits="UpdateESI" EnableEventValidation="false"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head><title>
</title>
<style type="text/css">
      #loadingmsg {
      color: red;
      background: #fff; 
      padding: 10px;
      position: fixed;
      top: 50%;
      left: 50%;
      z-index: 100;
      margin-right: -25%;
      margin-bottom: -25%;
      }
      #loadingover {
      background: black;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }
</style>
<link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
 <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
// <![CDATA[
 function showLoading() {
    document.getElementById('loadingmsg').style.display = 'block';
    document.getElementById('loadingover').style.display = 'block';
}
function closeLoading(){
        document.getElementById('loadingmsg').style.display = 'none';
        document.getElementById('loadingover').style.display = 'none';
}

function toProperCase(s)
{
  return s.toLowerCase().replace( /\b((m)(a?c))?(\w)/g,
          function($1, $2, $3, $4, $5) { if($2){return $3.toUpperCase()+$4+$5.toUpperCase();} return $1.toUpperCase(); });
}


function FromServer(arg, context) {
    switch (context) {
        case 1:
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) {
                FillDetails_1();
                }
            break;
        case 2:
            document.getElementById("<%= hdnItems.ClientID %>").value=arg;
            document.getElementById("<%= hdnDisplay.ClientID %>").value=arg;
            table_Head();
            table_Fill("");
            closeLoading();
            break;            
    }
}
function table_Head()
{
    var tab = "";
    tab += "<div style='width:100%;  height:auto; margin: 0px auto; overflow-y:scroll' class=mainhead>";
    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
    tab += "<tr height=30px;>";
    tab += "<td style='width:5%;text-align:center' >#</td>";
    tab += "<td style='width:10%;text-align:left' >Emp Code&nbsp;<input type='text' class='NormalText' id='txtEmpCode' style='width:40%;' maxlength='5' onkeyup='EmpCodeOnKeyPress()' onchange='EmpCodeOnChange()'  onblur='EmpCodeOnChange()'  /></td>";
    tab += "<td style='width:20%;text-align:left' >Name</td>";
    tab += "<td style='width:20%;text-align:left' >Branch</td>";
    tab += "<td style='width:20%;text-align:left' >Designation</td>";           
    tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSelectAll' onclick='SelectOnClick(-1)' /></td>";          
    tab += "<td style='width:20%;text-align:left'>ESIC IP Number</td>";
    tab += "</tr>";
    tab += "</table></div>";
    document.getElementById("<%= pnDispHead.ClientID %>").style.display = '';
    document.getElementById("<%= pnDispHead.ClientID %>").innerHTML = tab;
}

function table_Fill() {
    var tab = "";
    var row_bg = 0;
    var Data=document.getElementById("<%= hdnDisplay.ClientID %>").value;
    document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
    if(Data!="")
    {
    tab += "<div id='ScrollDiv' style='width:100%; height:283px;overflow: scroll;margin: 0px auto;' class=mainhead >";
    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='left'>";
         
    var row =Data.split("¥");
    for (n = 0; n <= row.length - 1; n++) {
        var col = row[n].split("µ"); 
     
            if (row_bg == 0) {
                row_bg = 1;
                tab += "<tr class='sub_first'; style='text-align:center;padding-left:20px;'>";
            }
            else {
                row_bg = 0;
                tab += "<tr class='sub_second'; style='text-align:center; padding-left:20px;'>";
            }
            var i = n + 1;               
            tab += "<td style='width:5%;text-align:center;'>" + i + "</td>";
            tab += "<td style='width:10%;text-align:left'>" + col[0] + "</td>";
            tab += "<td style='width:20%;text-align:left'>" + toProperCase(col[1]) + "</td>";
            tab += "<td style='width:20%;text-align:left'>" + toProperCase(col[2]) + "</td>";
            tab += "<td style='width:20%;text-align:left'>" + toProperCase(col[3]) + "</td>";
            if(document.getElementById("chkRemove").checked==true)
            tab += "<td style='width:5%;text-align:center '><input type='checkbox' id='chkSelect" + col[0] + "' /></td>";
            else
            tab += "<td style='width:5%;text-align:center '><input type='checkbox' id='chkSelect" + col[0] + "' onchange=SelectOnClick(" + col[0] + ") /></td>";
            tab += "<td style='width:20%;text-align:left'><input type='text' class='NormalText' id='txtIPNo"+ col[0] +"' value='"+ col[4] +"' style='width:99%;' maxlength='50' disabled=true; /></td>";         
            tab += "</tr>";           
        }    
    tab += "</table></div></div>";
    }           
    document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;
}
function EmpCodeOnChange(){
    var EmpCode=document.getElementById("txtEmpCode").value;
    FilterString(EmpCode);
}

function EmpCodeOnKeyPress()
{
    var EmpCode=document.getElementById("txtEmpCode").value;
    if(EmpCode.length>2)
        FilterString(EmpCode);
}
function FilterString(Str){
   var Data=document.getElementById("<%= hdnItems.ClientID %>").value;
   document.getElementById("<%= hdnDisplay.ClientID %>").value="";
   var row =Data.split("¥");
    for (n = 0; n <= row.length - 1; n++) {
     var col = row[n].split("µ"); 
     var matches = col[0].indexOf(Str) >= 0 ? true : false;
     if(matches)
     {
         if(document.getElementById("<%= hdnDisplay.ClientID %>").value!="")
            document.getElementById("<%= hdnDisplay.ClientID %>").value+="¥";
         document.getElementById("<%= hdnDisplay.ClientID %>").value+=  row[n];
     }
    }
    table_Fill();
}
function CloseOnClick() {
    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
}
function SelectOnClick(ID)
{
    var Data=document.getElementById("<%= hdnDisplay.ClientID %>").value;
    var row =Data.split("¥");
    if(ID<0)
    {              
        for (n = 0; n <= row.length - 1; n++) {
            var col = row[n].split("µ"); 
            if(document.getElementById("chkSelectAll").checked==true)     
            {
                document.getElementById("chkSelect"+col[0]).checked=true;
                document.getElementById("txtIPNo"+col[0]).disabled=false;                      
            }
            else
            {
                document.getElementById("chkSelect"+col[0]).checked=false;
                document.getElementById("txtIPNo"+col[0]).disabled=true ;                      
            }
                   
        }
    }
    else
    {
               
        if(document.getElementById("chkSelect"+ID).checked==true)
        {
            
            document.getElementById("txtIPNo"+ID).disabled=false;
                 
        }
        else
        {
            document.getElementById("txtIPNo"+ID).value="";
            document.getElementById("txtIPNo"+ID).disabled=true ;                  
        }
        var p=0;
        for (n = 0; n <= row.length - 1; n++) {
            var col = row[n].split("µ");
            if(document.getElementById("chkSelect"+col[0]).checked==true) 
            {
                p+=1;
            }
        }
        if(p== row.length)
            {document.getElementById("chkSelectAll").checked=true;}
        else
            document.getElementById("chkSelectAll").checked=false;
    }
}
       
function btnSave_onclick()
{
        var Data=document.getElementById("<%= hdnDisplay.ClientID %>").value;
        var row =Data.split("¥");
        var j=0;
        var Flag=0;
        if(document.getElementById("chkAdd").checked==true)
            Flag=1;
        document.getElementById("<%= hdnValue.ClientID %>").value="";
        if(Data!="")
        {
        for (n = 0; n <= row.length - 1; n++) {
            var col = row[n].split("µ"); 
            if(document.getElementById("chkSelect"+col[0]).checked==true)     
            {        
                var IPNo=document.getElementById("txtIPNo"+col[0]).value;
                if(IPNo.length<10)
                {alert("Enter a Valid IP Number");document.getElementById("txtIPNo"+col[0]).focus();return false;}
                document.getElementById("<%= hdnValue.ClientID %>").value+="¥"+col[0]+"µ"+IPNo;
                j+=1;
            }                
                 
        } }     
        var Funct=" Add "  ;
        var Prep=" To "
        if(Flag==0)  
        {
            Funct=" Remove  "  ; 
            Prep=" From " 
        } 
        if(confirm("Are You sure to "+ Funct +" the selected "+ j +" Employee(s)"+Prep+" ESIC" )==true)  
        {
            var ToData = "1Ø"+ Flag +"Ø" + document.getElementById("<%= hdnValue.ClientID %>").value;
            ToServer(ToData, 1);     
        }            
                  
}
function FillDetails()
{  showLoading();
    var BranchID=document.getElementById("<%= ddlBranch.ClientID %>").value;  
    var CadreID=document.getElementById("<%= ddlCadre.ClientID %>").value;
    var Flag=1;
    if(document.getElementById("chkAdd").checked==true)
        Flag=0;
    var ToData = "2Ø" + BranchID+"Ø"+Flag+"Ø"+CadreID;
                ToServer(ToData,2);
         
}

function FillDetails_1(){ 
    var BranchID=document.getElementById("<%= ddlBranch.ClientID %>").value;  
    var CadreID=document.getElementById("<%= ddlCadre.ClientID %>").value;
    var Flag=1;
    if(document.getElementById("chkAdd").checked==true)
        Flag=0;
    var ToData = "2Ø" + BranchID+"Ø"+Flag+"Ø"+CadreID;
                ToServer(ToData,2);
         
}



function btnGenerate_onclick() {
        FillDetails();
}

// ]]>
    </script>
</head>
<div> </div>
    <table align="center" style="width: 90%;text-align:center; margin:0px auto;">
        <tr>
            <td style="width:45%; text-align:left;">
                <asp:HiddenField ID="hdnItems" runat="server" />
            </td>
            <td style="width:55%;text-align:left;">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            </td>
        </tr>
        <tr>
            <td style="width:45%; text-align:right;">
                <asp:HiddenField ID="hdnDisplay" runat="server" />
            </td>
            <td style="width:55%;text-align:right;">
             
                <asp:HiddenField ID="hdnValue" runat="server" />
             
        </tr>
        <tr>
            <td style="width:45%; text-align:right;">
                Branch&nbsp;&nbsp;</td>
            <td style="width:55%;text-align:left;">
                <asp:DropDownList ID="ddlBranch" runat="server" CssClass="NormalText" 
                    Width="59%">
                </asp:DropDownList>
                &nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td style="width:45%; text-align:right;">
                Cadre&nbsp;&nbsp;</td>
            <td style="width:55%;text-align:left;">
                <asp:DropDownList ID="ddlCadre" runat="server" CssClass="NormalText" 
                    Width="59%">
                </asp:DropDownList>
                </td>
        </tr>
        <tr>
            <td style="width:45%; text-align:right;">
                &nbsp;</td>
            <td style="width:55%;text-align:left;">
                <input id="chkAdd" type="radio" name="opt" onclick="FillDetails()" 
                    checked="checked" />&nbsp;Add&nbsp;&nbsp;&nbsp;&nbsp;<input 
                    id="chkRemove" type="radio" name="opt" onclick="FillDetails()" />&nbsp;Remove</td>
        </tr>
        <tr>
            <td style="text-align:center;" colspan="2">
                <input id="btnGenerate" type="button" value="VIEW" 
                      style="font-family: Cambria; font-size: 10pt; width: 10%" onclick="return btnGenerate_onclick()" /></td>
        </tr>
        <tr>
            <td style="width:45%; text-align:right;">
                <div id='loadingmsg' style='display: none;'>Loading, please wait...</div>
                <div id='loadingover' style='display: none;'></div>
             
</td>
            <td style="width:55%;text-align:left;">
                  &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;" colspan="2">
                <asp:Panel ID="pnDispHead" runat="server" 
                    style="width:100%; text-align:center; margin:opx auto;">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                </asp:Panel>
            </td>
        </tr>
       
        <tr>
            <td style="text-align:center;" colspan="2">
                <asp:Panel ID="pnDisplay" runat="server" style="width:100%; text-align:center; margin:opx auto;">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                </asp:Panel>
            </td>
        </tr>
       
        <tr>
            <td style="text-align:center;" colspan="2">
                &nbsp;</td>
        </tr>
         <tr>
            <td style="text-align:center;" colspan="2">
                    <input id="btnSave" type="button" value="SAVE" 
                      
    style="font-family: Cambria; font-size: 10pt; width: 8%" 
    onclick="return btnSave_onclick()"  />&nbsp;&nbsp;&nbsp;<input id="btnExit" type="button" value="EXIT" 
                      
    style="font-family: Cambria; font-size: 10pt; width: 8%" 
    onclick="return CloseOnClick()"  /></td>
        </tr>
        <tr>
            <td style="width:45%; text-align:right;">
                </td>
            <td style="width:55%;text-align:left;">
                  </td>
        </tr>
        <tr>
            <td style="width:45%; text-align:right;">
                <asp:HiddenField ID="hdnStartDate" runat="server" />
            </td>
            <td style="width:55%;text-align:left;">
                  &nbsp;</td>
        </tr>
        <tr>
            <td style="width:45%; text-align:right;">
                &nbsp;</td>
            <td style="width:55%;text-align:left;">
                  &nbsp;</td>
        </tr>
    </table>
</asp:Content>

