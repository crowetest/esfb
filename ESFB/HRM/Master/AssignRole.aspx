﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AssignRole.aspx.vb" Inherits="AssignRole" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#3F719A;
             cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #EEB8A6 90%, #F1F3F4 100%);
        }
        .Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #A34747 0%, #A34747 0%, #A34747  100%);
            color:#FFF;
        }   
     .bg
     {
         background-color:#FFF;
     }
     </style>
    <script language="javascript" type="text/javascript">
    function delete_row(Roleid){
         if (confirm("Are You sure to Delete?")==1){
                var ToData = "6Ø" + document.getElementById("<%= txtEmpCode.ClientID %>").value + "Ø" +Roleid;
                ToServer(ToData, 6);
         }

    }
    function table_fill() 
        {   document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<br/><div class=mainhead style='width:85%; height:25px; padding-top:0px;margin:0px auto;background-color:#ADC2D6;' ><table style='width:100%;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            if (document.getElementById("<%= hid_tab.ClientID %>").value==1){
                tab += "<td style='width:15%;text-align:center;' class=NormalText>#</td>";
                tab += "<td style='width:75%;text-align:left' class=NormalText>Role Name</td>";
                
            }
           
            else if (document.getElementById("<%= hid_tab.ClientID %>").value==3){
                tab += "<td style='width:15%;text-align:center;' class=NormalText>#</td>";
                tab += "<td style='width:75%;text-align:left' class=NormalText>Role Name</td>";
                tab += "<td style='width:5%;'/></td>";
            }
           
            tab += "</tr></table></div><div style='width:85%; height:128px; overflow:auto; margin:0px auto;background-color:#ADC2D6;' ><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
           
              if(document.getElementById("<%= hid_dtls.ClientID %>").value!="")
              {
                    row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
               
                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                   
                                if (row_bg == 0) {
                                    row_bg = 1;
                                    tab += "<tr class=sub_first>";
                                }
                                else {
                                    row_bg = 0;
                                    tab += "<tr class=sub_second>";
                                }
                            
                                tab += "<td style='width:15%;text-align:center;' >" + col[0] + "</td>";
                                tab += "<td style='width:75%;text-align:left;' >" + col[1] + "</td>";
                                 if (document.getElementById("<%= hid_tab.ClientID %>").value==3){
                                        tab += "<td style='width:5%;text-align:center' onclick='delete_row(" + col[2] + ")'><img  src='../../Image/cross.png' style='align:middle;cursor:pointer;' /></td>";
                                  }
                                tab += "</tr>";
                            }
             }       
            tab += "</table></div>";
           
            document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }
        function Allcheck(){
         if (document.getElementById("txtmain").checked == 1) {
                    row = document.getElementById("<%= hid_form_dtls.ClientID %>").value.split("¥");
                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");

                       
                        document.getElementById("chk" + col[0]).checked = 1;
                    }
            }
            else{
                    row = document.getElementById("<%= hid_form_dtls.ClientID %>").value.split("¥");
                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                            if(col[3]==1){
                                document.getElementById("chk" + col[0]).checked = 1;
                            }
                            else{
                                document.getElementById("chk" + col[0]).checked = 0;
                            }
                    }
        }
        }
         function table_fill_Forms() 
        {
            document.getElementById("<%= pnlForms.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
           tab += "<br/><div class=mainhead style='width:85%; height:25px; padding-top:0px;margin:0px auto;background-color:#ADC2D6;' ><table style='width:100%;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:5%;text-align:center;'>#</td>";
            tab += "<td style='width:25%;text-align:left' >MENU NAME</td>";
            tab += "<td style='width:25%;text-align:left' >FORM NAME</td>";
            tab += "<td style='width:10%;text-align:center'><input id='txtmain' type='checkbox' onclick='Allcheck()'/></td>";
            tab += "</tr></table></div><div style='width:85%; height:128px; overflow:auto; margin:0px auto;background-color:#ADC2D6;' ><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            
            if (document.getElementById("<%= hid_form_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_form_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                   

                    tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                    tab += "<td style='width:40%;text-align:left'>" + col[1] + "</td>";
                     tab += "<td style='width:45%;text-align:left'>" + col[2] + "</td>";
                   if(col[3]==1){
                   var Val=" checked='true' ";
                   }
                   else{
                    var Val="";
                   }
                    var txtid = "chk" + col[0];
                    tab += "<td style='width:10%;text-align:center'><input id='" + txtid + "' type='checkbox'  " + Val +" /></td>";
                    tab += "</tr>";
                }
            }
            tab += "</table></div>";
            document.getElementById("<%= pnlForms.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }

        function UpdateValue() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_form_dtls.ClientID %>").value == "") {
                return false;
            }
           
            row = document.getElementById("<%= hid_form_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");

                var txtName = "chk" + col[0];
                
                if (document.getElementById("chk" + col[0]).checked == true) {

                    document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[0];
                }
               
            }
            
            return true;
        }
        function RoleOnchange() {

            var Data = document.getElementById("<%= CmbRoleType.ClientID %>").value;
            if (Data != -1 && Data !="" && document.getElementById("<%= cmbGroup.ClientID %>").value!=-1 && document.getElementById("<%= cmbGroup.ClientID %>").value!="") {
                var ToData = "2Ø" + Data + "Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value + "Ø" +document.getElementById("<%= cmbGroup.ClientID %>").value;
                ToServer(ToData, 2);
            }
            
            

        }
       function RoleFill() {

                var ToData = "4Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
                ToServer(ToData, 4);
       

        }
        function RoleTableFill() {

                var ToData = "5Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
                ToServer(ToData, 5);
       

        }
       function RequestID() {
              var EmpID = document.getElementById("<%= txtEmpCode.ClientID %>").value;
              if (EmpID > 0) {
                  var ToData = "3Ø" + EmpID;
                  ToServer(ToData,3);
              }
              else{
                document.getElementById("<%= CmbRoleType.ClientID %>").value=-1;
                document.getElementById("<%= txtEmpCode.ClientID %>").value="";
                document.getElementById("<%= txtBranch.ClientID %>").value="";
                document.getElementById("<%= txtEmpName.ClientID %>").value="";
                document.getElementById("<%= pnlDtls.ClientID %>").style.display='none';
                
              }
          }
        function FromServer(arg, context) {
            if (context == 1) {
                
                var Data = arg.split("Ø");
                  alert(Data[1]);
                  if (Data[0] == 0) {
                       if (document.getElementById("<%= hid_Tab.ClientID %>").value ==1){
                            document.getElementById("<%= txtRoleName.ClientID %>").value = "";
                            RoleTableFill();
                            
                            
                       }
                       else if (document.getElementById("<%= hid_Tab.ClientID %>").value ==2){
                             RoleOnchange();

                       }
                       else if (document.getElementById("<%= hid_Tab.ClientID %>").value ==3){
                          RequestID();
                       }
                   
                    }
            }
            else if (context == 2) {
                var Data = arg.split("~");
            
                    document.getElementById("<%= hid_form_dtls.ClientID %>").value = Data[1];
                    table_fill_Forms();
                
                
                return false;
                }
             else if (context == 3) {
                var Data = arg.split("~");
                    if(Data[0]==1){
                        var StrValue =Data[1].split("|");
                        var Val =StrValue[0].split("^");
                        document.getElementById("<%= txtEmpName.ClientID %>").value = Val[1];
                        document.getElementById("<%= txtBranch.ClientID %>").value = Val[3] +'-' +Val[5]+'-'+Val[6];
                        document.getElementById("<%= hid_dtls.ClientID %>").value =StrValue[1];
                        
                        table_fill();
                    }
                    else{
                    
                    document.getElementById("<%= txtEmpCode.ClientID %>").value = "";
                    document.getElementById("<%= txtEmpName.ClientID %>").value = "";
                    document.getElementById("<%= txtBranch.ClientID %>").value = "";
                    document.getElementById("<%= txtDep.ClientID %>").value = "";
                    document.getElementById("<%= txtDesig.ClientID %>").value = "";
                    document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                    }
                
                
                return false;
            }
             else if (context == 4) {
                
                    ComboFill(arg, "<%= cmbRoleType.ClientID %>");
                   
             return false;
             }
             else if (context == 5) {
               
                   document.getElementById("<%= hid_dtls.ClientID %>").value =arg;
                   table_fill();
                   
             return false;
             }
              else if (context == 6) {
               
                   var Data = arg.split("~");
                    if(Data[0]==1){
                        var StrValue =Data[1].split("|");
                        alert(StrValue[0]);
                        
                        document.getElementById("<%= hid_dtls.ClientID %>").value =StrValue[1];
                        
                        table_fill();
                    }
                    else{
                    alert(Data[1]);
                    }
                   
             return false;
             }
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        
        
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnSave_onclick() {

            if (document.getElementById("<%= hid_Tab.ClientID %>").value ==1){
                 
                 if (document.getElementById("<%= txtRoleName.ClientID %>").value == "") 
                {
                    alert("Enter Role Name");
                    document.getElementById("<%= txtRoleName.ClientID %>").focus();
                    return false;
                }
                   
	             var ToData = "1Ø"  + document.getElementById("<%= hid_Tab.ClientID %>").value+ "Ø" + document.getElementById("<%= txtRoleName.ClientID %>").value;   
           
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value ==2){
                    if (document.getElementById("<%= cmbRoleType.ClientID %>").value == "-1") 
                    {
                        alert("Select Role Type");
                        document.getElementById("<%= cmbRoleType.ClientID %>").focus();
                        return false;
                    }
                    UpdateValue();
                    if ( document.getElementById("<%= hid_temp.ClientID %>").value ==""){
                        alert("Select atleast one form");
                        return false;
                    }
                 var ToData = "1Ø"  + document.getElementById("<%= hid_Tab.ClientID %>").value+ "Ø" + document.getElementById("<%= cmbRoleType.ClientID %>").value+ "Ø" + document.getElementById("<%= cmbGroup.ClientID %>").value+ "Ø" +document.getElementById("<%= hid_temp.ClientID %>").value;   
                   
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value ==3){
                    
                 if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "") 
                {
                    alert("Enter Employee Code");
                    document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                    return false;
                }
                     if (document.getElementById("<%= CmbRoleType.ClientID %>").value == "-1") 
                    {
                        alert("Select Role Type ");
                        document.getElementById("<%= CmbRoleType.ClientID %>").focus();
                        return false;
                    }
                    
               var ToData = "1Ø"  + document.getElementById("<%= hid_Tab.ClientID %>").value+ "Ø" + document.getElementById("<%= cmbRoleType.ClientID %>").value+ "Ø" + document.getElementById("<%= txtEmpCode.ClientID %>").value;    
	                                    
            }
            
	        ToServer(ToData, 1); 
	       
}
  

        function RoleNameClick() {
            document.getElementById("EmpName").style.display = 'none'
            document.getElementById("Loc").style.display = 'none'
            document.getElementById("Dep").style.display = 'none'
            document.getElementById("Desig").style.display = 'none'
            document.getElementById("Group").style.display = 'none'
            document.getElementById("EmpCode").style.display = 'none'
            document.getElementById("RoleName").style.display = ''
            document.getElementById("Roles").style.display = 'none'
            document.getElementById("Forms").style.display = 'none'
            document.getElementById("List").style.display = ''
            
            document.getElementById("<%= hid_tab.ClientID %>").value = 1;
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display='';
            document.getElementById("<%= pnlForms.ClientID %>").style.display='none';

            document.getElementById("1").style.background='-moz-radial-gradient(center, ellipse cover,  #A34747 0%, #A34747 0%, #A34747 100%)';
            document.getElementById("1").style.color='#FFF';
             document.getElementById("2").style.background='-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #EEB8A6 90%, #F1F3F4 100%)';
            document.getElementById("2").style.color='#3F719A';
             document.getElementById("3").style.background='-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #EEB8A6 90%, #F1F3F4 100%)';
            document.getElementById("3").style.color='#3F719A';
            
            
            document.getElementById("<%= CmbRoleType.ClientID %>").value=-1;
            document.getElementById("<%= txtRoleName.ClientID %>").value="";
            document.getElementById("<%= txtEmpCode.ClientID %>").value="";
            document.getElementById("<%= txtBranch.ClientID %>").value="";
            document.getElementById("<%= txtEmpName.ClientID %>").value="";
            RoleTableFill();
            
        }
        function RoleAssignClick() {         

            document.getElementById("EmpName").style.display = 'none'
            document.getElementById("Group").style.display = ''
            document.getElementById("Loc").style.display = 'none'
            document.getElementById("Dep").style.display = 'none'
            document.getElementById("Desig").style.display = 'none'
            document.getElementById("EmpCode").style.display = 'none'
            document.getElementById("RoleName").style.display = 'none'
            document.getElementById("Roles").style.display = ''
            document.getElementById("Forms").style.display = ''
            document.getElementById("List").style.display = 'none'
            
            document.getElementById("<%= hid_tab.ClientID %>").value = 2;
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display='none';
            document.getElementById("<%= pnlForms.ClientID %>").style.display='';

            
           
            document.getElementById("1").style.background='-moz-radial-gradient(center, ellipse cover, #F1F3F4 20%, #EEB8A6 90%, #F1F3F4  100%)';
            document.getElementById("1").style.color='#3F719A';
             document.getElementById("2").style.background='-moz-radial-gradient(center, ellipse cover,#A34747 0%, #A34747 0%, #A34747 100%)';
            document.getElementById("2").style.color='#FFF';
             document.getElementById("3").style.background='-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #EEB8A6 90%, #F1F3F4 100%)';
            document.getElementById("3").style.color='#3F719A';

            document.getElementById("<%= CmbRoleType.ClientID %>").value=-1;
            document.getElementById("<%= txtRoleName.ClientID %>").value="";
            document.getElementById("<%= txtEmpCode.ClientID %>").value="";
             document.getElementById("<%= txtBranch.ClientID %>").value="";
             document.getElementById("<%= txtEmpName.ClientID %>").value="";
            RoleFill();
            
        }
        function EmpRoleAssignClick() {
            
            document.getElementById("EmpCode").style.display = ''
            document.getElementById("EmpName").style.display = ''
            document.getElementById("Loc").style.display = ''
            document.getElementById("Dep").style.display = 'none'
            document.getElementById("Desig").style.display = 'none'
            document.getElementById("Group").style.display = 'none'
            document.getElementById("RoleName").style.display = 'none'
            document.getElementById("Roles").style.display = ''
            document.getElementById("Forms").style.display = 'none'
            document.getElementById("List").style.display = ''
            
            document.getElementById("<%= hid_tab.ClientID %>").value = 3;
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display='none';
            document.getElementById("<%= pnlForms.ClientID %>").style.display='none';

           
            document.getElementById("1").style.background='-moz-radial-gradient(center, ellipse cover, #F1F3F4 20%, #EEB8A6 90%, #F1F3F4  100%)';
            document.getElementById("1").style.color='#3F719A';
             document.getElementById("2").style.background='-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #EEB8A6 90%, #F1F3F4 100%)';
            document.getElementById("2").style.color='#3F719A';
             document.getElementById("3").style.background='-moz-radial-gradient(center, ellipse cover,#A34747 0%, #A34747 0%, #A34747 100%)';
            document.getElementById("3").style.color='#FFF';

            document.getElementById("<%= CmbRoleType.ClientID %>").value=-1;
            document.getElementById("<%= txtRoleName.ClientID %>").value="";
            document.getElementById("<%= txtEmpCode.ClientID %>").value="";
            document.getElementById("<%= txtBranch.ClientID %>").value="";
            document.getElementById("<%= txtEmpName.ClientID %>").value="";
        }
    </script>
</head>
</html>
 <br />
 <div style="width:60%;background-color:#A34747;margin:0px auto;height:430px;">
    <div style="width:80%;padding-left:10px;margin:0px auto;">
    <div id='1' class="Button" style="width:30%; float:left;background: -moz-radial-gradient(center, ellipse cover, #A34747 0%, #A34747 0%, #A34747  100%);color:#FFF;" onclick="return RoleNameClick()">Role Type</div>
     <div id='2' class="Button" style="border-left:thick double #FFF;width:30%;float:left;" onclick="return RoleAssignClick()">Role List</div>
      <div id='3' class="Button" style="border-left:thick double #FFF;width:30%;float:left;" onclick="return EmpRoleAssignClick()">Role Assign</div>
      
      <br /> <br />
       </div>
    <div style="width:93%;background-color:white;margin:0px auto; height:330px;border-radius:25px;">
    <br /><br />
            <table style="width:90%;height:90px;margin:0px auto;">
               
                <tr id="RoleName" >
                    <td style="width:12%;"></td><td style="width:23%; align:left;">Role Name</td> 
                    <td style="width:55%;"><asp:TextBox ID="txtRoleName" runat="server" class="NormalText" Width="61%"  Rows="3"  MaxLength="500" />
                        </td>
                    </tr>
                <tr id="EmpCode" style="display:none;">
                    <td style="width:12%;"></td><td style="width:23%; align:left;">Employee Code</td> 
                    <td style="width:55%;"><asp:TextBox ID="txtEmpCode" runat="server" class="NormalText" Width="35%"  Rows="3"  MaxLength="500" onkeypress="NumericCheck(event)"/>
                        </td>
                    </tr>
                <tr id="EmpName" style="display:none;">
                    <td style="width:12%;"></td><td style="width:23%; align:left;">Employee Name</td> 
                    <td style="width:55%;"><asp:TextBox ID="txtEmpName" class="ReadOnlyTextBox" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="3" ReadOnly="true"  MaxLength="500" />
                        </td>
                    </tr>
                <tr id="Loc" style="display:none;">
                    <td style="width:12%;"></td><td style="width:23%; align:left;">Details</td> 
                    <td style="width:55%;"><asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="3"  ReadOnly="true" MaxLength="500" />
                        </td>
                    </tr>
                    <tr id="Dep" style="display:none;">
                    <td style="width:12%;"></td><td style="width:23%; align:left;">Department</td> 
                    <td style="width:55%;"><asp:TextBox ID="txtDep" class="ReadOnlyTextBox" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="3" ReadOnly="true" MaxLength="500" />
                        </td>
                    </tr>
                    <tr id="Desig" style="display:none;">
                    <td style="width:12%;"></td><td style="width:23%; align:left;">Designation</td> 
                    <td style="width:55%;"><asp:TextBox ID="txtDesig" class="ReadOnlyTextBox" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="3" ReadOnly="true" MaxLength="500" />
                        </td>
                    </tr>

               
                <tr id="Roles" style="display:none;"> 
                    <td style="width:12%;"></td><td style="width:23%; align:left;">Select Role</td> 
                    <td style="width:55%;"><asp:DropDownList ID="cmbRoleType" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="61%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList></td>
                    </tr>
               <tr id="Group" style="display:none;"> 
                    <td style="width:12%;"></td><td style="width:23%; align:left;">Select Module</td> 
                    <td style="width:55%;"><asp:DropDownList ID="cmbGroup" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="61%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList></td>
                    </tr>
                <tr id="Forms" style="display:none;">
                                     
                    <td style="width:55%;" colspan="3"><asp:Panel ID="pnlForms" style="width:100%; text-align:right;float:right;" runat="server">
                    </asp:Panel>
                        </td>
                     </tr>

                        
                <tr id="List">  <td style="text-align:center;" colspan="3"><br /><asp:Panel ID="pnlDtls" style="width:100%; text-align:right;float:right;" runat="server">
            </asp:Panel>
                        <asp:HiddenField ID="hid_tab" runat="server" /> <asp:HiddenField ID="hid_dtls" runat="server" />
                    <asp:HiddenField ID="hid_form_dtls" runat="server" />
                    <asp:HiddenField ID="hid_temp" runat="server" />
                        </td>
                </tr>
            </table> 
            
    </div>
    <div style="text-align:center;"><br /><input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" 
                            type="button" value="SAVE" onclick="return btnSave_onclick()"  />&nbsp;
                        &nbsp;
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                            type="button" value="EXIT" onclick="return btnExit_onclick()" /></div>
    </div>   
<br /><br />
</asp:Content>

