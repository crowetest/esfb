﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class RegionMaster
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim MAST As New Master



#Region "Page Load & Dispose"
    Protected Sub RegionMaster_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub RegionMaster_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1181) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "REGION MASTER"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            If Not IsPostBack Then
                DT = GF.GetQueryResult("select -1 as holidayZoneID,' -----SELECT-----' as HolidayZone UNION ALL select holidayZoneID,HolidayZone from holiday_zone where Status_ID=1 ")
                GF.ComboFill(cmbZone, DT, 0, 1)

                DT = GF.GetQueryResult("SELECT -1 as Emp_Code,' -----Select-----' as Emp_Name Union all select Emp_code,CAST(Emp_Code as varchar(5))+'   |   ' +emp_Name from EMP_MASTER  where Status_ID=1 ")
                GF.ComboFill(cmbRegionHead, DT, 0, 1)

            End If

            Me.txt_RegionName.Attributes.Add("onblur", "return RegionNameOnChange()")
            Me.cmbRegionHead.Attributes.Add("onchange", "RegionHeadOnChange()")


            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "Region_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        If CInt(Data(0)) = 2 Then
            DT = MAST.GetEmail(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "ʘ" + DR(0).ToString()
            Next


        ElseIf CInt(Data(0)) = 1 Then
            Dim Region_name As String = CStr(Data(1))
            Dim Region_Head As Integer = CInt(Data(2))
            Dim email As String = CStr(Data(3))
            Dim Zone_id As Integer = CInt(Data(4))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@Region_name", SqlDbType.VarChar, 50)
                Params(0).Value = Region_name
                Params(1) = New SqlParameter("@Region_Head", SqlDbType.Int)
                Params(1).Value = Region_Head
                Params(2) = New SqlParameter("@email", SqlDbType.VarChar, 50)
                Params(2).Value = email
                Params(3) = New SqlParameter("@Zone_id", SqlDbType.Int)
                Params(3).Value = Zone_id
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_Region_Reg", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        ElseIf CInt(Data(0)) = 3 Then
            DT = MAST.GET_Region(CStr(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
            Next
        ElseIf CInt(Data(0)) = 4 Then
            DT = DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY Region_Name ) as row,upper(Region_Name) as Region ,upper(s.emp_name) as emp_name,b.HolidayZone from Region_master a inner join holiday_zone b on a.Zone_ID=b.HolidayZoneID inner join emp_master s on s.emp_code=a.Region_head  where A.Status_ID=1 order by Region_name").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
            Next

        End If
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
End Class
