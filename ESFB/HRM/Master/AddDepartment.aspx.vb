﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class HRM_Master_AddDepartment
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim Mode As Char = "A"
    Dim DepartmentID As Integer
    Protected Sub HRM_Master_AddDepartment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            txtEmpCode.Attributes.Add("onchange", "return EmpCodeOnChange()")
            Mode = GN.Decrypt(Request.QueryString.Get("Mode"))
            If (Mode = "E") Then
                DepartmentID = GN.Decrypt(Request.QueryString.Get("DepartmentID"))
                DT = DB.ExecuteDataSet("select upper(a.department_name),isnull(a.department_head,''),isnull(upper(b.emp_name),''),isnull(a.email,''),isnull(a.phone,'') from department_master a left outer join emp_master b on( a.department_head=b.emp_code ) where a.Department_ID=" + DepartmentID.ToString() + "").Tables(0)
                txtDepartment.Text = DT.Rows(0)(0)
                txtEmpCode.Text = DT.Rows(0)(1)
                txtEmpName.Text = DT.Rows(0)(2)
                txtEmailID.Text = DT.Rows(0)(3)
                txtPhone.Text = DT.Rows(0)(4)
            End If
            hdnMode.Value = Mode
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "OnLoad();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        CallBackReturn = ""
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1
                DT = DB.ExecuteDataSet("select Emp_Name from emp_master where status_id=1 and emp_code=" + Data(1) + "").Tables(0)
                If (DT.Rows.Count > 0) Then
                    CallBackReturn = DT.Rows(0)(0)
                End If
            Case 2
                Dim DepartmentName As String = Data(1)
                Dim DepartmentHead As Integer = CInt(Data(2))
                Dim EmailID As String = Data(3)
                Dim PnoneNo As String = Data(4)
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(6) As SqlParameter
                    Params(0) = New SqlParameter("@DepartmentName", SqlDbType.VarChar, 50)
                    Params(0).Value = DepartmentName.ToUpper()
                    Params(1) = New SqlParameter("@DepartmentHead", SqlDbType.Int)
                    Params(1).Value = DepartmentHead
                    Params(2) = New SqlParameter("@EmailID", SqlDbType.VarChar, 50)
                    Params(2).Value = EmailID
                    Params(3) = New SqlParameter("@PnoneNo", SqlDbType.VarChar, 50)
                    Params(3).Value = PnoneNo
                    Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(4).Value = CInt(Session("UserID"))
                    Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(6).Direction = ParameterDirection.Output
                    DB.ExecuteDataSet("SP_HR_ADD_NEW_DEPARTMENT", Params)
                    ErrorFlag = CInt(Params(5).Value)
                    Message = CStr(Params(6).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
            Case 3
                Dim DepartmentName As String = Data(1)
                Dim DepartmentHead As Integer = CInt(Data(2))
                Dim EmailID As String = Data(3)
                Dim PnoneNo As String = Data(4)
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(7) As SqlParameter
                    Params(0) = New SqlParameter("@DepartmentName", SqlDbType.VarChar, 50)
                    Params(0).Value = DepartmentName.ToUpper()
                    Params(1) = New SqlParameter("@DepartmentHead", SqlDbType.Int)
                    Params(1).Value = DepartmentHead
                    Params(2) = New SqlParameter("@EmailID", SqlDbType.VarChar, 50)
                    Params(2).Value = EmailID
                    Params(3) = New SqlParameter("@PnoneNo", SqlDbType.VarChar, 50)
                    Params(3).Value = PnoneNo
                    Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(4).Value = CInt(Session("UserID"))
                    Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@DepartmentID", SqlDbType.Int)
                    Params(7).Value = DepartmentID
                    DB.ExecuteDataSet("SP_HR_EDIT_DEPARTMENT", Params)
                    ErrorFlag = CInt(Params(5).Value)
                    Message = CStr(Params(6).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
End Class
