﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class UpdateESI
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim Leave As New Leave
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Protected Sub MWF_VerifyData_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 372) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            DT = DB.ExecuteDataSet("select '-1' as branchid,' ALL' as branch Union All select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id = 1 order by 2").Tables(0)
            GF.ComboFill(ddlBranch, DT, 0, 1)
            DT = DB.ExecuteDataSet("select -1 as CadreID,' ALL' as Cadre Union All select Cadre_id as CadreID,upper(Cadre_Name) as Cadre from cadre_Master where rank_ID between 9 and 99 order by 2").Tables(0)
            GF.ComboFill(ddlCadre, DT, 0, 1)
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.ddlBranch.Attributes.Add("onchange", "return FillDetails()")
            Me.ddlCadre.Attributes.Add("onchange", "return FillDetails()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim Flag As Integer = CInt(Data(1))
            Dim UpdateDtl As String = Data(2).Substring(1)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@UpdFlag", SqlDbType.Int)
                Params(0).Value = Flag
                Params(1) = New SqlParameter("@UpdateDtl", SqlDbType.VarChar, 5000)
                Params(1).Value = UpdateDtl
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = UserID
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_HR_UPDATE_ESIFLAG", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            Dim Flag As Integer
            Dim BranchID, CadreID As Integer
            BranchID = CInt(Data(1))
            Flag = CInt(Data(2))
            CadreID = CInt(Data(3))
            DT = DB.ExecuteDataSet("select a.emp_code,emp_name,case when Branch_ID<100 then 'H O/'+Department_Name else Branch_Name end as Branch,Designation_Name,isnull(c.ESI_No,'') as ESINo  from Emp_List a left outer join emp_details c on(a.emp_code=c.emp_code),CADRE_MASTER b where a.Cadre_ID=b.Cadre_ID and b.Rank_ID between 9 and 99 and a.status_id=1 and esi_flag=" + Flag.ToString() + " and case when " + BranchID.ToString() + "=-1 then " + BranchID.ToString() + " else a.Branch_ID end =" + BranchID.ToString() + "  and case when " + CadreID.ToString() + "=-1 then " + CadreID.ToString() + " else a.cadre_id end =" + CadreID.ToString() + " order by 1").Tables(0)
            Dim i As Integer
            For Each DR As DataRow In DT.Rows
                CallBackReturn += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString()
                If i <> DT.Rows.Count - 1 Then
                    CallBackReturn += "¥"
                End If
                i = i + 1
            Next
        End If
    End Sub
End Class
