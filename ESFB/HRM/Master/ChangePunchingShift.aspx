﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ChangePunchingShift.aspx.vb" Inherits="ChangePunchingShift" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       

        function FromServer(arg, context) {

            switch (context) {

                case 1:
                    {
                        var Data = arg.split("~");
                        if (Data[0] != "2") 
                        {
                            var empDtls = Data[1].split("Ø");
                            document.getElementById("<%= txtName.ClientID %>").value = empDtls[0];
                            document.getElementById("<%= txtLocation.ClientID %>").value = empDtls[1];
                            document.getElementById("<%= txtDept.ClientID %>").value = empDtls[2];
                            document.getElementById("<%= txtDesig.ClientID %>").value = empDtls[3];
                            document.getElementById("<%= txtPost.ClientID %>").value = empDtls[4];
                            document.getElementById("<%= txtJoinDt.ClientID %>").value = empDtls[5];
                            document.getElementById("<%= txtCurrentShift.ClientID %>").value = empDtls[6];
                            document.getElementById("<%= cmbShift.ClientID %>").value = -1;
                            ComboFill(Data[2], "<%= cmbShift.ClientID %>");
                        }
                        else 
                        {
                            alert(Data[1]);
                            document.getElementById("<%= txtName.ClientID %>").value = "";
                            document.getElementById("<%= txtLocation.ClientID %>").value = "";
                            document.getElementById("<%= txtDept.ClientID %>").value = "";
                            document.getElementById("<%= txtDesig.ClientID %>").value = "";
                            document.getElementById("<%= txtPost.ClientID %>").value = "";
                            document.getElementById("<%= txtJoinDt.ClientID %>").value = "";
                              document.getElementById("<%= txtCurrentShift.ClientID %>").value="";
                            document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                        }
                        break;
                    }

                case 2:
                    {
                        var Data = arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("ChangePunchingShift.aspx?RptID="+ btoa(document.getElementById("<%= hdnRptID.ClientID %>").value), "_self");
                    }
            }
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnApprove_onclick() {           
            if(document.getElementById("<%= hdnRptID.ClientID %>").value=="2" && document.getElementById("<%= cmbEmployee.ClientID %>").value==-1)
            {alert("Select Employee"); document.getElementById("<%= cmbEmployee.ClientID %>").focus(); return false; }
            if(document.getElementById("<%= hdnRptID.ClientID %>").value=="1" && document.getElementById("<%= txtName.ClientID %>").value=="")
            {alert("Enter a valid Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false; }
             var ShiftID = document.getElementById("<%= cmbShift.ClientID %>").value;          
            if (ShiftID == "-1")
            { alert("Select Shift"); document.getElementById("<%= cmbShift.ClientID %>").focus(); return false; }
            var EmpID=0;
            if(document.getElementById("<%= hdnRptID.ClientID %>").value=="1")
                EmpID= document.getElementById("<%= txtEmpCode.ClientID %>").value;
            else
             EmpID= document.getElementById("<%= cmbEmployee.ClientID %>").value;
            var EffectiveDate=document.getElementById("<%= txtEffDt.ClientID %>").value;
           
            var ToData = "2Ø" + ShiftID + "Ø" + EmpID + "Ø" +EffectiveDate;          
            ToServer(ToData, 2);
        }

          function GetEmployeeDtls() {
          var EmpID=0;
          if(document.getElementById("<%= hdnRptID.ClientID %>").value=="1")
             EmpID= document.getElementById("<%= txtEmpCode.ClientID %>").value;
          else
             EmpID= document.getElementById("<%= cmbEmployee.ClientID %>").value;
            if (EmpID > 0) {
                var ToData = "1Ø" + EmpID;
                ToServer(ToData, 1);
            }
        }

        function showDetails()
        {
            if(document.getElementById("<%= hdnRptID.ClientID %>").value=="1")
            {
                document.getElementById("rowText").style.display="";
                document.getElementById("rowSelect").style.display="none";
            }
            else
            {
                document.getElementById("rowText").style.display="none";
                document.getElementById("rowSelect").style.display="";
            }
        }

    </script>
   
</head>
</html>
<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;" >
           <tr id="rowText" style="display:none;">
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Employee Code</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpCode" class="NormalText" runat="server" Width="15%" 
                MaxLength="5" onkeypress="return NumericCheck(event)" ></asp:TextBox>
            </td>
        </tr>
        <tr id="rowSelect" style="display:none;">
            <td style="width:25%;">
                &nbsp;</td>
            <td style="width:12%; text-align:left;">
                Select</td>
            <td style="width:63%">
                 &nbsp; &nbsp;<asp:DropDownList ID="cmbEmployee" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                   </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Name</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Location</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtLocation" class="ReadOnlyTextBox" runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Department</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDept" class="ReadOnlyTextBox" runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Designation</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesig" class="ReadOnlyTextBox" runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Post</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtPost" class="ReadOnlyTextBox" 
                    runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Join Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtJoinDt" class="ReadOnlyTextBox" 
                    runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
                &nbsp;</td>
            <td style="width:12%; text-align:left;">
                Current Shift</td>
            <td style="width:63%">
                 &nbsp; &nbsp;<asp:TextBox ID="txtCurrentShift" class="ReadOnlyTextBox" 
                    runat="server" Width="40%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr id="branch"><td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Shift Type</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbShift" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                   </asp:DropDownList>
            </td>
            
       </tr>
       
        <tr ><td style="width:25%;">&nbsp;</td>
        <td style="width:12%; text-align:left;">Effective Date</td>
            <td style="width:63%">
            &nbsp; &nbsp;<asp:TextBox ID="txtEffDt" class="NormalText" runat="server" Width="20%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CEFromDate" runat="server" TargetControlID="txtEffDt" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
            
       </tr>
       
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 <input id="btnApprove" 
                    style="width: 8%; font-family: Cambria; cursor: pointer;" type="button" 
                    value="SAVE" onclick="return btnApprove_onclick()"  />&nbsp;&nbsp;
            &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 8%;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
                 
                <asp:HiddenField ID="hdnRptID" runat="server" />
                 
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

