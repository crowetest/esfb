﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ChangeLeaveBalance
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Update Leave Balance"
            If GF.FormAccess(CInt(Session("UserID")), 154) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.txtCLBal.Attributes.Add("onblur", "return LeaveOnChange()")
            Me.txtPLBal.Attributes.Add("onblur", "return LeaveOnChange()")
            Me.txtSLBal.Attributes.Add("onblur", "return LeaveOnChange()")
            txtEmpcode.Attributes.Add("onchange", "EmployeeOnChange()")
            txtEmpcode.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EmpID As Integer = CInt(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 2 Then
            Dim CL As Double = CDbl(Data(2))
            Dim PL As Double = CDbl(Data(3))
            Dim SL As Double = CDbl(Data(4))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@EMP_CODE", SqlDbType.Int)
                Params(0).Value = EmpID
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@CLCount", SqlDbType.Float)
                Params(2).Value = CL
                Params(3) = New SqlParameter("@PLCount", SqlDbType.Float)
                Params(3).Value = PL
                Params(4) = New SqlParameter("@SLCount", SqlDbType.Float)
                Params(4).Value = SL
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_CHANGE_LEAVE_BALANCE", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 1 Then
            If CInt(Data(1)) > 0 Then
                DT_EMP = GF.GET_EMP_DEATILS(CInt(Data(1)))
                If DT_EMP.Rows.Count > 0 Then
                    CallBackReturn = CStr(DT_EMP.Rows(0).Item(0)) + "Ø"
                    DT = DB.ExecuteDataSet("SELECT CL_Bal ,SL_Bal ,PL_Bal  FROM EMP_LEAVE_BALANCE WHERE Emp_Code =" & CInt(Data(1)) & " ").Tables(0)
                    If DT.Rows.Count > 0 Then
                        CallBackReturn += CStr(DT.Rows(0).Item(0)) + "|" + CStr(DT.Rows(0).Item(1)) + "|" + CStr(DT.Rows(0).Item(2)) + "Ø"
                    Else
                        CallBackReturn += CStr(0) + "|" + CStr(0) + "|" + CStr(0) + "^"
                    End If
                    DT = DB.ExecuteDataSet("SELECT ISNULL(SUM(CASE WHEN Leave_Type=1 THEN  Leave_Days ELSE 0 END),0),ISNULL(SUM(CASE WHEN Leave_Type=2 THEN  Leave_Days ELSE 0 END),0),ISNULL(SUM(CASE WHEN Leave_Type=3 THEN  Leave_Days ELSE 0 END),0)  FROM EMP_LEAVE_REQUEST   WHERE Emp_Code =" & CInt(Data(1)) & " AND  STATUS_ID<5 and Leave_From >='31-Jan-2015' ").Tables(0)
                    If DT.Rows.Count > 0 Then
                        CallBackReturn += CStr(DT.Rows(0).Item(0)) + "|" + CStr(DT.Rows(0).Item(1)) + "|" + CStr(DT.Rows(0).Item(2)) + "Ø"
                    Else
                        CallBackReturn += CStr(0) + "|" + CStr(0) + "|" + CStr(0) + "^"
                    End If
                End If
            Else
                CallBackReturn = "ØØ"
            End If
        End If
    End Sub
#End Region
End Class
