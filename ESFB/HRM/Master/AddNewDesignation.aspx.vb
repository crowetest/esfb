﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class HRM_Master_AddNewDesignation
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
    Protected Sub HRM_Master_AddNewDepartment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 376) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            If Not IsPostBack Then
                DT = DB.ExecuteDataSet("SELECT STUFF((select 'Ñ' +cast(aa.designation_id as varchar)+'ÿ'+upper(aa.designation_name)+'ÿ'+UPPER(aa.cadre_name)+'ÿ'+cast(aa.Cadre_ID as varchar)+'ÿ'+isnull(cast(COUNT(bb.Emp_Code ) as varchar),'')from(select a.designation_id,a.designation_name,b.cadre_name,b.Cadre_ID from DESIGNATION_MASTER  a ,CADRE_MASTER b where a.Cadre_ID=b.Cadre_ID and a.status_id=1) aa left outer join EMP_MASTER bb on(aa.Cadre_ID=bb.Cadre_ID and  aa.Designation_ID=bb.Designation_ID and bb.Status_ID=1) group by aa.Designation_ID,aa.Designation_Name,aa.Cadre_name,aa.Cadre_ID  order by aa.Cadre_name , aa.Designation_Name  FOR XML PATH('')) ,1,1,'')").Tables(0)
                hdnData.Value = DT.Rows(0)(0)
                DT = DB.ExecuteDataSet("select -1 as cadreID,' -----SELECT-----' as Cadre union all select cadre_id,cadre_name from cadre_Master where status_id=1 order by 2").Tables(0)
                GN.ComboFill(cmbCadre, DT, 0, 1)
            End If

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "OnLoad();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1
                Dim DesignationID As Integer = CInt(Data(1))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(3) As SqlParameter
                    Params(0) = New SqlParameter("@DesignationID", SqlDbType.Int)
                    Params(0).Value = DesignationID
                    Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(1).Value = CInt(Session("UserID"))
                    Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(2).Direction = ParameterDirection.Output
                    Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(3).Direction = ParameterDirection.Output
                    DT = DB.ExecuteDataSet("SP_HR_DELETE_DESIGNATION", Params).Tables(0)
                    ErrorFlag = CInt(Params(2).Value)
                    Message = CStr(Params(3).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message + "ʘ" + DT.Rows(0)(0)
            Case 2
                Dim DesignationID As Integer = Data(1)
                Dim DesignationName As String = Data(2)
                Dim CadreID As Integer = CInt(Data(3))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(5) As SqlParameter
                    Params(0) = New SqlParameter("@DesignationID", SqlDbType.Int)
                    Params(0).Value = DesignationID
                    Params(1) = New SqlParameter("@DesignationName", SqlDbType.VarChar, 50)
                    Params(1).Value = DesignationName.ToUpper()
                    Params(2) = New SqlParameter("@CadreID", SqlDbType.Int)
                    Params(2).Value = CadreID
                    Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(3).Value = CInt(Session("UserID"))
                    Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(5).Direction = ParameterDirection.Output
                    DT = DB.ExecuteDataSet("SP_HR_ADD_EDIT_DESIGNATION", Params).Tables(0)
                    ErrorFlag = CInt(Params(4).Value)
                    Message = CStr(Params(5).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message + "ʘ" + DT.Rows(0)(0)
          
            Case 4
                DT = DB.ExecuteDataSet("select emp_code,upper(emp_name),upper(branch_name),upper(department_name) from Emp_List  where designation_id=" + Data(1).ToString() + " and status_id=1").Tables(0)
                CallBackReturn = ""
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1) + "ÿ" + DR(2) + "ÿ" + DR(3)
                Next
                DT = DB.ExecuteDataSet("select b.cadre_name+' - '+ a.designation_name from designation_master a,cadre_master b where a.cadre_id=b.cadre_id and a.designation_id=" + Data(1).ToString()).Tables(0)
                CallBackReturn += "ʘ" + DT.Rows(0)(0)
        End Select
    End Sub
End Class
