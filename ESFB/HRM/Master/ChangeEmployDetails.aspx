﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ChangeEmployDetails.aspx.vb" Inherits="ChangeEmployDetails"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server" >
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
     <script language="javascript" type="text/javascript">
        function EmpCodeOnChange()
        {
            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if(EmpCode == "")
            { alert("Enter Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false; }
            else
            {
                var Data = "1Ø" + EmpCode ;
                ToServer(Data,1);
            }
        }
        function BranchOnChange() 
        {
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("|");
//            if (BranchDtl[1] == 1 || BranchDtl[1]==3) 
//            {
//                document.getElementById("<%= cmbDepartment.ClientID %>").value = 21;
//                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = true ;               
//            }
//            else 
//            {
                document.getElementById("<%= cmbDepartment.ClientID %>").value = "-1";
                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = false;              
//            }
        }
        function CadreOnChange() {
            var CadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;           
            var ToData = "2Ø" + CadreID;          
            ToServer(ToData, 2);

        }
        function ChangeReportingOfficer() {
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("|");
            if(BranchDtl!="-1")
            {
                var BranchID = BranchDtl[0];
                var DepartmentID = document.getElementById("<%= cmbDepartment.ClientID %>").value;
                var DesignationID = document.getElementById("<%= cmbDesignation.ClientID %>").value;
                if(DepartmentID>0 && BranchID>=0 && DesignationID>0)
                    ToServer("3Ø" + BranchID + "Ø" + DepartmentID + "Ø" + DesignationID + "Ø", 3);
            }
        }
        function btnSave_onclick() {
            if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "") { alert("Enter Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtEmpName.ClientID %>").value == "") { alert("Enter Employee Name"); document.getElementById("<%= txtEmpName.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1") { alert("Select Branch"); document.getElementById("<%= cmbBranch.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbDepartment.ClientID %>").value == "-1") { alert("Select Department"); document.getElementById("<%= cmbDepartment.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbCadre.ClientID %>").value == "-1") { alert("Select Cadre"); document.getElementById("<%= cmbCadre.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbDesignation.ClientID %>").value == "-1") { alert("Select Designation"); document.getElementById("<%= cmbDesignation.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtDateOfJoin.ClientID %>").value == "") { alert("Enter Date of Join"); document.getElementById("<%= txtDateOfJoin.ClientID %>").focus(); return false; }            
            if (document.getElementById("<%= cmbReportingTo.ClientID %>").value == "-1") { alert("Select Reporting Officer"); document.getElementById("<%= cmbReportingTo.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbEmpPost.ClientID %>").value == "-1") { alert("Select Post"); document.getElementById("<%= cmbEmpPost.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbEmpType.ClientID %>").value == "-1") { alert("Select Employee Type"); document.getElementById("<%= cmbEmpType.ClientID %>").focus(); return false; }
            var EmpCode       = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            var EmpName       = document.getElementById("<%= txtEmpName.ClientID %>").value;
            var BranchDtl     = document.getElementById("<%= cmbBranch.ClientID %>").value.split("|");
            var BranchID      = BranchDtl[0];
            var DepartmentID  = document.getElementById("<%= cmbDepartment.ClientID %>").value;
            var CadreID       = document.getElementById("<%= cmbCadre.ClientID %>").value;
            var DesignationID = document.getElementById("<%= cmbDesignation.ClientID %>").value;
            var DateOfJoin    = document.getElementById("<%= txtDateOfJoin.ClientID %>").value;
            var ReportingTo   = document.getElementById("<%= cmbReportingTo.ClientID %>").value;
            var EmpPost       = document.getElementById("<%= cmbEmpPost.ClientID %>").value;
            var EmpStatus     = document.getElementById("<%= cmbEmpType.ClientID %>").value;            
            var Data = "4Ø" + EmpCode + "Ø" + EmpName + "Ø" + BranchID + "Ø" + DepartmentID + "Ø" + CadreID + "Ø" + DesignationID + "Ø" + DateOfJoin + "Ø" + ReportingTo + "Ø" + EmpPost + "Ø" + EmpStatus;
            ToServer(Data, 4);
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
        }
        function FromServer(Arg, Context) 
        {
           switch (Context) 
           {
                case 1:
                {
                    var Data = Arg.split("Ø");
                    
                    if(Data[0]=="-1")
                    {
                        alert("Check EmpCode");
                        document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                        return false;
                    }
                    else
                    {
                        //Emp_Name,Branch_ID,Department_ID,Cadre_ID,Designation_ID,Date_Of_Join,Reporting_To,Post_ID,EMP_STATUS_id 
                        document.getElementById("<%= txtEmpName.ClientID %>").value     = Data[0];
                        document.getElementById("<%= cmbBranch.ClientID %>").value      = Data[1];
                        document.getElementById("<%= cmbDepartment.ClientID %>").value  = Data[2];
                        document.getElementById("<%= cmbCadre.ClientID %>").value       = Data[3];
                        document.getElementById("<%= cmbDesignation.ClientID %>").value = Data[4];
                        document.getElementById("<%= txtDateOfJoin.ClientID %>").value  = Data[5];
                        document.getElementById("<%= cmbEmpPost.ClientID %>").value     = Data[7];
                        document.getElementById("<%= cmbEmpType.ClientID %>").value     = Data[8];
                        ComboFill(Data[9],"<%= cmbReportingTo.ClientID %>" );
                        document.getElementById("<%= cmbReportingTo.ClientID %>").value = Data[6];
                    }
                    break;
                }
                case 2:
                {
                    ComboFill(Arg, "<%= cmbDesignation.ClientID %>");
                    break;
                }
                case 3:
                {
                    ComboFill(Arg, "<%= cmbReportingTo.ClientID %>");
                    break;
                }
                case 4:
                {
                    var Data = Arg.split("ʘ");
                    alert(Data[1]);
                    if (Data[0] == 0) window.open("ChangeEmployDetails.aspx", "_self");
                    break;
                }    
           }
        }
    
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
         
    </script>
    </head>
</html>
<br />
    <table align="center" 
        style="width:60%; margin: 0px auto;">
        <tr>
            <td colspan="4">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Employee Code&nbsp;&nbsp;<b style="color: red;">*</b>
            </td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="NormalText"  MaxLength="6" onkeypress="return NumericCheck(event)"></asp:TextBox>
            </td>
            <td style="width: 20%; text-align: left;">
                Name&nbsp;&nbsp;
            </td>
            <td style="width: 30%;" class="Displays">
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpName" runat="server" Width="90%"  class="NormalText"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Location&nbsp;&nbsp;<b style="color: red;">*</b>
            </td>
            <td style="width: 30%;" class="Displays">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbBranch" runat="server" class="NormalText" Width="91%">  </asp:DropDownList>
            </td>
            <td style="width: 20%; text-align: left;" class="Displays">
                Department&nbsp;&nbsp;<b style="color: red;">*</b>
            </td>
            <td style="width: 30%;" class="Displays">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDepartment" runat="server" class="NormalText" Width="91%">   </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Cadre&nbsp;&nbsp;<b style="color: red;">*</b>
            </td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbCadre" runat="server" class="NormalText" Width="91%">  </asp:DropDownList>
            </td>
            <td style="width: 20%; text-align: left;">
                Designation&nbsp;&nbsp;<b style="color: red;">*</b>
            </td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDesignation" runat="server" class="NormalText"  Width="91%">
                            <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                             </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Date Of Join&nbsp;&nbsp;<b style="color: red;">*</b>
            </td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:TextBox ID="txtDateofJoin" runat="server" class="NormalText" Width="91%" ReadOnly="true"></asp:TextBox>
                <asp:CalendarExtender ID="CE1" runat="server" TargetControlID="txtDateofJoin" Format="dd-MMM-yyyy"> </asp:CalendarExtender>
            </td>
            <td style="width: 20%; text-align: left;">
                Reporting To&nbsp;&nbsp;<b style="color: red;">*</b>
            </td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbReportingTo" runat="server" class="NormalText" Width="91%">
                            <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                            </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Post&nbsp;&nbsp;<b style="color: red;">*</b>
            </td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbEmpPost" runat="server" class="NormalText" Width="91%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="width: 20%; text-align: left;">
                Employee Status&nbsp;&nbsp;<b style="color: red;">*</b>
            </td>
            <td style="width: 30%; text-align: left;">
                &nbsp;
                <asp:DropDownList ID="cmbEmpType" runat="server" class="NormalText" Width="91%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 30%;">
                &nbsp;&nbsp;
            </td>
            <td style="width: 20%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 30%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 30%;">
                &nbsp;
            </td>
            <td style="width: 20%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 30%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr style="text-align: center;">
            <td colspan="4" style="text-align: center;">
                <input id="btnSave" type="button" value="SAVE" onclick="return btnSave_onclick()"
                    style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;<input id="btnExit"
                        type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria;
                        width: 5%; cursor: pointer;" />
            </td>
        </tr>
    </table><br />

</asp:Content>

