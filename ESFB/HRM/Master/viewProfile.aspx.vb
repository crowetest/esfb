﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class viewProfile
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim MA As New Master
    Dim CallBackReturn As String = Nothing
    Dim CallBackStr As New System.Text.StringBuilder
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "View Profile"
            If GF.FormAccess(CInt(Session("UserID")), 377) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.txtEmpCode.Attributes.Add("onchange", "return GetEmployeeDtls()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim EmpID As Integer = CInt(Data(1))
            DT_EMP = DB.ExecuteDataSet("Select e.emp_code,e.emp_name,e.Branch_ID,e.Branch_name,e.Department_name,e.Designation_name,e.Date_Of_Join,e.Post_ID,p.Post_Name " & _
                    " from Emp_List e, EMP_POST_MASTER p where  e.Post_ID=p.Post_ID  and e.EMp_Code=" + CInt(Data(1)).ToString() + " and e.status_ID=1").Tables(0)
            If DT_EMP.Rows.Count > 0 Then
                CallBackStr.Append(DT_EMP.Rows(0)(1))
                CallBackStr.Append("Ø")
                CallBackStr.Append(DT_EMP.Rows(0)(3))
                CallBackStr.Append("Ø")
                CallBackStr.Append(DT_EMP.Rows(0)(4))
                CallBackStr.Append("Ø")
                CallBackStr.Append(DT_EMP.Rows(0)(5))
                CallBackStr.Append("Ø")
                CallBackStr.Append(DT_EMP.Rows(0)(8))
                CallBackStr.Append("Ø")
                CallBackStr.Append(Format(DT_EMP.Rows(0)(6), "dd/MMM/yyyy"))
                CallBackStr.Append("Ø")
                CallBackReturn = "1~" + CStr(CallBackStr.ToString)
            Else
                CallBackReturn = "2~" + "Invalid Employee Code!"
            End If
       
        End If
    End Sub
#End Region
End Class
