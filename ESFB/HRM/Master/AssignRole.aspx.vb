﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AssignRole
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim MAST As New Master
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 54) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Assign Role"
            Me.txtEmpCode.Attributes.Add("onchange", "return RequestID()")
            Me.cmbGroup.Attributes.Add("onchange", "return RoleOnchange()")
            Me.cmbRoleType.Attributes.Add("onchange", "return RoleOnchange()")
            GF.ComboFill(cmbRoleType, MAST.GetRoleType(), 0, 1)
            GF.ComboFill(cmbGroup, MAST.GetModule(), 0, 1)
            hid_tab.Value = CStr(1)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "RoleTableFill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim UserID As Integer = CInt(Session("UserID"))
        If RequestID = 1 Then
            Dim TabID As Integer = CInt(Data(1))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                If TabID = 1 Then
                    Dim RoleNAme As String = CStr(Data(2))
                    Dim Params(4) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@Data", SqlDbType.VarChar, 5000)
                    Params(1).Value = RoleNAme
                    Params(2) = New SqlParameter("@TabID", SqlDbType.Int)
                    Params(2).Value = TabID
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_ROLE_MASTER", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                ElseIf TabID = 2 Then
                    Dim RoleID As Integer = CInt(Data(2))
                    Dim FormsData As String = CStr(Data(4))
                    Dim Params(6) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@Data", SqlDbType.VarChar, 5000)
                    Params(1).Value = FormsData
                    Params(2) = New SqlParameter("@TabID", SqlDbType.Int)
                    Params(2).Value = TabID
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@RoleID", SqlDbType.Int)
                    Params(5).Value = RoleID
                    Params(6) = New SqlParameter("@ModuleID", SqlDbType.Int)
                    Params(6).Value = Data(3)
                    DB.ExecuteNonQuery("SP_ROLE_MASTER", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                ElseIf TabID = 3 Then
                    Dim RoleID As Integer = CInt(Data(2))
                    Dim EmpCode As String = CStr(Data(3))
                    Dim Params(5) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@EMP_Code", SqlDbType.VarChar, 5000)
                    Params(1).Value = EmpCode
                    Params(2) = New SqlParameter("@TabID", SqlDbType.Int)
                    Params(2).Value = TabID
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@RoleID", SqlDbType.Int)
                    Params(5).Value = RoleID
                    DB.ExecuteNonQuery("SP_ROLE_MASTER", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf RequestID = 2 Then
            DT = MAST.GetRoleFormList(CInt(Data(1)), CInt(Data(3)))
            Dim Strbranch As String
            Strbranch = MAST.GetStr_Form(DT)
            If DT.Rows.Count > 0 Then
                CallBackReturn = "1~" + Strbranch
            Else
                CallBackReturn = "2~" + "No Forms Exists!"
            End If
        ElseIf RequestID = 3 Then
            DT = GF.GET_EMP_DEATILS(CInt(Data(1)))
            If DT.Rows.Count > 0 Then
                Dim StrEMPName As String = CStr(DT.Rows(0).Item(0))
                DT = MAST.GetEmpRoleList(CInt(Data(1)))
                Dim StrExistRole As String
                StrExistRole = MAST.GetStr_EmpRoleAssign(DT)
                CallBackReturn = "1~" + StrEMPName + "|" + StrExistRole
            Else
                CallBackReturn = "2~" + "Invalid Employee Code!"
            End If
        ElseIf RequestID = 4 Then '----Auto fill ROle Type
            DT = MAST.GetRoleType()
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf RequestID = 5 Then '----Show Role Name in first tab
            DT = MAST.GetRoleNames()
            Dim StrExistRole As String
            StrExistRole = MAST.GetStr_EmpRole(DT)
            If DT.Rows.Count > 0 Then
                CallBackReturn = StrExistRole
            End If
        ElseIf RequestID = 6 Then '----Delete Assign role
            Dim RetVal As Integer = MAST.DeleteRoleAssign(CInt(Data(1)), CInt(Data(2)))
            If RetVal = 1 Then
                DT = MAST.GetEmpRoleList(CInt(Data(1)))
                Dim StrExistRole As String
                StrExistRole = MAST.GetStr_EmpRoleAssign(DT)
                CallBackReturn = "1~" + "Deleted Successfully" + "|" + StrExistRole
            Else
                CallBackReturn = "2~" + "Error Occured"
            End If
        End If
    End Sub
End Class
