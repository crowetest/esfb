﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class MasterData
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim MS As New Master
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 112) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Master Data Settings"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            GF.ComboFill(cmbRole, MS.GetRoleType(), 0, 1)
            Me.cmbModule.Attributes.Add("onchange", "return TypeOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim UserID As Integer = CInt(Session("UserID"))
        If RequestID = 1 Then
            Dim Type As Integer = CInt(Data(1))
            Dim Value As String = CStr(Data(2))
            Dim RoleID As Integer = CInt(Data(3))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@Type", SqlDbType.Int)
                Params(0).Value = Type
                Params(1) = New SqlParameter("@Value", SqlDbType.VarChar, 5000)
                Params(1).Value = Value
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@RoleID", SqlDbType.Int)
                Params(4).Value = CInt(RoleID)
                Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(5).Value = UserID
                DB.ExecuteNonQuery("SP_MASTER_DATA", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If
    End Sub
#End Region
End Class
