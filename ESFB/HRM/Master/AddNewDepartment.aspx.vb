﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class HRM_Master_AddNewDepartment
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
    Protected Sub HRM_Master_AddNewDepartment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 374) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            If Not IsPostBack Then
                DT = DB.ExecuteDataSet("SELECT STUFF((select 'Ñ' +cast(a.department_id as varchar)+'ÿ'+upper(a.department_name)+'ÿ'+(case when b.emp_name is null then 'N/A' else upper(b.emp_name) end )+'ÿ'+isnull(a.email,'')+'ÿ'+isnull(a.phone,'')+'ÿ'+cast(COUNT(c.emp_code)as varchar)+'ÿ'+isnull(cast(a.department_head as varchar),'') from department_master a left outer join emp_master b on( a.department_head=b.emp_code ) left outer join EMP_MASTER c on(a.Department_ID=c.Department_ID  and  c.Status_ID=1 ) where a.status_id=1 group by a.Department_ID,a.Department_Name ,a.EMail ,a.Department_Head ,a.phone,b.Emp_Name order by a.department_name FOR XML PATH('')) ,1,1,'')").Tables(0)
                hdnData.Value = DT.Rows(0)(0)
            End If
            txtEmpCode.Attributes.Add("onchange", "return EmpCodeOnChange()")
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "OnLoad();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1
                Dim DepartmentID As Integer = CInt(Data(1))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(3) As SqlParameter
                    Params(0) = New SqlParameter("@DepartmentID", SqlDbType.Int)
                    Params(0).Value = DepartmentID
                    Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(1).Value = CInt(Session("UserID"))
                    Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(2).Direction = ParameterDirection.Output
                    Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(3).Direction = ParameterDirection.Output
                    DT = DB.ExecuteDataSet("SP_HR_DELETE_DEPARTMENT", Params).Tables(0)
                    ErrorFlag = CInt(Params(2).Value)
                    Message = CStr(Params(3).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message + "ʘ" + DT.Rows(0)(0)
            Case 2
                Dim DepartmentID As Integer = Data(1)
                Dim DepartmentName As String = Data(2)
                Dim DepartmentHead As Integer = CInt(Data(3))
                Dim EmailID As String = Data(4)
                Dim PnoneNo As String = Data(5)
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(7) As SqlParameter
                    Params(0) = New SqlParameter("@DepartmentID", SqlDbType.Int)
                    Params(0).Value = DepartmentID
                    Params(1) = New SqlParameter("@DepartmentName", SqlDbType.VarChar, 50)
                    Params(1).Value = DepartmentName.ToUpper()
                    Params(2) = New SqlParameter("@DepartmentHead", SqlDbType.Int)
                    Params(2).Value = DepartmentHead
                    Params(3) = New SqlParameter("@EmailID", SqlDbType.VarChar, 50)
                    Params(3).Value = EmailID
                    Params(4) = New SqlParameter("@PnoneNo", SqlDbType.VarChar, 50)
                    Params(4).Value = PnoneNo
                    Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(5).Value = CInt(Session("UserID"))
                    Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(7).Direction = ParameterDirection.Output
                    DT = DB.ExecuteDataSet("SP_HR_ADD_EDIT_DEPARTMENT", Params).Tables(0)
                    ErrorFlag = CInt(Params(6).Value)
                    Message = CStr(Params(7).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message + "ʘ" + DT.Rows(0)(0)
            Case 3
                DT = DB.ExecuteDataSet("select Emp_Name from emp_master where status_id=1 and emp_code=" + Data(1) + "").Tables(0)
                If (DT.Rows.Count > 0) Then
                    CallBackReturn = DT.Rows(0)(0)
                End If
            Case 4
                DT = DB.ExecuteDataSet("select emp_code,upper(emp_name),upper(branch_name),upper(designation_name) from Emp_List  where department_id=" + Data(1).ToString() + " and status_id=1").Tables(0)
                CallBackReturn = ""
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1) + "ÿ" + DR(2) + "ÿ" + DR(3)
                Next
                DT = DB.ExecuteDataSet("select department_name from department_master where department_id=" + Data(1).ToString()).Tables(0)
                CallBackReturn += "ʘ" + DT.Rows(0)(0)
        End Select
    End Sub
End Class
