﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BranchRegistration
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim MAST As New Master
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 123) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Branch Master"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            GF.ComboFill(cmbState, MAST.GetState(), 0, 1)
            GF.ComboFill(cmbArea, MAST.GetArea(), 0, 1)
            DT = GF.GetQueryResult("select -1 HolidayZoneID, '---------Select---------' HolidayZone union all select HolidayZoneID,HolidayZone from HOLIDAY_ZONE order by 1")
            GF.ComboFill(cmbZonal, DT, 0, 1)
            DT1 = GF.GetQueryResult("select -1 Cluster_ID, '-----Select-----' Cluster_Name union all select Cluster_ID,Cluster_Name from Cluster_Master order by 1")
            GF.ComboFill(cmbCluster, DT1, 0, 1)
            Me.cmbState.Attributes.Add("onchange", "StateOnChange()")
            txtBranchCode.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim BranchCode As Integer = CInt(Data(1))
            Dim Name As String = CStr(Data(2))
            Dim Address As String = CStr(Data(3))
            Dim Phone As String = CStr(Data(4))
            Dim Email As String = CStr(Data(5))
            Dim StateID As Integer = CInt(Data(6))
            Dim DistrictID As Integer = CInt(Data(7))
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim AreaID As Integer = CInt(Data(8))
            Dim LeaveZone As Integer = CInt(Data(9))

            Dim Add1 As String = CStr(Data(10))
            Dim Add2 As String = CStr(Data(11))
            Dim Add3 As String = CStr(Data(12))
            Dim City As String = CStr(Data(13))
            Dim Pin As Integer = CInt(Data(14))
            Dim BranchType As Integer = CInt(Data(15))
            Dim ClusterID As Integer = CInt(Data(16))

            Dim Branch_Head As Integer = 0 ' CInt(Data(10))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(18) As SqlParameter
                Params(0) = New SqlParameter("@BranchCode", SqlDbType.Int)
                Params(0).Value = BranchCode
                Params(1) = New SqlParameter("@Name", SqlDbType.VarChar, 5000)
                Params(1).Value = Name
                Params(2) = New SqlParameter("@Address", SqlDbType.VarChar, 5000)
                Params(2).Value = Address
                Params(3) = New SqlParameter("@Phone", SqlDbType.VarChar, 5000)
                Params(3).Value = Phone
                Params(4) = New SqlParameter("@Email", SqlDbType.VarChar, 5000)
                Params(4).Value = Email
                Params(5) = New SqlParameter("@StateID", SqlDbType.Int)
                Params(5).Value = StateID
                Params(6) = New SqlParameter("@AreaID", SqlDbType.Int)
                Params(6).Value = AreaID
                Params(7) = New SqlParameter("@DistrictID", SqlDbType.Int)
                Params(7).Value = DistrictID
                Params(8) = New SqlParameter("@LeaveZone", SqlDbType.Int)
                Params(8).Value = LeaveZone
                Params(9) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(9).Direction = ParameterDirection.Output
                Params(10) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(10).Direction = ParameterDirection.Output
                Params(11) = New SqlParameter("@Branch_Head", SqlDbType.Int)
                Params(11).Value = Branch_Head
                Params(12) = New SqlParameter("@Add1", SqlDbType.VarChar, 100)
                Params(12).Value = Add1
                Params(13) = New SqlParameter("@Add2", SqlDbType.VarChar, 100)
                Params(13).Value = Add2
                Params(14) = New SqlParameter("@Add3", SqlDbType.VarChar, 100)
                Params(14).Value = Add3
                Params(15) = New SqlParameter("@City", SqlDbType.VarChar, 100)
                Params(15).Value = City
                Params(16) = New SqlParameter("@Pin", SqlDbType.Int)
                Params(16).Value = Pin
                Params(17) = New SqlParameter("@BranchType", SqlDbType.Int)
                Params(17).Value = BranchType
                Params(18) = New SqlParameter("@ClusterID", SqlDbType.Int)
                Params(18).Value = ClusterID
                DB.ExecuteNonQuery("SP_BRANCH_REG", Params)
                ErrorFlag = CInt(Params(9).Value)
                Message = CStr(Params(10).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            DT = MAST.GetDistrict(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub
#End Region
End Class
