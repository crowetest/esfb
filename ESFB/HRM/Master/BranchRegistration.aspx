﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="BranchRegistration.aspx.vb" Inherits="BranchRegistration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
    </style>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
          function StateOnChange() {
            var StateID = document.getElementById("<%= cmbState.ClientID %>").value;           
            var ToData = "2Ø" + StateID;          
            ToServer(ToData, 2);
          }        
          
          function FromServer(arg, context) {
             
              if(context==1){
                var Data = arg.split("Ø");
                  alert(Data[1]);
                  if (Data[0] == 0) window.open("BranchRegistration.aspx", "_self");
                  }
              else if(context==2){
                   ComboFill(arg, "<%= cmbDistrict.ClientID %>");
                   
              }
             
        }
        function ComboFill(data, ddlName) {
             document.getElementById(ddlName).options.length = 0;
             var rows = data.split("Ñ");
             for (a = 1; a < rows.length; a++) {
                 var cols = rows[a].split("ÿ");
                 var option1 = document.createElement("OPTION");
                 option1.value = cols[0];
                 option1.text = cols[1];
                 document.getElementById(ddlName).add(option1);
             }
         }
        

        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function BranchNameValid(e){
            var valid = (e.which == 32);
            //alert(valid);
            if (valid) {
                e.preventDefault();
            }
        }
        function btnConfirm_onclick() {           
           
            if (document.getElementById("<%= txtBranchCode.ClientID %>").value == "") 
            {
                alert("Enter Branch Code");
                document.getElementById("<%= txtBranchCode.ClientID %>").focus();
                return false;
            }
           
              if (document.getElementById("<%= txtName.ClientID %>").value == "") 
            {
                alert("Enter Branch Name");
                document.getElementById("<%= txtName.ClientID %>").focus();
                return false;
            }
          
            if (document.getElementById("<%= txtAddress.ClientID %>").value == "") 
            {
                alert("Enter Address");
                document.getElementById("<%= txtAddress.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtPhone.ClientID %>").value == "") 
            {
                alert("Enter Phone Number");
                document.getElementById("<%= txtPhone.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtEmail.ClientID %>").value == "") 
            {
                alert("Enter E-mail");
                document.getElementById("<%= txtEmail.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= cmbState.ClientID %>").value == "-1") 
            {
                alert("Select State");
                document.getElementById("<%= cmbState.ClientID %>").focus();
                return false;
            }
            
             if (document.getElementById("<%= cmbArea.ClientID %>").value == "-1") 
            {
                alert("Select Area");
                document.getElementById("<%= cmbArea.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbCluster.ClientID %>").value == "-1") 
            {
                alert("Select Cluster");
                document.getElementById("<%= cmbCluster.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbDistrict.ClientID %>").value == "-1") 
            {
                alert("Select District");
                document.getElementById("<%= cmbDistrict.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbZonal.ClientID %>").value == "-1") 
            {
                alert("Select Leave Application Related Zonal");
                document.getElementById("<%= cmbZonal.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtAdd1.ClientID %>").value == "" || document.getElementById("<%= txtAdd1.ClientID %>").value == " " || document.getElementById("<%= txtAdd1.ClientID %>").value == "0") 
            {
                alert("Please Enter Address 1");
                document.getElementById("<%= txtAdd1.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtAdd2.ClientID %>").value == "" || document.getElementById("<%= txtAdd2.ClientID %>").value == " " || document.getElementById("<%= txtAdd2.ClientID %>").value == "0") 
            {
                alert("Please Enter Address 2");
                document.getElementById("<%= txtAdd2.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtAdd3.ClientID %>").value == "" || document.getElementById("<%= txtAdd3.ClientID %>").value == " " || document.getElementById("<%= txtAdd3.ClientID %>").value == "0") 
            {
                alert("Please Enter Address 3");
                document.getElementById("<%= txtAdd3.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtCity.ClientID %>").value == "" || document.getElementById("<%= txtCity.ClientID %>").value == " " || document.getElementById("<%= txtCity.ClientID %>").value == "0") 
            {
                alert("Please Enter City");
                document.getElementById("<%= txtCity.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtPin.ClientID %>").value == "" || document.getElementById("<%= txtPin.ClientID %>").value == "0") 
            {
                alert("Please Enter PIN CODE");
                document.getElementById("<%= txtPin.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= branchType.ClientID %>").value == "-1") 
            {
                alert("Select Branch Type");
                document.getElementById("<%= branchType.ClientID %>").focus();
                return false;
            }
            var BranchID	= document.getElementById("<%= txtBranchCode.ClientID %>").value;
            var Name	= document.getElementById("<%= txtName.ClientID %>").value;
            var Address	= document.getElementById("<%= txtAddress.ClientID %>").value;
            var Phone	= document.getElementById("<%= txtPhone.ClientID %>").value;
            var Email	= document.getElementById("<%= txtEmail.ClientID %>").value;
            var StateID	= document.getElementById("<%= cmbState.ClientID %>").value;
            var ZoneID  = document.getElementById("<%= cmbZonal.ClientID %>").value;
            var Add1	= document.getElementById("<%= txtAdd1.ClientID %>").value;
            var Add2	= document.getElementById("<%= txtAdd2.ClientID %>").value;
            var Add3	= document.getElementById("<%= txtAdd3.ClientID %>").value;
            var City	= document.getElementById("<%= txtCity.ClientID %>").value;
            var Pin	= document.getElementById("<%= txtPin.ClientID %>").value;
            var BranchType = document.getElementById("<%= branchType.ClientID %>").value;

            if(document.getElementById("<%= txtEmail.ClientID %>").value!="")
            { var ret= checkEmail(document.getElementById("<%= txtEmail.ClientID %>")); if(ret==false) return false;}
            var DistrictID	= document.getElementById("<%= cmbDistrict.ClientID %>").value;

	        var AreaId =document.getElementById("<%= cmbArea.ClientID %>").value;
            var ClusterId =document.getElementById("<%= cmbCluster.ClientID %>").value;

	        var ToData = "1Ø" + BranchID + "Ø" + Name+ "Ø" + Address+ "Ø" + Phone+ "Ø" + Email+ "Ø" + StateID+ "Ø" + DistrictID + "Ø" + AreaId + "Ø" + ZoneID + "Ø" + Add1 + "Ø" + Add2 + "Ø" + Add3 + "Ø" + City + "Ø" + Pin + "Ø" + BranchType+ "Ø" + ClusterId ;
	       
            ToServer(ToData, 1);
        }

        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 80%; margin: 0px auto;">
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Branch Code
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranchCode" class="NormalText" runat="server" Width="15%"
                    MaxLength="5" onkeypress="NumericCheck(event)"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Branch Name
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:TextBox ID="txtName" class="NormalText" runat="server" Width="60%"
                    MaxLength="100" onkeypress='return BranchNameValid(event)'></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Address
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:TextBox ID="txtAddress" class="NormalText" runat="server" Width="60%"
                    MaxLength="100" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Phone
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:TextBox ID="txtPhone" class="NormalText" runat="server" Width="60%"
                    MaxLength="15" onkeypress="NumericCheck(event)"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                E-mail
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmail" class="NormalText" runat="server" Width="60%"
                    MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                State
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbState" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                District
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbDistrict" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Area
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbArea" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Cluster
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbCluster" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--<tr>
       <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Branch Head</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:DropDownList 
                     ID="cmbBranchHead" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                   
            </asp:DropDownList>
            </td>
       </tr>
       <tr>
       <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Start On</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDate" class="NormalText" runat="server" 
                    Width="40%" onkeypress="return false" ></asp:TextBox>
             <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtDate" EnabledOnClient="true"  Format="dd MMM yyyy">
             </asp:CalendarExtender>
            </td>
       </tr>--%>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Leave Application Related To
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbZonal" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="40%" ForeColor="Black">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Address 1</td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:TextBox ID="txtAdd1" class="NormalText" runat="server" Width="60%"
                    MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Address 2</td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:TextBox ID="txtAdd2" class="NormalText" runat="server" Width="60%"
                    MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Address 3</td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:TextBox ID="txtAdd3" class="NormalText" runat="server" Width="60%"
                    MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                City (Town/Location)</td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCity" class="NormalText" runat="server" Width="60%"
                    MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Pin Code</td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:TextBox ID="txtPin" class="NormalText" runat="server" Width="60%"
                    MaxLength="6" onkeypress='return NumericCheck(event)'></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Branch Type
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:DropDownList ID="branchType" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                    <asp:ListItem Value="0"> USB</asp:ListItem>
                    <asp:ListItem Value="1"> Retail</asp:ListItem>
                    <asp:ListItem Value="2"> Registered Office</asp:ListItem>
                    <asp:ListItem Value="3"> Combined</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;"
                    type="button" value="SAVE" onclick="return btnConfirm_onclick()" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
</asp:Content>
