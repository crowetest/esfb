﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ChangePunchingShift
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim MA As New Master
    Dim CallBackReturn As String = Nothing
    Dim CallBackStr As New System.Text.StringBuilder
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 126) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Change Punching Shift"
            Me.hdnRptID.Value = "1"
            If Not IsNothing(Request.QueryString.Get("RptID")) Then
                Me.hdnRptID.Value = Request.QueryString.Get("RptID")
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.CEFromDate.StartDate = CDate(Session("TraDt"))
            Me.CEFromDate.SelectedDate = CDate(Session("TraDt"))
            Me.cmbEmployee.Attributes.Add("onchange", "return GetEmployeeDtls()")
            Me.txtEmpCode.Attributes.Add("onchange", "return GetEmployeeDtls()")
            GF.ComboFill(cmbEmployee, GF.getReportingEmployees(CInt(Session("UserID"))), 0, 1)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "showDetails();", True)
            cmbShift.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim EmpID As Integer = CInt(Data(1))
            DT_EMP = DB.ExecuteDataSet("Select e.emp_code,e.emp_name,e.Branch_ID,e.Branch_name,e.Department_name,e.Designation_name,e.Date_Of_Join,e.Post_ID,p.Post_Name,q.shift_descr " & _
                    " from Emp_List e, EMP_POST_MASTER p,EMP_Shift_Master q where  e.Post_ID=p.Post_ID and e.shift_id=q.shift_id and e.EMp_Code=" + CInt(Data(1)).ToString() + " and e.status_ID=1").Tables(0)
            If DT_EMP.Rows.Count > 0 Then
                CallBackStr.Append(DT_EMP.Rows(0)(1))
                CallBackStr.Append("Ø")
                CallBackStr.Append(DT_EMP.Rows(0)(3))
                CallBackStr.Append("Ø")
                CallBackStr.Append(DT_EMP.Rows(0)(4))
                CallBackStr.Append("Ø")
                CallBackStr.Append(DT_EMP.Rows(0)(5))
                CallBackStr.Append("Ø")
                CallBackStr.Append(DT_EMP.Rows(0)(8))
                CallBackStr.Append("Ø")
                CallBackStr.Append(Format(DT_EMP.Rows(0)(6), "dd/MMM/yyyy"))
                CallBackStr.Append("Ø")
                CallBackStr.Append(DT_EMP.Rows(0)(9))
                CallBackStr.Append("Ø")
                DT = MA.GetShiftSchedule(CInt(DT_EMP.Rows(0)(2)))
                CallBackReturn = "1~" + CStr(CallBackStr.ToString) + "~"
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            Else
                CallBackReturn = "2~" + "Invalid Employee Code!"
            End If
        Else
            Dim EmPID As Integer = CInt(Data(2))
            Dim ShiftID As Integer = CInt(Data(1))
            Dim BranchID As Integer = CInt(Session("BranchID"))
            Dim EffectiveDt As DateTime = CDate(Data(3))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@userID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(1).Value = BranchID
                Params(2) = New SqlParameter("@EMPID", SqlDbType.Int)
                Params(2).Value = EmPID
                Params(3) = New SqlParameter("@ShiftID", SqlDbType.Int)
                Params(3).Value = ShiftID
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@Effective_Dt", SqlDbType.DateTime)
                Params(6).Value = EffectiveDt
                DB.ExecuteNonQuery("SP_SHIFT_CHANGE", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If
    End Sub
#End Region
End Class
