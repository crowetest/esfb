﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ChangePostOfEmployee.aspx.vb" Inherits="Audit_ChangePostOfEmployee" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <table class="style1" style="width:80%;margin: 0px auto;">
        <tr>
            <td style="width:25%;">
                &nbsp;</td>
            <td style="width:12%; text-align:left;">
                &nbsp;</td>
            <td style="width:63%">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Employee Code</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpCode" class="NormalText" runat="server" Width="15%" 
                MaxLength="5" onkeypress="return NumericCheck(event)" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Name</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Location</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtLocation" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Department</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDept" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Designation</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesig" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Post</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtPost" class="ReadOnlyTextBox" 
                    runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Join Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtJoinDt" class="ReadOnlyTextBox" 
                    runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:25%;">
                &nbsp;</td>
            <td style="width:12%; text-align:left;">
                Select Post</td>
            <td style="width:63%">
                &nbsp;&nbsp;
                <asp:DropDownList ID="cmbPost" Width="60%" runat="server" class="NormalText">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="rowEffData">
            <td style="width:25%;">
                &nbsp;</td>
            <td style="width:12%; text-align:left;">
                <asp:Label ID="lblEffective" runat="server" Text=""></asp:Label>
            </td>
            <td style="width:63%">
                &nbsp;&nbsp;
                <asp:DropDownList ID="cmbEffData" Width="60%" runat="server"  class="NormalText">
                </asp:DropDownList>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <br />
                <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE" onclick="return btnConfirm_onclick()" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
    // <![CDATA[

        function GetEmployeeDtls() {
            var EmpID = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if (EmpID > 0) {
                var ToData = "1Ø" + EmpID;
                ToServer(ToData, 1);
            }
        }
        
        function ChangePostEffect()
        {
            var PostID = document.getElementById("<%= cmbPost.ClientID %>").value;
            var ToData = "2Ø" + PostID;
            ToServer(ToData, 2);
        }

        function FromServer(arg, context) {
            switch (context) 
            {
                case 1:
                {
                    var Data = arg.split("~");
                    if (Data[0] != "2") 
                    {
                        var empDtls = Data[1].split("Ø");
                        document.getElementById("<%= txtName.ClientID %>").value = empDtls[0];
                        document.getElementById("<%= txtLocation.ClientID %>").value = empDtls[1];
                        document.getElementById("<%= txtDept.ClientID %>").value = empDtls[2];
                        document.getElementById("<%= txtDesig.ClientID %>").value = empDtls[3];
                        document.getElementById("<%= txtPost.ClientID %>").value = empDtls[4];
                        document.getElementById("<%= txtJoinDt.ClientID %>").value = empDtls[5];
                    }
                    else 
                    {
                        alert(Data[1]);
                        document.getElementById("<%= txtName.ClientID %>").value = "";
                        document.getElementById("<%= txtLocation.ClientID %>").value = "";
                        document.getElementById("<%= txtDept.ClientID %>").value = "";
                        document.getElementById("<%= txtDesig.ClientID %>").value = "";
                        document.getElementById("<%= txtPost.ClientID %>").value = "";
                        document.getElementById("<%= txtJoinDt.ClientID %>").value = "";
                        document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                    }
                    break;
                }
                case 2:
                {
                    var Data = arg.split("~");
                    if (Data[0] == "1") 
                    {
                        var effDtl = Data[1].split("¶");
                        document.getElementById("<%= cmbEffData.ClientID %>").options.length = 0;               
                        for(a=0;a<effDtl.length-1;a++)
                        {
                            var cols = effDtl[a].split("®");                        
                            var option1 = document.createElement("OPTION");
                            option1.value = cols[0];
                            option1.text  = cols[1];
                            document.getElementById("<%= cmbEffData.ClientID %>").add(option1);                       
                        }                   
                        var PostID = document.getElementById("<%= cmbPost.ClientID %>").value;
                        if (PostID == "5")
                        {
                            rowEffData.style.display ='';                        
                            document.getElementById("<%= lblEffective.ClientID %>").innerHTML ="Select Branch";
                        }
                        else if (PostID == "6")
                        {
                            rowEffData.style.display = '';                           
                            document.getElementById("<%= lblEffective.ClientID %>").innerHTML ="Select Area";
                        }
                        else if (PostID == "7")
                        {
                            rowEffData.style.display = '';                          
                            document.getElementById("<%= lblEffective.ClientID %>").innerHTML ="Select Region";
                        }
                        else if (PostID == "8")
                        {
                            rowEffData.style.display = '';                          
                            document.getElementById("<%= lblEffective.ClientID %>").innerHTML ="Select Zone";
                        }
                        else
                        {
                            rowEffData.style.display = 'none';
                        }
                    }
                    else
                    {
                        rowEffData.style.display = 'none';
                    }
                    break;
                }
                case 3:
                {
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == 0) 
                     window.open("ChangePostOfEmployee.aspx", "_self");
                    break;
                }
            }
        }
        
        function btnConfirm_onclick() 
        {
            if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "") 
            {
                alert("Select Employee Code");
                document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                return false;
            }
            else
            {
                var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            }
            if (document.getElementById("<%= cmbPost.ClientID %>").value == "-1") 
            {
                alert("Select Employee Post");
                document.getElementById("<%= cmbPost.ClientID %>").focus();
                return false;
            }
            else
            {
                var PostID = document.getElementById("<%= cmbPost.ClientID %>").value;
                var EffectiveVal;
                if (PostID == "5" || PostID == "6" || PostID == "7" || PostID == "8")
                {
                    if (document.getElementById("<%= cmbEffData.ClientID %>").value == "-1") 
                    {
                        alert("Select Category");
                        document.getElementById("<%= cmbEffData.ClientID %>").focus();
                        return false;
                    }   
                    else                    
                    {
                        EffectiveVal=document.getElementById("<%= cmbEffData.ClientID %>").value;            
                    } 
                }
                else
                {
                    EffectiveVal=0;
                }
            }
            var ToData = "3Ø" + EmpCode + "Ø" + PostID+ "Ø" + EffectiveVal;	       
            ToServer(ToData, 3);
        }

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        function window_onload() {
            rowEffData.style.display = 'none';
        }

// ]]>
    </script>
<script language="javascript" type="text/javascript" for="window" event="onunload">
// <![CDATA[
return window_onunload()
// ]]>
</script>
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
</script>
</asp:Content>

