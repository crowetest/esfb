﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="AddDepartment.aspx.vb" Inherits="HRM_Master_AddDepartment" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Script/AlertMessage.js" type="text/javascript"></script>
    <link href="../../Style/MessageStyle.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function btnExit_onclick() {          
                window.close();          
        }
        function EmpCodeOnChange() {
            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if (EmpCode != "")
            { ToServer("1ʘ" + EmpCode, 1); }
        }

        function FromServer(Arg, Context) {
            switch (Context) {
                case 1:
                    {
                        if (Arg == "")
                        { alert("Enter a valid Emp Code"); document.getElementById("<%= txtEmpName.ClientID %>").value = ""; document.getElementById("<%= txtEmpCode.ClientID %>").focus(); }
                        else
                        { document.getElementById("<%= txtEmpName.ClientID %>").value = Arg; }
                        break;
                    }
                case 2:
                    {
                        var Dtl = Arg.split("ʘ");
                        alert(Dtl[1]);
                        if (Dtl[0] == 0) {
                            if (window.opener != null && !window.opener.closed) {
                                window.opener.location.reload();
                                window.close();
                            }
                            window.onbeforeunload = RefreshParent;
                        }
                        break;
                    }
                case 3:
                    {
                        var Dtl = Arg.split("ʘ");
                        alert(Dtl[1]);
                        if (Dtl[0] == 0) {
                            if (window.opener != null && !window.opener.closed) {
                                window.opener.location.reload();
                                window.close();
                            }
                            window.onbeforeunload = RefreshParent;
                        }
                        break;
                    }
            }
        }



        function btnSave_onclick() {
            var DepartmentName = document.getElementById("<%= txtDepartment.ClientID %>").value;
            if (DepartmentName == "")
            { alert("Enter Department Name");document.getElementById("<%= txtDepartment.ClientID %>").focus();return false; }
            var DepartmentHead = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "" || document.getElementById("<%= txtEmpName.ClientID %>").value == "")
            { alert("Enter a valid  Emp Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false; }
            var EmailID = document.getElementById("<%= txtEmailID.ClientID %>").value;
            if (EmailID == "")
            {alert("Enter a Email ID");document.getElementById("<%= txtEmailID.ClientID %>").focus();return false; }
            var ret = checkEmail(document.getElementById("<%= txtEmailID.ClientID %>"));
            if (ret == false) {
                document.getElementById("<%= txtEmailID.ClientID %>").focus();
                return false;
            }
            var PhoneNo = document.getElementById("<%= txtPhone.ClientID %>").value;
            if (PhoneNo == "")
            { alert("Enter Phone No"); document.getElementById("<%= txtPhone.ClientID %>").focus(); return false; }
            alert(document.getElementById("<%= hdnMode.ClientID %>").value);
            if (document.getElementById("<%= hdnMode.ClientID %>").value == "A") {
                ToServer("2ʘ" + DepartmentName + "ʘ" + DepartmentHead + "ʘ" + EmailID + "ʘ" + PhoneNo, 2);
            }
            else {
                ToServer("3ʘ" + DepartmentName + "ʘ" + DepartmentHead + "ʘ" + EmailID + "ʘ" + PhoneNo, 3);
            }
        }

        function OnLoad() {
            if (document.getElementById("<%= hdnMode.ClientID %>").value == "E")
            { document.getElementById("btnSave").value = "UPDATE"; }
        }
// ]]>
    </script>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table align="center" style="width: 100%;text-align:center;">
        <tr>
            <td style="width:30%;text-align:left;">
                Department Name</td>
            <td style="width:70%;text-align:left;">
                <asp:TextBox ID="txtDepartment" runat="server" Width="90%"  style="text-transform:uppercase;"
                    class="NormalText" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:30%;text-align:left;">
                Department Head</td>
            <td style="width:70%;text-align:left;">
               <asp:TextBox ID="txtEmpCode" runat="server" Width="20%" class="NormalText"  
                    MaxLength="6" onkeypress="NumericCheck(event)"></asp:TextBox>
             
                <asp:TextBox ID="txtEmpName" runat="server" Width="68%" 
                    class="NormalText" MaxLength="100" ReadOnly="True" style="text-transform:uppercase;"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:30%;text-align:left;">
                Email ID</td>
            <td style="width:70%;text-align:left;">
                <asp:TextBox ID="txtEmailID" runat="server" Width="60%" 
                    class="NormalText" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:30%;text-align:left;">
                Phone
                </td>
            <td style="width:70%;text-align:left;">
                <asp:TextBox ID="txtPhone" runat="server" Width="60%" 
                    class="NormalText" MaxLength="100" onkeypress="NumericCheck(event)"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:30%;text-align:left;">
                &nbsp;</td>
            <td style="width:70%;text-align:left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;" colspan="2">
                <input id="btnSave" 
                    style="font-family: Cambria; font-size: 10pt; font-weight: normal; width: 10%;" 
                    type="button" value="SAVE" 
                     onclick="return btnSave_onclick()" />&nbsp;&nbsp;<input id="btnExit" 
                    style="font-family: Cambria; font-size: 10pt; font-weight: normal; width: 10%;" 
                    type="button" value="EXIT"   onclick="return btnExit_onclick()" /><asp:HiddenField 
                    ID="hdnMode" runat="server" />
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
</asp:Content>