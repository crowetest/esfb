﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ChangeLeaveBalance.aspx.vb" Inherits="ChangeLeaveBalance" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#EEB8A6; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
     
         function EmployeeOnChange() 
      {
          var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
          if (EmpCode != "")
              ToServer("1Ø" + EmpCode, 1);
      }
      function FromServer(Arg, Context) {

          switch (Context) {

              case 1:
                  {
                      if (Arg == "ØØ") {
                          alert("Invalid Employee Code"); document.getElementById("<%= txtName.ClientID %>").value = "";
                          document.getElementById("<%= txtBranch.ClientID %>").value = "";
                          document.getElementById("<%= txtDepartment.ClientID %>").value = "";
                          document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                          document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false;
                      }
                      var Data = Arg.split("Ø");
                      var EmpData = Data[0].split("^");
                      var BalData = Data[1].split("|");
                      var TakeLeaveData = Data[2].split("|");

                      document.getElementById("<%= txtName.ClientID %>").value = EmpData[1];
                      document.getElementById("<%= txtBranch.ClientID %>").value = EmpData[3];
                      document.getElementById("<%= txtDepartment.ClientID %>").value = EmpData[5];
                      document.getElementById("<%= txtDesignation.ClientID %>").value = EmpData[6];

                      document.getElementById("<%= txtCLBal.ClientID %>").value = BalData[0];
                      document.getElementById("<%= txtSLBal.ClientID %>").value = BalData[1];
                      document.getElementById("<%= txtPLBal.ClientID %>").value = BalData[2];

                      document.getElementById("<%= txtCL.ClientID %>").value = BalData[0];
                      document.getElementById("<%= txtSL.ClientID %>").value = BalData[1];
                      document.getElementById("<%= txtPL.ClientID %>").value = BalData[2];

                      document.getElementById("<%= txtFinalCL.ClientID %>").value = parseFloat(BalData[0])-parseFloat(TakeLeaveData[0]);
                      document.getElementById("<%= txtFinalSL.ClientID %>").value = parseFloat(BalData[1])-parseFloat(TakeLeaveData[1]);
                      document.getElementById("<%= txtFinalPL.ClientID %>").value =  parseFloat(BalData[2])-parseFloat(TakeLeaveData[2]);


                      document.getElementById("<%= txtTakeCL.ClientID %>").value = TakeLeaveData[0];
                      document.getElementById("<%= txtTakeSL.ClientID %>").value = TakeLeaveData[1];
                      document.getElementById("<%= txtTakePL.ClientID %>").value = TakeLeaveData[2];

                      document.getElementById("<%= txtClBal.ClientID %>").focus();
                      break;
                  }

              case 2:
                  {
                      var Data = Arg.split("Ø");
                      alert(Data[1]);
                      if (Data[0] == 0) window.open("ChangeLeaveBalance.aspx", "_self");
                      break;
                  }

          }

      }
      
      function LeaveOnChange(){
      document.getElementById("<%= txtCLBal.ClientID %>").value=(document.getElementById("<%= txtCLBal.ClientID %>").value =="") ? 0 : document.getElementById("<%= txtCLBal.ClientID %>").value;
      document.getElementById("<%= txtPLBal.ClientID %>").value=(document.getElementById("<%= txtPLBal.ClientID %>").value =="") ? 0 : document.getElementById("<%= txtPLBal.ClientID %>").value;
      document.getElementById("<%= txtSLBal.ClientID %>").value=(document.getElementById("<%= txtSLBal.ClientID %>").value =="") ? 0 : document.getElementById("<%= txtSLBal.ClientID %>").value;

       document.getElementById("<%= txtFinalCL.ClientID %>").value = parseFloat(document.getElementById("<%= txtCLBal.ClientID %>").value) - parseFloat(document.getElementById("<%= txtTakeCL.ClientID %>").value);
       document.getElementById("<%= txtFinalSL.ClientID %>").value = parseFloat(document.getElementById("<%= txtSLBal.ClientID %>").value) - parseFloat(document.getElementById("<%= txtTakeSL.ClientID %>").value);
       document.getElementById("<%= txtFinalPL.ClientID %>").value = parseFloat(document.getElementById("<%= txtPLBal.ClientID %>").value) - parseFloat(document.getElementById("<%= txtTakePL.ClientID %>").value);
       if (document.getElementById("<%= txtFinalCL.ClientID %>").value < 0) 
            {
                alert("CL Leave Balance can not be a negative value")                               
                var xxx="document.getElementById('<%= txtCLBal.ClientID %>').focus()";
                setTimeout(xxx,1);
                return false;
            }
             
            
              if (document.getElementById("<%= txtFinalPL.ClientID %>").value  < 0) 
            {
                alert("PL Leave Balance can not be a negative value") 
                
                var xxx="document.getElementById('<%= txtPLBal.ClientID %>').focus()";
                setTimeout(xxx,1);
                return false;
            }
            
             if (document.getElementById("<%= txtFinalSL.ClientID %>").value  < 0) 
            {
                alert("SL Leave Balance can not be a negative value") 
                               
                var xxx="document.getElementById('<%= txtSLBal.ClientID %>').focus()";
                setTimeout(xxx,1);
                return false;
            }
      }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnConfirm_onclick() {
            if (document.getElementById("<%= txtFinalCL.ClientID %>").value < 0) 
            {
                alert("CL Leave Balance can not be a negative value") 
                document.getElementById("<%= txtCLBal.ClientID %>").focus();
                return false;
            }
             
              if (document.getElementById("<%= txtFinalPL.ClientID %>").value  < 0) 
            {
                alert("PL Leave Balance can not be a negative value") 
                document.getElementById("<%= txtPLBal.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= txtFinalSL.ClientID %>").value  < 0) 
            {
                alert("SL Leave Balance can not be a negative value") 
                document.getElementById("<%= txtSLBal.ClientID %>").focus();
                return false;
            }
           
            if (document.getElementById("<%= txtCLBal.ClientID %>").value == "") 
            {
                alert("Enter CL Balance");
                document.getElementById("<%= txtCLBal.ClientID %>").focus();
                return false;
            }
           
             if (document.getElementById("<%= txtPLBal.ClientID %>").value == "") 
            {
                alert("Enter PL Balance");
                document.getElementById("<%= txtPLBal.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= txtSLBal.ClientID %>").value == "") 
            {
                alert("Enter SL Balance");
                document.getElementById("<%= txtSLBal.ClientID %>").focus();
                return false;
            }
	        var EmpID	= document.getElementById("<%= txtEmpcode.ClientID %>").value;
	        var CL	= document.getElementById("<%= txtCLBal.ClientID %>").value;
            var PL	= document.getElementById("<%= txtPLBal.ClientID %>").value;
            var SL	= document.getElementById("<%= txtSLBal.ClientID %>").value;
	        
            var ToData = "2Ø" + EmpID + "Ø" + CL + "Ø" + PL + "Ø" + SL ;
	       
            ToServer(ToData, 2);
        }
    </script>
   
</head>
</html>
    <asp:HiddenField ID="hid_depid" runat="server" />
    <asp:HiddenField ID="hid_data" runat="server" />
                 <asp:HiddenField ID="hid_ReportingTo" runat="server" />
<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;">
    <tr> <td style="width:25%;">
                   <asp:HiddenField ID="hdnCLBal" runat="server" />
                    </td>
    <td style="width:12%; text-align:left;">Employee Code</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpcode" class="NormalText" runat="server" Width="15%" 
                MaxLength="5"  ></asp:TextBox>
            </td>
    </tr>
        <tr>
        <td style="width:25%;">
                   <asp:HiddenField ID="hdnPLBal" runat="server" />
                    </td>
         <td style="width:12%; text-align:left;">Name</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:25%;">
                   <asp:HiddenField ID="hdnSLBal" runat="server" />
                   <asp:HiddenField ID="hdnTakenCL" runat="server" />
                   <asp:HiddenField ID="hdnTakenPL" runat="server" />
                    <asp:HiddenField ID="hdnTakenSL" runat="server" />
                    </td>
       <td style="width:12%; text-align:left;">Location</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Department</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDepartment" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Designation</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesignation" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">&nbsp;</td>
            <td style="width:63%">
                </td>


       </tr>
      <tr>
       <td style="width:100%;" colspan="3">
       <table align="center" style="width: 100%" >                                    
                    <tr>
                        <td style="width:12% ;text-align:center;" colspan="2" class="tblQal" >
                          Leave Balance As On 30-Jan-2015 </td>
                         <td style="width:12% ;text-align:center;"  colspan="6" class="tblQal">
                             Change
                          Leave Balance Details</td>
                    </tr>
                 
                    <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            CL Balance</td>
                       <td style="width:13% ;text-align:left;">
                             &nbsp;<asp:TextBox ID="txtCL" runat="server" 
                    Width="80%" class="ReadOnlyTextBox" MaxLength="6" onkeypress="return NumericCheck(event);" ReadOnly="true" ></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            CL Balance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtCLBal" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            Leave
                            Taken from&nbsp; 31-Jan-2015</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtTakeCL" runat="server" 
                    Width="80%" class="ReadOnlyTextBox" MaxLength="6" onkeypress="return NumericCheck(event);" ReadOnly="true"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:13% ;text-align:left;">
                            Final Balance -CL</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtFinalCL" runat="server" Width="80%" class="ReadOnlyTextBox" style="font-weight:bold;color:Red;"
                                MaxLength="6" onkeypress="return NumericCheck(event);" ReadOnly="true"></asp:TextBox>
                            </td>
                    </tr>
                  
                   <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            Current PL Balance</td>
                       <td style="width:13% ;text-align:left;">
                             &nbsp;<asp:TextBox ID="txtPL" runat="server" 
                    Width="80%" class="ReadOnlyTextBox" MaxLength="6" onkeypress="return NumericCheck(event);" ReadOnly="true" ></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            PL Balance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtPLBal" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);" ></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            Leave
                            Taken from&nbsp; 31-Jan-2015</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtTakePL" runat="server" 
                    Width="80%" class="ReadOnlyTextBox" MaxLength="6" onkeypress="return NumericCheck(event);" ReadOnly="true"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:13% ;text-align:left;">
                            Final Balance -PL</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtFinalPL" runat="server" Width="80%" class="ReadOnlyTextBox" style="font-weight:bold;color:Red;"
                                MaxLength="6" onkeypress="return NumericCheck(event);" ReadOnly="true"></asp:TextBox>
                            </td>
                    </tr>
                  <tr class="tblQalBody">
                        <td style="width:12% ;text-align:left;">
                            Current SL Balance</td>
                       <td style="width:13% ;text-align:left;">
                             &nbsp;<asp:TextBox ID="txtSL" runat="server" 
                    Width="80%" class="ReadOnlyTextBox" MaxLength="6" onkeypress="return NumericCheck(event);" ReadOnly="true" ></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            SL Balance</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtSLBal" runat="server" Width="80%" class="NormalText" 
                                MaxLength="6" onkeypress="return NumericCheck(event);"></asp:TextBox>
                        </td>
                        <td style="width:12% ;text-align:left;">
                            Leave
                            Taken from&nbsp; 31-Jan-2015</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtTakeSL" runat="server" 
                    Width="80%" class="ReadOnlyTextBox" MaxLength="6" onkeypress="return NumericCheck(event);" ReadOnly="true"></asp:TextBox>
                            &nbsp;</td>
                        <td style="width:13% ;text-align:left;">
                            Final Balance -SL</td>
                        <td style="width:13% ;text-align:left;">
                            &nbsp;<asp:TextBox ID="txtFinalSL" runat="server" Width="80%" class="ReadOnlyTextBox" style="font-weight:bold;color:Red;"
                                MaxLength="6" onkeypress="return NumericCheck(event);" ReadOnly="true"></asp:TextBox>
                            </td>
                    </tr>
                  
                      </table>
       </td>
       </tr>
       <tr>
       <td style="width:100%;" colspan="3">
       <asp:Panel ID="pnHistory" runat="server">
            </asp:Panel>
       </td>
       </tr>

       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE" onclick="return btnConfirm_onclick()"/>&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /><asp:HiddenField 
                    ID="hid_DesignationID" runat="server" />
            &nbsp;<asp:HiddenField ID="hid_branchid" runat="server" />
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

