﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AreaMaster.aspx.vb" Inherits="AreaMaster" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content3" ContentPlaceHolderID="CPH" runat="Server">

    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
         function window_onload() 
        {
            
            document.getElementById("<%= hdnData.ClientID %>").value = "";
            document.getElementById("<%= pnl_Area.ClientID %>").style.display = 'none';
        }
         $('input').on('keyup', function()
          {
             if (this.value.length > 1) 
             {
                var Region=document.getElementById("<%= cmbRegion.ClientID %>").value;
         var Area = document.getElementById("<%= txt_AreaName.ClientID %>").value;
         var ToData = "4ʘ" +Area;
         ToServer(ToData, 4);
             }

        }); 

        function btnExit_onclick() {

            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
       
         }
          function RegionOnChange()
        {
         
         var Region=document.getElementById("<%= cmbRegion.ClientID %>").value;
         var ToData = "3ʘ" + Region;
         ToServer(ToData, 3);

        }
         function AreaNameOnChange()
        {
         
         
         var Area = document.getElementById("<%= txt_AreaName.ClientID %>").value;
         var ToData = "4ʘ" +Area;
         ToServer(ToData, 4);

        }
         function AreaHeadOnChange(){

         var emp_code=document.getElementById("<%= cmbAreaHead.ClientID %>").value;
        
            if(document.getElementById("<%= cmbAreaHead.ClientID %>").value!="-1")
            {  
             var ToData = "2ʘ" + emp_code;
             
              ToServer(ToData, 2); 
            
            }
        }
        
           function table_Region_fill()
             {
            
                document.getElementById("<%= pnl_Area.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div  style='width:50%; height:25px;padding-top:0px;margin: 0px auto; background-color:#F8A0AA ' >";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr style='height:30px; background-color:Maroon; color:#FFF; font-weight:bold;'>";
                tab += "<td style='width:10%;text-align:center;color:#FFF;' >#</td>";
               
                tab += "<td style='width:40%;text-align:left;color:#FFF;' >Area Name</td>"; 
                tab += "<td style='width:50%;text-align:left;color:#FFF;' >Area Head</td>"; 
                tab += "</tr>";     
                tab += "</table></div>";  
                tab += "<div style='width:50%; height:250px; overflow:auto; margin:0px auto;' class='mainhead' >";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";  
                if (document.getElementById("<%= hdnData.ClientID %>").value != "")
                 {
                    row = document.getElementById("<%= hdnData.ClientID %>").value.split("¥");
                    for (n = 1; n <= row.length - 1; n++)
                    {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr  style='height:30px;text-align:center; background-color:#f1f1f1; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr  style='height:30px;text-align:center;background-color:#f1f1f1; padding-left:20px;'>";
                    }
                    
                    tab += "<td style='width:10%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:40%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:50%;text-align:left'>" + col[2] + "</td>";
                 
                    tab += "</tr>";
                 
                }
            }           
            tab += "</table></div>";
            document.getElementById("<%= pnl_Area.ClientID %>").innerHTML = tab;           

        }

         function table_Area_fill()
             {
               
                document.getElementById("<%= pnl_Area.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div  style='width:50%; height:25px;padding-top:0px;margin: 0px auto; background-color:#F8A0AA ' >";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr style='height:30px; background-color:Maroon; color:#FFF; font-weight:bold;'>";
                tab += "<td style='width:5%;text-align:center;color:#FFF;' >#</td>";
                tab += "<td style='width:25%;text-align:left;color:#FFF;' >Region Name</td>"; 
                tab += "<td style='width:35%;text-align:left;color:#FFF;' >Area Name</td>"; 
                tab += "<td style='width:33%;text-align:left;color:#FFF;' >Area Head</td>"; 
                tab += "<td style='width:2%;text-align:center;color:#FFF;' ></td>";
                tab += "</tr>";     
                tab += "</table></div>";  
                tab += "<div style='width:50%; height:250px; overflow:auto; margin:0px auto;' class='mainhead' >";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";  
                if (document.getElementById("<%= hdnData.ClientID %>").value != "")
                 {
                    row = document.getElementById("<%= hdnData.ClientID %>").value.split("¥");
                    for (n = 1; n <= row.length - 1; n++)
                    {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr  style='height:30px;text-align:center; background-color:#f1f1f1; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr  style='height:30px;text-align:center;background-color:#f1f1f1; padding-left:20px;'>";
                    }
                    
                    tab += "<td style='width:5%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:25%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:35%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:33%;text-align:left'>" + col[3] + "</td>";
                    tab += "</tr>";
                 
                }
            }           
            tab += "</table></div>";
            document.getElementById("<%= pnl_Area.ClientID %>").innerHTML = tab;           

        }

        
         function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("ʘ");
                alert(Data[1]);
                if (Data[0] == 0) window.open("AreaMaster.aspx", "_self");
           }
           if (context == 2)
           {
            var Data = arg.split("ʘ");
         
            document.getElementById("<%= txt_email.ClientID %>").value=Data[1];
           }
            else if (context == 3) {
               
                   document.getElementById("<%= hdnData.ClientID %>").value =arg;
             
                   table_Region_fill();
                   
             return false;
             }
              else if (context == 4) {
               
                         document.getElementById("<%= hdnData.ClientID %>").value =arg;
                            table_Area_fill();
                         return false;
                    }
             else
             {
             }
        }
     
        
        
       function btnSave_onclick() {
                         
                
                         var Region_id  = document.getElementById("<%= cmbRegion.ClientID %>").value;
                         var Region_Name = document.getElementById("<%= txt_AreaName.ClientID %>").value;
                         var Area_Head  = document.getElementById("<%= cmbAreaHead.ClientID %>").value;
                       
                     if (Region_id == -1) 
                    {
                        alert("Select Region");
                        document.getElementById("<%= cmbRegion.ClientID %>").focus();
                        return false;
                    }
                     if (Region_Name == "") 
                    {
                        alert("Enter Region Name");
                        document.getElementById("<%= txt_AreaName.ClientID %>").focus();
                        return false;
                    }
                    if(Area_Head==-1)
                    {

                         alert("Select Area Head");
                        document.getElementById("<%= cmbAreaHead.ClientID %>").focus();
                        return false;

                    }
                    

                      var ToData = "1ʘ" + Region_id+ "ʘ" + Region_Name + "ʘ" +Area_Head+ "ʘ" +document.getElementById("<%= txt_email.ClientID %>").value;
                            ToServer(ToData, 1);
                          


}

        
        
    </script>
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
     </script>
      
</head>
</html>

  <table align="center" style="width: 100%; margin: 0px auto;">
                <tr>
                    <td style="width: 27%; text-align: right;">
                      
                    </td>
                    
                    <td style="width: 32%; text-align: center; cursor:pointer;">
                        &nbsp;</td>
                    
                </tr>
               
                <tr id="Tr1">
                    <td style="width: 27%; text-align: right;">
                         Region&nbsp;&nbsp;</td>
                    
                    <td style="width: 27%; text-align: left;">
                         <asp:DropDownList ID="cmbRegion" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="32%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    
                </tr>
                <tr id="Tr2">
                    <td style="width: 27%; text-align: right;">
                        Area Name &nbsp;&nbsp;</td>
                    
                    <td style="width: 32%; text-align: left;">
                        <asp:TextBox ID="txt_AreaName" runat="server" height="20px" width="286px"></asp:TextBox>
                    </td>
                    
                </tr>
               <tr id="Tr4">
                    <td style="width: 27%; text-align: right;">
                         Area Head&nbsp;&nbsp;</td>
                    
                    <td style="width: 27%; text-align: left;">
                         <asp:DropDownList ID="cmbAreaHead" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="32%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    
                </tr>
              
                 <tr id="Tr3">
                    <td style="width: 27%; text-align: right;">
                       Email&nbsp;&nbsp;</td>
                    
                    <td style="width: 32%; text-align: left;">
                        <asp:TextBox ID="txt_email" runat="server" height="20px" width="286px" disabled="False"></asp:TextBox>
                    </td>
                    
                </tr>
                 <tr>
               <td style="width: 27%; text-align: right;"> </td>
               <td style="width: 32%; text-align: left;"></td>
                    
               </tr> 
                <tr>
               <td style="width: 27%; text-align: right;"> </td>
               <td style="width: 32%; text-align: left;"></td>
                    
               </tr> 
                <tr>
                     <td style="width:27%; text-align: center;" colspan="2" align='center'>
                         <asp:Panel ID="pnl_Area" style="width:100%; text-align:right;float:right; " runat="server">
                         </asp:Panel>
                         <asp:HiddenField ID="hdnData" runat="server" />
                     </td>
                     
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4">
                        &nbsp;&nbsp;<input
                        id="btnSave" type="button" value="SAVE"  style="font-family: cambria;
                        width: 6%; cursor: pointer;"   onclick="return btnSave_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input
                        id="btnExit" type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria;
                        width: 6%; cursor: pointer; height: 26px;"  onclick="return btnExit_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   
                    </td>
                </tr>
               
            </table>
   
<br /><br />
</asp:Content>

















