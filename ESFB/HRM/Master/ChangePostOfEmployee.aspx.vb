﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Audit_ChangePostOfEmployee
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim CallBackStr As New System.Text.StringBuilder
    Protected Sub Audit_ChangePostOfEmployee_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 370) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Change Employee Post"
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            If Not IsPostBack() Then
                DT = GF.GetEmpPost()
                If (DT.Rows.Count > 1) Then
                    Me.cmbPost.DataSource = DT
                    Me.cmbPost.DataValueField = DT.Columns(0).ColumnName
                    Me.cmbPost.DataTextField = DT.Columns(1).ColumnName
                    Me.cmbPost.DataBind()
                End If
            End If
            Me.txtEmpCode.Attributes.Add("onchange", "return GetEmployeeDtls()")
            Me.cmbPost.Attributes.Add("onchange", "return ChangePostEffect()")
            txtEmpCode.Focus()
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Select Case Data(0)
            Case CStr(1)
                Dim EmpID As Integer = CInt(Data(1))
                DT_EMP = GF.GetEmpPostDetails(CInt(Data(1)))
                If DT_EMP.Rows.Count > 0 Then
                    CallBackStr.Append(DT_EMP.Rows(0)(1))
                    CallBackStr.Append("Ø")
                    CallBackStr.Append(DT_EMP.Rows(0)(3))
                    CallBackStr.Append("Ø")
                    CallBackStr.Append(DT_EMP.Rows(0)(4))
                    CallBackStr.Append("Ø")
                    CallBackStr.Append(DT_EMP.Rows(0)(5))
                    CallBackStr.Append("Ø")
                    CallBackStr.Append(DT_EMP.Rows(0)(8))
                    CallBackStr.Append("Ø")
                    CallBackStr.Append(Format(DT_EMP.Rows(0)(6), "dd/MMM/yyyy"))
                    CallBackStr.Append("Ø")
                    CallBackReturn = "1~" + CStr(CallBackStr.ToString)
                Else
                    CallBackReturn = "2~" + "Invalid Employee Code!"
                End If
            Case CStr(2)
                Dim DR As DataRow
                Dim PostID As Integer = CInt(Data(1))
                If PostID = 5 Then
                    DT = GF.GetBranch()
                ElseIf PostID = 6 Then
                    DT = GF.GetAreas()
                ElseIf PostID = 7 Then
                    DT = GF.GetRegions()
                ElseIf PostID = 8 Then
                    DT = GF.GetZones()
                End If
                If DT.Rows.Count > 1 Then
                    For Each DR In DT.Rows
                        CallBackStr.Append(DR(0))
                        CallBackStr.Append("®")
                        CallBackStr.Append(DR(1))
                        CallBackStr.Append("¶")
                    Next
                    CallBackReturn = "1~" + CStr(CallBackStr.ToString)
                Else
                    CallBackReturn = "2~"
                End If
            Case CStr(3)
                Dim EmpCode As Integer = CInt(Data(1))
                Dim PostID As Integer = CInt(Data(2))
                Dim EffectiveValue As Integer = CInt(Data(3))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(5) As SqlParameter
                    Params(0) = New SqlParameter("@EmpID", SqlDbType.Int)
                    Params(0).Value = EmpCode
                    Params(1) = New SqlParameter("@PostID", SqlDbType.Int)
                    Params(1).Value = PostID
                    Params(2) = New SqlParameter("@EffValue", SqlDbType.Int)
                    Params(2).Value = EffectiveValue
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int, 5000)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(5).Value = CInt(Session("UserID"))
                    DB.ExecuteNonQuery("SP_EMP_POST_CHANGE", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End Select
    End Sub
End Class
