﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="TourEntryApprovalAuthority.aspx.vb" Inherits="TourEntryApprovalAuthority" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
     function cursorwait(e) {
            document.body.className = 'wait';
        }
           function cursordefault(e) {
            document.body.className = 'default';
        }
        function AddDay(strDate, intNum) {
            sdate = new Date(strDate);
            sdate.setDate(sdate.getDate() + intNum);
            return sdate.getMonth() + 1 + " " + sdate.getDate() + " " + sdate.getFullYear();
        }
        function Addrow() {
            if (document.getElementById("<%= hid_type.ClientID %>").value == 0) {
                if (document.getElementById("<%= txtlocation.ClientID %>").value == "") {
                    alert("Enter Location");
                    document.getElementById("<%= txtlocation.ClientID %>").focus();
                    return false;
                }
            }
            if (document.getElementById("<%= hid_type.ClientID %>").value == 1) {
                if (document.getElementById("<%= cmbbranch.ClientID %>").value == "-1") {
                    alert("Select branch");
                    document.getElementById("<%= cmbbranch.ClientID %>").focus();
                    return false;
                }
            }

            if (document.getElementById("<%= txtStartDt.ClientID %>").value == "") {
                alert("Select Start Date");
                document.getElementById("<%= txtStartDt.ClientID %>").focus();
                return false;
            }

            if (document.getElementById("<%= txtEndDt.ClientID %>").value == "") {
                alert("Select End Date");
                document.getElementById("<%= txtEndDt.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtpurpose.ClientID %>").value == "") {
                alert("Enter Travel Purpose");
                document.getElementById("<%= txtpurpose.ClientID %>").focus();
                return false;
            }


            var loctype = document.getElementById("<%= hid_type.ClientID %>").value;
            if (loctype == 0) {
                var location = document.getElementById("<%= txtlocation.ClientID %>").value;
                var branchname = document.getElementById("<%= txtlocation.ClientID %>").value;
                var branchId = 0;
            }
            if (loctype == 1) {
                var location = document.getElementById("<%= cmbbranch.ClientID %>").value;
                var branch = document.getElementById("<%= cmbbranch.ClientID %>").value.split("^");
                var branchname = branch[1];
                location = branchname;
                var branchId = branch[0];
            }
            
            var loctype = document.getElementById("<%= hid_type.ClientID %>").value;
            var startdate = document.getElementById("<%= txtStartDt.ClientID %>").value;
            var enddate = document.getElementById("<%= txtEndDt.ClientID %>").value;
            var purpose = document.getElementById("<%= txtpurpose.ClientID %>").value;
            document.getElementById("<%= hid_Display.ClientID %>").value += branchname + "µ" + loctype + "µ" + startdate + "µ" + enddate + "µ" + purpose + "µ" + branchId + "¥";
            
            table_fill();
            return false;
        }
        function table_fill() {
            if (document.getElementById("<%= hid_Display.ClientID %>").value == "") {
                document.getElementById("<%= pnlTourDtl.ClientID %>").innerHTML = "";
                
                return false;
            }

            var row_bg = 0;
            var tab = "";
            tab += "<table style='width:100%;font-family:cambria;frame=box ;border: thin solid #C0C0C0'>";
            tab += "<tr  class=mainhead>";
            tab += "<td style='width:10%;text-align:center'>Sl No</td>";
            tab += "<td style='width:30%;text-align:left' >Location</td>";
            tab += "<td style='width:10%;text-align:center'>Start Date</td>";
            tab += "<td style='width:10%;text-align:center'>End Date</td>";
            tab += "<td style='width:35%;text-align:left'>Purpose</td>";
            tab += "<td style='width:5%;text-align:center'></td>";
            tab += "</tr>";
            row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
            var txtBoxName = "";
            var txtBoxName1 = "";
            for (n = 0; n < row.length - 1; n++) {
                col = row[n].split("µ");
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr class=sub_first>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class=sub_second>";
                }
                var i = n + 1;
                tab += "<td style='width:10%;text-align:center'>" + i + "</td>";
                tab += "<td style='width:30%;text-align:left'>" + col[0] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[2] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[3] + "</td>";
                tab += "<td style='width:35%;text-align:left'>" + col[4] + "</td>";
                tab += "<td style='width:5%;text-align:center' onclick='delete_row(" + (n) + ")'><img  src='../../Image/cross.png' style='align:middle;cursor:pointer;' /></td>";
                tab += "</tr>";
            }
            tab += "</table>";
            
            document.getElementById("<%= pnlTourDtl.ClientID %>").innerHTML = tab;
            document.getElementById("<%= txtStartDt.ClientID %>").value="";
            document.getElementById("<%= txtEndDt.ClientID %>").value="";
            document.getElementById("<%= txtpurpose.ClientID %>").value = "";
            document.getElementById("<%= cmbbranch.ClientID %>").value = "-1";
            document.getElementById("<%= txtlocation.ClientID %>").value = "";
            if (document.getElementById("<%= hid_type.ClientID %>").value == 1) {
                document.getElementById("<%= cmbbranch.ClientID %>").focus();
            }
            else {
                document.getElementById("<%= txtlocation.ClientID %>").focus();
            }
        
        }



        
        function delete_row(a) {
            var data = "";

            var rows = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
            var len = Math.abs(rows.length-1); 
            for (n = 0; n <= len - 1; n++) {
                if (a != n) {
                    
                   data +=  rows[n] + "¥";

                }
            } 
            if (data == "¥") {
                document.getElementById("<%= hid_Display.ClientID %>").value = "";
            }
            else {
               
                document.getElementById("<%= hid_Display.ClientID %>").value = data;
            }
            
            
            table_fill();
        }
               
        function RequestOnchange(val) {
            if (val == "branch") {
                document.getElementById("branch").style.display = '';
                document.getElementById("location").style.display = 'none';
                document.getElementById("<%= hid_type.ClientID %>").value = 1; 
                document.getElementById("<%= cmbbranch.ClientID %>").focus();
            }
            if (val == "other") {
                document.getElementById("branch").style.display = 'none';
                document.getElementById("location").style.display = '';
                document.getElementById("<%= hid_type.ClientID %>").value = 0;
                document.getElementById("<%= txtlocation.ClientID %>").focus();
             }


        }

        function checkdate() {
        var Tourstartdt = document.getElementById("<%= txtTourStart.ClientID %>").value;
        var Tourenddt = document.getElementById("<%= txtTourEnd.ClientID %>").value;
        var startdt = document.getElementById("<%= txtStartDt.ClientID %>").value;
        var enddt = document.getElementById("<%= txtEndDt.ClientID %>").value;
        if (Tourstartdt != "" && Tourenddt != "") {
            if (startdt != "" && enddt != "") {
                var cnt = getDateDiff(startdt, enddt, "days");

                if (cnt < 0) {
                    alert("Invalid Date selection");
                    document.getElementById("<%= txtEndDt.ClientID %>").value = "";
                    document.getElementById("<%= txtEndDt.ClientID %>").focus();
                }


            }
        }
        else {
            alert("First Set the Travel Date");
            document.getElementById("<%= txtStartDt.ClientID %>").value = "";
            document.getElementById("<%= txtEndDt.ClientID %>").value = "";
            document.getElementById("<%= txtTourStart.ClientID %>").focus();
        }
        }
        function Mastercheckdate() {
            var startdt = document.getElementById("<%= txtTourStart.ClientID %>").value;
            var enddt = document.getElementById("<%= txtTourEnd.ClientID %>").value;
            document.getElementById("<%= hid_Display.ClientID %>").value = "";
            document.getElementById("<%= pnlTourDtl.ClientID %>").innerHTML = "";
            
            if (startdt != "" && enddt != "") {
                var cnt = getDateDiff(startdt, enddt, "days");

                if (cnt < 0) {
                    alert("Invalid Date selection");
                    document.getElementById("<%= txtTourEnd.ClientID %>").value = "";
                    document.getElementById("<%= txtTourEnd.ClientID %>").focus;
                }
                
                document.getElementById("<%= txtStartDt.ClientID %>").value = document.getElementById("<%= txtTourStart.ClientID %>").value;
                document.getElementById("<%= txtEndDt.ClientID %>").value = document.getElementById("<%= txtTourEnd.ClientID %>").value;

            }
           
        }
        function FromServer(arg, context) {
           cursordefault();
                var Data = arg.split("Ø");
                alert(Data[1]);
                document.getElementById("btnConfirm").disabled=false;
                if (Data[0] == 0) window.open("TourEntryApprovalAuthority.aspx", "_self");
           
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnConfirm_onclick() {
           var EmpCode=document.getElementById("<%= cmbEmployee.ClientID %>").value;
           if(EmpCode=="-1")
           { alert("Select Employee");
                document.getElementById("<%= cmbEmployee.ClientID %>").focus();
                return false;
           }
            if(document.getElementById("<%= hid_Display.ClientID %>").value==""){
                alert("Enter Atleast One Travel Details");
                document.getElementById("<%= radbranch.ClientID %>").focus();
                return false;
                  
            }

            var data = document.getElementById("<%= hid_Display.ClientID %>").value;
            var TourFrom = document.getElementById("<%= txtTourStart.ClientID %>").value;
            var TourTo = document.getElementById("<%= txtTourEnd.ClientID %>").value;
           var Remarks=document.getElementById("<%= txtRemarks.ClientID %>").value;
            var ToData = "1Ø" + data + "Ø" + TourFrom + "Ø" + TourTo + "Ø" + EmpCode+ "Ø" + Remarks;
             cursorwait();
            document.getElementById("btnConfirm").disabled=true;
            ToServer(ToData, 1);
        }
        function setStartDate(sender, args) {
            var StartDt = AddDay(document.getElementById("<%= txtTourStart.ClientID %>").value, 1);
            var EndDt = AddDay(document.getElementById("<%= txtTourEnd.ClientID %>").value, 1);
            sender._startDate = new Date(StartDt);
            sender._endDate = new Date(EndDt);
         }

         function viewReportOnClick()
         {
            var EmpCode=document.getElementById("<%= cmbEmployee.ClientID %>").value;
            if (EmpCode=="-1")
                window.open("../Reports/ConsolidatedBtwnDateReport.aspx?ID=1","_blank")
            else
                window.open("../Reports/viewPunchingReport.aspx?ID=3","_blank") 
         }

       
    </script>
   
</head>
</html>
<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;">
 
 <tr> <td style="width:25%;">&nbsp;</td>
       <td style="width:12%; text-align:left;">Employee</td>
            <td style="width:63%">
                 &nbsp; &nbsp;<asp:DropDownList ID="cmbEmployee" class="NormalText" runat="server" Font-Names="Cambria" 
                Width="61%" ForeColor="Black">
            </asp:DropDownList>
         </td>
       </tr>
 
 <tr> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Travel Start Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtTourStart" class="NormalText" runat="server" Width="30%" onkeypress="return false" ></asp:TextBox>
             <asp:CalendarExtender ID="txtTourStart_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtTourStart" Format="dd MMM yyyy">
             </asp:CalendarExtender>
            </td>
       </tr>
        <tr> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Travel End Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtTourEnd" class="NormalText" runat="server" Width="30%" onkeypress="return false"></asp:TextBox>
             <asp:CalendarExtender ID="txtTourEnd_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtTourEnd" Format="dd MMM yyyy">
             </asp:CalendarExtender>
            </td>
       </tr>
       
        <tr> <td style="width:25%;">&nbsp;</td>
        <td style="width:12%; text-align:left;">Remarks</td>
            <td style="width:63%">
                 &nbsp; &nbsp;<asp:TextBox ID="txtRemarks" class="NormalText" runat="server" Width="60%" 
                    MaxLength="100"></asp:TextBox>
            </td>
       </tr>
       
           <tr><td style="width:25%;"></td><td colspan="2" >
               <div style="background-color:#EEB8A6;color:#47476B; width: 551px; 
                   text-align:center;"><b>&nbsp; &nbsp;Enter Travel Details</b></div></td></tr>
            <tr> <td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">Location Type</td>
                <td style="width:63%">
                    &nbsp; &nbsp;<asp:RadioButton ID="radbranch" 
                     runat="server" Text="Branch" GroupName="rad" />&nbsp; &nbsp;
                <asp:RadioButton ID="radother" runat="server" Text="Other" GroupName="rad" />
                </td>
             </tr>
            <tr id="location" style="display:none;"> <td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">Location</td>
                <td style="width:63%">
                    &nbsp; &nbsp;<asp:TextBox ID="txtlocation" class="NormalText" runat="server" Width="60%" 
                    MaxLength="100"></asp:TextBox>
                </td>


           </tr>
       
            <tr id="branch"><td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">Select Branch</td>
                <td style="width:63%">
                    &nbsp; &nbsp;<asp:DropDownList ID="cmbbranch" class="NormalText" style="text-align: left;" runat="server" Font-Names="Cambria" 
                    Width="60%" ForeColor="Black">
                </asp:DropDownList>
                </td>
            
           </tr>
           <tr> <td style="width:25%;"></td>
           <td style="width:12%; text-align:left;">From Date</td>
                <td style="width:63%">
                    &nbsp; &nbsp;<asp:TextBox ID="txtStartDt" class="NormalText" runat="server" Width="30%" onkeypress="return false" ></asp:TextBox>
                 <asp:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" 
                     Enabled="true" TargetControlID="txtStartDt" OnClientShowing="setStartDate" Format="dd MMM yyyy">
                 </asp:CalendarExtender>
                </td>
           </tr>
            <tr > <td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">To Date</td>
                <td style="width:63%">
                    &nbsp; &nbsp;<asp:TextBox ID="txtEndDt" class="NormalText" runat="server" Width="30%" onkeypress="return false"></asp:TextBox>
                 <asp:CalendarExtender ID="txtEndDt_CalendarExtender1" runat="server" 
                     Enabled="true" TargetControlID="txtEndDt" OnClientShowing="setStartDate" Format="dd MMM yyyy">
                 </asp:CalendarExtender>
                </td>
           </tr>
           <tr><td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">Purpose of Travel</td>
                <td style="width:63%">
                    &nbsp; &nbsp;<asp:TextBox ID="txtpurpose" class="NormalText" runat="server" Width="60%" onkeypress="return validateNumber(event)" 
                    MaxLength="100"></asp:TextBox>
                 &nbsp; &nbsp;<asp:ImageButton ID="cmd_Add"  runat="server" Height="20px" 
                        Width="20px" ImageAlign="AbsMiddle" ImageUrl="~/Image/Add.png" ToolTip="Add" 
                        BorderStyle="Dotted"/></td>
           </tr>
       
       <tr><td style="text-align:center;" colspan="3"> &nbsp;</td></tr>
       <tr><td style="text-align:center;" colspan="3"><asp:Panel ID="pnlTourDtl"  style="background-color:#EEB8A6;" runat="server">
            </asp:Panel></td></tr>
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 
                 <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE" onclick="return btnConfirm_onclick()"/>&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /><asp:HiddenField 
                    ID="hid_Display" runat="server" />
                <asp:HiddenField ID="hid_From" runat="server" />
                <asp:HiddenField ID="hid_To" runat="server" />
&nbsp;<asp:HiddenField ID="hid_type" runat="server" />
            </td>
        </tr>

</table>    

<br />
</asp:Content>

