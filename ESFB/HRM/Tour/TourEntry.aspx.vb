﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class TourEntry
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim TR As New Tour
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 12) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Travel Request"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            DT = TR.GetBranch()
            GF.ComboFill(cmbbranch, DT, 0, 1)
            'Me.txtTourStart_CalendarExtender.StartDate = CDate(Session("TraDt")).AddDays(-20)
            'Me.txtTourEnd_CalendarExtender.StartDate = CDate(Session("TraDt"))
            'Me.cmbbranch.Attributes.Add("onchange", "return RequestOnchange()")
            Me.radbranch.Attributes.Add("onclick", "return RequestOnchange('branch')")
            Me.radother.Attributes.Add("onclick", "return RequestOnchange('other')")
            Me.txtStartDt.Attributes.Add("onchange", "return checkdate()")
            Me.txtEndDt.Attributes.Add("onchange", "return checkdate()")
            Me.txtTourStart.Attributes.Add("onchange", "return Mastercheckdate()")
            Me.txtTourEnd.Attributes.Add("onchange", "return Mastercheckdate()")
            Me.cmd_Add.Attributes.Add("onclick", "return Addrow()")
            ' Me.txtpurpose.Attributes.Add("onkeypress", "return AlphaNumericCheck()")
            radbranch.Checked = True
            radother.Checked = False
            cmbbranch.Focus()
            hid_type.Value = CStr(1)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        If RequestID = 1 Then
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim tourdata As String = CStr(Data(1))
            Dim tourStart As Date = CDate(Data(2))
            Dim tourEnd As Date = CDate(Data(3))

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@TourStart", SqlDbType.Date)
                Params(0).Value = tourStart
                Params(1) = New SqlParameter("@TourEnd", SqlDbType.Date)
                Params(1).Value = tourEnd
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = UserID
                Params(3) = New SqlParameter("@TourData", SqlDbType.VarChar, 5000)
                Params(3).Value = tourdata
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_HR_TOUR", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
                If ErrorFlag = 0 Then

                    Dim ToAddress As String = "hena.ek@esafmicrofin.com"
                    Dim EmpName As String = ""
                    Dim tt As String = SM.GetEmailAddress(UserID, 2)
                    Dim Val = Split(tt, "^")
                    If Val(0).ToString <> "" Then
                        ToAddress = Val(0)
                    End If
                    If Val(1).ToString <> "" Then
                        EmpName = Val(1).ToString
                    End If
                    Dim ccAddress As String = ""
                    'General.GetEmailAddress(EmpID, 3);
                    If ccAddress.Trim() <> "" Then
                        ccAddress += ","
                    End If

                    Dim Content As String = vbLf & vbLf & " Dear " + EmpName + ", " & vbLf & vbLf
                    
                    Content += " Travel Request generated from " + CStr(tourStart) + " To " + CStr(tourEnd) + " By " + CStr(Session("UserName")) + "[" + CStr(Session("UserID")) + "]" & vbLf & vbLf



                    Content = Content & Convert.ToString(vbLf & vbLf & " With Regards" & vbLf & vbLf & "HR Team" & vbLf & "Head, HR")
                    SM.SendMail(ToAddress, "Travel Request", Content, ccAddress & Convert.ToString("nisha.rathnan@esafmicrofin.com"))


                    End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
       
        End If
    End Sub
#End Region

End Class
