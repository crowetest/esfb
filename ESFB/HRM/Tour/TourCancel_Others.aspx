﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="TourCancel_Others.aspx.vb" Inherits="TourCancel_Others" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function table_fill() 
        {
            if (document.getElementById("<%= hid_Display.ClientID %>").value == "") {
                document.getElementById("<%= pnlTourDtl.ClientID %>").innerHTML = "";
                document.getElementById("<%= pnlTourDtl.ClientID %>").style.display = "none";
                return false;
            }

            var row_bg = 0;
            var tab = "";
            tab += "<table style='width:100%;font-family:cambria;frame=box ;border: thin solid #C0C0C0'>";
            tab += "<tr  class=mainhead>";
            tab += "<td style='width:10%;text-align:center'>Sl No</td>";
            tab += "<td style='width:30%;text-align:left' >Location</td>";
            tab += "<td style='width:10%;text-align:center'>Start Date</td>";
            tab += "<td style='width:10%;text-align:center'>End Date</td>";
            tab += "<td style='width:40%;text-align:left'>Purpose</td>";
            tab += "</tr>";
            row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
            var txtBoxName = "";
            var txtBoxName1 = "";
            for (n = 0; n < row.length - 1; n++) {
                col = row[n].split("µ");
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr class=sub_first>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class=sub_second>";
                }
                var i = n + 1;
                tab += "<td style='width:10%;text-align:center'>" + i + "</td>";
                tab += "<td style='width:30%;text-align:left'>" + col[0] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[1] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[2] + "</td>";
                tab += "<td style='width:40%;text-align:left'>" + col[3] + "</td>";
                tab += "</tr>";
            }
            tab += "</table>";

            document.getElementById("<%= pnlTourDtl.ClientID %>").innerHTML = tab;

        }
        function RequestOnchange() {
            if (document.getElementById("<%= cmbtour.ClientID %>").value != "-1") {
                var Data = document.getElementById("<%= cmbtour.ClientID %>").value.split("Ø");
                document.getElementById("<%= txtstartdt.ClientID %>").value = Data[2];
                document.getElementById("<%= txtenddt.ClientID %>").value = Data[3];
                document.getElementById("<%= txtdays.ClientID %>").value = Data[1];
                document.getElementById("<%= txtremarks.ClientID %>").value = Data[4];
                var RequestID = Data[0];
                var Data = "2Ø" + RequestID;
                ToServer(Data, 2);

            }
            else {
                document.getElementById("<%= txtstartdt.ClientID %>").value = "";
                document.getElementById("<%= txtenddt.ClientID %>").value = "";
                document.getElementById("<%= txtdays.ClientID %>").value = "";
                document.getElementById("<%= txtremarks.ClientID %>").value = "";
                document.getElementById("<%= txtreason.ClientID %>").value = "";

            }
            document.getElementById("<%= txtreason.ClientID %>").focus();
        }      
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("TourCancel_Others.aspx", "_self");
            }
            else if (context == 2) {
                document.getElementById("<%= hid_Display.ClientID %>").value = arg;
                table_fill();

            }
        }
        function btnConfirm_onclick() {
            if (document.getElementById("<%= cmbtour.ClientID %>").value == "-1") {
                alert("Select Travel Request");
                document.getElementById("<%= cmbtour.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtreason.ClientID %>").value == "") {
                alert("Enter Cancel Reason");
                document.getElementById("<%= txtreason.ClientID %>").focus();
                return false;
            }
            var ReqDtl = document.getElementById("<%= cmbtour.ClientID %>").value.split("Ø");
            var RequestID = ReqDtl[0];
            var reason = document.getElementById("<%= txtreason.ClientID %>").value;
            var Data = "1Ø" + RequestID + "Ø" + reason;
            ToServer(Data, 1);
        }

       

    </script>
</head>
</html>
<br /><br />

<table class="style1" style="width:80%;margin: 0px auto;">
    <tr> <td style="width:25%;"></td>
    <td style="width:12%; text-align:left;">Employee &amp; Travel</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbtour" class="NormalText" runat="server" Font-Names="Cambria" 
                Width="61%" ForeColor="Black">
            </asp:DropDownList>
            </td>
    </tr> 
    <tr> <td style="width:25%;"></td>
    <td style="width:12%; text-align:left;">Travel Start Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtstartdt" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="20%" 
                MaxLength="100"></asp:TextBox>
            </td>
       </tr>
       <tr><td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Travel End Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtenddt" class="ReadOnlyTextBox" ReadOnly="true" 
                runat="server" Width="20%" 
                MaxLength="100" ></asp:TextBox>
            
            </td>
       </tr>
       <tr><td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Travel Days</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtdays" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="20%" 
                MaxLength="100"></asp:TextBox>
            
            </td>
       </tr>
       <tr> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Remarks</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtremarks" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            
            </td>
       </tr>
       <tr><td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Cancel Reason</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtreason" class="NormalText" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            
            </td>
       </tr>
       <tr><td style="text-align:center;" colspan="3"><asp:Panel ID="pnlTourDtl"  style="background-color:#EEB8A6;" runat="server">
            </asp:Panel></td></tr>
       <tr> <td style="text-align:center;" colspan="3"><br />
                &nbsp; &nbsp; <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="CONFIRM" onclick="return btnConfirm_onclick()" />&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="window.open('../../Home.aspx', '_self');" />
            
            <asp:HiddenField 
                    ID="hid_Display" runat="server" />
            
            </td>
       </tr>
</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

