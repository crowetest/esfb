﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class TourApproval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim TR As New Tour
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 14) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Travel Approval"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            DT = TR.Getexist_tour(CInt(Session("UserID")), 0)
            GF.ComboFill(cmbtour, DT, 0, 1)

            Me.cmbtour.Attributes.Add("onchange", "return RequestOnchange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim M_RequestID As Integer = CInt(Data(1))
        Dim RequestID As Integer = CInt(Data(0))
        If RequestID = 1 Then
            Dim status As Integer = CInt(Data(2))
            Dim remarks As String = CStr(Data(3))
            Dim UserID As Integer = CInt(Session("UserID"))

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@loctype", SqlDbType.Int)
                Params(0).Value = status
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@moduletype", SqlDbType.Int)
                Params(4).Value = 3
                Params(5) = New SqlParameter("@requestid", SqlDbType.Int)
                Params(5).Value = M_RequestID
                Params(6) = New SqlParameter("@remarks", SqlDbType.VarChar, 500)
                Params(6).Value = remarks
                DB.ExecuteNonQuery("SP_HR_TOUR", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
                If ErrorFlag = 0 Then
                    Dim QRY As String = ""
                    QRY = "select convert(varchar,a.userid)+'|'+location+' - '+convert(varchar(11),tour_start_date,106)+' To '+convert(varchar(11),tour_end_date,106) +' - '+purpose  from EMP_TOUR_MASTER a,EMP_TOUR b where a.M_request_id=b.M_request_id and a.M_request_id=" & M_RequestID & ""

                    DT = DB.ExecuteDataSet(QRY).Tables(0)


                    Dim Employee() = Split(CStr(DT.Rows(0).Item(0)), "|")

                    Dim ToAddress As String = "hena.ek@esafmicrofin.com"
                    Dim EmpName As String = ""
                    Dim tt As String = SM.GetEmailAddress(CInt(Employee(0)), 1)
                    Dim Val = Split(tt, "^")
                    If Val(0).ToString <> "" Then
                        ToAddress = Val(0)
                    End If
                    If Val(1).ToString <> "" Then
                        EmpName = Val(1).ToString
                    End If
                    Dim ccAddress As String = ""
                    'General.GetEmailAddress(EmpID, 3);
                    If ccAddress.Trim() <> "" Then
                        ccAddress += ","
                    End If
                    Dim StrVal As String = ""
                    If status = 1 Then
                        StrVal = "Approved"
                    Else
                        StrVal = "Rejected"
                    End If
                    Dim Content As String = vbLf & vbLf & " Dear " + EmpName + ", " & vbLf & vbLf

                    Content += "Travel Request  " + StrVal + "   by " + CStr(Session("UserName")) + "[" + CStr(Session("UserID")) + "]" & vbLf & vbLf

                    Content = Content & Convert.ToString(vbLf & vbLf & " With Regards" & vbLf & vbLf & "HR Team" & vbLf & "Head, HR")
                    SM.SendMail(ToAddress, "Regularize", Content, ccAddress & Convert.ToString("nisha.rathnan@esafmicrofin.com"))


                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf RequestID = 2 Then
            Dim DTTS As New DataTable
            DTTS = TR.Getexist_tour_details(M_RequestID)
            hid_Display.Value = ""


            Dim strTS As New StringBuilder
            Dim strTSum As New StringBuilder
            Dim dr As DataRow
            'requestdate,leavetype,leavefrom,leaveto,leavedays,reason,status
            For Each dr In DTTS.Rows
                strTS.Append(dr(0).ToString())
                strTS.Append("µ")
                strTS.Append(dr(1).ToString())
                strTS.Append("µ")
                strTS.Append(dr(2).ToString())
                strTS.Append("µ")
                strTS.Append(dr(3).ToString())
                strTS.Append("¥")
            Next
            
            CallBackReturn = strTS.ToString()
        End If
    End Sub
#End Region
End Class
