﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class TourEntryApprovalAuthority
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim TR As New Tour
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Travel Approval"
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        DT = TR.GetBranch()
        GF.ComboFill(cmbbranch, DT, 0, 1)
        Me.txtTourStart_CalendarExtender.StartDate = DateAdd(DateInterval.Day, -10, CDate(Session("TraDt")))
        Me.txtTourEnd_CalendarExtender.StartDate = DateAdd(DateInterval.Day, -10, CDate(Session("TraDt")))
        'Me.cmbbranch.Attributes.Add("onchange", "return RequestOnchange()")
        Me.radbranch.Attributes.Add("onclick", "return RequestOnchange('branch')")
        Me.radother.Attributes.Add("onclick", "return RequestOnchange('other')")
        Me.txtstartdt.Attributes.Add("onchange", "return checkdate()")
        Me.txtEndDt.Attributes.Add("onchange", "return checkdate()")
        Me.txtTourStart.Attributes.Add("onchange", "return Mastercheckdate()")
        Me.txtTourEnd.Attributes.Add("onchange", "return Mastercheckdate()")
        Me.cmd_Add.Attributes.Add("onclick", "return Addrow()")
        DT = DB.ExecuteDataSet("select count(*) from roles_assigned where role_id = 4 and emp_code = " & CInt(Session("UserID")) & "").Tables(0)
        If CInt(DT.Rows(0)(0)) > 0 Then
            GF.ComboFill(cmbEmployee, GF.getReportingEmployees(CInt(Session("UserID")), 2), 0, 1)
        Else
            GF.ComboFill(cmbEmployee, GF.getReportingEmployees(CInt(Session("UserID")), 1), 0, 1)
        End If

        radbranch.Checked = True
        radother.Checked = False
        cmbbranch.Focus()
        hid_type.Value = CStr(1)
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        If RequestID = 1 Then
            Dim EmpCode As Integer = CInt(Data(4))
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim tourdata As String = CStr(Data(1))
            Dim tourStart As Date = CDate(Data(2))
            Dim tourEnd As Date = CDate(Data(3))
            Dim Remarks As String = CStr(Data(5))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(8) As SqlParameter
                Params(0) = New SqlParameter("@TourStart", SqlDbType.Date)
                Params(0).Value = tourStart
                Params(1) = New SqlParameter("@TourEnd", SqlDbType.Date)
                Params(1).Value = tourEnd
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = UserID
                Params(3) = New SqlParameter("@TourData", SqlDbType.VarChar, 5000)
                Params(3).Value = tourdata
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(6).Value = EmpCode
                Params(7) = New SqlParameter("@moduletype", SqlDbType.Int)
                Params(7).Value = 5
                Params(8) = New SqlParameter("@Remarks", SqlDbType.VarChar, 5000)
                Params(8).Value = Remarks
                DB.ExecuteNonQuery("SP_HR_TOUR", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)

            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message

        End If
    End Sub
#End Region

End Class
