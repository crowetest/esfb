﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Last_Work_date_Update_Details
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
#Region "Page Load & Disposed"
    Protected Sub Page_Break_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Update Last Working Date"
            Dim DR As DataRow
            Dim BranchList As String = CStr(GN.Decrypt(Request.QueryString.Get("BranchList")))
            DT = DB.ExecuteDataSet("select b.region_name,b.area_name,b.branch_id,b.branch_name,e.emp_code,e.emp_name,CONVERT(VARCHAR,d.emp_type_date,103) ,d.emp_type_remark from salary_inform_dtls d , salary_inform s , emp_master e ,BRMASTER B  " & _
                " where s.salary_id=d.salary_id and d.emp_code=e.emp_code and B.branch_id=s.branch_id and d.emp_type=5 and d.last_work_date is null and s.branch_id in ( " & BranchList & ")").Tables(0)
            If DT.Rows.Count <> 0 Then
                hid_Count.Value = DT.Rows.Count.ToString
                For Each DR In DT.Rows
                    Me.hid_Value.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ" + DR(2).ToString() + "ÿ" + DR(3).ToString() + "ÿ" + DR(4).ToString() + "ÿ" + DR(5).ToString() + "ÿ" + DR(6).ToString() + "ÿ" + DR(7).ToString()
                Next
            End If
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim StrValue As String = ""
        Select Case CInt(Data(0))
            Case 1
                StrValue = CStr(Data(1))
        End Select
    End Sub
#End Region
End Class
