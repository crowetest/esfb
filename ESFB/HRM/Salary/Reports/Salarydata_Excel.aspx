﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Salarydata_Excel.aspx.vb" Inherits="Salarydata_Excel" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head>
<title></title>
    <link href="../../../Style/Style.css" rel="stylesheet" type="text/css" />
    
   

    <script language="javascript" type="text/javascript">

        function  ShowOnClick() {
            var MonthID=document.getElementById("<%= hid_Month.ClientID %>").value;
            var YearID = document.getElementById("<%= hid_Year.ClientID %>").value;
            var TypeId=1;
            window.open("View_SalaryInputData_Excel.aspx?MonthID=" + btoa(MonthID) + "&YearID=" + btoa(YearID) + "&TypeId=" + btoa(TypeId) + "&TypeName=" + btoa("Resigned"), "_blank");
        }

       

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
</script>
</head>
 <table style="width: 80%;margin: 0px auto">
    <tr>
        <td colspan="3" style="width:100%; height: 15px;text-align: center;">
            
        </td>
    </tr>
    
    <tr>
        <td style="width: 35%; text-align: left;"></td>
        <td style="width: 10%; text-align: left;">
            Month
        </td>
        <td style="width: 55%">
            &nbsp; &nbsp;<asp:DropDownList ID="cmbMonth" runat="server" class="NormalText" Width="32%">
                </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="width: 35%; text-align: left;"></td>
        <td style="width: 10%; text-align: left;">
            Year
        </td>
        <td style="width: 45%">
            &nbsp; &nbsp;<asp:DropDownList ID="cmbYear" runat="server" class="NormalText" Width="32%">
                </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="width: 35%; text-align: left;"></td>
        <td style="width: 10%; text-align: left;">
            Employees  
        </td>
        <td style="width: 45%">
            &nbsp; &nbsp;<asp:DropDownList ID="cmbCategory" runat="server" class="NormalText" Width="32%">
                <asp:ListItem Value="-1"> All </asp:ListItem>
                </asp:DropDownList>
        </td>
    </tr>
    
    <tr>
        <td colspan="3" style="width:100%; height: 15px;text-align: center;">            
        </td>
    </tr>
    <tr>
        <td style="text-align: center; font-family:Cambria;" colspan="3">
           <input id="btnShow" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="SHOW"  onclick="return ShowOnClick()" />  &nbsp; &nbsp; &nbsp; 
           <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />   
                <asp:HiddenField ID="hid_Value" runat="server" />   
                <asp:HiddenField ID="hid_Month" runat="server" />
                <asp:HiddenField ID="hid_Year" runat="server" /> 
 
        </td>
    </tr>
 </table>
</asp:Content>

