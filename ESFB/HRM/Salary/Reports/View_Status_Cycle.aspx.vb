﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class View_Status_Cycle
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim WebTools As New WebApp.Tools
    Dim DT As New DataTable
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim MonthID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("MonthID")))
            Dim YearID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("YearID")))
            Dim EmpCode As Integer = CInt(GF.Decrypt(Request.QueryString.Get("EmpCode")))
            Dim DT As New DataTable
            DT = DB.ExecuteDataSet("select b.branch_id,b.branch_name,d.salary_status,d.salary_remark,d.LOP,d.ESI,d.ESIML, " & _
                 " ISNULL(d.last_work_date,'1/1/1900'),d.others,t.type_name,d.emp_type_date,e.emp_name  " & _
                 " from salary_inform s,salary_inform_dtls d, brmaster b , SALARY_STATUS_TYPES t,emp_master e " & _
                 " where s.salary_id=d.salary_id and b.branch_id=s.branch_id  and t.type_id=d.emp_type and d.emp_code=e.emp_code and  s.month_id=" & MonthID & " and s.year_id=" & YearID & " " & _
                 " and d.emp_code=" & EmpCode & " ").Tables(0)
            RH.Heading(Session("FirmName"), tb, "Salary Status Of " + DT.Rows(0)(11).ToString() + "(" + EmpCode.ToString() + ") in " + MonthName(MonthID).ToString() + " " + YearID.ToString(), 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 10, 10, "c", "Branch Code")
            RH.AddColumn(TRHead, TRHead_01, 35, 35, "c", "Branch Name")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "c", "Current Status")
            RH.AddColumn(TRHead, TRHead_03, 15, 15, "c", "Date")
            RH.AddColumn(TRHead, TRHead_04, 5, 5, "c", "Disburse")
            RH.AddColumn(TRHead, TRHead_05, 5, 5, "c", "Withheld")
            RH.AddColumn(TRHead, TRHead_06, 5, 5, "c", "LOP")
            RH.AddColumn(TRHead, TRHead_07, 5, 5, "c", "ESI")
            RH.AddColumn(TRHead, TRHead_08, 5, 5, "c", "ESI (Maternity)")
            RH.AddColumn(TRHead, TRHead_09, 5, 5, "c", "Unauthorized")
            tb.Controls.Add(TRHead)

            'RH.BlankRow(tb, 3)
            Dim i As Integer = 0
            Dim Remarks As String = ""
            Dim LastDt As String = ""
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid



                RH.AddColumn(TR3, TR3_00, 10, 10, "l", DR(0))
                RH.AddColumn(TR3, TR3_01, 35, 35, "l", DR(1))
                If DR(9) <> "Select" Then
                    RH.AddColumn(TR3, TR3_02, 10, 10, "l", DR(9))
                    RH.AddColumn(TR3, TR3_03, 15, 15, "l", CDate(DR(10)).ToString("dd/MM/yyyy"))
                Else
                    RH.AddColumn(TR3, TR3_02, 10, 10, "l", "")
                    RH.AddColumn(TR3, TR3_03, 15, 15, "l", "")
                End If
                If DR(2) = 1 Then
                    RH.AddColumn(TR3, TR3_04, 5, 5, "c", "<img id='attach' src='../../../Image/approve.png'   style='height:20px; width:20px; '/>")
                    RH.AddColumn(TR3, TR3_05, 5, 5, "l", "")
                Else
                    RH.AddColumn(TR3, TR3_04, 5, 5, "l", "")
                    RH.AddColumn(TR3, TR3_05, 5, 5, "c", "<img id='attach' src='../../../Image/approve.png'   style='height:20px; width:20px; '/>")
                End If
                If DR(4) = 0 Then
                    RH.AddColumn(TR3, TR3_06, 5, 5, "c", "")
                Else
                    RH.AddColumn(TR3, TR3_06, 5, 5, "c", DR(4).ToString())
                End If
                If DR(5) = 0 Then
                    RH.AddColumn(TR3, TR3_07, 5, 5, "c", "")
                Else
                    RH.AddColumn(TR3, TR3_07, 5, 5, "c", DR(5).ToString())
                End If
                If DR(6) = 0 Then
                    RH.AddColumn(TR3, TR3_08, 5, 5, "c", "")
                Else
                    RH.AddColumn(TR3, TR3_08, 5, 5, "c", DR(6).ToString())
                End If
                If DR(8) = 0 Then
                    RH.AddColumn(TR3, TR3_09, 5, 5, "c", "")
                Else
                    RH.AddColumn(TR3, TR3_09, 5, 5, "c", DR(8).ToString())
                End If

                tb.Controls.Add(TR3)
                Remarks = DR(3)
                LastDt = DR(7)

            Next

            RH.BlankRow(tb, 4)

            Dim TRRemark As New TableRow
            'TRRemark.BorderWidth = "1"
            'TRRemark.BorderStyle = BorderStyle.Solid
            Dim TRRemark_00, TRRemark_01 As New TableCell
            'TRRemark_00.BorderWidth = "1"
            'TRRemark_00.BorderColor = Drawing.Color.Silver
            'TRRemark_00.BorderStyle = BorderStyle.Solid
            'TRRemark_01.BorderWidth = "1"
            'TRRemark_01.BorderColor = Drawing.Color.Silver
            'TRRemark_01.BorderStyle = BorderStyle.Solid
            RH.AddColumn(TRRemark, TRRemark_00, 15, 15, "l", "Remark ")
            If LastDt <> "01/01/1900" Then
                Remarks += ", Last working date : " & LastDt
                RH.AddColumn(TRRemark, TRRemark_01, 85, 85, "l", "<img id='attach' src='../../../Image/star.png'   style='height:10px; width:10px; '/>" & " " & Remarks)
            Else
                RH.AddColumn(TRRemark, TRRemark_01, 85, 85, "l", Remarks)
            End If

            tb.Controls.Add(TRRemark)
            RH.BlankRow(tb, 4)

            Dim TRSHead As New TableRow
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")
            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            RH.AddColumn(TRSHead, TRSHead_00, 100, 100, "c", "STATUS HISTORY")
            tb.Controls.Add(TRSHead)
            RH.BlankRow(tb, 4)
            DT = DB.ExecuteDataSet("select c.stat_updated_date,e.emp_name,t.type_name,c.emp_type_date,c.salary_status,c.salary_remark  " & _
                 " from salary_inform s,salary_inform_dtls d, SALARY_STATUS_CYCLE C , SALARY_STATUS_TYPES t , emp_master e" & _
                 " where s.salary_id=d.salary_id and D.slno=c.slno  and t.type_id=c.emp_type  and  e.emp_code=c.stat_updated_user and s.month_id=" & MonthID & " and s.year_id=" & YearID & " " & _
                 " and d.emp_code=" & EmpCode & " ").Tables(0)

            Dim TRHead_1 As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"


            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver


            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead_1, TRHead_1_00, 5, 5, "C", "#")
            RH.AddColumn(TRHead_1, TRHead_1_01, 10, 10, "C", "Date")
            RH.AddColumn(TRHead_1, TRHead_1_02, 15, 15, "C", "User")
            RH.AddColumn(TRHead_1, TRHead_1_03, 10, 10, "C", "Status")
            RH.AddColumn(TRHead_1, TRHead_1_04, 10, 10, "C", "Date")
            RH.AddColumn(TRHead_1, TRHead_1_05, 5, 5, "C", "Disburse")
            RH.AddColumn(TRHead_1, TRHead_1_06, 5, 5, "C", "Withheld")
            RH.AddColumn(TRHead_1, TRHead_1_07, 40, 40, "C", "Remark")

            tb.Controls.Add(TRHead_1)
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                i += 1

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid




                RH.AddColumn(TR3, TR3_00, 5, 5, "C", i.ToString())
                RH.AddColumn(TR3, TR3_01, 10, 10, "l", CDate(DR(0)).ToString("dd/MM/yyyy"))
                RH.AddColumn(TR3, TR3_02, 15, 15, "l", DR(1))
                If DR(2) = "Select" Then
                    RH.AddColumn(TR3, TR3_03, 10, 10, "l", "")
                    RH.AddColumn(TR3, TR3_04, 10, 10, "l", "")
                Else
                    RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(2))
                    RH.AddColumn(TR3, TR3_04, 10, 10, "l", CDate(DR(3)).ToString("dd/MM/yyyy"))
                End If
                If CInt(DR(4)) = 1 Then
                    RH.AddColumn(TR3, TR3_05, 5, 5, "l", "<img id='attach' src='../../../Image/approve.png'   style='height:20px; width:20px; '/>")
                    RH.AddColumn(TR3, TR3_06, 5, 5, "l", "")
                Else
                    RH.AddColumn(TR3, TR3_05, 5, 5, "l", "")
                    RH.AddColumn(TR3, TR3_06, 5, 5, "l", "<img id='attach' src='../../../Image/approve.png'   style='height:20px; width:20px; '/>")
                End If
                RH.AddColumn(TR3, TR3_07, 40, 40, "l", DR(5).ToString())
                tb.Controls.Add(TR3)
            Next
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Export_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            WebTools.ExporttoExcel(DT, "Salary Status")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
