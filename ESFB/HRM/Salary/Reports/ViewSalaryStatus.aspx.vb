﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewSalaryStatus
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim WebTools As New WebApp.Tools
    Dim DT As New DataTable
    Dim MonthID As Integer
    Dim YearID As Integer
    Dim EmpCode As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim Id As Integer = CInt(GF.Decrypt(Request.QueryString.Get("ID"))) '1=dispersal , 2=withheld , 3=dispersal & withheld
            Dim TypeId As Integer = CInt(GF.Decrypt(Request.QueryString.Get("TypeId")))
            Dim BranchList As String = CStr(GF.Decrypt(Request.QueryString.Get("BranchList")))
            MonthID = CInt(GF.Decrypt(Request.QueryString.Get("MonthID")))
            YearID = CInt(GF.Decrypt(Request.QueryString.Get("YearID")))
            Dim IdName As String = CStr(GF.Decrypt(Request.QueryString.Get("IdName")))
            EmpCode = CInt(GF.Decrypt(Request.QueryString.Get("EmpCode")))
            Dim DR As DataRow

            If Id = 1 And TypeId = 1 Then 'dispersal
                RH.Heading(CStr(Session("FirmName")), tb, "Salary Disbursed Employees (Disbursal) ", 100)
                tb.Attributes.Add("width", "100%")
                RH.BlankRow(tb, 4)
                Dim TR2 As New TableRow
                Dim TR2_00 As New TableCell
                RH.InsertColumn(TR2, TR2_00, 100, 0, "<font size=3; ><b>Month : " & MonthName(MonthID) & " " & YearID & "</font>")
                TR2_00.BorderStyle = BorderStyle.None
                tb.Controls.Add(TR2)

                Dim TR As New TableRow
                Dim TR_00, TR_01, TR_02, TR_03, TR_04, TR_05, TR_06, TR_07, TR_08, TR_09, TR_10, TR_11, TR_12 As New TableCell
                RH.InsertColumn(TR, TR_00, 3, 2, "Sl No.")
                RH.InsertColumn(TR, TR_01, 12, 2, "Region Name")
                RH.InsertColumn(TR, TR_02, 13, 2, "Area Name")
                RH.InsertColumn(TR, TR_03, 5, 2, "Branch Code")
                RH.InsertColumn(TR, TR_04, 15, 2, "Branch Name")
                RH.InsertColumn(TR, TR_05, 5, 2, "Employee Code")
                RH.InsertColumn(TR, TR_06, 20, 2, "Employee Name")
                RH.InsertColumn(TR, TR_10, 5, 2, "LOP")
                RH.InsertColumn(TR, TR_11, 5, 2, "ESI")
                RH.InsertColumn(TR, TR_12, 5, 2, "ESI (Maternity)")
                RH.InsertColumn(TR, TR_07, 5, 2, "Unauthorized")
                tb.Controls.Add(TR)
                Dim I As Integer = 0
                Dim RegionName As String = ""
                Dim AreaName As String = ""
                Dim BrName As String = ""
                DT = DB.ExecuteDataSet("select b.region_name,b.area_name,b.branch_id,b.branch_name,e.emp_code,e.emp_name,d.LOP,d.ESI,d.ESIML,d.others " & _
                    " from salary_inform s,salary_inform_dtls d, brmaster b , emp_master e " & _
                    " where s.salary_id=d.salary_id and b.branch_id=s.branch_id and e.emp_code=d.emp_code and d.salary_status=1 and s.month_id=" & MonthID & " and s.year_id=" & YearID & " and s.branch_id in (" & BranchList & ") " & _
                    " order by 1,2,4,6").Tables(0)
                If DT.Rows.Count <> 0 Then
                    For Each DR In DT.Rows
                        I = I + 1
                        Dim TR1 As New TableRow
                        Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07, TR1_08, TR1_09, TR1_10, TR1_11, TR1_12 As New TableCell
                        RH.InsertColumn(TR1, TR1_00, 3, 0, I)
                        If RegionName = DR(0) Then
                            RH.InsertColumn(TR1, TR1_01, 12, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_01, 12, 0, DR(0))
                            RegionName = DR(0)
                        End If
                        TR1_01.Style.Add("font-weight", "bold")
                        If AreaName = DR(1) Then
                            RH.InsertColumn(TR1, TR1_02, 13, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_02, 13, 0, DR(1))
                            AreaName = DR(1)
                        End If
                        TR1_02.Style.Add("font-weight", "bold")
                        If BrName = DR(3) Then
                            RH.InsertColumn(TR1, TR1_03, 5, 0, "")
                            RH.InsertColumn(TR1, TR1_04, 15, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_03, 5, 0, DR(2))
                            RH.InsertColumn(TR1, TR1_04, 15, 0, DR(3))
                            BrName = DR(3)
                        End If
                        If Chk_Status_Cycle(CInt(DR(4))) = True Then
                            RH.InsertColumn(TR1, TR1_05, 5, 0, " <a href='View_Status_Cycle.aspx?MonthID=" + GF.Encrypt(MonthID.ToString) + "&YearID=" + GF.Encrypt(YearID.ToString) + " &EmpCode=" + GF.Encrypt(DR(4).ToString) + "' target='_blank'>" + DR(4).ToString())
                        Else
                            RH.InsertColumn(TR1, TR1_05, 5, 0, DR(4))
                        End If
                        RH.InsertColumn(TR1, TR1_06, 20, 0, DR(5))

                        If DR(6) = 0 Then
                            RH.InsertColumn(TR1, TR1_10, 5, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_10, 5, 2, DR(6))
                        End If
                        If DR(7) = 0 Then
                            RH.InsertColumn(TR1, TR1_11, 5, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_11, 5, 2, DR(7))
                        End If
                        If DR(8) = 0 Then
                            RH.InsertColumn(TR1, TR1_12, 5, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_12, 5, 2, DR(8))
                        End If
                        If DR(9) = 0 Then
                            RH.InsertColumn(TR1, TR1_07, 5, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_07, 5, 2, DR(9))
                        End If
                        tb.Controls.Add(TR1)
                    Next
                End If
            ElseIf Id = 2 And TypeId = 1 Then ' 2= withheld
                RH.Heading(CStr(Session("FirmName")), tb, "Salary Status Of Employees (Withheld)", 100)
                tb.Attributes.Add("width", "100%")
                RH.BlankRow(tb, 4)
                Dim TR2 As New TableRow
                Dim TR2_00 As New TableCell
                RH.InsertColumn(TR2, TR2_00, 100, 0, "<font size=3; ><b>Month : " & MonthName(MonthID) & " " & YearID & "</font>")
                TR2_00.BorderStyle = BorderStyle.None
                tb.Controls.Add(TR2)

                Dim TR As New TableRow
                Dim TR_00, TR_01, TR_02, TR_03, TR_04, TR_05, TR_06, TR_07, TR_08, TR_09, TR_10, TR_11, TR_12 As New TableCell
                RH.InsertColumn(TR, TR_00, 3, 2, "Sl No.")
                RH.InsertColumn(TR, TR_01, 8, 2, "Region Name")
                RH.InsertColumn(TR, TR_02, 8, 2, "Area Name")
                RH.InsertColumn(TR, TR_03, 5, 2, "Branch Code")
                RH.InsertColumn(TR, TR_04, 10, 2, "Branch Name")
                RH.InsertColumn(TR, TR_05, 5, 2, "Employee Code")
                RH.InsertColumn(TR, TR_06, 15, 2, "Employee Name")
                RH.InsertColumn(TR, TR_09, 27, 2, "Withheld Reason")
                RH.InsertColumn(TR, TR_10, 3, 2, "LOP")
                RH.InsertColumn(TR, TR_11, 3, 2, "ESI")
                RH.InsertColumn(TR, TR_12, 5, 2, "ESI (Maternity)")
                RH.InsertColumn(TR, TR_07, 5, 2, "Unauthorized")
                tb.Controls.Add(TR)
                Dim I As Integer = 0
                Dim RegionName As String = ""
                Dim AreaName As String = ""
                Dim BrName As String = ""
                DT = DB.ExecuteDataSet("select b.region_name,b.area_name,b.branch_id,b.branch_name,e.emp_code,e.emp_name,d.salary_status,d.salary_remark,d.LOP,d.ESI,d.ESIML,ISNULL(d.last_work_date,'1/1/1900'),d.others " & _
                    " from salary_inform s,salary_inform_dtls d, brmaster b , emp_master e " & _
                    " where s.salary_id=d.salary_id and b.branch_id=s.branch_id and e.emp_code=d.emp_code and d.salary_status=2 and s.month_id=" & MonthID & " and s.year_id=" & YearID & " and s.branch_id in (" & BranchList & ") " & _
                    " order by 1,2,4,6").Tables(0)
                If DT.Rows.Count <> 0 Then
                    For Each DR In DT.Rows
                        I = I + 1
                        Dim TR1 As New TableRow
                        Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07, TR1_08, TR1_09, TR1_10, TR1_11, TR1_12 As New TableCell
                        RH.InsertColumn(TR1, TR1_00, 3, 0, I)
                        If RegionName = DR(0) Then
                            RH.InsertColumn(TR1, TR1_01, 8, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_01, 8, 0, DR(0))
                            RegionName = DR(0)
                        End If
                        TR1_01.Style.Add("font-weight", "bold")
                        If AreaName = DR(1) Then
                            RH.InsertColumn(TR1, TR1_02, 8, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_02, 8, 0, DR(1))
                            AreaName = DR(1)
                        End If
                        TR1_02.Style.Add("font-weight", "bold")
                        If BrName = DR(3) Then
                            RH.InsertColumn(TR1, TR1_03, 5, 0, "")
                            RH.InsertColumn(TR1, TR1_04, 10, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_03, 5, 0, DR(2))
                            RH.InsertColumn(TR1, TR1_04, 10, 0, DR(3))
                            BrName = DR(3)
                        End If
                        If Chk_Status_Cycle(CInt(DR(4))) = True Then
                            RH.InsertColumn(TR1, TR1_05, 5, 0, " <a href='View_Status_Cycle.aspx?MonthID=" + GF.Encrypt(MonthID.ToString) + "&YearID=" + GF.Encrypt(YearID.ToString) + " &EmpCode=" + GF.Encrypt(DR(4).ToString) + "' target='_blank'>" + DR(4).ToString())
                        Else
                            RH.InsertColumn(TR1, TR1_05, 5, 0, DR(4))
                        End If
                        RH.InsertColumn(TR1, TR1_06, 15, 0, DR(5))
                        Dim Remark As String = DR(7)
                        If DR(11) <> "1/1/1900" Then
                            Remark += ", Last working date : " & DR(11)
                            RH.InsertColumn(TR1, TR1_09, 27, 0, "<img id='attach' src='../../../Image/star.png'   style='height:10px; width:10px; '/>" & " " & Remark)
                        Else
                            RH.InsertColumn(TR1, TR1_09, 27, 0, Remark)
                        End If
                        If DR(8) = 0 Then
                            RH.InsertColumn(TR1, TR1_10, 3, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_10, 3, 2, DR(8))
                        End If
                        If DR(9) = 0 Then
                            RH.InsertColumn(TR1, TR1_11, 3, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_11, 3, 2, DR(9))
                        End If
                        If DR(10) = 0 Then
                            RH.InsertColumn(TR1, TR1_12, 5, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_12, 5, 2, DR(10))
                        End If
                        If DR(12) = 0 Then
                            RH.InsertColumn(TR1, TR1_07, 5, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_07, 5, 2, DR(12))
                        End If


                        tb.Controls.Add(TR1)
                    Next
                End If
            ElseIf Id = 3 And TypeId = 1 Then ' 3=dispersal&withheld 
                RH.Heading(CStr(Session("FirmName")), tb, "Salary Status Of Employees ", 100)
                tb.Attributes.Add("width", "100%")
                RH.BlankRow(tb, 4)
                Dim TR2 As New TableRow
                Dim TR2_00 As New TableCell
                RH.InsertColumn(TR2, TR2_00, 100, 0, "<font size=3; ><b>Month : " & MonthName(MonthID) & " " & YearID & "</font>")
                TR2_00.BorderStyle = BorderStyle.None
                tb.Controls.Add(TR2)

                Dim TR As New TableRow
                Dim TR_00, TR_01, TR_02, TR_03, TR_04, TR_05, TR_06, TR_07, TR_08, TR_09, TR_10, TR_11, TR_12, TR_13 As New TableCell
                RH.InsertColumn(TR, TR_00, 3, 2, "Sl No.")
                RH.InsertColumn(TR, TR_01, 8, 2, "Region Name")
                RH.InsertColumn(TR, TR_02, 8, 2, "Area Name")
                RH.InsertColumn(TR, TR_03, 5, 2, "Branch Code")
                RH.InsertColumn(TR, TR_04, 8, 2, "Branch Name")
                RH.InsertColumn(TR, TR_05, 5, 2, "Employee Code")
                RH.InsertColumn(TR, TR_06, 13, 2, "Employee Name")
                RH.InsertColumn(TR, TR_07, 4, 2, "Disburse")
                RH.InsertColumn(TR, TR_08, 4, 2, "Withheld")
                RH.InsertColumn(TR, TR_09, 23, 2, "Withheld Reason")
                RH.InsertColumn(TR, TR_10, 3, 2, "LOP")
                RH.InsertColumn(TR, TR_11, 3, 2, "ESI")
                RH.InsertColumn(TR, TR_12, 5, 2, "ESI (Maternity)")
                RH.InsertColumn(TR, TR_13, 5, 2, "Unauthorized")
                tb.Controls.Add(TR)
                Dim I As Integer = 0
                Dim RegionName As String = ""
                Dim AreaName As String = ""
                Dim BrName As String = ""

                If EmpCode = 0 Then
                    DT = DB.ExecuteDataSet("select b.region_name,b.area_name,b.branch_id,b.branch_name,e.emp_code,e.emp_name,d.salary_status,d.salary_remark,d.LOP,d.ESI,d.ESIML,ISNULL(d.last_work_date,'1/1/1900'),d.others " & _
                    " from salary_inform s,salary_inform_dtls d, brmaster b , emp_master e " & _
                    " where s.salary_id=d.salary_id and b.branch_id=s.branch_id and e.emp_code=d.emp_code and s.month_id=" & MonthID & " and s.year_id=" & YearID & " and s.branch_id in (" & BranchList & ") " & _
                    " order by 1,2,4,6").Tables(0)
                Else
                    DT = DB.ExecuteDataSet("select b.region_name,b.area_name,b.branch_id,b.branch_name,e.emp_code,e.emp_name,d.salary_status,d.salary_remark,d.LOP,d.ESI,d.ESIML,ISNULL(d.last_work_date,'1/1/1900'),d.others " & _
                     " from salary_inform s,salary_inform_dtls d, brmaster b , emp_master e " & _
                     " where s.salary_id=d.salary_id and b.branch_id=s.branch_id and e.emp_code=d.emp_code and s.month_id=" & MonthID & " and s.year_id=" & YearID & " and d.emp_code=" & EmpCode & " and s.branch_id in (" & BranchList & ") " & _
                     " order by 1,2,4,6").Tables(0)
                End If
                If DT.Rows.Count <> 0 Then
                    For Each DR In DT.Rows
                        I = I + 1
                        Dim TR1 As New TableRow
                        Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07, TR1_08, TR1_09, TR1_10, TR1_11, TR1_12, TR1_13 As New TableCell
                        RH.InsertColumn(TR1, TR1_00, 3, 0, I)
                        If RegionName = DR(0) Then
                            RH.InsertColumn(TR1, TR1_01, 8, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_01, 8, 0, DR(0))
                            RegionName = DR(0)
                        End If
                        TR1_01.Style.Add("font-weight", "bold")
                        If AreaName = DR(1) Then
                            RH.InsertColumn(TR1, TR1_02, 8, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_02, 8, 0, DR(1))
                            AreaName = DR(1)
                        End If
                        TR1_02.Style.Add("font-weight", "bold")
                        If BrName = DR(3) Then
                            RH.InsertColumn(TR1, TR1_03, 5, 0, "")
                            RH.InsertColumn(TR1, TR1_04, 8, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_03, 5, 0, DR(2))
                            RH.InsertColumn(TR1, TR1_04, 8, 0, DR(3))
                            BrName = DR(3)
                        End If
                        If Chk_Status_Cycle(CInt(DR(4))) = True Then
                            RH.InsertColumn(TR1, TR1_05, 5, 0, " <a href='View_Status_Cycle.aspx?MonthID=" + GF.Encrypt(MonthID.ToString) + "&YearID=" + GF.Encrypt(YearID.ToString) + " &EmpCode=" + GF.Encrypt(DR(4).ToString) + "' target='_blank'>" + DR(4).ToString())
                        Else
                            RH.InsertColumn(TR1, TR1_05, 5, 0, DR(4))
                        End If

                        RH.InsertColumn(TR1, TR1_06, 13, 0, DR(5))
                        If DR(6) = 1 Then
                            RH.InsertColumn(TR1, TR1_07, 4, 2, "<img id='attach' src='../../../Image/approve.png'   style='height:20px; width:20px; '/>")
                            RH.InsertColumn(TR1, TR1_08, 4, 0, "")
                            RH.InsertColumn(TR1, TR1_09, 23, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_07, 4, 0, "")
                            RH.InsertColumn(TR1, TR1_08, 4, 2, "<img id='attach' src='../../../Image/approve.png'   style='height:20px; width:20px; '/>")
                            Dim Remark As String = DR(7)
                            If DR(11) <> "1/1/1900" Then
                                Remark += ", Last working date : " & DR(11)
                                RH.InsertColumn(TR1, TR1_09, 23, 0, "<img id='attach' src='../../../Image/star.png'   style='height:10px; width:10px; '/>" & " " & Remark)
                            Else
                                RH.InsertColumn(TR1, TR1_09, 23, 0, Remark)
                            End If

                        End If
                        If DR(8) = 0 Then
                            RH.InsertColumn(TR1, TR1_10, 3, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_10, 3, 2, DR(8))
                        End If
                        If DR(9) = 0 Then
                            RH.InsertColumn(TR1, TR1_11, 3, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_11, 3, 2, DR(9))
                        End If
                        If DR(10) = 0 Then
                            RH.InsertColumn(TR1, TR1_12, 5, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_12, 5, 2, DR(10))
                        End If
                        If DR(12) = 0 Then
                            RH.InsertColumn(TR1, TR1_13, 5, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_13, 5, 2, DR(12))
                        End If
                        tb.Controls.Add(TR1)
                    Next
                End If
            Else
                RH.Heading(CStr(Session("FirmName")), tb, IdName & " Employee's Salary Status Details ", 100)
                tb.Attributes.Add("width", "100%")
                RH.BlankRow(tb, 4)
                Dim TR2 As New TableRow
                Dim TR2_00 As New TableCell
                RH.InsertColumn(TR2, TR2_00, 100, 0, "<font size=3; ><b>Month : " & MonthName(MonthID) & " " & YearID & "</font>")
                TR2_00.BorderStyle = BorderStyle.None
                tb.Controls.Add(TR2)

                Dim TR As New TableRow
                Dim TR_00, TR_01, TR_02, TR_03, TR_04, TR_05, TR_06, TR_07, TR_08, TR_09, TR_10, TR_11, TR_12, TR_13, TR_14, TR_15 As New TableCell
                RH.InsertColumn(TR, TR_00, 3, 2, "Sl No.")
                RH.InsertColumn(TR, TR_01, 8, 2, "Region Name")
                RH.InsertColumn(TR, TR_02, 8, 2, "Area Name")
                RH.InsertColumn(TR, TR_03, 3, 2, "Branch Code")
                RH.InsertColumn(TR, TR_04, 8, 2, "Branch Name")
                RH.InsertColumn(TR, TR_05, 4, 2, "Employee Code")
                RH.InsertColumn(TR, TR_06, 10, 2, "Employee Name")
                RH.InsertColumn(TR, TR_14, 5, 2, IdName & " Date")
                RH.InsertColumn(TR, TR_15, 10, 2, "Remark")
                RH.InsertColumn(TR, TR_07, 4, 2, "Disburse")
                RH.InsertColumn(TR, TR_08, 4, 2, "Withheld")
                RH.InsertColumn(TR, TR_09, 14, 2, "Withheld Reason")
                RH.InsertColumn(TR, TR_10, 3, 2, "LOP")
                RH.InsertColumn(TR, TR_11, 3, 2, "ESI")
                RH.InsertColumn(TR, TR_12, 5, 2, "ESI (Maternity)")
                RH.InsertColumn(TR, TR_13, 5, 2, "Unauthorized")
                tb.Controls.Add(TR)
                Dim I As Integer = 0
                Dim RegionName As String = ""
                Dim AreaName As String = ""
                Dim BrName As String = ""
                DT = DB.ExecuteDataSet("select b.region_name,b.area_name,b.branch_id,b.branch_name,e.emp_code,e.emp_name,d.salary_status,d.salary_remark,d.LOP,d.ESI,d.ESIML,ISNULL(d.last_work_date,'1/1/1900'),d.others,d.emp_type_date,d.emp_type_remark " & _
                    " from salary_inform s,salary_inform_dtls d, brmaster b , emp_master e " & _
                    " where s.salary_id=d.salary_id and b.branch_id=s.branch_id and e.emp_code=d.emp_code and d.emp_type=" & Id & " and s.month_id=" & MonthID & " and s.year_id=" & YearID & " and s.branch_id in (" & BranchList & ") " & _
                    " order by 1,2,4,6").Tables(0)
                If DT.Rows.Count <> 0 Then
                    For Each DR In DT.Rows
                        I = I + 1
                        Dim TR1 As New TableRow
                        Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07, TR1_08, TR1_09, TR1_10, TR1_11, TR1_12, TR1_13, TR1_14, TR1_15 As New TableCell
                        RH.InsertColumn(TR1, TR1_00, 3, 0, I)
                        If RegionName = DR(0) Then
                            RH.InsertColumn(TR1, TR1_01, 8, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_01, 8, 0, DR(0))
                            RegionName = DR(0)
                        End If
                        TR1_01.Style.Add("font-weight", "bold")
                        If AreaName = DR(1) Then
                            RH.InsertColumn(TR1, TR1_02, 8, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_02, 8, 0, DR(1))
                            AreaName = DR(1)
                        End If
                        TR1_02.Style.Add("font-weight", "bold")
                        If BrName = DR(3) Then
                            RH.InsertColumn(TR1, TR1_03, 3, 0, "")
                            RH.InsertColumn(TR1, TR1_04, 8, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_03, 3, 0, DR(2))
                            RH.InsertColumn(TR1, TR1_04, 8, 0, DR(3))
                            BrName = DR(3)
                        End If
                        If Chk_Status_Cycle(CInt(DR(4))) = True Then
                            RH.InsertColumn(TR1, TR1_05, 5, 0, " <a href='View_Status_Cycle.aspx?MonthID=" + GF.Encrypt(MonthID.ToString) + "&YearID=" + GF.Encrypt(YearID.ToString) + " &EmpCode=" + GF.Encrypt(DR(4).ToString) + "' target='_blank'>" + DR(4).ToString())
                        Else
                            RH.InsertColumn(TR1, TR1_05, 5, 0, DR(4))
                        End If
                        RH.InsertColumn(TR1, TR1_06, 10, 0, DR(5))
                        RH.InsertColumn(TR1, TR1_14, 5, 0, DR(13))
                        RH.InsertColumn(TR1, TR1_15, 10, 0, DR(14))
                        If DR(6) = 1 Then
                            RH.InsertColumn(TR1, TR1_07, 4, 2, "<img id='attach' src='../../../Image/approve.png'   style='height:20px; width:20px; '/>")
                            RH.InsertColumn(TR1, TR1_08, 4, 0, "")
                            RH.InsertColumn(TR1, TR1_09, 14, 0, "")
                        Else
                            RH.InsertColumn(TR1, TR1_07, 4, 0, "")
                            RH.InsertColumn(TR1, TR1_08, 4, 2, "<img id='attach' src='../../../Image/approve.png'   style='height:20px; width:20px; '/>")
                            Dim Remark As String = DR(7)
                            If DR(11) <> "1/1/1900" Then
                                Remark += ", Last working date : " & DR(11)
                                RH.InsertColumn(TR1, TR1_09, 14, 0, "<img id='attach' src='../../../Image/star.png'   style='height:10px; width:10px; '/>" & " " & Remark)
                            Else
                                RH.InsertColumn(TR1, TR1_09, 14, 0, Remark)
                            End If

                        End If
                        If DR(8) = 0 Then
                            RH.InsertColumn(TR1, TR1_10, 3, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_10, 3, 2, DR(8))
                        End If
                        If DR(9) = 0 Then
                            RH.InsertColumn(TR1, TR1_11, 3, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_11, 3, 2, DR(9))
                        End If
                        If DR(10) = 0 Then
                            RH.InsertColumn(TR1, TR1_12, 5, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_12, 5, 2, DR(10))
                        End If
                        If DR(12) = 0 Then
                            RH.InsertColumn(TR1, TR1_13, 5, 2, "")
                        Else
                            RH.InsertColumn(TR1, TR1_13, 5, 2, DR(12))
                        End If
                        tb.Controls.Add(TR1)
                    Next
                End If
            End If

            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub
    Protected Sub cmd_Export_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            WebTools.ExporttoExcel(DT, "Salary Status")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Private Function Chk_Status_Cycle(EmpCode As Integer) As Boolean
        Dim DTT As New DataTable
        DTT = DB.ExecuteDataSet("select c.*  from salary_inform s,salary_inform_dtls d, SALARY_STATUS_CYCLE C" & _
             " where s.salary_id=d.salary_id and D.slno=c.slno   and s.month_id=" & MonthID & " and s.year_id=" & YearID & "  and d.emp_code=" & EmpCode & " ").Tables(0)
        If DTT.Rows.Count <> 0 Then
            Chk_Status_Cycle = True
        Else
            Chk_Status_Cycle = False
        End If

    End Function
End Class
