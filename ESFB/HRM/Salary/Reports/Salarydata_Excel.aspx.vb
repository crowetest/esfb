﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Salarydata_Excel
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
#Region "Page Load & Disposed"
    Protected Sub Page_Break_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Salary Input Details (Excel)"
            Me.hid_Month.Value = CStr(CDate(Session("TraDt")).Month)
            Me.hid_Year.Value = CStr(CDate(Session("TraDt")).Year)
            Dim cl_script1 As New System.Text.StringBuilder
            DT = DB.ExecuteDataSet("select '-1' as type_id,'------Select------' as type_name union all select type_id,type_name from salary_status_types where  status_id=1 and type_id<>-1 ").Tables(0)
            If DT.Rows.Count <> 0 Then
                GN.ComboFill(cmbCategory, DT, 0, 1)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim PostID As Integer = CInt(Session("Post_ID"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 '
                Dim IntId As Integer = CInt(Data(1))
                Dim ItemList As String = Nothing
                Dim StrValue As String = ""
                '-------------------REgion------------------------------------------------------------------------------
                If IntId = 1 Then '
                    If PostID = 5 Then
                        DT = DB.ExecuteDataSet("select region_id,region_name from brmaster where branch_head=" & UserID & " ").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    ElseIf PostID = 6 Then
                        DT = DB.ExecuteDataSet("select distinct region_id,region_name from brmaster where area_head=" & UserID & " ").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    Else
                        DT = DB.ExecuteDataSet("select distinct region_id,region_name from SALARY_STATE_EMP_DTLS s, brmaster b where s.state_id=b.state_id and s.emp_code=" & UserID & "").Tables(0)
                        If DT.Rows.Count > 0 Then
                            For Each DR In DT.Rows
                                StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                            Next
                        End If
                    End If
                    '------------------------------------Area------------------------------------------------
                ElseIf IntId = 2 Then
                    ItemList = CStr(Data(2))
                    If PostID = 5 Then
                        DT = DB.ExecuteDataSet("select area_id,area_name from brmaster where branch_head=" & UserID & " and region_id in (" & ItemList & ") ").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    ElseIf PostID = 6 Then
                        DT = DB.ExecuteDataSet("select distinct area_id,area_name from brmaster where area_head=" & UserID & " and region_id in (" & ItemList & ") ").Tables(0)
                        If DT.Rows.Count <> 0 Then
                            If DT.Rows.Count > 1 Then
                                For Each DR In DT.Rows
                                    StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                                Next
                            Else
                                StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                            End If
                        End If
                    Else
                        DT = DB.ExecuteDataSet("select distinct area_id,area_name from brmaster where  region_id in (" & ItemList & ") ").Tables(0)
                        If DT.Rows.Count <> 0 Then
                            For Each DR In DT.Rows
                                StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                            Next
                        End If
                    End If
                    '-------------------------------------------Branch-----------------------------
                ElseIf IntId = 3 Then
                    ItemList = CStr(Data(2))
                    If PostID = 5 Then
                        DT = DB.ExecuteDataSet("select branch_id,branch_name from brmaster where branch_head=" & UserID & " and area_id in (" & ItemList & ")").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    Else
                        DT = DB.ExecuteDataSet("select branch_id,branch_name from brmaster where  area_id in (" & ItemList & ")").Tables(0)
                        If DT.Rows.Count > 0 Then
                            For Each DR In DT.Rows
                                StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                            Next
                        End If
                    End If
                End If
                CallBackReturn = "1ʘ" + CStr(IntId) + "ʘ" + StrValue
            Case 2
                DT = DB.ExecuteDataSet("select -1 as type_id , '--------Select--------' as type_name union all  select type_id,type_name from SALARY_STATUS_TYPES where status_id=1   ").Tables(0)
                If DT.Rows.Count <> 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                    Next
                End If
        End Select
    End Sub
#End Region
End Class
