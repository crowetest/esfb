﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class View_SalaryInputData_Excel
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim DataRow As DataRow
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim HeaderText As String
            Dim MonthID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("MonthID")))
            Dim YearID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("YearID")))
            Dim TypeId As Integer = CInt(GF.Decrypt(Request.QueryString.Get("TypeId")))
            Dim TypeName As String = GF.Decrypt(Request.QueryString.Get("TypeName"))
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TypeID", TypeId), New System.Data.SqlClient.SqlParameter("@MonthId", MonthID), New System.Data.SqlClient.SqlParameter("@YearID", YearID)}
            DT = DB.ExecuteDataSet("[SP_SALARY_INPUT_EXCEL]", Parameters).Tables(0)
            'If hdnAuditTypeID.Value = "1" Then
            HeaderText = TypeName & "Employee Details"
            ExporttoExcel(DT, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Private Sub ExporttoExcel()
        If DT.Rows.Count > 0 Then
            Dim filename As String = "DownloadMobileNoExcel.xls"
            Dim tw As New System.IO.StringWriter()
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            Dim dgGrid As New DataGrid()
            dgGrid.DataSource = DT
            dgGrid.DataBind()



            '------

            '------
            'Get the HTML for the control.
            dgGrid.RenderControl(hw)
            'Write the HTML back to the browser.
            'Response.ContentType = application/vnd.ms-excel;
            Response.ContentType = "application/vnd.ms-excel"
            Response.AppendHeader("Content-Disposition", (Convert.ToString("attachment; filename=") & filename) + "")
            Me.EnableViewState = False
            Response.Write(tw.ToString())
            Response.[End]()
        End If
    End Sub

    Public Sub ExporttoExcel(ByRef dt As DataTable, ByVal FileName As String)
        Try
            CreateWorkBook(dt, "ExportToExcel", 140, 65)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Public Shared Sub CreateWorkBook(ByVal ds As DataTable, ByVal wbName As String, ByVal CellWidth As Integer, ByVal cellwidth1 As Integer)
        Dim attachment As String = "attachment; filename=""" & wbName & ".xls"""

        HttpContext.Current.Response.ClearContent()
        HttpContext.Current.Response.AddHeader("content-disposition", attachment)
        HttpContext.Current.Response.ContentType = "application/ms-excel"

        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()
        sw.WriteLine("<?xml version=""1.0""?>")

        sw.WriteLine("<?mso-application progid=""Excel.Sheet""?>")
        sw.WriteLine("<Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet""")

        sw.WriteLine("xmlns:o=""urn:schemas-microsoft-com:office:office""")
        sw.WriteLine("xmlns:x=""urn:schemas-microsoft-com:office:excel""")

        sw.WriteLine("xmlns:ss=""urn:schemas-microsoft-com:office:spreadsheet""")
        sw.WriteLine("xmlns:html=""http://www.w3.org/TR/REC-html40"">")

        sw.WriteLine("<DocumentProperties xmlns=""urn:schemas-microsoft-com:office:office"">")
        sw.WriteLine("<LastAuthor>Try Not Catch</LastAuthor>")

        sw.WriteLine("<Created>2010-05-15T19:14:19Z</Created>")
        sw.WriteLine("<Version>11.9999</Version>")

        sw.WriteLine("</DocumentProperties>")
        sw.WriteLine("<ExcelWorkbook xmlns=""urn:schemas-microsoft-com:office:excel"">")

        sw.WriteLine("<WindowHeight>9210</WindowHeight>")
        sw.WriteLine("<WindowWidth>19035</WindowWidth>")

        sw.WriteLine("<WindowTopX>0</WindowTopX>")
        sw.WriteLine("<WindowTopY>90</WindowTopY>")

        sw.WriteLine("<ProtectStructure>False</ProtectStructure>")
        sw.WriteLine("<ProtectWindows>False</ProtectWindows>")

        sw.WriteLine("</ExcelWorkbook>")
        sw.WriteLine("<Styles>")

        sw.WriteLine("<Style ss:ID=""Default"" ss:Name=""Normal"">")
        sw.WriteLine("<Alignment ss:Vertical=""Bottom""/>")

        sw.WriteLine("<Borders/>")
        sw.WriteLine("<Font/>")

        sw.WriteLine("<Interior/>")
        sw.WriteLine("<NumberFormat/>")

        sw.WriteLine("<Protection/>")
        sw.WriteLine("</Style>")

        sw.WriteLine("<Style ss:ID=""s22"">")
        sw.WriteLine("<Alignment ss:Vertical=""Bottom"" ss:WrapText=""1""/>")

        sw.WriteLine("<Borders>")
        sw.WriteLine("<Border ss:Position=""Bottom"" ss:LineStyle=""Continuous""  ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("</Borders>")
        sw.WriteLine("<Font ss:Color=""#B21C32"" ss:Bold=""1"" />")
        sw.WriteLine("<Interior ss:Color=""#F1F1F1"" ss:Pattern=""Solid""/>")
        sw.WriteLine("</Style>")

        sw.WriteLine("<Style ss:ID=""s23"">")
        sw.WriteLine("<Alignment ss:Vertical=""Bottom"" ss:WrapText=""1""/>")

        sw.WriteLine("<Borders>")
        sw.WriteLine("<Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("</Borders>")
        sw.WriteLine("<Interior ss:Color=""#EDC9AF"" ss:Pattern=""Solid""/>")
        sw.WriteLine("</Style>")
        sw.WriteLine("<Style ss:ID=""s24"">")

        sw.WriteLine("<Alignment ss:Horizontal=""Center"" ss:Vertical=""Center"" ss:WrapText=""1""/>")
        sw.WriteLine("<Borders>")

        sw.WriteLine("<Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1""")
        sw.WriteLine("ss:Color=""#757373""/>")

        sw.WriteLine("<Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1""")
        sw.WriteLine("ss:Color=""#757373""/>")

        sw.WriteLine("<Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1""")
        sw.WriteLine("ss:Color=""#757373""/>")

        sw.WriteLine("<Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1""")
        sw.WriteLine("ss:Color=""#757373""/>")

        sw.WriteLine("</Borders>")
        sw.WriteLine("<Font ss:Color=""#FFFFFF"" ss:Bold=""1""/>")

        sw.WriteLine("<Interior ss:Color=""#757373"" ss:Pattern=""Solid""/>") 'set header colour here
        sw.WriteLine("</Style>")


        sw.WriteLine("<Style ss:ID=""s25"">")
        sw.WriteLine("<Alignment ss:Vertical=""Bottom"" ss:WrapText=""1""/>")

        sw.WriteLine("<Borders>")
        sw.WriteLine("<Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("</Borders>")
        sw.WriteLine("<Interior ss:Color=""#FFFFFF"" ss:Pattern=""Solid""/>")
        sw.WriteLine("</Style>")


        sw.WriteLine("<Style ss:ID=""s26"">")
        sw.WriteLine("<Alignment ss:Vertical=""Bottom"" ss:WrapText=""1""/>")

        sw.WriteLine("<Borders>")
        sw.WriteLine("<Border ss:Position=""Bottom"" ss:LineStyle=""Continuous""  ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("<Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1""")

        sw.WriteLine("ss:Color=""#000000""/>")
        sw.WriteLine("</Borders>")
        sw.WriteLine("<Font ss:Color=""#B21C32"" ss:Bold=""1"" ss:Italic=""1""/>")
        sw.WriteLine("<Interior ss:Color=""#F1F1F1"" ss:Pattern=""Solid""/>")
        sw.WriteLine("</Style>")

        sw.WriteLine("</Styles>")
        CreateWorkSheet(ds.TableName, sw, ds, CellWidth, cellwidth1)
        sw.WriteLine("</Workbook>")
        HttpContext.Current.Response.Write(sw.ToString())
        HttpContext.Current.Response.End()

    End Sub
    Private Shared Sub CreateWorkSheet(ByVal wsName As String, ByVal sw As System.IO.StringWriter, ByVal dt As DataTable, ByVal cellwidth As Integer, ByVal cellwidth1 As Integer)
        If (dt.Columns.Count > 0) Then
            sw.WriteLine("<Worksheet ss:Name=""" & wsName & """>")
            Dim cCount As Integer = dt.Columns.Count
            Dim rCount As Long = dt.Rows.Count + 1
            'Dim cCount1 As Integer = 1
            'Dim rCount1 As Long = 1
            'Dim dType As String = "String"
            'Dim gcText As String = "Business Plan :2015-2016"
            'Dim cellwidth11 As Integer = 700
            'sw.WriteLine("<Table1 ss:ExpandedColumnCount=""" & cCount & """ ss:ExpandedRowCount=""" & rCount & """ x:FullColumns=""1""")
            'sw.WriteLine("x:FullRows=""1"">")
            'sw.WriteLine("<Column ss:AutoFitWidth=""1"" ss:Width=""" & cellwidth11 & """/>")
            'sw.WriteLine("<Row>")
            'sw.WriteLine("<Cell ss:StyleID=""s24""><Data ss:Type=""" & dType & """>" & gcText & "</Data></Cell>")
            'sw.WriteLine("</Row>")


            sw.WriteLine("<Table ss:ExpandedColumnCount=""" & cCount & """ ss:ExpandedRowCount=""" & rCount & """ x:FullColumns=""1""")
            sw.WriteLine("x:FullRows=""1"">")
            For i As Integer = 0 To (cCount - 1)
                sw.WriteLine("<Column ss:AutoFitWidth=""1"" ss:Width=""" & cellwidth & """/>")
                'If i = 0 Then
                '    sw.WriteLine("<Column ss:AutoFitWidth=""1"" ss:Width=""" & cellwidth & """/>")
                'Else
                '    sw.WriteLine("<Column ss:AutoFitWidth=""1"" ss:Width=""" & cellwidth1 & """/>")
                'End If
            Next

            GridRowIterate(dt, sw)
            'sw.WriteLine("</Table1>")
            sw.WriteLine("</Table>")
            sw.WriteLine("<WorksheetOptions xmlns=""urn:schemas-microsoft-com:office:excel"">")

            sw.WriteLine("<Selected/>")
            sw.WriteLine("<DoNotDisplayGridlines/>")

            sw.WriteLine("<ProtectObjects>False</ProtectObjects>")
            sw.WriteLine("<ProtectScenarios>False</ProtectScenarios>")

            sw.WriteLine("</WorksheetOptions>")
            sw.WriteLine("</Worksheet>")
        End If
    End Sub
    Private Shared Sub GridRowIterate(ByVal dt As DataTable, ByVal sw As System.IO.StringWriter)
        'sw.WriteLine("<Row>")
        'For Each tc As DataColumn In dt.Columns
        '    Dim tcText As String = tc.ColumnName
        '    Dim tcWidth As String = tc.MaxLength
        '    Dim dType As String = "String"
        '    If IsNumeric(tcText) = True Then
        '        dType = "Number"
        '    End If
        '    sw.WriteLine("<Cell ss:StyleID=""s24""><Data ss:Type=""String"">" & tcText & "</Data></Cell>")
        'Next
        'sw.WriteLine("</Row>")
        For Each dr As DataRow In dt.Rows
            sw.WriteLine("<Row>")
            For i = 0 To dt.Columns.Count - 1
                Dim gcText As String = ""
                If Not IsDBNull(dr.Item(i)) Then
                    gcText = dr.Item(i)
                End If
                Dim dType As String = "String"
                If IsNumeric(gcText) = True Then
                    dType = "Number"
                    gcText = CStr(CDbl(gcText))
                End If

                If dr.Item(0) = "Employee Code" Then
                    sw.WriteLine("<Cell ss:StyleID=""s24""><Data ss:Type=""" & dType & """>" & gcText & "</Data></Cell>")
                Else
                    sw.WriteLine("<Cell ss:StyleID=""s22""><Data ss:Type=""" & dType & """>" & gcText & "</Data></Cell>")
                End If
                
            Next
            sw.WriteLine("</Row>")
        Next
    End Sub

End Class
