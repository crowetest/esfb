﻿
Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf

Partial Class Reports_viewSalaryUpdationReport_Area
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT As New DataTable
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 360) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            DT = DB.ExecuteDataSet("SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0))").Tables(0)
            Dim SalaryDt As Date = CDate(DT.Rows(0)(0))
            Dim PostID As Integer = CInt(Session("Post_ID"))
            Dim UserID As Integer = CInt(Session("UserID"))

            If (PostID = 6) Then
                DT = DB.ExecuteDataSet("select a.area_id,a.area_name,sum(case when (isnull(b.STATUS_ID,0)=0 )then 1 else 0 end) as BM_Pending,sum(case when isnull(b.STATUS_ID,0) in (1,2) then 1 else 0 end) as AM_Pending from [BrMaster] a left outer join SALARY_INFORM b on (a.Branch_ID=b.BRANCH_ID and MONTH_ID =MONTH('" + SalaryDt + "') and YEAR_ID =YEAR('" + SalaryDt + "') ) where  a.Branch_ID > 100 and a.Area_Head = " & UserID & " group by  a.area_id,a.area_name order by 2").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select a.area_id,a.area_name,sum(case when (isnull(b.STATUS_ID,0)=0 )then 1 else 0 end) as BM_Pending,sum(case when isnull(b.STATUS_ID,0) in (1,2) then 1 else 0 end) as AM_Pending from [BrMaster] a left outer join SALARY_INFORM b on (a.Branch_ID=b.BRANCH_ID and MONTH_ID =MONTH('" + SalaryDt + "') and YEAR_ID =YEAR('" + SalaryDt + "') ) where  a.Branch_ID > 100 group by  a.area_id,a.area_name order by 2").Tables(0)
            End If

            RH.Heading(Session("FirmName"), tb, "SALARY UPDATION PENDING REPORT - AREA WISE", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            ' tb.Attributes.Add("border", "1")
            Dim TR0 As New TableRow

            Dim TR0_00, TR0_01, TR0_02, TR0_03 As New TableCell
            TR0.BackColor = Drawing.Color.WhiteSmoke
            RH.InsertColumn(TR0, TR0_00, 10, 2, "SI No")
            RH.InsertColumn(TR0, TR0_01, 30, 0, "Area")
            RH.InsertColumn(TR0, TR0_02, 30, 2, "BM Pending")
            RH.InsertColumn(TR0, TR0_03, 30, 2, "AM Pending")
            tb.Controls.Add(TR0)

            Dim TotalBM As Integer = 0
            Dim TotalAM As Integer = 0
            Dim Count As Integer = 1
            For Each DR In DT.Rows
                Dim TR1 As New TableRow
                Dim TR1_00, TR1_01, TR1_02, TR1_03 As New TableCell
                RH.InsertColumn(TR1, TR1_00, 10, 2, Count.ToString())
                RH.InsertColumn(TR1, TR1_01, 30, 0, "<a href='viewSalaryUpdationReport_Branch.aspx?&AreaID=" & GF.Encrypt(DR(0).ToString()) & "&SalaryDt=" + SalaryDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(1).ToString().ToUpper + "</a>")
                RH.InsertColumn(TR1, TR1_02, 30, 2, DR(2).ToString())
                RH.InsertColumn(TR1, TR1_03, 30, 2, DR(3).ToString())
                tb.Controls.Add(TR1)
                Count += 1
                TotalBM += CDbl(DR(2))
                TotalAM += CDbl(DR(3))
            Next
            RH.DrawLine(tb, 100)

            Dim TRTotal As New TableRow
            TRTotal.BackColor = Drawing.Color.Silver
            Dim TRTotal_00, TRTotal_01, TRTotal_02, TRTotal_03 As New TableCell
            TRTotal_00.BackColor = Drawing.Color.Silver
            TRTotal_01.BackColor = Drawing.Color.Silver
            TRTotal_02.BackColor = Drawing.Color.Silver
            TRTotal_03.BackColor = Drawing.Color.Silver

            RH.AddColumn(TRTotal, TRTotal_00, 10, 10, "c", "")
            RH.AddColumn(TRTotal, TRTotal_01, 30, 30, "l", "<a href='viewSalaryUpdationReport_Branch.aspx?&AreaID=" + GF.Encrypt("-1") + "&SalaryDt=" + SalaryDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>TOTAL</a>")
            RH.AddColumn(TRTotal, TRTotal_02, 30, 30, "c", TotalBM.ToString())
            RH.AddColumn(TRTotal, TRTotal_03, 30, 30, "c", TotalAM.ToString())

            tb.Controls.Add(TRTotal)
            RH.DrawLine(tb, 100)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
