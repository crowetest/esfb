﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewSalaryUpdationReport_Region
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT As New DataTable
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim SalaryDt As Date = CDate(Request.QueryString.Get("SalaryDt"))
            Dim AreaID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("AreaID")))
            Dim ID As Integer = CInt(Request.QueryString.Get("ID"))
            If AreaID = "-1" Then
                DT = DB.ExecuteDataSet("select a.Branch_ID ,a.Branch_Name ,case when (isnull(b.STATUS_ID,0)=0 )then 'NO' else 'YES' end as BM_Updated,case when isnull(b.STATUS_ID,0) <1 then 'NO' else 'YES' end as BM_Submitted ,case when isnull(b.STATUS_ID,0) <2 then 'NO' else 'YES' end as AM_Updated,case when isnull(b.STATUS_ID,0) <3 then 'NO' else 'YES' end as AM_Submitted from [BrMaster] a left outer join SALARY_INFORM b on (a.Branch_ID=b.BRANCH_ID and MONTH_ID =MONTH('" + SalaryDt + "') and YEAR_ID =YEAR('" + SalaryDt + "') ) where  a.Branch_ID > 100  order by a.branch_name,a.branch_id").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select a.Branch_ID ,a.Branch_Name ,case when (isnull(b.STATUS_ID,0)=0 )then 'NO' else 'YES' end as BM_Updated,case when isnull(b.STATUS_ID,0) <1 then 'NO' else 'YES' end as BM_Submitted ,case when isnull(b.STATUS_ID,0) <2 then 'NO' else 'YES' end as AM_Updated,case when isnull(b.STATUS_ID,0) <3 then 'NO' else 'YES' end as AM_Submitted from [BrMaster] a left outer join SALARY_INFORM b on (a.Branch_ID=b.BRANCH_ID and MONTH_ID =MONTH('" + SalaryDt + "') and YEAR_ID =YEAR('" + SalaryDt + "') ) where  a.Branch_ID > 100 and a.Area_ID = " & AreaID & " order by a.branch_name,a.branch_id").Tables(0)
            End If

            RH.Heading(Session("FirmName"), tb, "SALARY UPDATION PENDING REPORT - BRANCH WISE", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            ' tb.Attributes.Add("border", "1")
            Dim TR0 As New TableRow

            Dim TR0_00, TR0_01, TR0_02, TR0_03, TR0_04, TR0_05, TR0_06 As New TableCell
            TR0.BackColor = Drawing.Color.WhiteSmoke
            RH.InsertColumn(TR0, TR0_00, 10, 2, "#")
            RH.InsertColumn(TR0, TR0_01, 10, 2, "Branch ID")
            RH.InsertColumn(TR0, TR0_02, 20, 0, "Branch Name")
            RH.InsertColumn(TR0, TR0_03, 15, 2, "BM Updated")
            RH.InsertColumn(TR0, TR0_04, 15, 2, "BM Submitted")
            RH.InsertColumn(TR0, TR0_05, 15, 2, "AM Updated")
            RH.InsertColumn(TR0, TR0_06, 15, 2, "AM Submitted")
            tb.Controls.Add(TR0)

            Dim Total As Integer = 0
            Dim Count As Integer = 0
            For Each DR In DT.Rows
                Count += 1
                Dim TR1 As New TableRow
                Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06 As New TableCell
                RH.InsertColumn(TR1, TR1_00, 10, 2, Count.ToString())
                RH.InsertColumn(TR1, TR1_01, 10, 2, DR(0))
                RH.InsertColumn(TR1, TR1_02, 20, 0, DR(1).ToString().ToUpper)
                If (DR(2) = "NO") Then
                    TR1_03.ForeColor = Drawing.Color.Red
                Else
                    TR1_03.ForeColor = Drawing.Color.Blue
                End If
                RH.InsertColumn(TR1, TR1_03, 15, 2, DR(2).ToString())
                If (DR(3) = "NO") Then
                    TR1_04.ForeColor = Drawing.Color.Red
                Else
                    TR1_04.ForeColor = Drawing.Color.Blue
                End If
                RH.InsertColumn(TR1, TR1_04, 15, 2, DR(3).ToString())
                If (DR(4) = "NO") Then
                    TR1_05.ForeColor = Drawing.Color.Red
                Else
                    TR1_05.ForeColor = Drawing.Color.Blue
                End If
                RH.InsertColumn(TR1, TR1_05, 15, 2, DR(4).ToString())
                If (DR(5) = "NO") Then
                    TR1_06.ForeColor = Drawing.Color.Red
                Else
                    TR1_06.ForeColor = Drawing.Color.Blue
                End If
                RH.InsertColumn(TR1, TR1_06, 15, 2, DR(5).ToString())
                tb.Controls.Add(TR1)

            Next
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
