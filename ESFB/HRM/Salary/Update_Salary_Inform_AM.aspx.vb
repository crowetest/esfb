﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Update_Salary_Inform_AM
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
#Region "Page Load & Disposed"
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 358) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Me.Master.subtitle = "Update Salary Input Details"

            DT = DB.ExecuteDataSet("SELECT -1 as area_id,'--------Select---------' as Area_Name union all  SELECT distinct area_id,area_name FROM BrMaster WHERE Area_Head=" & CInt(Session("UserID")) & "  ").Tables(0)
            If DT.Rows.Count <> 0 Then
                GN.ComboFill(cmbArea, DT, 0, 1)
            End If
            Me.cmbArea.Attributes.Add("onchange", "return areachange() ")
            Me.cmbBranch.Attributes.Add("onchange", "return branchchange() ")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 4 Then
            Dim Mn As Integer = Month(Now.Date())
            Dim ye As Integer = Year(Now.Date())
            Dim DR As DataRow
            DT = DB.ExecuteDataSet(" SELECT -1 as BRANCH_ID,'--------Select---------' as Branch_Name union all  SELECT S.BRANCH_ID,B.BRANCH_NAME FROM BRMASTER B, SALARY_INFORM S WHERE B.BRANCH_ID=S.BRANCH_ID  AND S.MONTH_ID=" & Mn & " AND S.YEAR_ID=" & ye & " AND  S.STATUS_ID IN (1,2) AND  B.area_id=" & CInt(Data(1)) & "").Tables(0)
            If DT.Rows.Count <> 0 Then
                For Each DR In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            Else
                CallBackReturn = ""
            End If
        End If
    End Sub
#End Region
End Class
