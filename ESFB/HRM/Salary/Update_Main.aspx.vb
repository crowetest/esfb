﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Last_Work_date_Update
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
#Region "Page Load & Disposed"
    Protected Sub Page_Break_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 361) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.hid_Id.Value = CStr(Request.QueryString.Get("ID"))
            If CInt(Me.hid_Id.Value) = 1 Then
                Me.Master.subtitle = "Update Last Working Date"
            Else
                Me.Master.subtitle = "Change Status After First Level - Report"
            End If
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim PostID As Integer = CInt(Session("Post_ID"))
        Dim DR As DataRow
        Dim StrValue As String = ""
        Select Case CInt(Data(0))
            Case 1 '
                Dim IntId As Integer = CInt(Data(1))
                Dim ItemList As String = Nothing
                '-------------------REgion------------------------------------------------------------------------------
                If IntId = 1 Then '
                    If PostID = 5 Then
                        DT = DB.ExecuteDataSet("select region_id,region_name from brmaster where branch_head=" & UserID & " ").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    ElseIf PostID = 6 Then
                        DT = DB.ExecuteDataSet("select distinct region_id,region_name from brmaster where area_head=" & UserID & " ").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    Else
                        DT = DB.ExecuteDataSet("select distinct region_id,region_name from SALARY_STATE_EMP_DTLS s, brmaster b where s.state_id=b.state_id and s.emp_code=" & UserID & "").Tables(0)
                        If DT.Rows.Count > 0 Then
                            For Each DR In DT.Rows
                                StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                            Next
                        End If
                    End If
                    '------------------------------------Area------------------------------------------------
                ElseIf IntId = 2 Then
                    ItemList = CStr(Data(2))
                    If PostID = 5 Then
                        DT = DB.ExecuteDataSet("select area_id,area_name from brmaster where branch_head=" & UserID & " and region_id in (" & ItemList & ") ").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    ElseIf PostID = 6 Then
                        DT = DB.ExecuteDataSet("select distinct area_id,area_name from brmaster where area_head=" & UserID & " and region_id in (" & ItemList & ") ").Tables(0)
                        If DT.Rows.Count <> 0 Then
                            If DT.Rows.Count > 1 Then
                                For Each DR In DT.Rows
                                    StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                                Next
                            Else
                                StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                            End If
                        End If
                    Else
                        DT = DB.ExecuteDataSet("select distinct area_id,area_name from brmaster where  region_id in (" & ItemList & ") ").Tables(0)
                        If DT.Rows.Count <> 0 Then
                            For Each DR In DT.Rows
                                StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                            Next
                        End If
                    End If
                    '-------------------------------------------Branch-----------------------------
                ElseIf IntId = 3 Then
                    ItemList = CStr(Data(2))
                    If PostID = 5 Then
                        DT = DB.ExecuteDataSet("select branch_id,branch_name from brmaster where branch_head=" & UserID & " and area_id in (" & ItemList & ")").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    Else
                        DT = DB.ExecuteDataSet("select branch_id,branch_name from brmaster where  area_id in (" & ItemList & ")").Tables(0)
                        If DT.Rows.Count > 0 Then
                            For Each DR In DT.Rows
                                StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                            Next
                        End If
                    End If
                End If
                CallBackReturn = "1ʘ" + CStr(IntId) + "ʘ" + StrValue
        End Select
    End Sub
#End Region
End Class
