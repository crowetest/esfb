﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Emp_Salary_Status_Details.aspx.vb" Inherits="Emp_Salary_Status_Details" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head>
<title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Style/datepicker.css"  rel="stylesheet"/>
    <link href="../../Style/jquery.datepick.css" rel="stylesheet"/>
    <script type="text/javascript" src="../../Script/jquery.js"></script>
    <script type="text/javascript" src="../../Script/jquery.datepick.js"></script>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
     <script type="text/javascript">
         $(document).ready(function () {
             $('.date').datepick();

         });
    </script>
    <script language="javascript" type="text/javascript">

        function window_onload() {
            if (document.getElementById("<%= hid_ID.ClientID %>").value == 2)
                table_fill_Dispersal();
            else
                table_fill_Withheld();
        }

         function FromServer(Arg, Context) {
            switch (Context) {
                case 1:
                    {
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        if (Data[0] == 0)
                            window.open("Emp_Salary_Status.aspx?", "_self");
                        break;
                    }
            }
        }
                    



        function table_fill_Withheld() {
            if (document.getElementById("<%= hdnDtls.ClientID %>").value != "") {
                document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';

                var row_bg = 0;
                var tab = "";
                tab += "<div style='width:100%;height:auto; background-color:#A34747; text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
                tab += "<tr class=mainhead>"
                tab += "<td style='width:2%;text-align:center;' class=NormalText>#</td>";
                tab += "<td style='width:9%;text-align:center' class=NormalText>Region Name</td>";
                tab += "<td style='width:9%;text-align:center' class=NormalText>Area Name</td>";
                tab += "<td style='width:0%;text-align:center;display:none;' class=NormalText>Branch Code</td>";
                tab += "<td style='width:9%;text-align:center' class=NormalText>Branch Name</td>";
                tab += "<td style='width:5%;text-align:center' class=NormalText>Emp Code</td>";
                tab += "<td style='width:10%;text-align:center' class=NormalText>Employee Name</td>";
                tab += "<td style='width:25%;text-align:center' class=NormalText>Reason For Withheld</td>";
                tab += "<td style='width:4%;text-align:center' class=NormalText>Disburse</td>";
                tab += "<td style='width:24%;text-align:center' class=NormalText>Remark</td>";
                tab += "<td style='width:3%;text-align:center' class=NormalText></td>";
                tab += "</table></div>";
                tab += "<div style='width:100%;height:auto; background-color:#A34747; text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
                row = document.getElementById("<%= hdnDtls.ClientID %>").value.split("¥");
                var i = 0;
                var Region = '';
                var Area = '';
                var Branch = '';
                for (n = 0; n <= row.length - 2; n++) {
                    var col = row[n].split("~");
                    tab += "<tr  class='sub_first';>";
                    i=i+1;
                    tab += "<td style='width:2%;text-align:center' >" + i + "</td>";
                    if (Region == col[3])
                        tab += "<td style='width:9%;text-align:left' ></td>";
                    else {
                        Region = col[3];
                        tab += "<td style='width:9%;text-align:left' >" + col[3] + "</td>";
                    }
                    if (Area == col[4])
                        tab += "<td style='width:9%;text-align:left' ></td>";
                    else {
                        Area = col[4];
                        tab += "<td style='width:9%;text-align:left' >" + col[4] + "</td>";
                    }

                    var txtBRCode = "<input type=text id='txtBRCode" + i + "' name='txtBRCode" + i + "'   style='width:99%;text-align:center;background-color:#FFFFF0;border:none;display:none; ' class='NormalText' ReadOnly='true' value=" + col[5] + " />";
                    tab += "<td style='width:0%;text-align:center;border:none;display:none;' >" + txtBRCode + "</td>"; //brcode
                    if (Branch == col[5])
                        tab += "<td style='width:9%;text-align:left' ></td>";
                    else {
                        Branch = col[5];
                        tab += "<td style='width:9%;text-align:left' >" + col[6] + "</td>";
                    }
                    
                    var txtEmpCode= "<input type=text id='txtEmpCode" + i + "' name='txtEmpCode" + i + "'   style='width:99%;text-align:center;background-color:#FFFFF0;border:none; ' class='NormalText' ReadOnly='true' value=" + col[0] + " />";
                    tab += "<td style='width:5%;text-align:left;border:none;' >" + txtEmpCode + "</td>"; //empcode
                    tab += "<td style='width:10%;text-align:left' >" + col[1] + "</td>";
                    var txtReason = "<textarea id='txtReason" + i + "' name='txtReason" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;' disabled='true'  maxlength='3000' onkeypress='return TextAreaCheck(event)' >" + col[2] + "</textarea>";
                    tab += "<td style='width:25%;text-align:left' >" + txtReason + "</td>";
                    tab += "<td style='width:4%;text-align:center'><input id='chkDisperal" + i + "' type='checkbox' onclick=ChangeStatus(" + i + ") /></td>";
                    var txtRemark = "<textarea id='txtRemark" + i + "' name='txtRemark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;' disabled='true'  maxlength='3000' onkeypress='return TextAreaCheck(event)' ></textarea>";
                    tab += "<td style='width:24%;text-align:left' >" + txtRemark + "</td>";
                    tab += "<td style='width:3%;text-align:center' ><img  src='../../image/nex.jpg'  style='height:25px; width:25px; cursor:pointer;' onclick='View_onclick(" + col[5] + "," + col[0] + ")' ToolTip='View salary input details'   /></td>";
                    tab += "</tr>";      
                }                           
                tab += "</table></div>";
                document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
            }
        }

        function ChangeStatus(i) {
            if (document.getElementById("<%= hid_ID.ClientID %>").value == 1) {
                if (document.getElementById("chkDisperal" + i).checked == true)
                    document.getElementById("txtRemark" + i).disabled = false;
                else {
                    document.getElementById("txtRemark" + i).value = '';
                    document.getElementById("txtRemark" + i).disabled = true;
                }
            }
            else {
                if (document.getElementById("chkWithheld" + i).checked == true) {
                    document.getElementById("cmbType" + i).disabled = false;
                    document.getElementById("txtDate" + i).disabled = false;
                    document.getElementById("txtRemark" + i).disabled = false;

                }
                else {
                    document.getElementById("cmbType" + i).disabled = true;
                    document.getElementById("txtDate" + i).disabled = true;
                    document.getElementById("txtRemark" + i).disabled = true;
                    document.getElementById("cmbType" + i).value = '-1';
                    document.getElementById("txtDate" + i).value = '';
                    document.getElementById("txtRemark" + i).value = '';
                }

            }
        }


        function table_fill_Dispersal() {
            
            if (document.getElementById("<%= hdnDtls.ClientID %>").value != "") {
                document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';

                var row_bg = 0;
                var tab = "";
                tab += "<div style='width:100%;height:auto; background-color:#A34747; text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
                tab += "<tr class=mainhead>"
                tab += "<td style='width:2%;text-align:center;' class=NormalText>#</td>";
                tab += "<td style='width:8%;text-align:center' class=NormalText>Region Name</td>";
                tab += "<td style='width:8%;text-align:center' class=NormalText>Area Name</td>";
                tab += "<td style='width:0%;text-align:center;display:none;' class=NormalText>Branch Code</td>";
                tab += "<td style='width:9%;text-align:center' class=NormalText>Branch Name</td>";
                tab += "<td style='width:5%;text-align:center' class=NormalText>Emp Code</td>";
                tab += "<td style='width:10%;text-align:center' class=NormalText>Employee Name</td>";
                tab += "<td style='width:5%;text-align:center' class=NormalText>Withheld</td>";
                tab += "<td style='width:8%;text-align:center' class=NormalText>Current Status</td>";
                tab += "<td style='width:6%;text-align:center' class=NormalText>Date</td>";
                tab += "<td style='width:37%;text-align:center' class=NormalText>Remark</td>";
                tab += "<td style='width:3%;text-align:center' class=NormalText></td>";
                tab += "</table></div>";
                tab += "<div style='width:100%;height:auto; background-color:#A34747; text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
                row = document.getElementById("<%= hdnDtls.ClientID %>").value.split("¥");
                var i = 0;
                var Region = '';
                var Area = '';
                var Branch = '';
                for (n = 0; n <= row.length - 2; n++) {
                    var col = row[n].split("~");
                    tab += "<tr  class='sub_first';>";
                    i = i + 1;
                    tab += "<td style='width:2%;text-align:center' >" + i + "</td>";
                    if (Region == col[3])
                        tab += "<td style='width:8%;text-align:left' ></td>";
                    else {
                        Region = col[3];
                        tab += "<td style='width:8%;text-align:left' >" + col[3] + "</td>";
                    }
                    if (Area == col[4])
                        tab += "<td style='width:8%;text-align:left' ></td>";
                    else {
                        Area = col[4];
                        tab += "<td style='width:8%;text-align:left' >" + col[4] + "</td>";
                    }
                    var txtBRCode = "<input type=text id='txtBRCode" + i + "' name='txtBRCode" + i + "'   style='width:99%;text-align:center;background-color:#FFFFF0;border:none;display:none; ' class='NormalText' ReadOnly='true' value=" + col[5] + " />";
                    tab += "<td style='width:0%;text-align:center;border:none;display:none;' >" + txtBRCode + "</td>"; //brcode
                    if (Branch == col[5])
                        tab += "<td style='width:9%;text-align:left' ></td>";
                    else {
                        Branch = col[5];
                        tab += "<td style='width:9%;text-align:left' >" + col[6] + "</td>";
                    }
                    var txtEmpCode = "<input type=text id='txtEmpCode" + i + "' name='txtEmpCode" + i + "'   style='width:99%;text-align:center;background-color:#FFFFF0;border:none; ' class='NormalText' ReadOnly='true' value=" + col[0] + " />";
                    tab += "<td style='width:5%;text-align:left;border:none;' >" + txtEmpCode + "</td>"; //empcode
                    tab += "<td style='width:10%;text-align:left' >" + col[1] + "</td>";
                    tab += "<td style='width:5%;text-align:center'><input id='chkWithheld" + i + "' type='checkbox' onclick=ChangeStatus(" + i + ") /></td>";
                    var select = "<select id='cmbType" + i + "' name='cmbType" + i + "'  style='width:99%;text-align:left;background-color:#FFFFF0;border:none;' disabled='true'  >";
                        var row1 = document.getElementById("<%= hid_Type.ClientID %>").value.split("¥");
                        for (m = 0; m <= row1.length - 2; m++) {
                            var col1 = row1[m].split("~");
                            select += "<option value=" + col1[0] + " >" + col1[1] + "</option>";
                        } 
                    tab += "<td style='width:8%;text-align:left'>" + select + "</td>";
                    var txtDate = "<input type=text id='txtDate" + i + "' name='txtDate" + i + "'   style='width:99%;text-align:center; background-color:#FFFFF0;border:none;'  ReadOnly='true' disabled='true'  class='date'  />";
                    tab += "<td style='width:6%;text-align:left' >" + txtDate + "</td>";
                    var txtRemark = "<textarea id='txtRemark" + i + "' name='txtRemark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;' disabled='true'  maxlength='3000' onkeypress='return TextAreaCheck(event)' ></textarea>";
                    tab += "<td style='width:37%;text-align:left' >" + txtRemark + "</td>";
                    tab += "<td style='width:3%;text-align:center' ><img  src='../../image/nex.jpg'  style='height:25px; width:25px; cursor:pointer;' onclick='View_onclick("+ col[5] +","+ col[0] +")' ToolTip='View salary input details'   /></td>";
                    tab += "</tr>";
                }
                tab += "</table></div>";
                document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
            }
        }


        function View_onclick(branchid,empcode) {
            var MonthID = document.getElementById("<%= hid_Month.ClientID %>").value;
            var YearID = document.getElementById("<%= hid_Year.ClientID %>").value;
            window.open("Reports/ViewSalaryStatus.aspx?Id="+ btoa("3") +"&BranchList=" + btoa(branchid) + "&MonthID=" + btoa(MonthID) + "&YearID=" + btoa(YearID) + "&TypeId="+ btoa("1") +"&IdName=" + btoa("d") + "&EmpCode="+ btoa(empcode) +"", "_blank");
        }


        function SaveOnClick() {
            
            document.getElementById("<%= hdnDtls.ClientID %>").value = '';

            var RowLength = document.getElementById("<%= hid_Count.ClientID %>").value - 1;
            var Empcode = '';
            var Remark = '';
            var TypeId = '';
            var DateVal = '';
            var BranchId = '';
            var i = 0;
            if (document.getElementById("<%= hid_ID.ClientID %>").value == 2) {//dispersal
                for (i = 1; i <= RowLength + 1; i++) {
                    if (document.getElementById("chkWithheld" + i).checked == true) {
                        if (document.getElementById("cmbType" + i).value == '-1') {
                            alert("Enter Current Status ");
                            document.getElementById("cmbType" + i).focus();
                            return false;
                        }
                        if (document.getElementById("txtDate" + i).value == '') {
                            alert("Enter Date ");
                            document.getElementById("txtDate" + i).focus();
                            return false;
                        }
                        if (document.getElementById("txtRemark" + i).value == '') {
                            alert("Enter Remark ");
                            document.getElementById("txtRemark" + i).focus();
                            return false;
                        }
                        Empcode = document.getElementById("txtEmpCode" + i).value;
                        TypeId = document.getElementById("cmbType" + i).value;
                        TDate = document.getElementById("txtDate" + i).value;
                        if (document.getElementById("txtDate" + i).value != '') {
                            var DT = document.getElementById("txtDate" + i).value.split("/");
                            var userday = DT[0];
                            var usermonth = DT[1];
                            var useryear = DT[2];
                            var lenmonth = usermonth.length;
                            if (usermonth.substr(0, 1) == 0)
                                var res = usermonth.substr(1, lenmonth);
                            else
                                var res = usermonth;
                            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                            TDate = userday + "/" + months[res - 1] + "/" + useryear;
                        }
                        else
                            TDate = '';
                        Remark = document.getElementById("txtRemark" + i).value;
                        BranchId = document.getElementById("txtBRCode" + i).value;
                        document.getElementById("<%= hdnDtls.ClientID %>").value += Empcode + "ÿ" + Remark + "ÿ" + BranchId + "ÿ" + TypeId + "ÿ" + TDate + "Ñ";
                    }
                }
                if (document.getElementById("<%= hdnDtls.ClientID %>").value == '') {
                    alert("No status change has been effected ");
                    document.getElementById("chkWithheld1").focus();
                    return false;
                }
            }
            else {//withheld
                for (i = 1; i <= RowLength + 1; i++) {
                    if (document.getElementById("chkDisperal" + i).checked == true) {
                        if (document.getElementById("txtRemark" + i).value == '') {
                            alert("Enter Remark ");
                            document.getElementById("txtRemark" + i).focus();
                            return false;
                        }
                        Empcode = document.getElementById("txtEmpCode" + i).value;
                        Remark = document.getElementById("txtRemark" + i).value;
                        BranchId = document.getElementById("txtBRCode" + i).value;
                        document.getElementById("<%= hdnDtls.ClientID %>").value += Empcode + "ÿ" + Remark + "ÿ" + BranchId + "Ñ";
                    }
                }
                if (document.getElementById("<%= hdnDtls.ClientID %>").value == '') {
                    alert("No status change has been effected ");
                    document.getElementById("chkDisperal1").focus();
                    return false;
                }
            }
            
            var Id = document.getElementById("<%= hid_ID.ClientID %>").value;
            var EmpDetails = document.getElementById("<%= hdnDtls.ClientID %>").value;
            var MonthID = document.getElementById("<%= hid_Month.ClientID %>").value;
            var Year_ID = document.getElementById("<%= hid_Year.ClientID %>").value;
            
            ToServer("1ʘ" + Id + "ʘ" + MonthID + "ʘ" + Year_ID + "ʘ" + EmpDetails, 1);
            
        }



        function btnExit_onclick() {
            window.open("Emp_Salary_Status.aspx?", "_self");
        }


</script>
</head>
 <table style="width: 100%;margin: 0px auto">
    <tr>
        <td  style="width:100%; height: 15px;text-align: center;">
            <asp:TextBox ID="txtStat" runat="server" style=" font-family:Cambria;text-align: center;font-size:12pt;" Width="100%"   disabled="true"  />
        </td>
    </tr>
    <tr>  
        
        <td style="text-align:center;" ><asp:Panel ID="pnlDtls"  runat="server"></asp:Panel> </td>
    </tr>
    <tr>
        <td  style="width:100%; height: 23px;"></td>
    </tr>
    <tr>
        <td style="text-align: center; font-family:Cambria;" >
           <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="SAVE"  onclick="return SaveOnClick()" />        &nbsp; &nbsp; &nbsp;         
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
            <asp:HiddenField ID="hdnDtls" runat="server" />
            <asp:HiddenField ID="hid_Count" runat="server" />
            <asp:HiddenField ID="hid_ID" runat="server" />
            <asp:HiddenField ID="hid_Month" runat="server" />
            <asp:HiddenField ID="hid_Year" runat="server" />
            <asp:HiddenField ID="hid_Type" runat="server" />

        </td>
    </tr>
 </table>
</asp:Content>

