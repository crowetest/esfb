﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Update_Salary_Inform.aspx.vb" Inherits="Update_Salary_Inform" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head>
<title></title>
<link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
<link href="../../Style/datepicker.css"  rel="stylesheet"/>
<link href="../../Style/jquery.datepick.css" rel="stylesheet"/>

<script type="text/javascript" src="../../Script/jquery.js"></script>
<script type="text/javascript" src="../../Script/jquery.datepick.js"></script>
<script src="../../Script/Validations.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date').datepick();

    });
</script>


<script language="javascript" type="text/javascript">     


     function FromServer(arg, context) {
        if (context == 1) {
            var Data = arg.split("~");
            if (Data[0]==0){
                alert(Data[1]);
                if (document.getElementById("<%= hid_PostID.ClientID %>").value==6)
                    window.open("Update_Salary_Inform_AM.aspx?", "_self");

                return false;
            }
        } 
        else if (context == 2) {
            var Data = arg.split("~");
            if (Data[0]==0){
                alert(Data[1]);
                if (document.getElementById("<%= hid_PostID.ClientID %>").value==5)        
                    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
                else
                    window.open("Update_Salary_Inform_AM.aspx?", "_self");
                return false;
            }
        }       
    }


    function table_fill() {
        document.getElementById("<%= pnlDisplay.ClientID %>").style.display = ''; 
        var monthNames = ["January", "February", "March", "April", "May", "June",  "July", "August", "September", "October", "November", "December"];
        var mo=monthNames[new Date().getMonth()];
        document.getElementById("<%= hid_Month.ClientID %>").value=new Date().getMonth()+1;
        document.getElementById("<%= hid_Year.ClientID %>").value=new Date().getFullYear();
        var titl='Salary Input Sheet for the Month ' + mo + ' ' + document.getElementById("<%= hid_Year.ClientID %>").value ;
        var row_bg = 0;
        var tab = "";
            
        //tab += "<div style='width:100%;background-color:#A34747; height:auto; overflow:auto; margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
        tab += "<div style='width:100%;background-color:#A34747; height:auto; overflow:auto; margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
        tab += "<tr height=100px;  class='mainhead'>"; 
        tab += "<td style='width:100%;text-align:center' ><b>" + titl + " </b></td>";
        tab += "</tr></table>";
       // tab += "<div style='width:100%;background-color:#A34747; height:auto; overflow:auto; margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
        tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
        tab += "<tr height=100px;  class='sub_first'>"; 
        tab += "<td style='width:2%;text-align:center; ' ></td>";
        tab += "<td style='width:12%;text-align:center;  ' >Employee Details</td>";
        tab += "<td style='width:36%;text-align:center; ' >STATUS</td>";
        tab += "<td style='width:16%;text-align:center; ' >LEAVE</td>";
        tab += "<td style='width:22%;text-align:center; ' >ARREARS(if any)</td>";
        tab += "<td style='width:12%;text-align:center; ' >BANK DETAILS</td>";
        tab += "</tr></table>";
            
       // tab += "<div style='width:100%;background-color:#A34747; height:auto; overflow:auto; margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
        tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
        tab += "<tr height=100px;  class='mainhead'>";
        tab += "<td style='width:2%;text-align:center' >Slno</td>";
        tab += "<td style='width:3%;text-align:center' >Emp Code</td>";
        tab += "<td style='width:9%;text-align:center' >Emp Name</td>";
        tab += "<td style='width:6%;text-align:center' >Status As Per System</td>";
        tab += "<td style='width:9%;text-align:center' >Any Changes in Status ?</td>";            
        tab += "<td style='width:10%;text-align:center' >Date</td>";
        tab += "<td style='width:11%;text-align:center' >Remark</td>";
        tab += "<td style='width:2%;text-align:center' >LOP</td>";
        tab += "<td style='width:2%;text-align:center' >CL</td>";
        tab += "<td style='width:2%;text-align:center' >ML/ PL</td>";
        tab += "<td style='width:2%;text-align:center' >PL</td>";
        tab += "<td style='width:2%;text-align:center' >SL</td>";
        tab += "<td style='width:2%;text-align:center' >ESI</td>";
        tab += "<td style='width:2%;text-align:center' >ESI- ML</td>";
        tab += "<td style='width:2%;text-align:center' >UA</td>";
        tab += "<td style='width:5%;text-align:center' >Days</td>";
        tab += "<td style='width:17%;text-align:center' >Remark</td>";
        tab += "<td style='width:6%;text-align:center' >IFSC CODE</td>";
        tab += "<td style='width:6%;text-align:center' >Account No.</td>";            
        tab += "</tr>";
        var row = document.getElementById("<%= hid_Value.ClientID %>").value.split("¥"); 
        var i=0;
        for (n = 0; n <= row.length - 2; n++) {
            var col = row[n].split("~");
            if (row_bg == 0) {
                row_bg = 1;
                tab += "<tr  class='sub_first';>";
            }
            else {
                row_bg = 0;
                tab += "<tr class='sub_first';>";
            }
            i=i+1;

//              0=emp_code , 1=emp_name ,2=EMP_STATUS , 3=EMP_TYPE ,4=EMP_TYPE_DATE, 5=EMP_TYPE_REMARK ,6=LOP , 7=CL  , 	8=ML , 9=PL , 10=SL ,11= ESI  ,12= ESIML  , 13=OTHERS  ,
//			    14=ARREAR  ,15= ARREAR_REMARK  , 16= BANK_FLAG ,  17=IFSC_CODE , 18=acno               
               
            tab += "<td style='width:2%;text-align:center' >" + i + "</td>";
            var txtEmpCode= "<input type=text id='txtEmpCode" + i + "' name='txtEmpCode" + i + "'   style='width:99%;text-align:center;background-color:#FFFFF0;border:none; ' class='NormalText' ReadOnly='true' value=" + col[0] + " />";
            tab += "<td style='width:3%;text-align:center;border:none;' >"+ txtEmpCode +"</td>"; //empcode
            tab += "<td style='width:9%;text-align:left' class='NormalText' >" + col[1] + "</td>";//empname
            tab += "<td style='width:6%;text-align:center' >" + col[2] + "</td>"; //status                              
               
            //current status
            var select = "<select id='cmbType" + i + "' name='cmbType" + i + "'  style='width:99%;text-align:left;background-color:#FFFFF0;border:none;font-size:12px;font-family: Cambria;' onchange='TypeOnChange("+ i +")' >";
                var row1 = document.getElementById("<%= hid_Type.ClientID %>").value.split("¥");
                for (m = 0; m <= row1.length - 2; m++) {
                    var col1 = row1[m].split("~");
                    if (col[3]==col1[0])
                        select += "<option value=" + col1[0] + " selected=true>" + col1[1] + "</option>";
                    else
                        select += "<option value=" + col1[0] + " >" + col1[1] + "</option>";
                } 
            tab += "<td style='width:9%;text-align:left'>" + select + "</td>";               
            //status date , status remark
            if (col[4]!='1/1/1900')
                var txtDate= "<input type=text id='txtDate" + i + "' name='txtDate" + i + "'   style='width:99%;text-align:center;background-color:#FFFFF0; border:none;'   value="+ col[4] +"  ReadOnly='true'  class='date'  />";
            else
                var txtDate= "<input type=text id='txtDate" + i + "' name='txtDate" + i + "'   style='width:99%;text-align:center; background-color:#FFFFF0;border:none;'  ReadOnly='true' disabled=true  class='date'  />";                     
            tab += "<td style='width:10%;text-align:center' >"+ txtDate +"</td>";
            if(col[5]=='null')//remark
                var txtStatus_Remark="<textarea id='txtStatus_Remark" + i + "' name='txtStatus_Remark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;' disabled=true  maxlength='3000' onkeypress='return TextAreaCheck(event)' ></textarea>";
            else
                var txtStatus_Remark="<textarea id='txtStatus_Remark" + i + "' name='txtStatus_Remark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;'   maxlength='3000' onkeypress='return TextAreaCheck(event)'  >"+ col[5] +"</textarea>";
            tab += "<td style='width:11%;text-align:center' >"+ txtStatus_Remark +"</td>";                
                
              
            //Leave
            //LOP
            if (col[6]==0) 
                var txtLOP= "<input type=text id='txtLOP" + i + "' name='txtLOP" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none; '   class='NormalText' MaxLength='2'   disabled='true' onkeypress='return NumericCheck(event)'  />";
                else
                var txtLOP= "<input type=text id='txtLOP" + i + "' name='txtLOP" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none; ' value="+ col[6] +"    class='NormalText' MaxLength='2' disabled='true' onkeypress='return NumericCheck(event)'  />";
            tab += "<td style='width:2%;text-align:right' >"+ txtLOP +"</td>";
            // CL               
            if (col[7]==0)  
                var txtCL= "<input type=text id='txtCL" + i + "' name='txtCL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'  MaxLength='2' disabled='true'  onkeypress='return NumericCheck(event)' />";
                else
                var txtCL= "<input type=text id='txtCL" + i + "' name='txtCL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' value="+ col[7] +"  disabled='true'  MaxLength='2' onkeypress='return NumericCheck(event)' />";
            tab += "<td style='width:2%;text-align:center' >"+ txtCL +"</td>";
            //ML_PL
            if (col[8]==0)
                var txtML= "<input type=text id='txtML" + i + "' name='txtML" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'   disabled='true'   MaxLength='2' onkeypress='return NumericCheck(event)' />";
            else
                var txtML= "<input type=text id='txtML" + i + "' name='txtML" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'  value="+ col[8] +" disabled='true'   MaxLength='2' onkeypress='return NumericCheck(event)' />";
            tab += "<td style='width:2%;text-align:center' >"+ txtML +"</td>";
            //PL
            if (col[9]==0) 
                var txtPL= "<input type=text id='txtPL" + i + "' name='txtPL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'   disabled='true'  MaxLength='2' onkeypress='return NumericCheck(event)'  />";
            else
                var txtPL= "<input type=text id='txtPL" + i + "' name='txtPL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' disabled='true'  value="+ col[9] +" MaxLength='2' onkeypress='return NumericCheck(event)'  />";
            tab += "<td style='width:2%;text-align:center' >"+ txtPL +"</td>";
            //SL
            if (col[10]==0)
                var txtSL= "<input type=text id='txtSL" + i + "' name='txtSL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'   disabled='true'   MaxLength='2' onkeypress='return NumericCheck(event)'  />";
            else
                var txtSL= "<input type=text id='txtSL" + i + "' name='txtSL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' disabled='true'  value="+ col[10] +" MaxLength='2' onkeypress='return NumericCheck(event)'  />";
            tab += "<td style='width:2%;text-align:center' >"+ txtSL +"</td>";
            //ESI
            if (col[11]==0)
                var txtESI= "<input type=text id='txtESI" + i + "' name='txtESI" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'  MaxLength='2' disabled='true' onkeypress='return NumericCheck(event)'  />";                    
            else
                var txtESI= "<input type=text id='txtESI" + i + "' name='txtESI" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' disabled='true'  value="+ col[11] +" MaxLength='2' onkeypress='return NumericCheck(event)'  />";
            tab += "<td style='width:2%;text-align:center' >"+ txtESI +"</td>";
            //ESI Materinity
            if (col[12]==0)
                var txtESIML= "<input type=text id='txtESIML" + i + "' name='txtESIML" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'  MaxLength='2' disabled='true' onkeypress='return NumericCheck(event)'  />";
            else
                var txtESIML= "<input type=text id='txtESIML" + i + "' name='txtESIML" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' disabled='true'  value="+ col[12] +"  MaxLength='2' onkeypress='return NumericCheck(event)'  />";
            tab += "<td style='width:2%;text-align:center' >"+ txtESIML +"</td>";
            //OTHERS
            if (col[13]==0)
                var txtOthers= "<input type=text id='txtOthers" + i + "' name='txtOthers" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' disabled='true' class='NormalText'  MaxLength='2' onkeypress='return NumericCheck(event)'  />";
            else
                var txtOthers= "<input type=text id='txtOthers" + i + "' name='txtOthers" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' disabled='true' class='NormalText' value="+ col[13] +" MaxLength='2' onkeypress='return NumericCheck(event)'  />";
            tab += "<td style='width:2%;text-align:center' >"+ txtOthers +"</td>";
              
//                arrear
            if (col[14]==0)
                var txtArrear= "<input type=text id='txtArrear" + i + "' name='txtArrear" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' onblur=Arrear_RemarkUpdation("+ i +",0) class='NormalText'  MaxLength='8' onkeypress='return NumericCheck(event)''   />";
            else
                var txtArrear= "<input type=text id='txtArrear" + i + "' name='txtArrear" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' onblur=Arrear_RemarkUpdation("+ i +",0) class='NormalText' value="+ col[14] +" MaxLength='8' onkeypress='return NumericCheck(event)'   />";
            tab += "<td style='width:5%;text-align:center' >"+ txtArrear +"</td>";
            if(col[15]=='null')
                var txtA_Remark="<textarea id='txtA_Remark" + i + "' name='txtA_Remark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;' onblur=Arrear_RemarkUpdation("+ i +",1) maxlength='3000' onkeypress='return TextAreaCheck(event)' ></textarea>";
            else
                var txtA_Remark="<textarea id='txtA_Remark" + i + "' name='txtA_Remark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;'  onblur=Arrear_RemarkUpdation("+ i +",1) maxlength='3000' onkeypress='return TextAreaCheck(event)' >"+ col[15] +"</textarea>";
            tab += "<td style='width:17%;text-align:center' >"+ txtA_Remark +"</td>";              

//                bank details
            if (col[16]==1){
                var txtIFSC= "<input type=text id='txtIFSC" + i + "' name='txtIFSC" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[17] +"  MaxLength='11'  disabled='true'  class='NormalText' />";
                tab += "<td style='width:6%;text-align:center' >"+ txtIFSC +"</td>";
                    
                var txtAcNo= "<input type=text id='txtAcNo" + i + "' name='txtAcNo" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[18] +" MaxLength='50' disabled='true'  class='NormalText' />";
                tab += "<td style='width:6%;text-align:center' >"+ txtAcNo +"</td>";
            }
            else{
                if(col[17]=='null')
                    var txtIFSC= "<input type=text id='txtIFSC" + i + "' name='txtIFSC" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'  MaxLength='11' class='NormalText' />";
                else
                    var txtIFSC= "<input type=text id='txtIFSC" + i + "' name='txtIFSC" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'  value="+ col[17] +"  MaxLength='11' class='NormalText' />";
                tab += "<td style='width:6%;text-align:center' >"+ txtIFSC +"</td>";
                if(col[18]=='null')
                    var txtAcNo= "<input type=text id='txtAcNo" + i + "' name='txtAcNo" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'  MaxLength='50' class='NormalText' />";
                else
                    var txtAcNo= "<input type=text id='txtAcNo" + i + "' name='txtAcNo" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'  value="+ col[18] +"  MaxLength='50' class='NormalText' />";
                tab += "<td style='width:6%;text-align:center' >"+ txtAcNo +"</td>";
            }

            tab += "</tr>";
        }
          tab+="</table></div>"; 
        document.getElementById("<%= pnlDisplay.ClientID %>").innerHTML = tab;
    }

    function TypeOnChange(ID)
    {
        if(document.getElementById("cmbType"+ID).value==-1)
        {
            document.getElementById("txtStatus_Remark"+ID).value="";
            document.getElementById("txtDate"+ID).value="";
            document.getElementById("txtStatus_Remark"+ID).disabled=true;
            document.getElementById("txtDate"+ID).disabled=true;
        }
        else
        {
            document.getElementById("txtStatus_Remark"+ID).disabled=false;
            document.getElementById("txtDate"+ID).disabled=false;
        }
    }

    function Arrear_RemarkUpdation(i,n){
        if(n==0){
            if (document.getElementById("txtA_Remark" + i).value!='' && document.getElementById("txtArrear" + i).value=='') {
                alert("Enter Arrear Days ");
                document.getElementById("txtArrear" + i).focus();
            }
        }
        else{
            if (document.getElementById("txtA_Remark" + i).value=='' && document.getElementById("txtArrear" + i).value!='') {
                alert("Enter Arrear Remark");
                document.getElementById("txtA_Remark" + i).focus();
            }
        }
    }

 
    
    function Validate_date(n){
        if (document.getElementById("txtDate" + n).value!='') {
            var DT=document.getElementById("txtDate" + n).value.split("/");
            if (Math.abs(document.getElementById("<%= hid_Month.ClientID %>").value)!=Math.abs(DT[1])){
                alert("Date should be in current month .");
                document.getElementById("txtDate" + n).value='';
                document.getElementById("txtDate" + n).focus();
            }
        }
    }


    function SubmitOnClick(){
        var Conf = confirm("Unless and until all the updations of salary input details in this month are completed do not submit .Do you want to continue ?");
        if (Conf == true) {
            var BranchId=document.getElementById("<%= txtBranchId.ClientID %>").value;
            var MonthId=document.getElementById("<%= hid_Month.ClientID %>").value;
            var YearID=document.getElementById("<%= hid_Year.ClientID %>").value;
            var FromDt=document.getElementById("<%= hid_FromDt.ClientID %>").value;
            var ToDt=document.getElementById("<%= hid_ToDt.ClientID %>").value;
            ToServer("2Ø" + BranchId + "Ø" + MonthId + "Ø" +  YearID + "Ø" + FromDt + "Ø" +  ToDt , 2);
        }
    }


    function btnSave_onclick(){
        var Conf = confirm("Are you sure to save the salary information details ?");
        if (Conf == true) {
            SaveOnClick(1);
            
        }
    }



    function SaveOnClick(saveflag){        
        var BranchId=document.getElementById("<%= txtBranchId.ClientID %>").value;
        document.getElementById("<%= hid_Data.ClientID %>").value='';
        var Empcode='';
        var EmpType=null;
        var TDate='';
        var Status_Remark=null;
        var Arrear=0;
        var Arrear_Remark=null;
        var IFSC=null;
        var Account_No=null;
            
        var RowLength=document.getElementById("<%= hid_Item.ClientID %>").value-1;
        var n=0; 
        for (n = 1; n <= RowLength+1; n++) {
            //validations
            //status
            //status 
            if (document.getElementById("cmbType" + n).value!='-1'  ){
                if(document.getElementById("txtDate" + n).value==''){
                    alert("Select Date ");
                    document.getElementById("txtDate" + n).focus();
                    return false;
                }
                if(document.getElementById("txtStatus_Remark" + n).value==''){
                    alert("Enter Remark of Status");
                    document.getElementById("txtStatus_Remark" + n).focus();
                    return false;
                }
            }               
//            //date
//            if (document.getElementById("txtDate" + n).value!='' ){
//                if(document.getElementById("cmbType" + n).value=='-1'){
//                    alert("Select the Current Status ");
//                    document.getElementById("cmbType" + n).focus();
//                    return false;
//                }
//                if(document.getElementById("txtStatus_Remark" + n).value==''){
//                    alert("Enter Remark of Status");
//                    document.getElementById("txtStatus_Remark" + n).focus();
//                    return false;
//                }
//            }
//            //remark
//            if (document.getElementById("txtStatus_Remark" + n).value!=''){
//                if(document.getElementById("cmbType" + n).value=='-1'){
//                    alert("Select the Current Status ");
//                    document.getElementById("cmbType" + n).focus();
//                    return false;
//                }
//                if(document.getElementById("txtDate" + n).value==''){
//                    alert("Select Date");
//                    document.getElementById("txtDate" + n).focus();
//                    return false;
//                }
//            }

            //Arrear
            if (document.getElementById("txtA_Remark" + n).value=='' && document.getElementById("txtArrear" + n).value!='') {
                alert("Enter Arrear Remark");
                document.getElementById("txtA_Remark" + n).focus();
                return false;
            }
//            if (document.getElementById("txtA_Remark" + n).value!='' && document.getElementById("txtArrear" + n).value=='') {
//                alert("Enter Arrear Days");
//                document.getElementById("txtArrear" + n).focus();
//                return false;
//            }
//                //Bank DEtails
//                if (document.getElementById("txtIFSC" + n).disabled==false){
//                    if (document.getElementById("txtIFSC" + n).value=='' && document.getElementById("txtAcno" + n).value!='') {
//                        alert("Enter IFSC Code");
//                        document.getElementById("txtIFSC" + n).focus();
//                        return false;
//                    }
//                    if (document.getElementById("txtIFSC" + n).value!='' && document.getElementById("txtAcno" + n).value=='') {
//                        alert("Enter Account Number");
//                        document.getElementById("txtAcno" + n).focus();
//                        return false;
//                    }
//                }


            Empcode=document.getElementById("txtEmpCode" + n ).value;
            EmpType=document.getElementById("cmbType" + n ).value;
            TDate=document.getElementById("txtDate" + n).value;
            if (document.getElementById("txtDate" + n).value!='') {
                var DT=document.getElementById("txtDate" + n).value.split("/");
                var userday = DT[0];
                var usermonth = DT[1];
                var useryear = DT[2];
                var lenmonth=usermonth.length;
                if (usermonth.substr(0, 1)==0)
                    var res = usermonth.substr(1, lenmonth);
                else
                    var res =usermonth;
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                TDate = userday + "/" + months[res - 1] + "/" + useryear; 
            }
            else
                TDate='';
//            if (document.getElementById("txtStatus_Remark" + n).value!='')
                Status_Remark=document.getElementById("txtStatus_Remark" + n ).value;
//            else
//                Status_Remark=null;
            
            if (document.getElementById("txtArrear" + n).value!='')
                Arrear=document.getElementById("txtArrear" + n ).value;
            else
                Arrear=0;
            if (document.getElementById("txtA_Remark" + n).value!='')
                Arrear_Remark=document.getElementById("txtA_Remark" + n ).value;
            else
                Arrear_Remark=null;
               
            if (document.getElementById("txtIFSC" + n).value!='')
                IFSC=document.getElementById("txtIFSC"  + n ).value;
            else
                IFSC=null;
            if (document.getElementById("txtAcNo" + n).value!='')
                Account_No=document.getElementById("txtAcNo"  + n ).value;
            else
                Account_No=null
            document.getElementById("<%= hid_Data.ClientID %>").value+= Empcode + "ÿ" + EmpType  + "ÿ" + TDate + "ÿ" + Status_Remark + "ÿ"  + Arrear +  "ÿ"  + Arrear_Remark + "ÿ" +  IFSC + "ÿ" + Account_No  + "Ñ";
        }
        ToServer("1Ø" + BranchId + "Ø" + document.getElementById("<%= hid_Data.ClientID %>").value + "Ø" + document.getElementById("<%= hid_Month.ClientID %>").value + "Ø" +  document.getElementById("<%= hid_Year.ClientID %>").value + "Ø" + saveflag, 1);
        
    }



     function AllowIFSC(i) {
        if (document.getElementById("txtIFSC" + i).value!=''){
            var ifsc = document.getElementById("txtIFSC" + i).value;
            var reg = /[A-Z|a-z][a-zA-Z0-9]{7}$/;

            if (ifsc.match(reg)) {
                return true;
            }
            else {
                alert("You Entered Wrong IFSC Code \n\n ------ or------ \n\n IFSC code should be count 11 \n\n-> Starting 4 should be only alphabets[A-Z] \n\n-> Remaining 7 should be accepting only alphanumeric");
                document.getElementById("txtIFSC" + i).focus();
                return false;
            }
        }

    }









    function btnExit_onclick() {
        if (document.getElementById("<%= hid_PostID.ClientID %>").value==5 || document.getElementById("<%= hid_PostID.ClientID %>").value==19)        
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        else
            window.open("Update_Salary_Inform_AM.aspx?", "_self");
    }

</script>
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
</script>
    <table align="center" style="width: 100%">
        <tr>
            <td style="width:6%;">
                &nbsp;</td>
            <td style="width:10%;">
                &nbsp;</td>
            <td style="width:20%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:6%;">
               Branch Code</td>
            <td style="width:10%;">
            <asp:TextBox ID="txtBranchId" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="40%" MaxLength="5"  disabled="true"  /></td>
            <td style="width:20%;">
            
            </td>
            <td style="border:2px solid silver; text-align:center;" colspan="8">
               <b>Leave Abbrevations</b></td>
        </tr>
        <tr>
            <td style="width:6%;">
                Branch Name</td>
            <td colspan="2">
           <asp:TextBox ID="txtBranch" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="90%" MaxLength="50" disabled="true"   /></td>
            <td style="width:8%;border:1px solid silver;">
           LOP-Loss of Pay</td>
            <td style="width:8%;border:1px solid silver;">
             CL -Casual Leave</td>
            <td style="width:8%;border:1px solid silver;">
                ML/PL&nbsp;-Maternity/ Paternity&nbsp;Leave</td>
            <td style="width:8%;border:1px solid silver;">
                PL-Prevelage Leave</td>
            <td style="width:8%;border:1px solid silver;">
                SL- Sick Leave</td>
            <td style="width:8%;border:1px solid silver;">
                ESI-ESI Leave</td>
            <td style="width:8%;border:1px solid silver;">
                ESI ML- 
                ESI Maternity Leave</td>
            <td style="width:8%;border:1px solid silver;">
                UA- Un Authorized Leave</td>
        </tr>
        <tr>
            <td colspan="11" style="text-align:center;">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="11" style="text-align:center;">
                <asp:Label ID="lblInformation" runat="server" Text="" style="text-align:center;font-family:Cambria; font-weight:bold; font-size:13pt;"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="11">
                  <asp:Panel ID="pnlDisplay" runat="server">
            </asp:Panel></td>
        </tr>
        <tr>
            <td style="width:6%;">
                &nbsp;</td>
            <td style="width:10%;">
                &nbsp;</td>
            <td style="width:20%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="11" style="text-align:center;">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width:6%; height: 25px;" 
                type="button" value="SAVE"  onclick="return btnSave_onclick()" />                 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%; height: 25px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
               <input id="btnSubmit" style="font-family: cambria; cursor: pointer; width:6%; height: 25px;" 
                type="button" value="SUBMIT"  onclick="return SubmitOnClick()" /></td>
        </tr>
        <tr>
            <td style="width:6%;">
                &nbsp;</td>
            <td style="width:10%;">
               <asp:HiddenField ID="hid_Item" runat="server" />
               <asp:HiddenField ID="hid_Data" runat="server" />
               <asp:HiddenField ID="hid_Month" runat="server" />
               <asp:HiddenField ID="hid_Year" runat="server" />
               <asp:HiddenField ID="hid_Type" runat="server" />
               <asp:HiddenField ID="hid_FromDt" runat="server" />
               <asp:HiddenField ID="hid_ToDt" runat="server" />
               </td>
            <td style="width:20%;">
               <asp:HiddenField ID="hid_Value" runat="server" />  
               <asp:HiddenField ID="hid_PostID" runat="server" />
            </td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
            <td style="width:8%;">
                &nbsp;</td>
        </tr>
    </table>
</head>
</asp:Content>

