﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Update_Main.aspx.vb" Inherits="Last_Work_date_Update" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head>
<title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
     <link rel="stylesheet" href="../../Style/bootstrap-3.1.2.minNew.css" type="text/css" />
	<link rel="stylesheet" href="../../Style/bootstrap-risk-multiselect.css" type="text/css" />
    <link href="../../Style/datepicker.css"  rel="stylesheet"/>
    <link href="../../Style/jquery.datepick.css" rel="stylesheet"/>
    <script type="text/javascript" src="../../Script/jquery.datepick.js"></script>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
	<script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
	<script type="text/javascript" src="../../Script/bootstrap-multiselect_RISK.js"></script>
    

  
    <script type="text/javascript">
        $(document).ready(function () {
            $('#chkRegion').multiselect();
            buttonWidth: '500px'
        }); 0

        $(document).ready(function () {
            $('#chkArea').multiselect();
            buttonWidth: '500px'
        }); 0

        $(document).ready(function () {
            $('#chkBranch').multiselect();
            buttonWidth: '500px'
        }); 0
              
    </script>

    <script language="javascript" type="text/javascript">

        $(function () {
            $('#chkRegion').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true
            });
            $('#btnget').click(function () {
                alert($('#chkRegion').val());
            })
            
            $('#chkArea').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true
            });
            $('#btnget').click(function () {
                alert($('#chkArea').val());
            })
           

            $('#chkBranch').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true
            });
            $('#btnget').click(function () {
                alert($('#chkBranch').val());
            })
             
        });
        
        function window_onload() {
            ToServer("1ʘ1", 1);
        }
       

         function FromServer(Arg, Context) {
            switch (Context) {
                case 1:{
                    var Data = Arg.split("ʘ");
                    document.getElementById("<%= hid_Value.ClientID %>").value = Data[2];
                    if (Data[1]==1){
                        FillRegion();
                        GetSelectedRegion();
                        break;
                    }
                    else if (Data[1]==2){
                        FillArea();
                        GetSelectedArea();
                        break;
                    }
                    else{
                        FillBranch();
                        break;
                    }
                }
                             
            }
        }
                    
        
        


        
         function FillRegion() {
            var Cnt=0;
            var SelectGroup = "";
            Val = "";
            var rrr = document.getElementById("<%= hid_Value.ClientID %>").value.split("Ñ");
            if (rrr != "") {
                for (a = 1; a < rrr.length; a++) {
                    var cols = rrr[a].split("ÿ");
                    if (cols[2] == 1) { 
                        Val = "selected";
                        Cnt=Math.abs(Cnt)+1;
                    }
                    else
                        Val="";
                    SelectGroup += "<option value=" + cols[0] + " " + Val + " >" + cols[1] + "</option>";
                }
                $('#chkRegion').html(''); // clear out old list
                $('#chkRegion').multiselect('destroy');  // tell widget to clear itself
                $('#chkRegion').html(SelectGroup); // add in the new list
                $('#chkRegion').multiselect();
                if (Cnt!=0)
                    $('#chkRegion').multiselect('disable');
                else
                    $('#chkRegion').multiselect('enable');
            }
        }


         function GetSelectedRegion(){
            var SelectedItems = $('#chkRegion').val();
            var StrVal = SelectedItems.toString();
            var StrLast = StrVal.replace('multiselect-all,', '');            
            ToServer("1ʘ2ʘ" + StrLast, 1);
        }


        function FillArea() {
            var Cnt=0;
            var SelectGroup = "";
            Val = "";
            var rrr = document.getElementById("<%= hid_Value.ClientID %>").value.split("Ñ");
            if (rrr != "") {
                for (a = 1; a < rrr.length; a++) {
                    var cols = rrr[a].split("ÿ");
                    if (cols[2] == 1) {
                        Val = "selected";
                        Cnt=Math.abs(Cnt)+1;
                    }
                    else
                        Val="";                        
                    SelectGroup += "<option value=" + cols[0] + " " + Val + " >" + cols[1] + "</option>";
                }
                $('#chkArea').html(''); // clear out old list
                $('#chkArea').multiselect('destroy');  // tell widget to clear itself
                $('#chkArea').html(SelectGroup); // add in the new list
                $('#chkArea').multiselect();
                if (Cnt!=0)
                    $('#chkArea').multiselect('disable');
                else
                    $('#chkArea').multiselect('enable');
            }   
        }


        function GetSelectedArea() {
            var SelectedItems = $('#chkArea').val();
            var StrVal = SelectedItems.toString();
            var StrLast = StrVal.replace('multiselect-all,', '');
            ToServer("1ʘ3ʘ" + StrLast , 1);
        }


        function FillBranch() {
            var Cnt=0;
            var SelectGroup = "";
            Val = "";
            var rrr = document.getElementById("<%= hid_Value.ClientID %>").value.split("Ñ");
            if (rrr != "") {
                for (a = 1; a < rrr.length; a++) {
                    var cols = rrr[a].split("ÿ");
                    if (cols[2] == 1) {
                        Val = "selected";
                        Cnt=Math.abs(Cnt)+1;
                    }
                    else
                        Val="";                        
                    SelectGroup += "<option value=" + cols[0] + " " + Val + " >" + cols[1] + "</option>";
                }
                $('#chkBranch').html(''); // clear out old list
                $('#chkBranch').multiselect('destroy');  // tell widget to clear itself
                $('#chkBranch').html(SelectGroup); // add in the new list
                $('#chkBranch').multiselect();
                if (Cnt!=0)
                    $('#chkBranch').multiselect('disable');
                else
                    $('#chkBranch').multiselect('enable');
            }   
        }

        function  ShowOnClick() {
        var SelectedItems = $('#chkBranch').val(); 
            if (SelectedItems == null || SelectedItems=='multiselect-all') {
                alert("Select Branch");
                return false;
            }
            var StrVal = SelectedItems.toString();
            var StrLast = StrVal.replace('multiselect-all,', '');
            if(document.getElementById("<%= hid_Id.ClientID %>").value==1) //Last work date update 
                window.open("Last_Work_date_Update_Details.aspx?BranchList=" + btoa(StrLast), "_self");
            else
                window.open("Reports/View_Change_Status.aspx?BranchList=" + btoa(StrLast), "_blank");
        }



        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }


</script>
</head>
 <table style="width: 95%;margin: 0px auto">
    
    <tr>
        <td colspan="3" style="width:100%; height: 15px;text-align: center;">
            
        </td>
    </tr>
    <tr>
        <td style="width: 15%; text-align: left;"></td>
        <td style="width: 10%; text-align: left;">
            Region Name
        </td>
        <td style="width: 75%">
            &nbsp; &nbsp;<select id='chkRegion'  multiple='multiple' style='width:88%; text-align:center;' onchange='GetSelectedRegion()' name="D1"></select>
        </td>
    </tr>
    <tr>
        <td style="width: 15%; text-align: left;"></td>
        <td style="width: 10%; text-align: left;">
            Area Name
        </td>
        <td style="width: 75%">
            &nbsp; &nbsp;<select id='chkArea'  multiple='multiple' style='width:88%; text-align:center;' onchange='GetSelectedArea()' name="D1"></select>
        </td>
    </tr>
    <tr>
        <td style="width: 15%; text-align: left;"></td>
        <td style="width: 10%; text-align: left;">
            Branch Name
        </td>
        <td style="width: 75%">
            &nbsp; &nbsp;<select id='chkBranch'  multiple='multiple' style='width:88%; text-align:center;'  name="D1"></select>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="width:100%; height: 15px;text-align: center;">            
        </td>
    </tr>
    <tr>
        <td style="text-align: center; font-family:Cambria;" colspan="3">
           <input id="btnShow" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="SHOW"  onclick="return ShowOnClick()" />  &nbsp; &nbsp; &nbsp; 
           <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />   
                <asp:HiddenField ID="hid_Value" runat="server" />
                <asp:HiddenField ID="hid_Id" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="3" style="width:100%; height: 15px;text-align: center;">            
        </td>
    </tr>
    <tr>
        <td colspan="3" ">
            <asp:Panel ID="pnlDisplay" runat="server">
            </asp:Panel>
        </td>
        
    </tr>
 </table>
</asp:Content>

