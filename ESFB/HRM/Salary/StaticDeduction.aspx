﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="StaticDeduction.aspx.vb" Inherits="HRM_Salary_StaticDeduction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <table align="center" style="width: 60%; margin:0px auto;">
        <tr>
            <td style="width:10%">
                &nbsp;</td>
            <td style="width:40%">
                <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </ajaxToolkit:ToolkitScriptManager>
            </td>
            <td style="width:10%">
                &nbsp;</td>
            <td style="width:40%">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:10%">
                Emp Code</td>
            <td style="width:40%">
                <input id="txtEmpCode" style="width: 30%" type="text" /></td>
            <td style="width:10%">
                Name</td>
            <td style="width:40%">
                <input id="Text2" readonly="readonly" style="width: 95%" type="text" /></td>
        </tr>
        <tr>
            <td style="width:10%">
                Branch</td>
            <td style="width:40%">
                <input id="Text3" readonly="readonly" style="width: 95%" type="text" /></td>
            <td style="width:10%">
                Department</td>
            <td style="width:40%">
                <input id="Text4" readonly="readonly" style="width: 95%" type="text" /></td>
        </tr>
        <tr>
            <td style="width:10%">
                &nbsp;</td>
            <td style="width:40%">
                &nbsp;</td>
            <td style="width:10%">
                &nbsp;</td>
            <td style="width:40%">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" class="mainhead"style="text-align:center;">
                Deduction Details</td>
        </tr>
        <tr>
            <td style="width:10%">
                &nbsp;</td>
            <td style="width:40%">
                &nbsp;</td>
            <td style="width:10%">
                &nbsp;</td>
            <td style="width:40%">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:10%">
                Type</td>
            <td style="width:40%">
                <asp:DropDownList ID="ddlType" runat="server" Width="95%">
                </asp:DropDownList>
            </td>
            <td style="width:10%">
                Amount</td>
            <td style="width:40%">
                <input id="txtAmount" style="width: 30%" type="text" /></td>
        </tr>
        <tr>
            <td style="width:10%">
                installments</td>
            <td style="width:40%">
                <input id="txtInstallments" style="width: 30%" type="text" /></td>
            <td style="width:10%">
                Start Date</td>
            <td style="width:40%">
                <asp:TextBox ID="txtStartDate" runat="server" Width="30%"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="ce1" runat="server" 
                    Enabled="True" TargetControlID="txtStartDate">
                </ajaxToolkit:CalendarExtender>&nbsp;&nbsp;
                <input id="btnShow" style="font-family: Cambria; font-size: 10pt; width: 55%" 
                    type="button" value="Show Installment Distribution" /></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Panel ID="pn1" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="width:10%">
                &nbsp;</td>
            <td style="width:40%">
                &nbsp;</td>
            <td style="width:10%">
                &nbsp;</td>
            <td style="width:40%">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center;">
                <input id="btnSave" type="button" value="SAVE" 
                    style="font-family: Cambria; font-size: 11pt; width: 10%" />&nbsp;&nbsp;<input id="btnExit" 
                    type="button" value="EXIT" 
                    style="font-family: Cambria; font-size: 11pt; width: 10%" /></td>
        </tr>
        <tr>
            <td style="width:10%">
                &nbsp;</td>
            <td style="width:40%">
                &nbsp;</td>
            <td style="width:10%">
                &nbsp;</td>
            <td style="width:40%">
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

