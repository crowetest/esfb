﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Update_Salary_Inform
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GF As New GeneralFunctions
#Region "Page Load & Disposed"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 357) = False And GF.FormAccess(CInt(Session("UserID")), 358) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Update Salary Input Details"
            Dim Yea_To As Integer = CDate(Session("TraDt")).Year
            Dim Mnt_To As Integer = CDate(Session("TraDt")).Month
            hid_PostID.Value = CStr(Session("Post_ID"))

            Dim cl_script1 As New System.Text.StringBuilder
            If (CInt(Session("Post_ID")) = 5 Or CInt(Session("Post_ID")) = 19) Then  'BM
                txtBranchId.Text = CStr(Session("BranchID"))
                txtBranch.Text = CStr(Session("BranchName"))
                'submittted or not
                DT = DB.ExecuteDataSet("select * from salary_inform where branch_id=" & CInt(txtBranchId.Text) & " and month_id=" & Mnt_To & " and YEAR_ID=" & Yea_To & " and status_id>=1").Tables(0)
                If DT.Rows.Count <> 0 Then
                    cl_script1.Append("         alert(' The salary input details already submited in this month ');")
                    cl_script1.Append("         window.open('../../Home.aspx','_self'); ")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)
                    Exit Sub
                End If
            ElseIf CInt(Session("Post_ID")) = 6 Then  'AM

                txtBranchId.Text = CStr(GF.Decrypt(Request.QueryString.Get("BranchId")))
                txtBranch.Text = CStr(GF.Decrypt(Request.QueryString.Get("BranchName")))
                'submittted or not
                DT = DB.ExecuteDataSet("select * from salary_inform where branch_id=" & CInt(txtBranchId.Text) & " and month_id=" & Mnt_To & " and YEAR_ID=" & Yea_To & " and status_id>=3").Tables(0)
                If DT.Rows.Count <> 0 Then
                    cl_script1.Append("         alert(' The salary input details already submited in this month ');")
                    cl_script1.Append("         window.open('../../Home.aspx','_self'); ")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)
                    Exit Sub
                End If
            End If
            Dim i As Integer = 0
            'fill salary status types(current status)
            Me.hid_Type.Value = ""
            DT = DB.ExecuteDataSet("select '-1' as type_id,'No Change' as type_name union all select type_id,type_name from salary_status_types where  status_id=1 and type_id<>-1 ").Tables(0)
            While i < DT.Rows.Count
                Me.hid_Type.Value += CStr(DT.Rows(i)(0)) & "~" & UCase(CStr(DT.Rows(i)(1)))
                Me.hid_Type.Value += "¥"
                i = i + 1
            End While
            'salary input period
            Dim Day_From As Integer = 0
            Dim Day_To As Integer = 0
            Dim Yea_From As Integer = Yea_To
            Dim Mnt_From As Integer = 0
            If Mnt_To = 1 Then
                Yea_From = Yea_To - 1
                Mnt_From = 12
            Else
                Mnt_From = Mnt_To - 1
            End If
            DT = DB.ExecuteDataSet("select from_day,to_day from salary_period WHERE status_id=1").Tables(0)
            If DT.Rows.Count <> 0 Then
                Day_From = CInt(DT.Rows(0)(0))
                Day_To = CInt(DT.Rows(0)(1))
            End If
            Dim FromDt As New DateTime(Yea_From, Mnt_From, Day_From)
            Dim ToDt As New DateTime(Yea_To, Mnt_To, Day_To)
            hid_FromDt.Value = CStr(FromDt)
            hid_ToDt.Value = CStr(ToDt)

            'get datas
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@BranchID", CInt(txtBranchId.Text)), New System.Data.SqlClient.SqlParameter("@MonthID", Mnt_To), New System.Data.SqlClient.SqlParameter("@YearID", Yea_To), New System.Data.SqlClient.SqlParameter("@FromDt", FromDt), New System.Data.SqlClient.SqlParameter("@ToDt", ToDt)}
            DT = DB.ExecuteDataSet("[SP_SALARY_INPUT_DETAILS_GET]", Parameters).Tables(0)
            hid_Value.Value = ""
            hid_Item.Value = ""
            i = 0
            If DT.Rows.Count <> 0 Then
                hid_Item.Value = DT.Rows.Count.ToString
                While i < DT.Rows.Count
                    hid_Value.Value += CStr(DT.Rows(i)(0)) & "~" & CStr(DT.Rows(i)(1)) & "~" & CStr(DT.Rows(i)(2)) & "~" & CStr(DT.Rows(i)(3)) & "~" & CStr(DT.Rows(i)(4)) & "~" & CStr(DT.Rows(i)(5)) & "~" & CStr(DT.Rows(i)(6)) & "~" & CStr(DT.Rows(i)(7)) & "~" & CStr(DT.Rows(i)(8)) & "~" & CStr(DT.Rows(i)(9)) & "~" & CStr(DT.Rows(i)(10)) & "~" & CStr(DT.Rows(i)(11)) & "~" & CStr(DT.Rows(i)(12)) & "~" & CStr(DT.Rows(i)(13)) & "~" & CStr(DT.Rows(i)(14)) & "~" & CStr(DT.Rows(i)(15)) & "~" & CStr(DT.Rows(i)(16)) & "~" & CStr(DT.Rows(i)(17)) & "~" & CStr(DT.Rows(i)(18))
                    hid_Value.Value += "¥"
                    i = i + 1
                End While
            End If
            lblInformation.Text = "The leave considered for this report is  from " + FromDt.ToString("dd/MMM/yyyy") + " to " + ToDt.ToString("dd/MMM/yyyy")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub

#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim PostID As Integer = CInt(Session("Post_ID"))
        Dim ErrorFlag As Integer = 0
        Dim Message As String = ""
        Dim BranchId As Integer = 0
        Dim MonthId As Integer = 0
        Dim YearId As Integer = 0
        If CInt(Data(0)) = 1 Then
            BranchId = CInt(Data(1))
            Dim Hid_data As String = CStr(Data(2))
            MonthId = CInt(Data(3))
            YearId = CInt(Data(4))
            Try
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(0).Value = BranchId
                Params(1) = New SqlParameter("@hid_Data", SqlDbType.VarChar,5000)
                Params(1).Value = Hid_data
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = CInt(UserID)
                Params(5) = New SqlParameter("@Month", SqlDbType.Int)
                Params(5).Value = CInt(MonthId)
                Params(6) = New SqlParameter("@yearId", SqlDbType.Int)
                Params(6).Value = CInt(YearId)
                Params(7) = New SqlParameter("@PostID", SqlDbType.Int)
                Params(7).Value = CInt(PostID)
                DB.ExecuteNonQuery("SP_SALARY_INFORM_UPDATE", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "~" + Message
        ElseIf CInt(Data(0)) = 2 Then
            BranchId = CInt(Data(1))
            MonthId = CInt(Data(2))
            YearId = CInt(Data(3))
            Dim FromDt As String = CStr(Data(4))
            Dim ToDt As String = CStr(Data(5))
            Try
                Dim Params(8) As SqlParameter
                Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(0).Value = BranchId
                Params(1) = New SqlParameter("@PostID", SqlDbType.Int)
                Params(1).Value = CInt(PostID)
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = CInt(UserID)
                Params(5) = New SqlParameter("@Month", SqlDbType.Int)
                Params(5).Value = CInt(MonthId)
                Params(6) = New SqlParameter("@yearId", SqlDbType.Int)
                Params(6).Value = CInt(YearId)
                Params(7) = New SqlParameter("@FromDt", SqlDbType.VarChar, 20)
                Params(7).Value = CStr(FromDt)
                Params(8) = New SqlParameter("@ToDt", SqlDbType.VarChar, 20)
                Params(8).Value = CStr(ToDt)
                DB.ExecuteNonQuery("SP_SALARY_INFORM_SUBMIT", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "~" + Message + "~" + CStr(ID)
        End If

    End Sub
#End Region
End Class
