﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Emp_Salary_Status
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions

#Region "Page Load & Disposed"
    Protected Sub Page_Break_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 362) = False And GN.FormAccess(CInt(Session("UserID")), 359) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Update Salary Status"
            Me.hid_Month.Value = CStr(CDate(Session("TraDt")).Month)
            Me.hid_Year.Value = CStr(CDate(Session("TraDt")).Year)

            Me.hid_Post.Value = CStr(Session("Post_ID"))

            Dim cl_script1 As New System.Text.StringBuilder
            If CInt(Session("Post_ID")) = 5 Or CInt(Session("Post_ID")) = 6 Then
                If CInt(Session("Post_ID")) = 5 Then
                    DT = DB.ExecuteDataSet("select * from salary_inform s, brmaster b   where s.branch_id=b.branch_id and s.month_id=" & CInt(hid_Month.Value) & " and s.YEAR_ID=" & CInt(hid_Year.Value) & " and s.status_id<4 and s.branch_id=" & CInt(Session("BranchID")) & "").Tables(0)
                ElseIf CInt(Session("Post_ID")) = 6 Then
                    DT = DB.ExecuteDataSet("select * from salary_inform s, brmaster b   where s.branch_id=b.branch_id and s.month_id=" & CInt(hid_Month.Value) & " and s.YEAR_ID=" & CInt(hid_Year.Value) & " and s.status_id<4 and b.area_head=" & CInt(Session("UserID")) & "").Tables(0)
                End If
                If DT.Rows.Count <> 0 Then
                    cl_script1.Append("         alert('HR Department is not completed the first level checking of salary status  ');")
                    cl_script1.Append("         window.open('../../Home.aspx','_self'); ")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)
                    Exit Sub
                End If
            Else
                DT = DB.ExecuteDataSet("select s.status_id from salary_state_emp_dtls e, salary_inform s where e.state_id=s.state_id and e.emp_code=" & CInt(Session("UserID")) & " " & _
                    " and e.status_id=1 and s.month_id =" & CInt(hid_Month.Value) & " And s.YEAR_ID = " & CInt(hid_Year.Value) & "  ").Tables(0)
                If DT.Rows.Count = 0 Then
                    cl_script1.Append("         alert('Branches are not submit the salary input details ');")
                    cl_script1.Append("         window.open('../../Home.aspx','_self'); ")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)
                    Exit Sub
                Else
                    If CInt(DT.Rows(0)(0)) = 4 Then
                        lblName.Text = " If final level checking of HR is completed , click the SUBMIT button"
                        Me.hid_Submit.Value = CStr(5)
                    Else
                        lblName.Text = " If first level checking of HR is completed , click the SUBMIT button"
                        Me.hid_Submit.Value = CStr(4)
                    End If
                End If
            End If
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub

#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim PostID As Integer = CInt(Session("Post_ID"))


        Select Case CInt(Data(0))
            Case 1 '
                Dim IntId As Integer = CInt(Data(1))
                Dim ItemList As String = Nothing
                Dim DR As DataRow
                Dim StrValue As String = ""
                '-------------------REgion------------------------------------------------------------------------------
                If IntId = 1 Then '
                    If PostID = 5 Then
                        DT = DB.ExecuteDataSet("select region_id,region_name from brmaster where branch_head=" & UserID & " ").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    ElseIf PostID = 6 Then
                        DT = DB.ExecuteDataSet("select distinct region_id,region_name from brmaster where area_head=" & UserID & " ").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    Else
                        DT = DB.ExecuteDataSet("select distinct region_id,region_name from SALARY_STATE_EMP_DTLS s, brmaster b where s.state_id=b.state_id and s.emp_code=" & UserID & "").Tables(0)
                        If DT.Rows.Count > 0 Then
                            For Each DR In DT.Rows
                                StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                            Next
                        End If
                    End If
                    '------------------------------------Area------------------------------------------------
                ElseIf IntId = 2 Then
                    ItemList = CStr(Data(2))
                    If PostID = 5 Then
                        DT = DB.ExecuteDataSet("select area_id,area_name from brmaster where branch_head=" & UserID & " and region_id in (" & ItemList & ") ").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    ElseIf PostID = 6 Then
                        DT = DB.ExecuteDataSet("select distinct area_id,area_name from brmaster where area_head=" & UserID & " and region_id in (" & ItemList & ") ").Tables(0)
                        If DT.Rows.Count <> 0 Then
                            If DT.Rows.Count > 1 Then
                                For Each DR In DT.Rows
                                    StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                                Next
                            Else
                                StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                            End If
                        End If
                    Else
                        DT = DB.ExecuteDataSet("select distinct area_id,area_name from brmaster where  region_id in (" & ItemList & ") ").Tables(0)
                        If DT.Rows.Count <> 0 Then
                            For Each DR In DT.Rows
                                StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                            Next
                        End If
                    End If
                    '-------------------------------------------Branch-----------------------------
                ElseIf IntId = 3 Then
                    ItemList = CStr(Data(2))
                    If PostID = 5 Then
                        DT = DB.ExecuteDataSet("select branch_id,branch_name from brmaster where branch_head=" & UserID & " and area_id in (" & ItemList & ")").Tables(0)
                        If DT.Rows.Count > 0 Then
                            StrValue = "Ñ" + DT.Rows(0)(0).ToString() + "ÿ" + DT.Rows(0)(1).ToString() + "ÿ1Ñ0ÿ-ÿ0"
                        End If
                    Else
                        DT = DB.ExecuteDataSet("select branch_id,branch_name from brmaster where  area_id in (" & ItemList & ")").Tables(0)
                        If DT.Rows.Count > 0 Then
                            For Each DR In DT.Rows
                                StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                            Next
                        End If
                    End If
                End If
                
                CallBackReturn = "1ʘ" + CStr(IntId) + "ʘ" + StrValue
            Case 2
                DT = DB.ExecuteDataSet("select branch_id from brmaster where state_id=2 and branch_id not in ( " & _
                        " select s.branch_id from salary_state_emp_dtls e, salary_inform s where e.state_id=s.state_id and e.emp_code=" & CInt(Session("UserID")) & " " & _
                        " and e.status_id=1 and s.month_id =" & CInt(hid_Month.Value) & " And s.YEAR_ID =" & CInt(hid_Year.Value) & "  and s.status_id<3)").Tables(0)
                If DT.Rows.Count <> 0 Then
                    CallBackReturn = "1ʘAccess Denied ! Branches are not submit the salary input details "
                Else
                    Dim ErrorFlag As Integer = 0
                    Dim Message As String = ""
                    Dim MonthId As Integer = CInt(Data(1))
                    Dim YearId As Integer = CInt(Data(2))
                    Dim Stat As Integer = CInt(Data(3))
                    Try
                        Dim Params(5) As SqlParameter
                        Params(0) = New SqlParameter("@Month", SqlDbType.Int)
                        Params(0).Value = CInt(MonthId)
                        Params(1) = New SqlParameter("@yearId", SqlDbType.Int)
                        Params(1).Value = CInt(YearId)
                        Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(2).Direction = ParameterDirection.Output
                        Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(3).Direction = ParameterDirection.Output
                        Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                        Params(4).Value = CInt(UserID)
                        Params(5) = New SqlParameter("@Status", SqlDbType.Int)
                        Params(5).Value = CInt(Stat)
                        DB.ExecuteNonQuery("[SP_SALARY_STATUS_HR_SUBMIT]", Params)
                        ErrorFlag = CInt(Params(2).Value)
                        Message = CStr(Params(3).Value)
                    Catch ex As Exception
                        Message = ex.Message.ToString
                        ErrorFlag = 1
                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                    End Try
                    CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
                End If
        End Select
    End Sub


#End Region
End Class
