﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Emp_Salary_Status_Details
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
#Region "Page Load & Disposed"
    Protected Sub Page_Break_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            Me.hid_ID.Value = CStr(GN.Decrypt(Request.QueryString.Get("ID"))) 'dispersal/withheld
            Dim BranchList As String = CStr(GN.Decrypt(Request.QueryString.Get("BranchList")))
            Me.hid_Month.Value = CStr(GN.Decrypt(Request.QueryString.Get("MonthID")))
            Me.hid_Year.Value = CStr(GN.Decrypt(Request.QueryString.Get("YearID")))
            Me.Master.subtitle = "Update Salary Status"
            Dim I As Integer = 0
            DT = DB.ExecuteDataSet("select '-1' as type_id,'------Select------' as type_name union all select type_id,type_name from salary_status_types where type_stat=1 and status_id=1").Tables(0)
            While I < DT.Rows.Count
                Me.hid_Type.Value += CStr(DT.Rows(I)(0)) & "~" & UCase(CStr(DT.Rows(I)(1)))
                Me.hid_Type.Value += "¥"
                I = I + 1
            End While
            If CInt(Me.hid_ID.Value) = 2 Then 'dispersal
                txtStat.Text = "  Employee Details Enabled For Salary Disbursal "
                DT = DB.ExecuteDataSet("SELECT E.EMP_CODE,E.EMP_NAME,D.SALARY_REMARK,B.REGION_NAME,B.AREA_NAME,B.BRANCH_id,B.BRANCH_NAME FROM salary_inform S ,salary_inform_dtls D,EMP_MASTER E, BRMASTER B WHERE S.SALARY_ID=D.SALARY_ID AND " & _
                    " e.emp_code=d.emp_code  AND  S.BRANCH_ID=B.BRANCH_ID AND  D.SALARY_STATUS = 1 and E.BRANCH_ID in (" & BranchList & ") And S.MONTH_ID =" & Me.hid_Month.Value & " And S.YEAR_ID = " & Me.hid_Year.Value & " " & _
                    " ORDER BY 3,4,5,1").Tables(0)
            Else 'withheld
                txtStat.Text = " Details Of The Employees Whose Salaries Wilthhold "
                DT = DB.ExecuteDataSet("SELECT E.EMP_CODE,E.EMP_NAME,D.SALARY_REMARK,B.REGION_NAME,B.AREA_NAME,B.BRANCH_id,B.BRANCH_NAME FROM salary_inform S ,salary_inform_dtls D,EMP_MASTER E, BRMASTER B WHERE S.SALARY_ID=D.SALARY_ID AND " & _
                   " e.emp_code=d.emp_code  AND  S.BRANCH_ID=B.BRANCH_ID AND  D.SALARY_STATUS = 2 and E.BRANCH_ID in (" & BranchList & ") And S.MONTH_ID =" & Me.hid_Month.Value & " And S.YEAR_ID = " & Me.hid_Year.Value & " " & _
                   " ORDER BY 3,4,5,1").Tables(0)
            End If
            Dim StrData As String = Nothing
            If DT.Rows.Count <> 0 Then
                I = 0
                Me.hid_Count.Value = DT.Rows.Count.ToString
                While I < DT.Rows.Count
                    StrData += CStr(DT.Rows(I)(0)) & "~" & UCase(CStr(DT.Rows(I)(1))) & "~" & CStr(DT.Rows(I)(2)) & "~" & UCase(CStr(DT.Rows(I)(3))) & "~" & UCase(CStr(DT.Rows(I)(4))) & "~" & CStr(DT.Rows(I)(5)) & "~" & UCase(CStr(DT.Rows(I)(6)))
                    StrData += "¥"
                    I = I + 1
                End While
            End If
            Me.hdnDtls.Value = StrData
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        
        Select Case CInt(Data(0))
            Case 1 'save 
                Dim ErrorFlag As Integer = 0
                Dim Message As String = ""
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim ID As Integer = CInt(Data(1))
                Dim MonthID As Integer = CInt(Data(2))
                Dim YearID As Integer = CInt(Data(3))
                Dim Hid_data As String = CStr(Data(4))
                Try
                    Dim Params(6) As SqlParameter
                    Params(0) = New SqlParameter("@hid_Data", SqlDbType.VarChar, 5000)
                    Params(0).Value = Hid_data
                    Params(1) = New SqlParameter("@ChangeID", SqlDbType.Int)
                    Params(1).Value = CInt(ID)
                    Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(2).Direction = ParameterDirection.Output
                    Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(4).Value = CInt(UserID)
                    Params(5) = New SqlParameter("@MonthId", SqlDbType.Int)
                    Params(5).Value = CInt(MonthID)
                    Params(6) = New SqlParameter("@yearId", SqlDbType.Int)
                    Params(6).Value = CInt(YearID)
                    DB.ExecuteNonQuery("[SP_SALARY_STATUS_UPDATE]", Params)
                    ErrorFlag = CInt(Params(2).Value)
                    Message = CStr(Params(3).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message

        End Select
    End Sub


#End Region
End Class
