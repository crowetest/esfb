﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Last_Work_date_Update_Details.aspx.vb" Inherits="Last_Work_date_Update_Details" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head>
<title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <link href="../../Style/datepicker.css"  rel="stylesheet"/>
    <link href="../../Style/jquery.datepick.css" rel="stylesheet"/>

    <script type="text/javascript" src="../../Script/jquery.js"></script>
    <script type="text/javascript" src="../../Script/jquery.datepick.js"></script>
  <script type="text/javascript">
      $(document).ready(function () {
          $('.date').datepick();

      });
</script>
   
    <script language="javascript" type="text/javascript">

         function FromServer(Arg, Context) {
            switch (Context) {
      
                case 1:{
                    var Data = Arg.split("ʘ");
                    document.getElementById("<%= hid_Value.ClientID %>").value = Data[0];
                    table_fill();                    
                }                
            }
        }
                    
        
        function table_fill() {
            document.getElementById("<%= pnlDisplay.ClientID %>").style.display = ''; 
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%;background-color:#A34747; height:auto; overflow:auto; margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
            tab += "<tr height=100px;  class='mainhead'>";
            tab += "<td style='width:3%;text-align:center' >Slno</td>";
            tab += "<td style='width:9%;text-align:center' >Region Name</td>";
            tab += "<td style='width:9%;text-align:center' >Area Name</td>";
            tab += "<td style='width:4%;text-align:center' >Branch Code</td>";
            tab += "<td style='width:9%;text-align:center' >Branch Name</td>";
            tab += "<td style='width:4%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:10%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:5%;text-align:center' >Resigned Date</td>";
            tab += "<td style='width:18%;text-align:center' >Remark</td>"; 
            tab += "<td style='width:3%;text-align:center' ></td>";                       
            tab += "<td style='width:6%;text-align:center' >Last Working Date</td>";
            tab += "<td style='width:21%;text-align:center' >Remark</td>";           
            tab += "</tr>";
            var row = document.getElementById("<%= hid_Value.ClientID %>").value.split("Ñ"); 
            var i=0;
            var RegionName='';
            var AreaName='';
            var BrName='';
            for (n = 1; n <= row.length - 1; n++) {
                var col = row[n].split("ÿ");
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr  class='sub_first';>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class='sub_first';>";
                }
                i=i+1;
                tab += "<td style='width:3%;text-align:center' >" + i + "</td>";
                if (RegionName==col[0])
                        tab += "<td style='width:9%;text-align:left' class='NormalText' ></td>";//
                else{
                     tab += "<td style='width:9%;text-align:left' class='NormalText' >" + col[0] + "</td>";//
                    RegionName =col[0];
                }
                if (AreaName==col[1])
                        tab += "<td style='width:9%;text-align:left' class='NormalText' ></td>";//
                else{
                     tab += "<td style='width:9%;text-align:left' class='NormalText' >" + col[1] + "</td>";//
                    AreaName =col[1];
                }
                if (BrName==col[2])
                        tab += "<td style='width:4%;text-align:left' class='NormalText' ></td>";//
                else{
                     tab += "<td style='width:4%;text-align:left' class='NormalText' >" + col[2] + "</td>";//
                    BrName =col[2];
                }               
                tab += "<td style='width:9%;text-align:left' class='NormalText' >" + col[3] + "</td>";//
                var txtEmpCode= "<input type=text id='txtEmpCode" + i + "' name='txtEmpCode" + i + "'   style='width:99%;text-align:left;background-color:#FFFFF0;border:none; ' class='NormalText' ReadOnly='true' value=" + col[4] + " />";
                tab += "<td style='width:4%;text-align:left;border:none;' >"+ txtEmpCode +"</td>"; //empcode
                tab += "<td style='width:10%;text-align:left' class='NormalText' >" + col[5] + "</td>";//empname
                tab += "<td style='width:5%;text-align:left' >" + col[6] + "</td>"; //                              
                tab += "<td style='width:18%;text-align:left' >" + col[7] + "</td>"; // 
                tab += "<td style='width:3%;text-align:center'><input id='chkStatus" + i + "' type='checkbox' onclick=ChangeStatus(" + i + ") /></td>";
                var txtDate= "<input type=text id='txtDate" + i + "' name='txtDate" + i + "'   style='width:99%;text-align:center; background-color:#FFFFF0;border:none;'  onchange=Validate_date("+ i +") ReadOnly='true' disabled='true'  class='date'  />";                     
                tab += "<td style='width:6%;text-align:left' >"+ txtDate +"</td>";
                var txtRemark="<textarea id='txtRemark" + i + "' name='txtRemark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;' disabled='true'  maxlength='3000' onkeypress='return TextAreaCheck(event)'  ></textarea>";
                tab += "<td style='width:21%;text-align:left' >"+ txtRemark +"</td>";  
                tab += "</tr>";
            }
            document.getElementById("<%= pnlDisplay.ClientID %>").innerHTML = tab;
        }


        function ChangeStatus(i) {
            if (document.getElementById("chkStatus" + i).checked == true){
                document.getElementById("txtRemark" + i).disabled = false;
                document.getElementById("txtDate" + i).disabled = false;
            }
            else {
                document.getElementById("txtRemark" + i).value = '';
                document.getElementById("txtDate" + i).value = '';
                document.getElementById("txtRemark" + i).disabled = true;
                document.getElementById("txtDate" + i).disabled = true;
            }
        } 
       
      


       
        function btnSave_onclick(){
            var Conf = confirm("Are you sure to save the last date of resigned staffs ?");
            if (Conf == true) {
                document.getElementById("<%= hid_Value.ClientID %>").value='';
                var Empcode='';
                var TDate='';
                var Remark=null;
                var RowLength=document.getElementById("<%= hid_Count.ClientID %>").value-1;
                var n=0; 
                for (n = 1; n <= RowLength+1; n++) {           
                    if (document.getElementById("chkStatus" + n).checked==true  ){
                        if(document.getElementById("txtDate" + n).value==''){
                            alert("Select Date ");
                            document.getElementById("txtDate" + n).focus();
                            return false;
                        }
                        if(document.getElementById("txtRemark" + n).value==''){
                            alert("Enter Remark ");
                            document.getElementById("txtRemark" + n).focus();
                            return false;
                        }            
                        Empcode=document.getElementById("txtEmpCode" + n ).value;
                        TDate=document.getElementById("txtDate" + n).value;
                        if (document.getElementById("txtDate" + n).value!='') {
                            var DT=document.getElementById("txtDate" + n).value.split("/");
                            var userday = DT[0];
                            var usermonth = DT[1];
                            var useryear = DT[2];
                            var lenmonth=usermonth.length;
                            if (usermonth.substr(0, 1)==0)
                                var res = usermonth.substr(1, lenmonth);
                            else
                                var res =usermonth;
                            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                            TDate = userday + "/" + months[res - 1] + "/" + useryear; 
                        }
                        else
                            TDate='';
                        Remark=document.getElementById("txtRemark" + n ).value;
                        document.getElementById("<%= hid_Value.ClientID %>").value+= Empcode + "ÿ" + TDate + "ÿ" + Remark  + "Ñ";
                    }
                }
                ToServer("1ʘ" + document.getElementById("<%= hid_Value.ClientID %>").value, 1);
            }
        }




        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }


</script>
</head>
 <table style="width: 95%;margin: 0px auto">
    
   <tr>
        <td colspan="3" ">
            <asp:Panel ID="pnlDisplay" runat="server">
            </asp:Panel>
        </td>
        
    </tr>
    <tr>
        <td colspan="3" style="width:100%; height: 15px;text-align: center;">            
        </td>
    </tr>
    <tr>
        <td style="text-align: center; font-family:Cambria;" colspan="3">
           <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="SAVE"  onclick="return btnSave_onclick()" />  &nbsp; &nbsp; &nbsp; 
           <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />   
                <asp:HiddenField ID="hid_Value" runat="server" />
                <asp:HiddenField ID="hid_Count" runat="server" />
                
        </td>
    </tr>
    
    
 </table>
</asp:Content>

