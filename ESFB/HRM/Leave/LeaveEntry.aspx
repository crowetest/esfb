﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="LeaveEntry.aspx.vb" Inherits="LeaveEntry" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server" >
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
    <script src="../../Script/Calculations.js" language="javascript" type="text/javascript"> </script> 
    <script language="javascript" type="text/javascript">
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                if (Data[0] == 1) { 
                    alert( Data[1]);
                    document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                    document.getElementById("<%= txtLeaveFrom.ClientID %>").value = "";
                    document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                    document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false;
                }
                else if (Data[0] == 2) { 
                    if (Math.abs(document.getElementById("<%= txtLeaveDays.ClientID %>").value) >Math.abs(Data[1])){
                        var msg=" Materinity leave allowed only " + Data[1] + " days";
                        alert(msg);
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus(); return false;
                    }
                }
            }
        }
         function CatagoryOnChange()
        {
        document.getElementById("<%= cmbLeaveReason.ClientID %>").value=-1;
            if( document.getElementById("<%= cmbLeaveCategory.ClientID %>").value==11)
            {
            document.getElementById("<%= cmbLeaveReason.ClientID %>").value=3;
        

            }
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function showDetails() {            
            var tmptab;
            tmptab = ""
            tmptab = "<br/><table style='width:100%;margin: 0px auto;background-color:#A34747'><tr style='text-align:center;'></tr>";
            tmptab = tmptab + "<tr ><td style='text-align:center;color:#FFF;' colspan='5' ><b>LEAVE BALANCE</b></td></tr><tr class=mainhead style='text-align:center;'>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>Casual Leave</b></td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>Sick Leave</b></td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>Privilege Leave</b></td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>Compensatory</b></td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>RH</b></td></tr>";
            var DTL = document.getElementById("<%= hdnData.ClientID %>").value.split("ử");
            var rowSplitarr = DTL[0].split("~");
            var CLBal = Math.abs(rowSplitarr[0]) - Math.abs(rowSplitarr[5]);
            var SLBal = Math.abs(rowSplitarr[1]) - Math.abs(rowSplitarr[6]);
            var ELBal = Math.abs(rowSplitarr[2]) - Math.abs(rowSplitarr[7]);
            var CMBal = Math.abs(rowSplitarr[3]) - Math.abs(rowSplitarr[8]);
            var RHBal = Math.abs(rowSplitarr[4]) - Math.abs(rowSplitarr[9]);
            tmptab += "<tr style='text-align:center; color:#214263;background-color:#FFF; '>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>" + CLBal + "</td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%'  ><b>" + SLBal + "</td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>" + ELBal + "</td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>" + CMBal + "</td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>" + RHBal + "</td></tr></table>";
            document.getElementById("<%= pnLeaveDtl.ClientID %>").innerHTML = tmptab;
            document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
        }
        function DateOnChange() {
            document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
            document.getElementById("<%= chkHalfDay.ClientID %>").checked = false;
            document.getElementById("<%= ddlTiming.ClientID %>").value = "0";
            document.getElementById("<%= ddlTiming.ClientID %>").disabled = true;         
            var frmDt = document.getElementById("<%= txtLeaveFrom.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtLeaveTo.ClientID %>").value;
            var leaveTypeID = document.getElementById("<%= cmbLeaveCategory.ClientID %>").value;
            var DTL = document.getElementById("<%= hdnData.ClientID %>").value.split("ử");
             if(leaveTypeID==-1)
            {
                        alert("Choose Leave Category First");
                       
                        document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus();
                        return false;


            }
            else
            {
            if(leaveTypeID == 8)
            {
                 document.getElementById("<%= txtLeaveTo.ClientID %>").value = frmDt;
                 ToDt = frmDt ;
                 document.getElementById("<%= txtLeaveTo.ClientID %>").disabled = true;
                 document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
            }
            else
                document.getElementById("<%= txtLeaveTo.ClientID %>").disabled = false;
            var days = calculateDays(ToDt, frmDt);
            if (frmDt != "" && ToDt != "") {
                if (days > 0) {
                    document.getElementById("<%= txtLeaveDays.ClientID %>").value = days;
                    if (days == 1 && leaveTypeID != 8)
                    { document.getElementById("<%= chkHalfDay.ClientID %>").disabled = false; }
                    else
                    { document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true; document.getElementById("<%= chkHalfDay.ClientID %>").checked = false; }
                }
                else {
                    document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                    document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                    alert("Check Dates");
                    document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                    return false;
                }
            }
            
            if (DTL[1] != "2" && leaveTypeID == 4) {
                alert("Maternity Leave is only eligible for Female Employees");
                document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveFrom.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false;
            }

            if (DTL[1] != "2" && leaveTypeID == 9) {
                alert("ESI Maternity Leave is only eligible for Female Employees");
                document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveFrom.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false;
            }

            if (DTL[1] != "1" && leaveTypeID == 5) {
                alert("Paternity Leave is only eligible for Male Employees");
                document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveFrom.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false;
            } 
             
            if (frmDt != "" && ToDt != "" && leaveTypeID > 0) 
            {               
                var rowSplitarr = DTL[0].split("~");
                if (leaveTypeID == 1) 
                {
                    var dateDiff = calculateDays(document.getElementById("<%= txtLeaveFrom.ClientID %>").value, document.getElementById("<%= txtDOJ.ClientID %>").value);
                    var CLBal = Math.abs(rowSplitarr[0]) - Math.abs(rowSplitarr[5]);                                   
                    if (CLBal < days && CLBal != 0.5) {
                        alert("You have only " + CLBal + " Available CL");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                        return false;
                    }
                    if (CLBal === 0.5) {
                        document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
                        document.getElementById("<%= chkHalfDay.ClientID %>").checked = true;
                        var noDays = 0.5;
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = document.getElementById("<%= txtLeaveFrom.ClientID %>").value;
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = noDays;
                        document.getElementById("<%= ddlTiming.ClientID %>").disabled = false;
                        document.getElementById("<%= ddlTiming.ClientID %>").focus();

                    }               
                    if (days > 3 ) {
                        alert("Casual Leave Should be Max 3 Days");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                        return false;
                    }
                }
                if (leaveTypeID == 2) { 
                
                    var SLBal = Math.abs(rowSplitarr[1]) - Math.abs(rowSplitarr[6]);
                    if (SLBal < days && SLBal != 0.5) {
                        alert("You have only " + SLBal + " Available SL");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                        return false;
                    }
                    if (SLBal === 0.5) {
                        document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
                        document.getElementById("<%= chkHalfDay.ClientID %>").checked = true;
                        var noDays = 0.5;
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = document.getElementById("<%= txtLeaveFrom.ClientID %>").value;
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = noDays;
                        document.getElementById("<%= ddlTiming.ClientID %>").disabled = false;
                        document.getElementById("<%= ddlTiming.ClientID %>").focus();

                    }
                }
                if (leaveTypeID == 3) 
                {                    
                    var dateDiff = calculateDays(document.getElementById("<%= txtLeaveFrom.ClientID %>").value, document.getElementById("<%= txtDOJ.ClientID %>").value);
                    if (dateDiff < 365) {
                        alert("Privilege Leave can apply only after 1 year of Service");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveFrom.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false;
                    }
                    var ELBal = Math.abs(rowSplitarr[2]) - Math.abs(rowSplitarr[7]);
                    if (ELBal < days && ELBal != 0.5) {
                        alert("You have only " + ELBal + " Available PL");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                        return false;
                    }
                    if (ELBal === 0.5) {
                        document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
                        document.getElementById("<%= chkHalfDay.ClientID %>").checked = true;
                        var noDays = 0.5;
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = document.getElementById("<%= txtLeaveFrom.ClientID %>").value;
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = noDays;
                        document.getElementById("<%= ddlTiming.ClientID %>").disabled = false;
                        document.getElementById("<%= ddlTiming.ClientID %>").focus();

                    }                  
                }
                if (leaveTypeID == 7)
                {
                    var CMBal = Math.abs(rowSplitarr[3]) - Math.abs(rowSplitarr[8]);
                    if (CMBal < days) {
                        alert("You have only " + CMBal + " Available COMPENSATORY");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                        return false;
                    }
                    document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
                    document.getElementById("<%= chkHalfDay.ClientID %>").checked = false;
                }    
                if (leaveTypeID == 8)
                {
                    var RHBal = Math.abs(rowSplitarr[4]) - Math.abs(rowSplitarr[9]);
                    if (RHBal < days) {
                        alert("You have only " + RHBal + " Available RH");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                        return false;
                    }
                    document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
                    document.getElementById("<%= chkHalfDay.ClientID %>").checked = false;
                }
                if (DTL[1]==2 && (leaveTypeID == 4 || leaveTypeID == 9)) { 
                    var LeaveType=document.getElementById("<%= cmbLeaveCategory.ClientID %>").value;
                    var  Empcode=document.getElementById("<%= txtEmpCode.ClientID %>").value;
                    ToServer("1Ø" + LeaveType + "Ø" + Empcode, 1);
                } 
                 if(leaveTypeID == 11)
                {
                 
                    var dateDiff = calculateDays(document.getElementById("<%= txtLeaveFrom.ClientID %>").value, document.getElementById("<%= txtDOJ.ClientID %>").value);
                    if (dateDiff < 365) {
                        alert("Marriage Leave can apply only after 1 year of Service");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveFrom.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false;
                    }
               
                }      
            }
        }     
      }   
        function RequestOnClick() {
            document.getElementById("<%= hdnValue.ClientID %>").value = "";
            var LeaveTypeID = document.getElementById("<%= cmbLeaveCategory.ClientID %>").value;
            if (LeaveTypeID == -1)
            { alert("Select Leave Category"); document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbLeaveReason.ClientID %>").value == "-1")
            { alert("Select Reason"); document.getElementById("<%= cmbLeaveReason.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtLeaveFrom.ClientID %>").value == "")
            { alert("Select From Date"); document.getElementById("<%= txtLeaveFrom.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtLeaveTo.ClientID %>").value == "")
            { alert("Select To Date"); document.getElementById("<%= txtLeaveTo.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= chkHalfDay.ClientID %>").checked == true && document.getElementById("<%= ddlTiming.ClientID %>").value == 0)
            { alert("Select Session"); document.getElementById("<%= ddlTiming.ClientID %>").focus(); return false; }        
            var Days = document.getElementById("<%= txtLeaveDays.ClientID %>").value;
            if (LeaveTypeID == 2 && Days>3 && document.getElementById("<%= fup1.ClientID %>").value == "")
            { alert("Attach Medical & Fitness Certificate"); return false; }
            var Remarks=document.getElementById("<%= txtRemarks.ClientID %>").value;
            if (document.getElementById("<%= fup1.ClientID %>").value != "") {
                var fileName = document.getElementById("<%= fup1.ClientID %>").value;
                var file_extDelimiter = fileName.lastIndexOf(".") + 1;
                var file_ext = fileName.substring(file_extDelimiter).toLowerCase();
                if (file_ext != "png" && file_ext != "pdf" && file_ext != "jpeg" && file_ext != "jpg")
                { alert("Attach a png file"); return false; }
            }
            document.getElementById("<%= hdnValue.ClientID %>").value = document.getElementById("<%= txtEmpCode.ClientID %>").value + "~" + document.getElementById("<%= cmbLeaveCategory.ClientID %>").value + "~" + document.getElementById("<%= txtLeaveFrom.ClientID %>").value + "~" + document.getElementById("<%= txtLeaveTo.ClientID %>").value + "~" + document.getElementById("<%= txtLeaveDays.ClientID %>").value + "~" + document.getElementById("<%= cmbLeaveReason.ClientID %>").value + "~" + document.getElementById("<%= ddlTiming.ClientID %>").value+"~"+Remarks;
         
        }
        function HalfDayOnClick() {
            if (document.getElementById("<%= chkHalfDay.ClientID %>").checked == true) {
                var noDays = Math.abs(document.getElementById("<%= txtLeaveDays.ClientID %>").value) / 2;
                document.getElementById("<%= txtLeaveDays.ClientID %>").value = noDays;
                document.getElementById("<%= ddlTiming.ClientID %>").disabled = false;
                document.getElementById("<%= ddlTiming.ClientID %>").focus();
            }
            else {
                var frmDt = document.getElementById("<%= txtLeaveFrom.ClientID %>").value;
                var ToDt = document.getElementById("<%= txtLeaveTo.ClientID %>").value;
                var days = calculateDays(ToDt, frmDt);
                document.getElementById("<%= txtLeaveDays.ClientID %>").value = days;
                document.getElementById("<%= ddlTiming.ClientID %>").value = "0";
                document.getElementById("<%= ddlTiming.ClientID %>").disabled = true;
            }
        } 

        
       function AddDay(strDate, intNum) {
       
         
                    
            sdate = new Date(strDate);
            sdate.setDate(sdate.getDate() + intNum);
          
            return sdate;
        
           
        }

            function setStartDate(sender, args) {
          
             var EndDt =  new Date();
            
            var EndDt = AddDay(document.getElementById("<%= hdnDate.ClientID %>").value, 1); 
            
           
              var Today= new Date();
 
             if (Today.getDate() >20)
             {
                   
                     var curmonth21 =  new Date(Today.getFullYear(),Today.getMonth(),21);
                  
              
                    sender._startDate =  curmonth21;
                  
              }
              else
              { 
                
                var PrevMonthDate =  new Date();
                PrevMonthDate.setDate( Today.getDate() -Today.getDate());
                
                var Prevmonth21 =  new Date(PrevMonthDate.getFullYear(),PrevMonthDate.getMonth(),21);
              
                    sender._startDate =  Prevmonth21;

              }
                
            //// sender._endDate=EndDt;
            
        }

    </script>
</head>
</html>
<br />
<div style="text-align:center;" align="center">
<table style="text-align:center;width:80%; margin: 0px auto; ">

<tr >
        <td colspan="4" >
            <asp:Panel ID="pnLeaveDtl" runat="server" Width="100%" >
            </asp:Panel> 
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <br />
        </td>
    </tr>

<tr>
        <td style="width:12%;text-align:left; ">
            Employee Code</td>
        <td style="width:56% ;">
            <asp:TextBox ID="txtEmpCode" class="ReadOnlyTextBox" runat="server" Width="40%" 
                MaxLength="5" ReadOnly="True"></asp:TextBox>
        </td>
        <td style="width:12%;text-align:left; ">
            Name</td>
        <td style="width:20%;">
            <asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="100%" 
                MaxLength="50" ReadOnly="True"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width:12%;text-align:left; ">
            Branch/Department</td>
        <td style="width:56%;">
            <asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" runat="server"  Width="40%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
        </td>
        <td style="width:12%;text-align:left; ">
            Date Of Join</td>
        <td style="width:20%;">
            <asp:TextBox ID="txtDOJ" class="ReadOnlyTextBox" runat="server" Width="100%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
        </td>
    </tr>
    


    
   
    
    <tr>
        <td style="width:12%;text-align:left; ">
            Leave Category</td>
        <td style="width:56%">
            <asp:DropDownList ID="cmbLeaveCategory" class="NormalText" runat="server" Font-Names="Cambria" Width="40%" 
                 ForeColor="Black">
            </asp:DropDownList>
        </td>
        <td style="width:12%;text-align:left; ">
            Leave Reason</td>
        <td style="width:20%">
            <asp:DropDownList ID="cmbLeaveReason" class="NormalText" runat="server" Font-Names="Cambria" 
                Width="100%" ForeColor="Black">
            </asp:DropDownList>
        </td>
    </tr>
    
    <tr>
        <td style="width:12%;text-align:left; ">
            Leave From</td>
        <td style="width:56%">
            <asp:TextBox ID="txtLeaveFrom" class="NormalText" runat="server" Width="40%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CEFromDate" runat="server"   Enabled="True" EnabledOnClient="true" OnClientShowing="setStartDate" TargetControlID="txtLeaveFrom" Format="dd MMM yyyy"></asp:CalendarExtender>
        </td>
        <td style="width:12%;text-align:left; ">
            Leave To</td>
        <td style="width:20%">
            <asp:TextBox ID="txtLeaveTo" class="NormalText" runat="server" Width="100%" 
                MaxLength="100" ReadOnly="true"></asp:TextBox>
            <asp:CalendarExtender ID="CETo" runat="server" Enabled="True"  EnabledOnClient="true" OnClientShowing="setStartDate"  Format="dd MMM yyyy" TargetControlID="txtLeaveTo">
            </asp:CalendarExtender>
        </td>
    </tr>
    <tr>
        <td style="width:12%;text-align:left; ">
            Leave Days</td>
        <td style="width:56%">
            <asp:TextBox ID="txtLeaveDays" class="ReadOnlyTextBox" runat="server" Width="20%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
            &nbsp;&nbsp;<asp:CheckBox ID="chkHalfDay" runat="server" Font-Names="Times New Roman" 
                Font-Size="10pt" Text="Half Day" Width="20%" style="display:none;" />
        </td>
        <td style="width:12%;text-align:left; ">
            Session</td>
            
        <td style="width:20%">
            <form enctype="multipart/form-data" action="/upload/image" method="post">
            </form>
            <asp:DropDownList ID="ddlTiming" runat="server" Font-Names="Times New Roman" 
                Font-Size="10pt" Width="70%">
                <asp:ListItem Value="0">SELECT</asp:ListItem>
                <asp:ListItem Value="1">FORENOON</asp:ListItem>
                <asp:ListItem Value="2">AFTERNOON</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="width:12%;text-align:left; ">
            Remarks</td>
        <td style="width:56%">
            <asp:TextBox ID="txtRemarks" class="NormalTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
        </td>
        <td style="width:12%;text-align:left; ">
            Attachment If any</td>
            
        <td style="width:20%">
            <input id="fup1" type="file" runat="server" 
                style="font-family: Cambria; font-size: 10.5pt" /></td>
    </tr>
    <tr><td style="text-align:center;" colspan="4">&nbsp;</td></tr>
    <tr>
        <td style="text-align:center;" colspan="4">
            &nbsp;
           <asp:Button 
                ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" style="cursor:pointer;"
                 Width="5%" /> 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 5%; " 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
            <asp:HiddenField 
                ID="hdnValue" runat="server" /><asp:HiddenField 
                ID="hdnImg" runat="server" /><asp:HiddenField ID="hdnData" 
                runat="server" />
                <asp:HiddenField ID="hdnDate" runat="server" /> 
                
        </td>
    </tr>
</table>    
</div>
<br /><br />
</asp:Content>

