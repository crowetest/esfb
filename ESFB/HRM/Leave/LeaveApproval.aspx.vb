﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class LeaveApproval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim LV As New Leave
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 17) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Leave Approval"
            Me.cmd_view.Attributes.Add("onclick", "return viewattachment()")
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            DT = LV.Getexist_Leave_request(CInt(Session("UserID")), 0)
            If DT Is Nothing Then
            Else
                GF.ComboFill(cmbModule, DT, 0, 1)
            End If
            Me.cmbModule.Attributes.Add("onchange", "return RequestOnchange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", " window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        ' RequestID + "Ø" + status + "Ø" + remarks + "Ø" + days + "Ø" + approvestatus;
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim SelectedUserID As Integer = CInt(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        If RequestID = 1 Then
            Dim leaveid As Integer = CInt(Data(1))
            Dim status As Integer = CInt(Data(2))
            Dim remarks As String = CStr(Data(3))
            Dim Days As Integer = CInt(Data(4))
            Dim Approvestatus As Integer = CInt(Data(5))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@Remarks", SqlDbType.VarChar, 5000)
                Params(0).Value = remarks
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@Status_ID", SqlDbType.Int)
                Params(4).Value = status
                Params(5) = New SqlParameter("@approveStatus_ID", SqlDbType.Int)
                Params(5).Value = Approvestatus
                Params(6) = New SqlParameter("@leaverequestid", SqlDbType.Int)
                Params(6).Value = leaveid
                Params(7) = New SqlParameter("@TrNo", SqlDbType.Int)
                Params(7).Value = 2
                DB.ExecuteNonQuery("SP_Leave", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
                If ErrorFlag = 0 Then
                    Dim QRY As String = ""
                    QRY = "select c.Emp_Name +'['+convert(varchar,a.Emp_Code) +'] - From '+convert(varchar,a.Leave_From) +'To '+convert(varchar,a.Leave_To)+'|'+convert(varchar,a.Emp_Code)  from  EMP_LEAVE_REQUEST a,EMP_LEave_type b,emp_master c where a.leave_type=b.type_id and a.Emp_Code=c.Emp_Code and a.request_id=" & leaveid & ""
                    DT = DB.ExecuteDataSet(QRY).Tables(0)
                    Dim Employee() = Split(CStr(DT.Rows(0).Item(0)), "|")
                    Dim ToAddress As String = "hena.ek@esafmicrofin.com"
                    Dim EmpName As String = ""
                    Dim tt As String = SM.GetEmailAddress(CInt(Employee(1)), 1)
                    Dim Val = Split(tt, "^")
                    If Val(0).ToString <> "" Then
                        ToAddress = Val(0)
                    End If
                    If Val(1).ToString <> "" Then
                        EmpName = Val(1).ToString
                    End If
                    Dim ccAddress As String = ""
                    'General.GetEmailAddress(EmpID, 3);
                    If ccAddress.Trim() <> "" Then
                        ccAddress += ","
                    End If
                    Dim StrVal As String = ""
                    If status = 1 Then
                        StrVal = "Approved"
                    ElseIf status = 2 Then
                        StrVal = "Recommended"
                    Else
                        StrVal = "Rejected"
                    End If
                    Dim Content As String = vbLf & vbLf & " Dear " + EmpName + ", " & vbLf & vbLf
                    Content += "Leave Request  (" + Employee(1).ToString + ")" + StrVal + "   by " + CStr(Session("UserName")) + "[" + CStr(Session("UserID")) + "]" & vbLf & vbLf
                    Content = Content & Convert.ToString(vbLf & vbLf & " With Regards" & vbLf & vbLf & "HR Team" & vbLf & "Head, HR")
                    SM.SendMail(ToAddress, "Leave Approval/Rejection", Content, ccAddress & Convert.ToString("nisha.rathnan@esafmicrofin.com"))
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf RequestID = 2 Then
            DTTS = LV.Get_History_Leave_dtls(SelectedUserID)
            hid_dtls.Value = ""
            hid_sum.Value = ""
            Dim strTS As New StringBuilder
            Dim strTSum As New StringBuilder
            Dim dr As DataRow
            'requestdate,leavetype,leavefrom,leaveto,leavedays,reason,status
            For Each dr In DTTS.Rows
                strTS.Append("¥")
                strTS.Append(dr.Item(0).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(1).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(2).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(3).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(4).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(5).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(6).ToString())
                strTS.Append("µ")
            Next
            DTTS = LV.Get_History_Leave_summary(SelectedUserID)
            'empcode,empname,cl,clcount,sl,slcount,pl,plcount,other,othercnt
            For Each dr In DTTS.Rows
                strTSum.Append("¥")
                strTSum.Append(dr.Item(0).ToString())
                strTSum.Append("µ")
                strTSum.Append(dr.Item(1).ToString())
                strTSum.Append("µ")
                strTSum.Append(dr.Item(2).ToString())
                strTSum.Append("µ")
                strTSum.Append(dr.Item(3).ToString())
                strTSum.Append("µ")
                strTSum.Append(dr.Item(4).ToString())
                strTSum.Append("µ")
                strTSum.Append(dr.Item(5).ToString())
                strTSum.Append("µ")
                strTSum.Append(dr.Item(6).ToString())
                strTSum.Append("µ")
                strTSum.Append(dr.Item(7).ToString())
                strTSum.Append("µ")
                strTSum.Append(dr.Item(8).ToString())
                strTSum.Append("µ")
                strTSum.Append(dr.Item(9).ToString())
                strTSum.Append("µ")
            Next
            DTTS = GF.GetQueryResult(5, 0, CInt(Data(2)))
            Dim AbsentDays As Double = 1.0
            If (DTTS.Rows.Count > 0) Then
                AbsentDays = CDbl(DTTS.Rows(0)(0))
            End If
            CallBackReturn = Mid(strTS.ToString(), 2) + "^" + Mid(strTSum.ToString(), 2) + "^" + AbsentDays.ToString()
        End If
    End Sub
#End Region
End Class
