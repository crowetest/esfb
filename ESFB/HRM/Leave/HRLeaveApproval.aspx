﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="HRLeaveApproval.aspx.vb" Inherits="HRLeaveApproval" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var EMPID = -1;
        var RequestID=-1
        function RequestOnchange() {
            if (document.getElementById("<%= cmbModule.ClientID %>").value != "-1") {
                var Data = document.getElementById("<%= cmbModule.ClientID %>").value.split("Ø");
                EMPID = Data[5];
                RequestID = Data[0];
                document.getElementById("<%= txtStartDt.ClientID %>").value = Data[2];
                document.getElementById("<%= txtEndDt.ClientID %>").value = Data[3];
                document.getElementById("<%= txtDays.ClientID %>").value = Data[1];
                document.getElementById("<%= txtReason.ClientID %>").value = Data[4];
                document.getElementById("<%= txtRecommendedRemarks.ClientID %>").value=Data[7];
                document.getElementById("<%= txtRecommendedBy.ClientID %>").value=Data[8];
                if (Data[6] == "0")
                    document.getElementById("<%= cmd_view.ClientID %>").style.display = "none";
                else
                    document.getElementById("<%= cmd_view.ClientID %>").style.display = '';
                document.getElementById("<%= txtRemarks.ClientID %>").focus();
                document.getElementById("btnApprove").value = "APPROVE";
                document.getElementById("<%= hid_approve.ClientID %>").value = 0;              
                document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = "";
                document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
                viewrpt();
            }
            else {
                EMPID =-1;
                RequestID =-1;
                document.getElementById("<%= txtStartDt.ClientID %>").value = "";
                document.getElementById("<%= txtEndDt.ClientID %>").value = "";
                document.getElementById("<%= txtDays.ClientID %>").value = "";
                document.getElementById("<%= txtAbsentDays.ClientID %>").value = "";
                document.getElementById("<%= txtReason.ClientID %>").value = "";
                document.getElementById("<%= txtRecommendedRemarks.ClientID %>").value="";
                document.getElementById("<%= txtRecommendedBy.ClientID %>").value="";
                document.getElementById("<%= cmd_view.ClientID %>").style.display = "none";
                document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = "none";
            }           
        }

        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        function btnReject_onclick() {
            if (document.getElementById("<%= cmbModule.ClientID %>").value == "-1") {
                alert("Select Leave Request");
                document.getElementById("<%= cmbModule.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtremarks.ClientID %>").value == "") {
                alert("Enter Remarks");
                document.getElementById("<%= txtremarks.ClientID %>").focus();
                return false;
            }
            var ReqDtl = document.getElementById("<%= cmbModule.ClientID %>").value.split("Ø");
            var RequestID = ReqDtl[0];
            var remarks = document.getElementById("<%= txtremarks.ClientID %>").value;
            var status = 5;
            var approvestatus = document.getElementById("<%= hid_approve.ClientID %>").value;
            var days = document.getElementById("<%= txtDays.ClientID %>").value;
            var Data = "1Ø" + RequestID + "Ø" + status + "Ø" + remarks + "Ø" + days + "Ø" + approvestatus;
            ToServer(Data, 1);
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("HRLeaveApproval.aspx", "_self");
            }
            else if (context == 2) {
                var Data = arg.split("^");
                document.getElementById("<%= hid_dtls.ClientID %>").value = Data[0];
                document.getElementById("<%= hid_sum.ClientID %>").value = Data[1];                
                document.getElementById("<%= txtAbsentDays.ClientID %>").value ="Absent Days: "+ Data[2];
                if( document.getElementById("<%= txtDays.ClientID %>").value<Data[2])                                 
                    document.getElementById("<%= txtAbsentDays.ClientID %>").style.color="red";
                else
                    document.getElementById("<%= txtAbsentDays.ClientID %>").style.color="black";

                table_fill();
            }

        }
        function table_fill() {

            if (document.getElementById("<%= hid_sum.ClientID %>").value == "") {
                document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = "";
                document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = "none";
                alert("No Other Leaves!");
                return;
            }
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
             rowsum = document.getElementById("<%= hid_sum.ClientID %>").value.split("¥");

             for (m = 0; m <= rowsum.length - 1; m++) {
                 colsum = rowsum[m].split("µ");
                 tab += "<div style='margin: 0px auto; width:80%;border-color:#A34747; border-style:solid;'>";
                 tab += "<div style='text-align:center;width:100%; height:18px; margin: 0px auto;'  align='center' class=mainhead><span style='color:#FFF;'>LEAVE HISTORY</span></div>"
                 tab += "<div style='text-align:center;width:100%; margin: 0px auto;'  align='center'>"
                 tab += "<span>" + colsum[2] + " : " + colsum[3] + "</span>&nbsp;&nbsp;&nbsp;<span>" + colsum[4] + " : " + colsum[5] + "</span>";
                 tab += "&nbsp;&nbsp;&nbsp;<span>" + colsum[6] + " : " + colsum[7] + "</span>&nbsp;&nbsp;&nbsp;<span>" + colsum[8] + " : " + colsum[9] + "</span>";

                 tab += "</div>";
                
             }
             tab += "<div style='width:100%; height:170px;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
             tab += "<tr class=mainhead>";
            tab += "<td style='width:5%;text-align:center'>Sl No</td>";
            tab += "<td style='width:10%;text-align:center' >Request Date</td>";
            tab += "<td style='width:25%;text-align:left' >Leave Type</td>";
            tab += "<td style='width:10%;text-align:center'>Leave From</td>";
            tab += "<td style='width:10%;text-align:center'>Leave To</td>";
            tab += "<td style='width:10%;text-align:center'>Leave Days</td>";
            tab += "<td style='width:20%;text-align:left'>Reason</td>";
            tab += "<td style='width:10%;text-align:left'>Status</td>";
            tab += "</tr>";

            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
           
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");

                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr class=sub_first>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class=sub_second>";
                }
                i = n + 1;
                //L.clientid,C.name,L.disb_date,L.loanamt,L.loanid,L.accountno

                tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                tab += "<td style='width:10%;text-align:center' >" + col[0] + "</td>";
                tab += "<td style='width:25%;text-align:left' >" + col[1] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[2] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[3] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[4] + "</td>";
                tab += "<td style='width:20%;text-align:left'>" + col[5] + "</td>";
                tab += "<td style='width:10%;text-align:left'>" + col[6] + "</td>";

             
                tab += "</tr>";
            }
            tab += "</table><div><div>";
            //alert(tab);
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }
        function btnApprove_onclick() {
            if (document.getElementById("<%= cmbModule.ClientID %>").value == "-1") {
                alert("Select Leave Request");
                document.getElementById("<%= cmbModule.ClientID %>").focus();
                return false;
            }

            var ReqDtl = document.getElementById("<%= cmbModule.ClientID %>").value.split("Ø");
            var RequestID = ReqDtl[0];
            var status = 1;         
            var approvestatus = document.getElementById("<%= hid_approve.ClientID %>").value;
            var remarks = document.getElementById("<%= txtRemarks.ClientID %>").value;
            var days = document.getElementById("<%= txtDays.ClientID %>").value;
            var Data = "1Ø" + RequestID + "Ø" + status + "Ø" + remarks + "Ø" + days + "Ø" + approvestatus;
            
            ToServer(Data, 1);
        }
        function viewrpt() {

            if (EMPID != -1) {
                var Data = "2Ø" + EMPID+"Ø"+RequestID;
                ToServer(Data, 2);
            }
        }
        function viewattachment() {
            var ReqDtl = document.getElementById("<%= cmbModule.ClientID %>").value.split("Ø");
            var RequestID = ReqDtl[0];
            if (RequestID != -1)
                window.open("ShowAttachment.aspx?RequestID=" + btoa(RequestID));
            return false;
        }
        function window_onload() {
            document.getElementById("<%= cmd_view.ClientID %>").style.display = "none";
        }

    </script>
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
</script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_sum" runat="server" />
<br />

    <table class="style1" style="width:100%">
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12%; text-align:left; ">
                Select&nbsp; Leave</td>
            <td style="width:63%;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbModule" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12%; text-align:left; ">
                Leave From</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtStartDt" class="ReadOnlyTextBox" readonly="True" runat="server" Width="20%" 
                    MaxLength="100"></asp:TextBox>
            </td>
          </tr>
          <tr>
          <td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">
                Leave To</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEndDt" class="ReadOnlyTextBox" readonly="True" runat="server" Width="20%" 
                    MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">
                Leave Days</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDays" class="ReadOnlyTextBox" readonly="True" runat="server" Width="20%" 
                    MaxLength="100"></asp:TextBox>
                &nbsp; &nbsp;<asp:TextBox ID="txtAbsentDays" style="font-family:Cambria;  font-size:10pt; text-transform:uppercase;" readonly="True" 
                    runat="server" Width="40%" 
                    MaxLength="100" BorderStyle="None"></asp:TextBox>
            </td>
         </tr>
         <tr>
         <td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">
                Leave Reason</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtReason" class="ReadOnlyTextBox" readonly="True" runat="server" Width="20%" 
                    MaxLength="100"></asp:TextBox> <asp:ImageButton ID="cmd_view" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle" ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" />
                &nbsp; &nbsp; 
            </td>
        </tr>
      
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">
                Recommended By</td>
            <td style="width:63%;text-align:left; ">
                
                &nbsp; &nbsp;<asp:TextBox ID="txtRecommendedBy" class="ReadOnlyTextBox" readonly="True" 
                    runat="server" Width="20%" 
                    MaxLength="100"></asp:TextBox>
                
            </td>
        </tr>
        <tr>
        <td style="width:25%;">&nbsp;</td>
            <td style="width:12%; text-align:left;">
                Recommended Remarks</td>
            <td style="width:63%;text-align:left; ">
                
                &nbsp; &nbsp;<asp:TextBox ID="txtRecommendedRemarks" class="ReadOnlyTextBox"
                runat="server" Width="60%" 
                MaxLength="100" ReadOnly="True" ></asp:TextBox>
            
            </td>
        </tr>
          <tr> 
        <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Remarks</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtRemarks" class="NormalText"
                runat="server" Width="60%" 
                MaxLength="100" ></asp:TextBox>
            
            </td>
       </tr>
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3"><br />
                <asp:HiddenField ID="hid_approve" runat="server" />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 8%;" 
                    type="button" value="APPROVE" onclick="return btnApprove_onclick()" />&nbsp;
                <input id="btnReject" style="font-family: cambria; cursor: pointer; width:8%;" 
                    type="button" value="REJECT" onclick="return btnReject_onclick()" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

