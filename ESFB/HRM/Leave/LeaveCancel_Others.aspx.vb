﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class LeaveCancel_Others
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim FormID As Integer = 1
    Dim LV As New Leave
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 23) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Leave Cancellation"
            Me.cmbEmployee.Attributes.Add("onchange", "EmployeeOnChange()")
            Me.cmbLeave.Attributes.Add("onchange", "LeaveOnChange()")
            '--//---------- Script Registrations -----------//--]
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- For Call Back ---//
            If (Not IsNothing(Request.QueryString.Get("ID"))) Then
                FormID = CInt((Request.QueryString.Get("ID")))
            End If
            Me.hdnFormID.Value = FormID.ToString()
            If (FormID = 1) Then 'cancel self
                GF.ComboFill(cmbEmployee, DB.ExecuteDataSet("Select EMP_cODE,LEFT(CAST(EMP_cODE AS VARCHAR(6))+SPACE(120),120)+'|'+Emp_Name from Emp_Master where Emp_code=" + Session("UserID").ToString() + " and Status_ID=1").Tables(0), 0, 1)
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "EmployeeOnChange();", True)
            Else 'cancel other
                GF.ComboFill(cmbEmployee, DB.ExecuteDataSet("Select -1 as Emp_Code,'--------Select--------' as Emp_Name Union All Select EMP_cODE,LEFT(CAST(EMP_cODE AS VARCHAR(6))+SPACE(120),120)+'    |    '+Emp_Name from Emp_Master where Reporting_To=" + Session("UserID").ToString() + " and Status_ID=1").Tables(0), 0, 1)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim Leave As New Leave
        If (CInt(Data(0)) = 1) Then
            Dim DT As New DataTable
            Dim GN As New GeneralFunctions
            DT = GN.GetUserDetails(CInt(Data(1))).Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø"
                DT = Leave.getLeavesForCancellation(CInt(Data(1)), FormID).Tables(0)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
            Else
                CallBackReturn = "ØØ"
            End If

        ElseIf (CInt(Data(0)) = 2) Then
            CallBackReturn = Leave.LeaveCancel(CInt(Data(1)), Data(2), CInt(Session("UserID")), CInt(Data(3)))
        ElseIf (CInt(Data(0)) = 3) Then
            Dim SelectedUserID As Integer = CInt(Data(1))
            DTTS = LV.Get_History_Leave_dtls(SelectedUserID, 1)
            hid_dtls.Value = ""
            hid_sum.Value = ""
            Dim strTS As New StringBuilder
            Dim strTSum As New StringBuilder
            Dim dr As DataRow
            'requestdate,leavetype,leavefrom,leaveto,leavedays,reason,status
            If DTTS Is Nothing = False Then
                For Each dr In DTTS.Rows
                    strTS.Append("¥")
                    strTS.Append(dr(0).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr(1).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr(2).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr(3).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr(4).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr(5).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr(6).ToString())
                    strTS.Append("µ")
                Next
            End If
            If DTTS Is Nothing = False Then
                DTTS = LV.Get_History_Leave_summary(SelectedUserID)
                'empcode,empname,cl,clcount,sl,slcount,pl,plcount,other,othercnt
                For Each dr In DTTS.Rows
                    strTSum.Append("¥")
                    strTSum.Append(dr(0).ToString())
                    strTSum.Append("µ")
                    strTSum.Append(dr(1).ToString())
                    strTSum.Append("µ")
                    strTSum.Append(dr(2).ToString())
                    strTSum.Append("µ")
                    strTSum.Append(dr(3).ToString())
                    strTSum.Append("µ")
                    strTSum.Append(dr(4).ToString())
                    strTSum.Append("µ")
                    strTSum.Append(dr(5).ToString())
                    strTSum.Append("µ")
                    strTSum.Append(dr(6).ToString())
                    strTSum.Append("µ")
                    strTSum.Append(dr(7).ToString())
                    strTSum.Append("µ")
                    strTSum.Append(dr(8).ToString())
                    strTSum.Append("µ")
                    strTSum.Append(dr(9).ToString())
                    strTSum.Append("µ")
                Next
            End If
            CallBackReturn = Mid(strTS.ToString(), 2) + "^" + Mid(strTSum.ToString(), 2)
            End If
    End Sub
#End Region
End Class
