﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class LeaveEntry
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim Leave As New Leave
    Dim GN As New GeneralFunctions
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 317) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Leave Application"
            ''ESI FLAG
            'DT = DB.ExecuteDataSet("select esi_flag from emp_master where emp_code=" & CInt(txtEmpCode.Text) & "").Tables(0)
            'If DT.Rows.Count <> 0 Then
            GN.ComboFill(Me.cmbLeaveCategory, Leave.GetLeaveType(1).Tables(0), 0, 1)
            'End If
            GN.ComboFill(Me.cmbLeaveReason, Leave.GetLeaveReason().Tables(0), 0, 1)
            ddlTiming.SelectedIndex = 0
            ddlTiming.Enabled = False
            Me.txtEmpCode.Attributes.Add("onchange", "return CodeOnChange();")
            Me.txtLeaveFrom.Attributes.Add("onchange", "return DateOnChange();")
            Me.txtLeaveTo.Attributes.Add("onchange", "return DateOnChange();")
            cmbLeaveCategory.Attributes.Add("onchange", "return DateOnChange();")
            chkHalfDay.Attributes.Add("onclick", "return HalfDayOnClick()")
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
        Me.txtEmpCode.Attributes.Add("onchange", "return CodeOnChange()")
        'initializeControls()
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Save"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            hdnValue.Value += "~" + Session("UserID").ToString()
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim myFile As HttpPostedFile = fup1.PostedFile
            Dim nFileLen As Integer = myFile.ContentLength
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                AttachImg = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, nFileLen)
            End If
            Dim message As New String(CChar(" "), 1000)
            Dim arr As String() = hdnValue.Value.Split(CChar("~"))
            Try
                Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TrNo", 4),
                                                                          New System.Data.SqlClient.SqlParameter("@EmpCode", Integer.Parse(arr(0))),
                                                                          New System.Data.SqlClient.SqlParameter("@LeaveType", Integer.Parse(arr(1))),
                                                                          New System.Data.SqlClient.SqlParameter("@FromDt", Convert.ToDateTime(arr(2))),
                                                                          New System.Data.SqlClient.SqlParameter("@ToDt", Convert.ToDateTime(arr(3))),
                                                                          New System.Data.SqlClient.SqlParameter("@Days", Convert.ToDouble(arr(4))),
                                                                          New System.Data.SqlClient.SqlParameter("@ReasonID", Integer.Parse(arr(5))),
                                                                          New System.Data.SqlClient.SqlParameter("@Remarks", arr(7)),
                                                                          New System.Data.SqlClient.SqlParameter("@UserID", CInt(arr(8))),
                                                                          New System.Data.SqlClient.SqlParameter("@Attachment", AttachImg),
                                                                          New System.Data.SqlClient.SqlParameter("@ContentType", ContentType),
                                                                          New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0),
                                                                          New System.Data.SqlClient.SqlParameter("@OutputMessage", message),
                                                                          New System.Data.SqlClient.SqlParameter("@isHD", CInt(arr(6)))}
                Parameters(11).Direction = ParameterDirection.InputOutput
                Parameters(12).Direction = ParameterDirection.InputOutput
                DB.ExecuteNonQuery("SP_Leave", Parameters)
                message = Parameters(11).Value.ToString() + "Ø" + Parameters(12).Value.ToString()
            Catch ex As Exception
                message = ex.Message
                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try
            Dim result() = message.Split(CChar("Ø"))

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("alert('" + result(1) + "');")
            cl_script1.Append("window.open('HRLeaveEntry.aspx','_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Emp Details
                Dim EmpCode As Integer = CInt(Data(1))
                Dim DT As New DataTable
                DT = DB.ExecuteDataSet("Select emp_code,emp_name,Branch_name,Department_name,Designation_name,Gender_ID,CONVERT(VARCHAR(11),Date_Of_Join,106),Branch_ID,Emp_status,ISNULL(CONVERT(VARCHAR(11),Releiving_Date,106),'') from Emp_List where EMp_Code=" + EmpCode.ToString() + "").Tables(0)
                If (DT.Rows.Count > 0) Then
                    Dim Branch As String
                    Branch = DT.Rows(0)(2).ToString()
                    If CInt(DT.Rows(0)(7)) = 0 Then
                        Branch = DT.Rows(0)(3).ToString()
                    End If
                    CallBackReturn += DT.Rows(0)(0).ToString() + "¶" + DT.Rows(0)(1).ToString() + "¶" + Branch + "¶"
                    Dim DOJ As New Date
                    DOJ = CDate(DT.Rows(0)(6))
                    Dim YStartDt As New DateTime(CDate(Session("TraDt")).Year, 1, 1)
                    CallBackReturn += DOJ.ToString("dd MMM yyyy") + "¶"
                    CallBackReturn += DOJ.ToString + "¶"
                    Dim DOR As New Date
                    If (CStr(DT.Rows(0)(9)) = "") Then
                        DOR = New DateTime(CDate(Session("TraDt")).Year, 12, 31)
                    Else
                        DOR = CDate(DT.Rows(0)(9))
                    End If
                    CallBackReturn += DOR.ToString
                    CallBackReturn += "Æ"
                    CallBackReturn += Leave.GetAvailLeaves(EmpCode)
                    CallBackReturn += "ử" + DT.Rows(0)(5).ToString()
                End If
            Case 2
                Dim Data1() As String = eventArgument.Split(CChar("Ø"))
                If CInt(Data1(0)) = 1 Then
                    CallBackReturn = Leave.Materinity_Leave_Details(CInt(Data1(1)), CInt(Data1(2)), 0)
                End If
        End Select
    End Sub
#End Region
End Class
