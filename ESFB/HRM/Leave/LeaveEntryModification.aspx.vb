﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class LeaveEntryModification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim Leave As New Leave
    Dim GN As New GeneralFunctions
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 22) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Leave Cancel/Modification"
            Me.txtLeaveFrom.Attributes.Add("onchange", "return DateOnChange();")
            Me.txtLeaveTo.Attributes.Add("onchange", "return DateOnChange();")
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            cmbLeaveCategory.Attributes.Add("onchange", "return DateOnChange();")
            chkHalfDay.Attributes.Add("onclick", "return HalfDayOnClick()")
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            initializeControls()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
    Private Sub initializeControls()
        Me.hdnData.Value = Leave.GetAvailLeaves(CInt(Session("UserID")))
        Dim DT As New DataTable
        DT = GN.GetUserDetails(CInt(Session("UserID"))).Tables(0)
        If (DT.Rows.Count > 0) Then
            Dim Branch As String
            Branch = DT.Rows(0)(2).ToString()
            If CInt(DT.Rows(0)(7)) = 0 Then
                Branch = DT.Rows(0)(3).ToString()
            End If
            txtEmpCode.Text = DT.Rows(0)(0).ToString()
            txtName.Text = DT.Rows(0)(1).ToString()
            txtBranch.Text = Branch
            Dim DOJ As New Date
            DOJ = CDate(DT.Rows(0)(6))
            Dim YStartDt As New DateTime(CDate(Session("TraDt")).Year, 1, 1)
            txtDOJ.Text = DOJ.ToString("dd MMM yyyy")
            If (DOJ < YStartDt) Then
                DOJ = YStartDt
            End If
            Me.CEFromDate.StartDate = DOJ
            Me.CETo.StartDate = DOJ
            Me.CEFromDate.EndDate = New DateTime(CDate(Session("TraDt")).Year, 12, 31)
            Me.CETo.EndDate = New DateTime(CDate(Session("TraDt")).Year, 12, 31)
            Me.hdnData.Value += "ử" + DT.Rows(0)(5).ToString()
            Dim DTTS As New DataTable
            Dim LV As New Leave
            DTTS = Leave.Get_Leave_For_Modification(CInt(Session("UserID")))
            Dim strTS As New StringBuilder
            Dim strTSum As New StringBuilder
            Dim dr As DataRow
            'requestdate,leavetype,leavefrom,leaveto,leavedays,reason,status
            For Each dr In DTTS.Rows
                strTS.Append("¥")
                strTS.Append(dr.Item(0).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(1).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(2).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(3).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(4).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(5).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(6).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(7).ToString())
            Next
            Me.hdnData.Value += "ử" + strTS.ToString()
            DT = DB.ExecuteDataSet("select esi_flag from emp_master where emp_code=" & CInt(txtEmpCode.Text) & "").Tables(0)
            If DT.Rows.Count <> 0 Then
                GN.ComboFill(Me.cmbLeaveCategory, Leave.GetLeaveType(CInt(DT.Rows(0)(0))).Tables(0), 0, 1)
            End If
            GN.ComboFill(Me.cmbLeaveReason, Leave.GetLeaveReason().Tables(0), 0, 1)
            ddlTiming.SelectedIndex = 0
            ddlTiming.Enabled = False
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "showDetails();", True)
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            hdnValue.Value += "~" + Session("UserID").ToString()
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim myFile As HttpPostedFile = fup1.PostedFile
            Dim nFileLen As Integer = myFile.ContentLength
            If nFileLen > 0 Then
                ContentType = myFile.ContentType
                AttachImg = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, nFileLen)
            End If
            Dim Result As String
            Try
                Result = Leave.LeaveModification(hdnValue.Value, AttachImg, ContentType)
            Catch
                Result = "1ØError"
            End Try
            Dim arr() = Result.Split(CChar("Ø"))
            If CDbl(arr(0)) = 0 Then
                initializeControls()
            End If
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + arr(1) + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim Leave As New Leave
        If CInt(Data(0)) = 1 Then
            CallBackReturn = Leave.Materinity_Leave_Details(CInt(Data(1)), CInt(Data(2)), CInt(Data(3)))
        ElseIf CInt(Data(0)) = 2 Then
            CallBackReturn = Leave.LeaveCancel(CInt(Data(1)), Data(2), CInt(Session("UserID")), CInt(Data(3)))
        End If
    End Sub
End Class
