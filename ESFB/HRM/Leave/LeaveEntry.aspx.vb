﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class LeaveEntry
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim Leave As New Leave
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 16) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Leave Application"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.txtLeaveFrom.Attributes.Add("onchange", "return DateOnChange();")
            Me.txtLeaveTo.Attributes.Add("onchange", "return DateOnChange();")
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            cmbLeaveCategory.Attributes.Add("onchange", "return DateOnChange();")
            Me.cmbLeaveCategory.Attributes.Add("onchange", "return CatagoryOnChange();")
            chkHalfDay.Attributes.Add("onclick", "return HalfDayOnClick()")
            hdnDate.Value = CDate(Session("TraDt")).ToString("dd MMM yyyy")
            initializeControls()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            CallBackReturn = Leave.Materinity_Leave_Details(CInt(Data(1)), CInt(Data(2)), 0)
        End If
    End Sub
#End Region
    Private Sub initializeControls()
        Me.hdnData.Value = Leave.GetAvailLeaves(CInt(Session("UserID")))
        Dim DT As New DataTable
        DT = GN.GetUserDetails(CInt(Session("UserID"))).Tables(0)
        If (DT.Rows.Count > 0) Then
            Dim Branch As String
            Branch = DT.Rows(0)(2).ToString()
            If CInt(DT.Rows(0)(7)) = 0 Then
                Branch = DT.Rows(0)(3).ToString()
            End If
            txtEmpCode.Text = DT.Rows(0)(0).ToString()
            txtName.Text = DT.Rows(0)(1).ToString()
            txtBranch.Text = Branch
            Dim DOJ As New Date
            DOJ = CDate(DT.Rows(0)(6))
            Dim YStartDt As New DateTime(CDate(Session("TraDt")).Year, 1, 1)
            txtDOJ.Text = DOJ.ToString("dd MMM yyyy")
            If Month(YStartDt) = 1 Then
                YStartDt = DateAdd(DateInterval.Month, -1, YStartDt)
            End If
            If (DOJ < YStartDt) Then
                DOJ = YStartDt
            End If
            Me.CEFromDate.StartDate = DOJ
            Me.CETo.StartDate = DOJ
            Me.CEFromDate.EndDate = New DateTime(CDate(Session("TraDt")).Year, 12, 31)
            Me.CETo.EndDate = New DateTime(CDate(Session("TraDt")).Year, 12, 31)
            Me.hdnData.Value += "ử" + DT.Rows(0)(5).ToString()

            Dim DTT As New DataTable
            DTT = DB.ExecuteDataSet("select esi_flag from emp_master where emp_code=" & CInt(txtEmpCode.Text) & "").Tables(0)
            If DTT.Rows.Count <> 0 Then
                GN.ComboFill(Me.cmbLeaveCategory, Leave.GetLeaveType(CInt(DTT.Rows(0)(0))).Tables(0), 0, 1)
            End If
            GN.ComboFill(Me.cmbLeaveReason, Leave.GetLeaveReason().Tables(0), 0, 1)
            ddlTiming.SelectedIndex = 0
            ddlTiming.Enabled = False
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "showDetails();", True)
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            hdnValue.Value += "~" + Session("UserID").ToString()
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim myFile As HttpPostedFile = fup1.PostedFile
            Dim nFileLen As Integer = myFile.ContentLength
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                AttachImg = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, nFileLen)
            End If
            Dim Result As String
            Try
                 Result = Leave.LeaveRequest(hdnValue.Value, AttachImg, ContentType)
            Catch
                Result = "1ØError"
            End Try
            Dim arr() = Result.Split(CChar("Ø"))
            If CDbl(arr(0)) = 0 Then
                initializeControls()
            End If

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + arr(1) + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
