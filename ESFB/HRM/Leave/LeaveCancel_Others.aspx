﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="LeaveCancel_Others.aspx.vb" Inherits="LeaveCancel_Others" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="~/Script/Style.css" type="text/css" rel="Stylesheet"/>
     <script src="../Script/Validations.js" type="text/javascript"></script>   
    <script src="../Script/Calculations.js" language="javascript" type="text/javascript"> </script> 
     <script language="javascript" type="text/javascript">
         function btnExit_onclick() {
             window.open(" ../../Home.aspx", "_self");
         }
         function EmployeeOnChange() {
             var EmpCode = document.getElementById("<%= cmbEmployee.ClientID %>").value;
             if (EmpCode != "") {
                 ToServer("1Ø" + EmpCode, 1);
                 if (document.getElementById("<%= hdnFormID.ClientID %>").value == 2) {
                     viewrpt(EmpCode);
                 }
             }
         }
         function LeaveOnChange() {
             var Leave = document.getElementById("<%= cmbLeave.ClientID %>").value;            
             if (Leave != "-1") {                
                 var DTL = document.getElementById("<%= cmbLeave.ClientID %>").value.split("Ä");
                 document.getElementById("<%= txtLeaveFrom.ClientID %>").value = DTL[4];
                 document.getElementById("<%= txtLeaveTo.ClientID %>").value = DTL[5];
                 document.getElementById("<%= txtLeaveDays.ClientID %>").value = DTL[6];
                 document.getElementById("<%= txtReason.ClientID %>").value = DTL[8];
                 document.getElementById("<%= txtCategory.ClientID %>").value = DTL[7];
                 document.getElementById("<%= txtRequestDate.ClientID %>").value = DTL[1];
                 
                 document.getElementById("<%= txtCancelReason %>").focus();
             }
             else {
                 document.getElementById("<%= txtLeaveFrom.ClientID %>").value ="";
                 document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                 document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                 document.getElementById("<%= txtReason.ClientID %>").value = "";
                 document.getElementById("<%= txtCategory.ClientID %>").value = "";
                 document.getElementById("<%= txtRequestDate.ClientID %>").value = "";
             }
         }
         function FromServer(arg, context) {

             switch (context) {

                 case 1:
                     {
                         if (arg == "ØØ") {
                             alert("Invalid Employee Code"); document.getElementById("<%= txtName.ClientID %>").value = "";
                             document.getElementById("<%= txtBranch.ClientID %>").value = ""; 
                             document.getElementById("<%= cmbEmployee.ClientID %>").focus();
                             return false; }
                         var Data = arg.split("Ø");
                         document.getElementById("<%= txtName.ClientID %>").value = Data[0];
                         document.getElementById("<%= txtBranch.ClientID %>").value = Data[1];
                         document.getElementById("<%= txtDepartment.ClientID %>").value = Data[2];
                         ComboFill(Data[3], "<%= cmbLeave.ClientID %>");

                         break;
                     }

                 case 2:
                     {
                         var Data = arg.split("Ø");
                         alert(Data[1]);
                         if (Data[0] == 0) window.open("LeaveCancel_Others.aspx?ID=" + btoa(document.getElementById("<%= hdnFormID.ClientID %>").value), "_self");
                         break;
                     }
                 case 3:
                     {
                         
                         var Data = arg.split("^");
                         document.getElementById("<%= hid_dtls.ClientID %>").value = Data[0];
                         document.getElementById("<%= hid_sum.ClientID %>").value = Data[1];
                         table_fill();
                     }

             }

         }
         function viewrpt(EMPID) {

             if (EMPID != -1) {
                 var Data = "3Ø" + EMPID;
                 ToServer(Data, 3);
             }
         }
         function ComboFill(data, ddlName) {
             document.getElementById(ddlName).options.length = 0;
             var rows = data.split("Ñ");
             for (a = 1; a < rows.length; a++) {
                 var cols = rows[a].split("ÿ");
                 var option1 = document.createElement("OPTION");
                 option1.value = cols[0];
                 option1.text = cols[1];
                 document.getElementById(ddlName).add(option1);
             }
         }
         function btnConfirm_onclick() {
             if (document.getElementById("<%= cmbEmployee.ClientID %>").value == "-1")
             { alert("Select Employee"); document.getElementById("<%= cmbEmployee.ClientID %>").focus(); return false; }
             if (document.getElementById("<%= cmbLeave.ClientID %>").value == "-1")
             { alert("Select Leave"); document.getElementById("<%= cmbLeave.ClientID %>").focus(); return false; }
             if (document.getElementById("<%= txtCancelReason.ClientID %>").value == "")
             { alert("Enter Reason for Cancellation"); document.getElementById("<%= txtCancelReason.ClientID %>").focus(); return false; }
             var DTL = document.getElementById("<%= cmbLeave.ClientID %>").value.split("Ä");
             ToServer("2Ø" + DTL[0] + "Ø" + document.getElementById("<%= txtCancelReason.ClientID %>").value + "Ø" + DTL[6], 2);
         }
         function table_fill() {

             if (document.getElementById("<%= hid_sum.ClientID %>").value == "") {
                 document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = "";
                 document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = "none";
                 return;
             }
             document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
             var row_bg = 0;
             var tab = "";
             rowsum = document.getElementById("<%= hid_sum.ClientID %>").value.split("¥");

             for (m = 0; m <= rowsum.length - 1; m++) {
                 colsum = rowsum[m].split("µ");
                 tab += "<div style='margin: 0px auto; width:80%;border-color:#C0D0E5; border-style:solid;'>";
                 tab += "<div style='text-align:center;width:100%; height:18px; margin: 0px auto;'  align='center' class=mainhead><span style='color:#214263;'>LEAVE HISTORY</span></div>"
                 tab += "<div style='text-align:center;width:100%; margin: 0px auto;'  align='center'>"
                 tab += "<span>" + colsum[2] + " : " + colsum[3] + "</span>&nbsp;&nbsp;&nbsp;<span>" + colsum[4] + " : " + colsum[5] + "</span>";
                 tab += "&nbsp;&nbsp;&nbsp;<span>" + colsum[6] + " : " + colsum[7] + "</span>&nbsp;&nbsp;&nbsp;<span>" + colsum[8] + " : " + colsum[9] + "</span>";

                 tab += "</div>";

             }
             tab += "<div style='width:100%; height:170px;background-color:#C0D0E5; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
             tab += "<tr class=mainhead>";
             tab += "<td style='width:5%;text-align:center'>Sl No</td>";
             tab += "<td style='width:10%;text-align:center' >Request Date</td>";
             tab += "<td style='width:25%;text-align:left' >Leave Type</td>";
             tab += "<td style='width:10%;text-align:center'>Leave From</td>";
             tab += "<td style='width:10%;text-align:center'>Leave To</td>";
             tab += "<td style='width:10%;text-align:center'>Leave Days</td>";
             tab += "<td style='width:20%;text-align:left'>Reason</td>";
             tab += "<td style='width:10%;text-align:left'>Status</td>";
             tab += "</tr>";

             row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

             for (n = 0; n <= row.length - 1; n++) {
                 col = row[n].split("µ");

                 if (row_bg == 0) {
                     row_bg = 1;
                     tab += "<tr class=sub_first>";
                 }
                 else {
                     row_bg = 0;
                     tab += "<tr class=sub_second>";
                 }
                 i = n + 1;
                 //L.clientid,C.name,L.disb_date,L.loanamt,L.loanid,L.accountno

                 tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                 tab += "<td style='width:10%;text-align:center' >" + col[0] + "</td>";
                 tab += "<td style='width:25%;text-align:left' >" + col[1] + "</td>";
                 tab += "<td style='width:10%;text-align:center'>" + col[2] + "</td>";
                 tab += "<td style='width:10%;text-align:center'>" + col[3] + "</td>";
                 tab += "<td style='width:10%;text-align:center'>" + col[4] + "</td>";
                 tab += "<td style='width:20%;text-align:left'>" + col[5] + "</td>";
                 tab += "<td style='width:10%;text-align:left'>" + col[6] + "</td>";


                 tab += "</tr>";
             }
             tab += "</table><div><div>";
             //alert(tab);
             document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

             //--------------------- Clearing Data ------------------------//


         }
     </script><br />
 <div style="text-align:center;" align="center" >
<table align="center" style="width:80%;margin: 0px auto;">
 <tr>
        
        <td style="width:12%; text-align:left">
            Employee</td>
        <td style="width:51% ; text-align:left">
            &nbsp;&nbsp;<asp:DropDownList ID="cmbEmployee" class="NormalText" runat="server" Font-Names="Cambria" 
                Width="50%" ForeColor="Black">
            </asp:DropDownList>
        </td>
        <td style="width:12% ; text-align:left">
            Name</td>
        <td style="width:25% ; text-align:left">
            &nbsp;&nbsp;<asp:TextBox ID="txtName" runat="server" Width="80%" class="ReadOnlyTextBox" 
                ReadOnly="True"></asp:TextBox>
        </td>
    </tr>
 <tr>
        <td style="width:12% ; text-align:left">
            Location</td>
        <td style="width:51% ; text-align:left">
            &nbsp;&nbsp;<asp:TextBox ID="txtBranch" runat="server" Width="50%" class="ReadOnlyTextBox" 
                ReadOnly="True"></asp:TextBox>
        </td>
        <td style="width:12% ; text-align:left">
            Department</td>
        <td style="width:25% ; text-align:left">
            &nbsp;&nbsp;<asp:TextBox ID="txtDepartment" runat="server" Width="80%" class="ReadOnlyTextBox" 
                ReadOnly="True"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width:12% ; text-align:left">
            Select&nbsp; Leave</td>
        <td style="width:51% ; text-align:left">
            &nbsp;&nbsp;<asp:DropDownList ID="cmbLeave" class="NormalText" runat="server" Font-Names="Cambria" 
                Width="50%" ForeColor="Black">
            </asp:DropDownList>
        </td>
        <td style="width:12% ; text-align:left">
            Request Date</td>
        <td style="width:25%  ; text-align:left">
            &nbsp;&nbsp;<asp:TextBox ID="txtRequestDate" class="ReadOnlyTextBox" runat="server" Width="80%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
        </td>
        
    </tr>
   
    <tr>
        <td style="width:12% ; text-align:left">
            Leave From</td>
        <td style="width:51% ; text-align:left">
            &nbsp;&nbsp;<asp:TextBox ID="txtLeaveFrom" class="ReadOnlyTextBox" runat="server" Width="50%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
        </td>
        <td style="width:12% ; text-align:left">
            Leave To</td>
        <td style="width:25% ; text-align:left">
            &nbsp;&nbsp;<asp:TextBox ID="txtLeaveTo" class="ReadOnlyTextBox" runat="server" Width="80%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width:12% ; text-align:left">
            Leave Days</td>
        <td style="width:51% ; text-align:left">
            &nbsp;&nbsp;<asp:TextBox ID="txtLeaveDays" class="ReadOnlyTextBox" runat="server" Width="50%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
        </td>
        <td style="width:12% ; text-align:left">
            Leave Reason</td>
        <td style="width:25% ; text-align:left">
            &nbsp;&nbsp;<asp:TextBox ID="txtReason" class="ReadOnlyTextBox" runat="server" Width="80%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width:12% ; text-align:left">
            Cancel Reason</td>
        <td style="width:51% ; text-align:left">
             &nbsp;&nbsp;<asp:TextBox ID="txtCancelReason" class="NormalText" runat="server" Width="50%"  onkeypress="return AlphaNumericCheck(event)"
                MaxLength="100"></asp:TextBox>
        </td>
        <td style="width:12% ; text-align:left">Leave Category
            </td>
        <td style="width:25%  ; text-align:left">
            &nbsp;&nbsp;<asp:TextBox ID="txtCategory" class="ReadOnlyTextBox" runat="server" Width="80%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
        </td>
    </tr>
    
    <tr><td style="text-align:center;" colspan="4"><br /><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel><asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_sum" runat="server" /><br /></td></tr>
    <tr>
        <td style="text-align:center;" colspan="4">
            <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="CONFIRM" onclick="return btnConfirm_onclick()" />&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /><asp:HiddenField 
                ID="hdnFormID" runat="server" />
        </td>
    </tr>
</table>    
</div>
<br /><br />
</asp:Content>

