﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="LeaveEntryModification.aspx.vb" Inherits="LeaveEntryModification" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server" >
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
    <script src="../../Script/Calculations.js" language="javascript" type="text/javascript"> </script> 
    <script language="javascript" type="text/javascript">
        var MODFlag = 0;
        var RequestID = -1;
        var CLBal=0;
        var SLBal=0;
        var ELBal = 0;
        var CMBal = 0;
        var RHBal = 0;
        var CLBalD = 0;
        var SLBalD = 0;
        var ELBalD = 0;
        var CMBalD = 0;
        var RHBalD = 0;
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function showDetails() {
            var DTL = document.getElementById("<%= hdnData.ClientID %>").value.split("ử");
            var rowSplitarr = DTL[0].split("~");
            CLBal = Math.abs(rowSplitarr[0]) - Math.abs(rowSplitarr[5]);
            SLBal = Math.abs(rowSplitarr[1]) - Math.abs(rowSplitarr[6]);
            ELBal = Math.abs(rowSplitarr[2]) - Math.abs(rowSplitarr[7]);    
            CMBal = Math.abs(rowSplitarr[3]) - Math.abs(rowSplitarr[8]);
            RHBal = Math.abs(rowSplitarr[4]) - Math.abs(rowSplitarr[9]);
            table_fill(DTL[2]);
        }

        function ShowLeave() {
            var tmptab;
            tmptab = ""
            tmptab = "<br/><table style='width:100%;margin: 0px auto;background-color:#A34747'><tr style='text-align:center;'></tr>";
            tmptab = tmptab + "<tr ><td style='text-align:center;color:#FFF;' colspan='5' ><b>LEAVE BALANCE</b></td></tr><tr class=mainhead style='text-align:center;'>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>Casual Leave</b></td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>Sick Leave</b></td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>Privilege Leave</b></td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>Compensatory</b></td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>RH</b></td></tr>";          
            tmptab += "<tr style='text-align:center; color:#214263;background-color:#FFF; '>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>" + CLBalD + "</td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%'  ><b>" + SLBalD + "</td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>" + ELBalD + "</td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>" + CMBalD + "</td>";
            tmptab = tmptab + "<td style='text-align:center;width:20%' ><b>" + RHBalD + "</td></tr></table>";
            document.getElementById("<%= pnLeaveDtl.ClientID %>").innerHTML = tmptab;
            document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
        }

        function table_fill(arg) {

            if (arg == "") {
                document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = "";
                document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = "none";
                alert("No Leave to Modify!");
                return;
            }
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";            
            tab += "<div style='width:100%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#A34747;' align='center'>";
            tab += "<tr class=mainhead>";
            tab += "<td style='width:5%;text-align:center'>Sl No</td>";
            tab += "<td style='width:10%;text-align:center' >Request Date</td>";
            tab += "<td style='width:15%;text-align:left' >Leave Type</td>";
            tab += "<td style='width:10%;text-align:center'>Leave From</td>";
            tab += "<td style='width:10%;text-align:center'>Leave To</td>";
            tab += "<td style='width:10%;text-align:center'>Leave Days</td>";
            tab += "<td style='width:20%;text-align:left'>Reason</td>";
            tab += "<td style='width:10%;text-align:left'>Status</td>";
            tab += "<td style='width:5%;text-align:center'></td>";
            tab += "<td style='width:5%;text-align:center'></td>";
            tab += "</tr>";         
            row = arg.split("¥");

            for (n = 1; n <= row.length - 1; n++) {
                col = row[n].split("µ");

                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr class=sub_first>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class=sub_second>";
                }
               
                //L.clientid,C.name,L.disb_date,L.loanamt,L.loanid,L.accountno

                tab += "<td style='width:5%;text-align:center'>" + n + "</td>";
                tab += "<td style='width:10%;text-align:center' >" + col[0] + "</td>";
                tab += "<td style='width:15%;text-align:left' >" + col[1] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[2] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[3] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + col[4] + "</td>";
                tab += "<td style='width:20%;text-align:left'>" + col[5] + "</td>";
                tab += "<td style='width:10%;text-align:left'>" + col[6] + "</td>";
                tab += "<td style='width:5%;text-align:center' onclick=EditLeave('" + col[7] + "')><img  src='../../Image/Edit.png'; style='align:middle;cursor:pointer; height:20px; width:20px ;' title='Edit' /></td>";
                tab += "<td style='width:5%;text-align:center' onclick=CancelLeave('" + col[7] + "'," + col[4] + ")><img  src='../../Image/cross.png'; style='align:middle;cursor:pointer; height:20px; width:20px ;' title='Cancel'/></td>";
                tab += "</tr>";
            }
            tab += "</table><div><div>";
            //alert(tab);
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//


        }

        function CancelLeave(arg,Days) {
            document.getElementById("MDtl").style.display = 'none';
            document.getElementById("<%= btnSave.ClientID %>").style.display = 'none';
            if (confirm("Are you sure to cancel this leave?") == 1) {
                var CancelReason = prompt("Enter Reason : ", "Reason For Cancellation"); 
                var Data = arg.split("Ø");
                MODFlag = 2;
                RequestID = Data[0];
                var LeaveDays = Days;
                ToServer("2Ø" + RequestID + "Ø" + CancelReason  + "Ø" + LeaveDays, 2);
            }           
        }
        function EditLeave(arg) {
            var Data = arg.split("Ø");
            MODFlag = 1;
            RequestID = Data[0];
            CLBalD = CLBal;
            SLBalD = SLBal;
            ELBalD = ELBal;
            CMBalD = CMBal;
            RHBalD = RHBal;
            document.getElementById("<%= txtLeaveFrom.ClientID %>").value =new Date(Data[2]).format("dd MMM yyyy");
            document.getElementById("<%= txtLeaveTo.ClientID %>").value = new Date(Data[3]).format("dd MMM yyyy");
            document.getElementById("<%= txtLeaveDays.ClientID %>").value = Data[1];
            document.getElementById("<%= cmbLeaveReason.ClientID %>").value = Data[4];
            document.getElementById("<%= cmbLeaveCategory.ClientID %>").value = Data[5];
            if (Data[5] == 1)
            {
                CLBalD = parseFloat(parseFloat(CLBal) + parseFloat(Data[1]));
                document.getElementById("<%= txtLeaveTo.ClientID %>").disabled = false;
                document.getElementById("<%= chkHalfDay.ClientID %>").disabled = false;
            }
            else if (Data[5] == 2)
            {
                SLBalD = parseFloat(parseFloat(SLBal)+parseFloat(Data[1]));
                document.getElementById("<%= txtLeaveTo.ClientID %>").disabled = false;
                document.getElementById("<%= chkHalfDay.ClientID %>").disabled = false;
            }
            else if (Data[5] == 3)
            {
                ELBalD =parseFloat(parseFloat(ELBal)+parseFloat( Data[1]));
                document.getElementById("<%= txtLeaveTo.ClientID %>").disabled = false;
                document.getElementById("<%= chkHalfDay.ClientID %>").disabled = false;
            }
            else if (Data[5] == 7)
            {
                CMBalD =parseFloat(parseFloat(CMBal)+parseFloat( Data[1]));
                document.getElementById("<%= txtLeaveTo.ClientID %>").disabled = false;
                document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
            }
            else if (Data[5] == 8)
            {
                RHBalD =parseFloat(parseFloat(RHBal)+parseFloat( Data[1]));
                document.getElementById("<%= txtLeaveTo.ClientID %>").disabled = true;
                document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
            }
            if (document.getElementById("<%= txtLeaveDays.ClientID %>").value < 1) {
                document.getElementById("<%= ddlTiming.ClientID %>").value = Data[6];
                document.getElementById("<%= chkHalfDay.ClientID %>").disabled = false ;
                document.getElementById("<%= chkHalfDay.ClientID %>").checked = true;
            }
            else {
                document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
                document.getElementById("<%= chkHalfDay.ClientID %>").checked = false;
                document.getElementById("<%= ddlTiming.ClientID %>").value = "0";
            }
            ShowLeave();
            document.getElementById("MDtl").style.display = '';
            document.getElementById("<%= btnSave.ClientID %>").style.display = '';

        }
        function DateOnChange() {
            document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
            document.getElementById("<%= chkHalfDay.ClientID %>").checked = false;
            document.getElementById("<%= ddlTiming.ClientID %>").value = "0";
            document.getElementById("<%= ddlTiming.ClientID %>").disabled = true;         
            var frmDt = document.getElementById("<%= txtLeaveFrom.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtLeaveTo.ClientID %>").value;
            var leaveTypeID = document.getElementById("<%= cmbLeaveCategory.ClientID %>").value;
            var DTL = document.getElementById("<%= hdnData.ClientID %>").value.split("ử");
            if(leaveTypeID == 8)
            {
                 document.getElementById("<%= txtLeaveTo.ClientID %>").value = frmDt;
                 ToDt = frmDt ;
                 document.getElementById("<%= txtLeaveTo.ClientID %>").disabled = true;
                 document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
            }
            else
                document.getElementById("<%= txtLeaveTo.ClientID %>").disabled = false;
            var days = calculateDays(ToDt, frmDt);
            if (frmDt != "" && ToDt != "") {
                if (days > 0) {
                    document.getElementById("<%= txtLeaveDays.ClientID %>").value = days;
                    if (days == 1)
                    { document.getElementById("<%= chkHalfDay.ClientID %>").disabled = false; }
                    else
                    { document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true; document.getElementById("<%= chkHalfDay.ClientID %>").checked = false; }
                }
                else {
                    document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                    document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                    alert("Check Dates");
                    document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                    return false;
                }
            }
           
            if (DTL[1] != "2" && leaveTypeID == 4) {
                alert("Maternity Leave is only eligible for Female Employees");
                document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveFrom.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false;
            }
            if (DTL[1] != "2" && leaveTypeID == 9) {
                alert("ESI Maternity Leave is only eligible for Female Employees");
                document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveFrom.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false;
            }
            if (DTL[1] != "1" && leaveTypeID == 5) {
                alert("Paternity Leave is only eligible for Male Employees");
                document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveFrom.ClientID %>").value = "";
                document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false;
            }  
            if (frmDt != "" && ToDt != "" && leaveTypeID > 0) {               
                var rowSplitarr = DTL[0].split("~");
                if (leaveTypeID == 1) {
                    var dateDiff = calculateDays(document.getElementById("<%= txtLeaveFrom.ClientID %>").value, document.getElementById("<%= txtDOJ.ClientID %>").value);                                                 
                    if (CLBalD < days && CLBalD != 0.5) {
                        alert("You have only " + CLBalD + " Available CL");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                        return false;
                    }
                    if (CLBalD === 0.5) {
                        document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
                        document.getElementById("<%= chkHalfDay.ClientID %>").checked = true;
                        var noDays = 0.5;
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = document.getElementById("<%= txtLeaveFrom.ClientID %>").value;
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = noDays;
                        document.getElementById("<%= ddlTiming.ClientID %>").disabled = false;
                        document.getElementById("<%= ddlTiming.ClientID %>").focus();

                    }               
                    if (days > 3 ) {
                        alert("Casual Leave Should be Max 3 Days");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                        return false;
                    }
                }
                if (leaveTypeID == 2) {                 
                    if (SLBalD < days && SLBalD != 0.5) {
                        alert("You have only " + SLBalD + " Available SL");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                        return false;
                    }
                    if (SLBalD === 0.5) {
                        document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
                        document.getElementById("<%= chkHalfDay.ClientID %>").checked = true;
                        var noDays = 0.5;
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = document.getElementById("<%= txtLeaveFrom.ClientID %>").value;
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = noDays;
                        document.getElementById("<%= ddlTiming.ClientID %>").disabled = false;
                        document.getElementById("<%= ddlTiming.ClientID %>").focus();

                    }
                }
                if (leaveTypeID == 3) {                    
                    var dateDiff = calculateDays(document.getElementById("<%= txtLeaveFrom.ClientID %>").value, document.getElementById("<%= txtDOJ.ClientID %>").value);
                    if (dateDiff < 365) {
                        alert("Privilege Leave can apply only after 1 year of Service");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveFrom.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false;
                    }
                    if (ELBalD < days && ELBalD != 0.5) {
                        alert("You have only " + ELBalD + " Available PL");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                        return false;
                    }
                    if (ELBalD === 0.5) {
                        document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
                        document.getElementById("<%= chkHalfDay.ClientID %>").checked = true;
                        var noDays = 0.5;
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = document.getElementById("<%= txtLeaveFrom.ClientID %>").value;
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = noDays;
                        document.getElementById("<%= ddlTiming.ClientID %>").disabled = false;
                        document.getElementById("<%= ddlTiming.ClientID %>").focus();
                    }                  
                }
                if (leaveTypeID == 7)
                {
                    if (CMBalD < days) {
                        alert("You have only " + CMBalD + " Available COMPENSATORY");
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").focus();
                        return false;
                    }
                    document.getElementById("<%= chkHalfDay.ClientID %>").disabled = true;
                    document.getElementById("<%= chkHalfDay.ClientID %>").checked = false;
                } 
                if (DTL[1]==2 && (leaveTypeID == 4 || leaveTypeID == 9)) {
                    var LeaveType=document.getElementById("<%= cmbLeaveCategory.ClientID %>").value;
                    var  Empcode=document.getElementById("<%= txtEmpCode.ClientID %>").value;
                    ToServer("1Ø" + LeaveType + "Ø" + Empcode + "Ø" + RequestID, 1);
                }         
            }
        }
        function RequestOnClick() {
            document.getElementById("<%= hdnValue.ClientID %>").value = "";
            var LeaveTypeID = document.getElementById("<%= cmbLeaveCategory.ClientID %>").value;
            if (LeaveTypeID == -1)
            { alert("Select Leave Category"); document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbLeaveReason.ClientID %>").value == "-1")
            { alert("Select Reason"); document.getElementById("<%= cmbLeaveReason.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtLeaveFrom.ClientID %>").value == "")
            { alert("Select From Date"); document.getElementById("<%= txtLeaveFrom.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtLeaveTo.ClientID %>").value == "")
            { alert("Select To Date"); document.getElementById("<%= txtLeaveTo.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= chkHalfDay.ClientID %>").checked == true && document.getElementById("<%= ddlTiming.ClientID %>").value == 0)
            { alert("Select Session"); document.getElementById("<%= ddlTiming.ClientID %>").focus(); return false; }        
            var Days = document.getElementById("<%= txtLeaveDays.ClientID %>").value;
            if (LeaveTypeID == 2 && Days>3 && document.getElementById("<%= fup1.ClientID %>").value == "")
            { alert("Attach Medical & Fitness Certificate"); return false; }
            if (document.getElementById("<%= fup1.ClientID %>").value != "") {
                var fileName = document.getElementById("<%= fup1.ClientID %>").value;
                var file_extDelimiter = fileName.lastIndexOf(".") + 1;
                var file_ext = fileName.substring(file_extDelimiter).toLowerCase();
                if (file_ext != "png" && file_ext != "pdf" && file_ext != "jpeg" && file_ext != "jpg")
                { alert("Attach a png file"); return false; }
            }
            document.getElementById("<%= hdnValue.ClientID %>").value = document.getElementById("<%= txtEmpCode.ClientID %>").value + "~" + document.getElementById("<%= cmbLeaveCategory.ClientID %>").value + "~" + document.getElementById("<%= txtLeaveFrom.ClientID %>").value + "~" + document.getElementById("<%= txtLeaveTo.ClientID %>").value + "~" + document.getElementById("<%= txtLeaveDays.ClientID %>").value + "~" + document.getElementById("<%= cmbLeaveReason.ClientID %>").value + "~" + document.getElementById("<%= ddlTiming.ClientID %>").value + "~" + RequestID;
        }
        function HalfDayOnClick() {
            if (document.getElementById("<%= chkHalfDay.ClientID %>").checked == true) {
                var noDays = Math.abs(document.getElementById("<%= txtLeaveDays.ClientID %>").value) / 2;
                document.getElementById("<%= txtLeaveDays.ClientID %>").value = noDays;
                document.getElementById("<%= ddlTiming.ClientID %>").disabled = false;
                document.getElementById("<%= ddlTiming.ClientID %>").focus();
            }
            else {
                var frmDt = document.getElementById("<%= txtLeaveFrom.ClientID %>").value;
                var ToDt = document.getElementById("<%= txtLeaveTo.ClientID %>").value;
                var days = calculateDays(ToDt, frmDt);
                document.getElementById("<%= txtLeaveDays.ClientID %>").value = days;
                document.getElementById("<%= ddlTiming.ClientID %>").value = "0";
                document.getElementById("<%= ddlTiming.ClientID %>").disabled = true;
            }
        }

        function FromServer(Arg, Context) {
            switch (Context) {
                case 1:{
                    var Data = Arg.split("Ø");
                    if (Data[0] == 1) { alert("e");
                        alert( Data[1]);
                        document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveFrom.ClientID %>").value = "";
                        document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                        document.getElementById("<%= cmbLeaveCategory.ClientID %>").focus(); break;
                    }
                    else if (Data[0] == 2) {
                        if (Math.abs(document.getElementById("<%= txtLeaveDays.ClientID %>").value) > Math.abs(Data[1])){
                            var msg=" Materinity leave allowed only " + Data[1] + " days";
                            alert(msg);
                            document.getElementById("<%= txtLeaveDays.ClientID %>").value = "";
                            document.getElementById("<%= txtLeaveTo.ClientID %>").value = "";
                            document.getElementById("<%= txtLeaveTo.ClientID %>").focus(); break;
                        }
                    }
                }

                case 2:
                {
                    var Data = Arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == 0) window.open("LeaveEntryModification.aspx", "_self");
                    break;
                }

            }

        }
    </script>
</head>
</html>
<br />
<div style="text-align:center;width:80%; margin: 0px auto; ">
<asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel>
</div>
<div id="MDtl" style="text-align:center;display:none;" align="center">
<table style="text-align:center;width:80%; margin: 0px auto; ">
 <tr>   
            <td colspan="4"><asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager></td></tr>
 <tr >
        <td colspan="4" >
            <asp:Panel ID="pnLeaveDtl" runat="server" Width="100%" >
            </asp:Panel> 
           
            <br />
        </td>
    </tr>
    
<tr>
        <td style="width:12%;text-align:left; ">
            Employee Code</td>
        <td style="width:56% ;">
            <asp:TextBox ID="txtEmpCode" class="ReadOnlyTextBox" runat="server" Width="40%" 
                MaxLength="5" ReadOnly="True"></asp:TextBox>
        </td>
        <td style="width:12%;text-align:left; ">
            Name</td>
        <td style="width:20%;">
            <asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="100%" 
                MaxLength="50" ReadOnly="True"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width:12%;text-align:left; ">
            Branch/Department</td>
        <td style="width:56%;">
            <asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" runat="server"  Width="40%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
        </td>
        <td style="width:12%;text-align:left; ">
            Date Of Join</td>
        <td style="width:20%;">
            <asp:TextBox ID="txtDOJ" class="ReadOnlyTextBox" runat="server" Width="100%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
        </td>
    </tr>  
    <tr>
        <td style="width:12%;text-align:left; ">
            Leave Category</td>
        <td style="width:56%">
            <asp:DropDownList ID="cmbLeaveCategory" class="NormalText" runat="server" Font-Names="Cambria" Width="40%" 
                 ForeColor="Black">
            </asp:DropDownList>
        </td>
        <td style="width:12%;text-align:left; ">
            Leave Reason</td>
        <td style="width:20%">
            <asp:DropDownList ID="cmbLeaveReason" class="NormalText" runat="server" Font-Names="Cambria" 
                Width="100%" ForeColor="Black">
            </asp:DropDownList>
        </td>
    </tr>
    
    <tr>
        <td style="width:12%;text-align:left; ">
            Leave From</td>
        <td style="width:56%">
            <asp:TextBox ID="txtLeaveFrom" class="NormalText" runat="server" Width="40%" 
                MaxLength="100" Height="20px"></asp:TextBox>
             <asp:CalendarExtender ID="CEFromDate" runat="server" TargetControlID="txtLeaveFrom" Format="dd MMM yyyy"></asp:CalendarExtender>
        </td>
        <td style="width:12%;text-align:left; ">
            Leave To</td>
        <td style="width:20%">
            <asp:TextBox ID="txtLeaveTo" class="NormalText" runat="server" Width="100%" 
                MaxLength="100"></asp:TextBox>
            <asp:CalendarExtender ID="CETo" runat="server" Enabled="True" Format="dd MMM yyyy"
                TargetControlID="txtLeaveTo">
            </asp:CalendarExtender>
        </td>
    </tr>
    <tr>
        <td style="width:12%;text-align:left; ">
            Leave Days</td>
        <td style="width:56%">
            <asp:TextBox ID="txtLeaveDays" class="ReadOnlyTextBox" runat="server" Width="20%" 
                MaxLength="100" ReadOnly="True"></asp:TextBox>
            &nbsp;&nbsp;<asp:CheckBox ID="chkHalfDay" runat="server" Font-Names="Times New Roman" 
                Font-Size="10pt" Text="Half Day" Width="20%" />
        </td>
        <td style="width:12%;text-align:left; ">
            Session</td>
            
        <td style="width:20%">
            <form enctype="multipart/form-data" action="/upload/image" method="post">
            </form>
            <asp:DropDownList ID="ddlTiming" runat="server" Font-Names="Times New Roman" 
                Font-Size="10pt" Width="70%">
                <asp:ListItem Value="0">SELECT</asp:ListItem>
                <asp:ListItem Value="1">FORENOON</asp:ListItem>
                <asp:ListItem Value="2">AFTERNOON</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="width:12%;text-align:left; ">
            Attachment If any</td>
        <td style="width:56%">
            <input id="fup1" type="file" runat="server" 
                style="font-family: Cambria; font-size: 10.5pt" /></td>
        <td style="width:12%;text-align:left; ">
            &nbsp;</td>
            
        <td style="width:20%">
            &nbsp;</td>
    </tr>
    <tr><td style="text-align:center;" colspan="4">&nbsp;</td></tr>
    <tr>
        <td style="text-align:center;" colspan="4">                     
            <asp:HiddenField 
                ID="hdnValue" runat="server" /><asp:HiddenField 
                ID="hdnImg" runat="server" /><asp:HiddenField ID="hdnData" 
                runat="server" />
        </td>
    </tr>    
</table>    
</div><br />
<div style="text-align:center;width:80%; margin: 0px auto; ">
 <asp:Button 
   ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" style="cursor:pointer;display:none;"
                 Width="5%" /> 
  <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 5%; " 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
</div>
<br /><br />
</asp:Content>

