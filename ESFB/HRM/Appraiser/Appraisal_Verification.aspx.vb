﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Appraisal_Verification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler

    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim Leave As New Leave
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        'If GF.FormAccess(CInt(Session("UserID")), 16) = False Then
        '    Server.Transfer("~/AccessDenied.aspx")
        '    Return
        'End If
        Me.Master.subtitle = "Appraisal Verification"
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        If Not IsPostBack Then
            DT = DB.ExecuteDataSet("select -1 as emp_code,'Select' as EmpName union all select a.Emp_Code,b.Emp_Name  FROM  HR_APP_Appraisal_Master a,Emp_List b   where a.Emp_Code=b.Emp_Code and  a.Status_ID=2 and a.Reporting_Head=" + Session("USerID").ToString()).Tables(0)
            GF.ComboFill(cmbEmployee, DT, 0, 1)
            DT = DB.ExecuteDataSet("select grade_id,grade from HR_APP_Grade_Master where Status_ID=1 order by Grade_ID  ").Tables(0)
            Me.hdnGrade.Value = ""
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    Me.hdnGrade.Value += "Ñ" & DR(0).ToString() & "ÿ" & DR(1).ToString()
                Next
            End If
        End If
        cmbEmployee.Attributes.Add("onchange", "return EmpOnChange()")
        'Catch ex As Exception
        '    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
        '    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        'End Try
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1
                Dim EmpCode As Integer = CInt(Data(1))
                Dim AppraisalDetails As String = Data(2).Substring(1)
                Dim GeneralComments As String = Data(3)
                Dim Mothername As String = Data(4)
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(5) As SqlParameter
                    Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(0).Value = EmpCode
                    Params(1) = New SqlParameter("@AppraisalDetails", SqlDbType.VarChar)
                    Params(1).Value = AppraisalDetails
                    Params(2) = New SqlParameter("@GeneralComments", SqlDbType.VarChar, 500)
                    Params(2).Value = GeneralComments
                    Params(3) = New SqlParameter("@SecAnswer", SqlDbType.VarChar, 500)
                    Params(3).Value = Mothername
                    Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(5).Direction = ParameterDirection.Output
                    DB.ExecuteDataSet("SP_HR_APP_ASSESSMENT_VERIFY", Params)
                    ErrorFlag = CInt(Params(4).Value)
                    Message = CStr(Params(5).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
            Case 2
                Dim EmpCode As Integer = CInt(Data(1))
                CallBackReturn = ""
                Try
                    DT = DB.ExecuteDataSet("select a.emp_code,emp_Name,branch_Name,Designation_Name,convert(varchar,b.date_of_join,106) from emp_list a,HR_APP_Appraisal_Master b where a.emp_code=b.Emp_Code and b.Status_ID=2  and a.emp_code=" + EmpCode.ToString()).Tables(0)
                    If DT.Rows.Count > 0 Then
                        CallBackReturn += DT.Rows(0)(0).ToString() + "~" + DT.Rows(0)(1).ToString() + "~" + DT.Rows(0)(2).ToString() + "~" + DT.Rows(0)(3).ToString() + "~" + DT.Rows(0)(4).ToString()
                    End If
                    CallBackReturn += "ʘ"
                    DT = DB.ExecuteDataSet("select a.Category_ID,a.Category_Name,b.Trait_ID,b.Trait_Descr,d.Grade  from HR_App_category_Master a,HR_APP_Trait_Master b,HR_APP_Appraisal_DTL c,HR_APP_Grade_Master d where a.category_Id=b.category_ID and b.Trait_ID=c.trait_id and c.Self =d.Grade_ID  and c.Emp_Code=" + EmpCode.ToString() + "   order by Category_ID ,Trait_ID").Tables(0)
                    If DT.Rows.Count > 0 Then
                        For Each DR As DataRow In DT.Rows
                            CallBackReturn += "Ñ" & DR(0).ToString() & "ÿ" & DR(1).ToString() & "ÿ" & DR(2).ToString() & "ÿ" & DR(3).ToString() & "ÿ" & DR(4).ToString()
                        Next
                    End If
                Catch ex As Exception

                End Try

        End Select
    End Sub
End Class
