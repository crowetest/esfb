﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class HRM_Appraiser_Appraisal_Self
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler

    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim Leave As New Leave
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        'If GF.FormAccess(CInt(Session("UserID")), 16) = False Then
        '    Server.Transfer("~/AccessDenied.aspx")
        '    Return
        'End If
        Me.Master.subtitle = "Self Appraisal"
        DT = DB.ExecuteDataSet("select count(*) from HR_APP_APPRAISAL_MASTER WHERE EMP_CODE = " + Session("UserID").ToString() + "").Tables(0)
        If CInt(DT.Rows(0)(0)) <= 0 Then
            Dim cl_script0 As New System.Text.StringBuilder
            cl_script0.Append("         alert('You are Not in List for Appraisal');window.open('../../Home.aspx','_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
            Exit Sub
        End If
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        If Not IsPostBack Then
            DT = DB.ExecuteDataSet("select emp_code,emp_Name,branch_Name,Designation_Name,date_of_join  from Emp_List where Emp_Code=" + Session("USerID").ToString()).Tables(0)
            txtEmpCode.Text = DT.Rows(0)(0).ToString()
            txtName.Text = DT.Rows(0)(1).ToString()
            txtBranch.Text = DT.Rows(0)(2).ToString()
            txtDesignation.Text = DT.Rows(0)(3).ToString()
            txtDateofJoin.Text = CDate(DT.Rows(0)(4)).ToString("dd MMM yyyy")
            DT = DB.ExecuteDataSet("select a.Category_ID,a.Category_Name,b.Trait_ID,b.Trait_Descr from HR_App_category_Master a,HR_APP_Trait_Master b,HR_APP_Appraisal_Master c where  a.category_Id=b.category_ID  and a.Status_ID =1 and b.Status_ID =1 and c.Emp_Code=" + Session("UserID").ToString() + " and c.Status_ID=1  order by Category_ID ,Trait_ID ").Tables(0)
            Me.hdnTraits.Value = ""
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    Me.hdnTraits.Value += "Ñ" & DR(0).ToString() & "ÿ" & DR(1).ToString() & "ÿ" & DR(2).ToString() & "ÿ" & DR(3).ToString()
                Next
            End If
            DT = DB.ExecuteDataSet("select grade_id,grade from HR_APP_Grade_Master where Status_ID=1 order by Grade_ID  ").Tables(0)
            Me.hdnGrade.Value = ""
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    Me.hdnGrade.Value += "Ñ" & DR(0).ToString() & "ÿ" & DR(1).ToString()
                Next
            End If

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "initialize();", True)

        End If
        'Catch ex As Exception
        '    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
        '    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        'End Try
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1
                Dim EmpCode As Integer = CInt(Data(1))
                Dim DOJ As Date = CDate(Data(2))
                Dim AppraisalDetails As String = Data(3).Substring(1)
                Dim GeneralComments As String = Data(4)
                Dim NoofTrainings As Integer = CInt(Data(5))
                Dim TrainingNeeded As Integer = CInt(Data(6))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(7) As SqlParameter
                    Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(0).Value = EmpCode
                    Params(1) = New SqlParameter("@DOJ", SqlDbType.Date)
                    Params(1).Value = DOJ
                    Params(2) = New SqlParameter("@AppraisalDetails", SqlDbType.VarChar)
                    Params(2).Value = AppraisalDetails
                    Params(3) = New SqlParameter("@GeneralComments", SqlDbType.VarChar, 500)
                    Params(3).Value = GeneralComments
                    Params(4) = New SqlParameter("@NoOfTrainings", SqlDbType.Int)
                    Params(4).Value = NoofTrainings
                    Params(5) = New SqlParameter("@TrainingNeeded", SqlDbType.Int)
                    Params(5).Value = TrainingNeeded
                    Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(7).Direction = ParameterDirection.Output
                    DB.ExecuteDataSet("SP_HR_APP_SELF_ASSESSMENT", Params)
                    ErrorFlag = CInt(Params(6).Value)
                    Message = CStr(Params(7).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
End Class
