﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ProblemStatusReport
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim UserID As Integer
    Dim RH As New WholeHelper.ClsRepCtrl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      
        If Not IsPostBack Then
            DT = DB.ExecuteDataSet("select group_id,convert(varchar,interview_date,106)as InterViewDate from hr_app_appraisal_group order by group_id desc").Tables(0)
            GN.ComboFill(cmbGroup, DT, 0, 1)
        End If
        GenerateReport()
    End Sub

    Public Sub GenerateReport()
        pnDisplay.Controls.Clear()
        Dim GroupID As Integer = CInt(Me.cmbGroup.SelectedValue)
        Dim StatusID As Integer = CInt(Me.cmbStatus.SelectedValue)
        Dim SQL As String
        Dim StatusFilter As String = ""
        Dim Selection As String = ""
        If StatusID = 0 Then
            StatusFilter = " and a.STATUS_ID = " & StatusID & ""
        Else
            StatusFilter = " and a.STATUS_ID <> 0"
        End If
        Selection = "select a.Emp_Code,b.Emp_Name,convert(VARCHAR(11),a.Date_Of_join,106)  Date_of_join,b.Branch_Name,b.Department_Name,c.Status_Name from HR_APP_Appraisal_Master a,EMP_LIST b,HR_APP_Status c where a.Emp_Code = b.Emp_Code and a.group_id=" + GroupID.ToString() + " and a.Status_ID = c.Status_ID "
        SQL = Selection + StatusFilter + " order by a.APPROVE_DT,a.VERIFY_DT ,a.SELF_DT ,a.Emp_Code "
        DT = GN.GetQueryResult(SQL)

        Dim tb As New Table
        RH.Heading("", tb, Me.cmbStatus.SelectedItem.Text.ToUpper() + " STATUS REPORT", 100)
        Dim RowBG As Integer = 0
        Dim DR As DataRow
        tb.Attributes.Add("width", "100%")

        
        Dim TRHead As New TableRow
        TRHead.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell


        RH.InsertColumn(TRHead, TRHead_00, 10, 2, "#")
        RH.InsertColumn(TRHead, TRHead_01, 10, 2, "EMPCODE")
        RH.InsertColumn(TRHead, TRHead_02, 20, 0, "EMP NAME")
        RH.InsertColumn(TRHead, TRHead_03, 15, 2, "JOIN DATE")
        RH.InsertColumn(TRHead, TRHead_04, 20, 0, "BRANCH")
        RH.InsertColumn(TRHead, TRHead_05, 15, 0, "DEPARTMENT")
        RH.InsertColumn(TRHead, TRHead_06, 10, 0, "STATUS")
        tb.Controls.Add(TRHead)

        Dim i As Integer = 1
        For Each DR In DT.Rows
            Dim TR3 As New TableRow
            TR3.BorderWidth = "1"
            TR3.BorderStyle = BorderStyle.Solid

            Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell
            RH.InsertColumn(TR3, TR3_00, 10, 2, i)
            RH.InsertColumn(TR3, TR3_01, 10, 2, "<a href='AppraisalReport_0.aspx?EmpCode=" + GN.Encrypt(DR(0).ToString()) + "' target='_blank'>" + DR(0).ToString() + "</a>")
            RH.InsertColumn(TR3, TR3_02, 20, 0, DR(1))
            RH.InsertColumn(TR3, TR3_03, 15, 2, DR(2))
            RH.InsertColumn(TR3, TR3_04, 20, 0, DR(3))
            RH.InsertColumn(TR3, TR3_05, 15, 0, DR(4))
            RH.InsertColumn(TR3, TR3_06, 10, 0, DR(5))
            i += 1
            tb.Controls.Add(TR3)
        Next

        RH.BlankRow(tb, 4)
        pnDisplay.Controls.Add(tb)
    End Sub
    Protected Sub btnGenerate_Click(sender As Object, e As System.EventArgs) Handles btnGenerate.Click
        GenerateReport()
    End Sub
End Class
