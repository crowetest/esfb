﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="AppraisalReport_0.aspx.vb" Inherits="ProblemStatusReport" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <style>
    @media print {
    .screenOnly { display: none; }
   
}

@media screen {
    .printOnly { display: none; margin:0px; }    
}
#Menu,#Footer
{
    display:none;
}

</style>
</head>
<body style="background-color:Black;">

 <script language="javascript" type="text/javascript">
     function PrintPanel() {
         var panel = document.getElementById("<%=pnDisplay.ClientID %>");
         var printWindow = window.open('', '', 'height=400,width=800');
//         printWindow.document.write('<html><head><title>ESAF</title>');
         printWindow.document.write('</head><body style=margin:0mm 0mm 0mm 0mm; >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');        
         printWindow.document.close();
         setTimeout(function () {
             printWindow.print();
         }, 500);
         return false;
     }
     function Exitform() {
         window.close();
         return false;
     }
    
     </script>
    
     <form id="form1" runat="server"> 
       
    <div style="width:100%; background-color:white; min-height:750px; margin:0px auto;">
    <div style="text-align:right ;margin:0px auto; width:95%; background-color:white; ">
            <asp:HiddenField ID="hdnReportID" runat="server" />
            <asp:ImageButton ID="cmd_Back" style="text-align:left ;float:left; padding-top:5px;" runat="server" Height="35px" Width="35px" ImageAlign="AbsMiddle" ImageUrl="~/Image/close.png" ToolTip="Back"/>
            <asp:ImageButton ID="cmd_Print" style="text-align:right ;" runat="server" Height="40px" Width="40px" ImageAlign="AbsMiddle" ImageUrl="~/Image/print.png" ToolTip="Click to Print"/>
            &nbsp;&nbsp;</div>
     <br /> 
    <div>
    <table  style="width:100%;margin: 0px auto;font-family:Cambria;" border="1" >
    <tr><td><asp:Panel ID="pnDisplay"  style="text-align:center;"  runat="server" Width="100%">
            </asp:Panel></td></tr>
    </table>
     
    </div>
       <br style="background-color:white"/>
       
    </div>
    </form>
</body>

</html>
</asp:Content>