﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ProblemStatusReport
    Inherits System.Web.UI.Page
    Dim DT_Head, DT_Sub, DT, DT1 As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim EmpCode As Integer
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")
        EmpCode = CInt(GN.Decrypt(Request.QueryString.Get("EmpCode")))
        Dim SQL As String
        SQL = "select a.Emp_Code,b.Emp_Name,b.Department_Name,b.Designation_Name,b.Cadre_name,c.Zone_Name,c.Area_Name,c.Branch_Name,a.Date_Of_join,isnull(Average_Rating,0),isnull(Average_Rating_Verification,0),isnull(Average_Rating_Approver,0),isnull(Number_Of_Trainings_Attended,0), case when Training_Needed=1 then 'Yes' else 'No' End,isnull(general_comment,''),isnull(General_Comment_Verification,''),isnull(General_Comment_Approver,''),d.emp_name+' ('+cast(d.emp_code as varchar)+')',e.emp_name+' ('+cast(e.emp_code as varchar)+')' from HR_APP_Appraisal_Master a,EMP_list b,BrMaster c,emp_master d,emp_master e where a.Emp_Code = b.Emp_Code and b.Branch_ID =  c.Branch_ID and a.reporting_head = d.emp_code and a.department_head = e.emp_code and a.Emp_Code = " & EmpCode & ""
        DT_Head = GN.GetQueryResult(SQL)


        RH.Heading("", tb, "Appraisal Form – Evaluation sheet for Promotion", 100)
        Dim RowBG As Integer = 0
        Dim DR As DataRow
        tb.Attributes.Add("width", "100%")

        Dim TRHead00 As New TableRow
        'TRHead00.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead00_00, TRHead00_01, TRHead00_02, TRHead00_03 As New TableCell
        RH.InsertColumn(TRHead00, TRHead00_00, 18, 0, "Code&nbsp;&nbsp;&nbsp;: " + DT_Head.Rows(0)(0).ToString())
        RH.InsertColumn(TRHead00, TRHead00_01, 35, 0, "Name : " + StrConv(DT_Head.Rows(0)(1).ToString(), VbStrConv.ProperCase))
        RH.InsertColumn(TRHead00, TRHead00_02, 23, 0, "Zone : " + StrConv(DT_Head.Rows(0)(5).ToString(), VbStrConv.ProperCase))
        RH.InsertColumn(TRHead00, TRHead00_03, 24, 0, "Area : " + StrConv(DT_Head.Rows(0)(6).ToString(), VbStrConv.ProperCase))
        tb.Controls.Add(TRHead00)

        Dim TRHead01 As New TableRow
        'TRHead01.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead01_00, TRHead01_01, TRHead01_02, TRHead01_03 As New TableCell
        RH.InsertColumn(TRHead01, TRHead01_00, 18, 0, "Cadre : " + DT_Head.Rows(0)(4).ToString())
        RH.InsertColumn(TRHead01, TRHead01_01, 35, 0, "Designation : " + StrConv(DT_Head.Rows(0)(3).ToString(), VbStrConv.ProperCase))
        RH.InsertColumn(TRHead01, TRHead01_02, 23, 0, "Department : " + StrConv(DT_Head.Rows(0)(2).ToString(), VbStrConv.ProperCase))
        RH.InsertColumn(TRHead01, TRHead01_03, 24, 0, "Branch : " + StrConv(DT_Head.Rows(0)(7).ToString(), VbStrConv.ProperCase))
        tb.Controls.Add(TRHead01)
      
        Dim TRHead05 As New TableRow
        TRHead05.Style.Add("font-weight", "bold")
        'TRHead05.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead05_00, TRHead05_01, TRHead05_02 As New TableCell
        RH.InsertColumn(TRHead05, TRHead05_00, 18, 0, "DOJ : " + Format(DT_Head.Rows(0)(8), "dd-MMM-yyyy"))
        RH.InsertColumn(TRHead05, TRHead05_01, 35, 0, "Training Needed : " + DT_Head.Rows(0)(13).ToString())
        RH.InsertColumn(TRHead05, TRHead05_02, 47, 0, "No of Training attended : " + DT_Head.Rows(0)(12).ToString())

        tb.Controls.Add(TRHead05)
        RH.BlankRow(tb, 2)


        DT_Sub = DB.ExecuteDataSet("select STUFF((SELECT ',' +( QUOTENAME(cast(Grade_ID as varchar)+' - '+ Grade)) from HR_APP_Grade_Master a order by Grade_ID desc FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)') ,1,1,'') option (maxrecursion 365)").Tables(0)
        Dim TRHead02 As New TableRow
        Dim TRHead02_00 As New TableCell
        RH.InsertColumn(TRHead02, TRHead02_00, 100, 2, "<b>Scale : " + DT_Sub.Rows(0)(0).ToString().Replace("[", "").Replace("]", "") + "</b>")
        tb.Controls.Add(TRHead02)
        RH.BlankRow(tb, 2)

        Dim TRHead03 As New TableRow
        'TRHead.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead03_00, TRHead03_01, TRHead03_02, TRHead03_03, TRHead03_04 As New TableCell
        RH.InsertColumn(TRHead03, TRHead03_00, 3, 2, "#")
        RH.InsertColumn(TRHead03, TRHead03_01, 73, 0, "Traits")
        RH.InsertColumn(TRHead03, TRHead03_02, 8, 2, "Level 1")
        RH.InsertColumn(TRHead03, TRHead03_03, 8, 2, "Level 2")
        RH.InsertColumn(TRHead03, TRHead03_04, 8, 2, "Level 3")
        tb.Controls.Add(TRHead03)

        DT1 = DB.ExecuteDataSet("select category_id,category_name from HR_APP_Category_Master order by 1").Tables(0)
        DT = DB.ExecuteDataSet("select b.Category_id,b.Category_Name,c.Trait_Descr,a.Self,a.Verifier,a.Approver from HR_APP_Appraisal_DTL a,HR_APP_Category_Master b,HR_APP_Trait_Master c where a.Trait_ID = c.Trait_ID and c.Category_ID=b.Category_ID and a.Emp_Code = " & EmpCode & " order by b.Category_ID").Tables(0)
        Dim i As Integer = 1
        Dim CategoryID As Integer = 0
        For Each DR In DT1.Rows
            Dim TR1 As New TableRow
            TR1.BorderWidth = "1"
            TR1.BorderStyle = BorderStyle.Solid
            Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04 As New TableCell
            RH.InsertColumn(TR1, TR1_00, 3, 2, "")
            RH.InsertColumn(TR1, TR1_01, 73, 0, "<b>" + DR(1).ToString() + "</b>")
            RH.InsertColumn(TR1, TR1_02, 8, 2, "")
            RH.InsertColumn(TR1, TR1_03, 8, 2, "")
            RH.InsertColumn(TR1, TR1_04, 8, 2, "")
            tb.Controls.Add(TR1)
            If CategoryID <> DR(0) Then
                CategoryID = DR(0)
                For Each DR1 As DataRow In DT.Rows
                    If CategoryID = DR1(0) Then
                        Dim TR2 As New TableRow
                        TR2.BorderWidth = "1"
                        TR2.BorderStyle = BorderStyle.Solid
                        Dim TR2_00, TR2_01, TR2_02, TR2_03, TR2_04 As New TableCell
                        RH.InsertColumn(TR2, TR2_00, 3, 2, i)
                        RH.InsertColumn(TR2, TR2_01, 73, 0, DR1(2).ToString())
                        RH.InsertColumn(TR2, TR2_02, 8, 2, DR1(3).ToString())
                        RH.InsertColumn(TR2, TR2_03, 8, 2, DR1(4).ToString())
                        RH.InsertColumn(TR2, TR2_04, 8, 2, DR1(5).ToString())
                        tb.Controls.Add(TR2)
                        i += 1
                    End If
                Next
            End If
            i = 1
        Next

        Dim TREnd00 As New TableRow
        Dim TREnd00_00, TREnd00_01, TREnd00_02, TREnd00_03, TREnd00_04 As New TableCell
        RH.InsertColumn(TREnd00, TREnd00_00, 3, 2, "")
        RH.InsertColumn(TREnd00, TREnd00_01, 73, 0, "<b>Average Rating</b>")
        RH.InsertColumn(TREnd00, TREnd00_02, 8, 2, DT_Head.Rows(0)(9))
        RH.InsertColumn(TREnd00, TREnd00_03, 8, 2, DT_Head.Rows(0)(10))
        RH.InsertColumn(TREnd00, TREnd00_04, 8, 2, DT_Head.Rows(0)(11))
        tb.Controls.Add(TREnd00)

        'Dim TREnd01 As New TableRow
        'Dim TREnd01_00, TREnd01_01, TREnd01_02, TREnd01_03, TREnd01_04 As New TableCell
        'RH.InsertColumn(TREnd01, TREnd01_00, 3, 2, "")
        'RH.InsertColumn(TREnd01, TREnd01_01, 58, 0, "<b>Date of Joining</b>")
        'RH.InsertColumn(TREnd01, TREnd01_02, 13, 2, Format(DT_Head.Rows(0)(8), "dd-MMM-yyyy"))
        'RH.InsertColumn(TREnd01, TREnd01_03, 13, 2, "")
        'RH.InsertColumn(TREnd01, TREnd01_04, 13, 2, "")
        'tb.Controls.Add(TREnd01)

        'Dim TREnd02 As New TableRow
        'Dim TREnd02_00, TREnd02_01, TREnd02_02, TREnd02_03, TREnd02_04 As New TableCell
        'RH.InsertColumn(TREnd02, TREnd02_00, 3, 2, "")
        'RH.InsertColumn(TREnd02, TREnd02_01, 58, 0, "<b>Number of Training Attended</b>")
        'RH.InsertColumn(TREnd02, TREnd02_02, 13, 2, DT_Head.Rows(0)(12).ToString())
        'RH.InsertColumn(TREnd02, TREnd02_03, 13, 2, "")
        'RH.InsertColumn(TREnd02, TREnd02_04, 13, 2, "")
        'tb.Controls.Add(TREnd02)

        'Dim TREnd03 As New TableRow
        'Dim TREnd03_00, TREnd03_01, TREnd03_02, TREnd03_03, TREnd03_04 As New TableCell
        'RH.InsertColumn(TREnd03, TREnd03_00, 3, 2, "")
        'RH.InsertColumn(TREnd03, TREnd03_01, 58, 0, "<b>Training Needed</b>")
        'RH.InsertColumn(TREnd03, TREnd03_02, 13, 2, DT_Head.Rows(0)(13).ToString())
        'RH.InsertColumn(TREnd03, TREnd03_03, 13, 2, "")
        'RH.InsertColumn(TREnd03, TREnd03_04, 13, 2, "")
        'tb.Controls.Add(TREnd03)
        RH.BlankRow(tb, 5)
        Dim TREnd04 As New TableRow
        TREnd04.Style.Add("background-color", "silver")
        Dim TREnd04_00 As New TableCell
        RH.InsertColumn(TREnd04, TREnd04_00, 100, 0, "<b>General Comments</b>")
        tb.Controls.Add(TREnd04)

        Dim TREnd06 As New TableRow
        Dim TREnd06_00, TREnd06_01, TREnd06_02 As New TableCell
        RH.InsertColumn(TREnd06, TREnd06_00, 30, 0, DT_Head.Rows(0)(14).ToString())
        RH.InsertColumn(TREnd06, TREnd06_01, 40, 0, DT_Head.Rows(0)(15).ToString())
        RH.InsertColumn(TREnd06, TREnd06_02, 30, 0, DT_Head.Rows(0)(16).ToString())
        tb.Controls.Add(TREnd06)


        Dim TREnd08 As New TableRow
        TREnd08.Style.Add("height", "50px")
        Dim TREnd08_00, TREnd08_01, TREnd08_02 As New TableCell
        RH.InsertColumn(TREnd08, TREnd08_00, 30, 0, "")
        RH.InsertColumn(TREnd08, TREnd08_01, 40, 0, "")
        RH.InsertColumn(TREnd08, TREnd08_02, 30, 0, "")
        tb.Controls.Add(TREnd08)


        'Dim TREnd04 As New TableRow
        'Dim TREnd04_00, TREnd04_01, TREnd04_02, TREnd04_03, TREnd04_04 As New TableCell
        'RH.InsertColumn(TREnd04, TREnd04_00, 3, 2, "")
        'RH.InsertColumn(TREnd04, TREnd04_01, 58, 0, "<b>General Comment</b>")
        'If IsDBNull(DT_Head.Rows(0)(14)) Then
        '    RH.InsertColumn(TREnd04, TREnd04_02, 13, 2, "")
        'Else
        '    RH.InsertColumn(TREnd04, TREnd04_02, 13, 0, DT_Head.Rows(0)(14).ToString())
        'End If
        'If IsDBNull(DT_Head.Rows(0)(15)) Then
        '    RH.InsertColumn(TREnd04, TREnd04_03, 13, 2, "")
        'Else
        '    RH.InsertColumn(TREnd04, TREnd04_03, 13, 0, DT_Head.Rows(0)(15).ToString())
        'End If
        'If IsDBNull(DT_Head.Rows(0)(16)) Then
        '    RH.InsertColumn(TREnd04, TREnd04_04, 13, 2, "")
        'Else
        '    RH.InsertColumn(TREnd04, TREnd04_04, 13, 0, DT_Head.Rows(0)(16).ToString())
        'End If
        'tb.Controls.Add(TREnd04)
        'RH.BlankRow(tb, 20)

        Dim TREnd05 As New TableRow
        Dim TREnd05_00, TREnd05_01, TREnd05_02 As New TableCell
        RH.InsertColumn(TREnd05, TREnd05_00, 30, 0, DT_Head.Rows(0)(1).ToString() + " (" + DT_Head.Rows(0)(0).ToString() + ")")
        RH.InsertColumn(TREnd05, TREnd05_01, 40, 0, DT_Head.Rows(0)(17).ToString())
        RH.InsertColumn(TREnd05, TREnd05_02, 30, 0, DT_Head.Rows(0)(18).ToString())
        tb.Controls.Add(TREnd05)
        RH.BlankRow(tb, 10)
        Dim TREnd07 As New TableRow
        Dim TREnd07_00 As New TableCell
        RH.AddColumn(TREnd07, TREnd07_00, 100, 100, "c", "<b>This is an auto generated report and no signature is required</b>")
        tb.Controls.Add(TREnd07)
        RH.BlankRow(tb, 5)
        pnDisplay.Controls.Add(tb)
    End Sub

    Private Sub FillHead(ByVal PrintFlag As Integer)
        Dim TRHead00 As New TableRow
        'TRHead00.BackColor = Drawing.Color.WhiteSmoke
        If PrintFlag = 1 Then
            TRHead00.Attributes.Add("class", "printOnly")
        End If
        Dim TRHead00_00, TRHead00_01, TRHead00_02, TRHead00_03 As New TableCell
        RH.InsertColumn(TRHead00, TRHead00_00, 25, 0, "Code&nbsp;&nbsp;&nbsp;: " + DT_Head.Rows(0)(0).ToString())
        RH.InsertColumn(TRHead00, TRHead00_01, 25, 0, "Department : " + DT_Head.Rows(0)(2).ToString())
        RH.InsertColumn(TRHead00, TRHead00_02, 25, 0, "Cadre : " + DT_Head.Rows(0)(4).ToString())
        RH.InsertColumn(TRHead00, TRHead00_03, 25, 0, "Area &nbsp;&nbsp;&nbsp;&nbsp;: " + DT_Head.Rows(0)(6).ToString())
        tb.Controls.Add(TRHead00)

        Dim TRHead01 As New TableRow
        If PrintFlag = 1 Then
            TRHead01.Attributes.Add("class", "printOnly")
        End If
        'TRHead01.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead01_00, TRHead01_01, TRHead01_02, TRHead01_03 As New TableCell
        RH.InsertColumn(TRHead01, TRHead01_00, 25, 0, "Name : " + DT_Head.Rows(0)(1).ToString())
        RH.InsertColumn(TRHead01, TRHead01_01, 25, 0, "Designation : " + DT_Head.Rows(0)(3).ToString())
        RH.InsertColumn(TRHead01, TRHead01_02, 25, 0, "Zone&nbsp;&nbsp;&nbsp;: " + DT_Head.Rows(0)(5).ToString())
        RH.InsertColumn(TRHead01, TRHead01_03, 25, 0, "Branch : " + DT_Head.Rows(0)(7).ToString())
        tb.Controls.Add(TRHead01)
        RH.BlankRow(tb, 4)
    End Sub
End Class
