﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Appraisal_Self.aspx.vb" Inherits="HRM_Appraiser_Appraisal_Self" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<script src="../../Script/Validations.js" type="text/javascript"></script>
    <table align="center" style="width: 60%; margin:0px auto;">
        <tr>
            <td style="width:40%; text-align:right;" colspan="2">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            </td>
            <td style="width:60%; text-align:left;" colspan="2">
                    <asp:HiddenField ID="hdnTraits" runat="server" />
                    <asp:HiddenField ID="hdnGrade" runat="server" />
                    <asp:HiddenField ID="hdnValue" runat="server" />
                </td>
        </tr>
        <tr style="height:20px;">
            <td style="text-align:left; width:20%;">
                Emp Code&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:30%;">
                    <asp:TextBox ID="txtEmpCode" class="ReadOnlyTextBox" runat="server" Width="20%" MaxLength="50"
                        ReadOnly="True"></asp:TextBox>
                </td>
            <td style="text-align:left; width:20%;">
                Name&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:30%;">
                    <asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" 
                    Width="90%" MaxLength="50"
                        ReadOnly="True"></asp:TextBox>
                </td>
        </tr>
        <tr style="height:20px;">
            <td style="text-align:left; width:20%;">
                Branch&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:30%;">
                    <asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" runat="server" 
                    Width="90%" MaxLength="50"
                        ReadOnly="True"></asp:TextBox>
                </td>
            <td style="text-align:left; width:20%;">
                Designation&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:30%;">
                    <asp:TextBox ID="txtDesignation" class="ReadOnlyTextBox" runat="server" 
                    Width="90%" MaxLength="50"
                        ReadOnly="True"></asp:TextBox>
                </td>
        </tr>
        <tr style="height:20px;">
            <td style="text-align:left; width:20%;">
                Date of Join&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:30%;">
                <asp:TextBox ID="txtDateofJoin" runat="server" class="NormalText" Width="30%"  ReadOnly="true"></asp:TextBox> <asp:CalendarExtender ID="CE1" runat="server" TargetControlID="txtDateofJoin" Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
            <td style="text-align:left; width:20%;">
               No of Trainings Attended&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:30%;">
                <asp:TextBox ID="txtNoOfTrainings" runat="server" MaxLength="2" Width="20%" style="text-align:center;"></asp:TextBox>
                </td>
        </tr>
        <tr style="height:20px;">
            <td style="text-align:left; width:20%;">
                Trainings Needed&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:30%;">
                <asp:RadioButton ID="rdbYes" runat="server" GroupName="rdb" Text="Yes" style="vertical-align:middle;" />
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rdbNo" runat="server" GroupName="rdb" Text="No" style="vertical-align:middle;"/>
            </td>
            <td style="text-align:left; width:20%;">
                &nbsp;</td>
            <td style="text-align:left; width:30%;">
                &nbsp;</td>
        </tr>
        <tr style="height:20px;">
            <td style="width:40%; text-align:right;" colspan="2">
                &nbsp;</td>
            <td style="width:60%; text-align:left;" colspan="2">
                    &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:right;" colspan="4">
                <asp:Panel ID="PnDisplay" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr >
            <td style="text-align:center;" colspan="4">
                &nbsp;</td>
        </tr>
         <tr>
          <td style="text-align:left;width:20%" >General Comments</td>
            <td style="text-align:center;vertical-align:middle " colspan="3">
                <asp:TextBox ID="txtGeneralComments" runat="server" Rows="3" 
                    TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align:right;" colspan="4">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;" colspan="4">
           <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="SAVE"  onclick="return btnSave_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="EXIT"   onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>
    <link href="../../Style/NewStyle.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
// <![CDATA[

       
        function initialize() {
            table_fill();
        }

        function romanize(num) {
            if (! +num)
                return false;
            var digits = String(+num).split(""),
        key = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM",
               "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",
               "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],
        roman = "",
        i = 3;
            while (i--)
                roman = (key[+digits.pop() + (i * 10)] || "") + roman;
            return Array(+digits.join("") + 1).join("M") + roman;
        }


        function table_fill() {
           
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div  style='width:100%; height:auto; margin: 0px auto; background-color:#F8A0AA ' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr style='height:30px; background-color:Maroon; color:#FFF; font-weight:bold;'>";
            tab += "<td style='width:5%;text-align:center;color:#FFF;' >#</td>";
            tab += "<td style='width:70%;text-align:left;color:#FFF;' >Traits</td>";
            tab += "<td style='width:25%;text-align:left;color:#FFF;'>Assessment</td>";
            tab += "</tr>";
            var CategoryID = 0;
            var CatCnt = 1;
            var Cnt = 1;
            if (document.getElementById("<%= hdnTraits.ClientID %>").value != "") {
                row = document.getElementById("<%= hdnTraits.ClientID %>").value.split("Ñ");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("ÿ");
                    if (CategoryID != col[0]) {
                        if (CategoryID != 0)
                            tab += "</div>";
                        tab += "<div id='div"+col[0]+"'>";
                        tab += "<tr  style='height:30px;text-align:center;  padding-left:20px;' class='mainhead'>";
                        tab += "<td style='width:5%;text-align:center;  font-weight:bold;' >" + romanize(CatCnt) + "</td>";
                        // tab += "<td colspan='2' style='width:70%;text-align:left; font-weight:bold;'>" + col[1] + "<span style='float:right;margin-right:10px;' ><img src='../../image/sort_desc.png' onclick='return HideDiv("+ col[0] +")' /></span></td>";
                        tab += "<td colspan='2' style='width:70%;text-align:left; font-weight:bold;'>" + col[1] +"</td>";
                        tab += "</tr>";
                        Cnt = 1;
                        CatCnt += 1;
                    }
                    CategoryID = col[0];
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr  style='height:25px;text-align:center; background-color:#f1f1f1; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr  style='height:25px;text-align:center;background-color:#f1f1f1; padding-left:20px;'>";
                    }
                    tab += "<td style='width:5%;text-align:center' >" + Cnt + "</td>";
                    tab += "<td style='width:70%;text-align:left'>" + col[3] + "</td>";
                    var select1 = CreateSelect(col[2]);
                    tab += "<td style='width:25%;text-align:left'>" + select1 + "</td>";
                    tab += "</tr>";
                    Cnt += 1;
                }
            }           
            tab += "</table></div>";
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;           

        }

        function HideDiv(ID) {         
            document.getElementById("div" + ID).style.display = "none";
        }
        function CreateSelect(id) {
            var select1 = "<div class='dropdown'><select id='cmbGrade" + id + "' class='NormalText' name='cmbGrade" + id + "' style='width:100%'  >";
            var rows = document.getElementById("<%= hdnGrade.ClientID %>").value.split("Ñ");
            select1 += "<option value='-1' selected=true>Select</option>";
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");                  
                        select1 += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
                }
                    
            select1 += "</select></div>"
            return select1;
        }


        function btnSave_onclick() {
            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            var DOJ = document.getElementById("<%= txtDateofJoin.ClientID %>").value;
            var NoOfTrainings = document.getElementById("<%= txtNoOfTrainings.ClientID %>").value;
            if (document.getElementById("<%= txtDateofJoin.ClientID %>").value == "")
            { alert("Enter Date Of Join"); document.getElementById("<%= txtDateofJoin.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtNoOfTrainings.ClientID %>").value == "")
            { alert("Enter No of Trainings Attendned"); document.getElementById("<%= txtNoOfTrainings.ClientID %>").focus(); return false; }
            var TrainingNeeded =1;
            if (document.getElementById("<%= rdbYes.ClientID %>").checked == false && document.getElementById("<%= rdbNo.ClientID %>").checked == false)
            { alert("Select Training Needed"); document.getElementById("<%= rdbYes.ClientID %>").focus(); return false; }
            if(document.getElementById("<%= rdbNo.ClientID %>").checked==true)
                TrainingNeeded=0;
            row = document.getElementById("<%= hdnTraits.ClientID %>").value.split("Ñ");
            document.getElementById("<%= hdnValue.ClientID %>").value = "";
             for (n = 1; n <= row.length - 1; n++) {
                 col = row[n].split("ÿ");
                 if (document.getElementById("cmbGrade" + col[2]).value == -1) {
                     alert("Select Assessment");                     
                     setTimeout(function () { document.getElementById("cmbGrade" + col[2]).focus(); }, 4);                    
                     return false;
                 }
                 else
                     document.getElementById("<%= hdnValue.ClientID %>").value += "Ñ" + col[2] + "ÿ" + document.getElementById("cmbGrade" + col[2]).value;
             }

             var GeneralComments = document.getElementById("<%= txtGeneralComments.ClientID %>").value
             ToServer("1ʘ" + EmpCode + "ʘ" + DOJ + "ʘ" + document.getElementById("<%= hdnValue.ClientID %>").value + "ʘ" + GeneralComments + "ʘ" + NoOfTrainings + "ʘ" + TrainingNeeded, 1);
         }

         function FromServer(Arg, Context) {
             switch (Context) {
                 case 1:
                     {
                         var Dtl = Arg.split("ʘ");
                         alert(Dtl[1]);
                         window.open("../../Home.aspx","_self");
                     }
             } 
         }

         function btnExit_onclick() {
             window.open("../../Home.aspx", "_self");
         }

// ]]>
    </script>
</asp:Content>

