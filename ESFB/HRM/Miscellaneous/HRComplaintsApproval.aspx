﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="HRComplaintsApproval.aspx.vb" Inherits="HRComplaintsApproval" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function table_fill() 
        {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:5%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:5%;text-align:center' >Emp&nbsp;Code</td>";
            tab += "<td style='width:15%;text-align:left'>Name</td>";
            tab += "<td style='width:10%;text-align:center'>Complaint&nbsp;Date</td>";
            tab += "<td style='width:25%;text-align:left'>Complaint</td>";
            tab += "<td style='width:10%;text-align:center'>Resolve</td>";
            tab += "<td style='width:10%;text-align:center'>Reject</td>";
            tab += "<td style='width:20%;text-align:center'>Remarks</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");

                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("ÿ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n ;
                    tab += "<td style='width:5%;text-align:center'>" + i + " </td>";
                    tab += "<td style='width:5%;text-align:center' >" + col[0] + "</td>";
                    tab += "<td style='width:15%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:10%;text-align:center'>" + col[2] + "</td>";
                    tab += "<td style='width:25%;text-align:left'>" + col[3] + "</td>";
                    var chkid = "chk" + n;    
                    var gpnm = "aa" + n               
                    tab += "<td style='width:10%;text-align:left'><input id='" + chkid + "' type='radio' name =" + gpnm + "  checked= 'true' onclick='return ResolveOnClick(" + n + ")' style='width:98%;'  /></td>";
                    var chkrid = "chkr" + n;
                    tab += "<td style='width:10%;text-align:center'><input id='" + chkrid + "' type='radio' name =" + gpnm + "  onclick='return RejectOnClick(" + n + ")' style='width:98%;'  /></td>";
                    var txtid = "txt" + n;
                    tab += "<td style='width:20%;text-align:center'><textarea id='" + txtid + "' type='text' style='width:98%;' name='S1' cols='20' rows='1' Disabled=true onkeypress='return TextAreaCheck(event)'  ></textarea></td>";
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }

        function ResolveOnClick(ID)
        { 
            if (document.getElementById("chk" + ID).checked == true)
            {
                document.getElementById("txt" + ID).disabled = true;
            }
            else
            {
                document.getElementById("txt" + ID).disabled = false;
            }
        }

        function RejectOnClick(ID)
        { 
            if (document.getElementById("chkr" + ID).checked == true)
            {
                document.getElementById("txt" + ID).disabled = false;
            }
            else
            {
                document.getElementById("txt" + ID).disabled = true;
            }
        }
        function UpdateValue() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            var StatusFlag;
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("Complaints does not exists");
                return false;
            }
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");
            for (n = 1; n <= row.length - 1; n++) 
            { 
                col = row[n].split("ÿ");
                if (document.getElementById("chkr" + n).checked == true) 
                {
                    StatusFlag = 2;
                    if (document.getElementById("txt" + n).value == "")
                        { alert("Enter Remarks"); document.getElementById("txt" + n).focus();return false ;  }
                }
                if (document.getElementById("chk" + n).checked == true)
                    StatusFlag = 0;
                document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[4] + "µ" + StatusFlag  + "µ" + document.getElementById("txt" + n).value;
            }
           
            return true;
        }

        function FromServer(Arg, Context) {
            var Data = Arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("HRComplaintsApproval.aspx", "_self");
        }
        function btnApprove_onclick() {
            var ret = UpdateValue();
            if (ret == 0) return false;
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
            {
                alert("Select Any Complaint for Approval");
                return false;
            }

            var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
            var Data = "1Ø" + strempcode;
            ToServer(Data, 1);
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
    </script>
</head>
</html>
<br />

       
    <table align="center" style="width: 70%;margin :0px auto;">
        <tr>
            <td>
                <asp:Panel ID="pnLeaveApproveDtl" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="VERIFY" onclick="return btnApprove_onclick()" />&nbsp; <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>

       
<br />
                <asp:HiddenField ID="hid_temp" runat="server" />
                <asp:HiddenField ID="hid_dtls" runat="server" />
                
</asp:Content>

