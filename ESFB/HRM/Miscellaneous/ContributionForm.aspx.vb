﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ContributionForm
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 121) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            DT = DB.ExecuteDataSet("select count(*) from contribution_master where branch_id=" + Session("BranchID").ToString() + " and department_ID=" + Session("DepartmentID").ToString()).Tables(0)
            If (CInt(DT.Rows(0)(0)) = 0) Then
                Me.Master.subtitle = "Contribution Form"
                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
                Me.hid_dtls.Value = ""
                Dim BranchName As String = GN.GetBranch_Name(CInt(Session("BranchID")))
                txtBranch.Text = BranchName.ToUpper()
                txtUser.Text = Session("UserName").ToString().ToUpper()
                CE1.EndDate = CDate(Session("TraDt"))
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
            Else
                Response.Redirect("AlreadyDone.aspx", False)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim ContributionDtls As String = Data(1)
            Dim TotalAmount As Double = CDbl(Data(2))
            Dim BankName As String = Data(3)
            Dim BankBranch As String = Data(4)
            Dim DepDt As DateTime = CDate(Data(5))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(9) As SqlParameter
                Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(0).Value = CInt(Session("BranchID"))
                Params(1) = New SqlParameter("@DepartmentID", SqlDbType.Int)
                Params(1).Value = CInt(Session("DepartmentID"))
                Params(2) = New SqlParameter("@ContributionDtl", SqlDbType.VarChar)
                Params(2).Value = ContributionDtls.Substring(1)
                Params(3) = New SqlParameter("@TotalAmount", SqlDbType.VarChar)
                Params(3).Value = TotalAmount
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = CInt(Session("UserID"))
                Params(5) = New SqlParameter("@BankName", SqlDbType.VarChar, 200)
                Params(5).Value = BankName
                Params(6) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 200)
                Params(6).Value = BankBranch
                Params(7) = New SqlParameter("@DepositDt", SqlDbType.DateTime)
                Params(7).Value = DepDt
                Params(8) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(8).Direction = ParameterDirection.Output
                Params(9) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(9).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_CONTRIBUTION", Params)
                ErrorFlag = CInt(Params(8).Value)
                Message = CStr(Params(9).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            DT = DB.ExecuteDataSet("select emp_name from emp_master where emp_code=" + Data(1)).Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn = DT.Rows(0)(0).ToString + "Ø" + Data(2)
            Else
                CallBackReturn = "Ø" + Data(2)
            End If
        End If
    End Sub
End Class
