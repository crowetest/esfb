﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewContributionReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim MonthDesc As String
    Dim subHead As String = ""
    Dim GN As New GeneralFunctions
    Dim DT As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 122) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim PostID As Integer = Session("Post_ID")
            Dim SubHD As String = ""
            Dim LocationID As Integer
            Dim ContributionID As Integer
            If PostID = 5 Then
                DT = DB.ExecuteDataSet("select contribution_id from contribution_master where branch_id=" + Session("BranchID")).Tables(0)
                If DT.Rows.Count > 0 Then
                    ContributionID = DT.Rows(0)(0)
                Else
                    ContributionID = 0
                End If
                Response.Redirect("viewContribution_Individual.aspx?ContributionID=" + GN.Encrypt(ContributionID.ToString()), False)
            ElseIf PostID = 6 Then
                LocationID = GN.GetArea_ID(CInt(Session("UserID")))
                SubHD = GN.GetArea_Name(LocationID) + " Area"
            ElseIf PostID = 7 Then
                LocationID = GN.GetRegion_ID(CInt(Session("UserID")))
                SubHD = GN.GetRegion_Name(LocationID) + " Region"
            End If

            RH.Heading(Session("FirmName"), tb, "'GIFT FOR A SMILE' CONTRIBUTION REPORT " + SubHD, 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0


            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 30, 30, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 15, 15, "l", "Updated By")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "r", "Amount")
            RH.AddColumn(TRHead, TRHead_04, 15, 15, "l", "Bank")
            RH.AddColumn(TRHead, TRHead_05, 15, 15, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "Date")
            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 5)      
            Dim sql As String = "select Contribution_ID,case when a.Branch_ID<5 then ' ' +b.Branch_Name+'/'+c.Department_Name else  b.Branch_Name end as Branch ,d.Emp_Name,a.Total_Amount,a.bank_Name,a.bank_branch ,a.Deposit_date  from Contribution_Master a,BrMaster b,DEPARTMENT_MASTER c,EMP_MASTER d where a.Branch_ID=b.Branch_ID and a.Department_ID=c.Department_ID and a.User_ID=d.Emp_Code  "
            If PostID = 6 Then
                sql += " and b.area_head=" + Session("UserID").ToString()
            ElseIf PostID = 7 Then
                sql += " and b.region_head=" + Session("UserID").ToString()
            ElseIf PostID = 8 Then
                sql += " and b.zone_head=" + Session("UserID").ToString()
            End If
            sql += " order by 2"
            DT = DB.ExecuteDataSet(sql).Tables(0)

            Dim Total As Double = 0
            For Each DR In DT.Rows


                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid


                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid

                I = I + 1
                RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString().ToString())
                RH.AddColumn(TR3, TR3_01, 30, 30, "l", DR(1).ToString().ToUpper())
                RH.AddColumn(TR3, TR3_02, 15, 15, "l", DR(2).ToString().ToUpper())
                RH.AddColumn(TR3, TR3_03, 10, 10, "r", "<a href='viewContribution_Individual.aspx?ContributionID=" + GN.Encrypt(DR(0).ToString()) + "' target='blank'>" + CDbl(DR(3)).ToString("#####.00"))
                RH.AddColumn(TR3, TR3_04, 15, 15, "l", DR(4).ToString().ToUpper())
                RH.AddColumn(TR3, TR3_05, 15, 15, "l", DR(5).ToString().ToUpper())
                RH.AddColumn(TR3, TR3_06, 10, 10, "l", CDate(DR(6)).ToString("dd MMM yyyy"))
                tb.Controls.Add(TR3)
                Total += CDbl(DR(3))
            Next
            Dim TrEnd As New TableRow
            TrEnd.BackColor = Drawing.Color.WhiteSmoke
            Dim TrEnd_00, TrEnd_01, TrEnd_02, TrEnd_03, TrEnd_04, TrEnd_05, TrEnd_06 As New TableCell

            TrEnd_00.BorderWidth = "1"
            TrEnd_01.BorderWidth = "1"
            TrEnd_02.BorderWidth = "1"
            TrEnd_03.BorderWidth = "1"
            TrEnd_04.BorderWidth = "1"
            TrEnd_05.BorderWidth = "1"
            TrEnd_06.BorderWidth = "1"

            TrEnd_00.BorderColor = Drawing.Color.Silver
            TrEnd_01.BorderColor = Drawing.Color.Silver
            TrEnd_02.BorderColor = Drawing.Color.Silver
            TrEnd_03.BorderColor = Drawing.Color.Silver
            TrEnd_04.BorderColor = Drawing.Color.Silver
            TrEnd_05.BorderColor = Drawing.Color.Silver
            TrEnd_06.BorderColor = Drawing.Color.Silver

            TrEnd_00.BorderStyle = BorderStyle.Solid
            TrEnd_01.BorderStyle = BorderStyle.Solid
            TrEnd_02.BorderStyle = BorderStyle.Solid
            TrEnd_03.BorderStyle = BorderStyle.Solid
            TrEnd_04.BorderStyle = BorderStyle.Solid
            TrEnd_05.BorderStyle = BorderStyle.Solid
            TrEnd_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TrEnd, TrEnd_00, 5, 5, "c", "")
            RH.AddColumn(TrEnd, TrEnd_01, 30, 30, "l", "TOTAL")
            RH.AddColumn(TrEnd, TrEnd_02, 15, 15, "l", "")
            RH.AddColumn(TrEnd, TrEnd_03, 10, 10, "r", "<a href='viewContribution_Individual.aspx?ContributionID=" + GN.Encrypt("-1") + "' target='blank'>" + (Total).ToString("####.00") + "</a>")
            RH.AddColumn(TrEnd, TrEnd_04, 15, 15, "l", "")
            RH.AddColumn(TrEnd, TrEnd_05, 15, 15, "l", "")
            RH.AddColumn(TrEnd, TrEnd_06, 10, 10, "l", "")
            tb.Controls.Add(TrEnd)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
