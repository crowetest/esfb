﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AlreadyDone.aspx.vb" Inherits="HRM_Miscellaneous_AlreadyDone" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <table align="center" style="width: 70%; margin:0px auto;">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;">
                 <asp:Image ID="Image2" runat="server" BorderColor="#CC0000" BorderStyle="none" 
                    BorderWidth="1px" Height="100px" ImageUrl="~/Image/Gift.gif" /></td>
        </tr>
        <tr>
            <td style="text-align:center;">
                 &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center; font-family: 'Arial Black'; font-size: large; color: #CCCCCC;">
                <span style="color: #990000">CONTRIBUTION DETAILS ALREADY UPDATED FOR YOUR BRANCH/DEPARTMENT</span>&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;">
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
         window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

// ]]>
    </script>
</asp:Content>

