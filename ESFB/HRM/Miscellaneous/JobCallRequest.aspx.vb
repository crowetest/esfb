﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class JobCallRequest
    Inherits System.Web.UI.Page
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DT = DB.ExecuteDataSet("SELECT * FROM EMFIL.DBO.JOB_CALL WHERE EMP_CODE = " & CInt(Session("UserID")) & "").Tables(0)
            If DT.Rows.Count > 0 Then
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_script1.Append("alert('You Have Completed Already');")
                cl_script1.Append("window.open('../../home.aspx','_self');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
                Exit Sub
            End If
            Me.txtEmpCode.Text = Session("UserID").ToString() + " - " + Session("UserName")
            Me.Master.subtitle = "Job Call"
            If Not IsPostBack Then
                DT = DB.ExecuteDataSet("SELECT -1 AS FIRM_ID,' SELECT' AS FIRM_ABBR UNION ALL SELECT FIRM_ID,FIRM_ABBR FROM EMFIL.DBO.FIRM_MASTER  ORDER BY FIRM_ABBR ").Tables(0)
                GN.ComboFill(cmbEntity, DT, 0, 1)
                DT = DB.ExecuteDataSet("SELECT -1,' SELECT',-1 AS ORDER_ID UNION ALL SELECT QUALIFICATION_ID,QUALIFICATION_NAME,ORDER_ID FROM EMFIL.DBO.QUALIFICATION_MASTER ORDER BY ORDER_ID").Tables(0)
                GN.ComboFill(cmbQualification1, DT, 0, 1)
                GN.ComboFill(cmbQualification2, DT, 0, 1)
                GN.ComboFill(cmbQualification3, DT, 0, 1)
            End If
            Me.btnSave.Attributes.Add("onclick", "return btnSave_onclick()")

        Catch ex As Exception
           
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Save"
    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim EmpCode As Integer = CInt(Session("UserID"))
        Dim PresentDsg As String = Me.txtPresentDsg.Text
        Dim Entity As Integer = CInt(Me.cmbEntity.SelectedValue)
        Dim DOB As Date = CDate(Me.txtDOB.Text)
        Dim PresentBranch As String = Me.txtBranch.Text
        Dim DOJ As Date = CDate(Me.txtDOJ.Text)
        Dim JoinCadre As String = Me.txtJoinCadre.Text
        Dim JoinDsg As String = Me.txtJoinDsg.Text
        Dim PromotionCount As Integer = CInt(Me.txtNoofPromotion.Text)
        Dim LastPromotionDt As Date = Nothing
        If PromotionCount > 0 Then
            LastPromotionDt = CDate(Me.txtLastPromotionDt.Text)
        End If

        Dim Qualification1 As Integer = CInt(Me.cmbQualification1.Text)
        Dim Qualification2 As Integer = CInt(Me.cmbQualification2.Text)
        Dim Qualification3 As Integer = CInt(Me.cmbQualification3.Text)
        Dim TechQualification As String = Me.txtTechQualification.Text
        Dim EsafExp As Integer = CInt(Me.txtEsafExp.Text)
        Dim OutsideExp As Integer = CInt(Me.txtOutsideExp.Text)
        Dim PostApplied As String = Me.cmbPostApplied.SelectedValue
        Dim PreferLocation As String = Me.txtPreferLocation.Text
        Dim CurrentProfile As String = Me.txtCurrentProfile.Text
        Dim Languages As String = Me.txtLanguages.Text
        Dim WorkDetails As String = Me.txtWorkDetails.Text
        Dim ImprovementAreas As String = Me.txtImprovementAreas.Text
        Dim RelocateFlag As Integer = 0
        If Me.rbtnYes.Checked = True Then
            RelocateFlag = 0
        Else
            RelocateFlag = 1
        End If
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try
            Dim Params(24) As SqlParameter
            Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(0).Value = EmpCode
            Params(1) = New SqlParameter("@Entity", SqlDbType.Int)
            Params(1).Value = Entity
            Params(2) = New SqlParameter("@DOB", SqlDbType.Date)
            Params(2).Value = DOB
            Params(3) = New SqlParameter("@Location", SqlDbType.VarChar, 50)
            Params(3).Value = PresentBranch
            Params(4) = New SqlParameter("@DOJ", SqlDbType.Date)
            Params(4).Value = DOJ
            Params(5) = New SqlParameter("@JoinDsg", SqlDbType.VarChar, 50)
            Params(5).Value = JoinDsg
            Params(6) = New SqlParameter("@JoinCadre", SqlDbType.VarChar, 50)
            Params(6).Value = JoinCadre
            Params(7) = New SqlParameter("@PresDsg", SqlDbType.VarChar, 50)
            Params(7).Value = PresentDsg
            Params(8) = New SqlParameter("@Promotions", SqlDbType.Int)
            Params(8).Value = PromotionCount
            Params(9) = New SqlParameter("@LastPromDt", SqlDbType.Date)
            Params(9).Value = LastPromotionDt
            Params(10) = New SqlParameter("@Qual1", SqlDbType.Int)
            Params(10).Value = Qualification1
            Params(11) = New SqlParameter("@Qual2", SqlDbType.Int)
            Params(11).Value = Qualification2
            Params(12) = New SqlParameter("@Qual3", SqlDbType.Int)
            Params(12).Value = Qualification3
            Params(13) = New SqlParameter("@TechQual", SqlDbType.VarChar, 50)
            Params(13).Value = TechQualification
            Params(14) = New SqlParameter("@EsafExp", SqlDbType.Int)
            Params(14).Value = EsafExp
            Params(15) = New SqlParameter("@OutExp", SqlDbType.Int)
            Params(15).Value = OutsideExp
            Params(16) = New SqlParameter("@AppliedPost", SqlDbType.VarChar, 50)
            Params(16).Value = PostApplied
            Params(17) = New SqlParameter("@AppliedLoc", SqlDbType.VarChar, 50)
            Params(17).Value = PreferLocation
            Params(18) = New SqlParameter("@Relocate", SqlDbType.Int)
            Params(18).Value = RelocateFlag
            Params(19) = New SqlParameter("@Languages", SqlDbType.VarChar, 200)
            Params(19).Value = Languages
            Params(20) = New SqlParameter("@JobProfile", SqlDbType.VarChar, 200)
            Params(20).Value = CurrentProfile
            Params(21) = New SqlParameter("@ExcelledAreas", SqlDbType.VarChar, 200)
            Params(21).Value = WorkDetails
            Params(22) = New SqlParameter("@ImproveAreas", SqlDbType.VarChar, 200)
            Params(22).Value = ImprovementAreas
            Params(23) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(23).Direction = ParameterDirection.Output
            Params(24) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(24).Direction = ParameterDirection.Output
            DB.ExecuteNonQuery("EMFIL.DBO.SP_JOB_CALL", Params)
            ErrorFlag = CInt(Params(23).Value)
            Message = CStr(Params(24).Value)

        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("alert('" + Message + "');")
        cl_script1.Append("window.open('../../home.aspx','_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    End Sub
#End Region
End Class
