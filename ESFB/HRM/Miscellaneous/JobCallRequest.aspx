﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="JobCallRequest.aspx.vb" Inherits="JobCallRequest"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server" >
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
     <script language="javascript" type="text/javascript">
        
        function btnSave_onclick() {
            var PresentDsg          = document.getElementById("<%= txtPresentDsg.ClientID %>").value;
            if(PresentDsg == "")
            {alert("Enter Present Designation");document.getElementById("<%= txtPresentDsg.ClientID %>").focus();return false;}

            var Entity              = document.getElementById("<%= cmbEntity.ClientID %>").value;
            if(Entity == -1)
            {alert("Select Entity");document.getElementById("<%= cmbEntity.ClientID %>").focus();return false;}

            var DOB                 = document.getElementById("<%= txtDOB.ClientID %>").value;
            if(DOB == "")
            {alert("Select Date of Birth");document.getElementById("<%= txtDOB.ClientID %>").focus();return false;}

            var PresentBranch       = document.getElementById("<%= txtBranch.ClientID %>").value;
            if(PresentBranch == "")
            {alert("Enter Present Branch");document.getElementById("<%= txtBranch.ClientID %>").focus();return false;}

            var DOJ                 = document.getElementById("<%= txtDOJ.ClientID %>").value;
            if(DOJ == "")
            {alert("Select Date of Join");document.getElementById("<%= txtDOJ.ClientID %>").focus();return false;}

            var JoinCadre           = document.getElementById("<%= txtJoinCadre.ClientID %>").value;
            if(JoinCadre == "")
            {alert("Enter Cadre at Joining");document.getElementById("<%= txtJoinCadre.ClientID %>").focus();return false;}

            var PromotionCount      = document.getElementById("<%= txtNoofPromotion.ClientID %>").value;
            if(PromotionCount == "")
            {alert("Enter No of Promotions");document.getElementById("<%= txtNoofPromotion.ClientID %>").focus();return false;}
            if(PromotionCount > 0){
                var LastPromotionDt     = document.getElementById("<%= txtLastPromotionDt.ClientID %>").value;
                if(LastPromotionDt == "")
                {alert("Select Last Promotion Date");document.getElementById("<%= txtLastPromotionDt.ClientID %>").focus();return false;}
            }
            var Qualification1      = document.getElementById("<%= cmbQualification1.ClientID %>").value;
            var Qualification2      = document.getElementById("<%= cmbQualification2.ClientID %>").value;
            var Qualification3      = document.getElementById("<%= cmbQualification3.ClientID %>").value;
            if(Qualification1 == -1 && Qualification2 == -1 && Qualification3 == -1)
            {alert("Select Qualification");document.getElementById("<%= cmbQualification1.ClientID %>").focus();return false;}

            var TechQualification   = document.getElementById("<%= txtTechQualification.ClientID %>").value;   
            if(TechQualification == -1)
            {alert("Enter Technical Qualification");document.getElementById("<%= txtTechQualification.ClientID %>").focus();return false;}  
                   
            var EsafExp             = document.getElementById("<%= txtEsafExp.ClientID %>").value;    
            if(EsafExp == "")
            {alert("Enter Experience in ESAF");document.getElementById("<%= txtEsafExp.ClientID %>").focus();return false;} 
                   
            var OutsideExp          = document.getElementById("<%= txtOutsideExp.ClientID %>").value; 
            if(OutsideExp == "")
            {alert("Enter Experience outside ESAF");document.getElementById("<%= txtOutsideExp.ClientID %>").focus();return false;}
                       
            var PostApplied         = document.getElementById("<%= cmbPostApplied.ClientID %>").value;   
            var PreferLocation      = document.getElementById("<%= txtPreferLocation.ClientID %>").value; 
            if(PreferLocation == "")
            {alert("Enter Preferred Location");document.getElementById("<%= txtPreferLocation.ClientID %>").focus();return false;}
                       
            var CurrentProfile      = document.getElementById("<%= txtCurrentProfile.ClientID %>").value;       
            if(CurrentProfile == -1)
            {alert("Enter Current Profile");document.getElementById("<%= txtCurrentProfile.ClientID %>").focus();return false;}  
               
            var Languages           = document.getElementById("<%= txtLanguages.ClientID %>").value;    
            if(Languages == "")
            {alert("Enter Languages Known");document.getElementById("<%= txtLanguages.ClientID %>").focus();return false;}  
                  
            var WorkDetails         = document.getElementById("<%= txtWorkDetails.ClientID %>").value; 
            if(WorkDetails == -1)
            {alert("Enter the areas you excelled in your work");document.getElementById("<%= txtWorkDetails.ClientID %>").focus();return false;}
                       
            var ImprovementAreas    = document.getElementById("<%= txtImprovementAreas.ClientID %>").value; 
            if(ImprovementAreas == "")
            {alert("Enter Improvement Areas");document.getElementById("<%= txtImprovementAreas.ClientID %>").focus();return false;}  

            var RelocateFlag        = 0;
            var JoinDsg             = document.getElementById("<%= txtJoinDsg.ClientID %>").value;
            if(JoinDsg == "")
            {alert("Enter Joining Designation");document.getElementById("<%= txtJoinDsg.ClientID %>").focus();return false;}
             
            if (document.getElementById("<%= rbtnYes.ClientID %>").checked == true)  RelocateFlag = 1; else RelocateFlag = 0;  
            if (document.getElementById("<%= rbtnNo.ClientID %>").checked == true)   RelocateFlag = 0; else RelocateFlag = 1;                 
        }

        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
        }
        
    </script>
    </head>
</html>
<br />
    <table align="center" 
        style="width:80%; margin: 0px auto;">
        <tr>
            <td colspan="4">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Employee</td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpCode" 
                    runat="server" Width="91%" class="ReadOnlyTextBox"  MaxLength="100" 
                     ReadOnly="True"></asp:TextBox>
            </td>
            <td style="width: 20%; text-align: left;">
                Present Designation</td>
            <td style="width: 30%;" class="Displays">
                &nbsp;&nbsp;<asp:TextBox ID="txtPresentDsg" 
                    runat="server" class="NormalText" Width="91%" MaxLength="50"></asp:TextBox>
                        </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Entity</td>
            <td style="width: 30%;" class="Displays">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbEntity" runat="server" class="NormalText" Width="91%">  </asp:DropDownList>
            </td>
            <td style="width: 20%; text-align: left;" class="Displays">
                Date of Birth
            </td>
            <td style="width: 30%;" class="Displays">
                &nbsp;&nbsp;<asp:TextBox ID="txtDOB" runat="server" Width="91%" 
                    class="NormalText"  MaxLength="11" ></asp:TextBox>
                     <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDOB" Format="dd-MMM-yyyy"> </asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Present Branch/Area</td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:TextBox ID="txtBranch" 
                    runat="server" class="NormalText" Width="91%" MaxLength="50"></asp:TextBox>
            </td>
            <td style="width: 20%; text-align: left;">
                Date of Join</td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:TextBox ID="txtDOJ" runat="server" Width="91%" 
                    class="NormalText"  MaxLength="11" ></asp:TextBox>
                     <asp:CalendarExtender ID="CE1" runat="server" TargetControlID="txtDOJ" Format="dd-MMM-yyyy"> </asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Designation at Joining
            </td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:TextBox ID="txtJoinDsg" 
                    runat="server" class="NormalText" Width="91%" MaxLength="50"></asp:TextBox>
               
            </td>
            <td style="width: 20%; text-align: left;">
                Cadre at Joining</td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:TextBox ID="txtJoinCadre" runat="server" Width="91%" 
                    class="NormalText"  MaxLength="50" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                No of Promotions Received</td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:TextBox ID="txtNoofPromotion" runat="server" Width="91%" 
                    class="NormalText"  MaxLength="2" onkeypress="return NumericCheck(event)"></asp:TextBox>
            </td>
            <td style="width: 20%; text-align: left;">
                Date of Last Promotion</td>
            <td style="width: 30%; text-align: left;">
                &nbsp;
                <asp:TextBox ID="txtLastPromotionDt" runat="server" Width="91%" 
                    class="NormalText"  MaxLength="11" ></asp:TextBox>
                 <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtLastPromotionDt" Format="dd-MMM-yyyy"> </asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Qualification 1</td>
            <td style="width: 30%;">
                &nbsp; <asp:DropDownList ID="cmbQualification1" runat="server" class="NormalText" Width="91%">  </asp:DropDownList>
            </td>
            <td style="width: 20%; text-align: left;">
                Qualification 2</td>
            <td style="width: 30%; text-align: left;">
                &nbsp;
            <asp:DropDownList ID="cmbQualification2" runat="server" class="NormalText" 
                    Width="91%">  </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Qualification 3</td>
            <td style="width: 30%;">
                &nbsp; <asp:DropDownList ID="cmbQualification3" runat="server" class="NormalText" Width="91%">  </asp:DropDownList>
                &nbsp;</td>
            <td style="width: 20%; text-align: left;">
                Technical Qualification</td>
            <td style="width: 30%; text-align: left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtTechQualification" runat="server" Width="91%" 
                    class="NormalText"  MaxLength="50" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Experience in ESAF</td>
            <td style="width: 30%;">
                &nbsp; <asp:TextBox ID="txtEsafExp" runat="server" 
                    Width="91%" class="NormalText"  
                    MaxLength="2" onkeypress="return NumericCheck(event)"></asp:TextBox>
                &nbsp;</td>
            <td style="width: 20%; text-align: left;">
                Experience outside ESAF</td>
            <td style="width: 30%; text-align: left;">
                &nbsp; <asp:TextBox ID="txtOutsideExp" runat="server" 
                    Width="91%" class="NormalText"  
                    MaxLength="2" onkeypress="return NumericCheck(event)"></asp:TextBox>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Post Applied</td>
            <td style="width: 30%;">
                &nbsp; <asp:DropDownList ID="cmbPostApplied" runat="server" class="NormalText" Width="91%">
                    <asp:ListItem>AM</asp:ListItem>
                    <asp:ListItem>RM</asp:ListItem>
                </asp:DropDownList>
                &nbsp;</td>
            <td style="width: 20%; text-align: left;">
                Preferred Location</td>
            <td style="width: 30%; text-align: left;">
                &nbsp; <asp:TextBox ID="txtPreferLocation" runat="server" 
                    Width="91%" class="NormalText"  
                    MaxLength="50" ></asp:TextBox>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Willing to Relocate</td>
            <td style="width: 30%;">
                &nbsp;&nbsp;<asp:RadioButton ID="rbtnYes" runat="server" GroupName="aa" 
                    Text="Yes" Checked="True" />
&nbsp;&nbsp;
                <asp:RadioButton ID="rbtnNo" runat="server" GroupName="aa" Text="No" />
            </td>
            <td style="width: 20%; text-align: left;">
                &nbsp;</td>
            <td style="width: 30%; text-align: left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Current Job Profile</td>
            <td colspan="3">
                <asp:TextBox ID="txtCurrentProfile" runat="server" Width="100%" class="NormalText"  
                    MaxLength="200"  
                    TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Languages Known(Read/Write/Speak)</td>
            <td colspan="3">
                <asp:TextBox ID="txtLanguages" runat="server" Width="100%" class="NormalText"  
                    MaxLength="200" 
                    TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Any two areas you excelled in your work</td>
            <td colspan="3">
                
                <asp:TextBox ID="txtWorkDetails" runat="server" Width="100%" class="NormalText"  
                    MaxLength="200" 
                    TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: left;">
                Any two areas you need improvement</td>
            <td colspan="3">
                <asp:TextBox ID="txtImprovementAreas" runat="server" Width="100%" class="NormalText"  
                    MaxLength="200" 
                    TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
            </td>
        </tr>
        <tr style="text-align: center;">
            <td colspan="4" style="text-align: center;">
                &nbsp;</td>
        </tr>
        <tr style="text-align: center;">
            <td colspan="4" style="text-align: center;">
                <asp:Button ID="btnSave" runat="server" Text="SAVE" class="NormalText" Width="5%" />
                &nbsp;<input id="btnExit"
                        type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria;
                        width: 5%; cursor: pointer;" />
            </td>
        </tr>
    </table><br />
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
</script>
    <asp:HiddenField ID="hdnData" runat="server" />
</asp:Content>

