﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ContributionForm.aspx.vb" Inherits="ContributionForm" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }        
        .style1
        {
            width: 100%;
        }
        .style2
        {
            height: 25px;
            font-size: medium;
            color: Maroon;
            border-style: none;
            border-color: inherit;
            border-width: 9px;
            background-color: #FFFF;
        }
        .style3
        {
            font-size: medium;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function table_fill() 
        {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div id='ScrollDiv' style='width:90%; height:auto; overflow:auto;margin: 0px auto; max-height:208px;' class=mainhead>";          
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px; style='font-family:'cambria';'>";      
            tab += "<td style='width:10%;text-align:center; vertical-align:middle' >#</td>";            
            tab += "<td style='width:15%;text-align:left; vertical-align:middle' >Emp Code</td>";
            tab += "<td style='width:50%;text-align:left; vertical-align:middle' >Name</td>";
            tab += "<td style='width:20%;text-align:right; vertical-align:middle'>Amount</td>";   
             tab += "<td style='width:5%;text-align:left; vertical-align:middle' ></td>";
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";  
            var TotalAmount=0;  
            var n=0;  
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {          
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");              
              
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    tab += "<tr class='sub_first'; style='text-align:center; height:21px; padding-left:20px;'>";                  
                    i = n + 1;                                      
                    tab += "<td style='width:10%;text-align:center' >" + i  + "</td>";
                    var txtBox = "<input id='txtEmpCode" + i + "' name='txtEmpCode" + i + "' type='Text' style='width:99%; text-align:left;' class='NormalText'   onkeypress='NumericCheck(event)' readonly=true  value="+col[0]+" >";                 
                    tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>"; 
                   // alert(col[1]);
                    var txtBox1 = "<input id='txtName" + i + "' name='txtName" + i + "' type='Text' style='width:99%; text-align:left;' class='NormalText'  onkeypress='NumericCheck(event)' readonly=true  value='"+col[1]+"' >";  
                    tab += "<td style='width:50%;text-align:left'>" + txtBox1 + "</td>";  
                    var txtBox2 = "<input id='txtAmount" + i + "' name='txtAmount" + i  + "' type='Text' style='width:99%; text-align:right;'  class='NormalText'  onkeypress='NumericCheck(event)' readonly=true  value="+col[2]+" >";  
                    tab += "<td style='width:20%;text-align:center'>"+  txtBox2 +"</td>";  
                    tab += "<td style='width:5%;text-align:center' onclick=DeleteRow('" + n + "')><img  src='../../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'></td>"; 
                    tab += "</tr>";
                    TotalAmount+=parseFloat(col[2]);
                }  
               }                
                 tab += "<tr class='sub_first'; style='text-align:center; height:21px; padding-left:20px;'>";
                    i = n + 1;                                      
                    tab += "<td style='width:10%;text-align:center' >" + i  + "</td>";
                    var txtBox = "<input id='txtEmpCode" + i  + "' name='txtEmpCode" + i   + "' type='Text' style='width:99%; text-align:left;' class='NormalText'   maxlength='6' onkeypress='NumericCheck(event)' onchange='EmpCodeOnBlur("+ i +")' >";                 
                    tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>"; 
                    var txtBox1 = "<input id='txtName" + i  + "' name='txtName" + i   + "' type='Text' style='width:99%; text-align:left;' class='NormalText'  maxlength='30'  readonly=true '   >";                 
                    tab += "<td style='width:50%;text-align:left'>" + txtBox1 + "</td>";   
                    var txtBox2 = "<input id='txtAmount" + i  + "' name='txtAmount" + i   + "' type='Text' style='width:99%; text-align:right;' class='NormalText'  maxlength='5' onkeypress='NumericCheck(event)' onkeydown='CreateNewRow(event," + i + ")'  >";                 
                    tab += "<td style='width:20%;text-align:center'>"+ txtBox2 +"</td>";  
                    tab += "<td style='width:5%;text-align:center'></td>";  
                    tab += "</tr>";           
            tab += "</table></div></div>";         
          
            tab += "<div style='width:90%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr class='mainhead'; style='text-align:center; height:21px; padding-left:20px;'>";                 
            tab += "<td style='width:10%;text-align:center' ></td>";
            tab += "<td style='width:15%;text-align:center' ></td>";
            tab += "<td style='width:50%;text-align:left background-color:white'>TOTAL</td>";  
            // var txtBox = "<input id='txtTotalAmount' name='txtTotalAmount' type='Text' style='width:99%; text-align:right; font-weight:bold; color:black'  maxlength='3' disabled=true  value="+ TotalAmount +">";                 
            tab += "<td style='width:20%;text-align:right'>"+ TotalAmount +"</td>";
            tab += "<td style='width:5%;text-align:right'></td>";           
            tab += "</tr>";
            tab +="</div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
             setTimeout(function() {
                 document.getElementById("txtEmpCode"+i).focus().select();
                }, 4);
           
            
            //--------------------- Clearing Data ------------------------//


        }
         function DeleteRow(id)
        {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var NewStr=""

                for (n = 0; n <= row.length-1 ; n++) {
                   if(id!=n)         
                   {       
                     if(NewStr!="" )
                        NewStr+="¥";
                     NewStr+=row[n];   
                   }                
                
                }
                document.getElementById("<%= hid_dtls.ClientID %>").value=NewStr;               
                table_fill();
        }
        function EmpCodeOnBlur(ID)
        {
            var EmpCode=document.getElementById("txtEmpCode"+ID).value;
            if (EmpCode!="")
            {
            var ToData = "2Ø" + EmpCode+"Ø"+ID;                  
            ToServer(ToData, 2);
            }
            else{
                document.getElementById("txtEmpCode"+ID).focus();
            }
        }

        function CalcTotal()
        {
            var row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            var TotalDays=0;          
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    TotalDays+=parseFloat(document.getElementById("txtAmount" + i).value);
                }   
                document.getElementById("txtTotalAmount").value=  TotalAmount;          
        }
       
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }


        function CreateNewRow(e, val) {
        var n = (window.Event) ? e.which : e.keyCode;
        if (n == 13 || n == 9) {
            updateValue(val);
            table_fill();                          
        }
        }

        function updateValue(id)
        {       
        var EmpCode=document.getElementById("txtEmpCode"+id).value;    
        var EmpName=document.getElementById("txtName"+id).value;
        if(EmpCode=="")
        {alert("Enter Emp Code");document.getElementById("txtEmpCode"+id).focus(); return false;}
        var Amount=document.getElementById("txtAmount"+id) .value;   
        if(Amount=="")
        {alert("Enter Amount");document.getElementById("txtAmount"+id).focus(); return false;}  
        Amount=parseFloat(Amount);
        if(Amount==0)
        {alert("Enter Amount");document.getElementById("txtAmount"+id).focus(); return false;}  
        if( document.getElementById("<%= hid_dtls.ClientID %>").value!=""){ 
        row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");       
        for (n = 0; n <= row.length - 1; n++) {
        col = row[n].split("µ");                 
        i = n + 1;                                      
        if(col[0]==EmpCode)
        {alert("Already Added");document.getElementById("txtEmpCode"+id).focus();return false;}                  
       
        } 
        }                 
        if (document.getElementById("<%= hid_dtls.ClientID %>").value!="")
        document.getElementById("<%= hid_dtls.ClientID %>").value+="¥"
        document.getElementById("<%= hid_dtls.ClientID %>").value+=EmpCode+"µ"+EmpName+"µ"+Amount;  
           
              
        }
        function FromServer(arg, context) {
         switch (context) {
                case 1:
                    var Data=arg.split("Ø");
                    alert(Data[1]);
                    if(Data[0]==0) {window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");}
                    break;                    
                case 2:
                    var Data=arg.split("Ø");    
                    if(Data[0]=="")               
                    {alert("Invalid Employee Code");document.getElementById("txtEmpCode"+Data[1]).value=""; document.getElementById("txtName"+Data[1]).value=""; document.getElementById("txtAmount"+Data[1]).value=""; document.getElementById("txtEmpCode"+Data[1]).focus(); return false;}
                    document.getElementById("txtName"+Data[1]).value=Data[0];
                    document.getElementById("txtAmount"+Data[1]).focus();
                    break;
                    }
               }
function btSave_onclick() {
            document.getElementById("<%= hid_value.ClientID %>").value="";
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            var TotalAmount=0;
            for (n = 0; n <= row.length - 1; n++) {
            i = n + 1;  
            col = row[n].split("µ");                                    
            document.getElementById("<%= hid_value.ClientID %>").value+="¥"+ col[0]+"µ"+ col[2] ;
            TotalAmount+=parseFloat(col[2]);
            }
            if(document.getElementById("<%= txtBank.ClientID %>").value=="")
            {alert("Enter Bank");document.getElementById("<%= txtBank.ClientID %>").focus();return false;}
            if(document.getElementById("<%= txtBankBranch.ClientID %>").value=="")
            {alert("Enter Branch of Bank");document.getElementById("<%= txtBankBranch.ClientID %>").focus();return false;}
            if(document.getElementById("<%= txtDate.ClientID %>").value=="")
            {alert("Select Date");document.getElementById("<%= txtDate.ClientID %>").focus();return false;}
            var ToData = "1Ø" + document.getElementById("<%= hid_value.ClientID %>").value+"Ø"+TotalAmount+"Ø"+ document.getElementById("<%= txtBank.ClientID %>").value+"Ø"+document.getElementById("<%= txtBankBranch.ClientID %>").value+"Ø"+document.getElementById("<%= txtDate.ClientID %>").value;                   
            ToServer(ToData, 1);
}

    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_Value" runat="server" />
<br />
    <div style="border: medium solid #A5A09F; width:60%; height:auto; overflow:auto; margin: 0px auto;"  >
    <table  align="center" style="width: 100%;text-align:right; margin:0px auto; overflow:none;" >
        
        <tr> 
            <td style="text-align:center; border-bottom-style: solid; border-bottom-width: medium; border-bottom-color: #A5A09F;">
                <div style=" width:90%; height:auto; overflow:auto; margin: 0px auto;"  >
                <table align="center" class="style1">
                    <tr>
                        <td style="width:30%">
                           <asp:Image ID="Image2" runat="server" BorderColor="#CC0000" BorderStyle="none" 
                    BorderWidth="1px" Height="100px" ImageUrl="~/Image/Gift.gif" /></td>
                        <td  style="width:70%">
                            &nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial Black; font-weight:bold; font-size:18pt;vertical-align:center; color:Maroon;">CONTRIBUTION FORM</span></td>
                    </tr>
                </table>
             </div>
            </td></tr>
           <tr> 
            <td style="text-align:center "><div style="width:100%; height:auto; overflow:auto;margin: 0px auto; " >
                </div>
            </td></tr>
        <tr> 
            <td> <div style="width:90%; height:auto; overflow:auto;margin: 0px auto; " >
                <table align="center" class="style1">
                    <tr>
                        <td style="width:20%">
                            &nbsp;</td>
                        <td  style="width:80%">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:20%">
                            Branch</td>
                        <td  style="width:80%">
                            <asp:TextBox ID="txtBranch" runat="server" style="width:90%" class="ReadOnlyTextBox" ReadOnly="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td  style="width:20%">
                            Branch Manager</td>
                        <td  style="width:80%">
                            <asp:TextBox ID="txtUser" runat="server" style="width:90%" class="ReadOnlyTextBox" ReadOnly="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td  style="width:20%">
                            &nbsp;</td>
                        <td  style="width:80%">
                            &nbsp;</td>
                    </tr>
                      </table>
                </div></td></tr>

                
        
        <tr> 
            <td><asp:Panel ID="pnLeaveApproveDtl" runat="server" style="width:100%; text-align:center; margin:opx auto;">
            </asp:Panel></td></tr>
        
        <tr> 
            <td>&nbsp;</td></tr>
        
        <tr > 
            <td>
               <div style="width:90%; height:auto; overflow:auto;margin: 0px auto;" ></div>
            </td></tr>
        
        <tr> 
            <td><div style="width:90%; height:auto; overflow:auto;margin: 0px auto; border:thin solid #EEB8A6;  background-color: #FFF0E6;" >
                <table align="center" class="style2">
                    <tr>
                        <td style="width:20%" class="style2" colspan="2">
                         <asp:Panel ID="Panel1" runat="server" style="width:100%; height:auto; background-color:#FFF0E6; color:Maroon; text-align:center; margin:opx auto; font-family:Cambria; font-weight:bold;  vertical-align:middle;" >
            DEPOSIT DETAILS</asp:Panel></td>
                    </tr>
                    <tr>
                        <td style="width:20%" class="style2">
                            Bank</td>
                        <td style="width:80%" class="style2">
                            <asp:TextBox ID="txtBank" runat="server" style="width:90%" class="NormalText"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:20%" class="style2">
                            Branch</td>
                        <td style="width:80%" class="style2">
                            <asp:TextBox ID="txtBankBranch" runat="server" style="width:90%" class="NormalText"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:20%" class="style2">
                            Date</td>
                        <td style="width:80%" class="style2">
                            <asp:TextBox ID="txtDate" runat="server" ReadOnly="false" ></asp:TextBox><asp:CalendarExtender ID="CE1" runat="server" TargetControlID="txtDate" Format="dd MMM yyyy"></asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:20%">
                            &nbsp;</td>
                        <td style="width:80%">
                            &nbsp;</td>
                    </tr>
                  <tr> 
            <td colspan="2">  <div style="width:100%; background-color: #FFF0E6; height:auto; overflow:auto;margin: 0px auto; " >
                <table align="center" class="style1">
                    <tr>
                        <td colspan="2" class="style2">
                            The amount collected may be deposited in any bank to be transferred to the SB 
                            account of ESAF the details of which are given below.</td>
                    </tr>
                     <tr>
                        <td  style="width:20%" class="style2">
                            Name of bank</td>
                        <td  style="width:80%" class="style2">
                            State Bank of India</td>
                    </tr>
                     <tr>
                        <td  style="width:20%" class="style2">
                            Name of Account</td>
                        <td  style="width:80%" class="style2">
                            Evangelical Social Action Forum</td>
                    </tr>
                     <tr>
                        <td  style="width:20%" class="style2">
                            Account Number</td>
                        <td  style="width:80%" class="style2">
                            10584382148</td>
                    </tr>
                     <tr>
                        <td  style="width:20%" class="style2">
                            Branch Name</td>
                        <td  style="width:80%" class="style2">
                            Thrissur</td>
                    </tr>
                     <tr>
                        <td  style="width:20%" class="style2">
                            IFS Code</td>
                        <td  style="width:80%" class="style2">
                            SBIN0000940</td>
                    </tr>
                     <tr>
                        <td  style="width:20%" class="style2">
                            &nbsp;</td>
                        <td  style="width:80%" class="style2">
                            &nbsp;</td>
                    </tr>
                </table>
                </div></td></tr>      
                </table>
                </div></td></tr>
        
        <tr> 
            <td></td></tr>
        <tr>
            <td style="text-align:center;">
                <br />               
                <input id="btSave" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="SAVE"  onclick="return btSave_onclick()" onclick="return btSave_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
            
        <tr>
            <td style="text-align:center;">
                &nbsp;</td>
        </tr>
            
    </table>   
    </div>
    <br />  
</asp:Content>

