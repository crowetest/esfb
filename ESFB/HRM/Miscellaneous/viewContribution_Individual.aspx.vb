﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewContribution_Individual
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim ContributionID As Integer
    Dim MonthDesc As String
    Dim subHead As String = ""
    Dim DT As New DataTable
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 122) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim PostID As Integer = Session("Post_ID")
            ContributionID = GN.Decrypt(Request.QueryString.Get("ContributionID"))
            Dim SubHD As String = ""
            Dim LocationID As Integer
            If (ContributionID > 0) Then
                DT = DB.ExecuteDataSet("select branch_id from contribution_master where contribution_id=" + ContributionID.ToString()).Tables(0)
                If (DT.Rows.Count > 0) Then
                    LocationID = CInt(DT.Rows(0)(0))
                    SubHD = GN.GetBranch_Name(LocationID) + " Branch"
                Else
                    LocationID = 0
                    SubHD = ""
                End If
            End If

            RH.Heading(Session("FirmName"), tb, "'GIFT FOR A SMILE' CONTRIBUTION REPORT " + SubHD, 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0



            'RH.BlankRow(tb, 5)      
            Dim sql As String = "select a.emp_code,b.emp_name,case when c.Branch_ID<5 then ' ' +d.Branch_Name+'/'+e.Department_Name else  d.Branch_Name end as Branch,a.amount from Contribution_Dtl a,EMP_MASTER b,Contribution_Master c,BRANCH_MASTER d,DEPARTMENT_MASTER e where a.Emp_Code=b.Emp_Code and a.Contribution_ID=c.Contribution_ID and c.Branch_ID=d.Branch_ID  and c.Department_ID=e.Department_ID   "
            If ContributionID > 0 Then
                sql += " and a.Contribution_ID=" + ContributionID.ToString()
            End If
            sql += " order by 3"
            DT = DB.ExecuteDataSet(sql).Tables(0)
            Dim Total As Double = 0
            Dim Branch As String = ""
            For Each DR In DT.Rows
                If (Branch <> DR(2)) Then
                    Dim TRHeading As New TableRow
                    Dim TRHeading_00 As New TableCell
                    TRHeading_00.BorderWidth = "1"
                    TRHeading_00.BorderColor = Drawing.Color.Silver
                    TRHeading_00.BorderStyle = BorderStyle.Solid
                    TRHeading.BackColor = Drawing.Color.LightGray
                    RH.AddColumn(TRHeading, TRHeading_00, 100, 100, "c", DR(2).ToString().ToUpper())
                    tb.Controls.Add(TRHeading)
                    Dim TRHead As New TableRow
                    TRHead.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell

                    TRHead_00.BorderWidth = "1"
                    TRHead_01.BorderWidth = "1"
                    TRHead_02.BorderWidth = "1"
                    TRHead_03.BorderWidth = "1"


                    TRHead_00.BorderColor = Drawing.Color.Silver
                    TRHead_01.BorderColor = Drawing.Color.Silver
                    TRHead_02.BorderColor = Drawing.Color.Silver
                    TRHead_03.BorderColor = Drawing.Color.Silver


                    TRHead_00.BorderStyle = BorderStyle.Solid
                    TRHead_01.BorderStyle = BorderStyle.Solid
                    TRHead_02.BorderStyle = BorderStyle.Solid
                    TRHead_03.BorderStyle = BorderStyle.Solid


                    RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
                    RH.AddColumn(TRHead, TRHead_01, 15, 15, "l", "Emp Code")
                    RH.AddColumn(TRHead, TRHead_02, 70, 70, "l", "Name")
                    RH.AddColumn(TRHead, TRHead_03, 10, 10, "r", "Amount")

                    tb.Controls.Add(TRHead)
                End If
                Branch = DR(2)
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid


                Dim TR3_00, TR3_01, TR3_02, TR3_03 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid


                I = I + 1
                RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString().ToString())
                RH.AddColumn(TR3, TR3_01, 15, 15, "l", DR(0).ToString().ToUpper())
                RH.AddColumn(TR3, TR3_02, 70, 70, "l", DR(1).ToString().ToUpper())
                RH.AddColumn(TR3, TR3_03, 10, 10, "r", CDbl(DR(3)).ToString("#####.00"))

                tb.Controls.Add(TR3)
                Total += CDbl(DR(3))
            Next
            Dim TrEnd As New TableRow
            TrEnd.BackColor = Drawing.Color.WhiteSmoke
            Dim TrEnd_00, TrEnd_01, TrEnd_02, TrEnd_03 As New TableCell

            TrEnd_00.BorderWidth = "1"
            TrEnd_01.BorderWidth = "1"
            TrEnd_02.BorderWidth = "1"
            TrEnd_03.BorderWidth = "1"


            TrEnd_00.BorderColor = Drawing.Color.Silver
            TrEnd_01.BorderColor = Drawing.Color.Silver
            TrEnd_02.BorderColor = Drawing.Color.Silver
            TrEnd_03.BorderColor = Drawing.Color.Silver


            TrEnd_00.BorderStyle = BorderStyle.Solid
            TrEnd_01.BorderStyle = BorderStyle.Solid
            TrEnd_02.BorderStyle = BorderStyle.Solid
            TrEnd_03.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TrEnd, TrEnd_00, 5, 5, "c", "")
            RH.AddColumn(TrEnd, TrEnd_01, 15, 15, "l", "TOTAL")
            RH.AddColumn(TrEnd, TrEnd_02, 70, 70, "l", "")
            RH.AddColumn(TrEnd, TrEnd_03, 10, 10, "r", (Total).ToString("####.00"))

            tb.Controls.Add(TrEnd)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
