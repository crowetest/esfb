﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Regularize
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim SM As New SMTP
    Dim LV As New Leave
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 24) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Attendance Regularize Request"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            cmbCategory.Focus()
            hdnDate.Value = CDate(Session("TraDt")).ToString("dd MMM yyyy")
            txtDate.Attributes.Add("autocomplete", "off")
            'hdn_from.Value = CDate(DateAdd("d", -2, CDate(Session("TraDt")))).ToString("dd MMM yyyy")
            'hdn_to.Value = CDate(Session("TraDt")).ToString("dd MMM yyyy")
            'hdn_to.Value = CDate(DateAdd("d", -1, CDate(Session("TraDt")))).ToString("dd MMM yyyy")
            DT = LV.Get_Regularyze_Abscence_from(CDate(CDate(DateAdd("d", -3, CDate(Session("TraDt")))).ToString("dd MMM yyyy")), CDate(CDate(DateAdd("d", -1, CDate(Session("TraDt")))).ToString("dd MMM yyyy")))
            If DT.Rows.Count > 0 Then
                hdn_from.Value = CStr(DT.Rows(0).Item(0))
            Else
                hdn_from.Value = CStr(0)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim CategoryID As Integer = CInt(Data(1))
        Dim TRDate As Date = CDate(Data(2))
        Dim Reason As String = CStr(Data(3))
        Dim SessionID As Integer = CInt(Data(4))
        Dim typeID As Integer = CInt(Data(5))
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try
            Dim Params(7) As SqlParameter
            Params(0) = New SqlParameter("@CategoryID", SqlDbType.Int)
            Params(0).Value = CategoryID
            Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(1).Value = UserID
            Params(2) = New SqlParameter("@TraDt", SqlDbType.Date)
            Params(2).Value = TRDate
            Params(3) = New SqlParameter("@SessionID", SqlDbType.Int)
            Params(3).Value = SessionID
            Params(4) = New SqlParameter("@Remarks", SqlDbType.VarChar, 50)
            Params(4).Value = Reason
            Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(5).Direction = ParameterDirection.Output
            Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(6).Direction = ParameterDirection.Output
            Params(7) = New SqlParameter("@TypeID", SqlDbType.Int)
            Params(7).Value = typeID
            DB.ExecuteNonQuery("SP_HR_RegularizeAttendance", Params)
            ErrorFlag = CInt(Params(5).Value)
            Message = CStr(Params(6).Value)
            If ErrorFlag = 0 Then

                Dim ToAddress As String = "hena.ek@esafmicrofin.com"
                Dim EmpName As String = ""
                Dim tt As String = SM.GetEmailAddress(UserID, 2)
                Dim Val = Split(tt, "^")
                If Val(0).ToString <> "" Then
                    ToAddress = Val(0)
                End If
                If Val(1).ToString <> "" Then
                    EmpName = Val(1).ToString
                End If
                Dim ccAddress As String = ""
                'General.GetEmailAddress(EmpID, 3);
                If ccAddress.Trim() <> "" Then
                    ccAddress += ","
                End If

                Dim Content As String = vbLf & vbLf & " Dear " + EmpName + ", " & vbLf & vbLf
                If CategoryID = 1 Or CategoryID = 3 Or CategoryID = 4 Then 'Non MARKING
                    If SessionID = 1 Or SessionID = 2 Then
                        Content += " Non Marking Regularize Request generated from " + CStr(Session("UserName")) + "[" + CStr(Session("UserID")) + "]" & vbLf & vbLf
                        Content += "Reason -" + Reason
                    Else
                        If typeID = 2 Then 'late coming
                            Content += " Late Coming Regularize Request generated from " + CStr(Session("UserName")) + "[" + CStr(Session("UserID")) + "]" & vbLf & vbLf
                            Content += "Reason -" + Reason
                        Else 'Half Day
                            Content += " Half Day Regularize Request generated from " + CStr(Session("UserName")) + "[" + CStr(Session("UserID")) + "]" & vbLf & vbLf
                            Content += "Reason -" + Reason
                        End If
                    End If
                   
                ElseIf CategoryID = 5 Then 'Abscence
                    Content += " Abscence Regularize Request generated from " + CStr(Session("UserName")) + "[" + CStr(Session("UserID")) + "]" & vbLf & vbLf
                    Content += "Reason -" + Reason
                ElseIf CategoryID = 2 Then 'Early Going
                    Content += " Early Going Regularize Request generated from " + CStr(Session("UserName")) + "[" + CStr(Session("UserID")) + "]" & vbLf & vbLf
                    Content += "Reason -" + Reason
                End If
               
                Content = Content & Convert.ToString(vbLf & vbLf & " With Regards" & vbLf & vbLf & "HR Team" & vbLf & "Head, HR")
                SM.SendMail(ToAddress, "Regularize", Content, ccAddress & Convert.ToString("nisha.rathnan@esafmicrofin.com"))


            End If
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message

    End Sub
   
#End Region
End Class
