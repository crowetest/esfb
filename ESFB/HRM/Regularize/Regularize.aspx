﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Regularize.aspx.vb" Inherits="Regularize" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function CategoryOnChange() {
            var CategoryID = document.getElementById("<%= cmbCategory.ClientID %>").value;
            if (CategoryID == 1) {
                document.getElementById("<%= txtDate.ClientID %>").value = "";
                document.getElementById("RowSession").style.display = '';
                document.getElementById("<%= txtDate.ClientID %>").disabled = false;
            }
            else if (CategoryID == 2) {
                document.getElementById("<%= txtDate.ClientID %>").value = document.getElementById("<%= hdnDate.ClientID %>").value;
                document.getElementById("RowSession").style.display = 'none';
                document.getElementById("<%= txtDate.ClientID %>").disabled = true;
            }
            else if (CategoryID == 3 || CategoryID == 4) {
                document.getElementById("<%= txtDate.ClientID %>").value = document.getElementById("<%= hdnDate.ClientID %>").value;
                document.getElementById("RowSession").style.display = 'none';
                document.getElementById("<%= txtDate.ClientID %>").disabled = true;
            }
            else if (CategoryID == 5) {
                document.getElementById("<%= txtDate.ClientID %>").value = "";
                document.getElementById("RowSession").style.display = 'none';
                document.getElementById("<%= txtDate.ClientID %>").disabled = false;
            }
            else {
                document.getElementById("<%= txtDate.ClientID %>").value = "";
                document.getElementById("RowSession").style.display = 'none';
                document.getElementById("<%= txtDate.ClientID %>").disabled = false;
            }
        }
      
  
   function AddDay(strDate, intNum) {
      
                    
            sdate = new Date(strDate);
         
            sdate.setDate(sdate.getDate() + intNum);
          
            return sdate;
            ////return  sdate.getFullYear()+","+ 
////          alert( sdate.getMonth() + 1 + " " + sdate.getDate() + " " + sdate.getFullYear());
           
           
        }


         function setStartDate(sender, args) {
            var CategoryID = document.getElementById("<%= cmbCategory.ClientID %>").value; 
           var EndDt =  new Date();
            // // var EndDt=new Date(2019,1,23);
           var EndDt = AddDay(document.getElementById("<%= hdnDate.ClientID %>").value, 1); 
          ///// var EndDt = AddDay('23 Jan 2019', 1); 
           
           

//            if (CategoryID == 5) {
//            var Cnt = Math.abs(document.getElementById("<%= hdn_from.ClientID %>").value+3)*-1; 
//                var OrgStartDt = AddDay(document.getElementById("<%= hdnDate.ClientID %>").value, Cnt);
//                var StartDt = AddDay(OrgStartDt, 1);
//                sender._startDate =  new Date(StartDt);
//                sender._endDate =  new Date(EndDt);
//            }
//            else {

            var Today= new Date();

           //// var Today=new Date(2019,1,23);
     
             if (Today.getDate() >20)
              
              {
                  
                   var curmonth21 =  new Date(Today.getFullYear(),Today.getMonth(),21);
                   
                    sender._startDate =  curmonth21;
                 
                  

              }
              else
              { 
             
                
               var PrevMonthDate =  new Date();
              
              //var PrevMonthDate=new Date(2019,0,23);
             
                PrevMonthDate.setDate( Today.getDate() -Today.getDate());
            
                var Prevmonth21 =  new Date(PrevMonthDate.getFullYear(),PrevMonthDate.getMonth(),21);
               
                    sender._startDate =  Prevmonth21;
                    

              }
              sender._endDate=EndDt;
              
                ///sender._endDate =  new Date(document.getElementById("<%= hdnDate.ClientID %>").value);
            //}
        }

        function FromServer(Arg, Context) {
            var Data = Arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("Regularize.aspx", "_self");
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnConfirm_onclick() {
            if (document.getElementById("<%= cmbCategory.ClientID %>").value == -1) {

                alert("Select Category");
                document.getElementById("<%= cmbCategory.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtDate.ClientID %>").value=="") {
                  
                        alert("Enter Date");
                        document.getElementById("<%= txtDate.ClientID %>").focus();
                        return false;
                    
            }
          
            if (document.getElementById("<%= txtReason.ClientID %>").value == "") 
            {
                alert("Enter Reason");
                document.getElementById("<%= txtReason.ClientID %>").focus();
                return false;
            }
           var CategoryID = document.getElementById("<%= cmbCategory.ClientID %>").value;
           var typeid = 0;
           if (CategoryID == 1) {
               var typeid = 1;
           }
           else if (CategoryID == 3) {
               var typeid = 2;
           }
           else if (CategoryID == 4) {
               var typeid = 3;
           }
           else if (CategoryID == 5) {
               var typeid = 4;
           }
            var Date = document.getElementById("<%= txtDate.ClientID %>").value;
            var Reason = document.getElementById("<%= txtReason.ClientID %>").value;
            var SessionID = 0;
            if (CategoryID == 1)
                SessionID = document.getElementById("<%= cmbSession.ClientID %>").value;
            var ToData = "1Ø" + CategoryID + "Ø" + Date + "Ø" + Reason + "Ø" + SessionID + "Ø" + typeid;
            ToServer(ToData, 1);
        }

    </script>
   
</head>
</html>
<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;" >
           
        <tr id="branch"> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Category</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbCategory" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1">Non Marking</asp:ListItem>
                    <asp:ListItem Value="2">Early Going</asp:ListItem>
                    <asp:ListItem Value="5">Absent</asp:ListItem>
            </asp:DropDownList>
            </td>
            
       </tr>
       <tr> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDate" class="NormalText" runat="server" 
                    Width="40%" onkeypress="return false" ></asp:TextBox>
             <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtDate" EnabledOnClient="true" OnClientShowing="setStartDate" Format="dd MMM yyyy">
             </asp:CalendarExtender>
            </td>
       </tr>
       <tr id="RowSession" style="display:none;">  <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Session</td>
            <td style="width:63%">
                 &nbsp; &nbsp;<asp:DropDownList 
                     ID="cmbSession" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1">Morning</asp:ListItem>
                    <asp:ListItem Value="2">Evening</asp:ListItem>
            </asp:DropDownList>
            </td>
       </tr>
       <tr> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Reason</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtReason" class="NormalText" runat="server" 
                    Width="40%" onkeypress="return validateNumber(event)" 
                MaxLength="100" Rows="3" TextMode="MultiLine" ></asp:TextBox>
            </td>
       </tr>
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="REQUEST" onclick="return btnConfirm_onclick()"/>&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /><asp:HiddenField 
                    ID="hdn_from" runat="server" />
                 <asp:HiddenField ID="hdn_to" runat="server" />
            &nbsp;<asp:HiddenField ID="hdnDate" runat="server" />
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

