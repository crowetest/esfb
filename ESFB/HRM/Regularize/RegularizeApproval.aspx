﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="RegularizeApproval.aspx.vb" Inherits="RegularizeApproval" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function CategoryOnChange() 
        {
            var CategoryID = document.getElementById("<%= cmbCategory.ClientID %>").value;
            if (CategoryID == 1) {
                document.getElementById("<%= txtDate.ClientID %>").value = "";
                document.getElementById("RowSession").style.display = '';               
               
            }
            else if (CategoryID == 2) {
                document.getElementById("<%= txtDate.ClientID %>").value = "";
                document.getElementById("RowSession").style.display = 'none';

            }
            else if (CategoryID == 3 || CategoryID == 4) {
                document.getElementById("<%= txtDate.ClientID %>").value = "";
                document.getElementById("RowSession").style.display = 'none';
               
            }
            else {
                document.getElementById("<%= txtDate.ClientID %>").value = "";
                document.getElementById("RowSession").style.display = 'none';
               
            }
            var ToData = "1Ø" + CategoryID;
            ToServer(ToData, 1);
        }

        function FromServer(arg, context) {

            switch (context) {

                case 1:
                    {
                        ComboFill(arg, "<%= cmbRequest.ClientID %>");
                        break;
                    }

                case 2:
                    {
                        var Data = arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("RegularizeApproval.aspx", "_self");
                    }

            }
          
        }
      
        

        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function RequestOnChange() {
            var RequestID = document.getElementById("<%= cmbRequest.ClientID %>").value;           
            if (RequestID != "-1") {
                var Dtl = RequestID.split("Ø");
                document.getElementById("<%= txtDate.ClientID %>").value = Dtl[1];
                document.getElementById("<%= txtReason.ClientID %>").value = Dtl[2];
                if(document.getElementById("<%= cmbSession.ClientID %>"))
                    document.getElementById("<%= cmbSession.ClientID %>").value = Dtl[3];
            }
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnReject_onclick() 
        {
            var CategoryID = document.getElementById("<%= cmbCategory.ClientID %>").value;
            var typeid = 0;
            if (CategoryID == 1) {
                var typeid = 1;
            }
            else if (CategoryID == 3) {
                var typeid = 2;
            }
            else if (CategoryID == 4) {
                var typeid = 3;
            }
            else if (CategoryID == 5) {
                var typeid = 4;
            }
            var RequestID = document.getElementById("<%= cmbRequest.ClientID %>").value;
            if (RequestID == "-1")
            { alert("Select Request"); document.getElementById("<%= cmbRequest.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtRemarks.ClientID %>").value == "")
            { alert("Enter Remarks"); document.getElementById("<%= txtRemarks.ClientID %>").focus(); return false; }
            var Dtl = RequestID.split("Ø");
            var ToData = "2Ø" + CategoryID + "Ø" + Dtl[0] + "Ø2Ø" + document.getElementById("<%= txtRemarks.ClientID %>").value + "Ø" + typeid;
            ToServer(ToData, 2);
           
        }



        function btnApprove_onclick() {
            var CategoryID = document.getElementById("<%= cmbCategory.ClientID %>").value;
            var typeid = 0;
            if (CategoryID == 1) {
                var typeid = 1;
            }
            else if (CategoryID == 3) {
                var typeid = 2;
            }
            else if (CategoryID == 4) {
                var typeid = 3;
            }
            else if (CategoryID == 5) {
                var typeid = 4;
            }
            var RequestID = document.getElementById("<%= cmbRequest.ClientID %>").value;
            if (RequestID == "-1")
            { alert("Select Request"); document.getElementById("<%= cmbRequest.ClientID %>").focus(); return false; }
            var Dtl = RequestID.split("Ø");
            var ToData = "2Ø" + CategoryID + "Ø" + Dtl[0] + "Ø1Ø" + document.getElementById("<%= txtRemarks.ClientID %>").value + "Ø" + typeid;          
            ToServer(ToData, 2);
        }



    </script>
   
</head>
</html>
<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;" >
           
        <tr id="branch"><td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Category</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbCategory" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1">Non Marking</asp:ListItem>
                    <asp:ListItem Value="2">Early Going</asp:ListItem>
                    <asp:ListItem Value="5">Absent</asp:ListItem>
            </asp:DropDownList>
            </td>
            
       </tr>
       <tr> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Select Request</td>
            <td style="width:63%">
               &nbsp; &nbsp;<asp:DropDownList ID="cmbRequest" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="40%" ForeColor="Black"> <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
            </asp:DropDownList>
            </td>
       </tr>
       <tr> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDate" 
                    class="ReadOnlyTextBox" runat="server" 
                    Width="40%" ReadOnly="True"  ></asp:TextBox>
            </td>
       </tr>
       <tr id="RowSession" style="display:none;"> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Session</td>
            <td style="width:63%">
                 &nbsp; &nbsp;<asp:DropDownList 
                     ID="cmbSession" class="ReadOnlyTextBox" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="40%" ForeColor="Black" Enabled="False">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1">Morning</asp:ListItem>
                    <asp:ListItem Value="2">Evening</asp:ListItem>
            </asp:DropDownList>
            </td>
       </tr>
       <tr> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Reason</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtReason" 
                    class="ReadOnlyTextBox" runat="server" 
                    Width="40%" 
                MaxLength="100" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ReadOnly="True"></asp:TextBox>
            </td>
       </tr>
       <tr><td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Remarks</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtRemarks" class="NormalText" runat="server" 
                    Width="40%" 
                MaxLength="100" Rows="3" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
            </td>
       </tr>
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 <input id="btnApprove" 
                    style="width: 8%; font-family: Cambria; cursor: pointer;" type="button" 
                    value="APPROVE" onclick="return btnApprove_onclick()"  />&nbsp;
                <input 
                    id="btnReject" style="font-family: cambria; cursor: pointer; width: 8%;" 
                type="button" value="REJECT" onclick="return btnReject_onclick()" />
            &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 8%;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
                 
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

