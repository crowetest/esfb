﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class RegularizeApproval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim SM As New SMTP
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 25) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Attendance Regularize Approval"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            Me.cmbRequest.Attributes.Add("onchange", "RequestOnChange()")
            cmbCategory.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim QRY As String = ""
            Dim TypeID As Integer = CInt(Data(1))
            Dim INTVAL As Integer
            If TypeID = 1 Or TypeID = 3 Or TypeID = 4 Or TypeID = 5 Then
                If TypeID = 1 Then
                    INTVAL = 1
                ElseIf TypeID = 3 Then
                    INTVAL = 2
                ElseIf TypeID = 4 Then
                    INTVAL = 3
                ElseIf TypeID = 5 Then
                    INTVAL = 4
                End If
                QRY = "Select '-1' as RequestID,' -----Select-----' as Request UNION ALL select  cast(request_ID as varchar(5))+'Ø'+ CONVERT(VARCHAR(11),A.NM_Date , 106)+'Ø'+a.Reason+'Ø'+CAST(a.session_ID as varchar(2)) ,CONVERT(VARCHAR(11),A.NM_Date, 106)+'  |  '+CAST(A.Emp_Code as varchar(5)) +'  |  '+b.Emp_Name  from EMP_REGULARIZE  A,EMP_MASTER b where a.Emp_Code=b.Emp_Code and a.Status_ID=0 and a.requesttype=" & INTVAL & " and b.reporting_To=" + Session("UserID").ToString()
            ElseIf TypeID = 2 Then
                QRY = "Select '-1' as RequestID,' -----Select-----' as Request UNION ALL select  cast(request_ID as varchar(5))+'Ø'+ CONVERT(VARCHAR(11),A.Early_Date, 106)+'Ø'+a.Remarks ,CONVERT(VARCHAR(11),A.Early_Date, 106)+'  |  '+CAST(A.Emp_Code as varchar(5)) +'  |  '+b.Emp_Name  from EMP_EARLY_GOING A,EMP_MASTER b where a.Emp_Code=b.Emp_Code and a.Status_ID=0 and b.reporting_To=" + Session("UserID").ToString()
            End If
            DT = DB.ExecuteDataSet(QRY).Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 2 Then
            Dim CategoryID As Integer = CInt(Data(1))
            Dim RequestID As Integer = CInt(Data(2))
            Dim StatusID As Integer = CInt(Data(3)) '2-Rejected ,1-Approved
            Dim Remarks As String = CStr(Data(4))
            Dim typeID As Integer = CInt(Data(5))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@CategoryID", SqlDbType.Int)
                Params(0).Value = CategoryID
                Params(1) = New SqlParameter("@RequestID", SqlDbType.Int)
                Params(1).Value = RequestID
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = UserID
                Params(3) = New SqlParameter("@StatusID", SqlDbType.Int)
                Params(3).Value = StatusID
                Params(4) = New SqlParameter("@Remarks", SqlDbType.VarChar, 50)
                Params(4).Value = Remarks
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@TypeID", SqlDbType.Int)
                Params(7).Value = typeID
                DB.ExecuteNonQuery("SP_APPROVE_REGULARIZATION", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
                If ErrorFlag = 0 Then

                    Dim QRY As String = ""
                    If typeID = 1 Or typeID = 2 Or typeID = 3 Or typeID = 4 Then

                        QRY = "select  CONVERT(VARCHAR(11),A.NM_Date, 106)+'  |  '+CAST(A.Emp_Code as varchar(5)) +'  |  '+b.Emp_Name  from EMP_REGULARIZE  A,EMP_MASTER b where a.Emp_Code=b.Emp_Code and a.request_ID=" & RequestID & ""
                    Else
                        QRY = "select  CONVERT(VARCHAR(11),A.Early_Date, 106)+'  |  '+CAST(A.Emp_Code as varchar(5)) +'  |  '+b.Emp_Name  from EMP_EARLY_GOING A,EMP_MASTER b where a.Emp_Code=b.Emp_Code and a.request_ID=" & RequestID & ""
                    End If
                    DT = DB.ExecuteDataSet(QRY).Tables(0)

                    Dim Employee() = Split(CStr(DT.Rows(0).Item(0)), "|")
                    Dim ToAddress As String = "hena.ek@esafmicrofin.com"
                    Dim EmpName As String = ""
                    Dim tt As String = SM.GetEmailAddress(CInt(Employee(1)), 1)
                    Dim Val = Split(tt, "^")
                    If Val(0).ToString <> "" Then
                        ToAddress = Val(0)
                    End If
                    If Val(1).ToString <> "" Then
                        EmpName = Val(1).ToString
                    End If
                    Dim ccAddress As String = ""
                    'General.GetEmailAddress(EmpID, 3);
                    If ccAddress.Trim() <> "" Then
                        ccAddress += ","
                    End If
                    Dim StrVal As String = ""
                    If StatusID = 1 Then
                        StrVal = "Approved"
                    Else
                        StrVal = "Rejected"
                    End If

                    Dim Content As String = vbLf & vbLf & " Dear " + EmpName + ", " & vbLf & vbLf
                    If CategoryID = 1 Or CategoryID = 3 Or CategoryID = 4 Then 'Non MARKING
                        If typeID = 1 Then
                            Content += " Non Marking Regularize Request [" + Employee(1) + "]" + StrVal + "" & vbLf & vbLf

                        ElseIf typeID = 2 Then 'late coming
                            Content += " Late Coming Regularize Request  [" + Employee(1) + "]" + StrVal + "" & vbLf & vbLf

                        Else 'Half Day
                            Content += " Half Day Regularize Request  [" + Employee(1) + "]" + StrVal + "" & vbLf & vbLf

                        End If

                    ElseIf CategoryID = 5 Then 'Abscence
                        Content += " Abscence Regularize Request  [" + Employee(1) + "]" + StrVal + "" & vbLf & vbLf

                    ElseIf CategoryID = 2 Then 'Early Going
                        Content += " Early Going Regularize Request  [" + Employee(1) + "]" + StrVal + "" & vbLf & vbLf

                    End If

                    Content = Content & Convert.ToString(vbLf & vbLf & " With Regards" & vbLf & vbLf & "HR Team" & vbLf & "Head, HR")
                    SM.SendMail(ToAddress, "Regularize", Content, ccAddress & Convert.ToString("nisha.rathnan@esafmicrofin.com"))


                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If


    End Sub
#End Region
End Class
