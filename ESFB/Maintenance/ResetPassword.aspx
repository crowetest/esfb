﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ResetPassword.aspx.vb" Inherits="ResetPassword" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
     var ip;
      var RTCPeerConnection = window.webkitRTCPeerConnection || window.mozRTCPeerConnection;

      if (RTCPeerConnection) (function () {
          var rtc = new RTCPeerConnection({ iceServers: [] });

          if (1 || window.mozRTCPeerConnection) {
              rtc.createDataChannel('', { reliable: false });
          };

          rtc.onicecandidate = function (evt) {

              if (evt.candidate) grepSDP("a=" + evt.candidate.candidate);
          };
          rtc.createOffer(function (offerDesc) {
              grepSDP(offerDesc.sdp);
              rtc.setLocalDescription(offerDesc);
          }, function (e) { console.warn("offer failed", e); });


          var addrs = Object.create(null);
          addrs["0.0.0.0"] = false;
          function updateDisplay(newAddr) {
              if (newAddr in addrs) return;
              else addrs[newAddr] = true;
              var displayAddrs = Object.keys(addrs).filter(function (k)
               { return addrs[k]; });
              ip = displayAddrs.join(" | ") || "n/a";
             

          }

          function grepSDP(sdp) {
              var hosts = [];
              sdp.split('\r\n').forEach(function (line) {
                  if (~line.indexOf("a=candidate")) {
                      var parts = line.split(' '),
                    addr = parts[4],
                    type = parts[7];
                      if (type === 'host') updateDisplay(addr);
                  } else if (~line.indexOf("c=")) {
                      var parts = line.split(' '),
                    addr = parts[2];
                      updateDisplay(addr);
                  }
              });
          }
      })(); else {


      }
        function RequestOnchange() {

            var Data = document.getElementById("<%= hid_data.ClientID %>").value.split("^");
            document.getElementById("<%= txtName.ClientID %>").value = Data[1];
            document.getElementById("<%= txtBranch.ClientID %>").value = Data[3];
            document.getElementById("<%= txtDepartment.ClientID %>").value = Data[5];
            document.getElementById("<%= txtDesignation.ClientID %>").value = Data[6];
            document.getElementById("<%= txtReportingOfficer.ClientID %>").value = Data[8];             
          
          }
          function RequestID() {
              var EmpID = document.getElementById("<%= txtEmpcode.ClientID %>").value;
              if (EmpID > 0) {
                  var ToData = "2Ø" + EmpID;
                  ToServer(ToData,2);
              }
          }

          function FromServer(arg, context) {
              if (context == 1) {
                var Data = arg.split("Ø");
                  alert(Data[1]);
                  if (Data[0] == 0) window.open("ResetPassword.aspx", "_self");
              }
              else if (context == 2) {
                  var Data = arg.split("~");
                  if (Data[0] == 1) {
                      document.getElementById("<%= hid_data.ClientID %>").value = Data[1];
                      RequestOnchange();
                  }
                  else if (Data[0] == 2)
                  {
                      alert(Data[1]);
                      document.getElementById("<%= txtName.ClientID %>").value = "";
                      document.getElementById("<%= txtBranch.ClientID %>").value = "";
                      document.getElementById("<%= txtDepartment.ClientID %>").value = "";
                      document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                      document.getElementById("<%= txtEmpcode.ClientID %>").value = "";
                      document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                  }
              }
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnConfirm_onclick() {
           if (document.getElementById("<%= txtEmpcode.ClientID %>").value == "") 
            {
                alert("Select Employee Code");
                document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                return false;
            }
            
           var EmpID	= document.getElementById("<%= txtEmpcode.ClientID %>").value;
       
	      var ToData = "1Ø" + EmpID + "Ø"+ ip;
	       
           ToServer(ToData, 1);

        }
   
    </script>
   
</head>
</html>
<br />
    <asp:HiddenField ID="hid_data" runat="server" />
    <br />

 <table class="style1" style="width:80%;margin: 0px auto;">
    <tr> <td style="width:25%;"></td>
    <td style="width:12%; text-align:left;">Employee Code</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpcode" class="NormalText" runat="server" Width="15%" 
                MaxLength="5"  ></asp:TextBox>
            </td>
    </tr>
        <tr>
        <td style="width:25%;"></td>
         <td style="width:12%; text-align:left;">Name</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Location</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Department</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDepartment" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Designation</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesignation" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
         <tr> 
       <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Reporting Officer</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtReportingOfficer" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="RESET" onclick="return btnConfirm_onclick()"/>&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
                 </td>
        </tr>
  

        <asp:HiddenField ID="hid_OTP" runat="server" />
</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

</asp:Content>

