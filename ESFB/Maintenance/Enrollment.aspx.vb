﻿Imports System.Data

Partial Class Maintenance_Enrollment
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DT As New DataTable
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Employee Enrollment"
            GN.ComboFill(cmbBranch, EN.GetBranch(), 0, 1)
            GN.ComboFill(cmbDepartment, EN.GetDepartment(), 0, 1)
            GN.ComboFill(cmbCadre, EN.GetCadre(), 0, 1)
            GN.ComboFill(cmbEmpPost, GN.GetEmpPost(), 0, 1)
            DT = GN.GetQualification()
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- Register Client Side Functions ---//
            Me.cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            Me.cmbDepartment.Attributes.Add("onchange", "ChangeReportingOfficer()")
            Me.cmbDesignation.Attributes.Add("onchange", "ChangeReportingOfficer()")
            Me.cmbCadre.Attributes.Add("onchange", "return CadreOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If Data(0) = 1 Then
            Dim EmpCode As Integer = CInt(Data(1))
            Dim EmpName As String = CStr(Data(2))
            Dim BranchID As Integer = CInt(Data(3))
            Dim DepartmentID As Integer = CInt(Data(4))
            Dim CadreID As Integer = CInt(Data(5))
            Dim DesignationID As Integer = CInt(Data(6))
            Dim DateOfJoin As Date = CDate(Data(7))
            Dim ReportingTo As Integer = CInt(Data(8))
            Dim IsFieldStaff As Integer = CInt(Data(9))

            Dim EmpPost As Integer = CInt(Data(10))
            Dim UserID As Integer = CInt(Session("UserID"))
            CallBackReturn = EN.EnrollResignedEmployee(EmpCode, EmpName, BranchID, DepartmentID, CadreID, DesignationID, DateOfJoin, ReportingTo, IsFieldStaff, UserID, EmpPost)
        ElseIf Data(0) = 2 Then
            DT = EN.GetReportingOfficer(CInt(Data(1)), CInt(Data(2)), CInt(Data(3)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()

            Next
        ElseIf CInt(Data(0)) = 4 Then
            DT = EN.GetDesignation(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 5 Then
            DT = GN.GetDistrict(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 6 Then
            DT = GN.GetPost(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If

    End Sub

#End Region


End Class
