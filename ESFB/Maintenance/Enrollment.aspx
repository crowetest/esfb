﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Enrollment.aspx.vb" Inherits="Maintenance_Enrollment"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
        }
        function btnSave_onclick() {
            if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "") { alert("Enter Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtEmpName.ClientID %>").value == "") { alert("Enter Employee Name"); document.getElementById("<%= txtEmpName.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1") { alert("Select Branch"); document.getElementById("<%= cmbBranch.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbDepartment.ClientID %>").value == "-1") { alert("Select Department"); document.getElementById("<%= cmbDepartment.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbCadre.ClientID %>").value == "-1") { alert("Select Cadre"); document.getElementById("<%= cmbCadre.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbDesignation.ClientID %>").value == "-1") { alert("Select Designation"); document.getElementById("<%= cmbDesignation.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtDateOfJoin.ClientID %>").value == "") { alert("Enter Date of Join"); document.getElementById("<%= txtDateOfJoin.ClientID %>").focus(); return false; }            
            if (document.getElementById("<%= cmbReportingTo.ClientID %>").value == "-1") { alert("Select Reporting Officer"); document.getElementById("<%= cmbReportingTo.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= cmbEmpPost.ClientID %>").value == "-1") { alert("Select Post"); document.getElementById("<%= cmbEmpPost.ClientID %>").focus(); return false; }
           
            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            var EmpName = document.getElementById("<%= txtEmpName.ClientID %>").value;
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
            var BranchID = BranchDtl[0];
            var DepartmentID = document.getElementById("<%= cmbDepartment.ClientID %>").value;
            var CadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;
            var DesignationID = document.getElementById("<%= cmbDesignation.ClientID %>").value;
            var DateOfJoin = document.getElementById("<%= txtDateOfJoin.ClientID %>").value;         
            var ReportingTo = document.getElementById("<%= cmbReportingTo.ClientID %>").value;
            var empPost = document.getElementById("<%= cmbEmpPost.ClientID %>").value;    
            var isFieldStaff = (document.getElementById("<%= chkFieldStaff.ClientID %>").checked) ? 1 : 0;   
            var Data = "1Ø" + EmpCode + "Ø" + EmpName + "Ø" + BranchID + "Ø" + DepartmentID + "Ø" + CadreID + "Ø" + DesignationID + "Ø" + DateOfJoin + "Ø" + ReportingTo + "Ø" + isFieldStaff+ "Ø"+ empPost;
            ToServer(Data, 1);
            
        }

        function FromServer(Arg, Context) {
            switch (Context) {

                case 1:
                    {   var Data = Arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("Enrollment.aspx", "_self");
                        break;
                    }
                case 2:
                    {
                        ComboFill(Arg, "<%= cmbReportingTo.ClientID %>");
                        break;
                    }
                 case 4: {                   
                            ComboFill(Arg, "<%= cmbDesignation.ClientID %>");
                            break;
                         }
             
              
            }
        }
        function BranchOnChange() {
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
            if (BranchDtl[1] == 1 || BranchDtl[1]==3) {
                document.getElementById("<%= cmbDepartment.ClientID %>").value = 9;
                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = true ;               
            }
            else {
                document.getElementById("<%= cmbDepartment.ClientID %>").value = "-1";
                document.getElementById("<%= cmbDepartment.ClientID %>").disabled = false;              
            }
            ChangeReportingOfficer();
        }
        function ChangeReportingOfficer() {
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value.split("Ø");
            if(BranchDtl!="-1")
            {
                var BranchID = BranchDtl[0];
                var DepartmentID = document.getElementById("<%= cmbDepartment.ClientID %>").value;
                var DesignationID = document.getElementById("<%= cmbDesignation.ClientID %>").value;
                if(DepartmentID>0 && BranchID>=0 && DesignationID>0)
                    ToServer("2Ø" + BranchID + "Ø" + DepartmentID + "Ø" + DesignationID + "Ø", 2);
            }
        }
        function CadreOnChange() {
            var CadreID = document.getElementById("<%= cmbCadre.ClientID %>").value;           
            var ToData = "4Ø" + CadreID;          
            ToServer(ToData, 4);

        }
         

        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
      
function btnExit_onclick() {
  window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
}

    </script>
    <br />
    <table align="center" 
        style="width:80%; margin: 0px auto;" >
           <tr>
            <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                <strong>APPOINTMENT DETAILS</strong>
                
                </td>
           
        </tr>
        <tr>
            <td style="width:12% ;">
             <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>               </td>
            <td style="width:44% ;">
                &nbsp;</td>
            <td style="width:12% ;">
                &nbsp;</td>
            <td style="width:32% ;">
                &nbsp;</td>
        </tr>
      
        <tr>
            <td style="width:12%; text-align:left;">
                Employee Code</td>
            <td style="width:44% ;">
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Width="30%" class="NormalText"  MaxLength="6" onkeypress="return NumericCheck(event)"></asp:TextBox>
            </td>
            <td style="width:12% ;text-align:left;">
                Name</td>
            <td style="width:32% ;">
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpName" runat="server" Width="90%" 
                    class="NormalText"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:12% ;text-align:left;">
                Location</td>
            <td style="width:44% ;">
                &nbsp;&nbsp;<asp:DropDownList 
                    ID="cmbBranch" runat="server" class="NormalText" Width="50%">
                </asp:DropDownList>
                
            </td>
            <td style="width:12% ;text-align:left;">
                Department</td>
            <td style="width:32% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDepartment" 
                    runat="server" class="NormalText" Width="91%">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width:12% ;text-align:left;">
                Cadre</td>
            <td style="width:44% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbCadre" runat="server" 
                    class="NormalText" Width="50%">
                </asp:DropDownList>
            </td>
            <td style="width:12% ;text-align:left;">
                Designation</td>
            <td style="width:32% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbDesignation" runat="server" 
                    class="NormalText" Width="91%">
                  <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width:12% ;text-align:left;">
                Date Of Join</td>
            <td style="width:44% ;">
                &nbsp;&nbsp;<asp:TextBox ID="txtDateofJoin" runat="server" class="NormalText" Width="30%"  ReadOnly="true"></asp:TextBox>
                <asp:CalendarExtender ID="CE1" runat="server" TargetControlID="txtDateofJoin" Format="dd MMM yyyy"></asp:CalendarExtender>
                 
            </td>
            <td style="width:12% ;text-align:left;">
                Reporting To</td>
            <td style="width:32% ;">
                &nbsp;&nbsp;<asp:DropDownList 
                    ID="cmbReportingTo" runat="server" class="NormalText" Width="91%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
          <tr>
            <td style="width:12% ;text-align:left;">
                Post</td>
            <td style="width:44% ;">
                &nbsp;&nbsp;<asp:DropDownList 
                    ID="cmbEmpPost" runat="server" class="NormalText" Width="50%">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
              </td>
            <td style="width:12% ;text-align:left;">
                Field Staff&nbsp;&nbsp;&nbsp;</td>
            <td style="width:32% ; text-align:left;">
                &nbsp;&nbsp;<asp:CheckBox ID="chkFieldStaff" runat="server" Text=" " 
                    TextAlign="Left" />
              </td>
        </tr>
          <tr>
            <td style="width:12% ;text-align:left;">
                &nbsp;</td>
            <td style="width:44% ;">
                &nbsp;</td>
            <td style="width:12% ;text-align:left;">
                &nbsp;</td>
            <td style="width:32% ; text-align:left;">
                &nbsp;</td>
        </tr>
           <tr style="text-align:center;">
            <td  colspan="4" style="text-align:center;">
                <input id="btnSave" type="button" value="SAVE" 
                    onclick="return btnSave_onclick()" 
                    style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;<input id="btnExit" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table></br>
</asp:Content>

