﻿
Imports System.Collections
Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq


Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Web.Services
Imports System.Web.Script.Serialization

Partial Public Class ForgotPassword
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim Mobileno As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.txtEmpcode.Attributes.Add("onchange", "return RequestID()")
            txtEmpcode.Focus()

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim LoginUSerID As Integer
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim StrValue As String
        Dim Type As Integer
        StrValue = ""
        Type = 1
        LoginUSerID = CInt(Data(1))

        If (CInt(Data(0)) = 1) Then
            Dim CreatedPassword As String = Data(2).ToString() ''''' GF.GeneratePassword(7)
            Dim StrPassword As String = EncryptDecrypt.Encrypt(CreatedPassword, LoginUSerID.ToString())
            Mobileno = Right(Data(3).ToString(), 10)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(9) As SqlParameter
                Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(0).Value = LoginUSerID
                Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(1).Direction = ParameterDirection.Output
                Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@Passwd", SqlDbType.VarChar, 5000)
                Params(3).Value = StrPassword
                Params(4) = New SqlParameter("@UserData", SqlDbType.VarChar, 5000)
                Params(4).Value = StrValue
                Params(5) = New SqlParameter("@Type", SqlDbType.Int)
                Params(5).Value = Type
                Params(6) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(6).Value = UserID
                Params(7) = New SqlParameter("@RESET", SqlDbType.Int)
                Params(7).Value = 1
                Params(8) = New SqlParameter("@MOBILE", SqlDbType.BigInt)
                Params(8).Value = Mobileno
                Params(9) = New SqlParameter("@IP", SqlDbType.VarChar, 50)
                Params(9).Value = Data(4)
                DB.ExecuteNonQuery("SP_Reset_Password", Params)

                ErrorFlag = CInt(Params(1).Value)
                Message = CStr(Params(2).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            If CInt(Data(1)) > 0 Then
                DT_EMP = DB.ExecuteDataSet("SELECT CONVERT(VARCHAR,a.emp_code)+'^'+a.emp_name+'^'+CONVERT(VARCHAR,a.Branch_ID)+'^'+a.Branch_name+'^'+CONVERT(VARCHAR,a.Department_ID) +'^'+a.Department_name+'^'+a.Designation_name+'^'+CONVERT(VARCHAR,a.Designation_ID) FROM EMP_LIST a WHERE a.EMP_CODE = " & CInt(Data(1)) & "").Tables(0)
                'DT_EMP = GF.GET_EMP_DEATILS(CInt(Data(1)))
                'hid_data.Value = ""
                If DT_EMP.Rows.Count > 0 Then
                    CallBackReturn = "1~" + CStr(DT_EMP.Rows(0).Item(0))
                Else
                    CallBackReturn = "2~" + "Invalid Employee Code!"
                End If
            Else

            End If
        ElseIf CInt(Data(0)) = 3 Then

            Dim RetStr() As String
            Dim RetStr2() As String
            Dim CreatedOTP As String = GF.GenerateOTP(5)
            Dim MsgId As String
            Dim OTPSEND As String = "0"
            Dim Count As Integer = 0
            Dim DT As New DataTable
            Dim StrUrl As String
            DT_EMP = DB.ExecuteDataSet("SELECT case when cug_no = null then mobile when cug_no = '' then mobile else cug_no end as ContactNo FROM EMP_Profile a WHERE a.EMP_CODE = " & CInt(Data(1)) & "").Tables(0)
            'If DT_EMP.Rows.Count > 0 Then
            '    Mobileno = CStr(DT_EMP.Rows(0).Item(0))
            'End If
            If DT_EMP.Rows.Count > 0 Then
                If Not IsDBNull(DT_EMP.Rows(0).Item(0)) Then
                    Mobileno = CStr(DT_EMP.Rows(0).Item(0))
                Else
                    Mobileno = ""
                End If

            End If
            If Mobileno <> "" Then
                Mobileno = "91" + Mobileno.ToString()
                DT = DB.ExecuteDataSet("SELECT sender,username,password FROM SMS_PORTAL WHERE STATUS_ID =1").Tables(0)
                If DT.Rows.Count > 0 Then
                    Dim Sender As String = ""
                    Dim UserName As String = ""
                    Dim PassWord As String = ""
                    Sender = DT.Rows(0).Item(0).ToString()
                    UserName = DT.Rows(0).Item(1).ToString()
                    PassWord = DT.Rows(0).Item(2).ToString()
                    '' Send OTP

                    ''StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=ESAFUAT&password=ESAFUAT18&sender=InfoSMS&SMSText=" & CreatedOTP & "&GSM=" & Mobileno & ""
                    StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "Your OTP to change password is " & CreatedOTP & "&GSM=" & Mobileno & ""

                    RetStr = WEB_Request_Response(StrUrl, 1)

                    If RetStr(6) = "0" Then ' Delivered
                        ''MsgId = RetStr(9)

                        ''System.Threading.Thread.Sleep(10 * 1000)
                        ''StrUrl = "https://api.infobip.com/api/v3/dr/pull?user=" & UserName & "&password=" & PassWord & "&messageid=" + MsgId
                        ''RetStr2 = WEB_Request_Response(StrUrl, 2)
                        ''While RetStr2(0).ToString() = "NO_DATA"
                        ''    If Count = 10 Then
                        ''        Exit While
                        ''    End If
                        ''    RetStr2 = WEB_Request_Response(StrUrl, 2)
                        ''    Count += 1
                        ''End While
                        ''If RetStr2(0).ToString() <> "NO_DATA" Then
                        ''    If InStr(1, RetStr2(10), "DELIVERED", CompareMethod.Text) > 0 Then
                        OTPSEND = CreatedOTP + "Ø" + Mobileno

                        ''    End If
                        ''End If
                    End If
                End If

            Else
                OTPSEND = "NOMOBILE"

            End If
            CallBackReturn = OTPSEND
        End If
    End Sub
#End Region
#Region "Function"

    Public Shared Function WEB_Request_Response(Request As String, Request_type As Integer) As String()

        'Added On 22-10-2020 by 40013 - Changed request from IT
        Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
        Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString

        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        If Request_type = 1 Then
            delimiters = {"<", ">", ">/", "</", "/"}
        Else
            'delimiters = {"<", ">", ">/", "</"}
            delimiters = {"=", " "}
        End If
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)


        Return parts.ToArray()

        ' ''Dim json As String = "[" + parts(1) + "]"
        ' ''Dim js As JavaScriptSerializer = New JavaScriptSerializer()

        ' ''Dim SendDatas() As SendData = js.Deserialize(Of SendData())(json)
        '' ''Dim e As IEnumerable(Of SendData) = SendDatas()
        '' ''Dim SendDatatemp As SendData
        ' ''Return SendDatas.ToArray()


        '' ''Dim array() As SendData = SendDatas.ToArray()
        '' ''Dim e As IEnumerable(Of SendData) = SendDatas.ToArray()
        '' ''Return e.ToArray()


    End Function
#End Region
End Class
