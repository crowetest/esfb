﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="PasswordHelp.aspx.vb" Inherits="PasswordHelp" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ESFB</title>
    <link rel="shortcut icon" href="Image/Logo.png" type="image/x-icon" />
    <link href="Style/PortalStyle.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color:#E31E24">
<form id="form1" runat="server">
    <div style="height:150px;"><img src="Image/Logo.png" style="width:450px; height:150px;">
    </div>
    <div id="MainContents" style="background-color:White; height:370px; width:98%;">
        <div style="float:left; width:48%;">
	        <div class="Box" style="width:100%;">
	            <h2>How safe is your Password</h2>
	            <p>The first step in protecting your online privacy is creating a safe password - i.e. one that a computer program or persistent individual won't easily be able to guess in a short period of time. To help you choose a secure password, we've created a feature that lets you know visually how safe your password is as soon as you create it.</p>
	        </div>
	        <div class="Box" style="width:100%; height:130px;">
  		        <h2>Tips for creating a secure password</h2>
                <ul>
                    <li>Include punctuation marks and/or numbers</li>
                    <li>Mix capital and lowercase letters</li>
                    <li>Include similar looking substitutions, such as the number zero for the letter &#39;O&#39; or &#39;$&#39; for the letter &#39;S&#39;</li>
                    <li>Create a unique acronym</li>
                    <li>Include phonetic replacements, such as &#39;Luv 2 Laf&#39; for &#39;Love to Laugh&#39;</li>
                </ul>
	        </div>
        </div>
        <div style="float:right; width:48%;">
            <div class="Box" style="width:100%; height:130px;">
  	    	    <h2>Tips for keeping your password secure</h2>
                <ul>
                    <li>Never tell your password to anyone (this includes significant others, roommates, parrots, etc.)</li>
                    <li>Never write your password down</li>
                    <li>Never send your password by email</li>
                    <li>Periodically test your current password and change it to a new one</li>
                </ul>
            </div>
	        <div class="Box" style="width:100%; height:130px;">
  		        <h2>Things to Avoid</h2>
                <ul>
                    <li>Don&#39;t reuse passwords for multiple important accounts, such as Gmail and online banking</li>
                    <li>Don&#39;t use a password that is listed as an example of how to pick a good password</li>
                    <li>Don&#39;t use a password that contains personal information (name, birth date, etc.)</li>
                    <li>Don&#39;t use words or acronyms that can be found in a dictionary</li>
                    <li>Don&#39;t use keyboard patterns (asdf) or sequential numbers (1234)</li>
                    <li>Don&#39;t make your password all numbers, uppercase letters or lowercase letters</li>
                    <li>Don&#39;t use repeating characters (aa11)</li>
                </ul>
            </div>
        </div>
    </div>
</form>
</body>
</html>
</asp:Content>
