﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Downloads
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler

    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim Leave As New Leave
    Dim CallBackReturn As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


        Dim TypeID As Integer
        Dim EMP_Code As Integer

        EMP_Code = CInt(Session("UserID"))

        hid_EmpCode.Value = EMP_Code
        DT = GF.GetEmployee(EMP_Code)
        hid_dtls.Value = GF.GetEmployeeString(DT)
        'Get Employee Details

        'DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,content_Type,Caption,View_Flag,Description from dms_ESFB.dbo .hr_policies where status_id=1 and effective_dt<=DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)


        'Dim StrEmployee As String = ""
        'For n As Integer = 0 To DT.Rows.Count - 1
        '    StrEmployee += DT.Rows(n)(0) & "^" & DT.Rows(n)(1) & "^" & DT.Rows(n)(2) & "^" & DT.Rows(n)(3) & "^" & DT.Rows(n)(4) & "^" & DT.Rows(n)(5)
        '    If n < DT.Rows.Count - 1 Then
        '        StrEmployee += "¥"
        '    End If
        'Next


        'hid_dtls.Value = StrEmployee

        hid_User.Value = CStr(DT.Rows(0).Item(1))
        DT = GF.GetPhoto(EMP_Code)
        If DT.Rows.Count > 0 Then
            Dim bytes As Byte() = DirectCast(DT.Rows(0)(0), Byte())
            Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
            hid_image.Value = Convert.ToString("data:" + DT.Rows(0)(1) + ";base64,") & base64String
        Else
            hid_image.Value = "Image/userimage.png"
        End If
        Dim DT_Quali As New DataTable
        Dim DT_Exp As New DataTable
        Dim DT_ID As New DataTable
        Dim DT_ADDR As New DataTable
        Dim DT_FAM As New DataTable
        Dim DT_LANG As New DataTable

        Dim strTS As New StringBuilder


        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill_Risk();", True)
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim LoginUSerID As Integer = 0

        Dim StrEmployee As String = ""
        Dim strTS As New StringBuilder
        If CInt(Data(0)) = 1 Then 'Travel
            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,content_Type,Caption,View_Flag,Description from dms_ESFB.dbo .hr_policies where Dep_ID = 1 and status_id=1 and effective_dt<=DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)



            For n As Integer = 0 To DT.Rows.Count - 1
                StrEmployee += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString
                If n < DT.Rows.Count - 1 Then
                    StrEmployee += "¥"
                End If
            Next
        ElseIf CInt(Data(0)) = 2 Then 'Travel
            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,content_Type,Caption,View_Flag,Description from dms_ESFB.dbo .hr_policies where Dep_ID=3 and status_id=1 and effective_dt<=DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)



            For n As Integer = 0 To DT.Rows.Count - 1
                StrEmployee += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString
                If n < DT.Rows.Count - 1 Then
                    StrEmployee += "¥"
                End If
            Next

        ElseIf CInt(Data(0)) = 3 Then 'Travel
            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,content_Type,Caption,View_Flag,Description from dms_ESFB.dbo .hr_policies where Dep_ID=4 and status_id=1 and effective_dt<=DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)



            For n As Integer = 0 To DT.Rows.Count - 1
                StrEmployee += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString
                If n < DT.Rows.Count - 1 Then
                    StrEmployee += "¥"
                End If
            Next
        ElseIf CInt(Data(0)) = 4 Then 'Travel
            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,content_Type,Caption,View_Flag,Description from dms_ESFB.dbo .hr_policies where Dep_ID=5 and status_id=1 and effective_dt<=DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)



            For n As Integer = 0 To DT.Rows.Count - 1
                StrEmployee += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString
                If n < DT.Rows.Count - 1 Then
                    StrEmployee += "¥"
                End If
            Next

        ElseIf CInt(Data(0)) = 5 Then 'Travel
            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,content_Type,Caption,View_Flag,Description from dms_ESFB.dbo .hr_policies where Dep_ID=6 and status_id=1 and effective_dt<=DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)



            For n As Integer = 0 To DT.Rows.Count - 1
                StrEmployee += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString
                If n < DT.Rows.Count - 1 Then
                    StrEmployee += "¥"
                End If
            Next
        ElseIf CInt(Data(0)) = 11 Then 'Travel
            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,content_Type,Caption,View_Flag,Description from dms_ESFB.dbo .hr_policies where Dep_ID=2 and status_id=1 and effective_dt<=DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)



            For n As Integer = 0 To DT.Rows.Count - 1
                StrEmployee += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString
                If n < DT.Rows.Count - 1 Then
                    StrEmployee += "¥"
                End If
            Next

        End If
        CallBackReturn = StrEmployee.ToString
    End Sub
End Class
