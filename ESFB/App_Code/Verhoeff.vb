﻿Imports Microsoft.VisualBasic

Public Class Verhoeff
    'The multiplication table
    Shared ReadOnly d(,) As Integer = _
    {{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, _
    {1, 2, 3, 4, 0, 6, 7, 8, 9, 5}, _
    {2, 3, 4, 0, 1, 7, 8, 9, 5, 6}, _
    {3, 4, 0, 1, 2, 8, 9, 5, 6, 7}, _
    {4, 0, 1, 2, 3, 9, 5, 6, 7, 8}, _
    {5, 9, 8, 7, 6, 0, 4, 3, 2, 1}, _
    {6, 5, 9, 8, 7, 1, 0, 4, 3, 2}, _
    {7, 6, 5, 9, 8, 2, 1, 0, 4, 3}, _
    {8, 7, 6, 5, 9, 3, 2, 1, 0, 4}, _
    {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}}

    'The permutation table
    Shared ReadOnly p(,) As Integer = _
    {{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, _
    {1, 5, 7, 6, 2, 8, 3, 0, 9, 4}, _
    {5, 8, 0, 3, 7, 9, 6, 1, 4, 2}, _
    {8, 9, 1, 6, 0, 4, 3, 5, 2, 7}, _
    {9, 4, 5, 3, 1, 2, 6, 8, 7, 0}, _
    {4, 2, 8, 6, 5, 7, 3, 9, 0, 1}, _
    {2, 7, 9, 3, 8, 0, 6, 4, 1, 5}, _
    {7, 0, 4, 6, 9, 1, 3, 2, 5, 8}}

    'The inverse table
    Shared ReadOnly inv() As Integer = {0, 4, 3, 2, 1, 5, 6, 7, 8, 9}



    ''' <summary>
    ''' Validates that an entered number is Verhoeff compliant.
    ''' </summary>
    ''' <param name="num"></param>
    ''' <returns>True if Verhoeff compliant, otherwise false</returns>
    ''' <remarks>Make sure the check digit is the last one!</remarks>
    Public Shared Function validateVerhoeff(ByVal num As String) As Boolean

        Dim c As Integer = 0
        Dim myArray() As Integer = StringToReversedIntArray(num)

        For i As Integer = 0 To myArray.Length - 1
            c = d(c, p((i Mod 8), myArray(i)))
        Next i

        Return c.Equals(0)

    End Function


    ''' <summary>
    ''' For a given number generates a Verhoeff digit
    ''' </summary>
    ''' <param name="num"></param>
    ''' <returns>Verhoeff check digit as string</returns>
    ''' <remarks>Append this check digit to num</remarks>
    Public Shared Function generateVerhoeff(ByVal num As String) As String

        Dim c As Integer = 0
        Dim myArray() As Integer = StringToReversedIntArray(num)

        For i As Integer = 0 To myArray.Length - 1
            c = d(c, p(((i + 1) Mod 8), myArray(i)))
        Next i

        Return inv(c).ToString

    End Function


    ''' <summary>
    ''' Converts a string to a reversed integer array.
    ''' </summary>
    ''' <param name="str"></param>
    ''' <returns>Reversed integer array</returns>
    ''' <remarks></remarks>
    Private Shared Function StringToReversedIntArray(ByVal str As String) As Integer()

        Dim myArray(str.Length - 1) As Integer

        For i As Integer = 0 To str.Length - 1
            myArray(i) = Convert.ToInt16(str.Substring(i, 1))
            'we could use myArray(i) = Convert.ToInt16(str(i)) - 48 '48 is from Convert.ToInt16("0"c)
        Next

        Array.Reverse(myArray)

        Return myArray

        'A more modern version of this may look something like this
        'Return (From I In str.ToArray
        '        Select CInt(I.ToString)).Reverse.ToArray

    End Function
End Class
