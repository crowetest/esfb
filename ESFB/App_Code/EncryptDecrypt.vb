﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Security.Cryptography
Imports System.IO
Public Class EncryptDecrypt
    Public Shared hashAlgorithm As String = "SHA1"
    Public Shared initVector As String = "@1B2c3D4e5F6g7H8I9"
    Public Shared keySize As Integer = &H100
    Public Shared passPhrase As String = "magnicode001001"
    Public Shared passwordIterations As Integer = 10
    Public Shared saltValue As String
    ' = "mag2012aec";
    ' public static string saltValue = "mag2012aec";
    ' Methods
    Public Shared Function Decrypt(ByVal cipherText As String) As String
        Dim bytes As Byte() = Encoding.ASCII.GetBytes(initVector)
        Dim rgbSalt As Byte() = Encoding.ASCII.GetBytes(saltValue)
        Dim buffer As Byte() = Convert.FromBase64String(cipherText)
        Dim rgbKey As Byte() = New PasswordDeriveBytes(passPhrase, rgbSalt, hashAlgorithm, passwordIterations).GetBytes(keySize \ 8)
        Dim managed As New RijndaelManaged()
        managed.Mode = CipherMode.CBC
        Dim transform As ICryptoTransform = managed.CreateDecryptor(rgbKey, bytes)
        Dim stream As New MemoryStream(buffer)
        Dim stream2 As New CryptoStream(stream, transform, CryptoStreamMode.Read)
        Dim buffer5 As Byte() = New Byte(buffer.Length - 1) {}
        Dim count As Integer = stream2.Read(buffer5, 0, buffer5.Length)
        stream.Close()
        stream2.Close()
        Return Encoding.UTF8.GetString(buffer5, 0, count)
    End Function

    Public Shared Function Encrypt(ByVal plainText As String) As String
        Dim bytes As Byte() = Encoding.ASCII.GetBytes(initVector)
        Dim rgbSalt As Byte() = Encoding.ASCII.GetBytes(saltValue)
        Dim buffer As Byte() = Encoding.UTF8.GetBytes(plainText)
        Dim rgbKey As Byte() = New PasswordDeriveBytes(passPhrase, rgbSalt, hashAlgorithm, passwordIterations).GetBytes(keySize \ 8)
        Dim managed As New RijndaelManaged()
        managed.Mode = CipherMode.CBC
        Dim transform As ICryptoTransform = managed.CreateEncryptor(rgbKey, bytes)
        Dim stream As New MemoryStream()
        Dim stream2 As New CryptoStream(stream, transform, CryptoStreamMode.Write)
        stream2.Write(buffer, 0, buffer.Length)
        stream2.FlushFinalBlock()
        Dim inArray As Byte() = stream.ToArray()
        stream.Close()
        stream2.Close()
        Return Convert.ToBase64String(inArray)
    End Function

    Public Shared Function Decrypt(ByVal cipherText As String, ByVal saltValue As String) As String
        Dim bytes As Byte() = Encoding.ASCII.GetBytes(initVector)
        Dim rgbSalt As Byte() = Encoding.ASCII.GetBytes(saltValue)
        Dim buffer As Byte() = Convert.FromBase64String(cipherText)
        Dim rgbKey As Byte() = New PasswordDeriveBytes(passPhrase, rgbSalt, hashAlgorithm, passwordIterations).GetBytes(keySize \ 8)
        Dim managed As New RijndaelManaged()
        managed.Mode = CipherMode.CBC
        Dim transform As ICryptoTransform = managed.CreateDecryptor(rgbKey, bytes)
        Dim stream As New MemoryStream(buffer)
        Dim stream2 As New CryptoStream(stream, transform, CryptoStreamMode.Read)
        Dim buffer5 As Byte() = New Byte(buffer.Length - 1) {}
        Dim count As Integer = stream2.Read(buffer5, 0, buffer5.Length)
        stream.Close()
        stream2.Close()
        Return Encoding.UTF8.GetString(buffer5, 0, count)
    End Function

    Public Shared Function Encrypt(ByVal plainText As String, ByVal saltValue As String) As String
        Dim bytes As Byte() = Encoding.ASCII.GetBytes(initVector)
        Dim rgbSalt As Byte() = Encoding.ASCII.GetBytes(saltValue)
        Dim buffer As Byte() = Encoding.UTF8.GetBytes(plainText)
        Dim rgbKey As Byte() = New PasswordDeriveBytes(passPhrase, rgbSalt, hashAlgorithm, passwordIterations).GetBytes(keySize \ 8)
        Dim managed As New RijndaelManaged()
        managed.Mode = CipherMode.CBC
        Dim transform As ICryptoTransform = managed.CreateEncryptor(rgbKey, bytes)
        Dim stream As New MemoryStream()
        Dim stream2 As New CryptoStream(stream, transform, CryptoStreamMode.Write)
        stream2.Write(buffer, 0, buffer.Length)
        stream2.FlushFinalBlock()
        Dim inArray As Byte() = stream.ToArray()
        stream.Close()
        stream2.Close()
        Return Convert.ToBase64String(inArray)
    End Function
End Class
