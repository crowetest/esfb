﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Public Class Master
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Public Function GetEmail(ByVal emp_code As Integer) As DataTable
        Return DB.ExecuteDataSet("select isnull(official_mail_id,'') as mail_ID from emp_profile where emp_code=" & emp_code & "").Tables(0)
    End Function
    Public Function GET_Region(ByVal Region As String) As DataTable
        Return DB.ExecuteDataSet("Select ROW_NUMBER() OVER (ORDER BY Region_Name ) as row,upper(Region_Name) as Region ,upper(s.emp_name) as emp_name,b.HolidayZone from Region_master a inner join holiday_zone b on a.Zone_ID=b.HolidayZoneID inner join emp_master s on s.emp_code=a.Region_head where Region_name like'" & Region & "%' and a.Status_ID=1").Tables(0)
    End Function
    Public Function GET_AREA(ByVal Region As Integer) As DataTable
        Return DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY a.area_name ) as row,upper(a.area_name) as Area_name,upper(s.Emp_Name) as Emp_name  from  area_master a inner join EMP_MASTER s on a.Area_Head=s.emp_code where a.Status_ID=1 and a.Region_ID= " & Region & " order by a.area_name").Tables(0)
    End Function
    Public Function GetState() As DataTable
        Return DB.ExecuteDataSet("select -1 as State_ID,' -----Select-----' as State_Name union all  Select State_ID,State_Name  from dbo.STATE_MASTER order by State_Name").Tables(0)
    End Function
    Public Function GetRetailOffice() As DataTable
        Return DB.ExecuteDataSet("select -1 as Branch_ID,' -----Select-----' as  Branch_Name union all   select Branch_ID,Branch_Name from dbo. BRANCH_MASTER where type_id=1 order by  branch_name").Tables(0)
    End Function
   
    Public Function GetDistrict(ByVal StateID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as District_id,' -----Select-----' as District_Name union all  select District_ID,District_Name from dbo.DISTRICT_MASTER where State_ID=" & StateID & " order by District_Name").Tables(0)
    End Function
    Public Function GetArea() As DataTable
        Return DB.ExecuteDataSet("select -1 as Area_ID,' -----Select-----' as Area_Name union all  Select Area_ID,Area_Name  from Area_MASTER where area_id<>0 order by Area_Name").Tables(0)
    End Function
    Public Function GetRoleType() As DataTable
        Return DB.ExecuteDataSet("select -1 as Role_id,' -----Select-----' as role_Name,1 union all  select Role_ID,Role_Name,1 from dbo.ROLE_MASTER where Status_ID=1 order by Role_Name").Tables(0)
    End Function
    Public Function GetModule() As DataTable
        Return DB.ExecuteDataSet("select -1 as MODULE_ID,' -----Select-----' as MODULE_NAME union all  select MODULE_ID,MODULE_NAME from MODULE_MASTER where STATUS_ID=1 order by MODULE_NAME").Tables(0)
    End Function

    Public Function GetRoleFormList(ByVal Roleid As Integer, ByVal module_id As Integer) As DataTable
        Return DB.ExecuteDataSet("SELECT Menu_ID,upper(Menu_Parent_Name),upper(Menu_Name),STATUS_ID FROM (SELECT Menu_ID,Menu_Parent_Name,Menu_Name ,1 AS STATUS_ID FROM MENU_MASTER A,ROLE_DTL B WHERE A.Menu_ID = B.FORM_ID AND ROLE_ID = " & Roleid & "  AND A.Module_ID = " & module_id & "  AND A.Status_ID = 1 and a.Menu_URL is not null UNION SELECT Menu_ID,Menu_Parent_Name,Menu_Name,2 AS STATUS_ID FROM MENU_MASTER A WHERE A.Module_ID = " & module_id & "  AND A.Status_ID = 1  and a.Menu_URL is not null AND  A.Menu_ID NOT IN (SELECT FORM_ID FROM ROLE_DTL WHERE ROLE_ID = " & Roleid & " )) AA ORDER BY Menu_Parent_Name,MENU_NAME").Tables(0)

    End Function

    Public Function GetStr_Form(ByRef DT As DataTable) As String
        Dim StrForm As String = ""

        For n As Integer = 0 To DT.Rows.Count - 1
            StrForm += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3)
            If n < DT.Rows.Count - 1 Then
                StrForm += "¥"
            End If
        Next
        Return StrForm
    End Function
    Public Function GetStr_Role(ByRef DT As DataTable) As String
        Dim StrForm As String = ""

        For n As Integer = 0 To DT.Rows.Count - 1
            StrForm += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1)
            If n < DT.Rows.Count - 1 Then
                StrForm += "¥"
            End If
        Next
        Return StrForm
    End Function
    Public Function GetRoleNames() As DataTable
        Return DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY Role_Name) AS Row,upper(Role_Name) from dbo.ROLE_MASTER where Status_ID=1 order by Role_Name").Tables(0)
    End Function
    Public Function GetEmpRoleList(ByVal Emp_Code As Integer) As DataTable
        Return DB.ExecuteDataSet("Select ROW_NUMBER() OVER (ORDER BY b.Role_Name) AS Row,upper(b.ROLE_NAME),a.Role_id,a.Emp_Code  from ROLES_ASSIGNED a,ROLE_MASTER b where a.ROLE_ID=b.ROLE_ID and a.STATUS_ID=1 and a.emp_code=" & Emp_Code & "").Tables(0)

    End Function
    Public Function GetStr_EmpRole(ByRef DT As DataTable) As String
        Dim StrForm As String = ""

        For n As Integer = 0 To DT.Rows.Count - 1
            StrForm += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1)
            If n < DT.Rows.Count - 1 Then
                StrForm += "¥"
            End If
        Next
        Return StrForm
    End Function
    Public Function GetStr_EmpRoleAssign(ByRef DT As DataTable) As String
        Dim StrForm As String = ""

        For n As Integer = 0 To DT.Rows.Count - 1
            StrForm += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3)
            If n < DT.Rows.Count - 1 Then
                StrForm += "¥"
            End If
        Next
        Return StrForm
    End Function
    Public Function DeleteRoleAssign(ByVal Emp_Code As Integer, ByVal RoleId As Integer) As Integer
        Return DB.ExecuteNonQuery("DELETE from ROLES_ASSIGNED  where ROLE_ID=" & RoleId & " and emp_code=" & Emp_Code & "").ToString

    End Function
    Public Function GetShiftSchedule(ByVal BranchID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as ID,' -----Select-----' as Name union all  Select a.shift_id,c.shift_name+'('+ c.shift_descr +')'  from dbo.emp_shift_MASTER a,Branch_shift b,emp_shift_master c where a.Shift_ID=b.Shift_ID and b.shift_id=c.shift_id and b.branch_id=" & BranchID & " ").Tables(0)
    End Function
End Class
