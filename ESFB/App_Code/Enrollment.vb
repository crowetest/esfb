﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Public Class Enrollment
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim SM As New SMTP
    Public Function GetBranch(Optional ByVal type As Integer = 0) As DataTable
        Return GN.GetQueryResult(7, 0, type)
    End Function
    Public Function GetCadreForPromotion(ByVal UserID As Integer, ByVal TypeID As Integer) As DataTable
        ' Return DB.ExecuteDataSet("select '-1' as Branch_id,' -----Select-----' as Branch_Name union all select cast(Branch_id as varchar(5))+'Ø'+cast(status_ID as varchar(2)) as Branch_ID,Branch_Name from Branch_MASTER where status_id >0 order by 2").Tables(0)
        Return GN.GetQueryResult(8, UserID, TypeID)
    End Function
    Public Function GetDepartment() As DataTable
        Return DB.ExecuteDataSet("select -1 as Department_id,' -----Select-----' as Department_Name union all select Department_id,Department_Name from Department_MASTER where status_id=1  order by 2").Tables(0)
    End Function
    Public Function GetDesignation() As DataTable
        Return DB.ExecuteDataSet("select -1 as Designation_id,' -----Select-----' as Designation_Name union all select Designation_id,Designation_Name from Designation_MASTER where status_id=1 order by 2").Tables(0)
    End Function
    Public Function GetDesignationDOJ() As DataTable
        Return DB.ExecuteDataSet("select -1 as Designation_id,' -----Select-----' as Designation_Name union all select Designation_id,Designation_Name from Designation_MASTER order by 2").Tables(0)
    End Function
    Public Function GetDesignation(ByVal CadreID As Integer) As DataTable
        Return GN.GetQueryResult(9, 0, CadreID)
    End Function
    Public Function GetCadre() As DataTable
        Return DB.ExecuteDataSet("select -1 as Cadre_id,' -----Select-----' as Cadre_Name union all select Cadre_id,Cadre_Name from Cadre_MASTER where status_id =1 order by 2").Tables(0)
    End Function
    Public Function GetGender() As DataTable
        Return DB.ExecuteDataSet("select -1 as Gender_id,' -----Select-----' as Gender_Name union all select Gender_id,Gender_Name from Gender_MASTER where status_id =1 order by 1").Tables(0)
    End Function
    Public Function GetReportingTo() As DataTable
        Return DB.ExecuteDataSet("select -1 as Emp_Code,' -----Select-----' as Emp_Name union all select Emp_Code,Emp_Name from emp_MASTER where status_id =1 order by 2").Tables(0)
    End Function
    Public Function GetEmpStatus() As DataTable
        Return DB.ExecuteDataSet("select -1 as Status_ID,' -----Select-----' as Status union all select Status_ID,Status from Emp_Status where status_id <>1 order by 1").Tables(0)
    End Function
    Public Function GetEmployeeStatus() As DataTable
        Return DB.ExecuteDataSet("select -1 as Status_ID,' -----Select-----' as Status union all select Emp_Status_id as Status_ID,emp_status as Status from EMPLOYEE_STATUS  where status_id =1 order by 1").Tables(0)
    End Function
    Public Function GetBranches() As DataTable
        Return DB.ExecuteDataSet("select -1 as branch_id,' -----Select-----' as branch_name union all select branch_id,branch_name from branch_master where status_id=1 ").Tables(0)
    End Function
    Public Function EnrollEmployee(ByVal EmpCode As Integer, ByVal EmpName As String, ByVal BranchID As Integer, ByVal DepartmentID As Integer, ByVal CadreID As Integer, ByVal DesignationID As Integer, ByVal DateOfJoin As Date, ByVal GenderID As Integer, ByVal ReportingTo As Integer, ByVal isFieldstaff As Integer, ByVal UserID As Integer, ByVal CareOf As String, ByVal HouseName As String, ByVal Location As String, ByVal City As String, ByVal POstID As Integer, ByVal Email As String, ByVal Landline As String, ByVal Mobile As String, ByVal BasicPay As Double, ByVal DA As Double, ByVal HRA As Double, ByVal Conveyance As Double, ByVal SpecialAllowance As Double, ByVal LocalAllowance As Double, ByVal MedicalAllowance As Double, ByVal HospitalityAllowance As Double, ByVal PerformanceAllowance As Double, ByVal OtherAllowance As Double, ByVal FieldAllowance As Double, ByVal HasEsi As Integer, ByVal HasPF As Integer, ByVal HasSWF As Integer, ByVal HasESWT As Integer, ByVal HasCharity As Integer, ByVal DOB As Date, ByVal MstatusID As Integer, ByVal CareofPhone As String, ByVal IDProofType As Integer, ByVal IDProofNumber As String, ByVal AddressProofType As Integer, ByVal AddressProofNumber As String, ByVal QualificationDtl As String, ByVal EmpPost As Integer, Optional ByVal Father As String = "", Optional ByVal Mother As String = "", Optional ByVal ReligionId As Integer = 0, Optional ByVal CasteId As Integer = 0, Optional ByVal IsTwowheeler As Integer = 0, Optional ByVal DrivingNo As String = "", Optional ByVal IsSangamMember As Integer = 0, Optional ByVal MarriageDate As String = "15/08/1947", Optional ByVal MotherTongueId As Integer = 0, Optional ByVal BloodGroupId As Integer = 0, Optional ByVal Cug As String = "", Optional ByVal OfficialEmail As String = "", Optional ByVal ChkPanch As Integer = 0, Optional ByVal chkComm As Integer = 0, Optional ByVal chkPolice As Integer = 0, Optional ByVal newstrExp As String = "", Optional ByVal newstrID As String = "", Optional ByVal newstrAddress As String = "", Optional ByVal LangKnown As String = "", Optional ByVal EmpType As Integer = 0, Optional ByVal SuretyType As Integer = 0, Optional ByVal SuretyName As String = "", Optional ByVal SuretyAddr As String = "", Optional ByVal SuretyPhone As String = "", Optional ByVal Entity As Integer = 0, Optional ByVal newstrFamily As String = "", Optional ByVal OTP As String = "", Optional ByVal SMS As String = "", Optional ByVal ip As String = "") As String
        Dim ErrorFlag As Integer = 0
        Dim Message As String = ""
        Try
            Dim Params(75) As SqlParameter
            Params(0) = New SqlParameter("@TrNo", SqlDbType.Int)
            Params(0).Value = 1
            Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(1).Value = EmpCode
            Params(2) = New SqlParameter("@EmpName", SqlDbType.VarChar, 50)
            Params(2).Value = EmpName
            Params(3) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(3).Value = BranchID
            Params(4) = New SqlParameter("@DepartmentID", SqlDbType.Int)
            Params(4).Value = DepartmentID
            Params(5) = New SqlParameter("@CadreID", SqlDbType.Int)
            Params(5).Value = CadreID
            Params(6) = New SqlParameter("@DesignationID", SqlDbType.Int)
            Params(6).Value = DesignationID
            Params(7) = New SqlParameter("@GenderID", SqlDbType.Int)
            Params(7).Value = GenderID
            Params(8) = New SqlParameter("@DateOfJoin", SqlDbType.Date)
            Params(8).Value = DateOfJoin
            Params(9) = New SqlParameter("@EmpPassword", SqlDbType.VarChar, 50)
            Params(9).Value = EncryptDecrypt.Encrypt(OTP, EmpCode.ToString())
            Params(10) = New SqlParameter("@ReportingTo", SqlDbType.Int)
            Params(10).Value = ReportingTo
            Params(11) = New SqlParameter("@IsFieldStaff", SqlDbType.Int)
            Params(11).Value = isFieldstaff
            Params(12) = New SqlParameter("@CareOF", SqlDbType.VarChar, 50)
            Params(12).Value = CareOf
            Params(13) = New SqlParameter("@HouseName", SqlDbType.VarChar, 50)
            Params(13).Value = HouseName
            Params(14) = New SqlParameter("@Location", SqlDbType.VarChar, 50)
            Params(14).Value = Location
            Params(15) = New SqlParameter("@City", SqlDbType.VarChar, 50)
            Params(15).Value = City
            Params(16) = New SqlParameter("@PostID", SqlDbType.Int)
            Params(16).Value = POstID
            Params(17) = New SqlParameter("@Email", SqlDbType.VarChar, 50)
            Params(17).Value = Email
            Params(18) = New SqlParameter("@Landline", SqlDbType.VarChar, 50)
            Params(18).Value = Landline
            Params(19) = New SqlParameter("@Mobile", SqlDbType.VarChar, 50)
            Params(19).Value = Mobile
            Params(20) = New SqlParameter("@BasicPay", SqlDbType.Money)
            Params(20).Value = BasicPay
            Params(21) = New SqlParameter("@DA", SqlDbType.Money)
            Params(21).Value = DA
            Params(22) = New SqlParameter("@HRA", SqlDbType.Money)
            Params(22).Value = HRA
            Params(23) = New SqlParameter("@Conveyance", SqlDbType.Money)
            Params(23).Value = Conveyance
            Params(24) = New SqlParameter("@SpecialAllowance", SqlDbType.Money)
            Params(24).Value = SpecialAllowance
            Params(25) = New SqlParameter("@LocalAllowance", SqlDbType.Money)
            Params(25).Value = LocalAllowance
            Params(26) = New SqlParameter("@MedicalAllowance", SqlDbType.Money)
            Params(26).Value = MedicalAllowance
            Params(27) = New SqlParameter("@HospitalityAllowance", SqlDbType.Money)
            Params(27).Value = HospitalityAllowance
            Params(28) = New SqlParameter("@PerformanceAllowance", SqlDbType.Money)
            Params(28).Value = PerformanceAllowance
            Params(29) = New SqlParameter("@OtherAllowance", SqlDbType.Money)
            Params(29).Value = OtherAllowance
            Params(30) = New SqlParameter("@FieldAllowance", SqlDbType.Money)
            Params(30).Value = FieldAllowance
            Params(31) = New SqlParameter("@ESI", SqlDbType.Int)
            Params(31).Value = HasEsi
            Params(32) = New SqlParameter("@PF", SqlDbType.Int)
            Params(32).Value = HasPF
            Params(33) = New SqlParameter("@SWF", SqlDbType.Int)
            Params(33).Value = HasSWF
            Params(34) = New SqlParameter("@ESWT", SqlDbType.Int)
            Params(34).Value = HasESWT
            Params(35) = New SqlParameter("@Charity", SqlDbType.Int)
            Params(35).Value = HasCharity
            Params(36) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(36).Value = UserID
            Params(37) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(37).Direction = ParameterDirection.Output
            Params(38) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(38).Direction = ParameterDirection.Output
            Params(39) = New SqlParameter("@DOB", SqlDbType.Date)
            Params(39).Value = DOB
            Params(40) = New SqlParameter("@MStatusID", SqlDbType.Int)
            Params(40).Value = MstatusID
            Params(41) = New SqlParameter("@CareOfPhone", SqlDbType.VarChar, 50)
            Params(41).Value = CareofPhone
            Params(42) = New SqlParameter("@IDProofType", SqlDbType.Int)
            Params(42).Value = IDProofType
            Params(43) = New SqlParameter("@IDProofNumber", SqlDbType.VarChar, 50)
            Params(43).Value = IDProofNumber
            Params(44) = New SqlParameter("@AddressProofType", SqlDbType.Int)
            Params(44).Value = AddressProofType
            Params(45) = New SqlParameter("@AddressProofNumber", SqlDbType.VarChar, 50)
            Params(45).Value = AddressProofNumber
            Params(46) = New SqlParameter("@QualificationDtl", SqlDbType.VarChar, 5000)
            Params(46).Value = QualificationDtl
            Params(47) = New SqlParameter("@EmpPost", SqlDbType.VarChar, 500)
            Params(47).Value = EmpPost
            Params(48) = New SqlParameter("@Father_Name", SqlDbType.VarChar, 500)
            Params(48).Value = Father
            Params(49) = New SqlParameter("@Mother_Name", SqlDbType.VarChar, 500)
            Params(49).Value = Mother
            Params(50) = New SqlParameter("@ReligionId", SqlDbType.Int)
            Params(50).Value = ReligionId
            Params(51) = New SqlParameter("@Caste_id", SqlDbType.Int)
            Params(51).Value = CasteId
            Params(52) = New SqlParameter("@Twowheeler_Status", SqlDbType.Int)
            Params(52).Value = IsTwowheeler
            Params(53) = New SqlParameter("@Driving_License_No", SqlDbType.VarChar, 500)
            Params(53).Value = DrivingNo
            Params(54) = New SqlParameter("@Is_Sangam_member", SqlDbType.Int)
            Params(54).Value = IsSangamMember
            Params(55) = New SqlParameter("@Marriage_Date", SqlDbType.Date)
            If MarriageDate = "" Then
                Params(55).Value = CDate("01/01/1900")
            Else
                Params(55).Value = CDate(MarriageDate)
            End If

            Params(56) = New SqlParameter("@Mother_tongue", SqlDbType.Int)
            Params(56).Value = MotherTongueId
            Params(57) = New SqlParameter("@Blood_Group", SqlDbType.Int)
            Params(57).Value = BloodGroupId
            Params(58) = New SqlParameter("@Cug_No", SqlDbType.VarChar, 500)
            Params(58).Value = Cug
            Params(59) = New SqlParameter("@Official_mail_id", SqlDbType.VarChar, 500)
            Params(59).Value = OfficialEmail
            Params(60) = New SqlParameter("@Ref_Letter_Panchayath", SqlDbType.Int)
            Params(60).Value = ChkPanch
            Params(61) = New SqlParameter("@Ref_Letter_Community", SqlDbType.Int)
            Params(61).Value = chkComm
            Params(62) = New SqlParameter("@Ref_Letter_PoliceStation", SqlDbType.Int)
            Params(62).Value = chkPolice
            Params(63) = New SqlParameter("@Experience", SqlDbType.VarChar, 5000)
            Params(63).Value = newstrExp
            Params(64) = New SqlParameter("@IDProof", SqlDbType.VarChar, 5000)
            Params(64).Value = newstrID
            Params(65) = New SqlParameter("@AddressProof", SqlDbType.VarChar, 5000)
            Params(65).Value = newstrAddress
            Params(66) = New SqlParameter("@LangKnown", SqlDbType.VarChar, 5000)
            Params(66).Value = LangKnown
            Params(67) = New SqlParameter("@EmpType", SqlDbType.Int)
            Params(67).Value = EmpType
            Params(68) = New SqlParameter("@SuretyType", SqlDbType.Int)
            Params(68).Value = SuretyType
            Params(69) = New SqlParameter("@SuretyName", SqlDbType.VarChar, 500)
            Params(69).Value = SuretyName
            Params(70) = New SqlParameter("@SuretyAddr", SqlDbType.VarChar, 500)
            Params(70).Value = SuretyAddr
            Params(71) = New SqlParameter("@SuretyPhone", SqlDbType.VarChar, 500)
            Params(71).Value = SuretyPhone
            Params(72) = New SqlParameter("@Entity", SqlDbType.Int)
            Params(72).Value = Entity
            Params(73) = New SqlParameter("@Family", SqlDbType.VarChar, 5000)
            Params(73).Value = newstrFamily
            Params(74) = New SqlParameter("@SMS", SqlDbType.VarChar, 200)
            Params(74).Value = SMS
            Params(75) = New SqlParameter("@ip", SqlDbType.VarChar, 50)
            Params(75).Value = ip
            DB.ExecuteNonQuery("SP_Enrollment", Params)
            ErrorFlag = CInt(Params(37).Value)
            Message = CStr(Params(38).Value)

        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        Return ErrorFlag.ToString() + "Ø" + Message
    End Function

    Public Function UpdateEmployee_HR(ByVal EmpCode As Integer, ByVal EmpName As String, ByVal BranchID As Integer, ByVal DepartmentID As Integer, ByVal CadreID As Integer, ByVal DesignationID As Integer, ByVal DateOfJoin As Date, ByVal GenderID As Integer, ByVal ReportingTo As Integer, ByVal isFieldstaff As Integer, ByVal UserID As Integer, ByVal CareOf As String, ByVal HouseName As String, ByVal Location As String, ByVal City As String, ByVal POstID As Integer, ByVal Email As String, ByVal Landline As String, ByVal Mobile As String, ByVal BasicPay As Double, ByVal DA As Double, ByVal HRA As Double, ByVal Conveyance As Double, ByVal SpecialAllowance As Double, ByVal LocalAllowance As Double, ByVal MedicalAllowance As Double, ByVal HospitalityAllowance As Double, ByVal PerformanceAllowance As Double, ByVal OtherAllowance As Double, ByVal FieldAllowance As Double, ByVal HasEsi As Integer, ByVal HasPF As Integer, ByVal HasSWF As Integer, ByVal HasESWT As Integer, ByVal HasCharity As Integer, ByVal DOB As Date, ByVal MstatusID As Integer, ByVal CareofPhone As String, ByVal IDProofType As Integer, ByVal IDProofNumber As String, ByVal AddressProofType As Integer, ByVal AddressProofNumber As String, ByVal QualificationDtl As String, ByVal EmpPost As Integer, Optional ByVal Father As String = "", Optional ByVal Mother As String = "", Optional ByVal ReligionId As Integer = 0, Optional ByVal CasteId As Integer = 0, Optional ByVal IsTwowheeler As Integer = 0, Optional ByVal DrivingNo As String = "", Optional ByVal IsSangamMember As Integer = 0, Optional ByVal MarriageDate As String = "15/08/1947", Optional ByVal MotherTongueId As Integer = 0, Optional ByVal BloodGroupId As Integer = 0, Optional ByVal Cug As String = "", Optional ByVal OfficialEmail As String = "", Optional ByVal ChkPanch As Integer = 0, Optional ByVal chkComm As Integer = 0, Optional ByVal chkPolice As Integer = 0, Optional ByVal newstrExp As String = "", Optional ByVal newstrID As String = "", Optional ByVal newstrAddress As String = "", Optional ByVal LangKnown As String = "", Optional ByVal EmpType As Integer = 0, Optional ByVal SuretyType As Integer = 0, Optional ByVal SuretyName As String = "", Optional ByVal SuretyAddr As String = "", Optional ByVal SuretyPhone As String = "", Optional ByVal Entity As String = "", Optional ByVal newstrFamily As String = "") As String
        Dim ErrorFlag As Integer = 0
        Dim Message As String = ""
        Try
            Dim Params(73) As SqlParameter
            Params(0) = New SqlParameter("@TrNo", SqlDbType.Int)
            Params(0).Value = 1
            Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(1).Value = EmpCode
            Params(2) = New SqlParameter("@EmpName", SqlDbType.VarChar, 50)
            Params(2).Value = EmpName
            Params(3) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(3).Value = BranchID
            Params(4) = New SqlParameter("@DepartmentID", SqlDbType.Int)
            Params(4).Value = DepartmentID
            Params(5) = New SqlParameter("@CadreID", SqlDbType.Int)
            Params(5).Value = CadreID
            Params(6) = New SqlParameter("@DesignationID", SqlDbType.Int)
            Params(6).Value = DesignationID
            Params(7) = New SqlParameter("@GenderID", SqlDbType.Int)
            Params(7).Value = GenderID
            Params(8) = New SqlParameter("@DateOfJoin", SqlDbType.Date)
            Params(8).Value = DateOfJoin
            Params(9) = New SqlParameter("@EmpPassword", SqlDbType.VarChar, 50)
            Params(9).Value = EncryptDecrypt.Encrypt("Esaf123", EmpCode.ToString())
            Params(10) = New SqlParameter("@ReportingTo", SqlDbType.Int)
            Params(10).Value = ReportingTo
            Params(11) = New SqlParameter("@IsFieldStaff", SqlDbType.Int)
            Params(11).Value = isFieldstaff
            Params(12) = New SqlParameter("@CareOF", SqlDbType.VarChar, 50)
            Params(12).Value = CareOf
            Params(13) = New SqlParameter("@HouseName", SqlDbType.VarChar, 50)
            Params(13).Value = HouseName
            Params(14) = New SqlParameter("@Location", SqlDbType.VarChar, 50)
            Params(14).Value = Location
            Params(15) = New SqlParameter("@City", SqlDbType.VarChar, 50)
            Params(15).Value = City
            Params(16) = New SqlParameter("@PostID", SqlDbType.Int)
            Params(16).Value = POstID
            Params(17) = New SqlParameter("@Email", SqlDbType.VarChar, 50)
            Params(17).Value = Email
            Params(18) = New SqlParameter("@Landline", SqlDbType.VarChar, 50)
            Params(18).Value = Landline
            Params(19) = New SqlParameter("@Mobile", SqlDbType.VarChar, 50)
            Params(19).Value = Mobile
            Params(20) = New SqlParameter("@BasicPay", SqlDbType.Money)
            Params(20).Value = BasicPay
            Params(21) = New SqlParameter("@DA", SqlDbType.Money)
            Params(21).Value = DA
            Params(22) = New SqlParameter("@HRA", SqlDbType.Money)
            Params(22).Value = HRA
            Params(23) = New SqlParameter("@Conveyance", SqlDbType.Money)
            Params(23).Value = Conveyance
            Params(24) = New SqlParameter("@SpecialAllowance", SqlDbType.Money)
            Params(24).Value = SpecialAllowance
            Params(25) = New SqlParameter("@LocalAllowance", SqlDbType.Money)
            Params(25).Value = LocalAllowance
            Params(26) = New SqlParameter("@MedicalAllowance", SqlDbType.Money)
            Params(26).Value = MedicalAllowance
            Params(27) = New SqlParameter("@HospitalityAllowance", SqlDbType.Money)
            Params(27).Value = HospitalityAllowance
            Params(28) = New SqlParameter("@PerformanceAllowance", SqlDbType.Money)
            Params(28).Value = PerformanceAllowance
            Params(29) = New SqlParameter("@OtherAllowance", SqlDbType.Money)
            Params(29).Value = OtherAllowance
            Params(30) = New SqlParameter("@FieldAllowance", SqlDbType.Money)
            Params(30).Value = FieldAllowance
            Params(31) = New SqlParameter("@ESI", SqlDbType.Int)
            Params(31).Value = HasEsi
            Params(32) = New SqlParameter("@PF", SqlDbType.Int)
            Params(32).Value = HasPF
            Params(33) = New SqlParameter("@SWF", SqlDbType.Int)
            Params(33).Value = HasSWF
            Params(34) = New SqlParameter("@ESWT", SqlDbType.Int)
            Params(34).Value = HasESWT
            Params(35) = New SqlParameter("@Charity", SqlDbType.Int)
            Params(35).Value = HasCharity
            Params(36) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(36).Value = UserID
            Params(37) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(37).Direction = ParameterDirection.Output
            Params(38) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(38).Direction = ParameterDirection.Output
            Params(39) = New SqlParameter("@DOB", SqlDbType.Date)
            Params(39).Value = DOB
            Params(40) = New SqlParameter("@MStatusID", SqlDbType.Int)
            Params(40).Value = MstatusID
            Params(41) = New SqlParameter("@CareOfPhone", SqlDbType.VarChar, 50)
            Params(41).Value = CareofPhone
            Params(42) = New SqlParameter("@IDProofType", SqlDbType.Int)
            Params(42).Value = IDProofType
            Params(43) = New SqlParameter("@IDProofNumber", SqlDbType.VarChar, 50)
            Params(43).Value = IDProofNumber
            Params(44) = New SqlParameter("@AddressProofType", SqlDbType.Int)
            Params(44).Value = AddressProofType
            Params(45) = New SqlParameter("@AddressProofNumber", SqlDbType.VarChar, 50)
            Params(45).Value = AddressProofNumber
            Params(46) = New SqlParameter("@QualificationDtl", SqlDbType.VarChar, 5000)
            Params(46).Value = QualificationDtl
            Params(47) = New SqlParameter("@EmpPost", SqlDbType.VarChar, 500)
            Params(47).Value = EmpPost
            Params(48) = New SqlParameter("@Father_Name", SqlDbType.VarChar, 500)
            Params(48).Value = Father
            Params(49) = New SqlParameter("@Mother_Name", SqlDbType.VarChar, 500)
            Params(49).Value = Mother
            Params(50) = New SqlParameter("@ReligionId", SqlDbType.Int)
            Params(50).Value = ReligionId
            Params(51) = New SqlParameter("@Caste_id", SqlDbType.Int)
            Params(51).Value = CasteId
            Params(52) = New SqlParameter("@Twowheeler_Status", SqlDbType.Int)
            Params(52).Value = IsTwowheeler
            Params(53) = New SqlParameter("@Driving_License_No", SqlDbType.VarChar, 500)
            Params(53).Value = DrivingNo
            Params(54) = New SqlParameter("@Is_Sangam_member", SqlDbType.Int)
            Params(54).Value = IsSangamMember
            Params(55) = New SqlParameter("@Marriage_Date", SqlDbType.Date)
            If MarriageDate = "" Then
                Params(55).Value = CDate("01/01/1900")
            Else
                Params(55).Value = CDate(MarriageDate)
            End If

            Params(56) = New SqlParameter("@Mother_tongue", SqlDbType.Int)
            Params(56).Value = MotherTongueId
            Params(57) = New SqlParameter("@Blood_Group", SqlDbType.Int)
            Params(57).Value = BloodGroupId
            Params(58) = New SqlParameter("@Cug_No", SqlDbType.VarChar, 500)
            Params(58).Value = Cug
            Params(59) = New SqlParameter("@Official_mail_id", SqlDbType.VarChar, 500)
            Params(59).Value = OfficialEmail
            Params(60) = New SqlParameter("@Ref_Letter_Panchayath", SqlDbType.Int)
            Params(60).Value = ChkPanch
            Params(61) = New SqlParameter("@Ref_Letter_Community", SqlDbType.Int)
            Params(61).Value = chkComm
            Params(62) = New SqlParameter("@Ref_Letter_PoliceStation", SqlDbType.Int)
            Params(62).Value = chkPolice
            Params(63) = New SqlParameter("@Experience", SqlDbType.VarChar, 5000)
            Params(63).Value = newstrExp
            Params(64) = New SqlParameter("@IDProof", SqlDbType.VarChar, 5000)
            Params(64).Value = newstrID
            Params(65) = New SqlParameter("@AddressProof", SqlDbType.VarChar, 5000)
            Params(65).Value = newstrAddress
            Params(66) = New SqlParameter("@LangKnown", SqlDbType.VarChar, 5000)
            Params(66).Value = LangKnown
            Params(67) = New SqlParameter("@EmpType", SqlDbType.Int)
            Params(67).Value = EmpType
            Params(68) = New SqlParameter("@SuretyType", SqlDbType.Int)
            Params(68).Value = SuretyType
            Params(69) = New SqlParameter("@SuretyName", SqlDbType.VarChar, 500)
            Params(69).Value = SuretyName
            Params(70) = New SqlParameter("@SuretyAddr", SqlDbType.VarChar, 500)
            Params(70).Value = SuretyAddr
            Params(71) = New SqlParameter("@SuretyPhone", SqlDbType.VarChar, 500)
            Params(71).Value = SuretyPhone
            Params(72) = New SqlParameter("@Entity", SqlDbType.Int)
            Params(72).Value = Entity
            Params(73) = New SqlParameter("@Family", SqlDbType.VarChar, 5000)
            Params(73).Value = newstrFamily
            DB.ExecuteNonQuery("SP_Update_Profile", Params)
            ErrorFlag = CInt(Params(37).Value)
            Message = CStr(Params(38).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        Return ErrorFlag.ToString() + "Ø" + Message
    End Function
    Public Function UpdateEmployee(ByVal EmpCode As Integer, ByVal EmpName As String, ByVal BranchID As Integer, ByVal DepartmentID As Integer, ByVal CadreID As Integer, ByVal DesignationID As Integer, ByVal DateOfJoin As Date, ByVal GenderID As Integer, ByVal ReportingTo As Integer, ByVal isFieldstaff As Integer, ByVal UserID As Integer, ByVal CareOf As String, ByVal HouseName As String, ByVal Location As String, ByVal City As String, ByVal POstID As Integer, ByVal Email As String, ByVal Landline As String, ByVal Mobile As String, ByVal BasicPay As Double, ByVal DA As Double, ByVal HRA As Double, ByVal Conveyance As Double, ByVal SpecialAllowance As Double, ByVal LocalAllowance As Double, ByVal MedicalAllowance As Double, ByVal HospitalityAllowance As Double, ByVal PerformanceAllowance As Double, ByVal OtherAllowance As Double, ByVal FieldAllowance As Double, ByVal HasEsi As Integer, ByVal HasPF As Integer, ByVal HasSWF As Integer, ByVal HasESWT As Integer, ByVal HasCharity As Integer, ByVal DOB As Date, ByVal MstatusID As Integer, ByVal CareofPhone As String, ByVal IDProofType As Integer, ByVal IDProofNumber As String, ByVal AddressProofType As Integer, ByVal AddressProofNumber As String, ByVal QualificationDtl As String, ByVal EmpPost As Integer, Optional ByVal Father As String = "", Optional ByVal Mother As String = "", Optional ByVal ReligionId As Integer = 0, Optional ByVal CasteId As Integer = 0, Optional ByVal IsTwowheeler As Integer = 0, Optional ByVal DrivingNo As String = "", Optional ByVal IsSangamMember As Integer = 0, Optional ByVal MarriageDate As String = "15/08/1947", Optional ByVal MotherTongueId As Integer = 0, Optional ByVal BloodGroupId As Integer = 0, Optional ByVal Cug As String = "", Optional ByVal OfficialEmail As String = "", Optional ByVal ChkPanch As Integer = 0, Optional ByVal chkComm As Integer = 0, Optional ByVal chkPolice As Integer = 0, Optional ByVal newstrExp As String = "", Optional ByVal newstrID As String = "", Optional ByVal newstrAddress As String = "", Optional ByVal LangKnown As String = "", Optional ByVal EmpType As Integer = 0, Optional ByVal SuretyType As Integer = 0, Optional ByVal SuretyName As String = "", Optional ByVal SuretyAddr As String = "", Optional ByVal SuretyPhone As String = "", Optional ByVal Entity As Integer = 0, Optional ByVal newstrFamily As String = "") As String
        Dim ErrorFlag As Integer = 0
        Dim Message As String = ""
        Try
            Dim Params(65) As SqlParameter
            Params(0) = New SqlParameter("@TrNo", SqlDbType.Int)
            Params(0).Value = 1
            Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(1).Value = EmpCode
            Params(2) = New SqlParameter("@GenderID", SqlDbType.Int)
            Params(2).Value = GenderID
            Params(3) = New SqlParameter("@CareOF", SqlDbType.VarChar, 50)
            Params(3).Value = CareOf
            Params(4) = New SqlParameter("@HouseName", SqlDbType.VarChar, 50)
            Params(4).Value = HouseName
            Params(5) = New SqlParameter("@Location", SqlDbType.VarChar, 50)
            Params(5).Value = Location
            Params(6) = New SqlParameter("@City", SqlDbType.VarChar, 50)
            Params(6).Value = City
            Params(7) = New SqlParameter("@PostID", SqlDbType.Int)
            Params(7).Value = POstID
            Params(8) = New SqlParameter("@Email", SqlDbType.VarChar, 50)
            Params(8).Value = Email
            Params(9) = New SqlParameter("@Landline", SqlDbType.VarChar, 50)
            Params(9).Value = Landline
            Params(10) = New SqlParameter("@Mobile", SqlDbType.VarChar, 50)
            Params(10).Value = Mobile
            Params(11) = New SqlParameter("@BasicPay", SqlDbType.Money)
            Params(11).Value = BasicPay
            Params(12) = New SqlParameter("@DA", SqlDbType.Money)
            Params(12).Value = DA
            Params(13) = New SqlParameter("@HRA", SqlDbType.Money)
            Params(13).Value = HRA
            Params(14) = New SqlParameter("@Conveyance", SqlDbType.Money)
            Params(14).Value = Conveyance
            Params(15) = New SqlParameter("@SpecialAllowance", SqlDbType.Money)
            Params(15).Value = SpecialAllowance
            Params(16) = New SqlParameter("@LocalAllowance", SqlDbType.Money)
            Params(16).Value = LocalAllowance
            Params(17) = New SqlParameter("@MedicalAllowance", SqlDbType.Money)
            Params(17).Value = MedicalAllowance
            Params(18) = New SqlParameter("@HospitalityAllowance", SqlDbType.Money)
            Params(18).Value = HospitalityAllowance
            Params(19) = New SqlParameter("@PerformanceAllowance", SqlDbType.Money)
            Params(19).Value = PerformanceAllowance
            Params(20) = New SqlParameter("@OtherAllowance", SqlDbType.Money)
            Params(20).Value = OtherAllowance
            Params(21) = New SqlParameter("@FieldAllowance", SqlDbType.Money)
            Params(21).Value = FieldAllowance
            Params(22) = New SqlParameter("@ESI", SqlDbType.Int)
            Params(22).Value = HasEsi
            Params(23) = New SqlParameter("@PF", SqlDbType.Int)
            Params(23).Value = HasPF
            Params(24) = New SqlParameter("@SWF", SqlDbType.Int)
            Params(24).Value = HasSWF
            Params(25) = New SqlParameter("@ESWT", SqlDbType.Int)
            Params(25).Value = HasESWT
            Params(26) = New SqlParameter("@Charity", SqlDbType.Int)
            Params(26).Value = HasCharity
            Params(27) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(27).Value = UserID
            Params(28) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(28).Direction = ParameterDirection.Output
            Params(29) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(29).Direction = ParameterDirection.Output
            Params(30) = New SqlParameter("@DOB", SqlDbType.Date)
            Params(30).Value = DOB
            Params(31) = New SqlParameter("@MStatusID", SqlDbType.Int)
            Params(31).Value = MstatusID
            Params(32) = New SqlParameter("@CareOfPhone", SqlDbType.VarChar, 50)
            Params(32).Value = CareofPhone
            Params(33) = New SqlParameter("@IDProofType", SqlDbType.Int)
            Params(33).Value = IDProofType
            Params(34) = New SqlParameter("@IDProofNumber", SqlDbType.VarChar, 50)
            Params(34).Value = IDProofNumber
            Params(35) = New SqlParameter("@AddressProofType", SqlDbType.Int)
            Params(35).Value = AddressProofType
            Params(36) = New SqlParameter("@AddressProofNumber", SqlDbType.VarChar, 50)
            Params(36).Value = AddressProofNumber
            Params(37) = New SqlParameter("@QualificationDtl", SqlDbType.VarChar, 500)
            Params(37).Value = QualificationDtl


            Params(38) = New SqlParameter("@Father_Name", SqlDbType.VarChar, 500)
            Params(38).Value = Father
            Params(39) = New SqlParameter("@Mother_Name", SqlDbType.VarChar, 500)
            Params(39).Value = Mother
            Params(40) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(40).Value = BranchID
            Params(41) = New SqlParameter("@ReligionId", SqlDbType.Int)
            Params(41).Value = ReligionId
            Params(42) = New SqlParameter("@Caste_id", SqlDbType.Int)
            Params(42).Value = CasteId
            Params(43) = New SqlParameter("@Twowheeler_Status", SqlDbType.Int)
            Params(43).Value = IsTwowheeler
            Params(44) = New SqlParameter("@Driving_License_No", SqlDbType.VarChar, 500)
            Params(44).Value = DrivingNo
            Params(45) = New SqlParameter("@Is_Sangam_member", SqlDbType.Int)
            Params(45).Value = IsSangamMember
            Params(46) = New SqlParameter("@Marriage_Date", SqlDbType.Date)
            If MarriageDate = "" Then
                Params(46).Value = CDate("01/01/1900")
            Else
                Params(46).Value = CDate(MarriageDate)
            End If
            Params(47) = New SqlParameter("@DepartmentID", SqlDbType.Int)
            Params(47).Value = DepartmentID
            Params(48) = New SqlParameter("@Mother_tongue", SqlDbType.Int)
            Params(48).Value = MotherTongueId
            Params(49) = New SqlParameter("@Blood_Group", SqlDbType.Int)
            Params(49).Value = BloodGroupId
            Params(50) = New SqlParameter("@Cug_No", SqlDbType.VarChar, 500)
            Params(50).Value = Cug
            Params(51) = New SqlParameter("@Official_mail_id", SqlDbType.VarChar, 500)
            Params(51).Value = OfficialEmail
            Params(52) = New SqlParameter("@Ref_Letter_Panchayath", SqlDbType.Int)
            Params(52).Value = ChkPanch
            Params(53) = New SqlParameter("@Ref_Letter_Community", SqlDbType.Int)
            Params(53).Value = chkComm
            Params(54) = New SqlParameter("@Ref_Letter_PoliceStation", SqlDbType.Int)
            Params(54).Value = chkPolice
            Params(55) = New SqlParameter("@Experience", SqlDbType.VarChar, 5000)
            Params(55).Value = newstrExp
            Params(56) = New SqlParameter("@IDProof", SqlDbType.VarChar, 5000)
            Params(56).Value = newstrID
            Params(57) = New SqlParameter("@AddressProof", SqlDbType.VarChar, 5000)
            Params(57).Value = newstrAddress
            Params(58) = New SqlParameter("@LangKnown", SqlDbType.VarChar, 5000)
            Params(58).Value = LangKnown
            Params(59) = New SqlParameter("@EmpType", SqlDbType.Int)
            Params(59).Value = EmpType
            Params(60) = New SqlParameter("@SuretyType", SqlDbType.Int)
            Params(60).Value = SuretyType
            Params(61) = New SqlParameter("@SuretyName", SqlDbType.VarChar, 500)
            Params(61).Value = SuretyName
            Params(62) = New SqlParameter("@SuretyAddr", SqlDbType.VarChar, 500)
            Params(62).Value = SuretyAddr
            Params(63) = New SqlParameter("@SuretyPhone", SqlDbType.VarChar, 500)
            Params(63).Value = SuretyPhone
            Params(64) = New SqlParameter("@Entity", SqlDbType.Int)
            Params(64).Value = Entity
            Params(65) = New SqlParameter("@Family", SqlDbType.VarChar, 5000)
            Params(65).Value = newstrFamily
            DB.ExecuteNonQuery("SP_Update_Profile_ByUser", Params)
            ErrorFlag = CInt(Params(28).Value)
            Message = CStr(Params(29).Value)
            If ErrorFlag = 0 Then
                Dim ToAddress As String = "hena.ek@esafmicrofin.com"


                Dim Content As String = vbLf & vbLf & " Dear HR" & vbLf & vbLf



                Content += "  We wish to inform you that employee" + EmpName + "[" + EmpCode.ToString + "] changed his profile"


                Content = Content & Convert.ToString(vbLf & vbLf & " With Regards" & vbLf & vbLf & "HR Team" & vbLf & "Head, HR")
                SM.SendMail(ToAddress, "Employee Profile Updation", Content, Convert.ToString("nisha.rathnan@esafmicrofin.com"))


            End If
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        Return ErrorFlag.ToString() + "Ø" + Message
    End Function
    Public Function EmployeeExit(ByVal EmpCode As Integer, ByVal Reason As String, ByVal TypeID As Integer, ByVal EffectiveDt As Date, ByVal UserID As Integer) As String
        Dim ErrorFlag As Integer = 0
        Dim Message As String = ""
        Try
            Dim Params(7) As SqlParameter
            Params(0) = New SqlParameter("@TrNo", SqlDbType.Int)
            Params(0).Value = 1
            Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(1).Value = EmpCode
            Params(2) = New SqlParameter("@Reason", SqlDbType.VarChar, 150)
            Params(2).Value = Reason
            Params(3) = New SqlParameter("@ExitTypeID", SqlDbType.Int)
            Params(3).Value = TypeID
            Params(4) = New SqlParameter("@ReleivingDate", SqlDbType.Date)
            Params(4).Value = EffectiveDt
            Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(5).Value = UserID
            Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(6).Direction = ParameterDirection.Output
            Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(7).Direction = ParameterDirection.Output
            DB.ExecuteNonQuery("SP_EmployeeExit", Params)
            ErrorFlag = CInt(Params(6).Value)
            Message = CStr(Params(7).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        Return ErrorFlag.ToString() + "Ø" + Message
    End Function
    Public Function EmployeeStatusUpdation(ByVal EmpCode As Integer, ByVal TypeID As Integer, ByVal EffectiveDt As Date, ByVal UserID As Integer) As String
        Dim ErrorFlag As Integer = 0
        Dim Message As String = ""
        Dim DT As New DataTable
        Try
            Dim Params(5) As SqlParameter

            Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(0).Value = EmpCode
            Params(1) = New SqlParameter("@StatusTypeID", SqlDbType.Int)
            Params(1).Value = TypeID
            Params(2) = New SqlParameter("@EffectiveDate", SqlDbType.Date)
            Params(2).Value = EffectiveDt
            Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(3).Value = UserID
            Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(4).Direction = ParameterDirection.Output
            Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(5).Direction = ParameterDirection.Output
            DB.ExecuteNonQuery("SP_Employee_Status_Update", Params)
            ErrorFlag = CInt(Params(4).Value)
            Message = CStr(Params(5).Value)
            If ErrorFlag = 0 Then
                Dim ToAddress As String = "hena.ek@esafmicrofin.com"
                Dim EmpName As String = ""
                DT = DB.ExecuteDataSet("select convert(varchar,a.Emp_code)+'/'+c.Emp_Name+'/'+b.Emp_Status  from emp_status_cycle a,employee_status b,EMP_MASTER c where a.Emp_Status_Id=b.emp_status_id " & _
                    " and a.Emp_code=c.Emp_Code  and a.Emp_Status_Date='" & Format(EffectiveDt, "dd/MMM/yyyy") & "' and a.emp_code=" & EmpCode & "").Tables(0)
                Dim tt As String = SM.GetEmailAddress(EmpCode, 1)
                Dim Val = Split(tt, "^")
                If Val(0).ToString <> "" Then
                    ToAddress = Val(0)
                End If

                If Val(1).ToString <> "" Then
                    EmpName = Val(1).ToString
                End If
                Dim ccAddress As String = ""
                Dim tt1 As String = SM.GetEmailAddress(EmpCode, 2)
                Dim Val1 = Split(tt, "^")
                If Val1(0).ToString <> "" Then
                    ccAddress = Val1(0)
                End If


                'General.GetEmailAddress(EmpID, 3);
                If ccAddress.Trim() <> "" Then
                    ccAddress += ","
                End If
                Dim StrVal As String = ""

                Dim Content As String = vbLf & vbLf & " Dear " + EmpName + ", " & vbLf & vbLf



                Content += "  We wish to inform you that it has been decided to change your status [" + DT.Rows(0).Item(0).ToString + "]  With Effect From " + EffectiveDt.ToString + ". "


                Content = Content & Convert.ToString(vbLf & vbLf & " With Regards" & vbLf & vbLf & "HR Team" & vbLf & "Head, HR")
                SM.SendMail(ToAddress, "Employee Status", Content, ccAddress & Convert.ToString("nisha.rathnan@esafmicrofin.com"))


            End If
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        Return ErrorFlag.ToString() + "Ø" + Message
    End Function
    Public Function GetReportingOfficer(ByVal BranchID As Integer, ByVal DepartmentID As Integer, ByVal DesignationID As Integer, Optional ByVal EMP_ID As Integer = 0) As DataTable
        Dim DT As New DataTable
        Try
            Dim Params(3) As SqlParameter
            Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(0).Value = BranchID
            Params(1) = New SqlParameter("@DepartmentID", SqlDbType.Int)
            Params(1).Value = DepartmentID
            Params(2) = New SqlParameter("@DesignationID", SqlDbType.Int)
            Params(2).Value = DesignationID
            Params(3) = New SqlParameter("@EmpID", SqlDbType.Int)
            Params(3).Value = EMP_ID
            Return DB.ExecuteDataSet("SP_HR_GetReporting_Officer", Params).Tables(0)
        Catch ex As Exception
            Return DT
        End Try
    End Function
    Public Function UpdateEmployeePhoto(ByVal EmpCode As Integer, ByVal Attachment As Byte(), ByVal ContentType As String) As String
        Dim ErrorFlag As Integer = 0
        Dim Message As String = ""
        Try
            Dim Params(4) As SqlParameter
            Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(0).Value = EmpCode
            Params(1) = New SqlParameter("@EmpPhoto", SqlDbType.Image)
            Params(1).Value = Attachment
            Params(2) = New SqlParameter("@ContentType", SqlDbType.VarChar, 150)
            Params(2).Value = ContentType
            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(3).Direction = ParameterDirection.Output
            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(4).Direction = ParameterDirection.Output
            DB.ExecuteNonQuery("SP_HR_UPDATE_EMP_PHOTO", Params)
            ErrorFlag = CInt(Params(3).Value)
            Message = CStr(Params(4).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        Return ErrorFlag.ToString() + "Ø" + Message
    End Function
    Public Function GetEmployee(ByVal EMP_CODE As Integer) As DataTable
        Return DB.ExecuteDataSet("select a.Emp_Code,a.Emp_Name,e.Department_ID,cast(d.Branch_id as varchar(8))+'Ø'+cast(d.status_ID as varchar(2)),g.Designation_id,f.Cadre_id,convert(varchar(11),a.Date_Of_Join,106), " & _
            " isnull(b.HouseName,'') as House,ISNULL(b.Location,'') as location,ISNULL(b.city,'') as city,isnull(b.mobile,''), " & _
            " a.Gender_ID,isnull(b.CareOf,''),k.Post_ID,j.District_ID,i.State_ID,isnull(b.Landline,''),k.pin_Code,isnull(b.email,''), " & _
            " l.emp_name as ReportingTo,b.Marital_Status,convert(varchar(11),b.DOB,106),b.CareOf_Phone,b.IDProof_Type,b.IDProof_Number,b.AddressProof_Type,b.AddressProof_Number,c.Basic_Pay,c.DA,c.HRA,c.Conveyance,c.SpecialAllowance,c.LocalAllowance,c.MedicalAllowance,c.HospitalityAllowance,c.PerformanceAllowance,c.OtherAllowance,c.FieldAllowance,c.ESI,c.PF,c.SWF,c.ESWT,c.Charity " & _
            ",b.Fathers_name,b.Mothers_name,b.Caste_id,b.Blood_Group,b.Mother_tongue,case when b.Marriage_Date='1900-01-01' then '' else b.Marriage_Date end as Marriage_Date ,b.Twowheeler_Status, b.Driving_License_No,b.Cug_No,b.Official_mail_id,b.Ref_Letter_Panchayath " & _
            " ,b.Ref_Letter_Community,b.Ref_Letter_PoliceStation,b.Is_Sangam_member,b.surety_id, b.Surety_person_name, b.Surety_Address, b.Surety_Phone,m.Religion_Id  " & _
            " from EMP_MASTER a , Emp_Profile b LEFT JOIN CASTE_MASTER m ON  b.Caste_id =m.caste_id,EMP_SALARY_DTL c,BRANCH_MASTER d,DEPARTMENT_MASTER e, " & _
            " CADRE_MASTER f,DESIGNATION_MASTER g,State_Master i,District_Master j,Post_Master k,emp_master l where a.Emp_Code=b.Emp_Code and b.Emp_Code=c.Emp_Code and a.Branch_ID=d.Branch_ID and a.Department_ID=e.Department_ID and b.Post_ID=k.Post_ID and k.District_ID=j.District_ID  and j.State_ID =i.State_ID and a.Reporting_to=l.Emp_Code and " & _
            " a.Cadre_ID=f.Cadre_ID and a.Designation_ID=g.Designation_ID  and a.emp_code=" & EMP_CODE & "  ").Tables(0)
    End Function
    Public Function GetEmployeeForUpdation(ByVal EMP_CODE As Integer) As DataTable
        Return DB.ExecuteDataSet("select a.Emp_Code,a.Emp_Name,e.Department_ID,cast(d.Branch_id as varchar(8))+'|'+cast(d.status_ID as varchar(2)),g.Designation_id,f.Cadre_id,convert(varchar(11),a.Date_Of_Join,106), " & _
            " isnull(b.HouseName,'') as House,ISNULL(b.Location,'') as location,ISNULL(b.city,'') as city,isnull(b.mobile,''), " & _
            " a.Gender_ID,isnull(b.CareOf,''),ISNULL(k.Post_ID,-1),ISNULL(j.District_ID,-1),ISNULL(i.State_ID,-1),isnull(b.Landline,''),k.pin_Code,isnull(b.email,''), " & _
            " l.emp_code as ReportingTo,b.Marital_Status,convert(varchar(11),b.DOB,106),b.CareOf_Phone,b.IDProof_Type,b.IDProof_Number,b.AddressProof_Type,b.AddressProof_Number,c.Basic_Pay,c.DA,c.HRA,c.Conveyance,c.SpecialAllowance,c.LocalAllowance,c.MedicalAllowance,c.HospitalityAllowance,c.PerformanceAllowance,c.OtherAllowance,c.FieldAllowance,c.ESI,c.PF,c.SWF,c.ESWT,c.Charity " & _
            ",b.Fathers_name,b.Mothers_name,b.Caste_id,b.Blood_Group,b.Mother_tongue,case when b.Marriage_Date='01/01/1900' then '' else convert(varchar,b.Marriage_Date,106) end as Marriage_Date ,b.Twowheeler_Status, b.Driving_License_No,b.Cug_No,b.Official_mail_id,b.Ref_Letter_Panchayath " & _
            " ,b.Ref_Letter_Community,b.Ref_Letter_PoliceStation,b.Is_Sangam_member,b.surety_id, b.Surety_person_name, b.Surety_Address, b.Surety_Phone,a.Entity,case when m.religion_id=0 then -1 else m.religion_id  end  as Religion_ID,a.Emp_Status_id,a.post_id,a.Emp_Type_ID  " & _
            " from EMP_MASTER a LEFT JOIN  Emp_Profile b ON a.Emp_Code=b.Emp_Code   LEFT JOIN CASTE_MASTER m ON  b.Caste_id =m.caste_id " & _
        " LEFT JOIN EMP_SALARY_DTL c ON b.Emp_Code=c.Emp_Code LEFT JOIN BRANCH_MASTER d ON a.Branch_ID=d.Branch_ID LEFT JOIN DEPARTMENT_MASTER e ON a.Department_ID=e.Department_ID LEFT JOIN   CADRE_MASTER f ON  a.Cadre_ID=f.Cadre_ID LEFT JOIN DESIGNATION_MASTER g ON  a.Designation_ID=g.Designation_ID " & _
        " LEFT JOIN Post_Master k ON b.Post_ID=k.Post_ID LEFT JOIN District_Master j ON k.District_ID=j.District_ID LEFT JOIN State_Master i ON j.State_ID =i.State_ID LEFT JOIN emp_master l ON a.Reporting_to=l.Emp_Code LEFT JOIN GENDER_MASTER h ON a.gender_id=h.gender_id  " & _
         " LEFT JOIN FAMILY_MASTER r ON b.Surety_id=r.Family_id LEFT JOIN RELIGION_MASTER s ON m.Religion_Id=s.Religion_id LEFT JOIN BLOOD_GROUP t ON b.Blood_Group=t.Blood_Id LEFT JOIN LANGUAGE_MASTER u ON b.Mother_tongue=u.Language_id LEFT JOIN MARITAL_STATUS v ON v.MStatus_ID=b.Marital_Status " & _
         " LEFT JOIN employee_status w ON a.emp_status_id =w.emp_status_id where a.emp_code=" & EMP_CODE & "  ").Tables(0)
        'Return DB.ExecuteDataSet("select a.Emp_Code,a.Emp_Name,e.Department_ID,cast(d.Branch_id as varchar(8))+'|'+cast(d.status_ID as varchar(2)),g.Designation_id,f.Cadre_id,convert(varchar(11),a.Date_Of_Join,106), " & _
        '            " isnull(b.HouseName,'') as House,ISNULL(b.Location,'') as location,ISNULL(b.city,'') as city,isnull(b.mobile,''), " & _
        '            " a.Gender_ID,isnull(b.CareOf,''),k.Post_ID,j.District_ID,i.State_ID,isnull(b.Landline,''),k.pin_Code,isnull(b.email,''), " & _
        '            " l.emp_code as ReportingTo,b.Marital_Status,convert(varchar(11),b.DOB,106),b.CareOf_Phone,b.IDProof_Type,b.IDProof_Number,b.AddressProof_Type,b.AddressProof_Number,c.Basic_Pay,c.DA,c.HRA,c.Conveyance,c.SpecialAllowance,c.LocalAllowance,c.MedicalAllowance,c.HospitalityAllowance,c.PerformanceAllowance,c.OtherAllowance,c.FieldAllowance,c.ESI,c.PF,c.SWF,c.ESWT,c.Charity " & _
        '            ",b.Fathers_name,b.Mothers_name,b.Caste_id,b.Blood_Group,b.Mother_tongue,case when b.Marriage_Date='01/01/1900' then '' else convert(varchar,b.Marriage_Date,106) end as Marriage_Date ,b.Twowheeler_Status, b.Driving_License_No,b.Cug_No,b.Official_mail_id,b.Ref_Letter_Panchayath " & _
        '            " ,b.Ref_Letter_Community,b.Ref_Letter_PoliceStation,b.Is_Sangam_member,b.surety_id, b.Surety_person_name, b.Surety_Address, b.Surety_Phone,isnull(m.Religion_Id,-1),a.Emp_Status_id,a.post_id  " & _
        '            " from EMP_MASTER a , Emp_Profile b LEFT JOIN CASTE_MASTER m ON  b.Caste_id =m.caste_id,EMP_SALARY_DTL c,BRANCH_MASTER d,DEPARTMENT_MASTER e, " & _
        '            " CADRE_MASTER f,DESIGNATION_MASTER g,State_Master i,District_Master j,Post_Master k,emp_master l where a.Emp_Code=b.Emp_Code and b.Emp_Code=c.Emp_Code and a.Branch_ID=d.Branch_ID and a.Department_ID=e.Department_ID and b.Post_ID=k.Post_ID and k.District_ID=j.District_ID  and j.State_ID =i.State_ID and a.Reporting_to=l.Emp_Code and " & _
        '            " a.Cadre_ID=f.Cadre_ID and a.Designation_ID=g.Designation_ID  and a.emp_code=" & EMP_CODE & "  ").Tables(0)
    End Function
    Public Function Get_Transfer_Dtls(ByVal EmpCode As Integer) As DataTable
        '###29-10
        Return DB.ExecuteDataSet("select f.Branch_Name as prevBranch,d.Branch_Name as Branch,g.Department_Name as prevDep ,e.Department_Name as Dep , " & _
" h.Emp_Name as PrevReporting,b.Emp_Name as Reporting,i.Post_Name as prevPost ,c.Post_Name as Post,convert(varchar,a.Effective_Date,106) as Edate,j.designation_Name as Prev_Desig,k.designation_Name,l.Cadre_Name as Prev_Cadre,m.Cadre_Name   from EMP_TRANSFER  a,EMP_MASTER b,EMP_POST_MASTER c, " & _
" BRANCH_MASTER d,DEPARTMENT_MASTER e,BRANCH_MASTER f,DEPARTMENT_MASTER g,EMP_MASTER h,EMP_POST_MASTER i,Designation_master j,designation_master k,cadre_master l,cadre_master m where(a.Reporting_To = b.Emp_Code And a.EmpPost = " & _
" c.Post_ID And a.Branch_ID = d.Branch_ID And a.Department_ID = e.Department_ID) and a.Prev_Branch_ID=f.Branch_ID and a.Prev_Department_ID =g.Department_ID " & _
" and a.Prev_Reporting_To=h.emp_code and a.Prev_EmpPost=i.Post_ID and a.Prev_Designation_id=j.designation_id and a.Designation_id=k.designation_id and a.Prev_CadreID=l.Cadre_id and a.Cadreid=m.Cadre_id  and a.Emp_code=" + EmpCode.ToString() + " order by a.Effective_Date").Tables(0)
    End Function
    Public Function Get_Promotion_Dtls(ByVal EmpCode As Integer) As DataTable
        '###29-10
        Return DB.ExecuteDataSet(" select convert(varchar,a.Effective_Date,106) as edate,e.Designation_Name as fromdesg,f.Designation_Name as todesg,b.Emp_Name as fromReporting,h.Emp_Name as ToReporting  " & _
            " ,c.Post_Name as fromPost,i.Post_Name as toPost,a.trans_id from EMP_PROMOTION  a,EMP_MASTER b,EMP_POST_MASTER c,BRANCH_MASTER d,DESIGNATION_MASTER  e,DESIGNATION_MASTER  f,EMP_MASTER h,EMP_POST_MASTER i " & _
            " where(a.Reporting_To_From = b.Emp_Code And a.EmpPost = c.Post_ID And a.Branch_ID = d.Branch_ID And a.Designation_ID_From = e.Designation_ID) " & _
            " and a.Designation_ID_to =f.Designation_ID and a.Reporting_To_To=h.emp_code and a.EmpPost=i.Post_ID and a.Emp_code=" + EmpCode.ToString() + "  order by a.Effective_Date").Tables(0)
    End Function
    Public Function GetQualificationDtl(ByVal EmpID As Integer) As DataTable
        Return DB.ExecuteDataSet("select a.Qualification_ID,a.University_ID,a.YearOFPassing,b.QUALIFICATION_NAME,c.UNIVERSITY_NAME,a.Qualification from EMP_QUALIFICATION a,QUALIFICATION_MASTER b,UNIVERSITY_MASTER c where a.Qualification_ID=b.QUALIFICATION_ID and a.University_ID=c.UNIVERSITY_ID and a.Emp_Code=" + EmpID.ToString() + "").Tables(0)
    End Function
    Public Function GetExperienceDtl(ByVal EmpID As Integer) As DataTable '5 columns
        Return DB.ExecuteDataSet("select Company,Address,Phone,No_of_years,exp_id  from EMP_EXPERIENCE_DETAILS where Emp_Code=" + EmpID.ToString() + "").Tables(0)
    End Function
    Public Function GetFamilyDtl(ByVal EmpID As Integer) As DataTable '6 columns
        Return DB.ExecuteDataSet("select a.Family_id, Person_Name ,Age,Occupation,b.Family_person,Emp_Family_id   from EMP_FAMILY_DETAILS a ,FAMILY_MASTER b where a.Family_id=b.Family_id and a.Emp_code=" + EmpID.ToString() + "").Tables(0)
    End Function
    Public Function GetAddressProofDtl(ByVal EmpID As Integer) As DataTable '4 columns
        Return DB.ExecuteDataSet("select a.Address_Proof_Id,a.Address_proof_Name,b.Type_Name,a.Addr_Id  from EMP_ADDRESS_PROOF_DETAILS a,ID_Proof_Type b where a.Address_Proof_Id=b.Type_ID and a.Emp_Code=" + EmpID.ToString() + "").Tables(0)
    End Function
    Public Function GetIDProofDtl(ByVal EmpID As Integer) As DataTable '4 columns
        Return DB.ExecuteDataSet("select a.ID_Proof_Id,a.Proof_Name,b.Type_Name,a.Proof_Id  from EMP_ID_PROOF_DETAILS a,ID_Proof_Type b where a.ID_Proof_Id =b.Type_ID and a.Emp_Code=" + EmpID.ToString() + "").Tables(0)
    End Function
    Public Function GetLangDtl(ByVal EmpID As Integer) As DataTable '4 columns
        Return DB.ExecuteDataSet("select language_id from EMP_LANGUAGES_KNOWN where Emp_Code=" + EmpID.ToString() + "").Tables(0)
    End Function
    Public Function Get_LANGUAGE() As DataTable
        Return DB.ExecuteDataSet("select -1 as ID,' -----Select-----' as Name union all select language_ID as ID,language as name from Language_master where status_id =1 order by 2").Tables(0)
    End Function
    Public Function Get_RELIGION() As DataTable
        Return DB.ExecuteDataSet("select -1 as ID,' -----Select-----' as Name union all select Religion_ID as ID,Religion_name as name from Religion_master where status_id =1 order by 2").Tables(0)
    End Function
    Public Function Get_CASTE(ByVal Religion_Id As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as ID,' -----Select-----' as Name union all select caste_ID as ID,caste_name as name from Caste_master where religion_id=" & Religion_Id & " and status_id =1 order by 2").Tables(0)
    End Function
    Public Function Get_BloodGroup() As DataTable
        Return DB.ExecuteDataSet("select -1 as ID,' -----Select-----' as Name union all select Blood_ID as ID,Blood_name as name from blood_group where status_id =1 order by 2").Tables(0)
    End Function
    Public Function Getexist_Employee_Profile_For_Approval() As DataTable
        Return DB.ExecuteDataSet("select a.Emp_Code,a.Emp_Name,d.Branch_name,e.Department_name,g.Designation_name,f.Cadre_name, " & _
        " l.emp_name as ReportingTo,b.Cug_No,b.Official_mail_id,a.date_of_join  from EMP_MASTER a , Emp_Profile_temp b ,BRANCH_MASTER d,DEPARTMENT_MASTER e, " & _
        " CADRE_MASTER f,DESIGNATION_MASTER g,emp_master l " & _
        " where(a.Emp_Code = b.Emp_Code And a.Branch_ID = d.Branch_ID And a.Department_ID = e.Department_ID)" & _
        " and a.Reporting_to=l.Emp_Code and  a.Cadre_ID=f.Cadre_ID and a.Designation_ID=g.Designation_ID").Tables(0)
    End Function
    Public Function GetEmployee_Profile_String(ByRef DT As DataTable) As String
        Dim StrAttendance As String = ""
        For n As Integer = 0 To DT.Rows.Count - 1
            StrAttendance += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6) & "µ" & DT.Rows(n)(7) & "µ" & DT.Rows(n)(8) & "µ" & DT.Rows(n)(9)
            If n < DT.Rows.Count - 1 Then
                StrAttendance += "¥"
            End If
        Next
        Return StrAttendance
    End Function
    Public Function Get_CheckMac_Address(ByVal StrMacAddress As String) As Integer
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("select count(MacAddress) from MacAddress where MacAddress ='" & StrMacAddress & "'").Tables(0)
        If DT.Rows.Count > 0 Then
            Return DT.Rows(0).Item(0)
        Else
            Return 0
        End If
    End Function
    Public Function GetProfileStatus(ByVal Emp_Code As String) As Integer
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("select count(emp_code) from Emp_Profile_TEMP where Emp_code ='" & Emp_Code & "'").Tables(0)
        If DT.Rows.Count > 0 Then
            Return DT.Rows(0).Item(0)
        Else
            Return 0
        End If
    End Function
Public Function EnrollResignedEmployee(ByVal EmpCode As Integer, ByVal EmpName As String, ByVal BranchID As Integer, ByVal DepartmentID As Integer, ByVal CadreID As Integer, ByVal DesignationID As Integer, ByVal DateOfJoin As Date, ByVal ReportingTo As Integer, ByVal isFieldstaff As Integer, ByVal UserID As Integer, ByVal EmpPostID As Integer) As String
        Dim ErrorFlag As Integer = 0
        Dim Message As String = ""
        Try
            Dim Params(15) As SqlParameter
            Params(0) = New SqlParameter("@TrNo", SqlDbType.Int)
            Params(0).Value = 1
            Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(1).Value = EmpCode
            Params(2) = New SqlParameter("@EmpName", SqlDbType.VarChar, 50)
            Params(2).Value = EmpName
            Params(3) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(3).Value = BranchID
            Params(4) = New SqlParameter("@DepartmentID", SqlDbType.Int)
            Params(4).Value = DepartmentID
            Params(5) = New SqlParameter("@CadreID", SqlDbType.Int)
            Params(5).Value = CadreID
            Params(6) = New SqlParameter("@DesignationID", SqlDbType.Int)
            Params(6).Value = DesignationID
            Params(7) = New SqlParameter("@GenderID", SqlDbType.Int)
            Params(7).Value = 1
            Params(8) = New SqlParameter("@DateOfJoin", SqlDbType.Date)
            Params(8).Value = DateOfJoin
            Params(9) = New SqlParameter("@EmpPassword", SqlDbType.VarChar, 50)
            Params(9).Value = EncryptDecrypt.Encrypt("Esaf123", EmpCode.ToString())
            Params(10) = New SqlParameter("@ReportingTo", SqlDbType.Int)
            Params(10).Value = ReportingTo
            Params(11) = New SqlParameter("@IsFieldStaff", SqlDbType.Int)
            Params(11).Value = isFieldstaff

            Params(12) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(12).Value = UserID
            Params(13) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(13).Direction = ParameterDirection.Output
            Params(14) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(14).Direction = ParameterDirection.Output

            Params(15) = New SqlParameter("@EmpPost", SqlDbType.VarChar, 500)
            Params(15).Value = EmpPostID

            DB.ExecuteNonQuery("SP_Enrollment_Resigned", Params)
            ErrorFlag = CInt(Params(13).Value)
            Message = CStr(Params(14).Value)

        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        Return ErrorFlag.ToString() + "Ø" + Message
    End Function
    Public Function HREmployeeExit(ByVal EmpCode As Integer, ByVal Reason As String, ByVal TypeID As Integer, ByVal EffectiveDt As Date, ByVal UserID As Integer) As String
        Dim ErrorFlag As Integer = 0
        Dim Message As String = ""
        Try
            Dim Params(7) As SqlParameter
            Params(0) = New SqlParameter("@TrNo", SqlDbType.Int)
            Params(0).Value = 1
            Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(1).Value = EmpCode
            Params(2) = New SqlParameter("@Reason", SqlDbType.VarChar, 150)
            Params(2).Value = Reason
            Params(3) = New SqlParameter("@ExitTypeID", SqlDbType.Int)
            Params(3).Value = TypeID
            Params(4) = New SqlParameter("@ReleivingDate", SqlDbType.Date)
            Params(4).Value = EffectiveDt
            Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(5).Value = UserID
            Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(6).Direction = ParameterDirection.Output
            Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(7).Direction = ParameterDirection.Output
            DB.ExecuteNonQuery("SP_APP_HR_EXIT", Params)
            ErrorFlag = CInt(Params(6).Value)
            Message = CStr(Params(7).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        Return ErrorFlag.ToString() + "Ø" + Message
    End Function
End Class
