﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Public Class Audit
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Function GetAuditAccounts(ByVal ItemID As Integer, ByVal AuditID As Integer, ByVal SangamID As String) As DataTable
        Return GN.GetQueryResult(17, ItemID, AuditID, Date.Now.Date, Date.Now.Date, SangamID)
    End Function
    Function GetAudit(ByVal UserID As Integer) As DataTable
        Return GN.GetQueryResult(10, UserID, 0)
    End Function
    Function GetAuditBranches() As DataTable
        Return DB.ExecuteDataSet("SELECT a.Branch_ID,b.Branch_Name,a.RISK_CATEGORY,a.CLASSIFICATION,a.Audit_Interval From Audit_branches a,branch_master b where a.Branch_id=b.Branch_id and b.status_id=1 ").Tables(0)
    End Function
    Function GetAuditDtl(ByVal AuditID As Integer) As DataTable
        Return GN.GetQueryResult(11, 0, AuditID)
    End Function
    Function GetBranchForPotentialVerification() As DataTable
        Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT Branch_ID from AUDIT_DTL a,AUDIT_OBSERVATION b,AUDIT_OBSERVATION_DTL c where a.AUDIT_ID=b.AUDIT_ID and b.OBSERVATION_ID=c.OBSERVATION_ID and c.STATUS_ID=2 and h.BRANCH_ID=a.BRANCH_ID)").Tables(0)
    End Function
    'NMS ------------ 06-Feb-2016
    Function GetBranchForAuditReport(ByVal EmpCode As Integer, ByVal PostID As Integer) As DataTable
        If PostID = 5 Then
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,branch_master b where a.branch_id=b.branch_id and a.branch_ID=h.Branch_ID and a.STATUS_ID=2 and b.branch_head =" + EmpCode.ToString() + " )").Tables(0)
        ElseIf PostID = 6 Then
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,branch_master b,area_master c where  a.branch_id=b.branch_id and b.area_id=c.area_id and a.branch_ID=h.Branch_ID and a.STATUS_ID=2 and c.area_head=" + EmpCode.ToString() + " )").Tables(0)
        ElseIf PostID = 7 Then
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,branch_master b,area_master c,region_master d  where  a.branch_id=b.branch_id and b.area_id=c.area_id and c.region_id=d.region_id and a.branch_ID=h.Branch_ID and a.STATUS_ID=2 and d.region_head=" + EmpCode.ToString() + " )").Tables(0)
        ElseIf PostID = 3 Then
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Branches b where a.Branch_id=b.Branch_ID and  a.branch_ID=h.Branch_ID and a.STATUS_ID=2 and (b.Team_lead=" + EmpCode.ToString() + "  or b.emp_code=" + EmpCode.ToString() + " ))").Tables(0)
        ElseIf PostID = 4 Then
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Branches b where a.Branch_id=b.Branch_id and a.branch_ID=h.Branch_ID and a.STATUS_ID=2 and b.emp_code=" + EmpCode.ToString() + " )").Tables(0)
        ElseIf PostID = 8 Then
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,brmaster b where a.Branch_id=b.Branch_id and a.branch_ID=h.Branch_ID and a.STATUS_ID=2 and b.Zone_head=" + EmpCode.ToString() + " )").Tables(0)
        ElseIf PostID = 2 Then
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,(select * from audit_admin_branches where emp_code=" + EmpCode.ToString() + " and TYPE_ID=1) g where  a.branch_ID=h.Branch_ID and a.branch_id=g.branch_id and a.STATUS_ID=2)").Tables(0)
        Else
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a where  a.branch_ID=h.Branch_ID and a.STATUS_ID=2)").Tables(0)
        End If
    End Function
    Function GetBranchForFraudVerification(ByVal UserID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_DTL a,AUDIT_OBSERVATION b,AUDIT_OBSERVATION_DTL c,Audit_Branches d where a.AUDIT_ID=b.AUDIT_ID and b.OBSERVATION_ID=c.OBSERVATION_ID and c.STATUS_ID=3 and a.branch_id=d.branch_id and d.Emp_code=" + UserID.ToString() + " and h.BRANCH_ID=a.BRANCH_ID)").Tables(0)
    End Function
    Function GetCheckGroup(ByVal AuditType As Integer) As DataTable
        If AuditType > 0 Then
            Return DB.ExecuteDataSet("select -1 as Group_ID,' ------Select------' as Group_Name Union All SELECT GROUP_ID,GROUP_NAME FROM AUDIT_CHECK_GROUPS WHERE STATUS_ID=1 and AUDIT_TYPE=" + AuditType.ToString()).Tables(0)
        Else
            Return DB.ExecuteDataSet("select -1 as Group_ID,' ------Select------' as Group_Name Union All SELECT GROUP_ID,b.TYPE_NAME +'/'+UPPER( GROUP_NAME)  FROM AUDIT_CHECK_GROUPS a,AUDIT_TYPE b WHERE a.AUDIT_TYPE=b.TYPE_ID and  a.STATUS_ID=1 order by 2").Tables(0)
        End If
    End Function
    Function GetCheckSubGroup(ByVal GroupID As Integer, Optional ByVal Type As Integer = 0) As DataTable
        If Type = 0 Then
            Return DB.ExecuteDataSet("select -1 as sub_Group_ID,' ------Select------' as sub_Group_Name Union All select SUB_GROUP_ID,sub_group_name from AUDIT_CHECK_SUBGROUPS where GROUP_ID=" + GroupID.ToString() + " and STATUS_ID=1").Tables(0)
        Else
            Return DB.ExecuteDataSet("select -1 as sub_Group_ID,' ALL' as sub_Group_Name Union All select SUB_GROUP_ID,upper(sub_group_name) from AUDIT_CHECK_SUBGROUPS where GROUP_ID=" + GroupID.ToString() + " and STATUS_ID=1").Tables(0)
        End If

    End Function
    Function GetAuditForReport(ByVal BranchID As Integer) As DataTable
        If BranchID > 0 Then
            Return DB.ExecuteDataSet("select -1 as AuditID,' ------Select------' as Audit Union All select Group_ID, datename(MONTH,a.period_to) +'-'+CAST(YEAR(a.period_to) AS VARCHAR(4)) from Audit_Master a where  a.Branch_ID=" + BranchID.ToString() + " and a.status_id=2").Tables(0)
        Else
            Return DB.ExecuteDataSet("select '-1' as AuditID,' ------Select------' as Audit Union All select distinct cast(MONTH(a.Period_to) as varchar(2))+'~'+cast(year(a.Period_to) as varchar(4)), datename(MONTH,a.Period_to) +'-'+CAST(YEAR(a.Period_to) AS VARCHAR(4)) from Audit_Master a where  a.status_id<>9").Tables(0)
        End If
    End Function
    Function GetCheckList(ByVal SubGroupID As Integer, Optional ByVal Type As Integer = 0) As DataTable
        If Type = 0 Then
            Return DB.ExecuteDataSet("select -1 as sub_Group_ID,' ------Select------' as sub_Group_Name Union All select ITEM_ID,ITEM_NAME from AUDIT_CHECK_LIST where Sub_group_ID=" + SubGroupID.ToString() + " and STATUS_ID=1").Tables(0)
        Else
            Return DB.ExecuteDataSet("select -1 as sub_Group_ID,' ALL' as sub_Group_Name Union All select ITEM_ID,ITEM_NAME from AUDIT_CHECK_LIST where Sub_group_ID=" + SubGroupID.ToString() + " and STATUS_ID=1").Tables(0)
        End If
    End Function
    Function GetAuditAccounts(ByVal ItemID As Integer, ByVal AuditID As Integer) As DataTable
        Return GN.GetQueryResult(16, ItemID, AuditID, Date.Now.Date, Date.Now.Date, "0")
    End Function
    Function GetPotentialFraudAccounts(ByVal BranchID As Integer) As DataTable
        Return DB.ExecuteDataSet("select '-1' as Loan_no,' ------Select------' as Loan Union All select d.LOAN_NO+'~'+CAST(d.OBSERVATION_ID AS VARCHAR(8))+'~'+cast(d.SIno as varchar(6)), a.TYPE_NAME+'   ' +datename(MONTH,B.START_DT) +'-'+CAST(YEAR(B.START_DT) AS VARCHAR(4))+'  -  '+  d.loan_no +'  -  '+ e.item_Name   from AUDIT_OBSERVATION_DTL d,AUDIT_OBSERVATION c,AUDIT_DTL b,AUDIT_TYPE a,AUDIT_Check_List e where d.OBSERVATION_ID=c.OBSERVATION_ID and c.AUDIT_ID=b.AUDIT_ID and b.AUDIT_TYPE=a.TYPE_ID and c.Item_ID=e.Item_ID and b.BRANCH_ID=" + BranchID.ToString() + " and d.STATUS_ID=2 ").Tables(0)
    End Function
    Function GetFraudAccounts(ByVal BranchID As Integer) As DataTable
        Return DB.ExecuteDataSet("select '-1' as Loan_no,' ------Select------' as Loan Union All select d.LOAN_NO+'~'+CAST(d.OBSERVATION_ID AS VARCHAR(8))+'~'+cast(d.SIno as varchar(6)), a.TYPE_NAME+'   ' +datename(MONTH,B.START_DT) +'-'+CAST(YEAR(B.START_DT) AS VARCHAR(4))+'  -  '+  d.loan_no +'  -  '+ e.item_Name   from AUDIT_OBSERVATION_DTL d,AUDIT_OBSERVATION c,AUDIT_DTL b,AUDIT_TYPE a,AUDIT_Check_List e where d.OBSERVATION_ID=c.OBSERVATION_ID and c.AUDIT_ID=b.AUDIT_ID and b.AUDIT_TYPE=a.TYPE_ID and c.Item_ID=e.Item_ID and b.BRANCH_ID=" + BranchID.ToString() + " and d.STATUS_ID=3").Tables(0)
    End Function
    Function GetLoanAccountDetail(ByVal SINo As Integer) As DataTable
        Return DB.ExecuteDataSet("select a.CLIENT_NAME,a.Client_ID,convert(varchar(11),a.disbursed_Dt,106),a.Loan_Amt,a.scheme_ID,a.Mobile,a.Address,b.remarks,c.Center_Name+'('+cast(c.center_ID as varchar(8))+')' from LOAN_MASTER a,AUDIT_OBSERVATION_DTL b,center_Master c where a.Loan_No=b.LOAN_NO and a.center_ID=c.Center_ID and b.SINo=" + SINo.ToString()).Tables(0)
    End Function
    Function GetLoanDetails(ByVal LoanNo As String, ByVal CenterID As String) As DataTable
        Return GN.GetQueryResult(13, 0, CInt(CenterID), Date.Now.Date, Date.Now.Date, LoanNo)
    End Function
    Function GetLoanDetails(ByVal LoanNo As String) As DataTable
        Return GN.GetQueryResult(13, 0, 0, Date.Now.Date, Date.Now.Date, LoanNo)
    End Function
    Public Function GET_EMP_DEATILS(ByVal EMPID As Integer) As DataTable
        Return DB.ExecuteDataSet("Select CONVERT(VARCHAR,a.emp_code)+'^'+a.emp_name+'^'+CONVERT(VARCHAR,a.Branch_ID)+'^'+a.Branch_name+'^'+CONVERT(VARCHAR,a.Department_ID) +'^'+ a.Department_name+'^'+a.Designation_name+'^'+CONVERT(VARCHAR,a.Designation_ID) from Emp_List a,AUDIT_EMP_MASTER b where a.Emp_Code=b.EMP_CODE  and b.EMp_Code=" + EMPID.ToString() + " and b.status_ID=1").Tables(0)
    End Function
    Public Function GetAuditForApproval(ByVal UserID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as aud_ID,' ------Select------' as aud_name union all SELECT Distinct A.GROUP_ID, B.Branch_Name+ '  |  '  + DATENAME(MONTH,A.Period_to)+' - '+CAST(YEAR(A.Period_to) AS VARCHAR(4))+ '  |  ' + 'BY '+C.EMP_NAME  FROM AUDIT_MASTER A,BRANCH_MASTER B,EMP_MASTER C,AUDIT_BRANCHES d,Audit_Dtl e WHERE A.BRANCH_ID=B.Branch_ID AND A.EMP_CODE=C.Emp_Code and b.Branch_ID=d.BRANCH_ID and a.group_id=e.group_id and d.TEAM_LEAD=" + UserID.ToString() + " AND e.STATUS_ID=3").Tables(0)
    End Function
    Function GetAuditCategory(ByVal AuditID As Integer) As DataTable
        Return DB.ExecuteDataSet("select '-1' as Audit_ID,' ------Select------' as Audit Union All select cast(Audit_ID as varchar(5))+'Ø'+cast(Audit_Type as varchar(5)),Type_Name from Audit_Dtl a,Audit_Type b where a.Audit_Type=b.Type_ID and a.Group_ID=" + AuditID.ToString() + " and a.status_id=3").Tables(0)
    End Function
    Public Function GetSuspeciousAccountDetails() As DataTable
        Return DB.ExecuteDataSet("select '-1' as ID,'---------Select---------' as NAME union all select convert(varchar,b.observation_id)+'|'+c.LOAN_NO+'|'+c.client_id+'|'+c.CLIENT_NAME+'|'+convert(varchar,c.disbursed_dt,106)+'|'+convert(varchar,convert(numeric(16,0),c.disb_amt))+'|'+isnull(b.Operational_Remarks,'')  as ID,c.LOAN_NO+'|'+c.CLIENT_NAME+'|'+convert(varchar,c.disbursed_dt,106)+'|'+convert(varchar,convert(numeric(16,0),c.disb_amt)) as NAME from dbo.AUDIT_OBSERVATION a,dbo.AUDIT_OBSERVATION_DTL  b, " & _
                                 " dbo.LOAN_MASTER c WHERE b.status_id=4 and a.OBSERVATION_ID =b.OBSERVATION_ID and b.LOAN_NO=c.loan_no").Tables(0)
    End Function

    Public Function GetAuditDetails(ByVal BranchID As Integer) As DataTable
        Return DB.ExecuteDataSet("select '-1' as aud_ID,'---------Select---------' as aud_name union all select convert(varchar,a.audit_id)+'^'+convert(varchar,case when a.AUDIT_TYPE=3 then 1 else 2 end)  as aud_ID,convert(VARCHAR,DATENAME(month,END_DT))+ '|' +convert(VARCHAR,YEAR(END_DT)) +'|' + b.TYPE_NAME as aud_name  from AUDIT_DTL a,dbo.AUDIT_TYPE b  " & _
            " where a.AUDIT_TYPE=b.TYPE_ID and a.branch_id=" & BranchID & " and a.status_id=2 ").Tables(0)
    End Function
   

    Public Function GetObservationDetails(ByVal AuditID As Integer, ByVal ObservationID As Integer) As DataTable
        Return DB.ExecuteDataSet("select '-1' as ID,'---------Select---------' as NAME union all select convert(varchar,b.observation_id)+'|'+c.LOAN_NO+'|'+c.client_id+'|'+c.CLIENT_NAME+'|'+convert(varchar,c.disbursed_dt,106)+'|'+convert(varchar,convert(numeric(16,0),c.disb_amt)) as ID,c.LOAN_NO+'|'+c.CLIENT_NAME+'|'+convert(varchar,c.disbursed_dt,106)+'|'+convert(varchar,convert(numeric(16,0),c.disb_amt)) as NAME from dbo.AUDIT_OBSERVATION a,dbo.AUDIT_OBSERVATION_DTL  b, " & _
                                 " dbo.LOAN_MASTER c WHERE b.status_id=1 and a.OBSERVATION_ID =b.OBSERVATION_ID and b.LOAN_NO=c.loan_no   and a.AUDIT_ID=" & AuditID & " and a.OBSERVATION_ID=" & ObservationID & "").Tables(0)
    End Function
    Public Function Get_Audit_Verify_ObsrvtnDetails(ByVal AuditID As Integer, ByVal ObservationID As Integer) As DataTable
        Return DB.ExecuteDataSet("select '-1' as ID,'---------Select---------' as NAME union all select convert(varchar,b.observation_id)+'|'+c.LOAN_NO+'|'+c.client_id+'|'+c.CLIENT_NAME+'|'+convert(varchar,c.disbursed_dt,106)+'|'+convert(varchar,convert(numeric(16,0),c.disb_amt))+'|'+convert(varchar,isnull(b.force_close_status,0)) as ID,c.LOAN_NO+'|'+c.CLIENT_NAME+'|'+convert(varchar,c.disbursed_dt,106)+'|'+convert(varchar,convert(numeric(16,0),c.disb_amt)) as NAME from dbo.AUDIT_OBSERVATION a,dbo.AUDIT_OBSERVATION_DTL  b, " & _
                                 " dbo.LOAN_MASTER c WHERE b.status_id=5 and a.OBSERVATION_ID =b.OBSERVATION_ID and b.LOAN_NO=c.loan_no   and a.AUDIT_ID=" & AuditID & " and a.OBSERVATION_ID=" & ObservationID & "").Tables(0)
    End Function
    Public Function GetAuditVerifyDetails(ByVal UserID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as aud_ID,'---------Select---------' as aud_name union all select a.audit_id as aud_ID,c.Branch_Name+ '  |  '+ b.TYPE_NAME +' '+ convert(VARCHAR,DATENAME(month,END_DT))+ '-' +convert(VARCHAR,YEAR(END_DT))   as aud_name  from AUDIT_DTL a,dbo.AUDIT_TYPE b,branch_master c,AUDIT_OBSERVATION d,AUDIT_OBSERVATION_DTL e  " & _
            " where a.AUDIT_TYPE=b.TYPE_ID and a.status_id=2 and a.Branch_id=c.Branch_Id and a.AUDIT_ID =d.AUDIT_ID and d.OBSERVATION_ID=e.OBSERVATION_ID and e.STATUS_ID=5 and END_DT is not  null and a.BRANCH_ID IN (select BRANCH_ID from AUDIT_BRANCHES where EMP_CODE=" & UserID & ") ").Tables(0)
    End Function

    Public Function Get_Audit_Verify_All(ByVal AuditID As Integer) As DataTable
        Return DB.ExecuteDataSet("select b.observation_id,d.ITEM_NAME,c.LOAN_NO,c.client_id,c.CLIENT_NAME,convert(varchar,c.disbursed_dt,106) as disb_dt, " & _
        " convert(varchar,convert(numeric(16,0),c.disb_amt)) as disb_amt,isnull(b.force_close_status,0) as close_status " & _
        " from dbo.AUDIT_OBSERVATION a,dbo.AUDIT_OBSERVATION_DTL  b,dbo.LOAN_MASTER c,dbo.AUDIT_CHECK_LIST  d WHERE b.status_id=5 and a.OBSERVATION_ID " & _
        " =b.OBSERVATION_ID and b.LOAN_NO=c.loan_no and a.ITEM_ID =d.item_id  and a.AUDIT_ID=" & AuditID & "").Tables(0)
    End Function

    Public Function GetStr_Audit(ByRef DT As DataTable) As String
        Dim StrAudit As String = ""
        For n As Integer = 0 To DT.Rows.Count - 1
            StrAudit += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6) & "µ" & DT.Rows(n)(7)
            If n < DT.Rows.Count - 1 Then
                StrAudit += "¥"
            End If
        Next
        Return StrAudit
    End Function

    Public Function Getstaffstatus(ByVal StaffID As Integer) As DataTable
        Return DB.ExecuteDataSet("select case when a.branch_head=" & StaffID & " then 1 when  b.Area_Head=" & StaffID & " then 2 else 3 end as staffstatus from BRANCH_MASTER a,AREA_MASTER b,REGION_MASTER c WHERE a.Area_ID=b.Area_ID and b.Region_ID=c.Region_ID and (a.branch_head=" & StaffID & " or b.Area_Head=" & StaffID & " or c.Region_head=" & StaffID & ")").Tables(0)
    End Function
    Public Function GetObservation(ByVal AuditID As Integer, ByVal Criticality As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as OBSERVATION_ID,'---------Select---------' as ITEM_NAME union all select a.OBSERVATION_ID,b.ITEM_NAME  from dbo.AUDIT_OBSERVATION a,dbo.AUDIT_CHECK_LIST  b WHERE a.ITEM_ID =b.item_id  and a.AUDIT_ID=" & AuditID & " and b.criticality=" & Criticality & "").Tables(0)
    End Function
    Public Function Getexist_branch(ByVal empcode As Integer) As DataTable
        Return DB.ExecuteDataSet("select a.branch_id,d.Region_Name, c.Area_Name , b.branch_name ,case when ISNULL(m.BRANCH_ID,0)>0 then 1 else 22 end as statusid,isnull(a.EMP_CODE,0) as emp_code ," & empcode & " " & _
                                 " ,e.Emp_Name,case when ISNULL(m.BRANCH_ID,0)>0 then 1 else 22 end as statusid from AUDIT_BRANCHES a LEFT JOIN (select distinct BRANCH_ID  from AUDIT_MASTER  where STATUS_ID=3 or STATUS_ID=1)m ON a.branch_id=m.BRANCH_ID  " & _
                                 " LEFT JOIN emp_master e ON  a.EMP_CODE=e.Emp_Code ,branch_master b,AREA_MASTER c,REGION_MASTER d where a.branch_id=b.branch_id and b.Area_ID=c.Area_ID " & _
                                 " and c.Region_ID=d.Region_ID  and b.status_id=1 order by d.Region_Name, c.Area_Name , b.branch_name").Tables(0)
    End Function
    Public Function GetStr_branch(ByRef DT As DataTable) As String
        Dim StrAttendance As String = ""
        Dim val As Integer
        For n As Integer = 0 To DT.Rows.Count - 1
            If CInt(DT.Rows(n)(5)) = CInt(DT.Rows(n)(6)) Then
                val = 1
            Else
                val = 0
            End If
            StrAttendance += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & val & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6) & "µ" & DT.Rows(n)(7) & "µ" & DT.Rows(n)(8)
            If n < DT.Rows.Count - 1 Then
                StrAttendance += "¥"
            End If
        Next
        Return StrAttendance
    End Function
    Public Function Get_ExistAudit(ByVal TypeID As Integer) As DataTable
        Return DB.ExecuteDataSet("select '-1' as aud_ID,'---------Select---------' as NAME union all select convert(varchar,a.audit_id)+'|'+convert(varchar,a.branch_id) as aud_ID,c.Branch_Name+ ' - ' +b.TYPE_NAME  as NAME  from AUDIT_DTL a,dbo.AUDIT_TYPE b,BRANCH_MASTER c,EMP_MASTER d   where a.AUDIT_TYPE=b.TYPE_ID  and a.BRANCH_ID=c.Branch_ID  and a.EMP_CODE=d.Emp_Code and  a.status_id=0 and a.AUDIT_TYPE=" & TypeID & "").Tables(0)
    End Function
   
    Public Function GetStr_Loan(ByRef DT As DataTable) As String
        Dim StrLoan As String = ""

        For n As Integer = 0 To DT.Rows.Count - 1

            StrLoan += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6) & "µ" & DT.Rows(n)(7) & "µ" & DT.Rows(n)(8)
            If n < DT.Rows.Count - 1 Then
                StrLoan += "¥"
            End If
        Next
        Return StrLoan
    End Function
    Public Function Get_AuditTypes() As DataTable
        Return DB.ExecuteDataSet("select -1 as TYPE_ID,'---------Select---------' as type_name union all select TYPE_ID,type_name  from AUDIT_type where type_id<>2 and STATUS_ID=1").Tables(0)
    End Function
   
    Public Function Get_Audit_Sampling_Criteria() As DataTable
        Return DB.ExecuteDataSet("select -1 as CRITERIA_ID,'---------Select---------' as CRITERIA union all select CRITERIA_ID,CRITERIA  from SAMPLING_CRITERIA where STATUS_ID=1").Tables(0)
    End Function
    Public Function Get_CheckList_AuditGroup(ByVal AuditType As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as GROUP_ID,'---------Select---------' as GROUP_NAME union all select GROUP_ID,GROUP_NAME from dbo.AUDIT_CHECK_GROUPS where AUDIT_TYPE=" & AuditType & " and status_id=1").Tables(0)
    End Function
    Public Function Get_CheckList_AuditSubGroup(ByVal Groupid As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as SUB_GROUP_ID,'---------Select---------' as SUB_GROUP_NAME union all select SUB_GROUP_ID,SUB_GROUP_NAME from dbo.AUDIT_CHECK_SUBGROUPS where group_id=" & Groupid & "  and status_id=1").Tables(0)
    End Function
    Public Function Get_Exist_AuditGroup(ByVal AuditTypeID As Integer) As DataTable
        Return DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY GROUP_NAME) AS Row,GROUP_NAME,case when SPM_FLAG=1 then 'Yes' else 'No' end as SPM  from dbo.AUDIT_CHECK_GROUPS a, " & _
                                 " AUDIT_TYPE b where a.AUDIT_TYPE=b.TYPE_ID and a.STATUS_ID=1 and a.AUDIT_TYPE=" & AuditTypeID & " order by GROUP_NAME").Tables(0)
    End Function
    Public Function Get_Exist_AuditSubGroup(ByVal GroupID As Integer) As DataTable
        Return DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY c.SUB_GROUP_NAME) AS Row,c.SUB_GROUP_NAME  from AUDIT_CHECK_SUBGROUPS c,dbo.AUDIT_CHECK_GROUPS a, " & _
                                 " AUDIT_TYPE b where c.GROUP_ID=a.GROUP_ID and a.AUDIT_TYPE=b.TYPE_ID and c.STATUS_ID=1 and c.GROUP_ID=" & GroupID & " order by c.SUB_GROUP_NAME").Tables(0)
    End Function
    Public Function Get_Exist_AuditCheckList(ByVal SubGroupID As Integer) As DataTable
        Return DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY d.ITEM_NAME  ) AS Row,d.ITEM_NAME from AUDIT_CHECK_LIST d,AUDIT_CHECK_SUBGROUPS c, " & _
                "dbo.AUDIT_CHECK_GROUPS a,AUDIT_TYPE b  where d.SUB_GROUP_ID=c.SUB_GROUP_ID and c.GROUP_ID=a.GROUP_ID and " & _
                "a.AUDIT_TYPE=b.TYPE_ID and d.STATUS_ID=1 and d.SUB_GROUP_ID=" & SubGroupID & " and d.status_id=1 order by d.ITEM_NAME ").Tables(0)
    End Function
    Public Function Get_Exist_AuditGroupString(ByRef DT As DataTable) As String
        Dim StrAudit As String = ""

        For n As Integer = 0 To DT.Rows.Count - 1

            StrAudit += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1)


            If n < DT.Rows.Count - 1 Then
                StrAudit += "¥"
            End If
        Next
        Return StrAudit
    End Function

    Public Function Get_AuditTypes_for_Sampling() As DataTable
        Return DB.ExecuteDataSet("select -1 as TYPE_ID,'---------Select---------' as type_name union all select TYPE_ID,type_name  from AUDIT_type where TYPE_ID<>3 and STATUS_ID=1").Tables(0)
    End Function
    Public Function Get_Existing_Loan(ByVal FilterType As Integer, ByVal branchid As Integer, ByVal AuditID As Integer, Optional ByVal FromDt As String = "01-Jan-1980", Optional ByVal ToDt As String = "01-Jan-1980") As DataTable
        '1-Disbursed Date Between,2-Active Loan-Over Due Exist,3-Active Loan-Repeated Over Due,4-Closed Loan-Over Due Exist
        '5-Closed Loan-Repeated Over Due,6-Over Due Exist,7-Repeated Over Due
        Dim StrFilter As String = ""
        If FilterType = 1 Then
            StrFilter = "Where branch_id=" & branchid & " and Disbursed_Dt between '" & FromDt & "' and '" & ToDt & "'"
        ElseIf FilterType = 2 Then
            StrFilter = "Where branch_id=" & branchid & " and sanctioned_Dt between '" & FromDt & "' and '" & ToDt & "'"
        ElseIf FilterType = 3 Then
            StrFilter = "Where branch_id=" & branchid & " and Disbursed_Dt<='" & ToDt & "' and (status_id='A' or (status_id<>'A' and Close_Dt>'" & FromDt & "'))"
        End If
        Return DB.ExecuteDataSet("select branch_id,Loan_No,Client_ID,dbo.udfpropercase(CLIENT_NAME),scheme_id,convert(varchar,Sanctioned_Dt,106),convert(varchar,Disbursed_Dt,106), convert(numeric(16,0),Disb_Amt),convert(numeric(16,0),Loan_os) " & _
                                 " from loan_master  " & StrFilter & " and Loan_No not in (select loan_no from audit_accounts a,AUDIT_DTL b where a.AUDIT_ID=b.AUDIT_ID and b.audit_id=" & AuditID & " and b.BRANCH_ID =" & branchid & ")  order by scheme_id").Tables(0)
    End Function
    Function GetBranch_Samplingrate() As DataTable
        Return DB.ExecuteDataSet("SELECT a.Branch_ID,b.Branch_Name,isnull(a.branch_rate,0),isnull(a.field_rate,0),a.MARKS_DEDUCTED  From Audit_branches a,branch_master b where a.Branch_id=b.Branch_id and b.status_id=1 ").Tables(0)
    End Function
    Function getCenters(ByVal AuditID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as center_ID,' ------Select------' as center_name union all select center_ID,center_name from center_master c where exists (select distinct b.center_ID from audit_accounts a,LOAN_MASTER b where a.LOAN_NO=b.LOAN_NO and a.AUDIT_ID=" & AuditID.ToString() & " and c.center_id=b.center_id)  order by 2").Tables(0)
    End Function
    Function GetAuditAccountsLoanWise(ByVal LoanNo As String, ByVal AuditID As Integer, ByVal AuditTypeID As Integer) As DataTable
        Return DB.ExecuteDataSet("select f.group_name,e.Sub_Group_Name, a.ITEM_ID,a.ITEM_NAME,e.GROUP_ID,isnull(d.ITEM_ID,0),isnull(d.POT_FRAUD,0),isnull(d.SUSP_LEAKAGE,0),isnull(d.REMARKS,''),isnull(d.FIN_LEAKAGE,0),isnull(d.Fraud,0),isnull(d.Status_ID,1),isnull(d.Staff_Involvement,0),case when VERY_SERIOUS_FLAG=1 then 'V' when SERIOUS_FLAG=1 then 'S' else 'G' end from AUDIT_CHECK_LIST a left outer join  (select b.ITEM_ID,c.LOAN_NO,c.POT_FRAUD,c.REMARKS,c.SUSP_LEAKAGE,c.FIN_LEAKAGE,c.Fraud,c.Status_ID,c.Staff_Involvement  from AUDIT_OBSERVATION b,AUDIT_OBSERVATION_DTL c where b.observation_ID=c.OBSERVATION_ID and b.AUDIT_ID=" + AuditID.ToString() + " and c.LOAN_NO='" + LoanNo.ToString() + "') d on a.ITEM_ID=d.ITEM_ID,AUDIT_CHECK_SUBGROUPS e,audit_Check_Groups f where a.SUB_GROUP_ID=e.SUB_GROUP_ID and e.group_id=f.group_ID and f.status_id=1 and e.status_id=1 and a.STATUS_ID=1 and f.audit_type=" + AuditTypeID.ToString()).Tables(0)
    End Function
    Function GetLoanAccounts(ByVal CentreID As String, ByVal AuditID As Integer) As DataTable
        Return DB.ExecuteDataSet("select '-1' as LoanNo,' ------Select------' as Loan union all select a.LOAN_NO, a.LOAN_NO +' | '+b.Scheme_ID +' |'+b.client_id+'|'+ b.CLIENT_NAME from AUDIT_ACCOUNTS a,LOAN_MASTER b where a.LOAN_NO=b.LOAN_NO and a.AUDIT_ID=" + AuditID.ToString() + " and b.CENTER_ID=" + CentreID.ToString() + " ORDER BY 1").Tables(0)
    End Function
    Function GetAuditForCenterAssignment(ByVal EmpCode As Integer) As DataTable
        If EmpCode > 0 Then
            Return DB.ExecuteDataSet("select '-1' as AuditID,' ------Select------' as Audit union all select cast(a.GROUP_ID as varchar(6))+'~'+cast(a.BRANCH_ID as varchar(6))+'~'+cast(a.Status_ID as varchar(3)), datename(MONTH,a.PERIOD_To) +' '+CAST(YEAR(a.PERIOD_To) AS VARCHAR(4))+' - '+b.Branch_Name from AUDIT_MASTER a,BRANCH_MASTER b where a.BRANCH_ID=b.Branch_ID and a.emp_code=" + EmpCode.ToString() + " and a.status_id in(1,0,9) and a.skip_flag=0 order by 2").Tables(0)
        Else
            Return DB.ExecuteDataSet("select '-1' as AuditID,' ------Select------' as Audit union all select cast(a.GROUP_ID as varchar(6))+'~'+cast(a.BRANCH_ID as varchar(6))+'~'+cast(a.Status_ID as varchar(3)), datename(MONTH,a.PERIOD_To) +' '+CAST(YEAR(a.PERIOD_To) AS VARCHAR(4))+' - '+b.Branch_Name from AUDIT_MASTER a,BRANCH_MASTER b where a.BRANCH_ID=b.Branch_ID  and a.status_id in(0,9)  and a.skip_flag=0 order by 2").Tables(0)
        End If
    End Function
    Function GetCentersForAssign(ByVal BranchID As Integer) As DataTable
        Return GN.GetQueryResult(14, 0, BranchID, Date.Now.Date, Date.Now.Date, "")
    End Function
    Function GetAuditRate(ByVal BranchID As Integer, ByVal AuditID As Integer) As DataTable
        Return GN.GetQueryResult(15, AuditID, BranchID, Date.Now.Date, Date.Now.Date, "")
    End Function
    Function GetAuditDetailsforResponseApproval(ByVal AuditID As Integer) As DataTable
        Return DB.ExecuteDataSet("select a.PERIOD_FROM,a.period_to,b.Emp_Name from AUDIT_MASTER a,EMP_MASTER b where a.EMP_CODE=b.Emp_Code and a.GROUP_ID=" + AuditID.ToString()).Tables(0)
    End Function
    Function GetLoanDetailsforResponseApproval(ByVal LoanNo As String) As DataTable
        Return DB.ExecuteDataSet("select a.CENTER_ID,b.CENTER_NAME,a.LOAN_NO,a.CLIENT_NAME,isnull(a.DISBURSED_DT,a.SANCTIONED_DT),a.LOAN_AMT from LOAN_MASTER a,CENTER_MASTER b where a.CENTER_ID=b.CENTER_ID  and a.LOAN_NO='" + LoanNo.ToString() + "'").Tables(0)
    End Function
    Function GetAuditObservationCycle(ByVal SIno As Integer) As DataTable
        Return DB.ExecuteDataSet("select a.TRA_DT,b.LEVEL_NAME,c.Emp_Name,d.DESCRIPTION,a.REMARKS from AUDIT_OBSERVATION_CYCLE a,AUDIT_USER_LEVELS b,EMP_MASTER c,AUDIT_UPDATE_MASTER d where a.LEVEL_ID=b.LEVEL_ID and a.USER_ID=c.Emp_Code and a.UPDATE_ID=d.UPDATE_ID and a.SINO=" + SIno.ToString() + " order by a.order_ID").Tables(0)
    End Function
    Function GetItemName(ByVal ItemID As Integer, ByVal ObservationID As Integer) As DataTable
        If ItemID > 0 Then
            Return DB.ExecuteDataSet("select item_Name from audit_Check_list where Item_ID =" + ItemID.ToString() + "").Tables(0)
        Else
            Return DB.ExecuteDataSet("select a.item_Name from audit_Check_list a,audit_Observation b where a.Item_ID =b.Item_ID and b.Observation_ID=" + ObservationID.ToString() + "").Tables(0)
        End If

    End Function
    Function GetQueueID(ByVal PostID As Integer) As Integer
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("select queue_ID from Audit_User_LEVELS where POST_ID =" + PostID.ToString() + "").Tables(0)
        If DT.Rows.Count > 0 Then
            Return CInt(DT.Rows(0)(0))
        Else
            Return 0
        End If
    End Function

   
    'NMS ------5-Feb-2016
    Public Function GetBranchesForResponseApproval(ByVal UserID As Integer, ByVal PostID As Integer, ByVal QueueID As Integer) As DataTable
        Dim SqlStr As String = ""
        Dim StatusID As Integer = 1
        If QueueID = 2 Then
            StatusID = 5
        End If
        If PostID = 6 Then
            SqlStr = "select -1 as BranchID,' ---------Select----------' as BranchName Union all select distinct c.BRANCH_ID,d.Branch_Name  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,BRANCH_MASTER d,AUDIT_MASTER e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.BRANCH_ID=d.Branch_ID AND C.GROUP_ID=E.GROUP_ID AND E.STATUS_ID=2 and a.STATUS_ID=" + StatusID.ToString() + " and a.LEVEL_ID=2 and a.SUSP_LEAKAGE=0 and  d.Area_ID in(SELECT area_id from AREA_MASTER where Area_Head=" & UserID.ToString() & ")"
        ElseIf PostID = 7 Then
            SqlStr = "select -1 as BranchID,' ---------Select----------' as BranchName Union all select distinct c.BRANCH_ID,d.Branch_Name  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,BRANCH_MASTER d,AUDIT_MASTER e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.BRANCH_ID=d.Branch_ID AND C.GROUP_ID=E.GROUP_ID AND E.STATUS_ID=2 and a.STATUS_ID=" + StatusID.ToString() + " and a.LEVEL_ID=3  and a.Fraud=0 and  d.Area_ID in(SELECT area_id from AREA_MASTER aa,REGION_MASTER bb where aa.Region_ID=bb.Region_ID and  bb.Region_head=" & UserID.ToString() & ")"
        ElseIf PostID = 9 Then
            SqlStr = "select -1 as BranchID,' ---------Select----------' as BranchName Union all select distinct c.BRANCH_ID,d.Branch_Name  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,BRANCH_MASTER d,AUDIT_MASTER e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.BRANCH_ID=d.Branch_ID AND C.GROUP_ID=E.GROUP_ID AND E.STATUS_ID=2 and a.STATUS_ID=" + StatusID.ToString() + " and a.LEVEL_ID=5"
        ElseIf PostID = 3 Then
            SqlStr = "select -1 as BranchID,' ---------Select----------' as BranchName Union all select distinct c.BRANCH_ID,d.Branch_Name  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,BRANCH_MASTER d,AUDIT_MASTER e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.BRANCH_ID=d.Branch_ID AND C.GROUP_ID=E.GROUP_ID AND E.STATUS_ID=2 and a.STATUS_ID=" + StatusID.ToString() + " and ((a.LEVEL_ID =11 and C.BRANCH_ID IN(SELECT BRANCH_ID FROM AUDIT_BRANCHES WHERE EMP_CODE=" + UserID.ToString() + ") OR (a.LEVEL_ID =12 and C.BRANCH_ID IN(SELECT BRANCH_ID FROM AUDIT_BRANCHES WHERE TEAM_LEAD=" + UserID.ToString() + "))))"
        ElseIf PostID = 4 Then
            SqlStr = "select -1 as BranchID,' ---------Select----------' as BranchName Union all select distinct c.BRANCH_ID,d.Branch_Name  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,BRANCH_MASTER d,AUDIT_MASTER e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.BRANCH_ID=d.Branch_ID AND C.GROUP_ID=E.GROUP_ID AND E.STATUS_ID=2 and a.STATUS_ID=" + StatusID.ToString() + " and a.LEVEL_ID =11 and C.BRANCH_ID IN(SELECT BRANCH_ID FROM AUDIT_BRANCHES WHERE EMP_CODE=" + UserID.ToString() + ")"
        ElseIf PostID = 8 Then
            SqlStr = "select -1 as BranchID,' ---------Select----------' as BranchName Union all select distinct c.BRANCH_ID,d.Branch_Name  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,BRANCH_MASTER d,AUDIT_MASTER e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.BRANCH_ID=d.Branch_ID AND C.GROUP_ID=E.GROUP_ID AND E.STATUS_ID=2 and a.STATUS_ID=" + StatusID.ToString() + " and a.LEVEL_ID =4 and C.BRANCH_ID IN(SELECT BRANCH_ID FROM brmaster WHERE Zone_Head=" + UserID.ToString() + ")"
        ElseIf PostID = 2 Then
            SqlStr = "select -1 as BranchID,' ---------Select----------' as BranchName Union all select distinct c.BRANCH_ID,d.Branch_Name  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,BRANCH_MASTER d,AUDIT_USER_LEVELS e,AUDIT_MASTER f,(select * from audit_admin_branches where emp_code=" & UserID.ToString & " and TYPE_ID=1) g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.BRANCH_ID=d.Branch_ID AND C.GROUP_ID=f.GROUP_ID and c.BRANCH_ID=g.BRANCH_ID  AND f.STATUS_ID=2 and a.STATUS_ID=" + StatusID.ToString() + " and a.LEVEL_ID=e.LEVEL_ID and e.POST_ID=" + PostID.ToString()
        Else
            SqlStr = "select -1 as BranchID,' ---------Select----------' as BranchName Union all select distinct c.BRANCH_ID,d.Branch_Name  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,BRANCH_MASTER d,AUDIT_USER_LEVELS e,AUDIT_MASTER f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.BRANCH_ID=d.Branch_ID AND C.GROUP_ID=f.GROUP_ID AND f.STATUS_ID=2 and a.STATUS_ID=" + StatusID.ToString() + " and a.LEVEL_ID=e.LEVEL_ID and e.POST_ID=" + PostID.ToString()
        End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function
    Public Function GetObservationForResponseApproval(ByVal GroupID As Integer, ByVal PostID As Integer, ByVal QueueID As Integer, ByVal UserID As Integer) As DataTable
        Dim SqlStr As String = ""
        Dim StatusID As Integer = 1
        If QueueID = 2 Then
            StatusID = 5
        End If
        If PostID = 6 Then
            SqlStr = "select CAST((case when a.status_id=1 then (case when (c.cumilative_days-a.operation_days)<0 then 0 else (c.cumilative_days-a.operation_days) end ) else 0 end) as varchar(3))+'~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(g.Group_ID as varchar(6))+'~'+a.LOAN_NO+'~'+cast(e.Item_ID as varchar(6))+'~'+d.CLIENT_NAME+'~'+h.Item_Name+'~'+i.center_Name+'~'+j.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~'+convert(varchar(11),isnull(d.DISBURSED_DT,d.SANCTIONED_DT ),103)+'~'+cast(d.LOAN_AMT as varchar) from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j " & _
                    "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID and  a.order_ID=j.order_ID and  a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.SUSP_LEAKAGE=0 and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and g.Group_ID=" + GroupID.ToString() &
                    " union all " & _
                    "select  CAST((case when a.status_id=1 then (case when (g.cumilative_days-a.operation_days)<0 then 0 else (g.cumilative_days-a.operation_days) end ) else 0 end) as varchar(3))+'~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(i.Group_ID as varchar(6))+'~'+'~'+cast(b.Item_ID as varchar(6))+'~~'+c.ITEM_NAME+'~~'+j.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~~' from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and  a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + " and a.SUSP_LEAKAGE=0  and i.Group_ID=" & GroupID.ToString() & _
                   " order by 1"
        ElseIf PostID = 7 Then
            SqlStr = "select CAST((case when a.status_id=1 then (case when (c.cumilative_days-a.operation_days)<0 then 0 else (c.cumilative_days-a.operation_days) end ) else 0 end) as varchar(3))+'~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(g.Group_ID as varchar(6))+'~'+a.LOAN_NO+'~'+cast(e.Item_ID as varchar(6))+'~'+d.CLIENT_NAME+'~'+h.Item_Name+'~'+i.center_Name+'~'+j.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~'+convert(varchar(11),isnull(d.DISBURSED_DT,d.SANCTIONED_DT ),103)+'~'+cast(d.LOAN_AMT as varchar)  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j " & _
                    "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID and  a.order_ID=j.order_ID and  a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.fraud=0 and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and g.Group_ID=" + GroupID.ToString() &
                    " union all " & _
                    "select  CAST((case when a.status_id=1 then (case when (g.cumilative_days-a.operation_days)<0 then 0 else (g.cumilative_days-a.operation_days) end ) else 0 end) as varchar(3))+'~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(i.Group_ID as varchar(6))+'~'+'~'+cast(b.Item_ID as varchar(6))+'~~'+c.ITEM_NAME+'~~'+j.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~~' from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and  a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + " and a.fraud=0  and i.group_ID=" & GroupID.ToString() & _
                   " order by 1"
        ElseIf PostID = 9 Then
            SqlStr = "select CAST((case when a.status_id=1 then (case when (c.cumilative_days-a.operation_days)<0 then 0 else (c.cumilative_days-a.operation_days) end ) else 0 end) as varchar(3))+'~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(g.Group_ID as varchar(6))+'~'+a.LOAN_NO+'~'+cast(e.Item_ID as varchar(6))+'~'+d.CLIENT_NAME+'~'+h.Item_Name+'~'+i.center_Name+'~'+j.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~'+convert(varchar(11),isnull(d.DISBURSED_DT,d.SANCTIONED_DT ),103)+'~'+cast(d.LOAN_AMT as varchar)  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j  " & _
                   "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and  g.Status_id=2 and e.Item_ID=h.item_ID  and  a.order_ID=j.order_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and g.Group_ID=" + GroupID.ToString() &
                   " union all " & _
                   "select CAST((case when a.status_id=1 then (case when (g.cumilative_days-a.operation_days)<0 then 0 else (g.cumilative_days-a.operation_days) end ) else 0 end) as varchar(3))+'~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(i.Group_ID as varchar(6))+'~'+'~'+cast(b.Item_ID as varchar(6))+'~~'+c.ITEM_NAME+'~~'+j.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~~' from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and  a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + "  and i.group_ID=" & GroupID.ToString() & _
                  " order by 1"
        ElseIf PostID = 3 Then
            SqlStr = "select '0~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(g.Group_ID as varchar(6))+'~'+a.LOAN_NO+'~'+cast(e.Item_ID as varchar(6))+'~'+d.CLIENT_NAME+'~'+h.Item_Name+'~'+ i.center_Name+'~'+s.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~'+convert(varchar(11),isnull(d.DISBURSED_DT,d.SANCTIONED_DT ),103)+'~'+cast(d.LOAN_AMT as varchar)  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,AUDIT_BRANCHES J,center_master i,audit_observation_cycle s  " & _
              "where  a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and g.Status_id=2 and f.AUDIT_TYPE<>3 and e.Item_ID=h.item_ID and a.order_ID=s.order_ID AND F.BRANCH_ID=J.BRANCH_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id  and a.status_id=" + StatusID.ToString() + " and a.LEVEL_ID=( case when j.EMP_CODE=" + UserID.ToString() + " then 11 else -1 end )   and g.Group_ID=" + GroupID.ToString() &
              " union all " & _
              "select '0~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(g.Group_ID as varchar(6))+'~'+a.LOAN_NO+'~'+cast(e.Item_ID as varchar(6))+'~'+d.CLIENT_NAME+'~'+h.Item_Name+'~'+i.center_Name+'~'+s.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~'+convert(varchar(11),isnull(d.DISBURSED_DT,d.SANCTIONED_DT ),103)+'~'+cast(d.LOAN_AMT as varchar)  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,AUDIT_BRANCHES J,center_master i,audit_observation_cycle s " & _
              "where  a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and g.Status_id=2 and f.AUDIT_TYPE<>3 and e.Item_ID=h.item_ID and a.order_ID=s.order_ID AND F.BRANCH_ID=J.BRANCH_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id  and a.status_id=" + StatusID.ToString() + " and a.LEVEL_ID=( case when j.Team_lead=" + UserID.ToString() + " then 12 else -1 end )   and g.Group_ID=" + GroupID.ToString() &
              " union all " & _
              "select  '0~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(i.Group_ID as varchar(6))+'~'+'~'+cast(b.Item_ID as varchar(6))+'~~'+c.ITEM_NAME+'~~'+k.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~~' from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_DTL h,Audit_Master i,AUdit_Branches J,Audit_Observation_Cycle k WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and a.order_ID=k.order_ID and h.group_id=i.group_id and h.branch_Id=j.branch_ID and i.status_id=2 and b.ITEM_ID =c.item_id  and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3  and a.LEVEL_ID=( case when j.EMP_CODE=" + UserID.ToString() + " then 11 else -1 end )  and a.status_id=" + StatusID.ToString() + " and i.Group_ID=" + GroupID.ToString() & _
               " union all " & _
              "select  '0~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(i.Group_ID as varchar(6))+'~'+'~'+cast(b.Item_ID as varchar(6))+'~~'+c.ITEM_NAME+'~~'+k.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~~' from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_DTL h,Audit_Master i,AUdit_Branches J,Audit_Observation_Cycle k WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and a.order_ID=k.order_ID and h.group_id=i.group_id and h.branch_Id=j.branch_ID and i.status_id=2 and b.ITEM_ID =c.item_id  and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3  and a.LEVEL_ID=( case when j.Team_Lead=" + UserID.ToString() + " then 12 else -1 end )  and a.status_id=" + StatusID.ToString() + " and i.Group_ID=" + GroupID.ToString() & _
             " order by 1"
        ElseIf QueueID = 1 Then
            SqlStr = "select CAST((case when a.status_id=1 then (case when (c.cumilative_days-a.operation_days)<0 then 0 else (c.cumilative_days-a.operation_days) end ) else 0 end) as varchar(3))+'~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(g.Group_ID as varchar(6))+'~'+a.LOAN_NO+'~'+cast(e.Item_ID as varchar(6))+'~'+d.CLIENT_NAME+'~'+h.Item_Name++'~'+i.center_Name+'~'+j.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~'+convert(varchar(11),isnull(d.DISBURSED_DT,d.SANCTIONED_DT ),103)+'~'+cast(d.LOAN_AMT as varchar)  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j  " & _
                   "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.order_ID=j.order_ID  and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and g.Group_ID=" + GroupID.ToString() &
                   " union all " & _
                   "select  CAST((case when a.status_id=1 then (case when (g.cumilative_days-a.operation_days)<0 then 0 else (g.cumilative_days-a.operation_days) end ) else 0 end) as varchar(3))+'~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(i.Group_ID as varchar(6))+'~'+'~'+cast(b.Item_ID as varchar(6))+'~~'+c.ITEM_NAME+'~~'+j.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~~' from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + "  and i.Group_ID=" + GroupID.ToString() & _
                  " order by 1"
        Else
            SqlStr = "select '0~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(g.Group_ID as varchar(6))+'~'+a.LOAN_NO+'~'+cast(e.Item_ID as varchar(6))+'~'+d.CLIENT_NAME+'~'+h.Item_Name+'~'+i.center_Name+'~'+j.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~'+convert(varchar(11),isnull(d.DISBURSED_DT,d.SANCTIONED_DT ),103)+'~'+cast(d.LOAN_AMT as varchar)  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j  " & _
                 "where  a.LEVEL_ID=b.LEVEL_ID  and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID and a.order_ID=j.order_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id  and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and g.Group_ID=" + GroupID.ToString() &
                 " union all " & _
                 "select  '0~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+cast(i.Group_ID as varchar(6))+'~'+'~'+cast(b.Item_ID as varchar(6))+'~~'+c.ITEM_NAME+'~~'+j.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~~' from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + " and i.Group_ID=" + GroupID.ToString() & _
                " order by 1"
        End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function
    Public Function GetAuditDetails(ByVal Post_ID As Integer, ByVal Emp_code As Integer) As DataTable
        'Post_id 5-BM,6-AM,10-
        If Post_ID = 5 Then
            Return DB.ExecuteDataSet("select -1 as aud_ID,'---------Select---------' as aud_name union all SELECT   distinct A.GROUP_ID, E.Branch_Name+ '  |  '  + DATENAME(MONTH,A.Period_to)+' - '+CAST(YEAR(A.Period_to) AS VARCHAR(4))+ '  |  '  " & _
                " + 'BY '+C.EMP_NAME  FROM AUDIT_MASTER A,EMP_MASTER C,BRANCH_MASTER E,AUDIT_DTL h,AUDIT_OBSERVATION f,AUDIT_OBSERVATION_DTL g WHERE A.BRANCH_ID=E.Branch_ID  " & _
                " AND A.EMP_CODE=C.Emp_Code AND A.STATUS_ID=2  and a.GROUP_ID=h.GROUP_ID and h.AUDIT_ID=f.AUDIT_ID and f.OBSERVATION_ID=g.OBSERVATION_ID  and  " & _
                " e.branch_head =" & Emp_code & " and g.LEVEL_ID=1 and g.STATUS_ID=1").Tables(0)
        ElseIf Post_ID = 6 Then
            Return DB.ExecuteDataSet("select -1 as aud_ID,'---------Select---------' as aud_name union all SELECT   distinct A.GROUP_ID, E.Branch_Name+ '  |  '  + DATENAME(MONTH,A.Period_to)+' - '+CAST(YEAR(A.Period_to) AS VARCHAR(4))+ '  |  ' " & _
                " + 'BY '+C.EMP_NAME  FROM AUDIT_MASTER A,EMP_MASTER C,BRANCH_MASTER E,AUDIT_DTL h,AUDIT_OBSERVATION f,AUDIT_OBSERVATION_DTL g WHERE A.BRANCH_ID=E.Branch_ID  " & _
                " AND A.EMP_CODE=C.Emp_Code AND A.STATUS_ID=2  and a.GROUP_ID=h.GROUP_ID and h.AUDIT_ID=f.AUDIT_ID and f.OBSERVATION_ID=g.OBSERVATION_ID  and " & _
                " a.branch_id in (select branch_id from BRANCH_MASTER B,AREA_MASTER D where b.area_id=d.area_id and " & _
                " D.Area_head=" & Emp_code & ") and g.LEVEL_ID = 2 And g.STATUS_ID = 1 And g.SUSP_LEAKAGE = 1").Tables(0)
        Else
            If Post_ID = 7 Then
                Return DB.ExecuteDataSet("select -1 as aud_ID,'---------Select---------' as aud_name union all  SELECT   distinct A.GROUP_ID, E.Branch_Name+ '  |  '  + DATENAME(MONTH,A.START_DT)+' - '+CAST(YEAR(A.START_DT) AS VARCHAR(4))+ '  |  ' " & _
                    " + 'BY '+C.EMP_NAME  FROM AUDIT_MASTER A,EMP_MASTER C,brmaster E,AUDIT_DTL h,AUDIT_OBSERVATION f,AUDIT_OBSERVATION_DTL g WHERE A.BRANCH_ID=E.Branch_ID  " & _
                    " AND A.EMP_CODE=C.Emp_Code AND A.STATUS_ID=2  and a.GROUP_ID=h.GROUP_ID and h.AUDIT_ID=f.AUDIT_ID and f.OBSERVATION_ID=g.OBSERVATION_ID  and g.LEVEL_ID=3 and g.STATUS_ID=1 and g.FRAUD=1 and e.region_head=" + Emp_code.ToString() + " ").Tables(0)
            End If
            Return DB.ExecuteDataSet("select -1 as aud_ID,'---------Select---------' as aud_name").Tables(0)
        End If
    End Function

    Function GetAuditCategory_ForRectify(ByVal AuditID As Integer, ByVal Post_ID As Integer) As DataTable
        Dim strVal As String
        If Post_ID = 5 Then
            strVal = " and g.LEVEL_ID=1 and g.STATUS_ID=1"
        ElseIf Post_ID = 6 Then
            strVal = " and g.LEVEL_ID = 2 And g.STATUS_ID = 1 And g.SUSP_LEAKAGE = 1"
        Else
            strVal = " and g.LEVEL_ID=3 and g.STATUS_ID=1 and g.FRAUD=1"
        End If
        Return DB.ExecuteDataSet("select '-1Ø1' as Audit_ID,'---------Select---------' as Audit Union All select distinct cast(a.Audit_ID as varchar(5))+'Ø'+cast(Audit_Type as varchar(5)),Type_Name from Audit_Dtl a,Audit_Type b ,AUDIT_OBSERVATION f,AUDIT_OBSERVATION_DTL g  where a.Audit_Type=b.Type_ID and a.AUDIT_ID=f.AUDIT_ID and f.OBSERVATION_ID=g.OBSERVATION_ID and a.Group_ID=" + AuditID.ToString() + " and a.status_id=2 " & strVal & "").Tables(0)
    End Function
    Function IsAuthorized(ByVal PostID As Integer) As Boolean
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("select count(*) from Audit_User_LEVELS where POST_ID =" + PostID.ToString() + " and level_id>1").Tables(0)
        If CInt(DT.Rows(0)(0)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function GetObservations(ByVal AuditID As Integer) As DataTable
        Return DB.ExecuteDataSet("select  a.ITEM_ID,b.ITEM_NAME,c.POT_FRAUD,c.SUSP_LEAKAGE,c.REMARKS,c.FIN_LEAKAGE,c.status_ID,c.Staff_involvement,c.fraud from dbo.AUDIT_OBSERVATION a,dbo.AUDIT_CHECK_LIST  b,dbo.AUDIT_OBSERVATION_DTL c WHERE a.ITEM_ID =b.item_id and a.OBSERVATION_ID=c.OBSERVATION_ID  and a.AUDIT_ID=" + AuditID.ToString()).Tables(0)
    End Function
    Function GetAuditComments(ByVal SINo As Integer) As DataTable
        Return DB.ExecuteDataSet("select isnull(b.REMARKS,'') as remarks from AUDIT_OBSERVATION_DTL b where  b.SINo=" + SINo.ToString()).Tables(0)
    End Function
    Function GetAuditGeneralComments(ByVal AuditID As Integer) As DataTable
        Return DB.ExecuteDataSet("select isnull(b.GENERAL_COMMENTS,''),isnull(b.SERIOUS_COMMENTS,'') as remarks from AUDIT_DTL b where  b.Audit_ID=" + AuditID.ToString()).Tables(0)
    End Function
    Public Function ObservationPendingQueue(ByVal PostID As Integer) As DataTable
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@PostID", PostID)}
            Return DB.ExecuteDataSet("SP_AUDIT_OBS_PENDING_QUEUE", Parameters).Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function Get_Audit_Summary(ByVal TypeID As Integer, ByVal AuditID As Integer) As DataTable
        If TypeID = 1 Then
            Return DB.ExecuteDataSet("select (select count(distinct a.center_id) from AUDIT_ACCOUNTS a,LOAN_MASTER b where a.LOAN_NO=b.LOAN_NO  and a.AUDIT_ID=" + AuditID.ToString() + " and a.ASSIGN_TYPE=1),(select count(distinct a.center_id) from AUDIT_ACCOUNTS a,LOAN_MASTER b where a.LOAN_NO=b.LOAN_NO  and a.AUDIT_ID=" + AuditID.ToString() + " and a.ASSIGN_TYPE<>1),(select count(distinct a.center_id) from CENTER_MASTER  a,AUDIT_DTL  b where a.BRANCH_ID=b.BRANCH_ID  and b.AUDIT_ID=" + AuditID.ToString() + " and a.STATUS_ID=1),0").Tables(0)
        ElseIf TypeID = 2 Then
            Return DB.ExecuteDataSet("select (select COUNT(a.CENTER_ID) from AUDIT_FIELD_CENTER a,CENTER_MASTER b,AUDIT_DTL c,AUDIT_MASTER d where a.center_id=b.center_id and a.Audit_Id=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and b.FORMATION_DATE  between d.PERIOD_FROM and d.PERIOD_TO  and a.audit_id=" + AuditID.ToString() + "),(select COUNT(a.CENTER_ID) from AUDIT_FIELD_CENTER a,CENTER_MASTER b,AUDIT_DTL c,AUDIT_MASTER d where a.center_id=b.center_id and a.Audit_Id=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and b.FORMATION_DATE not between d.PERIOD_FROM and d.PERIOD_TO  and a.audit_id=" + AuditID.ToString() + "),(select COUNT(b.CENTER_ID) from CENTER_MASTER b,AUDIT_DTL c,AUDIT_MASTER d where b.BRANCH_ID =c.BRANCH_ID and c.GROUP_ID=d.GROUP_ID  and b.FORMATION_DATE between d.PERIOD_FROM and d.PERIOD_TO  and c.audit_id=" + AuditID.ToString() + "),(select COUNT(b.CENTER_ID) from CENTER_MASTER b,AUDIT_DTL c,AUDIT_MASTER d where b.BRANCH_ID =c.BRANCH_ID and c.GROUP_ID=d.GROUP_ID  and b.FORMATION_DATE not between d.PERIOD_FROM and d.PERIOD_TO  and c.audit_id=" + AuditID.ToString() + " and b.STATUS_ID=1)").Tables(0)
        ElseIf TypeID = 4 Then
            Return DB.ExecuteDataSet("select (select COUNT(a.CENTER_ID) from AUDIT_FIELD_CENTER a,CENTER_MASTER b,AUDIT_DTL c,AUDIT_MASTER d where a.center_id=b.center_id and a.Audit_Id=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and b.FORMATION_DATE  between d.PERIOD_FROM and d.PERIOD_TO  and a.audit_id=" + AuditID.ToString() + "),(select COUNT(a.CENTER_ID) from AUDIT_FIELD_CENTER a,CENTER_MASTER b,AUDIT_DTL c,AUDIT_MASTER d where a.center_id=b.center_id and a.Audit_Id=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and b.FORMATION_DATE not between d.PERIOD_FROM and d.PERIOD_TO  and a.audit_id=" + AuditID.ToString() + "),(select COUNT(b.CENTER_ID) from CENTER_MASTER b,AUDIT_DTL c,AUDIT_MASTER d where b.BRANCH_ID =c.BRANCH_ID and c.GROUP_ID=d.GROUP_ID  and b.FORMATION_DATE between d.PERIOD_FROM and d.PERIOD_TO  and c.audit_id=" + AuditID.ToString() + "),(select COUNT(b.CENTER_ID) from CENTER_MASTER b,AUDIT_DTL c,AUDIT_MASTER d where b.BRANCH_ID =c.BRANCH_ID and c.GROUP_ID=d.GROUP_ID  and b.FORMATION_DATE not between d.PERIOD_FROM and d.PERIOD_TO  and c.audit_id=" + AuditID.ToString() + " and b.STATUS_ID=1)").Tables(0)
        Else
            Return Nothing
        End If

    End Function
    Function GetCentersForAssignNew(ByVal BranchID As Integer, ByVal GroupID As Integer) As DataTable
        'Return DB.ExecuteDataSet("select ce.CENTER_ID,ce.CENTER_NAME,DATENAME(dw,ce.meeting_day-2)as meetingday,convert(varchar(11),ce.formation_date,106)," & _
        '                        "ISNULL(am.Branch,0)as branch,isnull(am.field,0) as Field,(case when ce.FORMATION_DATE between pr.PERIOD_FROM  and pr.PERIOD_TO  then 0 else 1 end) as fdatestatus ,convert(varchar(11),ISNULL(CE.BR_ASSIGN_DT,ISNULL(CE.FLD_ASSIGN_DT,'01/01/2014')),106) " & _
        '                        "from CENTER_MASTER  ce " & _
        '                        "left outer join  (select center_id,sum(case when aa.AUDIT_TYPE=1 then 1 else 0 end) as Branch,sum(case when aa.AUDIT_TYPE=4 then 1 else 0 end )as Field " & _
        '                        "from (select distinct b.center_id,e.AUDIT_TYPE  from  AUDIT_MASTER d,AUDIT_DTL e, AUDIT_ACCOUNTS a,LOAN_MASTER b where a.AUDIT_ID=e.AUDIT_ID and " & _
        '                         "e.GROUP_ID=d.GROUP_ID and  a.LOAN_NO=b.LOAN_NO and d.GROUP_ID=" + GroupID.ToString() + ") aa " & _
        '                         "group by center_id)  am on (ce.CENTER_ID=am.CENTER_ID ),(select PERIOD_FROM,PERIOD_TO  from AUDIT_MASTER where GROUP_ID=" + GroupID.ToString() + ") pr where ce.BRANCH_ID=" + BranchID.ToString() + " and ce.center_id in(select distinct center_id from loan_master where branch_id=" & BranchID.ToString() & ") order by 7,ISNULL(CE.BR_ASSIGN_DT,ISNULL(CE.FLD_ASSIGN_DT,'01/01/2014')),2").Tables(0)
        Return DB.ExecuteDataSet("select ce.CENTER_ID,ce.CENTER_NAME,DATENAME(dw,ce.meeting_day-2)as meetingday,convert(varchar(11),ce.formation_date,106)," & _
                               "ISNULL(am.Branch,0)as branch,isnull(am.field,0) as Field,(case when ce.FORMATION_DATE between pr.PERIOD_FROM  and pr.PERIOD_TO  then 0 else 1 end) as fdatestatus ,convert(varchar(11),ISNULL(CE.FLD_ASSIGN_DT,ISNULL(CE.BR_ASSIGN_DT,'01/01/2014')),106) " & _
                               "from CENTER_MASTER  ce " & _
                               "left outer join  (select center_id,sum(case when aa.AUDIT_TYPE=1 then 1 else 0 end) as Branch,sum(case when aa.AUDIT_TYPE=2 then 1 else 0 end )as Field " & _
                               "from (select distinct b.center_id,e.AUDIT_TYPE  from  AUDIT_MASTER d,AUDIT_DTL e, AUDIT_ACCOUNTS a,LOAN_MASTER b where a.AUDIT_ID=e.AUDIT_ID and " & _
                                "e.GROUP_ID=d.GROUP_ID and  a.LOAN_NO=b.LOAN_NO  and d.GROUP_ID=" + GroupID.ToString() + ") aa " & _
                                "group by center_id)  am on (ce.CENTER_ID=am.CENTER_ID ),(select PERIOD_FROM,PERIOD_TO  from AUDIT_MASTER where GROUP_ID=" + GroupID.ToString() + ") pr " & _
                                " ,(select distinct center_id from loan_master where branch_id=" + BranchID.ToString() + ") gh where ce.BRANCH_ID=" + BranchID.ToString() + "  and ce.CENTER_ID=gh.CENTER_ID order by 7,ISNULL(CE.BR_ASSIGN_DT,ISNULL(CE.FLD_ASSIGN_DT,'01/01/2014')),2").Tables(0)
    End Function
    Function GetConsolidatedAuditReportBranchWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal AreaID As Integer, ByVal CloseFlag As Integer) As DataTable
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select d.BRANCH_ID,e.Branch_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " and e.area_id=" + AreaID.ToString() + " and a.status_id in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ")"
        Else
            Sqlstr = "select d.BRANCH_ID,e.Branch_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2  and e.area_id=" + AreaID.ToString() + " and a.status_id in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ")"
        End If
        Sqlstr += " GROUP BY D.BRANCH_ID,e.Branch_Name"
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Function GetConsolidatedAuditReportAreaWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal RegionID As Integer, ByVal CloseFlag As Integer) As DataTable
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select e.area_ID,e.area_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " and e.region_id=" + RegionID.ToString() + " and a.status_id in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ")"
        Else
            Sqlstr = "select e.area_ID,e.area_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2  and e.region_id=" + RegionID.ToString() + " and a.status_id in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ")"
        End If
        Sqlstr += " GROUP BY e.area_id , e.area_name"
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Function GetConsolidatedAuditReport(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal StatusID As Integer, ByVal PostID As Integer, ByVal UserID As Integer) As DataTable
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select e.region_ID,e.Region_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " and a.status_id in(select observation_status from audit_observation_status_Dtl where status_id=" + StatusID.ToString() + ") "
        Else
            Sqlstr = "select e.region_ID,e.Region_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2  and a.status_id in(select observation_status from audit_observation_status_Dtl where status_id=" + StatusID.ToString() + ") "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        Sqlstr += " GROUP BY e.region_ID,e.Region_Name"
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Function GetAuditMonths() As DataTable
        Return DB.ExecuteDataSet("select '-1' as AuditID,' ------Select------' as Audit Union All select distinct CAST(month(a.Period_To) AS VARCHAR(2)) +'~'+CAST(YEAR(a.Period_To) AS VARCHAR(4)), datename(MONTH,a.Period_To) +'-'+CAST(YEAR(a.Period_To) AS VARCHAR(4)) from Audit_Master a where  a.status_id=2").Tables(0)
    End Function
    Function GetAuditMonthsALL() As DataTable
        Return DB.ExecuteDataSet("select '-1' as AuditID,' ALL' as Audit Union All select distinct CAST(month(a.Period_To) AS VARCHAR(2)) +'~'+CAST(YEAR(a.Period_To) AS VARCHAR(4)), datename(MONTH,a.Period_To) +'-'+CAST(YEAR(a.Period_To) AS VARCHAR(4)) from Audit_Master a where  a.status_id=2").Tables(0)
    End Function
    Function GetConsolidatedAuditReportObservationWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal BranchID As Integer, ByVal CloseFlag As Integer) As DataTable
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select b.Observation_ID,f.Item_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount,c.audit_Type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e,AUDIT_CHECK_LIST f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID and b.Item_ID=f.Item_ID  and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " and e.Branch_id=" + BranchID.ToString() + " and a.status_id in(select observation_status from audit_observation_status_dtl where status_id=" + CloseFlag.ToString() + ")"
        Else
            Sqlstr = "select b.Observation_ID,f.Item_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount,c.audit_Type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e,AUDIT_CHECK_LIST f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID and b.Item_ID=f.Item_ID  and d.STATUS_ID=2  and e.Branch_id=" + BranchID.ToString() + " and a.status_id in(select observation_status from audit_observation_status_dtl where status_id=" + CloseFlag.ToString() + ")"
        End If
        Sqlstr += " GROUP BY b.Observation_ID,f.Item_Name,c.audit_Type order by 3 desc,4 desc,2"
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Function GetConsolidatedAuditReportLoanWise(ByVal ObservationID As Integer, ByVal StatusID As Integer, ByVal CloseFlag As Integer, ByVal RType As Integer) As DataTable
        If StatusID = 1 Then
            Return DB.ExecuteDataSet("select a.Loan_no,c.CLIENT_NAME ,d.Center_Name,c.DISB_AMT,e.remarks,a.fin_Leakage,a.SINo  from AUDIT_OBSERVATION_DTL a, " & _
                                     " AUDIT_OBSERVATION b,Loan_Master c,Center_Master d,audit_observation_Cycle e where a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
                                     " a.Loan_no=c.Loan_no  and c.Center_ID=d.Center_ID and a.order_id=e.Order_ID and b.Observation_ID=" + ObservationID.ToString() + " " & _
                                     " and a.Fraud=1 and a.Status_ID in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ") " & _
                                     " order by 1").Tables(0)
        ElseIf StatusID = 2 Then
            Return DB.ExecuteDataSet("select a.Loan_no,c.CLIENT_NAME ,d.Center_Name,c.DISB_AMT,e.remarks,a.fin_Leakage,a.SINo   from AUDIT_OBSERVATION_DTL a " & _
                                     " ,AUDIT_OBSERVATION b,Loan_Master c,Center_Master d,audit_observation_Cycle e where a.OBSERVATION_ID=b.OBSERVATION_ID " & _
                                     " and a.Loan_no=c.Loan_no  and c.Center_ID=d.Center_ID and a.order_id=e.Order_ID  and b.Observation_ID=" + ObservationID.ToString() + "" & _
                                     " and a.Susp_Leakage=1 and a.Status_ID in(select observation_status from audit_observation_status_Dtl where " & _
                                     " status_id=" + CloseFlag.ToString() + ") order by 1").Tables(0)
        ElseIf StatusID = 3 Then
            Return DB.ExecuteDataSet("select a.Loan_no,c.CLIENT_NAME ,d.Center_Name,c.DISB_AMT,e.remarks,a.fin_Leakage,a.SINo   from AUDIT_OBSERVATION_DTL a " & _
                                     " ,AUDIT_OBSERVATION b,Loan_Master c,Center_Master d,audit_observation_Cycle e where a.OBSERVATION_ID=b.OBSERVATION_ID " & _
                                     " and a.Loan_no=c.Loan_no  and c.Center_ID=d.Center_ID and a.order_id=e.Order_ID  and b.Observation_ID=" + ObservationID.ToString() + "" & _
                                     " and a.Fraud=0 and a.Susp_Leakage=0 and a.Status_ID in(select observation_status from audit_observation_status_Dtl where status_id " & _
                                     " =" + CloseFlag.ToString() + ") order by 1").Tables(0)
        ElseIf StatusID = 4 Then
            Return DB.ExecuteDataSet("select a.Loan_no,c.CLIENT_NAME ,d.Center_Name,c.DISB_AMT,e.remarks,a.fin_Leakage ,a.SINo  from AUDIT_OBSERVATION_DTL a " & _
                                     " ,AUDIT_OBSERVATION b,Loan_Master c,Center_Master d,audit_observation_Cycle e where a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
                                     " a.Loan_no=c.Loan_no  and c.Center_ID=d.Center_ID and a.order_id=e.Order_ID  and b.Observation_ID=" + ObservationID.ToString() + "" & _
                                     " and a.Fin_Leakage>0 and a.Status_ID in(select observation_status from audit_observation_status_Dtl where status_id " & _
                                     " =" + CloseFlag.ToString() + ") order by 1").Tables(0)
        ElseIf StatusID = 0 Then
            Return DB.ExecuteDataSet("select a.Loan_no,c.CLIENT_NAME ,d.Center_Name,c.DISB_AMT,e.remarks,a.fin_Leakage,a.SINo   from AUDIT_OBSERVATION_DTL a " & _
                                     " ,AUDIT_OBSERVATION b,Loan_Master c,Center_Master d,audit_observation_Cycle e where a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
                                     " a.Loan_no=c.Loan_no  and c.Center_ID=d.Center_ID and a.order_id=e.Order_ID  and b.Observation_ID=" + ObservationID.ToString() + "" & _
                                     " and a.Status_ID in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ") " & _
                                     " order by 1").Tables(0)
        End If
    End Function

    Function GetFilterOptions(ByVal FilterID As Integer) As DataTable
        If (FilterID = 2) Then
            Return DB.ExecuteDataSet("select '-1' as FilterID,' ------Select------' as FilterText Union All select Region_ID,Region_Name from Region_Master where status_ID=1").Tables(0)
        ElseIf FilterID = 3 Then
            Return DB.ExecuteDataSet("select '-1' as FilterID,' ------Select------' as FilterText Union All select Area_ID,Area_Name from Area_Master where status_ID=1").Tables(0)
        ElseIf FilterID = 4 Then
            Return DB.ExecuteDataSet("select '-1' as FilterID,' ------Select------' as FilterText Union All select Branch_ID,Branch_Name from Branch_Master where status_ID=2").Tables(0)
        End If
    End Function
    Function GetLevelName(ByVal LevelID As Integer) As String
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("select Level_Name from AUDIT_USER_LEVELS  where Level_ID=" + LevelID.ToString() + "").Tables(0)
        If DT.Rows.Count > 0 Then
            Return DT.Rows(0)(0).ToString()
        Else
            Return ""
        End If
    End Function
    Function GetAbstractReport(ByVal Month As Integer, ByVal Year As Integer, ByVal RegionID As Integer, ByVal AreaID As Integer, ByVal BranchID As Integer, ByVal TypeID As Integer, ByVal CloseFlag As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime, Optional ByVal StatusID As Integer = 0, Optional ByVal ItemID As Integer = 0, Optional ByVal UserID As Integer = 0) As DataTable
        Try
            Dim Params(11) As SqlParameter
            Params(0) = New SqlParameter("@Month", SqlDbType.Int)
            Params(0).Value = Month
            Params(1) = New SqlParameter("@Year", SqlDbType.Int)
            Params(1).Value = Year
            Params(2) = New SqlParameter("@RegionID", SqlDbType.Int)
            Params(2).Value = RegionID
            Params(3) = New SqlParameter("@AreaID", SqlDbType.Int)
            Params(3).Value = AreaID
            Params(4) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(4).Value = BranchID
            Params(5) = New SqlParameter("@TypeID", SqlDbType.Int)
            Params(5).Value = TypeID
            Params(6) = New SqlParameter("@StatusID", SqlDbType.Int)
            Params(6).Value = StatusID
            Params(7) = New SqlParameter("@ItemID", SqlDbType.Int)
            Params(7).Value = ItemID
            Params(8) = New SqlParameter("@CloseFlag", SqlDbType.Int)
            Params(8).Value = CloseFlag
            Params(9) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(9).Value = UserID
            Params(10) = New SqlParameter("@FromDt", SqlDbType.DateTime)
            Params(10).Value = FromDt
            Params(11) = New SqlParameter("@ToDt", SqlDbType.DateTime)
            Params(11).Value = ToDt
            Return DB.ExecuteDataSet("SP_GET_AUDIT_ABSTRACT_REPORT_NEW", Params).Tables(0)

        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Function GetConsolidatedAuditReportLoanWise_Gen(ByVal ObservationID As Integer, ByVal StatusID As Integer, ByVal CloseFlag As Integer) As DataTable
        If StatusID = 1 Then
            Return DB.ExecuteDataSet("select e.remarks,a.fin_Leakage,a.SINo  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,audit_observation_Cycle e where a.OBSERVATION_ID=b.OBSERVATION_ID  and a.order_id=e.Order_ID and b.Observation_ID=" + ObservationID.ToString() + " and a.Fraud=1 and a.Status_ID in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ") order by 1").Tables(0)
        ElseIf StatusID = 2 Then
            Return DB.ExecuteDataSet("select e.remarks,a.fin_Leakage,a.SINo   from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,audit_observation_Cycle e where a.OBSERVATION_ID=b.OBSERVATION_ID  and a.order_id=e.Order_ID  and b.Observation_ID=" + ObservationID.ToString() + " and a.Susp_Leakage=1  and a.Status_ID in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ") order by 1").Tables(0)
        ElseIf StatusID = 3 Then
            Return DB.ExecuteDataSet("select e.remarks,a.fin_Leakage,a.SINo   from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,audit_observation_Cycle e where a.OBSERVATION_ID=b.OBSERVATION_ID  and a.order_id=e.Order_ID  and b.Observation_ID=" + ObservationID.ToString() + " and a.Fraud=0 and a.Susp_Leakage=0  and a.Status_ID in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ") order by 1").Tables(0)
        ElseIf StatusID = 4 Then
            Return DB.ExecuteDataSet("select e.remarks,a.fin_Leakage ,a.SINo  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,audit_observation_Cycle e where a.OBSERVATION_ID=b.OBSERVATION_ID  and a.order_id=e.Order_ID  and b.Observation_ID=" + ObservationID.ToString() + " and a.Fin_Leakage>0  and a.Status_ID in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ") order by 1").Tables(0)
        ElseIf StatusID = 0 Then
            Return DB.ExecuteDataSet("select e.remarks,a.fin_Leakage,a.SINo   from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,audit_observation_Cycle e where a.OBSERVATION_ID=b.OBSERVATION_ID  and a.order_id=e.Order_ID  and b.Observation_ID=" + ObservationID.ToString() + "  and a.Status_ID in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ") order by 1").Tables(0)
        End If
    End Function
    Function GetManagementNote(ByVal AuditID As Integer) As String
        Dim dt As New DataTable
        Dim ManagementNote As String
        dt = DB.ExecuteDataSet("select isnull(management_note,'') from audit_master where group_id=(select distinct group_id from audit_dtl where audit_id=" + AuditID.ToString() + ")").Tables(0)
        If dt.Rows.Count > 0 Then
            ManagementNote = dt.Rows(0)(0)
        Else
            ManagementNote = ""
        End If
        Return ManagementNote
    End Function
    Function getBranchesForManagementNoteReport(ByVal Month As Integer, ByVal Year As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as BranchID,' ALL' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,branch_master b where a.branch_id=b.branch_id and a.branch_ID=h.Branch_ID and a.STATUS_ID=2 and month(a.period_to) =" + Month.ToString() + " and year(a.period_to)=" + Year.ToString() + " and isnull(a.management_note,'')<>'' )").Tables(0)
    End Function
    Function GetAuditForManagementNoteReport() As DataTable
        Return DB.ExecuteDataSet("select '-1~-1' as AuditID,' ALL' as Audit Union All select distinct cast(month(a.period_to) as varchar(2)) +'~'+CAST(YEAR(a.period_to) AS VARCHAR(4)) , datename(MONTH,a.period_to) +'-'+CAST(YEAR(a.period_to) AS VARCHAR(4)) from Audit_Master a where a.status_id=2").Tables(0)
    End Function
    Function GetManagementNoteReport(ByVal Month As Integer, ByVal Year As Integer, ByVal BranchID As Integer) As DataTable
        Dim SqlStr As String = "select a.GROUP_ID,b.Branch_Name,a.PERIOD_TO, management_note    from AUDIT_MASTER a,BRANCH_MASTER b where a.BRANCH_ID=b.Branch_ID and ISNULL(a.Management_Note,'')<>'' and a.status_id=2"
        If Month > 0 And Year > 0 Then
            SqlStr += " and MONTH(a.PERIOD_TO)=" + Month.ToString() + " and YEAR(a.PERIOD_TO )=" + Year.ToString()
        End If
        If BranchID > 0 Then
            SqlStr += " and a.branch_id=" + BranchID.ToString()
        End If
        SqlStr += " order by b.branch_name,a.group_ID"
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function
    Function GetAuditForAgingReport(Optional ByVal PostID As Integer = 0, Optional ByVal UserID As Integer = 0) As DataTable
        Dim StrQry As String = ""
        If PostID = 5 Then
            StrQry = " and a.BRANCH_ID in (select branch_id from BrMaster where branch_head=" & UserID & ")  "
        ElseIf PostID = 6 Then
            StrQry = " and a.BRANCH_ID in (select branch_id from BrMaster where area_head=" & UserID & ")  "
        ElseIf PostID = 7 Then
            StrQry = " and a.BRANCH_ID in (select branch_id from BrMaster where Region_head=" & UserID & ")  "
        ElseIf PostID = 8 Then
            StrQry = " and a.BRANCH_ID in (select branch_id from BrMaster where zone_head=" & UserID & ")  "
        Else

        End If
        Return DB.ExecuteDataSet("select '-1' as AuditID,' ALL ' as Audit Union All " & _
                 " select distinct cast(MONTH(a.period_TO) as varchar(2)) +'-'+CAST(YEAR(a.period_TO) AS VARCHAR(4)) , datename(MONTH,a.period_TO) +'-'+CAST(YEAR(a.period_TO) AS VARCHAR(4)) from Audit_Master a where  a.status_id=2 " & StrQry & "").Tables(0)
    End Function



    Public Function AGING_AUDITWISE_REPORT(ByVal Type As Integer, ByVal RType As String, ByVal DayType As Integer, ByVal OptGreat As Integer, ByVal Days As Integer, ByVal SearchID As Integer, ByVal DetailSearch As Integer, ByVal HeadFlag As Integer, ByVal SearchID1 As Integer, ByVal AuditID As String, ByVal UserID As Integer, ByVal PostID As Integer, Optional ByVal SearchID2 As Integer = 0, Optional ByVal AsOnDate As String = "") As DataTable
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Type", Type), New System.Data.SqlClient.SqlParameter("@RTYPE", RType), New System.Data.SqlClient.SqlParameter("@DayType", DayType), New System.Data.SqlClient.SqlParameter("@OptGreat", OptGreat), New System.Data.SqlClient.SqlParameter("@Days", Days), New System.Data.SqlClient.SqlParameter("@SearchID", SearchID), New System.Data.SqlClient.SqlParameter("@HeadFlag", HeadFlag), New System.Data.SqlClient.SqlParameter("@DetailSearch", DetailSearch), New System.Data.SqlClient.SqlParameter("@SearchID1", SearchID1), New System.Data.SqlClient.SqlParameter("@AuditID", AuditID), New System.Data.SqlClient.SqlParameter("@SearchID2", SearchID2), New System.Data.SqlClient.SqlParameter("@UserID", UserID), New System.Data.SqlClient.SqlParameter("@PostID", PostID), New System.Data.SqlClient.SqlParameter("@Ason", CDate(AsOnDate))}
            Return DB.ExecuteDataSet("SP_AUDIT_BUCKET_AUDITWISE_NEW", Parameters).Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function AGINGREPORT(ByVal Type As Integer, ByVal RType As String, ByVal DayType As Integer, ByVal OptGreat As Integer, ByVal Days As Integer, ByVal SearchID As Integer, ByVal DetailSearch As Integer, ByVal HeadFlag As Integer, ByVal SearchID1 As Integer, ByVal AuditID As String, ByVal UserID As Integer, ByVal PostID As Integer, Optional ByVal SearchID2 As Integer = 0) As DataTable
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Type", Type), New System.Data.SqlClient.SqlParameter("@RTYPE", RType), New System.Data.SqlClient.SqlParameter("@DayType", DayType), New System.Data.SqlClient.SqlParameter("@OptGreat", OptGreat), New System.Data.SqlClient.SqlParameter("@Days", Days), New System.Data.SqlClient.SqlParameter("@SearchID", SearchID), New System.Data.SqlClient.SqlParameter("@HeadFlag", HeadFlag), New System.Data.SqlClient.SqlParameter("@DetailSearch", DetailSearch), New System.Data.SqlClient.SqlParameter("@SearchID1", SearchID1), New System.Data.SqlClient.SqlParameter("@AuditID", AuditID), New System.Data.SqlClient.SqlParameter("@SearchID2", SearchID2), New System.Data.SqlClient.SqlParameter("@UserID", UserID), New System.Data.SqlClient.SqlParameter("@PostID", PostID)}
            Return DB.ExecuteDataSet("SP_AUDIT_BUCKET_WISE", Parameters).Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function Audit_Skip() As String
        Dim DT As New DataTable
        Dim StrSkip As String = Nothing
        DT = DB.ExecuteDataSet("select b.Branch_Name + 'µ' + DATENAME (MONTH,a.PERIOD_TO) + '/' + cast(year(a.PERIOD_TO)  as varchar(4)) + 'µ' + cast(a.GROUP_ID as varchar(8))+ 'µ' + cast(a.branch_ID as varchar(8)) " & _
            " from AUDIT_MASTER a, BRANCH_MASTER b where a.BRANCH_ID=b.Branch_ID  and (a.STATUS_ID in(0,9)) and  a.SKIP_FLAG=0 and a.BRANCH_ID in " & _
            " (select BRANCH_ID from AUDIT_MASTER where STATUS_ID in (0,9) and  SKIP_FLAG=0 group by BRANCH_ID having COUNT(*)>1)  order by b.Branch_Name,a.PERIOD_TO ").Tables(0)

        For n As Integer = 0 To DT.Rows.Count - 1
            StrSkip += DT.Rows(n)(0).ToString()
            If n < DT.Rows.Count - 1 Then
                StrSkip += "¥"
            End If
        Next
        Return StrSkip
    End Function
    Function GetManagementNote_Master(ByVal GroupID As Integer) As String
        Dim dt As New DataTable
        Dim ManagementNote As String
        dt = DB.ExecuteDataSet("select isnull(management_note,'') from audit_master where group_id=" + GroupID.ToString()).Tables(0)
        If dt.Rows.Count > 0 Then
            ManagementNote = dt.Rows(0)(0)
        Else
            ManagementNote = ""
        End If
        Return ManagementNote
    End Function
    Function GetObservationDtlsForStaffInvolvement(ByVal AuditID As Integer, ByVal AuditType As Integer) As DataTable
        If AuditType = 1 Or AuditType = 2 Then
            Return DB.ExecuteDataSet("select a.SINO,a.REMARKS,d.ITEM_NAME,a.LOAN_NO,c.CLIENT_NAME,e.CENTER_NAME,(case when a.SUSP_LEAKAGE=1 then 'S' when a.FRAUD=1 then 'VS' else 'G' end)as Type,a.FIN_LEAKAGE   from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,loan_master c,AUDIT_CHECK_LIST d,CENTER_MASTER e where a.OBSERVATION_ID=b.OBSERVATION_ID and a.LOAN_NO=c.LOAN_NO and b.ITEM_ID=d.ITEM_ID and c.CENTER_ID=e.CENTER_ID   and b.AUDIT_ID=" + AuditID.ToString() + " and a.staff_involvement=1 and not exists (select sino from audit_staff_involvement k where a.sino=k.sino and k.status_id<>9) order by d.item_Name,a.sino").Tables(0)
        ElseIf AuditType = 4 Then
            Return DB.ExecuteDataSet("select a.SINO,a.REMARKS,d.ITEM_NAME,'','',e.CENTER_NAME,(case when a.SUSP_LEAKAGE=1 then 'S' when a.FRAUD=1 then 'VS' else 'G' end)as Type,a.FIN_LEAKAGE   from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,loan_master c,AUDIT_CHECK_LIST d,CENTER_MASTER e where a.OBSERVATION_ID=b.OBSERVATION_ID and a.LOAN_NO=c.LOAN_NO and b.ITEM_ID=d.ITEM_ID and c.CENTER_ID=e.CENTER_ID  and b.AUDIT_ID=" + AuditID.ToString() + " and a.staff_involvement=1 and not exists (select sino from audit_staff_involvement k where a.sino=k.sino and k.status_id<>9) order by d.item_Name,a.sino").Tables(0)
        Else
            Return DB.ExecuteDataSet("select a.SINO,a.REMARKS,d.ITEM_NAME,a.LOAN_NO,'','',(case when a.SUSP_LEAKAGE=1 then 'S' when a.FRAUD=1 then 'VS' else 'G' end)as Type,a.FIN_LEAKAGE   from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_CHECK_LIST d where a.OBSERVATION_ID=b.OBSERVATION_ID  and b.ITEM_ID=d.ITEM_ID  and b.AUDIT_ID=" + AuditID.ToString() + " and not exists (select sino from audit_staff_involvement k where a.sino=k.sino and k.status_id<>9)  and a.staff_involvement=1  order by d.item_Name,a.sino").Tables(0)
        End If

    End Function
    Function GetConsolidatedAudit(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer) As DataTable
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select e.region_ID,e.Region_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select e.region_ID,e.Region_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2  "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        Sqlstr += " GROUP BY e.region_ID,e.Region_Name"
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Function GetConsolidatedStaffInvolvement(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal StatusID As Integer) As DataTable
        Dim Sqlstr As String = ""
        Dim Sqlstr1 As String = ""
        Dim SqlFinal As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "(select f.Region_ID ,f.Region_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=0)  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=1)  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=9)  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " "
            Else
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement)  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " "
            End If

        Else
            Sqlstr = "(select f.Region_ID ,f.Region_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=0)  and d.status_id=2 "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=1)  and d.status_id=2 "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=9)  and d.status_id=2 "
            Else
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement)  and d.status_id=2 "
            End If

        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
            Sqlstr1 += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        Sqlstr += "  group by  f.Region_ID ,f.Region_Name"
        Sqlstr1 += "  group by  f.Region_ID ,f.Region_Name"
        SqlFinal = "select aa.Region_ID,aa.REgion_Name,aa.NoOfObservation,aa.NoOfStaff,isnull(bb.AmtInvolved,0) from " + Sqlstr + ")aa left outer join " + Sqlstr1 + ")bb on(aa.Region_ID=bb.region_id)"
        Return DB.ExecuteDataSet(SqlFinal).Tables(0)
    End Function

    Function GetStaffInvolvementArea(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal RegionID As Integer, ByVal StatusID As Integer) As DataTable
        Dim Sqlstr As String = ""
        Dim Sqlstr1 As String = ""
        Dim SqlFinal As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "(select f.Area_id ,f.Area_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=9) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            Else
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            End If

        Else
            Sqlstr = "(select f.Area_id ,f.Area_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2  and f.region_id=" + RegionID.ToString() + " "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + "  "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=9) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + "  "
            Else
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + "  "
            End If

        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        Sqlstr += "  group by  f.Area_id ,f.Area_Name"
        Sqlstr1 += "  group by  f.Area_id ,f.Area_Name"
        SqlFinal = "select aa.Area_id,aa.Area_Name,aa.NoOfObservation,aa.NoOfStaff,bb.AmtInvolved from " + Sqlstr + ")aa," + Sqlstr1 + ")bb where aa.Area_ID=bb.Area_ID"
        Return DB.ExecuteDataSet(SqlFinal).Tables(0)
    End Function
    Function GetStaffInvolvementBranch(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal AreaID As Integer, ByVal StatusID As Integer) As DataTable
        Dim Sqlstr As String = ""
        Dim Sqlstr1 As String = ""
        Dim SqlFinal As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "(select f.Branch_ID ,f.Branch_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=9) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            Else
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            End If

        Else
            Sqlstr = "(select f.Branch_ID ,f.Branch_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2  and f.Area_ID=" + AreaID.ToString() + " "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + "  "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=9) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + "  "
            Else
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + "  "
            End If

        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        Sqlstr += "  group by  f.Branch_ID ,f.Branch_Name"
        Sqlstr1 += "  group by  f.Branch_ID ,f.Branch_Name"
        SqlFinal = "select aa.Branch_ID,aa.Branch_Name,aa.NoOfObservation,aa.NoOfStaff,bb.AmtInvolved from " + Sqlstr + ")aa," + Sqlstr1 + ")bb where aa.Branch_ID=bb.Branch_ID"
        Return DB.ExecuteDataSet(SqlFinal).Tables(0)
    End Function
    Function GetStaffInvolvementObservation(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal StatusID As Integer) As DataTable
        Dim Sqlstr As String = ""
        Dim Sqlstr1 As String = ""
        Dim SqlFinal As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "(select g.Item_ID ,g.Item_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            If StatusID = 0 Then
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            Else
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id<>9) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            End If

        Else
            Sqlstr = "(select g.Item_ID ,g.Item_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2  and f.Branch_ID=" + BranchID.ToString() + " "
            If StatusID = 0 Then
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + "  "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=9) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + "  "
            Else
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + "  "
            End If

        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        Sqlstr += "  group by g.Item_ID ,g.Item_Name,c.audit_type"
        Sqlstr1 += "  group by g.Item_ID ,g.Item_Name,c.audit_type"
        SqlFinal = "select aa.Item_ID,aa.Item_Name,aa.NoOfObservation,aa.NoOfStaff,bb.AmtInvolved,aa.audit_type from " + Sqlstr + ")aa," + Sqlstr1 + ")bb where aa.Item_ID=bb.Item_ID"
        Return DB.ExecuteDataSet(SqlFinal).Tables(0)
    End Function
    Function GetStaffInvolvementLoan(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal EmpCode As Integer, ByVal StatusID As Integer, ByVal TypeID As Integer) As DataTable
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select a.loan_no,h.Display_Status ,a.FIN_LEAKAGE,COUNT(distinct e.emp_code)as StaffInvolved,a.sino,g.item_Name,f.branch_Name,i.Remarks,e.tra_dt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g,AUDIT_OBSERVATION_STATUS_MASTER h,audit_observation_Cycle i where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and a.STATUS_ID=h.Status_ID  and d.status_id=2 and a.order_id=i.order_Id  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select a.loan_no,h.Display_Status ,a.FIN_LEAKAGE,COUNT(distinct e.emp_code)as StaffInvolved,a.sino,g.item_Name,f.branch_Name,i.Remarks,e.tra_dt  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g,AUDIT_OBSERVATION_STATUS_MASTER h,audit_observation_Cycle i where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.STATUS_ID=h.Status_ID and d.status_id=2  and a.order_id=i.order_Id "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If TypeID = 1 Then
            Sqlstr += " and a.fraud=1"
        ElseIf TypeID = 2 Then
            Sqlstr += " and a.susp_leakage=1"
        ElseIf TypeID = 3 Then
            Sqlstr += " and a.fraud=0 and a.susp_leakage=0"
        End If
        If EmpCode > 0 Then
            Sqlstr += " and a.SINo in(select SINO from Audit_Staff_Involvement where Emp_Code=" + EmpCode.ToString() + ")"
        End If
        Sqlstr += "  group by a.loan_no,h.Display_Status ,a.FIN_LEAKAGE,a.sino,g.item_Name,f.branch_Name,i.Remarks,e.tra_dt order by e.tra_dt desc"
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Function GetTotalStaffInvolved(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal EmpCode As Integer, ByVal StatusID As Integer) As Integer
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select COUNT(distinct e.emp_Code) as NoOfStaff from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select COUNT(distinct e.emp_Code) as NoOfStaff from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If EmpCode > 0 Then
            Sqlstr += " and a.SINo in(select SINO from Audit_Staff_Involvement where Emp_Code=" + EmpCode.ToString() + ")"
        End If
      
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(Sqlstr).Tables(0)
        Dim TotalStaffs As Integer = CInt(dt.Rows(0)(0))
        Return TotalStaffs
    End Function
    Function GetStaffInvolvementStaff(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer) As DataTable
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select  e.EMP_CODE,h.emp_name,f.branch_name,h.designation_name,count(e.SINO)as Observations,sum(a.Fin_Leakage) as Leakage,COUNT(distinct d.GROUP_ID )as AI,sum(case when a.FRAUD=1 then 1 else 0 end) as Fraud,SUM(case when a.susp_leakage=1 then 1 else 0 end) as Serious,SUM(case when a.susp_leakage=0 and fraud=0 then 1 else 0 end) as General,MAX(e.tra_dt)as MaxDt  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g,emp_list h where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and e.emp_code=h.emp_code  and d.status_id=2   and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select  e.EMP_CODE,h.emp_name,f.branch_name,h.designation_name,count(e.SINO)as Observations,sum(a.Fin_Leakage) as Leakage,COUNT(distinct d.GROUP_ID )as AI,sum(case when a.FRAUD=1 then 1 else 0 end) as Fraud,SUM(case when a.susp_leakage=1 then 1 else 0 end) as Serious,SUM(case when a.susp_leakage=0 and fraud=0 then 1 else 0 end) as General,MAX(e.tra_dt)as MaxDt   from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g,emp_list h where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and e.emp_code=h.emp_code and d.status_id=2  "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        Sqlstr += "  group by e.EMP_CODE,h.emp_name,f.branch_name,h.designation_name order by 11 desc,1"

        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Function GetStaffInvolvementAudits(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer, ByVal EmpCode As Integer) As DataTable
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select  distinct d.GROUP_ID,d.PERIOD_FROM,D.PERIOD_TO,D.EMP_CODE,D.START_DT,D.END_DT,k.emp_name as Auditor  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g,emp_list h,EMP_LIST K where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and e.emp_code=h.emp_code  and d.status_id=2 and d.emp_code=k.emp_code  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "   and e.emp_code=" + EmpCode.ToString()
        Else
            Sqlstr = "select  distinct d.GROUP_ID,d.PERIOD_FROM,D.PERIOD_TO,D.EMP_CODE,D.START_DT,D.END_DT,k.emp_name as Auditor  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g,emp_list h,EMP_LIST K where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and e.emp_code=h.emp_code and d.status_id=2  and d.emp_code=k.emp_code and e.emp_code=" + EmpCode.ToString()
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        Sqlstr += "  order by 1"

        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function

    Function GetTotalStaffInvolvedObservations(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer) As Integer
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(Sqlstr).Tables(0)
        Dim TotalStaffs As Integer = CInt(dt.Rows(0)(0))
        Return TotalStaffs
    End Function
    Function GetTotalStaffInvolvedObservations_Fraud(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer) As Integer
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.fraud=1  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.fraud=1  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(Sqlstr).Tables(0)
        Dim TotalStaffs As Integer = CInt(dt.Rows(0)(0))
        Return TotalStaffs
    End Function
    Function GetTotalStaffInvolvedObservations_Serious(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer) As Integer
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.susp_leakage=1  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.susp_leakage=1  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(Sqlstr).Tables(0)
        Dim TotalStaffs As Integer = CInt(dt.Rows(0)(0))
        Return TotalStaffs
    End Function
    Function GetTotalStaffInvolvedObservations_General(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer) As Integer
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.susp_leakage=0 and a.fraud=0  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.susp_leakage=0 and a.fraud=0  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(Sqlstr).Tables(0)
        Dim TotalStaffs As Integer = CInt(dt.Rows(0)(0))
        Return TotalStaffs
    End Function
    Function getStaffsInvolved(ByVal SINo As Integer) As DataTable
        Dim Sqlstr As String = ""
        Sqlstr = "select  e.EMP_CODE,h.emp_name,h.branch_name,h.designation_name,e.amount,e.remarks,(case when e.Status_ID=1 then 'Pending' when e.Status_ID=9 then 'Cancel' else 'Closed' end )as Status from AUDIT_OBSERVATION_DTL a,AUDIT_STAFF_INVOLVEMENT e,emp_list h where a.SINO=e.SINO  and e.emp_code=h.emp_code  and e.sINo=" + SINo.ToString()
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Function GetEscalationLevels() As DataTable
        Return DB.ExecuteDataSet("select a.LEVEL_ID,a.LEVEL_NAME,b.DAYS  from AUDIT_USER_LEVELS a,AUDIT_ESCALATION_MATRIX_DTL b where a.QUEUE_ID=1 and a.STATUS_ID=1 and b.MATRIX_ID=0 and a.LEVEL_ID=b.LEVEL_ID order by LEVEL_ID  ").Tables(0)
    End Function
    Function GetObservationDtlsForStaffInvolvementPrevious(ByVal AuditID As Integer, ByVal Fraud As Integer, ByVal Serious As Integer, ByVal General As Integer, ByVal Leakage As Integer) As DataTable
        Dim Sqlstr As String = ""
        Dim SqlStrSub As String = ""
        Dim SqlstrGeneral As String = ""
        Sqlstr = "select a.SINO,a.REMARKS,d.ITEM_NAME,a.LOAN_NO,c.CLIENT_NAME,e.CENTER_NAME,(case when a.SUSP_LEAKAGE=1 then 'S' when a.FRAUD=1 then 'VS' else 'G' end)as Type,a.FIN_LEAKAGE,g.Type_Name,g.Type_ID   from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,loan_master c,AUDIT_CHECK_LIST d,CENTER_MASTER e,audit_dtl f,audit_Type g where a.OBSERVATION_ID=b.OBSERVATION_ID and a.LOAN_NO=c.LOAN_NO and b.ITEM_ID=d.ITEM_ID and b.Audit_ID=f.Audit_Id and f.Audit_Type<>3 and f.audit_type=g.type_id and c.CENTER_ID=e.CENTER_ID  and f.group_id=" + AuditID.ToString() + " and not exists (select sino from audit_staff_involvement k where a.sino=k.sino and k.status_id<>9)"
        SqlstrGeneral = "select a.SINO,a.REMARKS,d.ITEM_NAME,a.LOAN_NO,'','',(case when a.SUSP_LEAKAGE=1 then 'S' when a.FRAUD=1 then 'VS' else 'G' end)as Type,a.FIN_LEAKAGE,g.Type_Name,g.Type_ID    from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_CHECK_LIST d,audit_dtl f,audit_Type g  where a.OBSERVATION_ID=b.OBSERVATION_ID  and b.ITEM_ID=d.ITEM_ID  and b.Audit_ID=f.Audit_Id and f.Audit_Type=3 and f.audit_type=g.type_id  and f.group_id=" + AuditID.ToString() + " and not exists (select sino from audit_staff_involvement k where a.sino=k.sino and k.status_id<>9) "
        If Fraud = 1 Or Serious = 1 Or General = 1 Or Leakage = 1 Then
            Sqlstr += " and ("
            SqlstrGeneral += " and ("
        End If
        If Fraud = 1 Then
            SqlStrSub = "a.Fraud=1"
        End If
        If Serious = 1 Then
            If SqlStrSub <> "" Then
                SqlStrSub += " or "
            End If
            SqlStrSub += "a.susp_leakage=1"
        End If
        If General = 1 Then
            If SqlStrSub <> "" Then
                SqlStrSub += " or "
            End If
            SqlStrSub += "(a.fraud=0 and a.susp_leakage=0)"
        End If
        If Leakage = 1 Then
            If SqlStrSub <> "" Then
                SqlStrSub += " or "
            End If
            SqlStrSub += "  a.fin_leakage>0"
        End If
        If Fraud = 1 Or Serious = 1 Or General = 1 Or Leakage = 1 Then
            Sqlstr += SqlStrSub + " )"
            SqlstrGeneral += SqlStrSub + " )"
        End If
        Sqlstr += " Union all " + SqlstrGeneral
        Sqlstr += " order by g.Type_ID ,d.item_Name,a.sino"
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Public Function GetUserDetails(ByVal UserID As Integer) As DataSet
        Return DB.ExecuteDataSet("Select emp_code,emp_name,Branch_name,Department_name,Designation_name,Gender_ID,CONVERT(VARCHAR(11),Date_Of_Join,106),Branch_ID from Emp_List where EMp_Code=" + UserID.ToString() + "")

    End Function
    Function GetTotalLeakageStaffInvolvedObservations(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer) As Integer
        Dim Sqlstr As String = ""
        Dim SqlStr1 As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select distinct e.sino from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select distinct e.sino from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        SqlStr1 = "Select sum(fin_Leakage) from audit_observation_Dtl where sino in(" + Sqlstr + ")"
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(SqlStr1).Tables(0)
        Dim TotalLeakage As Integer = CInt(dt.Rows(0)(0))
        Return TotalLeakage
    End Function
    Function ConsolidatedAuditReportReportWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal StatusID As Integer, ByVal PostID As Integer, ByVal UserID As Integer) As DataTable
        Dim SqlStr As String = ""
        If (AuditMonth > 0) Then
            SqlStr = "select e.Region_ID  ,e.Region_Name   ,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports,SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both,SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e where  d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            SqlStr = "select e.Region_ID  ,e.Region_Name   ,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports,SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both,SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e where  d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2"
        End If
        If PostID = 8 Then
            SqlStr += " and e.zone_head=" + UserID.ToString()
        End If
        If StatusID = 1 Or StatusID = 2 Or StatusID = 5 Then
            SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        ElseIf StatusID = 0 Then
            SqlStr += " and not exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID>" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        End If
        SqlStr += " group by e.Region_ID ,e.Region_Name"
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function

    Function AreaWiseAuditReportReportWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal StatusID As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal RegionID As Integer) As DataTable
        Dim SqlStr As String = ""

        If (AuditMonth > 0) Then
            SqlStr = "select e.area_ID  ,e.Area_Name   ,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports," & _
                " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) " & _
                " as Both,SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e where  d.BRANCH_ID " & _
                " =e.Branch_ID and d.STATUS_ID=2  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            SqlStr = "select e.area_ID  ,e.Area_Name   ,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports," & _
                " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both," & _
                " SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e where  d.BRANCH_ID=e.Branch_ID and " & _
                " d.STATUS_ID=2"
        End If
        If RegionID > 0 Then
            SqlStr += " and e.region_id=" + RegionID.ToString()
        End If
        If StatusID = 1 Or StatusID = 2 Or StatusID = 5 Then
            SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c " & _
                " ,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID " & _
                " in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm " & _
                " where d.group_id=mm.group_id and mm.cnt>0)"
        ElseIf StatusID = 0 Then
            SqlStr += " and not exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b " & _
                " ,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 " & _
                " and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID>" + StatusID.ToString() + ")  group by d.GROUP_ID) mm " & _
                " where d.group_id=mm.group_id and mm.cnt>0)"
        End If
        SqlStr += " group by e.area_ID ,e.Area_Name"
        
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function
    Function BranchWiseAuditReportReportWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal StatusID As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal RegionID As Integer, ByVal AreaID As Integer, ByVal BranchID As Integer, ByVal TypeID As Integer) As DataTable
        Dim SqlStr As String = ""
        If (AuditMonth > 0) Then
            SqlStr = "select e.Branch_ID ,e.Branch_Name,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports,SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both,SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e where  d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            SqlStr = "select e.Branch_ID ,e.Branch_Name,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports,SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both,SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e where  d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2"
        End If
        If BranchID > 0 Then
            SqlStr += " and e.branch_ID=" + BranchID.ToString() + "  "
        ElseIf AreaID > 0 Then
            SqlStr += " and e.area_ID=" + AreaID.ToString() + "  "
        ElseIf RegionID > 0 Then
            SqlStr += " and e.region_ID=" + RegionID.ToString() + "  "
        End If
        If StatusID = 1 Or StatusID = 2 Or StatusID = 5 Then
            SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        ElseIf StatusID = 0 Then
            SqlStr += " and not exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID>" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        End If

        SqlStr += " group by e.Branch_ID ,e.Branch_Name "
        If TypeID = 1 Then
            SqlStr += " having SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END)>0"
        ElseIf TypeID = 2 Then
            SqlStr += "  having SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) >0"
        ElseIf TypeID = 3 Then
            SqlStr += " having SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END)>0 "
        ElseIf TypeID = 4 Then
            SqlStr += " having SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END)>0 "
        End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function
    Function AuditWiseAuditReportReportWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal StatusID As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal RegionID As Integer, ByVal AreaID As Integer, ByVal BranchID As Integer, ByVal TypeID As Integer) As DataTable
        Dim SqlStr As String = ""
        If (AuditMonth > 0) Then
            SqlStr = "select d.group_id,e.BRANCH_nAME ,d.Period_from,d.period_to from AUDIT_MASTER d,BrMaster e where  d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            SqlStr = "select d.group_id,e.BRANCH_nAME ,d.Period_from,d.period_to from AUDIT_MASTER d,BrMaster e where  d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2"
        End If
        If BranchID > 0 Then
            SqlStr += " and e.branch_ID=" + BranchID.ToString() + "  "
        ElseIf AreaID > 0 Then
            SqlStr += " and e.area_ID=" + AreaID.ToString() + "  "
        ElseIf RegionID > 0 Then
            SqlStr += " and e.region_ID=" + RegionID.ToString() + "  "
        End If

        If TypeID = 1 Then
            SqlStr += " and  D.FRAUD_COUNT>0 "
        ElseIf TypeID = 2 Then
            SqlStr += "  and  D.SERIOUS_COUNT>0 "
        ElseIf TypeID = 3 Then
            SqlStr += " and  D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 "
        ElseIf TypeID = 4 Then
            SqlStr += " and D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 "
        End If
        SqlStr += " order by e.BRANCH_nAME,d.group_id"
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function
    Function GetGradingSpecifications() As DataTable
        Return DB.ExecuteDataSet("select specification_id,grading_factor,(case when A.PERCENTAGE_LIMIT>0 then description_Perc else description_Number end),marks_awarded,b.category_Description,A.PERCENTAGE_LIMIT,a.number_limit,a.CATEGORY_ID,PERC_CRITERIA from audit_grading_specifications a,audit_grading_category b where a.category_ID=b.category_ID and a.status_id=1 order by a.category_ID,a.specification_ID").Tables(0)
    End Function
    'NMS -------------06-Feb-2016------
    Function GetEmployeesToCloseStaffInvolvement(ByVal EmpCode As Integer, ByVal PostID As Integer) As DataTable
        If PostID = 3 Then
            Return DB.ExecuteDataSet("select -1 as Emp_code,' ------Select------' as Emp_Name Union all SELECT h.emp_code,h.emp_Name From emp_Master h where EXISTS (SELECT distinct a.emp_Code from AUDIT_staff_involvement a,audit_observation_Dtl b,audit_observation c,audit_dtl d,Audit_Branches e where a.sino=b.sino and b.observation_ID=c.observation_ID and c.audit_ID=d.audit_ID and d.branch_ID=e.branch_id and a.emp_code=h.emp_code and d.STATUS_ID=2 and a.status_id=1  and (a.Amount-a.Refund_Amount)=0 and e.Team_lead=" + EmpCode.ToString() + ")").Tables(0)
        ElseIf PostID = 2 Then
            Return DB.ExecuteDataSet("select -1 as Emp_code,' ------Select------' as Emp_Name Union all SELECT h.emp_code,h.emp_Name From emp_Master h where EXISTS (SELECT distinct a.emp_Code from AUDIT_staff_involvement a,audit_observation_Dtl b,audit_observation c,audit_dtl d,Audit_Branches e,(select * from audit_admin_branches where emp_code=" & EmpCode.ToString & " and TYPE_ID=1) g  where a.sino=b.sino and b.observation_ID=c.observation_ID and c.audit_ID=d.audit_ID and d.branch_ID=e.branch_id and d.branch_ID=g.branch_id and a.emp_code=h.emp_code and d.STATUS_ID=2 and a.status_id=1  and (a.Amount-a.Refund_Amount)=0)").Tables(0)
        Else
            Return DB.ExecuteDataSet("select -1 as Emp_code,' ------Select------' as Emp_Name Union all SELECT h.emp_code,h.emp_Name From emp_Master h where EXISTS (SELECT distinct a.emp_Code from AUDIT_staff_involvement a,audit_observation_Dtl b,audit_observation c,audit_dtl d,Audit_Branches e where a.sino=b.sino and b.observation_ID=c.observation_ID and c.audit_ID=d.audit_ID and d.branch_ID=e.branch_id and a.emp_code=h.emp_code and d.STATUS_ID=2 and a.status_id=1  and (a.Amount-a.Refund_Amount)=0)").Tables(0)
        End If
    End Function
    Function GetObservationDtlsForStaffInvolvementRemoval(ByVal EmpCode As Integer, ByVal UserID As Integer, ByVal PostID As Integer) As DataTable
        Dim Sqlstr As String = ""
        Dim SqlStrSub As String = ""
        Dim SqlstrGeneral As String = ""
        Sqlstr = "select a.SINO,a.REMARKS,d.ITEM_NAME,a.LOAN_NO,c.CLIENT_NAME,e.CENTER_NAME,(case when a.SUSP_LEAKAGE=1 then 'S' when a.FRAUD=1 then 'VS' else 'G' end)as Type,a.FIN_LEAKAGE,g.Branch_Name,g.Branch_ID,i.display_Status from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,loan_master c,AUDIT_CHECK_LIST d,CENTER_MASTER e,audit_dtl f,Branch_Master g,audit_Staff_INvolvement h,AUDIT_OBSERVATION_STATUS_MASTER i where a.OBSERVATION_ID=b.OBSERVATION_ID and a.LOAN_NO=c.LOAN_NO and b.ITEM_ID=d.ITEM_ID and b.Audit_ID=f.Audit_Id and f.Audit_Type<>3 and f.branch_ID=g.branch_ID and c.CENTER_ID=e.CENTER_ID and a.sino=h.sino and a.status_id=i.status_ID and h.status_id=1  and (h.Amount-h.Refund_Amount)=0 and h.emp_code=" + EmpCode.ToString()
        SqlstrGeneral = "select a.SINO,a.REMARKS,d.ITEM_NAME,a.LOAN_NO,'','',(case when a.SUSP_LEAKAGE=1 then 'S' when a.FRAUD=1 then 'VS' else 'G' end)as Type,a.FIN_LEAKAGE,g.Branch_Name,g.Branch_ID,i.display_Status from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_CHECK_LIST d,audit_dtl f,Branch_Master g,audit_Staff_INvolvement h,AUDIT_OBSERVATION_STATUS_MASTER i  where a.OBSERVATION_ID=b.OBSERVATION_ID  and b.ITEM_ID=d.ITEM_ID  and b.Audit_ID=f.Audit_Id and f.Audit_Type=3 and f.branch_ID=g.branch_ID and a.sino=h.sino  and a.status_id=i.status_ID and h.status_id=1 and (h.Amount-h.Refund_Amount)=0  and h.emp_code=" + EmpCode.ToString() + "  "
        If PostID = 3 Then
            Sqlstr += " and f.branch_id in(select branch_id from audit_branches where team_lead=" + UserID.ToString() + ")"
            SqlstrGeneral += " and f.branch_id in(select branch_id from audit_branches where team_lead=" + UserID.ToString() + ")"
        End If
        Sqlstr += " Union all " + SqlstrGeneral
        Sqlstr += " order by g.Branch_ID ,d.item_Name,a.sino"
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Public Function GradingStatusDtl(ByVal BranchID As Integer) As DataTable
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@BranchID", BranchID)}
            Return DB.ExecuteDataSet("SP_GET_AUDIT_GRADING_DTL", Parameters).Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Function GetGradeDetails() As DataTable
        Return DB.ExecuteDataSet("select Grade_no,Grade,MARKS_FROM,MARKS_tO  from AUDIT_GRADE_MASTER WHERE STATUS_ID=1  order by marks_from  ").Tables(0)
    End Function
    Function GetGrades() As DataTable
        Return DB.ExecuteDataSet("select Grade_no,Grade  from AUDIT_GRADE_MASTER WHERE STATUS_ID<>0  order by Grade_no desc  ").Tables(0)
    End Function
    Public Function GradingReport(ByVal TypeID As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal RptID As Integer) As DataTable
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TypeID", TypeID), New System.Data.SqlClient.SqlParameter("@PostID", PostID), New System.Data.SqlClient.SqlParameter("@UserID", UserID), New System.Data.SqlClient.SqlParameter("@MarkFlag", RptID)}
            Return DB.ExecuteDataSet("SP_AUDIT_GRADING_REPORT", Parameters).Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Function GetGradeType() As DataTable
        Return DB.ExecuteDataSet("SELECT TYPEID,TypeDescription FROM AUDIT_GRADE_TYPE WHERE STATUS_ID=1").Tables(0)
    End Function
    Function GetGradeTypeAll() As DataTable
        Return DB.ExecuteDataSet("SELECT TYPEID,TypeDescription FROM AUDIT_GRADE_TYPE WHERE STATUS_ID>0").Tables(0)
    End Function
    Public Function Getexist_branch_TeamLead(ByVal empcode As Integer) As DataTable
        Return DB.ExecuteDataSet("select a.branch_id,d.Region_Name, c.Area_Name , b.branch_name ,isnull(a.Team_Lead,0) as emp_code," & empcode.ToString() & " " & _
                                 " ,e.Emp_Name from AUDIT_BRANCHES a " & _
                                 " LEFT JOIN emp_master e ON  a.TEAM_LEAD =e.Emp_Code ,branch_master b,AREA_MASTER c,REGION_MASTER d where a.branch_id=b.branch_id and b.Area_ID=c.Area_ID " & _
                                "  and c.Region_ID=d.Region_ID  and b.status_id=1 order by d.Region_Name, c.Area_Name , b.branch_name").Tables(0)
    End Function
    Public Function GetStr_branch_TeamLead(ByRef DT As DataTable) As String
        Dim StrAttendance As String = ""
        Dim val As Integer

        For n As Integer = 0 To DT.Rows.Count - 1
            If CInt(DT.Rows(n)(4)) = CInt(DT.Rows(n)(5)) Then
                val = 1
            Else
                val = 0
            End If
            StrAttendance += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & val & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6) & "µ22"
            If n < DT.Rows.Count - 1 Then
                StrAttendance += "¥"
            End If
        Next
        Return StrAttendance
    End Function
    Public Function AGINGDATEWISEREPORT(ByVal Type As Integer, ByVal RType As String, ByVal DayType As Integer, ByVal OptGreat As Integer, ByVal Days As Integer, ByVal SearchID As Integer, ByVal DetailSearch As Integer, ByVal HeadFlag As Integer, ByVal SearchID1 As Integer, ByVal AuditID As String, ByVal TRA_DT As DateTime) As DataTable
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Type", Type), New System.Data.SqlClient.SqlParameter("@RTYPE", RType), New System.Data.SqlClient.SqlParameter("@DayType", DayType), New System.Data.SqlClient.SqlParameter("@OptGreat", OptGreat), New System.Data.SqlClient.SqlParameter("@Days", Days), New System.Data.SqlClient.SqlParameter("@SearchID", SearchID), New System.Data.SqlClient.SqlParameter("@HeadFlag", HeadFlag), New System.Data.SqlClient.SqlParameter("@DetailSearch", DetailSearch), New System.Data.SqlClient.SqlParameter("@SearchID1", SearchID1), New System.Data.SqlClient.SqlParameter("@AuditID", AuditID), New System.Data.SqlClient.SqlParameter("@TRA_DT", TRA_DT)}
            Return DB.ExecuteDataSet("SP_AUDIT_BUCKET_WISE_DATE_WISE", Parameters).Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Function GetConsolidatedStaffInvolvement(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As DataTable
        Dim Sqlstr As String = ""
        Dim Sqlstr1 As String = ""
        Dim SqlFinal As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "(select f.Region_ID ,f.Region_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff,COUNT(distinct b.item_ID)as NoOfCases   from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=0 and DATEADD(day, DATEDIFF(day, 0, audit_Staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "')  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=1 and DATEADD(day, DATEDIFF(day, 0, audit_Staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "')  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=9 and DATEADD(day, DATEDIFF(day, 0, audit_Staff_involvement.Tra_DT), 0) between '" + FromDt + "'   and  '" + ToDt + "')  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " "
            Else
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where DATEADD(day, DATEDIFF(day, 0, audit_Staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' ) and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " "
            End If

        Else
            Sqlstr = "(select f.Region_ID ,f.Region_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff,COUNT(distinct b.item_ID)as NoOfCases from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=0 and DATEADD(day, DATEDIFF(day, 0, audit_Staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "')  and d.status_id=2 "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=1 and DATEADD(day, DATEDIFF(day, 0, audit_Staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "')  and d.status_id=2 "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where status_id=9 and DATEADD(day, DATEDIFF(day, 0, audit_Staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "')  and d.status_id=2 "
            Else
                Sqlstr1 = "(select f.Region_ID ,f.Region_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.SINO in(select distinct sino from audit_Staff_involvement where DATEADD(day, DATEDIFF(day, 0, audit_Staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.status_id=2 "
            End If

        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
            Sqlstr1 += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        Sqlstr += "  group by  f.Region_ID ,f.Region_Name"
        Sqlstr1 += "  group by  f.Region_ID ,f.Region_Name"
        SqlFinal = "select aa.Region_ID,aa.REgion_Name,aa.NoOfObservation,aa.NoOfStaff,isnull(bb.AmtInvolved,0),aa.NoOfCases from " + Sqlstr + ")aa left outer join " + Sqlstr1 + ")bb on(aa.Region_ID=bb.region_id)"
        Return DB.ExecuteDataSet(SqlFinal).Tables(0)
    End Function



    Function GetTotalStaffInvolved(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal EmpCode As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As Integer
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select COUNT(distinct e.emp_Code) as NoOfStaff from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select COUNT(distinct e.emp_Code) as NoOfStaff from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If EmpCode > 0 Then
            Sqlstr += " and a.SINo in(select SINO from Audit_Staff_Involvement where Emp_Code=" + EmpCode.ToString() + ")"
        End If

        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(Sqlstr).Tables(0)
        Dim TotalStaffs As Integer = CInt(dt.Rows(0)(0))
        Return TotalStaffs
    End Function



    Function GetStaffInvolvementStaff(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As DataTable
        Dim Sqlstr As String = ""
        Dim Sqlstr1 As String = ""
        Dim SqlSubstr As String = ""
        If PostID = 8 Then
            Sqlstr1 += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr1 += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr1 += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr1 += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr1 += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr1 += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr1 += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr1 += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr1 += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If

        If (AuditMonth > 0) Then
            SqlSubstr = ",(SELECT distinct r.emp_code,  r.branch_id, Resources = STUFF( (SELECT ','+ar.Name FROM (select distinct e.emp_code,datename(MONTH,d.Period_To) +'-'+CAST(YEAR(d.Period_To) AS VARCHAR(4)) as name,d.GROUP_ID,d.BRANCH_ID " & _
                " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g," & _
                " emp_list h  where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID " & _
                " and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  " & _
                " and a.STAFF_INVOLVEMENT=1 and e.emp_code=h.emp_code  and d.status_id=2   and month(d.period_to)=" + AuditMonth.ToString() + " " & _
                " and year(d.period_to)=" + AuditYear.ToString() + " " + Sqlstr1 + "  ) ar " & _
                " WHERE(ar.emp_code = r.emp_code And ar.BRANCH_ID = r.BRANCH_ID) " & _
                " FOR XML PATH('')), 1, 1, '') " & _
                " FROM " & _
                " (select distinct e.emp_code,convert(varchar,d.PERIOD_TO,106) as name" & _
                " ,d.GROUP_ID,d.BRANCH_ID " & _
                " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g," & _
                " emp_list h  where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID " & _
                " and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  " & _
                " and a.STAFF_INVOLVEMENT=1 and e.emp_code=h.emp_code  and d.status_id=2   and month(d.period_to)=" + AuditMonth.ToString() + "  " + Sqlstr1 + "  ) r) i"

            Sqlstr = "select  e.EMP_CODE,h.emp_name,f.branch_name,h.designation_name,count(e.SINO)as Observations,sum(a.Fin_Leakage) as Est_Amt_Involved," & _
                " COUNT(distinct d.GROUP_ID )as Audits,sum(case when a.FRAUD=1 then 1 else 0 end) as V_Serious,SUM(case when a.susp_leakage=1 then 1 else 0 end) " & _
                " as Serious,SUM(case when a.susp_leakage=0 and fraud=0 then 1 else 0 end) as General,MAX(e.tra_dt)as Last_Updated_On,f.branch_id,i.Resources as Audit_Periods,f.area_name as Area,f.region_name as Region " & _
                " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g," & _
                " emp_list h " & SqlSubstr & " where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID " & _
                " and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  " & _
                " and a.STAFF_INVOLVEMENT=1 and e.emp_code=h.emp_code  and d.status_id=2   and month(d.period_to)=" + AuditMonth.ToString() + " " & _
                " and year(d.period_to)=" + AuditYear.ToString() + " " + Sqlstr1 + " "
        Else
            SqlSubstr = ",(SELECT distinct r.emp_code,  r.branch_id, Resources = STUFF( (SELECT ','+ar.Name FROM (select distinct e.emp_code,datename(MONTH,d.Period_To) +'-'+CAST(YEAR(d.Period_To) AS VARCHAR(4)) as name,d.GROUP_ID,d.BRANCH_ID " & _
                " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g,emp_list h " & _
                "  where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
                " b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 " & _
                " and e.emp_code=h.emp_code and d.status_id=2  " + Sqlstr1 + " ) ar " & _
                " WHERE(ar.emp_code = r.emp_code And ar.BRANCH_ID = r.BRANCH_ID) " & _
                " FOR XML PATH('')), 1, 1, '') " & _
                " FROM " & _
                " (select distinct e.emp_code,convert(varchar,d.PERIOD_TO,106) as name" & _
                " ,d.GROUP_ID,d.BRANCH_ID " & _
                " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g,emp_list h " & _
                "  where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
                " b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 " & _
                " and e.emp_code=h.emp_code and d.status_id=2  " + Sqlstr1 + "  ) r) i"

          
            Sqlstr = "select   e.EMP_CODE,h.emp_name,f.branch_name,h.designation_name,count(e.SINO)as Observations,sum(a.Fin_Leakage) as Est_Amt_Involved," & _
                " COUNT(distinct d.GROUP_ID )as Audits,sum(case when a.FRAUD=1 then 1 else 0 end) as V_Serious,SUM(case when a.susp_leakage=1 then 1 else 0 end) " & _
                " as Serious,SUM(case when a.susp_leakage=0 and fraud=0 then 1 else 0 end) as General,MAX(e.tra_dt)as Last_Updated_On,f.branch_id,i.Resources as Audit_Periods,f.area_name as Area,f.region_name as Region " & _
                " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g,emp_list h " & _
                " " & SqlSubstr & " where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
                " b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 " & _
                " and e.emp_code=h.emp_code and d.status_id=2   " + Sqlstr1 + " "
        End If
        
        Sqlstr += " and e.emp_code=i.emp_code and d.Branch_ID=i.BRANCH_ID group by e.EMP_CODE,h.emp_name,f.branch_name,h.designation_name,f.branch_id,i.Resources,f.area_name,f.region_name order by f.region_name,f.area_name,f.branch_name,h.emp_name"

        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function



    Function GetStaffInvolvementAudits(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer, ByVal EmpCode As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As DataTable
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select  distinct d.GROUP_ID,d.PERIOD_FROM,D.PERIOD_TO,D.EMP_CODE,D.START_DT,D.END_DT,k.emp_name as Auditor,case when d.report_closed_dt is null then 'Not Closed' else 'Closed on -'+ convert(varchar(20),convert(varchar,d.report_closed_dt,106)) end  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g,emp_list h,EMP_LIST K where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and e.emp_code=h.emp_code  and d.status_id=2 and d.emp_code=k.emp_code  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "   and e.emp_code=" + EmpCode.ToString()
        Else
            Sqlstr = "select  distinct d.GROUP_ID,d.PERIOD_FROM,D.PERIOD_TO,D.EMP_CODE,D.START_DT,D.END_DT,k.emp_name as Auditor,case when d.report_closed_dt is null then 'Not Closed' else 'Closed on -'+ convert(varchar(20),convert(varchar,d.report_closed_dt,106)) end  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g,emp_list h,EMP_LIST K where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and e.emp_code=h.emp_code and d.status_id=2  and d.emp_code=k.emp_code and e.emp_code=" + EmpCode.ToString()
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        Sqlstr += "  order by 1"

        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function



    Function GetTotalStaffInvolvedObservations(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As Integer
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(Sqlstr).Tables(0)
        Dim TotalStaffs As Integer = CInt(dt.Rows(0)(0))
        Return TotalStaffs
    End Function


    Function GetTotalStaffInvolvedObservations_Fraud(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As Integer
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.fraud=1  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.fraud=1  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(Sqlstr).Tables(0)
        Dim TotalStaffs As Integer = CInt(dt.Rows(0)(0))
        Return TotalStaffs
    End Function


    Function GetTotalStaffInvolvedObservations_Serious(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As Integer
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.susp_leakage=1  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.susp_leakage=1  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(Sqlstr).Tables(0)
        Dim TotalStaffs As Integer = CInt(dt.Rows(0)(0))
        Return TotalStaffs
    End Function



    Function GetTotalStaffInvolvedObservations_General(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As Integer
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.susp_leakage=0 and a.fraud=0  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select COUNT(distinct e.sino) as NoOfObservation from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID and a.susp_leakage=0 and a.fraud=0  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(Sqlstr).Tables(0)
        Dim TotalStaffs As Integer = CInt(dt.Rows(0)(0))
        Return TotalStaffs
    End Function



    Function GetStaffInvolvementLoan(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal EmpCode As Integer, ByVal StatusID As Integer, ByVal TypeID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As DataTable
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select a.loan_no,h.Display_Status ,a.FIN_LEAKAGE,COUNT(distinct e.emp_code)as StaffInvolved,a.sino,g.item_Name,f.branch_Name,i.Remarks," & _
                " e.tra_dt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g, " & _
                " AUDIT_OBSERVATION_STATUS_MASTER h,audit_observation_Cycle i where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' " & _
                " and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and " & _
                " d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and a.STATUS_ID=h.Status_ID  and d.status_id=2 and a.order_id=i.order_Id  and " & _
                " month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select a.loan_no,h.Display_Status ,a.FIN_LEAKAGE,COUNT(distinct e.emp_code)as StaffInvolved,a.sino,g.item_Name,f.branch_Name," & _
                " i.Remarks,e.tra_dt  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f," & _
                " AUDIT_CHECK_LIST g,AUDIT_OBSERVATION_STATUS_MASTER h,audit_observation_Cycle i where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) " & _
                " between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID " & _
                " and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and a.STATUS_ID=h.Status_ID and d.status_id=2 " & _
                " and a.order_id=i.order_Id "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If TypeID = 1 Then
            Sqlstr += " and a.fraud=1"
        ElseIf TypeID = 2 Then
            Sqlstr += " and a.susp_leakage=1"
        ElseIf TypeID = 3 Then
            Sqlstr += " and a.fraud=0 and a.susp_leakage=0"
        End If
        If EmpCode > 0 Then
            Sqlstr += " and a.SINo in(select SINO from Audit_Staff_Involvement where Emp_Code=" + EmpCode.ToString() + ") and e.Emp_Code=" + EmpCode.ToString() + ""
        End If
        Sqlstr += "  group by a.loan_no,h.Display_Status ,a.FIN_LEAKAGE,a.sino,g.item_Name,f.branch_Name,i.Remarks,e.tra_dt order by e.tra_dt desc"
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function



    Function GetStaffInvolvementObservation(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As DataTable
        Dim Sqlstr As String = ""
        Dim Sqlstr1 As String = ""
        Dim SqlFinal As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "(select g.Item_ID ,g.Item_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            If StatusID = 0 Then
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            Else
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id<>9 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            End If

        Else
            Sqlstr = "(select g.Item_ID ,g.Item_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2  and f.Branch_ID=" + BranchID.ToString() + " "
            If StatusID = 0 Then
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + "  "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=9 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + "  "
            Else
                Sqlstr1 = "(select g.Item_ID ,g.Item_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved,c.audit_type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f,AUDIT_CHECK_LIST g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Branch_ID=" + BranchID.ToString() + "  "
            End If

        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        Sqlstr += "  group by g.Item_ID ,g.Item_Name,c.audit_type"
        Sqlstr1 += "  group by g.Item_ID ,g.Item_Name,c.audit_type"
        SqlFinal = "select aa.Item_ID,aa.Item_Name,aa.NoOfObservation,aa.NoOfStaff,bb.AmtInvolved,aa.audit_type from " + Sqlstr + ")aa," + Sqlstr1 + ")bb where aa.Item_ID=bb.Item_ID"
        Return DB.ExecuteDataSet(SqlFinal).Tables(0)
    End Function


    Function GetStaffInvolvementBranch(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal AreaID As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As DataTable
        Dim Sqlstr As String = ""
        Dim Sqlstr1 As String = ""
        Dim SqlFinal As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "(select f.Branch_ID ,f.Branch_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff ,COUNT(distinct b.item_id) as NoOfCases from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' ) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=9 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            Else
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            End If

        Else
            Sqlstr = "(select f.Branch_ID ,f.Branch_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff,COUNT(distinct b.item_id) as NoOfCases from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2  and f.Area_ID=" + AreaID.ToString() + " "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + "  "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=9 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + "  "
            Else
                Sqlstr1 = "(select f.Branch_ID ,f.Branch_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.Area_ID=" + AreaID.ToString() + "  "
            End If

        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        Sqlstr += "  group by  f.Branch_ID ,f.Branch_Name"
        Sqlstr1 += "  group by  f.Branch_ID ,f.Branch_Name"
        SqlFinal = "select aa.Branch_ID,aa.Branch_Name,aa.NoOfObservation,aa.NoOfStaff,bb.AmtInvolved,aa.NoOfCases from " + Sqlstr + ")aa," + Sqlstr1 + ")bb where aa.Branch_ID=bb.Branch_ID"
        Return DB.ExecuteDataSet(SqlFinal).Tables(0)
    End Function


    Function GetStaffInvolvementArea(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal RegionID As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As DataTable
        Dim Sqlstr As String = ""
        Dim Sqlstr1 As String = ""
        Dim SqlFinal As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "(select f.Area_id ,f.Area_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff,COUNT(distinct b.item_id) as NoOfCases from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=9 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' ) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            Else
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' ) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
            End If

        Else
            Sqlstr = "(select f.Area_id ,f.Area_Name ,COUNT(distinct a.sino) as NoOfObservation,COUNT(distinct e.emp_code) as NoOfStaff,COUNT(distinct b.item_id) as NoOfCases from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2  and f.region_id=" + RegionID.ToString() + " "
            If StatusID = 0 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=0 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  ) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + "  "
            ElseIf StatusID = 1 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=1 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' ) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + "  "
            ElseIf StatusID = 9 Then
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where status_id=9 and DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  ) and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + "  "
            Else
                Sqlstr1 = "(select f.Area_id ,f.Area_Name ,SUM(a.FIN_LEAKAGE) as AmtInvolved from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO in(select distinct sino from audit_staff_involvement where DATEADD(day, DATEDIFF(day, 0, audit_staff_involvement.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1  and d.status_id=2 and f.region_id=" + RegionID.ToString() + "  "
            End If

        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        Sqlstr += "  group by  f.Area_id ,f.Area_Name"
        Sqlstr1 += "  group by  f.Area_id ,f.Area_Name"
        SqlFinal = "select aa.Area_id,aa.Area_Name,aa.NoOfObservation,aa.NoOfStaff,bb.AmtInvolved,aa.NoOfCases from " + Sqlstr + ")aa," + Sqlstr1 + ")bb where aa.Area_ID=bb.Area_ID"
        Return DB.ExecuteDataSet(SqlFinal).Tables(0)
    End Function
   


    Function GetTotalLeakageStaffInvolvedObservations(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As Integer
        Dim Sqlstr As String = ""
        Dim SqlStr1 As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select distinct e.sino from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select distinct e.sino from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If
        SqlStr1 = "Select isnull(sum(fin_Leakage),0) from audit_observation_Dtl where sino in(" + Sqlstr + ")"
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(SqlStr1).Tables(0)
        Dim TotalLeakage As Integer
        If (dt.Rows(0)(0) > 0) Then
            TotalLeakage = CInt(dt.Rows(0)(0))
        Else
            TotalLeakage = 0
        End If
        Return TotalLeakage
    End Function
    '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^nms observation close date
    Function GetConsolidatedAuditReport(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal StatusID As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime, ByVal Rtype As Integer) As DataTable
        Dim Sqlstr As String = ""
        'If Rtype = 2 Then
        If (AuditMonth > 0) Then
            Sqlstr = "select e.region_ID,e.Region_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 " & _
                " then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.sino)as Total, " & _
                " sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL_datewise a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e  " & _
                " where  a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID " & _
                " and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " and " & _
                " year(d.period_to)=" + AuditYear.ToString() + ""
        Else
            Sqlstr = "select e.region_ID,e.Region_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then " & _
                " 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total," & _
                " sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e where " & _
                "  a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
                " b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If StatusID = 2 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) "
        ElseIf StatusID = 1 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <='" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL)  and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ") "
        ElseIf StatusID = 5 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL)  and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL_datewise where STATUS_ID=1 and DATEADD(day, DATEDIFF(day, 0, TRA_DT), 0) ='" + ToDt + "'))) "
        ElseIf StatusID = 0 Then
            Sqlstr += " and DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'"
        Else
            Sqlstr += " and DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' "
        End If
        Sqlstr += " group by e.Region_ID ,e.Region_Name"

        'Else
        'If (AuditMonth > 0) Then
        '    Sqlstr = "select e.region_ID,e.Region_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 " & _
        '        " then 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total, " & _
        '        " sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL_datewise a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e  " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where (DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' ) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID " & _
        '        " and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID " & _
        '        " and d.GROUP_ID =f.GROUP_ID " & _
        '        " and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " and " & _
        '        " year(d.period_to)=" + AuditYear.ToString() + " and a.status_id in(select observation_status from audit_observation_status_Dtl where " & _
        '        " status_id=" + StatusID.ToString() + ") "
        'Else
        '    Sqlstr = "select e.region_ID,e.Region_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then " & _
        '        " 1 else 0 end)as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total," & _
        '        " sum(Fin_Leakage)as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0) f " & _
        '        " where (DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' ) and a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
        '        " b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  " & _
        '        " and d.GROUP_ID =f.GROUP_ID " & _
        '        " and d.STATUS_ID=2  and a.status_id in(select observation_status " & _
        '        " from audit_observation_status_Dtl where status_id=" + StatusID.ToString() + ") "
        'End If
        'If PostID = 8 Then
        '    Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        'End If
        'Sqlstr += " GROUP BY e.region_ID,e.Region_Name"
        'End If

        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function

    Function GetConsolidatedAuditReportAreaWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal RegionID As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime, ByVal RTypeID As Integer) As DataTable
        Dim Sqlstr As String = ""
        'If RTypeID = 2 Then
        If (AuditMonth > 0) Then
            Sqlstr = "select e.area_ID,e.area_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end) " & _
                " as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.sino)as Total,sum(Fin_Leakage)as LeakageAmount " & _
                " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e " & _
                " where  a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
                " b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " " & _
                " and year(d.period_to)=" + AuditYear.ToString() + " and e.region_id=" + RegionID.ToString() + " "
        Else
            Sqlstr = "select e.area_ID,e.area_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as " & _
                " SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount " & _
                " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e " & _
                " where  a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
                " b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2  and e.region_id=" + RegionID.ToString() + " "
        End If
         If StatusID = 2 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) "
        ElseIf StatusID = 1 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <='" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL)  and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ") "
        ElseIf StatusID = 5 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL)  and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL_datewise where STATUS_ID=1 and DATEADD(day, DATEDIFF(day, 0, TRA_DT), 0) ='" + ToDt + "'))) "
        ElseIf StatusID = 0 Then
            Sqlstr += " and DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'"
        Else
            Sqlstr += " and DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' "
        End If
        Sqlstr += " GROUP BY e.area_id , e.area_name"
        'Else
        'If (AuditMonth > 0) Then
        '    Sqlstr = "select e.area_ID,e.area_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end) " & _
        '        " as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount " & _
        '        " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0) f " & _
        '        " where (DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
        '        " b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID " & _
        '        " and d.GROUP_ID =f.GROUP_ID " & _
        '        " and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " " & _
        '        " and year(d.period_to)=" + AuditYear.ToString() + " and e.region_id=" + RegionID.ToString() + " and " & _
        '        " a.status_id in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ")"
        'Else
        '    Sqlstr = "select e.area_ID,e.area_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as " & _
        '        " SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)as LeakageAmount " & _
        '        " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0) f " & _
        '        " where (DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "') and a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
        '        " b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID " & _
        '        " and d.GROUP_ID =f.GROUP_ID " & _
        '        " and d.STATUS_ID=2  and e.region_id=" + RegionID.ToString() + " " & _
        '        " and a.status_id in(select observation_status from audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ")"
        'End If
        'Sqlstr += " GROUP BY e.area_id , e.area_name"
        'End If

        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function


    Function GetConsolidatedAuditReportBranchWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal AreaID As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime, ByVal RTypeID As Integer) As DataTable

        Dim Sqlstr As String = ""
        'If RTypeID = 2 Then
        If (AuditMonth > 0) Then
            Sqlstr = "select d.BRANCH_ID,e.Branch_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as " & _
                " SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)  " & _
                " as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e where  " & _
                " a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID" & _
                " and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " " & _
                " and e.area_id=" + AreaID.ToString() + ""
        Else
            Sqlstr = "select d.BRANCH_ID,e.Branch_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end) " & _
                " as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage) " & _
                " as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e where " & _
                " a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and " & _
                " d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2  and e.area_id=" + AreaID.ToString() + ""
        End If
          If StatusID = 2 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) "
        ElseIf StatusID = 1 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <='" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL)  and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ") "
        ElseIf StatusID = 5 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL)  and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL_datewise where STATUS_ID=1 and DATEADD(day, DATEDIFF(day, 0, TRA_DT), 0) ='" + ToDt + "'))) "
        ElseIf StatusID = 0 Then
            Sqlstr += " and DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'"
        Else
            Sqlstr += " and DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' "
        End If

        Sqlstr += " GROUP BY D.BRANCH_ID,e.Branch_Name"
        'Else
        'If (AuditMonth > 0) Then
        '    Sqlstr = "select d.BRANCH_ID,e.Branch_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end)as " & _
        '        " SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage)  " & _
        '        " as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0) f " & _
        '        " where (DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) " & _
        '        " between '" + FromDt + "'  and  '" + ToDt + "') and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID" & _
        '        " and d.GROUP_ID =f.GROUP_ID " & _
        '        " and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " " & _
        '        " and e.area_id=" + AreaID.ToString() + " and a.status_id in(select observation_status from audit_observation_status_Dtl where " & _
        '        " status_id=" + CloseFlag.ToString() + ")"
        'Else
        '    Sqlstr = "select d.BRANCH_ID,e.Branch_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end) " & _
        '        " as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage) " & _
        '        " as LeakageAmount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0) f " & _
        '        " where (DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) " & _
        '        " between '" + FromDt + "'  and  '" + ToDt + "') and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  " & _
        '        " and d.GROUP_ID =f.GROUP_ID  and" & _
        '        " d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2  and e.area_id=" + AreaID.ToString() + " and a.status_id in(select observation_status from " & _
        '        " audit_observation_status_Dtl where status_id=" + CloseFlag.ToString() + ")"
        'End If
        'Sqlstr += " GROUP BY D.BRANCH_ID,e.Branch_Name"
        'End If
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Function GetConsolidatedAuditReportObservationWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal BranchID As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime, ByVal RtypeID As Integer) As DataTable
        Dim Sqlstr As String = ""
        'If RtypeID = 2 Then
        If (AuditMonth > 0) Then
            Sqlstr = "select b.Observation_ID,f.Item_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end) " & _
                " as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage) " & _
                " as LeakageAmount,c.audit_Type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e,AUDIT_CHECK_LIST f where " & _
                "  a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
                " b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID and b.Item_ID=f.Item_ID  and d.STATUS_ID=2 " & _
                " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " and e.Branch_id=" + BranchID.ToString() + ""
        Else
            Sqlstr = "select b.Observation_ID,f.Item_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end) " & _
                " as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage) " & _
                " as LeakageAmount,c.audit_Type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e,AUDIT_CHECK_LIST f where " & _
                "  a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID " & _
                " and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID and b.Item_ID=f.Item_ID  and d.STATUS_ID=2  and e.Branch_id=" + BranchID.ToString() + ""
        End If
        If StatusID = 2 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) "
        ElseIf StatusID = 1 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <='" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL)  and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ") "
        ElseIf StatusID = 5 Then
            Sqlstr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL)  and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL_datewise where STATUS_ID=1 and DATEADD(day, DATEDIFF(day, 0, TRA_DT), 0) ='" + ToDt + "'))) "
        ElseIf StatusID = 0 Then
            Sqlstr += " and DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'"
        Else
            Sqlstr += " and DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' "
        End If

        Sqlstr += " GROUP BY b.Observation_ID,f.Item_Name,c.audit_Type order by 3 desc,4 desc,2"
        'Else
        'If (AuditMonth > 0) Then
        '    Sqlstr = "select b.Observation_ID,f.Item_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end) " & _
        '        " as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage) " & _
        '        " as LeakageAmount,c.audit_Type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e,AUDIT_CHECK_LIST f  " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0) g " & _
        '        " where (DATEADD(day, DATEDIFF(day, 0, g.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "')  and a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
        '        " b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID and b.Item_ID=f.Item_ID " & _
        '        " and d.group_id=g.group_id" & _
        '        " and d.STATUS_ID=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + " and e.Branch_id=" + BranchID.ToString() + "" & _
        '        " and a.status_id in(select observation_status from audit_observation_status_dtl where status_id=" + CloseFlag.ToString() + ")"
        'Else
        '    Sqlstr = "select b.Observation_ID,f.Item_Name,SUM(case when a.FRAUD =1 then 1 else 0 end)as SuspectedFraud,SUM(case when a.SUSP_LEAKAGE =1 then 1 else 0 end) " & _
        '        " as SuspectedLeakage,SUM(case when (a.FRAUD=0 AND A.SUSP_LEAKAGE=0) then 1 else 0 end)as Others,COUNT(a.OBSERVATION_ID)as Total,sum(Fin_Leakage) " & _
        '        " as LeakageAmount,c.audit_Type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,brmaster e,AUDIT_CHECK_LIST f  " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0 ) g " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, g.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID " & _
        '        " and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID and b.Item_ID=f.Item_ID " & _
        '        " and d.group_id=g.group_id" & _
        '        " and d.STATUS_ID=2  and e.Branch_id=" + BranchID.ToString() + "" & _
        '        " and a.status_id in(select observation_status from audit_observation_status_dtl where status_id=" + CloseFlag.ToString() + ")"
        'End If
        'Sqlstr += " GROUP BY b.Observation_ID,f.Item_Name,c.audit_Type order by 3 desc,4 desc,2"
        'End If
        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function


    Function ConsolidatedAuditReportReportWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal StatusID As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime, ByVal RType As Integer) As DataTable
        Dim SqlStr As String = ""
        'If RType = 2 Then

        If (AuditMonth > 0) Then
            SqlStr = "select e.Region_ID  ,e.Region_Name   ,count(distinct d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports, " & _
                " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both, " & _
                " SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e " & _
                " where   d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            SqlStr = "select e.Region_ID  ,e.Region_Name   ,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports," & _
                " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both, " & _
                " SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e " & _
                " where   d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2"
        End If
        If PostID = 8 Then
            SqlStr += " and e.zone_head=" + UserID.ToString()
        End If
        'If StatusID = 2 Then
        '    SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b, AUDIT_DTL c,AUDIT_MASTER d " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2   group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'ElseIf StatusID = 1 Then
        '    SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b, AUDIT_DTL c,AUDIT_MASTER d " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'ElseIf StatusID = 5 Then
        '    SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL where STATUS_ID=1)))  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'ElseIf StatusID = 0 Then
        '    SqlStr += " and DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' "
        'Else
        '    SqlStr += " and DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' "
        'End If

        If StatusID = 2 Then
            SqlStr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) "
        ElseIf StatusID = 1 Then
            SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL_datewise a,AUDIT_OBSERVATION b, AUDIT_DTL c,AUDIT_MASTER d " & _
                " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <='" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        ElseIf StatusID = 5 Then
            SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL_datewise a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
                " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL_datewise where STATUS_ID=1  and DATEADD(day, DATEDIFF(day, 0, TRA_DT), 0) ='" + ToDt + "')))  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        ElseIf StatusID = 0 Then
            SqlStr += " and DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'"
        Else
            SqlStr += " and DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' "
        End If
        SqlStr += " group by e.Region_ID ,e.Region_Name"

        'Else
        'If (AuditMonth > 0) Then
        '    SqlStr = "select e.Region_ID  ,e.Region_Name   ,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports, " & _
        '        " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both, " & _
        '        " SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e " & _
        '         " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and  d.BRANCH_ID=e.Branch_ID and d.group_id=f.group_id and d.STATUS_ID=2  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        'Else
        '    SqlStr = "select e.Region_ID  ,e.Region_Name   ,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports," & _
        '        " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both, " & _
        '        " SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e " & _
        '         " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and  d.BRANCH_ID=e.Branch_ID and d.group_id=f.group_id and d.STATUS_ID=2"
        'End If
        'If PostID = 8 Then
        '    SqlStr += " and e.zone_head=" + UserID.ToString()
        'End If
        'If StatusID = 1 Or StatusID = 2 Then
        '    SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b, AUDIT_DTL c,AUDIT_MASTER d " & _
        '         " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.group_id=f.group_id and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'ElseIf StatusID = 5 Then
        '    SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
        '         " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.group_id=f.group_id and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL where STATUS_ID=1)))  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'ElseIf StatusID = 0 Then
        '    SqlStr += " and not exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
        '         " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '        " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '        " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID  HAVING  SUM(aa.STATUS_ID)=0) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.group_id=f.group_id and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID>" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'End If
        'SqlStr += " group by e.Region_ID ,e.Region_Name"
        'End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function

    
    Function AreaWiseAuditReportReportWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal StatusID As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal RegionID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime, ByVal RType As Integer) As DataTable
        Dim SqlStr As String = ""
        'If RType = 2 Then
        If (AuditMonth > 0) Then
            SqlStr = "select e.area_ID  ,e.Area_Name   ,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports," & _
                " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) " & _
                " as Both,SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e " & _
                " where    d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2 " & _
                " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            SqlStr = "select e.area_ID  ,e.Area_Name   ,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports, " & _
                " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both, " & _
                " SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e " & _
                " where   d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2"
        End If
        If RegionID > 0 Then
            SqlStr += " and e.region_id=" + RegionID.ToString()
        End If
        If StatusID = 2 Then
            SqlStr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) "
        ElseIf StatusID = 1 Then
            SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL_datewise a,AUDIT_OBSERVATION b, AUDIT_DTL c,AUDIT_MASTER d " & _
                " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <='" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        ElseIf StatusID = 5 Then
            SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL_datewise a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
                " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL_datewise where STATUS_ID=1 and DATEADD(day, DATEDIFF(day, 0, TRA_DT), 0) ='" + ToDt + "')))  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        ElseIf StatusID = 0 Then
            SqlStr += " and DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'"
        Else
            SqlStr += " and DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' "
        End If
        SqlStr += " group by e.area_ID ,e.Area_Name"
        'Else


        'If (AuditMonth > 0) Then
        '    SqlStr = "select e.area_ID  ,e.Area_Name   ,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports," & _
        '        " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) " & _
        '        " as Both,SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '       " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '       " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '       " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and   d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2 " & _
        '       " and d.GROUP_ID =f.GROUP_ID " & _
        '       " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        'Else
        '    SqlStr = "select e.area_ID  ,e.Area_Name   ,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports, " & _
        '        " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both, " & _
        '        " SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '       " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '       " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and  d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2 and d.GROUP_ID =f.GROUP_ID"
        'End If
        'If RegionID > 0 Then
        '    SqlStr += " and e.region_id=" + RegionID.ToString()
        'End If
        'If StatusID = 1 Or StatusID = 2 Then
        '    SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '       " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '       " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'ElseIf StatusID = 5 Then
        '    SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
        '    " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '       " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '       " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL where STATUS_ID=1)))  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'ElseIf StatusID = 0 Then
        '    SqlStr += " and not exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
        '    " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '       " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '       " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID>" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'End If
        'SqlStr += " group by e.area_ID ,e.Area_Name"
        'End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function

    Function BranchWiseAuditReportReportWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal StatusID As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal RegionID As Integer, ByVal AreaID As Integer, ByVal BranchID As Integer, ByVal TypeID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime, ByVal RType As Integer) As DataTable
        Dim SqlStr As String = ""
        'If RType = 2 Then
        If (AuditMonth > 0) Then
            SqlStr = "select e.Branch_ID ,e.Branch_Name,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports,SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both,SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e where   d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            SqlStr = "select e.Branch_ID ,e.Branch_Name,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports,SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both,SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e where  d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2"
        End If
        If BranchID > 0 Then
            SqlStr += " and e.branch_ID=" + BranchID.ToString() + "  "
        ElseIf AreaID > 0 Then
            SqlStr += " and e.area_ID=" + AreaID.ToString() + "  "
        ElseIf RegionID > 0 Then
            SqlStr += " and e.region_ID=" + RegionID.ToString() + "  "
        End If
        If StatusID = 2 Then
            SqlStr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) "
        ElseIf StatusID = 1 Then
            SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL_datewise a,AUDIT_OBSERVATION b, AUDIT_DTL c,AUDIT_MASTER d " & _
                " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <='" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        ElseIf StatusID = 5 Then
            SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL_datewise a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
                " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL_datewise where STATUS_ID=1 and DATEADD(day, DATEDIFF(day, 0, TRA_DT), 0) ='" + ToDt + "')))  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        ElseIf StatusID = 0 Then
            SqlStr += " and DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'"
        Else
            SqlStr += " and DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' "
        End If

        SqlStr += " group by e.Branch_ID ,e.Branch_Name "
        If TypeID = 1 Then
            SqlStr += " having SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END)>0"
        ElseIf TypeID = 2 Then
            SqlStr += "  having SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) >0"
        ElseIf TypeID = 3 Then
            SqlStr += " having SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END)>0 "
        ElseIf TypeID = 4 Then
            SqlStr += " having SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END)>0 "
        End If
        'Else
        'If (AuditMonth > 0) Then
        '    SqlStr = "select e.Branch_ID ,e.Branch_Name,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports, " & _
        '        " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both," & _
        '        " SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '           " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '           " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and  d.BRANCH_ID=e.Branch_ID and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2 " & _
        '        " and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        'Else
        '    SqlStr = "select e.Branch_ID ,e.Branch_Name,count(d.group_id)as TotalReports, SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END) as FraudReports," & _
        '        " SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as SeriousReports,SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) as Both " & _
        '        " ,SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END) as GENERAL from AUDIT_MASTER d,BrMaster e " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '           " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '           " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        "where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and d.BRANCH_ID=e.Branch_ID and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2"
        'End If
        'If BranchID > 0 Then
        '    SqlStr += " and e.branch_ID=" + BranchID.ToString() + "  "
        'ElseIf AreaID > 0 Then
        '    SqlStr += " and e.area_ID=" + AreaID.ToString() + "  "
        'ElseIf RegionID > 0 Then
        '    SqlStr += " and e.region_ID=" + RegionID.ToString() + "  "
        'End If
        'If StatusID = 1 Or StatusID = 2 Then
        '    SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c, AUDIT_MASTER d " & _
        '           " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '           " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '           " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'ElseIf StatusID = 5 Then
        '    SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '           " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '           " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL where STATUS_ID=1)))  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'ElseIf StatusID = 0 Then
        '    SqlStr += " and not exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '           " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '           " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, f.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID>" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'End If

        'SqlStr += " group by e.Branch_ID ,e.Branch_Name "
        'If TypeID = 1 Then
        '    SqlStr += " having SUM(case when D.FRAUD_COUNT>0 THEN 1 ELSE 0 END)>0"
        'ElseIf TypeID = 2 Then
        '    SqlStr += "  having SUM(case when D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END) >0"
        'ElseIf TypeID = 3 Then
        '    SqlStr += " having SUM(case when D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 THEN 1 ELSE 0 END)>0 "
        'ElseIf TypeID = 4 Then
        '    SqlStr += " having SUM(case when D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 THEN 1 ELSE 0 END)>0 "
        'End If
        'End If

        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function


    Function AuditWiseAuditReportReportWise(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal StatusID As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal RegionID As Integer, ByVal AreaID As Integer, ByVal BranchID As Integer, ByVal TypeID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime, ByVal RType As Integer) As DataTable
        Dim SqlStr As String = ""
        'If RType = 2 Then
        If (AuditMonth > 0) Then
            SqlStr = "select d.group_id,e.BRANCH_nAME ,d.Period_from,d.period_to from AUDIT_MASTER d,BrMaster e " & _
                " where  d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            SqlStr = "select d.group_id,e.BRANCH_nAME ,d.Period_from,d.period_to from AUDIT_MASTER d,BrMaster e " & _
                " where  d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2"
        End If
        If BranchID > 0 Then
            SqlStr += " and e.branch_ID=" + BranchID.ToString() + "  "
        ElseIf AreaID > 0 Then
            SqlStr += " and e.area_ID=" + AreaID.ToString() + "  "
        ElseIf RegionID > 0 Then
            SqlStr += " and e.region_ID=" + RegionID.ToString() + "  "
        End If

        If TypeID = 1 Then
            SqlStr += " and  D.FRAUD_COUNT>0 "
        ElseIf TypeID = 2 Then
            SqlStr += "  and  D.SERIOUS_COUNT>0 "
        ElseIf TypeID = 3 Then
            SqlStr += " and  D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 "
        ElseIf TypeID = 4 Then
            SqlStr += " and D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 "
        End If
         If StatusID = 2 Then
            SqlStr += " and  DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) "
        ElseIf StatusID = 1 Then
            SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL_datewise a,AUDIT_OBSERVATION b, AUDIT_DTL c,AUDIT_MASTER d " & _
                " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <='" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        ElseIf StatusID = 5 Then
            SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL_datewise a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
                " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and DATEADD(day, DATEDIFF(day, 0, a.TRA_DT), 0) ='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL_datewise where STATUS_ID=1 and DATEADD(day, DATEDIFF(day, 0, TRA_DT), 0) ='" + ToDt + "')))  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        ElseIf StatusID = 0 Then
            SqlStr += " and DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'"
        Else
            SqlStr += " and DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' "
        End If
        SqlStr += " order by e.BRANCH_nAME,d.group_id"

        'Else
        ''To Be continue

        'If (AuditMonth > 0) Then
        '    SqlStr = "select d.group_id,e.BRANCH_nAME ,d.Period_from,d.period_to from AUDIT_MASTER d,BrMaster e " & _
        '         " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '       " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '       " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, F.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and d.BRANCH_ID=e.Branch_ID and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2  and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        'Else
        '    SqlStr = "select d.group_id,e.BRANCH_nAME ,d.Period_from,d.period_to from AUDIT_MASTER d,BrMaster e " & _
        '         " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '       " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '       " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0, F.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and d.BRANCH_ID=e.Branch_ID and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2"
        'End If
        'If BranchID > 0 Then
        '    SqlStr += " and e.branch_ID=" + BranchID.ToString() + "  "
        'ElseIf AreaID > 0 Then
        '    SqlStr += " and e.area_ID=" + AreaID.ToString() + "  "
        'ElseIf RegionID > 0 Then
        '    SqlStr += " and e.region_ID=" + RegionID.ToString() + "  "
        'End If

        'If TypeID = 1 Then
        '    SqlStr += " and  D.FRAUD_COUNT>0 "
        'ElseIf TypeID = 2 Then
        '    SqlStr += "  and  D.SERIOUS_COUNT>0 "
        'ElseIf TypeID = 3 Then
        '    SqlStr += " and  D.FRAUD_COUNT>0 AND D.SERIOUS_COUNT>0 "
        'ElseIf TypeID = 4 Then
        '    SqlStr += " and D.FRAUD_COUNT=0 AND D.SERIOUS_COUNT=0 "
        'End If
        'If StatusID = 1 Or StatusID = 2 Then
        '    SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '       " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '       " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0,  F.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'ElseIf StatusID = 5 Then
        '    SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '       " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '       " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0,  F.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL where STATUS_ID=1)))  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'ElseIf StatusID = 0 Then
        '    SqlStr += " and not exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
        '        " , (select  MAX(LAST_UPDATED_DT) as LAST_CLOSED_DT,cc.GROUP_ID  FROM AUDIT_OBSERVATION_DTL aa " & _
        '       " INNER JOIN AUDIT_OBSERVATION bb  ON aa.OBSERVATION_ID=bb.OBSERVATION_ID LEFT JOIN Audit_dtl cc ON bb.AUDIT_ID=cc.AUDIT_ID " & _
        '       " LEFT JOIN AUDIT_MASTER dd ON cc.GROUP_ID=dd.GROUP_ID  GROUP BY cc.GROUP_ID HAVING  SUM(aa.STATUS_ID)=0 ) f " & _
        '        " where DATEADD(day, DATEDIFF(day, 0,  F.LAST_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and d.GROUP_ID =f.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID>" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where d.group_id=mm.group_id and mm.cnt>0)"
        'End If
        'SqlStr += " order by e.BRANCH_nAME,d.group_id"
        'End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function
    '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    Function GetGroupName(ByVal GroupID As Integer) As String
        Dim DT As DataTable = DB.ExecuteDataSet("select GROUP_NAME   from AUDIT_CHECK_GROUPS WHERE GRoup_ID= " + GroupID.ToString()).Tables(0)
        Dim GroupName As String = ""
        If DT.Rows.Count > 0 Then
            GroupName = DT.Rows(0)(0).ToString()
        End If
        Return GroupName
    End Function

    Function GetSubGroupName(ByVal SubGroupID As Integer) As String
        Dim DT As DataTable = DB.ExecuteDataSet("select SUB_GROUP_NAME   from AUDIT_CHECK_SUBGROUPS where SUB_GROUP_ID= " + SubGroupID.ToString()).Tables(0)
        Dim GroupName As String = ""
        If DT.Rows.Count > 0 Then
            GroupName = DT.Rows(0)(0).ToString()
        End If
        Return GroupName
    End Function
    Function GetCheckListName(ByVal CheckListID As Integer) As String
        Dim DT As DataTable = DB.ExecuteDataSet("select ITEM_NAME   from AUDIT_CHECK_LIST where ITEM_ID= " + CheckListID.ToString()).Tables(0)
        Dim GroupName As String = ""
        If DT.Rows.Count > 0 Then
            GroupName = DT.Rows(0)(0).ToString()
        End If
        Return GroupName
    End Function
    Function GetClassificationName(ByVal ClassificationID As Integer) As String
        Dim DT As DataTable = DB.ExecuteDataSet("select Classification_Name   from Audit_Classification where Classification_ID= " + ClassificationID.ToString()).Tables(0)
        Dim GroupName As String = ""
        If DT.Rows.Count > 0 Then
            GroupName = DT.Rows(0)(0).ToString()
        End If
        Return GroupName
    End Function

 Function GetAbstractReportDetail(ByVal Month As Integer, ByVal Year As Integer, ByVal RegionID As Integer, ByVal AreaID As Integer, ByVal BranchID As Integer, ByVal TypeID As Integer, ByVal CloseFlag As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime, ByVal Rtype As Integer, Optional ByVal SearchID As Integer = 0, Optional ByVal StatusID As Integer = 0, Optional ByVal ItemID As Integer = 0, Optional ByVal UserID As Integer = 0) As DataTable
        Try
            Dim Params(13) As SqlParameter
            Params(0) = New SqlParameter("@Month", SqlDbType.Int)
            Params(0).Value = Month
            Params(1) = New SqlParameter("@Year", SqlDbType.Int)
            Params(1).Value = Year
            Params(2) = New SqlParameter("@RegionID", SqlDbType.Int)
            Params(2).Value = RegionID
            Params(3) = New SqlParameter("@AreaID", SqlDbType.Int)
            Params(3).Value = AreaID
            Params(4) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(4).Value = BranchID
            Params(5) = New SqlParameter("@TypeID", SqlDbType.Int)
            Params(5).Value = TypeID
            Params(6) = New SqlParameter("@StatusID", SqlDbType.Int)
            Params(6).Value = StatusID
            Params(7) = New SqlParameter("@ItemID", SqlDbType.Int)
            Params(7).Value = ItemID
            Params(8) = New SqlParameter("@CloseFlag", SqlDbType.Int)
            Params(8).Value = CloseFlag
            Params(9) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(9).Value = UserID
            Params(10) = New SqlParameter("@FromDt", SqlDbType.DateTime)
            Params(10).Value = FromDt
            Params(11) = New SqlParameter("@ToDt", SqlDbType.DateTime)
            Params(11).Value = ToDt
            Params(12) = New SqlParameter("@Type", SqlDbType.Int)
            Params(12).Value = Rtype
            Params(13) = New SqlParameter("@SearchID", SqlDbType.Int)
            Params(13).Value = SearchID

            Return DB.ExecuteDataSet("SP_GET_AUDIT_ABSTRACT_REPORT_DETAIL", Params).Tables(0)

        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Function GetClassification() As DataTable
        Return DB.ExecuteDataSet("select -1 as Classification_ID,' ------Select------' as Classification_Name Union All SELECT Classification_ID,Classification_Name  FROM AUDIT_CLASSIFICATION order by 2").Tables(0)

    End Function
  
   
    Public Function Get_Exist_AuditGroupStringNew(ByRef DT As DataTable) As String
        Dim StrAudit As String = ""

        For n As Integer = 0 To DT.Rows.Count - 1

            StrAudit += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2)


            If n < DT.Rows.Count - 1 Then
                StrAudit += "¥"
            End If
        Next
        Return StrAudit
    End Function
    Function GetCheckGroupNew(ByVal Classification_ID As Integer, Optional ByVal Type As Integer = 0) As DataTable
        If Type = 0 Then
            Return DB.ExecuteDataSet("select -1 as Group_ID,' ALL' as Group_Name Union All SELECT GROUP_ID,b.TYPE_NAME +'/'+UPPER( GROUP_NAME)  FROM AUDIT_CHECK_GROUPS a,AUDIT_TYPE b WHERE a.AUDIT_TYPE=b.TYPE_ID and  a.STATUS_ID=1 and a.classification_id=" + Classification_ID.ToString() + " order by 2").Tables(0)
        Else
            Return DB.ExecuteDataSet("select -1 as Group_ID,' ALL' as Group_Name Union All SELECT GROUP_ID,b.TYPE_NAME +'/'+UPPER( GROUP_NAME)  FROM AUDIT_CHECK_GROUPS a,AUDIT_TYPE b WHERE a.AUDIT_TYPE=b.TYPE_ID and  a.STATUS_ID=1 order by 2").Tables(0)
        End If


    End Function
    Public Function Get_Department_For_Confirmation() As DataTable
        Return DB.ExecuteDataSet("SELECT -1 AS DEPARTMENT_ID,'-----SELECT------' AS DEPARTMENT_NAME UNION ALL SELECT DEPARTMENT_ID,DEPARTMENT_NAME FROM AUDIT_CONFIRMATION_DEPARTMENTS WHERE STATUS_ID=1").Tables(0)
    End Function
    Function GetAuditAccountsSangamWise(ByVal CenterID As Integer, ByVal AuditID As Integer, ByVal AuditTypeID As Integer) As DataTable
        Return DB.ExecuteDataSet("select f.group_name,e.Sub_Group_Name, a.ITEM_ID,a.ITEM_NAME,e.GROUP_ID,isnull(d.ITEM_ID,0),isnull(d.POT_FRAUD,0),isnull(d.SUSP_LEAKAGE,0),isnull(d.REMARKS,''),isnull(d.FIN_LEAKAGE,0),isnull(d.Fraud,0),isnull(d.Status_ID,1),isnull(d.Staff_Involvement,0),case when VERY_SERIOUS_FLAG=1 then 'V' when SERIOUS_FLAG=1 then 'S' else 'G' end from AUDIT_CHECK_LIST a left outer join  (select b.ITEM_ID,c.LOAN_NO,c.POT_FRAUD,c.REMARKS,c.SUSP_LEAKAGE,c.FIN_LEAKAGE,c.Fraud,c.Status_ID,c.Staff_Involvement  from AUDIT_OBSERVATION b,AUDIT_OBSERVATION_DTL c where b.observation_ID=c.OBSERVATION_ID and b.AUDIT_ID=" + AuditID.ToString() + " and c.LOAN_NO IN (select LOAN_NO from LOAN_MASTER where CENTER_ID=" + CenterID.ToString() + ")) d on a.ITEM_ID=d.ITEM_ID,AUDIT_CHECK_SUBGROUPS e,audit_Check_Groups f where a.SUB_GROUP_ID=e.SUB_GROUP_ID and e.group_id=f.group_ID and f.audit_type=" + AuditTypeID.ToString()).Tables(0)
    End Function
    Function GetOneLoanAccounts(ByVal CentreID As String, ByVal AuditID As Integer) As DataTable
        'and a.AUDIT_ID=" + AuditID.ToString() + "
        '----------- To Be Continueeee 
        Dim DTloan As New DataTable
        DTloan = DB.ExecuteDataSet("select top 1 d.LOAN_NO from audit_master a,audit_dtl b,audit_observation c,audit_observation_dtl d,LOAN_MASTER e " & _
                                   " where a.group_id=b.group_id and b.audit_id=c.audit_id and c.observation_id=d.observation_id and d.LOAN_NO=e.LOAN_NO and b.audit_id=" + AuditID.ToString() + " and e.CENTER_ID=" + CentreID.ToString() + " and b.audit_type=4").Tables(0)
        If DTloan.Rows.Count > 0 Then
            Return DTloan
        Else
            Return DB.ExecuteDataSet("select top 1 a.LOAN_NO from LOAN_MASTER a  where  a.CENTER_ID=" + CentreID.ToString() + " ORDER BY a.loan_no").Tables(0)
        End If

    End Function
    Function getSangams(ByVal AuditID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as center_ID,' ------Select------' as center_name union all select distinct center_ID,center_name from center_master c where exists (select distinct b.center_ID from " & _
            " (Select aa.* from audit_accounts aa ,AUDIT_DTL c where aa.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID =(select GROUP_ID from AUDIT_DTL where AUDIT_ID=" & AuditID.ToString() & "))a ,LOAN_MASTER b  " & _
            " where a.LOAN_NO=b.LOAN_NO  and c.center_id=b.center_id)  order by 2 ").Tables(0)
    End Function
    Function GetCheckListGeneral(ByVal SubGroupID As Integer, Optional ByVal Type As Integer = 0) As DataTable
        If Type = 0 Then
            Return DB.ExecuteDataSet("select '-1' as sub_Group_ID,' ------Select------' as sub_Group_Name Union All select cast(ITEM_ID as varchar)+'~'+cast(serious_Flag as varchar)+'~'+cast(very_serious_Flag as varchar),ITEM_NAME from AUDIT_CHECK_LIST where Sub_group_ID=" + SubGroupID.ToString() + " and STATUS_ID=1").Tables(0)
        Else
            Return DB.ExecuteDataSet("select '-1' as sub_Group_ID,' ALL' as sub_Group_Name Union All select cast(ITEM_ID as varchar)+'~'+cast(serious_Flag as varchar)+'~'+cast(very_serious_Flag as varchar),ITEM_NAME from AUDIT_CHECK_LIST where Sub_group_ID=" + SubGroupID.ToString() + " and STATUS_ID=1").Tables(0)
        End If
    End Function
    Public Function GetItemForResponseApproval(ByVal GroupID As Integer, ByVal PostID As Integer, ByVal QueueID As Integer, ByVal UserID As Integer) As DataTable
        Dim SqlStr As String = ""
        Dim StatusID As Integer = 1
        If QueueID = 2 Then
            StatusID = 5
        End If
        If PostID = 6 Then
            SqlStr = "select  e.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+h.Item_Name from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j " & _
                    "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID and  a.order_ID=j.order_ID and  a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.SUSP_LEAKAGE=0 and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and g.Group_ID=" + GroupID.ToString() + " group by e.Item_ID ,h.Item_Name" &
                    " union all " & _
                    "select   b.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+c.Item_Name from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and  a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + " and a.SUSP_LEAKAGE=0  and i.Group_ID=" + GroupID.ToString() + " group by  b.Item_ID ,c.Item_Name" & _
                   " order by 1"
        ElseIf PostID = 7 Then
            SqlStr = "select   e.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+h.Item_Name from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j " & _
                    "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID and  a.order_ID=j.order_ID and  a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.fraud=0 and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and  g.Group_ID=" + GroupID.ToString() + " group by e.Item_ID ,h.Item_Name" &
                    " union all " & _
                    "select     b.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+c.Item_Name from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and  a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + " and a.fraud=0  and i.Group_ID=" + GroupID.ToString() + " group by  b.Item_ID ,c.Item_Name" & _
                   " order by 1"
        ElseIf PostID = 9 Then
            SqlStr = "select   e.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+h.Item_Name  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j  " & _
                   "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID  and  a.order_ID=j.order_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and  g.Group_ID=" + GroupID.ToString() + " group by e.Item_ID ,h.Item_Name" &
                   " union all " & _
                   "select    b.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+c.Item_Name from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and  a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + "  and i.Group_ID=" + GroupID.ToString() + " group by  b.Item_ID ,c.Item_Name" & _
                  " order by 1"
        ElseIf PostID = 3 Then
            SqlStr = "select   e.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+h.Item_Name  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,AUDIT_BRANCHES J,center_master i,audit_observation_cycle s  " & _
              "where  a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID and a.order_ID=s.order_ID AND F.BRANCH_ID=J.BRANCH_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id  and a.status_id=" + StatusID.ToString() + " and a.LEVEL_ID=( case when j.EMP_CODE=" + UserID.ToString() + " then 11 else -1 end )   and  g.Group_ID=" + GroupID.ToString() + " group by e.Item_ID ,h.Item_Name" &
              " union all " & _
              "select   e.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+h.Item_Name  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,AUDIT_BRANCHES J,center_master i,audit_observation_cycle s " & _
              "where  a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID and a.order_ID=s.order_ID AND F.BRANCH_ID=J.BRANCH_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id  and a.status_id=" + StatusID.ToString() + " and a.LEVEL_ID=( case when j.Team_lead=" + UserID.ToString() + " then 12 else -1 end )   and g.Group_ID=" + GroupID.ToString() + " group by e.Item_ID ,h.Item_Name" &
              " union all " & _
              "select    b.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+c.Item_Name from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_DTL h,Audit_Master i,AUdit_Branches J,Audit_Observation_Cycle k WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and a.order_ID=k.order_ID and h.group_id=i.group_id and h.branch_Id=j.branch_ID and i.status_id=2 and b.ITEM_ID =c.item_id  and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3  and a.LEVEL_ID=( case when j.EMP_CODE=" + UserID.ToString() + " then 11 else -1 end )  and a.status_id=" + StatusID.ToString() + " and i.Group_ID=" + GroupID.ToString() + " group by  b.Item_ID ,c.Item_Name" & _
               " union all " & _
              "select    b.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+c.Item_Name from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_DTL h,Audit_Master i,AUdit_Branches J,Audit_Observation_Cycle k WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and a.order_ID=k.order_ID and h.group_id=i.group_id and h.branch_Id=j.branch_ID and i.status_id=2 and b.ITEM_ID =c.item_id  and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3  and a.LEVEL_ID=( case when j.Team_Lead=" + UserID.ToString() + " then 12 else -1 end )  and a.status_id=" + StatusID.ToString() + " and i.Group_ID=" + GroupID.ToString() + " group by  b.Item_ID ,c.Item_Name" & _
             " order by 1"
        ElseIf QueueID = 1 Then
            SqlStr = "select   e.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+h.Item_Name  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j  " & _
                   "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.order_ID=j.order_ID  and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and g.Group_ID=" + GroupID.ToString() + " group by e.Item_ID ,h.Item_Name" &
                   " union all " & _
                   "select     b.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+c.Item_Name from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + "  and i.Group_ID=" + GroupID.ToString() + " group by  b.Item_ID ,c.Item_Name" & _
                  " order by 1"
        Else
            SqlStr = "select   e.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+h.Item_Name from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j  " & _
                 "where  a.LEVEL_ID=b.LEVEL_ID  and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID and a.order_ID=j.order_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id  and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and g.Group_ID=" + GroupID.ToString() + " group by e.Item_ID ,h.Item_Name" &
                 " union all " & _
                 "select     b.Item_ID ,CAST(count(a.SINO) as varchar)+' - '+c.Item_Name from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + " and i.Group_ID=" + GroupID.ToString() + " group by  b.Item_ID ,c.Item_Name" & _
                " order by 1"
        End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function
    Public Function GetAuditForResponseApproval(ByVal BranchID As Integer, ByVal PostID As Integer, ByVal QueueID As Integer, ByVal UserID As Integer) As DataTable
        Dim SqlStr As String = ""
        Dim StatusID As Integer = 1
        If QueueID = 2 Then
            StatusID = 5
        End If
        If PostID = 6 Then
            SqlStr = "select distinct * from (select  g.Group_ID ,datename(MONTH,g.period_to) +'-'+CAST(YEAR(g.period_to) AS VARCHAR(4))as Audit from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j " & _
                    "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and g.Status_id=2 and e.Item_ID=h.item_ID and  a.order_ID=j.order_ID and  a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.SUSP_LEAKAGE=0 and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and f.BRANCH_ID=" + BranchID.ToString() & " group by  g.Group_ID,g.Period_To" &
                    " union all " & _
                    "select   i.Group_ID ,datename(MONTH,i.period_to) +'-'+CAST(YEAR(i.period_to) AS VARCHAR(4))as Audit from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and  a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + " and a.SUSP_LEAKAGE=0  and h.BRANCH_ID=" & BranchID.ToString() & " group by  i.Group_ID,i.Period_To" & _
                   ")aaa order by 1"
        ElseIf PostID = 7 Then
            SqlStr = "select distinct * from (select  g.Group_ID ,datename(MONTH,g.period_to) +'-'+CAST(YEAR(g.period_to) AS VARCHAR(4))as Audit from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j " & _
                    "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and g.Status_id=2 and e.Item_ID=h.item_ID and  a.order_ID=j.order_ID and  a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.fraud=0 and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and f.BRANCH_ID=" + BranchID.ToString() & " group by  g.Group_ID,g.Period_To" &
                    " union all " & _
                    "select    i.Group_ID ,datename(MONTH,i.period_to) +'-'+CAST(YEAR(i.period_to) AS VARCHAR(4))as Audit from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and  a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + " and a.fraud=0  and h.BRANCH_ID=" & BranchID.ToString() & " group by   i.Group_ID,i.Period_To" & _
                   ")aaa  order by 1"
        ElseIf PostID = 9 Then
            SqlStr = "select distinct * from (select   g.Group_ID ,datename(MONTH,g.period_to) +'-'+CAST(YEAR(g.period_to) AS VARCHAR(4))as Audit  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j  " & _
                   "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and g.Status_id=2 and e.Item_ID=h.item_ID  and  a.order_ID=j.order_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and f.BRANCH_ID=" + BranchID.ToString() & " group by  g.Group_ID,g.Period_To" &
                   " union all " & _
                   "select    i.Group_ID ,datename(MONTH,i.period_to) +'-'+CAST(YEAR(i.period_to) AS VARCHAR(4))as Audit from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and  a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + "  and h.BRANCH_ID=" & BranchID.ToString() & " group by   i.Group_ID,i.Period_To" & _
                  ")aaa order by 1"
        ElseIf PostID = 3 Then
            SqlStr = "select distinct * from (select   g.Group_ID ,datename(MONTH,g.period_to) +'-'+CAST(YEAR(g.period_to) AS VARCHAR(4)) as Audit  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,AUDIT_BRANCHES J,center_master i,audit_observation_cycle s  " & _
              "where  a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and g.Status_id=2 and e.Item_ID=h.item_ID and a.order_ID=s.order_ID AND F.BRANCH_ID=J.BRANCH_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id  and a.status_id=" + StatusID.ToString() + " and a.LEVEL_ID=( case when j.EMP_CODE=" + UserID.ToString() + " then 11 else -1 end )   and f.BRANCH_ID=" + BranchID.ToString() & " group by  g.Group_ID,g.Period_To" &
              " union all " & _
              "select  g.Group_ID ,datename(MONTH,g.period_to) +'-'+CAST(YEAR(g.period_to) AS VARCHAR(4)) as Audit from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,AUDIT_BRANCHES J,center_master i,audit_observation_cycle s " & _
              "where  a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and g.Status_id=2 and e.Item_ID=h.item_ID and a.order_ID=s.order_ID AND F.BRANCH_ID=J.BRANCH_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id  and a.status_id=" + StatusID.ToString() + " and a.LEVEL_ID=( case when j.Team_lead=" + UserID.ToString() + " then 12 else -1 end )   and f.BRANCH_ID=" + BranchID.ToString() & " group by  g.Group_ID,g.Period_To" &
              " union all " & _
              "select   i.Group_ID ,datename(MONTH,i.period_to) +'-'+CAST(YEAR(i.period_to) AS VARCHAR(4)) as Audit from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_DTL h,Audit_Master i,AUdit_Branches J,Audit_Observation_Cycle k WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and a.order_ID=k.order_ID and h.group_id=i.group_id and h.branch_Id=j.branch_ID and i.status_id=2 and b.ITEM_ID =c.item_id  and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3  and a.LEVEL_ID=( case when j.EMP_CODE=" + UserID.ToString() + " then 11 else -1 end )  and a.status_id=" + StatusID.ToString() + " and h.BRANCH_ID=" & BranchID.ToString() & " group by   i.Group_ID,i.Period_To" & _
               " union all " & _
              "select   i.Group_ID ,datename(MONTH,i.period_to) +'-'+CAST(YEAR(i.period_to) AS VARCHAR(4))as Audit from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_DTL h,Audit_Master i,AUdit_Branches J,Audit_Observation_Cycle k WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and a.order_ID=k.order_ID and h.group_id=i.group_id and h.branch_Id=j.branch_ID and i.status_id=2 and b.ITEM_ID =c.item_id  and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3  and a.LEVEL_ID=( case when j.Team_Lead=" + UserID.ToString() + " then 12 else -1 end )  and a.status_id=" + StatusID.ToString() + " and h.BRANCH_ID=" & BranchID.ToString() & " group by  i.Group_ID,i.Period_To" & _
             " )aaa order by 1"
        ElseIf QueueID = 1 Then
            SqlStr = "select distinct * from (select g.Group_ID ,datename(MONTH,g.period_to) +'-'+CAST(YEAR(g.period_to) AS VARCHAR(4))as Audit  from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b ,AUDIT_ESCALATION_MATRIX_DTL c,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j  " & _
                   "where  a.LEVEL_ID=b.LEVEL_ID and a.LEVEL_ID=c.LEVEL_ID and a.MATRIX_ID=c.MATRIX_ID and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and g.Status_id=2 and e.Item_ID=h.item_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.order_ID=j.order_ID  and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and f.BRANCH_ID=" + BranchID.ToString() & " group by  g.Group_ID,g.Period_To" &
                   " union all " & _
                   "select    i.Group_ID ,datename(MONTH,i.period_to) +'-'+CAST(YEAR(i.period_to) AS VARCHAR(4))as Audit from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_ESCALATION_MATRIX_DTL g,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.LEVEL_ID=g.LEVEL_ID and a.MATRIX_ID=g.MATRIX_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + "  and h.BRANCH_ID=" & BranchID.ToString() & " group by   i.Group_ID,i.Period_To" & _
                  ")aaa order by 1"
        Else
            SqlStr = "select distinct * from (select   g.Group_ID ,datename(MONTH,g.period_to) +'-'+CAST(YEAR(g.period_to) AS VARCHAR(4))as Audit from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j  " & _
                 "where  a.LEVEL_ID=b.LEVEL_ID  and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and g.Status_id=2 and e.Item_ID=h.item_ID and a.order_ID=j.order_ID and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id  and a.status_id=" + StatusID.ToString() + "  and b.POST_ID=" + PostID.ToString() + "  and f.BRANCH_ID=" + BranchID.ToString() & " group by  g.Group_ID,g.Period_To" &
                 " union all " & _
                 "select   i.Group_ID ,datename(MONTH,i.period_to) +'-'+CAST(YEAR(i.period_to) AS VARCHAR(4))as Audit from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and f.POST_ID=" & PostID.ToString() & " and a.status_id=" + StatusID.ToString() + " and h.BRANCH_ID=" & BranchID.ToString() & " group by   i.Group_ID,i.Period_To" & _
                ")aaa order by 1"
        End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function
    'nms ----------------------------Visited Sangams

    Function GetBranchForVisitedReport(ByVal EmpCode As Integer, ByVal PostID As Integer) As DataTable
        If PostID = 2 Then 'Admin
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Admin_Branches b where a.Branch_id=b.Branch_ID and  a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and b.type_id=1 and (b.emp_code=" + EmpCode.ToString() + " ))").Tables(0)
        ElseIf PostID = 3 Then 'Team Lead
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Branches b where a.Branch_id=b.Branch_ID and  a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and (b.Team_lead=" + EmpCode.ToString() + "  or b.emp_code=" + EmpCode.ToString() + " ))").Tables(0)
        ElseIf PostID = 4 Then 'Auditor
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Branches b where a.Branch_id=b.Branch_id and a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and b.emp_code=" + EmpCode.ToString() + " )").Tables(0)
        ElseIf PostID = 22 Then 'Junior Auditor
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Admin_Branches b where a.Branch_id=b.Branch_ID and  a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and b.type_id=0 and (b.emp_code=" + EmpCode.ToString() + " ))").Tables(0)
        Else
            Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName").Tables(0)
        End If
    End Function
    Function GetAuditDetails_Sangams(ByVal BranchID As Integer) As DataTable

        Return DB.ExecuteDataSet("select -1 as AuditID,' ------Select------' as Audit Union All select Group_ID, datename(MONTH,a.period_to) +'-'+CAST(YEAR(a.period_to) AS VARCHAR(4)) from Audit_Master a where  a.Branch_ID=" + BranchID.ToString() + " and a.group_id in  (select distinct group_id from audit_dtl b where a.group_id=b.group_id and b.status_id in (1))").Tables(0)

    End Function
    Function GetSangamDetails(ByVal GroupID As Integer) As DataTable
        Return DB.ExecuteDataSet("select distinct b.CENTER_ID,c.CENTER_NAME,DATENAME(DW,c.MEETING_DAY-2)as MeetingDay,S.EMP_CODE,s.emp_name from AUDIT_ACCOUNTS a,AUDIT_DTL E, " & _
            " AUDIT_MASTER D ,  LOAN_MASTER b,CENTER_MASTER  c LEFT JOIN (SELECT GROUP_ID,J.EMP_NAME + '/'+ CONVERT(VARCHAR(20),G.EMP_CODE) + ' - '+ convert(varchar,g.visited_dt) AS EMP_name,g.emp_code FROM  AUDIT_VISITED_SANGAM_MASTER F  LEFT JOIN AUDIT_VISITED_SANGAM_DTL G ON  " & _
            " F.VISITED_ID=G.VISITED_ID LEFT JOIN EMP_MASTER J ON G.EMP_CODE=J.EMP_CODE  where  AUDIT_GROUP_ID=" & GroupID.ToString() & ")s  ON C.CENTER_ID=S.GROUP_ID WHERE " & _
            " A.AUDIT_ID=E.AUDIT_ID AND E.GROUP_ID=D.GROUP_ID AND a.LOAN_NO=b.LOAN_NO and b.CENTER_ID=c.CENTER_ID and e.audit_type=4 and d.GROUP_ID=" & GroupID.ToString() & " order by 2").Tables(0)
    End Function
    Public Function GET_AUDIT_ADMIN_EMP_DEATILS(ByVal EMPID As Integer) As DataTable
        Return DB.ExecuteDataSet("Select CONVERT(VARCHAR,a.emp_code)+'^'+a.emp_name+'^'+CONVERT(VARCHAR,a.Branch_ID)+'^'+a.Branch_name+'^'+CONVERT(VARCHAR,a.Department_ID) +'^'+ a.Department_name+'^'+a.Designation_name+'^'+CONVERT(VARCHAR,a.Designation_ID) from Emp_List a,AUDIT_EMP_MASTER b where a.Emp_Code=b.EMP_CODE  and b.EMp_Code=" + EMPID.ToString() + " and post_id=2 and b.status_ID=1").Tables(0)
    End Function
    Public Function Getexist_branch_AuditAdmin(ByVal empcode As Integer) As DataTable
        Return DB.ExecuteDataSet("select B.branch_id,d.Region_Name, c.Area_Name , b.branch_name ,isnull(a.EMP_CODE,0) as emp_code," & empcode.ToString() & "," & _
                                 " e.Emp_Name from branch_master b LEFT JOIN (select branch_id,emp_code from AUDIT_ADMIN_BRANCHES where TYPE_ID=1) a ON a.branch_id=b.branch_id " & _
                                 " LEFT JOIN emp_master e ON  a.EMP_CODE =e.Emp_Code ,AREA_MASTER c,REGION_MASTER d " & _
                                 " where b.Area_ID = c.Area_ID and c.Region_ID=d.Region_ID  and b.status_id=1  order by d.Region_Name, c.Area_Name , b.branch_name").Tables(0)
    End Function
    Public Function GetStr_branch_AuditAdmin(ByRef DT As DataTable) As String
        Dim StrAttendance As String = ""
        Dim val As Integer

        For n As Integer = 0 To DT.Rows.Count - 1
            If CInt(DT.Rows(n)(4)) = CInt(DT.Rows(n)(5)) Then
                val = 1
            Else
                val = 0
            End If
            StrAttendance += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & val & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6) & "µ22"
            If n < DT.Rows.Count - 1 Then
                StrAttendance += "¥"
            End If
        Next
        Return StrAttendance
    End Function
    '------------------JUNIOR AUDITOR
    Public Function GET_JUNIOR_AUDIT_EMP_DEATILS(ByVal EMPID As Integer) As DataTable
        Return DB.ExecuteDataSet("Select CONVERT(VARCHAR,a.emp_code)+'^'+a.emp_name+'^'+CONVERT(VARCHAR,a.Branch_ID)+'^'+a.Branch_name+'^'+CONVERT(VARCHAR,a.Department_ID) +'^'+ a.Department_name+'^'+a.Designation_name+'^'+CONVERT(VARCHAR,a.Designation_ID) from Emp_List a,AUDIT_EMP_MASTER b where a.Emp_Code=b.EMP_CODE  and b.EMp_Code=" + EMPID.ToString() + " and post_id=22 and b.status_ID=1").Tables(0)
    End Function
    Public Function Getexist_branch_JuniorAudit(ByVal empcode As Integer) As DataTable
        Return DB.ExecuteDataSet("select B.branch_id,d.Region_Name, c.Area_Name , b.branch_name ,isnull(a.EMP_CODE,0) as emp_code," & empcode.ToString() & "," & _
                                 " e.Emp_Name from branch_master b LEFT JOIN (select branch_id,emp_code from AUDIT_ADMIN_BRANCHES where TYPE_ID=0) a ON a.branch_id=b.branch_id " & _
                                 " LEFT JOIN emp_master e ON  a.EMP_CODE =e.Emp_Code ,AREA_MASTER c,REGION_MASTER d " & _
                                 " where b.Area_ID = c.Area_ID and c.Region_ID=d.Region_ID  and b.status_id=1  order by d.Region_Name, c.Area_Name , b.branch_name").Tables(0)
    End Function
    '-------------------------------
    '------------------------------- Misleading
    Function GetObservationConfirmationDetail(ByVal Month As Integer, ByVal Year As Integer, ByVal RegionID As Integer, ByVal AreaID As Integer, ByVal BranchID As Integer, ByVal TypeID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime, ByVal DetailFlag As Integer, ByVal StatusID As Integer, ByVal UserID As Integer) As DataTable
        Try
            Dim Params(10) As SqlParameter
            Params(0) = New SqlParameter("@Month", SqlDbType.Int)
            Params(0).Value = Month
            Params(1) = New SqlParameter("@Year", SqlDbType.Int)
            Params(1).Value = Year
            Params(2) = New SqlParameter("@RegionID", SqlDbType.Int)
            Params(2).Value = RegionID
            Params(3) = New SqlParameter("@AreaID", SqlDbType.Int)
            Params(3).Value = AreaID
            Params(4) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(4).Value = BranchID
            Params(5) = New SqlParameter("@TypeID", SqlDbType.Int)
            Params(5).Value = TypeID
            Params(6) = New SqlParameter("@StatusID", SqlDbType.Int)
            Params(6).Value = StatusID
            Params(7) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(7).Value = UserID
            Params(8) = New SqlParameter("@FromDt", SqlDbType.DateTime)
            Params(8).Value = FromDt
            Params(9) = New SqlParameter("@ToDt", SqlDbType.DateTime)
            Params(9).Value = ToDt
            Params(10) = New SqlParameter("@DetailFlag", SqlDbType.Int)
            Params(10).Value = DetailFlag


            Return DB.ExecuteDataSet("SP_GET_AUDIT_OBS_CONFIRMATION_DETAIL", Params).Tables(0)

        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Function GetMaxLevelID(ByVal QueueID As Integer) As Integer 'nnnnnnnnnnnnnnnnn
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("select MAX(LEVEL_ID) from AUDIT_USER_LEVELS  where QUEUE_ID=" + QueueID.ToString() + " and status_id=1").Tables(0)
        If DT.Rows.Count > 0 Then
            Return CInt(DT.Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Function GetMaxPostID(ByVal QueueID As Integer) As Integer
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("select post_ID from AUDIT_USER_LEVELS where level_id=(select MAX(LEVEL_ID) from AUDIT_USER_LEVELS  where QUEUE_ID=" + QueueID.ToString() + "  and status_id=1)").Tables(0)
        If DT.Rows.Count > 0 Then
            Return CInt(DT.Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function GET_AUDIT_EMP_DEATILS(ByVal EMPID As Integer) As DataTable
        Return DB.ExecuteDataSet("Select CONVERT(VARCHAR,a.emp_code)+'^'+a.emp_name+'^'+CONVERT(VARCHAR,a.Branch_ID)+'^'+a.Branch_name+'^'+CONVERT(VARCHAR,a.Department_ID) +'^'+ a.Department_name+'^'+a.Designation_name+'^'+CONVERT(VARCHAR,a.Designation_ID) from Emp_List a,AUDIT_EMP_MASTER b where a.Emp_Code=b.EMP_CODE  and b.EMp_Code=" + EMPID.ToString() + " and b.status_ID=1 and a.post_id=4").Tables(0)
    End Function
    Function GetStaffInvolvementCases(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal SINo As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As DataTable
        Dim Sqlstr As String = ""
        Dim Sqlstr1 As String = ""
        Dim SqlSubstr As String = ""
        If PostID = 8 Then
            Sqlstr1 += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr1 += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr1 += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr1 += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr1 += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr1 += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr1 += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr1 += " and e.status_id=9"
        End If
        If SINo > 0 Then
            Sqlstr1 += " and e.Emp_Code in(select Emp_Code from Audit_Staff_Involvement where SINO=" + SINo.ToString() + ")"
        End If

        If (AuditMonth > 0) Then
            Sqlstr = "select  g.item_name,count(distinct a.SINO)as Observations,COUNT(distinct  e.Emp_Code)as NoofStaffs,sum(a.Fin_Leakage) as Est_Amt_Involved," & _
                " COUNT(distinct d.GROUP_ID )as Audits " & _
                " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g" & _
                "  where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID " & _
                " and b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  " & _
                " and a.STAFF_INVOLVEMENT=1   and d.status_id=2   and month(d.period_to)=" + AuditMonth.ToString() + " " & _
                " and year(d.period_to)=" + AuditYear.ToString() + " " + Sqlstr1 + " "
        Else
            Sqlstr = "select  g.item_name,count(distinct a.SINO)as Observations,COUNT(distinct  e.Emp_Code)as NoofStaffs,sum(a.Fin_Leakage) as Est_Amt_Involved," & _
                " COUNT(distinct d.GROUP_ID )as Audits" & _
                " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g " & _
                "  where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'  and a.OBSERVATION_ID=b.OBSERVATION_ID and " & _
                " b.AUDIT_ID=c.AUDIT_ID and b.item_id=g.Item_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 " & _
                "  and d.status_id=2   " + Sqlstr1 + " "
        End If

        Sqlstr += "  group by g.item_name order by  g.item_name"

        Return DB.ExecuteDataSet(Sqlstr).Tables(0)
    End Function
    Function GetTotalCasesInvolved(ByVal AuditMonth As Integer, ByVal AuditYear As Integer, ByVal PostID As Integer, ByVal UserID As Integer, ByVal BranchID As Integer, ByVal AreaID As Integer, ByVal RegionID As Integer, ByVal ItemID As Integer, ByVal EmpCode As Integer, ByVal StatusID As Integer, ByVal FromDt As DateTime, ByVal ToDt As DateTime) As Integer
        Dim Sqlstr As String = ""
        If (AuditMonth > 0) Then
            Sqlstr = "select COUNT(distinct g.item_name) as NoOfStaff from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID  and d.status_id=2 and month(d.period_to)=" + AuditMonth.ToString() + " and year(d.period_to)=" + AuditYear.ToString() + "  "
        Else
            Sqlstr = "select COUNT(distinct  g.item_name) as NoOfStaff from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_STAFF_INVOLVEMENT e,BrMaster f,AUDIT_CHECK_LIST g where DATEADD(day, DATEDIFF(day, 0, e.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and a.SINO=e.SINO and d.BRANCH_ID=f.Branch_ID  and a.STAFF_INVOLVEMENT=1 and b.item_id=g.Item_ID and d.status_id=2 "
        End If
        If PostID = 8 Then
            Sqlstr += " and d.Branch_ID in(Select Branch_ID from brmaster where zone_head=" + UserID.ToString() + ")"
        End If
        If BranchID > 0 Then
            Sqlstr += " and f.Branch_ID=" + BranchID.ToString()
        ElseIf AreaID > 0 Then
            Sqlstr += " and f.area_id=" + AreaID.ToString()
        ElseIf RegionID > 0 Then
            Sqlstr += " and f.region_ID=" + RegionID.ToString()
        End If
        If ItemID > 0 Then
            Sqlstr += " and g.item_id=" + ItemID.ToString()
        End If
        If StatusID = 0 Then
            Sqlstr += " and e.status_id=0"
        ElseIf StatusID = 1 Then
            Sqlstr += " and e.status_id=1"
        ElseIf StatusID = 9 Then
            Sqlstr += " and e.status_id=9"
        End If
        If EmpCode > 0 Then
            Sqlstr += " and a.SINo in(select SINO from Audit_Staff_Involvement where Emp_Code=" + EmpCode.ToString() + ")"
        End If

        Dim dt As New DataTable
        dt = DB.ExecuteDataSet(Sqlstr).Tables(0)
        Dim TotalCases As Integer = CInt(dt.Rows(0)(0))
        Return TotalCases
    End Function
    Function GetStaffDetails(ByVal PostID As Integer, ByVal UserID As Integer) As DataTable
        Dim DT As New DataTable
        If PostID = 5 Then
            DT = DB.ExecuteDataSet("select -1 EMP_CODE,'---------SELECT---------' EMP_NAME union all select distinct a.EMP_CODE,b.Emp_Name + '-' + CAST(a.EMP_CODE as varchar) as Emp_name from AUDIT_STAFF_INVOLVEMENT a,EMP_MASTER b,BrMaster c where a.Status_ID = 1 and a.Amount-isnull(a.Refund_Amount,0) > 0 and a.EMP_CODE = b.Emp_Code and b.Branch_ID = c.Branch_ID  and c.Branch_Head = " & UserID & " order by 2").Tables(0)
        ElseIf PostID = 6 Then
            DT = DB.ExecuteDataSet("select -1 EMP_CODE,'---------SELECT---------' EMP_NAME union all select distinct a.EMP_CODE,b.Emp_Name + '-' + CAST(a.EMP_CODE as varchar) as Emp_name from AUDIT_STAFF_INVOLVEMENT a,EMP_MASTER b,BrMaster c where a.Status_ID = 1 and a.Amount-isnull(a.Refund_Amount,0) > 0 and a.EMP_CODE = b.Emp_Code and b.Branch_ID = c.Branch_ID  and c.Area_Head = " & UserID & " order by 2").Tables(0)
        ElseIf PostID = 7 Then
            DT = DB.ExecuteDataSet("select -1 EMP_CODE,'---------SELECT---------' EMP_NAME union all select distinct a.EMP_CODE,b.Emp_Name + '-' + CAST(a.EMP_CODE as varchar) as Emp_name from AUDIT_STAFF_INVOLVEMENT a,EMP_MASTER b,BrMaster c where a.Status_ID = 1 and a.Amount-isnull(a.Refund_Amount,0) > 0 and a.EMP_CODE = b.Emp_Code and b.Branch_ID = c.Branch_ID  and c.Region_Head = " & UserID & " order by 2").Tables(0)
        Else
            DT = DB.ExecuteDataSet("select -1 EMP_CODE,'---------SELECT---------' EMP_NAME union all select distinct a.EMP_CODE,b.Emp_Name + '-' + CAST(a.EMP_CODE as varchar) as Emp_name from AUDIT_STAFF_INVOLVEMENT a,EMP_MASTER b,BrMaster c where a.Status_ID = 1 and a.Amount-isnull(a.Refund_Amount,0) > 0 and a.EMP_CODE = b.Emp_Code and b.Branch_ID = c.Branch_ID  order by 2").Tables(0)
        End If
        Return DT
    End Function
   
End Class
