﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Net.NetworkInformation
Public Class SMTP
    Dim DB As New MS_SQL.Connect
   

    Public Function GetEmailAddress(ByVal UserID As Integer, ByVal TypeID As Integer) As String

        Dim EmailAddress As String = ""
        Dim DT As DataTable
        If TypeID = 1 Then
            DT = DB.ExecuteDataSet("select case when Official_mail_id is not null then Official_mail_id  else Email end,emp_name   from emp_profile a,emp_master b where a.Emp_Code=b.Emp_Code and a.emp_code=" & UserID.ToString & "").Tables(0)
        ElseIf TypeID = 2 Then
            DT = DB.ExecuteDataSet("select distinct case when Official_mail_id is not null then Official_mail_id  else Email end,a.emp_name  from emp_master a,emp_profile b where a.Emp_Code =b.Emp_Code and b.emp_code in(select Reporting_To  from EMP_MASTER where Emp_Code=" & UserID.ToString & " )").Tables(0)
        Else
            DT = DB.ExecuteDataSet("").Tables(0)
        End If

        If DT.Rows.Count > 0 Then
            EmailAddress = DT.Rows(0)(0).ToString() + "^" + DT.Rows(0)(1).ToString()
        End If
        Return EmailAddress
    End Function


    Public Sub SendMail(ByVal ToAddress__1 As String, ByVal Subject__2 As String, ByVal content As String, ByVal ccAddress As String)
        ' Gmail Address from where you send the mail
        'Dim fromAddress = "softwareinfo@esafmicrofin.com"


        '' any address where the email will be sending
        'Dim toAddress__3 = ToAddress__1
        ''Password of your gmail address
        'Const fromPassword As String = "Emf1l@T$r"
        '' Passing the values and make a email formate to display
        'Dim subject__4 As String = Subject__2
        'Dim body As String = "From: " + "Software" + vbLf
        'body += "Email: " + fromAddress + vbLf
        'body += (Convert.ToString("Subject: ") & subject__4) + vbLf
        'body += (Convert.ToString(vbLf) & content) + " " & vbLf
        'body += "This is a System Generated Mail. Please Do not Reply"
        '' smtp settings
        'Dim smtp = New System.Net.Mail.SmtpClient()
        'If True Then
        '    smtp.Host = "emfil-org-smtp.mail.na.collabserv.com"
        '    'smtp.Port = 465
        '    smtp.EnableSsl = True
        '    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network
        '    smtp.Credentials = New System.Net.NetworkCredential(fromAddress, fromPassword)
        '    smtp.Timeout = 20000
        'End If
        '' Passing values to smtp object
        'Dim MyMessage As New System.Net.Mail.MailMessage(fromAddress, toAddress__3, subject__4, body)
        'If ccAddress <> "" Then
        '    MyMessage.CC.Add(ccAddress)
        'End If
        'smtp.Send(MyMessage)
    End Sub
    Public Sub SendMailBox(ByVal ToAddress__1 As String, ByVal Subject__2 As String, ByVal content As String, ByVal ccAddress As String)
        ' Gmail Address from where you send the mail
        Dim fromAddress = "softwareinfo@esafmicrofin.com"


        ' any address where the email will be sending
        Dim toAddress__3 = ToAddress__1
        'Password of your gmail address
        Const fromPassword As String = "Emf1l@T$r"
        ' Passing the values and make a email formate to display
        Dim subject__4 As String = Subject__2
        Dim body As String = "From: " + "Software" + vbLf
        body += "Email: " + fromAddress + vbLf
        body += (Convert.ToString("Subject: ") & subject__4) + vbLf
        body += (Convert.ToString(vbLf) & content) + " " & vbLf
        body += "This is a System Generated Mail. Please Do not Reply"
        ' smtp settings
        Dim smtp = New System.Net.Mail.SmtpClient()
        If True Then
            smtp.Host = "emfil-org-smtp.mail.na.collabserv.com"
            'smtp.Port = 465
            smtp.EnableSsl = True
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network
            smtp.Credentials = New System.Net.NetworkCredential(fromAddress, fromPassword)
            smtp.Timeout = 20000
        End If
        ' Passing values to smtp object
        Dim MyMessage As New System.Net.Mail.MailMessage(fromAddress, toAddress__3, subject__4, body)
        If ccAddress <> "" Then
            MyMessage.CC.Add(ccAddress)
        End If
        smtp.Send(MyMessage)
    End Sub
End Class
