﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient

Public Class HelpDesk
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Function GetBranches() As DataTable
        Return GN.GetQueryResult(7, 0, 0)
    End Function
    Public Function GetDepartment() As DataTable
        Return DB.ExecuteDataSet("select -1 as Department_id,' -----Select-----' as Department_Name union all select Department_id,Department_Name from Department_MASTER where status_id=1  order by 2").Tables(0)
    End Function

    Public Function GetGroup() As DataTable
        Return DB.ExecuteDataSet("select -1 as Group_id,' -----Select-----' as Group_Name union all select Group_id,Group_Name from HD_GROUP_MASTER where status_id=1  order by 2").Tables(0)
    End Function
    Public Function GetSubGroup(ByVal GroupID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as Sub_Group_id,' -----Select-----' as Sub_Group_Name union all select Sub_Group_id,Sub_Group_Name from HD_SUB_GROUP where group_id=" & GroupID & " and status_id=1  order by 2").Tables(0)
    End Function
    Public Function GetPROBLEM(ByVal SubGroupID As Integer) As DataTable
        Return DB.ExecuteDataSet("select '-1' as PROBLEM_ID,' -----Select-----' as PROBLEM union all select cast(PROBLEM_ID as varchar(8))+'~'+cast(mode_id as varchar(3))+'~'+CAST(ASSET_RELATED as varchar(2))+'~'+CAST(IS_CRITICAL as varchar(2))+'~'+isnull(PDESCRIPTION,'')+'~'+CAST(ANNEXTURE as varchar(2)),PROBLEM from HD_PROBLEM_TYPE where Sub_Group_id=" & SubGroupID & " and status_id=1  order by 2").Tables(0)
    End Function
    Public Function GetClassification() As DataTable
        Return DB.ExecuteDataSet("select -1 as Type_ID,' -----Select-----' as Classification_Type  union all select Type_ID,Classification_Type from HD_CLASSIFICATION where  status_id=1  order by 2").Tables(0)
    End Function
    Public Function GetTeam() As DataTable
        Return DB.ExecuteDataSet("select -1 as Team_ID,' -----Select-----' as Team_name  union all select Team_ID,Team_name from HD_TEAM_MASTER where  status_id=1  order by 2").Tables(0)
    End Function
    Public Function GetDirectTeam() As DataTable
        Return DB.ExecuteDataSet("select -1 as team_id , '----Select-----' as team_name UNION select team_id, team_name from hd_team_master where status_id = 1  order by team_name").Tables(0)
    End Function

    Public Function GetRequestDetails(ByVal RequestID As Integer) As DataTable
        Dim strval As String
        Return DB.ExecuteDataSet("select cast(a.REQUEST_ID as varchar)+'Ø'+cast(a.BRANCH_ID as varchar)+'Ø'+cast(b.Branch_Name as varchar)+'Ø'+cast(a.CLASSIFICATION_ID as varchar) " & _
            " +'Ø'+cast(a.DEPARTMENT_ID as varchar)+'Ø'+cast(e.Department_Name as varchar) +'Ø'+a.REMARKS +'Ø'+cast(a.TEAM_ID as varchar) " & _
            " +'Ø'+convert(varchar,a.REQUEST_DT ,106) +'Ø'+cast(c.PROBLEM_ID as varchar)+'Ø'+cast(c.SUB_GROUP_ID as varchar) +'Ø'+cast(d.Group_ID as varchar)+'Ø'+cast(a.STATUS_ID as varchar) " & _
            " +'Ø'+cast(isnull(a.VENDOR_ID,0) as varchar )+'Ø'+ cast(isnull(t.ticket_type,0) as varchar) +'Ø'+ cast(isnull(t.item_id,0) as varchar) +'Ø'+cast(isnull( t.requested_count,0) as varchar) " & _
            " +'Ø'+cast(isnull( t.asset_id,0) as varchar) +'Ø'+ cast(isnull(t.asset_tag,0) as varchar) +'Ø'+ cast(isnull(a.noof_attachments,0) as varchar)+'Ø'+CAST(c.IS_CRITICAL as varchar(2))+'Ø'+isnull(c.PDESCRIPTION,'') +'Ø'+CAST(c.ANNEXTURE as varchar(2))+'Ø'+ convert(varchar,isnull(a.correction_count,1)) as Req_id " & _
            " from HD_REQUEST_MASTER a left outer join HD_ASSET_TICKET t on a.request_id=t.request_id ,BRANCH_MASTER b, " & _
            " HD_PROBLEM_TYPE c,HD_SUB_GROUP  d,DEPARTMENT_MASTER  e  " & _
            " where a.BRANCH_ID=b.Branch_ID and a.PROBLEM_ID=c.PROBLEM_ID and c.SUB_GROUP_ID =d.Sub_Group_ID and a.DEPARTMENT_ID=e.Department_ID and " & _
            " a.REQUEST_ID=  " & RequestID.ToString() & "").Tables(0)

    End Function
    Public Function GetBranch(ByVal BranchID As Integer) As DataTable
        Return DB.ExecuteDataSet("select cast(Branch_id as varchar(5))+'Ø'+cast(status_ID as varchar(2)) from BRANCH_MASTER where  branch_id =" & BranchID & "").Tables(0)
    End Function
    Public Function GetPreviousRemarks(ByVal Request_ID As Integer) As DataTable
        Return DB.ExecuteDataSet("select b.ORDER_ID,b.REMARKS,case when c.TEAM_ID IS null then 'Branch/Department' else c.TEAM_NAME end as team, " & _
                        " d.PROBLEM, e.Sub_Group_Name, f.Group_Name,convert(varchar,b.TRA_DT,106)  from hd_request_cycle b LEFT JOIN HD_TEAM_MASTER c ON  b.TEAM_ID=c.TEAM_ID, hd_request_master a,  " & _
                        " HD_PROBLEM_TYPE d,HD_SUB_GROUP e ,HD_GROUP_MASTER f where a.REQUEST_ID=b.REQUEST_ID  and a.PROBLEM_ID=d.PROBLEM_ID  and d.SUB_GROUP_ID=e.Sub_Group_ID  " & _
                        " and e.Group_ID=f.Group_ID  and a.REQUEST_ID=" & Request_ID & " ORDER BY b.ORDER_ID").Tables(0)
    End Function
    Public Function GetString(ByRef DT As DataTable) As String
        Dim StrForm As String = ""

        For n As Integer = 0 To DT.Rows.Count - 1
            StrForm += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6)
            If n < DT.Rows.Count - 1 Then
                StrForm += "¥"
            End If
        Next
        Return StrForm
    End Function
    Public Function GetTEAM(ByVal UserID As Integer) As DataTable
        Return DB.ExecuteDataSet("select distinct a.team_id from HD_team_MASTER a,HD_team_dtl b where a.team_id=b.team_id and b.emp_code =" & UserID & "").Tables(0)
    End Function

    Public Function GetRequest(ByVal BranchID As Integer, ByVal UserID As Integer) As DataTable
        Dim strval As String

        If BranchID = 0 Then
            strval = "and (b.branch_id=" & BranchID & " and b.department_id  in(select department_id from department_master where department_head=" & UserID.ToString() & ") or b.user_id=" & UserID.ToString() & ")"
        Else
            strval = "and (b.branch_id=" & BranchID & " or b.user_id=" & UserID.ToString() & ")"
        End If
        Return DB.ExecuteDataSet("select '-1' as REQUEST_ID,' ------Select------' as request Union all " & _
                                 " Select cast(b.REQUEST_ID as varchar(6))+'¥'+b.REMARKS+'¥'+cast(b.PROBLEM_ID as varchar(6))+'¥'+s.status_name +'¥'+cast(s.status_id  as varchar(6))as REQUEST_ID,s.status_name + '- ' +b.TICKET_NO " & _
                                 " +'/'+a.PROBLEM as request from HD_PROBLEM_TYPE a,HD_REQUEST_MASTER b,hd_status s  where a.PROBLEM_ID=b.PROBLEM_ID and " & _
                                 " b.status_id=s.status_id and (b.STATUS_ID=4 or b.status_id=12) " & _
                                 " " & strval & "").Tables(0)
    End Function

    Public Function GetGroupName() As String
        Dim DT As New DataTable
        Dim StrGrpNam As String = ""
        DT = DB.ExecuteDataSet("select Group_Name,Group_ID from HD_GROUP_MASTER where Status_ID=1").Tables(0)
        For n As Integer = 0 To DT.Rows.Count - 1
            StrGrpNam += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1)

            If n < DT.Rows.Count - 1 Then
                StrGrpNam += "¥"
            End If
        Next
        Return StrGrpNam
    End Function
    Public Function GetCategory() As DataTable
        Return DB.ExecuteDataSet("select '-1' as  Group_ID ,' ------Select------' as Group_Name Union all Select Group_ID,Group_Name from HD_GROUP_MASTER where Status_ID=1").Tables(0)
    End Function
    Public Function GetModuleName(ByVal GroupID As Integer) As String
        Dim DT As New DataTable
        Dim StrGrpNam As String = ""
        DT = DB.ExecuteDataSet("select Sub_Group_Name,Sub_Group_ID from HD_SUB_GROUP where Group_ID=" & GroupID & " and Status_id=1").Tables(0)
        For n As Integer = 0 To DT.Rows.Count - 1
            StrGrpNam += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1)
            If n < DT.Rows.Count - 1 Then
                StrGrpNam += "¥"
            End If
        Next
        Return StrGrpNam
    End Function
    Public Function GetModule(ByVal GroupID As Integer) As DataTable
        Return DB.ExecuteDataSet("select '-1' as  Sub_Group_ID ,' ------Select------' as Sub_Group_Name Union all Select Sub_Group_ID,Sub_Group_Name from HD_SUB_GROUP where Group_ID=" & GroupID & " and Status_ID=1").Tables(0)
    End Function

    Public Function GetProblemName(ByVal SubGroupID As Integer) As String
        Dim DT As New DataTable
        Dim StrGrpNam As String = ""
        DT = DB.ExecuteDataSet("select PROBLEM + 'ÿ' + convert(varchar, MODE_ID) + 'ÿ' + convert(varchar,IS_CRITICAL) + 'ÿ' + isnull(PDESCRIPTION,'') + 'ÿ' + CONVERT(varchar,ANNEXTURE)  + 'ÿ' + CONVERT(varchar,Direct_Team)  + 'ÿ' + CONVERT(varchar,isnull(Root_Cause,-1))  + 'ÿ' + CONVERT(varchar,isnull(Severity,-1))  + 'ÿ' + CONVERT(varchar,isnull(Resp_TAT_Type,-1)) + 'ÿ' + CONVERT(varchar,isnull(Response_TAT,''))  + 'ÿ' + CONVERT(varchar,isnull(Resol_TAT_Type,-1))  + 'ÿ' + CONVERT(varchar,isnull(Resolution_TAT,'')) , PROBLEM_ID from HD_PROBLEM_TYPE where Sub_Group_ID=" & SubGroupID & " and Status_id=1").Tables(0)
        For n As Integer = 0 To DT.Rows.Count - 1
            StrGrpNam += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1)
            If n < DT.Rows.Count - 1 Then
                StrGrpNam += "¥"
            End If
        Next
        Return StrGrpNam
    End Function
    Public Function DeleteGroup(ByVal Tabs As Integer, ByVal GroupId As Integer) As Integer
        If Tabs = 1 Then
            Return DB.ExecuteNonQuery("UPDATE HD_GROUP_MASTER SET Status_ID=0 where Group_ID=" & GroupId & "").ToString
        ElseIf Tabs = 2 Then
            Return DB.ExecuteNonQuery("UPDATE HD_SUB_GROUP SET Status_ID=0 where Sub_Group_ID=" & GroupId & "").ToString
        Else
            Return DB.ExecuteNonQuery("UPDATE HD_PROBLEM_TYPE SET Status_ID=0 where PROBLEM_ID=" & GroupId & "").ToString
        End If
    End Function
    Public Function GetProblems(ByVal SubGroupID As Integer) As DataTable
        Return DB.ExecuteDataSet("Select PROBLEM_ID,PROBLEM from HD_PROBLEM_TYPE where SUB_GROUP_ID=" & SubGroupID & " and Status_ID=1").Tables(0)
    End Function
    Public Function CheckBranchDep(ByVal EmpID As Integer) As Boolean
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("Select branch_head from Branch_master where branch_head=" & EmpID.ToString & " UNION ALL  Select department_head from Department_master where department_head=" & EmpID.ToString & "").Tables(0)
        If DT.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function GetRequestexits(ByVal SubGroupID As Integer) As DataTable
        Return DB.ExecuteDataSet("Select REQUEST_ID from HD_REQUEST_MASTER where PROBLEM_ID=" & SubGroupID & " ").Tables(0)
    End Function
    Public Function GetStatus(ByVal Firstlevel As Integer) As DataTable
        If Firstlevel = 1 Then
            Return DB.ExecuteDataSet("select '-1' as  STATUS_ID ,' -----Select-----' as STATUS_NAME Union all select STATUS_ID ,(case when status_id=6 then 'SEND TO APPROVAL' else STATUS_NAME end) as Status_Name from HD_STATUS WHERE STATUS_LEVEL='G'  or STATUS_LEVEL='T' or STATUS_LEVEL='F' ").Tables(0)
        ElseIf Firstlevel = 2 Then
            Return DB.ExecuteDataSet("select '-1' as  STATUS_ID ,' -----Select-----' as STATUS_NAME Union all select STATUS_ID ,STATUS_NAME from HD_STATUS WHERE STATUS_LEVEL='S'  or STATUS_LEVEL='T' ").Tables(0)
        ElseIf Firstlevel = 3 Then
            Return DB.ExecuteDataSet("select '-1' as  STATUS_ID ,' -----Select-----' as STATUS_NAME Union all select STATUS_ID ,STATUS_NAME from HD_STATUS WHERE STATUS_LEVEL='F'  or STATUS_LEVEL='T' ").Tables(0)

        End If
    End Function
    Public Function EditMasterData(ByVal Tab As Integer, ByVal Id As Integer) As String 'shima 17/10
        Dim DT As New DataTable
        Dim StrPost As String = ""
        If Tab = 3 Then
            DT = DB.ExecuteDataSet("select MODE_ID from HD_PROBLEM_TYPE where PROBLEM_ID=" & Id & "").Tables(0)
            If DT.Rows.Count > 0 Then
                StrPost = DT.Rows(0)(0)
                If DT.Rows(0)(0) = 0 Then
                    StrPost += "¥0$0$0"
                Else
                    DT = DB.ExecuteDataSet("select POST_ID from HD_PRO_APPROVAL where PROBLEM_ID=" & Id & "").Tables(0)
                    StrPost += "¥"
                    For Each DR As DataRow In DT.Rows
                        StrPost += DR(0).ToString() + "$"
                    Next
                End If
            End If
        End If
        Return StrPost
    End Function
    Public Function GetTickets() As String
        Dim DT As New DataTable
        Dim StrTicket As String = ""
        DT = DB.ExecuteDataSet("select '-1' as  REQUEST_ID ,' ------Select------' as TICKET_NO Union all select REQUEST_ID,TICKET_NO from  HD_REQUEST_MASTER ").Tables(0)
        For n As Integer = 0 To DT.Rows.Count - 1
            StrTicket += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1)
            If n < DT.Rows.Count - 1 Then
                StrTicket += "¥"
            End If
        Next
        Return StrTicket
    End Function
    Public Function GetRequestForAssigning(ByVal TeamID As Integer) As DataTable
        Return DB.ExecuteDataSet("select mm.REQUEST_ID,mm.TICKET_NO,mm.REMARKS,mm.PROBLEM,mm.Group_Name,mm.Sub_Group_Name,mm.AttachFlag,isnull(n.Emp_name,'NA')as AssinedTo,mm.status_id,mm.URGENT_REQ,mm.Branch_Name,CONVERT(varchar,mm.request_dt,103) as request_dt from (select a.REQUEST_ID,a.TICKET_NO,a.REMARKS,c.PROBLEM,d.group_Name,e.Sub_Group_Name,ISNULL(f.cnt,0)as AttachFlag,a.ASSIGNED_TO,a.status_id,a.URGENT_REQ,br.Branch_Name,a.REQUEST_DT  from HD_REQUEST_MASTER a left outer join (select request_id,count(*)as cnt from  DMS_ESFB.dbo.HD_REQUEST_ATTACH group by request_id)f on(a.REQUEST_ID=f.REQUEST_ID),HD_TEAM_MASTER b,HD_PROBLEM_TYPE c,HD_GROUP_MASTER d,HD_SUB_GROUP e,BRANCH_MASTER br where a.TEAM_ID=b.TEAM_ID and a.PROBLEM_ID=c.PROBLEM_ID and c.sub_Group_Id=e.sub_Group_ID and e.group_ID=d.group_ID and a.BRANCH_ID=br.Branch_ID and a.STATUS_ID in(9,2,3) and a.Team_ID=" + TeamID.ToString() + ") mm left outer join EMP_MASTER n on(mm.ASSIGNED_TO=n.Emp_Code) order by mm.Urgent_REq desc,mm.Request_ID asc").Tables(0)
    End Function
    Public Function GetTeamMembers(ByVal UserID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as EmpCode,' -----Select-----' as EmpName union all select a.emp_code,c.emp_name from hd_team_dtl a, HD_TEAM_MASTER b,Emp_master c where a.TEAM_ID=b.TEAM_ID and a.Emp_code=c.emp_code and c.status_id=9  and (b.APP_USER1=" + UserID.ToString() + " or b.APP_USER2=" + UserID.ToString() + ")").Tables(0)
    End Function
    Public Function GetRequestForApproval(ByVal UserID As Integer, ByVal PostID As Integer) As DataTable
        Dim SqlStr As String
        SqlStr = "select a.REQUEST_ID,a.TICKET_NO,a.REMARKS,c.PROBLEM,d.group_Name,e.Sub_Group_Name,ISNULL(f.cnt,0)  from HD_REQUEST_MASTER a left outer join (select request_id,count(request_id)as Cnt from DMS_ESFB.dbo.HD_REQUEST_ATTACH group by Request_ID) f on(a.REQUEST_ID=f.REQUEST_ID),HD_TEAM_MASTER b,HD_PROBLEM_TYPE c,HD_GROUP_MASTER d,HD_SUB_GROUP e where a.TEAM_ID=b.TEAM_ID and a.PROBLEM_ID=c.PROBLEM_ID  and c.sub_Group_Id=e.sub_Group_ID and e.group_ID=d.group_ID AND A.STATUS_ID IN(6,7,8,10,15,16)  "
        If PostID = 6 Then
            SqlStr += " AND a.problem_id in(select problem_id from HD_PRO_APPROVAL  where POST_ID=6) AND (case when c.MODE_ID=2 then A.STATUS_ID   else 6 end)=6   and exists (select branch_id from brmaster k where a.branch_id=k.branch_id and k.area_head=" + UserID.ToString() + ")"
        ElseIf PostID = 7 Then
            SqlStr += " AND a.problem_id in(select problem_id from HD_PRO_APPROVAL  where POST_ID=7)  AND (case when c.MODE_ID=2 then A.STATUS_ID   else 7 end)=7  and exists (select branch_id from brmaster k where a.branch_id=k.branch_id and k.region_head=" + UserID.ToString() + ")"
        ElseIf PostID = 8 Then
            SqlStr += " AND a.problem_id in(select problem_id from HD_PRO_APPROVAL  where POST_ID=8)  AND (case when c.MODE_ID=2 then A.STATUS_ID   else 15 end)=15  and exists (select branch_id from brmaster k where a.branch_id=k.branch_id and k.zone_head=" + UserID.ToString() + ")"
        ElseIf PostID = 9 Then
            SqlStr += " AND a.problem_id in(select problem_id from HD_PRO_APPROVAL  where POST_ID=9)  AND (case when c.MODE_ID=2 then A.STATUS_ID   else 16 end)=16"
        Else
            ' SqlStr += " AND (a.problem_id in(select problem_id from HD_PRO_APPROVAL  where POST_ID=11)  AND (case when c.MODE_ID=2 then A.STATUS_ID   else 8 end)=8 AND ((A.STATUS_ID =8  and " + UserID.ToString() + " in(select emp_code from HD_BRANCH_DESK_USERS where type_id=1 and status_id=1))) OR( a.problem_id in(select problem_id from HD_PRO_APPROVAL  where POST_ID=10)  AND (case when c.MODE_ID=2 then A.STATUS_ID   else 10 end)=10 (A.STATUS_ID =10  and " + UserID.ToString() + " in(select emp_code from HD_BRANCH_DESK_USERS where type_id=2 and status_id=1))))"
            SqlStr += " AND ((a.problem_id in(select problem_id from HD_PRO_APPROVAL  where POST_ID=11)  AND (case when c.MODE_ID=2 then A.STATUS_ID   else 8 end)=8) AND (" + UserID.ToString() + " in(select emp_code from HD_BRANCH_DESK_USERS where type_id=1 and status_id=1)) OR( a.problem_id in(select problem_id from HD_PRO_APPROVAL  where POST_ID=10)  AND (case when c.MODE_ID=2 then A.STATUS_ID   else 10 end)=10  and " + UserID.ToString() + " in(select emp_code from HD_BRANCH_DESK_USERS where type_id=2 and status_id=1)))"
        End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function
    Public Function GetRequestDetailsToHelpDesk(ByVal TeamID As Integer) As DataTable
        Dim strval As String
        Return DB.ExecuteDataSet("select '-1' as Req_id,' ---------Select---------' as Req  union all select a1.Req_id +'Ø'+cast(ISNULL(hra.cnt ,0) as varchar(6))+'Ø'+cast(isnull(at.ticket_type,'')as varchar) +'Ø'+cast(isnull(at.requested_count,'')as varchar)  +'Ø'+cast(isnull(at.asset_tag,'')as varchar)  , " & _
                " a1.Req  from " & _
                 " (select cast(a.REQUEST_ID as varchar)+'Ø'+cast(a.BRANCH_ID as varchar)+'Ø'+cast(b.Branch_Name as varchar)+'Ø'+cast(a.CLASSIFICATION_ID as varchar) " & _
                 " +'Ø'+cast(a.DEPARTMENT_ID as varchar)+'Ø'+cast(e.Department_Name as varchar) +'Ø'+a.REMARKS +'Ø'+cast(a.TEAM_ID as varchar) " & _
                 " +'Ø'+convert(varchar,a.REQUEST_DT ,106) +'Ø'+cast(c.PROBLEM_ID as varchar)+'Ø'+cast(c.SUB_GROUP_ID as varchar) +'Ø'+cast(d.Group_ID as varchar)+'Ø'+cast(a.STATUS_ID as varchar)  " & _
                 " as Req_id,a.TICKET_NO +' - '+ b.Branch_Name +'('+ C.PROBLEM +')' as Req ,a.REQUEST_ID as id from HD_REQUEST_MASTER a,BRANCH_MASTER b, " & _
                 " HD_PROBLEM_TYPE c,HD_SUB_GROUP  d,DEPARTMENT_MASTER  e " & _
                 " where a.BRANCH_ID=b.Branch_ID and a.PROBLEM_ID=c.PROBLEM_ID and c.SUB_GROUP_ID =d.Sub_Group_ID and a.DEPARTMENT_ID=e.Department_ID and " & _
                 "  a.status_id=1  and a.team_id=" & TeamID.ToString() & " )a1 " & _
                 " left outer join ( select request_id,count(*) as cnt from  DMS_ESFB.dbo.HD_REQUEST_ATTACH group by request_id ) HRA on a1.id=HRA.REQUEST_ID " & _
                 " left outer join hd_asset_ticket at on a1.id=at.request_id " & _
                 " ORDER BY REQ_ID").Tables(0)
    End Function
    Public Function GetHDRequestDtl(ByVal TeamID As Integer, ByVal RequestID As Integer) As DataTable
        Dim strval As String
        Return DB.ExecuteDataSet("select a1.Req_id +'Ø'+cast(ISNULL(hra.cnt ,0) as varchar(6))+'Ø'+cast(isnull(at.ticket_type,'')as varchar)+'Ø'+cast(isnull(at.requested_count,'')as varchar)+'Ø'+cast(isnull(at.asset_tag,'')as varchar)+'Ø'+a1.Critical+'Ø'+a1.Descr+'Ø'+a1.Annex, " & _
                " a1.Req  from " & _
                 " (select cast(a.REQUEST_ID as varchar)+'Ø'+cast(a.BRANCH_ID as varchar)+'Ø'+cast(b.Branch_Name as varchar)+'Ø'+cast(a.CLASSIFICATION_ID as varchar) " & _
                 " +'Ø'+cast(a.DEPARTMENT_ID as varchar)+'Ø'+cast(e.Department_Name as varchar) +'Ø'+a.REMARKS +'Ø'+cast(a.TEAM_ID as varchar) " & _
                 " +'Ø'+convert(varchar,a.REQUEST_DT ,106) +'Ø'+cast(c.PROBLEM_ID as varchar)+'Ø'+cast(c.SUB_GROUP_ID as varchar) +'Ø'+cast(d.Group_ID as varchar)+'Ø'+cast(a.STATUS_ID as varchar)" & _
                 " as Req_id,a.TICKET_NO +' - '+ b.Branch_Name +'('+ C.PROBLEM +')' as Req ,a.REQUEST_ID as id,CAST(c.IS_CRITICAL as varchar(2)) Critical,isnull(c.PDESCRIPTION,'') Descr,CAST(c.ANNEXTURE as varchar(2)) Annex from HD_REQUEST_MASTER a,BRANCH_MASTER b, " & _
                 " HD_PROBLEM_TYPE c,HD_SUB_GROUP  d,DEPARTMENT_MASTER  e " & _
                 " where a.BRANCH_ID=b.Branch_ID and a.PROBLEM_ID=c.PROBLEM_ID and c.SUB_GROUP_ID =d.Sub_Group_ID and a.DEPARTMENT_ID=e.Department_ID and " & _
                 "  a.status_id in(1,17,18) and a.team_id=" & TeamID.ToString() & " and a.REQUEST_ID=" & RequestID.ToString() & ")a1 " & _
                 " left outer join ( select request_id,count(*) as cnt from  DMS_ESFB.dbo.HD_REQUEST_ATTACH group by request_id ) HRA on a1.id=HRA.REQUEST_ID " & _
                 " left outer join hd_asset_ticket at on a1.id=at.request_id " & _
                 " ORDER BY REQ_ID").Tables(0)
    End Function
    Public Function GetPROBLEMToAssign(ByVal SubGroupID As Integer) As DataTable
        Dim Query As String = "select '-1' as PROBLEM_ID,' -----Select-----' as PROBLEM union all select cast(PROBLEM_ID as varchar(8))+'~'+cast(isnull(ASSET_RELATED,0) as varchar(2))+'~'+CAST(IS_CRITICAL as varchar(2))+'~'+isnull(PDESCRIPTION,'')+'~'+CAST(ANNEXTURE as varchar(2)),PROBLEM from HD_PROBLEM_TYPE where Sub_Group_id=" & SubGroupID & " and status_id=1  order by 2"
        Return DB.ExecuteDataSet(Query).Tables(0)
    End Function
    Public Function GetTeamResolving(ByVal Update As Integer) As DataTable
        If Update = 1 Then
            Return DB.ExecuteDataSet("select -1 as Team_ID,' -----Select-----' as Team_name  union all select Team_ID,Team_name from HD_TEAM_MASTER where  status_id=1 union all  select 1000,'Vendor' union all  select 4,'Help Desk' order by 1").Tables(0)
        Else
            Return DB.ExecuteDataSet("select -1 as Team_ID,' -----Select-----' as Team_name  union all select Team_ID,Team_name from HD_TEAM_MASTER where  status_id=1 union all  select 1000,'Vendor' order by 2").Tables(0)
        End If
    End Function
    Public Function GetVendor(ByVal TeamID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as VendorID,' -----Select-----' as Vendor  union all select vendor_id,vendor_name from HD_Vendor_Master where team_id =" + TeamID.ToString() + " and Status_ID=1  order by 2").Tables(0)
    End Function
    Public Function GetRequestForApproval_Exception(ByVal PostID As Integer, ByVal EmpCode As Integer) As DataTable
        Dim SqlStr As String
        SqlStr = "select a.REQUEST_ID,a.TICKET_NO,a.REMARKS,c.PROBLEM,d.group_Name,e.Sub_Group_Name,ISNULL(f.cnt,0)  from HD_REQUEST_MASTER a left outer join (select request_id,count(request_id)as Cnt from DMS_ESFB.dbo.HD_REQUEST_ATTACH group by Request_ID) f on(a.REQUEST_ID=f.REQUEST_ID),HD_TEAM_MASTER b,HD_PROBLEM_TYPE c,HD_GROUP_MASTER d,HD_SUB_GROUP e where a.TEAM_ID=b.TEAM_ID and a.PROBLEM_ID=c.PROBLEM_ID  and c.sub_Group_Id=e.sub_Group_ID and e.group_ID=d.group_ID AND A.STATUS_ID IN(6,7,8,10,15,16)  "
        If PostID = 8 Then
            SqlStr += " and a.branch_id in(select branch_id from brmaster where zone_head=" + EmpCode.ToString() + ")"
        End If
        SqlStr += " order by a.request_id"
        Return DB.ExecuteDataSet(SqlStr).Tables(0)
    End Function
    Public Function GetALLBranch() As DataTable
        Return DB.ExecuteDataSet("select '-1' as Branch_id,'  ALL' as Branch_Name,''  union all  " & _
                "  select cast(Branch_id as varchar(5))+'Ø'+cast(status_ID as varchar(2)) as Branch_ID,Branch_Name,CAST(status_id AS VARCHAR(3) )  from Branch_MASTER where status_id >1  " & _
                " union all select '-2' as Branch_id,' ----------------','BB' as Branch_Name union all  " & _
                " select cast(Branch_id as varchar(5))+'Ø'+cast(status_ID as varchar(2)) as Branch_ID,Branch_Name,(case when status_id = 1 then 'Branch'+SPACE(6) else 'Admin Centre' end) +' | '+branch_name from Branch_MASTER where status_id=1  order by 3").Tables(0)
    End Function
    Public Function GetALLGroup() As DataTable
        Return DB.ExecuteDataSet("select -1 as Group_id,' ALL' as Group_Name union all select Group_id,Group_Name from HD_GROUP_MASTER where status_id=1  order by 2").Tables(0)
    End Function
    Public Function GetALLSubGroup(ByVal GroupID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as Sub_Group_id,' ALL' as Sub_Group_Name union all select Sub_Group_id,Sub_Group_Name from HD_SUB_GROUP where group_id=" & GroupID & " and status_id=1  order by 2").Tables(0)
    End Function
    Public Function GetALLTeamMembers() As DataTable
        Return DB.ExecuteDataSet("select 4 as team_id,'Help Desk' as team_name union all select team_id,team_name from hd_team_master where status_id=1").Tables(0)
    End Function
    Public Function GetALLStatus() As DataTable
        Return DB.ExecuteDataSet("select -3 as status_id,' ALL' as status  union all " & _
                " select -2 as status_id,'NORMAL CLOSING' as status union all select -1 as status_id,'AUTO CLOSING' as status union all  " & _
                " select status_id,status_name  from HD_status  order by status_id ").Tables(0)
    End Function


    Public Function GetRequestForAssigning(ByVal TeamID As Integer, ByVal UserID As Integer) As DataTable
        Dim Params(1) As SqlParameter
        Params(0) = New SqlParameter("@TeamID", SqlDbType.Int)
        Params(0).Value = TeamID
        Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
        Params(1).Value = UserID
        Return DB.ExecuteDataSet("SP_HD_GETREQUEST_WORKLOGUPDATE", Params).Tables(0)
    End Function
End Class