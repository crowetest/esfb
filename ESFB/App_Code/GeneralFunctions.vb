﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Management
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Configuration

Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq


Imports System.IO

Imports System.Web.Services
Imports System.Web.Script.Serialization
Public Class GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Enum Location
        HO = 0
        NormalBranch = 1
        AuctionCentre = 2
        RM_Office = 3
    End Enum
    Public Function Decrypt(ByVal Text As String) As String
        Dim DecodedText As String = Encoding.UTF8.GetString(Convert.FromBase64String((Text)))
        Return DecodedText
    End Function
    Public Function Encrypt(ByVal Text As String) As String
        Dim EncodedText As String = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Text))
        Return EncodedText
    End Function
    Public Function GetEntity() As DataTable
        Return DB.ExecuteDataSet(" select Entity_id, Entity_name  from  ENTITY_MASTER where  Status_ID in(1)").Tables(0)
    End Function
    Public Function FormAccess(ByVal EmpCode As Integer, ByVal FormID As Integer) As Boolean
        If EmpCode = 0 Then
            Return False
        End If
        Dim Access As Boolean = False
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("select COUNT(*) from ROLES_ASSIGNED a,ROLE_DTL B WHERE A.EMP_CODE = " & EmpCode & " AND B.FORM_ID = " & FormID & " AND A.ROLE_ID = B.ROLE_ID").Tables(0)
        If CInt(DT.Rows(0)(0)) > 0 Then
            Access = True
        End If
        Return Access
    End Function

    Public Function Get_Server_Date() As DataTable
        Return DB.ExecuteDataSet("select GETDATE()").Tables(0)
    End Function
    Public Function GetEmp_Name(ByVal userid As Integer, ByVal password As String) As DataTable
        Return DB.ExecuteDataSet("select emp_name from EMP_MASTER where Emp_Code=" & userid & " and Pass_word='" & password & "' and status_id=1").Tables(0)
    End Function
    Public Function GetEmp_Attendance(ByVal userid As Integer, ByVal password As String) As DataTable
        'Return DB.ExecuteDataSet("select emp_name from EMP_MASTER where Emp_Code=" & userid & " and Pass_word='" & password & "' ").Tables(0)
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@userid", userid), New System.Data.SqlClient.SqlParameter("@password", password)}
            Return DB.ExecuteDataSet("SP_CHECK_USER", Parameters).Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Shared Function getIPAddress() As String
        Dim myHost As String = System.Net.Dns.GetHostName()
        Dim myIP As String = System.Net.Dns.GetHostEntry(myHost).AddressList(0).ToString()

        Return myIP

    End Function

    Public Function GenerateOTP(OPT_Size As Integer) As String
        Dim allowedChars As String = ""

        allowedChars += "1,2,3,4,5,6,7,8,9,0"
        Dim sep() As Char = {CChar(",")}
        Dim arr() As String = allowedChars.Split(sep)

        Dim passwordString As String = ""
        Dim temp As String = ""
        Dim rand As Random = New Random()
        Dim i As Integer = 0
        While (i < OPT_Size)

            temp = arr(rand.Next(0, arr.Length))

            passwordString += temp

            i = i + 1

        End While

        Return passwordString
    End Function
    Public Shared Function getMACIPAddress() As String
        'Dim scope As New System.Management.ManagementScope("\\.\root\cimv2")
        'scope.Connect()
        'Dim objectQuery As New ObjectQuery("SELECT * FROM Win32_NetworkAdapter WHERE NetConnectionId IS NOT NULL")
        'Dim searcher As New ManagementObjectSearcher(scope, objectQuery)
        'Dim os As ManagementObject
        'Dim moColl As ManagementObjectCollection = searcher.Get()
        'Dim NetConnection As String
        'Dim MacAdd As String = Nothing
        'For Each os In moColl
        '    Dim Name As String = os("NetConnectionId").ToString
        '    NetConnection = Name.Substring(0, 5)
        '    If NetConnection = "Local" Then
        '        MacAdd = os("MacAddress").ToString
        '        Exit For
        '    End If
        'Next os
        'Return MacAdd
        'ETHERNET MAC ADDRESS
        'Dim macadress As [String] = String.Empty
        'For Each nic As NetworkInterface In NetworkInterface.GetAllNetworkInterfaces()
        '    Dim ot As OperationalStatus = nic.OperationalStatus
        '    'If nic.OperationalStatus = OperationalStatus.Up Then
        '    If nic.Name = "" Then
        '        macadress = nic.GetPhysicalAddress().ToString()
        '        If macadress <> "" Then
        '            Exit For
        '        End If
        '        '
        '    End If

        'Next
        Dim macadress As [String] = String.Empty
        For Each nic As NetworkInterface In NetworkInterface.GetAllNetworkInterfaces()
            Dim ot As OperationalStatus = nic.OperationalStatus
            'If nic.OperationalStatus = OperationalStatus.Up Then
            macadress = nic.GetPhysicalAddress().ToString()
            If macadress <> "" Then
                Exit For
            End If
            '
            'End If

        Next
        Return macadress
    End Function
   
    Public Sub ComboFill(ByRef ComboName As DropDownList, ByRef DataTableName As DataTable, ByVal ValueField As Integer, ByVal Textfield As Integer)
        ComboName.Items.Clear()
        If DataTableName.Rows.Count > 0 Then
            ComboName.DataSource = DataTableName
            ComboName.DataValueField = DataTableName.Columns(ValueField).ColumnName
            ComboName.DataTextField = DataTableName.Columns(Textfield).ColumnName
            ComboName.DataBind()
        End If
    End Sub
    Public Function GetQueryResult(ByVal TrNo As Integer, ByVal UserID As Integer, Optional ByVal FilterID As Integer = 0, Optional ByVal fromdate As Date = Nothing, Optional ByVal todate As Date = Nothing, Optional ByVal FilterText As String = "") As DataTable
        Try
            Dim FromDt As Date = Date.Now().Date
            Dim ToDt As Date = Date.Now().Date
            If (fromdate >= "01/01/2000") Then
                FromDt = fromdate
            End If
            If (fromdate >= "01/01/2000") Then
                ToDt = todate
            End If
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TrNo", TrNo), New System.Data.SqlClient.SqlParameter("@UserID", UserID), New System.Data.SqlClient.SqlParameter("@FilterID", FilterID), New System.Data.SqlClient.SqlParameter("@fromdate", FromDt), New System.Data.SqlClient.SqlParameter("@todate", ToDt), New System.Data.SqlClient.SqlParameter("@FilterText", FilterText)}
            Return DB.ExecuteDataSet("GetQueryResult", Parameters).Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function GetModules() As DataTable
        Return DB.ExecuteDataSet("select module_id,Module_name from MODULE_MASTER where status_id = 1 order by Module_name").Tables(0)
    End Function
    Public Function ChangePassword(ByVal HdnValue As String) As String
        Dim message As New String(" "c, 1000)
        Dim arr As String() = HdnValue.Split("ử")
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@UserID", Integer.Parse(arr(0))), New System.Data.SqlClient.SqlParameter("@OldPass", EncryptDecrypt.Encrypt(arr(1).Trim(), arr(0).Trim())), New System.Data.SqlClient.SqlParameter("@NewPass", EncryptDecrypt.Encrypt(arr(2).Trim(), arr(0).Trim())), New System.Data.SqlClient.SqlParameter("@Msg", message)}
            Parameters(3).Direction = ParameterDirection.InputOutput
            DB.ExecuteNonQuery("SP_Change_Password", Parameters)
            message = Parameters(3).Value.ToString()
        Catch ex As Exception
            message = ex.Message

        End Try
        Return message
    End Function

    Public Function GetBranch() As DataTable
        Return DB.ExecuteDataSet("select '-1' as branchid,' ------Select------' as branch Union All select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id = 1 order by 2").Tables(0)
    End Function

    Public Function GetUserDetails(ByVal UserID As Integer) As DataSet
        Return DB.ExecuteDataSet("Select emp_code,emp_name,Branch_name,Department_name,Designation_name,Gender_ID,CONVERT(VARCHAR(11),Date_Of_Join,106),Branch_ID,Emp_status from Emp_List where EMp_Code=" + UserID.ToString() + " AND STATUS_ID=1")

    End Function
    'Public Function GET_EMP_DEATILS(ByVal EMPID As Integer) As DataTable
    '    Return DB.ExecuteDataSet("Select CONVERT(VARCHAR,emp_code)+'^'+emp_name+'^'+CONVERT(VARCHAR,Branch_ID)+'^'+Branch_name+'^'+CONVERT(VARCHAR,Department_ID) +'^'+ Department_name+'^'+Designation_name+'^'+CONVERT(VARCHAR,Designation_ID)+'^'+CONVERT(VARCHAR,Reporting_To)+'^'+CONVERT(VARCHAR,post_ID) from Emp_List where EMp_Code=" + EMPID.ToString() + " and status_ID=1").Tables(0)
    'End Function
    Public Function GET_EMP_DEATILS(ByVal EMPID As Integer) As DataTable
        Return DB.ExecuteDataSet("Select CONVERT(VARCHAR,a.emp_code)+'^'+a.emp_name+'^'+CONVERT(VARCHAR,a.Branch_ID)+'^'+a.Branch_name+'^'+CONVERT(VARCHAR,a.Department_ID) +'^'+ " & _
             " a.Department_name+'^'+a.Designation_name+'^'+CONVERT(VARCHAR,a.Designation_ID)+'^'+CONVERT(VARCHAR,a.Reporting_To)  " & _
             " +'^'+CONVERT(VARCHAR,n.Basic_Pay)+'^'+CONVERT(VARCHAR,n.DA)+'^'+CONVERT(VARCHAR,n.HRA)+'^'+CONVERT(VARCHAR,n.Conveyance)+'^'+  " & _
             " CONVERT(VARCHAR,n.SpecialAllowance)+'^'+CONVERT(VARCHAR,n.LocalAllowance)+'^'+CONVERT(VARCHAR,n.MedicalAllowance)  " & _
             " +'^'+CONVERT(VARCHAR,n.HospitalityAllowance)+'^'+CONVERT(VARCHAR,n.PerformanceAllowance)+'^'+  " & _
            " CONVERT(VARCHAR,n.OtherAllowance)+'^'+CONVERT(VARCHAR,n.FieldAllowance)+'^'+CONVERT(VARCHAR,n.ESI)+'^'+CONVERT(VARCHAR,n.PF)  " & _
            " +'^'+CONVERT(VARCHAR,n.SWF)+'^'+CONVERT(VARCHAR,n.ESWT)+'^'+CONVERT(VARCHAR,n.Charity) +'^'+CONVERT(VARCHAR,post_ID)  +'^'+a.cadre_name" & _
            " from Emp_List a,EMP_SALARY_DTL n where a.EMp_Code=" & EMPID.ToString & " and a.status_ID=1 and a.Emp_Code=n.Emp_Code ").Tables(0)
    End Function
    Public Function GetDepartment() As DataTable
        Return DB.ExecuteDataSet("select '-1' as depid,' ------Select------' as department Union All select department_id as depid,Department_Name as department from DEPARTMENT_MASTER where status_id = 1").Tables(0)
    End Function
    Public Function GetDesignation() As DataTable
        Return DB.ExecuteDataSet("select '-1' as desid,' ------Select------' as designation Union All select designation_id as desid,designation_Name as designation from DESIGNATION_MASTER where status_id = 1").Tables(0)
    End Function
    Public Function GetEmployee(ByVal EMP_CODE As Integer) As DataTable '-------------DOB change
        Return DB.ExecuteDataSet("select a.Emp_Code,a.Emp_Name,e.Department_Name,d.Branch_Name,g.Designation_Name,  l.emp_name as ReportingTo,case when isnull(m.Email,'')='' " & _
            "then 'Nil' else m.Email END ,case when isnull(m.Mobile,'')=''  then 'Nil' else m.Mobile END ,m.official_mail_id,isnull(e.dob_updated_staff,0)  from EMP_MASTER a LEFT JOIN  Emp_Profile  m ON a.Reporting_to=m.Emp_Code,BRANCH_MASTER d,DEPARTMENT_MASTER e," & _
            " DESIGNATION_MASTER g,emp_master l where a.Branch_ID=d.Branch_ID and a.Department_ID=e.Department_ID" & _
            " and a.Reporting_to=l.Emp_Code  and a.Designation_ID=g.Designation_ID and a.emp_code=" & EMP_CODE & "  ").Tables(0)
    End Function
    Public Function GetEmployeeString(ByRef DT As DataTable) As String '-------------N
        Dim StrEmployee As String = ""
        For n As Integer = 0 To DT.Rows.Count - 1
            StrEmployee += DT.Rows(n)(0) & "^" & DT.Rows(n)(1) & "^" & DT.Rows(n)(2) & "^" & DT.Rows(n)(3) & "^" & DT.Rows(n)(4) & "^" & DT.Rows(n)(5) & "^" & DT.Rows(n)(6) & "^" & DT.Rows(n)(7) & "^" & DT.Rows(n)(8)
            If n < DT.Rows.Count - 1 Then
                StrEmployee += "¥"
            End If
        Next
        Return StrEmployee
    End Function
    Public Function GetRegionWiseAuditPending() As DataTable
        Return DB.ExecuteDataSet("select h.Region_ID,upper(h.region_name)  as RPT_DESCR,  COUNT(e.OBSERVATION_ID) as Count from AUDIT_DTL a,dbo.AUDIT_TYPE b, dbo.AUDIT_OBSERVATION C,dbo.AUDIT_CHECK_LIST D,dbo.AUDIT_OBSERVATION_DTL E,  BRANCH_MASTER F,AREA_MASTER g,REGION_MASTER h,Audit_master i " & _
        " WHERE(a.group_ID =i.group_ID and a.AUDIT_TYPE = b.TYPE_ID And i.status_id = 2 And E.status_id = 1 And C.ITEM_ID = D.item_id And C.OBSERVATION_ID = E.OBSERVATION_ID) " & _
        " and a.AUDIT_ID=c.AUDIT_ID and  a.BRANCH_ID=f.BRANCH_ID and f.Area_ID=g.Area_ID and g.Region_ID=h.Region_ID group by h.Region_ID,h.region_name").Tables(0)
    End Function
    Public Function GetRegionAuditString(ByRef DT As DataTable) As String
        Dim StrRegion As String = ""
        For n As Integer = 0 To DT.Rows.Count - 1
            StrRegion += DT.Rows(n)(0) & "^" & DT.Rows(n)(1) & "^" & DT.Rows(n)(2)
            If n < DT.Rows.Count - 1 Then
                StrRegion += "¥"
            End If
        Next
        Return StrRegion
    End Function


    Public Function GetTopBranchObservnPending() As DataTable
        Return DB.ExecuteDataSet(" select top 10 upper(F.branch_name) as RPT_DESCR,  COUNT(e.OBSERVATION_ID)  as Count,a.branch_id from AUDIT_DTL a,dbo.AUDIT_TYPE b, dbo.AUDIT_OBSERVATION C,dbo.AUDIT_CHECK_LIST D, " & _
            " dbo.AUDIT_OBSERVATION_DTL E, BRANCH_MASTER F,Audit_Master g WHERE a.group_ID=g.Group_ID and a.AUDIT_TYPE = b.TYPE_ID And  g.status_id = 2 And E.status_id = 1 And C.ITEM_ID = D.item_id And C.OBSERVATION_ID = E.OBSERVATION_ID  " & _
            " and a.AUDIT_ID=c.AUDIT_ID and  a.BRANCH_ID=f.BRANCH_ID  group by a.branch_id,F.branch_name order by COUNT(e.OBSERVATION_ID) DESC").Tables(0)
    End Function
    Public Function GetObservnIndex() As DataTable
        Return DB.ExecuteDataSet("select aa.* from (select top 5 left(DATENAME(month, g.Period_to),3)+'-'+cast(YEAR(g.Period_to) as varchar(4)) as RPT_DESCR,COUNT(e.observation_id)   as Count,month(g.period_to)as month,year(g.period_to)as year from AUDIT_DTL a, dbo.AUDIT_OBSERVATION C," & _
            "AUDIT_OBSERVATION_DTL E,Audit_Master g WHERE a.group_ID=g.Group_ID  and g.status_id=2  and a.AUDIT_ID=c.AUDIT_ID and c.OBSERVATION_ID=e.OBSERVATION_ID" & _
              " and g.ASSIGN_DT >=dateadd(mm,-10,CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(GETDATE())-1),GETDATE()),101))   GROUP bY left(DATENAME(month, g.Period_to),3)+'-'+cast(YEAR(g.Period_to) as varchar(4)),month(g.Period_to),YEAR(g.Period_to) order by YEAR(g.Period_to) desc,month(g.Period_to) desc)aa order by aa.year,aa.month").Tables(0)
    End Function
    Public Function getPendingStatus() As DataTable
        Return DB.ExecuteDataSet("select upper(b.description)  as RPT_DESCR,COUNT(a.status_ID) as count,a.status_id from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION_status_master b " & _
                                 ",AUDIT_OBSERVATION c,AUDIT_DTL d,AUDIT_MASTER e where a.STATUS_ID=b.status_id and a.OBSERVATION_ID=c.OBSERVATION_ID and c.AUDIT_ID=d.AUDIT_ID " & _
                                 "and d.GROUP_ID=e.GROUP_ID and a.STATUS_ID>0  and e.STATUS_ID=2 group by a.status_id,b.description order by 2 desc").Tables(0)
    End Function
    Public Function GetState() As DataTable
        Return DB.ExecuteDataSet("select -1 as State_ID,' -----Select-----' as State_name union all select State_ID,State_name from State_Master where status_id = 1 order by State_ID").Tables(0)
    End Function
    Public Function GetDistrict(ByVal StateID As Integer) As DataTable
        Return DB.ExecuteDataSet("select -1 as District_ID,' -----Select-----' as District_name union all select District_ID, District_name from  District_Master where status_id = 1 and State_ID=" + StateID.ToString() + " order by  District_Name").Tables(0)
    End Function
    Public Function GetPost(ByVal DistrictID As Integer) As DataTable
        Return DB.ExecuteDataSet("select '-1' as Post_ID,' -----Select-----' as Post_Office union all select cast(Post_ID as varchar(10))+'~'+cast(pin_Code as varchar(8)) as Post_ID, Post_Office+' - '+cast(pin_Code as varchar(8)) as Post_Office from  Post_Master where  District_ID=" + DistrictID.ToString() + " order by  Post_Office").Tables(0)
    End Function
    Public Function GetMaritalStatus() As DataTable
        Return DB.ExecuteDataSet("select -1 as MStatus_ID,' -----Select-----' as M_Status union all select MStatus_ID, MStatus  from  Marital_Status where  Status_ID=1").Tables(0)
    End Function
    Public Function GetQualification() As DataTable
        Return DB.ExecuteDataSet("select -1 as Qualification_ID,' -----Select-----' as Qualification_name union all select Qualification_ID,Qualification_name from Qualification_Master where status_id = 1 order by Qualification_ID").Tables(0)
    End Function
    Public Function GetUniversity() As DataTable
        Return DB.ExecuteDataSet("select -1 as University_ID,' -----Select-----' as University_name union all select University_ID,University_name from University_Master where status_id = 1 order by University_ID").Tables(0)
    End Function

    Public Function GetIDProof() As DataTable
        Return DB.ExecuteDataSet("select -1 as TypeID,' -----Select-----' as TypeName union all select Type_ID, Type_Name  from  ID_PRoof_Type where  Status_ID in(1,3)").Tables(0)
    End Function
    Public Function GetAddressProof() As DataTable
        Return DB.ExecuteDataSet("select -1 as TypeID,' -----Select-----' as TypeName union all select Type_ID, Type_Name  from  ID_PRoof_Type where  Status_ID in(2,3)").Tables(0)
    End Function
    Public Function GetFamily() As DataTable
        Return DB.ExecuteDataSet("select -1 as family_ID,' -----Select-----' as family_person union all select family_ID, family_person  from  FAmily_master where  Status_ID in(1)").Tables(0)
    End Function
    Public Function GetEmployeeStatus() As DataTable
        Return DB.ExecuteDataSet("select -1 as emp_status_ID,' -----Select-----' as emp_status union all select emp_status_id, emp_status  from  EMPLOYEE_STATUS where  Status_ID in(1)").Tables(0)
    End Function

    Public Function GetEmpPost() As DataTable
        Return DB.ExecuteDataSet("select -1 as PostID,' -----Select-----' as PostName union all select Post_ID, Post_Name  from  Emp_POst_Master where  Status_ID =1").Tables(0)
    End Function
    Public Function GetPhoto(ByVal EmpCode As Integer) As DataTable
        Return DB.ExecuteDataSet("select E_photo,content_type from DMS_ESFB.dbo.Emp_Photo where Emp_Code=" + EmpCode.ToString()).Tables(0)
    End Function
    Public Function GetNEWS() As DataTable
        Return DB.ExecuteDataSet("select DESCR from news where status_id=1").Tables(0)
    End Function
    Public Function GetParameterValue(ByVal ParameterID As Integer, ByVal ValueType As Integer) As String
        Dim Query As String = ""
        If ValueType = 1 Then
            Query = "select NUMERIC_VALUE from General_Parameters where PARAM_ID=" + ParameterID.ToString()
        ElseIf ValueType = 2 Then
            Query = "select TEXT_VALUE from General_Parameters where PARAM_ID=" + ParameterID.ToString()
        ElseIf ValueType = 3 Then
            Query = "select DATE_VALUE from General_Parameters where PARAM_ID=" + ParameterID.ToString()
        End If
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet(Query).Tables(0)
        If (DT.Rows.Count > 0) Then
            Return DT.Rows(0)(0).ToString()
        Else
            Return ""
        End If
    End Function
    Public Function isAuditAdmin(ByVal EmpCode As Integer) As Integer
        Dim dt As New DataTable
        dt = DB.ExecuteDataSet("declare @Roles as varchar(15) select @Roles= text_value from general_parameters where param_id=9 declare  @qry as varchar(500) set @qry='select count(*) from roles_assigned where role_id in(' + @Roles + ') and emp_code=" + EmpCode.ToString() + "' execute (@qry)").Tables(0)
        Return dt.Rows(0)(0)
    End Function
    Public Function ResetPasswordString() As String
        Dim Strvalue As String = ""
        Dim DT As New DataTable
        Dim StrPassword As String

        DT = DB.ExecuteDataSet("select emp_code from EMP_MASTER where status_id=1 and pass_word is null").Tables(0)
        For n As Integer = 0 To DT.Rows.Count - 1
            StrPassword = EncryptDecrypt.Encrypt("Esaf123", DT.Rows(n)(0).ToString())
            Strvalue += DT.Rows(n)(0) & "^" & StrPassword
            If n < DT.Rows.Count - 1 Then
                Strvalue += "¥"
            End If
        Next
        Return Strvalue

    End Function
    Public Function GetRegion_Name(ByVal RegionID As Integer) As String
        Dim DT As DataTable = DB.ExecuteDataSet("select Region_Name from Region_Master where Region_ID=" & RegionID).Tables(0)
        Dim RegionName As String = ""
        If DT.Rows.Count > 0 Then
            RegionName = DT.Rows(0)(0)
        End If
        Return RegionName
    End Function
    Public Function GetArea_Name(ByVal AreaID As Integer) As String
        Dim DT As DataTable = DB.ExecuteDataSet("select Area_Name from Area_Master where area_id=" & AreaID).Tables(0)
        Dim AreaName As String = ""
        If DT.Rows.Count > 0 Then
            AreaName = DT.Rows(0)(0)
        End If
        Return AreaName
    End Function
    Public Function GetBranch_Name(ByVal BranchID As Integer) As String
        Dim DT As DataTable = DB.ExecuteDataSet("select Branch_Name from Branch_Master where Branch_ID=" & BranchID).Tables(0)
        Dim BranchName As String = ""
        If DT.Rows.Count > 0 Then
            BranchName = DT.Rows(0)(0)
        End If
        Return BranchName
    End Function
    Public Function GetArea_ID(ByVal UserID As Integer) As Integer
        Dim DT As DataTable = DB.ExecuteDataSet("select Area_ID from Area_Master where area_head=" & UserID).Tables(0)
        Dim AreaID As Integer = 0
        If DT.Rows.Count > 0 Then
            AreaID = DT.Rows(0)(0)
        End If
        Return AreaID
    End Function
    Public Function GetRegion_ID(ByVal UserID As Integer) As Integer
        Dim DT As DataTable = DB.ExecuteDataSet("select Region_ID from Region_Master where Region_head=" & UserID).Tables(0)
        Dim RegionID As Integer = 0
        If DT.Rows.Count > 0 Then
            RegionID = DT.Rows(0)(0)
        End If
        Return RegionID
    End Function
    Public Function GetRegion(ByVal SelFlag As Integer, ByVal PostID As Integer, ByVal UserID As Integer) As DataTable
        Dim SqlStr As String = ""
        If SelFlag = 1 Then
            Return DB.ExecuteDataSet("select -1 as RegionID,' -----Select-----' as RegionName union all select region_id,region_Name from REGION_MASTER where status_id = 1 ORDER BY 2").Tables(0)
        Else
            If PostID = 5 Then
                SqlStr = "select region_id,region_Name from REGION_MASTER where status_id = 1 and Region_ID in(select region_ID from area_master where area_id in(select area_id from branch_master where branch_head=" + UserID.ToString() + ")) ORDER BY 2"
            ElseIf PostID = 6 Then
                SqlStr = "select region_id,region_Name from REGION_MASTER where status_id = 1 and Region_ID in(select region_ID from area_master where area_head=" + UserID.ToString() + ") ORDER BY 2"
            ElseIf PostID = 7 Then
                SqlStr = "select region_id,region_Name from REGION_MASTER where status_id = 1 and Region_Head=" + UserID.ToString() + " ORDER BY 2"
            ElseIf PostID = 8 Then
                SqlStr = "select -1 as RegionID,' ALL' as RegionName union all select region_id,region_Name from REGION_MASTER where status_id = 1 and zone_id in(select Zone_id from Zone_Master where Zone_Head=" + UserID.ToString() + ") ORDER BY 2"
            Else
                SqlStr = "select -1 as RegionID,' ALL' as RegionName union all select region_id,region_Name from REGION_MASTER where status_id = 1 ORDER BY 2"
            End If
            Return DB.ExecuteDataSet(SqlStr).Tables(0)
        End If

    End Function
    Public Function GetArea(ByVal RegionID As Integer, ByVal SelFlag As Integer) As DataTable
        If SelFlag = 1 Then
            Return DB.ExecuteDataSet("select -1 as AreaID,' -----Select-----' as AreaName union all select Area_id,Area_Name from Area_MASTER where region_ID=" + RegionID.ToString() + " and status_id = 1 ORDER BY 2").Tables(0)
        Else
            Return DB.ExecuteDataSet("select -1 as AreaID,' ALL' as AreaName union all select Area_id,Area_Name from Area_MASTER where region_ID=" + RegionID.ToString() + " and status_id = 1 ORDER BY 2").Tables(0)
        End If

    End Function
    Public Function GetBranch(ByVal AreaID As Integer, ByVal SelFlag As Integer) As DataTable
        If SelFlag = 1 Then
            Return DB.ExecuteDataSet("select -1 as Branch_id,' -----Select-----' as Branch_Name union all select Branch_id,Branch_Name from Branch_MASTER where Area_ID=" + AreaID.ToString() + " and status_id = 1 ORDER BY 2").Tables(0)
        Else
            Return DB.ExecuteDataSet("select -1 as Branch_id,' ALL' as Branch_Name union all select Branch_id,Branch_Name from Branch_MASTER where Area_ID=" + AreaID.ToString() + " and status_id = 1 ORDER BY 2").Tables(0)
        End If

    End Function
    Public Function GetEmp_Post_Name(ByVal PostID As Integer) As String
        Dim DT As DataTable = DB.ExecuteDataSet("select Post_Name from Emp_POst_Master where Post_ID=" & PostID.ToString()).Tables(0)
        Dim EmpPost As String = ""
        If DT.Rows.Count > 0 Then
            EmpPost = DT.Rows(0)(0)
        End If
        Return EmpPost
    End Function
    Public Function GetEmpPostDetails(ByVal EMPID As Integer) As DataTable
        Return DB.ExecuteDataSet("Select e.emp_code,e.emp_name,e.Branch_ID,e.Branch_name,e.Department_name,e.Designation_name,e.Date_Of_Join,m.Post_ID,p.Post_Name from Emp_List e,EMP_MASTER m, EMP_POST_MASTER p where e.Emp_Code=m.Emp_Code and m.Post_ID=p.Post_ID and e.EMp_Code=" + EMPID.ToString() + " and e.status_ID=1").Tables(0)
    End Function
    Public Function GetRegions() As DataTable
        Return DB.ExecuteDataSet("select '-1' as regionid,' ------Select------' as region Union All select Region_ID as regionid,Region_Name as reegion from REGION_MASTER where status_id = 1 order by 2").Tables(0)
    End Function
    Public Function GetAreas() As DataTable
        Return DB.ExecuteDataSet("select '-1' as areaid,' ------Select------' as area Union All select Area_ID as areaid,Area_Name as area from AREA_MASTER where status_id = 1 order by 2").Tables(0)
    End Function
    Public Function GetZones() As DataTable
        Return DB.ExecuteDataSet("select '-1' as zoneid,' ------Select------' as zone Union All select Zone_ID as zoneid,Zone_Name as zone from Zone_Master where status_id = 1 order by 2").Tables(0)
    End Function
    Public Function GetBranch_ID(ByVal UserID As Integer) As Integer
        Dim DT As DataTable = DB.ExecuteDataSet("select Branch_ID from Branch_master where Branch_Head=" & UserID).Tables(0)
        Dim RegionID As Integer = 0
        If DT.Rows.Count > 0 Then
            RegionID = DT.Rows(0)(0)
        End If
        Return RegionID
    End Function

    Public Function GetQueryResult(ByVal Query As String) As DataTable
        Try
            Return DB.ExecuteDataSet(Query).Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function GetEmpName(ByVal EmpCode As Integer) As DataTable
        Return DB.ExecuteDataSet("select emp_name from EMP_MASTER where Emp_Code = " & EmpCode & "").Tables(0)
    End Function
    Public Function getReportingEmployees(ByVal userid As Integer, Optional stat As Integer = 1) As DataTable
        If stat = 1 Then
            Return DB.ExecuteDataSet("select -1 as emp_code,'-----------Select-----------' as Emp_Name union all select a.emp_code,upper(a.emp_name) from EMP_MASTER a where a.status_id=1 and emp_status_id<>6 and a.reporting_to = " & userid & "").Tables(0)
        Else
            Return DB.ExecuteDataSet("select -1 as emp_code,'-----------Select-----------' as Emp_Name union all select a.emp_code,upper(a.emp_name) from EMP_MASTER a where a.status_id=1  and emp_status_id<>6 and a.emp_code>100").Tables(0)
        End If

    End Function
    Public Function GetDepartment_Name(ByVal DepartmentID As Integer) As String
        Dim DT As DataTable = DB.ExecuteDataSet("select Department_Name from Department_Master where Department_ID=" & DepartmentID).Tables(0)
        Dim Department_Name As String = ""
        If DT.Rows.Count > 0 Then
            Department_Name = DT.Rows(0)(0)
        End If
        Return Department_Name
    End Function
Public Function GetRegionBasedOnPost(ByVal PostID As Integer, ByVal UserID As Integer) As DataTable
        Dim SqlStr As String = ""
        If PostID = 5 Then
            SqlStr = "select -1 as RegionID,' ALL' as RegionName union all select region_id,region_Name from REGION_MASTER where status_id = 1 and region_id in(select region_id from brmaster where branch_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 6 Then
            SqlStr = "select -1 as RegionID,' ALL' as RegionName union all select region_id,region_Name from REGION_MASTER where status_id = 1 and region_id in(select region_id from brmaster where area_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 7 Then
            SqlStr = "select -1 as RegionID,' ALL' as RegionName union all select region_id,region_Name from REGION_MASTER where status_id = 1 and region_id in(select region_id from brmaster where region_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 8 Then
            SqlStr = "select -1 as RegionID,' ALL' as RegionName union all select region_id,region_Name from REGION_MASTER where status_id = 1 and region_id in(select region_id from brmaster where zone_head=" + UserID.ToString() + ") ORDER BY 2"
        Else
            SqlStr = "select -1 as RegionID,' ALL' as RegionName union all select region_id,region_Name from REGION_MASTER where status_id = 1 ORDER BY 2"
        End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)

    End Function
    Public Function GetTerritoryBasedOnPost(ByVal PostID As Integer, ByVal UserID As Integer) As DataTable
        Dim SqlStr As String = ""
        If PostID = 5 Then
            SqlStr = "select -1 as ZoneID,' ALL' as ZoneName union all select Zone_id,Zone_Name from Zone_MASTER where status_id = 1 and zone_id>1 and Zone_id in(select Zone_id from brmaster where branch_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 6 Then
            SqlStr = "select -1 as ZoneID,' ALL' as ZoneName union all select Zone_id,Zone_Name from Zone_MASTER where status_id = 1 and zone_id>1 and Zone_id in(select Zone_id from brmaster where area_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 7 Then
            SqlStr = "select -1 as ZoneID,' ALL' as ZoneName union all select Zone_id,Zone_Name from Zone_MASTER where status_id = 1 and zone_id>1 and Zone_id in(select Zone_id from brmaster where Region_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 8 Then
            SqlStr = "select -1 as ZoneID,' ALL' as ZoneName union all select Zone_id,Zone_Name from Zone_MASTER where status_id = 1 and zone_id>1 and zone_id in(select zone_id from brmaster where zone_head=" + UserID.ToString() + ") ORDER BY 2"
        Else
            SqlStr = "select -1 as ZoneID,' ALL' as ZoneName union all select Zone_id,Zone_Name from Zone_MASTER where status_id = 1 and zone_id>1 ORDER BY 2"
        End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)

    End Function
    Public Function GetAreaBasedOnPost(ByVal PostID As Integer, ByVal UserID As Integer) As DataTable
        Dim SqlStr As String = ""
        If PostID = 5 Then
            SqlStr = "select -1 as AreaID,' ALL' as AreaName union all select Area_id,Area_Name from Area_MASTER where status_id = 1 and Area_ID in(select Area_ID from brmaster where branch_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 6 Then
            SqlStr = "select -1 as AreaID,' ALL' as AreaName union all select Area_id,Area_Name from Area_MASTER where status_id = 1 and Area_ID in(select Area_ID from brmaster where area_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 7 Then
            SqlStr = "select -1 as AreaID,' ALL' as AreaName union all select Area_id,Area_Name from Area_MASTER where status_id = 1 and Area_ID in(select Area_ID from brmaster where Region_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 8 Then
            SqlStr = "select -1 as AreaID,' ALL' as AreaName union all select Area_id,Area_Name from Area_MASTER where status_id = 1 and Area_ID in(select Area_ID from brmaster where Zone_head=" + UserID.ToString() + ") ORDER BY 2"
        Else
            SqlStr = "select -1 as AreaID,' ALL' as AreaName union all select Area_id,Area_Name from Area_MASTER where status_id = 1 ORDER BY 2"
        End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)

    End Function
    Public Function GetBranchBasedOnPost(ByVal PostID As Integer, ByVal UserID As Integer) As DataTable
        Dim SqlStr As String = ""
        If PostID = 5 Then
            SqlStr = "select -1 as BranchID,' ALL' as BranchName union all select Branch_id,Branch_Name from Branch_MASTER where status_id = 1 and branch_id in(select branch_id from brmaster where branch_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 6 Then
            SqlStr = "select -1 as BranchID,' ALL' as BranchName union all select Branch_id,Branch_Name from Branch_MASTER where status_id = 1 and branch_id in(select Branch_id from brmaster where Area_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 7 Then
            SqlStr = "select -1 as BranchID,' ALL' as BranchName union all select Branch_id,Branch_Name from Branch_MASTER where status_id = 1 and branch_id in(select Branch_id from brmaster where Region_head=" + UserID.ToString() + ") ORDER BY 2"
        ElseIf PostID = 8 Then
            SqlStr = "select -1 as BranchID,' ALL' as BranchName union all select Branch_id,Branch_Name from Branch_MASTER where status_id = 1 and branch_id in(select Branch_id from brmaster where zone_head=" + UserID.ToString() + ") ORDER BY 2"
        Else
            SqlStr = "select -1 as BranchID,' ALL' as BranchName union all select Branch_id,Branch_Name from Branch_MASTER where status_id = 1 ORDER BY 2"
        End If
        Return DB.ExecuteDataSet(SqlStr).Tables(0)

    End Function
    Public Function InsertError(ByVal ErrorMessage As String, ByVal FormName As String) As Integer
        Dim ErrorFlag As Integer
        Try
            Dim Params(2) As SqlParameter
            Params(0) = New SqlParameter("@ErrorNo", SqlDbType.Int)
            Params(0).Direction = ParameterDirection.Output
            Params(1) = New SqlParameter("@ErrorMessage", SqlDbType.VarChar)
            Params(1).Value = ErrorMessage
            Params(2) = New SqlParameter("@FormName", SqlDbType.VarChar, 100)
            Params(2).Value = FormName
            DB.ExecuteNonQuery("SP_INSERT_ERROR", Params)
            ErrorFlag = CInt(Params(0).Value)
        Catch ex As Exception
        End Try
        Return ErrorFlag
    End Function
    Public Function GetChecklist(ByVal userid As String) As String
        Dim DT As New DataTable
        Dim StrGrpNam As String = ""
        'DT = DB.ExecuteDataSet("select Description,ChecklistID,HisID,remarks,case when frequency = 0 then 'Daily' when frequency = 1 then 'Weekly' when frequency = 2 then 'Monthly' when frequency = 3 then 'Quarterly' when frequency = 4 then 'Half Yearly' when frequency = 5 then 'Yearly' end as frequency,frequency,case when status_id=0 then 'No task available' when status_id=1 then 'Checklist active' when status_id=-1 then 'Checklist Inactive' end as status,status_id,case when ApprovedStatus=0 then 'No' when ApprovedStatus=1 then 'Yes' end as appsts from branch_surveillance_chklist_Tracker where makerid=" + userid + " or approvedstatus=1").Tables(0)
        DT = DB.ExecuteDataSet("select bsct.Description,bsct.ChecklistID,bsct.HisID,bsct.remarks,case when bsct.frequency = 1 then 'Daily' when bsct.frequency = 2 then 'Weekly' " +
                                "when bsct.frequency  = 4 then 'Monthly' when bsct.frequency  = 5 then 'Quarterly' when bsct.frequency  = 6 then 'Half Yearly' " +
                                "when bsct.frequency  = 7 then 'Yearly' when bsct.frequency  = 3 then 'Fortnightly' end as frequency,bsct.frequency ,case when bsct.status_id=0 then 'No task available' " +
                                "when bsct.status_id=1 then 'Checklist active' when bsct.status_id=-1 then 'Checklist Inactive' when bsct.status_id=999 then 'Checklist Deleted' end as status,bsct.status_id, " +
                                "case when bsct.ApprovedStatus=2 then 'Rejected' when bsct.ApprovedStatus=0 then 'No' when bsct.ApprovedStatus=1 then 'Yes' end as appsts " +
                                ",bsct.AffectedOn,bsct.interval from surve_chklist_tracker bsct  " +
                                "inner join surve_chklist_master bscm on bsct.hisid=bscm.hisid  " +
                                "where ((bsct.makerid=" + userid + " or bsct.approvedstatus=1) and bscm.status_id<>999)").Tables(0)
        For n As Integer = 0 To DT.Rows.Count - 1
            StrGrpNam += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6) & "µ" & DT.Rows(n)(7) & "µ" & DT.Rows(n)(8) & "µ" & DT.Rows(n)(9) & "µ" & DT.Rows(n)(10)

            If n < DT.Rows.Count - 1 Then
                StrGrpNam += "¥"
            End If
        Next
        Return StrGrpNam
    End Function
    Public Function GetChecklistData() As DataTable
        Return DB.ExecuteDataSet("select '-1' as Checklist_ID ,' ------Select------' as Checklist_Name Union all Select ChecklistID,Description from branch_surveillance_checklist where Status_ID=0").Tables(0)
    End Function
    Public Function GetChecklistDataTable(ByVal CheckListIDNo As Integer) As DataTable
        Return DB.ExecuteDataSet("Select ChecklistID,Description from surve_chklist_tracker where ChecklistID=" + CheckListIDNo.ToString() + "").Tables(0)
    End Function
    Public Function GetTasklistDataTable(ByVal TaskListIDNo As Integer) As DataTable
        Return DB.ExecuteDataSet("Select TaskDesc,TaskID from surve_task_tracker where  TaskID=" + TaskListIDNo.ToString() + "").Tables(0)
    End Function
    'Public Function GetTasklist(ByVal HisID As Integer, ByVal userid As String) As String
    '    Dim DT1 As New DataTable
    '    DT1 = DB.ExecuteDataSet("select checklistid from surve_chklist_tracker where hisid=" + HisID.ToString() + "").Tables(0)
    '    Dim checklistid As Integer = DT1.Rows(0).Item(0)
    '    Dim DT As New DataTable
    '    Dim StrGrpNam As String = ""
    '    'DT = DB.ExecuteDataSet("select TaskDesc,TaskID,remarks,weightage,frequency,case when frequency = 0 then 'Daily' when frequency = 1 then 'Weekly' when frequency = 2 then 'Monthly' when frequency = 3 then 'Quarterly' when frequency = 4 then 'Half Yearly' when frequency = 5 then 'Yearly' end as frequencyval,case when status_id=0 then 'Task not approved' when status_id=1 then 'Task active' when status_id=-1 then 'Task inactive' end as status,status_id,case when approvedstatus=0 then 'No' when approvedstatus=1 then 'Yes' end as appstatus from branch_surveillance_task_tracker where checklistid=" + checklistid.ToString() + " and status_id<>999 and makerid=" + userid + "").Tables(0)
    '    DT = DB.ExecuteDataSet("select bstt.TaskDesc,bstt.TaskID,bstt.remarks,bstt.weightage,bstt.frequency,case when bstt.frequency = 0 then 'Daily' " +
    '                            "when bstt.frequency = 1 then 'Weekly' when bstt.frequency = 2 then 'Monthly' " +
    '                            "when bstt.frequency = 3 then 'Quarterly' when bstt.frequency = 4 then 'Half Yearly' " +
    '                            "when bstt.frequency = 5 then 'Yearly' end as frequencyval,case when bstt.status_id=0 then 'Task not approved' " +
    '                            "when bstt.status_id=1 then 'Task active' when bstt.status_id=-1 then 'Task inactive' when bstt.status_id=999 then 'Task deleted' when bstt.status_id=100 then 'Task just added'end as status,bstt.status_id, " +
    '                            "case when bstt.approvedstatus=0 then 'No' when bstt.approvedstatus=1 then 'Yes' when bstt.approvedstatus=2 then 'Rejected' when bstt.approvedstatus=100 then 'Task not added in Approval queue' end as appstatus " +
    '                            "from surve_task_tracker bstt " +
    '                            "inner join surve_task_master bstm on bstt.hisid= bstm.hisid and bstt.taskid=bstm.taskid " +
    '                            "where (bstt.checklistid=" + checklistid.ToString() + ") and ((bstt.makerid=" + userid + " or bstt.approvedstatus=1) and bstm.status_id<>999)").Tables(0)
    '    For n As Integer = 0 To DT.Rows.Count - 1
    '        StrGrpNam += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6) & "µ" & DT.Rows(n)(7) & "µ" & DT.Rows(n)(8)

    '        If n < DT.Rows.Count - 1 Then
    '            StrGrpNam += "¥"
    '        End If
    '    Next
    '    Return StrGrpNam
    'End Function
    Public Function GetActiveTasklist(ByVal checklistid As Integer, ByVal userid As String) As String
        Dim DT As New DataTable
        Dim StrGrpNam As String = ""
        'DT = DB.ExecuteDataSet("select TaskDesc,TaskID,remarks,weightage,frequency,case when frequency = 0 then 'Daily' when frequency = 1 then 'Weekly' when frequency = 2 then 'Monthly' when frequency = 3 then 'Quarterly' when frequency = 4 then 'Half Yearly' when frequency = 5 then 'Yearly' end as frequencyval,case when status_id=0 then 'Task not approved' when status_id=1 then 'Task active' when status_id=-1 then 'Task inactive' end as status,status_id,case when approvedstatus=0 then 'No' when approvedstatus=1 then 'Yes' end as appstatus from branch_surveillance_task_tracker where checklistid=" + checklistid.ToString() + " and status_id<>999 and makerid=" + userid + "").Tables(0)
        DT = DB.ExecuteDataSet("select bstt.TaskDesc,bstt.TaskID,bstt.remarks,bstt.weightage,bstt.frequency,case when bstt.frequency = 0 then 'Daily' " +
                                "when bstt.frequency = 1 then 'Weekly' when bstt.frequency = 2 then 'Monthly' " +
                                "when bstt.frequency = 3 then 'Quarterly' when bstt.frequency = 4 then 'Half Yearly' " +
                                "when bstt.frequency = 5 then 'Yearly' end as frequencyval,case when bstt.status_id=0 then 'Task not approved' " +
                                "when bstt.status_id=1 then 'Task active' when bstt.status_id=-1 then 'Task inactive' end as status,bstt.status_id, " +
                                "case when bstt.approvedstatus=0 then 'No' when bstt.approvedstatus=1 then 'Yes' end as appstatus " +
                                "from surve_task_tracker bstt " +
                                "inner join surve_task_master bstm on bstt.hisid= bstm.hisid " +
                                "where bstm.checklistid=" + checklistid.ToString() + " and bstm.status_id=1 and bstm.approvedstatus=1").Tables(0)
        For n As Integer = 0 To DT.Rows.Count - 1
            StrGrpNam += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6) & "µ" & DT.Rows(n)(7) & "µ" & DT.Rows(n)(8)

            If n < DT.Rows.Count - 1 Then
                StrGrpNam += "¥"
            End If
        Next
        Return StrGrpNam
    End Function
    Public Function DeleteChecklist(ByVal Tabs As Integer, ByVal GroupId As Integer, ByVal userid As String) As Integer
        Dim status As Integer = 0
        If Tabs = 1 Then
            Dim DTTASK As New DataTable
            DTTASK = DB.ExecuteDataSet("select * from Surve_ChkList_Master scm inner join surve_task_master stm on stm.checklistid=scm.checklistid where(scm.checklistid = " + GroupId.ToString() + ")").Tables(0)
            Dim DT1 As New DataTable
            DT1 = DB.ExecuteDataSet("select hisid,approvedstatus from surve_chklist_master where checklistid=" + GroupId.ToString() + "").Tables(0)
            Dim hisid As Integer = DT1.Rows(0).Item(0)
            Dim Appstatus As Integer = DT1.Rows(0).Item(1)
            'If Appstatus = 0 Or DTTASK.Rows.Count = 0 Then
            '    status = DB.ExecuteNonQuery("UPDATE surve_chklist_tracker SET Status_ID=999 where hisid=" & hisid & "").ToString
            '    status = DB.ExecuteNonQuery("UPDATE surve_chklist_master SET Status_ID=999 where hisid=" & hisid & "").ToString
            If Appstatus = 1 Then
                Dim DT2 As New DataTable
                DT2 = DB.ExecuteDataSet("select hisid,approvedstatus from surve_chklist_tracker where hisid=" + hisid.ToString() + "").Tables(0)
                'Dim hisid As Integer = DT1.Rows(0).Item(0)
                Dim Appstatustrack As Integer = DT2.Rows(0).Item(1)
                If Appstatustrack = 0 Then
                    status = DB.ExecuteNonQuery("UPDATE surve_chklist_tracker SET Status_ID=999 where hisid=" & hisid & "").ToString
                ElseIf Appstatustrack = 1 Then
                    Dim DTMAX As New DataTable
                    DTMAX = DB.ExecuteDataSet("select Max(HisID) from surve_chklist_tracker").Tables(0)
                    Dim hismax As Integer = DTMAX.Rows(0).Item(0)
                    hismax = hismax + 1
                    status = DB.ExecuteNonQuery("insert into surve_chklist_tracker select " & hismax & ",checklistid,description,999,remarks,frequency, " +
                                                "Affectedon,interval,0," & userid & ", " +
                                                "getdate(),NULL,NULL from surve_chklist_tracker where hisid=" & hisid & "").ToString
                    status = DB.ExecuteNonQuery("UPDATE surve_chklist_master SET hisid=" & hismax & " where hisid=" & hisid & "").ToString
                End If
            End If
        ElseIf Tabs = 2 Then
            Dim DT3 As New DataTable
            DT3 = DB.ExecuteDataSet("select hisid,approvedstatus from surve_task_master where taskid=" + GroupId.ToString() + "").Tables(0)
            Dim hisidd As Integer = DT3.Rows(0).Item(0)
            Dim Appstatuss As Integer = DT3.Rows(0).Item(1)
            If Appstatuss = 0 Or Appstatuss = 100 Then
                status = DB.ExecuteNonQuery("UPDATE surve_task_master SET Status_ID=999 where hisid=" & hisidd & "").ToString
                status = DB.ExecuteNonQuery("UPDATE surve_task_tracker SET Status_ID=999 where hisid=" & hisidd & "").ToString
            ElseIf Appstatuss = 1 Then
                Dim DT2 As New DataTable
                DT2 = DB.ExecuteDataSet("select hisid,approvedstatus from surve_task_tracker where hisid=" + hisidd.ToString() + "").Tables(0)
                'Dim hisid As Integer = DT1.Rows(0).Item(0)
                Dim Appstatustrack As Integer = DT2.Rows(0).Item(1)
                If Appstatustrack = 0 Or Appstatuss = 100 Then
                    status = DB.ExecuteNonQuery("UPDATE surve_task_tracker SET Status_ID=999 where hisid=" & hisidd & "").ToString
                ElseIf Appstatustrack = 1 Then
                    Dim DTMAX As New DataTable
                    DTMAX = DB.ExecuteDataSet("select Max(HisID) from surve_task_tracker").Tables(0)
                    Dim hismax As Integer = DTMAX.Rows(0).Item(0)
                    hismax = hismax + 1
                    status = DB.ExecuteNonQuery("insert into surve_task_tracker select " & hismax & ",taskid,checklistid,taskdesc,999,remarks,weightage,frequency,0," & userid & ", " +
                                                "getdate(),NULL,NULL from surve_task_tracker where hisid=" & hisidd & "").ToString
                    status = DB.ExecuteNonQuery("UPDATE surve_task_master SET hisid=" & hismax & " where hisid=" & hisidd & "").ToString
                End If
            End If
        Else
            'Return DB.ExecuteNonQuery("UPDATE HD_PROBLEM_TYPE SET Status_ID=0 where PROBLEM_ID=" & GroupId & "").ToString
        End If
        Return status
    End Function
    Public Function GetActiveChecklistData(ByVal userid As String) As DataTable
        Return DB.ExecuteDataSet("select '-1' as Hisid ,' ------Select------' as Checklist_Name Union all select bsct.hisid,bsct.Description " +
                                 "from surve_chklist_tracker bsct  " +
                                 "inner join surve_chklist_master bscm on bsct.hisid=bscm.hisid  " +
                                 "where (bsct.makerid=" + userid + " or bsct.approvedstatus=1) and bsct.status_id<>-1 and bsct.status_id<>999").Tables(0)
        'Return DB.ExecuteDataSet("select '-1' as Hisid ,' ------Select------' as Checklist_Name Union all Select Hisid,Description from Branch_Surveillance_ChkList_Tracker where status_id<>-1").Tables(0)
    End Function
    Public Function GetSumWeightageValue(ByVal HisID As Integer) As DataTable
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("select checklistid from surve_chklist_tracker where hisid=" + HisID.ToString() + "").Tables(0)
        Dim checklistid As Integer = CInt(DT.Rows(0).Item(0))
        Return DB.ExecuteDataSet("select sum(bstt.weightage) from surve_task_tracker bstt " +
                                 "inner join surve_task_master bstm on bstt.hisid=bstm.hisid and bstm.taskid=bstt.taskid " +
                                 "where bstt.checklistid=" + checklistid.ToString() + " and bstt.status_id<>-1 and bstt.status_id<>999 and bstt.approvedstatus<>2").Tables(0)
    End Function
    Public Function GetValidChecklistData() As DataTable
        Return DB.ExecuteDataSet("select '-1' as Checklist_ID ,' ------Select------' as Checklist_Name Union all Select bst.ChecklistID,bsc.Description from surve_chklist_tracker bsc " +
                                 "inner join surve_chklist_master scm on bsc.hisid = scm.hisid " +
                                 "inner join surve_task_tracker bst on bsc.checklistid = bst.checklistid " +
                                 "where(bsc.status_id = 1 And bsc.approvedstatus = 1 And bst.approvedstatus = 0) " +
                                 "group by bst.ChecklistID,bsc.Description").Tables(0)
    End Function
    Public Function GetTasklistForApproval(ByVal CheckListID As Integer) As String
        Dim DT As New DataTable
        Dim StrGrpNam As String = ""
        DT = DB.ExecuteDataSet("select TaskDesc,TaskID,remarks,weightage,frequency,case when frequency = 0 then 'Daily' when frequency = 1 then 'Weekly' when frequency = 2 then 'Monthly' when frequency = 3 then 'Quarterly' when frequency = 4 then 'Half Yearly' when frequency = 5 then 'Yearly' end as frequencyval,case when status_id=0 then 'Task not approved' when status_id=1 then 'Task Active' when status_id=-1 then 'Task Inactive' when status_id=999 then 'Task deleted' end as status from surve_task_tracker where checklistid=" + CheckListID.ToString() + " and approvedstatus=0").Tables(0)
        For n As Integer = 0 To DT.Rows.Count - 1
            StrGrpNam += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6)

            If n < DT.Rows.Count - 1 Then
                StrGrpNam += "¥"
            End If
        Next
        Return StrGrpNam
    End Function
    Public Function GetPendingChecklist() As String
        Dim DT As New DataTable
        Dim StrGrpNam As String = ""
        DT = DB.ExecuteDataSet("select Description,bsc.HisId,bsc.remarks,case when bsc.frequency = 1 then 'Daily' " +
                                "when bsc.frequency = 2 then 'Weekly' when bsc.frequency = 3 then 'Fortnightly' " +
                                "when bsc.frequency = 4 then 'Monthly' when bsc.frequency = 5 then 'Quarterly' " +
                                "when bsc.frequency = 6 then 'HalfYearly' when bsc.frequency = 7 then 'Yearly' end as frequency,bsc.frequency, " +
                                "case when bsc.status_id=0 then 'No task available' when bsc.status_id=1 then 'Checklist active' " +
                                "when bsc.status_id=-1 then 'Checklist Inactive' when bsc.status_id=999 then 'Checklist Deleted'end as status,bsc.status_id," +
                                "case when bsc.ApprovedStatus=0 then 'No' when bsc.ApprovedStatus=1 then 'Yes' end as appsts " +
                                ",bsc.affectedon,bsc.interval from surve_chklist_tracker bsc " +
                                "where bsc.approvedstatus=0 group by bsc.HisId,Description,bsc.remarks,bsc.frequency,bsc.status_id,bsc.ApprovedStatus,bsc.affectedon,bsc.interval").Tables(0)
        For n As Integer = 0 To DT.Rows.Count - 1
            StrGrpNam += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6) & "µ" & DT.Rows(n)(7) & "µ" & DT.Rows(n)(8) & "µ" & DT.Rows(n)(9)

            If n < DT.Rows.Count - 1 Then
                StrGrpNam += "¥"
            End If
        Next
        Return StrGrpNam
    End Function
    Public Function CheckRetail(Branch_id As Integer) As Boolean
        Dim DT As New DataTable
        ''---------- Only available for RB 

        DT = DB.ExecuteDataSet("Select isnull(Branch_type,0) from branch_master where branch_id = " + Branch_id.ToString() + "").Tables(0)

        If DT.Rows(0)(0) = 1 Or DT.Rows(0)(0) = 3 Then
            Return True
        Else
            Return False
        End If




    End Function
    Public Function GetBranchData(ByRef DT As DataTable) As String '-------------N
        Dim StrEmployee As String = ""
        For n As Integer = 0 To DT.Rows.Count - 1

            StrEmployee += "¥"
            StrEmployee += DT.Rows(n)(0) & "^" & DT.Rows(n)(1) & "^" & DT.Rows(n)(2) & "^" & DT.Rows(n)(3) & "^" & DT.Rows(n)(4) & "^" & DT.Rows(n)(5) & "^" & DT.Rows(n)(6)
        Next
        Return StrEmployee
    End Function



    Public Function GetBranchDataIT(ByRef DT As DataTable) As String '-------------N
        Dim StrEmployee As String = ""
        For n As Integer = 0 To DT.Rows.Count - 1

            StrEmployee += "¥"
            StrEmployee += DT.Rows(n)(0) & "^" & DT.Rows(n)(1) & "^" & DT.Rows(n)(2) & "^" & DT.Rows(n)(3) & "^" & DT.Rows(n)(4) & "^" & DT.Rows(n)(5) & "^" & DT.Rows(n)(6) & "^" & DT.Rows(n)(7)
        Next
        Return StrEmployee
    End Function


    Public Function GetBranchDataITP2P(ByRef DT As DataTable) As String '-------------N
        Dim StrEmployee As String = ""
        For n As Integer = 0 To DT.Rows.Count - 1
            StrEmployee += "¥"
            StrEmployee += DT.Rows(n)(0) & "^" & DT.Rows(n)(1) & "^" & DT.Rows(n)(2) & "^" & DT.Rows(n)(3) & "^" & DT.Rows(n)(4) & "^" & DT.Rows(n)(5)
        Next
        Return StrEmployee
    End Function




    Public Function CheckdeptUser(ByVal EmpCode As Integer, ByVal Teamid As Integer) As Boolean
        If EmpCode = 0 Then
            Return False
        End If
        Dim Access As Boolean = False
        Dim DT As New DataTable
        DT = DB.ExecuteDataSet("select COUNT(*) from bo_team_dtl WHERE EMP_CODE = " & EmpCode & " AND team_id = " & Teamid & "").Tables(0)
        If CInt(DT.Rows(0)(0)) > 0 Then
            Access = True
        End If
        Return Access
    End Function

    Public Function GetTasklist(ByVal HisID As Integer, ByVal userid As String) As String
        Dim DT1 As New DataTable
        DT1 = DB.ExecuteDataSet("select checklistid from surve_chklist_tracker where hisid=" + HisID.ToString() + "").Tables(0)
        Dim checklistid As Integer = DT1.Rows(0).Item(0)
        Dim DT As New DataTable
        Dim StrGrpNam As String = ""
        'DT = DB.ExecuteDataSet("select TaskDesc,TaskID,remarks,weightage,frequency,case when frequency = 0 then 'Daily' when frequency = 1 then 'Weekly' when frequency = 2 then 'Monthly' when frequency = 3 then 'Quarterly' when frequency = 4 then 'Half Yearly' when frequency = 5 then 'Yearly' end as frequencyval,case when status_id=0 then 'Task not approved' when status_id=1 then 'Task active' when status_id=-1 then 'Task inactive' end as status,status_id,case when approvedstatus=0 then 'No' when approvedstatus=1 then 'Yes' end as appstatus from branch_surveillance_task_tracker where checklistid=" + checklistid.ToString() + " and status_id<>999 and makerid=" + userid + "").Tables(0)
        DT = DB.ExecuteDataSet("select bstt.TaskDesc,bstt.TaskID,bstt.remarks,bstt.weightage,bstt.frequency,case when bstt.frequency = 0 then 'Daily' " +
                                "when bstt.frequency = 1 then 'Weekly' when bstt.frequency = 2 then 'Monthly' " +
                                "when bstt.frequency = 3 then 'Quarterly' when bstt.frequency = 4 then 'Half Yearly' " +
                                "when bstt.frequency = 5 then 'Yearly' end as frequencyval,case when bstt.status_id=0 then 'Task not approved' " +
                                "when bstt.status_id=1 then 'Task active' when bstt.status_id=-1 then 'Task inactive' when bstt.status_id=999 then 'Task deleted' when bstt.status_id=100 then 'Task just added'end as status,bstt.status_id, " +
                                "case when bstt.approvedstatus=0 then 'No' when bstt.approvedstatus=1 then 'Yes' when bstt.approvedstatus=2 then 'Rejected' when bstt.approvedstatus=100 then 'Task not added in Approval queue' end as appstatus " +
                                ",bstt.Is_mandatory from surve_task_tracker bstt " +
                                "inner join surve_task_master bstm on bstt.hisid= bstm.hisid and bstt.taskid=bstm.taskid " +
                                "where (bstt.checklistid=" + checklistid.ToString() + ") and ((bstt.makerid=" + userid + " or bstt.approvedstatus=1) and bstm.status_id<>999)").Tables(0)
        For n As Integer = 0 To DT.Rows.Count - 1
            StrGrpNam += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & DT.Rows(n)(4) & "µ" & DT.Rows(n)(5) & "µ" & DT.Rows(n)(6) & "µ" & DT.Rows(n)(7) & "µ" & DT.Rows(n)(8) & "µ" & DT.Rows(n)(9)

            If n < DT.Rows.Count - 1 Then
                StrGrpNam += "¥"
            End If
        Next
        Return StrGrpNam
    End Function
    Public Function GetVCStatus() As DataTable
        Return DB.ExecuteDataSet("SELECT '-1' AS Status_id,'-----All-----' AS Status_name UNION ALL SELECT distinct Status_id,Status_name from vc_status_master").Tables(0)
    End Function
    Public Function WEB_Request_Response(ByVal Request As String, ByVal Request_type As Integer) As String()

        'Added On 22-10-2020 by 40013 - Changed request from IT
        Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
        Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString + "&indiaDltPrincipalEntityId=1001132201672101393"
        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        If Request_type = 1 Then
            delimiters = {"<", ">", ">/", "</", "/"}
        Else
            'delimiters = {"<", ">", ">/", "</"}
            delimiters = {"=", " "}
        End If
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)


        Return parts.ToArray()

        ' ''Dim json As String = "[" + parts(1) + "]"
        ' ''Dim js As JavaScriptSerializer = New JavaScriptSerializer()

        ' ''Dim SendDatas() As SendData = js.Deserialize(Of SendData())(json)
        '' ''Dim e As IEnumerable(Of SendData) = SendDatas()
        '' ''Dim SendDatatemp As SendData
        ' ''Return SendDatas.ToArray()


        '' ''Dim array() As SendData = SendDatas.ToArray()
        '' ''Dim e As IEnumerable(Of SendData) = SendDatas.ToArray()
        '' ''Return e.ToArray()


    End Function

    'Public Function WEB_Request_Response_NEW(ByVal UserName As String, ByVal PassWord As String, ByVal Sender As String, ByVal SMSText As String, ByVal Mobileno As String, ByVal Request_type As Integer) As String()

    '    '  Request = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "Your OTP to change password is " & CreatedOTP & "&GSM=" & Mobileno & ""
    '    'Added On 22-10-2020 by 40013 - Changed request from IT
    '    Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
    '    Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString + "&indiaDltPrincipalEntityId=1001132201672101393"
    '    Dim dataString As String = ""
    '    Dim ss As String = ""
    '    Dim delimiters() As String
    '    Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
    '    Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
    '        Using stream As Stream = httpResponse.GetResponseStream()
    '            dataString = (New StreamReader(stream)).ReadToEnd()
    '        End Using
    '    End Using
    '    ss = dataString
    '    If Request_type = 1 Then
    '        delimiters = {"<", ">", ">/", "</", "/"}
    '    Else
    '        'delimiters = {"<", ">", ">/", "</"}
    '        delimiters = {"=", " "}
    '    End If
    '    Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)


    '    Return parts.ToArray()

    '    ' ''Dim json As String = "[" + parts(1) + "]"
    '    ' ''Dim js As JavaScriptSerializer = New JavaScriptSerializer()

    '    ' ''Dim SendDatas() As SendData = js.Deserialize(Of SendData())(json)
    '    '' ''Dim e As IEnumerable(Of SendData) = SendDatas()
    '    '' ''Dim SendDatatemp As SendData
    '    ' ''Return SendDatas.ToArray()


    '    '' ''Dim array() As SendData = SendDatas.ToArray()
    '    '' ''Dim e As IEnumerable(Of SendData) = SendDatas.ToArray()
    '    '' ''Return e.ToArray()


    'End Function
    Public Function GeneralFunctionForTMR(ParameterId As String, Parameters As String) As DataTable
        Dim Params(3) As SqlParameter

        Params(0) = New SqlParameter("@p_Parameter_Id", SqlDbType.VarChar, 100)
        Params(0).Value = ParameterId
        Params(1) = New SqlParameter("@p_Parameters", SqlDbType.VarChar, 500)
        Params(1).Value = Parameters
        Params(2) = New SqlParameter("@p_ErrorStatus", SqlDbType.Int)
        Params(2).Direction = ParameterDirection.Output
        Params(3) = New SqlParameter("@p_OutputMessage", SqlDbType.VarChar, 5000)
        Params(3).Direction = ParameterDirection.Output
        Return DB.ExecuteDataSet("SP_TMR_QueryResult", Params).Tables(0)
    End Function

    Public Function GetESFBQueryResult(ParameterId As String, Parameters As String) As DataTable
        Dim Params(3) As SqlParameter

        Params(0) = New SqlParameter("@p_Parameter_Id", SqlDbType.VarChar, 100)
        Params(0).Value = ParameterId
        Params(1) = New SqlParameter("@p_Parameters", SqlDbType.VarChar, 500)
        Params(1).Value = Parameters
        Params(2) = New SqlParameter("@p_ErrorStatus", SqlDbType.Int)
        Params(2).Direction = ParameterDirection.Output
        Params(3) = New SqlParameter("@p_OutputMessage", SqlDbType.VarChar, 5000)
        Params(3).Direction = ParameterDirection.Output
        Return DB.ExecuteDataSet("SP_ESFB_QueryResult", Params).Tables(0)
    End Function

    'Added on 02-Jan-2020 for SMS change vendor API integration
    'Parameters Bind with ^
    Public Function WEB_Request_Response(mobileNo As String, parameters As String, smsId As Integer, Optional otpStat As Integer = 0) As String()
        Dim Request_type As Integer = 1
        Dim StrUrl As String
        Dim Request As String
        Dim Sender As String = ""
        Dim UserName As String = ""
        Dim PassWord As String = ""
        Dim templateId As String = ""
        Dim template As String = ""
        Dim message As String = ""
        Dim tempMessage As String = ""
        Dim cnt As Integer = 0
        ' To get SMS vendor Details
        Dim DT As DataTable = DB.ExecuteDataSet("SELECT sender,username,password,otp_sender FROM SMS_PORTAL WHERE STATUS_ID =1").Tables(0)
        If DT.Rows.Count > 0 Then
            If otpStat = 1 Then
                Sender = DT.Rows(0).Item(3).ToString()
            Else
                Sender = DT.Rows(0).Item(0).ToString()
            End If
            UserName = DT.Rows(0).Item(1).ToString()
            PassWord = DT.Rows(0).Item(2).ToString()
        End If
        ' To get SMS Templates Details
        Dim DT1 As DataTable = DB.ExecuteDataSet("select template_id,template_message from SMS_TEMPLATES where sms_id='" & smsId & "' and status_id=1").Tables(0)
        If DT1.Rows.Count > 0 Then
            templateId = DT1.Rows(0).Item(0).ToString()
            template = DT1.Rows(0).Item(1).ToString()
            Dim strData() As String = template.Split({"{#var#}"}, StringSplitOptions.RemoveEmptyEntries)
            Dim parData() As String = parameters.Split({"^"}, StringSplitOptions.RemoveEmptyEntries)
            cnt = parData.Count()
            For i As Integer = 0 To cnt
                If i = cnt Then
                    tempMessage = strData(i).ToString
                Else
                    tempMessage = strData(i).ToString + parData(i).ToString
                End If
                message = message + tempMessage
            Next
        End If
        '//------------Call New SMS  API------------------//
        StrUrl = "https://392qv.api.infobip.com/sms/1/text/query?username=" & UserName & "&password=" & PassWord & "&from=" & Sender & "&to=" & mobileNo & "&text=" & message & "&indiaDltContentTemplateId=" & templateId & "&indiaDltPrincipalEntityId=1001132201672101393"
        Request = StrUrl
        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        delimiters = {"{", "[", "]", "}", ",", ":"}
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)
        Return parts.ToArray()
        '//------------End of New Call SMS API------------------//

        '//------------Call old SMS  API------------------//
        'StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & message & "&GSM=" & mobileNo & ""
        'Request = StrUrl
        'Request = HttpUtility.UrlEncode(StrUrl)
        'Dim dataString As String = ""
        'Dim ss As String = ""
        'Dim delimiters() As String
        'Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        'Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
        '    Using stream As Stream = httpResponse.GetResponseStream()
        '        dataString = (New StreamReader(stream)).ReadToEnd()
        '    End Using
        'End Using
        'ss = dataString
        'If Request_type = 1 Then
        '    delimiters = {"<", ">", ">/", "</", "/"}
        'Else
        '    'delimiters = {"<", ">", ">/", "</"}
        '    delimiters = {"=", " "}
        'End If
        'Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)
        'Return parts.ToArray()
        '//------------End of old Call SMS API------------------//

    End Function

End Class
