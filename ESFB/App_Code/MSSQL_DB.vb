Imports System.Data
Imports System.Data.SqlClient
Namespace MS_SQL
    Public Class Connect
        Private strConnectionString As String
        Public con As System.Data.IDbConnection = Nothing
        Dim sServer As String = Nothing
        Dim sDataBase As String = Nothing
        Dim sUser As String = Nothing
        Dim sPassword As String = Nothing
        Sub New()
            Dim ConnectedToServer As Boolean = False

            If ConnectedToServer = True Then
                sServer = "10.0.25.12"
                sDataBase = "ESFB"
                sUser = "appadmin"
                sPassword = "Esaf@admin2020"
            Else
                sServer = "10.65.0.71"
                sDataBase = "ESFB"
                sUser = "appadmin"
                sPassword = "MNBvcx@123#"
            End If

            'If ConnectedToServer = True Then
            '    sServer = "10.0.25.12"
            '    sDataBase = "ESFB"
            '    sUser = "appadmin"
            '    sPassword = "Esaf@admin2020"
            'Else
            '    sServer = "10.65.0.73"
            '    sDataBase = "ESFB"
            '    sUser = "7931"
            '    sPassword = "QAZwsx@1234"
            'End If

            SqlConnection.ClearAllPools()
            Dim con As System.Data.IDbConnection = Nothing
            strConnectionString = String.Format("Data Source={0};Database={1};User ID={2};Password={3}", sServer, sDataBase, sUser, sPassword)
        End Sub
        Public Function ExecuteDataSet(ByVal query As String) As DataSet
            con = New SqlConnection()
            con.ConnectionString = strConnectionString
            Dim cmd = New SqlCommand(query, con)
            cmd.CommandType = CommandType.Text
            con.Open()
            Dim da As New SqlDataAdapter()
            da.SelectCommand = cmd
            Dim ds As New DataSet()
            da.Fill(ds)
            con.Close()
            Return ds
        End Function
        'Public Function ExecuteDataSet(ByVal query As String, ByVal parameters_value As SqlParameter()) As DataSet
        '    Dim con As New SqlConnection(strConnectionString)
        '    Dim cmd As New SqlCommand(query, con)
        '    cmd.CommandType = CommandType.StoredProcedure
        '    cmd.CommandTimeout = 0
        '    Dim i As Integer
        '    For i = 0 To parameters_value.Length - 1
        '        cmd.Parameters.Add(parameters_value(i))
        '    Next
        '    Dim da As New SqlDataAdapter()
        '    da.SelectCommand = cmd
        '    Dim ds As New DataSet()
        '    da.Fill(ds)
        '    con.Close()
        '    Return ds
        'End Function
        'Modified on 13-oct-2020 by 40013 for VAPT changes
        Public Function ExecuteDataSet(ByVal query As String, ByVal parameters_value As SqlParameter(), Optional ByVal CmdType As Integer = 0) As DataSet
            Dim con As New SqlConnection(strConnectionString)
            Dim cmd As New SqlCommand(query, con)
            If CmdType = 0 Then
                cmd.CommandType = CommandType.StoredProcedure
            ElseIf CmdType = 1 Then
                cmd.CommandType = CommandType.Text
            End If
            cmd.CommandTimeout = 0
            Dim i As Integer
            For i = 0 To parameters_value.Length - 1
                cmd.Parameters.Add(parameters_value(i))
            Next
            Dim da As New SqlDataAdapter()
            da.SelectCommand = cmd
            Dim ds As New DataSet()
            da.Fill(ds)
            con.Close()
            Return ds
        End Function
        Public Function ExecuteDataSetLongTime(ByVal query As String, ByVal parameters_value As SqlParameter()) As DataSet
            Dim con As New SqlConnection(strConnectionString)
            Dim cmd As New SqlCommand(query, con)
            cmd.CommandTimeout = 0
            cmd.CommandType = CommandType.StoredProcedure
            Dim i As Integer
            For i = 0 To parameters_value.Length - 1
                cmd.Parameters.Add(parameters_value(i))
            Next
            Dim da As New SqlDataAdapter()
            da.SelectCommand = cmd
            Dim ds As New DataSet()
            da.Fill(ds)
            con.Close()
            Return ds
        End Function

        Public Function ExecuteNonQuery(ByVal query As String, ByVal parameters_value As SqlParameter()) As Integer
            Dim con As New SqlConnection(strConnectionString)
            Dim cmd As New SqlCommand(Query, con)
            cmd.CommandType = CommandType.StoredProcedure
            Dim i As Integer
            For i = 0 To parameters_value.Length - 1
                cmd.Parameters.Add(parameters_value(i))
            Next
            con.Open()
            Dim retval As Integer
            retval = cmd.ExecuteNonQuery()
            con.Close()
            Return retval
        End Function
        Public Function ExecuteNonQuery(ByVal query As String) As Integer
            Try
                Dim con As New SqlConnection(strConnectionString)
                Dim cmd As New SqlCommand(query, con)
                If (query.StartsWith("INSERT") Or query.StartsWith("insert") Or query.StartsWith("UPDATE") Or query.StartsWith("update") Or query.StartsWith("DELETE") Or query.StartsWith("delete") Or query.StartsWith("exec")) Then
                    cmd.CommandType = CommandType.Text
                Else
                    cmd.CommandType = CommandType.StoredProcedure
                End If
                Dim retval As Integer
                Try
                    con.Open()
                    retval = cmd.ExecuteNonQuery()
                Catch exp As Exception
                    Throw exp
                Finally
                    If (con.State = ConnectionState.Open) Then
                        con.Close()
                    End If
                End Try
                Return retval
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub dispose()
            Me.Finalize()
        End Sub

        Sub Open()
            Throw New NotImplementedException
        End Sub

        Sub Close()
            Throw New NotImplementedException
        End Sub

    End Class
End Namespace

