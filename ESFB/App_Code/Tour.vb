﻿Imports Microsoft.VisualBasic
Imports System.Data

Public Class Tour
    Dim DB As New MS_SQL.Connect
    Public Function Getexist_tour(ByVal userid As Integer, ByVal statusid As Integer) As DataTable
        Dim strfield As String = ""

        If statusid = 1 Then
            strfield = "+'Ø'+T.approved_remarks"
        End If

        Return DB.ExecuteDataSet("select '-1' as reqid,' ------Select------' as data Union All select convert(varchar,T.M_request_id)+'Ø'+convert(varchar,datediff(day,T.tour_from ,t.tour_to)+1)+'Ø'+convert(varchar(11),T.tour_from,106)+'Ø'+convert(varchar(11),t.tour_to,106)" & strfield & " as reqid,E.Emp_Name+'('+convert(varchar,T.userid) +') |('+convert(varchar(11),T.tour_from,106) " & _
            "+ '  To ' +convert(varchar(11),t.tour_to,106) +')'AS data from [EMP_TOUR_MASTER] T INNER JOIN EMP_MASTER E ON T.userid=E.emp_code where T.userid in (select emp_code from EMP_MASTER where " & _
 " reporting_to = " & userid & ") And T.status_id = " & statusid & "").Tables(0)
    End Function
    Public Function Getexist_tourself(ByVal userid As Integer, ByVal statusid As Integer) As DataTable

        Return DB.ExecuteDataSet("select '-1' as reqid,' ------Select------' as data Union All select convert(varchar,T.M_request_id)+'Ø'+convert(varchar,datediff(day,T.tour_from ,t.tour_to)+1)+'Ø'+convert(varchar(11),T.tour_from,106)+'Ø'+convert(varchar(11),t.tour_to,106) as reqid,E.Emp_Name+'('+convert(varchar,T.userid) +') | '+convert(varchar(11),T.tour_from,106) " & _
            "+ '  To ' +convert(varchar(11),t.tour_to,106) +')'AS data from [EMP_TOUR_MASTER] T LEFT JOIN EMP_MASTER E ON T.userid=E.emp_code where T.userid=" & userid & " and T.status_id=" & statusid & "").Tables(0)
    End Function
    Public Function GetBranch() As DataTable
        Return DB.ExecuteDataSet("select '-1' as branchid,' ------Select------' as branch Union All select convert(varchar,branch_id) +'^'+Branch_name as branchid,Branch_Name as branch from BRANCH_MASTER where status_id = 1").Tables(0)
    End Function
    Public Function Getexist_tour_details(ByVal M_request_id As Integer) As DataTable
        Return DB.ExecuteDataSet("select location,convert(varchar(11),tour_start_date,106),convert(varchar(11),tour_end_date,106) ,purpose  from EMP_TOUR_MASTER a,EMP_TOUR b where a.M_request_id=b.M_request_id and a.M_request_id=" & M_request_id & "").Tables(0)
    End Function
End Class
