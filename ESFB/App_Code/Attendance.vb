﻿Imports Microsoft.VisualBasic
Imports System.Data
Public Class Attendance
    Dim DB As New MS_SQL.Connect
    Public Function Getexist_FS_Attendnce(ByVal branchid As Integer, ByVal TypeID As Integer) As DataTable
        If TypeID = 1 Then
            Return DB.ExecuteDataSet("select EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time from dbo.EMP_ATTEND EA INNER JOIN EMP_LIST EM ON EA.emp_code=EM.emp_code where EM.Emp_Type_ID=2 and EA.FS_approved_by IS NULL and EA.branch_id=" & branchid & "").Tables(0)
        ElseIf TypeID = 2 Then
            Return DB.ExecuteDataSet("select EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time from dbo.EMP_ATTEND EA INNER JOIN EMP_LIST EM ON EA.emp_code=EM.emp_code where  EM.POST_ID = 6 and EA.FS_approved_by IS NULL and EM.Reporting_To=" & branchid & "").Tables(0)
        ElseIf TypeID = 3 Then
            Return DB.ExecuteDataSet("select EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time from dbo.EMP_ATTEND EA INNER JOIN EMP_LIST EM ON EA.emp_code=EM.emp_code where  EM.POST_ID = 7 and EA.FS_approved_by IS NULL and EM.Reporting_To=" & branchid & "").Tables(0)
        Else
            Return DB.ExecuteDataSet("select EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time from dbo.EMP_ATTEND EA INNER JOIN EMP_LIST EM ON EA.emp_code=EM.emp_code where EM.Emp_Type_ID = 4 and EA.FS_approved_by IS NULL and EM.Reporting_To=" & branchid & "").Tables(0)
        End If

    End Function
    Public Function GetFS_Attendance_Request(ByRef DT As DataTable) As String
        Dim StrAttendance As String = ""
        For n As Integer = 0 To DT.Rows.Count - 1
            StrAttendance += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1) & "µ" & DT.Rows(n)(2) & "µ" & DT.Rows(n)(3) & "µ" & DT.Rows(n)(4)
            If n < DT.Rows.Count - 1 Then
                StrAttendance += "¥"
            End If
        Next
        Return StrAttendance
    End Function
    Public Function GetNotPunchingStaffdetails(ByVal userid As Integer) As DataTable
        Return DB.ExecuteDataSet("select a.Emp_Code,a.Emp_Name  from EMP_MASTER a,EMP_ATTEND b where a.Emp_Code=b.Emp_Code and E_Time is null " & _
                                 " and M_Time is null and reporting_to = " & userid & " and a.emp_code not in (select e.emp_code from EMP_LEAVE_VERIFY_MASTER d," & _
                                 " EMP_LEAVE_VERIFY e WHERE d.verify_id=e.verify_id and dateadd(ss,1,dateadd(dd,-1,dateadd(ss,-1,dateadd(dd,1,convert(datetime,convert(varchar(10),d.verify_date,101))))))='" & Format(Date.Now, "dd-MMM-yyyy") & "')").Tables(0)
    End Function
    Public Function GetDepartmentdetails(ByVal userid As Integer) As DataTable
        Return DB.ExecuteDataSet("select d.Department_Name   from EMP_MASTER a,DEPARTMENT_MASTER d where a.Department_ID =d.Department_ID  and a.Emp_Code = " & userid & "").Tables(0)
    End Function
    Public Function GetNotPunchingStaffdetails_data(ByRef DT As DataTable) As String
        Dim StrAttendance As String = ""
        For n As Integer = 0 To DT.Rows.Count - 1
            StrAttendance += DT.Rows(n)(0) & "µ" & DT.Rows(n)(1)
            If n < DT.Rows.Count - 1 Then
                StrAttendance += "¥"
            End If
        Next
        Return StrAttendance
    End Function
End Class
