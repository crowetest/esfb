﻿Imports Microsoft.VisualBasic
Imports System.Data

Public Class Leave
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim SM As New SMTP
    Public Function GetAvailLeaves(ByVal Emp_Code As Integer) As String
        Dim LeaveDtl As String
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@Emp_Code", Emp_Code), New System.Data.SqlClient.SqlParameter("@CLCount", 0.0), New System.Data.SqlClient.SqlParameter("@SLCount", 0.0), New System.Data.SqlClient.SqlParameter("@PLCount", 0.0), New System.Data.SqlClient.SqlParameter("@CMCount", 0.0), New System.Data.SqlClient.SqlParameter("@RHCount", 0.0), New System.Data.SqlClient.SqlParameter("@CLTaken", 0.0), New System.Data.SqlClient.SqlParameter("@SLTaken", 0.0), New System.Data.SqlClient.SqlParameter("@PLTaken", 0.0), New System.Data.SqlClient.SqlParameter("@CMTaken", 0.0), New System.Data.SqlClient.SqlParameter("@RHTaken", 0.0)}
            Parameters(1).Direction = ParameterDirection.InputOutput
            Parameters(2).Direction = ParameterDirection.InputOutput
            Parameters(3).Direction = ParameterDirection.InputOutput
            Parameters(4).Direction = ParameterDirection.InputOutput
            Parameters(5).Direction = ParameterDirection.InputOutput
            Parameters(6).Direction = ParameterDirection.InputOutput
            Parameters(7).Direction = ParameterDirection.InputOutput
            Parameters(8).Direction = ParameterDirection.InputOutput
            Parameters(9).Direction = ParameterDirection.InputOutput
            Parameters(10).Direction = ParameterDirection.InputOutput
            DB.ExecuteNonQuery("SP_GET_LEAVE_BALANCE", Parameters)
            LeaveDtl = Parameters(1).Value.ToString() + "~" + Parameters(2).Value.ToString() + "~" + Parameters(3).Value.ToString() + "~" + Parameters(4).Value.ToString() + "~" + Parameters(5).Value.ToString() + "~" + Parameters(6).Value.ToString() + "~" + Parameters(7).Value.ToString() + "~" + Parameters(8).Value.ToString() + "~" + Parameters(9).Value.ToString() + "~" + Parameters(10).Value.ToString()
        Catch ex As Exception
            LeaveDtl = ex.Message
        End Try
        Return LeaveDtl
    End Function
    Public Function Materinity_Leave_Details(ByVal Leave_Type As Integer, ByVal Emp_code As Integer, ByVal ReqID As Integer) As String
        Dim StrDtls As String = Nothing
        Dim DT1 As DataTable
        If ReqID <> 0 Then
            DT1 = DB.ExecuteDataSet("select * from emp_leave_request where emp_code=" & Emp_code & " and leave_type in (4,9) and year(Leave_From)=" & Year(Now.Date) & " and status_id < 5 and request_id<>" & ReqID & "").Tables(0)
        Else
            DT1 = DB.ExecuteDataSet("select * from emp_leave_request where emp_code=" & Emp_code & " and leave_type in (4,9) and year(Leave_From)=" & Year(Now.Date) & " and status_id < 5").Tables(0)
        End If
        If DT1.Rows.Count <> 0 Then
            StrDtls = "1ØMaternity leave already applied in this year"
        Else
            DT1 = DB.ExecuteDataSet("select maternity_L_days,how_many_times from EMP_MAERINITY_LEAVE_DETAILS where leave_type=" & Leave_Type & "").Tables(0)
            If DT1.Rows.Count <> 0 Then
                StrDtls = "2Ø" + DT1.Rows(0)(0).ToString + "Ø" + DT1.Rows(0)(1).ToString
            End If
        End If
        Return StrDtls


    End Function

    Public Function GetLeaveType(ByVal ESI_Flag As Integer) As DataSet
        If ESI_Flag = 1 Then
            Return DB.ExecuteDataSet("select -1 as Type_ID,'---------Select---------' as Type_name union all Select Type_ID,Type_name from EMP_LEAVE_TYPE where status_id=1 ORDER BY Type_ID")
        Else
            Return DB.ExecuteDataSet("select -1 as Type_ID,'---------Select---------' as Type_name union all Select Type_ID,Type_name from EMP_LEAVE_TYPE where status_id=1 and type_id not in (9,10) ORDER BY Type_ID ")
        End If
    End Function

    Public Function GetLeaveReason() As DataSet
        Return DB.ExecuteDataSet("select -1 as Reason_ID,'---------Select---------' as Reason union all Select Reason_ID,Reason from EMP_LEAVE_REASONS WHERE status_ID=1")
    End Function
    Public Function LeaveRequest(ByVal HdnValue As String, ByVal Attachment As Byte(), ByVal ContentType As String) As String
        Dim message As New String(" ", 1000)
        Dim arr As String() = HdnValue.Split("~")
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TrNo", 1),
                                                                      New System.Data.SqlClient.SqlParameter("@EmpCode", Integer.Parse(arr(0))),
                                                                      New System.Data.SqlClient.SqlParameter("@LeaveType", Integer.Parse(arr(1))),
                                                                      New System.Data.SqlClient.SqlParameter("@FromDt", Convert.ToDateTime(arr(2))),
                                                                      New System.Data.SqlClient.SqlParameter("@ToDt", Convert.ToDateTime(arr(3))),
                                                                      New System.Data.SqlClient.SqlParameter("@Days", Convert.ToDouble(arr(4))),
                                                                      New System.Data.SqlClient.SqlParameter("@ReasonID", Integer.Parse(arr(5))),
                                                                      New System.Data.SqlClient.SqlParameter("@Remarks", arr(7)),
                                                                      New System.Data.SqlClient.SqlParameter("@UserID", CInt(arr(8))),
                                                                      New System.Data.SqlClient.SqlParameter("@Attachment", Attachment),
                                                                      New System.Data.SqlClient.SqlParameter("@ContentType", ContentType),
                                                                      New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0),
                                                                      New System.Data.SqlClient.SqlParameter("@OutputMessage", message),
                                                                      New System.Data.SqlClient.SqlParameter("@isHD", CInt(arr(6)))}
            Parameters(11).Direction = ParameterDirection.InputOutput
            Parameters(12).Direction = ParameterDirection.InputOutput
            DB.ExecuteNonQuery("SP_Leave", Parameters)
            message = Parameters(11).Value.ToString() + "Ø" + Parameters(12).Value.ToString()
        Catch ex As Exception
            message = ex.Message
        End Try
        Return message
    End Function

    Public Function LeaveModification(ByVal HdnValue As String, ByVal Attachment As Byte(), ByVal ContentType As String) As String
        Dim message As New String(" ", 1000)
        Dim arr As String() = HdnValue.Split("~")
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TrNo", 1),
                                                                      New System.Data.SqlClient.SqlParameter("@EmpCode", Integer.Parse(arr(0))),
                                                                      New System.Data.SqlClient.SqlParameter("@LeaveType", Integer.Parse(arr(1))),
                                                                      New System.Data.SqlClient.SqlParameter("@FromDt", Convert.ToDateTime(arr(2))),
                                                                      New System.Data.SqlClient.SqlParameter("@ToDt", Convert.ToDateTime(arr(3))),
                                                                      New System.Data.SqlClient.SqlParameter("@Days", Convert.ToDouble(arr(4))),
                                                                      New System.Data.SqlClient.SqlParameter("@ReasonID", Integer.Parse(arr(5))),
                                                                      New System.Data.SqlClient.SqlParameter("@UserID", CInt(arr(8))),
                                                                      New System.Data.SqlClient.SqlParameter("@Attachment", Attachment),
                                                                      New System.Data.SqlClient.SqlParameter("@ContentType", ContentType),
                                                                      New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0),
                                                                      New System.Data.SqlClient.SqlParameter("@OutputMessage", message),
                                                                      New System.Data.SqlClient.SqlParameter("@isHD", CInt(arr(6))),
                                                                     New System.Data.SqlClient.SqlParameter("@RequestID", Integer.Parse(arr(7)))}
            Parameters(10).Direction = ParameterDirection.InputOutput
            Parameters(11).Direction = ParameterDirection.InputOutput
            DB.ExecuteNonQuery("SP_ModifyLeave", Parameters)
            message = Parameters(10).Value.ToString() + "Ø" + Parameters(11).Value.ToString()
        Catch ex As Exception
            message = ex.Message
        End Try
        Return message
    End Function

    Public Function getLeavesForCancellation(ByVal UserID As Integer, ByVal OptionID As Integer) As DataSet
        Dim Qry As String
        If OptionID = 1 Then
            Qry = "SELECT    '-1' AS Request_ID,'-------SELECT-------'AS Request UNION ALL SELECT cast(a.Request_ID as varchar(6))+'Ä'+CONVERT(VARCHAR(11),A.Request_Date, 106)+'Ä'+cast(a.Emp_Code as varchar(5))+'Ä'+b.Emp_Name+'Ä'+CONVERT(VARCHAR(11),A.Leave_From, 106)+'Ä'+CONVERT(VARCHAR(11),A.Leave_To, 106)+'Ä'+cast(a.Leave_Days as varchar(5))+'Ä'+c.Type_Name++'Ä'+d.Reason  As Request_ID,LEFT(b.Emp_Name+SPACE(50), 50)+'------'+CAST(a.Leave_From AS VARCHAR(12)) +'------'+CAST(a.Leave_To AS VARCHAR(12))  as Request from EMP_LEAVE_REQUEST a,EMP_MASTER  b,EMP_LEAVE_TYPE c,EMP_LEAVE_REASONS d Where a.Emp_Code =b.Emp_Code  and a.Leave_Type= c.Type_ID and a.Reason_ID=d.Reason_ID  and a.status_ID in(0) and a.Emp_Code=" + UserID.ToString() + " and d.status_id=1"
        Else
            Qry = "SELECT    '-1' AS Request_ID,'-------SELECT-------'AS Request UNION ALL SELECT cast(a.Request_ID as varchar(6))+'Ä'+CONVERT(VARCHAR(11),A.Request_Date, 106)+'Ä'+cast(a.Emp_Code as varchar(5))+'Ä'+b.Emp_Name+'Ä'+CONVERT(VARCHAR(11),A.Leave_From, 106)+'Ä'+CONVERT(VARCHAR(11),A.Leave_To, 106)+'Ä'+cast(a.Leave_Days as varchar(5))+'Ä'+c.Type_Name++'Ä'+d.Reason  As Request_ID,LEFT(b.Emp_Name+SPACE(50), 50)+'------'+CAST(a.Leave_From AS VARCHAR(12)) +'------'+CAST(a.Leave_To AS VARCHAR(12))  as Request from EMP_LEAVE_REQUEST a,EMP_MASTER  b,EMP_LEAVE_TYPE c,EMP_LEAVE_REASONS d Where a.Emp_Code =b.Emp_Code  and a.Leave_Type= c.Type_ID and a.Reason_ID=d.Reason_ID  and a.status_ID in(1,2) and a.Emp_Code=" + UserID.ToString() + " and d.status_id=1"
        End If
        Return DB.ExecuteDataSet(Qry)
    End Function
    Public Function LeaveCancel(ByVal RequestID As Integer, ByVal Reason As String, ByVal UserID As Integer, ByVal LeaveDays As Integer) As String
        Dim message As New String(" ", 1000)
        Dim DT As DataTable
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TrNo", 3), New System.Data.SqlClient.SqlParameter("@RequestID", RequestID), New System.Data.SqlClient.SqlParameter("@Remarks", Reason), New System.Data.SqlClient.SqlParameter("@Days", LeaveDays), New System.Data.SqlClient.SqlParameter("@UserID", UserID), New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0), New System.Data.SqlClient.SqlParameter("@OutputMessage", message)}
            Parameters(5).Direction = ParameterDirection.InputOutput
            Parameters(6).Direction = ParameterDirection.InputOutput
            DB.ExecuteNonQuery("SP_Leave", Parameters)
            message = Parameters(5).Value.ToString() + "Ø" + Parameters(6).Value.ToString()
            If Parameters(4).Value = 0 Then
                Dim QRY As String = ""
                QRY = "select c.Emp_Name +'['+convert(varchar,a.Emp_Code) +'] - From '+convert(varchar,a.Leave_From) +'To '+convert(varchar,a.Leave_To)+'|'+convert(varchar,a.Emp_Code)+'|'+convert(varchar,a.status_id)  from  EMP_LEAVE_REQUEST a,EMP_LEave_type b,emp_master c where a.leave_type=b.type_id and a.Emp_Code=c.Emp_Code and a.request_id=" & RequestID & ""

                DT = DB.ExecuteDataSet(QRY).Tables(0)
                Dim Employee() = Split(CStr(DT.Rows(0).Item(0)), "|")
                If CInt(Employee(2)) = 1 Then

                    Dim StrVal As String = CStr(Employee(0))
                    Dim ToAddress As String = "soumya.menon@esafbank.com"
                    Dim EmpName As String = ""
                    Dim tt As String = SM.GetEmailAddress(CInt(Employee(1)), 1)
                    Dim Val = Split(tt, "^")
                    If Val(0).ToString <> "" Then
                        ToAddress = Val(0)
                    End If
                    If Val(1).ToString <> "" Then
                        EmpName = Val(1).ToString
                    End If
                    Dim ccAddress As String = ""
                    Dim tt1 As String = SM.GetEmailAddress(CInt(UserID), 1)
                    Dim Val1 = Split(tt1, "^")
                    'General.GetEmailAddress(EmpID, 3);
                    If ccAddress.Trim() <> "" Then
                        ccAddress += ","
                    End If

                    Dim Content As String = vbLf & vbLf & " Dear " + EmpName + ", " & vbLf & vbLf

                    Content += "Approved Travel Request Cancelled  " + StrVal + "   By " + CStr(Val1(1).ToString) + "[" + UserID.ToString + "]" & vbLf & vbLf



                    Content = Content & Convert.ToString(vbLf & vbLf & " With Regards" & vbLf & vbLf & "HR Team" & vbLf & "Head, HR")
                    SM.SendMail(ToAddress, "Approved Leave Request Cancel", Content, ccAddress & Convert.ToString("nisha.rathnan@esafmicrofin.com"))

                Else
                    Dim StrVal As String = CStr(Employee(0))
                    Dim ToAddress As String = "hena.ek@esafmicrofin.com"
                    Dim EmpName As String = ""
                    Dim tt As String = SM.GetEmailAddress(CInt(Employee(1)), 2)
                    Dim Val = Split(tt, "^")
                    If Val(0).ToString <> "" Then
                        ToAddress = Val(0)
                    End If
                    If Val(1).ToString <> "" Then
                        EmpName = Val(1).ToString
                    End If
                    Dim ccAddress As String = ""
                    Dim tt1 As String = SM.GetEmailAddress(CInt(UserID), 1)
                    Dim Val1 = Split(tt1, "^")
                    'General.GetEmailAddress(EmpID, 3);
                    If ccAddress.Trim() <> "" Then
                        ccAddress += ","
                    End If

                    Dim Content As String = vbLf & vbLf & " Dear " + EmpName + ", " & vbLf & vbLf

                    Content += "Travel Request Cancelled  " + StrVal + "   By " + CStr(Val1(1).ToString) + "[" + UserID.ToString + "]" & vbLf & vbLf



                    Content = Content & Convert.ToString(vbLf & vbLf & " With Regards" & vbLf & vbLf & "HR Team" & vbLf & "Head, HR")
                    SM.SendMail(ToAddress, "Leave Request Cancel", Content, ccAddress & Convert.ToString("nisha.rathnan@esafmicrofin.com"))
                End If
            End If
        Catch ex As Exception
            message = ex.Message

        End Try
        Return message
    End Function
    Public Function Getexist_Leave_request(ByVal userid As Integer, ByVal statusid As Integer) As DataTable
        Return GN.GetQueryResult(1, userid, statusid)
    End Function
    Public Function GetLeaveAttachment(ByVal EmpID As Integer) As DataSet
        Dim sh As New MS_SQL.Connect()
        Try
            sh.Open()
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TrNo", 33), New System.Data.SqlClient.SqlParameter("@EmpID", EmpID)}
            Dim dt As DataSet = sh.ExecuteDataSet("GetReport", Parameters)
            sh.Close()
            Return dt
        Catch ex As Exception
            sh.Close()
            Return Nothing
        End Try
    End Function
    Public Function Get_History_Leave_summary(ByVal userid As Integer) As DataTable
        'empcode,empname,cl,clcount,sl,slcount,pl,plcount,other,othercnt
        Return GN.GetQueryResult(2, userid)
    End Function
    Public Function Get_History_Leave_dtls(ByVal userid As Integer, Optional ByVal FilterID As Integer = 0) As DataTable
        'requestdate,leavetype,leavefrom,leaveto,leavedays,reason,status
        Return GN.GetQueryResult(3, userid, FilterID)
    End Function
    Public Function Get_Leave_For_Modification(ByVal userid As Integer) As DataTable
        'requestdate,leavetype,leavefrom,leaveto,leavedays,reason,status
        Return GN.GetQueryResult(6, userid)
    End Function
    Public Function Get_Regularyze_Abscence_from(ByVal fromdt As Date, ByVal todt As Date) As DataTable
        'requestdate,leavetype,leavefrom,leaveto,leavedays,reason,status
        Return GN.GetQueryResult(4, 0, 0, fromdt, todt)
    End Function
Public Function Getexist_Leave_Request_HR() As DataTable
        Return DB.ExecuteDataSet("select '-1' as reqid,' ------Select------' as data Union All select convert(varchar,ss1.request_id)+'Ø'+convert(varchar,ss1.leave_days)+'Ø'+convert(varchar(11),ss1.leave_from,106)+'Ø'+convert(varchar(11),ss1.leave_to,106)+'Ø'+ss1.reason+'Ø'+convert(varchar,ss1.emp_code)+'Ø'+cast(isnull(la.RequestID,0) as varchar(4))+'Ø'+ss1.Recommend_Remarks+'Ø'+ss1.RecommendedBy   as reqid,data  from (select L.request_id,L.leave_days,L.leave_from,L.leave_to,R.reason,E.emp_code, E.Emp_Name+'('+convert(varchar,E.emp_code) +') | '+T.Type_Name+' | ('+convert(varchar(11),L.Leave_from,106)  + '  To ' +convert(varchar(11),L.Leave_To,106) +') | '+convert(varchar,R.Reason)AS data,l.Recommend_Remarks,s.Emp_Name as RecommendedBy  from EMP_LEAVE_REQUEST L,EMP_MASTER E,EMP_LEAVE_REASONS R,EMP_LEAVE_TYPE T,EMP_MASTER S Where L.emp_code=E.Emp_Code and L.reason_id=R.reason_id and L.Leave_Type=T.Type_ID And L.status_id = 2 and l.Recommend_By=s.Emp_Code  ) ss1 left outer join dms_ESFB.dbo.LeaveAttachment la on(ss1.request_id=la.RequestID)").Tables(0)
    End Function
End Class
