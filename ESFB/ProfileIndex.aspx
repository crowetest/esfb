﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="ProfileIndex.aspx.vb" Inherits="ProfileIndex" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ESAF</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="Style/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="Style/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="Style/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="Style/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
     <script src="../../Script/Calculations.js" type="text/javascript"></script>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    
  <script language="javascript" type="text/javascript">
      function window_onload() {
          
          var tab = "<div><img src='" + document.getElementById("<%= hid_image.ClientID %>").value + "' class='user-image img-responsive' ></div>";
          document.getElementById("<%= pnlImg.ClientID %>").innerHTML = tab;
          document.getElementById("Dates").style.display = 'none';
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("lblName").innerHTML = document.getElementById("<%= hid_User.ClientID %>").value;
          
          table_fill();
      }

      function table_fill() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 1;
          document.getElementById("lblCaption").innerHTML = 'My Profile';
          document.getElementById("<%= pnlProfile.ClientID %>").style.display = '';
          document.getElementById("Complaints").style.display = 'none';
          var row_bg = 0;
          var tab = "<br/><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Official Details</u></h4></div>";
          tab += "<div style='width:100%;'><table style='width:100%;' >";



          if (document.getElementById("<%= hid_Employee.ClientID %>").value != "") {

              row = document.getElementById("<%= hid_Employee.ClientID %>").value.split("¥");
              var ShowFlag = 0;
              for (n = 0; n <= row.length - 1; n++) {
                  col = row[n].split("µ");
                  
                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Emp.Code</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[0] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Department</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[5] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Surety Type</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[52] + "</td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Name</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[1] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Cadre</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[3] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Surety Name</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[53] + "</td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Date Of Joining</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[6] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Designation</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[4] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Surety Address</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[54] + "</td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Branch </td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[2] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Status</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[57] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Surety Phone</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[55] + "</td></tr>";


                  tab += "<tr><td colspan='9'><hr/><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Personal Details</u></h4></div></td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Date Of Birth</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[7] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Marital Status</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[17] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Gender</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[9] + "</td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Contact Person</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[13] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Contact No</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[14] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >House Name</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[11] + "</td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Location</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[16] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >City</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[15] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >State</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[33] + "</td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >District</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[32] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Post & Pin</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[20] + "-" + col[19] +"</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >E-mail</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[8] + "</td></tr>";


                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Land Line</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[12] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Mobile</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[10] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Fathers Name</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[39] + "</td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Mothers Name</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[40] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Religion</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[56] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Caste</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[18] + "</td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Blood Group</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[41] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Marriage Date</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  var MarriageDate = 'NA'
                  if (col[43] != '01 Jan 1900')
                      MarriageDate = col[43];
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + MarriageDate + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Two Wheeler Status</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[44] + "</td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Driving Liscence No</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[45] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >CUG No</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[46] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Official e-Mail</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[47] + "</td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Panchayath Ref. Letter</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[48] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Community Ref. Letter</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[49] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Police Ref. Letter</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[50] + "</td></tr>";

                  tab += "<tr><td style='width:11%;height:10px; text-align:left;' >Is Sangam Member?</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[51] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Languages Known</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + document.getElementById("<%= hid_Lang.ClientID %>").value + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >Mother Tongue</td>";
                  tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[42] + "</td></tr>";

                  tab += "<tr><td colspan='9'><hr/><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Qualification Details</u></h4></div></td></tr>";

                  if (document.getElementById("<%= hid_Quali.ClientID %>").value != "") {

                      row1 = document.getElementById("<%= hid_Quali.ClientID %>").value.split("¥");
                      var ShowQuali = 0;
                      for (m = 1; m <= row1.length - 1; m++) {
                          col1 = row1[m].split("µ");
                          if (ShowQuali == 0) {
                              tab += "<tr><td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Level</td>";
                              tab += "<td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='3' >Qualification</td>";
                              tab += "<td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='2' >University</td>";
                              tab += "<td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Year of Passing</td></tr>";
                              tab += "<tr><td colspan='9'><br/></td></tr>";
                              ShowQuali = 1;

                          }
                          tab += "<tr><td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='2' >" + col1[0] + "</td>";
                          tab += "<td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='3' >" + col1[1] + "</td>";
                          tab += "<td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='2' >" + col1[2] + "</td>";
                          tab += "<td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='2' >" + col1[3] + "</td></tr>";

                      }
                      tab += "<tr><td colspan='9'><hr/></td></tr>";
                  }

                  tab += "<tr><td colspan='9'><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Experience Details</u></h4></div></td></tr>";
                  if (document.getElementById("<%= hid_Exp.ClientID %>").value != "") {

                      row2 = document.getElementById("<%= hid_Exp.ClientID %>").value.split("¥");
                      var ShowExp = 0;
                      for (m = 1; m <= row2.length - 1; m++) {
                          col2 = row2[m].split("µ");
                          if (ShowExp == 0) {
                              tab += "<tr><td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Company</td>";
                              tab += "<td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='3' >Address</td>";
                              tab += "<td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Contact No</td>";
                              tab += "<td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='2' >No Of Years</td></tr>";
                              tab += "<tr><td colspan='9'><br/></td></tr>";
                              ShowExp = 1;

                          }
                          tab += "<tr><td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='2' >" + col2[0] + "</td>";
                          tab += "<td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='3' >" + col2[1] + "</td>";
                          tab += "<td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='2' >" + col2[2] + "</td>";
                          tab += "<td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='2' >" + col2[3] + "</td></tr>";

                      }
                      tab += "<tr><td colspan='9'><hr/></td></tr>";
                  }
                  tab += "<tr><td colspan='9'><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Family Details</u></h4></div></td></tr>";
                  if (document.getElementById("<%= hid_Fam.ClientID %>").value != "") {

                      row2 = document.getElementById("<%= hid_Fam.ClientID %>").value.split("¥");
                      var ShowFam = 0;
                      for (m = 1; m <= row2.length - 1; m++) {
                          col2 = row2[m].split("µ");
                          if (ShowFam == 0) {
                              tab += "<tr><td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Family Type</td>";
                              tab += "<td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='3' >Name</td>";
                              tab += "<td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Age</td>";
                              tab += "<td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Occupation</td></tr>";
                              tab += "<tr><td colspan='9'><br/></td></tr>";
                              ShowFam = 1;

                          }
                          tab += "<tr><td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='2' >" + col2[0] + "</td>";
                          tab += "<td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='3' >" + col2[1] + "</td>";
                          tab += "<td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='2' >" + col2[2] + "</td>";
                          tab += "<td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='2' >" + col2[3] + "</td></tr>";

                      }
                      tab += "<tr><td colspan='9'><hr/></td></tr>";
                  }
                  tab += "<tr><td colspan='9'><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>ID Proof Details</u></h4></div></td></tr>";
                  if (document.getElementById("<%= hid_IDProof.ClientID %>").value != "") {

                      row2 = document.getElementById("<%= hid_IDProof.ClientID %>").value.split("¥");
                      var ShowID = 0;
                      for (m = 1; m <= row2.length - 1; m++) {
                          col2 = row2[m].split("µ");
                          if (ShowID == 0) {
                              tab += "<tr><td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='4' >Proof Type</td>";
                              tab += "<td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='5' >Value</td></tr>";
                              tab += "<tr><td colspan='9'><br/></td></tr>";

                              ShowID = 1;

                          }
                          tab += "<tr><td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='4' >" + col2[0] + "</td>";
                          tab += "<td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='5' >" + col2[1] + "</td></tr>";

                      }
                      tab += "<tr><td colspan='9'><hr/></td></tr>";
                  }
                  tab += "<tr><td colspan='9'><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Address Proof Details</u></h4></div></td></tr>";
                  if (document.getElementById("<%= hid_ADProof.ClientID %>").value != "") {

                      row2 = document.getElementById("<%= hid_ADProof.ClientID %>").value.split("¥");
                      var ShowAD = 0;
                      for (m = 1; m <= row2.length - 1; m++) {
                          col2 = row2[m].split("µ");
                          if (ShowAD == 0) {
                              tab += "<tr><td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='4' >Proof Type</td>";
                              tab += "<td style='width:34%;height:10px; text-align:left;font-weight:bold;' colspan='5' >Value</td></tr>";
                              tab += "<tr><td colspan='9'><br/></td></tr>";

                              ShowAD = 1;

                          }
                          tab += "<tr><td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='4' >" + col2[0] + "</td>";
                          tab += "<td style='width:34%;height:10px; text-align:left;color:#878787;' colspan='5' >" + col2[1] + "</td></tr>";

                      }
                      tab += "<tr><td colspan='9'></td></tr>";
                  }
                  if (document.getElementById("<%= hid_TypeID.ClientID %>").value == 1) {
                      tab += "<tr><td colspan='9'><hr/><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Salary Details</u></h4></div></td></tr>";
                       row = document.getElementById("<%= hid_Employee.ClientID %>").value.split("¥");
                          var ShowFlag = 0;
                          for (n = 0; n <= row.length - 1; n++) {
                              col = row[n].split("µ");
                              tab += "<tr><td colspan='9'>";
                              tab += "<table style='width:90%;'><tr><td style='width:11%;height:10px; text-align:center;background-color:#F8F8F8;font-weight:bold;' colspan='6'>EARNINGS</td>";
                              tab += "<td style='width:1%;'></td><td style='width:11%;height:10px; text-align:center;background-color:#F8F8F8;font-weight:bold;' colspan='3'>DEDUCTIONS</td></tr>";


                              tab += "<tr><td style='width:20%;height:10px; text-align:left;' >Basic Pay</td>";
                              tab += "<td style='width:2%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:11%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[21]).toFixed(0) + "</td>";
                              tab += "<td style='width:16%;height:10px; text-align:left;' >DA</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[22]).toFixed(0) + "</td>";
                              tab += "<td style='width:5%;height:10px; text-align:left;'></td>";
                              tab += "<td style='width:20%;height:10px; text-align:left;' >&nbsp;&nbsp;ESI</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[34]).toFixed(0) + "</td></tr>";

                              tab += "<tr><td style='width:11%;height:10px; text-align:left;' colspan='10'></td></tr>";

                              tab += "<tr><td style='width:20%;height:10px; text-align:left;' >Conveyance</td>";
                              tab += "<td style='width:2%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:11%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[24]).toFixed(0) + "</td>";
                              tab += "<td style='width:16%;height:10px; text-align:left;' >Special Allwnce</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[25]).toFixed(0) + "</td>";
                              tab += "<td style='width:5%;height:10px; text-align:left;'></td>";
                              tab += "<td style='width:20%;height:10px; text-align:left;' >&nbsp;&nbsp;PF</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[35]).toFixed(0) + "</td></tr>";

                              tab += "<tr><td style='width:11%;height:10px; text-align:left;' colspan='9'></td></tr>";

                              tab += "<tr><td style='width:20%;height:10px; text-align:left;' >Perform. Allwnce</td>";
                              tab += "<td style='width:2%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:11%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[27]).toFixed(0) + "</td>";
                              tab += "<td style='width:16%;height:10px; text-align:left;' >Other Allwnce</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[28]).toFixed(0) + "</td>";
                              tab += "<td style='width:5%;height:10px; text-align:left;'></td>";
                              tab += "<td style='width:20%;height:10px; text-align:left;' >&nbsp;&nbsp;Staff Welfare</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[36]).toFixed(0) + "</td></tr>";

                              tab += "<tr><td style='width:11%;height:10px; text-align:left;' colspan='9'></td></tr>";

                              tab += "<tr><td style='width:20%;height:10px; text-align:left;' >Hospital Allwnce</td>";
                              tab += "<td style='width:2%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:11%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[30]).toFixed(0) + "</td>";
                              tab += "<td style='width:16%;height:10px; text-align:left;' >Field Allwnce</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[31]).toFixed(0) + "</td>";
                              tab += "<td style='width:5%;height:10px; text-align:left;'></td>";
                              tab += "<td style='width:20%;height:10px; text-align:left;' >&nbsp;&nbsp;ESWT</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[38]).toFixed(0) + "</td></tr>";

                              tab += "<tr><td style='width:11%;height:10px; text-align:left;' colspan='9'></td></tr>";

                              tab += "<tr><td style='width:20%;height:10px; text-align:left;' >HRA</td>";
                              tab += "<td style='width:2%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:11%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[23]).toFixed(0) + "</td>";
                              tab += "<td style='width:16%;height:10px; text-align:left;' >Local Allwnce</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[26]).toFixed(0) + "</td>";
                              tab += "<td style='width:5%;height:10px; text-align:left;'></td>";
                              tab += "<td style='width:20%;height:10px; text-align:left;' >&nbsp;&nbsp;Charity Fund</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[37]).toFixed(0) + "</td></tr>";

                              tab += "<tr><td style='width:11%;height:10px; text-align:left;' colspan='9'></td></tr>";

                              tab += "<tr><td style='width:20%;height:10px; text-align:left;' >Medical Allwnce</td>";
                              tab += "<td style='width:2%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:11%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;" + parseFloat(col[29]).toFixed(0) + "</td>";
                              tab += "<td style='width:16%;height:10px; text-align:left;' ></td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'></td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;</td>";
                              tab += "<td style='width:5%;height:10px; text-align:left;'></td>";
                              tab += "<td style='width:20%;height:10px; text-align:left;' ></td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'></td>";
                              tab += "<td style='width:10%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;</td></tr>";

                              tab += "<tr><td style='width:11%;height:10px; text-align:left;' colspan='9'></td></tr>";
                              tab += "<tr><td style='width:11%;height:10px; text-align:left;' colspan='9'></td></tr>";

                              var Total = parseFloat(col[21]) + parseFloat(col[22]) + parseFloat(col[23]) + parseFloat(col[24]) + parseFloat(col[25]) + parseFloat(col[26]) + parseFloat(col[27]) + parseFloat(col[28]) + parseFloat(col[29]) + parseFloat(col[30]) + parseFloat(col[31]);
                              var Deduction = parseFloat(col[34]) + parseFloat(col[35]) + parseFloat(col[36]) + parseFloat(col[37]) + parseFloat(col[38]);
                              tab += "<tr><td style='width:20%;height:10px; text-align:left;' ></td>";
                              tab += "<td style='width:2%;height:10px; text-align:left;'></td>";
                              tab += "<td style='width:11%;height:10px; text-align:left; color:#878787;'>&nbsp;&nbsp;</td>";
                              tab += "<td style='width:16%;height:10px; text-align:left;font-weight:bold;' >Total[+]</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold; color:#E31E24;'>&nbsp;&nbsp;" + Total + "</td>";
                              tab += "<td style='width:5%;height:10px; text-align:left;'></td>";
                              tab += "<td style='width:20%;height:10px; text-align:left;font-weight:bold;' >Total[-]</td>";
                              tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                              tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold; color:#E31E24;'>&nbsp;&nbsp;" + Deduction + "</td></tr>";

                              tab += "</tr></table>";

                          }
                  }
              }
          }
          tab += "</table></div>";

          


          document.getElementById("<%= pnlProfile.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }

      function table_fill_Leave() {
          document.getElementById("Dates").style.display = '';
          document.getElementById("lblCaption").innerHTML = 'Leave Details';
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("<%= pnlProfile.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";


          if (document.getElementById("<%= hid_Leave.ClientID %>").value != "") {
              Data = document.getElementById("<%= hid_Leave.ClientID %>").value.split("Ø");
              col = Data[0].split("~");
              tab += "<tr><td colspan='18'><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Leave Balance Details</u></h4></div></td></tr>";
              var CLBal = Math.abs(col[0]) - Math.abs(col[5]);
              var SLBal = Math.abs(col[1]) - Math.abs(col[6]);
              var ELBal = Math.abs(col[2]) - Math.abs(col[7]);
              var CMBal = Math.abs(col[3]) - Math.abs(col[8]);
              var RHBal = Math.abs(col[4]) - Math.abs(col[9]);
              tab += "<tr><td style='width:10%; text-align:left;font-weight:bold;' >Casual Leave</td>";
              tab += "<td style='width:1%; text-align:left;'>:</td>";
              tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + CLBal + "</td>";
              tab += "<td style='width:10%; text-align:left;font-weight:bold;' >Sick Leave</td>";
              tab += "<td style='width:1%; text-align:left;'>:</td>";
              tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + SLBal + "</td>";
              tab += "<td style='width:10%; text-align:left;font-weight:bold;' >Privilege Leave</td>";
              tab += "<td style='width:1%; text-align:left;'>:</td>";
              tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + ELBal + "</td>";
              tab += "<td style='width:10%; text-align:left; font-weight:bold;'>Compensatory</td>";
              tab += "<td style='width:1%; text-align:left; '>:</td>";
              tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + CMBal + "</td>";
              tab += "<td style='width:10%; text-align:left;font-weight:bold;'>Restr.&nbsp;Holiday</td>";
              tab += "<td style='width:1%; text-align:left; '>:</td>";
              tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + RHBal + "</td></tr>";
              tab += "<td style='width:5%; text-align:left;font-weight:bold;'>&nbsp;&nbsp;</td>";
              tab += "<td style='width:1%; text-align:left; '>&nbsp;&nbsp;</td>";
              tab += "<td style='width:10%; text-align:left; color:#878787;'>&nbsp;&nbsp;</td></tr>";
              tab += "<tr><td colspan='18'><hr/><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Leave Taken During This Month </u></h4></div></td></tr>";
              

              col1 = Data[1].split("^");
              tab += "<tr><td style='width:10%; text-align:left;font-weight:bold;' >Casual Leave</td>";
              tab += "<td style='width:1%; text-align:left;'>:</td>";
              tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col1[2] + "</td>";
              tab += "<td style='width:10%; text-align:left;font-weight:bold;' >Sick Leave</td>";
              tab += "<td style='width:1%; text-align:left;'>:</td>";
              tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col1[3] + "</td>";
              tab += "<td style='width:10%; text-align:left;font-weight:bold;' >Privilege Leave</td>";
              tab += "<td style='width:1%; text-align:left;'>:</td>";
              tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col1[4] + "</td>";
              tab += "<td style='width:10%; text-align:left; font-weight:bold; '>Compensatory</td>";
              tab += "<td style='width:1%; text-align:left; color:#878787;'>:</td>";
              tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col1[5] + "</td>";

              tab += "<td style='width:10%; text-align:left; font-weight:bold; '>Restr.&nbsp;Holiday</td>";
              tab += "<td style='width:1%; text-align:left; color:#878787;'>:</td>";
              tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col1[6] + "</td>";

              tab += "<td style='width:5%; text-align:left; font-weight:bold; '>Others</td>";
              tab += "<td style='width:1%; text-align:left; color:#878787;'>:</td>";
              tab += "<td style='width:10%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col1[7] + "</td></tr>";
              tab += "<tr><td colspan='18'><hr/><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Leave Details During This Month </u></h4></div></td></tr>";

              row1 = Data[2].split("¥");
              var ShowQuali = 0;
              for (m = 1; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {
                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Request Date</td>";
                      tab += "<td style='width:15%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Leave Type</td>";
                      tab += "<td style='width:13%;height:10px; text-align:left;font-weight:bold;' >From</td>";
                      tab += "<td style='width:15%;height:10px; text-align:left;font-weight:bold;' colspan='2' >To</td>";
                      tab += "<td style='width:13%;height:10px; text-align:left;font-weight:bold;' >Days</td>";
                      tab += "<td style='width:15%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Satus</td>";
                      tab += "<td style='width:28%;height:10px; text-align:left;font-weight:bold;' colspan='3' >Reason</td></tr>";
                      tab += "<tr><td colspan='9'><br/></td></tr>";
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;height:10px; text-align:left;'>" + col2[0] + "</td>";
                  tab += "<td style='width:15%;height:10px; text-align:left;' colspan='2' >" + col2[1] + "</td>";
                  tab += "<td style='width:13%;height:10px; text-align:left;' >" + col2[2] + "</td>";
                  tab += "<td style='width:15%;height:10px; text-align:left;' colspan='2' >" + col2[3] + "</td>";
                  tab += "<td style='width:13%;height:10px; text-align:left;' >" + col2[4] + "</td>";
                  tab += "<td style='width:15%;height:10px; text-align:left;' colspan='2' >" + col2[6] + "</td>";
                  tab += "<td style='width:28%;height:10px; text-align:left;' colspan='3' >" + col2[5] + "</td></tr>";

              }
              tab += "<tr><td colspan='18'><hr/><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Previous Leave History </u></h4></div></td></tr>";

             
              tab += "<tr><td colspan='11'></td></tr>";
          
          

              tab += "</table></div>";
          }
          document.getElementById("<%= pnlProfile.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }
      function table_fill_Previous_Leave() {

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          document.getElementById("Complaints").style.display = 'none';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";


          if (document.getElementById("<%= hid_Data.ClientID %>").value != "") {
             
              row1 = document.getElementById("<%= hid_Data.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 1; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {
                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Request Date</td>";
                      tab += "<td style='width:15%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Leave Type</td>";
                      tab += "<td style='width:13%;height:10px; text-align:left;font-weight:bold;' >From</td>";
                      tab += "<td style='width:15%;height:10px; text-align:left;font-weight:bold;' colspan='2' >To</td>";
                      tab += "<td style='width:13%;height:10px; text-align:left;font-weight:bold;' >Days</td>";
                      tab += "<td style='width:15%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Satus</td>";
                      tab += "<td style='width:28%;height:10px; text-align:left;font-weight:bold;' colspan='3' >Reason</td></tr>";
                      tab += "<tr><td colspan='7'><br/></td></tr>";
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;height:10px; text-align:left;'>" + col2[0] + "</td>";
                  tab += "<td style='width:15%;height:10px; text-align:left;' colspan='2' >" + col2[1] + "</td>";
                  tab += "<td style='width:13%;height:10px; text-align:left;' >" + col2[2] + "</td>";
                  tab += "<td style='width:15%;height:10px; text-align:left;' colspan='2' >" + col2[3] + "</td>";
                  tab += "<td style='width:13%;height:10px; text-align:left;' >" + col2[4] + "</td>";
                  tab += "<td style='width:15%;height:10px; text-align:left;' colspan='2' >" + col2[6] + "</td>";
                  tab += "<td style='width:28%;height:10px; text-align:left;' colspan='3' >" + col2[5] + "</td></tr>";

              }
             
              tab += "<tr><td colspan='7'></td></tr>";



              tab += "</table></div>";
          }
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }

      function table_fill_Travel() {
          document.getElementById("Dates").style.display = '';
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("lblCaption").innerHTML = 'Travel Details';
          document.getElementById("<%= pnlProfile.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='5'><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Travel Details During This Month </u></h4></div></td></tr>";

          if (document.getElementById("<%= hid_Travel.ClientID %>").value != "") {

              row1 = document.getElementById("<%= hid_Travel.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 1; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {
                      
                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>From Date</td>";
                      tab += "<td style='width:15%;height:10px; text-align:left;font-weight:bold;' colspan='2' >To Date Type</td>";
                      tab += "<td style='width:19%;height:10px; text-align:left;font-weight:bold;' >Location</td>";
                      tab += "<td style='width:43%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Purpose</td>";
                      tab += "<td style='width:22%;height:10px; text-align:left;font-weight:bold;' >Status</td></tr>";
                      tab += "<tr><td colspan='5'><br/></td></tr>";
                      
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;height:10px; text-align:left;'>" + col2[1] + "</td>";
                  tab += "<td style='width:15%;height:10px; text-align:left;' colspan='2' >" + col2[2] + "</td>";
                  tab += "<td style='width:19%;height:10px; text-align:left;' >" + col2[0] + "</td>";
                  tab += "<td style='width:43%;height:10px; text-align:left;' colspan='2' >" + col2[3] + "</td>";
                  tab += "<td style='width:22%;height:10px; text-align:left;' >" + col2[4] + "</td></tr>";

              }

              tab += "<tr><td colspan='5'></td></tr>";

          }
          tab += "<tr><td colspan='5'><hr/><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Previous Travel Details </u></h4></div></td></tr>";


          tab += "</table></div>";
          document.getElementById("<%= pnlProfile.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }
      function table_fill_Previous_Travel() {

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          document.getElementById("Complaints").style.display = 'none';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";


          if (document.getElementById("<%= hid_Data.ClientID %>").value != "") {

              row1 = document.getElementById("<%= hid_Data.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 1; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {
                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>From Date</td>";
                      tab += "<td style='width:15%;height:10px; text-align:left;font-weight:bold;' colspan='2' >To Date Type</td>";
                      tab += "<td style='width:19%;height:10px; text-align:left;font-weight:bold;' >Location</td>";
                      tab += "<td style='width:43%;height:10px; text-align:left;font-weight:bold;' colspan='2' >Purpose</td>";
                      tab += "<td style='width:22%;height:10px; text-align:left;font-weight:bold;' >Status</td></tr>";
                      tab += "<tr><td colspan='5'><br/></td></tr>";
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;height:10px; text-align:left;'>" + col2[1] + "</td>";
                  tab += "<td style='width:15%;height:10px; text-align:left;' colspan='2' >" + col2[2] + "</td>";
                  tab += "<td style='width:19%;height:10px; text-align:left;' >" + col2[0] + "</td>";
                  tab += "<td style='width:43%;height:10px; text-align:left;' colspan='2' >" + col2[3] + "</td>";
                  tab += "<td style='width:22%;height:10px; text-align:left;' >" + col2[4] + "</td></tr>";

              }

              tab += "<tr><td colspan='5'></td></tr>";



              tab += "</table></div>";
          }
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }
      function table_fill_Transfer() {
          document.getElementById("Dates").style.display = 'none';
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("lblCaption").innerHTML = 'Transfer Details';
          document.getElementById("<%= pnlProfile.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='11'><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Joining Details </u></h4></div></td></tr>";

          if (document.getElementById("<%= hid_Transfer.ClientID %>").value != "") {

              row1 = document.getElementById("<%= hid_Transfer.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 1; m <= row1.length - 1; m++) {
                  col = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      
                      tab += "<tr><td colspan='5' style='font-weight:bold;'><br/></td></tr>";
                      tab += "<tr><td style='width:11%; text-align:left;font-weight:bold;' >Location</td>";
                      tab += "<td style='width:3%; text-align:left;'>:</td>";
                      tab += "<td style='width:15%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[0] + "</td>";
                      tab += "<td style='width:10%; text-align:left;font-weight:bold;' >Department</td>";
                      tab += "<td style='width:3%; text-align:left;'>:</td>";
                      tab += "<td style='width:15%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[2] + "</td>";
                      tab += "<td style='width:5%; text-align:left;font-weight:bold;' >Cadre</td>";
                      tab += "<td style='width:3%; text-align:left;'>:</td>";
                      tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[11] + "</td>";
                      tab += "<td style='width:12%; text-align:left; font-weight:bold; '>Designation </td>";
                      tab += "<td style='width:3%; text-align:left;'>:</td>";
                      tab += "<td style='width:20%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[9] + "</td></tr>";

                      tab += "<tr><td style='width:11%; text-align:left;font-weight:bold;' >Reporting Officer</td>";
                      tab += "<td style='width:3%; text-align:left;'>:</td>";
                      tab += "<td style='width:15%; text-align:left; color:#878787;'>&nbsp;&nbsp;" + col[4] + "</td>";
                      tab += "<td style='width:10%; text-align:left;font-weight:bold;' ></td>";
                      tab += "<td style='width:3%; text-align:left;'></td>";
                      tab += "<td style='width:15%; text-align:left; color:#878787;'>&nbsp;&nbsp;</td>";
                      tab += "<td style='width:5%; text-align:left;font-weight:bold;' ></td>";
                      tab += "<td style='width:3%; text-align:left;'></td>";
                      tab += "<td style='width:5%; text-align:left; color:#878787;'>&nbsp;&nbsp;</td>";
                      tab += "<td style='width:12%; text-align:left; font-weight:bold; '></td>";
                      tab += "<td style='width:3%; text-align:left;'></td>";
                      tab += "<td style='width:20%; text-align:left; color:#878787;'>&nbsp;&nbsp;</td></tr>";
                      tab += "<tr><td style='width:10%;height:10px; text-align:left;' colspan=12><hr/></td></tr>";
                      tab += "<tr><td colspan='12' style='font-weight:bold;'><h4 style='color:#E31E24;font-style:italic;'><u>Transfer Details </u></h4></td></tr>";
                      tab += "<tr>";
                      tab += "<td style='width:10%;height:10px; text-align:left;' colspan=12>";
                      tab += "<table style='width:100%;'><tr><td style='width:10%;height:10px; text-align:left;font-weight:bold;'>Date</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;'colspan='2' >Location</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;'colspan='2'>Department</td>";
                      tab += "<td style='width:5%;height:10px; text-align:left;font-weight:bold;'colspan='2'>Cadre</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;'colspan='2'>Designation</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;'colspan='2'>Reporting Officer</td>";
                      tab += "</tr>";
                      
                      tab += "<tr><td colspan='11'><br/></td></tr>";

                      ShowQuali = 1;

                  }


                      tab += "<tr><td style='width:10%;height:10px; text-align:left;'>" + col[8] + "</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;'colspan='2' >" + col[1] + "</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;'colspan='2'>" + col[3] + "</td>";
                      tab += "<td style='width:5%;height:10px; text-align:left;'colspan='2'>" + col[12] + "</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;'colspan='2'>" + col[10] + "</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;'colspan='2'>" + col[5] + "</td>";
                      tab += "</tr>";

                                      

              }

              tab += "<tr><td colspan='11'></td></tr></table></td></tr>";

          }
          tab += "<tr style='display:none;'><td colspan='11'><hr/><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Previous Transfer Details </u></h4></div></td></tr>";


          tab += "</table></div>";
          
          document.getElementById("<%= pnlProfile.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }
      function table_fill_Previous_Transfer() {
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";


          if (document.getElementById("<%= hid_Data.ClientID %>").value != "") {

              row1 = document.getElementById("<%= hid_Data.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 1; m <= row1.length - 1; m++) {
                  col = row1[m].split("µ");
                  if (ShowQuali == 0) {
                      tab += "<tr>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;' >Date</td>";
                      tab += "<td style='width:10%;height:10px; text-align:center;font-weight:bold;'colspan='2' >Location</td>";
                      tab += "<td style='width:10%;height:10px; text-align:center;font-weight:bold;'colspan='2'>Department</td>";
                      tab += "<td style='width:5%;height:10px; text-align:center;font-weight:bold;'colspan='2'>Cadre</td>";
                      tab += "<td style='width:10%;height:10px; text-align:center;font-weight:bold;'colspan='2'>Designation</td>";
                      tab += "<td style='width:10%;height:10px; text-align:center;font-weight:bold;'colspan='2'>Reporting To</td>";
                      tab += "</tr>";
                      tab += "<tr>";

                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;' ></td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;' >From</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;' >To</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;'>From</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;'>To</td>";
                      tab += "<td style='width:5%;height:10px; text-align:left;font-weight:bold;'>From</td>";
                      tab += "<td style='width:5%;height:10px; text-align:left;font-weight:bold;'>To</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;'>From</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;'>To</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;'>From</td>";
                      tab += "<td style='width:10%;height:10px; text-align:left;font-weight:bold;'>To</td>";
                      tab += "</tr>";
                     
                      tab += "<tr><td colspan='11'><br/></td></tr>";

                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:10%;height:10px; text-align:left;' >" + col[8] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;' >" + col[0] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;'>" + col[1] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;'>" + col[2] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;'>" + col[3] + "</td>";
                  tab += "<td style='width:5%;height:10px; text-align:left;' >" + col[11] + "</td>";
                  tab += "<td style='width:5%;height:10px; text-align:left;'>" + col[12] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;'>" + col[9] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;'>" + col[10] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;'>" + col[4] + "</td>";
                  tab += "<td style='width:10%;height:10px; text-align:left;'>" + col[5] + "</td>";
                  tab += "</tr>";

              }

              tab += "<tr><td colspan='11'></td></tr>";



              tab += "</table></div>";
          }
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }
      function table_fill_Promotion() {
          document.getElementById("Dates").style.display = 'none';
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("lblCaption").innerHTML = 'Promotion Details';
          document.getElementById("<%= pnlProfile.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='5'><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Promotion Details</u></h4></div></td></tr>";

          if (document.getElementById("<%= hid_Promotion.ClientID %>").value != "") {

              row1 = document.getElementById("<%= hid_Promotion.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 1; m <= row1.length - 1; m++) {
                  col = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:14%;text-align:left;font-weight:bold;' >Date</td>";
                      tab += "<td style='width:14%;text-align:left;font-weight:bold;' >Previous Designation</td>";
                      tab += "<td style='width:14%;text-align:left;font-weight:bold;'>Current Designation</td>";
                      tab += "<td style='width:14%;text-align:left;font-weight:bold;'>Previous Reporting</td>";
                      tab += "<td style='width:14%;text-align:left;font-weight:bold;'>Current Reporting</td>";
                      tab += "<td style='width:14%;text-align:left;font-weight:bold;'>Previous Post</td>";
                      tab += "<td style='width:13%;text-align:left;font-weight:bold;'>Current Post</td></tr>";
                      tab += "<tr><td colspan='7'><br/></td></tr>";

                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;text-align:left' >" + col[0] + "</td>";
                  tab += "<td style='width:14%;text-align:left' >" + col[1] + "</td>";
                  tab += "<td style='width:14%;text-align:left'>" + col[2] + "</td>";
                  tab += "<td style='width:14%;text-align:left'>" + col[3] + "</td>";
                  tab += "<td style='width:14%;text-align:left'>" + col[4] + "</td>";
                  tab += "<td style='width:14%;text-align:left' >" + col[5] + "</td>";
                  tab += "<td style='width:13%;text-align:left'>" + col[6] + "</td></tr>";

              }

              tab += "<tr><td colspan='7'></td></tr>";

          }
          tab += "<tr style='display:none;'><td colspan='7'><hr/><div style='width:100%;'><h4 style='color:#E31E24;font-style:italic;'><u>Previous Promotion Details </u></h4></div></td></tr>";


          tab += "</table></div>";
          document.getElementById("<%= pnlProfile.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }
      function table_fill_Previous_Promotion() {
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";


          if (document.getElementById("<%= hid_Data.ClientID %>").value != "") {

              row1 = document.getElementById("<%= hid_Data.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 1; m <= row1.length - 1; m++) {
                  col = row1[m].split("µ");
                  if (ShowQuali == 0) {
                      tab += "<tr><td style='width:14%;text-align:left;font-weight:bold;' >Date</td>";
                      tab += "<td style='width:14%;text-align:left;font-weight:bold;' >Previous Designation</td>";
                      tab += "<td style='width:14%;text-align:left;font-weight:bold;'>Current Designation</td>";
                      tab += "<td style='width:14%;text-align:left;font-weight:bold;'>Previous Reporting</td>";
                      tab += "<td style='width:14%;text-align:left;font-weight:bold;'>Current Reporting</td>";
                      tab += "<td style='width:14%;text-align:left;font-weight:bold;'>Previous Post</td>";
                      tab += "<td style='width:13%;text-align:left;font-weight:bold;'>Current Post</td></tr>";
                      tab += "<tr><td colspan='7'><br/></td></tr>";
                      
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;text-align:left' >" + col[0] + "</td>";
                  tab += "<td style='width:14%;text-align:left' >" + col[1] + "</td>";
                  tab += "<td style='width:14%;text-align:left'>" + col[2] + "</td>";
                  tab += "<td style='width:14%;text-align:left'>" + col[3] + "</td>";
                  tab += "<td style='width:14%;text-align:left'>" + col[4] + "</td>";
                  tab += "<td style='width:14%;text-align:left' >" + col[5] + "</td>";
                  tab += "<td style='width:13%;text-align:left'>" + col[6] + "</td></tr>";

              }

              tab += "<tr><td colspan='7'></td></tr>";



              tab += "</table></div>";
          }
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }
      function table_fill_JobProfile() {
          document.getElementById("Dates").style.display = 'none';
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("lblCaption").innerHTML = 'Available Roles';
          document.getElementById("<%= pnlProfile.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          
          if (document.getElementById("<%= hid_Job.ClientID %>").value != "") {

              row1 = document.getElementById("<%= hid_Job.ClientID %>").value.split("¥");
              var ShowQuali = "";
              var i = 0;
              for (m = 1; m <= row1.length - 1; m++) {
                  col = row1[m].split("µ");
                  
                  if (ShowQuali != col[0]) {
                       i = 0;
                      tab += "<tr><td colspan='3'><br/><h4 style='color:#E31E24;font-style:italic;'><u>" + col[0] + " </u></h4><br/></td></tr>";

                      tab += "<tr><td style='width:10%;height:10px; text-align:left;font-weight:bold;'>Sl NO</td>";
                      tab += "<td style='width:20%;height:10px; text-align:left;font-weight:bold;'>Menu Name</td>";
                      tab += "<td style='width:70%;height:10px; text-align:left;font-weight:bold;'>Form Name</td>";
                      tab += "</tr>";
                      tab += "<tr><td colspan='3'><br/></td></tr>";
                      ShowQuali = col[0];

                  }

                  i += 1;
                  tab += "<tr><td style='width:10%;height:10px; text-align:left;'>"+ i +"</td>";
                  tab += "<td style='width:20%;height:10px; text-align:left;'>" + col[1] + "</td>";
                  tab += "<td style='width:70%;height:10px; text-align:left;'>" + col[2] + "</td>";
                  tab += "</tr>";
                  tab += "<tr><td colspan='3'><br/></td></tr>";

              }

              tab += "<tr><td colspan='3'></td></tr></table></td></tr>";

          }
          


          tab += "</table></div>";

          document.getElementById("<%= pnlProfile.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }
     
      function FromServer(arg, context) {

          switch (context) {
              case 5:
                  {

                      document.getElementById("<%= hid_Data.ClientID %>").value = arg;
                      if (document.getElementById("<%= hid_Type.ClientID %>").value == 2) 
                      {
                          table_fill_Previous_Leave();
                      }
                      else if (document.getElementById("<%= hid_Type.ClientID %>").value == 3) {
                          table_fill_Previous_Travel();
                      }
                      else if (document.getElementById("<%= hid_Type.ClientID %>").value == 4) {
                          table_fill_Previous_Transfer();
                      }
                      else if (document.getElementById("<%= hid_Type.ClientID %>").value == 5) {
                          table_fill_Previous_Promotion();
                      }
                      break;
                  }
              case 2:
                  {

                      document.getElementById("<%= hid_Leave.ClientID %>").value = arg;
                     
                      table_fill_Leave();
                      break;
                  }
              case 3:
                  {
                      document.getElementById("<%= hid_Transfer.ClientID %>").value = arg;
                      table_fill_Transfer();
                      break;
                  }
              case 4:
                  {
                      document.getElementById("<%= hid_Promotion.ClientID %>").value = arg;
                      table_fill_Promotion();
                      break;
                  }
              case 1:
                  {
                      document.getElementById("<%= hid_Travel.ClientID %>").value = arg;
                      table_fill_Travel();
                      break;
                  }
              case 6:
                  {
                      document.getElementById("<%= hid_Job.ClientID %>").value = arg;
                      table_fill_JobProfile();
                      break;
                  }
              case 7:
                  {
                      var Data = arg.split("ʘ");
                      alert(Data[1]);
                      document.getElementById("<%= txtComplaint.ClientID %>").value = "";
                      Complaint();
                      break;
                  }
          }

      }

      function Leave() {
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("<%= hid_Type.ClientID %>").value = 2;
          document.getElementById("txtFromDt").value = "";
          document.getElementById("txtToDt").value = "";
          ToServer("2Ø" + document.getElementById("<%= hid_EmpCode.ClientID %>").value, 2);
      }
      function Travel() {
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("<%= hid_Type.ClientID %>").value = 3;
          document.getElementById("txtFromDt").value = "";
          document.getElementById("txtToDt").value = "";
          ToServer("1Ø" + document.getElementById("<%= hid_EmpCode.ClientID %>").value, 1);
      }
      function Transfer() {
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("<%= hid_Type.ClientID %>").value = 4;
          document.getElementById("txtFromDt").value = "";
          document.getElementById("txtToDt").value = "";
          ToServer("3Ø" + document.getElementById("<%= hid_EmpCode.ClientID %>").value, 3);
      }
      function Promotion() {
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("<%= hid_Type.ClientID %>").value = 5;
          document.getElementById("txtFromDt").value = "";
          document.getElementById("txtToDt").value = "";
          ToServer("4Ø" + document.getElementById("<%= hid_EmpCode.ClientID %>").value, 4);
      }
      function JobProfile() {
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
          document.getElementById("Complaints").style.display = 'none';
          document.getElementById("<%= hid_Type.ClientID %>").value = 6;
          document.getElementById("txtFromDt").value = "";
          document.getElementById("txtToDt").value = "";
          ToServer("6Ø" + document.getElementById("<%= hid_EmpCode.ClientID %>").value, 6);
      }
      function GenerateOnClick() {
          var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
          var ToDt = document.getElementById("<%= txtToDt.ClientID %>").value;
          if (FromDt == "") { alert("Enter From Date"); return false; }
          if (ToDt == "") { alert("Enter To Date"); return false; }
          if (ValidateForm("txtFromDt") == false) {
              return false;
          }
          if (ValidateForm("txtToDt") == false) {
              return false;
          }
          DateChange(FromDt, 1);
          DateChange(ToDt, 2);
          var Data = document.getElementById("<%= hid_EmpCode.ClientID %>").value + "Ø" + document.getElementById("<%= hid_Type.ClientID %>").value + "Ø" + document.getElementById("<%= hdnFromDt.ClientID %>").value + "Ø" + document.getElementById("<%= hdnToDt.ClientID %>").value;
          
          ToServer("5Ø" + Data, 5);

      }

      function Complaint() {
          document.getElementById("Complaints").style.display = '';
          document.getElementById("Dates").style.display = 'none';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = 'none';
          document.getElementById("<%= pnlProfile.ClientID %>").style.display = 'none';
          document.getElementById("lblCaption").innerHTML = 'Compliant Registration';
      }

      function RegisterOnClick() {
          var ComplaintName = document.getElementById("<%= txtComplaint.ClientID %>").value
          if (ComplaintName == "") { alert("Enter Complaints");document.getElementById("<%= txtComplaint.ClientID %>").focus(); return false; }
          var Data = document.getElementById("<%= hid_EmpCode.ClientID %>").value + "Ø" + ComplaintName;
          ToServer("7Ø" + Data, 7);
      }
      
      function DataClear() {
          document.getElementById("<%= hid_Data.ClientID %>").value = "";
          if (document.getElementById("<%= hid_Type.ClientID %>").value == 2) {
              table_fill_Previous_Leave();
          }
          else if (document.getElementById("<%= hid_Type.ClientID %>").value == 3) {
              table_fill_Previous_Leave();
          }
          
      }
      var dtCh = "/";
      var minYear = 1900;
      var maxYear = 2100;

      // Declaring valid date character, minimum year and maximum year
      function isInteger(s) {
          var i;
          for (i = 0; i < s.length; i++) {
              // Check that current character is number.
              var c = s.charAt(i);
              if (((c < "0") || (c > "9"))) return false;
          }
          // All characters are numbers.
          return true;
      }

      function stripCharsInBag(s, bag) {
          var i;
          var returnString = "";
          // Search through string's characters one by one.
          // If character is not in bag, append to returnString.
          for (i = 0; i < s.length; i++) {
              var c = s.charAt(i);
              if (bag.indexOf(c) == -1) returnString += c;
          }
          return returnString;
      }

      function daysInFebruary(year) {
          // February has 29 days in any year evenly divisible by four,
          // EXCEPT for centurial years which are not also divisible by 400.
          return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
      }
      function DaysArray(n) {
          for (var i = 1; i <= n; i++) {
              this[i] = 31
              if (i == 4 || i == 6 || i == 9 || i == 11) { this[i] = 30 }
              if (i == 2) { this[i] = 29 }
          }
          return this
      }



      function isDate(dtStr) {

          var daysInMonth = DaysArray(12)
          var pos1 = dtStr.indexOf(dtCh)
          var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
          var strDay = dtStr.substring(0, pos1)
          var strMonth = dtStr.substring(pos1 + 1, pos2)
          var strYear = dtStr.substring(pos2 + 1)
          var strYr = strYear
          if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1)
          if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1)
          for (var i = 1; i <= 3; i++) {
              if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
          }
          var month = parseInt(strMonth)
          var day = parseInt(strDay)
          var year = parseInt(strYr)
          if (pos1 == -1 || pos2 == -1) {
              alert("The date format should be : dd/mm/yyyy")
              return false
          }
          if (strMonth.length < 1 || month < 1 || month > 12) {
              alert("Please enter a valid month")
              return false
          }
          if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
              alert("Please enter a valid day")
              return false
          }
          if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
              alert("Please enter a valid 4 digit year between " + minYear + " and " + maxYear)
              return false
          }
          if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
              alert("Please enter a valid date")
              return false
          }
          return true
      }


      function ValidateForm(ctrlID) {
          var dt = document.getElementById(ctrlID);
          if (isDate(dt.value) == false) {
              dt.value = "";
              dt.focus();
              return false
          }
          else
              return true;
      }

      function DateChange(ctrlID, ID) {
          sdate = ctrlID;
          var splitdate = sdate.split("/");
          var dt1 = splitdate[1] + "/" + splitdate[0] + "/" + splitdate[2];
          if (ID == 1)
              document.getElementById("<%= hdnFromDt.ClientID %>").value = dt1;
          else
              document.getElementById("<%= hdnToDt.ClientID %>").value = dt1;

      }
      function BackOnClick() {
          if (document.getElementById("<%= hid_TypeID.ClientID %>").value == 2) {
              window.open("HRM/Master/viewProfile.aspx", "_self");
          }
          else {
              window.open("Portal.aspx", "_self");
          }
          return false;
      }
    </script>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            
  <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">&nbsp; <a href="" onclick="return BackOnClick()" class="btn btn-danger square-btn-adjust">Back</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                <asp:Panel ID="pnlImg" runat="server"></asp:Panel>
                    
					</li>
				
					
                    <li onclick ="table_fill()"  >
                        <a  href="#"> My Profile <span style="float:right;"> &raquo;</span></a>
                    </li>
                     <li  onclick ="Leave()" >
                        <a  href="#"> Leave Details <span style="float:right;"> &raquo;</span></a>
                    </li>
                    <li  onclick ="Travel()" style="top: 2px; left: 0px">
                        <a  href="#"> Travel Details <span style="float:right;"> &raquo;</span></a>
                    </li>
				    <li  onclick ="Transfer()" >
                        <a   href="#"> Transfer Details <span style="float:right;"> &raquo;</span></a>
                    </li>	
                     <li  onclick ="Promotion()" >
                        <a   href="#"> Promotion Details <span style="float:right;"> &raquo;</span></a>
                    </li>	
                    <li  onclick ="JobProfile()" >
                        <a   href="#"> Available Roles <span style="float:right;"> &raquo;</span></a>
                    </li> 
                    <li  onclick ="Complaint()" >
                        <a   href="#"> Compliant Registration <span style="float:right;"> &raquo;</span></a>
                    </li> 
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2><label id="lblCaption"></label><asp:HiddenField ID="hid_image" runat="server" />
                        <asp:HiddenField ID="hid_Employee" runat="server" />
                        <asp:HiddenField ID="hid_User" runat="server" />
                        <asp:HiddenField ID="hid_EmpCode" runat="server" />
                        <asp:HiddenField ID="hid_TypeID" runat="server" />
                        <asp:HiddenField ID="hid_Quali" runat="server" />
                        <asp:HiddenField ID="hid_Fam" runat="server" />
                        <asp:HiddenField ID="hid_Exp" runat="server" />
                        <asp:HiddenField ID="hid_IDProof" runat="server" />
                        <asp:HiddenField ID="hid_ADProof" runat="server" />
                        <asp:HiddenField ID="hid_Lang" runat="server" />
                        <asp:HiddenField ID="hid_Leave" runat="server" />
                        <asp:HiddenField ID="hid_Promotion" runat="server" />
                        <asp:HiddenField ID="hid_Travel" runat="server" />
                        <asp:HiddenField ID="hid_dtls" runat="server" />
                        <asp:HiddenField ID="hdnSubID" runat="server" />
                        <asp:HiddenField ID="hdnFromDt" runat="server" />
                        <asp:HiddenField ID="hdnToDt" runat="server" />
                        <asp:HiddenField ID="hid_Data" runat="server" />
                        <asp:HiddenField ID="hid_Transfer" runat="server" />
                        <asp:HiddenField ID="hid_Type" runat="server" />
                        <asp:HiddenField ID="hid_Job" runat="server" />
                        <asp:HiddenField ID="hid_Compliant" runat="server" />
                        </h2>   
                        <h5>Welcome <label id="lblName"></label>, Love to see you back. </h5>
                        <asp:Panel ID="pnlProfile" runat="server"></asp:Panel>
                        <table id="Dates"><tr><td style="width:15%;">From<span class="style1"><br/>(dd/MM/yyyy)</span></td>
                        <td style="width:25%;"> <asp:TextBox ID="txtFromDt"  Width="80%" runat="server" class="NormalText" ></asp:TextBox></td>
                        
                       <td style="width:15%;">To<span class="style1"><br/>(dd/MM/yyyy)</span></td>
                       <td style="width:25%;">  <asp:TextBox ID="txtToDt"  Width="80%" runat="server"  class="NormalText"></asp:TextBox> </td>
                       <td style="width:20%;"><input id="btnView"  type="button" value="View"  style="font-family: cambria; width: 45%; cursor:pointer" onclick="GenerateOnClick()"  />
                &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 45%;" 
                type="button" value="Clear" onclick="DataClear()"  /></td>
                </tr></table>
                <table id="Complaints">
                <tr><td style="width:100%;">For inappropriate details appearing in your profile please register your complaints below</td></tr>
                <tr><td style="width:100%;">&nbsp;</td></tr>
                <tr><td style="width:100%;">  
                    <asp:TextBox ID="txtComplaint"  Width="100%" 
                        runat="server"  class="NormalText" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Rows="10"></asp:TextBox> </td> </tr>
             
                <tr><td style="width:20%; text-align: center;">&nbsp;</td></tr>
             
                <tr><td style="width:20%; text-align: center;"><input id="btnRegister"  type="button" value="Register"  style="font-family: cambria; width: 15%; cursor:pointer" onclick="RegisterOnClick()"  />
                 &nbsp;<input id="btnClose" style="font-family: cambria; cursor: pointer; width: 15%;"  type="button" value="Clear" onclick="DataClear()"  /></td></tr>
                </table>
                <br /><asp:Panel ID="pnlDtls" runat="server"></asp:Panel>
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                              
                           
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <!-- METISMENU SCRIPTS -->
    
    
   
    </form>
    
   
</body>
</html>
</asp:Content>
