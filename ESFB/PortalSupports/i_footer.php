<div id="FooterContainer">
  <div id="Footer" class="warpper">
    <div id="FooterContent">
      <div id="AddressBox">
        <h3>Regional Offices</h3>
        <div id="AddressTabs">
          <div id="TabbedPanels1" class="TabbedPanels">
            <ul class="TabbedPanelsTabGroup">
              <li class="TabbedPanelsTab" tabindex="0">Kerala</li>
              <li class="TabbedPanelsTab" tabindex="0">Karnataka</li>
              <li class="TabbedPanelsTab" tabindex="0">Tamil Nadu</li>
            </ul>
            <div class="TabbedPanelsContentGroup">
              <div class="TabbedPanelsContent"><strong>Manappuram Chits India Ltd</strong><br />
                First Floor, krishna Tower, TUDA Road<br />
                Aswani Jn., Thrissur - Pin 680 012<br />
                Tel : +91 (487) 3104892, 2420799, 8590438553 <br />
                Email : office@manappuramchits.com <br />
              </div>
              <div class="TabbedPanelsContent"> </div>
              <div class="TabbedPanelsContent"> </div>
            </div>
          </div>
        </div>
      </div>
      <div id="QuickLinks">
        <h3 style="padding-left:25px;">Quick Links </h3>
        <ul>
          <li><a href="profile.php">Corporate Profile</a></li>
          <li><a href="chitties.php">Our Chitties</a></li>
          <li><a href="management.php">Management Team</a></li>
          <li><a href="downloads.php">Download Centre</a></li>
          <li><a href="services.php">Services Offered</a></li>
          <li><a href="register.php">Register Online</a></li>
          <li><a href="news.php">News &amp; Events</a></li>
          <li><a href="sitemap.php">Sitemap</a></li>
          <li><a href="contact.php">Contact Us</a></li>
          <div class="clear"></div>
        </ul>
      </div>
      <div id="FollowUs">
        <h3>Follow Us On </h3>
        <div id="Links"> <a href="#" target="_blank" style="background-image:url(images/facebook.jpg)"></a> <a href="#" target="_blank" style="background-image:url(images/gplus.jpg)"></a> <a href="#" target="_blank" style="background-image:url(images/twitter.jpg)"></a> </div>
      </div>
    </div>
    <div id="Credits"> &copy; Copyright 2013. All rights reserved at Manappuram Chits  | Website powered by <a href="http://www.chaithrainfotech.com" target="_blank">Chaithra Infotech</a> </div>
  </div>
</div>
<script type="text/javascript">
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script>