Imports System.Data
Imports System.Data.SqlClient

Partial Class Home
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not IsNothing(Request.QueryString.Get("ModuleID")) Then
                Session("ModuleID") = Request.QueryString.Get("ModuleID")
                DT = DB.ExecuteDataSet("select Module_Name from Module_Master where Module_ID=" & Session("ModuleID") & "").Tables(0)
                Session("ModuleName") = DT.Rows(0)(0).ToString()
            End If
            GetMenuData()
        End If
    End Sub
    Private Sub GetMenuData()
        '  DT = DB.ExecuteDataSet("select menu_id as menu_id,menu_name,null as parent_id,null as menu_url,1 as order_id,status_id from menu_groups where status_id=1 and module_id=" + Session("ModuleID").ToString() + " union all select form_id as menu_id,form_name as menu_name,menu_id as parent_id,url as menu_url,order_id,status_id from menu_forms where status_id=1 and ( CHILD_FLAG=1 OR  form_id in(select form_id from role_dtl where role_id in(select role_id from roles_assigned where emp_code=" + Session("UserID").ToString() + "))) order by parent_id,order_id").Tables(0)
        'DT = DB.ExecuteDataSet("select  menu_id,  menu_name,   menu_parent_id,  menu_url,order_id from Menu_Master where status_id=1 and menu_id in(select  menu_parent_id from Menu_Master where status_id=1 and menu_id in(select form_id from role_dtl where role_id=" & Session("RoleID").ToString() & ")) union select  menu_id,  menu_name,   menu_parent_id,  menu_url,order_id  from Menu_Master where status_id=1 and menu_id in(select form_id from role_dtl where role_id=" & Session("RoleID").ToString() & ") and menu_parent_id is not null ORDER BY  3,5").Tables(0)
        DT = DB.ExecuteDataSet("select distinct menu_id,menu_name,menu_parent_id,menu_url,order_id from (select menu_id,menu_name,menu_parent_id,menu_url,order_id from Menu_Master where status_id=1 and module_id = " & Session("ModuleID") & " AND Menu_ID IN (SELECT FORM_ID FROM ROLE_DTL A,ROLES_ASSIGNED B WHERE A.ROLE_ID = B.ROLE_ID AND B.EMP_CODE = " & Session("UserID") & " AND A.STATUS_ID = 1 AND B.STATUS_ID = 1) AND  Menu_Parent_ID IS NOT NULL UNION ALL select menu_id,menu_name,menu_parent_id,menu_url,order_id from Menu_Master where status_id=1 and module_id = " & Session("ModuleID") & " AND Menu_parent_id IS NULL AND Menu_ID IN (SELECT distinct Menu_Parent_ID FROM MENU_MASTER WHERE Module_ID=" & Session("ModuleID") & " AND Menu_ID IN(SELECT FORM_ID FROM ROLE_DTL A,ROLES_ASSIGNED B WHERE A.ROLE_ID = B.ROLE_ID AND B.EMP_CODE = " & Session("UserID") & " AND A.STATUS_ID = 1 AND B.STATUS_ID = 1)))AA ORDER BY  3,5").Tables(0)
        Dim view As New DataView(DT)
        view.RowFilter = "menu_parent_id is NULL"
        For Each dv As DataRowView In view
            Dim menuitem = New MenuItem(dv("menu_name").ToString(), dv("menu_id").ToString())
            menuitem.NavigateUrl = dv("menu_url").ToString()
            menuBar.Items.Add(menuitem)
            AddChildItems(DT, menuitem)
        Next
    End Sub
    Private Sub AddChildItems(table As DataTable, menuItem As MenuItem)
        Dim view As New DataView(table)
        view.RowFilter = "menu_parent_id=" + menuItem.Value
        For Each childView As DataRowView In view
            Dim childItem = New MenuItem(childView("menu_name"), childView("menu_id").ToString())
            childItem.NavigateUrl = childView("menu_url").ToString()
            menuItem.ChildItems.Add(childItem)
            AddChildItems(table, childItem)
        Next
    End Sub
End Class
