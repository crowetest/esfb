﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class Compliance_Add_HOTeam_Master
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
    Protected Sub HRM_Master_AddNewDepartment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1411) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            If Not IsPostBack Then
                DT = DB.ExecuteDataSet("SELECT STUFF((select 'Ñ' +cast(aa.id as varchar)+'ÿ'+upper(bb.Emp_name)+'ÿ'+cast(aa.emp_code as varchar)+'ÿ'+cast(aa.Access_Date as varchar)from(select a.id,a.emp_code,a.access_date from Comp_HO_Master  a where a.[status]=1) aa left outer join EMP_MASTER bb on(aa.emp_code=bb.Emp_code and bb.Status_ID=1) group by aa.id,aa.emp_code,aa.Access_Date,bb.Emp_name  FOR XML PATH('')) ,1,1,'')").Tables(0)
                If (DT.Rows.Count > 0) Then
                    hdnData.Value = DT.Rows(0)(0).ToString()

                End If
                DT = DB.ExecuteDataSet("select -1 as EmployeeID,' -----SELECT-----' as Employee union all select Emp_Code,Emp_Name from emp_Master where status_id=1 and branch_id=0 order by 2").Tables(0)
                GN.ComboFill(cmdEmployee, DT, 0, 1)
                End If

                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "OnLoad();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1
                Dim id As Integer = CInt(Data(1))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(2) As SqlParameter
                    Params(0) = New SqlParameter("@id", SqlDbType.Int)
                    Params(0).Value = id
                    Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(1).Direction = ParameterDirection.Output
                    Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(2).Direction = ParameterDirection.Output
                    DT = DB.ExecuteDataSet("SP_Comp_DELETE_HOTeam_Master", Params).Tables(0)
                    ErrorFlag = CInt(Params(1).Value)
                    Message = CStr(Params(2).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message + "ʘ" + DT.Rows(0)(0)

            Case 2
                Dim id As Integer = Data(1)
                Dim EffectiveDate As String = Data(2)
                Dim EmpCode As Integer = CInt(Data(3))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(4) As SqlParameter
                    Params(0) = New SqlParameter("@id", SqlDbType.Int)
                    Params(0).Value = id
                    Params(1) = New SqlParameter("@EmpCode", SqlDbType.NVarChar, 50)
                    Params(1).Value = EmpCode
                    Params(2) = New SqlParameter("@EffectiveDate", SqlDbType.Date)
                    Params(2).Value = EffectiveDate
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    DT = DB.ExecuteDataSet("SP_Comp_ADD_EDIT_HOTeam_Master", Params).Tables(0)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message + "ʘ" + DT.Rows(0)(0)

            Case 4

                DT = DB.ExecuteDataSet("select Emp_Code+' - '+ AccessDate from Comp_HO_Master  where id=" + Data(1).ToString()).Tables(0)
                CallBackReturn += "ʘ" + DT.Rows(0)(0)
        End Select
    End Sub
End Class
