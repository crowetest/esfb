﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Compliance_ChangeComplaintsFillingPeriod
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
        Dim DB As New MS_SQL.Connect
        Dim DT As New DataTable
        Dim DT1 As New DataTable
        Dim DT2 As New DataTable
        Dim DT3 As New DataTable
        Dim DT4 As New DataTable
        Dim HD As New HelpDesk
        Dim GF As New GeneralFunctions
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1409) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            If GF.FormAccess(CInt(Session("UserID")), 1412) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim UserID As String = CStr(Session("UserID"))

            Dim QS1 As String = ""
            DT1 = DB.ExecuteDataSet("select b.branch_id,b.branch_name,a.Report_Filing_Period from  branch_master b left outer join Comp_Freq_Settings a on a.branch_id=b.branch_id  where b.branch_id<50000 order by b.branch_name ").Tables(0)
            For n As Integer = 0 To DT1.Rows.Count - 1
                QS1 += DT1.Rows(n)(0).ToString() + "ÿ" + DT1.Rows(n)(1).ToString() + "ÿ" + DT1.Rows(n)(2).ToString() + "Ñ"
            Next
            Me.hidQS1.Value = QS1


            DT3 = DB.ExecuteDataSet("SELECT QUARTER_NAME,START_DATE,REPORT_FILING_PERIOD,Freq_ID,convert(date,DATEADD(day,DATEDIFF(day, 0,START_DATE ),REPORT_FILING_PERIOD)) as END_DATE,case when getdate() between start_date and convert(date,DATEADD(day,DATEDIFF(day, 0,START_DATE ),REPORT_FILING_PERIOD)) then 1 else 0 end as date_status,Frequency FROM Comp_Freq_Settings WHERE Freq_ID = (SELECT MIN(Freq_ID) FROM Comp_FREQ_SETTINGS) ").Tables(0)
            If (DT3.Rows.Count > 0) Then
                Me.hidQDetails.Value = DT3.Rows(0)(0).ToString() + "ÿ" + DT3.Rows(0)(1).ToString() + "ÿ" + DT3.Rows(0)(2).ToString() + "ÿ" + DT3.Rows(0)(3).ToString() + "ÿ" + DT3.Rows(0)(4).ToString() + "ÿ" + DT3.Rows(0)(5).ToString() + "ÿ" + DT3.Rows(0)(6).ToString()
            End If
            Me.Master.subtitle = "Change Compliance Filling Period"
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", " DisplayGroup();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub
        Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
            Return CallBackReturn
        End Function
        Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
            Dim Data() As String = eventArgument.Split(CChar("Ø"))
            If CInt(Data(0)) = 1 Then
            Dim quarter As String = Data(2).ToString()
            Dim Created_By As Integer = CInt(Session("UserID"))
            Dim Freq_ID As Integer = CInt(Data(1).ToString())
            Dim noofdays As Integer = CInt(Data(3).ToString())
            Dim startdate As Date = CDate(Data(4).ToString())
            Dim Ans1 As String = Data(5).ToString()
            Dim ErrorFlag As Integer = 0
                Dim Message As String = Nothing
                Try
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@startdate", SqlDbType.Date)
                Params(0).Value = startdate

                Params(1) = New SqlParameter("@Frequency", SqlDbType.Int)
                Params(1).Value = Freq_ID
                Params(2) = New SqlParameter("@noofdays", SqlDbType.Int)
                Params(2).Value = noofdays
                Params(3) = New SqlParameter("@quarter", SqlDbType.NVarChar, 100)
                Params(3).Value = quarter
                Params(4) = New SqlParameter("@Ans1", SqlDbType.VarChar, 5000)
                Params(4).Value = Ans1
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_COMP_ChangeDate_SAVE", Params)
                Message = CStr(Params(6).Value)
                ErrorFlag = CInt(Params(5).Value)
            Catch ex As Exception
                    Message = Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
            End If

    End Sub
    End Class


