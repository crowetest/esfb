﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Compliance_Comp_Entry
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim DT5 As New DataTable
    Dim DT6 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1398) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Dim UserID As String = CStr(Session("UserID"))


            DT6 = DB.ExecuteDataSet("select branch_id,branch_name,branch_head from branch_master   where branch_head = '" + UserID + "'").Tables(0)
            If (DT6.Rows.Count > 0) Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            Else

                If GF.FormAccess(CInt(Session("UserID")), 1404) = False Then
                    Response.Redirect("~/AccessDenied.aspx", False)
                    Return
                End If
            End If
               
                Dim finish As Integer = CInt(Request.QueryString.Get("finish"))
                Dim Levelid As Integer = CInt(Request.QueryString.Get("Levelid"))
                Dim Actionid As Integer = CInt(Request.QueryString.Get("Actionid"))
                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            'Dim UserID As String = CStr(Session("UserID"))


            DT = DB.ExecuteDataSet("select b.branch_id,b.branch_name from emp_master a inner join branch_master b on a.branch_id = b.branch_id where a.emp_code = '" + UserID + "'").Tables(0)
                Me.hidBid.Value = DT.Rows(0)(0).ToString()
                Dim branchid As String = CStr(Session("BranchID"))

                Me.hidBName.Value = DT.Rows(0)(1).ToString()

                Dim QS1 As String = ""
                Dim QS2 As String = ""
                If (Levelid = 3) And (Actionid = 1) Then
                    DT1 = Nothing

                Else
                    DT6 = DB.ExecuteDataSet("select branch_id,Level_ID,Action_ID from Comp_Master where branch_id='" + branchid.ToString() + "'").Tables(0)
                    If (DT6.Rows.Count > 0) Then
                        If (DT6.Rows(0)(1).ToString() <> CStr(1)) And DT6.Rows(0)(2).ToString() = CStr(1) Then
                            Me.hidQS2.Value = DT6.Rows(0)(1).ToString()
                            ' Response.Write("<script language=""javascript"">alert('No Compliance Pending');</script>")

                        Else

                            Dim branch_id = DT6.Rows(0)(0).ToString()
                            DT5 = DB.ExecuteDataSet("SELECT  q.Question_ID,q.Question,q.Critical_Flag,d.Comp_Status,d.Reason,d.Entry_ID,q.Question_No  FROM Comp_Question_Master q" +
                                          " left join Comp_Question_Dtl d on q.Question_ID=d.Question_ID AND d.branch_id='" + branchid.ToString() + "' " +
    " Where q.Section_ID = 1 AND q.Status_ID = 1  ORDER BY q.Question_No").Tables(0)

                            For n As Integer = 0 To DT5.Rows.Count - 1
                                QS2 += DT5.Rows(n)(0).ToString() + "ÿ" + DT5.Rows(n)(1).ToString() + "ÿ" + DT5.Rows(n)(2).ToString() + "ÿ" + DT5.Rows(n)(3).ToString() + "ÿ" + DT5.Rows(n)(4).ToString() + "ÿ" + DT5.Rows(n)(5).ToString() + "ÿ" + DT5.Rows(n)(6).ToString() + "Ñ"
                            Next
                            Me.hidQS1.Value = QS2
                        End If
                    Else

                        DT1 = DB.ExecuteDataSet(" SELECT q.Question_ID,q.Question,q.Critical_Flag,q.Question_No  FROM Comp_Question_Master q" +
                                          " Where q.Section_ID = 1 AND q.Status_ID = 1   ORDER BY q.Question_No").Tables(0)
                        If (DT1.Rows.Count > 0) Then

                            For n As Integer = 0 To DT1.Rows.Count - 1
                                QS1 += DT1.Rows(n)(0).ToString() + "ÿ" + DT1.Rows(n)(1).ToString() + "ÿ" + DT1.Rows(n)(2).ToString() + "ÿ" + "" + "ÿ" + "" + "ÿ" + "" + "ÿ" + DT1.Rows(n)(3).ToString() + "Ñ"
                            Next
                            Me.hidQS1.Value = QS1

                        End If

                    End If
                End If
                DT3 = DB.ExecuteDataSet(" SELECT QUARTER_NAME,START_DATE,REPORT_FILING_PERIOD,Freq_ID,convert(date,DATEADD(day,DATEDIFF(day, 0,START_DATE ),REPORT_FILING_PERIOD)) as END_DATE,case when getdate() between start_date and convert(date,DATEADD(day,DATEDIFF(day, 0,START_DATE ),REPORT_FILING_PERIOD)) then 1 else 0 end as date_status FROM Comp_Freq_Settings WHERE Freq_ID = (SELECT MAX(Freq_ID) FROM Comp_FREQ_SETTINGS where Branch_id='" + branchid.ToString() + "') ").Tables(0)
                If (DT3.Rows.Count > 0) Then
                    Me.hidQDetails.Value = DT3.Rows(0)(0).ToString() + "ÿ" + DT3.Rows(0)(1).ToString() + "ÿ" + DT3.Rows(0)(2).ToString() + "ÿ" + DT3.Rows(0)(3).ToString() + "ÿ" + DT3.Rows(0)(4).ToString() + "ÿ" + DT3.Rows(0)(5).ToString() + "ÿ" + finish.ToString()
                End If
                DT4 = DB.ExecuteDataSet("select * from Comp_Master where Branch_ID = " + DT.Rows(0)(0).ToString() + "  and freq_id = " + DT3.Rows(0)(3).ToString()).Tables(0)
                If DT4.Rows.Count = 0 Then
                    Me.hidDataHis.Value = CStr(1)
                Else
                    Me.hidDataHis.Value = CStr(0)
                End If
                Me.Master.subtitle = "Quaterly Compliance Management"
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", " tablefill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim Branch_ID As Integer = CInt(Data(1).ToString())
            Dim Created_By As Integer = CInt(Session("UserID"))
            Dim Freq_ID As Integer = CInt(Data(2).ToString())
            Dim Ans1 As String = Data(3).ToString()
            Dim Level_ID As Integer = 1
            Dim Action_ID As Integer = 1


            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Try
                Dim Params(10) As SqlParameter
                Params(0) = New SqlParameter("@Branch_ID", SqlDbType.Int)
                Params(0).Value = Branch_ID
                Params(1) = New SqlParameter("@Level_ID", SqlDbType.Int)
                Params(1).Value = Level_ID
                Params(2) = New SqlParameter("@Action_ID", SqlDbType.Int)
                Params(2).Value = Action_ID
                Params(3) = New SqlParameter("@Created_By", SqlDbType.Int)
                Params(3).Value = Created_By
                Params(4) = New SqlParameter("@Done_By", SqlDbType.Int)
                Params(4).Value = Created_By
                Params(5) = New SqlParameter("@Freq_ID", SqlDbType.Int)
                Params(5).Value = Freq_ID
                Params(6) = New SqlParameter("@Ans1", SqlDbType.VarChar, 5000)
                Params(6).Value = Ans1
                Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(8).Direction = ParameterDirection.Output
                Params(9) = New SqlParameter("@Status_ID", SqlDbType.Int)
                Params(9).Value = 1 'status_id 1 for save
                Params(10) = New SqlParameter("@Remarks", SqlDbType.VarChar, 5000)
                Params(10).Value = ""

                DB.ExecuteNonQuery("SP_COMP_ENTRY_SAVE", Params)
                Message = CStr(Params(8).Value)
                ErrorFlag = CInt(Params(7).Value)
            Catch ex As Exception
                Message = Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
        End If


        If CInt(Data(0)) = 2 Then

            Dim Branch_ID As Integer = CInt(Session("BranchID"))
            Dim Status As Integer = CInt(Data(1).ToString())
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Try
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@Branch_ID", SqlDbType.Int)
                Params(0).Value = Branch_ID
                Params(1) = New SqlParameter("@FinishStatus", SqlDbType.Int)
                Params(1).Value = Status
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_COMP_ENTRY_SAVE", Params)

                Message = CStr(Params(3).Value)
                ErrorFlag = CInt(Params(2).Value)

            Catch ex As Exception
                Message = Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
        End If
    End Sub
End Class
