﻿
Imports System.Data
Imports System.Data.SqlClient
Partial Class Compliance_AddQuestionMaster
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
        Dim CallBackReturn As String = Nothing
        Dim DB As New MS_SQL.Connect
        Dim GF As New GeneralFunctions
        Dim DT As New DataTable
        Dim RequestID As Integer
#Region "Page Load & Dispose"
        Protected Sub Vet_Opinion_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
            GC.Collect()
            DT.Dispose()
        End Sub
        Protected Sub Vet_Opinion_Load(sender As Object, e As System.EventArgs) Handles Me.Load
            Try
            If GF.FormAccess(CInt(Session("UserID")), 1414) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Me.Master.subtitle = "Compliance Points"
            If Not IsPostBack Then
                DT = DB.ExecuteDataSet("SELECT STUFF((select 'Ñ' +cast(aa.question_id as varchar)+'ÿ'+cast(aa.Question_No as varchar)+'ÿ'+cast(aa.question as varchar(max))+'ÿ'+cast(aa.Critical_Flag as varchar)from(select a.question_id,a.question_no,a.question,a.critical_flag from Comp_Question_Master  a where a.status_id=1) aa  group by aa.question_id,aa.question_no,aa.question,aa.critical_flag order by aa.question_no  FOR XML PATH('')) ,1,1,'')").Tables(0)

                hdnData.Value = DT.Rows(0)(0)

            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
                Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "OnLoad();", True)

        Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub
#End Region
#Region "Call Back"
        Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
            Return CallBackReturn
        End Function

        Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1
                Dim Questionid As Integer = CInt(Data(1))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(2) As SqlParameter
                    Params(0) = New SqlParameter("@questionid", SqlDbType.Int)
                    Params(0).Value = Questionid
                    Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(1).Direction = ParameterDirection.Output
                    Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(2).Direction = ParameterDirection.Output
                    DT = DB.ExecuteDataSet("SP_Comp_DELETE_Question_Master", Params).Tables(0)
                    ErrorFlag = CInt(Params(1).Value)
                    Message = CStr(Params(2).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message + "ʘ" + DT.Rows(0)(0)

            Case 2
                Dim questionid As Integer = Data(1)
                Dim No As Integer = CInt(Data(2))
                Dim Question As String = CStr(Data(3))
                Dim Flag As Integer = CInt(Data(4))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(5) As SqlParameter
                    Params(0) = New SqlParameter("@questionid", SqlDbType.Int)
                    Params(0).Value = questionid
                    Params(1) = New SqlParameter("@Qno", SqlDbType.Int)
                    Params(1).Value = No
                    Params(2) = New SqlParameter("@Question", SqlDbType.VarChar, 5000)
                    Params(2).Value = Question
                    Params(3) = New SqlParameter("@flag", SqlDbType.Int)
                    Params(3).Value = Flag
                    Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(5).Direction = ParameterDirection.Output
                    ' DT = + "ʘ" + DT.Rows(0)(0)
                    DT = DB.ExecuteDataSet("SP_Comp_ADD_EDIT_Question_Master", Params).Tables(0)
                    ErrorFlag = CInt(Params(4).Value)
                    Message = CStr(Params(5).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try

                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message + "ʘ" + DT.Rows(0)(0)

            Case 4

                DT = DB.ExecuteDataSet("select Question_No,Question,Critical_Flag from Comp_Question_Master  where question_id=" + Data(1).ToString()).Tables(0)
                CallBackReturn += "ʘ" + DT.Rows(0)(0)
        End Select

    End Sub
#End Region
    End Class


