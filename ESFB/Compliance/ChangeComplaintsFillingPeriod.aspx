﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ChangeComplaintsFillingPeriod.aspx.vb" Inherits="Compliance_ChangeComplaintsFillingPeriod" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script src="../Script/jquery.min.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);          
        }        
        .Button:hover
        {           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);            
            color:#801424;
        }                   
     .bg
     {
         background-color:#FFF;
     }
     .style1
        {
            width: 40%;
            height: 104px;
        }           
    </style>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/jquery.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">       
        window.onload = function () {

           
            
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                 alert(Data[1]);
              //  $(".error-messages").text(Data[1]).fadeIn();
                if (Data[0] == 0) {
                  
                    if(Data[1]=="Saved Successfully")
                    {
                        DisplayGroup();
                    }
                }
            }
            if(context == 2){
                
            }
            if(context == 3){
                      
            }
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
        function white_space(field)
        {
            if(field.value.length==1){
                field.value = (field.value).replace(' ','');
            }
        }
        function NumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = ((e.which >= 48 && e.which <= 57) || (e.keyWhich == 9) || (e.keyCode == 9) || (e.keyWhich == 8) || (e.keyCode == 8));
            //alert(valid);
            if (!valid) {
                e.preventDefault();
            }

        }
        function DecimalCheck(e,el)
        {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                    if (unicode == 37 || unicode == 38) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (unicode == 46) {
                        var parts = el.value.split('.');
                        if (parts.length == 2)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            else {
                return true;
            }
        }
         function DisplayGroup()
        {
             //debugger;
               var QDetails = document.getElementById("<%= hidQDetails.ClientID %>").value.split("ÿ");
          
                document.getElementById("<%= cmbfreq.ClientID %>").value = QDetails[6];
             if (QDetails[2]!="")
            document.getElementById("<%= txtnoofdays.ClientID %>").value = QDetails[2];
             if (QDetails[0] != "")
                 document.getElementById("<%= txtquarter.ClientID %>").value = QDetails[0].toString();
             if (QDetails[1] != "")
                    document.getElementById("<%= txtstartdate.ClientID %>").value = QDetails[1];
            var QS1 = document.getElementById("<%= hidQS1.ClientID %>").value.split("Ñ");

            var Count1 = QS1.length - 1;
            if (Count1 > 0) {
                document.getElementById("<%= pnlSec1.ClientID %>").style.display = '';


                var row_bg1 = 0;
                var SlNo = 0;
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>Date Extension </td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:15%;text-align:center' >Select All<input type='checkbox' id='chkSelectall" + Count1 + "' onchange=SelectOnAllClick(" + Count1 + ") /></td>";
                Tab += "<td style='width:45%;text-align:left'>Branch&nbsp;Name</td>";
                Tab += "<td style='width:20%;text-align:left'>StartDate</td>";
                Tab += "<td style='width:20%;text-align:left'>FillingPeriod</td>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:360px; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                for (var i = 0; i < Count1; i++) {
                    var qData1 = QS1[i].toString().split("ÿ");

                    if (row_bg1 == 0) {
                        row_bg1 = 1;
                        Tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg1 = 0;
                        Tab += "<tr class='sub_second';>";
                    }
                    SlNo = SlNo + 1;

                    Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                    Tab += "<td style='width:15%;text-align:center'><input type='checkbox' id='chkSelect" + qData1[0] + "' onchange=SelectOnClick(" + qData1[0] + ") /></td>";
                    Tab += "<td style='width:45%;text-align:left'><input id='txtbranch" + qData1[0] + "' name='txtbranch" + qData1[0] + "' disabled=true value='" + qData1[1] + "' type='Text' maxlength='100' style='width:100%;' class='NormalText'/></td>";
                    Tab += "<td style='width:20%;text-align:left'><input id='txtstartdate" + qData1[0] + "' name='txtstartdate" + qData1[0] + "' disabled=true value='" + QDetails[1] + "' type='Text' maxlength='100' style='width:100%;' class='NormalText'/></td>";
                    if (qData1[2].toString() =='') {
                        Tab += "<td style='width:20%;text-align:left'><input id='txtfillingPeriod" + qData1[0] + "' name='txtfillingPeriod" + qData1[0] + "' disabled=true value='" + QDetails[2] + "' type='Text' maxlength='100' style='width:100%;' class='NormalText'/></td>";
                    }
                    else {
                        Tab += "<td style='width:20%;text-align:left'><input id='txtfillingPeriod" + qData1[0] + "' name='txtfillingPeriod" + qData1[0] + "' disabled=true value='" + qData1[2].toString() + "' type='Text' maxlength='100' style='width:100%;' class='NormalText'/></td>";
                    }
                }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div>";
                    Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                    Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    Tab += "<tr class='sub_second';>";
                    Tab += "<td style='width:5%;text-align:center'></td></tr>";
                    Tab += "</table></div>";
                    document.getElementById("<%= pnlSec1.ClientID %>").innerHTML = Tab;
                }
            
            }
       function SelectOnAllClick(val) {
            //debugger;
          
        
                var qs= document.getElementById("<%= hidQS1.ClientID %>").value.split("Ñ");
          
            var Count1 = qs.length - 1;
          
                for (var i = 0; i < Count1; i++)
                {
                    var qData1 = qs[i].toString().split("ÿ");
                    document.getElementById("chkSelect" + qData1[0]).checked = true;
                }
          
        }
       
        function SelectOnClick(val) {
            //debugger;
            if (document.getElementById("chkSelect" + val).checked == true) {
                document.getElementById("txtfillingPeriod" + val).disabled = false;
                document.getElementById("txtbranch" + val).focus();
            }
            else {
                document.getElementById("txtfillingPeriod" + val).disabled = true;
               
                document.getElementById("txtbranch" + val).focus();
            }

        }
     function Saveonclick() {
         //debugger;
           var freq=  document.getElementById("<%= cmbfreq.ClientID %>").value;
            
        
           var noofdays= document.getElementById("<%= txtnoofdays.ClientID %>").value ;
           
         var quarter = document.getElementById("<%= txtquarter.ClientID %>").value;
         var startdate= document.getElementById("<%= txtstartdate.ClientID %>").value;
            var QS1 = document.getElementById("<%= hidQS1.ClientID %>").value.split("Ñ");

            var Count1 = QS1.length - 1;
            var Ans1 = "";
         
            if (Count1 > 0) {
                for (var i = 0; i < Count1; i++) {
                    var qData1 = QS1[i].toString().split("ÿ");
                    if (document.getElementById("chkSelect" + qData1[0]).checked == true) {
                        var branchid =  qData1[0]
                        var fillingperiod = document.getElementById("txtfillingPeriod" + qData1[0]).value

                        Ans1 += branchid.toString() +"ÿ"+fillingperiod.toString()+ "Ñ";

                    }

                }

            }
                var saveData = "1Ø" + freq.toString() + "Ø" + quarter.toString() + "Ø" + noofdays.toString() + "Ø" + startdate.toString()+"Ø" +Ans1.toString();
                ToServer(saveData, 1);

        }

    </script>
</head>
</html>
<br />
  
<div id="parentTable" style="width:90%; height:auto; margin:0px auto; background-color:#EEB8A6;">
    <br />
    <div id = "divSection1" class = "sec1" style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table class="style1" style="width:70%;margin: 0px auto;height:auto;line-height:35px;">
            <tr> 
                <td style="width:20%;">
                    
                    <asp:HiddenField ID="hidQDetails" runat="server" />
                   
                    <asp:HiddenField ID="hidQS1" runat="server" />
                  
                </td>
                <td style="width:30%; text-align:left;">
                </td>
                <td style="width:20%">
                </td>
                <td style="width:30%">
                </td>
            </tr>
            <tr> 
                <td>
                    Quarter  :
                </td>
                <td>
                    <asp:TextBox ID="txtquarter"  runat="server" Width="87%" ></asp:TextBox>
                </td>
                <td>
                    Frequency :
                </td>
                <td>
                    <asp:DropDownList ID="cmbfreq" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="89%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                        <asp:ListItem Value="1"> Yearly</asp:ListItem>
                        <asp:ListItem Value="2"> HalfYearly</asp:ListItem>
                        <asp:ListItem Value="3"> Quarterly</asp:ListItem>
                        <asp:ListItem Value="4"> Monthly</asp:ListItem>
                </asp:DropDownList>
                </td>
            </tr>
            <tr> 
                <td>
                    Start Date :
                </td>
                <td>
                    <asp:TextBox ID="txtstartdate"  runat="server" Width="87%" ></asp:TextBox>
                      <asp:CalendarExtender ID="txtstartdate_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtstartdate" Format="dd MMM yyyy">
                </asp:CalendarExtender>
                </td>
                <td>
                    No.Of Days :
                </td>
                <td>
                    <asp:TextBox ID="txtnoofdays"  runat="server" Width="87%" ></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table ID="tblSec1" style="width:90%;height:90px;margin:0px auto;">            
            <tr id="Tr1">
                <td style="text-align: center;">
                    <asp:Panel ID="pnlSec1" Style="width: 100%;  text-align: left; float: left;" runat="server">               

                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />
        <br />
       <div class="error-messages" style="width:90px;height:10px;text-align:center; display:none;"></div>
        <br /><br />    
    </div>
     
    <div style="width:80%;height:auto; padding-left:10px;margin:0px auto;">
        <div style="text-align:center; height: 63px;"><br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="SAVE" onclick="return Saveonclick()"/>       
            &nbsp;
            &nbsp;
       
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
        </div>
    </div>
</div>
</asp:Content>

