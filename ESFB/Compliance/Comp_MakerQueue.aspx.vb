﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient

Partial Class Compliance_Comp_MakerQueue
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT6 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1401) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Dim UserID As String = CStr(Session("UserID"))


            DT6 = DB.ExecuteDataSet("select branch_id,branch_name,branch_head from branch_master   where branch_head = '" + UserID + "'").Tables(0)
            If (DT6.Rows.Count > 0) Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            Else

                If GF.FormAccess(CInt(Session("UserID")), 1406) = False Then
                    Response.Redirect("~/AccessDenied.aspx", False)
                    Return
                End If
            End If
          
            Me.Master.subtitle = "Compliance Maker Queue"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim EmpCode = CInt(Session("UserID"))
            Dim branchid As String = CStr(Session("BranchID"))
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
            If Not IsPostBack Then
                DT = DB.ExecuteDataSet("SELECT A.BRANCH_ID,B.BRANCH_NAME,c.Done_Date,c.Level_ID,c.Action_ID,c.Remarks FROM Comp_Master A INNER JOIN BRANCH_MASTER B ON A.BRANCH_ID = B.BRANCH_ID INNER JOIN Comp_Cycle c on c.Comp_ID=A.Comp_ID WHERE  c.Level_ID!=1 and a.branch_id='" + branchid.ToString() + "' ").Tables(0)
                Dim i As Integer = 0
                For Each dtr In DT.Rows
                    Me.hidQueueData.Value += DT.Rows(i)(0).ToString() + "ÿ" + DT.Rows(i)(1).ToString() + "ÿ" + DT.Rows(i)(2).ToString() + "ÿ" + DT.Rows(i)(3).ToString() + "ÿ" + DT.Rows(i)(4).ToString() + "ÿ" + DT.Rows(i)(5).ToString() + "Ñ"
                    i += 1
                Next
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EmpID As Integer = CInt(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then

        ElseIf CInt(Data(0)) = 2 Then

        ElseIf CInt(Data(0)) = 3 Then

        ElseIf CInt(Data(0)) = 4 Then

        End If
    End Sub
#End Region




End Class
