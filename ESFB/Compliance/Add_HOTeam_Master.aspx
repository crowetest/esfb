﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Add_HOTeam_Master.aspx.vb" Inherits="Compliance_Add_HOTeam_Master" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
    <table align="center" style="width: 80%; margin:0px auto;">
        <tr>
            <td style="width:30%;text-align:left;">
                &nbsp;</td>
            <td style="width:70%;text-align:left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:30%;text-align:left;">
                &nbsp;</td>
            <td style="width:70%;text-align:left;">
                </td>
        </tr>
        <tr id="rowList">
            <td style="text-align:left;" colspan="2">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
        </tr>
      
        <tr id="rowAdd" style="display:none;" >
            <td style="text-align:left;" colspan="2">
            <div style="width:50%; height:150px;  margin: 0px auto; " class="sub_first">
                <table align="center" style="margin:0px auto;width:100%;">
                    <tr>
                        <td id="subHd" colspan="2"  style="width:20%;text-align:center; color:maroon;height:30px; font-weight:bold;" class="mainhead">Add New HO Team</td>
                            
                    </tr>
                    <tr class="sub_second" style="height:28px;">
                        <td style="width:30%;">
                            Employee Name</td>
                        <td  style="width:70%;">
                            <asp:DropDownList ID="cmdEmployee" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="68%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="sub_second" style="height:28px;">
                        <td style="width:30%;">
                            Effective Date</td>
                        <td  style="width:70%;">
             
                <asp:TextBox ID="txtDate" runat="server" Width="68%" 
                    MaxLength="100"  style="text-transform:uppercase;"></asp:TextBox>
                            <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtDate" Format="dd MMM yyyy">
                </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr class="sub_second" style="height:40px;">
                        <td colspan="2" style="text-align:center;">
                <input id="btnSave" 
                    style="font-family: Cambria; font-size: 10pt; font-weight: normal; width: 20%;height:30px;" 
                    type="button" value="SAVE" 
                     onclick="return btnSave_onclick()" />&nbsp;&nbsp;<input id="btnExitAdd" 
                    style="font-family: Cambria; font-size: 10pt; font-weight: normal; width: 20%;height:30px;" 
                    type="button" value="EXIT"    onclick="return btnExitAdd_onclick()" /></td>
                    </tr>
                </table></div></td>
        </tr>     
        <tr>
            <td style="text-align:left;" colspan="2">
                &nbsp;</td>
        </tr>
      <%--  <tr id="rowExit">
            <td style="text-align:center;" colspan="2">--%>
                <%--<input id="btnExit" 
                    style="font-family: Cambria; font-size: 10pt; font-weight: normal; width: 10%; " 
                    type="button" value="EXIT"  onclick="return btnExit_onclick()" />--%></td>
      <%--  </tr>--%>
        <tr>
            <td style="width:30%;text-align:left;">
                <asp:HiddenField ID="hdnData" runat="server" />
            </td>
            <td style="width:70%;text-align:left;">
                &nbsp;</td>
        </tr>
    </table>
    <style type="text/css">
          #Button
        {
            width:90%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
           color:#E0E0E0;
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
            color:#036;
        }        
    
      #loadingover {
      background: black;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }
     #loadingmsg {           
      z-index: 100;  
      position:fixed; 
      top:50px;
      left:50px;
      }
        
    </style>
     <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var EmployeeID = 0;
        function btnExit_onclick() {
            window.open("../home.aspx","_self");
        }
        window.onload = function () {

            FillHOTeam();
        }
        function displayChange(ID) {
            document.getElementById("rowAdd").style.display = "none";
            document.getElementById("rowList").style.display = "none";
           // document.getElementById("rowExit").style.display = "none";
          
            if (ID == 1) {
                document.getElementById("rowList").style.display = "";
                document.getElementById("rowExit").style.display = "";
            }
            else if (ID == 2) {

                if (EmployeeID == 0) {
                    document.getElementById("btnSave").value = "ADD";
                    document.getElementById("subHd").innerHTML = "Add New HOTeam";
                    document.getElementById("<%= cmdEmployee.ClientID %>").value = -1;
                    document.getElementById("<%= txtDate.ClientID %>").value = "";
                  
                }
                else {
                    document.getElementById("btnSave").value = "UPDATE";
                    document.getElementById("subHd").innerHTML = "Edit HOTeam Details";
                    var rowval = document.getElementById("<%= hdnData.ClientID %>").value.split("Ñ");
                    for (n = 0; n <= rowval.length - 1; n++) {
                        colval = rowval[n].split("ÿ");
                       
                        if (EmployeeID == colval[0]) {
                         
                            document.getElementById("<%= cmdEmployee.ClientID %>").value = colval[2];
                            document.getElementById("<%= txtDate.ClientID %>").value = colval[3];                           
                            break;
                        }
                    }
                }
                document.getElementById("rowAdd").style.display = "";
            }
         
        }
        function AddHOTeam() {
     
            EmployeeID = 0;
            displayChange(2);
        }
        function FillHOTeam() {
            //debugger;
            var row_bg = 0;
            var tab = "";
            var cnt = 0;
            tab += "<div style='width:70%; height:auto; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria'; ' align='center'>";
            tab += "<tr style='height:30px;' >";
            tab += "<td style='width:5%;text-align:left ; color:#FFF' ><div id='Button' onClick='AddHOTeam()' >New</div></td>";
            tab += "<td style='width:20%;text-align:center; color:Maroon' colspan=4><b>HOTeam List</b></td>";
            tab += "<td style='width:3%;text-align:right; color:#FFF' colspan=2> <img id='imgClose' src='../Image/close.png' onclick='return btnExit_onclick()' style='height:25px;width:25px; float:right; padding-right:5px;'></td>";
           
            tab += "</tr>";
            tab += "<tr style='height:30px; background-color:#B21C32; ' >";
            tab += "<td style='width:10%;text-align:center ; color:#FFF' ><b>#</b></td>";
            tab += "<td style='width:20%;text-align:left; color:#FFF'><b>Employee Name</b></td>";
            tab += "<td style='width:20%;text-align:left; color:#FFF'><b>Employee Code</b></td>";
            tab += "<td style='width:40%;text-align:left; color:#FFF'><b>Effective Date</b></td>";
            tab += "<td style='width:5%;text-align:left; color:#FFF'></td>";
            tab += "<td style='width:5%;text-align:left; color:#FFF'></td>";
            tab += "</tr>";
            tab += "</table></div>";
            tab += "<div style='width:70%; height:425px; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria'; ' align='center'>";
            if (document.getElementById("<%= hdnData.ClientID %>").value != "") {
                rowval = document.getElementById("<%= hdnData.ClientID %>").value.split("Ñ");
                for (n = 0; n <= rowval.length - 1; n++) {
                    colval = rowval[n].split("ÿ");
                  
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first' style='height:28px;'  >";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second' style='height:28px;' >";
                    }
                    i = n + 1;

                    tab += "<td style='width:10%;text-align:center; '>" + i + "</td>";
                    tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" + colval[1] + "</td>";
                    tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" + colval[2] + "</td>";
                    tab += "<td style='width:40%;text-align:left;font-size:9pt;'>" + colval[3] + "</td>";
                    //if (colval[4] == 0)
                    //    tab += "<td style='width:20%;text-align:center;font-size:9pt;'></td>";
                    //else
 
                    tab += "<td  style='width:5%;text-align:center' ><img src='../image/edit1.png' title='Edit'  style='height:18px;width:18px;cursor:pointer'  onclick=EditOnClick(" + colval[0] + ") /></td>";
                    tab += "<td  style='width:5%;text-align:center' ><img src='../image/delete.png' title='Delete'  style='height:18px;width:18px;cursor:pointer'  onclick=DeleteOnClick(" + colval[0] + ") /></td>";
                    tab += "</tr>";
                    //cnt +=parseInt(colval[2]);
                }
            }
            tab += "</table></div>";            
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;

        }

        function RowOnClick(ID) {
            ToServer("4ʘ" + ID, 4);      
          //  window.open("ShowEmpDetails.aspx?DepartmentID=" + btoa(ID) + "", "_blank", "toolbar=0,top=" + getOffset(document.getElementById("<%= pnDisplay.ClientID %>")).top + "px,left=" + getOffset(document.getElementById("<%= pnDisplay.ClientID %>")).left + "px,width=900,height=400,,menubar=0,center=1,channelmode =1,scrollbars=2,status=0,titlebar=0");
        }
        function EditOnClick(ID) {
            EmployeeID = ID;
            displayChange(2);
        }
        function DeleteOnClick(ID) {
          
                if(confirm("Are you sure to delete this HO From Team?")==true)
                ToServer("1ʘ"+ID, 1);
          
                // alert("There are <span style='color:red; font-weight:bold;'> live employees </span>in this department.Hence <span style='color:red; font-weight:bold;'>Deletion is not possible.</span>"); return false;
               // alert("There are  live employees with this Designation.Hence Deletion is not possible."); return false;
            
        }
        function FromServer(Arg, Context) {
            //debugger;
            switch (Context) {
                case 1:
                    {
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        document.getElementById("<%= hdnData.ClientID %>").value = Data[2];
                        FillHOTeam();
                        break;
                    }
                case 2:
                    {
                        var Dtl = Arg.split("ʘ");
                        alert(Dtl[1]);
                        if (Dtl[0] == 0) {
                            document.getElementById("<%= hdnData.ClientID %>").value = Dtl[2];
                            FillHOTeam();
                            displayChange(1);
                        }
                        break;
                    }
          
            }
        }
        function btnExitAdd_onclick() {
            displayChange(1);
        }



        function btnSave_onclick() {
            var CadreID = document.getElementById("<%= cmdEmployee.ClientID %>").value;
            if (CadreID == -1)
            { alert("Select Employee"); document.getElementById("<%= cmdEmployee.ClientID %>").focus(); return false; }           
            var DesignationName = document.getElementById("<%= txtDate.ClientID %>").value;
            if (DesignationName == "")
            { alert("Enter Effective Date"); document.getElementById("<%= txtDate.ClientID %>").focus(); return false; }           
            ToServer("2ʘ" + EmployeeID + "ʘ" + DesignationName + "ʘ" + CadreID, 2);          
        }


    </script>
</asp:Content>




















