﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Compliance_Reports_View_Detailed_QCC_Report
    Inherits System.Web.UI.Page

    Dim DB As New MS_SQL.Connect
        Dim DT As New DataTable
        Dim DTEXCEL As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim DT5 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
        Dim tb As New Table
        Dim PostID As Integer
        Dim GN As New GeneralFunctions
        Dim WebTools As New WebApp.Tools
        Dim TraDt As Date

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim DT As New DataTable
                Dim ViewType As Integer
                ViewType = CInt(Request.QueryString.Get("ViewType"))
                Dim Freq = Request.QueryString.Get("Quarter_ID")
                Dim strwhere As String = ""
                Dim NA As String = ""
                Dim Comp As String = ""
                Dim NonComp As String = ""
                Dim critic As String = ""
                Dim branchID = Request.QueryString.Get("Branch_Code")

            Dim qno As String = ""
            'and e.Comp_Status=2
            DT3 = DB.ExecuteDataSet("select distinct a.branch_id,a.branch_name,f.Question_No,l.Quarter_name,l.Start_date from branch_master a inner join Comp_Master c on c.branch_id=a.branch_id  inner join Comp_Cycle d on d.Comp_id=c.Comp_id inner join Comp_Question_Dtl e on e.Cycle_ID=d.Cycle_ID inner join Comp_Question_Master f on f.Question_ID=e.Question_ID inner join Comp_Freq_Settings l on l.Freq_ID=c.Freq_ID where a.branch_id=" + branchID.ToString() + " ").Tables(0)
            DT5 = DB.ExecuteDataSet("select distinct Question_No from Comp_Question_Master c inner join Comp_Question_Dtl a on a.Question_ID=c.Question_ID where Comp_Status=2 and branch_id=" + branchID.ToString() + "").Tables(0)
            For n As Integer = 0 To DT5.Rows.Count - 1

                qno += DT5.Rows(n)(0).ToString() + ","
            Next

            Label19.Text = DT3.Rows(0)(0).ToString()
            Label7.Text = DT3.Rows(0)(1).ToString()
            Label10.Text = DT3.Rows(0)(3).ToString() + ")"
            Label11.Text = DT3.Rows(0)(1).ToString()
            Label12.Text = DT3.Rows(0)(4).ToString()
            Label14.Text = qno.TrimEnd(",")
            Dim dat As DateTime = DateTime.Now

            Label8.Text = dat.Date

            DT1 = DB.ExecuteDataSet("select distinct a.Emp_Name from emp_master a inner join Comp_Cycle c on c.Done_By=a.Emp_code inner join Comp_Master d on d.Comp_ID=c.Comp_ID  where d.branch_id=" + branchID.ToString() + " and c.Level_ID=2 and c.Action_ID=1").Tables(0)
            Label17.Text = DT1.Rows(0)(0).ToString()

            DT2 = DB.ExecuteDataSet("select distinct a.Emp_Name from emp_master a inner join Comp_Question_Dtl c on c.Created_By=a.Emp_code where c.branch_id=" + branchID.ToString() + " and c.Entry_ID= (select max(Entry_ID) from Comp_Question_Dtl where branch_id=" + branchID.ToString() + ") ").Tables(0)
            Label2.Text = DT2.Rows(0)(0).ToString()

            DT4 = DB.ExecuteDataSet("select distinct a.Emp_Name from emp_master a inner join Comp_Cycle c on c.Done_By=a.Emp_code where c.Level_ID=3 and c.Action_ID=1").Tables(0)
            Label16.Text = DT4.Rows(0)(0).ToString()

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
                cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RH.Heading("", tb, "Detailed QCC Report", 100)
            tb.Attributes.Add("width", "88%")

            Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.WhiteSmoke

                Dim RowBG As Integer = 0
                Dim DR As DataRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell

            TRHead_00.BorderWidth = "1"
                TRHead_01.BorderWidth = "1"
                TRHead_02.BorderWidth = "1"
                TRHead_03.BorderWidth = "1"
                TRHead_04.BorderWidth = "1"
                TRHead_05.BorderWidth = "1"
                TRHead_06.BorderWidth = "1"



            TRHead_00.BorderColor = Drawing.Color.Silver
                TRHead_01.BorderColor = Drawing.Color.Silver
                TRHead_02.BorderColor = Drawing.Color.Silver
                TRHead_03.BorderColor = Drawing.Color.Silver
                TRHead_04.BorderColor = Drawing.Color.Silver
                TRHead_05.BorderColor = Drawing.Color.Silver
                TRHead_06.BorderColor = Drawing.Color.Silver



            TRHead_00.BorderStyle = BorderStyle.Solid
                TRHead_01.BorderStyle = BorderStyle.Solid
                TRHead_02.BorderStyle = BorderStyle.Solid
                TRHead_03.BorderStyle = BorderStyle.Solid
                TRHead_04.BorderStyle = BorderStyle.Solid
                TRHead_05.BorderStyle = BorderStyle.Solid
                TRHead_06.BorderStyle = BorderStyle.Solid


            TRHead_00.Font.Bold = True
                TRHead_01.Font.Bold = True
                TRHead_02.Font.Bold = True
                TRHead_03.Font.Bold = True
                TRHead_04.Font.Bold = True
                TRHead_05.Font.Bold = True
                TRHead_06.Font.Bold = True




            RH.AddColumn(TRHead, TRHead_00, 4, 4, "c", "Sl.No")
            RH.AddColumn(TRHead, TRHead_01, 30, 30, "c", "Compliance Points")
            RH.AddColumn(TRHead, TRHead_02, 4, 4, "c", "Compliance No")
            RH.AddColumn(TRHead, TRHead_03, 4, 4, "c", "Critical Y/N")
            RH.AddColumn(TRHead, TRHead_04, 8, 8, "c", "Compliance Status")
            RH.AddColumn(TRHead, TRHead_05, 14, 14, "c", "Remarks")
            RH.AddColumn(TRHead, TRHead_06, 8, 8, "c", "Branch Name")



            tb.Controls.Add(TRHead)

                RH.BlankRow(tb, 3)
                Dim i As Integer


            DT = DB.ExecuteDataSet(" SELECT  q.Question,q.Question_No,q.Critical_Flag,d.Comp_Status,d.Reason,a.branch_name  FROM Comp_Question_Master q" +
                                      " inner join Comp_Question_Dtl d on q.Question_ID=d.Question_ID AND d.branch_id='" + branchID.ToString() + "' " +
                                     " inner join branch_master a on a.branch_id=d.branch_id" +
                                     " inner join Comp_Master b on b.branch_id=d.branch_id" +
            " Where q.Section_ID = 1 AND q.Status_ID = 1 AND b.Level_ID=3 AND b.Action_ID=1  ORDER BY q.Question_No").Tables(0)



            DTEXCEL = DB.ExecuteDataSet("SELECT  q.Question As [Compliance Points],q.Question_No As [Compliance No],case when q.Critical_Flag=0 then 'N'else 'Y' end As [Critical Y/N]" +
" ,case when d.Comp_Status=1 then 'Complied' when d.Comp_Status=2 then 'NonComplied' else 'NA' end As [Compliance Status],d.Reason As [Remarks],a.branch_name As [BranchName]  FROM Comp_Question_Master q" +
                                      " inner join Comp_Question_Dtl d on q.Question_ID=d.Question_ID AND d.branch_id='" + branchID.ToString() + "' " +
                                     " inner join branch_master a on a.branch_id=d.branch_id" +
                                     " inner join Comp_Master b on b.branch_id=d.branch_id" +
            " Where q.Section_ID = 1 AND q.Status_ID = 1 AND b.Level_ID=3 AND b.Action_ID=1 ORDER BY q.Question_No").Tables(0)

            If ViewType = 1 Then
                    Export_Excel_Click()
                End If

                Dim createddate As String = ""

                For Each DR In DT.Rows
                    i += 1

                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"
                    TR3_06.BorderWidth = "1"




                TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver
                    TR3_06.BorderColor = Drawing.Color.Silver



                TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid
                    TR3_06.BorderStyle = BorderStyle.Solid

                Dim status As String = ""


                If (DR(2).ToString() = 0) Then

                    status = "N"
                Else
                    status = "Y"


                End If

                Dim compstatus As String = ""


                If (DR(3).ToString() = 1) Then

                    compstatus = "Complied"
                ElseIf (DR(3).ToString() = 2) Then
                    compstatus = "NonComplied"
                Else
                    compstatus = "NA"

                End If


                RH.AddColumn(TR3, TR3_00, 4, 4, "c", i)
                RH.AddColumn(TR3, TR3_01, 30, 30, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 4, 4, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 4, 4, "l", status)
                RH.AddColumn(TR3, TR3_04, 8, 8, "l", compstatus)
                RH.AddColumn(TR3, TR3_05, 14, 14, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_06, 8, 8, "l", DR(5).ToString())


                tb.Controls.Add(TR3)

                Next
            RH.BlankRow(tb, 20)
            ' Pnlcomp.Controls.Add(tb)
            pnDisplay.Controls.Add(tb)
            tbdata.Controls.Add(tb)
        Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub

        Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

        End Sub

        Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

        End Sub

        Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
            Try
                Response.ContentType = "application/pdf"
                Response.AddHeader("content-disposition", "attachment;filename=ACReasonsReport.pdf")
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            Pnlcomp.RenderControl(hw)
            pnDisplay.RenderControl(hw)
            tbdata.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
                Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
                Dim htmlparser As New HTMLWorker(pdfDoc)
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
                pdfDoc.Open()
                htmlparser.Parse(sr)
                pdfDoc.Close()
                Response.Write(pdfDoc)
                Response.[End]()
            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub
        Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
            Try
                Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Detailed QCC Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
            Catch ex As Exception
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End Try
        End Sub
        Protected Sub Export_Excel_Click()
            Try
                Dim HeaderText As String
            HeaderText = "Detailed QCC Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
            Catch ex As Exception
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End Try
        End Sub
    End Class






