﻿
Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Compliance_Reports_View_ConsolidatedQCCReport
    Inherits System.Web.UI.Page

    Dim DB As New MS_SQL.Connect
        Dim DT As New DataTable
        Dim DTEXCEL As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim DT5 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
        Dim tb As New Table
        Dim PostID As Integer
        Dim GN As New GeneralFunctions
        Dim WebTools As New WebApp.Tools
        Dim TraDt As Date

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
            Dim DT As New DataTable
            Dim DT1 As New DataTable
            Dim ViewType As Integer
                ViewType = CInt(Request.QueryString.Get("ViewType"))
            Dim Freq = Request.QueryString.Get("Quarter_ID")
            Dim strwhere As String = ""
            Dim NA As String = ""
            Dim Comp As String = ""
            Dim NonComp As String = ""
            Dim critic As String = ""
            Dim branchID = Request.QueryString.Get("Branch_Code")

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
                cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RH.Heading(Session("FirmName"), tb, "Consolidated QCC Report", 100)
            tb.Attributes.Add("width", "100%")

                Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.WhiteSmoke

                Dim RowBG As Integer = 0
                Dim DR As DataRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09 As New TableCell

            TRHead_00.BorderWidth = "1"
                TRHead_01.BorderWidth = "1"
                TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
                TRHead_01.BorderColor = Drawing.Color.Silver
                TRHead_02.BorderColor = Drawing.Color.Silver
                TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver


            TRHead_00.BorderStyle = BorderStyle.Solid
                TRHead_01.BorderStyle = BorderStyle.Solid
                TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid

            TRHead_00.Font.Bold = True
                TRHead_01.Font.Bold = True
                TRHead_02.Font.Bold = True
            TRHead_03.Font.Bold = True
            TRHead_04.Font.Bold = True
            TRHead_05.Font.Bold = True
            TRHead_06.Font.Bold = True
            TRHead_07.Font.Bold = True
            TRHead_08.Font.Bold = True
            TRHead_09.Font.Bold = True



            RH.AddColumn(TRHead, TRHead_00, 4, 4, "c", "Sl.No")
            RH.AddColumn(TRHead, TRHead_01, 14, 14, "c", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 14, 14, "c", "Cluster")
            RH.AddColumn(TRHead, TRHead_03, 14, 14, "c", "District")
            RH.AddColumn(TRHead, TRHead_04, 14, 14, "c", "State")
            RH.AddColumn(TRHead, TRHead_05, 8, 8, "c", "Date of Submission")
            RH.AddColumn(TRHead, TRHead_06, 8, 8, "c", "No of NA Cases")
            RH.AddColumn(TRHead, TRHead_07, 8, 8, "c", "No of Compliance")
            RH.AddColumn(TRHead, TRHead_08, 8, 8, "c", "No of Non Compliance")
            RH.AddColumn(TRHead, TRHead_09, 8, 8, "c", "No of Critical Non Compliance")




            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)

            Dim i As Integer
            If branchID <> -1 Then
                NA = "(select count(Entry_id) from Comp_Question_Dtl where Comp_Status=3 and Branch_id=" + branchID + ")as [No of NA Cases],"
                Comp = "(select count(Entry_id) from Comp_Question_Dtl where Comp_Status=1 and Branch_id=" + branchID + ")as  [No of Compliance],"
                NonComp = "(select count(Entry_id) from Comp_Question_Dtl where Comp_Status=2 and Branch_id=" + branchID + ")as [No of Non Compliance],"
                critic = "(select count(Entry_id) from Comp_Question_Dtl n inner join Comp_Question_Master m on m.Question_id=n.Question_id  where n.Comp_Status=2 and m.Critical_Flag=1 and Branch_id=" + branchID + ")as  [No of Critical Non-Compliances]"

                strwhere = " where e.Freq_id = " + Freq
                strwhere += " and e.Branch_Id = " + branchID + "and"
                DT = DB.ExecuteDataSet("select distinct a.branch_name,b.cluster_name,c.district_name,d.state_name,f.Done_Date," + NA + Comp + NonComp + critic + " from branch_master a left join cluster_master b on a.cluster_id=b.cluster_id left join district_master c on c.district_id=a.district_id left join state_master d on d.state_id=a.state_id inner join Comp_Master e on e.branch_id=a.branch_id inner join Comp_Cycle f on f.Comp_id=e.Comp_id " + strwhere + " f.Level_id=3 And f.Action_id=1").Tables(0)

                DTEXCEL = DB.ExecuteDataSet("select distinct a.branch_name As [Branch],b.cluster_name As [Cluster],c.district_name As [District],d.state_name As [State],f.Done_Date As [Date Of Submission]," + NA + Comp + NonComp + critic + "  From branch_master a left join cluster_master b on a.cluster_id=b.cluster_id left join district_master c on c.district_id=a.district_id left join state_master d on d.state_id=a.state_id inner join Comp_Master e on e.branch_id=a.branch_id inner join Comp_Cycle f on f.Comp_id=e.Comp_id " + strwhere + " f.Level_id=3 And f.Action_id=1").Tables(0)

                For Each DR In DT.Rows
                    i += 1
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid

                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"
                    TR3_06.BorderWidth = "1"
                    TR3_07.BorderWidth = "1"
                    TR3_08.BorderWidth = "1"
                    TR3_09.BorderWidth = "1"



                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver
                    TR3_06.BorderColor = Drawing.Color.Silver
                    TR3_07.BorderColor = Drawing.Color.Silver
                    TR3_08.BorderColor = Drawing.Color.Silver
                    TR3_09.BorderColor = Drawing.Color.Silver


                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid
                    TR3_06.BorderStyle = BorderStyle.Solid
                    TR3_07.BorderStyle = BorderStyle.Solid
                    TR3_08.BorderStyle = BorderStyle.Solid
                    TR3_09.BorderStyle = BorderStyle.Solid

                    Dim createddate As String = ""

                    RH.AddColumn(TR3, TR3_00, 4, 4, "c", i)
                    RH.AddColumn(TR3, TR3_01, 14, 14, "l", DR(0).ToString())
                    RH.AddColumn(TR3, TR3_02, 14, 14, "l", DR(1).ToString())
                    RH.AddColumn(TR3, TR3_03, 14, 14, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_04, 14, 14, "l", DR(3).ToString())
                    RH.AddColumn(TR3, TR3_05, 8, 8, "l", DR(4).ToString())
                    RH.AddColumn(TR3, TR3_06, 8, 8, "l", DR(5).ToString())
                    RH.AddColumn(TR3, TR3_07, 8, 8, "l", DR(6).ToString())
                    RH.AddColumn(TR3, TR3_08, 8, 8, "l", DR(7).ToString())
                    RH.AddColumn(TR3, TR3_09, 8, 8, "l", DR(8).ToString())

                    tb.Controls.Add(TR3)
                Next
            Else

                DT = DB.ExecuteDataSet("select distinct a.branch_id,a.branch_name,b.cluster_name,c.district_name,d.state_name,f.Done_Date from branch_master a left join cluster_master b on a.cluster_id=b.cluster_id left join district_master c on c.district_id=a.district_id left join state_master d on d.state_id=a.state_id inner join Comp_Master e on e.branch_id=a.branch_id inner join Comp_Cycle f on f.Comp_id=e.Comp_id where f.Level_id=3 And f.Action_id=1").Tables(0)

                Dim createddate As String = ""

                For Each DR In DT.Rows
                    DT1 = DB.ExecuteDataSet("select count(Entry_id) from Comp_Question_Dtl where Comp_Status=3 and Branch_id=" + DR(0).ToString()).Tables(0)
                    DT2 = DB.ExecuteDataSet("Select count(Entry_id) from Comp_Question_Dtl where Comp_Status=1 And Branch_id=" + DR(0).ToString()).Tables(0)
                    DT3 = DB.ExecuteDataSet("select count(Entry_id) from Comp_Question_Dtl where Comp_Status=2 and Branch_id=" + DR(0).ToString()).Tables(0)
                    DT4 = DB.ExecuteDataSet("Select count(Entry_id) from Comp_Question_Dtl n inner join Comp_Question_Master m On m.Question_id=n.Question_id  where n.Comp_Status=2 And m.Critical_Flag=1 And Branch_id=" + DR(0).ToString()).Tables(0)

                    i += 1
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid

                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"
                    TR3_06.BorderWidth = "1"
                    TR3_07.BorderWidth = "1"
                    TR3_08.BorderWidth = "1"
                    TR3_09.BorderWidth = "1"



                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver
                    TR3_06.BorderColor = Drawing.Color.Silver
                    TR3_07.BorderColor = Drawing.Color.Silver
                    TR3_08.BorderColor = Drawing.Color.Silver
                    TR3_09.BorderColor = Drawing.Color.Silver


                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid
                    TR3_06.BorderStyle = BorderStyle.Solid
                    TR3_07.BorderStyle = BorderStyle.Solid
                    TR3_08.BorderStyle = BorderStyle.Solid
                    TR3_09.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TR3, TR3_00, 4, 4, "c", i)
                    RH.AddColumn(TR3, TR3_01, 14, 14, "l", DR(1).ToString())
                    RH.AddColumn(TR3, TR3_02, 14, 14, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_03, 14, 14, "l", DR(3).ToString())
                    RH.AddColumn(TR3, TR3_04, 14, 14, "l", DR(4).ToString())
                    RH.AddColumn(TR3, TR3_05, 8, 8, "l", DR(5).ToString())
                    RH.AddColumn(TR3, TR3_06, 8, 8, "l", DT1.Rows(0)(0).ToString())
                    RH.AddColumn(TR3, TR3_07, 8, 8, "l", DT2.Rows(0)(0).ToString())
                    RH.AddColumn(TR3, TR3_08, 8, 8, "l", DT3.Rows(0)(0).ToString())
                    RH.AddColumn(TR3, TR3_09, 8, 8, "l", DT4.Rows(0)(0).ToString())


                    DT5 = DB.ExecuteDataSet("select distinct a.branch_name As [Branch],b.cluster_name As [Cluster],c.district_name As [District],d.state_name As [State],f.Done_Date As [Date Of Submission]," + DT1.Rows(0)(0).ToString() + " As [No of NA Cases]," + DT2.Rows(0)(0).ToString() + " As [No of Compliance]," + DT3.Rows(0)(0).ToString() + " As [No of Non Compliance]," + DT4.Rows(0)(0).ToString() + " As [No of Critical Non-Compliances] " + " From branch_master a left join cluster_master b on a.cluster_id=b.cluster_id left join district_master c on c.district_id=a.district_id left join state_master d on d.state_id=a.state_id inner join Comp_Master e on e.branch_id=a.branch_id inner join Comp_Cycle f on f.Comp_id=e.Comp_id where f.Level_id=3 And f.Action_id=1").Tables(0)
                    DTEXCEL = DT5.Copy

                    tb.Controls.Add(TR3)


                Next
                End If

            If ViewType = 1 Then
                Export_Excel_Click()
            End If
            RH.BlankRow(tb, 20)
                pnDisplay.Controls.Add(tb)
            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub

        Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

        End Sub

        Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

        End Sub

        Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
            Try
                Response.ContentType = "application/pdf"
                Response.AddHeader("content-disposition", "attachment;filename=ACReasonsReport.pdf")
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Dim sw As New StringWriter()
                Dim hw As New HtmlTextWriter(sw)
                pnDisplay.RenderControl(hw)

                Dim sr As New StringReader(sw.ToString())
                Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
                Dim htmlparser As New HTMLWorker(pdfDoc)
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
                pdfDoc.Open()
                htmlparser.Parse(sr)
                pdfDoc.Close()
                Response.Write(pdfDoc)
                Response.[End]()
            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub
        Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
            Try
                Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Consolidated QCC Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
            Catch ex As Exception
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End Try
        End Sub
        Protected Sub Export_Excel_Click()
            Try
                Dim HeaderText As String
            HeaderText = "Consolidated QCC Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
            Catch ex As Exception
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End Try
        End Sub
    End Class




