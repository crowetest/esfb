﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="NonCompliance_QCC_Report.aspx.vb" Inherits="Compliance_Reports_NonCompliance_QCC_Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

<br />
<table align="center" style="width: 40%; text-align:center; margin:0px auto;">
    <tr>
        <td style="width:15%;">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
        </td>
        <td  style="width:85%;">
        </td>
    </tr>      
               
    <tr>           
        <td style="width:15%; text-align:left;">
            Branch
        </td>
        <td style="width:85%">
              <asp:TextBox ID="txtBranchName" class="ReadOnlyTextBox" runat="server" Width="87%" ReadOnly="true"></asp:TextBox>
            <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" Width="86%" ForeColor="Black" Visible="false">
                
            </asp:DropDownList>
        </td> 
    </tr>   
      <tr>           
        <td style="width:15%; text-align:left;">
            Quarter
        </td>
        <td style="width:85%">
            <asp:DropDownList ID="cmbQuarter" class="NormalText" runat="server" Font-Names="Cambria" Width="86%" ForeColor="Black">
                
            </asp:DropDownList>
        </td> 
    </tr>       
    <tr>
        <td style="width:15%; text-align:left;"></td>
        <td style="width:85%">
            <br />           
            <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 20%" type="button" value="VIEW"   onclick="return btnView_onclick()" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input id="cmd_Export_Excel" style="font-family: Cambria; cursor: pointer; width: 22%;" type="button" value="VIEW EXCEL" onclick="return btnExcelView_onclick()" />
            &nbsp;&nbsp;
            <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 20%" type="button" value="EXIT"  onclick="return btnExit_onclick()" />
        </td>
        <td style="width:100%; height: 18px; text-align:center;"></td>
    </tr>       
    <tr>
        <td style="width:15%;">&nbsp;</td>
        <td  style="width:85%;">
            <asp:HiddenField ID="hdnReportID" runat="server" />
             <asp:HiddenField ID="hidEntryData" runat="server" />
                         
          
        </td>
    </tr>
    <tr>
        <td style="width:15%;">&nbsp;</td>
        <td  style="width:85%;">               
            <asp:HiddenField ID="hdnValue" runat="server" />
        </td>               
    </tr>
</table>
<script language="javascript" type="text/javascript">
    function FromServer(arg, context) {

         if (context == 1) {
            
         
             ComboFill(arg,"<%= cmbQuarter.ClientID %>");
                   
                }
                
    }
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 0; a < rows.length-1; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function btnView_onclick() 
    {
        var Branch_Code = "";
        
        if (document.getElementById("<%= txtBranchName.ClientID %>").value!="") {
            Branch_Code = document.getElementById("<%= hdnValue.ClientID %>").value;
           
        }
        else {
            Branch_Code = document.getElementById("<%= cmbBranch.ClientID %>").value;
        }
        var Quarter_ID = document.getElementById("<%= cmbQuarter.ClientID %>").value;
        if(Quarter_ID == -1)
        {
            alert("Select Quarter");
        }
        else
        {
            window.open("View_NonCompliance_QCC_Report.aspx?ViewType=-1 &Branch_Code=" + Branch_Code + "&Quarter_ID=" + Quarter_ID +" ", "_self");  
        }
    }
    function BranchOnChange() {
     
       
                var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
                var ToData = "1Ø" + BranchID;
                ToServer(ToData, 1);
            }
    function btnExcelView_onclick() 
    {
         var Branch_Code = "";
        
        if (document.getElementById("<%= txtBranchName.ClientID %>").value!="") {
            Branch_Code = document.getElementById("<%= hdnValue.ClientID %>").value;
           
        }
        else {
            Branch_Code = document.getElementById("<%= cmbBranch.ClientID %>").value;
        }
        var Quarter_ID = document.getElementById("<%= cmbQuarter.ClientID %>").value;
        if(Quarter_ID == -1)
        {
            alert("Select Quarter");
        }
        else
        {
            window.open("View_NonCompliance_QCC_Report.aspx?ViewType=1 &Branch_Code=" + Branch_Code + "&Quarter_ID=" + Quarter_ID + " ", "_self");
        }
    }    
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }  
</script>
</asp:Content>

