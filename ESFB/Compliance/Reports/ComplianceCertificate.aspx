﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="ComplianceCertificate.aspx.vb" Inherits="Compliance_Reports_ComplianceCertificate" %>


<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/ExportMenu.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
*                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
*                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
        .auto-style2 {
            width: 414px;
        }
        .auto-style5 {
            width: 10249px;
        }
        .auto-style6 {
            width: 376px;
        }
    </style>
</head>
<body>
 <script language="javascript" type="text/javascript">
     function PrintPanel() {
         var panel = document.getElementById("<%=pnDisplay.ClientID %>");
         var printWindow = window.open('', '', 'height=400,width=800,location=0,status=0');
         printWindow.document.write('<html><head><title>ESAF</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () {
             printWindow.print();
         }, 500);
         return false;
     }
     function Exitform() {
      window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
         return false;
     }
     </script>
    <form id="form1" runat="server">    
    <div style="width:80%; background-color:white; min-height:750px; margin:0px auto;">
        <div style="text-align:right ;margin:0px auto; width:95%; background-color:white; ">          
            <asp:ImageButton ID="cmd_Back" style="text-align:left ;float:left; padding-top:12px;" runat="server" Height="35px" Width="35px" ImageAlign="AbsMiddle" ImageUrl="~/Image/back.png" ToolTip="Back"/>
            <asp:ImageButton ID="cmd_Print" style="text-align:right ; padding-top:8px;" runat="server" Height="40px" Width="40px" ImageAlign="AbsMiddle" ImageUrl="~/Image/print.png" ToolTip="Click to Print"/>
            <div id="MenuContainer" style="height:40px; width:40px; float:right;" ><ul class="menu">			               
			                <li>
                                <a href="#"><span><asp:ImageButton ID="ImageButton1" style="text-align:right ;" runat="server" Height="30px" Width="30px" ImageAlign="AbsMiddle" ImageUrl="~/Image/Export.png" ToolTip="Export"/></span></a>
				                <ul class="menu-hover">			
                                    <li><a href="#"><asp:ImageButton ID="cmd_Export" style="text-align:right ;" runat="server" Height="30px" Width="30px" ImageAlign="AbsMiddle" ImageUrl="~/Image/PDFExport.png" ToolTip="To PDF"/></a></li>		               
					                <li><a href="#"><asp:ImageButton ID="cmd_Export_Excel" style="text-align:right ;" runat="server" Height="30px" Width="30px" ImageAlign="AbsMiddle" ImageUrl="~/Image/ExcelExport.png" ToolTip="To Excel"/></a></li>					              
                                </ul>
			                </li>			            
		                </ul></div>
        </div>
        <br style="background-color:white"/>
        <div style="text-align:left;margin:0px auto; width:95%; background-color:white; font-family:Cambria">   
            <asp:Panel ID="pnDisplay" runat="server" Width="100%">
               
          
               
                <table>
                    <tr>
                        <td class="auto-style2" style="text-align:left">
                             <asp:Label ID="Label1" runat="server" Text="Br.ID"></asp:Label>
  
                        &nbsp;
                           &nbsp;
                             <asp:Label ID="Label19" runat="server" Text=""></asp:Label>
                        </td>
                      </tr>
                   <%-- </tr>
                    <tr>
                          <td class="auto-style2">
                            
                            </td>
                    </tr>
                    <tr>
                        <td >

                        </td>
                    </tr>--%>
                    <tr>
                        <td style="text-align:left">
                             <asp:Label ID="Label3" Width="200px" runat="server" Text="ESAF SMALL FINANCE BANK"></asp:Label>
                           
                        </td>
                      <td style="text-align:right">
                            <asp:Label ID="Label4" runat="server" Width="200px" Text="Branch:"></asp:Label>
                           
                              <asp:Label ID="Label7" runat="server" Width="130px"></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td style="text-align:left">
                             <asp:Label ID="Label5" runat="server"  Width="200px" Text="H.O: Mannuthy, Thrissur"></asp:Label>
                           
                        </td>
      
                        <td style="text-align:right" >
                            <asp:Label ID="Label6" runat="server" Width="200px" Text="Date:"></asp:Label>
                          
                              <asp:Label ID="Label8" runat="server" Width="148px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                       <%-- <td  style="border-style: dashed; border-color: inherit; border-width: medium;" class="auto-style2">
                            
                        </td>--%>
                         <td  style="border-style: solid; border-color: inherit; border-width: medium;" class="auto-style5">
                            
                      
                            
                        </td>
                         <td  style="border-style: solid; border-color: inherit; border-width: medium;" class="auto-style6">
                            
                        </td>
                      
                    </tr>
                    <tr>
                        
                        <td style="text-align:center;" colspan="3" class="auto-style4">
                             <asp:Label ID="Label9" runat="server" Text="Compliance Certificate" 
                                 ></asp:Label>
                        </td>
                      
                    
                        
                    </tr>
                    <tr>
                        
                      
                        <td style="text-align:center;" colspan="3" class="auto-style5">
                            <asp:Label ID="Label15" runat="server" Text="(for the quarter ended:" 
                                 ></asp:Label>
                              
                             <asp:Label ID="Label10" runat="server" Text="" 
                                 ></asp:Label>
                        </td>
                       
                            
                     
                    </tr>
                    <tr>
                        <td></td><td class="auto-style6"></td>
                    </tr>
                    <tr>
                       
                        <td class="auto-style5" colspan="3" style="text-align:left"><p style="width: 887px">
                            This is to certify that <asp:Label ID="Label11" runat="server" Text="" 
                                 ></asp:Label> branch has complied with all the compliance regulations as on
                                  <asp:Label ID="Label12" runat="server" Text="" 
                                 ></asp:Label>(except items specifically mentioned below). </p></td>
                        </tr>
                    <tr>
                       
                                  <td class="auto-style5" colspan="3" style="text-align:left"> 
Following are the regulations/compliance points which are not complied with </td>

            
                      
                    </tr>
                      </table>
                  <table>
                  
                    <tr>
                       
                         
                        <td style="border:solid" colspan="3" >
                            <asp:Label ID="Label13" runat="server" Text="NotCompliedCheckListNo" 
                                 ></asp:Label>
                        </td>
                        <td class="auto-style4"></td>
                            <td style="border:solid" class="auto-style4">
                            <asp:Label ID="Label14" runat="server" Text="" 
                                 ></asp:Label>
                        </td>
                       
                    </tr>
               <tr>  <td class="auto-style2" style="text-align:left;">
                          BOM :
                            &nbsp;
                             &nbsp;
                             &nbsp;
                             &nbsp;
                         </td>
                   <td style="text-align:left;">
                          <asp:Label ID="Label2" runat="server" Text="" 
                                 ></asp:Label>
                            </td></tr>
                       <tr>  <td class="auto-style2" style="text-align:left;">
                           CHECKER : &nbsp; &nbsp; &nbsp;
                             &nbsp;
                            </td>
                           <td style="text-align:left;" >
                           <asp:Label ID="Label17" runat="server" Text="" 
                                 ></asp:Label>
                             </td></tr>
                        <%--<tr>  <td class="auto-style2" style="text-align:left;">
                           HO : &nbsp; &nbsp; &nbsp;
                             &nbsp;
                            </td>
                            <td style="text-align:left;">
                           <asp:Label ID="Label16" runat="server" Text="" 
                                 ></asp:Label>
                             </td></tr>--%>
               </table>
            </asp:Panel>
           
        </div>
    </div>
    </form>
</body>
</html>
</asp:Content>


