﻿
Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Compliance_Reports_ComplianceCertificate
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DTEXCEL As New DataTable
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date
    Dim QS1 As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable



            Dim branchID = Request.QueryString.Get("BranchID")

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")



            Dim qno As String = ""

            DT = DB.ExecuteDataSet("select distinct a.branch_id,a.branch_name,f.Question_No,l.Quarter_name,l.Start_date from branch_master a inner join Comp_Master c on c.branch_id=a.branch_id  inner join Comp_Cycle d on d.Comp_id=c.Comp_id inner join Comp_Question_Dtl e on e.Cycle_ID=d.Cycle_ID inner join Comp_Question_Master f on f.Question_ID=e.Question_ID inner join Comp_Freq_Settings l on l.Freq_ID=c.Freq_ID where l.Status_id=1 and a.branch_id=" + branchID.ToString() + "").Tables(0)
            DT1 = DB.ExecuteDataSet("select distinct Question_ID from Comp_Question_Dtl where Comp_Status=2 and branch_id=" + branchID.ToString() + "").Tables(0)
            For n As Integer = 0 To DT1.Rows.Count - 1

                qno += DT1.Rows(n)(0).ToString() + ","
            Next

            Label19.Text = DT.Rows(0)(0).ToString()
            Label7.Text = DT.Rows(0)(1).ToString()
            Label10.Text += DT.Rows(0)(3).ToString() + ")"
            Label11.Text = DT.Rows(0)(1).ToString()
            Label12.Text = DT.Rows(0)(4).ToString()
            Label14.Text = qno.TrimEnd(",")
            Dim dat As DateTime = DateTime.Now

            Label8.Text = dat.Date


            DT2 = DB.ExecuteDataSet("select distinct a.Emp_Name from emp_master a inner join Comp_Cycle c on c.Done_By=a.Emp_code where a.branch_id=" + branchID.ToString() + " and c.Level_ID=2 and c.Action_ID=1").Tables(0)
            Label17.Text = DT2.Rows(0)(0).ToString()

            DT3 = DB.ExecuteDataSet("select distinct a.Emp_Name from emp_master a inner join Comp_Question_Dtl c on c.Created_By=a.Emp_code where a.branch_id=" + branchID.ToString() + " and c.Entry_ID= (select max(Entry_ID) from Comp_Question_Dtl where branch_id=" + branchID.ToString() + ") ").Tables(0)
            Label2.Text = DT3.Rows(0)(0).ToString()


        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Back_Click(sender As Object, e As ImageClickEventArgs) Handles cmd_Back.Click

    End Sub
    Protected Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click

    End Sub
    Protected Sub cmd_Print_Click(sender As Object, e As ImageClickEventArgs) Handles cmd_Print.Click

    End Sub
End Class
