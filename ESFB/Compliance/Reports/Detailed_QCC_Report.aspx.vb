﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class Compliance_Reports_Detailed_QCC_Report
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler

    Dim CallBackReturn As String = Nothing
        Dim DT As New DataTable
        Dim DT1 As New DataTable
        Dim GF As New GeneralFunctions
        Dim GMASTER As New Master
        Dim TraDt As Date
        Dim QS1 As String = ""
        Dim DB As New MS_SQL.Connect
        Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim strWhere As String = ""
            Try
            'If GF.FormAccess(CInt(Session("UserID")), 1427) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            If GF.FormAccess(CInt(Session("UserID")), 1428) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim branchid As Integer = CInt(Session("BranchID"))
            Me.hdnValue.Value = branchid
            Me.Master.subtitle = "Detailed QCC Report"
            If Not IsPostBack Then
                If (Session("BranchID") = 0) Then

                    cmbBranch.Visible = True
                    txtBranchName.Style.Add("display", "none")
                    DT = GF.GetQueryResult("SELECT -1 BRANCH_ID,'--Select--' BRANCH_NAME UNION ALL SELECT a.BRANCH_ID,b.BRANCH_NAME FROM Comp_Master a inner join BRANCH_MASTER b on a.branch_id=b.branch_id WHERE STATUS_ID = 1 and a.Level_ID=3 and a.Action_ID=1 ORDER BY 2")
                    GF.ComboFill(cmbBranch, DT, 0, 1)
                Else


                    txtBranchName.Text = Session("BranchName")
                    DT = GF.GetQueryResult("SELECT -1 FREQ_ID,'--Select--' Quarter UNION ALL SELECT distinct FREQ_ID,QUARTER_NAME FROM Comp_Freq_Settings where branch_id=" + branchid.ToString() + " ORDER BY 1")
                    GF.ComboFill(cmbQuarter, DT, 0, 1)
                End If
            End If
                cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub
        Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
            GC.Collect()
            DT.Dispose()
            DB.dispose()
        End Sub
#End Region
#Region "Events"
#End Region
#Region "Call Back"
        Public Function GetCallbackResult() As String Implements ICallbackEventHandler.GetCallbackResult
            Return CallBackReturn
        End Function
        Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements ICallbackEventHandler.RaiseCallbackEvent
            Dim Data() As String = eventArgument.Split(CChar("Ø"))
            If CInt(Data(0)) = 1 Then
                Dim Branch_ID1 As Integer = CInt(Data(1).ToString())
            DT = GF.GetQueryResult("SELECT -1 FREQ_ID,'--Select--' Quarter UNION ALL SELECT distinct FREQ_ID,QUARTER_NAME FROM Comp_Freq_Settings where branch_id=" + Branch_ID1.ToString() + " ORDER BY 2")

            If (DT.Rows.Count > 0) Then

                    For n As Integer = 0 To DT.Rows.Count - 1
                        QS1 += DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString() + "Ñ"
                    Next

                    CallBackReturn = QS1
                End If
            End If
        End Sub
#End Region
    End Class




