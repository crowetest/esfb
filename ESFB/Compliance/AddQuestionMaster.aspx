﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AddQuestionMaster.aspx.vb" Inherits="Compliance_AddQuestionMaster" %>


<%@ MasterType VirtualPath="~/ESFB.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
 <script src="../Script/Validations.js" type="text/javascript"></script>
    <table align="center" style="width: 80%; margin:0px auto;">
        <tr>
            <td style="text-align:left;" class="auto-style2">
                </td>
            <td style="text-align:left;" class="auto-style3">
                </td>
        </tr>
        <tr>
            <td style="width:30%;text-align:left;">
                &nbsp;</td>
            <td style="width:70%;text-align:left;">
                </td>
        </tr>
        <tr id="rowList">
            <td style="text-align:left;" colspan="2">
                <asp:Panel ID="pnDisplay" runat="server">
                    
                </asp:Panel>
            </td>
        </tr>
      
        <tr id="rowAdd" style="display:none;" >
             <td style="text-align:left;" colspan="2">
           <div style="width:100%; height:170px;  margin: 0px auto; " class="sub_first">
                        <table align="center" style="margin:0px auto;width:100%;">
                            <tr>
                                <td id="subHd" colspan="2"  style="width:100%;text-align:center; color:maroon;height:30px; font-weight:bold;" class="mainhead">Add New Compliance</td>
                            </tr>
                            <tr class="sub_second" style="height:28px;">
                                <td style="width:30%;">Q.No</td>
                                <td  style="width:70%;">
                                    <asp:TextBox ID="txtqno" runat="server" Width="36%" 
                    class="NormalText" MaxLength="100" ></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="sub_second" style="height:28px;">
                                <td style="width:30%;">Question</td>
                                <td  style="width:70%;">
                                    <asp:TextBox ID="txtquestion" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="80%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="108px"
                            MaxLength="5000" />
                                </td>
                            </tr>
                            <tr class="sub_second" style="height:28px;">
                                <td style="width:30%;">Critical</td>
                                <td  style="width:70%;">
                                 
                       <input id="rad1" value="1" type="radio" name="check" checked="checked"/>Yes
                       <input id="rad2" value="0" type="radio" name="check" />No


                                </td>
                            </tr>
                            <tr class="sub_second">
                                <td colspan="2" style="text-align:center;" class="auto-style1">
                                    <input id="btnSave" 
                    style="font-family: Cambria; font-size: 10pt; font-weight: normal; width: 20%;height:30px;" 
                    type="button" value="SAVE" 
                     onclick="return btnSave_onclick()" />
                                    &nbsp;&nbsp;<input id="btnExitAdd" 
                    style="font-family: Cambria; font-size: 10pt; font-weight: normal; width: 20%;height:30px;" 
                    type="button" value="EXIT"    onclick="return btnExitAdd_onclick()" /></td>
                            </tr>
                        </table>
                    </div>
                 </td>
        </tr>     
        <tr>
            <td style="text-align:left;" colspan="2">
                &nbsp;</td>
        </tr>
        <%--<tr id="rowExit">
            <td style="text-align:center;" colspan="2">
                <input id="btnExit" 
                    style="font-family: Cambria; font-size: 10pt; font-weight: normal; width: 10%; " 
                    type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
        </tr>--%>
          <tr>
            <td style="width:30%;text-align:left;">
                <asp:HiddenField ID="hdnData" runat="server" />
            </td>
            <td style="width:70%;text-align:left;">
                &nbsp;</td>
        </tr>
    </table>
    <style type="text/css">
          #Button
        {
            width:90%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
           color:#E0E0E0;
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
            color:#036;
        }        
    
      #loadingover {
      background: black;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }
     #loadingmsg {           
      z-index: 100;  
      position:fixed; 
      top:50px;
      left:50px;
      }
        
        .auto-style1 {
            height: 83px;
        }
        
        .auto-style2 {
            width: 30%;
            height: 18px;
        }
        .auto-style3 {
            width: 70%;
            height: 18px;
        }
        
    </style>
     <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var QuestionID = 0;
        function btnExit_onclick() {
            window.open("../home.aspx","_self");
        }
        window.onload = function () {
            
            //debugger;
            FillComplaints();
        }
        function displayChange(ID) {
            //debugger;
            document.getElementById("rowAdd").style.display = "none";
            document.getElementById("rowList").style.display = "none";
            //document.getElementById("rowExit").style.display = "none";
          
            if (ID == 1) {
                document.getElementById("rowList").style.display = "";
                document.getElementById("rowExit").style.display = "";
            }
            else if (ID == 2) {
                if (QuestionID == 0) {
                    document.getElementById("btnSave").value = "ADD";
                    document.getElementById("subHd").innerHTML = "Add New Compliance";
                    document.getElementById("<%= txtqno.ClientID %>").value = "";
                    document.getElementById("<%= txtquestion.ClientID %>").value = "";
                 
                }
                else {
                    document.getElementById("btnSave").value = "UPDATE";
                    document.getElementById("subHd").innerHTML = "Edit Compliance Points";
                    var rowval = document.getElementById("<%= hdnData.ClientID %>").value.split("Ñ");
                    for (n = 0; n <= rowval.length - 1; n++) {
                        colval = rowval[n].split("ÿ");
                        if (QuestionID == colval[0]) {
                            document.getElementById("<%= txtqno.ClientID %>").value = colval[1];
                            document.getElementById("<%= txtquestion.ClientID %>").value = colval[2];     
                            var flag = "";
                            var radiobtn = "";
                            if (colval[3] == 1) {
                                flag = 'Y';
                               
                                radiobtn = document.getElementById("rad1");
                                radiobtn.checked = true;
                            }
                            else {
                                flag = '';
                                radiobtn = document.getElementById("rad2");
                                radiobtn.checked = true;
                              
                            }
                            break;
                        }
                    }
                }
                document.getElementById("rowAdd").style.display = "";
            }
         
        }
        function AddComplaints() {
            //debugger;
            QuestionID = 0;
            displayChange(2);
        }
        function FillComplaints() {
            //debugger;
            var row_bg = 0;
            var tab = "";
            var cnt = 0;
            tab += "<div style='width:100%; height:auto; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria'; ' align='center'>";
            tab += "<tr style='height:30px;' >";
            tab += "<td style='width:5%;text-align:left ; color:#FFF' ><div id='Button' onClick='AddComplaints()' >New</div></td>";
            tab += "<td style='width:20%;text-align:center; color:Maroon' colspan=4><b>Compliance List</b></td>";
            tab += "<td style='width:3%;text-align:right; color:#FFF' colspan=2> <img id='imgClose' src='../Image/close.png' onclick='return btnExit_onclick()' style='height:25px;width:25px; float:right; padding-right:5px;'></td>";
           
            tab += "</tr>";
            tab += "<tr style='height:30px; background-color:#B21C32; ' >";
            tab += "<td style='width:10%;text-align:center ; color:#FFF' ><b>#</b></td>";
            tab += "<td style='width:10%;text-align:left; color:#FFF'><b>Compliance No</b></td>";
            tab += "<td style='width:40%;text-align:left; color:#FFF'><b>Compliance Points</b></td>";
            tab += "<td style='width:10%;text-align:left; color:#FFF'><b>Critical</b></td>";
            tab += "<td style='width:5%;text-align:left; color:#FFF'></td>";
            tab += "<td style='width:5%;text-align:left; color:#FFF'></td>";
            tab += "</tr>";
            tab += "</table></div>";
            tab += "<div style='width:100%; height:425px; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria'; ' align='center'>";
            if (document.getElementById("<%= hdnData.ClientID %>").value != "") {
                rowval = document.getElementById("<%= hdnData.ClientID %>").value.split("Ñ");
                for (n = 0; n <= rowval.length - 1; n++) {
                    colval = rowval[n].split("ÿ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first' style='height:28px;'  >";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second' style='height:28px;' >";
                    }
                    i = n + 1;
                    var flag = "";

                    if (colval[3] == 1) {
                        flag = 'Y';
                    }
                    else {
                        flag = '';
                    }
                    tab += "<td style='width:10%;text-align:center; '>" + i + "</td>";
                    tab += "<td style='width:10%;text-align:left;font-size:9pt;'>" + colval[1] + "</td>";
                    tab += "<td style='width:40%;text-align:left;font-size:9pt;'>" + colval[2] + "</td>";
                    tab += "<td style='width:10%;text-align:left;font-size:9pt;'>" + flag + "</td>";
                    //if (colval[4] == 0)
                    //    tab += "<td style='width:20%;text-align:center;font-size:9pt;'></td>";
                    //else
 
                    tab += "<td  style='width:5%;text-align:center' ><img src='../../image/edit1.png' title='Edit'  style='height:18px;width:18px;cursor:pointer'  onclick=EditOnClick(" + colval[0] + ") /></td>";
                    tab += "<td  style='width:5%;text-align:center' ><img src='../../image/delete.png' title='Delete'  style='height:18px;width:18px;cursor:pointer'  onclick=DeleteOnClick(" + colval[0] + ") /></td>";
                    tab += "</tr>";
                    cnt +=parseInt(colval[2]);
                }
            }
            tab += "</table></div>";            
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;

        }

        function RowOnClick(ID) {
            ToServer("4ʘ" + ID, 4);      
          //  window.open("ShowEmpDetails.aspx?DepartmentID=" + btoa(ID) + "", "_blank", "toolbar=0,top=" + getOffset(document.getElementById("<%= pnDisplay.ClientID %>")).top + "px,left=" + getOffset(document.getElementById("<%= pnDisplay.ClientID %>")).left + "px,width=900,height=400,,menubar=0,center=1,channelmode =1,scrollbars=2,status=0,titlebar=0");
        }
        function EditOnClick(ID) {
            QuestionID = ID;
            displayChange(2);
        }
        function DeleteOnClick(ID) {
          
                if(confirm("Are you sure to delete this Question?")==true)
                ToServer("1ʘ"+ID, 1);
          
                // alert("There are <span style='color:red; font-weight:bold;'> live employees </span>in this department.Hence <span style='color:red; font-weight:bold;'>Deletion is not possible.</span>"); return false;
               // alert("There are  live employees with this Designation.Hence Deletion is not possible."); return false;
            
        }
        function FromServer(Arg, Context) {
            //debugger;
            switch (Context) {
                case 1:
                    {
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        document.getElementById("<%= hdnData.ClientID %>").value = Data[2];
                        FillComplaints();
                        break;
                    }
                case 2:
                    {
                        var Dtl = Arg.split("ʘ");
                        alert(Dtl[1]);
                        if (Dtl[0] == 0) {
                            document.getElementById("<%= hdnData.ClientID %>").value = Dtl[2];
                            FillComplaints();
                            displayChange(1);
                        }
                        break;
                    }
          
            }
        }
        function btnExitAdd_onclick() {
            displayChange(1);
        }



        function btnSave_onclick() {
            //debugger;
            var Qno = document.getElementById("<%= txtqno.ClientID %>").value;
            if (Qno == "")
            { alert("Enter Qno"); document.getElementById("<%= txtqno.ClientID %>").focus(); return false; }           
            var Question = document.getElementById("<%= txtquestion.ClientID %>").value;
            if (Question == "")
            { alert("Enter Question"); document.getElementById("<%= txtquestion.ClientID %>").focus(); return false; }  
            var critical = "";
            
            radiobtn = document.getElementById("rad1");
            radiobtn1 = document.getElementById("rad2");
          
            if(radiobtn.checked==true)
            {
              
                critical = 1;
            }
            else {
                critical = 0;
            }
         
            ToServer("2ʘ" + QuestionID + "ʘ" + Qno + "ʘ" + Question + "ʘ" + critical, 2);
        }


    </script>
</asp:Content>





















