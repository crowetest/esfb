﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient

Partial Class Compliance_Comp_Approval
    Inherits System.Web.UI.Page

    Implements Web.UI.ICallbackEventHandler
        Dim CallBackReturn As String = Nothing
        Dim DB As New MS_SQL.Connect
        Dim DT As New DataTable
        Dim DT1 As New DataTable
        Dim DT2 As New DataTable
        Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim DT5 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Dim QS1 As String = ""
    Dim Levelid As Integer = 0
    Dim checker As Integer = 0
    Dim Actionid As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1400) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            If GF.FormAccess(CInt(Session("UserID")), 1405) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim UserID As String = CStr(Session("UserID"))
            Dim BranchID As Integer = CInt(Session("BranchID"))
            If (BranchID = 0) Then

                DT = DB.ExecuteDataSet("SELECT A.BRANCH_ID,B.BRANCH_NAME,B.BRANCH_HEAD,c.Quarter_Name,A.Level_id,A.Action_id FROM Comp_Master A INNER JOIN BRANCH_MASTER B ON A.BRANCH_ID = B.BRANCH_ID INNER JOIN Comp_Freq_Settings c on  A.FREQ_ID = c.Freq_ID where c.Status_id=1 and a.level_id=2 and a.action_id=1").Tables(0)

            Else

                DT = DB.ExecuteDataSet("SELECT A.BRANCH_ID,B.BRANCH_NAME,B.BRANCH_HEAD,c.Quarter_Name,A.Level_id,A.Action_id FROM Comp_Master A INNER JOIN BRANCH_MASTER B ON A.BRANCH_ID = B.BRANCH_ID INNER JOIN Comp_Freq_Settings c on  A.FREQ_ID = c.Freq_ID where c.Status_id=1 and A.BRANCH_ID= " + BranchID.ToString() + "").Tables(0)

            End If

            Me.hidEntryData.Value = DT.Rows(0)(0).ToString() + "Ñ" + DT.Rows(0)(1).ToString() + "Ñ" + DT.Rows(0)(2).ToString() + "Ñ" + UserID + "Ñ" + DT.Rows(0)(3).ToString()
            If Not IsPostBack Then
                DT3 = DB.ExecuteDataSet("SELECT -1,'--Select--' branch_name UNION ALL select b.BRANCH_ID,b.BRANCH_NAME FROM Comp_Master c inner join BRANCH_MASTER b on b.branch_id=c.branch_id where c.Level_ID=2 and c.Action_ID=1 ").Tables(0)
                GF.ComboFill(cmbbranch, DT3, 0, 1)
            End If
            cmbbranch.Attributes.Add("onchange", "BranchOnChange()")
            Levelid = CInt(DT.Rows(0)(4).ToString())
            Actionid = CInt(DT.Rows(0)(5).ToString())
            If (UserID = DT.Rows(0)(2).ToString()) Then

                checker = 1
                DT1 = GetComplaints(BranchID, Levelid, Actionid, checker)
                If (DT1.Rows.Count > 0) Then

                    For n As Integer = 0 To DT1.Rows.Count - 1
                        QS1 += DT1.Rows(n)(0).ToString() + "ÿ" + DT1.Rows(n)(1).ToString() + "ÿ" + DT1.Rows(n)(2).ToString() + "ÿ" + DT1.Rows(n)(3).ToString() + "ÿ" + DT1.Rows(n)(4).ToString() + "ÿ" + DT1.Rows(n)(5).ToString() + "ÿ" + DT1.Rows(n)(6).ToString() + "ÿ" + DT1.Rows(n)(7).ToString() + "Ñ"
                    Next

                    Me.hidQS1.Value = QS1

                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", " tablefill();", True)
                Else
                    Label1.Visible = False

                    txtRemarks.Visible = False

                End If
            Else
                DT5 = DB.ExecuteDataSet("select emp_code from Comp_Ho_Master where Status =1 and emp_code=" + UserID.ToString() + "").Tables(0)
                If (DT5.Rows.Count > 0) Then
                    'If (UserID = DT5.Rows(0)(0).ToString()) Then
                    checker = 2

                        cmbbranch.Visible = True
                        txtBranchName.Style.Add("display", "none")

                    'End If
                End If
            End If
            Me.Master.subtitle = "Quaterly Compliance Approve/Reject Form"

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

#Region "Function"
    Public Function GetComplaints(BranchID As Integer, Levelid As Integer, Actionid As Integer, checker As Integer) As DataTable
        If (checker = 1) Then

            If (Levelid = 2) And (Actionid = 1) OrElse (Levelid = 3) And (Actionid = 1) Then
                DT1 = Nothing

                Label1.Visible = False

                txtRemarks.Visible = False

            Else
                Return DB.ExecuteDataSet("SELECT distinct m.Question_ID,c.Question,c.Critical_Flag,m.Reason,m.Created_By,m.Created_Date,m.Comp_Status,s.Set_Name FROM Comp_Question_Master c inner join Comp_Question_Dtl m on m.Question_ID=c.Question_ID inner join emp_master a on a.emp_code=m.Created_By inner join Comp_Settings_Master s on s.set_ID = m.Comp_Status inner join Comp_Master t on t.Branch_ID=a.Branch_ID inner join Comp_Cycle k on k.Comp_id=t.Comp_id  Where t.BRANCH_ID = " + BranchID.ToString() + " AND c.Section_ID = 1 AND c.Status_ID = 1 AND s.set_Type=3 AND k.Finish_Status=1 ").Tables(0)
            End If

        Else
            If ((Levelid = 3) And (Actionid = 1)) Then
                DT1 = Nothing

                Label1.Visible = False

                txtRemarks.Visible = False

            Else
                Return DB.ExecuteDataSet("SELECT distinct m.Question_ID,c.Question,c.Critical_Flag,m.Reason,m.Created_By,m.Created_Date,m.Comp_Status,s.Set_Name FROM Comp_Question_Master c " +
" inner join Comp_Question_Dtl m on m.Question_ID=c.Question_ID inner join emp_master a on a.emp_code=m.Created_By inner join Comp_Settings_Master s on s.set_ID = m.Comp_Status inner join Comp_Master t" +
" on t.Branch_ID=a.Branch_ID inner join Comp_Cycle k on k.Comp_id=t.Comp_id  Where m.BRANCH_ID = " + BranchID.ToString() + " AND c.Section_ID = 1 AND c.Status_ID = 1 AND s.set_Type=3 AND k.Finish_Status=1 ").Tables(0)

            End If
        End If

    End Function
#End Region

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
            Return CallBackReturn
        End Function
        Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
            Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim Branch_ID As Integer = CInt(Data(1).ToString())
            Dim Remarks As String = Data(2).ToString()
            Dim Status_ID As Integer = CInt(Data(3).ToString())
            Dim Done_By As Integer = CInt(Session("UserID"))
            Dim Level_ID = CInt(Data(4).ToString())
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Try
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@Branch_ID", SqlDbType.Int)
                Params(0).Value = Branch_ID
                Params(1) = New SqlParameter("@Done_By", SqlDbType.Int)
                Params(1).Value = Done_By
                Params(2) = New SqlParameter("@Level_ID", SqlDbType.Int)
                Params(2).Value = Level_ID
                Params(3) = New SqlParameter("@Remarks", SqlDbType.VarChar, 1000)
                Params(3).Value = Remarks
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@Status_ID", SqlDbType.Int)
                Params(6).Value = Status_ID 'status_id 2 for approve,3 for reject
                DB.ExecuteNonQuery("SP_COMP_ENTRY_SAVE", Params)
                Message = CStr(Params(5).Value)
                ErrorFlag = CInt(Params(4).Value)
            Catch ex As Exception
                Message = Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString() + "Ø" + Level_ID.ToString() + "Ø" + Status_ID.ToString() + "Ø" + Branch_ID.ToString()
        End If
        If CInt(Data(0)) = 2 Then

            Dim Branch_ID1 As Integer = CInt(Data(1).ToString())
            checker = 2
            DT1 = GetComplaints(Branch_ID1, Levelid, Actionid, checker)
            If (DT1 IsNot Nothing) Then

                If (DT1.Rows.Count > 0) Then


                    For n As Integer = 0 To DT1.Rows.Count - 1
                        QS1 += DT1.Rows(n)(0).ToString() + "ÿ" + DT1.Rows(n)(1).ToString() + "ÿ" + DT1.Rows(n)(2).ToString() + "ÿ" + DT1.Rows(n)(3).ToString() + "ÿ" + DT1.Rows(n)(4).ToString() + "ÿ" + DT1.Rows(n)(5).ToString() + "ÿ" + DT1.Rows(n)(6).ToString() + "ÿ" + DT1.Rows(n)(7).ToString() + "Ñ"
                    Next

                    CallBackReturn = QS1

                End If
            Else
                Label1.Visible = False

                txtRemarks.Visible = False
            End If

        End If
            If CInt(Data(0)) = 3 Then

        End If
    End Sub
End Class