﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Comp_Approval.aspx.vb" Inherits="Compliance_Comp_Approval" %>
   <%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script src="../Script/jquery.min.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);          
        }        
        .Button:hover
        {           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);            
            color:#801424;
        }                   
     .bg
     {
         background-color:#FFF;
     }
     .style1
        {
            width: 40%;
            height: 104px;
        }           
    </style>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/jquery.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">       
        window.onload = function () {
            //debugger;
           <%-- var Count1 = document.getElementById("<%= cmbbranch.ClientID %>").value;

            if (Count1 >= 0) {
                document.getElementById("<%= cmbbranch.ClientID %>").style.display = "block";
            }
           --%>
            

        }
            function FromServer(arg, context) {
                //debugger;
                if (context == 1) {
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == 0 && Data[2]==3 && Data[3]==2){
                        window.open("Reports/ComplianceCertificate.aspx?BranchID=" + Data[4]+ "", "_self");
                    }
                    else if(Data[0]==0)
                    {
                        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");

                    }
                }
                if (context == 2) {
                     var entryData = document.getElementById("<%= hidEntryData.ClientID %>").value.split("Ñ");
                     var cmbval = document.getElementById("<%= cmbbranch.ClientID %>").value;
                    document.getElementById("<%= txtBranchCode.ClientID %>").value = cmbval;
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0!
                    var yyyy = today.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    today = dd + '/' + mm + '/' + yyyy;
                    document.getElementById("<%= txtDate.ClientID %>").value = today;
                    document.getElementById("<%= txtQuarterName.ClientID %>").value = entryData[4].toString();
                    var Data = arg.split("Ñ");
                    var Count1 = Data.length - 1;
                    if (Count1 > 0) {
                    document.getElementById("<%= pnlSec1.ClientID %>").style.display = '';
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div>";
                    tab += "<table style='width:100%'>";
                    tab += "<tr>";
                    tab += "<td>";
                    tab += "<div class='mainhead' style='width:75%; height:auto; padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
                    tab += "<table id = 'x' style='width:100%; margin:0px auto;font-family:cambria;line-height:20px;background-color:#EEB8A6;' align='center'>";
                    tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;height:35px;' align='left' class='sec2'>";
                    tab += "<td style='text-align:center;width:5%;font-weight:bold;' class='NormalText'>Sl No</td>";
                    tab += "<td style='text-align:center;width:40%;font-weight:bold;' class='NormalText'>Question</td>";
                    tab += "<td style='text-align:center;width:5%;font-weight:bold;' class='NormalText'>Critical</td>";
                    tab += "<td style='text-align:center;width:5%;font-weight:bold;' class='NormalText'>Complied / Non-complied / NA</td>";
                    tab += "<td style='text-align:center;width:60%;font-weight:bold;' class='NormalText'>Reason, if marked non-compliance/ & Not applicable</td>";
                    tab += "</tr>";
                    for (var i = 0; i < Count1; i++) {
                        var qData1 = Data[i].toString().split("ÿ");
                      
                        var flag = "";

                        if (qData1[2] == 1) {
                            flag = 'Y';
                        }
                        else {
                            flag = '';
                        }
                        tab += "<tr style='width:100%;font-family:cambria;height:10%;' class=sub_first align='left'>";
                        tab += "<td style='text-align:center;width:5%;text-align:center;' class='NormalText' id='slno_" + qData1[0] + "'>" + (i + 1) + "</td>";
                        tab += "<td style='text-align:center;width:40%;text-align:left;padding-left:2%;' class='NormalText' id='Question_" + qData1[0] + "'>" + qData1[1] + "</td>";
                        tab += "<td style='text-align:center;width:5%;text-align:left;padding-left:2%;' class='NormalText' id='Critical_" + qData1[0] + "'>" + flag + "</td>";
                        tab += "<td style='text-align:center;width:20%;text-align:left;padding-left:2%;' class='NormalText' id='CompStatus_" + qData1[0] + "'>" + qData1[7] + "</td>";
                        tab += "<td style='text-align:center;width:40%;text-align:left;padding-left:2%;' class='NormalText' id='Reason_" + qData1[0] + "'>" + qData1[3] + "</td>";
                        tab += "</td>";
                        tab += "</tr>"
                    }
                    tab += "</table>";
                    tab += "</div>";
                    tab += "</td>";
                    tab += "</tr>";
                    tab += "</table>";
                    tab += "</div>";
                    document.getElementById("<%= pnlSec1.ClientID %>").innerHTML = tab;
                }
                }
                if (context == 3) {

                }
            }
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ñ");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function ClearCombo(control) {
                document.getElementById(control).options.length = 0;
                var option1 = document.createElement("OPTION");
                option1.value = -1;
                option1.text = " -----Select-----";
                document.getElementById(control).add(option1);
            }
            function btnExit_onclick() {
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
            function white_space(field) {
                if (field.value.length == 1) {
                    field.value = (field.value).replace(' ', '');
                }
            }
            function DecimalCheck(e, el) {
                var unicode = e.charCode ? e.charCode : e.keyCode;
                if (unicode != 8) {
                    if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                        if (unicode == 37 || unicode == 38) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    else {
                        if (unicode == 46) {
                            var parts = el.value.split('.');
                            if (parts.length == 2) {
                                return false;
                            }
                            return true;
                        }
                    }
                }
                else {
                    return true;
                }
            }
            function tablefill() {
               
                var entryData = document.getElementById("<%= hidEntryData.ClientID %>").value.split("Ñ");

                document.getElementById("<%= txtBranchCode.ClientID %>").value = entryData[0].toString();
              
                  document.getElementById("<%= txtBranchName.ClientID %>").value = entryData[1].toString();
             var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0!
                    var yyyy = today.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    today = dd + '/' + mm + '/' + yyyy;
                    document.getElementById("<%= txtDate.ClientID %>").value = today;
               
            document.getElementById("<%= txtQuarterName.ClientID %>").value = entryData[4].toString();
                var QS1 = document.getElementById("<%= hidQS1.ClientID %>").value.split("Ñ");
               
                var Count1 = QS1.length - 1;
                if (Count1 > 0) {
                    document.getElementById("<%= pnlSec1.ClientID %>").style.display = '';
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div>";
                    tab += "<table style='width:100%'>";
                    tab += "<tr>";
                    tab += "<td>";
                    tab += "<div class='mainhead' style='width:75%; height:auto; padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
                    tab += "<table id = 'x' style='width:100%; margin:0px auto;font-family:cambria;line-height:20px;background-color:#EEB8A6;' align='center'>";
                    tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;height:35px;' align='left' class='sec2'>";
                    tab += "<td style='text-align:center;width:5%;font-weight:bold;' class='NormalText'>Sl No</td>";
                    tab += "<td style='text-align:center;width:40%;font-weight:bold;' class='NormalText'>Question</td>";
                    tab += "<td style='text-align:center;width:5%;font-weight:bold;' class='NormalText'>Critical</td>";
                    tab += "<td style='text-align:center;width:5%;font-weight:bold;' class='NormalText'>Complied / Non-complied / NA</td>";
                    tab += "<td style='text-align:center;width:60%;font-weight:bold;' class='NormalText'>Reason, if marked non-compliance/ & Not applicable</td>";
                    tab += "</tr>";
                    for (var i = 0; i < Count1; i++) {
                        var qData1 = QS1[i].toString().split("ÿ");
                       
                        var flag = "";

                        if (qData1[2] == 1) {
                            flag = 'Y';
                        }
                        else {
                            flag = '';
                        }
                        tab += "<tr style='width:100%;font-family:cambria;height:10%;' class=sub_first align='left'>";
                        tab += "<td style='text-align:center;width:5%;text-align:center;' class='NormalText' id='slno_" + qData1[0] + "'>" + (i + 1) + "</td>";
                        tab += "<td style='text-align:center;width:40%;text-align:left;padding-left:2%;' class='NormalText' id='Question_" + qData1[0] + "'>" + qData1[1] + "</td>";
                        tab += "<td style='text-align:center;width:5%;text-align:left;padding-left:2%;' class='NormalText' id='Critical_" + qData1[0] + "'>" + flag + "</td>";
                        tab += "<td style='text-align:center;width:20%;text-align:left;padding-left:2%;' class='NormalText' id='CompStatus_" + qData1[0] + "'>" + qData1[7] + "</td>";
                        tab += "<td style='text-align:center;width:40%;text-align:left;padding-left:2%;' class='NormalText' id='Reason_" + qData1[0] + "'>" + qData1[3] + "</td>";
                        tab += "</td>";
                        tab += "</tr>"
                    }
                    tab += "</table>";
                    tab += "</div>";
                    tab += "</td>";
                    tab += "</tr>";
                    tab += "</table>";
                    tab += "</div>";
                    document.getElementById("<%= pnlSec1.ClientID %>").innerHTML = tab;
                   <%--  document.getElementById("<%= txtRemarks.ClientID %>").value = qData1[8].toString();--%>
                }


            }

        function Rejectonclick() {
          
            var Level = "";
           if (document.getElementById("<%= txtBranchName.ClientID %>").style.display == 'none') {
                var name = document.getElementById("<%= cmbbranch.ClientID %>").value;
               
                if (name != "") {
                    Level = 3;
                }
               
           }
           else {
               Level = 2;
           }
                var branch_ID = document.getElementById("<%= txtBranchCode.ClientID %>").value
                
                if (document.getElementById("<%= txtremarks.ClientID %>").value == "") {
                    alert("Enter Remarks");
                    document.getElementById("<%= txtremarks.ClientID %>").focus();
                    return false;
                }

                var remarks = document.getElementById("<%= txtremarks.ClientID %>").value;
                var Status = 3;
                var Data = "1Ø" + branch_ID.toString() + "Ø" + remarks + "Ø" + Status + "Ø" + Level;
                ToServer(Data, 1);
            }



        function Approveonclick() {
            //debugger;
            var Level = "";
            if (document.getElementById("<%= txtBranchName.ClientID %>").style.display == 'none') {
                var name = document.getElementById("<%= cmbbranch.ClientID %>").value;
               
                if (name != "") {
                    Level = 3;
                }
               
            }
            else {
                Level = 2;
            }

           
                var branch_ID = document.getElementById("<%= txtBranchCode.ClientID %>").value;
                if (document.getElementById("<%= txtremarks.ClientID %>").value == "") {
                    alert("Enter Remarks");
                    document.getElementById("<%= txtremarks.ClientID %>").focus();
                    return false;
                }

                var remarks = document.getElementById("<%= txtremarks.ClientID %>").value;
                var Status = 2;
                var Data = "1Ø" + branch_ID.toString() + "Ø" + remarks + "Ø" + Status + "Ø" + Level;
                ToServer(Data, 1);
            }
            function BranchOnChange() {
     
       
                var BranchID = document.getElementById("<%= cmbbranch.ClientID %>").value;
                var ToData = "2Ø" + BranchID;
                ToServer(ToData, 2);
            }
        
    </script>
</head>
</html>
<br />
<div  style="width:90%; height:auto; margin:0px auto; background-color:#EEB8A6;">
    <br />
    <div id = "divSection1" class = "sec1" style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table class="style1" style="width:70%;margin: 0px auto;height:auto;line-height:35px;">
            <tr> 
                <td style="width:20%;">
                    <asp:HiddenField ID="hidEntryData" runat="server" />
                    <asp:HiddenField ID="hidBName" runat="server" />
                    <asp:HiddenField ID="hidQName" runat="server" />
                    <asp:HiddenField ID="hidBid" runat="server" />
                    <asp:HiddenField ID="hidQS1" runat="server" />
                    <asp:HiddenField ID="hidQS2" runat="server" />
                </td>
                <td style="width:30%; text-align:left;">
                </td>
                <td style="width:20%">
                </td>
                <td style="width:30%">
                </td>
            </tr>
            <tr> 
                <td>
                    Name of the RBO :
                </td>
                <td>
                    <asp:TextBox ID="txtBranchName" class="ReadOnlyTextBox" runat="server" Width="87%" ReadOnly="true"></asp:TextBox>
            <asp:DropDownList ID="cmbbranch" class="NormalText" runat="server" Font-Names="Cambria" Visible="false"
                    Width="89%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList>
              </td>
                <td>
                    Quarter :
                </td>
                <td>
                    <asp:TextBox ID="txtQuarterName" class="ReadOnlyTextBox" runat="server" Width="87%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr> 
                <td>
                    Branch Code :
                </td>
                <td>
                    <asp:TextBox ID="txtBranchCode" class="ReadOnlyTextBox" runat="server" Width="87%" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    Date :
                </td>
                <td>
                    <asp:TextBox ID="txtDate" class="ReadOnlyTextBox" runat="server" Width="87%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table ID="tblSec1" style="width:90%;height:90px;margin:0px auto;">            
            <tr id="Tr1">
                <td style="text-align: center;">
                    <asp:Panel ID="pnlSec1" Style="width: 100%;  text-align: left; float: left;" runat="server">               

                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />
        <br />
         
     
        <table class="style1" style="width:70%;margin: 0px auto;height:auto;line-height:35px;">
            
            
            <tr> 
                 <td style="width:15%; text-align:left;">
                </td>
                <td>

                </td>
                <td>
                  <asp:Label ID="Label1"  runat="server" Text="Remarks/Comments :"></asp:Label> 
                </td>
                <td>
                    <asp:TextBox ID="txtRemarks" TextMode="MultiLine"  onkeypress='return TextAreaCheck(event)' runat="server" Width="95%"></asp:TextBox>
                </td>
                <td>
                    
                </td>
            </tr>
        </table>
        <br />
        <br />  
    </div>
    <div style="width:80%;height:auto; padding-left:10px;margin:0px auto;">
        <div style="text-align:center; height: 63px;"><br />
             <input id="btnApprove" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="APPROVE" onclick="return Approveonclick()"/>       
             &nbsp;
            &nbsp;
            <input id="btnReject" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="REJECT" onclick="return Rejectonclick()"/>       
            &nbsp;
            &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
        </div>
    </div>
</div>   
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>



