﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Portal
    Inherits System.Web.UI.Page
    Dim DT, DTNew As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.hidPost.Value = Session("Post_ID")
            Me.hidUserID.Value = Session("UserID")
            Dim Msg As String
            Dim NewLine As String = "\n"
            DTNew = DB.ExecuteDataSet("select EmpName,InterDate,InterTime from interview_schedule where EmpCode=" & Session("UserID") & " and status_id = 1").Tables(0)
            If (DTNew.Rows.Count > 0) Then
                Dim ErrorFlag As Integer = 0
                Try
                    Dim UserID As Integer = CInt(Session("UserID"))
                    Dim Params(2) As SqlParameter
                    Params(0) = New SqlParameter("@Emp_Code", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(1).Direction = ParameterDirection.Output
                    Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(2).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_INTERVIEW_UPDATES", Params)
                    ErrorFlag = CInt(Params(1).Value)
                    Msg = CStr(Params(2).Value)
                Catch ex As Exception
                    ErrorFlag = 1
                    Msg = ex.Message.ToString
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try

                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_script1.Append(" alert('" + Msg.ToString() + "');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            End If

            'Get Employee Details

            DT = GF.GetEmployee(Session("UserID"))
            hid_dtls.Value = GF.GetEmployeeString(DT)
            hidStaffFlag.Value = CStr(DT.Rows(0)(9))



            DT = GF.GetPhoto(CInt(Session("UserID")))
            If DT.Rows.Count > 0 Then
                Dim bytes As Byte() = DirectCast(DT.Rows(0)(0), Byte())
                Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                hid_image.Value = Convert.ToString("data:" + DT.Rows(0)(1) + ";base64,") & base64String
            Else
                hid_image.Value = "Image/userimage.png"
            End If
            '   DT = DB.ExecuteDataSet("Select count(*) from Election_Online_Voters Where Emp_code=" + Session("UserID").ToString() + " and status_id=1 and booth_no in(select booth_no from election_booth_master where status_id=1)").Tables(0)
            hidPoll.Value = "0"
          
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString() + " &Flg=1")
            End If

        End Try
    End Sub
    Protected Sub cmd_Update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Update.Click
        Try
            Response.Redirect("General/Birthday_Entries.aspx", False)
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString() + " &Flg=1")
        End Try
    End Sub

    Protected Sub btnBrAddress_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBrAddress.Click
        Try
            Response.Redirect("General/Correct_Branch_Address.aspx", False)
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString() + " &Flg=1")
        End Try
    End Sub
End Class
