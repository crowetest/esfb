﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient

Partial Class Budget_Budget_RequesteeQueue
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1446) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            'If GF.FormAccess(CInt(Session("UserID")), 1441) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Me.Master.subtitle = "Requestee Queue"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim EmpCode = CInt(Session("UserID"))
            Dim branchid As String = CStr(Session("BranchID"))
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
            If Not IsPostBack Then
                DT1 = DB.ExecuteDataSet("select branch_type,Branch_Category from ESFB.dbo.branch_master where status_id=1 and branch_id=" + branchid.ToString()).Tables(0)
                DT = DB.ExecuteDataSet("select distinct B.Req_id,c.GL_Name,c.Carry_frwd,D.Budg_Amt,B.Req_Amt,A.Done_Date,A.Level_ID,A.Action_ID,A.Remarks,c.GL_id " +
                 " from Budg_Req_Master B inner join Budg_GL_Master c on C.GL_id = B.GL_id inner join Budget_Master D on D.GL_id=B.GL_id " +
                 " inner join Budg_Req_Cycle A on A.Req_id=B.Req_id where  D.Status_id=1 and B.Branch_id=" + branchid.ToString() + " And " +
                 " D.Br_Category='" + DT1.Rows(0)(1).ToString() + "' and  A.Cycle_id in (select max(Cycle_id) from Budg_Req_Cycle group by Req_id) and D.Branch_Type=" + DT1.Rows(0)(0).ToString() + " ").Tables(0)
                Dim i As Integer = 0
                For Each dtr In DT.Rows
                    Me.hidQueueData.Value += DT.Rows(i)(0).ToString() + "ÿ" + DT.Rows(i)(1).ToString() + "ÿ" +
                        DT.Rows(i)(2).ToString() + "ÿ" + DT.Rows(i)(3).ToString() + "ÿ" + DT.Rows(i)(4).ToString() + "ÿ" + DT.Rows(i)(5).ToString() +
                    "ÿ" + DT.Rows(i)(6).ToString() + "ÿ" + DT.Rows(i)(7).ToString() + "ÿ" + DT.Rows(i)(8).ToString() + "ÿ" + DT.Rows(i)(9).ToString() + "ÿ" +
                    branchid.ToString() + "Ñ"
                    i += 1
                Next
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EmpID As Integer = CInt(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then

        ElseIf CInt(Data(0)) = 2 Then

        ElseIf CInt(Data(0)) = 3 Then

        ElseIf CInt(Data(0)) = 4 Then

        End If
    End Sub
#End Region




End Class
