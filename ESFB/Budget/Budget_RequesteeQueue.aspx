﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Budget_RequesteeQueue.aspx.vb" Inherits="Budget_Budget_RequesteeQueue" %>

<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>

 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 25%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script src="../../Script/jquery.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        window.onload = function () {
            //debugger;
            var QueueData = document.getElementById("<%= hidQueueData.ClientID %>").value;                
            TableFill();     
        }
        function FromServer(arg, context) {             
            if (context == 1) {         
                 
            }
        }
        function ReqUpdateOnClick(ReqID, BranchID, GLID, action) {
            if (action == 3) {
                window.open("Budget_Request_Form.aspx?ReqID=" + ReqID + "&BranchID=" + BranchID + "&GLID=" + GLID + "", "_self");
            }
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function TableFill() {
            //debugger;
            var QueueData = "";
            QueueData = document.getElementById("<%= hidQueueData.ClientID %>").value.split("Ñ");
            document.getElementById("<%= QueuePanel.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";            
            tab += "<div style='width:95%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'>";
            tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center' id='QueueTable'>";
            tab += "<tr class=mainhead style='height:50px;'>";
            tab += "<td style='width:5%;text-align:center'>Sl.No</td>";
            tab += "<td style='width:15%;text-align:center' >Expense Head</td>";
            tab += "<td style='width:5%;text-align:center;'>Amount Carry Forward</td>";
            tab += "<td style='width:10%;text-align:center;' >Budget Allocated/Month</td>";
            tab += "<td style='width:10%;text-align:center;' >Actual Bill Amount</td>";
            tab += "<td style='width:5%;text-align:center'>Done Date</td>";
            tab += "<td style='width:15%;text-align:center'>Done By</td>";
            tab += "<td style='width:10%;text-align:center;' >Action Taken</td>";
            tab += "<td style='width:20%;text-align:center;' >Remarks</td>";
            tab += "<td style='width:10%;text-align:center;'></td>";
            tab += "</tr>";
            var QueueLen = QueueData.length-1;
            for(var k=0;k<QueueLen;k++){
                var QueueRow = QueueData[k].split("ÿ");
                var level = "";
                var action = "";
                var carry="";
                if (QueueRow[2]==1)
                    carry='Yes'
                else
                    carry='No'
                if (QueueRow[6] == 1) {
                    level = 'Branch Manager'
                }
                else if (QueueRow[6] == 2) {
                    level='Cluster Manager'
                }
                else if (QueueRow[6] == 3) {
                    level = 'Branch Banking Head'
                }
                else if (QueueRow[6] == 4) {
                    level = 'EVP'
                }
                if (QueueRow[7] == 1) {
                    action = 'Approved'
                }
                else if (QueueRow[7] == 2) {
                    action = 'Rejected'
                }
                else if (QueueRow[7] == 3) {
                    action = 'Returned'
                }
                else if (QueueRow[7] == 0)
                {
                    action='Pending'
                }
                var date = QueueRow[5];
                date = date.split(' ')[0];
                tab += "<tr class=sub_first style ='height:30px;'>";
                tab += "<td style='width:5%;text-align:center'>"+(k+1)+"</td>";
                tab += "<td style='width:15%;text-align:center'>"+QueueRow[1]+"</td>";
                tab += "<td style='width:5%;text-align:center'>" + carry + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + QueueRow[3] + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + QueueRow[4] + "</td>";
                tab += "<td style='width:5%;text-align:center'>" + date + "</td>";
                tab += "<td style='width:15%;text-align:center'>" + level + "</td>";
                tab += "<td style='width:10%;text-align:center'>" + action + "</td>";
                tab += "<td style='width:20%;text-align:center'>" + QueueRow[8] + "</td>";
                tab += "<td style='width:10%;text-align:center'><img src='../image/update2.png' title='Update' style='height:22px; width:22px; cursor:pointer;' onclick='ReqUpdateOnClick("  + QueueRow[0] + "," + QueueRow[10] + "," + QueueRow[9]+"," + QueueRow[7] + ")' /></td>";
                tab += "</tr>";
            }
            tab += "</table></div>";
            document.getElementById("<%= QueuePanel.ClientID %>").innerHTML = tab;    
           <%-- if(QueueData.toString() == ""){
                document.getElementById("<%= QueuePanel.ClientID %>").innerHTML = "<table style='width:100%'><tr><td style='width:30%'></td><td style='width:40%;text-align:center;font-size:16px;'><b>No requests pending.</b></td><td style='width:30%'></td>";  
            }   --%>
        }
        //function ComboFill(data, ddlName) {
        //    document.getElementById(ddlName).options.length = 0;
        //    var rows = data.split("Ñ");
        //    for (a = 0; a < rows.length-1; a++) {
        //        var cols = rows[a].split("ÿ");
        //        var option1 = document.createElement("OPTION");
        //        option1.value = cols[0];
        //        option1.text = cols[1];
        //        document.getElementById(ddlName).add(option1);
        //    }
        //}
        //function ClearCombo(control) {
        //    document.getElementById(control).options.length = 0;
        //    var option1 = document.createElement("OPTION");
        //    option1.value = -1;
        //    option1.text = " -----Select-----";
        //    document.getElementById(control).add(option1);
        //}
    </script>   
</head>
</html>
<br />
<div  style="width:95%; height:auto; margin:0px auto; background-color:#EEB8A6;">
<br />
    <div id = "divSection1" class = "sec1" style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
    <br />   <br />
        <table class="style1" id="UserTable" style="width:100%;margin: 0px auto;">
            <tr>
                <td>
                    <asp:HiddenField ID="hidQueueData" runat="server" />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="width:95%;">
                    <asp:Panel ID="QueuePanel" runat="server">
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <div style="width:80%;height:auto; padding-left:10px;margin:0px auto;">
            <div style="text-align:center; height: 63px;"><br />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
            </div>
        </div>
    </div>
    <br />
</div>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>



