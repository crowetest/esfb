﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Budget_Budget_Request_Form
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim Budgid As New Integer

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
            'If GN.FormAccess(CInt(Session("UserID")), 1439) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            If GN.FormAccess(CInt(Session("UserID")), 1444) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Expense Amount Request Form"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            If Not IsPostBack Then
                Dim ReqID As Integer = CInt(Request.QueryString.Get("ReqID"))
                Dim BranchID As Integer = CInt(Request.QueryString.Get("BranchID"))
                Dim GLID As Integer = CInt(Request.QueryString.Get("GLID"))

                If (ReqID <> 0) Then
                    DT = DB.ExecuteDataSet("select GL_ID,GL_Name,GL_Code from ESFB.dbo.Budg_GL_Master where GL_ID= " + GLID.ToString()).Tables(0)

                    GN.ComboFill(cmbExpense, DT, 0, 1)
                    txtcode.Text = DT.Rows(0)(2).ToString()
                    Me.hidcode.Value = CStr(ReqID)
                    DT1 = DB.ExecuteDataSet("select Req_Amt,Reason,Req_dt from ESFB.dbo.Budg_Req_Master where Req_id= " + ReqID.ToString()).Tables(0)
                    txtRequestamnt.Text = DT1.Rows(0)(0).ToString()
                    txtreason.Text = DT1.Rows(0)(1).ToString()
                    txtFromDt.Text = DT1.Rows(0)(2).ToString()
                    ' Me.cmbExpense.Attributes.Add("onchange", "return ExpenseOnChange()")

                Else
                    DT = DB.ExecuteDataSet("select '-1' as id,'------Select--------' union all select GL_ID,GL_Name from ESFB.dbo.Budg_GL_Master ").Tables(0)

                    GN.ComboFill(cmbExpense, DT, 0, 1)


                    Me.cmbExpense.Attributes.Add("onchange", "return ExpenseOnChange()")
                End If

            End If
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")

            cmbExpense.Focus()
        Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub
        Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
            DT.Dispose()
            DB.dispose()
            GC.Collect()
        End Sub
#End Region
#Region "Call Back"
        Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
            Return CallBackReturn
        End Function
        Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
            Dim Data() As String = eventArgument.Split(CChar("Ø"))

        If CInt(Data(0)) = 1 Then
            Dim GL_Id As Integer = CInt(Data(1).ToString())
            Dim BranchID As Integer = CInt(Session("BranchID"))
            ' DT = DB.ExecuteDataSet("select a.branch_type,a.Branch_Category,b.Budg_ID from ESFB.dbo.branch_master a inner join Budget_Master b on b.branch_type=a.branch_type and b.Br_Category=a.Branch_Category  where b.status_id=1 and a.branch_id=" + BranchID.ToString()).Tables(0)
            DT = DB.ExecuteDataSet("select branch_type,Branch_Category from ESFB.dbo.branch_master  where status_id=1 and branch_id=" + BranchID.ToString()).Tables(0)

            If (DT.Rows.Count > 0) Then
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@BRANCH_Type", SqlDbType.Int)
                Params(0).Value = DT.Rows(0)(0).ToString()
                Params(1) = New SqlParameter("@Br_Category", SqlDbType.VarChar, 10)
                Params(1).Value = DT.Rows(0)(1).ToString()
                Params(2) = New SqlParameter("@Branch_id", SqlDbType.Int)
                Params(2).Value = BranchID
                Params(3) = New SqlParameter("@GL_Id", SqlDbType.Int)
                Params(3).Value = GL_Id
                'Params(4) = New SqlParameter("@Budg_Id", SqlDbType.Int)
                'Params(4).Value = DT.Rows(0)(2).ToString()
                DT1 = DB.ExecuteDataSet("Sp_Get_Budg_BalanceAmount", Params).Tables(0)


                If (DT1.Rows.Count > 0) Then

                    CallBackReturn = DT1.Rows(0)(0).ToString() + "Ø" + DT1.Rows(0)(1).ToString() + "Ø" + DT1.Rows(0)(2).ToString() + "Ø" + DT1.Rows(0)(3).ToString() + "Ø" + DT1.Rows(0)(4).ToString()
                End If

            End If

        End If
        If CInt(Data(0)) = 2 Then





        End If
    End Sub
#End Region

        Private Sub initializeControls()



    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim Data As String() = hdnvalue.Value.Split(CChar("Ø"))
            Dim ErrorFlag As Integer = 0
            Dim ReqID As Integer = 0
            Dim subID As Integer = 0
            Dim Reqdt As Date
            Dim Message As String = Nothing
            Dim reqamnt As Double = 0.0
            Dim billamnt As Double = 0.0
            Dim reason As String = Nothing
            Dim UserID As String = Session("UserID").ToString()
            Dim Expense_ID As Integer = CInt(Data(1).ToString())
            Dim Branch As Integer = CInt(Session("BranchID"))
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim myFile As HttpPostedFile = fup1.PostedFile
            Dim FileLen As Integer = myFile.ContentLength
            Dim FileName As String = ""
            If (FileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(FileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, FileLen)
            End If
            If (Data(2).ToString() = Nothing) Then
                reqamnt = 0.0
            Else
                reqamnt = CInt(Data(2).ToString())

            End If


            If (Data(3).ToString() = Nothing) Then
                reason = Nothing
            Else
                reason = CStr(Data(3).ToString())
            End If

            Dim nooflevels As Integer = CInt(Data(4).ToString())
            If (Data(5).ToString() = Nothing) Then
                ReqID = 0
            Else
                ReqID = CInt(Data(5).ToString())
            End If
            Budgid = CInt(Data(6).ToString())
            Reqdt = CDate(Data(7).ToString())
            Try
                Dim Params(14) As SqlParameter
                Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
                Params(0).Value = Branch
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@GL_id", SqlDbType.Int)
                Params(2).Value = Expense_ID
                Params(3) = New SqlParameter("@Req_Amnt", SqlDbType.Decimal)
                Params(3).Value = reqamnt
                Params(4) = New SqlParameter("@Bill_Amnt", SqlDbType.Money)
                Params(4).Value = billamnt = 0
                Params(5) = New SqlParameter("@reason", SqlDbType.NVarChar, 4000)
                Params(5).Value = reason
                Params(6) = New SqlParameter("@budgid", SqlDbType.Int)
                Params(6).Value = Budgid
                Params(7) = New SqlParameter("@nooflevels", SqlDbType.Int)
                Params(7).Value = nooflevels
                Params(8) = New SqlParameter("@Reqid", SqlDbType.Int)
                Params(8).Value = ReqID
                Params(9) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(9).Direction = ParameterDirection.Output
                Params(10) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(10).Direction = ParameterDirection.Output
                Params(11) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                Params(11).Value = AttachImg
                Params(12) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                Params(12).Value = ContentType
                Params(13) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                Params(13).Value = FileName
                Params(14) = New SqlParameter("@reqdt", SqlDbType.DateTime)
                Params(14).Value = Reqdt

                DB.ExecuteNonQuery("SP_Add_Budg_Request_Form", Params)
                ErrorFlag = CInt(Params(9).Value)
                Message = CStr(Params(10).Value)
            Catch ex1 As Exception
                Message = ex1.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try


            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("        window.open('Budget_Request_Form.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)


        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class


