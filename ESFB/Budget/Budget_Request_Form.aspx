﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Budget_Request_Form.aspx.vb" Inherits="Budget_Budget_Request_Form" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %> 
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">



    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
        .auto-style1 {
            width: 22%;
        }
      



    </style>
  
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
         
            

        function setStartDate(sender, args) {
            
            var date= new Date();
            var startDt =  new Date(date.getFullYear(),date.getMonth(),2);
            var EndDt= new Date();

                
            sender._startDate =  new Date(startDt);
            sender._endDate =  new Date(EndDt);    
              }
           
        

   
           function ExpenseOnChange() {
         
               var Expenseid = document.getElementById("<%= cmbExpense.ClientID %>").value;
               if (Expenseid > 0) {
                   
                   var ToData = "1Ø" + Expenseid; 
                    ToServer(ToData, 1);
                }
            }
           
       
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ñ");
       
                for (a = 0; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function ClearCombo(control) {
                document.getElementById(control).options.length = 0;
                var option1 = document.createElement("OPTION");
                option1.value = -1;
                option1.text = " -----Select-----";
                document.getElementById(control).add(option1);
            }
            function FromServer(arg, context) {
                //debugger;
                if (context == 1) {
                    var Data = arg.split("Ø");
                  
                    var budgetvalue = Data[0];
                   
                    document.getElementById("<%= txtcode.ClientID %>").value= Data[1];
                    var reqamnt=  document.getElementById("<%= txtRequestamnt.ClientID %>").value;
                    var availablebal=Data[2];
                   
                    var unclearedbalance=Data[3];
                    var exceeded=0.0;
                    
                    
                  
                    var budgid=Data[4];
                    document.getElementById("<%= hid_avail_bal.ClientID %>").value=availablebal+"Ø"+unclearedbalance+"Ø"+exceeded+"Ø"+budgetvalue;
                    document.getElementById("<%= hdn_budg.ClientID %>").value=budgid;
                    FillBudget(budgetvalue,availablebal,unclearedbalance,exceeded);
               
    }
 
       
}
function btnExit_onclick() {
    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
}

     function  FillBudget(budgetvalue,availablebal,unclearedbalance,exceeded) 
            
     {
         
         if(availablebal<0)
         {
             availablebal=0.0000;
         }
         if(unclearedbalance<0)
                       
         {
                         
             exceeded=unclearedbalance*-1;
             unclearedbalance=0.0000;
                        
                       
         }
         else
         {
             exceeded=0.0;
         }
         var percentage=0.0;
         var nooflevelsinitial=1;
         var FinalApprover="Branch Manager";
        
                   
             percentage=(Math.abs(exceeded).toFixed(2)/(Math.abs(budgetvalue).toFixed(2)))*100;
                   
                  
         
         if (percentage>0.0 && percentage<=10)
         {
                    
             nooflevelsinitial=2;
             FinalApprover="Cluster Head"
         }
         else if(percentage>10 && percentage<=20)
         {
                    
             nooflevelsinitial=3;
             FinalApprover="HOD"
         }
         else if(percentage>20)
         { 
                   
             nooflevelsinitial=4;
             FinalApprover="EVP"
         }
           
            
            var row_bg = 0;
            var tab = "";
            var cnt = 0;
            tab += "<div style='width:45%; height:auto; margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria'; ' align='center'>";
           
            tab += "<tr style='height:30px; background-color:#B21C32; ' >";
            tab += "<td style='width:10%;text-align:center ; color:#FFF' ><b></b></td>";
            tab += "<td style='width:20%;text-align:left; color:#FFF'><b>Monthly</b></td>";
          
            tab += "</tr>";
          
           
            tab += "<tr style='height:30px; background-color:#ffcccb; ' >";
            tab += "<td style='width:20%;text-align:center ;' ><b>Budget</b></td>";
            tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" + budgetvalue + "</td>";
         
            tab += "</tr>";

            tab += "<tr style='height:30px; background-color:#ffcccb; ' >";
            tab += "<td style='width:20%;text-align:center ;' ><b>Available Balance</b></td>";
            tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" + unclearedbalance  + "</td>";
           // availablebal
            tab += "</tr>";

            //tab += "<tr style='height:30px; background-color:#ffcccb; ' >";
            //tab += "<td style='width:20%;text-align:center ;' ><b>Uncleared Balance</b></td>";
            //tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" +unclearedbalance  + "</td>";
          
            tab += "</tr>";

            tab += "<tr style='height:30px; background-color:#ffcccb; ' >";
            tab += "<td style='width:20%;text-align:center ;'><b>Exceeded Amount</b></td>";
            tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" +exceeded  + "</td>";
           
            tab += "</tr>";

            tab += "<tr style='height:30px; background-color:#ffcccb; ' >";
            tab += "<td style='width:20%;text-align:center ;'><b>Exceeded %</b></td>";
            tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" +percentage  + "</td>";
           
            tab += "</tr>";

            tab += "<tr style='height:30px; background-color:#ffcccb; ' >";
            tab += "<td style='width:20%;text-align:center ;'><b>Final Approver</b></td>";
            tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" +FinalApprover  + "</td>";
           
            tab += "</tr>";


            tab += "</table></div>";
           
            document.getElementById("<%= pnlSec1.ClientID %>").innerHTML = tab;

        }

   function RequestOnClick() {   
             
                                                                                                                                                                                                                  
    if (document.getElementById("<%= cmbExpense.ClientID%>").value == "-1") 
    {
        alert("Select Expense");
        document.getElementById("<%= cmbExpense.ClientID%>").focus();
        return false;
    }
      
  
        if (document.getElementById("<%= txtRequestamnt.ClientID%>").value == "") 
    {
       
        alert("Enter Your Request Amount");
        document.getElementById("<%= txtRequestamnt.ClientID%>").focus();
            return false;
    }
     
       
        if (document.getElementById("<%= txtFromDt.ClientID%>").value == "") 
    {
       
        alert("Enter Transaction date");
        document.getElementById("<%= txtFromDt.ClientID%>").focus();
            return false;
    }
       var Expense_ID=document.getElementById("<%= cmbExpense.ClientID%>").value;
               
       var reqamnt	= document.getElementById("<%= txtRequestamnt.ClientID%>").value;
       var nooflevelsinitial=1;         
       var appendvalues=document.getElementById("<%= hid_avail_bal.ClientID %>").value;
       
       if(appendvalues!='')
       {
          
           var splitbal= appendvalues.split("Ø");
                 
           var availablebal=splitbal[0];
           
           var exceeded=splitbal[2];
           
           
           if(parseFloat(reqamnt)>parseFloat(splitbal[1]) && parseFloat(splitbal[1])!=parseFloat(splitbal[0]))
           {
                   
               alert("Request is not possible for exceeded amount till pending approval exists.");
               return false;
                
           }
           var percentage=0.0;
           var nooflevelsinitial=1;
           
           if(parseFloat(reqamnt)>parseFloat(splitbal[1]))
           {
                    
               exceeded=parseFloat(reqamnt)-parseFloat(splitbal[1]);
                   
               percentage=(Math.abs(exceeded).toFixed(2)/(Math.abs(splitbal[3]).toFixed(2)))*100;
                   
                   
           }
           if (percentage>0.0 && percentage<=10)
           {
                    
               nooflevelsinitial=2;
           }
           else if(percentage>10 && percentage<=20)
           {
                    
               nooflevelsinitial=3;
           }
           else if(percentage>20)
           { 
                   
               nooflevelsinitial=4;
           }
           
       
       }
      
       var reason	= document.getElementById("<%= txtreason.ClientID%>").value;
      var reqdt     = document.getElementById("<%= txtFromDt.ClientID%>").value;
      var reqid=document.getElementById("<%= hidcode.ClientID %>").value;
      var budgid=document.getElementById("<%= hdn_budg.ClientID %>").value;
       document.getElementById("<%= hdnvalue.ClientID %>").value = "1Ø" + Expense_ID+ "Ø" + reqamnt +  "Ø" + reason + "Ø" + nooflevelsinitial+ "Ø" + reqid+"Ø" + budgid+"Ø" + reqdt;      
               
   }
   
            function calc()
            
            {
                //debugger;
               
                var reqamnt	= document.getElementById("<%= txtRequestamnt.ClientID%>").value;
      
                var appendvalues=document.getElementById("<%= hid_avail_bal.ClientID %>").value;
               
                if(appendvalues!='')
                {
          
                    var splitbal= appendvalues.split("Ø");
                 
                    var availablebal=splitbal[0];
           
                    var exceeded=splitbal[2];

                  
                   
                    if((parseFloat(reqamnt)>parseFloat(splitbal[1])) && (parseFloat(splitbal[1])!=parseFloat(splitbal[0])))
                    {
                   
                        alert("Request is not possible for exceeded amount till pending approval exists.");
                       
                        return false;
                
                    }
                     document.getElementById("<%= txtRequestamnt.ClientID%>").focus;
                }
            }
            function window_onload(){
                ExpenseOnChange();
               
            }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return window_onload()
            // ]]>
        </script>
    </head>
    </html>
   
    <br />
    <br />
       <table class="style1" style="width: 70%; margin: 0px auto; align-content:center">
            <tr>
                <td></td>
          <td style="width:15%;">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
        </td>
            <td style="width:15%;">
    
 
   </td></tr>    
        <tr id="App">
            <td class="auto-style1">
                   <asp:HiddenField ID="hid_Expense" runat="server" />
            </td>
           
            <td style="width: 12%; text-align: left;">
                GL Description</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbExpense"  Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
      
         <tr id="code">
            <td class="auto-style1">
                   <asp:HiddenField ID="hidcode" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                GL Code</td>
           <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtcode" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15" ReadOnly="true"  />
            </td>
        </tr>
        <tr id="RequestAmnt">
             <td class="auto-style1">
                   <asp:HiddenField ID="hid_avail_bal" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Request Amount</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtRequestamnt" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15" onblur="return calc()" />
            </td>
        </tr>
             <tr id="Img">
        <td style="width:25%;"><asp:HiddenField 
                    ID="hdn_budg" runat="server" />
                </td>
            <td style="width:12% ; text-align:left;">
                Attachments if any</td>
            <td style="width:63% ">
                &nbsp;&nbsp;<input id="fup1" type="file" runat="server" 
                style="font-family: Cambria; font-size: 10pt" />
                </td>

        </tr>
       <tr >
            <td style="width:25%;"><asp:HiddenField 
                    ID="hdn_ex" runat="server" />
                </td>
            <td style="width:12%; text-align:left;">
                Transaction Date </td>
            <td style="width:63%">
                 &nbsp;&nbsp;<asp:TextBox ID="txtFromDt" class="NormalText" runat="server" 
                    Width="50%" onkeypress="return false" Height="20px" ReadOnly="true" ></asp:TextBox>
                <asp:CalendarExtender ID="ce1" runat="server" 
                 Enabled="True" TargetControlID="txtFromDt" Format="dd MMM yyyy"
                      EnabledOnClient="true" OnClientShowing="setStartDate">
                </asp:CalendarExtender>
                
            </td> 
 </tr>                 
            <tr id="Reason">
            <td class="auto-style1">
                <asp:HiddenField ID="hdnvalue" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Remarks
            </td>
            <td style="width: 63%">
               &nbsp; &nbsp;
                <asp:TextBox ID="txtreason" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="1000" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="55px"  />
            </td>
        </tr>
            </table>
     <br />
        <br />
     <table ID="tblSec1" style="width:60%;height:90px;margin:0px auto; align-content:center">            
            <tr id="Tr1">
                <td style="text-align: center;">
                    <asp:Panel ID="pnlSec1" Style="width: 100%;  text-align: left; float: left;" runat="server">               

                    </asp:Panel>
                </td>
            </tr>
          <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                
                <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="14%" Height="26px" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 14%; height: 26px;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
               
            </td>
        </tr>
        </table> 
        <br />
        <br />
         
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>



