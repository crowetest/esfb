﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Budget_Reports_CHExpenseReport
    Inherits System.Web.UI.Page

    Implements Web.UI.ICallbackEventHandler
        Dim DT As New DataTable
    Dim DT5 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DB As New MS_SQL.Connect
        Dim GF As New GeneralFunctions


    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
            'If GF.FormAccess(CInt(Session("UserID")), 1444) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            If GF.FormAccess(CInt(Session("UserID")), 1451) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Cluster Head Expense Report"
            Dim Clusterid As Integer
            Dim CH As String
            Dim UserID As String = CStr(Session("UserID"))
            If Not IsPostBack Then
                DT5 = DB.ExecuteDataSet("Select Cluster_id,Cluster_Head_id from Cluster_Master where Cluster_Head_id=" + UserID.ToString()).Tables(0)
                If (DT5.Rows.Count > 0) Then
                    Clusterid = CInt(DT5.Rows(0)(0).ToString())
                    CH = DT5.Rows(0)(1).ToString()
                End If
                DT2 = DB.ExecuteDataSet("SELECT distinct cluster_id,cluster_name FROM cluster_master where cluster_id=" + Clusterid.ToString()).Tables(0)
                txtcluster.Text = DT2.Rows(0)(1).ToString()

                DT3 = DB.ExecuteDataSet("SELECT -1,'--All--' UNION ALL select distinct b.BRANCH_ID,b.BRANCH_NAME FROM Budg_Req_Master c left join BRANCH_MASTER b on b.branch_id=c.branch_id where c.Finish_id=1 and b.cluster_id=" + Clusterid.ToString()).Tables(0)
                GF.ComboFill(cmbbranch, DT3, 0, 1)

                cmbbranch.Attributes.Add("onchange", "BranchOnChange()")
                Me.hdnValue.Value = CStr(DT2.Rows(0)(0).ToString())

                DT = DB.ExecuteDataSet("SELECT -1,'--All--' UNION ALL select GL_ID,GL_Name from ESFB.dbo.Budg_GL_Master ").Tables(0)

                GF.ComboFill(cmbGLName, DT, 0, 1)


                Me.cmbGLName.Attributes.Add("onchange", "ExpenseOnChange()")
            End If


            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub
        Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
            DT.Dispose()
        DT5.Dispose()
        DB.dispose()
            GC.Collect()
        End Sub
#End Region
#Region "Call Back"
        Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
            Return CallBackReturn
        End Function
        Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent


            Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim BranchID As Integer = CInt(Data(1).ToString())
            CallBackReturn = CStr(BranchID)
        End If
        If CInt(Data(0)) = 2 Then

            Dim ExpenseID As Integer = CInt(Data(1).ToString())
            CallBackReturn = CStr(ExpenseID)
        End If
    End Sub

#End Region

    End Class






