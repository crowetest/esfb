﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="HOExpenseReport.aspx.vb" Inherits="Budget_Reports_HOExpenseReport" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  

 
<br />

    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
     <tr>
          <td style="width:15%;">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
        </td>
            <td style="width:15%;">
    
 
   </td></tr>
        <%--<tr>
            <td style="width:15%;">
                State</td>
           <td  style="width:85%;">
                 <asp:DropDownList ID="cmbstate"  Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black" Height="20px">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
           </td>
        </tr>--%>
      
           <tr>
            <td style="width:15%;">
                Branch Name</td>
           <td  style="width:85%;">
                  <asp:DropDownList ID="cmbbranch"  Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black" Height="20px">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
           
      </td>
          
        </tr>
         <tr>
            <td style="width:15%;">
                GL Name</td>
           <td  style="width:85%;">
                  <asp:DropDownList ID="cmbGLName"  Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black" Height="20px">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
           
      </td>
          
        </tr>
       
            <tr >
           
            <td style="width:15%; text-align:left;">
                From Date </td>
            <td style="width:85%">
                <asp:TextBox ID="txtFromDt" class="NormalText" runat="server" 
                    Width="50%" onkeypress="return false" Height="20px" ReadOnly="true" ></asp:TextBox>
                <asp:CalendarExtender ID="ce1" runat="server" 
                 Enabled="True" TargetControlID="txtFromDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
                
            </td> 
 </tr>                 
           <tr >
           
            <td style="width:15%; text-align:left;">
                To Date </td>
            <td style="width:85%">
                <asp:TextBox ID="txtToDt" class="NormalText" runat="server" 
                    Width="50%" onkeypress="return false" Height="20px" ReadOnly="true" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" 
                 Enabled="True" TargetControlID="txtToDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
                
            </td> 
 </tr>                  
          <tr>
           <td style="width:15%; text-align:left;">
                 </td>
            <td style="width:85%">
            
           
                <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 20%" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;
                
                <input id="cmd_Export_Excel" style="font-family: Cambria; cursor: pointer; width: 20%;" 
                type="button" value="VIEW EXCEL" onclick="return btnExcelView_onclick()" />&nbsp;&nbsp;
                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 20%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>

         


            <td style="width:100%; height: 18px; text-align:center;">   </td> 
                        
             
          
      
        </tr>
       
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
               
                 <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
               
        </tr>
         <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
               
                 <asp:HiddenField ID="hdnExpense" runat="server" />
            </td>
               
        </tr>
    </table>

  
   
    <script language="javascript" type="text/javascript">

  
     function FromServer(arg, context) {
         if (context == 1) {
             var Data = arg;
           
               document.getElementById("<%= hdnReportID.ClientID %>").value = Data;
               
           }

         if (context == 2) {
             var Data = arg;
               document.getElementById("<%= hdnExpense.ClientID %>").value = Data;
               
            }
            
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

        
           function ExpenseOnChange() {
         
               var Expenseid = document.getElementById("<%= cmbGLName.ClientID %>").value;
             
                   
                   var ToData = "2Ø" + Expenseid; 
                    ToServer(ToData, 2);
                
            }
          function BranchOnChange() {
     
           
                var branchid = document.getElementById("<%= cmbbranch.ClientID %>").value;
            var ToData = "1Ø" + branchid;
                ToServer(ToData, 1);
            }
         function btnView_onclick() 
        {
            // var ClusterID = document.getElementById("<%= hdnValue.ClientID %>").value;
             var BranchID = document.getElementById("<%= hdnReportID.ClientID %>").value
            // alert(BranchID);
              var ExpenseID=document.getElementById("<%= hdnExpense.ClientID %>").value
             var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            // alert(FromDate);
           //  var FromDt = new Date(FromDate);
            
             var ToDt = document.getElementById("<%= txttODt.ClientID %>").value;
            // alert(ToDate);
           //  var ToDt = new Date(ToDate);
             if (FromDt == "")
             {
                 alert("Select From Date");
                 return false
             }
             if (ToDt == "") {
                 alert("Select To Date");
                 return false
             }
                  if (ToDt < FromDt) {
                 alert("To date should be greater than From date");
                 return false;
             }
   
                  window.open("View_HOExpenseReport.aspx?ViewType=-1 &BID=" + BranchID +"&EID=" + ExpenseID  + "&TDT=" + ToDt + "&FDT=" + FromDt + " ", "_self");
            
        
       }     
        function btnExcelView_onclick() 
        {
         // var BranchID = document.getElementById("<%= hdnValue.ClientID %>").value;
          var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var BranchID = document.getElementById("<%= hdnReportID.ClientID %>").value
             var ExpenseID=document.getElementById("<%= hdnExpense.ClientID %>").value
           // alert(FromDate)
        //  var FromDt = new Date(FromDate);
          //alert(FromDt)
            var ToDt = document.getElementById("<%= txttODt.ClientID %>").value;
            // var ToDt = new Date(ToDate);
            if (FromDt == "") {
                alert("Select From Date");
                return false
            }
            if (ToDt == "") {
                alert("Select To Date");
                return false
            }

            if (ToDt < FromDt) {
                 alert("To date should be greater than From date");
                 return false;
             }
           
            window.open("View_HOExpenseReport.aspx?ViewType=1 &BID=" + BranchID +"&EID=" + ExpenseID + "&TDT=" + ToDt + "&FDT=" + FromDt + " ", "_self");

       }     
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }
   
  
       
    </script>
</asp:Content>













