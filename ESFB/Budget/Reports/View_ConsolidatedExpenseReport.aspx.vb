﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Budget_Reports_View_ConsolidatedExpenseReport
    Inherits System.Web.UI.Page

    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTEXCEL As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim ViewType As Integer
            ViewType = CInt(Request.QueryString.Get("ViewType"))
            Dim FromDate, ToDate As Date

            Dim strwhere As String = ""
            '  Dim BranchID = Request.QueryString.Get("BID")
            ' Dim ExpenseID = Request.QueryString.Get("EID")
            FromDate = CDate(Request.QueryString.Get("FDT"))
            ToDate = CDate(Request.QueryString.Get("TDT"))
            'strwhere = " where d.Freq_id = " + Freq

            Dim DateFrom As String
            DateFrom = FromDate.ToString("yyyy-MM-dd")



            Dim DateTo As String
            DateTo = ToDate.ToString("yyyy-MM-dd")

            strwhere = " And cast(B.Submitted_dt As Date) between '" & DateFrom & "' and '" & DateTo & "'" + "and"
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RH.Heading(Session("FirmName"), tb, "Consolidated Expense Report", 100)
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_12 As New TableCell
            ' TRHead_11

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            ' TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            'TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver


            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            '  TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid

            TRHead_00.Font.Bold = True
            TRHead_01.Font.Bold = True
            TRHead_02.Font.Bold = True
            TRHead_03.Font.Bold = True
            TRHead_04.Font.Bold = True
            TRHead_05.Font.Bold = True
            TRHead_06.Font.Bold = True
            TRHead_07.Font.Bold = True
            TRHead_08.Font.Bold = True
            TRHead_09.Font.Bold = True
            TRHead_10.Font.Bold = True
            '  TRHead_11.Font.Bold = True
            TRHead_12.Font.Bold = True



            RH.AddColumn(TRHead, TRHead_00, 4, 4, "c", "Sl.No")
            RH.AddColumn(TRHead, TRHead_01, 10, 10, "c", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "c", "State")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "c", "Cluster")
            RH.AddColumn(TRHead, TRHead_04, 4, 4, "c", "Branch Category")
            RH.AddColumn(TRHead, TRHead_05, 8, 8, "c", "Branch Type")
            RH.AddColumn(TRHead, TRHead_06, 8, 8, "c", "Financial Year")
            RH.AddColumn(TRHead, TRHead_07, 8, 8, "c", "Transaction Date")
            RH.AddColumn(TRHead, TRHead_08, 14, 14, "c", "Expense Head")
            RH.AddColumn(TRHead, TRHead_09, 8, 8, "c", "Budget Amount")
            RH.AddColumn(TRHead, TRHead_10, 8, 8, "c", "Bill Amount")
            ' RH.AddColumn(TRHead, TRHead_11, 4, 4, "c", "Attachment")
            RH.AddColumn(TRHead, TRHead_12, 14, 14, "c", "Reason")



            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)

            Dim i As Integer

            DT = DB.ExecuteDataSet("select distinct A.Branch_Name,G.State_Name,F.Cluster_Name,A.Branch_Category,A.branch_type,D.Budg_From,B.Req_dt, " +
                                    " c.GL_Name,D.Budg_Amt,B.Req_Amt,E.ReqNo,B.Reason from Branch_Master A left join State_Master G on G.State_id=A.State_id " +
                                    " inner join Budg_Req_Master B on B.Branch_id=A.Branch_id inner join Budg_GL_Master c on C.GL_id = B.GL_id " +
                                    " inner join Budget_Master D on D.Budg_id=B.Budg_id left join DMS_ESFB.dbo.Budg_ATTACH E on E.ReqNo=B.Req_id " +
                                    " inner join Cluster_Master F on F.Cluster_id=A.Cluster_id " +
                                    " where  D.Status_id=1 " + strwhere + " B.Action_id=1 and B.Finish_id=1").Tables(0)


            DTEXCEL = DB.ExecuteDataSet("select distinct A.Branch_Name As [BranchName],G.State_Name As [StateName],F.Cluster_Name As [ClusterName],A.Branch_Category As [Branch Category], " +
                                        " A.branch_type As [BranchType],D.Budg_From As [Month/Year],B.Req_dt As [Transaction Date], " +
                                       " c.GL_Name As [Expense Head],D.Budg_Amt As [Budget Amount],B.Req_Amt As [Bill Amount],B.Reason As [Reason] from Branch_Master A " +
                                       " left join State_Master G on G.State_id=A.State_id " +
                                       " inner join Budg_Req_Master B on B.Branch_id=A.Branch_id inner join Budg_GL_Master c on C.GL_id = B.GL_id " +
                                    " inner join Budget_Master D on D.Budg_id=B.Budg_id left join DMS_ESFB.dbo.Budg_ATTACH E on E.ReqNo=B.Req_id " +
                                    " inner join Cluster_Master F on F.Cluster_id=A.Cluster_id " +
                              " where  D.Status_id=1 " + strwhere + " B.Action_id=1 and B.Finish_id=1  ").Tables(0)


            If ViewType = 1 Then
                Export_Excel_Click()
            End If

            Dim createddate As String = ""

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_12 As New TableCell
                ' TR3_11,
                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                ' TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"



                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                ' TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                '  TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                Dim Type As String
                If (DR(4).ToString() = "1") Then
                    Type = "WithOut MB"
                Else
                    Type = "With MB"
                End If

                Dim dat As Date = CDate(DR(5).ToString())
                Dim frmon As Integer = Year(dat)
                Dim Tomon As Integer = frmon + 1

                Dim Period As String = Nothing

                Period = frmon.ToString() + "-" + Tomon.ToString()
                '+ "-" + frmon+1


                RH.AddColumn(TR3, TR3_00, 4, 4, "c", i)
                RH.AddColumn(TR3, TR3_01, 10, 10, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 10, 10, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_04, 4, 4, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_05, 8, 8, "l", Type)
                RH.AddColumn(TR3, TR3_06, 8, 8, "l", Period)
                RH.AddColumn(TR3, TR3_07, 8, 8, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_08, 14, 14, "l", DR(7).ToString())
                RH.AddColumn(TR3, TR3_09, 8, 8, "l", DR(8).ToString())
                RH.AddColumn(TR3, TR3_10, 8, 8, "l", DR(9).ToString())
                '  RH.AddColumn(TR3, TR3_11, 4, 4, "l", DR(10).ToString())
                RH.AddColumn(TR3, TR3_12, 14, 14, "l", DR(11).ToString())

                tb.Controls.Add(TR3)
            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=ACReasonsReport.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "BM Expense Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Protected Sub Export_Excel_Click()
        Try
            Dim HeaderText As String
            HeaderText = "BM Expense Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class







