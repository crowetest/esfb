﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Budget_Reports_View_CHExpenseReport
    Inherits System.Web.UI.Page

    Dim DB As New MS_SQL.Connect
        Dim DT As New DataTable
        Dim DTEXCEL As New DataTable
        Dim DT1 As New DataTable
        Dim RH As New WholeHelper.ClsRepCtrl
        Dim tb As New Table
        Dim PostID As Integer
        Dim GN As New GeneralFunctions
        Dim WebTools As New WebApp.Tools
        Dim TraDt As Date

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim DT As New DataTable
                Dim ViewType As Integer
                ViewType = CInt(Request.QueryString.Get("ViewType"))
                Dim FromDate, ToDate As Date

                Dim strwhere As String = ""
            Dim BranchID = Request.QueryString.Get("BID")
            Dim ExpenseID = Request.QueryString.Get("EID")
            FromDate = CDate(Request.QueryString.Get("FDT"))
            ToDate = CDate(Request.QueryString.Get("TDT"))
            'strwhere = " where d.Freq_id = " + Freq
            Dim frmon As Integer = Month(FromDate)
            Dim tomon As Integer = Month(ToDate)
                Dim Period As String = Nothing
            If (frmon = tomon) Then
                If (frmon = 1) Then
                    Period = "January"
                ElseIf (frmon = 2) Then
                    Period = "February"
                ElseIf (frmon = 3) Then
                    Period = "March"
                ElseIf (frmon = 4) Then
                    Period = "April"
                ElseIf (frmon = 5) Then
                    Period = "May"
                ElseIf (frmon = 6) Then
                    Period = "June"
                ElseIf (frmon = 7) Then
                    Period = "July"
                ElseIf (frmon = 8) Then
                    Period = "August"
                ElseIf (frmon = 9) Then
                    Period = "September"
                ElseIf (frmon = 10) Then
                    Period = "October"
                ElseIf (frmon = 11) Then
                    Period = "November"
                ElseIf (frmon = 12) Then
                    Period = "December"
                End If
            Else
                Period = FromDate.ToString("dd MMM yyyy") + "-" + ToDate.ToString("dd MMM yyyy")
            End If
            Dim id As String = CStr(BranchID)
            Dim Expid As String = CStr(ExpenseID)
            If ((id <> "") And (Expid <> "")) Then
                strwhere = " and B.Branch_Id = " + BranchID + " and B.GL_id = " + Expid
            ElseIf ((id <> "") And (Expid = "")) Then

                strwhere = " and B.Branch_Id = " + BranchID

            ElseIf ((id = "") And (Expid <> "")) Then
                strwhere = " and B.GL_id = " + Expid
            ElseIf ((id = "") And (Expid = "")) Then
                strwhere = ""
            End If
            Dim DateFrom As String
                DateFrom = FromDate.ToString("yyyy-MM-dd")

                'If ToDate = TDay Then
                '    ToDate = DateAdd(DateInterval.Day, -1, ToDate)
                'End If

                Dim DateTo As String
                DateTo = ToDate.ToString("yyyy-MM-dd")

            strwhere += " And cast(B.Submitted_dt As Date) between '" & DateFrom & "' and '" & DateTo & "'" + "and"
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
                cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RH.Heading(Session("FirmName"), tb, "CH Expense Report", 100)
            tb.Attributes.Add("width", "100%")

                Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.WhiteSmoke

                Dim RowBG As Integer = 0
                Dim DR As DataRow
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell

                TRHead_00.BorderWidth = "1"
                TRHead_01.BorderWidth = "1"
                TRHead_02.BorderWidth = "1"
                TRHead_03.BorderWidth = "1"
                TRHead_04.BorderWidth = "1"
                TRHead_05.BorderWidth = "1"


                TRHead_00.BorderColor = Drawing.Color.Silver
                TRHead_01.BorderColor = Drawing.Color.Silver
                TRHead_02.BorderColor = Drawing.Color.Silver
                TRHead_03.BorderColor = Drawing.Color.Silver
                TRHead_04.BorderColor = Drawing.Color.Silver
                TRHead_05.BorderColor = Drawing.Color.Silver


                TRHead_00.BorderStyle = BorderStyle.Solid
                TRHead_01.BorderStyle = BorderStyle.Solid
                TRHead_02.BorderStyle = BorderStyle.Solid
                TRHead_03.BorderStyle = BorderStyle.Solid
                TRHead_04.BorderStyle = BorderStyle.Solid
                TRHead_05.BorderStyle = BorderStyle.Solid

                TRHead_00.Font.Bold = True
                TRHead_01.Font.Bold = True
                TRHead_02.Font.Bold = True
                TRHead_03.Font.Bold = True
                TRHead_04.Font.Bold = True
                TRHead_05.Font.Bold = True


                RH.AddColumn(TRHead, TRHead_00, 4, 4, "c", "Sl.No")
                RH.AddColumn(TRHead, TRHead_01, 20, 20, "c", "GL Name")
                RH.AddColumn(TRHead, TRHead_02, 4, 4, "c", "Carry Forward")
                RH.AddColumn(TRHead, TRHead_03, 4, 4, "c", "Budget Amount")
                RH.AddColumn(TRHead, TRHead_04, 8, 8, "c", "Request Amount")
                RH.AddColumn(TRHead, TRHead_05, 14, 14, "c", "Month/Year")



                tb.Controls.Add(TRHead)

                RH.BlankRow(tb, 3)
                Dim i As Integer


                DT = DB.ExecuteDataSet("select distinct c.GL_Name,c.Carry_frwd,D.Budg_Amt,sum(B.Req_Amt) As ReqAmnt " +
                                   " From Budg_Req_Master B inner join Budg_GL_Master c on C.GL_id = B.GL_id inner join Budget_Master  " +
                                  "  D on D.Budg_id=B.Budg_id where  D.Status_id=1 " + strwhere + " B.Action_id=1 and B.Finish_id=1  " +
                                   " Group by c.GL_Name,c.Carry_frwd,D.Budg_Amt").Tables(0)

                DTEXCEL = DB.ExecuteDataSet("select distinct c.GL_Name As [GL Name],c.Carry_frwd As [Carry Forward Y/N],D.Budg_Amt As [Budget],sum(B.Req_Amt) As [Request Amount] " +
                                   " From Budg_Req_Master B inner join Budg_GL_Master c on C.GL_id = B.GL_id inner join Budget_Master  " +
                                  "  D on D.Budg_id=B.Budg_id where  D.Status_id=1 " + strwhere + " B.Action_id=1 and B.Finish_id=1  " +
                                   " Group by c.GL_Name,c.Carry_frwd,D.Budg_Amt").Tables(0)
                '  DTEXCEL = DB.ExecuteDataSet("SELECT a.Question AS [Compliance Points],a.Question_No AS [Compliance No],case when a.Critical_Flag=0 then 'N'else 'Y' end as [Critical Y/N] ,e.branch_name AS [Branch Name],b.Reason As [Remarks] from Comp_Question_Master a inner join Comp_Question_Dtl b on a.Question_ID=b.Question_ID inner join branch_master e on b.branch_ID=e.Branch_ID inner join Comp_Master d on d.branch_id=b.branch_id " + strwhere + "  b.Comp_Status=3 And d.Level_id=3 And d.Action_id=1 ORDER BY b.BRANCH_ID").Tables(0)

                If ViewType = 1 Then
                    Export_Excel_Click()
                End If

                Dim createddate As String = ""

                For Each DR In DT.Rows
                    i += 1

                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid

                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"

                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver

                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid

                    Dim status As String = ""


                    If (DR(1).ToString() = 0) Then

                        status = "N"
                    Else
                        status = "Y"


                    End If
                Dim dat As String = ""
                dat = CStr(Date.Today.Year)
                RH.AddColumn(TR3, TR3_00, 4, 4, "c", i)
                    RH.AddColumn(TR3, TR3_01, 20, 20, "l", DR(0).ToString())
                    RH.AddColumn(TR3, TR3_02, 4, 4, "l", status)
                    RH.AddColumn(TR3, TR3_03, 4, 4, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_04, 8, 8, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_05, 14, 14, "l", Period + "-" + dat)



                tb.Controls.Add(TR3)

                Next
                RH.BlankRow(tb, 20)
                pnDisplay.Controls.Add(tb)
            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub

        Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

        End Sub

        Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

        End Sub

        Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
            Try
                Response.ContentType = "application/pdf"
                Response.AddHeader("content-disposition", "attachment;filename=ACReasonsReport.pdf")
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Dim sw As New StringWriter()
                Dim hw As New HtmlTextWriter(sw)
                pnDisplay.RenderControl(hw)

                Dim sr As New StringReader(sw.ToString())
                Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
                Dim htmlparser As New HTMLWorker(pdfDoc)
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
                pdfDoc.Open()
                htmlparser.Parse(sr)
                pdfDoc.Close()
                Response.Write(pdfDoc)
                Response.[End]()
            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End Sub
        Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
            Try
                Dim HeaderText As String
                'Dim DT As New DataTable
                HeaderText = "BM Expense Report"

                WebTools.ExporttoExcel(DTEXCEL, HeaderText)
            Catch ex As Exception
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End Try
        End Sub
        Protected Sub Export_Excel_Click()
            Try
                Dim HeaderText As String
                HeaderText = "BM Expense Report"

                WebTools.ExporttoExcel(DTEXCEL, HeaderText)
            Catch ex As Exception
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End Try
        End Sub
    End Class

