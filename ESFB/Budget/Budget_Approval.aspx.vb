﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Budget_Budget_Approval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim DT5 As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim QS1 As String
    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1445) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            'If GF.FormAccess(CInt(Session("UserID")), 1440) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Dim UserID As String = CStr(Session("UserID"))
            Dim BranchID As Integer = CInt(Session("BranchID"))
            Dim Clusterid As Integer = 0
            Dim BM As String = ""
            Dim CH As String = ""
            txtmnth.Text = CStr(Date.Today.ToString("MMM")) + "-" + CStr(Date.Today.Year)
            DT4 = DB.ExecuteDataSet("Select Branch_Head,Cluster_id FROM Branch_Master where Branch_Head=" + UserID.ToString()).Tables(0)
            If (DT4.Rows.Count > 0) Then
                BM = DT4.Rows(0)(0).ToString()
            End If
            DT5 = DB.ExecuteDataSet("Select Cluster_id,Cluster_Head_id from Cluster_Master where Cluster_Head_id=" + UserID.ToString()).Tables(0)
            If (DT5.Rows.Count > 0) Then
                Clusterid = CInt(DT5.Rows(0)(0).ToString())
                CH = DT5.Rows(0)(1).ToString()
            End If
            Me.Master.subtitle = "Expense Approval Form"
            If Not IsPostBack Then

                If (UserID = BM) Then

                    DT3 = DB.ExecuteDataSet("select distinct b.BRANCH_ID,b.BRANCH_NAME,b.cluster_id FROM Budg_Req_Master c inner join BRANCH_MASTER b on b.branch_id=c.branch_id where c.Finish_id=0 and c.Branch_id=" + BranchID.ToString()).Tables(0)
                    GF.ComboFill(cmbbranch, DT3, 0, 1)
                    DT2 = DB.ExecuteDataSet("select distinct cluster_id,cluster_name FROM cluster_master where cluster_id=" + DT3.Rows(0)(2).ToString()).Tables(0)
                    GF.ComboFill(cmbcluster, DT2, 0, 1)

                    Dim Params(2) As SqlParameter

                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                    Params(1).Value = cmbbranch.SelectedValue
                    Params(2) = New SqlParameter("@ClustID", SqlDbType.Int)
                    Params(2).Value = cmbcluster.SelectedValue

                    DT1 = DB.ExecuteDataSet("Sp_Get_Budg_Appr_Balance", Params).Tables(0)
                    If (DT1.Rows.Count > 0) Then

                        For n As Integer = 0 To DT1.Rows.Count - 1
                            QS1 += DT1.Rows(n)(0).ToString() + "Ø" + DT1.Rows(n)(1).ToString() + "Ø" + DT1.Rows(n)(2).ToString() + "Ø" + DT1.Rows(n)(3).ToString() + "Ø" + DT1.Rows(n)(4).ToString() +
                                    "Ø" + DT1.Rows(n)(5).ToString() + "Ø" + DT1.Rows(n)(6).ToString() + "Ø" + DT1.Rows(n)(7).ToString() + "Ø" + DT1.Rows(n)(8).ToString() + "Ø" + DT1.Rows(n)(9).ToString() +
                                    "Ø" + DT1.Rows(n)(10).ToString() + "Ø" + DT1.Rows(n)(11).ToString() + "Ø" + DT1.Rows(n)(12).ToString() + "Ø" + DT1.Rows(n)(13).ToString() + "Ø" + DT1.Rows(n)(14).ToString() + "Ø" + DT1.Rows(n)(15).ToString() + "Ñ"
                        Next
                        Me.hid_dtls.Value = QS1

                    End If

                ElseIf (UserID = CH) Then

                    DT2 = DB.ExecuteDataSet("SELECT distinct cluster_id,cluster_name FROM cluster_master where cluster_id=" + Clusterid.ToString()).Tables(0)
                    GF.ComboFill(cmbcluster, DT2, 0, 1)

                    DT3 = DB.ExecuteDataSet("SELECT 0,'--Select--' branch_name UNION ALL select -1,'--All--' UNION ALL select distinct b.BRANCH_ID,b.BRANCH_NAME FROM Budg_Req_Master c left join BRANCH_MASTER b on b.branch_id=c.branch_id where c.Nooflevels >=2 and c.Finish_id=0 and b.cluster_id=" + Clusterid.ToString()).Tables(0)
                    GF.ComboFill(cmbbranch, DT3, 0, 1)

                    cmbbranch.Attributes.Add("onchange", "BranchOnChange()")

                Else
                    DT2 = DB.ExecuteDataSet("SELECT 0,'--Select--' cluster_name UNION ALL select -1,'--All--' UNION ALL select cluster_id,cluster_name FROM cluster_master").Tables(0)
                    GF.ComboFill(cmbcluster, DT2, 0, 1)

                    cmbcluster.Attributes.Add("onchange", "clusterOnChange()")
                    cmbbranch.Attributes.Add("onchange", "BranchOnChange()")

                End If

            End If



            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT3.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))

        If CInt(Data(0)) = 1 Then

            Dim RequestID As Integer = CInt(Data(0))
            Dim dataval As String = CStr(Data(1).ToString())
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim BranchID As Integer = CInt(Data(2).ToString())


            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@userID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(1).Value = BranchID

                Params(2) = New SqlParameter("@action", SqlDbType.VarChar)
                Params(2).Value = dataval
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@ClustID", SqlDbType.Int)
                Params(5).Value = cmbcluster.SelectedValue

                DB.ExecuteNonQuery("SP_Budg_APP_APPROVAL", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString
        End If
        If CInt(Data(0)) = 2 Then

            DT3 = DB.ExecuteDataSet("SELECT 0,'--Select--' branch_name UNION ALL select -1,'--All--' UNION ALL select distinct b.BRANCH_ID,b.BRANCH_NAME FROM Budg_Req_Master c left join BRANCH_MASTER b on b.branch_id=c.branch_id where c.Nooflevels >=3 and c.Finish_id=0 and b.Cluster_id=" + Data(1).ToString()).Tables(0)
            If (DT3.Rows.Count > 0) Then
                    Dim StrVal1 As String = ""

                    For n As Integer = 0 To DT3.Rows.Count - 1
                        StrVal1 += DT3.Rows(n)(0).ToString & "ÿ" & DT3.Rows(n)(1).ToString
                        If n < DT3.Rows.Count - 1 Then
                            StrVal1 += "Ñ"
                        End If
                    Next

                    Dim StrVal2 As String = ""
                    CallBackReturn = StrVal1

                Else
                    CallBackReturn = "ØØ"
                End If


        End If
            If CInt(Data(0)) = 3 Then


            Dim UserID As String = CStr(Session("UserID"))
            Dim BranchID As Integer = CInt(Data(1).ToString())
            Dim Params(2) As SqlParameter

            Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(1).Value = BranchID
            Params(2) = New SqlParameter("@ClustID", SqlDbType.Int)
            Params(2).Value = cmbcluster.SelectedValue
            DT1 = DB.ExecuteDataSet("Sp_Get_Budg_Appr_Balance", Params).Tables(0)
            If (DT1.Rows.Count > 0) Then

                For n As Integer = 0 To DT1.Rows.Count - 1
                    QS1 += DT1.Rows(n)(0).ToString() + "Ø" + DT1.Rows(n)(1).ToString() + "Ø" + DT1.Rows(n)(2).ToString() + "Ø" + DT1.Rows(n)(3).ToString() + "Ø" + DT1.Rows(n)(4).ToString() +
                    "Ø" + DT1.Rows(n)(5).ToString() + "Ø" + DT1.Rows(n)(6).ToString() + "Ø" + DT1.Rows(n)(7).ToString() + "Ø" + DT1.Rows(n)(8).ToString() + "Ø" + DT1.Rows(n)(9).ToString() +
                    "Ø" + DT1.Rows(n)(10).ToString() + "Ø" + DT1.Rows(n)(11).ToString() + "Ø" + DT1.Rows(n)(12).ToString() + "Ø" + DT1.Rows(n)(13).ToString() + "Ø" + DT1.Rows(n)(14).ToString() + "Ø" + DT1.Rows(n)(15).ToString() + "Ñ"
                Next
                CallBackReturn = QS1
            End If
        End If
    End Sub
#End Region

End Class
