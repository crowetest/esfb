﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Budget_Approval.aspx.vb" Inherits="Budget_Budget_Approval" %>
   <%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>


<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script src="../Script/jquery.min.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);          
        }        
        .Button:hover
        {           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);            
            color:#801424;
        }                   
     .bg
     {
         background-color:#FFF;
     }
     .style1
        {
            width: 40%;
            height: 107px;
        }           
        .auto-style1 {
            width: 29%;
        }
        .auto-style2 {
            width: 23%;
        }
        .auto-style6 {
            width: 122px;
        }
    </style>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/jquery.js" type="text/javascript"></script>
  
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

     function SelectUnSelectAll()
        {
        
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");
            var i =1 ;       
            for (n = 0; n < row.length; n++) {                   
                 col = row[n].split("Ø");
                   
                if (document.getElementById("MainChk").checked == true)
                {
                    document.getElementById("chk" + i.toString()).checked = true;
                   
                  
                  document.getElementById("cmb" + col[0]).value=1;
                 }
                else 
                {
                    document.getElementById("chk" + i.toString()).checked = false;
                   document.getElementById("cmb" + col[0]).value=-1;
                }
                    
                i+=1;
                
            }
        }
        function SingleToggle(i)
        {
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");
            var i =1 ;       
            for (n = 0; n < row.length; n++) {                   
                 col = row[n].split("Ø");
                   
                if (document.getElementById("chk" + i.toString()).checked == false)
                {
                document.getElementById("MainChk").checked = false;
                   
                  
                  document.getElementById("cmb" + col[0]).value=-1;
                 }
               
                    
                i+=1;
                
            }
             
                
            
        }
        function  table_fill() {
            document.getElementById("<%= pnbudgetApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
             tab += "<td style='width:3%;text-align:center;font-size:11pt;' onclick=SelectUnSelectAll() ><input id='MainChk' type='checkbox' /></td>";
            tab += "<td style='width:1%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:5%;text-align:left' >Branch Name</td>";
            tab += "<td style='width:2%;text-align:left'>Branch Category</td>";
            tab += "<td style='width:3%;text-align:left' >Branch Type</td>";
            tab += "<td style='width:7%;text-align:left' >Expense Head</td>";
           // tab += "<td style='width:2%;text-align:left'>Amount Carry Forward</td>";
            tab += "<td style='width:2%;text-align:left' >Balance Allocated/Year</td>";
            tab += "<td style='width:2%;text-align:left'>Budget Allocated/Month</td>";
            tab += "<td style='width:2%;text-align:left'>Actual Bill Amount</td>";
            tab += "<td style='width:2%;text-align:left'>Amount Exceeded (Yes/No)</td>";
            //tab += "<td style='width:2%;text-align:left'>% exceeded</td>";
            tab += "<td style='width:5%;text-align:left'>Request Date</td>";
            tab += "<td style='width:8%;text-align:left'>Reason</td>";
            tab += "<td style='width:2%;text-align:left'>Attachment</td>";
            tab += "<td style='width:5%;text-align:center'>Action</td>";
            tab += "<td style='width:10%;text-align:center'>Remarks</td>";
            tab += "<td style='width:5%;text-align:center'>Next Approver</td>";
            tab += "<td style='width:5%;text-align:center'>Final Approver</td>";
           
            tab += "</tr>";
              var i =  1;
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
               
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");
                
                for (n = 0; n < row.length-1; n++) {
                    col = row[n].split("Ø");
                   
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }

                
                    var exceeded = "";
                    var type = "";
                   
                    if (col[12]>1) {
                        exceeded = 'Yes';
                    }
                    else {
                        exceeded = 'No';
                    }
                    var FinalApprover = "";
                    if (col[12] == 1)
                    {
                        FinalApprover = 'Branch Manager';
                    }
                    else if ((col[12] == 2)) {
                        FinalApprover = 'Cluster Head';
                    }
                    else if (col[12] == 3)
                    {
                        FinalApprover = 'HOD';
                    }
                    else
                    {
                        FinalApprover = 'EVP';
                    }
                    var NextApprover = "";
                    if((col[13]==0)&&(col[14]==0)&&(col[12]==1))
                    {
                        NextApprover='Branch Manager';
                    }
                   else if((col[13]==0)&&(col[14]==0)&&(col[12]>1))
                    {
                        NextApprover = 'Cluster Head';
                    }
                    else if ((col[13] == 1) && (col[14] == 1) && (col[12] >2))
                    {
                        NextApprover = 'HOD';
                    }
                    else if ((col[13] == 2) && (col[14] == 1) && (col[12] > 3))

                    {
                        NextApprover='EVP'
                    }
                    else if ((col[13] == 3) && (col[14] == 1) && (col[12] == 4)) {
                        NextApprover = 'EVP'
                    }
                    var yearly = col[7] * 12;
                    var yearlybalance=col[15]-yearly;
                    if (col[3] == 1) {
                        type = 'Without MB';
                    }
                    else {
                        type = 'With MB';
                    }

                    var date = col[9];
                    date = date.split(' ')[0];
                     var txtid = "chk" + i.toString();
                    tab += "<td style='width:3%;text-align:center' onclick=SingleToggle("+i+") ><input id='"+ txtid +"' type='checkbox' /></td>";
                    tab += "<td style='width:1%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:5%;text-align:left' >" + col[1] + "</td>";
                    tab += "<td style='width:2%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:3%;text-align:left' >" + type + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[5] + "</td>";
                   // tab += "<td style='width:2%;text-align:left'>" + carryfrwd   + "</td>";
                    tab += "<td style='width:2%;text-align:left'>" + yearlybalance + "</td>";
                    tab += "<td style='width:2%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:2%;text-align:left'>" + col[8] + " </td>";
                    tab += "<td style='width:2%;text-align:left'>" + exceeded + " </td>";
                    tab += "<td style='width:5%;text-align:left'>" + date + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[10] + "</td>";
                    if (col[11] != "") {
                        tab += "<td style='width:2%;text-align:left'><img id='ViewReport' src='../Image/attchment2.png' onclick='viewReport(" + col[11] + ",1)' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    }
                    else {
                        tab += "<td style='width:2%;text-align:left'></td>";
                    }
                    var select = "<select id='cmb" + col[0] + "' class='NormalText' name='cmb" + col[0] + "' >";
                    select += "<option value='-1'>Select</option>";
                    select += "<option value='1'>Approved</option>";
                    select += "<option value='2'>Rejected</option>";
                    select += "<option value='3'>Returned</option>";

                    tab += "<td style='width:5%;text-align:left'>" + select + "</td>";



                    var txtBox = "<textarea id='txtRemarks" + col[0] + "' name='txtRemarks" + col[0] + "' style='width:80%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)' ></textarea>";

                    tab += "<td style='width:10%;text-align:left'>" + txtBox + "</td>";

                    tab += "<td style='width:5%;text-align:left'>" + NextApprover + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + FinalApprover + "</td>";
                    tab += "</tr>";
                    i+=1;
                }
                tab += "</table></div></div></div>";
                document.getElementById("<%= pnbudgetApproveDtl.ClientID %>").innerHTML = tab;

                //--------------------- Clearing Data ------------------------//
            }

        }
        function viewReport(ID,Rpt) {
           
            window.open("Reports/ShowAttachment.aspx?RequestID=" + btoa(ID) +"&RptID ="+Rpt);
         
            return false;
        }
        function UpdateValue() {
           
        document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("No pending Approval");
                return false;
            }
            var i=1;
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");
            for (n = 0; n < row.length - 1; n++) {
                col = row[n].split("Ø");
              
              var txtid = "chk" + i.toString();
   
                 if (document.getElementById(txtid).checked == false ){
                 if( document.getElementById("cmb" + col[0]).value ==-1)
                 {
               
               alert("Please Select an action");
               return false;
               
               }
        
                    if (document.getElementById("txtRemarks" + col[0]).value == ""  ) {
                        alert("Enter Reason");
                        document.getElementById("txtRemarks" + col[0]).focus();
                        return false;
                    }

       
     }
                    document.getElementById("<%= hid_temp.ClientID %>").value += col[0] + "µ" + document.getElementById("cmb" + col[0]).value + "µ" + document.getElementById("txtRemarks" + col[0]).value + "¥";
                 
              
              
                i+=1;
            }
          
            return true;
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");

            for (a = 0; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
            if (context == 2) {
               
                var Data = arg;
                //alert(Data);
                 ComboFill(Data, "<%= cmbbranch.ClientID%>");
            
            }
            if (context == 3) {
               
                var Data = arg;
                var Count1 = Data.length - 1;
                if (Count1 > 0) {
                    document.getElementById("<%= hid_dtls.ClientID %>").value = Data;
                  
                    table_fill();
                }
            }
        }
        function btnApprove_onclick() {
            //debugger;
            var ret = UpdateValue();
           
          
            if (ret == 0) return false;
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                alert("Select Any Expense for Approval");
                return false;
            }

            var straction = document.getElementById("<%= hid_temp.ClientID %>").value;
            var branch_id=document.getElementById("<%=cmbbranch.ClientID %>").value;
            var Data = "1Ø" + straction + "Ø" + branch_id;
          
            ToServer(Data, 1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
         function clusterOnChange() {
     
           
             var ClusterID = document.getElementById("<%= cmbcluster.ClientID %>").value;
             
             var ToData = "2Ø" + ClusterID;
                ToServer(ToData, 2);
         }

        function BranchOnChange() {
     
           
                var branchid = document.getElementById("<%= cmbbranch.ClientID %>").value;
            var ToData = "3Ø" + branchid;
                ToServer(ToData, 3);
            }
    </script>
</head>
</html>
<br />
<div  style="width:100%; height:auto; margin:0px auto; background-color:#EEB8A6;">
    <br />
    <br />
    <div id = "divSection1" class = "sec1" style="width:97%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
     
       <table class="style1" style="width: 100%; margin: 0px auto; align-content:center">
           <tr>
               <td></td>
            <td class="auto-style6">
                   <asp:HiddenField ID="hid_temp" runat="server" />
            </td>   
                <td class="auto-style1">
                   <asp:HiddenField ID="hid_dtls" runat="server" />
            </td>
            <td></td>
               </tr>  
      
        <tr id="App1">
            <td></td>
            <td style="text-align: center;" class="auto-style2">
                Cluster</td>
            <td>
          <asp:DropDownList ID="cmbcluster"  Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black" Height="20px">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
          </td>
           
           <td></td>
              </tr>
             <tr id="App">
            <td></td>
            <td style="text-align: center;" class="auto-style2">
                &nbsp;Branch Name</td>
        
            <td class="auto-style1">
                <asp:DropDownList ID="cmbbranch"  Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black" Height="20px">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
           <td></td>
           
        </tr>
      
            <tr id="App1">
                <td></td>
            <td style="text-align: center;" class="auto-style2">
                Month/Year</td>
            <td>
          <asp:TextBox ID="txtmnth" class="ReadOnlyTextBox" runat="server" Width="50%" ReadOnly="true" Height="20px"></asp:TextBox>
          </td>
                <td></td>
      
           
         
        </tr>
            </table>
     <br />
        <br />
     <table ID="tblSec1" style="width:100%;height:90px;margin:0px auto; align-content:center">            
            <tr id="Tr1">
                <td style="text-align: center;">
                    <asp:Panel ID="pnbudgetApproveDtl" Style="width: 100%;  text-align: left; float: left;" runat="server">               

                    </asp:Panel>
                </td>
            </tr>
          <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
        </table> 
        <br />
        <br />
         
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
         
     
      
        <br />
        <br />  
    </div>
   <br />
    <br />
   
</div>   
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>



