﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="ADBranchRequestClose.aspx.vb" Inherits="ADBranchRequestClose" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <style type="text/css">
    #Button
    {
        width:100%;
        height:40px;
        font-weight:bold;
        line-height:40px;
        text-align:center;
        border-top-left-radius: 25px;
	    border-top-right-radius: 25px;
	    border-bottom-left-radius: 25px;
	    border-bottom-right-radius: 25px;
        cursor:pointer;
        background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
        color:#E0E0E0;
    }
        
    #Button:hover
    {
        background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        color:#036;
    }        
</style>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function table_fill() {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
           
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr style='background-color:#efe1ef;'>";
            tab += "<td style='width:1%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:5%;text-align:left' >Application</td>";
            tab += "<td style='width:5%;text-align:left' >Branch</td>";
            tab += "<td style='width:3%;text-align:left' >From</td>";
            tab += "<td style='width:3%;text-align:left'>Type</td>";
            tab += "<td style='width:5%;text-align:center'>Role</td>";
            tab += "<td style='width:5%;text-align:left'>Request No</td>";
            tab += "<td style='width:8%;text-align:left'>Emp_Name</td>";
            tab += "<td style='width:4%;text-align:left'>Dep_Name</td>";
            tab += "<td style='width:4%;text-align:left'>Desg_Name</td>";
            tab += "<td style='width:4%;text-align:left'>DL</td>";
            tab += "<td style='width:4%;text-align:left'>Email</td>";
            tab += "<td style='width:4%;text-align:left'>Mobile</td>";
            tab += "<td style='width:4%;text-align:left'>Approved By</td>";
            tab += "<td style='width:6%;text-align:center'>Password</td>";
            tab += "<td style='width:9%;text-align:center'>Requested Email</td>";
            tab += "<td style='width:12%;text-align:center'>Remarks</td>";
            tab += "<td style='width:4%;text-align:center'>Approve</td>";
            tab += "<td style='width:4%;text-align:center'>Reject</td>";
            tab += "<td style='width:6%;text-align:center'>User Remarks</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr style='background-color:#FBEFFB;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr style='background-color:#FFF;'>";
                    }
                    i = n + 1;
                    //c.app_request_id,d.app_dtl_id,j.app_name,h.branch_name,d.emp_code, f.Emp_Name,e.App_Type_name,d.Remarks,g.App_role_name,i.branch_name as To_branch
                    tab += "<td style='width:1%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:5%;text-align:left' >" + col[2] + "</td>";
                    tab += "<td style='width:5%;text-align:left' >" + col[3] + "</td>";
                    tab += "<td style='width:3%;text-align:left' >" + col[25] + "</td>";
                    tab += "<td style='width:3%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[8] + "</td>";
                    tab += "<td style='width:5%;text-align:left'><a href='Reports/ViewRequestDtl.aspx?App_Request_ID=" + col[30] + "' target='_blank'>" + col[23] + "</a></td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[5]+" - " +col[4]  + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[12] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[13] + "</td>";
                    
                    tab += "<td style='width:4%;text-align:left'>" + col[31] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[10] + "</td>";
                   
                    
                    tab += "<td style='width:4%;text-align:left'>" + col[11] + "</td>";
                  
                    tab += "<td style='width:4%;text-align:left'>" + col[22] + "</td>";
                    
                    if (col[24] != "")
                        var txtBox1 = "<textarea id='txtpswd" + col[0] + "' name='txtpswd" + col[0] + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)' >" + col[24] + "</textarea>";
                    else
                        var txtBox1 = "<textarea id='txtpswd" + col[0] + "' name='txtpswd" + col[0] + "' style='width:99%; float:left;' disabled='true' maxlength='300' onkeypress='return TextAreaCheck(event)' ></textarea>";;
                    tab += "<td style='width:6%;text-align:left'>" + txtBox1 + "</td>";

                    tab += "<td style='width:9%;text-align:left'>" + col[20] + "</td>";

                    var txtBox = "<textarea id='txtRemarks" + col[0] + "' name='txtRemarks" + col[0] + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)' ></textarea>";

                    tab += "<td style='width:12%;text-align:left'>" + txtBox + "</td>";

                    //var txtid = "chk" + col[0];
                    //tab += "<td style='width:5%;text-align:center'><input id='" + txtid + "' type='checkbox' /></td>";
                    var LinkText = "Closed";
                    tab += "<td style='width:4%; text-align:center; padding-left:5px; ' onclick=Startval(1,'" + col[0] + "','" + col[11] + "','" + col[23] + "','" + col[24] + "','" + LinkText + "','" + col[1] + "')><div id='Button' >" + LinkText + "</div></td>";
                    var LinkText = "Reject";
                    tab += "<td style='width:4%; text-align:center; padding-left:5px; ' onclick=Startval(2,'" + col[0] + "','" + col[11] + "','" + col[23] + "','" + col[24] + "','" + LinkText + "','" + col[1] + "')><div id='Button' >" + LinkText + "</div></td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[29] + "</td>";
                    tab += "</tr>";
                }
            }
           
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//


        }
        function Startval(status, REQ_ID, mobile, Service_Request_No, Passwd, LinkText, app_id) {
          
            if (status == 2 && document.getElementById("txtRemarks" + REQ_ID).value == "") {
                alert("Enter The Reason For Reject");
                document.getElementById("txtRemarks" + REQ_ID).focus();
            }
            else {
                
                var Remarks = document.getElementById("txtRemarks" + REQ_ID).value;
                Passwd = document.getElementById("txtpswd" + REQ_ID).value;
                
                if (confirm("Are you sure to Close This " + Service_Request_No.toString() + " ?") == 1) {
                    var ToData = "1Ø" + mobile + "Ø" + "¥" + REQ_ID + "µ" + Passwd + "µ" + Remarks + "µ" + status + "Ø" + Passwd + "Ø" + status + "Ø" + document.getElementById("txtRemarks" + REQ_ID).value + "Ø" + app_id;

                    ToServer(ToData, 1);
                }
            }
           
        }
        function window_onload() {
            table_fill();
            AutoRefresh(160000);

        }
        function AutoRefresh(t) {
            setTimeout("location.reload(true);", t);
        }
        function FromServer(arg, context) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("ADBranchRequestClose.aspx", "_self");
        }
        
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />
                <br />
                &nbsp;
                &nbsp;
                </td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

