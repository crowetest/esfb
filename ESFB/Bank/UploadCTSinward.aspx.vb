﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Partial Class UploadCTSinward
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim DTExcel As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim values As New DataTable
#Region "Page Load & Dispose"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "file Uploading"
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        cmbType.Attributes.Add("onclick", "return TypeOnChange()")



    End Sub


    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
    Sub DeleteFilesFromFolder(Folder As String)
        If Directory.Exists(Folder) Then
            For Each _file As String In Directory.GetFiles(Folder)
                File.Delete(_file)
            Next
            For Each _folder As String In Directory.GetDirectories(Folder)

                DeleteFilesFromFolder(_folder)
            Next

        End If

    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

       



    End Sub
#End Region
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            If CDbl(cmbType.Text) = 1 Then
                HeaderText = "CTS Inward Report"
            ElseIf CDbl(cmbType.Text) = 2 Then
                HeaderText = "CTS Outward Report"
            ElseIf CDbl(cmbType.Text) = 3 Then
                HeaderText = "CTS Inward Return Report"
            ElseIf CDbl(cmbType.Text) = 4 Then
                HeaderText = "CTS Outward Return Report"
            End If

            DTExcel = DirectCast(ViewState("myDataSet"), DataTable)
            'Dim displayView = New DataView(DTExcel, "", "Zone_Id,region_id,area_id,branch_id,group_id", DataViewRowState.OriginalRows)
            'DTExcel = displayView.ToTable(False, "Territory", "Region", "Area", "Branch", "Audit", "PendingDays", "Total Queries", "Pending Queries", "BM", "AM", "RM", "TM", "GM", "Confirmation", "Auditor")
            'Dim HeaderText As String
            'HeaderText = "Monitoring Report"
            'WebTools.ExporttoExcel(DTExcel, HeaderText)

            WebTools.ExporttoExcel(DTExcel, HeaderText)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim msg As String = ""
        Dim connectionString As String = ""

        If (FileUpload1.PostedFile.FileName <> "") Then

            Dim fileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)



            Dim fileExtension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)
            Dim fileType As String = Path.GetExtension(FileUpload1.PostedFile.ContentType)
            Dim fileSize As String = ""
            Dim fileCreatedDate As String = ""

            'Dim filesize As Integer = Path.GetExtension(FileUpload1.PostedFile.ContentLength)
            Dim fileLocation As String = Server.MapPath("../" & fileName)

            DeleteFilesFromFolder(fileLocation)
            FileUpload1.SaveAs(fileLocation)

            Dim infoReader As System.IO.FileInfo
            infoReader = My.Computer.FileSystem.GetFileInfo(fileLocation)
            'MsgBox("File was created on " & infoReader.CreationTime)

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            'con.Open()

            Dim dt As New DataTable()
            Dim col As New DataColumn("test")
            col.DataType = System.Type.[GetType]("System.String")
            dt.Columns.Add(col)

            Dim aa As String() = File.ReadAllLines(fileLocation)
            For Each item As String In aa
                Dim dr As DataRow = dt.NewRow()
                dr(0) = item.ToString()
                dt.Rows.Add(dr)
            Next
            Dim StrCAFDetails As String = ""

            Dim count As Integer
            Dim val As Integer
            If CDbl(cmbType.Text) = 1 Then 'inward


                values.Columns.Add("Batch", GetType(String), Nothing)  '0
                values.Columns.Add("Cost Center", GetType(String), Nothing) '1
                values.Columns.Add("Product", GetType(String), Nothing) '2   default 'B'
                values.Columns.Add("Branch", GetType(String), Nothing) '3
                values.Columns.Add("AcNo", GetType(String), Nothing) '4
                values.Columns.Add("Customer", GetType(String), Nothing) '5
                values.Columns.Add("ChequeNo", GetType(String), Nothing) '6
                values.Columns.Add("ChequeAmt", GetType(String), Nothing) '7
                values.Columns.Add("PresentingBank", GetType(String), Nothing)  '8
                values.Columns.Add("Payee", GetType(String), Nothing)  '9
                values.Columns.Add("Status", GetType(String), Nothing)  '10
                values.Columns.Add("ourstatus", GetType(String), Nothing) '11

                For count = 1 To dt.Rows.Count - 1

                    Dim row = values.NewRow()
                    If Len(dt.Rows(count)(0).ToString) >= 126 Then
                        If Len(Replace(dt.Rows(count)(0).ToString, " ", "")) > 7 Then
                            If Replace(dt.Rows(count)(0).ToString.Substring(0, 4), " ", "") <> "" And Replace(dt.Rows(count)(0).ToString.Substring(0, 4), " ", "") <> "====" And dt.Rows(count)(0).ToString.Substring(0, 4) <> "ESAF" And dt.Rows(count)(0).ToString.Substring(0, 5) <> "Batch" And dt.Rows(count)(0).ToString.Substring(0, 4) <> "From" And dt.Rows(count)(0).ToString.Substring(0, 6) <> "Inward" And dt.Rows(count)(0).ToString.Substring(0, 6) <> "     " Then

                                row(0) = dt.Rows(count)(0).ToString.Substring(0, 18)  'Batch
                                row(1) = dt.Rows(count)(0).ToString.Substring(21, 5)  'Cost Center
                                row(2) = dt.Rows(count)(0).ToString.Substring(26, 4)  'Product
                                row(3) = dt.Rows(count)(0).ToString.Substring(34, 5)  'Branch
                                row(4) = dt.Rows(count)(0).ToString.Substring(38, 15)  'AcNo
                                row(5) = dt.Rows(count)(0).ToString.Substring(53, 15)  'Customer
                                row(6) = dt.Rows(count)(0).ToString.Substring(69, 6)  'ChequeNo
                                row(7) = dt.Rows(count)(0).ToString.Substring(75, 13)  'ChequeAmt
                                row(8) = dt.Rows(count)(0).ToString.Substring(88, 18)  'PresentingBank
                                row(9) = dt.Rows(count)(0).ToString.Substring(108, 17)  'Payee
                                val = Len(dt.Rows(count)(0).ToString) - 126
                                row(10) = dt.Rows(count)(0).ToString.Substring(126, val)  'Status
                                row(11) = IIf(CDbl(row(7)) > 300000, "Exceeded", "")  'our Status



                            Else
                                row(0) = ""  'Batch
                                row(1) = ""  'Cost Center
                                row(2) = ""  'Product
                                row(3) = ""  'Branch
                                row(4) = ""  'AcNo
                                row(5) = ""  'Customer
                                row(6) = ""  'ChequeNo
                                row(7) = ""  'ChequeAmt
                                row(8) = ""  'PresentingBank
                                row(9) = ""  'Payee
                                row(10) = ""  'Status
                                row(11) = ""  'our Status
                            End If
                            If Len(row(0).ToString) > 7 And row(0).ToString <> "" Then
                                If row(0).ToString.Substring(1, 4) <> "ESAF" And row(0).ToString.Substring(1, 4) <> "From" And row(0).ToString.Substring(1, 5) <> "Inward" And row(0).ToString.Substring(1, 5) <> "     " Then
                                    values.Rows.Add(row)
                                End If
                            End If

                        End If
                    End If



                Next
                ViewState("myDataSet") = values

            ElseIf cmbType.Text = "2" Then 'outward" Then

                values.Columns.Add("Book Date", GetType(String), Nothing)  '0
                values.Columns.Add("Cost Center", GetType(String), Nothing) '1
                values.Columns.Add("Batch No", GetType(String), Nothing) '2   default 'B'
                values.Columns.Add("Branch", GetType(String), Nothing) '3
                values.Columns.Add("MICR Days", GetType(String), Nothing) '4
                values.Columns.Add("Product", GetType(String), Nothing) '5
                values.Columns.Add("AcNo", GetType(String), Nothing) '6
                values.Columns.Add("Customer", GetType(String), Nothing) '7
                values.Columns.Add("ChequeNo", GetType(String), Nothing)  '8
                values.Columns.Add("ChequeAmt", GetType(String), Nothing)  '9
                values.Columns.Add("Drawee Bank", GetType(String), Nothing)  '10
                values.Columns.Add("Status", GetType(String), Nothing)  '11
                values.Columns.Add("Ourstatus", GetType(String), Nothing) '12

                For count = 1 To dt.Rows.Count - 1

                    Dim row = values.NewRow()
                    If Len(dt.Rows(count)(0).ToString) >= 117 Then
                        If Len(Replace(dt.Rows(count)(0).ToString, " ", "")) > 7 Then
                            If Replace(dt.Rows(count)(0).ToString.Substring(0, 4), " ", "") <> "" And Replace(dt.Rows(count)(0).ToString.Substring(0, 4), " ", "") <> "====" And dt.Rows(count)(0).ToString.Substring(0, 4) <> "ESAF" And dt.Rows(count)(0).ToString.Substring(0, 4) <> "Book" And dt.Rows(count)(0).ToString.Substring(0, 4) <> "From" And dt.Rows(count)(0).ToString.Substring(0, 7) <> "Outward" And dt.Rows(count)(0).ToString.Substring(0, 6) <> "     " Then


                                row(0) = dt.Rows(count)(0).ToString.Substring(0, 10)  'Book Date
                                row(1) = dt.Rows(count)(0).ToString.Substring(12, 4)  'Cost Center
                                row(2) = dt.Rows(count)(0).ToString.Substring(16, 19)  'Batch No
                                row(3) = dt.Rows(count)(0).ToString.Substring(38, 5)  'Branch
                                row(4) = dt.Rows(count)(0).ToString.Substring(44, 1)  'MICR Days
                                row(5) = dt.Rows(count)(0).ToString.Substring(48, 4)  'Product
                                row(6) = dt.Rows(count)(0).ToString.Substring(52, 15)  'AcNo
                                row(7) = dt.Rows(count)(0).ToString.Substring(67, 28)  'Customer
                                row(8) = dt.Rows(count)(0).ToString.Substring(95, 7)  'ChequeNo
                                row(9) = dt.Rows(count)(0).ToString.Substring(103, 10)  'ChequeAmt
                                row(10) = dt.Rows(count)(0).ToString.Substring(113, 4)  'Drawee Bank
                                Val = Len(dt.Rows(count)(0).ToString) - 117
                                row(11) = dt.Rows(count)(0).ToString.Substring(117, val)  'Status

                                row(12) = IIf(CDbl(row(9)) > 1000000, "Exceeded", "")  'our Status



                            Else
                                row(0) = ""  'Batch
                                row(1) = ""  'Cost Center
                                row(2) = ""  'Product
                                row(3) = ""  'Branch
                                row(4) = ""  'AcNo
                                row(5) = ""  'Customer
                                row(6) = ""  'ChequeNo
                                row(7) = ""  'ChequeAmt
                                row(8) = ""  'PresentingBank
                                row(9) = ""  'Payee
                                row(10) = ""  'Status
                                row(11) = ""  'our Status
                            End If
                            If Len(row(0).ToString) > 7 And row(0).ToString <> "" Then
                                If row(0).ToString.Substring(1, 4) <> "ESAF" And row(0).ToString.Substring(1, 4) <> "From" And row(0).ToString.Substring(1, 5) <> "Inward" And row(0).ToString.Substring(1, 5) <> "     " Then
                                    values.Rows.Add(row)
                                End If
                            End If

                        End If
                    End If



                Next
                ViewState("myDataSet") = values


            End If
            For n As Integer = 0 To values.Rows.Count - 1
                If cmbType.Text = "1" Then 'inward

                    StrCAFDetails += values.Rows(n)(0).ToString & "µ" & values.Rows(n)(1).ToString & "µ" & values.Rows(n)(2).ToString & "µ" & values.Rows(n)(3).ToString & "µ" & values.Rows(n)(4).ToString & "µ" & values.Rows(n)(5).ToString & "µ" & values.Rows(n)(6).ToString & "µ" & values.Rows(n)(7).ToString & "µ" & values.Rows(n)(8).ToString & "µ" & values.Rows(n)(9).ToString & "µ" & values.Rows(n)(10).ToString & "µ" & values.Rows(n)(11).ToString
                    If n < dt.Rows.Count - 1 Then
                        StrCAFDetails += "¥"
                    End If

                ElseIf cmbType.Text = "2" Then  'outward
                    StrCAFDetails += values.Rows(n)(0).ToString & "µ" & values.Rows(n)(1).ToString & "µ" & values.Rows(n)(2).ToString & "µ" & values.Rows(n)(3).ToString & "µ" & values.Rows(n)(4).ToString & "µ" & values.Rows(n)(5).ToString & "µ" & values.Rows(n)(6).ToString & "µ" & values.Rows(n)(7).ToString & "µ" & values.Rows(n)(8).ToString & "µ" & values.Rows(n)(9).ToString & "µ" & values.Rows(n)(10).ToString & "µ" & values.Rows(n)(11).ToString & "µ" & values.Rows(n)(12).ToString
                    If n < dt.Rows.Count - 1 Then
                        StrCAFDetails += "¥"
                    End If
                End If



            Next
            hid_filename.Value = fileName
            hid_FileType.Value = fileType
            hid_FileSize.Value = fileSize
            hid_FileDate.Value = fileCreatedDate
            hid_dtls.Value = StrCAFDetails
            If cmbType.Text = "1" Then  'inward
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill_inward();", True)
            ElseIf cmbType.Text = "2" Then  'outward
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill_Outward();", True)
            End If


        Else
            Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_srpt1.Append("alert('Select File For uploading');reset();")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
        End If
    End Sub


End Class
