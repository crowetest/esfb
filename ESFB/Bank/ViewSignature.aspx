﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ViewSignature.aspx.vb" Inherits="ViewSignature" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
    <script src="../../Script/Calculations.js" language="javascript" type="text/javascript"> </script> 
  <script language="javascript" type="text/javascript">
      function btnExit_onclick() 
      {
          window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
      }
      
      function PostOnChange() {
          if ( document.getElementById("<%= cmbBranch.ClientID%>").value != "-1")
          {
              
              var PostID = document.getElementById("<%= cmbPost.ClientID%>").value;
              
              if (PostID != "")
                  ToServer("2Ø" + PostID + "Ø" + document.getElementById("<%= cmbBranch.ClientID%>").value, 2);
          }
          else

          {
              alert("Select Branch");
              return false;
          }
       }
      
      
      function FromServer(Arg, Context) {
         
          switch (Context) {
             
             
              case 2:
                  {
                     
                      document.getElementById("<%= hid_dtls.ClientID %>").value = Arg;
                      table_fill();
                      break;
                  }
              
              case 3:
                  {
                      
                      if (Arg == "ØØ") {
                          document.getElementById("<%= txtState.ClientID%>").value = "";
                          document.getElementById("<%= txtDistrict.ClientID%>").value = "";
                          return false;
                      }
                      var Data = Arg.split("Ø");
                      document.getElementById("<%= txtState.ClientID%>").value = Data[1];
                      document.getElementById("<%= txtDistrict.ClientID%>").value = Data[2];
                      document.getElementById("<%= cmbPost.ClientID%>").value = "-1";
                      document.getElementById("<%= hid_dtls.ClientID %>").value = "";
                      table_fill();
                      break;
                  }
          }

      }
      function BranchOnChange() {
          
          if (document.getElementById("<%= cmbBranch.ClientID%>").value != "-1") {
              
              var BranchID = document.getElementById("<%= cmbBranch.ClientID%>").value;
              document.getElementById("<%= hdn_Branch.ClientID%>").value = BranchID;
              if (BranchID != "")
                  ToServer("3Ø" + BranchID,3);
          }
      }
      
      function table_fill() {
          
          if (document.getElementById("<%= hid_dtls.ClientID %>").value == "")
          {
              document.getElementById("<%= pnHistory.ClientID %>").style.display = 'none';
          }
          else {
              document.getElementById("<%= pnHistory.ClientID %>").style.display = '';
              var row_bg = 0;
              var tab = "";
              tab += "<div style='width:100%; background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto; align=center;color:#FFF;font-family:cambria; font-size:10pt;'>HISTORY</div>"
              tab += "<div style='width:100%; height:510px;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#FFF;' align='center'>";
              tab += "<tr class=mainhead>";
              tab += "<td style='width:5%;text-align:center'>No</td>";
              tab += "<td style='width:20%;text-align:left' >Emp_code</td>";
              tab += "<td style='width:25%;text-align:left;'>Emp Name</td>";
              tab += "<td style='width:10%;text-align:left;'>Mobile</td>";
              tab += "<td style='width:20%;text-align:left'>Status</td>";
              tab += "<td style='width:20%;text-align:left'>Signature</td>";
              tab += "</tr>";
              //tranid,branch_id,branch_name,emp_code,emp_name,designation_name,status,e_signature,content_type
              row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

              for (n = 0; n <= row.length - 1; n++) {
                  
                  col = row[n].split("µ");
                  if (document.getElementById("<%= hid_Dtls.ClientID %>").value != "") {
                      if (row_bg == 0) {
                          row_bg = 1;
                          tab += "<tr height=60px; style='background-color:#FFF;'>";
                      }
                      else {
                          row_bg = 0;
                          tab += "<tr height=60px;  style='background-color:#FFF;'>";
                      }
                      i = n + 1;

                      //a.tranid, a.branch_id, c.branch_name, a.emp_code, b.designation_name
                      tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                      tab += "<td style='width:20%;text-align:left' >" + col[2] + "</td>";
                      tab += "<td style='width:25%;text-align:left'>" + col[3] + "</td>";
                      tab += "<td style='width:10%;text-align:left'>" + col[5] + "</td>";
                      tab += "<td style='width:20%;text-align:left'>" + col[6] + "</td>";
                      tab += "<td style='width:20%;text-align:center' ><img id='ViewReport' src='"+ col[7] +"' style='width:180px; height:90px;' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                      tab += "</tr>";
                  }
              }
              tab += "</table><div><div>";
              //alert(tab);
              document.getElementById("<%= pnHistory.ClientID %>").innerHTML = tab;

              //--------------------- Clearing Data ------------------------//

          }
      }

     
// ]]>
  </script><br />
    <table align="center" style="width:80%;margin: 0px auto;">
         <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Branch</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbBranch" runat="server" class="NormalText" Font-Names="Cambria" ForeColor="Black" Style="text-align: left;" Width="50%">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                State
                </td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtState" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;">
                   <asp:HiddenField ID="hid_Dtls" runat="server" />
                   </td>
            <td style="width:12% ; text-align:left;">
                District
                </td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtDistrict" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager><asp:HiddenField 
                    ID="hdn_Image" runat="server" /><asp:HiddenField 
                    ID="hdn_Branch" runat="server" />
               
                Post</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbPost" runat="server" class="NormalText" Font-Names="Cambria" ForeColor="Black" Style="text-align: left;" Width="50%">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                </td>
        </tr>
      
        <tr>
       <td style="width:100%;" colspan="3">
       <asp:Panel ID="pnHistory" runat="server">
            </asp:Panel>
       </td>
       </tr>
        <tr>
        
         <td style="text-align:center;" colspan="3"> <br /><br />  &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
        
    </table>
  <br /><br />
</asp:Content>

