﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="BranchRequest.aspx.vb" Inherits="BranchRequest" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../Style/bootstrap-3.1.1.min.css" type="text/css" />
        <link rel="stylesheet" href="../Style/bootstrap-multiselect.css" type="text/css" />
        <script type="text/javascript" src="../Script/jquery-1.8.2.js"></script>
        <script type="text/javascript" src="../Script/bootstrap-2.3.2.min.js"></script>
        <script type="text/javascript" src="../Script/bootstrap-multiselect_RISK.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#chkCrmTeam').multiselect();
                buttonWidth: '500px'
            }); 0

            $(document).ready(function () {
                $('#chkCrmRole').multiselect();
                buttonWidth: '500px'
            }); 0

            $(document).ready(function () {
                $('#chkCrmBranch').multiselect();
                buttonWidth: '500px'
            }); 0
            //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
            //<Remarks>Multiple Report Code selection added</Remarks>
            $(document).ready(function () {
                $('#chkReport').multiselect();
                buttonWidth: '500px'
            }); 0   
        </script>
        <script language="javascript" type="text/javascript">

            $(function () {
                $('#chkCrmTeam').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true
                });
                $('#btnget').click(function () {
                    alert($('#chkCrmTeam').val());
                })
            
                $('#chkCrmRole').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true
                });
                $('#btnget').click(function () {
                    alert($('#chkCrmRole').val());
                })
                
                $('#chkCrmBranch').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true
                });
                $('#btnget').click(function () {
                    alert($('#chkCrmBranch').val());
                })    
                
                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                $('#chkReport').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true
                });                    
            });
    
            function RequestOnchange() {
        
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                document.getElementById("Cug").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;

                document.getElementById("<%= txtContactNo.ClientID %>").value="";
                document.getElementById("<%= txtEmailid.ClientID %>").value="";  
                document.getElementById("<%= txtHandOver.ClientID %>").value="";

                document.getElementById("<%= txtFromDt.ClientID %>").value="";
                document.getElementById("<%= txtToDt.ClientID %>").value="";

                document.getElementById("rCrmTeam").style.display = "none"; 
                document.getElementById("rCrmRole").style.display = "none"; 
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";
           
            }
            function GetTeamDtls()
            {
                FillCrmTeam();
                GetSelectedTeam(); 
            }
            function GetRoleDtls()
            {
                FillCrmRole(); 
                GetSelectedRole();
            }

            function GetBranchDtls()
            {
                FillCrmBranch(); 
                GetSelectedBranch();
            }

            //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
            //<Remarks>Multiple Report Code selection added</Remarks>
            function GetReportDtls()
            {
                FillReport(); 
                GetSelectedReport();
            }

            function FillCrmTeam()
            {
                var Cnt=0;
                var SelectGroup = "";
                Val = "";
                var rrr = document.getElementById("<%= hdnCrmTeam.ClientID %>").value.split("Ñ");
                if (rrr != "") {
                    for (a = 1; a < rrr.length; a++) {
                        var cols = rrr[a].split("ÿ");
                        if (cols[2] == 1) { 
                            Val = "selected";
                            Cnt=Math.abs(Cnt)+1;
                        }
                        else
                            Val="";
                        SelectGroup += "<option value=" + cols[0] + " " + Val + " >" + cols[1] + "</option>";
                    }
                    $('#chkCrmTeam').html(''); // clear out old list
                    $('#chkCrmTeam').multiselect('destroy');  // tell widget to clear itself
                    $('#chkCrmTeam').html(SelectGroup); // add in the new list
                    $('#chkCrmTeam').multiselect();
                }
            }
            function GetSelectedTeam()
            {
                var SelectedItems = $('#chkCrmTeam').val();
                if(SelectedItems != null)
                {
                    var StrVal = SelectedItems.toString();
                    var StrLast = StrVal.replace('multiselect-all,', '');      
                }      
                //ToServer("1ʘ2ʘ" + StrLast, 1);
            }

            function FillCrmRole()
            {
                var Cnt=0;
                var SelectGroup = "";
                Val = "";
                var rrr = document.getElementById("<%= hdnCrmRole.ClientID %>").value.split("Ñ");
                if (rrr != "") {
                    for (a = 1; a < rrr.length; a++) {
                        var cols = rrr[a].split("ÿ");
                        if (cols[2] == 1) { 
                            Val = "selected";
                            Cnt=Math.abs(Cnt)+1;
                        }
                        else
                            Val="";
                        SelectGroup += "<option value=" + cols[0] + " " + Val + " >" + cols[1] + "</option>";
                    }
                    $('#chkCrmRole').html(''); // clear out old list
                    $('#chkCrmRole').multiselect('destroy');  // tell widget to clear itself
                    $('#chkCrmRole').html(SelectGroup); // add in the new list
                    $('#chkCrmRole').multiselect();
                }
            }
            function GetSelectedRole()
            {
                var SelectedItems = $('#chkCrmRole').val();
                if(SelectedItems != null)
                {
                    var StrVal = SelectedItems.toString();
                    var StrLast = StrVal.replace('multiselect-all,', ''); 
                }  
            }
            function FillCrmBranch()
            {
                var Cnt=0;
                var SelectGroup = "";
                Val = "";
                var rrr = document.getElementById("<%= hdnCrmBranch.ClientID %>").value.split("Ñ");
                if (rrr != "") {
                    for (a = 1; a < rrr.length; a++) {
                        var cols = rrr[a].split("ÿ");
                        if (cols[2] == 1) { 
                            Val = "selected";
                            Cnt=Math.abs(Cnt)+1;
                        }
                        else
                            Val="";
                        SelectGroup += "<option value=" + cols[0] + " " + Val + " >" + cols[1] + "</option>";
                    }
                    $('#chkCrmBranch').html(''); // clear out old list
                    $('#chkCrmBranch').multiselect('destroy');  // tell widget to clear itself
                    $('#chkCrmBranch').html(SelectGroup); // add in the new list
                    $('#chkCrmBranch').multiselect();
//                    if (Cnt!=0)
//                        $('#chkCrmTeam').multiselect('disable');
//                    else
//                        $('#chkCrmTeam').multiselect('enable');
                }
            }
            function GetSelectedBranch()
            {
                var SelectedItems = $('#chkCrmBranch').val();
                if(SelectedItems != null)
                {
                    var StrVal = SelectedItems.toString();
                    var StrLast = StrVal.replace('multiselect-all,', '');      
                } 
            }

            //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
            //<Remarks>Multiple Report Code selection added</Remarks>
            function FillReport()
            {
                var Cnt=0;
                var SelectGroup = "";
                Val = "";
                var rrr = document.getElementById("<%= hdnReport.ClientID %>").value.split("Ñ");
                if (rrr != "") {
                    for (a = 1; a < rrr.length; a++) {
                        var cols = rrr[a].split("ÿ");
                        if (cols[2] == 1) { 
                            Val = "selected";
                            Cnt=Math.abs(Cnt)+1;
                        }
                        else
                            Val="";
                        SelectGroup += "<option value=" + cols[0] + " " + Val + " >" + cols[1] + "</option>";
                    }
                    $('#chkReport').html(''); // clear out old list
                    $('#chkReport').multiselect('destroy');  // tell widget to clear itself
                    $('#chkReport').html(SelectGroup); // add in the new list
                    $('#chkReport').multiselect();
                }
            }

            //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
            //<Remarks>Multiple Report Code selection added</Remarks>
            function GetSelectedReport()
            {
                var SelectedItems = $('#chkReport').val();
                if(SelectedItems != null)
                {
                    var StrVal = SelectedItems.toString();
                    var StrLast = StrVal.replace('multiselect-all,', '');      
                } 
            }

            function TellerOnChange(){
                if (document.getElementById("<%= cmbRole.ClientID%>").value == 1 || document.getElementById("<%= cmbTeller.ClientID%>").value == 1)
                {
                    document.getElementById("PrevEmpCode").style.display = "";
                    
                }
                else 
                {
                    document.getElementById("PrevEmpCode").style.display = "none";
                    
                }
            }
            function RoleOnChange() {
                document.getElementById("<%= cmbTeller.ClientID%>").value ="-1";
      
                if (document.getElementById("<%= cmbRole.ClientID%>").value == 1)
                {

                    document.getElementById("Teller").style.display = "";
                    
                }
                else if (document.getElementById("<%= cmbRole.ClientID%>").value == 44 || document.getElementById("<%= cmbRole.ClientID%>").value == 45 || document.getElementById("<%= cmbRole.ClientID%>").value == 196 || document.getElementById("<%= cmbRole.ClientID%>").value == 197)
                {
                    document.getElementById("PrevEmpCode").style.display = "";
                    document.getElementById("Teller").style.display = "none";
                }
                else 
                {
                    document.getElementById("PrevEmpCode").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                }

            }
            function OldTransferOnChange() {        
                if (document.getElementById("<%= cmbTransBranchOld.ClientID%>").value != "-1")
                {
                    var ToData = "4Ø" + document.getElementById("<%= cmbTransBranchOld.ClientID%>").value ; 
                    ToServer(ToData, 4);
                }  
            }
            function OldRoleOnChange() { 
                if (document.getElementById("<%= cmbCategory.ClientID%>").value != 18)
                    var ToData = "5Ø" + document.getElementById("<%= cmbRoleOld.ClientID%>").value +"Ø" +document.getElementById("<%= cmbCategory.ClientID%>").value+"Ø" + 0; 
                else
                    var ToData = "5Ø" + document.getElementById("<%= cmbRoleOld.ClientID%>").value +"Ø" +document.getElementById("<%= cmbCategory.ClientID%>").value+"Ø" + document.getElementById("<%= cmbUserType.ClientID%>").value; 
                    
                ToServer(ToData, 5);
           
            }
            function UserTypeOnChange() { 
                    var ToData = "3Ø" +document.getElementById("<%= cmbCategory.ClientID%>").value +"Ø" +document.getElementById("<%= cmbUserType.ClientID%>").value; 
                    ToServer(ToData, 3);
           
            }
            function CategoryOnChange() {
                ClearCombo("<%= cmbType.ClientID %>");
                ClearCombo("<%= cmbRole.ClientID%>");
                ClearCombo("<%= cmbRoleOld.ClientID%>");
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Cug").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").value = "";
                document.getElementById("<%= txtEmpName.ClientID%>").value = "";
                document.getElementById("<%= txtCUG.ClientID%>").value = "";
                document.getElementById("<%= txtDepartment.ClientID%>").value = "";
                document.getElementById("<%= txtDesignation.ClientID%>").value = "";
                document.getElementById("<%= txtDateOfJoin.ClientID%>").value = "";
                document.getElementById("<%= txtReportingTo.ClientID%>").value = "";
                document.getElementById("<%= txtEmpStatus.ClientID%>").value = "";
                document.getElementById("<%= txtContactNo.ClientID%>").value = "";
                document.getElementById("<%= txtEmailid.ClientID%>").value = "";

                document.getElementById("<%= txtDOB.ClientID%>").value = "";
                document.getElementById("<%= txtJoinDt.ClientID%>").value = "";
                document.getElementById("<%= txtProofOne.ClientID%>").value = "";
                document.getElementById("<%= txtProofTwo.ClientID%>").value = "";
                document.getElementById("<%= txtUserID.ClientID%>").value = "";
                document.getElementById("<%= txtHandOver.ClientID %>").value="";
                document.getElementById("<%= txtFromDt.ClientID %>").value="";
                document.getElementById("<%= txtToDt.ClientID %>").value="";
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;

                if (document.getElementById("<%= cmbCategory.ClientID %>").value =="-1" ){
                    document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;           
                }
                else{
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=false;
                }
                document.getElementById("<%= cmbType.ClientID%>").value ="-1";
                var Groupid = document.getElementById("<%= cmbCategory.ClientID %>").value;
                if (Groupid > 0) {
                    var ToData = "2Ø" + Groupid; 
                    ToServer(ToData, 2);
                }
                document.getElementById("rCrmTeam").style.display = "none"; 
                document.getElementById("rCrmRole").style.display = "none"; 
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";
            }
            function TypeOnChange() {                 
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=false;
                document.getElementById("<%= txtEmpCode.ClientID%>").value = "";
                document.getElementById("<%= txtEmpName.ClientID%>").value = "";
                document.getElementById("<%= txtCUG.ClientID%>").value = "";
                document.getElementById("<%= txtDepartment.ClientID%>").value = "";
                document.getElementById("<%= txtDesignation.ClientID%>").value = "";
                document.getElementById("<%= txtDateOfJoin.ClientID%>").value = "";
                document.getElementById("<%= txtReportingTo.ClientID%>").value = "";
                document.getElementById("<%= txtEmpStatus.ClientID%>").value = "";
                document.getElementById("<%= txtContactNo.ClientID%>").value = "";
                document.getElementById("<%= txtEmailid.ClientID%>").value = "";
                document.getElementById("<%= txtDOB.ClientID%>").value = "";
                document.getElementById("<%= txtJoinDt.ClientID%>").value = "";
                document.getElementById("<%= txtProofOne.ClientID%>").value = "";
                document.getElementById("<%= txtProofTwo.ClientID%>").value = "";
                document.getElementById("<%= txtUserID.ClientID%>").value = "";
                document.getElementById("<%= txtHandOver.ClientID %>").value="";

                document.getElementById("<%= txtFromDt.ClientID %>").value="";
                document.getElementById("<%= txtToDt.ClientID %>").value="";

                document.getElementById("DL").style.display = "none";

                document.getElementById("txtBranch").value = "";
                document.getElementById("txtTeam").value = "";
                document.getElementById("txtRole").value = "";

                if (document.getElementById("<%= cmbCategory.ClientID %>").value =="-1" ){
                    document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                }
                else{
                    document.getElementById("<%= txtEmpCode.ClientID%>").disabled=false;
                }
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false;
                document.getElementById("<%= txtEmpCode.ClientID%>").value="";
                document.getElementById("<%= txtEmpName.ClientID%>").value="";
                var Type1 = document.getElementById("<%= cmbType.ClientID %>").value;
                document.getElementById("PrevEmpCode").style.display = "none";
                if (document.getElementById("<%= cmbCategory.ClientID %>").value ==1 || document.getElementById("<%= cmbCategory.ClientID %>").value ==18 ){//profile
                    document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                    document.getElementById("EmpCode").style.display = ""; 
                    document.getElementById("Name").style.display = "";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Usertype").style.display = "none";

                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("rDOB").style.display = "none";
                    document.getElementById("rJDate").style.display = "none";
                    document.getElementById("rProofCmpOne").style.display = "none";
                    document.getElementById("rPrNoOne").style.display = "none";
                    document.getElementById("rProofCmpTwo").style.display = "none";
                    document.getElementById("rPrNoTwo").style.display = "none";
                    document.getElementById("rUserID").style.display = "none";
                    document.getElementById("rHandOver").style.display = "none";
                    //---
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none";
                    //----

                    document.getElementById("rCrmTeam").style.display = "none"; 
                    document.getElementById("rCrmRole").style.display = "none"; 
                    document.getElementById("rCrmTeam1").style.display = "none"; 
                    document.getElementById("rCrmRole1").style.display = "none";
                    document.getElementById("rCrmBranch").style.display = "none"; 
                    document.getElementById("rCrmBranch1").style.display = "none";

                    //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                    //<Remarks>Multiple Report Code selection added</Remarks>
                    document.getElementById("rReport").style.display = "none";
                    
                    document.getElementById("OldProfID").style.display = "none";
                    document.getElementById("NewProfID").style.display = "none";

                    if (Type1 == 1 || Type1 == 99) { //New User
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        if (Type1 == 99)
                            document.getElementById("Usertype").style.display = "";
                        else
                            document.getElementById("Usertype").style.display = "none";

                        document.getElementById("Role").style.display = "";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "none";
                        document.getElementById("rHandOver").style.display = "none";
                        //---
                        document.getElementById("rFromDt").style.display = "none";
                        document.getElementById("rToDt").style.display = "none";
                        //----

                        
                    document.getElementById("OldProfID").style.display = "none";
                    document.getElementById("NewProfID").style.display = "";
                    }
                    else if (Type1 == 2 || Type1 == 100) { //Disable/Delete User
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("OldProfID").style.display = "none";
                    }
                    else if (Type1 == 3 || Type1 == 101) {//Transfer User
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                        document.getElementById("Transfer1").style.display = "";
                        document.getElementById("Transfer2").style.display = "";
                         document.getElementById("Transfer").style.display = "";
                         document.getElementById("Teller").style.display = "none";
                         document.getElementById("rUserID").style.display = "";
                         document.getElementById("rHandOver").style.display = "none";
                    }
                    else if (Type1 == 26 || Type1 ==20  || Type1 ==21  || Type1 ==22   || Type1==34 || Type1 ==103  || Type1 ==104   || Type1==106 ){// Unlock & Reset Password // Unlock UserID                  
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Usertype").style.display = "none";

                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                        document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                        EmployeeOnChange(); 
                        
                        document.getElementById("OldProfID").style.display = "none";
                        document.getElementById("NewProfID").style.display = "none";           
                    }
                    else if (Type1 == 27 ){ // Change Role
                        if (document.getElementById("<%= cmbRole.ClientID%>").value == 44 || document.getElementById("<%= cmbRole.ClientID%>").value == 45 || (document.getElementById("<%= cmbRole.ClientID%>").value == 1 && document.getElementById("<%= cmbTeller.ClientID%>").value ==1))
                        {
                            document.getElementById("PrevEmpCode").style.display = "";
                        }
                        else 
                        {
                            document.getElementById("PrevEmpCode").style.display = "none";
                        }
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Role").style.display = "";
                        document.getElementById("Role1").style.display = "";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("OldProfID").style.display = "";
                        document.getElementById("NewProfID").style.display = "";
                    }
                    else if (Type1 == 105 ){ // Change Role
                        if (document.getElementById("<%= cmbRole.ClientID%>").value == 196 || document.getElementById("<%= cmbRole.ClientID%>").value == 197)
//                        || (document.getElementById("<%= cmbRole.ClientID%>").value == 1 && document.getElementById("<%= cmbTeller.ClientID%>").value ==1))
                        {
                            document.getElementById("PrevEmpCode").style.display = "";
                        }
                        else 
                        {
                            document.getElementById("PrevEmpCode").style.display = "none";
                        }
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Usertype").style.display = "";

                        document.getElementById("Role").style.display = "";
                        document.getElementById("Role1").style.display = "";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("OldProfID").style.display = "";
                        document.getElementById("NewProfID").style.display = "";
                    }
                    else {                        
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Usertype").style.display = "none";

                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        //---
                        document.getElementById("rFromDt").style.display = "none";
                        document.getElementById("rToDt").style.display = "none";
                        //----

                        document.getElementById("rCrmTeam").style.display = "none";
                        document.getElementById("rCrmRole").style.display = "none"; 
                        document.getElementById("rCrmTeam1").style.display = "none"; 
                        document.getElementById("rCrmRole1").style.display = "none";
                        document.getElementById("rCrmBranch").style.display = "none"; 
                        document.getElementById("rCrmBranch1").style.display = "none";

                        //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                        //<Remarks>Multiple Report Code selection added</Remarks>
                        document.getElementById("rReport").style.display = "none";

                        document.getElementById("OldProfID").style.display = "none";
                        document.getElementById("NewProfID").style.display = "none";

                    }
            }
            //nms 01/06/2019
            else if (document.getElementById("<%= cmbCategory.ClientID %>").value ==10 ){//BR NET - on 22 July 2019
                //===========================================================================================
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("Usertype").style.display = "none";

                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";                 
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none";   
                
                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none"; 
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none"; 

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";

                if (Type1 == 50) { //New User
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";

                    document.getElementById("rDOB").style.display = "";
                    document.getElementById("rJDate").style.display = "";
                    document.getElementById("rProofCmpOne").style.display = "";
                    document.getElementById("rPrNoOne").style.display = "";
                    document.getElementById("rProofCmpTwo").style.display = "";
                    document.getElementById("rPrNoTwo").style.display = "";
                    document.getElementById("rUserID").style.display = "none";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none"; 
                }
                else if (Type1 == 51) { //Delete UserID
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rUserID").style.display = ""; 
                    document.getElementById("rHandOver").style.display = "none";   
                    document.getElementById("rUserID").style.display = "";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none";  
                }
                else if (Type1 == 52) {//Transfer User
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                    document.getElementById("Transfer1").style.display = "";
                    document.getElementById("Transfer2").style.display = "";
                        document.getElementById("Transfer").style.display = "";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("rFromDt").style.display = "none";
                        document.getElementById("rToDt").style.display = "none"; 
                }
                else if (Type1 == 53 ){// Unlock & Reset Password // Unlock UserID                  
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rUserID").style.display = "";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none"; 
                    document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                    document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                    EmployeeOnChange();            
                }
                else if (Type1 == 54 ){ // Change Role
                    if (document.getElementById("<%= cmbRole.ClientID%>").value == 44 || document.getElementById("<%= cmbRole.ClientID%>").value == 45 || (document.getElementById("<%= cmbRole.ClientID%>").value == 1 && document.getElementById("<%= cmbTeller.ClientID%>").value ==1))
                    {
                        document.getElementById("PrevEmpCode").style.display = "";
                    }
                    else 
                    {
                        document.getElementById("PrevEmpCode").style.display = "none";
                    }
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "";
                    document.getElementById("Role1").style.display = "";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rUserID").style.display = "";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none"; 
                }
                else if (Type1 == 67) { //Disable User ID
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rUserID").style.display = ""; 
                    document.getElementById("rHandOver").style.display = "none";    
                    document.getElementById("rFromDt").style.display = "";
                    document.getElementById("rToDt").style.display = ""; 
                }
                else {                        
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rDOB").style.display = "none";
                    document.getElementById("rJDate").style.display = "none";
                    document.getElementById("rProofCmpOne").style.display = "none";
                    document.getElementById("rPrNoOne").style.display = "none";
                    document.getElementById("rProofCmpTwo").style.display = "none";
                    document.getElementById("rPrNoTwo").style.display = "none";
                    document.getElementById("rUserID").style.display = "";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none"; 

                    document.getElementById("rCrmTeam").style.display = "none";
                    document.getElementById("rCrmRole").style.display = "none"; 
                    document.getElementById("rCrmTeam1").style.display = "none"; 
                    document.getElementById("rCrmRole1").style.display = "none";
                    document.getElementById("rCrmBranch").style.display = "none"; 
                    document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";
                }
                //===========================================================================================
            }
            else if (document.getElementById("<%= cmbCategory.ClientID %>").value == 11 ){// SERVOSYS - on 22 July 2019
                //===========================================================================================
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 

                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none"; 
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";

                if (Type1 == 58) { //New User
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rHandOver").style.display = "none";
                }
                else if (Type1 == 59) { //Disable/Delete User
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rHandOver").style.display = "";   
                }
                else if (Type1 == 60) {//Transfer User
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                        document.getElementById("Transfer1").style.display = "";
                        document.getElementById("Transfer").style.display = "";
                        document.getElementById("Transfer2").style.display = "";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rHandOver").style.display = "";
                }
                else if (Type1 == 61){// Unlock & Reset Password // Unlock UserID                  
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                    document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                    EmployeeOnChange();            
                }
                else if (Type1 == 62){ // Change Role
                    if (document.getElementById("<%= cmbRole.ClientID%>").value == 44 || document.getElementById("<%= cmbRole.ClientID%>").value == 45 || (document.getElementById("<%= cmbRole.ClientID%>").value == 1 && document.getElementById("<%= cmbTeller.ClientID%>").value ==1))
                    {
                        document.getElementById("PrevEmpCode").style.display = "";
                    }
                    else 
                    {
                        document.getElementById("PrevEmpCode").style.display = "none";
                    }
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "";
                    document.getElementById("Role1").style.display = "";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rDOB").style.display = "none";
                    document.getElementById("rJDate").style.display = "none";
                    document.getElementById("rProofCmpOne").style.display = "none";
                    document.getElementById("rPrNoOne").style.display = "none";
                    document.getElementById("rProofCmpTwo").style.display = "none";
                    document.getElementById("rPrNoTwo").style.display = "none";
                    document.getElementById("rUserID").style.display = "none";

                    //<Summary>Changed by 40020 on 08-Feb-2021</Summary>
                    //<Comments>Hand Over added</Comments>
                    document.getElementById("rHandOver").style.display = "";

                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none"; 
                }
                else {                        
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rDOB").style.display = "none";
                    document.getElementById("rJDate").style.display = "none";
                    document.getElementById("rProofCmpOne").style.display = "none";
                    document.getElementById("rPrNoOne").style.display = "none";
                    document.getElementById("rProofCmpTwo").style.display = "none";
                    document.getElementById("rPrNoTwo").style.display = "none";
                    document.getElementById("rUserID").style.display = "none";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none"; 
                    document.getElementById("rCrmTeam").style.display = "none";
                    document.getElementById("rCrmRole").style.display = "none"; 
                    document.getElementById("rCrmTeam1").style.display = "none"; 
                    document.getElementById("rCrmRole1").style.display = "none";
                    document.getElementById("rCrmBranch").style.display = "none"; 
                    document.getElementById("rCrmBranch1").style.display = "none";

                    //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                    //<Remarks>Multiple Report Code selection added</Remarks>
                    document.getElementById("rReport").style.display = "none";
                }
                //===========================================================================================
            } 
            else if (document.getElementById("<%= cmbCategory.ClientID %>").value == 12 ) // Laptop
            {
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 
                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none"; 
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";

                if (Type1 == 63) { //Unlock Laptop
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";                       
                }
                else {                        
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rDOB").style.display = "none";
                    document.getElementById("rJDate").style.display = "none";
                    document.getElementById("rProofCmpOne").style.display = "none";
                    document.getElementById("rPrNoOne").style.display = "none";
                    document.getElementById("rProofCmpTwo").style.display = "none";
                    document.getElementById("rPrNoTwo").style.display = "none";
                    document.getElementById("rUserID").style.display = "none";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none"; 
                    document.getElementById("rCrmTeam").style.display = "none";
                    document.getElementById("rCrmRole").style.display = "none"; 
                    document.getElementById("rCrmTeam1").style.display = "none"; 
                    document.getElementById("rCrmRole1").style.display = "none";
                    document.getElementById("rCrmBranch").style.display = "none"; 
                    document.getElementById("rCrmBranch1").style.display = "none";

                    //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                    //<Remarks>Multiple Report Code selection added</Remarks>
                    document.getElementById("rReport").style.display = "none";
                }
            }           
            else if (document.getElementById("<%= cmbCategory.ClientID %>").value ==9 ){//Servosys - CASA TAB
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";     
                document.getElementById("Transfer2").style.display = "none";     
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";   
                document.getElementById("rUserID").style.display = "none";   
                document.getElementById("rHandOver").style.display = "none";    
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none";   
                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none"; 
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";  

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";

                if (Type1 == 41) { //Newuser
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                }
                else if (Type1 == 45 || Type1 == 79) { //Disable user / Enable User
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";                       
                }
                else if (Type1 == 42) { // Transfer
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                    document.getElementById("Transfer1").style.display = "";
                    document.getElementById("Transfer").style.display = "";
                    document.getElementById("Transfer2").style.display = "";
                    document.getElementById("Teller").style.display = "none";  
                }
                else if (Type1 == 44 || Type1 == 43 ){ //Unlock / Reset Password                 
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                    document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                    EmployeeOnChange();            
                }
                else if (Type1 == 46 ){  // Change Role                      
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "";
                    document.getElementById("Role1").style.display = "";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";                                  
                }                    
                else  {
                    document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false;
                }   
            }
            else if   (document.getElementById("<%= cmbCategory.ClientID %>").value ==2 || document.getElementById("<%= cmbCategory.ClientID %>").value ==4 ){//AD/EMAIL             
                document.getElementById("Role").style.display = "none";
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 
                document.getElementById("Cug").style.display = "";
                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none"; 
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";

                if (Type1 == 20 || Type1 ==31 || Type1==32 || Type1==33  ){
                    document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                            document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                            EmployeeOnChange();
                }
                else if (Type1 ==36 || Type1 ==37 ||Type1 ==38){
                    document.getElementById("DL").style.display = "";
                    document.getElementById("<%= lblDL.ClientID%>").innerText = "Distribution List";
                }
                else if (Type1 ==48 || Type1 ==49 || Type1 ==55 ){//nms 12-jun-2019
                    document.getElementById("DL").style.display = "";
                    document.getElementById("<%= lblDL.ClientID%>").innerText = "Generic ID";
                }
            }
            else  if (document.getElementById("<%= cmbCategory.ClientID %>").value == 13 ){ //FIS - MIS                    
                document.getElementById("FolderName").style.display = "";
                document.getElementById("Storage").style.display = "";
                document.getElementById("Access").style.display = "";
                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none"; 
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";

                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                if (Type1 == 64) {// MIS
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";                       
                    document.getElementById("<%= txtFolderName.ClientID%>").value = "";
                    document.getElementById("<%= txtFolderName.ClientID%>").disabled = false; 
                    document.getElementById("rCrmTeam").style.display = "none";
                    document.getElementById("rCrmRole").style.display = "none"; 
                    document.getElementById("rCrmTeam1").style.display = "none"; 
                    document.getElementById("rCrmRole1").style.display = "none";
                    document.getElementById("rCrmBranch").style.display = "none"; 
                    document.getElementById("rCrmBranch1").style.display = "none";    
                    
                    //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                    //<Remarks>Multiple Report Code selection added</Remarks>
                    document.getElementById("rReport").style.display = "none";                    
                }
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("Cug").style.display = "";
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 
                //document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
            }
            else if (document.getElementById("<%= cmbCategory.ClientID %>").value ==14 ){ // CRM *******************
                    document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                    document.getElementById("EmpCode").style.display = ""; 
                    document.getElementById("Name").style.display = "";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("rDOB").style.display = "none";
                    document.getElementById("rJDate").style.display = "none";
                    document.getElementById("rProofCmpOne").style.display = "none";
                    document.getElementById("rPrNoOne").style.display = "none";
                    document.getElementById("rProofCmpTwo").style.display = "none";
                    document.getElementById("rPrNoTwo").style.display = "none";
                    document.getElementById("rUserID").style.display = "none";
                    document.getElementById("rHandOver").style.display = "none";
                    //---
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none";
                    //----
                    document.getElementById("rCrmTeam").style.display = "none"; 
                    document.getElementById("rCrmRole").style.display = "none";
                    document.getElementById("rCrmTeam1").style.display = "none"; 
                    document.getElementById("rCrmRole1").style.display = "none";
                    document.getElementById("rCrmBranch").style.display = "none"; 
                    document.getElementById("rCrmBranch1").style.display = "none";

                    //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                    //<Remarks>Multiple Report Code selection added</Remarks>
                    document.getElementById("rReport").style.display = "none";

                    if (Type1 == 68) { //New User
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Usertype").style.display = "none";
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "none";
                        document.getElementById("rHandOver").style.display = "none";
                        //---
                        document.getElementById("rFromDt").style.display = "none";
                        document.getElementById("rToDt").style.display = "none";
                        document.getElementById("rCrmTeam").style.display = ""; 
                        document.getElementById("rCrmRole").style.display = "";
                        document.getElementById("rCrmTeam1").style.display = "none"; 
                        document.getElementById("rCrmRole1").style.display = "none";
                        document.getElementById("rCrmBranch").style.display = "none"; 
                        document.getElementById("rCrmBranch1").style.display = "none";

                        //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                        //<Remarks>Multiple Report Code selection added</Remarks>
                        document.getElementById("rReport").style.display = "none";
                        //----
                    }
                    else if (Type1 == 71 || Type1 == 72  || Type1 ==76) { //Disable User, Enable User, All Branch Access
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                        document.getElementById("Usertype").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("rCrmTeam").style.display = "none";
                        document.getElementById("rCrmRole").style.display = "none";
                        document.getElementById("rCrmTeam1").style.display = ""; 
                        document.getElementById("rCrmRole1").style.display = "";
                        document.getElementById("rCrmBranch").style.display = "none"; 
                        document.getElementById("rCrmBranch1").style.display = "";

                        //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                        //<Remarks>Multiple Report Code selection added</Remarks>
                        document.getElementById("rReport").style.display = "none";
                    }
                    else if (Type1 == 69 || Type1 == 70  || Type1 == 107){ // Unlock UserID, Reset Password               
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                        document.getElementById("Usertype").style.display = "none";
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                        document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                        document.getElementById("rCrmTeam").style.display = "none";
                        document.getElementById("rCrmRole").style.display = "none";
                        document.getElementById("rCrmTeam1").style.display = ""; 
                        document.getElementById("rCrmRole1").style.display = "";
                        document.getElementById("rCrmBranch").style.display = "none"; 
                        document.getElementById("rCrmBranch1").style.display = "";

                        //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                        //<Remarks>Multiple Report Code selection added</Remarks>
                        document.getElementById("rReport").style.display = "none";

                        EmployeeOnChange();            
                    }
                    else if (Type1 == 74 ){ // Role Access                        
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Usertype").style.display = "none";
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("rCrmTeam").style.display = "none";
                        document.getElementById("rCrmRole").style.display = "";
                        document.getElementById("rCrmTeam1").style.display = ""; 
                        document.getElementById("rCrmRole1").style.display = "none";
                        document.getElementById("rCrmBranch").style.display = "none"; 
                        document.getElementById("rCrmBranch1").style.display = "";

                        //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                        //<Remarks>Multiple Report Code selection added</Remarks>
                        document.getElementById("rReport").style.display = "none";
                    }
                    else if (Type1 == 75 ){ // Team Access                        
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Usertype").style.display = "none";
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("rCrmTeam").style.display = "";
                        document.getElementById("rCrmRole").style.display = "none";
                        document.getElementById("rCrmTeam1").style.display = "none"; 
                        document.getElementById("rCrmRole1").style.display = "";
                        document.getElementById("rCrmBranch").style.display = "none"; 
                        document.getElementById("rCrmBranch1").style.display = "";

                        //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                        //<Remarks>Multiple Report Code selection added</Remarks>
                        document.getElementById("rReport").style.display = "none";
                    }
                    else if (Type1 == 73 ){ // Branch Access                        
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Usertype").style.display = "none";
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("rCrmTeam").style.display = "none";
                        document.getElementById("rCrmRole").style.display = "none";
                        document.getElementById("rCrmTeam1").style.display = ""; 
                        document.getElementById("rCrmRole1").style.display = "";
                        document.getElementById("rCrmBranch").style.display = ""; 
                        document.getElementById("rCrmBranch1").style.display = "none";

                        //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                        //<Remarks>Multiple Report Code selection added</Remarks>
                        document.getElementById("rReport").style.display = "none";
                    }
                    else {                        
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Usertype").style.display = "none";
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        //---
                        document.getElementById("rFromDt").style.display = "none";
                        document.getElementById("rToDt").style.display = "none";
                        //----
                        document.getElementById("rCrmTeam").style.display = "none";
                        document.getElementById("rCrmRole").style.display = "none";
                        document.getElementById("rCrmTeam1").style.display = "none"; 
                        document.getElementById("rCrmRole1").style.display = "none";
                        document.getElementById("rCrmBranch").style.display = "none"; 
                        document.getElementById("rCrmBranch1").style.display = "none";

                        //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                        //<Remarks>Multiple Report Code selection added</Remarks>
                        document.getElementById("rReport").style.display = "none";
                    }
            }
            else  if (document.getElementById("<%= cmbCategory.ClientID %>").value == 5 ){ //Share Folder      
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;              
                document.getElementById("FolderName").style.display = "";
                document.getElementById("Storage").style.display = "";
                document.getElementById("Access").style.display = "";
                 document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none";
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";

                if (Type1 == 13) {//Share folder
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";                       
                    document.getElementById("<%= txtFolderName.ClientID%>").value = "";
                    document.getElementById("<%= txtFolderName.ClientID%>").disabled = false;                         
                }
                else if (Type1 == 14) { //own folder
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("<%= txtFolderName.ClientID%>").value = document.getElementById("<%= txtEmpName.ClientID%>").value;
                    document.getElementById("<%= txtFolderName.ClientID%>").disabled = true;                        
                }
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("Cug").style.display = "";
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
            }
            else if (document.getElementById("<%= cmbCategory.ClientID %>").value == 3  ){ //AOCO
                document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;        
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("Cug").style.display = "";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 
                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none";
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";

                if  (Type1 ==17){
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("Transfer").style.display = ""; 
                    document.getElementById("Transfer1").style.display = "";
                    document.getElementById("Transfer2").style.display = "";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("<%= txtEmpCode.ClientID%>").focus();
                }
                else if  (Type1 ==56){//nms 10/jun/2019
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "";
                    document.getElementById("Role1").style.display = "";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("<%= txtEmpCode.ClientID%>").focus();
                }
                else if  (Type1 ==7){
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("Transfer").style.display = "none"; 
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("<%= txtEmpCode.ClientID%>").focus();
                }
                else if  (Type1 ==22) //unlock application
                {
                    document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false; 
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("Transfer").style.display = "none"; 
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
                    document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                    document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                    EmployeeOnChange();
                }
                else
                {
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("Transfer").style.display = "none"; 
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("rDOB").style.display = "none";
                    document.getElementById("rJDate").style.display = "none";
                    document.getElementById("rProofCmpOne").style.display = "none";
                    document.getElementById("rPrNoOne").style.display = "none";
                    document.getElementById("rProofCmpTwo").style.display = "none";
                    document.getElementById("rPrNoTwo").style.display = "none";
                    document.getElementById("rUserID").style.display = "none";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none"; 
                    document.getElementById("<%= txtEmpCode.ClientID%>").focus();
                    document.getElementById("rCrmTeam").style.display = "none";
                    document.getElementById("rCrmRole").style.display = "none";
                    document.getElementById("rCrmTeam1").style.display = "none"; 
                    document.getElementById("rCrmRole1").style.display = "none";
                    document.getElementById("rCrmBranch").style.display = "none"; 
                    document.getElementById("rCrmBranch1").style.display = "none";

                    //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                    //<Remarks>Multiple Report Code selection added</Remarks>
                    document.getElementById("rReport").style.display = "none";
                }            
            }
            else  if (document.getElementById("<%= cmbCategory.ClientID %>").value == 7  ){ //RLOS
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true; 
            document.getElementById("rCrmTeam").style.display = "none";
            document.getElementById("rCrmRole").style.display = "none";
            document.getElementById("rCrmTeam1").style.display = "none"; 
            document.getElementById("rCrmRole1").style.display = "none";
            document.getElementById("rCrmBranch").style.display = "none"; 
            document.getElementById("rCrmBranch1").style.display = "none";

           //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
           //<Remarks>Multiple Report Code selection added</Remarks>
           document.getElementById("rReport").style.display = "none";

            if (Type1 ==23)
            {
                document.getElementById("<%= cmbRole.ClientID%>").disabled = false;              
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("Cug").style.display = "";
                document.getElementById("Role").style.display = "";
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            }
            else if  (Type1 ==57){//nms 10/jun/2019
                document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "";
                document.getElementById("Role1").style.display = "";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            }
            else  if (Type1 ==35) //unlock application
            {
                document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false; 
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("Cug").style.display = "";
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
                document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                EmployeeOnChange();                
            }
            else{
                document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("Cug").style.display = "";
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none";
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";
            }
        }
        else  if (document.getElementById("<%= cmbCategory.ClientID %>").value == 6 ){ //Cross Fraud
            document.getElementById("<%= cmbRole.ClientID%>").disabled = false;                     
            document.getElementById("EmpCode").style.display = ""; 
            document.getElementById("Name").style.display = "";
            document.getElementById("Cug").style.display = "";
            document.getElementById("Usertype").style.display = "none";
            document.getElementById("Role").style.display = "";
            document.getElementById("Teller").style.display = "none";
            document.getElementById("Transfer").style.display = "none"; 
            document.getElementById("Transfer1").style.display = "none";
            document.getElementById("Transfer2").style.display = "none";
            document.getElementById("Role1").style.display = "none";
            document.getElementById("FolderName").style.display = "none";
            document.getElementById("Storage").style.display = "none";
            document.getElementById("Access").style.display = "none";
            document.getElementById("rDOB").style.display = "none";
            document.getElementById("rJDate").style.display = "none";
            document.getElementById("rProofCmpOne").style.display = "none";
            document.getElementById("rPrNoOne").style.display = "none";
            document.getElementById("rProofCmpTwo").style.display = "none";
            document.getElementById("rPrNoTwo").style.display = "none";
            document.getElementById("rUserID").style.display = "none";
            document.getElementById("rHandOver").style.display = "none";
            document.getElementById("rFromDt").style.display = "none";
            document.getElementById("rToDt").style.display = "none"; 
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
            document.getElementById("rCrmTeam").style.display = "none";
            document.getElementById("rCrmRole").style.display = "none";
            document.getElementById("rCrmTeam1").style.display = "none"; 
            document.getElementById("rCrmRole1").style.display = "none";
            document.getElementById("rCrmBranch").style.display = "none"; 
            document.getElementById("rCrmBranch1").style.display = "none";

            //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
            //<Remarks>Multiple Report Code selection added</Remarks>
            document.getElementById("rReport").style.display = "none";

            //<Summary>Change done by 40020 on 05-Jan-2021</Summary>
            //<Remarks>Unlock & Reset Password // Unlock UserID added</Remarks>
            if (Type1 == 113 || Type1 ==114 ){// Unlock & Reset Password // Unlock UserID                  
                document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                document.getElementById("Role").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Usertype").style.display = "none";

                document.getElementById("Teller").style.display = "none";
                document.getElementById("rUserID").style.display = "";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                EmployeeOnChange(); 
                        
                document.getElementById("OldProfID").style.display = "none";
                document.getElementById("NewProfID").style.display = "none";           
            }
        }
        else if (document.getElementById("<%= cmbCategory.ClientID %>").value ==15 ){//CIB
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
            document.getElementById("EmpCode").style.display = ""; 
            document.getElementById("Name").style.display = "";
            document.getElementById("FolderName").style.display = "none";
            document.getElementById("Storage").style.display = "none";
            document.getElementById("Access").style.display = "none";
            document.getElementById("Usertype").style.display = "none";
            document.getElementById("Role").style.display = "none";
            document.getElementById("Role1").style.display = "none";
            document.getElementById("Transfer").style.display = "none";
            document.getElementById("Transfer1").style.display = "none";
            document.getElementById("Transfer2").style.display = "none";
            document.getElementById("rDOB").style.display = "none";
            document.getElementById("rJDate").style.display = "none";
            document.getElementById("rProofCmpOne").style.display = "none";
            document.getElementById("rPrNoOne").style.display = "none";
            document.getElementById("rProofCmpTwo").style.display = "none";
            document.getElementById("rPrNoTwo").style.display = "none";
            document.getElementById("rUserID").style.display = "none";
            document.getElementById("rHandOver").style.display = "none";
            //---
            document.getElementById("rFromDt").style.display = "none";
            document.getElementById("rToDt").style.display = "none";
            //----

            document.getElementById("rCrmTeam").style.display = "none"; 
            document.getElementById("rCrmRole").style.display = "none"; 
            document.getElementById("rCrmTeam1").style.display = "none"; 
            document.getElementById("rCrmRole1").style.display = "none";
            document.getElementById("rCrmBranch").style.display = "none"; 
            document.getElementById("rCrmBranch1").style.display = "none";

            //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
            //<Remarks>Multiple Report Code selection added</Remarks>
            document.getElementById("rReport").style.display = "none";

            if (Type1 == 81) { //New User
                document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                //---
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none";
                //----
            }
            else if (Type1 == 82 || Type1 ==87   || Type1==88 || Type1==89) { //Disable/Delete User / Update Mobile Number / Update Email / Extend validity
                document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("rUserID").style.display = "";
                document.getElementById("rHandOver").style.display = "none";
            }
            else if (Type1 == 84 || Type1 ==85  || Type1 ==86){// Reset Auth Password / Reset Login Password / Unlock User                   
                document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("rUserID").style.display = "";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                EmployeeOnChange();            
            }
            else if (Type1 == 83 ){ // Change Role               
                document.getElementById("PrevEmpCode").style.display = "none";
                document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "";
                document.getElementById("Role1").style.display = "";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("rUserID").style.display = "";
                document.getElementById("rHandOver").style.display = "none";
            }
            else {                        
                document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("rUserID").style.display = "";
                document.getElementById("rHandOver").style.display = "none";
                //---
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none";
                //----

                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none"; 
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";
            }
        }
        else if (document.getElementById("<%= cmbCategory.ClientID %>").value == 16 ){//SAS EGRC
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
            document.getElementById("EmpCode").style.display = ""; 
            document.getElementById("Name").style.display = "";
            document.getElementById("FolderName").style.display = "none";
            document.getElementById("Storage").style.display = "none";
            document.getElementById("Access").style.display = "none";
            document.getElementById("Usertype").style.display = "none";
            document.getElementById("Role").style.display = "none";
            document.getElementById("Role1").style.display = "none";
            document.getElementById("Transfer").style.display = "none";
            document.getElementById("Transfer1").style.display = "none";
            document.getElementById("Transfer2").style.display = "none";
            document.getElementById("rDOB").style.display = "none";
            document.getElementById("rJDate").style.display = "none";
            document.getElementById("rProofCmpOne").style.display = "none";
            document.getElementById("rPrNoOne").style.display = "none";
            document.getElementById("rProofCmpTwo").style.display = "none";
            document.getElementById("rPrNoTwo").style.display = "none";
            document.getElementById("rUserID").style.display = "none";
            document.getElementById("rHandOver").style.display = "none";
            //---
            document.getElementById("rFromDt").style.display = "none";
            document.getElementById("rToDt").style.display = "none";
            //----

            document.getElementById("rCrmTeam").style.display = "none"; 
            document.getElementById("rCrmRole").style.display = "none"; 
            document.getElementById("rCrmTeam1").style.display = "none"; 
            document.getElementById("rCrmRole1").style.display = "none";
            document.getElementById("rCrmBranch").style.display = "none"; 
            document.getElementById("rCrmBranch1").style.display = "none";

            //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
            //<Remarks>Multiple Report Code selection added</Remarks>
            document.getElementById("rReport").style.display = "none";

            if (Type1 == 90) { //New User
                document.getElementById("<%= cmbRole.ClientID%>").disabled = false;
                document.getElementById("Usertype").style.display = "none"; 
                document.getElementById("Role").style.display = "";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                //---
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none";
                //----
            }
            else if (Type1 == 91 || Type1 == 93) { //Disable/Delete User / Enable User
                document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("rUserID").style.display = "";
                document.getElementById("rHandOver").style.display = "none";
            }
            else if (Type1 == 92) {//Transfer User
                document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                document.getElementById("Transfer1").style.display = "";
                document.getElementById("Transfer").style.display = "";
                document.getElementById("Transfer2").style.display = "";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("rUserID").style.display = "";
                document.getElementById("rHandOver").style.display = "none";
            }
            else if (Type1 == 94 ){ // Change Role               
                document.getElementById("PrevEmpCode").style.display = "none";
                document.getElementById("<%= cmbRole.ClientID%>").disabled = false;
                document.getElementById("Usertype").style.display = "none"; 
                document.getElementById("Role").style.display = "";
                document.getElementById("Role1").style.display = "";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("rUserID").style.display = "";
                document.getElementById("rHandOver").style.display = "none";
            }
            else {                        
                document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("rUserID").style.display = "";
                document.getElementById("rHandOver").style.display = "none";
                //---
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none";
                //----

                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none"; 
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";
            }
        }
        else if (document.getElementById("<%= cmbCategory.ClientID %>").value == 17 ){ //P2P
            document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false;        
            document.getElementById("EmpCode").style.display = ""; 
            document.getElementById("Name").style.display = "";
            document.getElementById("Cug").style.display = "";
            document.getElementById("rDOB").style.display = "none";
            document.getElementById("rJDate").style.display = "none";
            document.getElementById("rProofCmpOne").style.display = "none";
            document.getElementById("rPrNoOne").style.display = "none";
            document.getElementById("rProofCmpTwo").style.display = "none";
            document.getElementById("rPrNoTwo").style.display = "none";
            document.getElementById("rUserID").style.display = "none";
            document.getElementById("rHandOver").style.display = "none";
            document.getElementById("rFromDt").style.display = "none";
            document.getElementById("rToDt").style.display = "none"; 
            document.getElementById("rCrmTeam").style.display = "none";
            document.getElementById("rCrmRole").style.display = "none";
            document.getElementById("rCrmTeam1").style.display = "none"; 
            document.getElementById("rCrmRole1").style.display = "none";
            document.getElementById("rCrmBranch").style.display = "none"; 
            document.getElementById("rCrmBranch1").style.display = "none";

            //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
            //<Remarks>Multiple Report Code selection added</Remarks>
            document.getElementById("rReport").style.display = "none";

            document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();            
            if  (Type1 == 98){//Change Role
                document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "";
                document.getElementById("Role1").style.display = "";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            }
            else if  (Type1 == 95){// New User
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            }
            else if  (Type1 == 96) // Disable / Delete User
            {  
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;     
                document.getElementById("Usertype").style.display = "none";         
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
                document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                EmployeeOnChange();
            }
            else if  (Type1 == 97) //Unlock application
            {  
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false;    
                document.getElementById("Usertype").style.display = "none";          
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
                document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                EmployeeOnChange();
            }
            else
            {
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none";
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";
            }            
        }
        else if (document.getElementById("<%= cmbCategory.ClientID %>").value == 8){ //Profile MIS
            document.getElementById("EmpCode").style.display = ""; 
            document.getElementById("Name").style.display = "";
            document.getElementById("Cug").style.display = "";
            document.getElementById("Usertype").style.display = "none";
            document.getElementById("Role").style.display = "none";
            document.getElementById("Teller").style.display = "none";
            document.getElementById("Transfer").style.display = "none";
            document.getElementById("FolderName").style.display = "none";
            document.getElementById("Transfer1").style.display = "none";
            document.getElementById("Transfer2").style.display = "none";
            document.getElementById("Role1").style.display = "none";
            document.getElementById("Storage").style.display = "none";
            document.getElementById("Access").style.display = "none";
            document.getElementById("rDOB").style.display = "none";
            document.getElementById("rJDate").style.display = "none";
            document.getElementById("rProofCmpOne").style.display = "none";
            document.getElementById("rPrNoOne").style.display = "none";
            document.getElementById("rProofCmpTwo").style.display = "none";
            document.getElementById("rPrNoTwo").style.display = "none";
            document.getElementById("rUserID").style.display = "none";
            document.getElementById("rHandOver").style.display = "none";
            document.getElementById("rFromDt").style.display = "none";
            document.getElementById("rToDt").style.display = "none"; 
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
            document.getElementById("rCrmTeam").style.display = "none";
            document.getElementById("rCrmRole").style.display = "none";
            document.getElementById("rCrmTeam1").style.display = "none"; 
            document.getElementById("rCrmRole1").style.display = "none";
            document.getElementById("rCrmBranch").style.display = "none"; 
            document.getElementById("rCrmBranch1").style.display = "none";

            //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
            //<Remarks>Multiple Report Code selection added</Remarks>
            document.getElementById("rReport").style.display = "none";

            //<Summary>Created By 40020 on 31-Dec-2020</Summary>
            //<Remarks>Service Now Ticket No mandatory for Transfer User,Additional Report Access & Additional Branch Access</Remarks>
            if  (Type1 ==28){ //New User
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false;
            }
            else if (Type1==115) {//Transfer User
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false;
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                        document.getElementById("Transfer1").style.display = "";
                        document.getElementById("Transfer2").style.display = "";
                         document.getElementById("Transfer").style.display = "";
                         document.getElementById("Teller").style.display = "none";
                         document.getElementById("rUserID").style.display = "";
                         document.getElementById("rHandOver").style.display = "none";
                    }
            else if  (Type1 ==29 || Type1 ==30){ // Disable/Delete User || Unlock & Reset Password
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true; 
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
                document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                EmployeeOnChange();
            }
            else if (Type1 == 116 ){ // Additional Report Code Access    
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Usertype").style.display = "none";
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("rCrmTeam").style.display = "none";
                        document.getElementById("rCrmRole").style.display = "none";
                        document.getElementById("rCrmTeam1").style.display = "none"; 
                        document.getElementById("rCrmRole1").style.display = "none";
                        document.getElementById("rCrmBranch").style.display = "none"; 
                        document.getElementById("rCrmBranch1").style.display = "none";
                        
                        //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                        //<Remarks>Multiple Report Code selection added</Remarks>
                        document.getElementById("rReport").style.display = "";
            }
            else if (Type1 == 117 ){ // Additional Branch Access    
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Usertype").style.display = "none";
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("rCrmTeam").style.display = "none";
                        document.getElementById("rCrmRole").style.display = "none";
                        document.getElementById("rCrmTeam1").style.display = "none"; 
                        document.getElementById("rCrmRole1").style.display = "none";
                        document.getElementById("rCrmBranch").style.display = ""; 
                        document.getElementById("rCrmBranch1").style.display = "none";
                        
                        //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                        //<Remarks>Multiple Report Code selection added</Remarks>
                        document.getElementById("rReport").style.display = "none";
            }
            else
            {
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none"; 
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
                document.getElementById("rCrmTeam").style.display = "none";
                document.getElementById("rCrmRole").style.display = "none";
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                document.getElementById("rReport").style.display = "none";
            }            
        }

        //<Summary>Added by 40020 on 12-Jan-2021</Summary>
        //<Remarks>Rubix Added</Remarks>
        else if (document.getElementById("<%= cmbCategory.ClientID %>").value == 20  ){ //Rubix
                document.getElementById("Cug").style.display = "";
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Usertype").style.display = "none";
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Transfer2").style.display = "none";
                document.getElementById("rDOB").style.display = "none";
                document.getElementById("rJDate").style.display = "none";
                document.getElementById("rProofCmpOne").style.display = "none";
                document.getElementById("rPrNoOne").style.display = "none";
                document.getElementById("rProofCmpTwo").style.display = "none";
                document.getElementById("rPrNoTwo").style.display = "none";
                document.getElementById("rUserID").style.display = "none";
                document.getElementById("rHandOver").style.display = "none";
                document.getElementById("rFromDt").style.display = "none";
                document.getElementById("rToDt").style.display = "none";
                document.getElementById("rCrmTeam").style.display = "none"; 
                document.getElementById("rCrmRole").style.display = "none"; 
                document.getElementById("rCrmTeam1").style.display = "none"; 
                document.getElementById("rCrmRole1").style.display = "none";
                document.getElementById("rCrmBranch").style.display = "none"; 
                document.getElementById("rCrmBranch1").style.display = "none";
                document.getElementById("rReport").style.display = "none";
                document.getElementById("OldProfID").style.display = "none";
                document.getElementById("NewProfID").style.display = "none";

                if  (Type1 ==118){ //New User
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Role").style.display = "";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("Transfer").style.display = "none"; 
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("<%= txtEmpCode.ClientID%>").focus();
                }
                else if (Type1 == 119) { //Disable User
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rUserID").style.display = "";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("OldProfID").style.display = "none";
                }
                else if (Type1 == 120) {//Transfer User
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                    document.getElementById("Transfer1").style.display = "";
                    document.getElementById("Transfer2").style.display = "";
                    document.getElementById("Transfer").style.display = "";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rUserID").style.display = "";
                    document.getElementById("rHandOver").style.display = "none";
                }
                else if (Type1 == 121 ){// Unlock & Reset Password                   
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Usertype").style.display = "none";

                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rUserID").style.display = "";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                    document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                    EmployeeOnChange(); 
                        
                    document.getElementById("OldProfID").style.display = "none";
                    document.getElementById("NewProfID").style.display = "none";           
                }
                else if (Type1 == 122 ){ // Change Role
                    if (document.getElementById("<%= cmbRole.ClientID%>").value == 44 || document.getElementById("<%= cmbRole.ClientID%>").value == 45 || (document.getElementById("<%= cmbRole.ClientID%>").value == 1 && document.getElementById("<%= cmbTeller.ClientID%>").value ==1))
                    {
                        document.getElementById("PrevEmpCode").style.display = "";
                    }
                    else 
                    {
                        document.getElementById("PrevEmpCode").style.display = "none";
                    }
                    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    document.getElementById("Role").style.display = "";
                    document.getElementById("Role1").style.display = "";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("rUserID").style.display = "";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("OldProfID").style.display = "none";
                    document.getElementById("NewProfID").style.display = "none";
                }
                else
                {
                    document.getElementById("Usertype").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                    document.getElementById("Transfer").style.display = "none"; 
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("rDOB").style.display = "none";
                    document.getElementById("rJDate").style.display = "none";
                    document.getElementById("rProofCmpOne").style.display = "none";
                    document.getElementById("rPrNoOne").style.display = "none";
                    document.getElementById("rProofCmpTwo").style.display = "none";
                    document.getElementById("rPrNoTwo").style.display = "none";
                    document.getElementById("rUserID").style.display = "none";
                    document.getElementById("rHandOver").style.display = "none";
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none"; 
                    document.getElementById("<%= txtEmpCode.ClientID%>").focus();
                    document.getElementById("rCrmTeam").style.display = "none";
                    document.getElementById("rCrmRole").style.display = "none";
                    document.getElementById("rCrmTeam1").style.display = "none"; 
                    document.getElementById("rCrmRole1").style.display = "none";
                    document.getElementById("rCrmBranch").style.display = "none"; 
                    document.getElementById("rCrmBranch1").style.display = "none";
                    document.getElementById("rReport").style.display = "none";
                }            
            }
            //<Summary>Changed by 40020 on 14-Jan-2021</Summary>
            //<Comments>Aadhar Login Application added</Comments>
            else if (document.getElementById("<%= cmbCategory.ClientID %>").value ==21){//Aadhar Login
                    document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                    document.getElementById("EmpCode").style.display = ""; 
                    document.getElementById("Name").style.display = "";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Usertype").style.display = "none";

                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Transfer2").style.display = "none";
                    document.getElementById("rDOB").style.display = "none";
                    document.getElementById("rJDate").style.display = "none";
                    document.getElementById("rProofCmpOne").style.display = "none";
                    document.getElementById("rPrNoOne").style.display = "none";
                    document.getElementById("rProofCmpTwo").style.display = "none";
                    document.getElementById("rPrNoTwo").style.display = "none";
                    document.getElementById("rUserID").style.display = "none";
                    document.getElementById("rHandOver").style.display = "none";
                    //---
                    document.getElementById("rFromDt").style.display = "none";
                    document.getElementById("rToDt").style.display = "none";
                    //----

                    document.getElementById("rCrmTeam").style.display = "none"; 
                    document.getElementById("rCrmRole").style.display = "none"; 
                    document.getElementById("rCrmTeam1").style.display = "none"; 
                    document.getElementById("rCrmRole1").style.display = "none";
                    document.getElementById("rCrmBranch").style.display = "none"; 
                    document.getElementById("rCrmBranch1").style.display = "none";

                    document.getElementById("rReport").style.display = "none";
                    
                    document.getElementById("OldProfID").style.display = "none";
                    document.getElementById("NewProfID").style.display = "none";

                    
                    if (Type1 == 123 ){// Unlock & Reset Password                
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Usertype").style.display = "none";

                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                        document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                        EmployeeOnChange(); 
                        
                        document.getElementById("OldProfID").style.display = "none";
                        document.getElementById("NewProfID").style.display = "none";           
                    }                                       
                    else {                        
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Usertype").style.display = "none";

                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Transfer2").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("rUserID").style.display = "";
                        document.getElementById("rHandOver").style.display = "none";
                        //---
                        document.getElementById("rFromDt").style.display = "none";
                        document.getElementById("rToDt").style.display = "none";
                        //----

                        document.getElementById("rCrmTeam").style.display = "none";
                        document.getElementById("rCrmRole").style.display = "none"; 
                        document.getElementById("rCrmTeam1").style.display = "none"; 
                        document.getElementById("rCrmRole1").style.display = "none";
                        document.getElementById("rCrmBranch").style.display = "none"; 
                        document.getElementById("rCrmBranch1").style.display = "none";

                        document.getElementById("rReport").style.display = "none";

                        document.getElementById("OldProfID").style.display = "none";
                        document.getElementById("NewProfID").style.display = "none";

                  }
             }
        else { 
            document.getElementById("EmpCode").style.display = ""; 
            document.getElementById("Name").style.display = "";
            document.getElementById("Cug").style.display = "";
            document.getElementById("Usertype").style.display = "none";
            document.getElementById("Role").style.display = "none";
            document.getElementById("Teller").style.display = "none";
            document.getElementById("Transfer").style.display = "none";
            document.getElementById("FolderName").style.display = "none";
            document.getElementById("Transfer1").style.display = "none";
            document.getElementById("Transfer2").style.display = "none";
            document.getElementById("Role1").style.display = "none";
            document.getElementById("Storage").style.display = "none";
            document.getElementById("Access").style.display = "none";
            document.getElementById("rDOB").style.display = "none";
            document.getElementById("rJDate").style.display = "none";
            document.getElementById("rProofCmpOne").style.display = "none";
            document.getElementById("rPrNoOne").style.display = "none";
            document.getElementById("rProofCmpTwo").style.display = "none";
            document.getElementById("rPrNoTwo").style.display = "none";
            document.getElementById("rUserID").style.display = "none";
            document.getElementById("rHandOver").style.display = "none";
            document.getElementById("rFromDt").style.display = "none";
            document.getElementById("rToDt").style.display = "none"; 
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
            document.getElementById("rCrmTeam").style.display = "none";
            document.getElementById("rCrmRole").style.display = "none";
            document.getElementById("rCrmTeam1").style.display = "none"; 
            document.getElementById("rCrmRole1").style.display = "none";
            document.getElementById("rCrmBranch").style.display = "none"; 
            document.getElementById("rCrmBranch1").style.display = "none";

            //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
            //<Remarks>Multiple Report Code selection added</Remarks>
            document.getElementById("rReport").style.display = "none";
        }
    }
       
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
       
        for (a = 0; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function ClearCombo(control) {
        document.getElementById(control).options.length = 0;
        var option1 = document.createElement("OPTION");
        option1.value = -1;
        option1.text = " -----Select-----";
        document.getElementById(control).add(option1);
    }
    function FromServer(arg, context) 
    {
        if (context == 1) 
        {
            var Datas = arg.split("Ř");
            var Data = Datas[0].split("Ø");
            if (arg == "ØØ") 
            {
                alert("Invalid Employee Code");
                document.getElementById("<%= cmbRole.ClientID%>").value = "-1";
              
                document.getElementById("<%= txtEmpCode.ClientID%>").value = "";
                document.getElementById("<%= txtEmpName.ClientID%>").value = "";
                document.getElementById("<%= txtCUG.ClientID%>").value = "";
                document.getElementById("<%= txtDepartment.ClientID%>").value = "";
                document.getElementById("<%= txtDesignation.ClientID%>").value = "";
                document.getElementById("<%= txtDateOfJoin.ClientID%>").value = "";
                document.getElementById("<%= txtReportingTo.ClientID%>").value = "";
                document.getElementById("<%= txtEmpStatus.ClientID%>").value = "";
                document.getElementById("<%= txtContactNo.ClientID%>").value = "";
                document.getElementById("<%= txtEmailid.ClientID%>").value = "";
                document.getElementById("<%= lblRequestMail.ClientID%>").value = "";
                document.getElementById("<%= txtDOB.ClientID%>").value = "";
                document.getElementById("<%= txtJoinDt.ClientID%>").value = "";
            }
            else
            { 
                document.getElementById("<%= txtEmpName.ClientID%>").value = Data[1];
                document.getElementById("<%= txtContactNo.ClientID%>").value = Data[3];
                if(document.getElementById("<%= cmbCategory.ClientID%>").value ==4 && document.getElementById("<%= cmbType.ClientID%>").value == 10 )
                {                    
                    document.getElementById("<%= lblRequestMail.ClientID%>").innerText = (Data[1].toString().replace(" ",".")).toString().replace(" ","") +"@esafbank.com";    
                }
                else{
                document.getElementById("<%= lblRequestMail.ClientID%>").innerText = "";                   
                }
                    
                document.getElementById("<%= txtEmailid.ClientID%>").value = Data[2];
                document.getElementById("<%= txtCUG.ClientID%>").value = Data[4];
                document.getElementById("<%= txtDepartment.ClientID%>").value =  Data[5];
                document.getElementById("<%= txtDesignation.ClientID%>").value =  Data[6];
                document.getElementById("<%= txtDateOfJoin.ClientID%>").value =  Data[7];
                document.getElementById("<%= txtReportingTo.ClientID%>").value =   Data[9];
                document.getElementById("<%= txtEmpStatus.ClientID%>").value =  Data[10];
                document.getElementById("<%= txtDOB.ClientID%>").value = Data[12];
                document.getElementById("<%= txtJoinDt.ClientID%>").value = Data[13];
                document.getElementById("<%= cmbRole.ClientID%>").value = (Data[11] =="") ? -1 : Data[11];
                document.getElementById("<%= txtFolderName.ClientID%>").value = ((document.getElementById("<%= cmbCategory.ClientID %>").value == 5 && document.getElementById("<%= cmbType.ClientID%>").value == 14)) ? Data[1] : "";
                if((document.getElementById("<%= cmbCategory.ClientID%>").value == 14) && (document.getElementById("<%= cmbType.ClientID%>").value == 69 || document.getElementById("<%= cmbType.ClientID%>").value == 70 || document.getElementById("<%= cmbType.ClientID%>").value == 71 || document.getElementById("<%= cmbType.ClientID%>").value == 72 || document.getElementById("<%= cmbType.ClientID%>").value == 76 || document.getElementById("<%= cmbType.ClientID%>").value == 107))
                {
                    if(Datas[1] != "") //Branch
                    {
                        var BrDtl = "";
                        var Branch = Datas[1].split("Ñ");
                        for (a = 1; a < Branch.length; a++) {
                            var cols = Branch[a].split("ÿ");
                            BrDtl += cols[1];
                            if (a+1 < Branch.length)
                                BrDtl += ', ';
                        }
                        document.getElementById("txtBranch").value = BrDtl;
                    }
                    if(Datas[2] != "") //Team
                    {
                        var TeamDtl = "";
                        var Team = Datas[2].split("Ñ");
                        for (a = 1; a < Team.length; a++) {
                            var cols = Team[a].split("ÿ");
                            TeamDtl += cols[1];
                            if (a+1 < Team.length)
                                TeamDtl += ', ';
                        }
                        document.getElementById("txtTeam").value = TeamDtl;
                    }
                    if(Datas[3] != "")
                    {
                        var RoleDtl = "";
                        var Role = Datas[3].split("Ñ");
                        for (a = 1; a < Role.length; a++) {
                            var cols = Role[a].split("ÿ");
                            RoleDtl += cols[1];
                            if (a+1 < Role.length)
                                RoleDtl += ', ';
                        }
                        document.getElementById("txtRole").value = RoleDtl;
                    }
                }  
                else if(document.getElementById("<%= cmbCategory.ClientID%>").value == 14 && document.getElementById("<%= cmbType.ClientID%>").value == 74) //Multiple Role Access
                {
                    if(Datas[1] != "") //Branch
                    {
                        var BrDtl = "";
                        var Branch = Datas[1].split("Ñ");
                        for (a = 1; a < Branch.length; a++) {
                            var cols = Branch[a].split("ÿ");
                            BrDtl += cols[1];
                            if (a+1 < Branch.length)
                                BrDtl += ', ';
                        }
                        document.getElementById("txtBranch").value = BrDtl;
                    }
                    if(Datas[2] != "") //Team
                    {
                        var TeamDtl = "";
                        var Team = Datas[2].split("Ñ");
                        for (a = 1; a < Team.length; a++) {
                            var cols = Team[a].split("ÿ");
                            TeamDtl += cols[1];
                            if (a+1 < Team.length)
                                TeamDtl += ', ';
                        }
                        document.getElementById("txtTeam").value = TeamDtl;
                    }
                    if(Datas[3] != "") // Role
                    {                
                        var NewStr = "";
                        var temp = "";
                        var Flag;
                        var Role = Datas[3].split("Ñ");
                        row = document.getElementById("<%= hdnCrmRole.ClientID %>").value.split("Ñ");
                    
                        for (n = 1; n < row.length; n++) 
                        {
                            Flag = 0; 
                            var cols = row[n].split("ÿ");
                            for (j = 1; j < Role.length; j++)
                            {
                                var roCm = Role[j].split("ÿ");
                                if(cols[0] == roCm[0])
                                {
                                    temp = Role[j];
                                    Flag = 1;
                                }                                                        
                            } 
                            if(Flag == 1)  
                                NewStr += "Ñ" + temp;
                            else
                                NewStr += "Ñ" + row[n];              
                        }
                        document.getElementById("<%= hdnCrmRole.ClientID %>").value = NewStr;
                        GetRoleDtls();
                    }
                    else
                    {
                        GetRoleDtls();
                    }
                }   
                else if(document.getElementById("<%= cmbCategory.ClientID%>").value == 14 && document.getElementById("<%= cmbType.ClientID%>").value == 75) //Multiple Team Access
                {
                    if(Datas[1] != "") //Branch
                    {
                        var BrDtl = "";
                        var Branch = Datas[1].split("Ñ");
                        for (a = 1; a < Branch.length; a++) {
                            var cols = Branch[a].split("ÿ");
                            BrDtl += cols[1];
                            if (a+1 < Branch.length)
                                BrDtl += ', ';
                        }
                        document.getElementById("txtBranch").value = BrDtl;
                    }
                    if(Datas[2] != "") //Team
                    {  
                        var NewStr = "";
                        var temp = "";
                        var Flag;
                        var Team = Datas[2].split("Ñ");
                        row = document.getElementById("<%= hdnCrmTeam.ClientID %>").value.split("Ñ");
                    
                        for (n = 1; n < row.length; n++) 
                        {
                            Flag = 0; 
                            var cols = row[n].split("ÿ");
                            for (j = 1; j < Team.length; j++)
                            {
                                var teCm = Team[j].split("ÿ");
                                if(cols[0] == teCm[0])
                                {
                                    temp = Team[j];
                                    Flag = 1;
                                }                                                        
                            } 
                            if(Flag == 1)  
                                NewStr += "Ñ" + temp;
                            else
                                NewStr += "Ñ" + row[n];              
                        }
                        document.getElementById("<%= hdnCrmTeam.ClientID %>").value = NewStr;
                        GetTeamDtls();
                    }
                    else{
                        GetTeamDtls();
                    }
                    if(Datas[3] != "") // Role
                    {
                        var RoleDtl = "";
                        var Role = Datas[3].split("Ñ");
                        for (a = 1; a < Role.length; a++) {
                            var cols = Role[a].split("ÿ");
                            RoleDtl += cols[1];
                            if (a+1 < Role.length)
                                RoleDtl += ', ';
                        }
                        document.getElementById("txtRole").value = RoleDtl;
                    }
                }   
                //<Summary>Craeted by 40020 on 02-Jan-2021</Summary>
                //<Comments>Added Multiple Branch selection for Profile-MIS also</Comments>
                //else if((document.getElementById("<%= cmbCategory.ClientID%>").value == 14 && document.getElementById("<%= cmbType.ClientID%>").value == 73)) //Multiple Branch Access
                else if((document.getElementById("<%= cmbCategory.ClientID%>").value == 14 && document.getElementById("<%= cmbType.ClientID%>").value == 73)||(document.getElementById("<%= cmbCategory.ClientID%>").value == 8 && document.getElementById("<%= cmbType.ClientID%>").value == 117)) //Multiple Branch Access
                {
                    if(Datas[1] != "") //Branch
                    {
                        var NewStr = "";
                        var temp = "";
                        var Flag;
                        var Branch = Datas[1].split("Ñ");
                        row = document.getElementById("<%= hdnCrmBranch.ClientID %>").value.split("Ñ");
                    
                        for (n = 1; n < row.length; n++) 
                        {
                            Flag = 0; 
                            var cols = row[n].split("ÿ");
                            for (j = 1; j < Branch.length; j++)
                            {
                                var BrCm = Branch[j].split("ÿ");
                                if(cols[0] == BrCm[0])
                                {
                                    temp = Branch[j];
                                    Flag = 1;
                                }                                                        
                            } 
                            if(Flag == 1)  
                                NewStr += "Ñ" + temp;
                            else
                                NewStr += "Ñ" + row[n];              
                        }
                        document.getElementById("<%= hdnCrmBranch.ClientID %>").value = NewStr;
                        GetBranchDtls();
                    }
                    else
                    {
                        GetBranchDtls();
                    }
                    if(Datas[2] != "") //Team
                    {
                        var TeamDtl = "";
                        var Team = Datas[2].split("Ñ");
                        for (a = 1; a < Team.length; a++) {
                            var cols = Team[a].split("ÿ");
                            TeamDtl += cols[1];
                            if (a+1 < Team.length)
                                TeamDtl += ', ';
                        }
                        document.getElementById("txtTeam").value = TeamDtl;
                    }
                    if(Datas[3] != "")
                    {
                        var RoleDtl = "";
                        var Role = Datas[3].split("Ñ");
                        for (a = 1; a < Role.length; a++) {
                            var cols = Role[a].split("ÿ");
                            RoleDtl += cols[1];
                            if (a+1 < Role.length)
                                RoleDtl += ', ';
                        }
                        document.getElementById("txtRole").value = RoleDtl;
                    }
                }

                //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                //<Remarks>Multiple Report Code selection added</Remarks>
                else if((document.getElementById("<%= cmbCategory.ClientID%>").value == 8 && document.getElementById("<%= cmbType.ClientID%>").value == 116)) //Multiple Report Access
                {
                    if(Datas[1] != "") //Report Code
                    {
                        var NewStr = "";
                        var temp = "";
                        var Flag;
                        var Branch = Datas[1].split("Ñ");
                        row = document.getElementById("<%= hdnReport.ClientID %>").value.split("Ñ");
                    
                        for (n = 1; n < row.length; n++) 
                        {
                            Flag = 0; 
                            var cols = row[n].split("ÿ");
                            for (j = 1; j < Branch.length; j++)
                            {
                                var BrCm = Branch[j].split("ÿ");
                                if(cols[0] == BrCm[0])
                                {
                                    temp = Branch[j];
                                    Flag = 1;
                                }                                                        
                            } 
                            if(Flag == 1)  
                                NewStr += "Ñ" + temp;
                            else
                                NewStr += "Ñ" + row[n];              
                        }
                        document.getElementById("<%= hdnReport.ClientID %>").value = NewStr;
                        GetReportDtls();
                    }
                    else
                    {
                        GetReportDtls();
                    }                    
                }

            }
        }
        else if (context == 2) {
            var Dtls = arg.split("Ř");
            var Data = Dtls[0].split("|");
            ComboFill(Data[0], "<%= cmbType.ClientID%>");
            if (document.getElementById("<%= cmbCategory.ClientID%>").value == 18)
            {
                ComboFill(Data[1], "<%= cmbUserType.ClientID%>");

            }
            else
            {
                ComboFill(Data[1], "<%= cmbRole.ClientID%>");

                ComboFill(Data[1], "<%= cmbRoleOld.ClientID%>");
            }

            if(Dtls[1] == "" && document.getElementById("<%= cmbCategory.ClientID%>").value == 14)
            {
                alert("No team for CRM");
                return false;
            }
            else{
                document.getElementById("<%= hdnCrmTeam.ClientID %>").value = Dtls[1];
                GetTeamDtls();
            }
            if(Dtls[2] == "" && document.getElementById("<%= cmbCategory.ClientID%>").value == 14)
            {
                alert("No Roles for CRM");
                return false;
            }
            else{
                document.getElementById("<%= hdnCrmRole.ClientID %>").value = Dtls[2];
                GetRoleDtls(); 
            } 
            if (Dtls[3] != "" && document.getElementById("<%= cmbCategory.ClientID%>").value == 18)
            {
                ComboFill(Dtls[3], "<%= cmbRole.ClientID%>");
                ComboFill(Dtls[3], "<%= cmbRoleOld.ClientID%>");
            }
  
        }
        else if (context == 3) {
            var Data = arg;
            ComboFill(Data, "<%= cmbRole.ClientID%>");
            ComboFill(Data, "<%= cmbRoleOld.ClientID%>");
        }

        else if (context == 4) {
            var Data = arg;
            ComboFill(Data, "<%= cmbTransBranch.ClientID%>");  
        }
        else if (context == 5) {
            var Data = arg;
            ComboFill(Data, "<%= cmbRole.ClientID%>");

        }
    }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

    function RequestOnClick() {  
        if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1") 
        {
            alert("Select Branch");
            document.getElementById("<%= cmbBranch.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= cmbCategory.ClientID%>").value == "-1") 
        {
        alert("Select Application Type");
        document.getElementById("<%= cmbCategory.ClientID%>").focus();
            return false;
        }      
        if (document.getElementById("<%= cmbType.ClientID %>").value == "-1") 
        {
            alert("Select Type");
            document.getElementById("<%= cmbType.ClientID %>").focus();
            return false;
        }        
        if (document.getElementById("<%= txtServiceRequestNo.ClientID%>").value == "") 
        {//nms 01/06/2019
            if ((document.getElementById("<%= cmbCategory.ClientID%>").value == 17 && document.getElementById("<%= cmbType.ClientID%>").value == 97) || (document.getElementById("<%= cmbCategory.ClientID%>").value == 8 && document.getElementById("<%= cmbType.ClientID%>").value != 29) && (document.getElementById("<%= cmbCategory.ClientID%>").value == 8 && document.getElementById("<%= cmbType.ClientID%>").value != 30) && document.getElementById("<%= cmbCategory.ClientID%>").value != 12 && document.getElementById("<%= cmbCategory.ClientID%>").value != 13 && document.getElementById("<%= cmbCategory.ClientID%>").value != 14 && document.getElementById("<%= cmbCategory.ClientID%>").value != 15 && document.getElementById("<%= cmbCategory.ClientID%>").value != 16 && document.getElementById("<%= cmbCategory.ClientID%>").value != 11 && document.getElementById("<%= cmbCategory.ClientID%>").value != 5 && document.getElementById("<%= cmbCategory.ClientID%>").value != 9 && document.getElementById("<%= cmbCategory.ClientID%>").value != 10 && document.getElementById("<%= cmbCategory.ClientID%>").value != 2 && document.getElementById("<%= cmbCategory.ClientID%>").value != 4 && (document.getElementById("<%= cmbCategory.ClientID%>").value != 1  || (document.getElementById("<%= cmbCategory.ClientID%>").value == 1 && document.getElementById("<%= cmbType.ClientID%>").value == 9)) &&  (document.getElementById("<%= cmbCategory.ClientID%>").value != 3  || (document.getElementById("<%= cmbCategory.ClientID%>").value == 3 && document.getElementById("<%= cmbType.ClientID%>").value == 22))  && (document.getElementById("<%= cmbCategory.ClientID%>").value !=7  || (document.getElementById("<%= cmbCategory.ClientID%>").value == 7 && document.getElementById("<%= cmbType.ClientID%>").value == 35))) {
                //alert(document.getElementById("<%= cmbCategory.ClientID%>").value );
                alert("Enter Your Service Request No");
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
                return false;
            }
        }        
        if (document.getElementById("txtDetails").value == "" && document.getElementById("<%= cmbType.ClientID %>").value =="2")
        {
            alert("Enter Description");
            document.getElementById("txtDetails").focus();
            return false;
        }  
        if (document.getElementById("<%= cmbType.ClientID %>").value =="27" && document.getElementById("<%= cmbRoleOld.ClientID%>").value =="-1" )
        {
            alert("Select Current Role");
            document.getElementById("<%= cmbRoleOld.ClientID%>").focus();
            return false;
        }  
        if ((document.getElementById("<%= cmbType.ClientID %>").value =="1" || document.getElementById("<%= cmbType.ClientID %>").value =="7"  || document.getElementById("<%= cmbType.ClientID %>").value =="23"  || document.getElementById("<%= cmbType.ClientID %>").value =="41" || document.getElementById("<%= cmbType.ClientID %>").value =="50" || document.getElementById("<%= cmbType.ClientID %>").value =="58" || document.getElementById("<%= cmbType.ClientID %>").value =="81" || document.getElementById("<%= cmbType.ClientID %>").value =="90" || document.getElementById("<%= cmbType.ClientID %>").value =="95") && document.getElementById("<%= cmbRole.ClientID%>").value =="-1" )
        {
            alert("Select Role");
            document.getElementById("<%= cmbRole.ClientID%>").focus();
            return false;
        } 
        //<Summary>Change done by 40020 on 09-Feb-2021</Summary>
        //<Remarks>M-Bank & CASA-TAB Transfer User added</Remarks>
        //<Summary>Change done by 40020 on 21-Jan-2021</Summary>
        //<Remarks>Rubix Transfer User added</Remarks> 
        //<Summary>Change done by 40020 on 05-Jan-2021</Summary>
        //<Remarks>Profile-MIS Transfer User added</Remarks>
        if ((document.getElementById("<%= cmbType.ClientID %>").value =="3" ||document.getElementById("<%= cmbType.ClientID %>").value =="17"||document.getElementById("<%= cmbType.ClientID %>").value =="115"||document.getElementById("<%= cmbType.ClientID %>").value =="120"||document.getElementById("<%= cmbType.ClientID %>").value =="42"||document.getElementById("<%= cmbType.ClientID %>").value =="60") && document.getElementById("<%= cmbTransBranchOld.ClientID%>").value =="-1" )
        {
            alert("Select Current Branch");
            document.getElementById("<%= cmbRoleOld.ClientID%>").focus();
            return false;
        }  
        if ((document.getElementById("<%= cmbType.ClientID %>").value =="3" ||document.getElementById("<%= cmbType.ClientID %>").value =="17"||document.getElementById("<%= cmbType.ClientID %>").value =="115"||document.getElementById("<%= cmbType.ClientID %>").value =="120"||document.getElementById("<%= cmbType.ClientID %>").value =="42"||document.getElementById("<%= cmbType.ClientID %>").value =="60") && document.getElementById("<%= cmbTransBranch.ClientID%>").value =="-1" )
        {
            alert("Select New Branch");
            document.getElementById("<%= cmbRole.ClientID%>").focus();
            return false;
        }  
        if ((document.getElementById("<%= cmbCategory.ClientID%>").value == "1" && (document.getElementById("<%= cmbType.ClientID%>").value == "1" ||document.getElementById("<%= cmbType.ClientID%>").value == "27")) && document.getElementById("<%= cmbRole.ClientID%>").value == "1" && document.getElementById("<%= cmbTeller.ClientID%>").value == "-1") 
        {
            alert("Select Teller Type");
            document.getElementById("<%= cmbTeller.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= cmbCategory.ClientID %>").value ==1 &&  document.getElementById("<%= txtEmpCode.ClientID%>").value == "")
        {
            alert("Enter Emp Code");
            document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%= cmbCategory.ClientID %>").value ==6 && ((document.getElementById("<%= cmbType.ClientID %>").value ==113) || (document.getElementById("<%= cmbType.ClientID %>").value ==114) ) &&  document.getElementById("<%= txtEmpCode.ClientID%>").value == "")
        {
            alert("Enter Emp Code");
            document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            return false;
        }
        //<Summary>Change done by 40020 on 13-Jan-2021</Summary>
        //<Remarks>Rubix validations added</Remarks>
        if (document.getElementById("<%= cmbCategory.ClientID %>").value ==20)
        {          
          if(document.getElementById("<%= txtEmpCode.ClientID%>").value == "")
          {
            alert("Enter Emp Code");
            document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            return false;
          }  
          if ((document.getElementById("<%= cmbType.ClientID %>").value =="118"||document.getElementById("<%= cmbType.ClientID %>").value =="122")  && document.getElementById("<%= cmbRole.ClientID%>").value =="-1" )
          {
            alert("Select Role");
            document.getElementById("<%= cmbRole.ClientID%>").focus();
            return false;
          }  
          if (document.getElementById("<%= cmbType.ClientID %>").value =="122" && document.getElementById("<%= cmbRoleOld.ClientID%>").value =="-1" )
          {
            alert("Select Current Role");
            document.getElementById("<%= cmbRoleOld.ClientID%>").focus();
            return false;
          }  
        }
        

        if(document.getElementById("<%= cmbCategory.ClientID %>").value == 5)
        {
            if (document.getElementById("<%= txtFolderName.ClientID%>").value == "") 
            {
                alert("Enter Folder Name ");
                document.getElementById("<%= txtFolderName.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbStorage.ClientID%>").value == "-1") 
            {
                alert("Select Storage limit");
                document.getElementById("<%= cmbStorage.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbAccess.ClientID%>").value == "") 
            {
                alert("Select  Access Type");
                document.getElementById("<%= cmbAccess.ClientID%>").focus();
                return false;
            }
        }                
        if (document.getElementById("<%= txtContactNo.ClientID %>").value== "") 
        {
            alert("Enter Contact Number");
            document.getElementById("<%= txtContactNo.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= txtContactNo.ClientID%>").value != "") 
        {
            var ret1 = checkPhone(document.getElementById("<%= txtContactNo.ClientID%>"));
            if (ret1 == false)
                return false;
        }      
               
        if (document.getElementById("<%= cmbCategory.ClientID%>").value == "2")
        {
            if (document.getElementById("<%= txtCUG.ClientID%>").value == "" || (document.getElementById("<%= txtCUG.ClientID%>").value).length<10) 
            {
                alert("Enter CUG Number");
                document.getElementById("<%= txtCUG.ClientID%>").focus();
                return false;
            }
        }
        if (document.getElementById("<%= txtCUG.ClientID%>").value != "") 
        {
            var ret1 = checkPhone(document.getElementById("<%= txtCUG.ClientID%>"));
            if (ret1 == false)
                return false;
        } 
        if (document.getElementById("<%= txtEmailid.ClientID %>").value == "") 
        {
            alert("Enter Email Id ");
            document.getElementById("<%= txtEmailid.ClientID %>").focus();
            return false;
        }                   
        if ((document.getElementById("<%= cmbType.ClientID %>").value =="36" || document.getElementById("<%= cmbType.ClientID %>").value =="37" || document.getElementById("<%= cmbType.ClientID %>").value =="38") && document.getElementById("<%= cmbCategory.ClientID%>").value =="4" && document.getElementById("<%= txtDL.ClientID%>").value =="")
        {
            alert("Enter Distribution List");
            document.getElementById("<%= txtDL.ClientID%>").focus();
            return false;
        } 
    
        var DL =  document.getElementById("<%= txtDL.ClientID%>").value; 
        var Branch	= document.getElementById("<%= cmbBranch.ClientID %>").value;
        var APP_ID	= document.getElementById("<%= cmbCategory.ClientID%>").value;
        var trans_branch_id	= document.getElementById("<%= cmbTransBranch.ClientID%>").value;
 
        var TYPE_ID	= document.getElementById("<%= cmbType.ClientID %>").value;
        var CUG =document.getElementById("<%= txtCUG.ClientID%>").value;
        var Descr = document.getElementById("txtDetails").value; 
        var contact=document.getElementById("<%= txtContactNo.ClientID %>").value;
        var emailid=document.getElementById("<%= txtEmailid.ClientID %>").value;
        var Emp_code=document.getElementById("<%= txtEmpCode.ClientID%>").value;
        var OldRoleID =document.getElementById("<%= cmbRoleOld.ClientID%>").value;
        var OldTransferID =document.getElementById("<%= cmbTransBranchOld.ClientID%>").value;
        var RoleID =document.getElementById("<%= cmbRole.ClientID%>").value;
        var Tellertype =document.getElementById("<%= cmbTeller.ClientID %>").value;
        var FolderName =document.getElementById("<%= txtFolderName.ClientID%>").value;
        var Storage =document.getElementById("<%= cmbStorage.ClientID%>").value;
        var Accesstype =document.getElementById("<%= cmbAccess.ClientID%>").value;
        var NewMailID =document.getElementById("<%= lblRequestMail.ClientID%>").innerText ;
        var ServiceRequestNo	= document.getElementById("<%= txtServiceRequestNo.ClientID%>").value;
        var PrevEmpCode	= document.getElementById("<%= txtPrevCode.ClientID%>").value;
        var PrfNameOne="";
        var PrfNoOne="";
        var PrfNameTwo="";
        var PrfNoTwo="";
    
        var ExistingUserID="";
        var FromDt ="";
        var ToDt ="";

        //<Summary>Changed by 40020 on 21-Jan-2021</Summary>
        //<Comments>Rubix Application added</Comments>
        //<Summary>Changed by 40020 on 14-Jan-2021</Summary>
        //<Comments>Aadhar Login Application added</Comments>
        if(APP_ID == 1 || APP_ID == 10 || APP_ID == 18 || APP_ID == 21 || APP_ID == 20) // Profile Or BrNet Or eTHIC Or AadharLogin Or Rubix
        {
            //<Summary>Changed by 40020 on 14-Jan-2021</Summary>
            //<Comments>Aadhar Login Application - Unlock & Reset Password added</Comments>
            if (TYPE_ID == 51 || TYPE_ID == 52 || TYPE_ID == 53 || TYPE_ID == 54 || TYPE_ID == 65 || TYPE_ID == 2 || TYPE_ID == 3 || TYPE_ID == 9 || TYPE_ID == 16 || TYPE_ID == 21 || TYPE_ID == 26 || TYPE_ID == 27 || TYPE_ID == 34 || TYPE_ID == 47 || TYPE_ID == 67  || TYPE_ID == 100 || TYPE_ID == 101 || TYPE_ID == 102|| TYPE_ID == 103|| TYPE_ID == 104|| TYPE_ID == 105|| TYPE_ID == 106 || TYPE_ID==123 || TYPE_ID==120 || TYPE_ID==121 || TYPE_ID==122) // Not NewUser
            {
                if( APP_ID == 18 )
                {
                    ExistingUserID = document.getElementById("<%= txtEmpCode.ClientID%>").value;
                }   
                //User ID Mandatory
                else if(document.getElementById("<%= txtUserID.ClientID%>").value == "" || document.getElementById("<%= txtUserID.ClientID%>").value == '')
                {
                    if (APP_ID == 1)
                    {
                        alert("Please enter Profile User ID");
                        document.getElementById("<%= txtUserID.ClientID %>").focus();
                        return false;
                    }
                    else if (APP_ID == 10)
                    {
                        alert("Please enter BrNet User ID");
                        document.getElementById("<%= txtUserID.ClientID %>").focus();
                        return false;
                    }  
                    else if(APP_ID == 21)  
                    {
                        alert("Please enter Aadhar User ID");
                        document.getElementById("<%= txtUserID.ClientID %>").focus();
                        return false;
                    }   
                    else if(APP_ID == 20)  
                    {
                        alert("Please enter Rubix User ID");
                        document.getElementById("<%= txtUserID.ClientID %>").focus();
                        return false;
                    }               
                }           
                else
                {
                    ExistingUserID = document.getElementById("<%= txtUserID.ClientID%>").value;
                    if(ExistingUserID.length < 6 && (APP_ID == 1 || APP_ID == 10) )
                    {
                        if (APP_ID == 1)
                            alert("Please enter Valid Profile UserID");
                        else if (APP_ID == 10)
                            alert("Please enter Valid BrNet UserID");
                        document.getElementById("<%= txtUserID.ClientID %>").focus();
                        return false;
                    }
                }

                if(TYPE_ID == 67)
                {
                    if(document.getElementById("<%= txtFromDt.ClientID %>").value !="" && document.getElementById("<%= txtToDt.ClientID %>").value ==""){
                        alert("Select Disable To Date");
                        document.getElementById("<%= txtToDt.ClientID %>").focus();
                        return false;
                    }
                    else if(document.getElementById("<%= txtFromDt.ClientID %>").value =="" && document.getElementById("<%= txtToDt.ClientID %>").value !=""){
                        alert("Select Disable From Date");
                        document.getElementById("<%= txtFromDt.ClientID %>").focus();
                        return false;
                    }
                    else if(document.getElementById("<%= txtFromDt.ClientID %>").value =="" && document.getElementById("<%= txtToDt.ClientID %>").value ==""){
                        alert("Select Disable From Date and To Date");
                        document.getElementById("<%= txtFromDt.ClientID %>").focus();
                        return false;
                    }
                    else
                    {
                        FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
                        ToDt = document.getElementById("<%= txtToDt.ClientID %>").value;
                    }
                }
            }
        } 
        
        //Added by Vidya for profile old and new user id ---begin
        
        var oldProfUserID = "";
        var newProfUserID = "";
        if(APP_ID == 1 || APP_ID == 18){
            if (TYPE_ID == 105)
            {
                oldProfUserID = document.getElementById("<%= txtEmpCode.ClientID%>").value;
                newProfUserID = document.getElementById("<%= txtEmpCode.ClientID%>").value;
            }
            else if(TYPE_ID == 27)
            {
                oldProfUserID = document.getElementById("<%= txtOldProfID.ClientID%>").value;
                newProfUserID = document.getElementById("<%= txtNewProfID.ClientID%>").value;
                if(oldProfUserID == ""){
                    alert("Enter Old Profile User ID");
                    document.getElementById("<%= txtOldProfID.ClientID %>").focus();
                    return false;
                }
                if(newProfUserID == ""){
                    alert("Enter New Profile User ID");
                    document.getElementById("<%= txtNewProfID.ClientID %>").focus();
                    return false;
                }
            }
        }
        //Added by Vidya for profile old and new user id ---end

        //Added by Vidhya for profile Transfer type ---begin
        
        var transferType = "";
       
       if(APP_ID == 1){
            if (TYPE_ID == 3)
            {
                transferType = document.getElementById("<%= cmbTransferType.ClientID%>").value;
                if(transferType == "-1"){
                    alert("Select Transfer Type");
                    document.getElementById("<%= cmbTransferType.ClientID %>").focus();
                    return false;
                }
            }          
        }


        //Added by Vidya for profile old and new user id ---end

        var HandOverTo = "";    
        //<Summary>Changed by 40020 on 08-Feb-2021</Summary>
        //<Comments>HandOver added for Change Role also</Comments>
        if(APP_ID == 11 && (TYPE_ID == 59 || TYPE_ID == 60 || TYPE_ID == 62))
        {
            if(document.getElementById("<%= txtHandOver.ClientID %>").value=="")
            {
                alert("Please enter Hand Over Employee Code");
                document.getElementById("<%= txtHandOver.ClientID %>").focus();
                return false;
            }
            else
            {
                HandOverTo=document.getElementById("<%= txtHandOver.ClientID %>").value;
                if(HandOverTo.length < 3)
                {
                    alert("Please enter Valid Employee Code");
                    document.getElementById("<%= txtHandOver.ClientID %>").focus();
                    return false;
                }
            }
            //<Summary>Changed by 40020 on 08-Feb-2021</Summary>
            //<Comments>Transfer Type validation added</Comments>
            if(TYPE_ID==60)
            {
                transferType = document.getElementById("<%= cmbTransferType.ClientID%>").value;
                if(transferType == "-1"){
                    alert("Select Transfer Type");
                    document.getElementById("<%= cmbTransferType.ClientID %>").focus();
                    return false;
                }
            }
        }

        //<Summary>Changed by 40020 on 08-Feb-2021</Summary>
        //<Comments>CASA Transfer Type validation added</Comments>
         if(APP_ID == 9)
        {
            if(TYPE_ID==42)
            {
                transferType = document.getElementById("<%= cmbTransferType.ClientID%>").value;
                if(transferType == "-1"){
                    alert("Select Transfer Type");
                    document.getElementById("<%= cmbTransferType.ClientID %>").focus();
                    return false;
                }
            }
        }


        if(APP_ID == 10 && TYPE_ID == 50)
        {
            if (document.getElementById("<%= cmbProofOne.ClientID %>").value == "-1") 
            {
                alert("Select ID Proof One");
                document.getElementById("<%= cmbProofOne.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbProofTwo.ClientID %>").value == "-1") 
            {
                alert("Select ID Proof Two");
                document.getElementById("<%= cmbProofTwo.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtProofOne.ClientID %>").value == "") 
            {
                alert("Enter ID Proof Number");
                document.getElementById("<%= txtProofOne.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtProofTwo.ClientID %>").value == "-1") 
            {
                alert("Enter ID Proof Number");
                document.getElementById("<%= txtProofTwo.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbProofOne.ClientID %>").value == document.getElementById("<%= cmbProofTwo.ClientID %>").value) 
            {
                alert("Kindly Select Different Type of Proof");
                document.getElementById("<%= cmbProofTwo.ClientID %>").focus();
                return false;
            }
            DOB=document.getElementById("<%= txtDOB.ClientID%>").value;
            JoinDt=document.getElementById("<%= txtJoinDt.ClientID%>").value;
            PrfNameOne=document.getElementById("<%= cmbProofOne.ClientID %>").options[document.getElementById("<%= cmbProofOne.ClientID %>").selectedIndex].text;
            PrfNoOne=document.getElementById("<%= txtProofOne.ClientID %>").value;
            PrfNameTwo=document.getElementById("<%= cmbProofTwo.ClientID %>").options[document.getElementById("<%= cmbProofTwo.ClientID %>").selectedIndex].text;
            PrfNoTwo=document.getElementById("<%= txtProofTwo.ClientID %>").value;         
        } 
        var StrTeam = "";
        var StrRole = "";
        var StrBranch =  "";

        //<Summary>Change done by 40020 on 05-Jan-2021</Summary>
        //<Remarks>Profile-MIS Multiple Report Code selection added</Remarks>
        var StrReport =  "";

        var SelectedItems = ""; 
        if (APP_ID == 14) // CRM
        {   
            if(TYPE_ID == 68) // New User
            {
                SelectedItems = $('#chkCrmTeam').val(); 
                if (SelectedItems == null || SelectedItems=='multiselect-all') {
                    alert("Select Team");
                    return false;
                }
                var StrVal = SelectedItems.toString();
                var StrTeam = StrVal.replace('multiselect-all,', '');
                //----------- ----------- -------------
                SelectedItems = ""; 
                SelectedItems = $('#chkCrmRole').val(); 
                if (SelectedItems == null || SelectedItems=='multiselect-all') {
                    alert("Select Role");
                    return false;
                }
                var StrRVal = SelectedItems.toString();
                var StrRole = StrRVal.replace('multiselect-all,', '');  
                //--------------------------------------
            } 
            if(TYPE_ID == 73) // Multiple Branch
            {
                SelectedItems = "";
                SelectedItems = $('#chkCrmBranch').val(); 
                if (SelectedItems == null || SelectedItems=='multiselect-all') {
                    alert("Select Branch");
                    return false;
                }
                var StrVal = SelectedItems.toString();
                var StrBranch = StrVal.replace('multiselect-all,', '');
            }
            if(TYPE_ID == 74) // Multiple Role
            {
                SelectedItems = ""; 
                SelectedItems = $('#chkCrmRole').val(); 
                if (SelectedItems == null || SelectedItems=='multiselect-all') {
                    alert("Select Role");
                    return false;
                }
                var StrRVal = SelectedItems.toString();
                var StrRole = StrRVal.replace('multiselect-all,', ''); 
            }  
            if(TYPE_ID == 75) // Multiple Team
            {
                SelectedItems = ""; 
                 SelectedItems = $('#chkCrmTeam').val(); 
                if (SelectedItems == null || SelectedItems=='multiselect-all') {
                    alert("Select Team");
                    return false;
                }
                var StrVal = SelectedItems.toString();
                var StrTeam = StrVal.replace('multiselect-all,', '');
            }                
        }  
        //<Summary>Change done by 40020 on 04-Jan-2021</Summary>
        //<Remarks>Multiple Report Code selection added</Remarks>
        if(APP_ID==8) // Profile-MIS
        {
            if (TYPE_ID == 115)
            {
                transferType = document.getElementById("<%= cmbTransferType.ClientID%>").value;
                if(transferType == "-1"){
                    alert("Select Transfer Type");
                    document.getElementById("<%= cmbTransferType.ClientID %>").focus();
                    return false;
                }
            }    
            if(TYPE_ID == 116) // Multiple Report Codes
            {
                SelectedItems = "";
                SelectedItems = $('#chkReport').val(); 
                if (SelectedItems == null || SelectedItems=='multiselect-all') {
                    alert("Select Report Code");
                    return false;
                }
                var StrVal = SelectedItems.toString();
                var StrReport = StrVal.replace('multiselect-all,', '');
            }
            if(TYPE_ID == 117) // Multiple Branch
            {
                SelectedItems = "";
                SelectedItems = $('#chkCrmBranch').val(); 
                if (SelectedItems == null || SelectedItems=='multiselect-all') {
                    alert("Select Branch");
                    return false;
                }
                var StrVal = SelectedItems.toString();
                var StrBranch = StrVal.replace('multiselect-all,', '');
            }
        }

        if(APP_ID == 20){
            if (TYPE_ID == 120)
            {
                transferType = document.getElementById("<%= cmbTransferType.ClientID%>").value;
                if(transferType == "-1"){
                    alert("Select Transfer Type");
                    document.getElementById("<%= cmbTransferType.ClientID %>").focus();
                    return false;
                }
            }          
        }

        document.getElementById("<%= hdnValue.ClientID %>").value = "1Ø" + Branch + "Ø" + APP_ID + "Ø" + trans_branch_id + "Ø" + TYPE_ID+ "Ø" + Descr + "Ø" + contact + "Ø" + emailid + "Ø" + Emp_code + "Ø" + RoleID +"Ø" + CUG + "Ø" + Tellertype + "Ø" + FolderName + "Ø" + Storage + "Ø" + Accesstype + "Ø" + NewMailID + "Ø" + ServiceRequestNo + "Ø" + OldRoleID + "Ø" +OldTransferID + "Ø" +DL + "Ø" + PrevEmpCode + "Ø" + PrfNameOne + "Ø" + PrfNoOne + "Ø" + PrfNameTwo + "Ø" + PrfNoTwo  + "Ø" + ExistingUserID + "Ø" + HandOverTo + "Ø" + FromDt + "Ø" + ToDt + "Ø" + StrTeam + "Ø" + StrRole + "Ø" + StrBranch + "Ø" + oldProfUserID + "Ø" + newProfUserID + "Ø" + transferType + "Ø" + StrReport;            
    }
    function EmployeeOnChange() 
    {   var APP_ID	= document.getElementById("<%= cmbCategory.ClientID%>").value;
        var EmpCode = document.getElementById("<%= txtEmpCode.ClientID%>").value;
        var TYPE_ID	= document.getElementById("<%= cmbType.ClientID %>").value;
        
        if (EmpCode != "")
            ToServer("1Ø" + EmpCode +"Ø" + document.getElementById("<%= cmbCategory.ClientID%>").value + "Ø" +TYPE_ID, 1);
    }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return RequestOnchange()
            // ]]>
        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 80%; margin: 0px auto;">
        <tr id="branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="App">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Application
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbCategory" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Type">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Type
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbType" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="ServiceRequestNo">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Service Now Ticket No
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtServiceRequestNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15" />
            </td>
        </tr>
        <tr id="EmpCode">
            <td style="width: 25%;">
                &nbsp;
                <asp:HiddenField ID="hid_Empcode" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Employee Code
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="20%" class="NormalText" MaxLength="50" onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="Name">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Emp Name
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpName" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="60%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
            </td>
        </tr>
        <tr id="Dep">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_Dtls" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Department
            </td>
            <td style="width: 63%; text-align: left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtDepartment" runat="server" Width="60%" class="ReadOnlyTextBox"
                    ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr id="Des">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Designation
            </td>
            <td style="width: 63%; text-align: left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesignation" runat="server" Width="60%" class="ReadOnlyTextBox"
                    ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr id="Doj">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Date of Join
            </td>
            <td style="width: 63%; text-align: left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtDateOfJoin" runat="server" Width="60%" class="ReadOnlyTextBox"
                    ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr id="ReportingTo">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Reporting Officer
            </td>
            <td style="width: 63%; text-align: left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtReportingTo" runat="server" Width="60%" class="ReadOnlyTextBox"
                    ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr id="Status">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Employee Status
            </td>
            <td style="width: 63%; text-align: left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpStatus" runat="server" Width="60%" class="ReadOnlyTextBox"
                    ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr id="Cug">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                CUG
            </td>
            <td style="width: 63%; text-align: left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtCUG" runat="server" Width="60%" class="NormalTextBox"
                    onkeypress='return NumericCheck(event)' MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr id="Usertype">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                User Type
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbUserType" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="49%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Role1">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Old Role
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbRoleOld" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="20.5%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                <span id="OldProfID" style="display: none;">&nbsp; &nbsp; Profile ID &nbsp; &nbsp;<asp:TextBox
                    ID="txtOldProfID" runat="server" Width="30%" class="NormalTextBox" MaxLength="30"></asp:TextBox>
                </span>
            </td>
        </tr>
        <tr id="Role">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                New Role
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbRole" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="20.5%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                <span id="NewProfID" style="display: none;">&nbsp; &nbsp; Profile ID &nbsp; &nbsp;<asp:TextBox
                    ID="txtNewProfID" runat="server" Width="30%" class="NormalTextBox" MaxLength="30"></asp:TextBox>
                </span>
            </td>
        </tr>
        <tr id="Teller">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Teller Type
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbTeller" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1"> Head Teller </asp:ListItem>
                    <asp:ListItem Value="2"> Teller Only</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="PrevEmpCode">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Previously Handled By (EmpCode)
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtPrevCode" runat="server" Width="60%" class="NormalTextBox"
                    onkeypress='return NumericCheck(event)' MaxLength="5"></asp:TextBox>
            </td>
        </tr>
        <tr id="Transfer1">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Transfer From
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbTransBranchOld" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Transfer">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Transfer To
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbTransBranch" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Transfer2">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Transfer Type
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbTransferType" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1">Transfer</asp:ListItem>
                    <asp:ListItem Value="2">Deputation</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Tr1">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_Post" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Description
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<textarea id="txtDetails" class="NormalText" cols="20" name="S1" rows="3"
                    onkeypress='return TextAreaCheck(event)' maxlength="1000" style="width: 70%"></textarea>
            </td>
        </tr>
        <tr id="FolderName">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Folder Name
            </td>
            <td style="width: 63%; text-align: left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtFolderName" runat="server" Width="60%" class="NormalTextBox"
                    onkeypress='return AlphaNumericCheck(event)' MaxLength="25"></asp:TextBox>
            </td>
        </tr>
        <tr id="Storage">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Storage Requirement
            </td>
            <td style="width: 63%; text-align: left;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbStorage" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1"> 1 GB </asp:ListItem>
                    <asp:ListItem Value="2"> 2 GB</asp:ListItem>
                    <asp:ListItem Value="3"> 3 GB</asp:ListItem>
                    <asp:ListItem Value="4"> 4 GB</asp:ListItem>
                    <asp:ListItem Value="5"> 5 GB</asp:ListItem>
                    <asp:ListItem Value="6"> 6 GB</asp:ListItem>
                    <asp:ListItem Value="7"> 7 GB</asp:ListItem>
                    <asp:ListItem Value="8"> 8 GB</asp:ListItem>
                    <asp:ListItem Value="9"> 9 GB</asp:ListItem>
                    <asp:ListItem Value="10"> 10 GB</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="DL">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                <asp:Label ID="lblDL" runat="server" Text="Distribution List"></asp:Label>
            </td>
            <td style="width: 63%; text-align: left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtDL" runat="server" Width="60%" class="NormalTextBox"
                    MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr id="Access">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Access Type
            </td>
            <td style="width: 63%; text-align: left;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbAccess" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1"> Read </asp:ListItem>
                    <asp:ListItem Value="2"> Write</asp:ListItem>
                    <asp:ListItem Value="3"> Both </asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="contactno">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Contact No.
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtContactNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="10" onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="Email">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Official Email Id
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmailid" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" MaxLength="50" />
                <asp:Label ID="lblRequestMail" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr id="rDOB">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                DOB
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtDOB" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="10" onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="rJDate">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Joining Date&nbsp;
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtJoinDt" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="10" onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="rProofCmpOne">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                ID Proof 1
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbProofOne" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1"> Driving Licence </asp:ListItem>
                    <asp:ListItem Value="2"> Aadhar Card</asp:ListItem>
                    <asp:ListItem Value="3"> PAN Card</asp:ListItem>
                    <asp:ListItem Value="4"> PRAN Card</asp:ListItem>
                    <asp:ListItem Value="5"> Passport</asp:ListItem>
                    <asp:ListItem Value="6"> Voters ID</asp:ListItem>
                    <asp:ListItem Value="7"> Ration Card</asp:ListItem>
                    <asp:ListItem Value="8"> Bank Passbook</asp:ListItem>
                    <asp:ListItem Value="9"> LPG ID</asp:ListItem>
                    <asp:ListItem Value="10"> NREGA Job Card</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="rPrNoOne">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                ID Proof 1 No.
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtProofOne" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="25" />
            </td>
        </tr>
        <tr id="rProofCmpTwo">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                ID Proof 2
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbProofTwo" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1"> Driving Licence </asp:ListItem>
                    <asp:ListItem Value="2"> Aadhar Card</asp:ListItem>
                    <asp:ListItem Value="3"> PAN Card</asp:ListItem>
                    <asp:ListItem Value="4"> PRAN Card</asp:ListItem>
                    <asp:ListItem Value="5"> Passport</asp:ListItem>
                    <asp:ListItem Value="6"> Voters ID</asp:ListItem>
                    <asp:ListItem Value="7"> Ration Card</asp:ListItem>
                    <asp:ListItem Value="8"> Bank Passbook</asp:ListItem>
                    <asp:ListItem Value="9"> LPG ID</asp:ListItem>
                    <asp:ListItem Value="10"> NREGA Job Card</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="rPrNoTwo">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                ID Proof 2 No.
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtProofTwo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="25" />
            </td>
        </tr>
        <tr id="rUserID">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                User ID
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtUserID" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="25" />
            </td>
        </tr>
        <tr id="rHandOver">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Hand Over To
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtHandOver" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="25" onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="rFromDt">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Disable From
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtFromDt" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="25" />
                <ajaxToolkit:CalendarExtender ID="txtFromDt_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="txtFromDt" Format="dd MMM yyyy">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
        <tr id="rToDt">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Disable To
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtToDt" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="25" />
                <ajaxToolkit:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="txtToDt" Format="dd MMM yyyy">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
        <tr id="rCrmTeam">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; align: left;">
                Team
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<select id="chkCrmTeam" multiple="multiple" style='width: 50%; text-align: center;'></select>
            </td>
        </tr>
        <tr id="rCrmRole">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; align: left;">
                Role
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<select id="chkCrmRole" multiple="multiple" style='width: 50%; text-align: center;'></select>
            </td>
        </tr>
        <tr id="rCrmBranch">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; align: left;">
                Branch
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<select id="chkCrmBranch" multiple="multiple" style='width: 50%; text-align: center;'></select>
            </td>
        </tr>
        <%--//<Summary>Change done by 40020 on 04-Jan-2021</Summary>
            //<Remarks>Multiple Report Code selection added</Remarks>--%>
        <tr id="rReport">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; align: left;">
                Report Code
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<select id="chkReport" multiple="multiple" style='width: 50%; text-align: center;'></select>
            </td>
        </tr>
        <tr id="rCrmTeam1">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; align: left;">
                Team
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<textarea id="txtTeam" class="ReadOnlyTextBox" cols="20" name="S1" rows="3"
                    onkeypress='return TextAreaCheck(event)' maxlength="1000" style="width: 50%"></textarea>
            </td>
        </tr>
        <tr id="rCrmRole1">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; align: left;">
                Role
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<textarea id="txtRole" class="ReadOnlyTextBox" cols="20" name="S1" rows="3"
                    onkeypress='return TextAreaCheck(event)' maxlength="1000" style="width: 50%"></textarea>
            </td>
        </tr>
        <tr id="rCrmBranch1">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; align: left;">
                Branch
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<textarea id="txtBranch" class="ReadOnlyTextBox" cols="20" name="S1"
                    rows="3" onkeypress='return TextAreaCheck(event)' maxlength="1000" style="width: 50%"></textarea>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:HiddenField ID="hdn_RetailFlg" runat="server" />
                <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
                <asp:HiddenField ID="hdnCrmTeam" runat="server" />
                <asp:HiddenField ID="hdnCrmRole" runat="server" />
                <asp:HiddenField ID="hdnCrmBranch" runat="server" />
                <asp:HiddenField ID="hdnReport" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                &nbsp;
            </td>
        </tr>
    </table>
    <configuration>
    <system.web>
    <compilaton debug="true" targetFramework="4.0"></compilaton>
    </system.web>
    </configuration>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
