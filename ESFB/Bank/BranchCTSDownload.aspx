﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="BranchCTSDownload.aspx.vb" Inherits="BranchCTSDownload" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <link href="../Style/ExportMenu.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript">return window_onload()</script>
        <script language="javascript" type="text/javascript">
       
   
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
     
       

        function window_onload() { 
            var FilterID =document.getElementById("<%= ddlFilter.ClientID %>").value;
            if(FilterID>0 && FilterID<8)
            {
                document.getElementById("<%= txtFilter.ClientID %>").disabled=false;
                document.getElementById("<%= txtFilter.ClientID %>").focus();
            }
            else
            {
                document.getElementById("<%= txtFilter.ClientID %>").disabled=true ;
                document.getElementById("<%= txtFilter.ClientID %>").value="";
            }     
            btnSearch_onclick();
        }
        function AutoRefresh( t ) {
	        setTimeout("location.reload(true);", t);
        }
        function table_Fill()
        {
            var tab="";
            var row_bg = 0;

            tab += "<div id='ScrollDiv' style='width:100%; height:auto;overflow: scroll;margin: 0px auto; background-color:#E0C2C2;' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px;>";
                  
            tab += "<td style='width:3%;text-align:center' >#</td>";                
            tab += "<td style='width:11%;text-align:left' >Request ID</td>";
            tab += "<td style='width:81%;text-align:left' >Branch</td>";              
            tab += "<td style='width:5%;text-align:center' ></td>";                
            tab += "</tr>";
           // tab += "</table></div>";
            if(document.getElementById("<%= hid_Items.ClientID %>").value=="")
            {
                document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none'; 
                return;
            }            
         
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';  
            //tab += "<div id='ScrollDiv' style='width:100%; height:274px;overflow: scroll;margin: 0px auto;' class=mainhead >";
           // tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='left'>";     
            
            var row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");                               
            for (n = 0; n <= row.length - 1; n++) 
            {
                var col = row[n].split("µ");                    
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr class='sub_first'; style='text-align:center;padding-left:20px;'>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class='sub_second'; style='text-align:center; padding-left:20px;'>";
                }                    
                var i = n + 1; 

                tab += "<td style='width:3%;text-align:center;'>" + i + "</td>";
                
                tab += "<td style='width:11%;text-align:left'>"+ col[0] +"</td>"; //TicketNo
                tab += "<td style='width:81%;text-align:left'>" + col[1] + "</td>"; //Branch
               
                if(col[2]==0)
                    tab += "<td style='width:5%;text-align:center'></td>";                        
                else
                {  
                    tab+="<td  style='width:5%;text-align:center; '><img id='ViewReport' src='../Image/attchment2.png' onclick='viewReport("+ col[0] +","+ col[2]+")' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' >";			               
                    tab+="</td>";                    
                }
               
                tab += "</tr>";                
            }
            tab += "</table></div></div></div>";            
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;  
        }
        
        function viewReport(ID,AttachCount)
        {
            if(AttachCount==1)
                window.open("Reports/ShowAttachment.aspx?RequestID=" + btoa(ID));
            else
                window.open("Reports/ShowMultipleAttachments.aspx?RequestID=" + btoa(ID),"_blank");
            return false;
        }
        
        function btnSearch_onclick() {
        
            var row = document.getElementById("<%= hid_ItemAll.ClientID %>").value.split("¥"); 
            document.getElementById("<%= hid_Items.ClientID %>").value="";      
            var SearchText=document.getElementById("<%= txtFilter.ClientID %>").value;
            var rootString ="";
            var SearchTypeID=document.getElementById("<%= ddlFilter.ClientID %>").value;                      
            for (n = 0; n <= row.length - 1; n++) 
            {
                var col = row[n].split("µ");   
                switch (SearchTypeID) 
                {
//                    "1">TICKET NO
//                    "2">BRANCH
//                    "3">ASSIGN DATE
//                    "4">GROUP
//                    "5">SUB GROUP
//                    "6">PROBLEM

                    case "1":                  
                        rootString=col[0].toUpperCase(); //Ticket Number
                        break; 
                    case "2":
                        rootString=col[1].toUpperCase(); //Branch Name
                        break; 
                    
                }     
                if(SearchTypeID!="8")       
                {      
                    if(rootString.indexOf(SearchText.toUpperCase()) > -1)
                    {
                        document.getElementById("<%= hid_Items.ClientID %>").value+="¥"+ row[n];              
                    }
                }
                else
                {
                    if(col[7]=="NA")
                    document.getElementById("<%= hid_Items.ClientID %>").value+="¥"+ row[n];
                }  
            } 
            document.getElementById("<%= hid_Items.ClientID %>").value=document.getElementById("<%= hid_Items.ClientID %>").value.substring(1); 
            table_Fill();
        }

        function FilterOnChange()
        {
            var FilterID=document.getElementById("<%= ddlFilter.ClientID %>").value;    
            if(FilterID>0 && FilterID<8)
            {
                document.getElementById("<%= txtFilter.ClientID %>").disabled=false;
                document.getElementById("<%= txtFilter.ClientID %>").focus();
            }
            else
            {
                document.getElementById("<%= txtFilter.ClientID %>").disabled=true ;
                document.getElementById("<%= txtFilter.ClientID %>").value="";
            }
        }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    </html>
   
    <br />
    <br />
    <div style="width: 90%; text-align: center; margin: 0px auto; height: 35px;background-color:silver;">
        <table style="width: 100%; margin: 0px auto;">
            <tr id="Tr1">
                <td style="width: 15%; text-align: Left;">
                    Filter&nbsp;By&nbsp;
                </td>
                <td style="width: 35%; text-align: left;">
                    <asp:DropDownList ID="ddlFilter" runat="server" Width="90%" class="NormalText">
                        <asp:ListItem Value="-1">ALL</asp:ListItem>
                        <asp:ListItem Value="2">BRANCH</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 15%; text-align: Left;">
                    Search Value
                </td>
                <td style="width: 35%; text-align: left;">
                    <asp:TextBox ID="txtFilter" runat="server" class="NormalText" Style="width: 60%;"></asp:TextBox>
                    &nbsp;&nbsp;<input id="btnSearch" style="font-family: cambria; cursor: pointer; width: 26%;
                        height: 25px;" type="button" value="SEARCH" onclick="return btnSearch_onclick()" />
                </td>
            </tr>
        </table>
    </div>
    <table class="style1" style="width: 90%; margin: 0px auto;">
        <tr>
            <td style="width: 15%; text-align: Left;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 15%; text-align: Left;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 15%;" colspan="4">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="4">
                <br />
                <input id="btnRefresh" style="font-family: cambria; cursor: pointer; width: 6%;"
                    type="button" value="REFRESH" onclick="return btnRefresh_onclick()" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;<asp:HiddenField ID="hid_Items"
                        runat="server" />
                <asp:HiddenField ID="hid_Value" runat="server" />
                <asp:HiddenField ID="hid_EmpDetails" runat="server" />
                <asp:HiddenField ID="hid_ItemAll" runat="server" />
                <br />
                <asp:HiddenField ID="hid_User" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="4">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <br />
</asp:Content>
