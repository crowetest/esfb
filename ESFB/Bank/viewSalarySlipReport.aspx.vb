﻿
Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports iTextSharp.text



Partial Class viewSalarySlipReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect

    Dim DT As New DataTable
    Dim DTExcel As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New System.Web.UI.WebControls.Table
    Dim tb1 As New System.Web.UI.WebControls.Table
    Dim Months As String
    Dim WebTools As New WebApp.Tools

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Months = CStr(Request.QueryString.Get("Month"))
        Dim EmpCode As Integer = CInt(Request.QueryString.Get("EmpCode"))
        Dim EmpName As String = CStr(Request.QueryString.Get("EmpName"))
        hdnFormID.Value = CInt(Request.QueryString.Get("ID"))
        DT = DB.ExecuteDataSet("select convert(varchar(100),isnull([Employee_No],0))+'~'+ convert(varchar(100),isnull([Name],'')) +'~'+ convert(varchar(100),isnull(b.Designation_Name,'')) +'~'+ convert(varchar(100),isnull(c.Department_Name,''))  " & _
                " +'~'+convert(varchar(100),isnull(d.branch_name,''))  +'~'+convert(varchar(100),isnull([Designation],''))  +'~'+convert(varchar(100),isnull([Cadre],''))    +'~'+convert(varchar(100),isnull([JoinDate],'')) +'~'+convert(varchar(100),isnull([ESIC_IP_No],0)) +'~'+  " & _
                " convert(varchar(100),isnull([EMP_EFFECTIVE_WORK_DAYS],0)) +'~'+convert(varchar(100),isnull([LOP_NoofDays],0)) +'~'+convert(varchar(100),isnull([BASIC],0))  +'~'+convert(varchar(100),isnull([DA],0))  +'~'+convert(varchar(100),isnull([HRA],0))   " & _
                " +'~'+convert(varchar(100),isnull([Special_Allowance],0))  +'~'+convert(varchar(100),isnull([Local_Allowance],0)) +'~'+convert(varchar(100),isnull([Medical_Allowance],0))    +'~'+convert(varchar(100),isnull([HA],0))+'~'+  " & _
                " convert(varchar(100),isnull([Performance_Allowance],0))   +'~'+convert(varchar(100),isnull([Conveyance],0)) +'~'+convert(varchar(100),isnull([Other_Allowance],0))  +'~'+convert(varchar(100),isnull([FA],0))  " & _
                " +'~'+convert(varchar(100),isnull([GROSS],0)) +'~'+convert(varchar(100),isnull([PF],0)) +'~'+convert(varchar(100),isnull([ESIC],0))   +'~'+convert(varchar(100),isnull([SE_Scheme],0))  +'~'+  " & _
                " convert(varchar(100),isnull([ESWT],0))+'~'+convert(varchar(100),isnull([CHARITY_FUND],0))  +'~'+convert(varchar(100),isnull([Staff_Advance],0))+'~'+convert(varchar(100),isnull([Staff_Loan],0)) +'~'+  " & _
                " convert(varchar(100), isnull([Vehicle_Loan],0))  +'~'+convert(varchar(100),isnull([Staff_Welfare_Fund1],0)) +'~'+convert(varchar(100),isnull([Medical_Insurance],0))  +'~'+convert(varchar(100),isnull([Telephone],0)) +'~'+  " & _
                " convert(varchar(100),isnull([ROP],0))  +'~'+convert(varchar(100),isnull([TOTAL_DEDUCTIONS],0))  +'~'+   convert(varchar(100),isnull([other_Pay_arrear],0))  +'~'+convert(varchar(100),isnull([NET_PAY],0))     " & _
                " +'~'+convert(varchar(100),isnull([Ac_No],0)) +'~'+convert(varchar(100),isnull([Bank],''))  +'~'+convert(varchar(100),isnull([IFSC_Code],0)) +'~'+convert(varchar(100),isnull([EMail_Id],'')) +'~'+convert(varchar(100),isnull([PF_NO],''))  +'~'+convert(varchar(100),isnull([PAN_NO],'')) +'~'+convert(varchar(100),isnull([PROV_TAX],0)) +'~'+convert(varchar(100),isnull([OTHER_DEDUCT],0)) +'~'+convert(varchar(100),isnull([METRO_ALLOW],0)) FROM EMP_MASTER a,DESIGNATION_MASTER b,DEPARTMENT_MASTER c,Branch_master d, excel_salary_data e " & _
                " where a.Designation_ID = b.Designation_ID And a.Department_ID = c.Department_ID And a.branch_id = d.branch_id and a.emp_code=e.[Employee_No] and [Employee_No]=" + EmpCode.ToString() + " and DATENAME(MONTH, currentmonthyear)+'-'+convert(varchar(100),YEAR(currentmonthyear))='" + Months.ToString() + "'").Tables(0)
        'DTExcel = DB.ExecuteDataSet("select convert(varchar(100),isnull([Employee_No],0)) as, convert(varchar(100),isnull([Name],''))  as, convert(varchar(100),isnull(b.Designation_Name,''))  as, convert(varchar(100),isnull(c.Department_Name,''))  " & _
        '       "  as,convert(varchar(100),isnull(d.branch_name,''))   as,convert(varchar(100),isnull([Designation],''))   as,convert(varchar(100),isnull([Cadre],''))     as,convert(varchar(100),isnull([JoinDate],''))  as,convert(varchar(100),isnull([ESIC_IP_No],0))  as,  " & _
        '       " convert(varchar(100),isnull([EMP_EFFECTIVE_WORK_DAYS],0))  as,convert(varchar(100),isnull([LOP_NoofDays],0))  as,convert(varchar(100),isnull([BASIC],0))   as,convert(varchar(100),isnull([DA],0))   as,convert(varchar(100),isnull([HRA],0))   " & _
        '       "  as,convert(varchar(100),isnull([Special_Allowance],0))   as,convert(varchar(100),isnull([Local_Allowance],0))  as,convert(varchar(100),isnull([Medical_Allowance],0))     as,convert(varchar(100),isnull([HA],0)) as,  " & _
        '       " convert(varchar(100),isnull([Performance_Allowance],0))    as,convert(varchar(100),isnull([Conveyance],0))  as,convert(varchar(100),isnull([Other_Allowance],0))   as,convert(varchar(100),isnull([FA],0))  " & _
        '       "  as,convert(varchar(100),isnull([GROSS],0))  as,convert(varchar(100),isnull([PF],0))  as,convert(varchar(100),isnull([ESIC],0))    as,convert(varchar(100),isnull([Staff_Welfare_FUND],0))   as,  " & _
        '       " convert(varchar(100),isnull([ESWT],0)) as,convert(varchar(100),isnull([CHARITY_FUND],0))   as,convert(varchar(100),isnull([Staff_Advance],0)) as,convert(varchar(100),isnull([Staff_Loan],0))  as,  " & _
        '       " convert(varchar(100), isnull([Vehicle_Loan],0))   as,convert(varchar(100),isnull([Staff_Welfare_Fund1],0))  as,convert(varchar(100),isnull([Medical_Insurance],0))   as,convert(varchar(100),isnull([Telephone],0))  as,  " & _
        '       " convert(varchar(100),isnull([ROP],0))   as,convert(varchar(100),isnull([TOTAL_DEDUCTIONS],0))   as,   convert(varchar(100),isnull([other_Pay_arrear],0))   as,convert(varchar(100),isnull([NET_PAY],0))     " & _
        '       "  as,convert(varchar(100),isnull([Ac_No],0))  as,convert(varchar(100),isnull([Bank],''))   as,convert(varchar(100),isnull([IFSC_Code],0))  as,convert(varchar(100),isnull([EMail_Id],''))  as,convert(varchar(100),isnull([PF_NO],''))   as,convert(varchar(100),isnull([PAN_NO],'')) FROM EMP_MASTER a,DESIGNATION_MASTER b,DEPARTMENT_MASTER c,Branch_master d, excel_salary_data e " & _
        '       " where a.Designation_ID = b.Designation_ID And a.Department_ID = c.Department_ID And a.branch_id = d.branch_id and a.emp_code=e.[Employee_No] and [Employee_No]=" + EmpCode.ToString() + " and DATENAME(MONTH, currentmonthyear)+'-'+convert(varchar(100),YEAR(currentmonthyear))='" + Months.ToString() + "'").Tables(0)

        hdnDtls.Value = DT.Rows(0).Item(0)
        hdnMonth.Value = Months
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    'Protected Sub cmd_Word_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Word.Click
    '    Response.ContentType = "application/pdf"
    '    Response.AddHeader("content-disposition", "attachment;filename=Panel.pdf")
    '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '    Dim sw As New StringWriter()
    '    Dim hw As New HtmlTextWriter(sw)
    '    pnDisplay.RenderControl(hw)
    '    Dim sr As New StringReader(sw.ToString())
    '    Dim pdfDoc As New Document(PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
    '    Dim htmlparser As New HTMLWorker(pdfDoc)
    '    PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
    '    pdfDoc.Open()
    '    htmlparser.Parse(sr)
    '    pdfDoc.Close()
    '    Response.Write(pdfDoc)
    '    Response.End()

    'End Sub
End Class
