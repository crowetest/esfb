﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class SRDepartmentMaster
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim LV As New Leave
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
    Dim UserID As Integer
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1283) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "SR Department Master"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Dim UserID As Integer = CInt(Session("UserID"))
            DT = GF.GetQueryResult("select -1,'---Select Department ---' union select department_id,department_name from department_master order by 2")
            If DT Is Nothing Then
            Else
                GF.ComboFill(cmbDept, DT, 0, 1)
            End If
            Me.cmbDept.Attributes.Add("onchange", "return DeptOnChange()")
            Me.txtEmpCode.Attributes.Add("onchange", "return HeadOnChange()")
            Me.txtSubCode.Attributes.Add("onchange", "return SubHeadOnChange()")

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        UserID = CInt(Session("UserID"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1
                Dim DeptID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select case when a.department_head is null or a.department_head=0 then '' else convert(varchar,a.department_head) end+'Æ'+b.emp_name+'Æ'+a.email+'Æ'+case when a.department_head1 is null or a.department_head1=0 then '' else convert(varchar,a.department_head1) end+'Æ'+case when c.emp_name is null then '' else c.emp_name end from sr_department_master a left join emp_master c on a.department_head1=c.emp_code, emp_master b where a.department_head is not null and a.department_head=b.emp_code and a.department_id=" & DeptID & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn += DT.Rows(0)(0).ToString()
                Else
                    CallBackReturn += ""
                End If
            Case 2
                Try
                    Dim DeptID As Integer = CInt(Data(1))
                    Dim Err As Integer = DB.ExecuteNonQuery("delete from sr_department_master where department_id=" & DeptID & "")
                    If Err = 1 Then
                        CallBackReturn = CStr(0) + "ʘ" + "Deleted Successfully"
                    End If
                Catch ex As Exception
                    Response.Redirect("~/CatchException.aspx?ErrorNo=1")
                End Try
            Case 3
                Dim DeptID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select convert(varchar,a.department_head)+'Æ'+b.emp_name+'Æ'+a.email+'Æ'+case when a.department_head1 is null then '' else convert(varchar,a.department_head1) end+'Æ'+case when c.emp_name is null then '' else c.emp_name end from sr_department_master a left join emp_master c on a.department_head1=c.emp_code, emp_master b where a.department_head is not null and a.department_head=b.emp_code and a.department_id=" & DeptID & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn += DT.Rows(0)(0).ToString()
                Else
                    CallBackReturn += ""
                End If
            Case 4
                Dim EmpCode As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select emp_name from emp_master where emp_code=" & EmpCode & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn += DT.Rows(0)(0).ToString()
                Else
                    CallBackReturn += ""
                End If
            Case 6
                Dim DeptID As Integer = CInt(Data(1))
                Dim EmpCode As Integer = CInt(Data(2))
                Dim MailID As String = CStr(Data(3))
                Dim SubHead As Integer = CInt(Data(4))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(6) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@DeptID", SqlDbType.VarChar)
                    Params(1).Value = DeptID
                    Params(2) = New SqlParameter("@HeadCode", SqlDbType.VarChar)
                    Params(2).Value = EmpCode
                    Params(3) = New SqlParameter("@MailID", SqlDbType.VarChar)
                    Params(3).Value = MailID
                    Params(4) = New SqlParameter("@SubHead", SqlDbType.VarChar)
                    Params(4).Value = SubHead
                    Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(6).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_SRDEPT_MASTER", Params)
                    ErrorFlag = CInt(Params(5).Value)
                    Message = CStr(Params(6).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region
End Class
