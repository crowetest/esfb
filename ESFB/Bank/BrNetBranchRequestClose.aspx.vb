﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Configuration

Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq


Imports System.IO
Imports System.Net
Imports System.Web.Services
Imports System.Web.Script.Serialization
Partial Class BrNetBranchRequestClose
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
    Dim Mobileno As String
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1232) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            DT = DB.ExecuteDataSet("select c.app_request_id,c.app_id,j.app_name,c.created_branch_name +'-' +convert(varchar,c.created_branch),d.emp_code,upper(replace(case when l.emp_code is null then c.Emp_Name else l.account_title end,'.','')),e.App_Type_name,d.Remarks,g.App_role_name,case when to_branch=-1 then '' else to_branch_name+' - ' +convert(varchar,d.to_branch) + case when y.usb_code is null then '' else', DSC = ' + isnull(y.usb_name+' - '+convert(varchar,y.usb_code),'') end end as To_branch,d.mail_id as email,d.mobile as mobile ,c.department, c.Designation ,k.Order_ID ,c.level_id,K.LEVEL_NAME,c.folder_name,c.storage_in_gb ,case when c.access_type =1 then 'Read' when  c.access_type =2 then 'Write'  when  c.access_type =3 then 'R & W' else '' end  ,RequestMailID,e.app_type_id " &
                " ,'HO App Dt :'+convert(varchar,c.ho_approved_on)+' Reporting By : '+isnull(reporting_name,'')+'('+convert(varchar,isnull(reporting_to,''))+') HO:'+isnull(q.name,'')+'('+convert(varchar,isnull(q.emp_code,''))+')' as Approved_By ,c.request_no, case when ((c.app_id=10 and c.app_type_id in(53,50,78))) then dbo.shuffle(upper(substring(replace(c.Emp_Name,' ',''),1,1))+lower(substring(replace(case when l.emp_code is null then c.Emp_Name else l.account_title end,' ',''),2,3)))+'@123' else '' end as passwd,case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESCCO' else 'LBS' end,case when old_branch=-1 then '' else old_branch_name+ ' - ' +(convert(varchar,old_branch))+ case when x.usb_code is null then '' else ', DSC = ' + isnull(x.usb_name+' - '+convert(varchar,x.usb_code),'') end end as oldBranch,old_role_name  as oldRole,case when c.teller_id=1 then 'Head Teller' when c.teller_id=2 then " &
                " 'Cashier' when  c.teller_id=3 then 'Head Teller&Cashier' else '' end as TellerType,d.remarks,DL,case when c.app_type_id=50 then convert(varchar,c.dob,103) else null end DOB,case when c.app_type_id=50 then convert(varchar,c.joindate,103) else null end JDate,c.PrfNameOne,c.PrfNoOne,c.PrfNameTwo,c.PrfNoTwo,isnull(x.usb_name+' - '+convert(varchar,x.usb_code),'') MappedBranch,case when c.existUser_ID is null or c.existUser_ID='' then upper(substring(replace(replace(case when l.emp_code is null then c.Emp_Name else l.account_title end,' ',''),'.',''),1,3))+convert(varchar,d.emp_code) else upper(c.existUser_ID) end as UserID,c.disablefrom, c.disableto" &
                " from ESFB.dbo.app_level k ,ESFB.dbo.app_request_master c  WITH (NOLOCK)  left join esfb.dbo.app_level_email q on c.ho_approved_by=q.emp_code left join esfb.dbo.App_Merged_Branch_Master x on c.created_branch=x.retail_brcode left join esfb.dbo.APP_BRNET_REQUEST_BATCH_DTL l on c.App_request_id=l.App_request_id,ESFB.dbo.app_master j,ESFB.dbo.app_type_master e , " &
                " ESFB.dbo.app_dtl_profile d  left join ESFB.dbo.app_role_master g on d.Role_id=g.App_role  left join esfb.dbo.App_Merged_Branch_Master y on d.to_branch=y.retail_brcode where " &
                " j.app_id=c.app_id and k.app_id=c.app_id and c.App_type_id=e.App_type_id and c.App_request_id=d.App_request_id  and c.HO_Approved_status=1 " &
            " and c.level_id=k.level_id  and c.app_id in (10) and brnet_status=1 and c.closed_status is null order by c.ho_approved_on ").Tables(0)

            Me.Master.subtitle = "BR.Net Branch Request Closing"

            Dim decryptval As String
            Dim StrAttendance As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                decryptval = GF.Encrypt(DT.Rows(n)(0).ToString)
                StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString & "µ" & DT.Rows(n)(16).ToString & "µ" & DT.Rows(n)(17).ToString & "µ" & DT.Rows(n)(18).ToString & "µ" & DT.Rows(n)(19).ToString & "µ" & DT.Rows(n)(20).ToString & "µ" & DT.Rows(n)(21).ToString & "µ" & DT.Rows(n)(22).ToString & "µ" & DT.Rows(n)(23).ToString & "µ" & DT.Rows(n)(24).ToString & "µ" & DT.Rows(n)(25).ToString & "µ" & DT.Rows(n)(26).ToString & "µ" & DT.Rows(n)(27).ToString & "µ" & DT.Rows(n)(28).ToString & "µ" & DT.Rows(n)(29).ToString & "µ" & decryptval.ToString & "µ" & DT.Rows(n)(30).ToString & "µ" & DT.Rows(n)(31).ToString & "µ" & DT.Rows(n)(32).ToString & "µ" & DT.Rows(n)(33).ToString & "µ" & DT.Rows(n)(34).ToString & "µ" & DT.Rows(n)(35).ToString & "µ" & DT.Rows(n)(36).ToString & "µ" & DT.Rows(n)(37).ToString & "µ" & DT.Rows(n)(38).ToString & "µ" & DT.Rows(n)(39).ToString & "µ" & DT.Rows(n)(40).ToString
                If n < DT.Rows.Count - 1 Then
                    StrAttendance += "¥"
                End If
            Next

            hid_dtls.Value = StrAttendance
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim Mobile As String = CStr(Data(1))
        Dim dataval As String = CStr(Data(2))
        Dim Passwd As String = CStr(Data(3))
        Dim Status As Integer = CInt(Data(4))
        Dim Remarks As String = CStr(Data(5))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim Mobileno As String = ""
        Dim AppID As String = CStr(Data(6))
        Dim AppTypeID As String = CStr(Data(7))


        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try

            Dim Params(4) As SqlParameter
            Params(0) = New SqlParameter("@userID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(1).Value = BranchID
            Params(2) = New SqlParameter("@EMPDtl", SqlDbType.VarChar)
            Params(2).Value = dataval.Substring(1)
            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(3).Direction = ParameterDirection.Output
            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(4).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_APP_APPROVAL_CLOSE_BRNET", Params)
            ErrorFlag = CInt(Params(3).Value)
            Message = CStr(Params(4).Value)

            Dim RetStr() As String

            Dim MsgId As String

            Dim Count As Integer = 0
            Dim DT As New DataTable
            Dim StrUrl As String

            If ErrorFlag = 0 Then
                If Mobile <> "" Then
                    Mobileno = "91" + Mobile.ToString()
                    DT = DB.ExecuteDataSet("SELECT sender,username,password FROM ESFB.dbo.SMS_PORTAL WHERE STATUS_ID =1").Tables(0)
                    If DT.Rows.Count > 0 Then
                        Dim Sender As String = ""
                        Dim UserName As String = ""
                        Dim PassWord As String = ""
                        Sender = DT.Rows(0).Item(0).ToString()
                        UserName = DT.Rows(0).Item(1).ToString()
                        PassWord = DT.Rows(0).Item(2).ToString()
                        If AppID = 10 Then 'adid
                            If Passwd <> "" And Status = 1 Then
                                If CStr(AppTypeID) = "50" Then '53-unlock
                                    StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "Br.Net -Your Login password is " & Passwd & "  " & Remarks & "&GSM=" & Mobileno & ""
                                ElseIf CStr(AppTypeID) = "78" Then 'Reset True Cell
                                    StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "TrueCell -Your Login password is " & Passwd & "  " & Remarks & "&GSM=" & Mobileno & ""
                                Else
                                    StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "Br.Net -Your Login password is " & Passwd & "  " & Remarks & "&GSM=" & Mobileno & ""
                                End If

                            ElseIf Passwd = "" And Status = 1 Then
                                If CStr(AppTypeID) = "51" Then
                                    StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "Br.Net/Servosys userid has been disabled. " & "&GSM=" & Mobileno & ""
                                ElseIf CStr(AppTypeID) = "52" Then
                                    StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "Br.Net/Servosys userid has been transferred. " & "&GSM=" & Mobileno & ""
                                ElseIf CStr(AppTypeID) = "54" Then
                                    StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "Br.Net/Servosys userid role has been changed. " & "&GSM=" & Mobileno & ""
                                Else
                                    StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "Br.Net request has been updated. " & "&GSM=" & Mobileno & ""
                                End If
                            Else
                                StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "Br.Net/Servosys -Your Request has been rejected. " & "&GSM=" & Mobileno & ""
                            End If

                            RetStr = WEB_Request_Response(StrUrl, 1)

                        End If

                    End If

                End If
            End If
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString
    End Sub
#End Region

#Region "Function"
    Public Shared Function WEB_Request_Response(ByVal Request As String, ByVal Request_type As Integer) As String()

        'Added On 22-10-2020 by 40013 - Changed request from IT
        Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
        Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString

        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        If Request_type = 1 Then
            delimiters = {"<", ">", ">/", "</", "/"}
        Else
            delimiters = {"=", " "}
        End If
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)


        Return parts.ToArray()


    End Function
#End Region
End Class
