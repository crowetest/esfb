﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class HRRequestApproval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1174) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)




            DT = DB.ExecuteDataSet("select c.app_request_id,c.app_id,j.app_name,c.created_branch_name,d.emp_code, c.Emp_Name,e.App_Type_name,d.Remarks,g.App_role_name,to_branch_name as To_branch,d.mail_id as email,d.mobile as mobile ,c.department, c.Designation ,k.Order_ID ,c.level_id,K.LEVEL_NAME,c.folder_name,c.storage_in_gb ,case when c.access_type =1 then 'Read' when  c.access_type =2 then 'Write'  when  c.access_type =3 then 'R & W' else '' end  ,RequestMailID,e.app_type_id " & _
                " ,'Reporting By : '+isnull(reporting_name,'')+'('+convert(varchar,isnull(reporting_to,''))+')               HO :'+isnull(q.name,'')+'('+convert(varchar,isnull(q.emp_code,''))+')' as Approved_By ,c.service_request_no,c.DL  " & _
                " from ESFB.dbo.app_level k ,ESFB.dbo.app_request_master c  WITH (NOLOCK)  left join esfb.dbo.app_level_email q on c.ho_approved_by=q.emp_code,ESFB.dbo.app_master j,ESFB.dbo.app_type_master e ,    " & _
                " ESFB.dbo.app_dtl_profile d  left join ESFB.dbo.app_role_master g on d.Role_id=g.App_role  where   " & _
                "   j.app_id=c.app_id and k.app_id=c.app_id and c.App_type_id=e.App_type_id and c.App_request_id=d.App_request_id    " & _
                " and c.level_id=k.level_id  and hr_status =1 and (approved_status is null or (ho_approved_status is null and approved_status=1)) and (c.Approved_status is null or ( c.HO_Approved_status is null and  c.approved_status=1) )").Tables(0)



            Me.Master.subtitle = "HR -Branch Request Verification"



            Dim StrAttendance As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString & "µ" & DT.Rows(n)(16).ToString & "µ" & DT.Rows(n)(17).ToString & "µ" & DT.Rows(n)(18).ToString & "µ" & DT.Rows(n)(19).ToString & "µ" & DT.Rows(n)(20).ToString & "µ" & DT.Rows(n)(21).ToString & "µ" & DT.Rows(n)(22).ToString & "µ" & DT.Rows(n)(23).ToString & "µ" & DT.Rows(n)(24).ToString
                If n < DT.Rows.Count - 1 Then
                    StrAttendance += "¥"
                End If
            Next


            hid_dtls.Value = StrAttendance
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim dataval As String = CStr(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))



        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try

            Dim Params(4) As SqlParameter
            Params(0) = New SqlParameter("@userID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(1).Value = BranchID
            Params(2) = New SqlParameter("@EMPDtl", SqlDbType.VarChar)
            Params(2).Value = dataval.Substring(1)
            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(3).Direction = ParameterDirection.Output
            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(4).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_APP_APPROVAL_HR", Params)
            ErrorFlag = CInt(Params(3).Value)
            Message = CStr(Params(4).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString



    End Sub
#End Region

End Class
