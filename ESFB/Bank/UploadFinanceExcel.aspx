﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="UploadFinanceExcel.aspx.vb" Inherits="UploadFinanceExcel" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

       function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function InsertOnClick(){
           if (document.getElementById("<%= txtMonth.ClientID %>").value ==""){
                alert("Select Month");
                document.getElementById("<%= txtMonth.ClientID %>").focus();
                return false;
           }
           else{
                document.getElementById("<%= hdnDate.ClientID %>").value =document.getElementById("<%= txtMonth.ClientID %>").value;
           }
            
        }
    </script>
   
</head>
</html>
<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;" >
           <tr id="Tr1"> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Salary Month</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtMonth" class="NormalText" runat="server" Width="30%" 
                MaxLength="100" onkeypress="return false"></asp:TextBox>
            <asp:CalendarExtender ID="Months" runat="server" Enabled="True" Format="MMM yyyy"
                TargetControlID="txtMonth">
            </asp:CalendarExtender>
            </td>
            
       </tr>
        <tr id="branch"> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Upload File</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:FileUpload ID="FileUpload1" runat="server" />
            </td>
            
       </tr>
       
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                  <asp:Button ID="btnUpload" class="NormalText" runat="server"  Text="UPLOAD" />&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />&nbsp;<asp:HiddenField ID="hdnDate" runat="server" />
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

