﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Service_Request_Close.aspx.vb" Inherits="Service_Request_Close" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
     <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10pt;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10pt;color:#476C91;
        }
    </style>

    <script src="../Script/Validations.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
       
        
        function FromServer(Arg, Context) {
            
            if (Context == 1) {
                var Data = Arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("Service_Request_Close.aspx", "_self");
            }
          
            
        }
       
        function btnApprove_onclick() {
           
            
            UpdateValue();
           
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "") {
                alert("No Data For Update");
                return false;
            }
            
            var strempcode = document.getElementById("<%= hid_temp.ClientID%>").value;
            var Data = "1Ø" + strempcode ;
            ToServer(Data, 1);
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        
        function table_fill() {
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                document.getElementById("<%= pnFamily.ClientID %>").style.display = '';
             var row_bg = 0;
             var tab = "";
             tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
             tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
             tab += "<tr>";
             tab += "<td style='width:1%;text-align:center;'>Sl No</td>";
             tab += "<td style='width:5%;text-align:left' >Application</td>";
             tab += "<td style='width:3%;text-align:left'>Type</td>";
             tab += "<td style='width:5%;text-align:left' >Branch</td>";
             tab += "<td style='width:3%;text-align:left'>ServiceReqNo</td>";
             tab += "<td style='width:8%;text-align:left'>Emp_Name</td>";
             tab += "<td style='width:5%;text-align:left'>Dep_Name</td>";
             tab += "<td style='width:5%;text-align:left'>created On</td>";
             tab += "<td style='width:6%;text-align:left'>OnbehalfOf</td>";
             tab += "<td style='width:5%;text-align:left'>Effdate</td>";
             tab += "<td style='width:13%;text-align:left'>Issue</td>";
             tab += "<td style='width:10%;text-align:left'>Proposed Solution</td>";
             tab += "<td style='width:12%;text-align:left'>Remarks</td>";
             tab += "<td style='width:5%;text-align:left'>Attachment</td>";
             tab += "<td style='width:5%;text-align:center'>Action</td>";
             tab += "<td style='width:9%;text-align:center'>Remarks</td>";
             tab += "</tr>";
             if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;




                    tab += "<td style='width:1%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:5%;text-align:left' >" + col[3] + "</td>";
                    tab += "<td style='width:3%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:5%;text-align:left' >" + col[2] + "</td>";
                    tab += "<td style='width:3%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[14] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[15] + " </td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[12] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[11] + "</td>";
                    tab += "<td style='width:13%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:12%;text-align:left'>" + col[8] + "</td>";
                    tab += "<td style='width:5%;text-align:left'><img id='ViewReport' src='../Image/attchment2.png' onclick='viewReport(" + col[0] + ",1)' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' ></td>";



                    var select = "<select id='cmb" + col[0] + "' class='NormalText' name='cmb" + col[0] + "' >";
                    select += "<option value='-1'>Select</option>";
                    select += "<option value='1'>Closed</option>";
                    select += "<option value='2'>Update status</option>";

                    tab += "<td style='width:5%;text-align:left'>" + select + "</td>";



                    var txtBox = "<textarea id='txtRemarks" + col[0] + "' name='txtRemarks" + col[0] + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)'></textarea>";

                    tab += "<td style='width:9%;text-align:left'>" + txtBox + "</td>";


                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnFamily.ClientID %>").innerHTML = tab;
              
        }
        else
            document.getElementById("<%= pnFamily.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }

        function viewReport(ID, Rpt) {

            window.open("Reports/ShowAttachment.aspx?RequestID=" + btoa(ID) + "&RptID =" + Rpt);

            return false;
        }
        function UpdateValue() {
            
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("No Data Exists");
                return false;
            }
           
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
           
            for (n = 0; n <= row.length - 1; n++) {
               
                col = row[n].split("µ");
               
                document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[0] + "µ" + document.getElementById("cmb" + col[0]).value + "µ" + document.getElementById("txtRemarks" + col[0]).value ;
               
            }
                       
        }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
       
        <tr> 
            <td colspan="3"><asp:Panel ID="pnFamily" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
               
                <asp:HiddenField ID="hid_CloseStatus" runat="server" />
               
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SAVE"  onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

