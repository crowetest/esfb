﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="UploadBranchUser.aspx.vb" Inherits="UploadBranchUser" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

    </script>
   
</head>
</html>
<br /><br />

 <table class="style1" style="width:100%;margin: 0px auto;" >
           <tr id="Tr3"> <td style="width:25%;"></td>
               <td style="width:12%; text-align:left;"></td>
            <td>&nbsp; &nbsp;<input id="filename" type="text" style="font-family: sans-serif; cursor: pointer; width:30%; border:none;" /></td>
            
       </tr>
        <tr id="branch"> <td style="width:25%;">&nbsp;</td>
        <td style="width:12%; text-align:left;">Upload File</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:FileUpload ID="FileUpload1" runat="server"  /> 
                &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;<asp:Button ID="btnUpload" runat="server" style="font-family: sans-serif; cursor: pointer; width: 10%;" Text="UPLOAD" Font-Names="sans-serif"/>&nbsp;
                &nbsp; &nbsp;<input id="btnExit" style="font-family: sans-serif; cursor: pointer; width: 10%;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" />
            </td>
            
       </tr>
       <tr id="Tr2"> <td style="width:25%;" colspan="3">
                 &nbsp; &nbsp;<asp:Panel ID="PnlCAFDtl" runat="server"></asp:Panel>
                </td>
        </tr>
       <tr>
            <td style="text-align:center;" colspan="3"><br />
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

