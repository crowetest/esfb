﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BranchRequest
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT, DTBr, DTTe, DTRo As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim StrValue As String
    Dim DR As DataRow
    Dim CallBackReturn As String = Nothing
    Dim Dep_Id As Integer
    Dim Entity As Integer
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1140) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Branch Request Creation"
            hid_Empcode.Value = CStr(Session("UserID"))
            Dep_Id = CInt(Session("DepartmentID"))
            Entity = 1
            Dim Branch_Id As Integer = CInt(Session("BranchID"))
            If Branch_Id > 10 Then
                Dep_Id = 0
            End If

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0


            DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER where branch_id=" & CInt(Session("BranchID")) & "").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
            cmbBranch.SelectedIndex = 0

            If CInt(Session("BranchID")) < 100 Then
                DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER ").Tables(0)
                hdn_RetailFlg.Value = "1"
            ElseIf CInt(Session("BranchID")) > 50000 Then
                DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER ").Tables(0)
                hdn_RetailFlg.Value = "0"
            Else
                DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER ").Tables(0)
                hdn_RetailFlg.Value = "1"
            End If

            DT = DB.ExecuteDataSet("select '-1' as id,'------Select--------' union all  select distinct Branch_ID,Branch_Name from BRANCH_MASTER ").Tables(0)
            GN.ComboFill(cmbTransBranchOld, DT, 0, 1)
            cmbBranch.SelectedIndex = 0

            If (CInt(Session("BranchID")) < 100) Then
                '<Summary>Changed by 40020 on 17-Dec-2020</Summary>
                '<Comments>Where clause added with USB_Flg</Comments>
                'DT = DB.ExecuteDataSet("select '-1' as id,'------Select--------' union all select app_id,app_name from ESFB.dbo.app_master ").Tables(0)
                DT = DB.ExecuteDataSet("select '-1' as id,'------Select--------' union all select app_id,app_name from ESFB.dbo.app_master where Status_ID=1").Tables(0)
            Else
                '<Summary>Changed by 40020 on 17-Dec-2020</Summary>
                '<Comments>Where clause added with USB_Flg</Comments>
                'DT = DB.ExecuteDataSet("select '-1' as id,'------Select--------' union all select app_id,app_name from ESFB.dbo.app_master where app_id not in (15,17)").Tables(0)
                DT = DB.ExecuteDataSet("select '-1' as id,'------Select--------' union all select app_id,app_name from ESFB.dbo.app_master where app_id not in (15,17) and Status_ID=1").Tables(0)
            End If
            GN.ComboFill(cmbCategory, DT, 0, 1)

            StrValue = ""
            DT = DB.ExecuteDataSet("select app_team,app_team_name from App_crm_team_master where status_id=1").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR In DT.Rows
                    StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                Next
            End If
            Me.hdnCrmTeam.Value = StrValue.ToString

            StrValue = ""
            DT = DB.ExecuteDataSet("select distinct A.app_role,app_role_name from ESFB.dbo.App_role_master A inner join App_Role_Dtl B On A.App_Role= B.App_Role where A.status_id=1 and B.Status_id = 1 and entity=" + Entity.ToString() + " and  app_id=14 and department_id = " + Dep_Id.ToString() + " order By app_role_name").Tables(0)

            'DT = DB.ExecuteDataSet("select app_role,app_role_name from ESFB.dbo.App_role_master where status_id=1 and sfb_status=1 and  app_id=14 order By app_role_name").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR In DT.Rows
                    StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                Next
            End If
            Me.hdnCrmRole.Value = StrValue.ToString

            StrValue = ""
            DT = DB.ExecuteDataSet("select distinct Branch_ID,Branch_Name from BRANCH_MASTER order by 2").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR In DT.Rows
                    StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                Next
            End If
            Me.hdnCrmBranch.Value = StrValue.ToString

            '<Summary>Change done by 40020 on 05-Jan-2021</Summary>
            '<Remarks>Multiple Report Code selection added</Remarks>
            StrValue = ""
            DT = DB.ExecuteDataSet("select MIS_Master_ID ID,Master_Name Name from App_Profile_MIS_Master where Category_ID=2 and Status_ID=1 order by 2").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR In DT.Rows
                    StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                Next
            End If
            Me.hdnReport.Value = StrValue.ToString

            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            Me.cmbType.Attributes.Add("onchange", "return TypeOnChange()")
            Me.cmbRole.Attributes.Add("onchange", "return RoleOnChange()")
            Me.cmbRoleOld.Attributes.Add("onchange", "return OldRoleOnChange()")
            Me.cmbTransBranchOld.Attributes.Add("onchange", "return OldTransferOnChange()")
            Me.cmbTeller.Attributes.Add("onchange", "return TellerOnChange()")
            Me.cmbUserType.Attributes.Add("onchange", "return UserTypeOnChange()")

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "RequestOnchange();", True)

            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            txtEmpCode.Attributes.Add("onchange", "EmployeeOnChange()")
            cmbCategory.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub

#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim EmpID As Integer = CInt(Data(1))
        Dim strval As String
        Dim strfld As String
        Dim strfld1 As String
        Dim strwhere As String
        If CInt(Data(0)) = 1 Then
            If CInt(Data(2)) > 0 Then 'Profile
                strval = " left join ESFB.dbo.App_dtl_profile g on a.emp_code=g.emp_code  left join ESFB.dbo.app_request_master h on g.App_request_id=h.App_request_id and h.App_id=" & Data(2).ToString & " and h.App_type_id=" & Data(3).ToString & "  and h.Approved_By is null"
                strfld = "b.mobile,"
                strwhere = ""
                strfld1 = " ,g.Role_id"
            Else
                strval = ""
                strfld = ""
                strfld1 = ""
            End If
            DT = DB.ExecuteDataSet("select distinct a.emp_code,a.emp_name,Official_mail_id," & strfld.ToString & " " & _
                    " cug_no,department_name,designation_name,convert(varchar,a.date_of_join,106),a.reporting_to, " & _
                    " e.emp_name + '('+ convert(varchar,a.reporting_to)+')'  as reportingEmpCode,Emp_status " & strfld1.ToString & " from " & _
                    " Emp_master a " & strval.ToString & ",emp_profile b,department_master c,designation_master d,Emp_master e, " & _
                    " Employee_status f where a.emp_code=b.emp_code and a.department_id=c.Department_ID and a.designation_id=d.designation_id  " & _
                    " and a.reporting_to=e.emp_code and a.emp_status_id=f.emp_status_id  and a.status_id=1 and a.Emp_status_id not in (5,6) and a.emp_code=" + CInt(Data(1)).ToString + " " & strwhere.ToString & " and a.branch_id=" & CInt(Session("BranchID"))).Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString() + "Ø" + DT.Rows(0)(6).ToString() + "Ø" + DT.Rows(0)(7).ToString() + "Ø" + DT.Rows(0)(8).ToString() + "Ø" + DT.Rows(0)(9).ToString() + "Ø" + DT.Rows(0)(10).ToString() + "Ø" + DT.Rows(0)(11).ToString()
                If (CInt(Data(2)) = 10 And CInt(Data(3)) = 50) Then
                    DT = DB.ExecuteDataSet("select a.dob,convert(date,b.date_of_join) from emp_profile a, emp_master b where a.emp_code= " + CInt(Data(1)).ToString + "and a.emp_code=b.emp_code").Tables(0)
                    CallBackReturn += "Ø" + CDate(DT.Rows(0)(0)).ToString("dd/MM/yyyy") + "Ø" + CDate(DT.Rows(0)(1)).ToString("dd/MM/yyyy")
                ElseIf (CInt(Data(2)) = 14 And CInt(Data(3)) <> 68) Then
                    CallBackReturn += "Ř"
                    StrValue = ""
                    DTBr = DB.ExecuteDataSet("select TYPE_ID,TYPE_NAME from App_Crm_Request_master where emp_code=" + CInt(Data(1)).ToString + " and category='Branch' and type_id <> -1").Tables(0)
                    If DTBr.Rows.Count > 0 Then
                        For Each DR In DTBr.Rows
                            StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ1"
                        Next
                    End If
                    CallBackReturn += StrValue.ToString
                    CallBackReturn += "Ř"
                    StrValue = ""
                    DTTe = DB.ExecuteDataSet("select TYPE_ID,TYPE_NAME from App_Crm_Request_master where emp_code=" + CInt(Data(1)).ToString + " and category='Team'").Tables(0)
                    If DTTe.Rows.Count > 0 Then
                        For Each DR In DTTe.Rows
                            StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ1"
                        Next
                    End If
                    CallBackReturn += StrValue.ToString
                    CallBackReturn += "Ř"
                    StrValue = ""
                    DTRo = DB.ExecuteDataSet("select TYPE_ID,TYPE_NAME from App_Crm_Request_master where emp_code=" + CInt(Data(1)).ToString + " and category='Role'").Tables(0)
                    If DTRo.Rows.Count > 0 Then
                        For Each DR In DTRo.Rows
                            StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ1"
                        Next
                    End If
                    CallBackReturn += StrValue.ToString
                    '<Summary>Change done by 40020 on 04-Jan-2021</Summary>
                    '<Remarks>Multiple Report Code selection added</Remarks>
                    CallBackReturn += "Ř"
                    StrValue = ""
                    DTRo = DB.ExecuteDataSet("select MIS_Master_ID ID,Master_Name Name from App_Profile_MIS_Master where Category_ID=2 and Status_ID=1 order by 2").Tables(0)
                    If DTRo.Rows.Count > 0 Then
                        For Each DR In DTRo.Rows
                            StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ1"
                        Next
                    End If
                    CallBackReturn += StrValue.ToString
                Else
                    CallBackReturn += "Ø0Ø0ŘŘŘ"
                End If
            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf CInt(Data(0)) = 2 Then
            DT = DB.ExecuteDataSet("select  '-1' as id,' -----Select------' as Name union all  select app_type_id,app_type_name from ESFB.dbo.App_Type_master where status_id=1  and app_type_id not in (36,37,38) and app_id=" + Data(1).ToString()).Tables(0)
            If (DT.Rows.Count > 0) Then
                Dim StrVal1 As String = ""

                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal1 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal1 += "Ñ"
                    End If
                Next
                CallBackReturn = StrVal1 + "|"

                Dim StrVal2 As String = ""
                If CInt(Data(1)) <> 18 Then
                    DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name union all  select distinct A.app_role,app_role_name from ESFB.dbo.App_role_master A inner join App_Role_Dtl B On A.App_Role= B.App_Role where A.status_id=1 and B.Status_id = 1 and entity=" + Entity.ToString() + " and  app_id=" + Data(1).ToString() + " and department_id = " + Dep_Id.ToString() + ")A  order By app_role_name").Tables(0)
                    'DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name union all  select app_role,app_role_name from ESFB.dbo.App_role_master where status_id=1 and sfb_status=1 and  app_id=" + Data(1).ToString() + ")A  order By app_role_name").Tables(0)
                    If (DT.Rows.Count > 0) Then
                        For n As Integer = 0 To DT.Rows.Count - 1
                            StrVal2 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                            If n < DT.Rows.Count - 1 Then
                                StrVal2 += "Ñ"
                            End If
                        Next
                        CallBackReturn += StrVal2
                    End If
                Else
                    If Dep_Id = 0 Then 'Branch
                        DT = DB.ExecuteDataSet("select User_type_id, User_type_name from ESFB.dbo.App_User_Role_Type where app_id=" + Data(1).ToString() + " and User_type_id = 1 order by User_type_id ").Tables(0)
                    Else
                        DT = DB.ExecuteDataSet("select  '-1' as User_type_id,' -----Select------' as User_type_name union all select User_type_id, User_type_name from ESFB.dbo.App_User_Role_Type where app_id=" + Data(1).ToString() + " and User_type_id <> 1  order by User_type_id ").Tables(0)
                    End If
                    StrVal2 = ""
                    If (DT.Rows.Count > 0) Then
                        For n As Integer = 0 To DT.Rows.Count - 1
                            StrVal2 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                            If n < DT.Rows.Count - 1 Then
                                StrVal2 += "Ñ"
                            End If
                        Next
                        CallBackReturn += StrVal2

                    End If

                End If


                CallBackReturn += "Ř"
                If CInt(Data(1)) = 14 Then
                    StrValue = ""
                    DT = DB.ExecuteDataSet("select app_team,app_team_name from App_crm_team_master where status_id=1").Tables(0)
                    If DT.Rows.Count > 0 Then
                        For Each DR In DT.Rows
                            StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                        Next
                        CallBackReturn += StrValue.ToString
                    End If
                    CallBackReturn += "Ř"
                    StrValue = ""
                    DT = DB.ExecuteDataSet("select distinct A.app_role,app_role_name from ESFB.dbo.App_role_master A inner join App_Role_Dtl B On A.App_Role= B.App_Role where A.status_id=1 and B.Status_id = 1 and entity=" + Entity.ToString() + " and  app_id=14 and department_id = " + Dep_Id.ToString() + " order By app_role_name").Tables(0)

                    'DT = DB.ExecuteDataSet("select app_role,app_role_name from ESFB.dbo.App_role_master where status_id=1 and sfb_status=1 and  app_id=14 order By app_role_name").Tables(0)
                    If DT.Rows.Count > 0 Then
                        For Each DR In DT.Rows
                            StrValue += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ0"
                        Next
                        CallBackReturn += StrValue.ToString
                    End If
                    CallBackReturn += "Ř"
                Else
                    CallBackReturn += "Ř"
                    CallBackReturn += "Ř"

                End If
                StrVal2 = ""
                If Dep_Id = 0 And CInt(Data(1)) = 18 Then
                    DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name union all  select distinct A.app_role,app_role_name from ESFB.dbo.App_role_master A inner join App_Role_Dtl B On A.App_Role= B.App_Role where A.status_id=1 and B.Status_id = 1 and entity=" + Entity.ToString() + " and  app_id=" + Data(1).ToString() + " and department_id = " + Dep_Id.ToString() + " and A.App_User_Type_Id =1  )A  order By app_role_name").Tables(0)
                    If (DT.Rows.Count > 0) Then
                        For n As Integer = 0 To DT.Rows.Count - 1
                            StrVal2 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                            If n < DT.Rows.Count - 1 Then
                                StrVal2 += "Ñ"
                            End If
                        Next
                        CallBackReturn += StrVal2
                    End If
                End If
                CallBackReturn += "Ř"
            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf CInt(Data(0)) = 5 Then
            Dim StrVal2 As String = ""
            If CInt(Data(1)) = 1 Then
                If CInt(Data(2)) <> 18 Then
                    DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name union all  select distinct A.app_role,app_role_name from ESFB.dbo.App_role_master A inner join App_Role_Dtl B On A.App_Role= B.App_Role where A.status_id=1 and B.Status_id = 1 and entity=" + Entity.ToString() + " and  app_id=" + Data(2).ToString() + " and department_id = " + Dep_Id.ToString() + ")A  order By app_role_name").Tables(0)
                Else
                    DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name union all  select distinct A.app_role,app_role_name from ESFB.dbo.App_role_master A inner join App_Role_Dtl B On A.App_Role= B.App_Role where A.status_id=1 and B.Status_id = 1 and entity=" + Entity.ToString() + " and  app_id=" + Data(2).ToString() + " and department_id = " + Dep_Id.ToString() + " and A.App_User_Type_Id = " + Data(3).ToString() + " )A  order By app_role_name").Tables(0)

                End If
            Else
                If CInt(Data(2)) <> 18 Then
                    DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name union all  select distinct A.app_role,app_role_name from ESFB.dbo.App_role_master A inner join App_Role_Dtl B On A.App_Role= B.App_Role where A.status_id=1 and B.Status_id = 1 and entity=" + Entity.ToString() + " and A.app_role<>" + Data(1).ToString() + " and  app_id=" + Data(2).ToString() + " and department_id = " + Dep_Id.ToString() + ")A  order By app_role_name").Tables(0)
                Else
                    DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name union all  select distinct A.app_role,app_role_name from ESFB.dbo.App_role_master A inner join App_Role_Dtl B On A.App_Role= B.App_Role where A.status_id=1 and B.Status_id = 1 and entity=" + Entity.ToString() + " and A.app_role<>" + Data(1).ToString() + " and  app_id=" + Data(2).ToString() + " and department_id = " + Dep_Id.ToString() + " and A.App_User_Type_Id = " + Data(3).ToString() + " )A  order By app_role_name").Tables(0)
                End If
            End If

            If (DT.Rows.Count > 0) Then
                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal2 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal2 += "Ñ"
                    End If
                Next
                CallBackReturn += StrVal2
            End If
        ElseIf CInt(Data(0)) = 4 Then
            Dim StrVal2 As String = ""
            DT = DB.ExecuteDataSet("select * from (select  '-1' as Branch_ID,' -----Select------' as Branch_Name union all  select distinct Branch_ID,Branch_Name from BRANCH_MASTER  where branch_id<>" & Data(1).ToString() & ")A  order By Branch_Name").Tables(0)
            If (DT.Rows.Count > 0) Then
                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal2 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal2 += "Ñ"
                    End If
                Next
                CallBackReturn += StrVal2
            End If
        ElseIf CInt(Data(0)) = 3 Then
            Dim UserType As Integer = CInt(Data(2))
            Dim StrVal2 As String = ""
            DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name union all  select distinct A.app_role,app_role_name from ESFB.dbo.App_role_master A inner join App_Role_Dtl B On A.App_Role= B.App_Role where A.status_id=1 and B.Status_id = 1 and entity=" + Entity.ToString() + " and  app_id=" + Data(1).ToString() + " and department_id = " + Dep_Id.ToString() + " and A.App_User_Type_Id = " + UserType.ToString() + "  )A  order By app_role_name").Tables(0)
            'DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name union all  select app_role,app_role_name from ESFB.dbo.App_role_master where status_id=1 and sfb_status=1 and  app_id=" + Data(1).ToString() + ")A  order By app_role_name").Tables(0)
            If (DT.Rows.Count > 0) Then
                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal2 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal2 += "Ñ"
                    End If
                Next
                CallBackReturn += StrVal2
            End If

        End If
    End Sub
#End Region

    Private Sub initializeControls()
        cmbBranch.Focus()
        cmbCategory.Text = "-1"
        cmbType.Text = "-1"
        txtEmpCode.Text = ""
        txtEmpName.Text = ""
        cmbRole.Text = "-1"
        cmbTransBranch.Text = "-1"
        'txtdetails.Text = ""
        txtContactNo.Text = ""
        txtEmailid.Text = ""
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim Branch As Integer = CInt(Data(1))
            Dim REQUEST_ID As Integer = 0
            Dim APP_ID As Integer = CInt(Data(2))
            Dim trans_branch_id As Integer = CInt(Data(3))
            Dim TYPE_ID As Integer = CInt(Data(4))
            Dim Descr As String = CStr(Data(5))
            Dim Contactno As String = CStr(Data(6))
            Dim EmailId As String = CStr(Data(7))
            Dim EMP_CODE As Integer = CInt(Data(8))
            Dim ROLE_ID As String = CStr(IIf(Data(9) = "", 0, Data(9)))
            Dim CUG As String = CStr(Data(10))
            Dim TELLER_ID As Integer = CInt(Data(11)) '1-HeadTeller 2-Cashier,3-Both

            Dim FolderName As String = CStr(Data(12))
            Dim Storage As Integer = CInt(Data(13))
            Dim AccessType As Integer = CInt(Data(14))
            Dim RequestMailID As String = CStr(Data(15))
            Dim ServiceRequestNo As String = CStr(Data(16))
            Dim RoleOld As Integer = CInt(Data(17))
            Dim TransferOld As Integer = CInt(Data(18))
            Dim DL As String = CStr(Data(19))
            Dim Prevcode As Integer = CInt(IIf(Data(20) = "", 0, Data(20)))
            Dim PrfNameOne As String = CStr(Data(21))
            Dim PrfNoOne As String = CStr(Data(22))
            Dim PrfNameTwo As String = CStr(Data(23))
            Dim PrfNoTwo As String = CStr(Data(24))
            Dim ExistingUserID As String = CStr(Data(25))
            Dim HandOverTo As Integer = CInt(IIf(Data(26) = "", 0, Data(26)))
            Dim FromDt As String = CStr(Data(27))
            Dim ToDt As String = CStr(Data(28))
            Dim StrTeam As String = CStr(Data(29))
            Dim StrRole As String = CStr(Data(30))
            Dim StrBranch As String = CStr(Data(31))

            '<Summary>Change done by 40020 on 05-Jan-2021</Summary>
            '<Remarks>Profile-MIS Multiple Report Code selection added</Remarks>
            Dim StrReport As String = CStr(Data(35))

            'Added by Vidya for profile old and new user id ---begin
            Dim OldProfUserID As String = CStr(Data(32))
            Dim NewProfUserID As String = CStr(Data(33))
            'Added by Vidya for profile old and new user id ---end

            Dim tranferType As Integer = 0
            If Data(34) <> "" Then
                tranferType = CInt(Data(34))
            End If


            Dim ActTeam As String = ""
            Dim ActRole As String = ""
            Dim ActBranch As String = ""

            '<Summary>Change done by 40020 on 05-Jan-2021</Summary>
            '<Remarks>Profile-MIS Multiple Report Code selection added</Remarks>
            Dim ActReport As String = ""

            If (StrTeam <> "") Then
                DT = DB.ExecuteDataSet("select convert(varchar,app_team)+'Ñ'+app_team_name from app_crm_team_master where app_team in(" & StrTeam & ")").Tables(0)
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        ActTeam += DR(0).ToString() + "ÿ"
                    Next
                End If
            End If
            If (StrRole <> "") Then
                DT = DB.ExecuteDataSet("select distinct convert(varchar,A.app_role)+'Ñ'+app_role_name from ESFB.dbo.App_role_master A inner join App_Role_Dtl B On A.App_Role= B.App_Role where A.status_id=1 and B.Status_id = 1 and entity=" + Entity.ToString() + " and A.app_role in(" & StrRole & ") and  app_id=14 and department_id = " + Dep_Id.ToString() + " order By 1").Tables(0)

                'DT = DB.ExecuteDataSet("select convert(varchar,app_role)+'Ñ'+app_role_name from App_role_master where app_id=14 and app_role in(" & StrRole & ")").Tables(0)
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        ActRole += DR(0).ToString() + "ÿ"
                    Next
                End If
            End If
            If (StrBranch <> "") Then
                DT = DB.ExecuteDataSet("select convert(varchar,Branch_ID)+'Ñ'+Branch_Name from BRANCH_MASTER where Branch_ID in(" & StrBranch & ")").Tables(0)
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        ActBranch += DR(0).ToString() + "ÿ"
                    Next
                End If
            End If

            '<Summary>Change done by 40020 on 05-Jan-2021</Summary>
            '<Remarks>Profile-MIS Multiple Report Code selection added</Remarks>
            If (StrReport <> "") Then
                DT = DB.ExecuteDataSet("select convert(varchar,MIS_Master_ID)+'Ñ'+Master_Name from App_Profile_MIS_Master where MIS_Master_ID in(" & StrReport & ") and Category_ID=2").Tables(0)
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        ActReport += DR(0).ToString() + "ÿ"
                    Next
                End If
            End If

            Dim UserID As String = Session("UserID").ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                '<Summary>Change done by 40020 on 05-Jan-2021</Summary>
                '<Remarks>Profile-MIS Multiple Report Code selection added - @ReportCode field added</Remarks>
                Dim Params(38) As SqlParameter
                Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
                Params(0).Value = Branch
                Params(1) = New SqlParameter("@EMP_CODE", SqlDbType.Int)
                Params(1).Value = EMP_CODE
                Params(2) = New SqlParameter("@DESCR", SqlDbType.VarChar, 5000)
                Params(2).Value = Descr
                Params(3) = New SqlParameter("@APP_ID", SqlDbType.Int)
                Params(3).Value = APP_ID
                Params(4) = New SqlParameter("@trans_branch_id", SqlDbType.Int)
                Params(4).Value = trans_branch_id
                Params(5) = New SqlParameter("@TYPE_ID", SqlDbType.Int)
                Params(5).Value = TYPE_ID
                Params(6) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(6).Value = UserID
                Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(8).Direction = ParameterDirection.Output
                Params(9) = New SqlParameter("@REQUEST_ID", SqlDbType.Int)
                Params(9).Direction = ParameterDirection.Output
                Params(10) = New SqlParameter("@CONTACTNO", SqlDbType.VarChar, 15)
                Params(10).Value = Contactno
                Params(11) = New SqlParameter("@CUG", SqlDbType.VarChar, 15)
                Params(11).Value = CUG
                Params(12) = New SqlParameter("@EMAIL", SqlDbType.VarChar, 100)
                Params(12).Value = EmailId
                Params(13) = New SqlParameter("@ROLE_ID", SqlDbType.Int)
                Params(13).Value = ROLE_ID
                Params(14) = New SqlParameter("@TELLER_ID", SqlDbType.Int)
                Params(14).Value = TELLER_ID
                Params(15) = New SqlParameter("@FolderName", SqlDbType.VarChar, 25)
                Params(15).Value = FolderName
                Params(16) = New SqlParameter("@Storage", SqlDbType.Int)
                Params(16).Value = Storage
                Params(17) = New SqlParameter("@AccessType", SqlDbType.Int)
                Params(17).Value = AccessType
                Params(18) = New SqlParameter("@RequestMailID", SqlDbType.VarChar, 50)
                Params(18).Value = RequestMailID
                Params(19) = New SqlParameter("@ServiceRequestNo", SqlDbType.VarChar, 50)
                Params(19).Value = ServiceRequestNo
                Params(20) = New SqlParameter("@OldROLE_ID", SqlDbType.Int)
                Params(20).Value = RoleOld
                Params(21) = New SqlParameter("@Oldtrans_branch_id", SqlDbType.Int)
                Params(21).Value = TransferOld
                Params(22) = New SqlParameter("@DL", SqlDbType.VarChar, 100)
                Params(22).Value = DL
                Params(23) = New SqlParameter("@PrevCode", SqlDbType.Int)
                Params(23).Value = Prevcode
                Params(24) = New SqlParameter("@PrfNameOne", SqlDbType.VarChar, 50)
                Params(24).Value = PrfNameOne
                Params(25) = New SqlParameter("@PrfNoOne", SqlDbType.VarChar, 50)
                Params(25).Value = PrfNoOne
                Params(26) = New SqlParameter("@PrfNameTwo", SqlDbType.VarChar, 50)
                Params(26).Value = PrfNameTwo
                Params(27) = New SqlParameter("@PrfNoTwo", SqlDbType.VarChar, 50)
                Params(27).Value = PrfNoTwo
                Params(28) = New SqlParameter("@ExistingUserID", SqlDbType.VarChar, 50)
                Params(28).Value = ExistingUserID
                Params(29) = New SqlParameter("@HandOverTo", SqlDbType.Int)
                Params(29).Value = HandOverTo
                Params(30) = New SqlParameter("@FromDt", SqlDbType.VarChar, 50)
                Params(30).Value = FromDt
                Params(31) = New SqlParameter("@ToDt", SqlDbType.VarChar, 50)
                Params(31).Value = ToDt
                Params(32) = New SqlParameter("@CrmTeam", SqlDbType.VarChar, 5000)
                Params(32).Value = ActTeam
                Params(33) = New SqlParameter("@CrmRole", SqlDbType.VarChar, 5000)
                Params(33).Value = ActRole
                Params(34) = New SqlParameter("@CrmBranch", SqlDbType.VarChar, 5000)
                Params(34).Value = ActBranch
                Params(35) = New SqlParameter("@OldProfUserID", SqlDbType.VarChar, 50)
                Params(35).Value = OldProfUserID
                Params(36) = New SqlParameter("@NewProfUserID", SqlDbType.VarChar, 50)
                Params(36).Value = NewProfUserID
                Params(37) = New SqlParameter("@TransferType", SqlDbType.Int)
                Params(37).Value = tranferType
                Params(38) = New SqlParameter("@ReportCode", SqlDbType.VarChar, 5000)
                Params(38).Value = ActReport

                DB.ExecuteNonQuery("SP_APP_REQUEST", Params)
                ErrorFlag = CInt(Params(7).Value)
                Message = CStr(Params(8).Value)
                'REQUEST_ID = CInt(Params(9).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "'); ")
            cl_script1.Append("        window.open('BranchRequest.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            If ErrorFlag = 0 Then
                initializeControls()
            End If

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
