﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CTSBranchClose_Del.aspx.vb" Inherits="CTSBranchClose_Del" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
     <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10pt;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10pt;color:#476C91;
        }
    </style>

    <script src="../Script/Validations.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
       
        
        function FromServer(Arg, Context) {
            
            if (Context == 1) {
                var Data = Arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("CTSBranchClose_Del.aspx", "_self");
            }
            else {
                var Data = Arg;      
                document.getElementById("<%= hid_dtls.ClientID%>").value = Data;
                table_fill();
            }
            
        }
       
        function btnApprove_onclick() {
           
            if (document.getElementById("<%= cmbbranch.ClientID%>").value == "-1") {
                alert("select Branch");
                return false;
             }
            if (document.getElementById("<%= txtToDt.ClientID%>").value == "") {
                alert("select Date");
                return false;
            }
            UpdateValue();
           
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "") {
                alert("No Data For Update");
                return false;
            }
            
            var strempcode = document.getElementById("<%= hid_temp.ClientID%>").value;
            var Data = "1Ø" + strempcode + "Ø" + document.getElementById("<%= cmbbranch.ClientID%>").value;
            ToServer(Data, 1);
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function BranchOnchange() {
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            table_fill();
        }
        function table_fill() {
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                document.getElementById("<%= pnFamily.ClientID %>").style.display = '';
             var row_bg = 0;
             var tab = "";
             tab += "<div style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
             tab += "<tr height=20px;  class='tblQal'>";
             tab += "<td style='width:5%;text-align:center' >#</td>";
             tab += "<td style='width:15%;text-align:left' >A/C Number</td>";
             tab += "<td style='width:20%;text-align:left' >Cust Name</td>";
             tab += "<td style='width:15%;text-align:left' >Cheque No</td>";
             tab += "<td style='width:10%;text-align:left'>Amount</td>";
             tab += "<td style='width:10%;text-align:left'>Created Time</td>";
             tab += "<td style='width:10%;text-align:left'>Status</td>";
             tab += "<td style='width:15%;text-align:left'>Remarks</td>";
             tab += "</tr>";
             tab += "</table></div>";
             tab += "<div id='ScrollDiv' style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

             row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var i = 0;
            for (n = 1; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                if (col[5] != 1) {
                    var i = i+1;
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }

                    tab += "<td style='width:5%;text-align:center' >" + i + "</td>";

                   
                    tab += "<td style='width:15%;text-align:left' >" + col[0] + "</td>";
                    
                    tab += "<td style='width:20%;text-align:left' >" + col[1] + "</td>";
                   
                    tab += "<td style='width:15%;text-align:left' >" + col[2] + "</td>";
                    
                    tab += "<td style='width:10%;text-align:left' >" + col[3] + "</td>";
                    
                    tab += "<td style='width:10%;text-align:left' >" + col[4] + "</td>";
                    var select = "<select id='cmb" + col[5] + "' class='NormalText' name='cmb" + col[5] + "' style='width:100%;' >";
                                      
                    if (col[6] == "2") {
                        select += "<option value='1'>Processed</option>";
                        select += "<option value='2' selected =true>Rejected</option>";
                    }
                    else {
                        select += "<option value='1' selected=true>Processed</option>";
                        select += "<option value='2'>Rejected</option>";
                    }
                    
                   
                    tab += "<td style='width:10%;text-align:left' >" + select + "</td>";
                    var txtBox = "<textarea id='txtRemarks" + col[5] + "' name='txtRemarks" + col[5] + "' style='width:99%; float:left;' maxlength='200' onkeypress='return TextAreaCheck(event)' >"+ col[7]+"</textarea>";
                    tab += "<td style='width:15%;text-align:left' >" + txtBox + "</td>";
                    tab += "</tr>";
                }
            }

            tab += "</table></div></div></div>";
            document.getElementById("<%= pnFamily.ClientID %>").innerHTML = tab;
              
        }
        else
            document.getElementById("<%= pnFamily.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }

        function viewReport() {
            document.getElementById("<%= hid_dtls.ClientID%>").value = "";
            table_fill();
            var branch = document.getElementById("<%= cmbbranch.ClientID%>").value;
            if (document.getElementById("<%= cmbbranch.ClientID%>").value == "-1") {
                alert("Select Branch");
                document.getElementById("<%= cmbbranch.ClientID%>").focus();

                return false;
            }
            else if (document.getElementById("<%= txtToDt.ClientID%>").value == "") {
                alert("Select Date");
                document.getElementById("<%= txtToDt.ClientID%>").focus();

                return false;
            }
            else {
                var Data = "2Ø" + branch + "Ø" + document.getElementById("<%= txtToDt.ClientID%>").value;
                ToServer(Data, 2);
            }

           
        }
        function UpdateValue() {
           
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("No Data Exists");
                return false;
            }
           
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
           
            for (n = 1; n <= row.length - 1; n++) {
                col = row[n].split("µ");
               
                document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[5] + "µ" + document.getElementById("cmb" + col[5]).value + "µ" + document.getElementById("txtRemarks" + col[5]).value ;
               
            }
                       
        }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
        <tr id="branch"><td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">Select Branch</td>
                <td style="width:63%">
                    &nbsp; &nbsp;<asp:DropDownList ID="cmbbranch" class="NormalText" style="text-align: left;" runat="server" Font-Names="Cambria" 
                    Width="30%" ForeColor="Black">
                </asp:DropDownList>
                    <asp:TextBox ID="txtToDt" class="NormalText" runat="server" Width="20%" onkeypress="return false"
                        ReadOnly="True"></asp:TextBox>
                    <asp:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtToDt" Format="dd/MMM/yyyy">
                    </asp:CalendarExtender>
                    <img id="ViewReport" src="../Image/start.png" onclick="viewReport()" title="View Report" style="height:20px; width:20px; cursor:pointer;"/>
                </td>
            
           </tr>
        <tr> 
            <td colspan="3"><asp:Panel ID="pnFamily" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
               
                <asp:HiddenField ID="hid_CloseStatus" runat="server" />
               
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SAVE"  onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

