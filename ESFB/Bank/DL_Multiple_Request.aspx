﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="DL_Multiple_Request.aspx.vb" Inherits="DL_Multiple_Request"
    EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
        function RequestOnchange() { 
            document.getElementById("Multiple").style.display = "";
        }    
        function CategoryOnChange() {
            ClearCombo("<%= cmbType.ClientID %>");     
            document.getElementById("<%= cmbType.ClientID%>").value ="-1";
            var Groupid = document.getElementById("<%= cmbCategory.ClientID %>").value;
            if (Groupid > 0) {
                var ToData = "2Ø" + Groupid; 
                ToServer(ToData, 2);
            }
        }   
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
       
            for (a = 0; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function FromServer(arg, context) {        
            if (context == 1) {
                var Data = arg.split("Ø");

            }
            else if (context == 2) {
                var Data = arg.split("|");
                ComboFill(Data[0], "<%= cmbType.ClientID%>");
       
            }
            else if (context == 3) {
               
                var Data = arg.split("µ");
                var id = Data[5];
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var NewStr = ""
                for (n = 1; n <= row.length - 1; n++) {
                    if (id == n) { 
                        // 0 - Entity, 1 - EmployeeCode, 2 - EmployeeName, 3- DistributionList, 4- ContactNo, 5-OfficialEmailID                     
                        NewStr += "¥" + Data[0] + "µ" + Data[1] + "µ" + Data[2] + "µ" + Data[6] + "µ"+ Data[3] +"µ" + Data[4] + "µ";
                    }
                    else {
                        NewStr += "¥" + row[n];
                    }
                }
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
                FillNewDetails();       
            }
        }

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        function RequestOnClick() {          
            if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1") 
            {
            alert("Select Branch");
            document.getElementById("<%= cmbBranch.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= cmbCategory.ClientID%>").value == "-1") 
            {
            alert("Select Application Type");
            document.getElementById("<%= cmbCategory.ClientID%>").focus();
                return false;
            }
      
            if (document.getElementById("<%= cmbType.ClientID %>").value == "-1") 
            {
            alert("Select Type");
            document.getElementById("<%= cmbType.ClientID %>").focus();
                return false;
            } 
        }

        function AddNewDetails()
        {        
            if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var Len = row.length - 1;
                col = row[Len].split("µ");
                if (col[0] != "-1" && col[1] != "") {
                    document.getElementById("<%= hdnNewDtl.ClientID %>").value += "¥µµµµµµµ";
                }
            }
            else
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = "¥µµµµµµµ";
            FillNewDetails();
        }

        function FillNewDetails()
        {
            var DisList = document.getElementById("<%= cmbDList.ClientID %>").value; 
            if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                var row_bg = 0;
                var tab = "";
                tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr height=20px;  class='tblQal'>";
                tab += "<td style='width:5%;text-align:center' >#</td>";
                tab += "<td style='width:10%;text-align:left' >Entity</td>";
                tab += "<td style='width:10%;text-align:left'>EmployeeCode</td>";
                tab += "<td style='width:20%;text-align:left'>EmployeeName</td>";
                tab += "<td style='width:15%;text-align:left'>DistributionList</td>";
                tab += "<td style='width:15%;text-align:left'>ContactNo</td>";
                tab += "<td style='width:20%;text-align:left'>OfficialEmailID</td>";
                tab += "<td style='width:5%;text-align:left'></td>";
                tab += "</tr>";
                tab += "</table></div>";
                tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second';>";
                    }
                    // 0 - Entity, 1 - EmployeeCode, 2 - EmployeeName, 3- DistributionList, 4- ContactNo, 5-OfficialEmailID
                    tab += "<td style='width:5%;text-align:center' >" + n + "</td>";

                    var select = "<select id='cmbEntity" + n + "' class='NormalText' name='cmbEntity" + n + "' style='width:100%' onchange='UpdateValueRequest(" + n + ")' >";
                    var rows = document.getElementById("<%= hdnEntity.ClientID %>").value.split("Ř"); //Entity Details
                    for (a = 1; a < rows.length; a++) {
                        var cols = rows[a].split("Ĉ");
                        if (cols[0] == col[0])
                            select += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                        else
                            select += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
                    }                                      
                    tab += "<td style='width:10%;text-align:center'>" + select + "</td>"; //Entity
                     
                    if (col[1] != "")//EmployeeCode
                    {     
                        var txtBox = "<textarea id='txtEmpCode" + n + "' name='txtEmpCode" + n + "' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)'  onchange='GetEmployee(" + n + ")''>" + col[1] + "</textarea>";
                        tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>";
                    }
                    else{
                        var txtBox = "<textarea id='txtEmpCode" + n + "' name='txtEmpCode" + n + "' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)'  onchange='GetEmployee(" + n + ")''></textarea>";
                        tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>";
                    }

                    if (col[2] != "")//EmployeeName
                    {
                        tab += "<td style='width:20%;text-align:left' >" + col[2] + "</td>"; 
                    }
                    else{                        
                        tab += "<td style='width:20%;text-align:left' ></td>";
                    }

                    var select1 = "<select id='cmbDl" + n + "' class='NormalText' disabled=true name='cmbDl" + n + "' style='width:100%' onchange='UpdateValueRequest(" + n + ")' >";
                    var rows = document.getElementById("<%= hdnDl.ClientID %>").value.split("Ř"); //DL Details
                    for (a = 1; a < rows.length; a++) {
                        var cols = rows[a].split("Ĉ");
                        if (cols[0] == DisList)
                            select1 += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                        else
                            select1 += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
                    }
                    tab += "<td style='width:15%;text-align:center'>" + select1 + "</td>";// DistributionList

                    if (col[4] != "")
                    {
                        var txtBox = "<textarea id='txtContact" + n + "' name='txtContact" + n + "' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)'  onchange='UpdateValueRequest(" + n + ")' onkeydown='CreateNewRequest(event," + n + ")'>" + col[4] + "</textarea>";
                        tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>";                        
                    }
                    else{
                        var txtBox = "<textarea id='txtContact" + n + "' name='txtContact" + n + "' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)'  onchange='UpdateValueRequest(" + n + ")' onkeydown='CreateNewRequest(event," + n + ")'></textarea>";
                        tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>"; 
                    }                    

                    if (col[5] != "")
                    {
                        var txtBox = "<textarea id='txtOffMail" + n + "' name='txtOffMail" + n + "' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)'  onchange='UpdateValueRequest(" + n + ")' onkeydown='CreateNewRequest(event," + n + ")'>" + col[5] + "</textarea>";
                        tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>";
                    }
                    else{
                        var txtBox = "<textarea id='txtOffMail" + n + "' name='txtOffMail" + n + "' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onchange='UpdateValueRequest(" + n + ")' onkeydown='CreateNewRequest(event," + n + ")'></textarea>";
                        tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>";
                    }

                    if (col[0] == "-1")
                        tab += "<td style='width:5%;text-align:left'></td>";
                    else
                        tab += "<td style='width:5%;text-align:center' onclick=DeleteRequest('" + n + "')><img  src='../Image/remove.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                    tab += "</tr>";
                }
                tab += "</table></div></div></div>";
                document.getElementById("<%= pnlMultiple.ClientID %>").innerHTML = tab;
            }
                //setTimeout(function () { document.getElementById("cmbDocument" + (n - 1)).focus().select(); }, 4);
        }

        function GetEmployee(val) {
        //debugger;   
            if (document.getElementById("cmbEntity" + val).value == "-1") 
            {
                alert("Please select Entity") 
                document.getElementById("cmbEntity" + val).focus();
                return false;   
            }
            else                                
                var Entity = document.getElementById("cmbEntity" + val).value;
            var EmpCode = document.getElementById("txtEmpCode" + val).value;   
            var DlVal = document.getElementById("cmbDl" + val).value;
            var Type = document.getElementById("<%= cmbType.ClientID %>").value;
            var ToData = "3Ø" + Entity + "Ø" + EmpCode + "Ø" + Type + "Ø" + val + "Ø" + DlVal; 
            ToServer(ToData, 3);
        }

        function UpdateValueRequest(id)
        {//debugger;
            row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
            var NewStr = ""
            for (n = 1; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                if (id == n) {
                    // 0 - Entity, 1 - EmployeeCode, 2 - EmployeeName, 3- DistributionList, 4- ContactNo, 5-OfficialEmailID
                    var Entity = document.getElementById("cmbEntity" + id).value;
                    var EmpCode = document.getElementById("txtEmpCode" + id).value;
                    var EmpName = col[2];
                    var Distri  = document.getElementById("cmbDl" + id).value;
                    var ContactNo  = document.getElementById("txtContact" + id).value;
                    var OffMailID = document.getElementById("txtOffMail" + id).value;
                    NewStr += "¥" + Entity + "µ" + EmpCode + "µ" + EmpName + "µ" + Distri + "µ"+ ContactNo +"µ" + OffMailID + "µ";
                }
                else {
                    NewStr += "¥" + row[n];
                }
            }
            document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
        }

        function CreateNewRequest(e, val)
        {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                UpdateValueRequest(val);
                AddNewDetails();
                document.getElementById("txtEmpCode" + val).focus();
            }            
        }

        function DeleteRequest(id) {
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var NewStr = ""
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if (col[0] != id) {                    
                        NewStr += "¥" + col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ"+ col[4] +"µ" + col[5] + "µ";
                    }
                }
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
                var Conf = confirm("Are you sure?");
                if (Conf == true)
                    FillNewDetails();
        }
        function btnSave_onclick() {
            var SaveDtl = document.getElementById("<%= hdnNewDtl.ClientID %>").value;
            var Branch  = document.getElementById("<%= cmbBranch.ClientID %>").value;           
            var AppID	= document.getElementById("<%= cmbCategory.ClientID%>").value;
            var TypeID	= document.getElementById("<%= cmbType.ClientID %>").value; 
            var DLID	= document.getElementById("<%= cmbDList.ClientID %>").value; 
            var DLName  = document.getElementById("<%= cmbDList.ClientID%>").options[document.getElementById("<%= cmbDList.ClientID%>").selectedIndex].text; 
            var Descpt  = document.getElementById("txtDetails").value;  
            var ToData = "4Ø" + Branch + "Ø" + AppID + "Ø" + TypeID + "Ø" + DLID + "Ø" + SaveDtl + "Ø" + DLName + "Ø" + Descpt; 
            ToServer(ToData, 4);
        }

        </script>
        <%--<script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return window_onload()
            // ]]>
        </script>--%>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 80%; margin: 0px auto;">
        <tr id="branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="App">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Application
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbCategory" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Type">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Type
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbType" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="DL_Request.aspx">Create New DL</a></td>
        </tr>
        <tr id="Type">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Distribution List
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbDList" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Type">
            <td style="text-align: center;" colspan="3">
                <strong>ADD DETAILS</strong>
                <img onclick="AddNewDetails()" src="../Image/addIcon.png" style="height: 25px; width: 25px;
                    float: center; z-index: 1; cursor: pointer; padding-right: 10px;" title="Add New" />
            </td>
        </tr>
        <tr id="Multiple">
            <td colspan="3">
                <asp:Panel ID="pnlMultiple" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Description
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<textarea id="txtDetails" class="NormalText" name="S1" maxlength="1000" onkeypress='return TextAreaCheck(event)' 
                    style="width: 50%; height: 55px;"></textarea>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:HiddenField ID="hdn_RetailFlg" runat="server" />
                <asp:HiddenField ID="hid_Empcode" runat="server" />
                <asp:HiddenField ID="hid_Dtls" runat="server" />
                <asp:HiddenField ID="hid_Post" runat="server" />
                <br />
                <asp:HiddenField ID="hdnNewDtl" runat="server" />
                <asp:HiddenField ID="hdnEntity" runat="server" />
                <asp:HiddenField ID="hdnDl" runat="server" />
                <%--<asp:Button ID="btnSave" runat="server" Text="SUBMIT" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;--%>
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 68px;" type="button"
                    value="SAVE" onclick="return btnSave_onclick()" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
                <br />
            </td>
        </tr>
    </table>
    <br />
    <configuration>
    <system.web>
    <compilaton debug="true" targetFramework="4.0"></compilaton>
    </system.web>
    </configuration>
</asp:Content>
