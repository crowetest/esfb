﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CTSBranchUpdation
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim AT As New Attendance
    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1128) = False Then
                'If GF.FormAccess(CInt(Session("UserID")), 1131) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Me.Master.subtitle = "Branch Data Updation"
            DT = DB.ExecuteDataSet("select count(*) from CTS_Branch_close where status=1 and branch_id='" & Session("BranchID").ToString() & "' and convert(varchar,updated_on,105)=convert(varchar,getdate(),105) ").Tables(0)
            If CInt(DT.Rows(0)(0).ToString) > 0 Then
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_script1.Append("         alert('Branch Closed For Updation.Contact ESS For Releasing');")
                cl_script1.Append("        window.open('../home.aspx', '_self');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
                Return
            End If

            hid_dtls.Value = ""
            Dim StrAttendance As String = ""

            DT = DB.ExecuteDataSet("select acno,acname,chequeno,amt,tranid from cts_branch_updation where  branch_id  =" & Session("BranchID").ToString() & " and  convert(varchar,updated_on,105)=convert(varchar,getdate(),105) ").Tables(0)
            For Each DR As DataRow In DT.Rows
                StrAttendance += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ0"
            Next
            'If StrAttendance = "" Then
            StrAttendance += "¥µµµµµ"
            'End If
            hid_dtls.Value = StrAttendance
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim dataval As String = CStr(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim valuess As String
        If (Len(dataval) > 0) Then
            valuess = dataval.Substring(1)
        Else
            valuess = ""
        End If


            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@userID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(1).Value = BranchID
                Params(2) = New SqlParameter("@EMPDtl", SqlDbType.VarChar)
            Params(2).Value = valuess.ToString
            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_CTS_BRANCH_UPDATE", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString



    End Sub
#End Region

End Class
