﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Partial Class UploadFinanceExcel
    Inherits System.Web.UI.Page

    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim LV As New Leave
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Upload ATM GST data"
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        btnUpload.Attributes.Add("onclick", "return InsertOnClick()")

    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim msg As String = ""
        Dim connectionString As String = ""

        If Me.FileUpload1.HasFile = False Then
            Dim cl_script0 As New System.Text.StringBuilder
            cl_script0.Append("         alert('Please browse excel');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
            Exit Sub
        Else
            Dim fileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
            Dim fileExtension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)
            Dim fileLocation As String = Server.MapPath("../../" & fileName)
            FileUpload1.SaveAs(fileLocation)

            If fileExtension = ".xls" Then
                If Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE") = "x86" Then
                    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=YES;"""
                Else
                    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                End If

                'ElseIf fileExtension = ".xlsx" Then
                '    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties='Excel 12.0 Xml;HDR=yes'"
            Else
                'fileExtension = ".xlsx"
                Dim cl_script0 As New System.Text.StringBuilder
                cl_script0.Append("         alert('Uploaded Only Excel file with Extention .xls, Ex::sample.xls ');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
                Exit Sub

            End If
            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            Try
                Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TableData", dtExcelRecords), New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0), New System.Data.SqlClient.SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000), New System.Data.SqlClient.SqlParameter("@Month", hdnDate.Value)}
                Parameters(1).Direction = ParameterDirection.Output
                Parameters(2).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SALARY_DATA_MIGRATION", Parameters)

                ErrorFlag = CInt(Parameters(1).Value)
                Message = CStr(Parameters(2).Value)

                Dim cl_script1 As New System.Text.StringBuilder
                cl_script1.Append("         alert('" + Message.ToString + "');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)


            Catch ex As Exception
                MsgBox("Exceptional Error Occurred.Please Inform Application Team")
                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try
        End If
    End Sub
#End Region
End Class
