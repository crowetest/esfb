﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="UploadCTSinward.aspx.vb" Inherits="UploadCTSinward" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function table_fill_inward() {
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                
               
                document.getElementById("<%= PnlCAFDtl.ClientID%>").style.display = '';
                document.getElementById("filename").value = document.getElementById("<%= hid_filename.ClientID%>").value;
               var row_bg = 0;
               var tab = "";
               tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
               tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'sans-serif';' align='center'>";
               tab += "<tr style='background-color:#d3cdcd'>";
               tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
               tab += "<td style='width:8%;text-align:left' >Batch</td>";
               tab += "<td style='width:8%;text-align:left'>Cost Center</td>";
               tab += "<td style='width:5%;text-align:left' >Product</td>";
               tab += "<td style='width:5%;text-align:left'>Branch</td>";
               tab += "<td style='width:10%;text-align:left' >AcNo</td>";
               tab += "<td style='width:10%;text-align:left'>Customer</td>";
               tab += "<td style='width:4%;text-align:left'>ChequeNo</td>";
               tab += "<td style='width:6%;text-align:left' >ChequeAmt</td>";
               tab += "<td style='width:12%;text-align:left'>PresentingBank</td>";
               tab += "<td style='width:19%;text-align:left'>Payee</td>";
               tab += "<td style='width:4%;text-align:left'>Status</td>";
               tab += "<td style='width:7%;text-align:left'>Remarks</td>";
               tab += "</tr>";


               row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                    if (col[11] == "") {
                        var fontcolor = '';
                       
                    }
                    else {
                        var fontcolor = 'background-color:#c41e1e;color:#FFF;';
                    }

                    tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:8%;text-align:left' >" + col[0] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:5%;text-align:left' >" + col[2] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:10%;text-align:left' >" + col[4] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:6%;text-align:right' >" + col[7] + "</td>";
                    tab += "<td style='width:12%;text-align:left'>" + col[8] + "</td>";
                    tab += "<td style='width:19%;text-align:left'>" + col[9] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[10] + "</td>";
                    
                    tab += "<td style='width:7%;text-align:center;color:#FFF;" + fontcolor + "'>" + col[11] + "</td>";
                    
                    tab += "</tr>";
                }

                tab += "</table></div></div></div>";
              
                document.getElementById("<%= PnlCAFDtl.ClientID%>").innerHTML = tab;
            }
            else {
                document.getElementById("<%= PnlCAFDtl.ClientID%>").style.display = 'none';
                alert("Invalid File");
                document.getElementById("<%= cmbType.ClientID%>").value = "-1";
           }
            //--------------------- Clearing Data ------------------------//


       }
        function table_fill_Outward() {
           if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {


               document.getElementById("<%= PnlCAFDtl.ClientID%>").style.display = '';
              document.getElementById("filename").value = document.getElementById("<%= hid_filename.ClientID%>").value;
              var row_bg = 0;
              var tab = "";
              tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
              tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'sans-serif';' align='center'>";
              tab += "<tr style='background-color:#d3cdcd'>";
              tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
              tab += "<td style='width:5%;text-align:left' >Book Date</td>";
              tab += "<td style='width:5%;text-align:left'>Cost Center</td>";
              tab += "<td style='width:5%;text-align:left' >Batch No</td>";
              tab += "<td style='width:5%;text-align:left'>Branch</td>";
              tab += "<td style='width:10%;text-align:left' >MICR Days</td>";
              tab += "<td style='width:10%;text-align:left'>Product</td>";
              tab += "<td style='width:4%;text-align:left'>AcNo</td>";
              tab += "<td style='width:15%;text-align:left' >Customer</td>";
              tab += "<td style='width:12%;text-align:left'>ChequeNo</td>";
              tab += "<td style='width:6%;text-align:left'>ChequeAmt</td>";
              tab += "<td style='width:10%;text-align:left'>Drawee Bank</td>";
              tab += "<td style='width:4%;text-align:left'>Status</td>";
              tab += "<td style='width:7%;text-align:left'>Remarks</td>";
              tab += "</tr>";


              row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

               for (n = 0; n <= row.length - 2; n++) {
                   col = row[n].split("µ");

                   if (row_bg == 0) {
                       row_bg = 1;
                       tab += "<tr class=sub_first>";
                   }
                   else {
                       row_bg = 0;
                       tab += "<tr class=sub_second>";
                   }
                   i = n + 1;
                   if (col[12] == "") {
                       var fontcolor = '';

                   }
                   else {
                       var fontcolor = 'background-color:#c41e1e;color:#FFF;';
                   }
                   tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                   tab += "<td style='width:5%;text-align:left' >" + col[0] + "</td>";
                   tab += "<td style='width:5%;text-align:left'>" + col[1] + "</td>";
                   tab += "<td style='width:5%;text-align:left' >" + col[2] + "</td>";
                   tab += "<td style='width:5%;text-align:left'>" + col[3] + "</td>";
                   tab += "<td style='width:10%;text-align:left' >" + col[4] + "</td>";
                   tab += "<td style='width:10%;text-align:left'>" + col[5] + "</td>";
                   tab += "<td style='width:4%;text-align:left'>" + col[6] + "</td>";
                   tab += "<td style='width:15%;text-align:left' >" + col[7] + "</td>";
                   tab += "<td style='width:12%;text-align:left'>" + col[8] + "</td>";
                   tab += "<td style='width:6%;text-align:left'>" + col[9] + "</td>";
                   tab += "<td style='width:10%;text-align:left'>" + col[10] + "</td>";
                   tab += "<td style='width:4%;text-align:left'>" + col[11] + "</td>";
                  


                   tab += "<td style='width:7%;text-align:center;color:#FFF;" + fontcolor + "'>" + col[12] + "</td>";

                   tab += "</tr>";
               }

               tab += "</table></div></div></div>";

               document.getElementById("<%= PnlCAFDtl.ClientID%>").innerHTML = tab;
            }
            else {
                document.getElementById("<%= PnlCAFDtl.ClientID%>").style.display = 'none';
              alert("Invalid File");
              document.getElementById("<%= cmbType.ClientID%>").value = "-1";
            }
           //--------------------- Clearing Data ------------------------//

        }
        
        function btnClear_onclick() {
            document.getElementById("filename").value = "";
            document.getElementById("<%= hid_dtls.ClientID%>").value = "";
            document.getElementById("<%= PnlCAFDtl.ClientID%>").style.display = 'none';

            document.getElementById("<%= cmbType.ClientID%>").value = "-1";
            TypeOnChange();
        }
        function reset() {

            //document.getElementById("filename").innerHTML= document.getElementById("<%= hid_filename.ClientID%>").value;
            document.getElementById("<%= FileUpload1.ClientID%>").disabled = true;
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function TypeOnChange() {
            document.getElementById("<%= hid_dtls.ClientID%>").value = "";
            


            if (document.getElementById("<%= cmbType.ClientID%>").value != "-1") {

                document.getElementById("<%= FileUpload1.ClientID%>").disabled = false;
                document.getElementById("<%= btnUpload.ClientID%>").disabled = false;
            }
            else {

                document.getElementById("<%= FileUpload1.ClientID%>").disabled = true;
                document.getElementById("<%= btnUpload.ClientID%>").disabled = true;
            }
            document.getElementById("filename").value = "";
            document.getElementById("<%= hid_dtls.ClientID%>").value = "";
             document.getElementById("<%= PnlCAFDtl.ClientID%>").style.display = 'none';
        }
    </script>
   
</head>
</html>
<br /><br />

 <table class="style1" style="width:100%;margin: 0px auto;" >
           <tr id="Tr1"> <td style="width:25%;"><asp:HiddenField ID="hid_dtls" runat="server" /></td>
               <td style="width:12%; text-align:left;">Type</td>
            <td>&nbsp; &nbsp;<asp:DropDownList 
                     ID="cmbType" class="NormalText" 
                    style="text-align: left;" runat="server" 
                Width="40%" >
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                   <asp:ListItem Value="1">Inward</asp:ListItem>
                   <asp:ListItem Value="2">Outward</asp:ListItem>
                
                   
            </asp:DropDownList></td>
            
       </tr>
     <tr id="Tr3"> <td style="width:25%;"></td>
               <td style="width:12%; text-align:left;"></td>
            <td>&nbsp; &nbsp;<input id="filename" type="text" style="font-family: sans-serif; cursor: pointer; width:30%; border:none;" /></td>
            
       </tr>
        <tr id="branch"> <td style="width:25%;"><asp:HiddenField ID="hid_filename" runat="server" /></td>
        <td style="width:12%; text-align:left;">Upload File</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:FileUpload ID="FileUpload1" runat="server"  Enabled="false" /> 
                &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;<asp:Button ID="btnUpload" runat="server" style="font-family: sans-serif; cursor: pointer; width: 10%;" Text="UPLOAD" Font-Names="sans-serif" Enabled="false"/>&nbsp;
                &nbsp; &nbsp;<asp:ImageButton ID="cmd_Export_Excel" runat="server" Height="30px" ImageUrl="~/Image/ExcelExport.png" style="text-align:right ;" ToolTip="CTS" Width="30px" />
&nbsp;&nbsp; &nbsp;<input id="btnClear" style="font-family: sans-serif; cursor: pointer; width: 10%;" 
                type="button" value="CLEAR" onclick="return btnClear_onclick()" />
            &nbsp; &nbsp;<input id="btnExit" style="font-family: sans-serif; cursor: pointer; width: 10%;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
            </td>
            
       </tr>
       <tr id="Tr2"> <td style="width:25%;" colspan="3">
                 &nbsp; &nbsp;<asp:Panel ID="PnlCAFDtl" runat="server"></asp:Panel>
                </td>
        </tr>
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 &nbsp;<asp:HiddenField ID="hid_FileType" runat="server" /><asp:HiddenField ID="hid_FileSize" runat="server" /><asp:HiddenField ID="hid_FileDate" runat="server" />
            </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

