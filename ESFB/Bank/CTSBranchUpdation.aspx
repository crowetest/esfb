﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="CTSBranchUpdation.aspx.vb" Inherits="CTSBranchUpdation" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
     <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload 
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10pt;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10pt;color:#476C91;
        }
    </style>

    <script src="../Script/Validations.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
       
        function AlphaNumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = (e.which >= 48 && e.which <= 57) || (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || (e.which == 8) || (e.which == 13) || (e.which == 32) || (e.which == 0);

            if (!valid) {
                e.preventDefault();
            }
        }
        function NumericDotCheck(e)//------------function to check whether a value is alpha numeric
        {
            // el - this, e - event
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                    if (unicode == 37 || unicode == 38) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (unicode == 46) {
                        var parts = el.value.split('.');
                        if (parts.length == 2)
                            return false;
                        return true;
                    }
                }
            }
            else {
                return true;
            }
        }
        function NumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = ((e.which >= 48 && e.which <= 57) || (e.keyWhich == 9) || (e.keyCode == 9) || (e.keyWhich == 8) || (e.keyCode == 8));
            //alert(valid);
            if (!valid) {
                e.preventDefault();
            }

        }
        function FromServer(Arg, Context) {
            var Data = Arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("CTSBranchUpdation.aspx", "_self");
        }
        function btnApprove_onclick() {
            var ret = SaveData();
            if (ret==false) 
            {
                return false;
            }
           
          
            var strempcode = document.getElementById("<%= hid_temp.ClientID%>").value;
            var Data = "1Ø" + strempcode;
            ToServer(Data, 1);
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function DeleteRow(id) {
            
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            
            var NewStr = "";
             for (n =1; n <= row.length - 1 ; n++) {
                     
                     if (id != n) {

                         NewStr += "¥" + row[n];
                     }
                     else {
                         var AcNo = document.getElementById("txtAcNo" + n).value;
                         var Name = document.getElementById("txtAcName" + n).value;
                         var Cheque = document.getElementById("txtChkNo" + n).value;
                         var Amt = document.getElementById("txtAmt" + n).value;
                         var TranID = (document.getElementById("txtTranID" + n).value == "") ? 0 : document.getElementById("txtTranID" + n).value;

                         var strval = "¥" + AcNo + "µ" + Name + "µ" + Cheque + "µ" + Amt + "µ" + TranID + "µ1";
                   
                         if (TranID == 0 || TranID == "") {
                             NewStr += "";
                       
                         }
                         
                         else if  (TranID > 0 )
                         {
                             NewStr += strval;
                             }
                         else {

                             NewStr = "¥µµµµµ";
                        }

                   
                 }

             }
             
            document.getElementById("<%= hid_dtls.ClientID %>").value = NewStr; 
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "¥µµµµ0µ1" || document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                document.getElementById("<%= hid_dtls.ClientID %>").value = "¥µµµµµ";
            }
                table_fill();
        }
        function updateValue(id) {
            var NewStr = ""
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "¥µµµµµ") {
               
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                
               
                for (n = 1; n <= row.length - 1 ; n++) {
                   
                    if (id == n) {
                       
                       
                        var AcNo = document.getElementById("txtAcNo" + id).value;
                        var Name = document.getElementById("txtAcName" + id).value;
                        var Cheque = document.getElementById("txtChkNo" + id).value;
                        var Amt = document.getElementById("txtAmt" + id).value;
                        var TranID = (document.getElementById("txtTranID" + id).value == "") ? 0 : document.getElementById("txtTranID" + id).value;
                       
                        NewStr += "¥" + AcNo + "µ" + Name + "µ" + Cheque + "µ" + Amt + "µ" + TranID  + "µ0" ;

                    }
                    else {
                       
                        NewStr += "¥" + row[n];
                    }
                }
                
            }
            else {
                
                var AcNo = document.getElementById("txtAcNo1").value;;
                var Name = document.getElementById("txtAcName1").value;;
                var Cheque = document.getElementById("txtChkNo1").value;
                var Amt = document.getElementById("txtAmt1").value;
                var TranID = (document.getElementById("txtTranID1").value == "") ? 0 : document.getElementById("txtTranID1").value; 
                
                NewStr += "¥" + AcNo + "µ" + Name + "µ" + Cheque + "µ" + Amt + "µ" + TranID + "µ0";
            }
           
            document.getElementById("<%= hid_dtls.ClientID %>").value = NewStr;
            
        }
        function SaveData() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            //if (document.getElementById("<%= hid_dtls.ClientID %>").value == "" || document.getElementById("<%= hid_dtls.ClientID %>").value == "¥µµµµµ") {
              //  alert("Enter any cheque details");
              //  return false;
           // }
            var NewStr = ""
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "¥µµµµµ") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");


                for (n = 1; n <= row.length - 1 ; n++) {
                    if (row[n] != "µµµµµ") {
                        col = row[n].split("µ");
                        if (col[0] != "" && ( col[1] == "" || col[2] == "" || col[3] <= 0 || col[3] == "")) {
                            alert("Enter empty field");
                            document.getElementById("txtAcNo" + n).focus();
                            return false;

                        }
                        if (col[0] == "" &&  col[1] != "" ) {
                            alert("Enter Account Number");
                            document.getElementById("txtAcNo" + n).focus();
                            return false;

                        }
                        NewStr += "¥" + row[n];
                    }
                }

            }
          

            document.getElementById("<%= hid_temp.ClientID%>").value = NewStr;
            return true;
        }

        function CreateNewRow(e, val) {
            
            var n = (window.Event) ? e.which : e.keyCode;
            
            if (n == 13 || n == 9) {
                
                updateValue(val);
                AddNewRow();
            }
        }
        function table_fill() {
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                document.getElementById("<%= pnFamily.ClientID %>").style.display = '';
             var row_bg = 0;
             var tab = "";
             tab += "<div style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
             tab += "<tr height=20px;  class='tblQal'>";
             tab += "<td style='width:5%;text-align:center' >#</td>";
             tab += "<td style='width:20%;text-align:left' >A/C Number</td>";
             tab += "<td style='width:30%;text-align:left' >Cust Name</td>";
             tab += "<td style='width:25%;text-align:left' >Cheque No</td>";
             tab += "<td style='width:10%;text-align:left'>Amount</td>";
             tab += "<td style='width:1%;text-align:left; display:none;'></td>";
             tab += "<td style='width:9%;text-align:left'></td>";

             tab += "</tr>";
             tab += "</table></div>";
             tab += "<div id='ScrollDiv' style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

             row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var i = 0;
            for (n = 1; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                if (col[5] != 1) {
                    var i = i+1;
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }

                    tab += "<td style='width:5%;text-align:center' >" + i + "</td>";

                    //if (col[0] == "") {

                    var txtBox = "<input id='txtAcNo" + n + "' name='txtAcNo" + n + "' type='Text' style='width:99%;' value='" + col[0] + "' class='NormalText' onchange='updateValue(" + n + ")' maxlength='14' onkeypress='return NumericCheck(event)'  />";
                    tab += "<td style='width:20%;text-align:left' >" + txtBox + "</td>";
                    //}
                    //else {
                    //    tab += "<td style='width:30%;text-align:left'>" + col[0] + "</td>";
                    //}
                    //if (col[1] == "") {

                    var txtBox = "<input id='txtAcName" + n + "' name='txtAcName" + n + "' type='Text' value='" + col[1] + "' style='width:99%;' class='NormalText' maxlength='100' onchange='updateValue(" + n + ")' onkeypress='return AlphaNumericCheck(event)' />";
                    tab += "<td style='width:30%;text-align:left' >" + txtBox + "</td>";
                    //}
                    //else {
                    //    tab += "<td style='width:30%;text-align:left' >" + col[1] + "</td>";

                    //}

                    //if (col[2] == "") {

                    var txtBox = "<input id='txtChkNo" + n + "' name='txtChkNo" + n + "' type='Text' value='" + col[2] + "' style='width:99%;' class='NormalText' maxlength='6' onchange='updateValue(" + n + ")' onkeypress='return NumericCheck(event)' />";
                    tab += "<td style='width:25%;text-align:left' >" + txtBox + "</td>";
                    //}
                    //else {
                    //    tab += "<td style='width:30%;text-align:left' >" + col[2] + "</td>";

                    //}
                    //if (col[3] == "") {

                    var txtBox = "<input id='txtAmt" + n + "' name='txtAmt" + n + "' type='Text' value='" + col[3] + "' style='width:99%;' class='NormalText' maxlength='12' onkeypress='return NumericDotCheck(event)' onchange='updateValue(" + n + ")'  onkeydown='CreateNewRow(event," + n + ")'/>";
                    tab += "<td style='width:10%;text-align:left' >" + txtBox + "</td>";
                    //}
                    //else {
                    //    tab += "<td style='width:30%;text-align:left' >" + col[3] + "</td>";
                    var txtBox = "<input id='txtTranID" + n + "' name='txtTranID" + n + "' type='Text' value='" + col[4] + "' style='width:99%;' class='NormalText' maxlength='100' onkeypress='return NumericCheck(event)' onchange='updateValue(" + n + ")'  onkeydown='CreateNewRow(event," + n + ")'/>";
                    tab += "<td style='width:1%;text-align:left; display:none;'>" + txtBox + "</td>";
                    //}
                    //tab += "<td style='width:10%;text-align:left'></td>";
                    //if (col[0] == "" && col[1] == "" && col[2] == "")
                    //    tab += "<td style='width:10%;text-align:left'></td>";
                    //else
                    tab += "<td style='width:9%;text-align:center' onclick=DeleteRow('" + n + "')><img  src='../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                    tab += "</tr>";
                }
            }

            tab += "</table></div></div></div>";
            document.getElementById("<%= pnFamily.ClientID %>").innerHTML = tab;
            
            setTimeout(function () {
                document.getElementById("txtAcNo" + (n - 1)).focus().select();
            }, 4);
        }
        else
            document.getElementById("<%= pnFamily.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }

        function AddNewRow() {
            
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var Len = row.length - 1;
                col = row[Len].split("µ");
                if (col[0] != "" && col[1] != "" && col[2] != "" && col[3] != "") {

                    document.getElementById("<%= hid_dtls.ClientID %>").value += "¥µµµµµ";

                }
            }
            else {
                
                document.getElementById("<%= hid_dtls.ClientID%>").value = "¥µµµµµ";
            }
               
                table_fill();
        }
       
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />
     <div class="ScrollClass">
        <b>For any queries please write to cts@esafbank.com and for escalations, please mark mail to sanjeev.kumar@esafbank.com</b><hr
            style="color: #E31E24; margin-top: 0px;" />
    </div>
    <br />
    <br />
    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnFamily" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
               
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="UPDATE"  onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

