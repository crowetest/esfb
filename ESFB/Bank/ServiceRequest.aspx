﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="ServiceRequest.aspx.vb" Inherits="ServiceRequest" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %> 
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
            function RequestOnchange() {                     
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                document.getElementById("<%= txtCUGNo.ClientID%>").value="";
                document.getElementById("<%= txtEmailid.ClientID %>").value="";                 
           }            
   
           function CategoryOnChange() {           
               ClearCombo("<%= cmbType.ClientID %>");
       
               var Groupid = document.getElementById("<%= cmbCategory.ClientID %>").value;
                if (Groupid > 0) {
                    var ToData = "2Ø" + Groupid; 
                    ToServer(ToData, 2);
                }
            }
                  
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ñ");
       
                for (a = 0; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function ClearCombo(control) {
                document.getElementById(control).options.length = 0;
                var option1 = document.createElement("OPTION");
                option1.value = -1;
                option1.text = " -----Select-----";
                document.getElementById(control).add(option1);
            }
        function FromServer(arg, context) {        
            if (context == 1) 
            {
                var Data = arg.split("Ø");
                if (arg == "ØØ") 
                {
                    alert("Invalid Employee Code");
           
                    document.getElementById("<%= txtEmpCode.ClientID%>").value = "";
                    document.getElementById("<%= txtEmpName.ClientID%>").value = "";
            
                    document.getElementById("<%= txtDepartment.ClientID%>").value = "";
                    document.getElementById("<%= txtDesignation.ClientID%>").value = "";
            
                    document.getElementById("<%= txtReportingTo.ClientID%>").value = "";
            
                    document.getElementById("<%= txtCUGNo.ClientID%>").value = "";
                    document.getElementById("<%= txtEmailid.ClientID%>").value = "";            
                }
                else
                { 
                    document.getElementById("<%= txtEmpCode.ClientID%>").value = Data[0];
                    document.getElementById("<%= txtEmpName.ClientID%>").value = Data[1];
                    document.getElementById("<%= txtCUGNo.ClientID%>").value = Data[3];
                    document.getElementById("<%= txtDepartment.ClientID%>").value = Data[4];
                    document.getElementById("<%= txtDesignation.ClientID%>").value = Data[5];
                    document.getElementById("<%= txtEmailid.ClientID%>").value = Data[2];
                    document.getElementById("<%= txtReportingTo.ClientID%>").value = Data[8];
                }
            }
            else if (context == 2) {
                var Data = arg;
                ComboFill(Data, "<%= cmbType.ClientID%>");
                var Groupid = document.getElementById("<%= cmbCategory.ClientID %>").value;
                var Type_Id = document.getElementById("<%= cmbType.ClientID %>").value;

                if(Groupid == 8)
                    document.getElementById("rwDown").style.display = "";
                else
                    document.getElementById("rwDown").style.display = "none";

                if (Groupid == 9) 
                {
                    document.getElementById("TrIssue").style.display = "none";
                    document.getElementById("TrSolu").style.display = "none";
                    document.getElementById("TrHost").style.display = "";
                    document.getElementById("TrIP").style.display = "";
                    document.getElementById("TrUrl").style.display = "none";
                    document.getElementById("ServiceRequestNo").style.display = "";

                }
                else if (Groupid == 10)
                {
                    document.getElementById("TrIssue").style.display = "none";
                    document.getElementById("TrSolu").style.display = "none";
                    document.getElementById("TrHost").style.display = "none";
                    document.getElementById("TrIP").style.display = "none";
//                    alert(Type_Id);
//                    if (Type_Id == 33)
                        document.getElementById("TrUrl").style.display = "";
//                    else
//                        document.getElementById("TrUrl").style.display = "none";

                    document.getElementById("ServiceRequestNo").style.display = "";

                    
                }
                else
                {
                    document.getElementById("TrIssue").style.display = "";
                    document.getElementById("TrSolu").style.display = "";
                    document.getElementById("TrHost").style.display = "none";
                    document.getElementById("TrIP").style.display = "none";
                    document.getElementById("TrUrl").style.display = "none";
                    document.getElementById("ServiceRequestNo").style.display = "";

                }

            }       
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        function RequestOnClick() 
        {        
      
            var APP_ID	= document.getElementById("<%= cmbCategory.ClientID%>").value;
            var APP_Type_ID	= document.getElementById("<%= cmbType.ClientID%>").value;

         
            if (document.getElementById("<%= cmbCategory.ClientID%>").value == "-1") 
            {
                alert("Select Application Type");
                document.getElementById("<%= cmbCategory.ClientID%>").focus();
                return false;
            }      
            if (document.getElementById("<%= cmbType.ClientID %>").value == "-1") 
            {
                alert("Select Type");
                document.getElementById("<%= cmbType.ClientID %>").focus();
                return false;
            }     
            if (document.getElementById("<%= txtServiceRequestNo.ClientID%>").value == "")
            {       
                alert("Enter Your Service Request No");
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
                return false;
            }
            if (APP_ID == 9 )
            {
                if (document.getElementById("<%= txtHostName.ClientID%>").value == "") 
                {       
                    alert("Enter Host Name");
                    document.getElementById("<%= txtHostName.ClientID%>").focus();
                    return false;
                }
                if (document.getElementById("<%= txtIPName.ClientID%>").value == "") 
                {       
                    alert("Enter IP Name");
                    document.getElementById("<%= txtIPName.ClientID%>").focus();
                    return false;
                }  
            }
            else if (APP_ID == 10 )// new ssl vpn
            {
                if ((document.getElementById("txtUrl").value == "")  && (APP_Type_ID ==33))
                {       
                    alert("Enter URLs Required");
                    document.getElementById("txtUrl").focus();
                    return false;
                } 
            }
            else
            {
                if (document.getElementById("txtIssue").value == "") 
                {       
                    alert("Enter the Issue");
                    document.getElementById("txtIssue").focus();
                    return false;
                }
            }
            if (document.getElementById("<%= txtCUGNo.ClientID%>").value == "") 
            {
       
                alert("Enter Your CUG No");
                document.getElementById("<%= txtCUGNo.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%= txtEmailid.ClientID%>").value == "") 
            {       
                alert("Enter Your MailID");
                document.getElementById("<%= txtEmailid.ClientID%>").focus();
                return false;
            }
            var SRNo	= document.getElementById("<%= txtServiceRequestNo.ClientID%>").value;
            var Emailid	= document.getElementById("<%= txtEmailid.ClientID%>").value;
            var CUGNo	= document.getElementById("<%= txtCUGNo.ClientID%>").value;
            var Issue	= document.getElementById("txtIssue").value;
            var Solution	= document.getElementById("txtSolution").value;
            var Comments	= document.getElementById("txtComments").value;
            var TYPE_ID	= document.getElementById("<%= cmbType.ClientID%>").value;
            var onbehalfof	= document.getElementById("<%= txtonbehalfof.ClientID%>").value;
            var Effdate	= document.getElementById("<%= txteffdate.ClientID%>").value;
            var Empcode	= document.getElementById("<%= txtEmpCode.ClientID%>").value;
           

            var Host= document.getElementById("<%= txtHostName.ClientID%>").value;
            var IP	= document.getElementById("<%= txtIPName.ClientID%>").value;

            var URL	=  document.getElementById("txtUrl").value;
           

            document.getElementById("<%= hdnValue.ClientID %>").value = "1Ø" + APP_ID + "Ø" + TYPE_ID + "Ø" + SRNo + "Ø" + Emailid + "Ø" + CUGNo + "Ø" + Issue + "Ø" + Solution + "Ø" + Comments + "Ø" + onbehalfof +"Ø" + Effdate + "Ø" + Empcode + "Ø" + Host + "Ø" + IP + "Ø" + URL;       
        }
        function window_onload(){
            document.getElementById("rwDown").style.display = "none";
            EmployeeOnChange();
        }
        function EmployeeOnChange() 
        {  
            document.getElementById("<%= txtEmpCode.ClientID%>").value=document.getElementById("<%= hid_Empcode.ClientID%>").value;
            var EmpCode = document.getElementById("<%= txtEmpCode.ClientID%>").value;
            var TYPE_ID	= document.getElementById("<%= cmbType.ClientID %>").value;
        
            if (EmpCode != "")
                ToServer("1Ø" + EmpCode +"Ø" + document.getElementById("<%= cmbCategory.ClientID%>").value + "Ø" +TYPE_ID, 1);
        }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return window_onload()
            // ]]>
        </script>
    </head>
    </html>
   
    <br />
    <br />
       <table class="style1" style="width: 80%; margin: 0px auto;">
               
        <tr id="App">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Application
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbCategory" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Type">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Type
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbType" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
        <tr id="ServiceRequestNo">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
               Service Now Ticket No
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtServiceRequestNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15"  />
            </td>
        </tr>
        <tr id="Tr4">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
               On Behalf of
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtonbehalfof" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15"  />
            </td>
        </tr>
        <tr id="EmpCode">
            <td style="width: 25%;">
                &nbsp;
                   <asp:HiddenField ID="hid_Empcode" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
               Employee Code
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="20%" class="ReadOnlyTextBox" MaxLength="50" ReadOnly="true"  />
            </td>
        </tr>
        <tr id="Name">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Emp Name
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpName" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="60%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
            </td>
        </tr>
         <tr  id="Dep">
        <td style="width:25%;">
                   <asp:HiddenField ID="hid_Dtls" runat="server" />
                   </td>
            <td style="width:12% ; text-align:left;">
                Department</td>
            <td style="width:63% ;text-align:left;">
               &nbsp; &nbsp;<asp:TextBox ID="txtDepartment" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr  id="Des">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Designation</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesignation" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr id="ReportingTo">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Reporting Officer</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtReportingTo" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>

         
        
        <tr id="TrIssue">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_Post" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Issue
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<textarea id="txtIssue" class="NormalText" cols="20" name="S1" rows="3" onkeypress='return TextAreaCheck(event)' 
                    maxlength="1000" style="width: 70%"></textarea>
            </td>
        </tr>
        <tr id="TrSolu">
            <td style="width: 25%;">
                <asp:HiddenField ID="HiddenField1" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Proposed Solution
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<textarea id="txtSolution" class="NormalText" cols="20" name="S1" rows="3" onkeypress='return TextAreaCheck(event)' 
                    maxlength="1000" style="width: 70%"></textarea>
            </td>
        </tr>
        <tr id="TrHost" style="display:none">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Host Name
            </td>
            <td style="width: 63%">
                             
                &nbsp; &nbsp;<asp:TextBox ID="txtHostName" runat="server" Style="font-family: Cambria;font-size: 10pt;" Width="50%" class="NormalText" MaxLength="20"  />
            </td>
        </tr>
        <tr id="TrIP" style="display:none">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                IP Name
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtIPName" runat="server" Style="font-family: Cambria;font-size: 10pt;" Width="50%" class="NormalText" MaxLength="20"  />
            </td>
        </tr>
        <tr id="TrUrl" style="display:none">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                URLs Required
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<textarea id="txtUrl" class="NormalText" cols="20" name="S1" rows="3" onkeypress='return TextAreaCheck(event)' 
                    maxlength="1000" style="width: 70%"></textarea>
            </td>
        </tr>
        <tr id="Tr3">
            <td style="width: 25%;">
                <asp:HiddenField ID="HiddenField2" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Comments
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<textarea id="txtComments" class="NormalText" cols="20" name="S1" rows="3" onkeypress='return TextAreaCheck(event)' 
                    maxlength="1000" style="width: 70%"></textarea>
            </td>
        </tr>

        <tr id="rwDown">
        <td style="width:25%;">&nbsp;</td>
            <td style="text-align:left;" colspan="2">
                <asp:HyperLink ID="sftpFormat" runat="server" Font-Underline="True" 
                    ForeColor="#3333CC" href="SFTP_User-Creation_IT.XLSX">Download SFTP Format</asp:HyperLink>
            </td>
        </tr>
      
        
        <tr id="Img">
        <td style="width:25%;"><asp:HiddenField 
                    ID="hdn_Post" runat="server" />
                </td>
            <td style="width:12% ; text-align:left;">
                Attachments if any</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<input id="fup1" type="file" runat="server" 
                style="font-family: Cambria; font-size: 10.5pt" />
                </td>

        </tr>
      
        
        <tr id="contactno">
            <td style="width: 25%;">
             
            </td>
            <td style="width: 12%; align: left;">
                CUG No.
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtCUGNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="10" onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="Email">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Official
                Email Id
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmailid" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" MaxLength="50" />
            </td>
        </tr>
        <tr id="Tr5">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Effective Date
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txteffdate" class="NormalText" runat="server" Width="40%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CEFromDate" runat="server" TargetControlID="txteffdate" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
        </tr>
        
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:HiddenField ID="hdn_RetailFlg" runat="server" />
                <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
