﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CTSBranchClose_Del
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1161) = False Then
                'If GF.FormAccess(CInt(Session("UserID")), 1132) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Me.Master.subtitle = "Branch Updation Inactivation"
            
            'DT = DB.ExecuteDataSet("select '-1' as id,'------- select --------' union all select distinct a.Branch_id,Branch_name from branch_master a,cts_branch_close b where  a.branch_id=b.branch_id and (a.branch_id<=50000 and a.branch_id>1000)").Tables(0)
            DT = DB.ExecuteDataSet("select '-1' as id,'------- select --------' union select '-2' as id,'--- ALL ---' union all select distinct a.Branch_id,Branch_name from branch_master a,cts_branch_close b where  a.branch_id=b.branch_id and (a.branch_id<=50000 and a.branch_id>1000)").Tables(0)
            GF.ComboFill(cmbbranch, DT, 0, 1)

            'Me.cmbbranch.Attributes.Add("onchange", "return BranchOnchange()")
            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "setdata();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))



        If CInt(Data(0)) = 1 Then


            Dim UserID As Integer = CInt(Session("UserID"))
            Dim DTls As String = CStr(Data(1))

            Dim BranchID As Integer = CInt(Data(2))



            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@userID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(1).Value = BranchID
                Params(2) = New SqlParameter("@Dtls", SqlDbType.VarChar)
                Params(2).Value = DTls.Substring(1)
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_CTS_BRANCH_UPDATION_STATUS", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString
        ElseIf CInt(Data(0)) = 2 Then
            Dim StrAttendance As String = ""

            'DT = DB.ExecuteDataSet("select acno,acname,chequeno,amt,Format(a.updated_on, 'hh:mm tt'),tranid,isnull(delete_status,0),remarks  from cts_branch_updation a,cts_branch_close b where a.branch_id=b.branch_id and convert(date,a.updated_on)=convert(date,b.updated_on)" & _
            '        "  and  a.branch_id  =" & CInt(Data(1)).ToString() & " and convert(date,a.updated_on)='" & Data(2).ToString() & "'").Tables(0)

            If CInt(Data(1)) = -2 Then
                DT = DB.ExecuteDataSet("select acno,acname,chequeno,amt,Format(a.updated_on, 'hh:mm tt'),tranid,isnull(delete_status,0),remarks  from cts_branch_updation a,cts_branch_close b where a.branch_id=b.branch_id and convert(date,a.updated_on)=convert(date,b.updated_on)" & _
                    " and convert(date,a.updated_on)='" & Data(2).ToString() & "'").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select acno,acname,chequeno,amt,Format(a.updated_on, 'hh:mm tt'),tranid,isnull(delete_status,0),remarks  from cts_branch_updation a,cts_branch_close b where a.branch_id=b.branch_id and convert(date,a.updated_on)=convert(date,b.updated_on)" & _
                    "  and  a.branch_id  =" & CInt(Data(1)).ToString() & " and convert(date,a.updated_on)='" & Data(2).ToString() & "'").Tables(0)
            End If

            For Each DR As DataRow In DT.Rows
                StrAttendance += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString() + "µ" + DR(7).ToString()
            Next
            CallBackReturn = StrAttendance

        End If







    End Sub
#End Region

End Class
