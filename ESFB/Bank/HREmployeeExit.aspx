﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="HREmployeeExit.aspx.vb" Inherits="HREmployeeExit" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
    <script src="../../Script/Calculations.js" language="javascript" type="text/javascript"> </script> 
  <script language="javascript" type="text/javascript">
      function btnExit_onclick() 
      {
          window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
      }
      function EmployeeOnChange() 
      {
          var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
          if (EmpCode != "")
              ToServer("1Ø" + EmpCode, 1);
      }
      function FromServer(Arg, Context) {

          switch (Context) {

              case 1:
                  {
                      if (Arg == "ØØ") {
                          alert("Invalid Employee Code"); document.getElementById("<%= txtName.ClientID %>").value = "";
                          document.getElementById("<%= txtBranch.ClientID %>").value = "";
                          document.getElementById("<%= txtDepartment.ClientID %>").value = "";
                          document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                          document.getElementById("<%= txtDateOfJoin.ClientID %>").value = "";
                          document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false;
                      }
                      var Data = Arg.split("Ø");
                      document.getElementById("<%= txtName.ClientID %>").value = Data[0];
                      document.getElementById("<%= txtBranch.ClientID %>").value = Data[1];
                      document.getElementById("<%= txtDepartment.ClientID %>").value = Data[2];
                      document.getElementById("<%= txtDesignation.ClientID %>").value = Data[3];
                      document.getElementById("<%= txtDateOfJoin.ClientID %>").value = Data[4];
                      document.getElementById("<%= cmbType.ClientID %>").focus();
                      break;
                  }

              case 2:
                  {
                      var Data = Arg.split("Ø");
                      alert(Data[1]);
                      if (Data[0] == 0) window.open("HREmployeeExit.aspx", "_self");
                      break;
                  }

          }

      }
      function btnSave_onclick() {
          if (document.getElementById("<%= txtName.ClientID %>").value == "")
          { alert("Enter a Valid Employee Code"); document.getElementById("<%= txtEmpCode.ClientID %>").focus(); return false; }
          if (document.getElementById("<%= cmbType.ClientID %>").value == "-1")
          { alert("Select Type of Exit"); document.getElementById("<%= cmbType.ClientID %>").focus(); return false; }
          if (document.getElementById("<%= txtReason.ClientID %>").value == "")
          { alert("Enter Reason"); document.getElementById("<%= txtReason.ClientID %>").focus(); return false; }
         
          var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
          var TypeID = document.getElementById("<%= cmbType.ClientID %>").value;
          var Reason = document.getElementById("<%= txtReason.ClientID %>").value;
          var EffectiveDt = document.getElementById("<%= txtEffectiveDt.ClientID %>").value;
          ToServer("2Ø" + EmpCode + "Ø" + TypeID + "Ø" + Reason + "Ø" + EffectiveDt, 2);
      }



// ]]>
  </script><br />
    <table align="center" style="width:80%;margin: 0px auto;">
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
                </td>
            <td style="width:63% ;text-align:left;">
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Employee Code&nbsp;
                </td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Width="20%" class="NormalText" 
                    MaxLength="5" ></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Name
                </td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtName" runat="server" Width="60%" class="ReadOnlyTextBox"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Branch</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtBranch" runat="server" Width="60%" class="ReadOnlyTextBox"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Department</td>
            <td style="width:63% ;text-align:left;">
               &nbsp;&nbsp;<asp:TextBox ID="txtDepartment" runat="server" Width="60%" class="ReadOnlyTextBox"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Designation</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtDesignation" runat="server" Width="60%" class="ReadOnlyTextBox"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Date of Join</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtDateOfJoin" runat="server" Width="60%" class="ReadOnlyTextBox"></asp:TextBox>
                </td>
        </tr>
        <tr>

            <td style="text-align:center;" colspan="3">
                <strong>Exit Details</strong></td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Type</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbType" runat="server" class="NormalText" Width="60%">
                </asp:DropDownList>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Reason</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtReason" runat="server" Width="60%" class="NormalText"></asp:TextBox>
                </td>
        </tr>
        <tr >
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Effective Date</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtEffectiveDt" runat="server" class="NormalText" Width="40%" ReadOnly="true" ></asp:TextBox> 
                <asp:CalendarExtender ID="CE1" runat="server" TargetControlID="txtEffectiveDt" 
                    Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
        </tr>
       <br /><br />
        <tr>
        
         <td style="text-align:center;" colspan="3"> <br /><br /> <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE"  onclick="return btnSave_onclick()" />&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
        
    </table>
  <br /><br />
</asp:Content>

