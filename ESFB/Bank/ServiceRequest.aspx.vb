﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ServiceRequest
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1177) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Service Request Creation"
            Me.CEFromDate.StartDate = CDate(Session("TraDt"))
            hid_Empcode.Value = CStr(Session("UserID"))
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0

            DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER  where branch_id=" & CInt(Session("BranchID")) & "").Tables(0)

            If CInt(Session("BranchID")) < 100 Then
                DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER  ").Tables(0)
                hdn_RetailFlg.Value = "1"
            ElseIf CInt(Session("BranchID")) > 50000 Then
                DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER  ").Tables(0)
                hdn_RetailFlg.Value = "0"
            Else
                DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER ").Tables(0)
                hdn_RetailFlg.Value = "1"
            End If


            'If hdn_RetailFlg.Value = "0" Then 'usb
            DT = DB.ExecuteDataSet("select '-1' as id,'------Select--------' union all select app_id,app_name from ESFB.dbo.sr_app_master ").Tables(0)
            'Else
            'DT = DB.ExecuteDataSet("select '-1' as id,'------Select--------' union all select app_id,app_name from app_master ").Tables(0)
            'End If

            GN.ComboFill(cmbCategory, DT, 0, 1)


            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")


            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            txtEmpCode.Attributes.Add("onchange", "EmployeeOnChange()")
            cmbCategory.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim EmpID As Integer = CInt(Data(1))
        Dim strval As String
        Dim strfld As String
        Dim strfld1 As String
        Dim strwhere As String
        If CInt(Data(0)) = 1 Then
            If CInt(Data(2)) > 0 Then 'Profile

            Else
                strval = ""
                strfld = ""
                strfld1 = ""
            End If
            DT = DB.ExecuteDataSet("select distinct a.emp_code,a.emp_name,Official_mail_id " & _
                    " ,cug_no,department_name,designation_name,convert(varchar,a.date_of_join,106),a.reporting_to, " & _
                    " e.emp_name + '('+ convert(varchar,a.reporting_to)+')'  as reportingEmpCode,Emp_status from " & _
                    " Emp_master a,emp_profile b,department_master c,designation_master d,Emp_master e, " & _
                    " Employee_status f where a.emp_code=b.emp_code and a.department_id=c.Department_ID and a.designation_id=d.designation_id  " & _
                    " and a.reporting_to=e.emp_code and a.emp_status_id=f.emp_status_id  and  a.emp_code=" + CInt(Data(1)).ToString + " and a.branch_id=" & CInt(Session("BranchID"))).Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString() + "Ø" + DT.Rows(0)(6).ToString() + "Ø" + DT.Rows(0)(7).ToString() + "Ø" + DT.Rows(0)(8).ToString() + "Ø" + DT.Rows(0)(9).ToString()
            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf CInt(Data(0)) = 2 Then
            DT = DB.ExecuteDataSet("select  '-1' as id,' -----Select------' as Name union all  select app_type_id,app_type_name from ESFB.dbo.sr_App_Type_master where status_id=1 and app_id=" + Data(1).ToString()).Tables(0)
            If (DT.Rows.Count > 0) Then
                Dim StrVal1 As String = ""

                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal1 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal1 += "Ñ"
                    End If
                Next

                Dim StrVal2 As String = ""
                CallBackReturn = StrVal1

            Else
                CallBackReturn = "ØØ"
            End If


        End If
    End Sub
#End Region

    Private Sub initializeControls()

        cmbCategory.Text = "-1"
        cmbType.Text = "-1"
        txtEmpCode.Text = ""
        txtEmpName.Text = ""

        'txtdetails.Text = ""
        txtCUGNo.Text = ""

        txtDepartment.Text = ""
        txtDesignation.Text = ""
        txtEmpName.Text = ""
        txtEmailid.Text = ""
        txteffdate.Text = ""
        txtonbehalfof.Text = ""
        txtServiceRequestNo.Text = ""


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim myFile As HttpPostedFile = fup1.PostedFile
            Dim FileLen As Integer = myFile.ContentLength
            Dim FileName As String = ""
            If (FileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(FileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, FileLen)
            End If
            Dim Result As String
            Dim UserID As String = Session("UserID").ToString()

            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim APP_ID As Integer = CInt(Data(1))
            Dim Branch As Integer = CInt(Session("BranchID"))
            Dim TYPE_ID As Integer = CInt(Data(2))
            Dim SRNo As String = CStr(Data(3))
            Dim Emailid As String = CStr(Data(4))
            Dim CUGNo As String = CStr(Data(5))
            Dim Issue As String = CStr(Data(6))
            Dim Solution As String = CStr(Data(7))
            Dim Comments As String = CStr(Data(8))
            Dim onbehalfof As String = CStr(Data(9))
            Dim Effdate As Date = CDate(IIf(Data(10) = "", "01/01/1900", Data(10)))
            Dim Empcode As Integer = CInt(Data(11))
            Dim Host As String = CStr(Data(12))
            Dim IP As String = CStr(Data(13))
            Dim URL As String = CStr(Data(14))



            Try
                Dim Params(20) As SqlParameter
                Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
                Params(0).Value = Branch
                Params(1) = New SqlParameter("@APP_ID", SqlDbType.Int)
                Params(1).Value = APP_ID
                Params(2) = New SqlParameter("@TYPE_ID", SqlDbType.Int)
                Params(2).Value = TYPE_ID
                Params(3) = New SqlParameter("@REQUEST_ID", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = UserID
                Params(5) = New SqlParameter("@PROPOSED_SOLN", SqlDbType.NVarChar, 4000)
                Params(5).Value = Solution
                Params(6) = New SqlParameter("@Issue", SqlDbType.NVarChar, 4000)
                Params(6).Value = Issue
                Params(7) = New SqlParameter("@Comments", SqlDbType.NVarChar, 4000)
                Params(7).Value = Comments
                Params(8) = New SqlParameter("@ON_BEHALF_OF", SqlDbType.VarChar, 500)
                Params(8).Value = onbehalfof
                Params(9) = New SqlParameter("@CUG", SqlDbType.VarChar, 20)
                Params(9).Value = CUGNo
                Params(10) = New SqlParameter("@EMAIL", SqlDbType.VarChar, 100)
                Params(10).Value = Emailid
                Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(11).Direction = ParameterDirection.Output
                Params(12) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(12).Direction = ParameterDirection.Output
                Params(13) = New SqlParameter("@ServiceRequestNo", SqlDbType.VarChar, 50)
                Params(13).Value = SRNo
                Params(14) = New SqlParameter("@eff_date", SqlDbType.Date)
                Params(14).Value = Effdate.ToString
                Params(15) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                Params(15).Value = AttachImg
                Params(16) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                Params(16).Value = ContentType
                Params(17) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                Params(17).Value = FileName
                Params(18) = New SqlParameter("@HostName", SqlDbType.VarChar, 20)
                Params(18).Value = Host
                Params(19) = New SqlParameter("@IPName", SqlDbType.VarChar, 20)
                Params(19).Value = IP
                Params(20) = New SqlParameter("@URL", SqlDbType.VarChar, 1000)
                Params(20).Value = URL
                DB.ExecuteNonQuery("SP_SR_APP_REQUEST", Params)
                ErrorFlag = CInt(Params(11).Value)
                Message = CStr(Params(12).Value)
            Catch ex1 As Exception
                Message = ex1.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("        window.open('ServiceRequest.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)





        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
