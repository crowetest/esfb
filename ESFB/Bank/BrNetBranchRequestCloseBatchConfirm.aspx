﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="BrNetBranchRequestCloseBatchConfirm.aspx.vb" Inherits="BrNetBranchRequestCloseBatchConfirm"
    EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style2
        {
            width: 11%;
        }
    </style>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

        function window_onload() {
            document.getElementById("rowplnShow").style.display = "none";
            ToServer("1ʘ", 1);
        }

        function FromServer(Arg, Context) 
        {
            switch(Context)
            {
                case 1: //  Employee Fill On Change Of Department
                {                     
                    if(Arg != "")
                    {
                        ComboFill(Arg, "<%= cmbBatch.ClientID %>");
                    }
                    break;
                }
                case 2: //  Employee Fill On Change Of Department
                {               
                    if(Arg != "")
                    {                        
                        var Data = Arg.split("¶");
                       
                        document.getElementById("txtCommonRemarks").value = Data[1];
                        document.getElementById("<%= hid_dtls.ClientID %>").value = Data[0];  
                        document.getElementById("<%= hid_verData.ClientID %>").value = Data[2]; 
                        table_fill();
                    }
                    break;
                }
                case 3:
                {
                    if(Arg != "")
                    { 
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("BrNetBranchRequestCloseBatchConfirm.aspx", "_self");
                    }
                }
            }
        }

        function BatchOnChange()
        {
            var BatchNo = document.getElementById("<%= cmbBatch.ClientID %>").value;
            ToServer("2ʘ" + BatchNo, 2);
        }

        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ř");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("Ĉ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

        function table_fill() {           
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';           
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr style='background-color:#efe1ef;'>";
            tab += "<td style='width:1%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:5%;text-align:left'>Request No</td>";
            tab += "<td style='width:5%;text-align:left'>CreatedBranch</td>";
            tab += "<td style='width:3%;text-align:left'>UserID</td>";
            tab += "<td style='width:5%;text-align:left'>Emp_Name</td>";
            tab += "<td style='width:4%;text-align:left' >Entity</td>";
            tab += "<td style='width:5%;text-align:left' >Application</td>";
            tab += "<td style='width:5%;text-align:left'>Type</td>";
            tab += "<td style='width:5%;text-align:left' >FromBranch</td>";
            tab += "<td style='width:5%;text-align:left' >ToBranch</td>";
            tab += "<td style='width:4%;text-align:center'>Role</td>";
            tab += "<td style='width:4%;text-align:center'>PrevRole</td>";
            tab += "<td style='width:5%;text-align:left'>Dep_Name</td>";
            tab += "<td style='width:5%;text-align:left'>Desg_Name</td>";            
            tab += "<td style='width:5%;text-align:left'>Mobile</td>";
            tab += "<td style='width:5%;text-align:left'>Created By</td>";
            tab += "<td style='width:3%;text-align:center'>Edit Batch</br><input type='checkbox' id='chkBASelectAll'  onclick='SelectOnClick(-1,4,0,0)' /></td>";            
            tab += "<td style='width:5%;text-align:center'>CIF</td>";
            tab += "<td style='width:3%;text-align:center'>CA</td>";
            tab += "<td style='width:3%;text-align:center'>GL</td>";
            tab += "<td style='width:5%;text-align:center'>Remarks</td>";
            tab += "<td style='width:3%;text-align:center'>Reject</td>";
            tab += "<td style='width:7%;text-align:center'>Reason</td>";
            tab += "</tr>";
            
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {            

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
               
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("µ");
                    
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr style='background-color:#FBEFFB;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr style='background-color:#FFF;'>";
                    }
                    i = n + 1;
                    //c.app_request_id,d.app_dtl_id,j.app_name,h.branch_name,d.emp_code, f.Emp_Name,e.App_Type_name,d.Remarks,g.App_role_name,i.branch_name as To_branch

                    tab += "<td style='width:1%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[21] + "</td>";
                    tab += "<td style='width:3%;text-align:left'>" + col[24] + "</td>";// UserID
                    tab += "<td style='width:5%;text-align:left'>" + col[8] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[23] + "</td>";
                    tab += "<td style='width:5%;text-align:left' >" + col[2] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[3] + "</td>";                    
                    tab += "<td style='width:5%;text-align:left' >" + col[4] + "</td>";
                    tab += "<td style='width:5%;text-align:left' >" + col[5] + "</td>";
                    tab += "<td style='width:4%;text-align:center'>" + col[7] + "</td>";
                    tab += "<td style='width:4%;text-align:center'>" + col[6] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[9] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[10] + "</td>";                   
                    tab += "<td style='width:5%;text-align:left'>" + col[12] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[13] + " By</td>";                 
                    tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkSelectBA" + col[17] + "' onkeypress='return TextAreaCheck(event)'  onclick='SelectOnClick(" + col[17] + ",4," + col[18] + "," + col[14] + ")'  /></td>";
                    var txtBox2 = "<textarea id='txtCIF" + col[17] + "' name='txtCIF" + col[17] + "' style='width:99%; float:left;' maxlength='14' onkeypress='return NumericCheck(event)' onchange='updateCifValue(" + col[17] + ")' >" +  col[19] + "</textarea>";
                    tab += "<td style='width:5%;text-align:center'>" + txtBox2 + "</td>";                    
                    if(col[15] == 1)
                    {
                        tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkSelectCA" + col[17] + "' checked=true onclick='SelectOnClick(" + col[17] + ",2," + col[18] + "," + col[14] + ")'  /></td>";
                    }
                    else {
                        if (col[14] == 50)
                            tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkSelectCA" + col[17] + "' disabled=false onclick='SelectOnClick(" + col[17] + ",2," + col[18] + "," + col[14] + ")'  /></td>";
                        else
                            tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkSelectCA" + col[17] + "' disabled=true onclick='SelectOnClick(" + col[17] + ",2," + col[18] + "," + col[14] + ")'  /></td>";
                    }
                    if(col[16] == 1)
                    {
                        tab += "<td style='width:3%;text-align:center'><input checked type='checkbox' id='chkSelectGL" + col[17] + "' checked=true  onclick='SelectOnClick(" + col[17] + ",3," + col[18] + "," + col[14] + ")'  /></td>";
                    }
                    else
                    {
                        if(col[14] == 50)
                            tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkSelectGL" + col[17] + "' disabled=false onclick='SelectOnClick(" + col[17] + ",3," + col[18] + "," + col[14] + ")'  /></td>";
                        else
                            tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkSelectGL" + col[17] + "' disabled=true onclick='SelectOnClick(" + col[17] + ",3," + col[18] + "," + col[14] + ")'  /></td>";
                    }
                    var txtBox1 = "<textarea id='txtRemarks" + col[17] + "' name='txtRemarks" + col[17] + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)'  onchange='updateRemarkValue(" + col[17] + ")' >" + col[20] + "</textarea>";
                    tab += "<td style='width:5%;text-align:center'>"+ txtBox1 +"</td>";     
                    
                    tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkReject" + col[17] + "' onclick='RejectOnClick(" + col[17] + ",1," + col[18] + "," + col[14] + ")'  /></td>";
                            
                    var txtBox1 = "<textarea id='txtReason" + col[17] + "' name='txtReason" + col[17] + "' disabled=true style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)' onchange='updateReasonValue(" + col[17] + ")' ></textarea>";
                    tab += "<td style='width:7%;text-align:center'>"+ txtBox1 +"</td>";
                                             
                    tab += "</tr>";
                }
            }
           
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
            document.getElementById("rowplnShow").style.display = "";

            //--------------------- Clearing Data ------------------------//
        }
        function SelectOnClick(BatchDtlID,ID,BatchID,apptypeid) {  
            var Newstr = "";
            if (BatchDtlID < 0) 
            {                
                if (ID == 4) 
                {
                    row = document.getElementById("<%= hid_verData.ClientID%>").value.split("¥");
                    for (n = 0; n <= row.length - 2; n++)
                    {
                        //'CA, GL, Batch_dtl_ID, BatchID, CIF ID, Remarks, ApptypeID, IsReject, Reason;
                        col = row[n].split("µ");                        
                            
                        if(document.getElementById("chkBASelectAll").checked == true)
                        {
                            document.getElementById("chkSelectBA" + col[2]).checked = true;
                            if (document.getElementById("chkSelectBA" + col[2]).checked == false) {                                
                                document.getElementById("chkSelectCA" + col[2]).checked = false;
                                document.getElementById("chkSelectGL" + col[2]).checked = false;
                            }
                            if (document.getElementById("chkSelectBA" + col[2]).checked == true && col[6] ==50) {                                
                                document.getElementById("chkSelectCA" + col[2]).disabled = false;
                                document.getElementById("chkSelectGL" + col[2]).disabled = false;
                                document.getElementById("txtCIF" + col[2]).disabled = false;
                            }
                        }
                        else{
                            document.getElementById("chkSelectBA" + col[2]).checked = false;                           
                            document.getElementById("chkSelectCA" + col[2]).disabled = true;
                            document.getElementById("chkSelectGL" + col[2]).disabled = true;
                            document.getElementById("txtCIF" + col[2]).disabled = true;
                        }
                    }
                }
            }
            else{
                row = document.getElementById("<%= hid_verData.ClientID%>").value.split("¥");
                for (n = 0; n <= row.length - 2; n++)
                {
                    //'CA, GL, Batch_dtl_ID, BatchID, CIF ID, Remarks, ApptypeID, IsReject, Reason;
                    col = row[n].split("µ");    
                    if (col[2] == BatchDtlID) { 
                        if(ID == 2)
                        {
                            if (document.getElementById("chkSelectCA" + BatchDtlID).checked == true)   
                                Newstr += "1µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";
                            else
                                Newstr += "0µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";
                        }
                        if(ID == 3)
                        {
                            if (document.getElementById("chkSelectGL" + BatchDtlID).checked == true)   
                                Newstr += col[0] + "µ1µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";
                            else
                                Newstr += col[0] + "µ0µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";
                        }
                    }  
                    else {
                        Newstr += row[n] + "¥";
                    }                                    
                }
                if (Newstr != "")
                    document.getElementById("<%= hid_verData.ClientID %>").value = Newstr;

                if (document.getElementById("chkSelectBA" + BatchDtlID).checked == false) {
                    document.getElementById("chkBASelectAll").checked = false;
                }
                if (document.getElementById("chkSelectBA" + BatchDtlID).checked == true && apptypeid == 50) {                        
                    document.getElementById("chkSelectCA" + BatchDtlID).disabled = false;
                    document.getElementById("chkSelectGL" + BatchDtlID).disabled = false;
                    document.getElementById("txtCIF" + BatchDtlID).disabled = false;
                }
                else if (document.getElementById("chkSelectBA" + BatchDtlID).checked == false && apptypeid == 50) {
                    document.getElementById("chkSelectCA" + BatchDtlID).disabled = true;
                    document.getElementById("chkSelectGL" + BatchDtlID).disabled = true;
                    document.getElementById("txtCIF" + BatchDtlID).disabled = true;
                }
                else {                                            
                    document.getElementById("chkSelectCA" + BatchDtlID).disabled = true;
                    document.getElementById("chkSelectGL" + BatchDtlID).disabled = true;
                    //document.getElementById("txtCIF" + BatchDtlID).disabled = true;
                }
            }
        }        
        function RejectOnClick(BatchDtlID,ID,BatchID,apptypeid) {             
            if (document.getElementById("chkReject" + BatchDtlID).checked == true) 
                document.getElementById("txtReason" + BatchDtlID).disabled=false;
            else
                 document.getElementById("txtReason" + BatchDtlID).disabled=true;     
            updateReasonValue(BatchDtlID);      
        }
        function updateReasonValue(BatchDtlID)
        { 
            var Newstr = "";    
            var txtReason = document.getElementById("txtReason" + BatchDtlID).value;     
            if(document.getElementById("chkReject" + BatchDtlID).checked == true && txtReason == "") {
                alert("Please enter Reject Reason");
                document.getElementById("txtReason" + BatchDtlID).focus();
                return false;     
            } 
            else if(document.getElementById("chkReject" + BatchDtlID).checked == true)       
            {            
                row = document.getElementById("<%= hid_verData.ClientID%>").value.split("¥");
                for (n = 0; n <= row.length - 2; n++)
                {                    
                    col = row[n].split("µ");   
                    //'CA, GL, Batch_dtl_ID, BatchID, CIF ID, Remarks, ApptypeID, IsReject, Reason;;
                    if (col[2] == BatchDtlID)  
                        Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ1µ" + txtReason + "¥";                    
                    else
                        Newstr += row[n] + "¥";                    
                }
                document.getElementById("<%= hid_verData.ClientID %>").value = Newstr;
            }
            else{
                row = document.getElementById("<%= hid_verData.ClientID%>").value.split("¥");
                for (n = 0; n <= row.length - 2; n++)
                {                   
                    col = row[n].split("µ");   
                    //'CA, GL, Batch_dtl_ID, BatchID, CIF ID, Remarks, ApptypeID, IsReject, Reason;
                    if (col[2] == BatchDtlID)  
                        Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ0µ¥";                    
                    else
                        Newstr += row[n] + "¥";                    
                }
                document.getElementById("<%= hid_verData.ClientID %>").value = Newstr;
            }
        }  
        function updateRemarkValue(BatchDtlID)
        { 
            var Newstr = "";    
            var txtRemarks = document.getElementById("txtRemarks" + BatchDtlID).value;    
            row = document.getElementById("<%= hid_verData.ClientID%>").value.split("¥");
            for (n = 0; n <= row.length - 2; n++)
            {               
                col = row[n].split("µ");   
                //'CA, GL, Batch_dtl_ID, BatchID, CIF ID, Remarks, ApptypeID, IsReject, Reason;
                if (col[2] == BatchDtlID)  
                    Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + txtRemarks + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";                    
                else
                    Newstr += row[n] + "¥";                    
            }
            document.getElementById("<%= hid_verData.ClientID %>").value = Newstr;
        } 
        function updateCifValue(BatchDtlID)
        {                        
            var Newstr = "";    
            var txtCif = document.getElementById("txtCIF" + BatchDtlID).value;   
            if(txtCif.length != "12") {
                alert("CIF length should be 12 digits ");
                document.getElementById("txtCIF" + BatchDtlID).focus();
                return false;     
            }         
            var row = document.getElementById("<%= hid_verData.ClientID%>").value.split("¥");
            for (n = 0; n <= row.length - 2; n++)
            {               
                col = row[n].split("µ");   
                //'CA, GL, Batch_dtl_ID, BatchID, CIF ID, Remarks, ApptypeID, IsReject, Reason;
                if (col[2] == BatchDtlID)  
                    Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + txtCif + "µ" + col[5] + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";                    
                else
                    Newstr += row[n] + "¥";                    
            }
            document.getElementById("<%= hid_verData.ClientID %>").value = Newstr;
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnSave_onclick(){  
            row = document.getElementById("<%= hid_verData.ClientID%>").value.split("¥");
            for (n = 0; n <= row.length - 2; n++)
            {    
                col = row[n].split("µ"); 
                if (col[7] == 1 && col[8] == "")   
                {
                    alert("CIF length should be 12 digits ");
                    document.getElementById("txtReason" + col[2]).focus();
                    return false;    
                } 
            }
            if (confirm("Are you sure to Save This ") == 1) {
                var ToData = "3ʘ" + document.getElementById("txtCommonRemarks").value + "ʘ" + document.getElementById("<%= hid_verData.ClientID %>").value;
                   
                ToServer(ToData, 3);
            }
        }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
    // <![CDATA[
    return window_onload()
    // ]]>
        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 100%; margin: 0px auto;">
        <tr id="Tr3">
            <td style="width: 25%;">
            </td>
            <td style="text-align: left;" class="style2">
            </td>
            <td>
                &nbsp; &nbsp;<input id="filename" type="text" style="font-family: sans-serif; cursor: pointer;
                    width: 30%; border: none;" />
            </td>
        </tr>
        <tr id="Tr1">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="text-align: left;" class="style2">
                Select Batch
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbBatch" runat="server" Height="16px" Width="317px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="rowplnShow">
            <td style="width: 25%;" colspan="3">
                <asp:Panel ID="pnLeaveApproveDtl" runat="server" Width="100%">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <textarea id="txtCommonRemarks" name="txtCommonRemarks" style="width: 100%; float: left;" onkeypress='return TextAreaCheck(event)' 
                    maxlength="300"></textarea>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="SAVE" onclick="return btnSave_onclick()" />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_verData" runat="server" />
            </td>
        </tr>
    </table>
    <configuration>
    <system.web>
    <compilaton debug="true" targetFramework="4.0"></compilaton>
    </system.web>
    </configuration>
</asp:Content>
