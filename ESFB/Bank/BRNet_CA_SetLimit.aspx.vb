﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports System.Configuration
Imports System
Imports System.Text

Partial Class BRNet_CA_SetLimit
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim DTExcel As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim values As New DataTable
    Dim RequestID, UserID As Integer

#Region "Page Load & Dispose"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If GF.FormAccess(CInt(Session("UserID")), 1239) = False Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        If Not IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.cmbBatch.Attributes.Add("onchange", "return BatchOnChange()")
        End If

    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
    Sub DeleteFilesFromFolder(ByVal Folder As String)
        If Directory.Exists(Folder) Then
            For Each _file As String In Directory.GetFiles(Folder)
                File.Delete(_file)
            Next
            For Each _folder As String In Directory.GetDirectories(Folder)

                DeleteFilesFromFolder(_folder)
            Next

        End If

    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        UserID = CInt(Session("UserID"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1 'Fill Vet Request For Approve              
                DT = GF.GetQueryResult("select -1,'---Select---' union select distinct b.batch_id, b.batch_name from ESFB.dbo.App_brnet_request_batch_dtl a, ESFB.dbo.App_brnet_request_batch b where a.ca_status_dtl=1 and a.batch_id=b.batch_id and a.acno is not null and b.status_id=1 and b.ca_status=10 and limitset_by is null")
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2
                Dim BatchID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select convert(varchar(12),a.cif)+'Æ'+convert(varchar(12),a.emp_code)+'Æ'+a.account_title+'Æ'+cast(cast(a.acno as bigint) as varchar(14)) from ESFB.dbo.App_brnet_request_batch_dtl a, ESFB.dbo.App_brnet_request_batch b where a.batch_id=" & BatchID & " and a.ca_status_dtl=1 and a.batch_id=b.batch_id and a.acno is not null and b.status_id=1 and b.ca_status=10 and limitset_by is null")
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += DR(0).ToString() + "®"
                    Next
                End If
            Case 3
                Dim BatchID As Integer = CInt(Data(1))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(3) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@BatchID", SqlDbType.VarChar)
                    Params(1).Value = BatchID
                    Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(2).Direction = ParameterDirection.Output
                    Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(3).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_UPDATE_CA_LIMIT", Params)
                    ErrorFlag = CInt(Params(2).Value)
                    Message = CStr(Params(3).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region

   
End Class
