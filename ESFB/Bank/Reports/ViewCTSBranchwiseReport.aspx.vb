﻿Imports System.Data
Imports System.IO

Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewCTSBranchwiseReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DTExcel As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim BRANCHID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("BRANCHID")))
            Dim BRANCHNAME As String = CStr(GF.Decrypt(Request.QueryString.Get("BRANCHNAME")))
            Dim DateFrom As Date = CDate(Request.QueryString.Get("FROM"))


            Dim DTHead As New DataTable
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable



            RH.Heading(Session("FirmName"), tb, "BRANCH WISE -CTS REPORT (" + Format(DateFrom, "dd-MMM-yyyy") + ")", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")


           

            
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            '-----------------
            
          
            Dim strval As String
            If BRANCHID > 0 Then
                strval = " 1=1 and a.branch_id  =" & CInt(BRANCHID).ToString() & "  "
            Else

                strval = " 1=1 "
            End If


            Dim prevBranchid As Integer = 0
            Dim prevBranchName As String


            DTExcel = DB.ExecuteDataSet("select convert(varchar,ROW_NUMBER() OVER(ORDER BY chequeno asc)) AS 'Sl No',a.branch_id as BranchCode,upper(b.branch_name)  as BranchName,acno as AccountNo,upper(acname) as AccountName,chequeno  as ChequeNo,amt as Amount,Format(updated_on, 'hh:mm tt') as Time,case when (delete_status is null or delete_status=0 or delete_status=1) then 'Processed' else 'Rejected' end as Status,status_updated_on,remarks from cts_branch_updation a left join branch_master b on a.branch_id=b.branch_id  where  " & strval & " and convert(date,updated_on)='" + DateFrom.ToString("dd/MMM/yyyy") + "'  order by chequeno").Tables(0)
            'RH.BlankRow(tb, 3)
            DT = DB.ExecuteDataSet("select convert(varchar,ROW_NUMBER() OVER(ORDER BY chequeno DESC)) AS 'Sl No',a.branch_id,acno,upper(acname),chequeno,amt,Format(updated_on, 'hh:mm tt'),tranid,upper(b.branch_name),isnull(delete_status,0),case when (delete_status is null or delete_status=0 or delete_status=1) then 'Processed' else 'Rejected' end as Status,status_updated_on,remarks from cts_branch_updation a left join branch_master b on a.branch_id=b.branch_id  where  " & strval & " and convert(date,updated_on)='" + DateFrom.ToString("dd/MMM/yyyy") + "' order by a.branch_id,a.delete_status").Tables(0)

            Dim Chkcnt As Integer = 0
            Dim TotAmount As Double = 0
            Dim GrandTotal As Double = 0
            j = 0

            'Dim TRHead11 As New TableRow
            'TRHead11.BackColor = Drawing.Color.WhiteSmoke
            'Dim TRHead11_00, TRHead11_01, TRHead11_02, TRHead11_03, TRHead11_04, TRHead11_05 As New TableCell

            'TRHead11_00.BorderWidth = "1"
            'TRHead11_01.BorderWidth = "1"
            'TRHead11_02.BorderWidth = "1"
            'TRHead11_03.BorderWidth = "1"
            'TRHead11_04.BorderWidth = "1"
            'TRHead11_05.BorderWidth = "1"


            'TRHead11_00.BorderColor = Drawing.Color.Silver
            'TRHead11_01.BorderColor = Drawing.Color.Silver
            'TRHead11_02.BorderColor = Drawing.Color.Silver
            'TRHead11_03.BorderColor = Drawing.Color.Silver
            'TRHead11_04.BorderColor = Drawing.Color.Silver
            'TRHead11_05.BorderColor = Drawing.Color.Silver


            'TRHead11_00.BorderStyle = BorderStyle.Solid
            'TRHead11_01.BorderStyle = BorderStyle.Solid
            'TRHead11_02.BorderStyle = BorderStyle.Solid
            'TRHead11_03.BorderStyle = BorderStyle.Solid
            'TRHead11_04.BorderStyle = BorderStyle.Solid
            'TRHead11_05.BorderStyle = BorderStyle.Solid



            'RH.AddColumn(TRHead11, TRHead11_00, 5, 5, "l", "Sl No.")
            'RH.AddColumn(TRHead11, TRHead11_01, 25, 25, "l", "A/C Number")
            'RH.AddColumn(TRHead11, TRHead11_02, 40, 40, "l", "A/C Name")
            'RH.AddColumn(TRHead11, TRHead11_03, 10, 10, "l", "Cheque No")
            'RH.AddColumn(TRHead11, TRHead11_04, 10, 10, "l", "Time")
            'RH.AddColumn(TRHead11, TRHead11_05, 10, 10, "l", "Amount")
            'tb.Controls.Add(TRHead11)
            Dim ActiveAmt As Double = 0
            Dim InactiveAMt As Double = 0
            Dim Grand_ActiveAmt As Double = 0
            Dim Grand_InactiveAMt As Double = 0
            For Each DR In DT.Rows
                

                j += 1

                I = I + 1
                TotAmount += CDbl(DR(5))
                If DR(9) = 2 Then
                    InactiveAMt += CDbl(DR(5))
                Else
                    ActiveAmt += CDbl(DR(5))
                End If
                If prevBranchid <> DR(1) Then
                    If prevBranchid <> 0 Then
                        I = I - 1
                        TotAmount -= CDbl(DR(5))
                        If DR(9) = 2 Then
                            InactiveAMt = InactiveAMt - CDbl(DR(5))
                        Else
                            ActiveAmt = ActiveAmt - CDbl(DR(5))
                        End If
                        Dim TRItemHead72 As New TableRow
                        TRItemHead72.ForeColor = Drawing.Color.DarkBlue
                        Dim TRItemHead72_00, TRItemHead72_01, TRItemHead72_02, TRItemHead72_03, TRItemHead72_04, TRItemHead72_05 As New TableCell

                        RH.AddColumn(TRItemHead72, TRItemHead72_00, 10, 10, "l", "<b><i></i></b>")
                        RH.AddColumn(TRItemHead72, TRItemHead72_01, 50, 50, "l", "<b><i></i></b>")
                        RH.AddColumn(TRItemHead72, TRItemHead72_02, 10, 10, "l", "")
                        RH.AddColumn(TRItemHead72, TRItemHead72_03, 10, 10, "l", "<b><i>Total Cheque No  :-     " & I.ToString() & " </i></b>")
                        RH.AddColumn(TRItemHead72, TRItemHead72_04, 20, 20, "r", "<b><i>Total Amount  :-     " & Math.Round(TotAmount, 2).ToString() & " </i></b>")

                        tb.Controls.Add(TRItemHead72)
                        RH.BlankRow(tb, 10)


                        Dim TRItemHead78 As New TableRow
                        TRItemHead78.ForeColor = Drawing.Color.DarkBlue
                        Dim TRItemHead78_00, TRItemHead78_01, TRItemHead78_02, TRItemHead78_03, TRItemHead78_04, TRItemHead78_05 As New TableCell

                        RH.AddColumn(TRItemHead78, TRItemHead78_00, 10, 10, "l", "<b><i></i></b>")
                        RH.AddColumn(TRItemHead78, TRItemHead78_01, 50, 50, "l", "<b><i></i></b>")
                        RH.AddColumn(TRItemHead78, TRItemHead78_02, 10, 10, "l", "")
                        RH.AddColumn(TRItemHead78, TRItemHead78_03, 10, 10, "l", "<b><i>Total Rejected Amount  :-     " & Math.Round(InactiveAMt, 2).ToString() & "</i></b>")
                        RH.AddColumn(TRItemHead78, TRItemHead78_04, 20, 20, "r", "<b><i>Total Processed Amount  :-     " & Math.Round(ActiveAmt, 2).ToString() & " </i></b>")

                        tb.Controls.Add(TRItemHead78)
                        RH.BlankRow(tb, 10)
                        RH.DrawLine(tb, 100)
                        
                    End If
                    Dim TRItemHead2 As New TableRow
                    TRItemHead2.ForeColor = Drawing.Color.DarkBlue
                    Dim TRItemHead2_00, TRItemHead2_01, TRItemHead2_02, TRItemHead2_03, TRItemHead2_04, TRItemHead2_05 As New TableCell

                    RH.AddColumn(TRItemHead2, TRItemHead2_00, 10, 10, "l", "<b><i>BRANCH NAME  :- </i></b>")
                    RH.AddColumn(TRItemHead2, TRItemHead2_01, 50, 50, "l", "<b><i> " & DR(8).ToString & " </i></b>")
                    RH.AddColumn(TRItemHead2, TRItemHead2_02, 10, 10, "l", "")
                    RH.AddColumn(TRItemHead2, TRItemHead2_03, 10, 10, "l", "<b><i> </i></b>")
                    RH.AddColumn(TRItemHead2, TRItemHead2_04, 20, 20, "r", "<b><i> </i></b>")

                    tb.Controls.Add(TRItemHead2)



                    Dim TRHead As New TableRow
                    TRHead.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07 As New TableCell

                    TRHead_00.BorderWidth = "1"
                    TRHead_01.BorderWidth = "1"
                    TRHead_02.BorderWidth = "1"
                    TRHead_03.BorderWidth = "1"
                    TRHead_04.BorderWidth = "1"
                    TRHead_05.BorderWidth = "1"
                    TRHead_06.BorderWidth = "1"
                    TRHead_07.BorderWidth = "1"

                    TRHead_00.BorderColor = Drawing.Color.Silver
                    TRHead_01.BorderColor = Drawing.Color.Silver
                    TRHead_02.BorderColor = Drawing.Color.Silver
                    TRHead_03.BorderColor = Drawing.Color.Silver
                    TRHead_04.BorderColor = Drawing.Color.Silver
                    TRHead_05.BorderColor = Drawing.Color.Silver
                    TRHead_06.BorderColor = Drawing.Color.Silver
                    TRHead_07.BorderColor = Drawing.Color.Silver


                    TRHead_00.BorderStyle = BorderStyle.Solid
                    TRHead_01.BorderStyle = BorderStyle.Solid
                    TRHead_02.BorderStyle = BorderStyle.Solid
                    TRHead_03.BorderStyle = BorderStyle.Solid
                    TRHead_04.BorderStyle = BorderStyle.Solid
                    TRHead_05.BorderStyle = BorderStyle.Solid
                    TRHead_06.BorderStyle = BorderStyle.Solid
                    TRHead_07.BorderStyle = BorderStyle.Solid



                    RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "Sl No.")
                    RH.AddColumn(TRHead, TRHead_01, 25, 25, "l", "A/C Number")
                    RH.AddColumn(TRHead, TRHead_02, 20, 20, "l", "A/C Name")
                    RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Cheque No")
                    RH.AddColumn(TRHead, TRHead_05, 10, 10, "l", "Time")
                    RH.AddColumn(TRHead, TRHead_04, 10, 10, "l", "Amount")
                    RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "HO-Status")
                    RH.AddColumn(TRHead, TRHead_07, 10, 10, "l", "Remarks")
                    tb.Controls.Add(TRHead)
                    I = 1
                    TotAmount = CDbl(DR(5))
                    InactiveAMt = 0
                    ActiveAmt = 0
                    If DR(9) = 2 Then
                        InactiveAMt = CDbl(DR(5))
                    Else
                        ActiveAmt = CDbl(DR(5))
                    End If
                End If

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid







                GrandTotal += CDbl(DR(5))
                If DR(9) = 2 Then
                    Grand_InactiveAMt += CDbl(DR(5))
                Else
                    Grand_ActiveAmt += CDbl(DR(5))
                End If
                Dim Val As String
                RH.AddColumn(TR3, TR3_00, 5, 5, "l", j.ToString())
                RH.AddColumn(TR3, TR3_01, 25, 25, "l", DR(2).ToString)
                Val = DR(3).ToString
                RH.AddColumn(TR3, TR3_02, 20, 20, "l", Val)

                Val = DR(4).ToString
                RH.AddColumn(TR3, TR3_03, 10, 10, "l", Val)

                Val = DR(6).ToString
                RH.AddColumn(TR3, TR3_05, 10, 10, "l", Val)

                Val = DR(5).ToString
                RH.AddColumn(TR3, TR3_04, 10, 10, "r", Val)

                Val = DR(10).ToString
                RH.AddColumn(TR3, TR3_06, 10, 10, "l", Val)

                Val = DR(12).ToString
                RH.AddColumn(TR3, TR3_07, 10, 10, "l", Val)


                tb.Controls.Add(TR3)

                prevBranchid = DR(1)
                prevBranchName = DR(8).ToString
            Next

            Dim TRItemHead22 As New TableRow
            TRItemHead22.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead22_00, TRItemHead22_01, TRItemHead22_02, TRItemHead22_03, TRItemHead22_04, TRItemHead22_05 As New TableCell

            RH.AddColumn(TRItemHead22, TRItemHead22_00, 10, 10, "l", "<b><i></i></b>")
            RH.AddColumn(TRItemHead22, TRItemHead22_01, 50, 50, "l", "<b><i> </i></b>")
            RH.AddColumn(TRItemHead22, TRItemHead22_02, 10, 10, "l", "")
            RH.AddColumn(TRItemHead22, TRItemHead22_03, 10, 10, "l", "<b><i>Total Cheque No  :-     " & I.ToString() & " </i></b>")
            RH.AddColumn(TRItemHead22, TRItemHead22_04, 20, 20, "r", "<b><i>Total Amount  :-     " & Math.Round(TotAmount, 2).ToString() & " </i></b>")

            tb.Controls.Add(TRItemHead22)
            RH.BlankRow(tb, 10)

            Dim TRItemHead21 As New TableRow
            TRItemHead21.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead21_00, TRItemHead21_01, TRItemHead21_02, TRItemHead21_03, TRItemHead21_04, TRItemHead21_05 As New TableCell

            RH.AddColumn(TRItemHead21, TRItemHead21_00, 10, 10, "l", "<b><i></i></b>")
            RH.AddColumn(TRItemHead21, TRItemHead21_01, 50, 50, "l", "<b><i> </i></b>")
            RH.AddColumn(TRItemHead21, TRItemHead21_02, 10, 10, "l", "")
            RH.AddColumn(TRItemHead21, TRItemHead21_03, 10, 10, "l", "<b><i>Total Rejected Amount  :-     " & Math.Round(InactiveAMt, 2).ToString() & " </i></b>")
            RH.AddColumn(TRItemHead21, TRItemHead21_04, 20, 20, "r", "<b><i>Total Processed Amount  :-     " & Math.Round(ActiveAmt, 2).ToString() & " </i></b>")

            tb.Controls.Add(TRItemHead21)
            RH.BlankRow(tb, 10)


            Dim TRTot As New TableRow
            TRTot.BorderWidth = "1"
            TRTot.BorderStyle = BorderStyle.Solid
            TRTot.Style.Add("font-weight", "bold")
            TRTot.Style.Add("background-color", "silver")
            Dim TRTot_00, TRTot_01, TRTot_02, TRTot_03, TRTot_04 As New TableCell

            TRTot_00.BorderWidth = "1"
            TRTot_01.BorderWidth = "1"
            TRTot_02.BorderWidth = "1"
            TRTot_03.BorderWidth = "1"
            TRTot_04.BorderWidth = "1"

            TRTot_00.BorderColor = Drawing.Color.Silver
            TRTot_01.BorderColor = Drawing.Color.Silver
            TRTot_02.BorderColor = Drawing.Color.Silver
            TRTot_03.BorderColor = Drawing.Color.Silver
            TRTot_04.BorderColor = Drawing.Color.Silver

            TRTot_00.BorderStyle = BorderStyle.Solid
            TRTot_01.BorderStyle = BorderStyle.Solid
            TRTot_02.BorderStyle = BorderStyle.Solid
            TRTot_03.BorderStyle = BorderStyle.Solid
            TRTot_04.BorderStyle = BorderStyle.Solid

            Dim Val1 As String
            RH.AddColumn(TRTot, TRTot_00, 50, 50, "l", "")
            RH.AddColumn(TRTot, TRTot_01, 15, 15, "l", "Total cheques  :")
            Val1 = j.ToString
            RH.AddColumn(TRTot, TRTot_02, 10, 10, "l", Val1)
            RH.AddColumn(TRTot, TRTot_03, 15, 15, "l", "Total Amount  :")
            Val1 = Math.Round(GrandTotal, 2).ToString()
            RH.AddColumn(TRTot, TRTot_04, 10, 10, "r", GrandTotal)

           


            tb.Controls.Add(TRTot)
            RH.BlankRow(tb, 5)


            Dim TRtot1 As New TableRow
            TRtot1.BorderWidth = "1"
            TRtot1.BorderStyle = BorderStyle.Solid
            TRtot1.Style.Add("font-weight", "bold")
            TRtot1.Style.Add("background-color", "silver")
            Dim TRtot1_00, TRtot1_01, TRtot1_02, TRtot1_03, TRtot1_04 As New TableCell

            TRtot1_00.BorderWidth = "1"
            TRtot1_01.BorderWidth = "1"
            TRtot1_02.BorderWidth = "1"
            TRtot1_03.BorderWidth = "1"
            TRtot1_04.BorderWidth = "1"

            TRtot1_00.BorderColor = Drawing.Color.Silver
            TRtot1_01.BorderColor = Drawing.Color.Silver
            TRtot1_02.BorderColor = Drawing.Color.Silver
            TRtot1_03.BorderColor = Drawing.Color.Silver
            TRtot1_04.BorderColor = Drawing.Color.Silver

            TRtot1_00.BorderStyle = BorderStyle.Solid
            TRtot1_01.BorderStyle = BorderStyle.Solid
            TRtot1_02.BorderStyle = BorderStyle.Solid
            TRtot1_03.BorderStyle = BorderStyle.Solid
            TRtot1_04.BorderStyle = BorderStyle.Solid

            Dim Val11 As String
            RH.AddColumn(TRtot1, TRtot1_00, 50, 50, "l", "")
            RH.AddColumn(TRtot1, TRtot1_01, 15, 15, "l", "Total Rejected Amount  :")
            Val11 = Math.Round(Grand_InactiveAMt, 2).ToString()
            RH.AddColumn(TRtot1, TRtot1_02, 10, 10, "l", Val11)
            RH.AddColumn(TRtot1, TRtot1_03, 15, 15, "l", "Total Processed Amount  :")
            Val11 = Math.Round(Grand_ActiveAmt, 2).ToString()
            RH.AddColumn(TRtot1, TRtot1_04, 10, 10, "r", Val11)




            tb.Controls.Add(TRtot1)
            RH.BlankRow(tb, 5)
            'Dim workRow As DataRow = DTExcel.NewRow()
            'workRow(0) = ""
            'workRow(1) = "GRAND TOTAL"

            'workRow(6) = GrandTotal
            'DTExcel.Rows.Add(workRow)

            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub


    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String

            HeaderText = "Branchwise-CTS Data Report"

            WebTools.ExporttoExcel(DTExcel, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
