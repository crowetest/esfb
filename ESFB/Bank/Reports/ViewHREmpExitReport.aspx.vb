﻿Imports System.Data
Imports System.IO

Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewHREmpExitReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DTExcel As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DateFrom As Date
            Dim DateTo As Date
            If Request.QueryString.Get("FROM") <> "" Then
                DateFrom = CDate(Request.QueryString.Get("FROM"))

            End If
            If Request.QueryString.Get("TO") <> "" Then
                DateTo = CDate(Request.QueryString.Get("TO"))

            End If
            Dim TypeID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("TypeID")))

            Dim DTHead As New DataTable
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable


            If DateFrom <> "#12:00:00 AM#" Then
                RH.Heading(Session("FirmName"), tb, "HR -EMPLOYEE EXIT REPORT (" + Format(DateFrom, "dd-MMM-yyyy") + " - " + Format(DateTo, "dd-MMM-yyyy") + ")", 100)
            Else
                RH.Heading(Session("FirmName"), tb, "HR -EMPLOYEE EXIT REPORT", 100)
            End If

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")





            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            '-----------------


            Dim strval As String



            Dim prevBranchid As Integer = 0
            Dim prevBranchName As String
            If TypeID = -1 Then 'ALL
                strval = " "

                If DateFrom <> "#12:00:00 AM#" Then
                    strval += " and convert(date,submited_date) between '" + DateFrom.ToString("dd/MMM/yyyy") + "' and '" + DateTo.ToString("dd/MMM/yyyy") + "' "
                End If


            ElseIf TypeID = 1 Then 'Created On
                strval = " and convert(date,submited_date) between '" + DateFrom.ToString("dd/MMM/yyyy") + "' and '" + DateTo.ToString("dd/MMM/yyyy") + "' "
            ElseIf TypeID = 2 Then 'Approved On
                strval = " and convert(date,hr_approved_on) between '" + DateFrom.ToString("dd/MMM/yyyy") + "' and '" + DateTo.ToString("dd/MMM/yyyy") + "' "
            ElseIf TypeID = 3 Then 'Relieved On
                strval = " and convert(date,relieving_date) between '" + DateFrom.ToString("dd/MMM/yyyy") + "' and '" + DateTo.ToString("dd/MMM/yyyy") + "' "
            ElseIf TypeID = 4 Then 'Approval Pending
                strval = " and  a.status_id =0 and convert(date,submited_date) between '" + DateFrom.ToString("dd/MMM/yyyy") + "' and '" + DateTo.ToString("dd/MMM/yyyy") + "' "
            End If

            DTExcel = DB.ExecuteDataSet("select convert(varchar,ROW_NUMBER() OVER(ORDER BY resignation_id asc)) AS 'Sl No', upper(b.emp_name)+'('+ convert(varchar,a.emp_code) +')'  as employee " & _
                        " ,e.status as Status,convert(varchar,convert(date,relieving_date),104) as Releived_date,reason,convert(varchar,convert(date,submited_date),104) as created_on, " & _
                        " c.emp_name +'('+convert(varchar,a.User_ID) +')'as created_by,convert(varchar,convert(date,hr_approved_on),104) as approved_on, " & _
                        " d.emp_name +'('+convert(varchar,a.hr_approved_by) +')' as Approved_by,case when a.status_id=1 then 'Approved' when a.status_id=2 then 'Rejected' else 'Approval Pending' end, a.emp_code, a.status_id" & _
                        " from hr_emp_exit a left join emp_master c on  a.user_id=c.emp_code left join emp_master d on  a.hr_approved_by=d.emp_code ,  " & _
                        " emp_master b ,emp_status e where a.exit_type_id=e.status_id and  a.emp_code=b.emp_code and a.status_id<>10 " & _
                        " " & strval.ToString & "   order by resignation_id").Tables(0)
            'RH.BlankRow(tb, 3)
            DT = DB.ExecuteDataSet("select convert(varchar,ROW_NUMBER() OVER(ORDER BY resignation_id asc)) AS 'Sl No', upper(b.emp_name)+'('+ convert(varchar,a.emp_code) +')'  as employee " & _
                        " ,e.status as Status,convert(varchar,convert(date,relieving_date),104) as Releived_date,reason,convert(varchar,convert(date,submited_date),104) as created_on, " & _
                        " c.emp_name +'('+convert(varchar,a.User_ID) +')'as created_by,convert(varchar,convert(date,hr_approved_on),104) as approved_on, " & _
                        " d.emp_name +'('+convert(varchar,a.hr_approved_by) +')' as Approved_by,case when a.status_id=1 then 'Approved' when a.status_id=2 then 'Rejected' else 'Approval Pending' end, a.emp_code, a.status_id" & _
                        " from hr_emp_exit a left join emp_master c on  a.user_id=c.emp_code left join emp_master d on  a.hr_approved_by=d.emp_code ,  " & _
                        " emp_master b ,emp_status e where a.exit_type_id=e.status_id and  a.emp_code=b.emp_code  and a.status_id<>10 " & _
                        " " & strval.ToString & "    order by resignation_id").Tables(0)

            Dim Chkcnt As Integer = 0
            Dim TotAmount As Double = 0
            Dim GrandTotal As Double = 0
            j = 0




            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver


            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 15, 15, "l", "Staff")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "l", "Exit Type")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Releiving date")
            RH.AddColumn(TRHead, TRHead_05, 10, 10, "l", "Reason")
            RH.AddColumn(TRHead, TRHead_04, 10, 10, "l", "Created On")
            RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "created By")
            RH.AddColumn(TRHead, TRHead_07, 10, 10, "l", "Approved On")
            RH.AddColumn(TRHead, TRHead_08, 10, 10, "l", "Approved By")
            RH.AddColumn(TRHead, TRHead_09, 10, 10, "l", "Status")
            tb.Controls.Add(TRHead)


            For Each DR In DT.Rows


                j += 1

                Dim TRval As New TableRow
                TRval.BackColor = Drawing.Color.WhiteSmoke
                Dim TRval_00, TRval_01, TRval_02, TRval_03, TRval_04, TRval_05, TRval_06, TRval_07, TRval_08, TRval_09 As New TableCell

                TRval_00.BorderWidth = "1"
                TRval_01.BorderWidth = "1"
                TRval_02.BorderWidth = "1"
                TRval_03.BorderWidth = "1"
                TRval_04.BorderWidth = "1"
                TRval_05.BorderWidth = "1"
                TRval_06.BorderWidth = "1"
                TRval_07.BorderWidth = "1"
                TRval_08.BorderWidth = "1"
                TRval_09.BorderWidth = "1"

                TRval_00.BorderColor = Drawing.Color.Silver
                TRval_01.BorderColor = Drawing.Color.Silver
                TRval_02.BorderColor = Drawing.Color.Silver
                TRval_03.BorderColor = Drawing.Color.Silver
                TRval_04.BorderColor = Drawing.Color.Silver
                TRval_05.BorderColor = Drawing.Color.Silver
                TRval_06.BorderColor = Drawing.Color.Silver
                TRval_07.BorderColor = Drawing.Color.Silver
                TRval_08.BorderColor = Drawing.Color.Silver
                TRval_09.BorderColor = Drawing.Color.Silver


                TRval_00.BorderStyle = BorderStyle.Solid
                TRval_01.BorderStyle = BorderStyle.Solid
                TRval_02.BorderStyle = BorderStyle.Solid
                TRval_03.BorderStyle = BorderStyle.Solid
                TRval_04.BorderStyle = BorderStyle.Solid
                TRval_05.BorderStyle = BorderStyle.Solid
                TRval_06.BorderStyle = BorderStyle.Solid
                TRval_07.BorderStyle = BorderStyle.Solid
                TRval_08.BorderStyle = BorderStyle.Solid
                TRval_09.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TRval, TRval_00, 5, 5, "c", j.ToString())
                If (CInt(DR(11).ToString()) <> 0) Then
                    RH.AddColumn(TRval, TRval_01, 15, 15, "l", "<a href='ViewHREmpExitSubReport.aspx?empName=" + GF.Encrypt(DR(1).ToString()) + "&empCode=" + GF.Encrypt(DR(10).ToString()) + "&creBy=" + GF.Encrypt(DR(6).ToString()) + "&appBy=" + GF.Encrypt(DR(8).ToString()) + "' target='_blank'>" + DR(1).ToString + "</a>")
                Else
                    RH.AddColumn(TRval, TRval_01, 15, 15, "l", DR(1).ToString)
                End If
                RH.AddColumn(TRval, TRval_02, 10, 10, "l", DR(2).ToString)
                RH.AddColumn(TRval, TRval_03, 10, 10, "l", DR(3).ToString)
                RH.AddColumn(TRval, TRval_05, 10, 10, "l", DR(4).ToString)
                RH.AddColumn(TRval, TRval_04, 10, 10, "l", DR(5).ToString)
                RH.AddColumn(TRval, TRval_06, 10, 10, "l", DR(6).ToString)
                RH.AddColumn(TRval, TRval_07, 10, 10, "l", DR(7).ToString)
                RH.AddColumn(TRval, TRval_08, 10, 10, "l", DR(8).ToString)
                RH.AddColumn(TRval, TRval_09, 10, 10, "l", DR(9).ToString)
                tb.Controls.Add(TRval)

            Next
            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub


    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String

            HeaderText = "Branchwise-CTS Data Report"

            WebTools.ExporttoExcel(DTExcel, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
