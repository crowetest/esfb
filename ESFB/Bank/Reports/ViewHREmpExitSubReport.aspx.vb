﻿Imports System.Data
Imports System.IO

Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewHREmpExitSubReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DTExcel As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            
            Dim EmpName As String = CStr(GF.Decrypt(Request.QueryString.Get("empName")))
            Dim EmpCode As Integer = CInt(GF.Decrypt(Request.QueryString.Get("empCode")))
            Dim creBy As String = CStr(GF.Decrypt(Request.QueryString.Get("creBy")))
            Dim appBy As String = CStr(GF.Decrypt(Request.QueryString.Get("appBy")))

            Dim DTHead As New DataTable
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable

            RH.Heading(Session("FirmName"), tb, "APPLICATION DISABLE STATUS OF " + EmpName, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            '-----------------

            Dim strval As String


            DT = DB.ExecuteDataSet("select convert(varchar,ROW_NUMBER() OVER(ORDER BY Request_No asc)) AS 'Sl No',c.App_name,d.App_Type_name,a.Request_No,case when a.closed_status=1 then 'Closed' when a.closed_status=2 then 'Rejected' else '' end Status, " & _
                                   "isnull(convert(varchar,a.closed_remarks),'') Remark,isnull(convert(varchar,a.closed_by),'') ClosedBy,isnull(convert(varchar,convert(date,a.closed_on)),'') ClosedOn " & _
                                   "from App_Request_master a, HR_Emp_Exit b, App_Master c, App_Type_master d " & _
                                   "where a.emp_code =  " & EmpCode & " and a.hr_status=2 and a.emp_code=b.emp_code and a.App_id=c.App_id and a.App_type_id=d.App_type_id order by 4").Tables(0)

            Dim Chkcnt As Integer = 0
            Dim TotAmount As Double = 0
            Dim GrandTotal As Double = 0
            j = 0


            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09 As New TableCell

            TRHead_00.BorderWidth = "2"
            TRHead_01.BorderWidth = "2"
            TRHead_02.BorderWidth = "2"
            TRHead_03.BorderWidth = "2"
            TRHead_04.BorderWidth = "2"
            TRHead_05.BorderWidth = "2"
            TRHead_06.BorderWidth = "2"
            TRHead_07.BorderWidth = "2"
            TRHead_08.BorderWidth = "2"
            TRHead_09.BorderWidth = "2"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 10, 10, "l", "Application Name")
            RH.AddColumn(TRHead, TRHead_02, 15, 15, "l", "Application Type")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "c", "Request No")
            RH.AddColumn(TRHead, TRHead_04, 10, 10, "l", "Status")
            RH.AddColumn(TRHead, TRHead_05, 10, 10, "l", "Remark")
            RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "Created By")
            RH.AddColumn(TRHead, TRHead_07, 10, 10, "l", "Approved By")
            RH.AddColumn(TRHead, TRHead_08, 10, 10, "c", "Closed By")
            RH.AddColumn(TRHead, TRHead_09, 10, 10, "c", "Closed On")
            tb.Controls.Add(TRHead)

            For Each DR In DT.Rows

                j += 1

                Dim TRval As New TableRow
                TRval.BackColor = Drawing.Color.WhiteSmoke
                Dim TRval_00, TRval_01, TRval_02, TRval_03, TRval_04, TRval_05, TRval_06, TRval_07, TRval_08, TRval_09 As New TableCell

                TRval_00.BorderWidth = "2"
                TRval_01.BorderWidth = "2"
                TRval_02.BorderWidth = "2"
                TRval_03.BorderWidth = "2"
                TRval_04.BorderWidth = "2"
                TRval_05.BorderWidth = "2"
                TRval_06.BorderWidth = "2"
                TRval_07.BorderWidth = "2"
                TRval_08.BorderWidth = "2"
                TRval_09.BorderWidth = "2"

                TRval_00.BorderColor = Drawing.Color.Silver
                TRval_01.BorderColor = Drawing.Color.Silver
                TRval_02.BorderColor = Drawing.Color.Silver
                TRval_03.BorderColor = Drawing.Color.Silver
                TRval_04.BorderColor = Drawing.Color.Silver
                TRval_05.BorderColor = Drawing.Color.Silver
                TRval_06.BorderColor = Drawing.Color.Silver
                TRval_07.BorderColor = Drawing.Color.Silver
                TRval_08.BorderColor = Drawing.Color.Silver
                TRval_09.BorderColor = Drawing.Color.Silver

                TRval_00.BorderStyle = BorderStyle.Solid
                TRval_01.BorderStyle = BorderStyle.Solid
                TRval_02.BorderStyle = BorderStyle.Solid
                TRval_03.BorderStyle = BorderStyle.Solid
                TRval_04.BorderStyle = BorderStyle.Solid
                TRval_05.BorderStyle = BorderStyle.Solid
                TRval_06.BorderStyle = BorderStyle.Solid
                TRval_07.BorderStyle = BorderStyle.Solid
                TRval_08.BorderStyle = BorderStyle.Solid
                TRval_09.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TRval, TRval_00, 5, 5, "c", DR(0).ToString)
                RH.AddColumn(TRval, TRval_01, 10, 10, "l", DR(1).ToString)
                RH.AddColumn(TRval, TRval_02, 15, 15, "l", DR(2).ToString)
                RH.AddColumn(TRval, TRval_03, 10, 10, "c", DR(3).ToString)
                RH.AddColumn(TRval, TRval_05, 10, 10, "l", DR(4).ToString)
                RH.AddColumn(TRval, TRval_04, 10, 10, "l", DR(5).ToString)
                RH.AddColumn(TRval, TRval_06, 10, 10, "l", creBy)
                RH.AddColumn(TRval, TRval_07, 10, 10, "l", appBy)
                RH.AddColumn(TRval, TRval_08, 10, 10, "c", DR(6).ToString)
                RH.AddColumn(TRval, TRval_09, 10, 10, "c", DR(7).ToString)
        
                tb.Controls.Add(TRval)
            Next
            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub


    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String

            HeaderText = "Disable Status Report"

            WebTools.ExporttoExcel(DTExcel, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
