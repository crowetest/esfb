﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewStaffRequestClose_dtl
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1169) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim DTT As New DataTable


            Dim Struser As String = Session("UserID").ToString()
            Dim EmpCode As String
            EmpCode = CStr(GF.Decrypt(Request.QueryString.Get("EmpCode")))
            Dim SearchWise As Integer = CInt(Request.QueryString.Get("SearchWise"))

            Dim StrFromDate As String = ""
            If CStr(Request.QueryString.Get("From")) <> "1" Then
                StrFromDate = CStr(Request.QueryString.Get("From"))
            End If
            Dim StrToDate As String = ""
            If CStr(Request.QueryString.Get("To")) <> "1" Then
                StrToDate = CStr(Request.QueryString.Get("To"))
            End If

            Dim StrSubQry As String = ""
            Dim StrSubQry1 As String = ""
            Dim StrQuery As String = ""

            Dim strTeamQry As String = Nothing



            Dim SqlStr As String
            Dim StrTemp As String = ""

            If EmpCode <> "" Then
                If SearchWise = 1 Then
                    StrTemp = " and c.emp_code= " & EmpCode.ToString
                ElseIf SearchWise = 2 Then
                    StrTemp = " and c.closed_by= " & EmpCode.ToString
                End If
            End If

            If StrFromDate <> "" And StrToDate <> "" Then
                StrTemp += " and convert(date,c.closed_on) between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "' and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
            End If
            'StrFromDate
            SqlStr = "select upper(c.request_no) as REQUEST_NO,c.created_on  as REQUEST_DATE,c.approved_on  as Rep_DATE,c.ho_approved_on  as HO_DATE,upper(c.created_branch_name) as  BRANCH,d.emp_code as EMP_CODE, upper(c.Emp_Name) as EMP_NAME,upper(j.app_name) as APPLICATION,upper(e.App_Type_name) as TYPE,upper(g.App_role_name) as ROLE ,datediff(day,c.ho_approved_on,c.closed_on) as day_cnt, " & _
                        " case when c.closed_status=1 then 'Closed'  when c.closed_status=2 then 'Rejected' else '' end ,upper(closed_Remarks) as REMARKS,c.closed_by,c.closed_on,c.App_Request_ID,c.Approved_By,case when c.app_type_id in(1,2,3,16,27,47) then 1 else 0 end CheckerFlag,case  when d.Role_id in(44,45) then 1 when d.Role_id=1 and c.teller_id=1 then 1 else 0 end RoleFlag, case when c.closed_status is null then 0 else c.closed_status end SubFlag,c.maker_1,c.maker_1_dt,c.checker_1,c.checker_1_dt,c.maker_2,c.maker_2_dt,c.checker_2,c.checker_2_dt    from ESFB.dbo.app_level k  left join ESFB.dbo.app_level_email x on k.order_id=x.emp_code" & _
                        " ,ESFB.dbo.app_request_master c left join emp_master m on c.maker_1 = m.emp_code left join emp_master n on c.checker_1 = n.emp_code left join emp_master o on c.maker_2 = o.emp_code left join emp_master p on c.checker_2 = p.emp_code ,ESFB.dbo.app_master j,  " & _
                        " ESFB.dbo.app_type_master e ,     ESFB.dbo.app_dtl_profile d  " & _
                        " left join ESFB.dbo.app_role_master  " & _
                        " g on d.Role_id=g.App_role   where    k.app_id=j.App_id and c.App_type_id=e.App_type_id  " & _
                        " and c.App_request_id=d.App_request_id and    c.level_id=k.level_id  and c.closed_status in (1,2) " & StrTemp.ToString & "  order by c.closed_on"
            'SqlStr += "   order by a.REQUEST_ID"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim b As Integer = DT.Rows.Count


            RH.Heading(Session("FirmName"), tb, "CLOSED REPORT ", 150)

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            '
            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            Dim StrUserNam As String = Nothing

            DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
            If DTT.Rows.Count > 0 Then
                StrUserNam = DTT.Rows(0)(0)
            End If
            Dim TRHead_1 As New TableRow

            RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)


            RH.BlankRow(tb, 4)
            tb.Controls.Add(TRSHead)


            'TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_1_000, TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14, TRHead_1_15, TRHead_1_16, TRHead_1_17, TRHead_1_18, TRHead_1_19, TRHead_1_20, TRHead_1_21, TRHead_1_22 As New TableCell
            TRHead_1_000.BorderWidth = "1"
            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"
            TRHead_1_08.BorderWidth = "1"
            TRHead_1_09.BorderWidth = "1"
            TRHead_1_10.BorderWidth = "1"
            TRHead_1_11.BorderWidth = "1"
            TRHead_1_12.BorderWidth = "1"
            TRHead_1_13.BorderWidth = "1"
            TRHead_1_14.BorderWidth = "1"
            TRHead_1_15.BorderWidth = "1"
            TRHead_1_16.BorderWidth = "1"
            TRHead_1_17.BorderWidth = "1"
            TRHead_1_18.BorderWidth = "1"
            TRHead_1_19.BorderWidth = "1"
            TRHead_1_20.BorderWidth = "1"
            TRHead_1_21.BorderWidth = "1"
            TRHead_1_22.BorderWidth = "1"

            TRHead_1_000.BorderColor = Drawing.Color.Silver
            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver
            TRHead_1_08.BorderColor = Drawing.Color.Silver
            TRHead_1_09.BorderColor = Drawing.Color.Silver
            TRHead_1_10.BorderColor = Drawing.Color.Silver
            TRHead_1_11.BorderColor = Drawing.Color.Silver
            TRHead_1_12.BorderColor = Drawing.Color.Silver
            TRHead_1_13.BorderColor = Drawing.Color.Silver
            TRHead_1_14.BorderStyle = BorderStyle.Solid
            TRHead_1_15.BorderStyle = BorderStyle.Solid
            TRHead_1_16.BorderStyle = BorderStyle.Solid
            TRHead_1_17.BorderStyle = BorderStyle.Solid
            TRHead_1_18.BorderStyle = BorderStyle.Solid
            TRHead_1_19.BorderStyle = BorderStyle.Solid
            TRHead_1_20.BorderStyle = BorderStyle.Solid
            TRHead_1_21.BorderStyle = BorderStyle.Solid
            TRHead_1_22.BorderStyle = BorderStyle.Solid


            TRHead_1_000.BorderStyle = BorderStyle.Solid
            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid
            TRHead_1_08.BorderStyle = BorderStyle.Solid
            TRHead_1_09.BorderStyle = BorderStyle.Solid
            TRHead_1_10.BorderStyle = BorderStyle.Solid
            TRHead_1_11.BorderStyle = BorderStyle.Solid
            TRHead_1_12.BorderStyle = BorderStyle.Solid
            TRHead_1_13.BorderStyle = BorderStyle.Solid
            TRHead_1_14.BorderStyle = BorderStyle.Solid
            TRHead_1_15.BorderStyle = BorderStyle.Solid
            TRHead_1_16.BorderStyle = BorderStyle.Solid
            TRHead_1_17.BorderStyle = BorderStyle.Solid
            TRHead_1_18.BorderStyle = BorderStyle.Solid
            TRHead_1_19.BorderStyle = BorderStyle.Solid
            TRHead_1_20.BorderStyle = BorderStyle.Solid
            TRHead_1_21.BorderStyle = BorderStyle.Solid
            TRHead_1_22.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead_1, TRHead_1_000, 2, 2, "l", "Sl No") '
            RH.AddColumn(TRHead_1, TRHead_1_00, 4, 4, "l", "Request No") '
            RH.AddColumn(TRHead_1, TRHead_1_01, 3, 3, "l", "Request Date")
            RH.AddColumn(TRHead_1, TRHead_1_13, 3, 3, "l", "Rep_Approved Date")
            RH.AddColumn(TRHead_1, TRHead_1_14, 3, 3, "l", "HO_Approved_Date")
            RH.AddColumn(TRHead_1, TRHead_1_02, 4, 4, "l", "Branch_name")
            RH.AddColumn(TRHead_1, TRHead_1_03, 3, 3, "c", "Emp_code")
            RH.AddColumn(TRHead_1, TRHead_1_04, 7, 7, "l", "Emp Name")
            RH.AddColumn(TRHead_1, TRHead_1_05, 7, 7, "l", "Category")

            RH.AddColumn(TRHead_1, TRHead_1_06, 5, 5, "l", "Request Type")
            RH.AddColumn(TRHead_1, TRHead_1_07, 5, 5, "l", "Role")
            RH.AddColumn(TRHead_1, TRHead_1_08, 4, 4, "l", "Day Count")

            RH.AddColumn(TRHead_1, TRHead_1_09, 5, 5, "l", "Status")
            RH.AddColumn(TRHead_1, TRHead_1_10, 7, 7, "l", "Remarks")
            RH.AddColumn(TRHead_1, TRHead_1_11, 4, 4, "l", "In Progress By")
            RH.AddColumn(TRHead_1, TRHead_1_12, 4, 4, "l", "Approved By")
            RH.AddColumn(TRHead_1, TRHead_1_13, 3, 3, "l", "Closed By")
            RH.AddColumn(TRHead_1, TRHead_1_14, 3, 3, "l", "Closed On")


            RH.AddColumn(TRHead_1, TRHead_1_15, 3, 3, "l", "Maker 1")
            RH.AddColumn(TRHead_1, TRHead_1_16, 3, 3, "l", "Maker 1 Date")
            RH.AddColumn(TRHead_1, TRHead_1_17, 3, 3, "l", "Checker 1")
            RH.AddColumn(TRHead_1, TRHead_1_18, 3, 3, "l", "Checker 1 Date")
            RH.AddColumn(TRHead_1, TRHead_1_19, 3, 3, "l", "Maker 2")
            RH.AddColumn(TRHead_1, TRHead_1_20, 3, 3, "l", "Maker 2 Date")
            RH.AddColumn(TRHead_1, TRHead_1_21, 3, 3, "l", "Checker 2")
            RH.AddColumn(TRHead_1, TRHead_1_22, 3, 3, "l", "Checker 2 Date")

            tb.Controls.Add(TRHead_1)
            Dim i As Integer = 0
            For Each DR In DT.Rows
                i = i + 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3_000, TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16, TR3_17, TR3_18, TR3_19, TR3_20, TR3_21, TR3_22 As New TableCell

                TR3_000.BorderWidth = "1"
                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"
                TR3_15.BorderWidth = "1"
                TR3_16.BorderWidth = "1"
                TR3_17.BorderWidth = "1"
                TR3_18.BorderWidth = "1"
                TR3_19.BorderWidth = "1"
                TR3_20.BorderWidth = "1"
                TR3_21.BorderWidth = "1"
                TR3_22.BorderWidth = "1"


                TR3_000.BorderColor = Drawing.Color.Silver
                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver
                TR3_15.BorderColor = Drawing.Color.Silver
                TR3_16.BorderColor = Drawing.Color.Silver
                TR3_17.BorderColor = Drawing.Color.Silver
                TR3_18.BorderColor = Drawing.Color.Silver
                TR3_19.BorderColor = Drawing.Color.Silver
                TR3_20.BorderColor = Drawing.Color.Silver
                TR3_21.BorderColor = Drawing.Color.Silver
                TR3_22.BorderColor = Drawing.Color.Silver


                TR3_000.BorderStyle = BorderStyle.Solid
                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid
                TR3_15.BorderStyle = BorderStyle.Solid
                TR3_16.BorderStyle = BorderStyle.Solid
                TR3_17.BorderStyle = BorderStyle.Solid
                TR3_18.BorderStyle = BorderStyle.Solid
                TR3_19.BorderStyle = BorderStyle.Solid
                TR3_20.BorderStyle = BorderStyle.Solid
                TR3_21.BorderStyle = BorderStyle.Solid
                TR3_22.BorderStyle = BorderStyle.Solid



                'RH.AddColumn(TR3, TR3_00, 8, 8, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(12).ToString) + "' target='_blank'>" + DR(0).ToString())
                RH.AddColumn(TR3, TR3_000, 2, 2, "l", i.ToString())
                RH.AddColumn(TR3, TR3_00, 4, 4, "l", "<a href='ViewRequestDtl.aspx?App_Request_ID=" + GF.Encrypt(DR(15).ToString()) + "' target='_blank'>" + DR(0).ToString + "</a>")

                RH.AddColumn(TR3, TR3_01, 3, 3, "l", DR(1).ToString)
                RH.AddColumn(TR3, TR3_13, 3, 3, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_14, 3, 3, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_02, 4, 4, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_03, 3, 3, "c", DR(5).ToString())
                RH.AddColumn(TR3, TR3_04, 7, 7, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_05, 7, 7, "l", DR(7).ToString())

                RH.AddColumn(TR3, TR3_06, 5, 5, "l", DR(8).ToString())
                RH.AddColumn(TR3, TR3_07, 5, 5, "l", DR(9).ToString())
                RH.AddColumn(TR3, TR3_08, 4, 4, "l", DR(10).ToString())



                RH.AddColumn(TR3, TR3_09, 5, 5, "l", DR(11).ToString())
                RH.AddColumn(TR3, TR3_10, 7, 7, "l", DR(12).ToString())
                If DR(17).ToString() = "1" And DR(18).ToString() = "1" And DR(19).ToString() = "0" Then
                    RH.AddColumn(TR3, TR3_11, 4, 4, "l", DR(16).ToString())
                    RH.AddColumn(TR3, TR3_12, 4, 4, "l", "")
                Else
                    RH.AddColumn(TR3, TR3_11, 4, 4, "l", "")
                    RH.AddColumn(TR3, TR3_12, 4, 4, "l", DR(16).ToString())
                End If


                RH.AddColumn(TR3, TR3_13, 3, 3, "l", DR(13).ToString())
                RH.AddColumn(TR3, TR3_14, 3, 3, "l", DR(14).ToString())


                RH.AddColumn(TR3, TR3_15, 3, 3, "l", DR(20).ToString())
                RH.AddColumn(TR3, TR3_16, 3, 3, "l", DR(21).ToString())
                RH.AddColumn(TR3, TR3_17, 3, 3, "l", DR(22).ToString())
                RH.AddColumn(TR3, TR3_18, 3, 3, "l", DR(23).ToString())
                RH.AddColumn(TR3, TR3_19, 3, 3, "l", DR(24).ToString())
                RH.AddColumn(TR3, TR3_20, 3, 3, "l", DR(25).ToString())
                RH.AddColumn(TR3, TR3_21, 3, 3, "l", DR(26).ToString())
                RH.AddColumn(TR3, TR3_22, 3, 3, "l", DR(27).ToString())


                'If DR(11) = 1 Then

                '    TR3_01.Style.Add("background-color", "#F3C2C2")
                '    TR3_02.Style.Add("background-color", "#F3C2C2")
                '    TR3_03.Style.Add("background-color", "#F3C2C2")
                '    TR3_04.Style.Add("background-color", "#F3C2C2")
                '    TR3_05.Style.Add("background-color", "#F3C2C2")
                '    TR3_06.Style.Add("background-color", "#F3C2C2")
                '    TR3_07.Style.Add("background-color", "#F3C2C2")
                '    TR3_08.Style.Add("background-color", "#F3C2C2")
                '    TR3_09.Style.Add("background-color", "#F3C2C2")
                '    TR3_10.Style.Add("background-color", "#F3C2C2")
                '    TR3_11.Style.Add("background-color", "#F3C2C2")
                '    TR3_12.Style.Add("background-color", "#F3C2C2")

                'End If

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        'Try
        WebTools.ExporttoExcel(DT, "Ticket")
        'Catch ex As Exception
        '    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
        '    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        'End Try
    End Sub
End Class
