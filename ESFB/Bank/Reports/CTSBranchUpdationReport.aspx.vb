﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class CTSBranchUpdationReport
    Inherits System.Web.UI.Page

    Dim DT As New DataTable
    Dim GN As New GeneralFunctions

    Dim ReportID As Integer = 1
    Dim LocationID As Integer = 0
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1130) = False Then
                'If GN.FormAccess(CInt(Session("UserID")), 1133) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If


            ReportID = CInt(Request.QueryString.Get("RptID"))

            Dim BranchID As Integer = CInt(Session("BranchID"))
            If BranchID < 100 Then
                DT = DB.ExecuteDataSet("select '-1' as branch_id, '------ Select ------' as Branch_Name union all select distinct b.branch_id,b.branch_name from  CTS_branch_close a,branch_master b   where a.branch_id=b.branch_id ").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select distinct b.branch_id,b.branch_name from  CTS_branch_close a,branch_master b   where a.branch_id=b.branch_id and a.branch_id= " & BranchID.ToString & "").Tables(0)
            End If



            GN.ComboFill(cmbBranch, DT, 0, 1)



            Me.Master.subtitle = "CTS Branch Summary Report"
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub


End Class
