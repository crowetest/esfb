﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewRequestDtl
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim App_request_id As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            App_request_id = CInt(GF.Decrypt(Request.QueryString.Get("App_request_id")))
            hdnReportID.Value = App_request_id.ToString()

            Dim EmpCode As Integer
            Dim EmpName As String

            Dim DT As New DataTable
            Dim DT_Sum As New DataTable
            'Session("GraphType") = "RangeColumn"
            Dim TR22 As New TableRow
            Dim TR55 As New TableRow
            Dim TR66 As New TableRow
            Dim TR77 As New TableRow
            Dim TR88 As New TableRow
            Dim TR51 As New TableRow
            Dim TR52 As New TableRow
            Dim TR99 As New TableRow
            Dim TR30, TR31, TR32, TR33, TR34, TR35, TR36, TR37, TR38, TR39, TR40, TR41, TR42, TR43, TR59, TR60, TR61, TR994 As New TableRow


            'DT = DB.ExecuteDataSet("select c.service_request_no,upper(c.request_no) as REQUEST_NO,d.emp_code as EMP_CODE, upper(c.Emp_Name) as EMP_NAME  ,d.mobile as mobile,d.mail_id as send_email, " & _
            '                 " c.created_by,c.created_on  as REQUEST_DATE,  " & _
            '                 " case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESMACO' else 'LBS' end as Entity,upper(c.created_branch_name)+'('+convert(varchar,c.created_branch)+')' as  BRANCH, " & _
            '                 " upper(j.app_name) as APPLICATION,upper(e.App_Type_name) as TYPE,case when c.app_id = 14 then (SELECT Stuff((select cr.category + ' = ' + cr.type_name + ', ' AS 'data()' from ESFB.dbo.App_Crm_Request_all cr where cr.app_request_id=c.app_request_id FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,0,N'')) else upper(g.App_role_name) end Role,case when c.teller_id=1 then 'Head Teller' when c.teller_id=2 then " & _
            '                 " 'Teller Only' when  c.teller_id=3 then 'Head Teller&Cashier' else '' end as TellerType  ,  " & _
            '                 " lower(c.requestmailid) as Requested_Email,  " & _
            '                 " c.storage_in_gb, " & _
            '                 " case when Access_type=1 then 'Read' when access_type=2 then 'write' when access_type=3 then 'Read&Write' else '' end as access_type, " & _
            '                 " upper(Remarks) as Requested_Person_Remarks,upper(CASE WHEN (k.order_id=0 and c.Approved_status is null) THEN 'Reporting level Pending - ' + isnull(c.reporting_NAME, '') +' ( '  " & _
            '                 " + CONVERT(VARCHAR,c.reporting_to) +')'   " & _
            '                 " when (k.order_id>0 and (c.approved_status=1 and c.HO_Approved_status is null)) THEN  k.level_name +' - ' + '( ' + CONVERT(VARCHAR,k.order_id) +')'   " & _
            '                 " WHEN  (k.order_id>=0 and c.Approved_status =2) then 'Rejected By Reporting Person'    " & _
            '                 " WHEN  (k.order_id>0 and c.HO_Approved_status =2) then 'Rejected By HO'  WHEN  (k.order_id>0 and c.HO_Approved_status =1) then 'Approved By HO'    " & _
            '                 " ELSE '' end ) AS current_status, " & _
            '                 " case when approved_status=1 then 'Approved'  when approved_status=2 then 'Rejected'  when approved_status is null then 'Pending for Approval' else '' end as approved_status, " & _
            '                 " c.reporting_name +'('+ convert(varchar,c.reporting_to)+')' as approved_by,c.reporting_mail_id, c.approved_on,c.approved_remarks, " & _
            '                 " case when ho_approved_status=1 then 'Approved'  when ho_approved_status=2 then 'Rejected'  when ho_approved_status is null then 'Pending for Approval' else '' end as HO_approved_status, " & _
            '                 " x.name+'('+convert(varchar,x.emp_code)+')' as HO_Approved_By, " & _
            '                 " x.email as HO_Approved_MAil_ID, " & _
            '                 " c.ho_approved_on, c.ho_approved_remarks, " & _
            '                 " c.send_date as SendForProcessing,c.app_request_id,d.to_branch_name,c.app_id,c.app_type_id ,c.closed_remarks,case when c.closed_status=1 then 'Closed'  when c.closed_status=2 then 'Rejected' else '' end+'('+convert(varchar,c.closed_by)+')',old_role_name,c.dl,c.previous_staff   from ESFB.dbo.app_level k   " & _
            '                 " ,ESFB.dbo.app_request_master c left join ESFB.dbo.app_level_email x on c.ho_approved_by=x.emp_code ,ESFB.dbo.app_master j,   " & _
            '                 " ESFB.dbo.app_type_master e ,     ESFB.dbo.app_dtl_profile d    " & _
            '                 " left join ESFB.dbo.app_role_master    " & _
            '                 " g on d.Role_id=g.App_role  where    k.app_id=j.App_id and c.App_type_id=e.App_type_id    " & _
            '                 " and c.App_request_id=d.App_request_id  and    c.level_id=k.level_id and c.app_request_id=" & App_request_id.ToString & "").Tables(0)

            'update by vidya ---begin (for changing current level status of dl mapping )

            'DT = DB.ExecuteDataSet("select c.service_request_no,upper(c.request_no) as REQUEST_NO,d.emp_code as EMP_CODE, upper(c.Emp_Name) as EMP_NAME  ,d.mobile as mobile,d.mail_id as send_email, " & _
            '                 " c.created_by,c.created_on  as REQUEST_DATE,  " & _
            '                 " case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESMACO' else 'LBS' end as Entity,upper(c.created_branch_name)+'('+convert(varchar,c.created_branch)+')' as  BRANCH, " & _
            '                 " upper(j.app_name) as APPLICATION,upper(e.App_Type_name) as TYPE,case when c.app_id = 14 then (SELECT Stuff((select cr.category + ' = ' + cr.type_name + ', ' AS 'data()' from ESFB.dbo.App_Crm_Request_all cr where cr.app_request_id=c.app_request_id FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,0,N'')) else upper(g.App_role_name) end Role,case when c.teller_id=1 then 'Head Teller' when c.teller_id=2 then " & _
            '                 " 'Teller Only' when  c.teller_id=3 then 'Head Teller&Cashier' else '' end as TellerType  ,  " & _
            '                 " lower(c.requestmailid) as Requested_Email,  " & _
            '                 " c.storage_in_gb, " & _
            '                 " case when Access_type=1 then 'Read' when access_type=2 then 'write' when access_type=3 then 'Read&Write' else '' end as access_type, " & _
            '                 " upper(Remarks) as Requested_Person_Remarks,upper(CASE WHEN (k.order_id=0 and c.Approved_status is null and c.app_type_id <> 36) THEN 'Reporting level Pending - ' + isnull(c.reporting_NAME, '') +' ( '  " & _
            '                 " + CONVERT(VARCHAR,c.reporting_to) +')'  when (k.order_id = 0 and c.approved_status is null and c.app_type_id = 36) then 'Department level pending' " & _
            '                 " when (k.order_id>0 and (c.approved_status=1 and c.HO_Approved_status is null)) THEN  k.level_name +' - ' + '( ' + CONVERT(VARCHAR,k.order_id) +')'   " & _
            '                 " WHEN  (k.order_id>=0 and c.Approved_status =2) then 'Rejected By Reporting Person'    " & _
            '                 " WHEN  (k.order_id>0 and c.HO_Approved_status =2) then 'Rejected By HO'  WHEN  (k.order_id>0 and c.HO_Approved_status =1) then 'Approved By HO'    " & _
            '                 " ELSE '' end ) AS current_status, " & _
            '                 " case when approved_status=1 then 'Approved'  when approved_status=2 then 'Rejected'  when approved_status is null then 'Pending for Approval' else '' end as approved_status, " & _
            '                 " c.reporting_name +'('+ convert(varchar,c.reporting_to)+')' as approved_by,c.reporting_mail_id, c.approved_on,c.approved_remarks, " & _
            '                 " case when ho_approved_status=1 then 'Approved'  when ho_approved_status=2 then 'Rejected'  when ho_approved_status is null then 'Pending for Approval' else '' end as HO_approved_status, " & _
            '                 " x.name+'('+convert(varchar,x.emp_code)+')' as HO_Approved_By, " & _
            '                 " x.email as HO_Approved_MAil_ID, " & _
            '                 " c.ho_approved_on, c.ho_approved_remarks, " & _
            '                 " c.send_date as SendForProcessing,c.app_request_id,d.to_branch_name,c.app_id,c.app_type_id ,c.closed_remarks,case when c.closed_status=1 then 'Closed'  when c.closed_status=2 then 'Rejected' else '' end+'('+convert(varchar,c.closed_by)+')',old_role_name,c.dl,c.previous_staff,c.closed_on,f.emp_name +'('+convert(varchar,c.closed_by)+')' as closed_by   from ESFB.dbo.app_level k   " & _
            '                 " ,ESFB.dbo.app_request_master c left join ESFB.dbo.app_level_email x on c.ho_approved_by=x.emp_code left join ESFB.dbo.emp_master f on f.emp_code = c.closed_by,ESFB.dbo.app_master j,   " & _
            '                 " ESFB.dbo.app_type_master e ,     ESFB.dbo.app_dtl_profile d    " & _
            '                 " left join ESFB.dbo.app_role_master    " & _
            '                 " g on d.Role_id=g.App_role  where    k.app_id=j.App_id and c.App_type_id=e.App_type_id    " & _
            '                 " and c.App_request_id=d.App_request_id  and    c.level_id=k.level_id and c.app_request_id=" & App_request_id.ToString & "").Tables(0)

            'update by vidya ---end

            '<Summary>Changed by 40020 on 04-Jan-2021</Summary>
            '<Comments>Procedure Call Added</Comments>

            Dim Parameters1 As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@CategoryID", 2),
                                                                        New System.Data.SqlClient.SqlParameter("@AppRequestID", App_request_id.ToString)}
            DT = DB.ExecuteDataSet("SP_APP_GET_REQUESTS", Parameters1).Tables(0)


            RH.Heading(CStr(Session("FirmName")), tb, "BRANCH REQUEST DEATILED REPORT", 100)

            Dim DR As DataRow
            If DT.Rows.Count > 0 Then



                Dim TR55_00, TR55_01, TR55_02, TR55_03, TR55_04, TR55_05, TR55_06, TR55_07 As New TableCell
                RH.AddColumn(TR55, TR55_00, 23, 23, "l", "&nbsp;&nbsp;SERVICE REQUEST NO")
                RH.AddColumn(TR55, TR55_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR55, TR55_02, 23, 23, "l", DT.Rows(0)(0).ToString())
                RH.AddColumn(TR55, TR55_03, 3, 3, "l", "")
                RH.AddColumn(TR55, TR55_04, 3, 3, "l", "")
                RH.AddColumn(TR55, TR55_05, 23, 23, "l", "&nbsp;&nbsp;REQUEST NO")
                RH.AddColumn(TR55, TR55_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR55, TR55_07, 23, 23, "l", DT.Rows(0)(1).ToString())
                tb.Controls.Add(TR55)
                TR55_00.BackColor = Drawing.Color.WhiteSmoke
                TR55_01.BackColor = Drawing.Color.WhiteSmoke
                TR55_02.BackColor = Drawing.Color.WhiteSmoke
                TR55_05.BackColor = Drawing.Color.WhiteSmoke
                TR55_06.BackColor = Drawing.Color.WhiteSmoke
                TR55_07.BackColor = Drawing.Color.WhiteSmoke
                RH.BlankRow(tb, 1)
                Dim TR66_00, TR66_01, TR66_02, TR66_03, TR66_04, TR66_05, TR66_06, TR66_07 As New TableCell
                RH.AddColumn(TR66, TR66_00, 23, 23, "l", "&nbsp;&nbsp;EMP CODE")
                RH.AddColumn(TR66, TR66_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR66, TR66_02, 23, 23, "l", DT.Rows(0)(2).ToString())
                RH.AddColumn(TR66, TR66_03, 3, 3, "l", "")
                RH.AddColumn(TR66, TR66_04, 3, 3, "l", "")
                RH.AddColumn(TR66, TR66_05, 23, 23, "l", "&nbsp;&nbsp;NAME")
                RH.AddColumn(TR66, TR66_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR66, TR66_07, 23, 23, "l", DT.Rows(0)(3).ToString())
                TR66_00.BackColor = Drawing.Color.WhiteSmoke
                TR66_01.BackColor = Drawing.Color.WhiteSmoke
                TR66_02.BackColor = Drawing.Color.WhiteSmoke
                TR66_05.BackColor = Drawing.Color.WhiteSmoke
                TR66_06.BackColor = Drawing.Color.WhiteSmoke
                TR66_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR66)

                Dim TR77_00, TR77_01, TR77_02, TR77_03, TR77_04, TR77_05, TR77_06, TR77_07 As New TableCell
                RH.AddColumn(TR77, TR77_00, 23, 23, "l", "&nbsp;&nbsp;MOBILE")
                RH.AddColumn(TR77, TR77_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR77, TR77_02, 23, 23, "l", DT.Rows(0)(4).ToString())
                RH.AddColumn(TR77, TR77_03, 3, 3, "l", "")
                RH.AddColumn(TR77, TR77_04, 3, 3, "l", "")
                RH.AddColumn(TR77, TR77_05, 23, 23, "l", "&nbsp;&nbsp;MAIL_ID")
                RH.AddColumn(TR77, TR77_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR77, TR77_07, 23, 23, "l", DT.Rows(0)(5).ToString())
                TR77_00.BackColor = Drawing.Color.WhiteSmoke
                TR77_01.BackColor = Drawing.Color.WhiteSmoke
                TR77_02.BackColor = Drawing.Color.WhiteSmoke
                TR77_05.BackColor = Drawing.Color.WhiteSmoke
                TR77_06.BackColor = Drawing.Color.WhiteSmoke
                TR77_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR77)

                If DT.Rows(0)(37).ToString() <> "" Then
                    Dim TR34_00, TR34_01, TR34_02, TR34_03, TR34_04, TR34_05, TR34_06, TR34_07 As New TableCell
                    RH.AddColumn(TR34, TR34_00, 23, 23, "l", "&nbsp;&nbsp;Distribution List")
                    RH.AddColumn(TR34, TR34_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR34, TR34_02, 23, 23, "l", DT.Rows(0)(37).ToString())
                    RH.AddColumn(TR34, TR34_03, 3, 3, "l", "")
                    RH.AddColumn(TR34, TR34_04, 3, 3, "l", "")
                    RH.AddColumn(TR34, TR34_05, 23, 23, "l", "&nbsp;&nbsp;")
                    RH.AddColumn(TR34, TR34_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR34, TR34_07, 23, 23, "l", "")
                    TR34_00.BackColor = Drawing.Color.WhiteSmoke
                    TR34_01.BackColor = Drawing.Color.WhiteSmoke
                    TR34_02.BackColor = Drawing.Color.WhiteSmoke
                    TR34_05.BackColor = Drawing.Color.WhiteSmoke
                    TR34_06.BackColor = Drawing.Color.WhiteSmoke
                    TR34_07.BackColor = Drawing.Color.WhiteSmoke
                    tb.Controls.Add(TR34)
                End If
                Dim TR88_00, TR88_01, TR88_02, TR88_03, TR88_04, TR88_05, TR88_06, TR88_07 As New TableCell
                RH.AddColumn(TR88, TR88_00, 23, 23, "l", "&nbsp;&nbsp;CREATED BY")
                RH.AddColumn(TR88, TR88_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR88, TR88_02, 23, 23, "l", DT.Rows(0)(6).ToString())
                RH.AddColumn(TR88, TR88_03, 3, 3, "l", "")
                RH.AddColumn(TR88, TR88_04, 3, 3, "l", "")
                RH.AddColumn(TR88, TR88_05, 23, 23, "l", "&nbsp;&nbsp;REQUESTED_DATE")
                RH.AddColumn(TR88, TR88_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR88, TR88_07, 23, 23, "l", DT.Rows(0)(7).ToString())
                TR88_00.BackColor = Drawing.Color.WhiteSmoke
                TR88_01.BackColor = Drawing.Color.WhiteSmoke
                TR88_02.BackColor = Drawing.Color.WhiteSmoke
                TR88_05.BackColor = Drawing.Color.WhiteSmoke
                TR88_06.BackColor = Drawing.Color.WhiteSmoke
                TR88_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR88)

                Dim TR30_00, TR30_01, TR30_02, TR30_03, TR30_04, TR30_05, TR30_06, TR30_07 As New TableCell
                RH.AddColumn(TR30, TR30_00, 23, 23, "l", "&nbsp;&nbsp;ENTITY")
                RH.AddColumn(TR30, TR30_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR30, TR30_02, 23, 23, "l", DT.Rows(0)(8).ToString())
                RH.AddColumn(TR30, TR30_03, 3, 3, "l", "")
                RH.AddColumn(TR30, TR30_04, 3, 3, "l", "")
                RH.AddColumn(TR30, TR30_05, 23, 23, "l", "&nbsp;&nbsp;BRANCH")
                RH.AddColumn(TR30, TR30_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR30, TR30_07, 23, 23, "l", DT.Rows(0)(9).ToString())
                TR30_00.BackColor = Drawing.Color.WhiteSmoke
                TR30_01.BackColor = Drawing.Color.WhiteSmoke
                TR30_02.BackColor = Drawing.Color.WhiteSmoke
                TR30_05.BackColor = Drawing.Color.WhiteSmoke
                TR30_06.BackColor = Drawing.Color.WhiteSmoke
                TR30_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR30)

                Dim TR31_00, TR31_01, TR31_02, TR31_03, TR31_04, TR31_05, TR31_06, TR31_07 As New TableCell
                RH.AddColumn(TR31, TR31_00, 23, 23, "l", "&nbsp;&nbsp;APPLICATION")
                RH.AddColumn(TR31, TR31_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR31, TR31_02, 23, 23, "l", DT.Rows(0)(10).ToString())
                RH.AddColumn(TR31, TR31_03, 3, 3, "l", "")
                RH.AddColumn(TR31, TR31_04, 3, 3, "l", "")
                RH.AddColumn(TR31, TR31_05, 23, 23, "l", "&nbsp;&nbsp;TYPE")
                RH.AddColumn(TR31, TR31_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR31, TR31_07, 23, 23, "l", DT.Rows(0)(11).ToString())
                TR31_00.BackColor = Drawing.Color.WhiteSmoke
                TR31_01.BackColor = Drawing.Color.WhiteSmoke
                TR31_02.BackColor = Drawing.Color.WhiteSmoke
                TR31_05.BackColor = Drawing.Color.WhiteSmoke
                TR31_06.BackColor = Drawing.Color.WhiteSmoke
                TR31_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR31)

                If DT.Rows(0)(32).ToString() = 1 Or DT.Rows(0)(32).ToString() = 10 Then
                    Dim TR32_00, TR32_01, TR32_02, TR32_03, TR32_04, TR32_05, TR32_06, TR32_07 As New TableCell
                    RH.AddColumn(TR32, TR32_00, 23, 23, "l", "&nbsp;&nbsp;ROLE")
                    RH.AddColumn(TR32, TR32_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR32, TR32_02, 23, 23, "l", DT.Rows(0)(12).ToString())
                    RH.AddColumn(TR32, TR32_03, 3, 3, "l", "")
                    RH.AddColumn(TR32, TR32_04, 3, 3, "l", "")
                    RH.AddColumn(TR32, TR32_05, 23, 23, "l", "&nbsp;&nbsp;PREVIOUS ROLE")
                    RH.AddColumn(TR32, TR32_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR32, TR32_07, 23, 23, "l", DT.Rows(0)(36).ToString())
                    TR32_00.BackColor = Drawing.Color.WhiteSmoke
                    TR32_01.BackColor = Drawing.Color.WhiteSmoke
                    TR32_02.BackColor = Drawing.Color.WhiteSmoke
                    TR32_05.BackColor = Drawing.Color.WhiteSmoke
                    TR32_06.BackColor = Drawing.Color.WhiteSmoke
                    TR32_07.BackColor = Drawing.Color.WhiteSmoke
                    tb.Controls.Add(TR32)

                    Dim TR33_00, TR33_01, TR33_02, TR33_03, TR33_04, TR33_05, TR33_06, TR33_07 As New TableCell
                    RH.AddColumn(TR33, TR33_00, 23, 23, "l", "&nbsp;&nbsp;TELLER TYPE")
                    RH.AddColumn(TR33, TR33_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR33, TR33_02, 23, 23, "l", DT.Rows(0)(13).ToString())
                    RH.AddColumn(TR33, TR33_03, 3, 3, "l", "")
                    RH.AddColumn(TR33, TR33_04, 3, 3, "l", "")
                    RH.AddColumn(TR33, TR33_05, 23, 23, "l", "&nbsp;&nbsp;PREVIOUS STAFF")
                    RH.AddColumn(TR33, TR33_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR33, TR33_07, 23, 23, "l", DT.Rows(0)(38).ToString())
                    TR33_00.BackColor = Drawing.Color.WhiteSmoke
                    TR33_01.BackColor = Drawing.Color.WhiteSmoke
                    TR33_02.BackColor = Drawing.Color.WhiteSmoke
                    TR33_05.BackColor = Drawing.Color.WhiteSmoke
                    TR33_06.BackColor = Drawing.Color.WhiteSmoke
                    TR33_07.BackColor = Drawing.Color.WhiteSmoke
                    tb.Controls.Add(TR33)
                End If

                If DT.Rows(0)(32).ToString() = 14 Then
                    Dim TR32_00, TR32_01, TR32_02, TR32_03, TR32_04, TR32_05, TR32_06, TR32_07 As New TableCell
                    'RH.AddColumn(TR32, TR32_00, 23, 23, "l", "&nbsp;&nbsp;Access To")
                    RH.AddColumn(TR32, TR32_00, 23, 23, "l", "&nbsp;&nbsp;ACCESS TO")
                    RH.AddColumn(TR32, TR32_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR32, TR32_02, 23, 23, "l", DT.Rows(0)(12).ToString())
                    RH.AddColumn(TR32, TR32_03, 3, 3, "l", "")
                    RH.AddColumn(TR32, TR32_04, 3, 3, "l", "")
                    RH.AddColumn(TR32, TR32_05, 23, 23, "l", "")
                    RH.AddColumn(TR32, TR32_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR32, TR32_07, 23, 23, "l", "")
                    TR32_00.BackColor = Drawing.Color.WhiteSmoke
                    TR32_01.BackColor = Drawing.Color.WhiteSmoke
                    TR32_02.BackColor = Drawing.Color.WhiteSmoke
                    TR32_05.BackColor = Drawing.Color.WhiteSmoke
                    TR32_06.BackColor = Drawing.Color.WhiteSmoke
                    TR32_07.BackColor = Drawing.Color.WhiteSmoke
                    tb.Controls.Add(TR32)
                End If

                '<Summary>Changed by 40020 on 04-Jan-2021</Summary>
                '<Comments>Profile-MIS added</Comments>
                If DT.Rows(0)(32).ToString() = 8 Then
                    If DT.Rows(0)(33).ToString() = 116 Or DT.Rows(0)(33).ToString() = 117 Then
                        Dim TR32_00, TR32_01, TR32_02, TR32_03, TR32_04, TR32_05, TR32_06, TR32_07 As New TableCell
                        RH.AddColumn(TR32, TR32_00, 23, 23, "l", "&nbsp;&nbsp;ACCESS TO")
                        RH.AddColumn(TR32, TR32_01, 1, 1, "c", ":&nbsp;&nbsp;")
                        RH.AddColumn(TR32, TR32_02, 23, 23, "l", DT.Rows(0)(12).ToString())
                        RH.AddColumn(TR32, TR32_03, 3, 3, "l", "")
                        RH.AddColumn(TR32, TR32_04, 3, 3, "l", "")
                        RH.AddColumn(TR32, TR32_05, 23, 23, "l", "")
                        RH.AddColumn(TR32, TR32_06, 1, 1, "c", ":&nbsp;&nbsp;")
                        RH.AddColumn(TR32, TR32_07, 23, 23, "l", "")
                        TR32_00.BackColor = Drawing.Color.WhiteSmoke
                        TR32_01.BackColor = Drawing.Color.WhiteSmoke
                        TR32_02.BackColor = Drawing.Color.WhiteSmoke
                        TR32_05.BackColor = Drawing.Color.WhiteSmoke
                        TR32_06.BackColor = Drawing.Color.WhiteSmoke
                        TR32_07.BackColor = Drawing.Color.WhiteSmoke
                        tb.Controls.Add(TR32)
                    End If
                    If DT.Rows(0)(33).ToString() = 115 Then
                        Dim TR32_00, TR32_01, TR32_02, TR32_03, TR32_04, TR32_05, TR32_06, TR32_07 As New TableCell
                        'RH.AddColumn(TR32, TR32_00, 23, 23, "l", "&nbsp;&nbsp;Access To")
                        RH.AddColumn(TR32, TR32_00, 23, 23, "l", "&nbsp;&nbsp;TO BE TRANSFER BRANCH")
                        RH.AddColumn(TR32, TR32_01, 1, 1, "c", ":&nbsp;&nbsp;")
                        RH.AddColumn(TR32, TR32_02, 23, 23, "l", DT.Rows(0)(31).ToString())
                        RH.AddColumn(TR32, TR32_03, 3, 3, "l", "")
                        RH.AddColumn(TR32, TR32_04, 3, 3, "l", "")
                        RH.AddColumn(TR32, TR32_05, 23, 23, "l", "")
                        RH.AddColumn(TR32, TR32_06, 1, 1, "c", ":&nbsp;&nbsp;")
                        RH.AddColumn(TR32, TR32_07, 23, 23, "l", "")
                        TR32_00.BackColor = Drawing.Color.WhiteSmoke
                        TR32_01.BackColor = Drawing.Color.WhiteSmoke
                        TR32_02.BackColor = Drawing.Color.WhiteSmoke
                        TR32_05.BackColor = Drawing.Color.WhiteSmoke
                        TR32_06.BackColor = Drawing.Color.WhiteSmoke
                        TR32_07.BackColor = Drawing.Color.WhiteSmoke
                        tb.Controls.Add(TR32)
                    End If
                End If


                '<Summary>Changed by 40020 on 08-Feb-2021</Summary>
                '<Comments>CASA TAB Transfer user added</Comments>
                If DT.Rows(0)(32).ToString() = 9 Then
                    If DT.Rows(0)(33).ToString() = 42 Then
                        Dim TR32_00, TR32_01, TR32_02, TR32_03, TR32_04, TR32_05, TR32_06, TR32_07 As New TableCell
                        'RH.AddColumn(TR32, TR32_00, 23, 23, "l", "&nbsp;&nbsp;Access To")
                        RH.AddColumn(TR32, TR32_00, 23, 23, "l", "&nbsp;&nbsp;TO BE TRANSFER BRANCH")
                        RH.AddColumn(TR32, TR32_01, 1, 1, "c", ":&nbsp;&nbsp;")
                        RH.AddColumn(TR32, TR32_02, 23, 23, "l", DT.Rows(0)(31).ToString())
                        RH.AddColumn(TR32, TR32_03, 3, 3, "l", "")
                        RH.AddColumn(TR32, TR32_04, 3, 3, "l", "")
                        RH.AddColumn(TR32, TR32_05, 23, 23, "l", "")
                        RH.AddColumn(TR32, TR32_06, 1, 1, "c", ":&nbsp;&nbsp;")
                        RH.AddColumn(TR32, TR32_07, 23, 23, "l", "")
                        TR32_00.BackColor = Drawing.Color.WhiteSmoke
                        TR32_01.BackColor = Drawing.Color.WhiteSmoke
                        TR32_02.BackColor = Drawing.Color.WhiteSmoke
                        TR32_05.BackColor = Drawing.Color.WhiteSmoke
                        TR32_06.BackColor = Drawing.Color.WhiteSmoke
                        TR32_07.BackColor = Drawing.Color.WhiteSmoke
                        tb.Controls.Add(TR32)
                    End If
                End If

                '<Summary>Changed by 40020 on 08-Feb-2021</Summary>
                '<Comments>M-Bank Transfer user added</Comments>
                If DT.Rows(0)(32).ToString() = 11 Then
                    If DT.Rows(0)(33).ToString() = 60 Then
                        Dim TR32_00, TR32_01, TR32_02, TR32_03, TR32_04, TR32_05, TR32_06, TR32_07 As New TableCell
                        'RH.AddColumn(TR32, TR32_00, 23, 23, "l", "&nbsp;&nbsp;Access To")
                        RH.AddColumn(TR32, TR32_00, 23, 23, "l", "&nbsp;&nbsp;TO BE TRANSFER BRANCH")
                        RH.AddColumn(TR32, TR32_01, 1, 1, "c", ":&nbsp;&nbsp;")
                        RH.AddColumn(TR32, TR32_02, 23, 23, "l", DT.Rows(0)(31).ToString())
                        RH.AddColumn(TR32, TR32_03, 3, 3, "l", "")
                        RH.AddColumn(TR32, TR32_04, 3, 3, "l", "")
                        RH.AddColumn(TR32, TR32_05, 23, 23, "l", "")
                        RH.AddColumn(TR32, TR32_06, 1, 1, "c", ":&nbsp;&nbsp;")
                        RH.AddColumn(TR32, TR32_07, 23, 23, "l", "")
                        TR32_00.BackColor = Drawing.Color.WhiteSmoke
                        TR32_01.BackColor = Drawing.Color.WhiteSmoke
                        TR32_02.BackColor = Drawing.Color.WhiteSmoke
                        TR32_05.BackColor = Drawing.Color.WhiteSmoke
                        TR32_06.BackColor = Drawing.Color.WhiteSmoke
                        TR32_07.BackColor = Drawing.Color.WhiteSmoke
                        tb.Controls.Add(TR32)
                    End If
                End If

                If DT.Rows(0)(32).ToString() = 4 Then
                    Dim TR994_00, TR994_01, TR994_02, TR994_03, TR994_04, TR994_05, TR994_06, TR994_07 As New TableCell
                    RH.AddColumn(TR994, TR994_00, 23, 23, "l", "&nbsp;&nbsp;TO BE CREATE MAILID")
                    RH.AddColumn(TR994, TR994_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR994, TR994_02, 23, 23, "l", DT.Rows(0)(14).ToString())
                    RH.AddColumn(TR994, TR994_03, 3, 3, "l", "")
                    RH.AddColumn(TR994, TR994_04, 3, 3, "l", "")
                    RH.AddColumn(TR994, TR994_05, 23, 23, "l", "&nbsp;&nbsp;")
                    RH.AddColumn(TR994, TR994_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR994, TR994_07, 23, 23, "l", DT.Rows(0)(37).ToString())
                    TR994_00.BackColor = Drawing.Color.WhiteSmoke
                    TR994_01.BackColor = Drawing.Color.WhiteSmoke
                    TR994_02.BackColor = Drawing.Color.WhiteSmoke
                    TR994_05.BackColor = Drawing.Color.WhiteSmoke
                    TR994_06.BackColor = Drawing.Color.WhiteSmoke
                    TR994_07.BackColor = Drawing.Color.WhiteSmoke
                    tb.Controls.Add(TR994)
                End If
                If DT.Rows(0)(32).ToString() = 5 Then
                    Dim TR35_00, TR35_01, TR35_02, TR35_03, TR35_04, TR35_05, TR35_06, TR35_07 As New TableCell
                    RH.AddColumn(TR35, TR35_00, 23, 23, "l", "&nbsp;&nbsp;STORAGE")
                    RH.AddColumn(TR35, TR35_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR35, TR35_02, 23, 23, "l", DT.Rows(0)(15).ToString())
                    RH.AddColumn(TR35, TR35_03, 3, 3, "l", "")
                    RH.AddColumn(TR35, TR35_04, 3, 3, "l", "")
                    RH.AddColumn(TR35, TR35_05, 23, 23, "l", "&nbsp;&nbsp;ACCESS TYPE")
                    RH.AddColumn(TR35, TR35_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR35, TR35_07, 23, 23, "l", DT.Rows(0)(16).ToString())
                    TR35_00.BackColor = Drawing.Color.WhiteSmoke
                    TR35_01.BackColor = Drawing.Color.WhiteSmoke
                    TR35_02.BackColor = Drawing.Color.WhiteSmoke
                    TR35_05.BackColor = Drawing.Color.WhiteSmoke
                    TR35_06.BackColor = Drawing.Color.WhiteSmoke
                    TR35_07.BackColor = Drawing.Color.WhiteSmoke
                    tb.Controls.Add(TR35)
                End If
                If DT.Rows(0)(32).ToString() = 1 And DT.Rows(0)(33).ToString() = 3 Then
                    Dim TR41_00, TR41_01, TR41_02, TR41_03, TR41_04, TR41_05, TR41_06, TR41_07 As New TableCell
                    RH.AddColumn(TR41, TR41_00, 23, 23, "l", "&nbsp;&nbsp;TO BE TRANSFER BRANCH")
                    RH.AddColumn(TR41, TR41_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR41, TR41_02, 23, 23, "l", DT.Rows(0)(31).ToString())
                    RH.AddColumn(TR41, TR41_03, 3, 3, "l", "")
                    RH.AddColumn(TR41, TR41_04, 3, 3, "l", "")
                    RH.AddColumn(TR41, TR41_05, 23, 23, "l", "&nbsp;&nbsp;")
                    RH.AddColumn(TR41, TR41_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR41, TR41_07, 23, 23, "l", "")
                    TR41_00.BackColor = Drawing.Color.WhiteSmoke
                    TR41_01.BackColor = Drawing.Color.WhiteSmoke
                    TR41_02.BackColor = Drawing.Color.WhiteSmoke
                    TR41_05.BackColor = Drawing.Color.WhiteSmoke
                    TR41_06.BackColor = Drawing.Color.WhiteSmoke
                    TR41_07.BackColor = Drawing.Color.WhiteSmoke
                    tb.Controls.Add(TR41)
                End If
                '<Summary>Changed by 40020 on 21-Jan-2021</Summary>
                '<Comments>Rubix added</Comments>
                If DT.Rows(0)(32).ToString() = 20 Then

                    Dim TR32_00, TR32_01, TR32_02, TR32_03, TR32_04, TR32_05, TR32_06, TR32_07 As New TableCell
                    RH.AddColumn(TR32, TR32_00, 23, 23, "l", "&nbsp;&nbsp;ROLE")
                    RH.AddColumn(TR32, TR32_01, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR32, TR32_02, 23, 23, "l", DT.Rows(0)(12).ToString())
                    RH.AddColumn(TR32, TR32_03, 3, 3, "l", "")
                    RH.AddColumn(TR32, TR32_04, 3, 3, "l", "")
                    RH.AddColumn(TR32, TR32_05, 23, 23, "l", "&nbsp;&nbsp;PREVIOUS ROLE")
                    RH.AddColumn(TR32, TR32_06, 1, 1, "c", ":&nbsp;&nbsp;")
                    RH.AddColumn(TR32, TR32_07, 23, 23, "l", DT.Rows(0)(36).ToString())
                    TR32_00.BackColor = Drawing.Color.WhiteSmoke
                    TR32_01.BackColor = Drawing.Color.WhiteSmoke
                    TR32_02.BackColor = Drawing.Color.WhiteSmoke
                    TR32_05.BackColor = Drawing.Color.WhiteSmoke
                    TR32_06.BackColor = Drawing.Color.WhiteSmoke
                    TR32_07.BackColor = Drawing.Color.WhiteSmoke
                    tb.Controls.Add(TR32)

                    If DT.Rows(0)(33).ToString() = 120 Then
                        Dim TR41_00, TR41_01, TR41_02, TR41_03, TR41_04, TR41_05, TR41_06, TR41_07 As New TableCell
                        RH.AddColumn(TR41, TR41_00, 23, 23, "l", "&nbsp;&nbsp;TO BE TRANSFER BRANCH")
                        RH.AddColumn(TR41, TR41_01, 1, 1, "c", ":&nbsp;&nbsp;")
                        RH.AddColumn(TR41, TR41_02, 23, 23, "l", DT.Rows(0)(31).ToString())
                        RH.AddColumn(TR41, TR41_03, 3, 3, "l", "")
                        RH.AddColumn(TR41, TR41_04, 3, 3, "l", "")
                        RH.AddColumn(TR41, TR41_05, 23, 23, "l", "&nbsp;&nbsp;")
                        RH.AddColumn(TR41, TR41_06, 1, 1, "c", ":&nbsp;&nbsp;")
                        RH.AddColumn(TR41, TR41_07, 23, 23, "l", "")
                        TR41_00.BackColor = Drawing.Color.WhiteSmoke
                        TR41_01.BackColor = Drawing.Color.WhiteSmoke
                        TR41_02.BackColor = Drawing.Color.WhiteSmoke
                        TR41_05.BackColor = Drawing.Color.WhiteSmoke
                        TR41_06.BackColor = Drawing.Color.WhiteSmoke
                        TR41_07.BackColor = Drawing.Color.WhiteSmoke
                        tb.Controls.Add(TR41)
                    End If
                End If
                Dim TR36_00, TR36_01, TR36_02, TR36_03, TR36_04, TR36_05, TR36_06, TR36_07 As New TableCell
                RH.AddColumn(TR36, TR36_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR36, TR36_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR36, TR36_02, 23, 23, "l", DT.Rows(0)(17).ToString())
                RH.AddColumn(TR36, TR36_03, 3, 3, "l", "")
                RH.AddColumn(TR36, TR36_04, 3, 3, "l", "")
                RH.AddColumn(TR36, TR36_05, 23, 23, "l", "&nbsp;&nbsp;CURRENT STATUS")
                RH.AddColumn(TR36, TR36_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR36, TR36_07, 23, 23, "l", DT.Rows(0)(18).ToString())
                TR36_00.BackColor = Drawing.Color.WhiteSmoke
                TR36_01.BackColor = Drawing.Color.WhiteSmoke
                TR36_02.BackColor = Drawing.Color.WhiteSmoke
                TR36_05.BackColor = Drawing.Color.WhiteSmoke
                TR36_06.BackColor = Drawing.Color.WhiteSmoke
                TR36_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR36)

                RH.DrawLine(tb, 100)
                Dim TR0 As New TableRow
                TR0.BackColor = Drawing.Color.LightSlateGray
                Dim TR0_00 As New TableCell
                RH.AddColumn(TR0, TR0_00, 100, 100, "c", "APPROVED DETAILS")
                tb.Controls.Add(TR0)
                TR0_00.Font.Bold = True
                TR0_00.Font.Size = 20
                TR0_00.ForeColor = Drawing.Color.White
                RH.DrawLine(tb, 100)

                Dim TR37_00, TR37_01, TR37_02, TR37_03, TR37_04, TR37_05, TR37_06, TR37_07 As New TableCell
                RH.AddColumn(TR37, TR37_00, 23, 23, "l", "&nbsp;&nbsp;STATUS")
                RH.AddColumn(TR37, TR37_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR37, TR37_02, 23, 23, "l", DT.Rows(0)(19).ToString())
                RH.AddColumn(TR37, TR37_03, 3, 3, "l", "")
                RH.AddColumn(TR37, TR37_04, 3, 3, "l", "")
                RH.AddColumn(TR37, TR37_05, 23, 23, "l", "&nbsp;&nbsp;UPDATED BY")
                RH.AddColumn(TR37, TR37_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR37, TR37_07, 23, 23, "l", DT.Rows(0)(20).ToString())
                TR37_00.BackColor = Drawing.Color.WhiteSmoke
                TR37_01.BackColor = Drawing.Color.WhiteSmoke
                TR37_02.BackColor = Drawing.Color.WhiteSmoke
                TR37_05.BackColor = Drawing.Color.WhiteSmoke
                TR37_06.BackColor = Drawing.Color.WhiteSmoke
                TR37_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR37)


                Dim TR38_00, TR38_01, TR38_02, TR38_03, TR38_04, TR38_05, TR38_06, TR38_07 As New TableCell
                RH.AddColumn(TR38, TR38_00, 23, 23, "l", "&nbsp;&nbsp;UPDATED ON")
                RH.AddColumn(TR38, TR38_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR38, TR38_02, 23, 23, "l", DT.Rows(0)(22).ToString())
                RH.AddColumn(TR38, TR38_03, 3, 3, "l", "")
                RH.AddColumn(TR38, TR38_04, 3, 3, "l", "")
                RH.AddColumn(TR38, TR38_05, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR38, TR38_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR38, TR38_07, 23, 23, "l", DT.Rows(0)(23).ToString())
                TR38_00.BackColor = Drawing.Color.WhiteSmoke
                TR38_01.BackColor = Drawing.Color.WhiteSmoke
                TR38_02.BackColor = Drawing.Color.WhiteSmoke
                TR38_05.BackColor = Drawing.Color.WhiteSmoke
                TR38_06.BackColor = Drawing.Color.WhiteSmoke
                TR38_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR38)

                RH.DrawLine(tb, 100)
                Dim TR00 As New TableRow
                TR00.BackColor = Drawing.Color.LightSlateGray
                Dim TR00_00 As New TableCell
                RH.AddColumn(TR00, TR00_00, 100, 100, "c", "HO APPROVED DETAILS")
                TR00_00.Font.Bold = True
                TR00_00.Font.Size = 20
                TR00_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR00)

                RH.DrawLine(tb, 100)

                Dim TR39_00, TR39_01, TR39_02, TR39_03, TR39_04, TR39_05, TR39_06, TR39_07 As New TableCell
                RH.AddColumn(TR39, TR39_00, 23, 23, "l", "&nbsp;&nbsp;STATUS")
                RH.AddColumn(TR39, TR39_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR39, TR39_02, 23, 23, "l", DT.Rows(0)(24).ToString())
                RH.AddColumn(TR39, TR39_03, 3, 3, "l", "")
                RH.AddColumn(TR39, TR39_04, 3, 3, "l", "")
                RH.AddColumn(TR39, TR39_05, 23, 23, "l", "&nbsp;&nbsp;UPDATED BY")
                RH.AddColumn(TR39, TR39_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR39, TR39_07, 23, 23, "l", DT.Rows(0)(25).ToString())
                TR39_00.BackColor = Drawing.Color.WhiteSmoke
                TR39_01.BackColor = Drawing.Color.WhiteSmoke
                TR39_02.BackColor = Drawing.Color.WhiteSmoke
                TR39_05.BackColor = Drawing.Color.WhiteSmoke
                TR39_06.BackColor = Drawing.Color.WhiteSmoke
                TR39_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR39)

                Dim TR40_00, TR40_01, TR40_02, TR40_03, TR40_04, TR40_05, TR40_06, TR40_07 As New TableCell
                RH.AddColumn(TR40, TR40_00, 23, 23, "l", "&nbsp;&nbsp;UPDATED ON")
                RH.AddColumn(TR40, TR40_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR40, TR40_02, 23, 23, "l", DT.Rows(0)(27).ToString())
                RH.AddColumn(TR40, TR40_03, 3, 3, "l", "")
                RH.AddColumn(TR40, TR40_04, 3, 3, "l", "")
                RH.AddColumn(TR40, TR40_05, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR40, TR40_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR40, TR40_07, 23, 23, "l", DT.Rows(0)(28).ToString())
                TR40_00.BackColor = Drawing.Color.WhiteSmoke
                TR40_01.BackColor = Drawing.Color.WhiteSmoke
                TR40_02.BackColor = Drawing.Color.WhiteSmoke
                TR40_05.BackColor = Drawing.Color.WhiteSmoke
                TR40_06.BackColor = Drawing.Color.WhiteSmoke
                TR40_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR40)


                Dim TR42_00, TR42_01, TR42_02, TR42_03, TR42_04, TR42_05, TR42_06, TR42_07 As New TableCell
                RH.AddColumn(TR42, TR42_00, 23, 23, "l", "&nbsp;&nbsp;MAIL SEND DATE FOR PROCESSING")
                RH.AddColumn(TR42, TR42_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR42, TR42_02, 23, 23, "l", DT.Rows(0)(27).ToString())
                RH.AddColumn(TR42, TR42_03, 3, 3, "l", "")
                RH.AddColumn(TR42, TR42_04, 3, 3, "l", "")
                RH.AddColumn(TR42, TR42_05, 23, 23, "l", "&nbsp;&nbsp;")
                RH.AddColumn(TR42, TR42_06, 1, 1, "c", "&nbsp;&nbsp;")
                RH.AddColumn(TR42, TR42_07, 23, 23, "l", "")
                TR42_00.BackColor = Drawing.Color.WhiteSmoke
                TR42_01.BackColor = Drawing.Color.WhiteSmoke
                TR42_02.BackColor = Drawing.Color.WhiteSmoke
                TR42_05.BackColor = Drawing.Color.WhiteSmoke
                TR42_06.BackColor = Drawing.Color.WhiteSmoke
                TR42_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR42)


                RH.DrawLine(tb, 100)


                Dim TR51_00, TR51_01, TR51_02, TR51_03, TR51_04, TR51_05, TR51_06, TR51_07 As New TableCell
                RH.AddColumn(TR59, TR51_00, 23, 23, "l", "&nbsp;&nbsp;Status (Profile)")
                RH.AddColumn(TR59, TR51_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR59, TR51_02, 23, 23, "l", DT.Rows(0)(35).ToString())
                RH.AddColumn(TR59, TR51_03, 3, 3, "l", "")
                RH.AddColumn(TR59, TR51_04, 3, 3, "l", "")
                RH.AddColumn(TR59, TR51_05, 23, 23, "l", "&nbsp;&nbsp;Closed Remarks")
                RH.AddColumn(TR59, TR51_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR59, TR51_07, 23, 23, "l", DT.Rows(0)(34).ToString())
                TR51_00.BackColor = Drawing.Color.WhiteSmoke
                TR51_01.BackColor = Drawing.Color.WhiteSmoke
                TR51_02.BackColor = Drawing.Color.WhiteSmoke
                TR51_05.BackColor = Drawing.Color.WhiteSmoke
                TR51_06.BackColor = Drawing.Color.WhiteSmoke
                TR51_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR59)

                Dim TR52_00, TR52_01, TR52_02, TR52_03, TR52_04, TR52_05, TR52_06, TR52_07 As New TableCell
                RH.AddColumn(TR61, TR52_00, 23, 23, "l", "&nbsp;&nbsp;Closed On")
                RH.AddColumn(TR61, TR52_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR61, TR52_02, 23, 23, "l", DT.Rows(0)(39).ToString())
                RH.AddColumn(TR61, TR52_03, 3, 3, "l", "")
                RH.AddColumn(TR61, TR52_04, 3, 3, "l", "")
                RH.AddColumn(TR61, TR52_05, 23, 23, "l", "")
                RH.AddColumn(TR61, TR52_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR61, TR52_07, 23, 23, "l", "")
                TR52_00.BackColor = Drawing.Color.WhiteSmoke
                TR52_01.BackColor = Drawing.Color.WhiteSmoke
                TR52_02.BackColor = Drawing.Color.WhiteSmoke
                TR52_05.BackColor = Drawing.Color.WhiteSmoke
                TR52_06.BackColor = Drawing.Color.WhiteSmoke
                TR52_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR61)


                RH.DrawLine(tb, 100)
            End If
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
        
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
