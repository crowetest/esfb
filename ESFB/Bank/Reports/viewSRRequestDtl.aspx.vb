﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewSRRequestDtl
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim App_request_id As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            App_request_id = CInt(GF.Decrypt(Request.QueryString.Get("App_request_id")))
            hdnReportID.Value = App_request_id.ToString()

            Dim EmpCode As Integer
            Dim EmpName As String

            Dim DT As New DataTable
            Dim DT_Sum As New DataTable
            'Session("GraphType") = "RangeColumn"
            Dim TR22 As New TableRow
            Dim TR55 As New TableRow
            Dim TR66 As New TableRow
            Dim TR77 As New TableRow
            Dim TR88 As New TableRow
            Dim TR51 As New TableRow
            Dim TR99 As New TableRow
            Dim TR30, TR31, TR32, TR33, TR34, TR35, TR36, TR37, TR38, TR39, TR40, TR41, TR42, TR43, TR59, TR60 As New TableRow


            DT = DB.ExecuteDataSet("select upper(c.request_no) as REQUEST_NO,upper(c.SR_TICKET_no) as SR_REQUEST_NO,c.created_by as EMP_CODE,upper(c.created_Name) as EMP_NAME,cug as mobile,lower(c.mail_id) as Email, " & _
            " case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESMACO' else 'LBS' end,upper(branch_name), " & _
            " upper(j.app_name) as APPLICATION,upper(e.App_Type_name) as TYPE, upper(issue),upper(proposed_solution), upper(comments) as REMARKS,c.created_on  as REQUEST_DATE," & _
            " CASE WHEN (c.Approved_status is null) then 'Pending' WHEN (c.Approved_status =1) then 'Approved' WHEN (c.Approved_status =2) then 'Rejected' else '' end, " & _
            " approved_name +' - '+convert(varchar,approved_by),c.approved_on ,upper(approved_remarks), " & _
             " CASE WHEN (c.HO_Approved_status is null and Approved_status=1) then 'Pending' WHEN (c.HO_Approved_status=2 and Approved_status=1) then 'Rejected' WHEN (c.HO_Approved_status=1 and Approved_status=1) then 'Approved' else '' end, " & _
            " ho_approved_name +' - '+convert(varchar,ho_approved_by),c.ho_approved_on ,upper(ho_approved_remarks),mail_send_on,closed_status_on,closed_status,g.requestid,g.val from ESFB.dbo.sr_request_master c left join DMS_ESFB.dbo.SR_Attachment g on c.sr_request_id=g.requestid,ESFB.dbo.sr_app_master j,   ESFB.dbo.sr_app_type_master e   " & _
            " where     c.App_type_id=e.App_type_id  and c.App_id=j.App_id and c.sr_request_id=" & App_request_id.ToString & "").Tables(0)
            RH.Heading(CStr(Session("FirmName")), tb, "SERVICE REQUEST DEATILED REPORT", 100)

            Dim DR As DataRow
            If DT.Rows.Count > 0 Then



                Dim TR55_00, TR55_01, TR55_02, TR55_03, TR55_04, TR55_05, TR55_06, TR55_07 As New TableCell
                RH.AddColumn(TR55, TR55_00, 23, 23, "l", "&nbsp;&nbsp;SERVICE REQUEST NO")
                RH.AddColumn(TR55, TR55_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR55, TR55_02, 23, 23, "l", DT.Rows(0)(0).ToString())
                RH.AddColumn(TR55, TR55_03, 3, 3, "l", "")
                RH.AddColumn(TR55, TR55_04, 3, 3, "l", "")
                RH.AddColumn(TR55, TR55_05, 23, 23, "l", "&nbsp;&nbsp;REQUEST NO")
                RH.AddColumn(TR55, TR55_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR55, TR55_07, 23, 23, "l", IIf(DT.Rows(0)(26).ToString() <> "", "<a href='ShowAttachment.aspx?RequestID=" + GF.Encrypt(DT.Rows(0)(25).ToString()) + " &RptID =1' target='_blank' title ='Download Attachments'>" + DT.Rows(0)(1).ToString() + "</a>", DT.Rows(0)(1).ToString()))
                tb.Controls.Add(TR55)
                TR55_00.BackColor = Drawing.Color.WhiteSmoke
                TR55_01.BackColor = Drawing.Color.WhiteSmoke
                TR55_02.BackColor = Drawing.Color.WhiteSmoke
                TR55_05.BackColor = Drawing.Color.WhiteSmoke
                TR55_06.BackColor = Drawing.Color.WhiteSmoke
                TR55_07.BackColor = Drawing.Color.WhiteSmoke
                RH.BlankRow(tb, 1)
                Dim TR66_00, TR66_01, TR66_02, TR66_03, TR66_04, TR66_05, TR66_06, TR66_07 As New TableCell
                RH.AddColumn(TR66, TR66_00, 23, 23, "l", "&nbsp;&nbsp;EMP CODE")
                RH.AddColumn(TR66, TR66_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR66, TR66_02, 23, 23, "l", DT.Rows(0)(2).ToString())
                RH.AddColumn(TR66, TR66_03, 3, 3, "l", "")
                RH.AddColumn(TR66, TR66_04, 3, 3, "l", "")
                RH.AddColumn(TR66, TR66_05, 23, 23, "l", "&nbsp;&nbsp;NAME")
                RH.AddColumn(TR66, TR66_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR66, TR66_07, 23, 23, "l", DT.Rows(0)(3).ToString())
                TR66_00.BackColor = Drawing.Color.WhiteSmoke
                TR66_01.BackColor = Drawing.Color.WhiteSmoke
                TR66_02.BackColor = Drawing.Color.WhiteSmoke
                TR66_05.BackColor = Drawing.Color.WhiteSmoke
                TR66_06.BackColor = Drawing.Color.WhiteSmoke
                TR66_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR66)

                Dim TR77_00, TR77_01, TR77_02, TR77_03, TR77_04, TR77_05, TR77_06, TR77_07 As New TableCell
                RH.AddColumn(TR77, TR77_00, 23, 23, "l", "&nbsp;&nbsp;MOBILE")
                RH.AddColumn(TR77, TR77_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR77, TR77_02, 23, 23, "l", DT.Rows(0)(4).ToString())
                RH.AddColumn(TR77, TR77_03, 3, 3, "l", "")
                RH.AddColumn(TR77, TR77_04, 3, 3, "l", "")
                RH.AddColumn(TR77, TR77_05, 23, 23, "l", "&nbsp;&nbsp;MAIL_ID")
                RH.AddColumn(TR77, TR77_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR77, TR77_07, 23, 23, "l", DT.Rows(0)(5).ToString())
                TR77_00.BackColor = Drawing.Color.WhiteSmoke
                TR77_01.BackColor = Drawing.Color.WhiteSmoke
                TR77_02.BackColor = Drawing.Color.WhiteSmoke
                TR77_05.BackColor = Drawing.Color.WhiteSmoke
                TR77_06.BackColor = Drawing.Color.WhiteSmoke
                TR77_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR77)

                
              

                Dim TR30_00, TR30_01, TR30_02, TR30_03, TR30_04, TR30_05, TR30_06, TR30_07 As New TableCell
                RH.AddColumn(TR30, TR30_00, 23, 23, "l", "&nbsp;&nbsp;ENTITY")
                RH.AddColumn(TR30, TR30_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR30, TR30_02, 23, 23, "l", DT.Rows(0)(6).ToString())
                RH.AddColumn(TR30, TR30_03, 3, 3, "l", "")
                RH.AddColumn(TR30, TR30_04, 3, 3, "l", "")
                RH.AddColumn(TR30, TR30_05, 23, 23, "l", "&nbsp;&nbsp;BRANCH")
                RH.AddColumn(TR30, TR30_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR30, TR30_07, 23, 23, "l", DT.Rows(0)(7).ToString())
                TR30_00.BackColor = Drawing.Color.WhiteSmoke
                TR30_01.BackColor = Drawing.Color.WhiteSmoke
                TR30_02.BackColor = Drawing.Color.WhiteSmoke
                TR30_05.BackColor = Drawing.Color.WhiteSmoke
                TR30_06.BackColor = Drawing.Color.WhiteSmoke
                TR30_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR30)

                Dim TR31_00, TR31_01, TR31_02, TR31_03, TR31_04, TR31_05, TR31_06, TR31_07 As New TableCell
                RH.AddColumn(TR31, TR31_00, 23, 23, "l", "&nbsp;&nbsp;APPLICATION")
                RH.AddColumn(TR31, TR31_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR31, TR31_02, 23, 23, "l", DT.Rows(0)(8).ToString())
                RH.AddColumn(TR31, TR31_03, 3, 3, "l", "")
                RH.AddColumn(TR31, TR31_04, 3, 3, "l", "")
                RH.AddColumn(TR31, TR31_05, 23, 23, "l", "&nbsp;&nbsp;TYPE")
                RH.AddColumn(TR31, TR31_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR31, TR31_07, 23, 23, "l", DT.Rows(0)(9).ToString())
                TR31_00.BackColor = Drawing.Color.WhiteSmoke
                TR31_01.BackColor = Drawing.Color.WhiteSmoke
                TR31_02.BackColor = Drawing.Color.WhiteSmoke
                TR31_05.BackColor = Drawing.Color.WhiteSmoke
                TR31_06.BackColor = Drawing.Color.WhiteSmoke
                TR31_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR31)
               

               
               
                
                Dim TR36_00, TR36_01, TR36_02, TR36_03, TR36_04, TR36_05, TR36_06, TR36_07 As New TableCell
                RH.AddColumn(TR36, TR36_00, 23, 23, "l", "&nbsp;&nbsp;ISSUE")
                RH.AddColumn(TR36, TR36_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR36, TR36_02, 23, 23, "l", DT.Rows(0)(10).ToString())
                RH.AddColumn(TR36, TR36_03, 3, 3, "l", "")
                RH.AddColumn(TR36, TR36_04, 3, 3, "l", "")
                RH.AddColumn(TR36, TR36_05, 23, 23, "l", "&nbsp;&nbsp;PROPOSED SOLUTION")
                RH.AddColumn(TR36, TR36_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR36, TR36_07, 23, 23, "l", DT.Rows(0)(11).ToString())
                TR36_00.BackColor = Drawing.Color.WhiteSmoke
                TR36_01.BackColor = Drawing.Color.WhiteSmoke
                TR36_02.BackColor = Drawing.Color.WhiteSmoke
                TR36_05.BackColor = Drawing.Color.WhiteSmoke
                TR36_06.BackColor = Drawing.Color.WhiteSmoke
                TR36_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR36)

                Dim TR88_00, TR88_01, TR88_02, TR88_03, TR88_04, TR88_05, TR88_06, TR88_07 As New TableCell
                RH.AddColumn(TR88, TR88_00, 23, 23, "l", "&nbsp;&nbsp;COMMENTS")
                RH.AddColumn(TR88, TR88_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR88, TR88_02, 23, 23, "l", DT.Rows(0)(12).ToString())
                RH.AddColumn(TR88, TR88_03, 3, 3, "l", "")
                RH.AddColumn(TR88, TR88_04, 3, 3, "l", "")
                RH.AddColumn(TR88, TR88_05, 23, 23, "l", "&nbsp;&nbsp;REQUESTED_DATE")
                RH.AddColumn(TR88, TR88_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR88, TR88_07, 23, 23, "l", DT.Rows(0)(13).ToString())
                TR88_00.BackColor = Drawing.Color.WhiteSmoke
                TR88_01.BackColor = Drawing.Color.WhiteSmoke
                TR88_02.BackColor = Drawing.Color.WhiteSmoke
                TR88_05.BackColor = Drawing.Color.WhiteSmoke
                TR88_06.BackColor = Drawing.Color.WhiteSmoke
                TR88_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR88)

                RH.DrawLine(tb, 100)
                Dim TR0 As New TableRow
                TR0.BackColor = Drawing.Color.LightSlateGray
                Dim TR0_00 As New TableCell
                RH.AddColumn(TR0, TR0_00, 100, 100, "c", "HOD APPROVED DETAILS")
                tb.Controls.Add(TR0)
                TR0_00.Font.Bold = True
                TR0_00.Font.Size = 20
                TR0_00.ForeColor = Drawing.Color.White
                RH.DrawLine(tb, 100)

                Dim TR37_00, TR37_01, TR37_02, TR37_03, TR37_04, TR37_05, TR37_06, TR37_07 As New TableCell
                RH.AddColumn(TR37, TR37_00, 23, 23, "l", "&nbsp;&nbsp;STATUS")
                RH.AddColumn(TR37, TR37_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR37, TR37_02, 23, 23, "l", DT.Rows(0)(14).ToString())
                RH.AddColumn(TR37, TR37_03, 3, 3, "l", "")
                RH.AddColumn(TR37, TR37_04, 3, 3, "l", "")
                RH.AddColumn(TR37, TR37_05, 23, 23, "l", "&nbsp;&nbsp;UPDATED BY")
                RH.AddColumn(TR37, TR37_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR37, TR37_07, 23, 23, "l", DT.Rows(0)(15).ToString())
                TR37_00.BackColor = Drawing.Color.WhiteSmoke
                TR37_01.BackColor = Drawing.Color.WhiteSmoke
                TR37_02.BackColor = Drawing.Color.WhiteSmoke
                TR37_05.BackColor = Drawing.Color.WhiteSmoke
                TR37_06.BackColor = Drawing.Color.WhiteSmoke
                TR37_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR37)


                Dim TR38_00, TR38_01, TR38_02, TR38_03, TR38_04, TR38_05, TR38_06, TR38_07 As New TableCell
                RH.AddColumn(TR38, TR38_00, 23, 23, "l", "&nbsp;&nbsp;UPDATED ON")
                RH.AddColumn(TR38, TR38_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR38, TR38_02, 23, 23, "l", DT.Rows(0)(16).ToString())
                RH.AddColumn(TR38, TR38_03, 3, 3, "l", "")
                RH.AddColumn(TR38, TR38_04, 3, 3, "l", "")
                RH.AddColumn(TR38, TR38_05, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR38, TR38_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR38, TR38_07, 23, 23, "l", DT.Rows(0)(17).ToString())
                TR38_00.BackColor = Drawing.Color.WhiteSmoke
                TR38_01.BackColor = Drawing.Color.WhiteSmoke
                TR38_02.BackColor = Drawing.Color.WhiteSmoke
                TR38_05.BackColor = Drawing.Color.WhiteSmoke
                TR38_06.BackColor = Drawing.Color.WhiteSmoke
                TR38_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR38)

                RH.DrawLine(tb, 100)
                Dim TR00 As New TableRow
                TR00.BackColor = Drawing.Color.LightSlateGray
                Dim TR00_00 As New TableCell
                RH.AddColumn(TR00, TR00_00, 100, 100, "c", "HO APPROVED DETAILS")
                TR00_00.Font.Bold = True
                TR00_00.Font.Size = 20
                TR00_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR00)

                RH.DrawLine(tb, 100)

                Dim TR39_00, TR39_01, TR39_02, TR39_03, TR39_04, TR39_05, TR39_06, TR39_07 As New TableCell
                RH.AddColumn(TR39, TR39_00, 23, 23, "l", "&nbsp;&nbsp;STATUS")
                RH.AddColumn(TR39, TR39_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR39, TR39_02, 23, 23, "l", DT.Rows(0)(18).ToString())
                RH.AddColumn(TR39, TR39_03, 3, 3, "l", "")
                RH.AddColumn(TR39, TR39_04, 3, 3, "l", "")
                RH.AddColumn(TR39, TR39_05, 23, 23, "l", "&nbsp;&nbsp;UPDATED BY")
                RH.AddColumn(TR39, TR39_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR39, TR39_07, 23, 23, "l", DT.Rows(0)(19).ToString())
                TR39_00.BackColor = Drawing.Color.WhiteSmoke
                TR39_01.BackColor = Drawing.Color.WhiteSmoke
                TR39_02.BackColor = Drawing.Color.WhiteSmoke
                TR39_05.BackColor = Drawing.Color.WhiteSmoke
                TR39_06.BackColor = Drawing.Color.WhiteSmoke
                TR39_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR39)

                Dim TR40_00, TR40_01, TR40_02, TR40_03, TR40_04, TR40_05, TR40_06, TR40_07 As New TableCell
                RH.AddColumn(TR40, TR40_00, 23, 23, "l", "&nbsp;&nbsp;UPDATED ON")
                RH.AddColumn(TR40, TR40_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR40, TR40_02, 23, 23, "l", DT.Rows(0)(20).ToString())
                RH.AddColumn(TR40, TR40_03, 3, 3, "l", "")
                RH.AddColumn(TR40, TR40_04, 3, 3, "l", "")
                RH.AddColumn(TR40, TR40_05, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR40, TR40_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR40, TR40_07, 23, 23, "l", DT.Rows(0)(21).ToString())
                TR40_00.BackColor = Drawing.Color.WhiteSmoke
                TR40_01.BackColor = Drawing.Color.WhiteSmoke
                TR40_02.BackColor = Drawing.Color.WhiteSmoke
                TR40_05.BackColor = Drawing.Color.WhiteSmoke
                TR40_06.BackColor = Drawing.Color.WhiteSmoke
                TR40_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR40)


                Dim TR42_00, TR42_01, TR42_02, TR42_03, TR42_04, TR42_05, TR42_06, TR42_07 As New TableCell
                RH.AddColumn(TR42, TR42_00, 23, 23, "l", "&nbsp;&nbsp;MAIL SEND DATE FOR PROCESSING")
                RH.AddColumn(TR42, TR42_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR42, TR42_02, 23, 23, "l", DT.Rows(0)(22).ToString())
                RH.AddColumn(TR42, TR42_03, 3, 3, "l", "")
                RH.AddColumn(TR42, TR42_04, 3, 3, "l", "")
                RH.AddColumn(TR42, TR42_05, 23, 23, "l", "&nbsp;&nbsp;")
                RH.AddColumn(TR42, TR42_06, 1, 1, "c", "&nbsp;&nbsp;")
                RH.AddColumn(TR42, TR42_07, 23, 23, "l", "")
                TR42_00.BackColor = Drawing.Color.WhiteSmoke
                TR42_01.BackColor = Drawing.Color.WhiteSmoke
                TR42_02.BackColor = Drawing.Color.WhiteSmoke
                TR42_05.BackColor = Drawing.Color.WhiteSmoke
                TR42_06.BackColor = Drawing.Color.WhiteSmoke
                TR42_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR42)


                RH.DrawLine(tb, 100)

                Dim TR51_00, TR51_01, TR51_02, TR51_03, TR51_04, TR51_05, TR51_06, TR51_07 As New TableCell
                RH.AddColumn(TR59, TR51_00, 23, 23, "l", "&nbsp;&nbsp;Status")
                RH.AddColumn(TR59, TR51_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR59, TR51_02, 23, 23, "l", DT.Rows(0)(23).ToString())
                RH.AddColumn(TR59, TR51_03, 3, 3, "l", "")
                RH.AddColumn(TR59, TR51_04, 3, 3, "l", "")
                RH.AddColumn(TR59, TR51_05, 23, 23, "l", "&nbsp;&nbsp;Closed Remarks")
                RH.AddColumn(TR59, TR51_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR59, TR51_07, 23, 23, "l", DT.Rows(0)(24).ToString())
                TR51_00.BackColor = Drawing.Color.WhiteSmoke
                TR51_01.BackColor = Drawing.Color.WhiteSmoke
                TR51_02.BackColor = Drawing.Color.WhiteSmoke
                TR51_05.BackColor = Drawing.Color.WhiteSmoke
                TR51_06.BackColor = Drawing.Color.WhiteSmoke
                TR51_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR59)

                RH.DrawLine(tb, 100)
            End If
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
