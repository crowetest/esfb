﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class HREmpExit
    Inherits System.Web.UI.Page

    Dim DT As New DataTable
    Dim GN As New GeneralFunctions

    Dim ReportID As Integer = 1
    Dim LocationID As Integer = 0
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1216) = False Then
                'If GN.FormAccess(CInt(Session("UserID")), 1133) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If




           


            Me.cmbType.Attributes.Add("onchange", "return TypeOnChange()")
            Me.Master.subtitle = "HR EXIT Report"
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub


End Class
