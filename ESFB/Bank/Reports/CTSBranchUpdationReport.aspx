﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CTSBranchUpdationReport.aspx.vb" Inherits="CTSBranchUpdationReport" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server"> 
<br />
 <table align="center" style="width:60%; text-align:center; margin:0px auto;">
 <tr>
         <td style="width:15%;">
                </td>
            <td style="width:15%;">
                Branch</td>
            <td  style="width:55%;">
                
                <asp:DropDownList ID="cmbBranch" runat="server" class="NormalText" Width="80%">
                <asp:ListItem>------Select------</asp:ListItem>
                </asp:DropDownList>
                
            </td>
            <td style="width:15%;">
            </td>
        </tr>
        <tr>
         <td style="width:15%;"><ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
                </td>
            <td style="width:15%;">
                Date</td>
            <td  style="width:55%;">
                <asp:TextBox ID="txtFrom" runat="server" class="NormalText" Width="60%" ReadOnly="true" ></asp:TextBox> 
                <asp:CalendarExtender ID="txtFrom_Extender" runat="server" TargetControlID="txtFrom" 
                    Format="dd MMM yyyy"></asp:CalendarExtender>
                
                
               
                </td>
                 <td style="width:15%"> <asp:HiddenField ID="hdnReportID" runat="server" />
                <asp:HiddenField ID="hdnPostID" runat="server" />
                 <asp:HiddenField ID="hdnLocationID" runat="server" />
                     <asp:HiddenField ID="hdnName" runat="server" />
                </td>
        </tr>
        
           <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="4">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()"  />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
       
      </table>
   
    <script language="javascript" type="text/javascript">

        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }

        function btnView_onclick() {
            var Branch = document.getElementById("<%= cmbBranch.ClientID %>").value;
            var From = document.getElementById("<%= txtFrom.ClientID %>").value;
            
            
            if (From == "")
            { alert("Select the Date"); document.getElementById("<%= txtFrom.ClientID %>").focus(); return false; }
           
           
            window.open("ViewCTSBranchwiseReport.aspx?BRANCHID=" + btoa(Branch) + "&BRANCHNAME=" + btoa(document.getElementById("<%= cmbBranch.ClientID %>").options[document.getElementById("<%= cmbBranch.ClientID %>").selectedIndex].text) + " &FROM=" + From, "_blank");

        }
        
    </script>
</asp:Content>

