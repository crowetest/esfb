﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewSRRequestStatus
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1202) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim DTT As New DataTable
            Dim StrEmpCode As String = GF.Decrypt(Request.QueryString.Get("Empcode"))
            Dim StrTicketNo As String = GF.Decrypt(Request.QueryString.Get("TicketNo"))
            Dim StrStatus As Integer = GF.Decrypt(Request.QueryString.Get("StatusID"))
            Dim StrBranch As Integer = GF.Decrypt(Request.QueryString.Get("BranchID"))
            Dim AppID As Integer = GF.Decrypt(Request.QueryString.Get("AppID"))
            Dim TypeID As Integer = GF.Decrypt(Request.QueryString.Get("TypeID"))
            Dim SRNo As String = GF.Decrypt(Request.QueryString.Get("SRNo"))


            Dim StrFromDate As String = ""
            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(Request.QueryString.Get("frmdate"))
            End If
            Dim StrToDate As String = ""
            If CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrToDate = CStr(Request.QueryString.Get("todate"))
            End If

            Dim Struser As String = Session("UserID").ToString()
            Dim StrSubQry As String = ""
            Dim StrSubQry1 As String = ""
            Dim StrQuery As String = " "
            If CStr(Request.QueryString.Get("frmdate")) <> "1" And CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrQuery += " and  DATEADD(day,DATEDIFF(day, 0,c.created_on ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
            End If


            If StrBranch <> -1 Then
                StrQuery += " and c.branch_id=" & StrBranch & ""
            End If
            If TypeID <> -1 Then
                StrQuery += " and c.app_type_id =" & TypeID & ""
            End If
            If AppID <> -1 Then
                StrQuery += " and c.app_id =" & AppID & ""
            End If

            Dim strTeamQry As String = Nothing


            If StrStatus = 1 Then
                StrQuery += " and k.groupid =1"
            ElseIf StrStatus = 2 Then
                StrQuery += " and k.groupid =2"

            ElseIf StrStatus = 3 Then '--Approved
                StrQuery += " and ((approved_by =" & Session("UserID") & " and approved_status=1) or (HO_approved_by =" & Session("UserID") & " and ho_approved_status=1))  "
            ElseIf StrStatus = 4 Then  '--Rejected
                StrQuery += " and ((approved_by =" & Session("UserID") & " and approved_status=2) or (HO_approved_by =" & Session("UserID") & " and ho_approved_status=2))  "
            End If
            'End If
            If StrTicketNo <> "" Then
                StrQuery += " and c.request_NO  ='" & StrTicketNo & "'"
            End If

            If SRNo <> "" Then
                StrQuery += " and c.service_request_NO  ='" & SRNo & "'"
            End If
            If StrEmpCode <> "" Then
                StrQuery += " and c.created_by  ='" & StrEmpCode & "'"
            End If

            Dim SqlStr As String


            'StrFromDate    check x.emp_name
            SqlStr = "select upper(c.request_no) as REQUEST_NO,convert(varchar,c.created_on,106)  as REQUEST_DATE,upper(c.branch_name) as  BRANCH,DEPARTMENT_NAME,c.created_by as EMP_CODE,  " & _
                        " upper(c.created_Name) as EMP_NAME,upper(j.app_name) as APPLICATION,upper(e.App_Type_name) as TYPE ,cug as mobile,  " & _
                         " lower(c.mail_id) as Email, issue,proposed_solution, upper(comments) as REMARKS,upper(CASE WHEN (c.Approved_status is null) THEN 'HOD Pending'   " & _
                         "  when ((c.approved_status=1 and c.HO_Approved_status is null)) THEN  'HO level Approval Pending'  " & _
                         " WHEN  (c.Approved_status =2) then 'Rejected By Reporting Person'   WHEN  (c.HO_Approved_status =2) then 'Rejected By HO'  WHEN  (c.HO_Approved_status =1)  " & _
                         " then 'Approved By HO'   ELSE '' end ) AS NEXT_APPROVAL,upper(CASE WHEN (c.Approved_status is null) THEN ''  when (c.HO_Approved_status is null) THEN   " & _
                         " c.approved_remarks   WHEN  (c.Approved_status =2) then c.approved_remarks    WHEN  (c.HO_Approved_status =2) then c.HO_approved_remarks  WHEN   " & _
                         " (c.HO_Approved_status =1) then c.HO_approved_remarks   ELSE '' end ) AS PREVREMARKS,case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESMACO' else 'LBS' end,c.sr_request_id,g.val " & _
                         ",ISNULL(C.URLS_REQUIRED,'') AS URL from  ESFB.dbo.sr_request_master c left join DMS_ESFB.dbo.SR_Attachment g on c.sr_request_id=g.requestid,ESFB.dbo.sr_app_master j,   ESFB.dbo.sr_app_type_master e   " & _
                         " where     c.App_type_id=e.App_type_id  and c.App_id=j.App_id" & StrQuery & "  "


            'SqlStr += "   order by a.REQUEST_ID"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim b As Integer = DT.Rows.Count
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RH.Heading(Session("FirmName"), tb, "SERVICE REQUEST STATUS ", 150)

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            '
            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            Dim StrUserNam As String = Nothing

            DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
            If DTT.Rows.Count > 0 Then
                StrUserNam = DTT.Rows(0)(0)
            End If
            Dim TRHead_1 As New TableRow

            RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)


            RH.BlankRow(tb, 4)
            tb.Controls.Add(TRSHead)



            'TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"
            TRHead_1_08.BorderWidth = "1"
            TRHead_1_09.BorderWidth = "1"
            TRHead_1_10.BorderWidth = "1"
            TRHead_1_11.BorderWidth = "1"
            TRHead_1_12.BorderWidth = "1"
            TRHead_1_13.BorderWidth = "1"
            TRHead_1_14.BorderWidth = "1"
           

            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver
            TRHead_1_08.BorderColor = Drawing.Color.Silver
            TRHead_1_09.BorderColor = Drawing.Color.Silver
            TRHead_1_10.BorderColor = Drawing.Color.Silver
            TRHead_1_11.BorderColor = Drawing.Color.Silver
            TRHead_1_12.BorderColor = Drawing.Color.Silver
            TRHead_1_13.BorderColor = Drawing.Color.Silver
            TRHead_1_14.BorderColor = Drawing.Color.Silver
       
            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid
            TRHead_1_08.BorderStyle = BorderStyle.Solid
            TRHead_1_09.BorderStyle = BorderStyle.Solid
            TRHead_1_10.BorderStyle = BorderStyle.Solid
            TRHead_1_11.BorderStyle = BorderStyle.Solid
            TRHead_1_12.BorderStyle = BorderStyle.Solid
            TRHead_1_13.BorderStyle = BorderStyle.Solid
            TRHead_1_14.BorderStyle = BorderStyle.Solid
        


            RH.AddColumn(TRHead_1, TRHead_1_00, 5, 5, "l", "Request No") '
            RH.AddColumn(TRHead_1, TRHead_1_01, 5, 5, "l", "Request Date")
            RH.AddColumn(TRHead_1, TRHead_1_02, 5, 5, "l", "Request From")
            RH.AddColumn(TRHead_1, TRHead_1_03, 5, 5, "l", "Branch_name")
            RH.AddColumn(TRHead_1, TRHead_1_04, 4, 4, "c", "Emp_code")
            RH.AddColumn(TRHead_1, TRHead_1_05, 8, 8, "l", "Emp Name")
            RH.AddColumn(TRHead_1, TRHead_1_06, 8, 8, "l", "Category")

            RH.AddColumn(TRHead_1, TRHead_1_07, 6, 6, "l", "Request Type")

            RH.AddColumn(TRHead_1, TRHead_1_08, 5, 5, "l", "Mobile")

            RH.AddColumn(TRHead_1, TRHead_1_09, 5, 5, "l", "Requested e-mail")
            RH.AddColumn(TRHead_1, TRHead_1_10, 8, 8, "l", "Issue")
            RH.AddColumn(TRHead_1, TRHead_1_11, 8, 8, "l", "Proposed Solution")
            RH.AddColumn(TRHead_1, TRHead_1_12, 10, 10, "l", "Comments")
            RH.AddColumn(TRHead_1, TRHead_1_13, 9, 9, "l", "Status")
            RH.AddColumn(TRHead_1, TRHead_1_14, 4, 4, "l", "View Attachment")
          

            tb.Controls.Add(TRHead_1)

            For Each DR In DT.Rows

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"



                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid

                'RH.AddColumn(TR3, TR3_00, 8, 8, "l", " <a href='viewTicketDetails.aspx?RequestID=" + GF.Encrypt(DR(12).ToString) + "' target='_blank'>" + DR(0).ToString())




                RH.AddColumn(TR3, TR3_00, 5, 5, "l", "<a href='ViewSRRequestDtl.aspx?App_Request_ID=" + GF.Encrypt(DR(16).ToString()) + "' target='_blank'>" + DR(0).ToString + "</a>")

                RH.AddColumn(TR3, TR3_01, 5, 5, "l", CDate(DR(1)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_02, 5, 5, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_03, 5, 5, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_04, 4, 4, "c", DR(4).ToString())
                RH.AddColumn(TR3, TR3_05, 8, 8, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_06, 8, 8, "l", DR(6).ToString())

                RH.AddColumn(TR3, TR3_07, 6, 6, "l", DR(7).ToString())

                RH.AddColumn(TR3, TR3_08, 5, 5, "l", DR(8).ToString())



                RH.AddColumn(TR3, TR3_09, 5, 5, "l", DR(9).ToString())
                RH.AddColumn(TR3, TR3_10, 8, 8, "l", DR(10).ToString())
                RH.AddColumn(TR3, TR3_11, 8, 8, "l", DR(11).ToString())
                RH.AddColumn(TR3, TR3_12, 10, 10, "l", DR(12).ToString())

                RH.AddColumn(TR3, TR3_13, 9, 9, "l", DR(13).ToString())
                RH.AddColumn(TR3, TR3_14, 4, 4, "l", IIf(DR(14).ToString <> "", "<a href='ShowAttachment.aspx?RequestID=" + GF.Encrypt(DR(16).ToString()) + " &RptID =1' target='_blank' title ='Download Attachments'><img id='ViewReport' src='../../Image/attchment2.png'  style='height:20px; width:20px;  cursor:pointer;'/> </a>", ""))


                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)

        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        'Try
        WebTools.ExporttoExcel(DT, "Request")
        'Catch ex As Exception
        '    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
        '    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        'End Try
    End Sub
End Class
