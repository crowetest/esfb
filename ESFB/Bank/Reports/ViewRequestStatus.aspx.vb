﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewRequestStatus
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1142) = False And GF.FormAccess(CInt(Session("UserID")), 1243) = False And GF.FormAccess(CInt(Session("UserID")), 1244) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim DTT As New DataTable
            Dim StrEmpCode As String = GF.Decrypt(Request.QueryString.Get("Empcode"))
            Dim StrTicketNo As String = GF.Decrypt(Request.QueryString.Get("TicketNo"))
            Dim StrStatus As Integer = IIf(GF.Decrypt(Request.QueryString.Get("StatusID")) = "", 0, GF.Decrypt(Request.QueryString.Get("StatusID")))
            Dim StrBranch As Integer = GF.Decrypt(Request.QueryString.Get("BranchID"))
            Dim AppID As Integer = GF.Decrypt(Request.QueryString.Get("AppID"))
            Dim TypeID As Integer = GF.Decrypt(Request.QueryString.Get("TypeID"))
            Dim SRNo As String = GF.Decrypt(Request.QueryString.Get("SRNo"))

            Dim Previous As Integer = GF.Decrypt(Request.QueryString.Get("Previous"))
            Dim RptStaus As Integer = IIf(GF.Decrypt(Request.QueryString.Get("RptStaus")) = "", 1, GF.Decrypt(Request.QueryString.Get("RptStaus")))

            Dim StrFromDate As String = ""
            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(Request.QueryString.Get("frmdate"))
            End If
            Dim StrToDate As String = ""
            If CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrToDate = CStr(Request.QueryString.Get("todate"))
            End If

            Dim Struser As String = Session("UserID").ToString()
            Dim StrSubQry As String = ""
            Dim StrSubQry1 As String = ""
            Dim StrQuery As String = " "
            If CStr(Request.QueryString.Get("frmdate")) <> "1" And CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrQuery += " and  DATEADD(day,DATEDIFF(day, 0,c.created_on ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
            End If

            If StrBranch <> -1 Then 'Branch Wise Search
                StrQuery += " and c.created_branch=" & StrBranch & ""
            End If
            If TypeID <> -1 Then 'Application User Type
                StrQuery += " and c.app_type_id =" & TypeID & ""
            End If
            If AppID <> -1 Then 'Application Name   
                If (StrStatus = 3) Then
                    StrQuery += " and c.app_id =" & AppID & " and c.app_type_id not in(9,22,35)"
                Else
                    StrQuery += " and c.app_id =" & AppID & ""
                End If
            End If

            Dim strTeamQry As String = Nothing


            If RptStaus = 1 Then 'Listed Users Level
                If StrStatus = 1 Then '-- Reporting Officer Level Pending
                    StrQuery += " and c.approved_status is null"
                ElseIf StrStatus = 2 Then  '-- HO Level Pending
                    StrQuery += " and c.approved_status=1 and ho_approved_status is null"
                ElseIf StrStatus = 3 Then  '-- UAM Level Pending
                    StrQuery += " and c.ho_approved_status=1 and closed_status is null"
                End If
            ElseIf RptStaus = 2 Then 'Branch User Level
                StrQuery += " and c.db_id=1 "
                If StrStatus = 1 Then '-- Reporting Officer Level Pending
                    StrQuery += " and c.approved_status is null and c.branch_id=" & CInt(Session("BranchID")) & "  and c.db_id=1"
                ElseIf StrStatus = 2 Then  '-- HO Level Pending
                    StrQuery += " and c.approved_status=1 and c.ho_approved_status is null and c.branch_id=" & CInt(Session("BranchID")) & "  and c.db_id=1"
                ElseIf StrStatus = 3 Then  '-- UAM Level Pending
                    StrQuery += " and c.ho_approved_status=1 and c.closed_status is null and c.branch_id=" & CInt(Session("BranchID")) & "  and c.db_id=1"
                End If
            Else ' HO Level
                StrQuery += " and c.db_id=1 "
                If StrStatus = 2 Then  '-- HO Level Pending
                    StrQuery += " and c.approved_status=1 and c.ho_approved_status is null and c.db_id=1"
                ElseIf StrStatus = 3 Then  '-- UAM Level Pending
                    StrQuery += " and c.ho_approved_status=1 and c.closed_status is null and c.db_id=1"
                End If
            End If

            'TODO // Form - HO - Status 2 and 3 Only // 

            'End If
            If StrTicketNo <> "" Then
                StrQuery += " and c.request_NO  ='" & StrTicketNo & "'"
            End If

            If SRNo <> "" Then
                StrQuery += " and c.service_request_NO  ='" & SRNo & "'"
            End If

            If StrEmpCode <> "" Then
                If Previous = 1 Then
                    StrQuery += " and c.previous_staff  ='" & StrEmpCode & "'"
                Else
                    If RptStaus = 2 Then
                        StrQuery += " and c.emp_code  ='" & CInt(Session("UserID")) & "'"
                    Else
                        StrQuery += " and c.emp_code  ='" & StrEmpCode & "'"
                    End If
                End If
            End If

            Dim SqlStr As String

            'StrFromDate    check x.emp_name
            'SqlStr = "select upper(c.request_no) as REQUEST_NO,convert(varchar,c.created_on,106)  as REQUEST_DATE,upper(c.created_branch_name) as  BRANCH,d.emp_code as EMP_CODE, upper(c.Emp_Name) as EMP_NAME,upper(j.app_name) as APPLICATION,upper(e.App_Type_name) as TYPE,upper(g.App_role_name) as ROLE ,d.mobile as mobile,lower(c.requestmailid) as Email, " & _
            '            " upper(Remarks) as REMARKS,upper(case when hr_status is null then CASE WHEN (k.order_id=0 and c.Approved_status is null) THEN 'Reporting level Pending - ' + isnull(c.reporting_NAME, '') +' ( ' + CONVERT(VARCHAR,c.reporting_to) +')' " & _
            '            " when (k.order_id>0 and (c.approved_status=1 and c.HO_Approved_status is null)) THEN  k.level_name +' - ' +  X.NAME +' ( ' + CONVERT(VARCHAR,k.order_id) +')'  " & _
            '            " WHEN  (k.order_id>=0 and c.Approved_status =2) then 'Rejected By Reporting Person'  " & _
            '            " WHEN  (k.order_id>0 and c.HO_Approved_status =2) then 'Rejected By HO'  WHEN  (k.order_id>0 and c.HO_Approved_status =1) then 'Approved By HO'  " & _
            '            " ELSE '' end  else case when approved_status is null then 'HR Head Approval Pending' when approved_status=1 then 'HR Head Approved' when approved_status=2 then 'HR Head Rejected' when (approved_status=1 and ho_approved_status is null) then 'HO level Pending -HR' when (approved_status=1 and ho_approved_status =1) then 'HO level Approved - HR'  when (approved_status=1 and ho_approved_status =2) then 'HO level Rejected - HR'  else '' end end )  AS NEXT_APPROVAL,upper(CASE WHEN (k.order_id=0 and c.Approved_status is null) THEN '' " & _
            '            " when (k.order_id>0 and c.HO_Approved_status is null) THEN  c.approved_remarks  " & _
            '            " WHEN  (k.order_id=0 and c.Approved_status =2) then c.approved_remarks   " & _
            '            " WHEN  (k.order_id>0 and c.HO_Approved_status =2) then c.HO_approved_remarks  WHEN  (k.order_id>0 and c.HO_Approved_status =1) then c.HO_approved_remarks  " & _
            '            " ELSE '' end ) AS PREVREMARKS,case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESMACO' else 'LBS' end,d.mail_id as Mail_To, convert(varchar,convert(date,c.closed_on),106) ,c.app_request_id,c.closed_remarks,case when c.closed_status=1 then 'Closed'  when c.closed_status=2 then 'Rejected' else '' end,c.previous_staff    from ESFB.dbo.app_level k  left join ESFB.dbo.app_level_email x on k.order_id=x.emp_code" & _
            '            " ,ESFB.dbo.app_request_master c ,ESFB.dbo.app_master j,  " & _
            '            " ESFB.dbo.app_type_master e ,     ESFB.dbo.app_dtl_profile d  " & _
            '            " left join ESFB.dbo.app_role_master  " & _
            '            " g on d.Role_id=g.App_role  where    k.app_id=j.App_id and c.App_type_id=e.App_type_id  " & _
            '            " and c.App_request_id=d.App_request_id  and c.level_id=k.level_id  " & StrQuery & " order by convert(date,c.Created_on),convert(date,c.HO_Approved_on)"

            'update by vidya for dl current level

            SqlStr = "select upper(c.request_no) as REQUEST_NO,convert(varchar,c.created_on,106)  as REQUEST_DATE,upper(c.created_branch_name) as  BRANCH,d.emp_code as EMP_CODE, upper(c.Emp_Name) as EMP_NAME,upper(j.app_name) as APPLICATION,upper(e.App_Type_name) as TYPE,upper(g.App_role_name) as ROLE ,d.mobile as mobile,lower(c.requestmailid) as Email, " & _
                        " upper(Remarks) as REMARKS,upper(case when hr_status is null then CASE WHEN (k.order_id=0 and c.Approved_status is null and c.app_type_id <> 36) THEN 'Reporting level Pending - ' + isnull(c.reporting_NAME, '') +' ( ' + CONVERT(VARCHAR,c.reporting_to) +')' " & _
                        " when (k.order_id = 0 and c.approved_status is null and c.app_type_id = 36) then 'Department level pending' when (k.order_id>0 and (c.approved_status=1 and c.HO_Approved_status is null)) THEN  k.level_name +' - ' +  X.NAME +' ( ' + CONVERT(VARCHAR,k.order_id) +')'  " & _
                        " WHEN  (k.order_id>=0 and c.Approved_status =2) then 'Rejected By Reporting Person'  " & _
                        " WHEN  (k.order_id>0 and c.HO_Approved_status =2) then 'Rejected By HO'  WHEN  (k.order_id>0 and c.HO_Approved_status =1) then 'Approved By HO'  " & _
                        " ELSE '' end  else case when (c.app_type_id <> 36 and approved_status is null) then 'HR Head Approval Pending' when (c.app_type_id = 36 and approved_status is null) then 'Department level pending' when approved_status=1 then 'HR Head Approved' when approved_status=2 then 'HR Head Rejected' when (approved_status=1 and ho_approved_status is null) then 'HO level Pending -HR' when (approved_status=1 and ho_approved_status =1) then 'HO level Approved - HR'  when (approved_status=1 and ho_approved_status =2) then 'HO level Rejected - HR'  else '' end end )  AS NEXT_APPROVAL,upper(CASE WHEN (k.order_id=0 and c.Approved_status is null) THEN '' " & _
                        " when (k.order_id>0 and c.HO_Approved_status is null) THEN  c.approved_remarks  " & _
                        " WHEN  (k.order_id=0 and c.Approved_status =2) then c.approved_remarks   " & _
                        " WHEN  (k.order_id>0 and c.HO_Approved_status =2) then c.HO_approved_remarks  WHEN  (k.order_id>0 and c.HO_Approved_status =1) then c.HO_approved_remarks  " & _
                        " ELSE '' end ) AS PREVREMARKS,case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESMACO' else 'LBS' end,d.mail_id as Mail_To, convert(varchar,convert(date,c.closed_on),106) ,c.app_request_id,c.closed_remarks,case when c.closed_status=1 then 'Closed'  when c.closed_status=2 then 'Rejected' else '' end,c.previous_staff    from ESFB.dbo.app_level k  left join ESFB.dbo.app_level_email x on k.order_id=x.emp_code" & _
                        " ,ESFB.dbo.app_request_master c ,ESFB.dbo.app_master j,  " & _
                        " ESFB.dbo.app_type_master e ,     ESFB.dbo.app_dtl_profile d  " & _
                        " left join ESFB.dbo.app_role_master  " & _
                        " g on d.Role_id=g.App_role  where    k.app_id=j.App_id and c.App_type_id=e.App_type_id  " & _
                        " and c.App_request_id=d.App_request_id  and c.level_id=k.level_id  " & StrQuery & " order by convert(date,c.Created_on),convert(date,c.HO_Approved_on)"


            'update by vidya end


            'SqlStr += "   order by a.REQUEST_ID"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim b As Integer = DT.Rows.Count
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RH.Heading(Session("FirmName"), tb, "REQUEST STATUS ", 150)

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            '
            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            Dim StrUserNam As String = Nothing

            DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
            If DTT.Rows.Count > 0 Then
                StrUserNam = DTT.Rows(0)(0)
            End If
            Dim TRHead_1 As New TableRow

            RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)


            RH.BlankRow(tb, 4)
            tb.Controls.Add(TRSHead)

            'TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHeadS_1_00, TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14, TRHead_1_15 As New TableCell

            TRHeadS_1_00.BorderWidth = "1"
            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"
            TRHead_1_08.BorderWidth = "1"
            TRHead_1_09.BorderWidth = "1"
            TRHead_1_10.BorderWidth = "1"
            TRHead_1_11.BorderWidth = "1"
            TRHead_1_12.BorderWidth = "1"
            TRHead_1_13.BorderWidth = "1"
            TRHead_1_14.BorderWidth = "1"
            TRHead_1_15.BorderWidth = "1"

            TRHeadS_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver
            TRHead_1_08.BorderColor = Drawing.Color.Silver
            TRHead_1_09.BorderColor = Drawing.Color.Silver
            TRHead_1_10.BorderColor = Drawing.Color.Silver
            TRHead_1_11.BorderColor = Drawing.Color.Silver
            TRHead_1_12.BorderColor = Drawing.Color.Silver
            TRHead_1_13.BorderColor = Drawing.Color.Silver
            TRHead_1_14.BorderColor = Drawing.Color.Silver
            TRHead_1_15.BorderColor = Drawing.Color.Silver

            TRHeadS_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid
            TRHead_1_08.BorderStyle = BorderStyle.Solid
            TRHead_1_09.BorderStyle = BorderStyle.Solid
            TRHead_1_10.BorderStyle = BorderStyle.Solid
            TRHead_1_11.BorderStyle = BorderStyle.Solid
            TRHead_1_12.BorderStyle = BorderStyle.Solid
            TRHead_1_13.BorderStyle = BorderStyle.Solid
            TRHead_1_14.BorderStyle = BorderStyle.Solid
            TRHead_1_15.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead_1, TRHeadS_1_00, 4, 4, "l", "Serial No")
            RH.AddColumn(TRHead_1, TRHead_1_00, 5, 5, "l", "Request No") '
            RH.AddColumn(TRHead_1, TRHead_1_01, 5, 5, "l", "Request Date")
            RH.AddColumn(TRHead_1, TRHead_1_11, 5, 5, "l", "Request From")
            RH.AddColumn(TRHead_1, TRHead_1_02, 5, 5, "l", "Branch_name")
            RH.AddColumn(TRHead_1, TRHead_1_03, 4, 4, "c", "Emp_code")
            RH.AddColumn(TRHead_1, TRHead_1_04, 10, 10, "l", "Emp Name")
            RH.AddColumn(TRHead_1, TRHead_1_05, 7, 7, "l", "Category")

            RH.AddColumn(TRHead_1, TRHead_1_06, 5, 5, "l", "Request Type")
            RH.AddColumn(TRHead_1, TRHead_1_07, 5, 5, "l", "Role")
            RH.AddColumn(TRHead_1, TRHead_1_08, 5, 5, "l", "Mobile")

            RH.AddColumn(TRHead_1, TRHead_1_09, 5, 5, "l", "Requested e-mail")
            RH.AddColumn(TRHead_1, TRHead_1_10, 8, 8, "l", "Description")
            RH.AddColumn(TRHead_1, TRHead_1_15, 9, 9, "l", "Status")
            RH.AddColumn(TRHead_1, TRHead_1_12, 4, 4, "l", "UAM Closed Date")
            RH.AddColumn(TRHead_1, TRHead_1_13, 5, 5, "l", "UAM Status")
            RH.AddColumn(TRHead_1, TRHead_1_14, 11, 11, "l", "UAM Remarks")

            tb.Controls.Add(TRHead_1)
            Dim I As Integer = 0

            For Each DR In DT.Rows
                I = I + 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3S_00, TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15 As New TableCell

                TR3S_00.BorderWidth = "1"
                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"
                TR3_15.BorderWidth = "1"

                TR3S_00.BorderColor = Drawing.Color.Silver
                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver
                TR3_15.BorderColor = Drawing.Color.Silver

                TR3S_00.BorderStyle = BorderStyle.Solid
                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid
                TR3_15.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3S_00, 4, 4, "c", I.ToString()) 'SlNO
                RH.AddColumn(TR3, TR3_00, 5, 5, "l", "<a href='ViewRequestDtl.aspx?App_Request_ID=" + GF.Encrypt(DR(16).ToString()) + "' target='_blank'>" + DR(0).ToString + "</a>")

                RH.AddColumn(TR3, TR3_01, 5, 5, "l", CDate(DR(1)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_11, 5, 5, "l", DR(13).ToString())
                RH.AddColumn(TR3, TR3_02, 5, 5, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_03, 4, 4, "c", DR(3).ToString())
                RH.AddColumn(TR3, TR3_04, 10, 10, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_05, 7, 7, "l", DR(5).ToString())

                RH.AddColumn(TR3, TR3_06, 5, 5, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_07, 5, 5, "l", DR(7).ToString())
                RH.AddColumn(TR3, TR3_08, 5, 5, "l", DR(8).ToString())



                RH.AddColumn(TR3, TR3_09, 5, 5, "l", DR(9).ToString())
                RH.AddColumn(TR3, TR3_10, 8, 8, "l", DR(10).ToString())

                RH.AddColumn(TR3, TR3_15, 9, 9, "l", DR(11).ToString())
                RH.AddColumn(TR3, TR3_12, 4, 4, "l", DR(15).ToString)
                RH.AddColumn(TR3, TR3_13, 5, 5, "l", DR(18).ToString())
                RH.AddColumn(TR3, TR3_14, 11, 11, "l", DR(17).ToString())


                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)

        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        'Try
        WebTools.ExporttoExcel(DT, "Request")
        'Catch ex As Exception
        '    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
        '    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        'End Try
    End Sub
End Class
