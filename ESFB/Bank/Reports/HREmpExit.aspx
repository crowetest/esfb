﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="HREmpExit.aspx.vb" Inherits="HREmpExit" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server"> 
    <br />
 <table align="center" style="width:60%; text-align:center; margin:0px auto;">
 <tr>
         <td style="width:15%;">
                </td>
            <td style="width:15%;">
                Branch</td>
            <td  style="width:55%;">
                
                <asp:DropDownList ID="cmbType" runat="server" class="NormalText" Width="80%">
                <asp:ListItem Value="-1">ALL</asp:ListItem>
                <asp:ListItem Value="1">Created On</asp:ListItem>
                <asp:ListItem Value="2">Approved On</asp:ListItem>
                <asp:ListItem Value="3">Relieved On</asp:ListItem>
                <asp:ListItem Value="4">Approval Pending</asp:ListItem>
                </asp:DropDownList>
                
            </td>
            <td style="width:15%;">
            </td>
        </tr>
      <tr id="From" style="display:none">
         <td style="width:15%;">
                </td>
            <td style="width:15%;">
                Date From</td>
            <td  style="width:55%;">
                <asp:TextBox ID="txtFrom" runat="server" class="NormalText" Width="60%" ReadOnly="true" ></asp:TextBox> 
                <asp:CalendarExtender ID="txtFrom_Extender" runat="server" TargetControlID="txtFrom" 
                    Format="dd MMM yyyy"></asp:CalendarExtender>
                
                
               
                </td>
                 <td style="width:15%"> <asp:HiddenField ID="HiddenField1" runat="server" />
                <asp:HiddenField ID="HiddenField2" runat="server" />
                 <asp:HiddenField ID="HiddenField3" runat="server" />
                     <asp:HiddenField ID="HiddenField4" runat="server" />
                </td>
        </tr>
        <tr id="To" style="display:none">
         <td style="width:15%;"><ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
                </td>
            <td style="width:15%;">
                Date To</td>
            <td  style="width:55%;">
                <asp:TextBox ID="txtTo" runat="server" class="NormalText" Width="60%" ReadOnly="true" ></asp:TextBox> 
                <asp:CalendarExtender ID="txtTo_Extender" runat="server" TargetControlID="txtTo" 
                    Format="dd MMM yyyy"></asp:CalendarExtender>
                
                
               
                </td>
                 <td style="width:15%"> <asp:HiddenField ID="hdnReportID" runat="server" />
                <asp:HiddenField ID="hdnPostID" runat="server" />
                 <asp:HiddenField ID="hdnLocationID" runat="server" />
                     <asp:HiddenField ID="hdnName" runat="server" />
                </td>
        </tr>
        
           <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="4">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()"  />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
       
      </table>
   
    <script language="javascript" type="text/javascript">

        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }

        function btnView_onclick() {
        
            var From = document.getElementById("<%= txtFrom.ClientID %>").value;
            var To = document.getElementById("<%= txtTo.ClientID%>").value;
            var Type = document.getElementById("<%= cmbType.ClientID%>").value;

            if (From != "" && To == "" && Type != "-1")
            { alert("Select the To Date"); document.getElementById("<%= txtTo.ClientID%>").focus(); return false; }
            if (From == "" && To != "" && Type != "-1")
            { alert("Select the To Date"); document.getElementById("<%= txtTo.ClientID%>").focus(); return false; }

           
           
            window.open("ViewHREmpExitReport.aspx?TypeID=" + btoa(Type) + "&FROM=" + From + "&TO=" + To, "_blank");

        }

        function TypeOnChange() {
            var Type = document.getElementById("<%= cmbType.ClientID%>").value;
            if (Type == "-1") {
                document.getElementById("From").style.display = "none";
                document.getElementById("To").style.display = "none";
            }
            else {
                document.getElementById("From").style.display = "";
                document.getElementById("To").style.display = "";
            }

        }
        
    </script>
</asp:Content>

