﻿Imports System.Data
Partial Class ShowAttachment
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RptID As Integer = 1
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim RequestID As Integer = CInt(GF.Decrypt(Request.QueryString.[Get]("RequestID")))
            If Not IsNothing(Request.QueryString.Get("RptID")) Then
                RptID = CInt(Request.QueryString.Get("RptID"))
            End If
            Dim dt As DataTable
            If RptID = 1 Then
                dt = DB.ExecuteDataSet("SELECT val,content_Type,isnull(case when filename='' then 'Attachment' else filename end,'Attachment') FileName FROM DMS_ESFB.dbo.SR_ATTACHMENT  where RequestId=" & RequestID & " ").Tables(0)
            Else 'CTS
                dt = DB.ExecuteDataSet("SELECT ATTACHMENT,content_Type,isnull(case when Attach_file_name='' then 'Attachment' else Attach_file_name end,'Attachment') FileName FROM DMS_ESFB.dbo.CTS_REQUEST_ATTACH where Request_Id=" & RequestID & " ").Tables(0)
            End If

            If dt IsNot Nothing Then
                If dt.Rows.Count > 0 Then
                    Dim bytes() As Byte = CType(dt.Rows(0)(0), Byte())
                    Response.Buffer = True
                    Response.Charset = ""
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.ContentType = dt.Rows(0)(1).ToString()
                    Response.AddHeader("content-disposition", "attachment;filename=" + dt.Rows(0)(2).ToString().Replace(" ", ""))
                    'Response.AddHeader("content-disposition", dt.Rows(0)(2).ToString()) ' ----------  OPenwith Dialogbox 
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()
                End If

            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected() Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
