﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CustomizeRequestStatusReport
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler

    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim RptStatus As Integer
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1142) = False And GF.FormAccess(CInt(Session("UserID")), 1243) = False And GF.FormAccess(CInt(Session("UserID")), 1244) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            hid_RptStaus.Value = CStr(Request.QueryString.Get("RptStatus"))

            Me.Master.subtitle = "Request Status Report"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            If CInt(Session("BranchID")) = 0 Then
                DT = DB.ExecuteDataSet("select '-1' as ID,'--------- Select ---------' as name union all select branch_id,branch_name from branch_master   order by 2").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select branch_id,branch_name from branch_master where branch_id= " & CInt(Session("BranchID")) & "   order by 2").Tables(0)
            End If

            GF.ComboFill(cmbBranch, DT, 0, 1)
            DT = DB.ExecuteDataSet("select '-1' as ID,'--------- Select ---------' as name union all select app_id,app_name from ESFB.DBO.app_master ").Tables(0)
            GF.ComboFill(cmbGroup, DT, 0, 1)

            DT = DB.ExecuteDataSet("select '-1' as ID,'--------- Select ---------' as name  union all select app_type_id,app_type_name from ESFB.DBO.app_type_master ").Tables(0)
            GF.ComboFill(cmbSubGroup, DT, 0, 1)

            If (CInt(hid_RptStaus.Value) = 3) Then
                DT = DB.ExecuteDataSet("select '-1' as ID,'ALL' union all select 2, 'HO Level Pending' union all select 3, 'UAM Level Pending'").Tables(0)
                GF.ComboFill(cmbStatus, DT, 0, 1)
            Else
                DT = DB.ExecuteDataSet("select '-1' as ID,'ALL' union all select 1, 'Reporting Officer Level Pending' union all select 2, 'HO Level Pending' union all select 3, 'UAM Level Pending'").Tables(0)
                GF.ComboFill(cmbStatus, DT, 0, 1)
            End If

            Me.cmbGroup.Attributes.Add("onchange", "return GroupOnChange()")
            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function


    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))

        If CInt(Data(0)) = 1 Then
            'If CInt(Data(1)) > 0 Then
            DT = DB.ExecuteDataSet("select '-1' as ID,'--------- Select ---------' as name union all select app_type_id,app_type_name from esfb.dbo.App_Type_master where app_id=" & Data(1).ToString & "").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            'End If
        End If
    End Sub

End Class
