﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="ViewStaffRequestCloseStatus.aspx.vb" Inherits="ViewStaffRequestCloseStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <br />
    <table align="center" style="width: 60%; text-align: center; margin: 0px auto;">
        <tr>
            <td style="width: 15%;">
                &nbsp;
            </td>
            <td style="width: 15%;">
                Search By
            </td>
            <td style="width: 55%;">
                <asp:DropDownList ID="cmbSearch" runat="server" Height="16px" Width="199px">
                    <asp:ListItem Value="0"> None</asp:ListItem>
                    <asp:ListItem Value="1"> Created By</asp:ListItem>
                    <asp:ListItem Value="2"> Closed By</asp:ListItem>
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td style="width: 15%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 15%;">
                &nbsp;
            </td>
            <td style="width: 15%;">
                Employee code
            </td>
            <td style="width: 55%;">
                <asp:TextBox ID="txtEmpcode" runat="server" Style="font-family: Cambria; font-size: 10pt;"
                    Width="50%" MaxLength="50" onkeypress='return NumericCheck(event)' />
            </td>
            <td style="width: 15%;">
            </td>
        </tr>
        <tr>
            <td style="width: 15%;">
                &nbsp;
            </td>
            <td style="width: 15%;">
                From Date
            </td>
            <td style="width: 55%;">
                <asp:TextBox ID="txtFrom" runat="server" class="NormalText" Width="60%" ReadOnly="true"></asp:TextBox>
                <asp:CalendarExtender ID="txtFrom_Extender" runat="server" TargetControlID="txtFrom" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
            <td style="width: 15%">
                <asp:HiddenField ID="hdnReportID" runat="server" />
                <asp:HiddenField ID="hdnPostID" runat="server" />
                <asp:HiddenField ID="hdnLocationID" runat="server" />
                <asp:HiddenField ID="hdnName" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width: 15%;">
                &nbsp;
            </td>
            <td style="width: 15%;">
                To Date
            </td>
            <td style="width: 55%;">
                <asp:TextBox ID="txtTo" runat="server" class="NormalText" Width="60%" ReadOnly="true"></asp:TextBox>
                <asp:CalendarExtender ID="txtTo_Extender" runat="server" TargetControlID="txtTo" Format="dd MMM yyyy" ></asp:CalendarExtender>
            </td>
            <td style="width: 15%">
            </td>
        </tr>
        <tr>
            <td style="width: 15%; height: 18px; text-align: center;" colspan="4">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                    value="VIEW" onclick="return btnView_onclick()" />&nbsp;&nbsp;<input id="btnExit"
                        style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT"
                        onclick="return btnExit_onclick()" />
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">

        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }

        function btnView_onclick() {
            var EmpCode = document.getElementById("<%= txtEmpcode.ClientID%>").value;
            var From = document.getElementById("<%= txtFrom.ClientID %>").value;
            if (From == "") {
                alert("Select From Date");
                return false;
            }
            var UserWise = document.getElementById("<%= cmbSearch.ClientID %>").value;
            var To = document.getElementById("<%= txtTo.ClientID %>").value;
            if (To == "") {
                alert("Select To Date");
                return false;
            }

            window.open("ViewStaffRequestClose_dtl.aspx?EmpCode=" + btoa(EmpCode) + "&From=" + From + "&To=" + To + "&SearchWise=" + UserWise, "_blank");
        }
        
    </script>
</asp:Content>
