﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Bank_Asset_UploadData.aspx.vb" Inherits="Asset_UploadAssetData" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
// <![CDATA[


        function btnExit_onclick() {
            window.open('../home.aspx','_self');
        }

// ]]>
    </script>
</head>
</html>
<br />
<div>
<table style="width: 70%; margin :0px auto;" >
                <tr>
                    <td style="width: 40%;text-align:right; font-size: 10pt;">
                        Upload File</td>
                   <td style="width: 1%;text-align:right;">
                        </td>
                    <td style="width: 40%;text-align:left;">
                        <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" />
                        </td>
                        <td style="width: 19%;text-align:right;">
                        </td>
                </tr>
                 <tr>
                    <td style="width: 40%;text-align:right;font-size: 10pt;">
                        &nbsp;</td>
                   <td style="width: 1%;text-align:right;">
                        </td>
                    <td style="width: 40%;text-align:left;">
                        &nbsp;</td>
                        <td style="width: 19%;text-align:right;">
                        </td>
                </tr>
               
                <tr>
                    <td style="text-align:center;" colspan="4">
                       <asp:Button ID="btnUpload" class="NormalText" runat="server"  Text="UPLOAD" />&nbsp;
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" 
                    type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
                   
                </tr>
            </table>
            <br />
    </div>
</asp:Content>

