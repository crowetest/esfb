﻿Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Partial Class Asset_UploadAssetData
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim fileLocation As String
#Region "Page Load & Dispose"
    Protected Sub MWF_UploadBrNetData_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Upload Asset Data"
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        File.Delete(fileLocation)
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Confirmation"
    Protected Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim connectionString As String = ""
        If Me.fup1.HasFile = False Then
            Dim cl_script0 As New System.Text.StringBuilder
            cl_script0.Append("         alert('Please browse excel');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
            Exit Sub
        Else
            Dim fileName As String = Path.GetFileName(fup1.PostedFile.FileName)
            Dim fileExtension As String = Path.GetExtension(fup1.PostedFile.FileName)
            fileLocation = Server.MapPath("../" & fileName)
            fup1.SaveAs(fileLocation)
            'Dim connectionString As String =[String].Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0 Xml;HDR=YES;IMEX=1;""", filepath)
            If fileExtension = ".xls" Then
                If Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE") = "x86" Then
                    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=YES;"""
                Else
                    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                End If
            Else
                Dim cl_script0 As New System.Text.StringBuilder
                cl_script0.Append("         alert('Uploaded Only Excel file with Extention .xls, Ex::sample.xls ');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
                Exit Sub

            End If
            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            Try
                'CInt(Session("UserID"))
                Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TableData", dtExcelRecords), New System.Data.SqlClient.SqlParameter("@UserID", 3366), New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0), New System.Data.SqlClient.SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)}
                Parameters(1).Direction = ParameterDirection.Input
                Parameters(2).Direction = ParameterDirection.Output
                Parameters(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_FA_ASSET_DETAILS_UPLOAD", Parameters)

                ErrorFlag = CInt(Parameters(2).Value)
                Message = CStr(Parameters(3).Value)
                Dim Result() = Message.Split("Ø")
                Dim UpdateID As Integer = Result(1)

                If UpdateID <> 0 Then
                    Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_script1.Append("alert('" + Result(0) + "');")
                    'cl_script1.Append("window.open('Reports/NotUpdatedRecord.aspx?StatusID=1&UpdateID='" + UpdateID.ToString + "');")
                    cl_script1.Append("window.open('Reports/NotUpdatedRecord.aspx?StatusID=1&UpdateID=" & UpdateID & "','_self');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)
                Else
                    Dim cl_script As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_script.Append("alert('" + Result(0) + "');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script.ToString, True)
                End If
            Catch ex As Exception
                MsgBox("Exceptional Error Occurred.Please Inform Application Team")
                Response.Redirect("~/CatchException.aspx?ErrorNo=1")

            End Try
            File.Delete(fileLocation)
        End If
    End Sub
#End Region

End Class
