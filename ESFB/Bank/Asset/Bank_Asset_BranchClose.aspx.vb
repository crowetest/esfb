﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Bank_Asset_BranchClose
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1361) = False Then
                'If GF.FormAccess(CInt(Session("UserID")), 1132) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Me.Master.subtitle = "Branch Close/Reopen"
            DT = DB.ExecuteDataSet("select count(*) from BK_AT_Branch_close where status<>1 ").Tables(0)
            If CInt(DT.Rows(0)(0).ToString) >= 0 Then
                hid_CloseStatus.Value = "0"
            Else
                hid_CloseStatus.Value = "1"
            End If
            '    Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            '    cl_script1.Append("         alert('Branch Closed For Updation.Contact ESS For Releasing');")
            '    cl_script1.Append("        window.open('../home.aspx', '_self');")
            '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            '    Return
            'End If
            DT = DB.ExecuteDataSet("select '-1' as id,'------- select --------' union all select distinct a.Branch_id,Branch_name from branch_master a,(select branch_id from bk_at_details group by branch_id having max(verify_flg)=0 )   b where a.branch_id=b.branch_id ").Tables(0)
            GF.ComboFill(cmbbranch, DT, 0, 1)

            Me.cmbbranch.Attributes.Add("onchange", "return BranchOnchange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "setdata();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))


        If CInt(Data(0)) = 1 Then


            Dim UserID As Integer = CInt(Session("UserID"))
            Dim BranchID As Integer = CInt(Data(1))



            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@userID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(1).Value = BranchID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_BK_AT_BRANCH_CLOSE", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString
        ElseIf CInt(Data(0)) = 2 Then
            Dim StrAttendance As String = ""
            DT = DB.ExecuteDataSet("select closeid,branch_id,status from [BK_AT_Branch_Close]  where  branch_id  =" & CInt(Data(1)).ToString() & "  ").Tables(0)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    StrAttendance += DR(0).ToString() + "|" + DR(1).ToString() + "|" + DR(2).ToString() + "Ø"
                Next
            Else
                StrAttendance += "Ø"
            End If


            DT = DB.ExecuteDataSet("select asset_tag,isnull(c.category_name,''),isnull(b.item_name,''),isnull(d.make_name,''),isnull([model],''),isnull([serial_no],'')," & _
        " isnull(convert(varchar,[Entered_By])+' - '+[Entered_Emp_name] ,''),isnull(convert(varchar,[Branch_Verified_By])+' - '+k.emp_name ,'') , " & _
        " isnull([remarks] ,''),isnull(convert(varchar,[ho_Verified_By])+' - '+l.emp_name ,''),case when [verify_flg] =0 then '' when [verify_flg] =1 then 'Branch Level pending'  else 'Ho Level Pending' end,isnull([Branch_verified_on] ,''),isnull([HO_verified_on] ,''),isnull([ho_remarks] ,''),isnull(g.status_name ,''),  " & _
        " a.[Branch_id],c.[category_id],a.[item_id],a.[make_id],[Delivery_dt],[department],a.[department_id],  " & _
        " [change_dt],verify_flg from [BK_AT_details] a left join  emp_master k on a.branch_verified_by =k.emp_code left join  emp_master l on " & _
        " a.ho_verified_by =l.emp_code ,BK_AT_item_master b, " & _
        " BK_AT_category_master c,BK_AT_make_master d,BK_AT_status_master g where a.item_id=b.item_id and b.category_id=c.category_id and a.make_id=d.make_id " & _
        " and a.Asset_status  =g.status_id and  a.branch_id  =" & CInt(Data(1)).ToString() & "").Tables(0)
            For Each DR As DataRow In DT.Rows
                StrAttendance += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString() + "µ" + DR(7).ToString() + "µ" + DR(8).ToString() + "µ" + DR(9).ToString() + "µ" + DR(10).ToString() + "µ" + DR(11).ToString() + "µ" + DR(12).ToString() + "µ" + DR(13).ToString() + "µ" + DR(14).ToString() + "µ" + DR(15).ToString() + "µ" + DR(16).ToString() + "µ" + DR(17).ToString() + "µ" + DR(18).ToString() + "µ" + DR(19).ToString() + "µ" + DR(20).ToString() + "µ" + DR(21).ToString() + "µ" + DR(22).ToString() + "µ" + DR(23).ToString()
            Next
            CallBackReturn = StrAttendance

        End If







    End Sub
#End Region

End Class
