﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true"  CodeFile="Bank_Asset_ItemMaster.aspx.vb" Inherits="Bank_Asset_ItemMaster" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
         .fileUpload{
    width:255px;    
    font-size:11px;
    color:#000000;
    border:solid;
    border-width:1px;
    border-color:#7f9db9;    
    height:17px;
    }
            .ScrollClass
        {           
            text-align:center; color:#E31E24;
            background-color:#EBDDDD;
            height:20px;
           
        }
        .style2
     {
         width: 23%;
     }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        
        function window_onload() 
        {
            document.getElementById("<%= hdnNewDtl.ClientID %>").value = "";
            document.getElementById("<%= hdnItemValue.ClientID %>").value = "";
            document.getElementById("rowSubAsset").style.display = "none";
        }

        function FromServer(arg, context) 
        {
            switch (context) {
             case 1:
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("Bank_Asset_ItemMaster.aspx", "_self");
                break;            
             case 2:
                document.getElementById("<%= hdnItemValue.ClientID %>").value = arg;
                tablefill_Item();
                break;
              default:
            break;
        
            }
        }
         
        function CheckSubAsset()
        {
            if (document.getElementById("<%= chkSubAsset.ClientID %>").checked == true)
            {
            
                AddNewSubAsset(); 
                
            }                            
            else
            document.getElementById("<%= pnlDtls.ClientID %>").style.display = "none";
        }

        function AddNewSubAsset() 
        {
            if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") 
            {
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var Len = row.length - 1;
                col = row[Len];
            if (col != "-1" && col != "") 
            {
                document.getElementById("<%= hdnNewDtl.ClientID %>").value += "¥";
            }
            }
            else
            {
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = "¥";   
            }
            tablefillSubAsset();
        }
         function tablefill_Item()
            {
                document.getElementById("<%= pnlItemDtls.ClientID %>").style.display = '';
            
                var row_bg = 0;
                var tab = "";
                if (document.getElementById("<%= hdnItemValue.ClientID %>").value != "") {
                    tab += "<div style='height:auto;width:55%; text-align:center;margin: 0px auto;font-size:12pt;' align='center' >ITEM DETAILS</div>"
                    tab += "<div style='height:auto;width:55%;text-align:center;margin: 0px auto;' align='center' class='mainhead'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
                
                    tab += "<tr class=mainhead>";
                    tab += "<td style='width:5%;text-align:center;' >SLNo</td>";                
                    tab += "<td style='width:45%;text-align:left' >Item Name</td>";                 
                    tab += "</tr></table></div>";                    
               
                    tab += "<div style='height:258px;width:55%; overflow:auto;text-align:center;margin: 0px auto;' align='center' class='mainhead'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>"; 
                    row = document.getElementById("<%= hdnItemValue.ClientID %>").value.split("¥");  
                    for (n = 0; n <= row.length - 1; n++) {                    
                        col = row[n].split("µ");  
                                    
                        if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class=sub_first>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class=sub_second>";
                        }
                        i = n+1 ;                   
                        tab += "<td style='width:5%;text-align:center;' >" + i + "</td>";
                        tab += "<td style='width:45%;text-align:left;' >" + col[1] + "</td>";                    
  
                        tab += "</tr>"; 
                    } 
                }   
                tab += "</table></div>";            
                document.getElementById("<%= pnlItemDtls.ClientID %>").innerHTML = tab;
            
            
            }
        

        function tablefillSubAsset() 
        {
       
            if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") 
            {
                document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div style='width:55%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
                tab += "<table style='width:55%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr height=20px;  class='tblQal'>";
                tab += "<td style='width:5%;text-align:center'></td>";
                tab += "<td style='width:45%;text-align:left'>Sub Asset Name</td>";                
                tab += "<td style='width:5%;text-align:left'></td>";
                tab += "</tr>";
                tab += "</table></div>";
                tab += "<div id='ScrollDiv' style='width:55%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");               
                for (n = 1; n <= row.length - 1; n++) 
                {
                    col = row[n];
                    if (row_bg == 0) 
                    {
                        row_bg = 1;
                        tab += "<tr class='sub_first';>";
                    }
                    else 
                    {
                        row_bg = 0;
                        tab += "<tr class='sub_second';>";
                    }
                    
                    tab += "<td style='width:5%;text-align:center' >" + n + "</td>";              
                    if (col != "")
                    {
                        tab += "<td style='width:45%;text-align:left' >" + col + "</td>"; //SubAsset
                        tab += "<td style='width:5%;text-align:center' onclick=DeleteRowSubAsset('" + n + "')><img  src='../Image/remove.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                    }
                    else 
                    {
                        var txtBox = "<input id='txtSubAsset" + n + "' name='txtSubAsset" + n + "' type='Text' style='width:99%;'  maxlength='500' onchange='UpdateValueSubAsset(" + n + ")' onkeydown='CreateNewSubAsset(event," + n + ")'/>";
                        tab += "<td style='width:45%;text-align:left' >" + txtBox + "</td>";
                        tab += "<td style='width:5%;text-align:left'></td>";
                    }
                    
                    tab += "</tr>";
                }
                tab += "</table></div></div></div>";
                document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
                setTimeout(function () { document.getElementById("txtSubAsset" + (n - 1)).focus().select(); }, 4);
            }
            else 
                document.getElementById("pnlDtls").style.display = "none";
        }
         
            
        function CreateNewSubAsset(e, val) 
        {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) 
            {
                UpdateValueSubAsset(val);
                AddNewSubAsset();
                document.getElementById("txtSubAsset" + val).focus();
            }
        }
            
        function UpdateValueSubAsset(id) 
        {
            row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
            var NewStr = ""
            for (n = 1; n <= row.length - 1; n++) 
            {
                if (id == n) 
                {
                    var SubAsset = document.getElementById("txtSubAsset" + id).value;                        
                    NewStr += "¥" + SubAsset ;
                }
                else 
                {
                    NewStr += "¥" + row[n];
                }
            }
            document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
        }

        function DeleteRowSubAsset(id) 
        {   
            row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
            var NewStr = ""
            for (n = 1; n <= row.length - 1; n++) 
            {
                if (id != n)
                NewStr += "¥" + row[n];
            }
            document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
            if (document.getElementById("<%= hdnNewDtl.ClientID %>").value == "")
            document.getElementById("<%= hdnNewDtl.ClientID %>").value = "¥";
            tablefillSubAsset();
        }
          
          
       
        function btnSave_onclick() 
        {
            if (document.getElementById("<%= cmbCategory.ClientID %>").value == "-1") { alert("Select Category"); document.getElementById("<%= cmbCategory.ClientID %>").focus(); return false; }
            if (document.getElementById("<%= txtItemName.ClientID %>").value == "") { alert("Enter Item Name"); document.getElementById("<%= txtItemName.ClientID %>").focus(); return false; }                
             var ItemName = document.getElementById("<%= txtItemName.ClientID %>").value;
                            
       
            var CategoryId=document.getElementById("<%= cmbCategory.ClientID %>").value;                
            var ItemName=document.getElementById("<%=txtItemName.ClientID %>").value;             
            var ToData = "1Ø" + CategoryId + "Ø" + ItemName ;
            ToServer(ToData, 1);
        }

        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        } 

        function CategoryOnChange(){
            var CategoryId = document.getElementById("<%= cmbCategory.ClientID %>").value;  
            if(CategoryId != -1){
                var ToData = "2Ø" + CategoryId;
                ToServer(ToData, 2);
                }

        }

    </script>
   
</head>
</html>

<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;" >
           
        <tr id="branch"> <td style="width:5%;"></td><td style="width:18%"></td>
        <td style="text-align:left;" class="style2">Category Name</td>
            <td style="width:50%">
                &nbsp; 
                <asp:DropDownList ID="cmbCategory" runat="server" Width="50%" class="NormalText">
                </asp:DropDownList>
                &nbsp;</td>
            <td style="width:15%;"></td>
       </tr>
       
       <tr id="Tr1"> <td style="width:5%;"></td><td style="width:18%">&nbsp;</td>
        <td style="text-align:left;" class="style2">Item Name</td>
            <td style="width:50%">
                &nbsp;
                <asp:TextBox ID="txtItemName" runat="server"  
                    style=" font-family:Cambria;font-size:10pt;" Width="50%" class="NormalText" 
                    MaxLength="50" height="20px" />
            </td>
            <td style="width:15%;"></td>
       </tr>
       
       <tr id="Tr1" style="display:none;"> <td style="width:5%;">&nbsp;</td><td style="width:18%">&nbsp;</td>
        <td style="text-align:left;" class="style2">Ownership</td>
            <td style="width:50%">
                <asp:RadioButton ID="rbIndividual" runat="server" Text="Individual" 
                    GroupName="grpown" Font-Names="Cambria" Font-Size="10pt" />
&nbsp;&nbsp;
                <asp:RadioButton ID="rbLocation" runat="server" Text="Location Owner" 
                    GroupName="grpown" />
            </td>
            <td style="width:15%;">&nbsp;</td>
       </tr>
       
       <tr id="Tr1" style="display:none;"> <td style="width:5%;">&nbsp;</td><td style="width:18%">&nbsp;</td>
        <td style="text-align:left;" class="style2">Whether Sub Assets Under ?</td>
            <td style="width:50%">
                <asp:CheckBox ID="chkSubAsset" runat="server" />
            </td>
            <td style="width:15%;">&nbsp;</td>
       </tr>
       
            <tr style="text-align:center;"> 
            <td  colspan="5" style="text-align:center;">
                <asp:Panel ID="pnlDtls" style="display:none" runat="server" Width="93%">
                    </asp:Panel>
                </td>

        </tr>
        <tr style="text-align:center;"> 
            <td  colspan="5" style="text-align:center;">
               
                </td>

        </tr>
         <tr style="text-align:center;"> 
            <td  colspan="5" style="text-align:center;">
                <asp:Panel ID="pnlItemDtls" style="display:none" runat="server" Width="93%">
                    </asp:Panel>
                </td>

               
        </tr>
       <tr>
            <td style="text-align:center;" colspan="4"><br />
                <input id="btnSave" style="font-family: Cambria; cursor: pointer;width: 6%;" 
                    type="button" value="SAVE" onclick="return btnSave_onclick()" />&nbsp;
                 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
                <asp:HiddenField ID="hdnItemValue" runat="server" />
            <asp:HiddenField ID="hdnNewDtl" runat="server" />
                </td>
                <td style="width:15%;"></td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

