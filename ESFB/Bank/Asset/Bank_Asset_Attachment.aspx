﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="Bank_Asset_Attachment.aspx.vb" Inherits="Bank_Asset_Attachment" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    *                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
*                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
    </style>
    <script language="javascript" type="text/javascript">
        function RequestOnClick() {
            if (document.getElementById("<%= fup1.ClientID %>").value == "")
            { alert("Select Image"); return false; }
            if (document.getElementById("<%= fup1.ClientID %>").value != "") {
                var fileName = document.getElementById("<%= fup1.ClientID %>").value;
                var file_extDelimiter = fileName.lastIndexOf(".") + 1;
                var file_ext = fileName.substring(file_extDelimiter).toLowerCase();
                if (file_ext != "png" && file_ext != "jpeg" && file_ext != "jpg")
                { alert("Attach a png/Jpeg file"); return false; }
            }
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        } 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table align="center" class="style1">
            <tr>
                <td style="width:30%;">
                    &nbsp;</td>
                <td style="width:70%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width:30%; text-align:center;" colspan="2" class="mainhead">
                    ADD ASSET ATTACHMENT</td>
             
            </tr>
            <tr>
                <td style="width:30%; text-align:right;">
                    &nbsp;&nbsp;Select Photo</td>
                <td style="width:70%; text-align:left;">
            &nbsp;&nbsp;&nbsp;&nbsp;<input id="fup1" type="file" runat="server" 
                style="font-family: Cambria; font-size: 10.5pt" /></td>
            </tr>
            <tr>
                <td style="width:30%;">
                    &nbsp;</td>
                <td style="width:70%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width:30%; text-align:center;" colspan="2">
           <asp:Button 
                ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" style="cursor:pointer;"
                 Width="6%" /> <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
                </td>
             
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
</asp:Content>