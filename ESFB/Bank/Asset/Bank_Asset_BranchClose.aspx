﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Bank_Asset_BranchClose.aspx.vb" Inherits="Bank_Asset_BranchClose" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10pt;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10pt;color:#476C91;
        }
    </style>

    <script src="../../Script/Validations.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
       
        
        function FromServer(Arg, Context) {
            
            if (Context == 1) {
                var Data = Arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("Bank_Asset_BranchClose.aspx", "_self");
            }
            else {
                var Data = Arg.split("Ø");
                
                
                if (Data[0] != "") {
                    var Branchstatus = Data[0].split("|"); 
                    document.getElementById("<%= hid_CloseStatus.ClientID%>").value = Branchstatus[2];
                }
                else
                    document.getElementById("<%= hid_CloseStatus.ClientID%>").value = 0;
              
                if (document.getElementById("<%= hid_CloseStatus.ClientID%>").value == 1) {
                    document.getElementById("chkval").innerHTML= "<b style='color:red;font-size:15pt;'>Reopen the Branch</b>";
                    
                }
                else {
                    document.getElementById("chkval").innerHTML = "<b style='color:red;font-size:15pt;'>Close the Branch</b>";
                    //document.getElementById("chkCloseReopen").textContent = "Close the Branch";
                }
                document.getElementById("<%= hid_dtls.ClientID%>").value = Data[1];
                table_fill();
            }
            
        }
        function setdata() {
            if (document.getElementById("<%= hid_CloseStatus.ClientID%>").value == 1) {
                document.getElementById("chkval").innerHTML = "<b style='color:red;font-size:15pt;'>Reopen the ALL Branches</b>";

            }
            else {
                document.getElementById("chkval").innerHTML = "<b style='color:red;font-size:15pt;'>Close the ALL Branches</b>";
                
            }
        }
        function btnApprove_onclick() {
            if (document.getElementById("<%= cmbbranch.ClientID%>").value == "-1") {
                if (document.getElementById("chkCloseReopen").checked == true) {
                    if (document.getElementById("<%= hid_CloseStatus.ClientID%>").value == 0) {
                        if (confirm("Are you sure to close All Branch?") == 0)
                        { return false; }
                        else
                        {
                            var Data = "1Ø-1";
                            ToServer(Data, 1);
                            return false;
                        }
                    }
                    else
                    {
                        if (confirm("Are you sure to Open All Branch?") == 0)
                        { return false; }
                        else
                        {
                            var Data = "1Ø-2";
                            ToServer(Data, 1);
                            return false;
                        }
                    }
                }
                else {
                    alert("Check the All Branch Close status");
                    return false;
                }
               
            }
            if (document.getElementById("chkCloseReopen").checked == true) {

                var branch = document.getElementById("<%= cmbbranch.ClientID%>").value;
                 var Data = "1Ø" + branch;
                 ToServer(Data, 1);
             }
             else {


                 if (document.getElementById("<%= hid_CloseStatus.ClientID%>").value == 1) {
                     alert("Check the Branch Reopen status");
                     return false;
                 }
                 else {
                     alert("Check the Branch Close status");
                     return false;
                 }

             }
           

        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function BranchOnchange() {
            document.getElementById("<%= hid_dtls.ClientID%>").value = "";
            table_fill();
            var branch = document.getElementById("<%= cmbbranch.ClientID%>").value;
            if (document.getElementById("<%= cmbbranch.ClientID%>").value == "-1") {
                
                return false;
            }
            else {
                var Data = "2Ø" + branch;
                ToServer(Data,2);
            }
           
        }
       function table_fill() {
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                document.getElementById("<%= pnFamily.ClientID %>").style.display = '';
             var row_bg = 0;
             var tab = "";
             tab += "<div style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
             tab += "<tr height=20px;  class='tblQal'>";
             tab += "<td style='width:5%;text-align:center' >#</td>";
             tab += "<td style='width:10%;text-align:left' >Assettag</td>";
             tab += "<td style='width:10%;text-align:left' >Category</td>";
             tab += "<td style='width:10%;text-align:left' >Item</td>";
             tab += "<td style='width:10%;text-align:left'>Make</td>";
             tab += "<td style='width:10%;text-align:left'>Model</td>";
             tab += "<td style='width:10%;text-align:left'>Serial No</td>";
             tab += "<td style='width:10%;text-align:left'>Branch Verified By</td>";
              tab += "<td style='width:10%;text-align:left'>HO Verified By</td>";
             tab += "<td style='width:15%;text-align:left'>Status</td>";
             tab += "</tr>";
             tab += "</table></div>";
             tab += "<div id='ScrollDiv' style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

             row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var i = 0;
            for (n = 1; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                if (col[5] != 1) {
                    var i = i+1;
                   
                        
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px; background-color: #FFF;'>";
                    

                    tab += "<td style='width:5%;text-align:center' >" + i + "</td>";

                   
                    tab += "<td style='width:10%;text-align:left' >" + col[0] + "</td>";
                    
                    tab += "<td style='width:10%;text-align:left' >" + col[1] + "</td>";
                   
                    tab += "<td style='width:10%;text-align:left' >" + col[2] + "</td>";
                    
                    tab += "<td style='width:10%;text-align:left' >" + col[3] + "</td>";
                    
                    tab += "<td style='width:10%;text-align:left' >" + col[4] + "</td>";
                    tab += "<td style='width:10%;text-align:left' >" + col[5] + "</td>";
                    tab += "<td style='width:10%;text-align:left' >" + col[6] + "</td>";
                    tab += "<td style='width:10%;text-align:left' >" + col[7] + "</td>";
                    tab += "<td style='width:15%;text-align:left' >" + col[14] + "</td>";
                    tab += "</tr>";
                }
            }

            tab += "</table></div></div></div>";
            document.getElementById("<%= pnFamily.ClientID %>").innerHTML = tab;
              
        }
        else
            document.getElementById("<%= pnFamily.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }

        function viewSummary()
        {
           var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
           
            window.open("Reports/ViewAssetpendingclose.aspx");
            
            return false;
        }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
        <tr id="branch"><td style="width:25%;"></td>
            <td style="width:12%; text-align:left;">Select Branch</td>
                <td style="width:63%">
                    &nbsp; &nbsp;<asp:DropDownList ID="cmbbranch" class="NormalText" style="text-align: left;" runat="server" Font-Names="Cambria" 
                    Width="60%" ForeColor="Black">
                </asp:DropDownList><img id='ViewAsset' src='../../Image/eswtClose2.png' onclick='viewSummary()' title='View Asset List'  style='height:20px; width:20px;  cursor:pointer;' />
                    <input id="chkCloseReopen" title="New" type="checkbox"   style="border-style: none" 
                    />&nbsp;&nbsp;<label id="chkval"><b style="color:red;font-size:15pt;">CLOSE ALL BRANCH</b></label>
                </td>
            
           </tr>
        <tr> 
            <td colspan="3"><asp:Panel ID="pnFamily" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
               
                <asp:HiddenField ID="hid_CloseStatus" runat="server" />
               
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SAVE"  onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

