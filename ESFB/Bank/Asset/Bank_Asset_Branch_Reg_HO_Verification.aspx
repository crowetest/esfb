﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true"  CodeFile="Bank_Asset_Branch_Reg_HO_Verification.aspx.vb" Inherits="Bank_Asset_Branch_Reg_HO_Verification" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
       .fileUpload{
                width:255px;    
                font-size:11px;
                color:#000000;
                border:solid;
                border-width:1px;
                border-color:#7f9db9;    
                height:17px;
        }
            .ScrollClass
        {           
            text-align:center; color:#E31E24;
            background-color:#EBDDDD;
            height:20px;
           
        } 
        </style>

         <style> 
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
     
        function AlphaNumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = (e.which >= 48 && e.which <= 57) || (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || (e.which == 8) || (e.which == 13) || (e.which == 32) || (e.which == 0);

            if (!valid) {
                e.preventDefault();
            }
        }
        function NumericDotCheck(e)//------------function to check whether a value is alpha numeric
        {
            // el - this, e - event
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                    if (unicode == 37 || unicode == 38) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (unicode == 46) {
                        var parts = el.value.split('.');
                        if (parts.length == 2)
                            return false;
                        return true;
                    }
                }
            }
            else {
                return true;
            }
        }
        function NumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = ((e.which >= 48 && e.which <= 57) || (e.keyWhich == 9) || (e.keyCode == 9) || (e.keyWhich == 8) || (e.keyCode == 8));
            //alert(valid);
            if (!valid) {
                e.preventDefault();
            }

        }
   
    
        function attachment_onclick() {
            //alert(1);
        } 
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }    
        function table_fill() 
        {  
            document.getElementById("<%= pnlRequest.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:1%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:4%;text-align:left'>Image</td>";  
            tab += "<td style='width:6%;text-align:left' >Branch</td>";
            tab += "<td style='width:6%;text-align:left'>Category</td>";
            tab += "<td style='width:7%;text-align:left'>Item</td>";
            tab += "<td style='width:8%;text-align:left'>Make</td>";
            tab += "<td style='width:9%;text-align:left'>Model</td>";
            tab += "<td style='width:9%;text-align:left'>SerialNo</td>";
            tab += "<td style='width:6%;text-align:left'>Current Status</td>";
            tab += "<td style='width:6%;text-align:left'>Procurement Date</td>";
            tab += "<td style='width:6%;text-align:left'>Warranty Start</td>";
            tab += "<td style='width:6%;text-align:left'>Warranty End</td>";
            tab += "<td style='width:6%;text-align:left'>created By</td>";
            tab += "<td style='width:6%;text-align:left'>Created On</td>";
            tab += "<td style='width:4%;text-align:center'>Action</td>";
            tab += "<td style='width:7%;text-align:center'>Remarks</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hdn_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hdn_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                    

                   

                   tab += "<td style='width:1%;text-align:center;'>" + i + "</td>";
                   
                   
                     if(col[26]=="")     
                    {           
                    tab += "<td style='width:4%;text-align:left' class='NormalText'> </td>";
                    }
                    else
                    {

                    tab += "<td style='width:4%;text-align:left' class='NormalText'><img id='ViewAttachment' src='../../Image/attchment2.png' onclick='viewReport("+ i +")' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' /> <img id='ImgPhoto"+ i +" alt='' src='"+ col[25] +"' style='height:200px; width:200px;display:none;' title='' /></td>";
                    }
                                        
                  tab += "<td style='width:6%;text-align:left' >" + col[1] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[20] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[19] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[21] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:9%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:9%;text-align:left'>" + col[9] + "</td>";
                  
                   
                    tab += "<td style='width:6%;text-align:left'>" + col[12] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[13] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[14] + "</td>";
                     tab += "<td style='width:6%;text-align:left'>" + col[10] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[11] + "</td>";

                    var select = "<select id='cmbVerify" + i + "' class='NormalText' name='cmbVerify" + i + "' >";
                    select += "<option value='-1'>Select</option>";
                    select += "<option value='1'>Approved</option>";
                    select += "<option value='2'>Rejected</option>";

                    tab += "<td style='width:4%;text-align:left'>" + select + "</td>";


                    var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)' ></textarea>";

                    tab += "<td style='width:7%;text-align:left'>" + txtBox + "</td>";


                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnlRequest.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//

        }
         
       
        
         
         function viewReport(ID)
        {
       
                document.getElementById("ImgPhoto").style.display = '';
                document.getElementById("btnClear").style.display = '';
                document.getElementById("ImgPhoto").setAttribute('src',"");
                row = document.getElementById("<%= hdn_dtls.ClientID %>").value.split("¥");
                var n=0;
                for (i = 0; i <= row.length - 1; i++) {
                     n = i+1;
                    col = row[i].split("µ");  
                    if (n==ID)
                    document.getElementById("ImgPhoto").setAttribute('src',col[25]); 

                    }
            
        
            return false;
        }
          function AddAttachment(ID)
        {
          
                //window.showModalDialog("Bank_Asset_Attachment.aspx?RequestID=" + btoa(ID),"",'dialogHeight:200px;dialogWidth:500px; dialogLeft:300;dialogTop:300; center:yes'); 
                window.open("Bank_Asset_Attachment.aspx?RequestID=" + btoa(ID),"_self");
            return false;
        }
         function viewHistory(ID)
        {
           
           
            window.open("Reports/ViewAssetHistory.aspx?AssetID=" + btoa(ID));
            
            return false;
        }
         function viewSummary()
        {
           var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
           
            window.open("Reports/ViewAssetSummary.aspx?BranchID=" + btoa(BranchID));
            
            return false;
        }
       
        function FromServer(arg, context) {

          switch (context) {

              case 2:
                  {
                     
                      document.getElementById("<%= hdn_dtls.ClientID %>").value = arg;
                      table_fill();
                      
                      break;
                  }

              case 1:
                  {
                      var Data = arg.split("Ø");
                      alert(Data[1]);
                      if (Data[0] == 0) window.open("Bank_Asset_Branch_Reg_HO_Verification.aspx", "_self");
                      break;
                  }

          }
        }
        

      
        function updateValue() {
            var NewStr = ""
           
            if (document.getElementById("<%= hdn_dtls.ClientID %>").value != "¥µµ-1µ-1µ-1µµµµµµµµµµµµµµµµµµ") {
               
                row = document.getElementById("<%= hdn_dtls.ClientID %>").value.split("¥");
               
               //alert(row.length);
               var id =0;
                for (n = 0; n < row.length ; n++) {
                   var col = row[n].split("µ");
                    id = n+1;
                       
                        if (document.getElementById("cmbVerify" + id).value!="-1"){

                           
                            var VerifyStatus = document.getElementById("cmbVerify" + id).value;
                            var Remarks = document.getElementById("txtRemarks" + id).value;

                       
                            NewStr += "¥" + col[0] + "µ" + Remarks + "µ" + VerifyStatus + "µ"  + document.getElementById("<%= hid_Brtype.ClientID %>").value;
                        }
                    
                }
                
           
            }
           
            document.getElementById("<%= hid_temp.ClientID %>").value = NewStr;
            //alert(document.getElementById("<%= hdn_dtls.ClientID %>").value);
        }
function btnSave_onclick() {
        
        alert(100);
          if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1")
          { alert("Select Branch"); document.getElementById("<%= cmbBranch.ClientID %>").focus(); return false; }
          if (updateValue()==1){
            alert ("Atleast one data is needed for saving");
              return false;
          }
          alert(document.getElementById("<%= hid_temp.ClientID %>").value);
          ToServer("1Ø" + document.getElementById("<%= cmbBranch.ClientID %>").value+"Ø"+ document.getElementById("<%= hid_temp.ClientID %>").value, 1);
        }
        function RequestOnchange (){
        BranchOnChange();
        }
         function BranchOnChange() {
        
               
                if (document.getElementById("<%= cmbBranch.ClientID%>").value != "-1")
                {
                    var ToData = "2Ø" + document.getElementById("<%= cmbBranch.ClientID%>").value +"Ø"+ document.getElementById("<%= hid_Brtype.ClientID%>").value; 
                    ToServer(ToData, 2);
                }
           
            }
       
        function btnClear_onclick(){
       document.getElementById("ImgPhoto").style.display = 'none';
       document.getElementById("btnClear").style.display = 'none';
       }

    </script>
   
</head>
</html>

<br /><br />
<table style="width:20%;margin: 0px auto;"><tr><td style="width:90%"><img id="ImgPhoto" alt="" src="" style="height:300px; width:1000px;display:none;"  title="" /></td> <td style="width:10%"><input id="btnClear" style="font-family: cambria; cursor: pointer; width: 100%;display:none;"  type="button" value="Clear"  onclick="return btnClear_onclick()" /></td></tr></table>
 <table class="style1" style="width:100%;margin: 0px auto;" >
           
            
        <tr id="branch"> <td style="width:25%;">&nbsp;</td>
        <td style="width:15%; text-align:left;">Branch</td>
            <td style="width:63%">
                &nbsp; <asp:DropDownList ID="cmbBranch" class="NormalText" style="text-align: left; " runat="server" Font-Names="Cambria" text-align="top" Width="50%" Height="100%"  ForeColor="Black"  >
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    </asp:DropDownList>
                <img id='ViewAsset' src='../../Image/eswtClose2.png' onclick='viewSummary()' title='View Asset List'  style='height:20px; width:20px;  cursor:pointer;' />
                
            </td>
            
       </tr>
        <tr>
   
        <td style="width:25" colspan="3" ><asp:HiddenField ID="hdn_dtls" runat="server" />
                <asp:HiddenField ID="hdn_check" runat="server" />
                <asp:HiddenField ID="hid_temp" runat="server" /><asp:Panel ID="pnlRequest" runat="server" >
                </asp:Panel></td>
                
        </tr>
        
       <tr>
            <td style="text-align:center;" colspan="5"><br />
                <asp:HiddenField ID="hid_Category" 
                runat="server" />
                <asp:HiddenField ID="hid_ImgPhoto" 
                runat="server" />
                <asp:HiddenField ID="hid_Item" runat="server" />
                <asp:HiddenField ID="hid_Maker" runat="server" />
                <asp:HiddenField ID="hid_Status" runat="server" />
                <asp:HiddenField ID="hid_Brtype" runat="server" />
                <asp:HiddenField ID="hid_AssetMap" runat="server" />
                <asp:HiddenField ID="hid_StatusVerify" runat="server" />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 8%;" 
                type="button" value="SUBMIT"   onclick="return btnSave_onclick()" />&nbsp;
                 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />&nbsp;
                </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

