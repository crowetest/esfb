﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Bank_Asset_Branch_Reg_HO_Verification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1352) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "New Asset HO Verification"


            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            'DT = DB.ExecuteDataSet("select count(*) from BK_AT_Branch_Close where status=1  and branch_id='" & Session("BranchID").ToString() & "'").Tables(0)
            'If CInt(DT.Rows(0)(0).ToString) > 0 Then
            '    Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            '    cl_script1.Append("         alert('Branch Closed For Updation.Contact It Infra Head Office Team For Releasing');")
            '    cl_script1.Append("        window.open('../home.aspx', '_self');")
            '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            '    Return
            'End If



            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0



            DT = DB.ExecuteDataSet("select -1 as Branch_ID,' -----Select-----' as Branch_Name union all  select distinct Branch_ID,Branch_Name from BRANCH_MASTER where branch_id in (select distinct branch_id from BK_AT_details_tmp where verify_flg is null) and status_id = 1 order by 2 ").Tables(0)
            hid_Brtype.Value = "2"

            GN.ComboFill(cmbBranch, DT, 0, 1)
            cmbBranch.SelectedIndex = 0

            cmbBranch.SelectedIndex = 0



            Me.cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "RequestOnchange();", True)


        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try



    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))

        If CInt(Data(0)) = 2 Then

            DT = DB.ExecuteDataSet("select a.[Asset_ID],g.branch_name+'('+convert(varchar,a.[Branch_id]) +')',c.[category_id],a.[item_id],a.[make_id],[model],[serial_no],[Entered_By],[Entered_Emp_name]," & _
                                   " h.status_name,[entered_emp_name],convert(varchar,convert(date,[change_dt]),104),case when convert(date,[delivery_dt])='01/jan/1900' then '' else convert(varchar,convert(date,[delivery_dt]),104) end,case when convert(date,[warrant_start])='01/jan/1900' then '' else convert(varchar,convert(date,[warrant_start]),104) end,case when convert(date,[warrant_end])='01/jan/1900' then '' else convert(varchar,convert(date,[warrant_end]),104) end,asset_tag,[Branch_verified_on], " & _
                                   " [remarks],[ho_remarks],b.item_name,c.category_name,d.make_name,a.mapassetid,a.asset_tag,a.new_flg,k.val,k.content_type from [BK_AT_details_tmp] a left join " & _
                                   " DMS_ESFB.dbo.BK_AT_Attachment_tmp k on a.asset_id=k.asset_id left join branch_master g on a.branch_id=g.branch_id left join bk_at_status_master h on a.asset_status=h.status_id,BK_AT_item_master b, " & _
                " BK_AT_category_master c,BK_AT_make_master d where a.item_id=b.item_id and b.category_id=c.category_id and a.make_id=d.make_id and a.branch_id=" & Data(1).ToString & " and verify_flg is null ").Tables(0)
            Dim decryptval As String
            Dim StrAttendance As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                decryptval = GN.Encrypt(DT.Rows(n)(0).ToString)
                StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString & "µ" & DT.Rows(n)(16).ToString & "µ" & DT.Rows(n)(17).ToString & "µ" & DT.Rows(n)(18).ToString & "µ" & DT.Rows(n)(19).ToString & "µ" & DT.Rows(n)(20).ToString & "µ" & DT.Rows(n)(21).ToString & "µ" & DT.Rows(n)(22).ToString & "µ" & DT.Rows(n)(23).ToString & "µ" & DT.Rows(n)(24).ToString
                If DT.Rows(n)(26).ToString <> "" Then
                    Dim bytes As Byte() = DirectCast(DT.Rows(n)(25), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                    StrAttendance += "µ" + Convert.ToString("data:" + DT.Rows(n)(26).ToString + ";base64,") & base64String
                Else
                    StrAttendance += "µ"
                End If
                If n < DT.Rows.Count - 1 Then
                    StrAttendance += "¥"
                End If
            Next
            CallBackReturn = (StrAttendance.ToString)
        Else
            Dim BranchID As Integer = CInt(Data(1))
            Dim dataval As String = CStr(Data(2))
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@userID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(1).Value = BranchID
                Params(2) = New SqlParameter("@EMPDtl", SqlDbType.VarChar)
                Params(2).Value = dataval.Substring(1)
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_BK_AT_NEW_REG_HO_VERIFICATION", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message

        End If
    End Sub

#End Region



End Class
