﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Bank_Asset_CategoryMaster
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If GN.FormAccess(CInt(Session("UserID")), 1357) = False Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        Me.Master.subtitle = "Create Category"
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        DT = DB.ExecuteDataSet("SELECT a.category_ID,a.category_NAME,a.STATUS_ID From BK_AT_category_MASTER a order by 2").Tables(0)
        Dim StrMak As String = ""
        For n As Integer = 0 To DT.Rows.Count - 1
            StrMak += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString
            If n < DT.Rows.Count - 1 Then
                StrMak += "¥"
            End If
        Next
        hdnValue.Value = StrMak
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "windows_onload();", True)
    End Sub

    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        '"1Ø" +categoryName
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If (CInt(Data(0)) = 1) Then
            Dim categoryName As String = CStr(Data(1))
            Dim ErrorFlag As Integer = 0
            Dim Message As String = ""
            Try
                Dim Params(2) As SqlParameter
                Params(0) = New SqlParameter("@categoryName", SqlDbType.VarChar, 50)
                Params(0).Value = categoryName
                Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(1).Direction = ParameterDirection.Output
                Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(2).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_BK_AT_CREATE_category", Params)
                ErrorFlag = CInt(Params(1).Value)
                Message = CStr(Params(2).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message

        End If
    End Sub
#End Region
End Class




