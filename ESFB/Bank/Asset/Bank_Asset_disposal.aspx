﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="Bank_Asset_disposal.aspx.vb" Inherits="Bank_Asset_disposal" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
                  
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ñ");
       
                for (a = 0; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function ClearCombo(control) {
                document.getElementById(control).options.length = 0;
                var option1 = document.createElement("OPTION");
                option1.value = -1;
                option1.text = " -----Select-----";
                document.getElementById(control).add(option1);
            }
            function FromServer(arg, context) {
                if (context == 4) {
                    var Data = arg.split("Ø");
                    if (arg == "ØØ") 
                    {
                        alert("Invalid Asset");
                        document.getElementById("<%= cmbBranch.ClientID%>").value = "-1";
                        
                        document.getElementById("<%= cmbCategory.ClientID%>").value = "-1";
                        document.getElementById("<%= cmbItem.ClientID%>").value = "-1";
                        document.getElementById("<%= txtMake.ClientID%>").value = "";
                        document.getElementById("<%= txtModel.ClientID%>").value = "";
                        document.getElementById("<%= txtSerialNo.ClientID%>").value = "";
                        document.getElementById("<%= txtStatus.ClientID%>").value = "";
                        document.getElementById("txtDetails").value = "";
           
                    }
                    else
                    { 
                        document.getElementById("<%= txtMake.ClientID%>").value = Data[1];
                        document.getElementById("<%= txtModel.ClientID%>").value =  Data[2];
                        document.getElementById("<%= txtSerialNo.ClientID%>").value =  Data[3];
                        document.getElementById("<%= txtStatus.ClientID%>").value = Data[4];
                       }
                }
                else if (context == 3) {
                     var Data = arg;
                    ComboFill(Data, "<%= cmbItem.ClientID%>");
                }
               
                else if (context == 1) {
                     var Data = arg;
                    ComboFill(Data, "<%= cmbAsset.ClientID%>");
                }
        
            }
            function btnExit_onclick() {
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }

            function RequestOnClick() {   
       
                if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1") 
                {
                    alert("Select Branch");
                    document.getElementById("<%= cmbBranch.ClientID %>").focus();
                        return false;
                    }
                    if (document.getElementById("<%= cmbCategory.ClientID %>").value == "-1") 
                    {
                        alert("Select Category");
                        document.getElementById("<%= cmbCategory.ClientID %>").focus();
                        return false;
                    }
                     if (document.getElementById("<%= cmbItem.ClientID %>").value == "-1") 
                    {
                        alert("Select Item");
                        document.getElementById("<%= cmbItem.ClientID %>").focus();
                        return false;
                    }
                     if (document.getElementById("<%= cmbAsset.ClientID %>").value == "-1") 
                    {
                        alert("Select Asset");
                        document.getElementById("<%= cmbAsset.ClientID %>").focus();
                        return false;
                    }
                    
        
                        if (document.getElementById("txtDetails").value == "")
                {
                    alert("Enter Description");
                    document.getElementById("txtDetails").focus();
                    return false;
                }  
     

    
                var Branch	= document.getElementById("<%= cmbBranch.ClientID %>").value;
                
 
                var Asset_ID	= document.getElementById("<%= cmbAsset.ClientID %>").value;
    
                var Descr	= document.getElementById("txtDetails").value; 
    
                document.getElementById("<%= hdnValue.ClientID %>").value = "10Ø" + Branch + "Ø" +  Asset_ID + "Ø" + Descr;       
     
            }
            function ItemOnChange() 
            {   var Item_ID	= document.getElementById("<%= cmbItem.ClientID%>").value;
            var Branch	= document.getElementById("<%= cmbBranch.ClientID %>").value;
        
                    if (Item_ID != "")
                        ToServer("1Ø" + Item_ID+"Ø"+Branch, 1);       
             }
            function AssetOnChange() 
            {   var Asset_ID	= document.getElementById("<%= cmbAsset.ClientID%>").value;
        
                    if (Asset_ID != "")
                        ToServer("4Ø" + Asset_ID, 4);       
             }
           
             function CategoryOnChange() 
            {   var Category_ID	= document.getElementById("<%= cmbCategory.ClientID%>").value;
        
                    if (Category_ID != "")
                        ToServer("3Ø" + Category_ID, 3);       
             }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return window_onload()
            // ]]>
        </script>
    </head>
    </html>
   
    <br />
    <br />
    <table class="style1" style="width: 80%; margin: 0px auto;">
        <tr id="branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black" >
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
        <tr id="Cat">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                category</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbCategory" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Item">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Item</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbItem" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
        
        <tr id="Asset">
            <td style="width: 25%;">
                &nbsp;
                   <asp:HiddenField ID="hid_Empcode" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
               Asset Details
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbAsset" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Tr2">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Make
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtMake" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="60%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
            </td>
        </tr>
        <tr id="Model">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Model
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtModel" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="60%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
            </td>
        </tr>
         <tr  id="SerialNo">
        <td style="width:25%;">
                   <asp:HiddenField ID="hid_Dtls" runat="server" />
                   </td>
            <td style="width:12% ; text-align:left;">
                SerialNo</td>
            <td style="width:63% ;text-align:left;">
               &nbsp; &nbsp;<asp:TextBox ID="txtSerialNo" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        
         <tr id="Status">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Asset Status</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtStatus" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>

         
        
        <tr id="Tr1">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_Post" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Description
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<textarea id="txtDetails" class="NormalText" cols="20" name="S1" rows="3" onkeypress='return TextAreaCheck(event)' 
                    maxlength="100" style="width: 70%"></textarea>
            </td>
        </tr>
              
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:HiddenField ID="hdn_RetailFlg" runat="server" />
                <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
