﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="Bank_Asset_Details_Approval.aspx.vb" Inherits="Bank_Asset_Details_Approval" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function table_fill() {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:1%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:6%;text-align:left' >Type</td>";
            tab += "<td style='width:9%;text-align:left' >Asset Tag</td>";
            tab += "<td style='width:4%;text-align:left'>Image</td>";  
            tab += "<td style='width:6%;text-align:left' >Branch</td>";
            tab += "<td style='width:7%;text-align:left'>Category</td>";
            tab += "<td style='width:7%;text-align:left'>Item</td>";
            tab += "<td style='width:6%;text-align:left'>Make</td>";
            tab += "<td style='width:7%;text-align:left'>Model</td>";
            tab += "<td style='width:6%;text-align:left'>SerialNo</td>";
            tab += "<td style='width:6%;text-align:left'>Current Status</td>";
            tab += "<td style='width:8%;text-align:left'>Transfer To</td>";
            tab += "<td style='width:8%;text-align:left'>Previous Remarks</td>";
            tab += "<td style='width:8%;text-align:left'>Approved By</td>";
            tab += "<td style='width:4%;text-align:center'>Action</td>";
            tab += "<td style='width:7%;text-align:center'>Remarks</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                    

                   

                   tab += "<td style='width:1%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:6%;text-align:left' >" + col[1] + "</td>";
                    tab += "<td style='width:9%;text-align:left' >" + col[8] + "</td>";
                     if(col[11]=="")     
                    {           
                    tab += "<td style='width:4%;text-align:left' class='NormalText'> </td>";
                    }
                    else
                    {

                    tab += "<td style='width:4%;text-align:left' class='NormalText'><img id='ViewAttachment' src='../../Image/attchment2.png' onclick='viewReport("+ i +")' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' /> <img id='ImgPhoto"+ i +" alt='' src='"+ col[16] +"' style='height:200px; width:200px;display:none;' title='' /></td>";
                    }
                                        
                    tab += "<td style='width:6%;text-align:left' >" + col[2] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[9] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[13] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[14] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[12] + "</td>";
                    
                    

                    var select = "<select id='cmb" + i + "' class='NormalText' name='cmb" + i + "' >";
                    select += "<option value='-1'>Select</option>";
                    select += "<option value='1'>Approved</option>";
                    select += "<option value='2'>Rejected</option>";

                    tab += "<td style='width:4%;text-align:left'>" + select + "</td>";


                    var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)'  ></textarea>";

                    tab += "<td style='width:7%;text-align:left'>" + txtBox + "</td>";


                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//


        }
        function viewReport(ID)
        {
       
                document.getElementById("ImgPhoto").style.display = '';
                document.getElementById("btnClear").style.display = '';
                document.getElementById("ImgPhoto").setAttribute('src',"");
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var n=0;
                for (i = 0; i <= row.length - 1; i++) {
                     n = i+1;
                    col = row[i].split("µ");  
                    if (n==ID)
                    document.getElementById("ImgPhoto").setAttribute('src',col[16]); 

                    }
            
        
            return false;
        }
        function UpdateValue() {
        
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("No pending Approval");
                return false;
            }

            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");
               var i=n+1;
                
                
                    if (document.getElementById("cmb" + i).value != -1) {
                    if (document.getElementById("txtRemarks" + i).value == ""  && document.getElementById("cmb" + i).value=="2") {
                        alert("Enter Rejected Reason");
                        document.getElementById("txtRemarks" + i).focus();
                        return false;
                    }
                    
                    
                    document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[0] + "µ"  + col[1]  + "µ"  + document.getElementById("cmb" + i).value + "µ" + document.getElementById("txtRemarks" + i).value + "µ" + col[15] ;
                }
            }

            return true;
        }
        function FromServer(arg, context) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("Bank_Asset_Details_Approval.aspx", "_self");
        }
        function btnApprove_onclick() {
            var ret = UpdateValue();
            if (ret == 0) return false;
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                alert("Select Any Request for Approval");
                return false;
            }

            var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
            var Data = "1Ø" + strempcode;
            ToServer(Data, 1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
         function btnClear_onclick(){
       document.getElementById("ImgPhoto").style.display = 'none';
       document.getElementById("btnClear").style.display = 'none';
       }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />
<table style="width:20%;margin: 0px auto;"><tr><td style="width:90%"><img id="ImgPhoto" alt="" src="" style="height:300px; width:1000px;display:none;"  title="" /></td> <td style="width:10%"><input id="btnClear" style="font-family: cambria; cursor: pointer; width: 100%;display:none;"  type="button" value="Clear"  onclick="return btnClear_onclick()" /></td></tr></table>
    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

