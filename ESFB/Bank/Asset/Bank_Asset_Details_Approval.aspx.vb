﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Bank_Asset_Details_Approval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1365) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            'Type,Branch,Category,Item,Model,SerialNo,Current Status,Transfer To,Previous Remarks,Approved By

            DT = DB.ExecuteDataSet("select a.[Asset_ID],'Transfer' as type,a.[Branch_id],c.category_name,b.item_name,d.make_name,[model],[serial_no],asset_tag," & _
                                   " e.status_id,k.val,k.content_type,'Created By:- '+isnull(g.emp_name,''),t.branch_name as transbranch,q.remarks,q.tran_id" & _
                                   " from BK_AT_transfer  q left join emp_master h on q.approved_by=h.emp_code left join branch_master t on q.to_branch=t.branch_id, [BK_AT_details] a left join  DMS_ESFB.dbo.BK_AT_Attachment k " & _
                                   " on a.asset_id=k.asset_id,BK_AT_item_master b,branch_master f,emp_master g,BK_AT_category_master c,BK_AT_make_master d,BK_AT_status_master e " & _
                                   " where(a.item_id = b.item_id And b.category_id = c.category_id And a.make_id = d.make_id) " & _
                                   " and a.asset_status=e.status_id and q.asset_id=a.asset_id and a.branch_id=f.branch_id and q.status_id=0 " & _
                                   " and q.created_by=g.emp_code and  q.branch_id=" & CInt(Session("BranchID")) & " " & _
                                   " Union all select a.[Asset_ID],'Disposal' as type,a.[Branch_id],c.category_name,b.item_name,d.make_name,[model],[serial_no],asset_tag," & _
                                   " e.status_id,k.val,k.content_type,'Created By:- '+isnull(g.emp_name,''),'' as transbranch,q.remarks,q.tran_id" & _
                                   " from BK_AT_Disposal  q left join emp_master h on q.approved_by=h.emp_code, [BK_AT_details] a left join  DMS_ESFB.dbo.BK_AT_Attachment k " & _
                                   " on a.asset_id=k.asset_id,BK_AT_item_master b,branch_master f,emp_master g,BK_AT_category_master c,BK_AT_make_master d,BK_AT_status_master e " & _
                                   " where(a.item_id = b.item_id And b.category_id = c.category_id And a.make_id = d.make_id) " & _
                                   " and a.asset_status=e.status_id and q.asset_id=a.asset_id and a.branch_id=f.branch_id and q.status_id=0 " & _
                                   " and q.created_by=g.emp_code and  q.branch_id=" & CInt(Session("BranchID")) & "").Tables(0)



            Me.Master.subtitle = "Transfer/Disposal Approval"



            Dim StrAttendance As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString
                If DT.Rows(n)(11).ToString <> "" Then
                    Dim bytes As Byte() = DirectCast(DT.Rows(n)(10), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                    StrAttendance += "µ" + Convert.ToString("data:" + DT.Rows(n)(11).ToString + ";base64,") & base64String
                Else
                    StrAttendance += "µ"
                End If
                If n < DT.Rows.Count - 1 Then
                    StrAttendance += "¥"
                End If
            Next


            hid_dtls.Value = StrAttendance
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim dataval As String = CStr(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))



        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try

            Dim Params(4) As SqlParameter
            Params(0) = New SqlParameter("@userID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(1).Value = BranchID
            Params(2) = New SqlParameter("@EMPDtl", SqlDbType.VarChar)
            Params(2).Value = dataval.Substring(1)
            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(3).Direction = ParameterDirection.Output
            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(4).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_BK_AT_APPROVAL", Params)
            ErrorFlag = CInt(Params(3).Value)
            Message = CStr(Params(4).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString



    End Sub
#End Region

End Class
