﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Bank_Asset_ItemMaster
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing

#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If GN.FormAccess(CInt(Session("UserID")), 1358) = False Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        Me.Master.subtitle = "Item Registration"
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "windows_onload();", True)
        DT = GN.GetQueryResult("select '-1' ,'--------------SELECT--------------' union all SELECT Category_ID,Category_Name from ESFB.dbo.BK_AT_CATEGORY_MASTER")
        GN.ComboFill(cmbCategory, DT, 0, 1)
        Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
        Me.chkSubAsset.Attributes.Add("onclick", "return CheckSubAsset()")
    End Sub

    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        '"1Ø" + VendorName + "Ø" + Address + "Ø" + ContactPerson + "Ø" + ContactNo + "Ø" + Email + "Ø" + Tin + "Ø" + Tan;
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If (CInt(Data(0)) = 1) Then
            Dim CategoryId As String = CStr(Data(1))
            Dim ItemName As String = CStr(Data(2))
            
            Dim ErrorFlag As Integer = 0
            Dim Message As String = ""

            Try
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@Category_ID", SqlDbType.Int)
                Params(0).Value = CategoryId
                Params(1) = New SqlParameter("@Item_Name", SqlDbType.VarChar, 50)
                Params(1).Value = ItemName
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_BK_AT_CREATE_ITEM", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message
        Else
            DT = DB.ExecuteDataSet("select Item_id,upper(Item_Name)  from ESFB.dbo.BK_AT_item_MASTER  where Status_ID=1 and category_id=" & CInt(Data(1)) & " order by 2").Tables(0)
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += DT.Rows(n)(0).ToString() + "µ" + DT.Rows(n)(1).ToString()
                If n < DT.Rows.Count - 1 Then
                    CallBackReturn += "¥"
                End If
            Next
        End If
    End Sub
#End Region
End Class




