﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Bank_Asset_disposal
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1363) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Asset Disposal"
            hid_Empcode.Value = CStr(Session("UserID"))
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0

            DT = DB.ExecuteDataSet(" select -1 as Branch_ID,' -----Select-----' as Branch_Name union all  select distinct Branch_ID,Branch_Name from BRANCH_MASTER").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
            cmbBranch.SelectedIndex = 0
            DT = DB.ExecuteDataSet("select -1 as category_ID,' -----Select-----' as category_name union all select category_ID,category_name from BK_AT_category_master where status_id = 1 order by category_Name").Tables(0)
            GN.ComboFill(cmbCategory, DT, 0, 1)
            cmbCategory.SelectedIndex = 0

            Me.cmbBranch.Attributes.Add("onchange", "return BranchOnChange()")
            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            Me.cmbItem.Attributes.Add("onchange", "return ItemOnChange()")
            Me.cmbAsset.Attributes.Add("onchange", "return AssetOnChange()")

            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "RequestOnchange();", True)

            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")

            cmbCategory.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim EmpID As Integer = CInt(Data(1))
        Dim strval As String
        Dim strfld As String
        Dim strfld1 As String
        Dim strwhere As String
        Dim StrVal2 As String = ""
        If CInt(Data(0)) = 1 Then 'fill asset
            DT = DB.ExecuteDataSet("select -1 as ID,' -----Select-----' as Name union all  select a.[Asset_ID] as ID,a.asset_tag + '[ ' +b.[item_name] +']' as Name from [BK_AT_details] a ,BK_AT_item_master b where a.item_id=b.item_id and a.asset_status=3 and a.item_id=" & Data(1).ToString & " and a.branch_id=" & Data(2).ToString & "").Tables(0)
            If (DT.Rows.Count > 0) Then
                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal2 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal2 += "Ñ"
                    End If
                Next
                CallBackReturn += StrVal2
            End If

        ElseIf CInt(Data(0)) = 4 Then 'fill asset details
            DT = DB.ExecuteDataSet("select a.[Asset_ID],d.[make_name],[model],[serial_no],e.status_name from [BK_AT_details] a left join " & _
                               " DMS_ESFB.dbo.BK_AT_Attachment k on a.asset_id=k.asset_id,BK_AT_make_master d,BK_AT_status_master e where  a.make_id=d.make_id and a.asset_status=e.status_id  and a.Asset_id=" & Data(1).ToString & "").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString() + "Ø" + DT.Rows(0)(6).ToString() + "Ø" + DT.Rows(0)(7).ToString() + "Ø" + DT.Rows(0)(8).ToString() + "Ø" + DT.Rows(0)(9).ToString() + "Ø" + DT.Rows(0)(10).ToString() + "Ø" + DT.Rows(0)(11).ToString()

            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf CInt(Data(0)) = 3 Then 'fill item


            DT = DB.ExecuteDataSet("select -1 as item_ID,' -----Select-----' as item_name union all select item_ID,item_name from BK_AT_item_master where status_id = 1 and category_id=" & Data(1).ToString & " order by item_Name").Tables(0)
            If (DT.Rows.Count > 0) Then
                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal2 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal2 += "Ñ"
                    End If
                Next
                CallBackReturn += StrVal2
            End If
               End If
    End Sub
#End Region

    Private Sub initializeControls()
        cmbBranch.Focus()
        cmbCategory.Text = "-1"
        cmbItem.Text = "-1"
        txtModel.Text = ""
        txtMake.Text = ""
        cmbBranch.Text = "-1"

        'txtdetails.Text = ""
        txtSerialNo.Text = ""
        txtStatus.Text = ""

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            'Branch + "Ø" + trans_branch_id + "Ø" + Asset_ID + "Ø" + Descr;      
            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim Branch As Integer = CInt(Data(1))

            Dim Asset_id As Integer = CInt(Data(2))
            Dim Descr As String = CStr(Data(3))

            Dim UserID As String = Session("UserID").ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try


                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
                Params(0).Value = Branch
                Params(1) = New SqlParameter("@asset_id", SqlDbType.Int)
                Params(1).Value = Asset_id
                Params(2) = New SqlParameter("@Remarks", SqlDbType.VarChar, 5000)
                Params(2).Value = Descr
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = UserID
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_BK_AT_DISPOSAL", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
                'REQUEST_ID = CInt(Params(9).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "'); ")
            cl_script1.Append("        window.open('Bank_Asset_disposal.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            If ErrorFlag = 0 Then
                initializeControls()
            End If



        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
