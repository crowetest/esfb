﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="Bank_Asset_New_Reg_Branch.aspx.vb" Inherits="Bank_Asset_New_Reg_Branch" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %> 
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
            function RequestOnchange() {
        
 
               document.getElementById("<%= txtModel.ClientID%>").value="";
               document.getElementById("<%= txtSerialNo.ClientID%>").value="";
               document.getElementById("<%= txtRemarks.ClientID %>").value="";
               document.getElementById("<%= txteffdate.ClientID%>").value ="";
           
           }
            
   
           function CategoryOnChange() {
               ClearCombo("<%= cmbItem.ClientID %>");
       
               var Groupid = document.getElementById("<%= cmbCategory.ClientID %>").value;
                if (Groupid > 0) {
                    var ToData = "1Ø" + Groupid; 
                    ToServer(ToData,1);
                }
            }
           
       
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ñ");
       
                for (a = 0; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function ClearCombo(control) {
                document.getElementById(control).options.length = 0;
                var option1 = document.createElement("OPTION");
                option1.value = -1;
                option1.text = " -----Select-----";
                document.getElementById(control).add(option1);
            }
            function FromServer(arg, context) {
                if (context == 1) {
                    var Data = arg;
                    ComboFill(Data, "<%= cmbItem.ClientID%>");
            document.getElementById("<%= cmbItem.ClientID%>").focus();

                }
                else if (context == 2) {
                    var Data = arg;
                    ComboFill(Data, "<%= cmbDepartment.ClientID%>");
            document.getElementById("<%= cmbCategory.ClientID%>").focus();

                }
       
             }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

    function RequestOnClick() {   
       
     if (document.getElementById("<%= cmbBranch.ClientID%>").value == "-1") 
        {
            alert("Select Branch");
            document.getElementById("<%= cmbBranch.ClientID%>").focus();
            return false;
        }
        
               
        if (document.getElementById("<%= cmbCategory.ClientID%>").value == "-1") 
        {
            alert("Select Application Type");
            document.getElementById("<%= cmbCategory.ClientID%>").focus();
            return false;
        }
      
        if (document.getElementById("<%= cmbItem.ClientID %>").value == "-1") 
        {
            alert("Select Item");
            document.getElementById("<%= cmbItem.ClientID %>").focus();
                return false;
            }
           if (document.getElementById("<%= cmbMake.ClientID %>").value == "-1") 
        {
            alert("Select Make");
            document.getElementById("<%= cmbMake.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= txtModel.ClientID%>").value == "") 
        {
       
            alert("Enter Model");
            document.getElementById("<%= txtModel.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%= txtSerialNo.ClientID%>").value == "") 
        {
       
              alert("Enter SerialNo");
              document.getElementById("<%= txtSerialNo.ClientID%>").focus();
              return false;
        }
       
  var Branch	= document.getElementById("<%= cmbBranch.ClientID%>").value;
    var Item	= document.getElementById("<%= cmbItem.ClientID%>").value;
    var Make	= document.getElementById("<%= cmbMake.ClientID%>").value;
    var Category	= document.getElementById("<%= cmbCategory.ClientID%>").value;
    var Model	= document.getElementById("<%= txtModel.ClientID%>").value;
    var Effdate	= document.getElementById("<%= txteffdate.ClientID%>").value;
    var SerialNo	= document.getElementById("<%= txtSerialNo.ClientID%>").value;
    var Remarks	= document.getElementById("<%= txtRemarks.ClientID%>").value;
     var Dep	= document.getElementById("<%= cmbDepartment.ClientID%>").value;
      var warrantstart	= document.getElementById("<%= txtWarrantyStart.ClientID%>").value;
       var warrantend	= document.getElementById("<%= txtWarrantyEnd.ClientID%>").value;

    document.getElementById("<%= hdnValue.ClientID %>").value = "1Ø" + Category + "Ø" + Item + "Ø" + Make + "Ø" + Model + "Ø" + SerialNo + "Ø" + Remarks + "Ø" + Effdate + "Ø" + Branch + "Ø" + Dep + "Ø" + warrantstart + "Ø" +warrantend;       
}
            function window_onload(){
                
            }
            function BranchOnChange() {
        
               
                if (document.getElementById("<%= cmbBranch.ClientID%>").value != "-1")
                {
                    var ToData = "2Ø" + document.getElementById("<%= cmbBranch.ClientID%>").value +"Ø"+ document.getElementById("<%= hdn_RetailFlg.ClientID%>").value; 
                    ToServer(ToData, 2);
                }
           
            }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return window_onload()
            // ]]>
        </script>
    </head>
    </html>
   
    <br />
    <br />
       <table class="style1" style="width: 80%; margin: 0px auto;">
         <tr id="Branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>   
        <tr id="Tr2">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Department
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbDepartment" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>   
        <tr id="cat">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Category
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbCategory" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="item">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Item</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbItem" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
         <tr id="make">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Make</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbMake" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="model">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
               Model
                     <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtModel" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15"  />
            </td>
        </tr>
        <tr id="serialno">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
              Serial No
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtSerialNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15"  />
            </td>
        </tr>
        
         
        
        <tr id="Tr1">
            <td style="width: 25%;">
               
            </td>
            <td style="width: 12%; text-align: left;">
                Remarks
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtRemarks" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="100"  />
            &nbsp;</td>
        </tr>
        
       
        <tr id="Img">
        <td style="width:25%;"><asp:HiddenField 
                    ID="hdn_Post" runat="server" />
                </td>
            <td style="width:12% ; text-align:left;">
                Attachments if any</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<input id="fup1" type="file" runat="server" 
                style="font-family: Cambria; font-size: 10.5pt" />
                </td>

        </tr>
      
      
        <tr id="Tr5">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Procurement Date
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txteffdate" class="NormalText" runat="server" Width="40%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CEFromDate" runat="server" TargetControlID="txteffdate" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
        </tr>
        <tr id="Tr3">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Warranty Start Date
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtWarrantyStart" class="NormalText" runat="server" Width="40%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtWarrantyStart" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
        </tr>
        <tr id="Tr4">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Warranty End Date
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtWarrantyEnd" class="NormalText" runat="server" Width="40%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtWarrantyEnd" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:HiddenField ID="hdn_RetailFlg" runat="server" />
                <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
