﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true"  CodeFile="Bank_Asset_Details_HO_Verification.aspx.vb" Inherits="Asset_Details_HO_Verification" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
   .fileUpload{
    width:255px;    
    font-size:11px;
    color:#000000;
    border:solid;
    border-width:1px;
    border-color:#7f9db9;    
    height:17px;
    }
            .ScrollClass
        {           
            text-align:center; color:#E31E24;
            background-color:#EBDDDD;
            height:20px;
           
        } 
        </style>

         <style> 
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
     
        function AlphaNumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = (e.which >= 48 && e.which <= 57) || (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || (e.which == 8) || (e.which == 13) || (e.which == 32) || (e.which == 0);

            if (!valid) {
                e.preventDefault();
            }
        }
        function NumericDotCheck(e)//------------function to check whether a value is alpha numeric
        {
            // el - this, e - event
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                    if (unicode == 37 || unicode == 38) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (unicode == 46) {
                        var parts = el.value.split('.');
                        if (parts.length == 2)
                            return false;
                        return true;
                    }
                }
            }
            else {
                return true;
            }
        }
        function NumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = ((e.which >= 48 && e.which <= 57) || (e.keyWhich == 9) || (e.keyCode == 9) || (e.keyWhich == 8) || (e.keyCode == 8));
            //alert(valid);
            if (!valid) {
                e.preventDefault();
            }

        }
   
    
        function attachment_onclick() {
            //alert(1);
        } 
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }    
        function table_fill() 
        {document.getElementById("<%= hdn_check.ClientID %>").value ="";
         if (document.getElementById("<%= hdn_dtls.ClientID %>").value != "") {
            document.getElementById("<%= pnlRequest.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>"; 
            tab += "<td style='width:13%;text-align:left'>AssetTag</td>"; 
            tab += "<td style='width:8%;text-align:left'>Category</td>"; 
            tab += "<td style='width:8%;text-align:left'>Item</td>";   
            tab += "<td style='width:8%;text-align:left'>Maker</td>";
            tab += "<td style='width:8%;text-align:left'>Model</td>";    
            tab += "<td style='width:8%;text-align:left'>Serial No</td>"; 
            tab += "<td style='width:8%;text-align:left'>Status</td>";    
            tab += "<td style='width:4%;text-align:left'>Image</td>";  
            tab += "<td style='width:4%;text-align:left'>View History</td>";  
            tab += "<td style='width:9%;text-align:left'>Parent Asset</td>";
            tab += "<td style='width:6%;text-align:left'>Approve/Reject</td>"; 
            tab += "<td style='width:6%;text-align:left'>Remarks</td>"; 
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
           //[Asset_ID],[Branch_id],c.[category_id],a.[item_id],a.[make_id],[model],[serial_no],[Entered_By],[Entered_Emp_name],[Asset_status],
           //[Branch_Verified_By],[Branch_verified_on],[HO_Verified_By],[HO_verified_on],[verify_flg],asset_tag,[change_dt],[remarks],[ho_remarks],b.item_name
           //,c.category_name,d.make_name
                row = document.getElementById("<%= hdn_dtls.ClientID %>").value.split("¥");
                var n=0;
                for (i = 0; i <= row.length - 1; i++) {
                     n = i+1;
                    col = row[i].split("µ");                    
                    
                     
                       
                    tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;background-color:#FFF;'>";
                    tab += "<td style='width:5%;text-align:center' >" + n  + "</td>"; 
                    
                                                  
                  
                   
                    
                     txtval = "<input id='txtAssettag" + n + "' name='txtAssettag" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='50' disabled=true value="+ col[15]+" />";                                            
                  

                    tab += "<td style='width:13%;text-align:left'>" + txtval + "</td>";
                    
                    
                      
                            
                    var select1 = CreateSelect(n,1,col[2],"","0") ;                                               
                    tab += "<td style='width:8%;text-align:left'>" + select1 + "</td>";
                   
                    
                    var select2 = CreateSelect(n,2,col[3],"","0") ;                                 
                    tab += "<td style='width:8%;text-align:left'>" + select2 + "</td>";
                   
                   
                        var select2 = CreateSelect(n,3,col[4],"","0")  ;                                
                        tab += "<td style='width:8%;text-align:left'>" + select2 + "</td>";
                      var chkval ="0";
                     
                      if (chkval=="0")
                        var txtval = "<input id='txtModel" + n + "' name='txtModel" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='50' disabled=true value="+ col[5]+" />";                                            
                      else
                        var txtval = "<input id='txtModel" + n + "' name='txtModel" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='50'  value="+ col[5]+" />";                                            

                    
                     tab += "<td style='width:8%;text-align:left'>" + txtval + "</td>";
                   
                   
                    if (chkval=="0")
                        var txtval = "<input id='txtSerialNo" + n + "' name='txtSerialNo" + n + "' type='Text' style='width:99%; height:99%;' class='NormalText' disabled=true maxlength='50' value="+ col[6]+" />";                                            
                    else
                        var txtval = "<input id='txtSerialNo" + n + "' name='txtSerialNo" + n + "' type='Text' style='width:99%; height:99%;' class='NormalText'  maxlength='50' value="+ col[6]+" />";                                                                                 

                     

                    tab += "<td style='width:8%;text-align:left'>" + txtval + "</td>";
               
                   
                    var select2 = CreateSelect(n,4,col[9],"","0")  ;                                
                    tab += "<td style='width:8%;text-align:left'>" + select2 + "</td>";
                    
                  
                     if(col[22]=="")     
                    {           
                    tab += "<td style='width:4%;text-align:left' class='NormalText'><img id='ViewAttachment' src='../../Image/Add.png' onclick='AddAttachment("+ col[0] +")' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' /> </td>";
                    }
                    else
                    {

                    tab += "<td style='width:4%;text-align:left' class='NormalText'><img id='ViewAttachment' src='../../Image/attchment2.png' onclick='viewReport("+ n +")' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' /> </td>";
                    }

                     if(col[0]=="")     
                    {           
                     var txtval = "<input id='txtView" + n + "' name='txtView" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='50'  />";                                            
                    tab += "<td style='width:4%;text-align:left'>" + txtval + "</td>";
                    }
                    else
                    {

                     tab += "<td style='width:4%;text-align:left' class='NormalText'><img id='ViewReport' src='../../Image/eswtClose2.png' onclick='viewHistory("+ col[0] +")' title='View History'  style='height:20px; width:20px;  cursor:pointer;' /></td>";
                    }

                     var select2 = CreateSelect(n,6,col[22],col[0],chkval)  ;                                
                    tab += "<td style='width:9%;text-align:left'>" + select2 + "</td>";
//                      
                   var select1 = "<select id='cmbVerify" + n + "' class='NormalText' name='cmbVerify" + n + "' style='width:100%' >"; 
                    select1 += "<option value='-1' > -----Select-----</option>";  
                    select1 += "<option value='1' >Approve</option>";  
                    select1 += "<option value='2' >Reject</option></select>";  
                    tab += "<td style='width:6%;text-align:left'>" + select1 + "</td>";
                    
                   
                                 
                    
                         tab += "<td style='width:6%;text-align:left' ><input id='txtRemarks" + n + "' name='txtRemarks" + n + "' type='Text' style='width:99%;' class='NormalText'  maxlength='100' /></td>";
                   
                   

                    tab += "</tr>";
                }
            
                tab += "</table></div></div></div>";
               
                document.getElementById("<%= pnlRequest.ClientID %>").innerHTML = tab;
                setTimeout(function() {
                document.getElementById("cmbVerify"+(n-1)).focus().select();
                }, 4);
            }
            else
            document.getElementById("<%= pnlRequest.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }
         
        function DeleteRow(id) {
            
            row = document.getElementById("<%= hdn_dtls.ClientID %>").value.split("¥");
            
            var NewStr = "";
             for (n =1; n <= row.length - 1 ; n++) {
                     
                     if (id != n) {

                         NewStr += "¥" + row[n];
                     }
                     else {
//                         var AcNo = document.getElementById("txtAcNo" + n).value;
//                         var Name = document.getElementById("txtAcName" + n).value;
//                         var Cheque = document.getElementById("txtChkNo" + n).value;
//                         var Amt = document.getElementById("txtAmt" + n).value;
//                         var TranID = (document.getElementById("txtTranID" + n).value == "") ? 0 : document.getElementById("txtTranID" + n).value;

//                         var strval = "¥" + AcNo + "µ" + Name + "µ" + Cheque + "µ" + Amt + "µ" + TranID + "µ1";
//                   
//                         if (TranID == 0 || TranID == "") {
//                             NewStr += "";
//                       
//                         }
//                         
//                         else if  (TranID > 0 )
//                         {
//                             NewStr += strval;
//                             }
//                         else {

//                             NewStr = "¥µµµµµ";
//                        }

                   
                 }

             }
             
            document.getElementById("<%= hdn_dtls.ClientID %>").value = NewStr; 
            if (document.getElementById("<%= hdn_dtls.ClientID %>").value == "¥µµµµ0µ1" || document.getElementById("<%= hdn_dtls.ClientID %>").value == "") {
                document.getElementById("<%= hdn_dtls.ClientID %>").value = "¥µµµµµ";
            }
                table_fill();
        }

         function CreateNewRow(e, val) {
            
            var n = (window.Event) ? e.which : e.keyCode;
            
            if (n == 13 || n == 9) {
                  row = document.getElementById("<%= hdn_dtls.ClientID %>").value.split("¥");
                    var Lenval = row.length;   
                //updateValue(val);
              
                if (Lenval==val)
                AddNewRow();
            }
        }
         
         function viewReport(ID)
        {
       
                document.getElementById("ImgPhoto").style.display = '';
                document.getElementById("btnClear").style.display = '';
                document.getElementById("ImgPhoto").setAttribute('src',"");
                row = document.getElementById("<%= hdn_dtls.ClientID %>").value.split("¥");
                var n=0;
                for (i = 0; i <= row.length - 1; i++) {
                     n = i+1;
                    col = row[i].split("µ");  
                    if (n==ID)
                    document.getElementById("ImgPhoto").setAttribute('src',col[25]); 

                    }
            
        
            return false;
        }
          function AddAttachment(ID)
        {
          
                //window.showModalDialog("Bank_Asset_Attachment.aspx?RequestID=" + btoa(ID),"",'dialogHeight:200px;dialogWidth:500px; dialogLeft:300;dialogTop:300; center:yes'); 
                window.open("Bank_Asset_Attachment.aspx?RequestID=" + btoa(ID),"_self");
            return false;
        }
         function viewHistory(ID)
        {
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
           
            window.open("Reports/ViewAssetApprovalPendingDetails.aspx?AssetID=" + btoa(ID)+"&BranchID="+ btoa(BranchID));
            
            return false;
        }
         function viewSummary()
        {
           var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
           
            window.open("Reports/ViewAssetSummary.aspx?BranchID=" + btoa(BranchID));
            
            return false;
        }
        function CreateSelect(n,id,value,checkID,chkvalue) { 
        
            if(id==1)
            {
             if(chkvalue=="0")
                var select1 = "<select id='cmbCategory" + n + "' class='NormalText' name='cmbCategory" + n + "' style='width:100%' disabled=true  >"; 
             else
                var select1 = "<select id='cmbCategory" + n + "' class='NormalText' name='cmbCategory" + n + "' style='width:100%'   >"; 
              
            var rows =document.getElementById("<%= hid_Category.ClientID %>") .value.split("Ñ");            
            for (a = 1; a < rows.length; a++) {
             var cols = rows[a].split("ÿ");
             if(cols[0]==value)
               select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
            else
                select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
            }
            }
            else if (id==2)
            {
              if(chkvalue=="0")
                var select1 = "<select id='cmbItem" + n + "' class='NormalText' name='cmbItem" + n + "' style='width:100%' disabled=true  >";
             else
                var select1 = "<select id='cmbItem" + n + "' class='NormalText' name='cmbItem" + n + "' style='width:100%'   >"; 
              
            var rows = document.getElementById("<%= hid_Item.ClientID %>").value.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
               if(cols[0]==value)
                    select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
                else
                    select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
                }
            }
            else if (id==3)
            {
               if(chkvalue=="0")
                var select1 = "<select id='cmbMaker" + n + "' class='NormalText' name='cmbMaker" + n + "' style='width:100%' disabled=true >"; 
             else
                var select1 = "<select id='cmbMaker" + n + "' class='NormalText' name='cmbMaker" + n + "' style='width:100%'  >"; 
              
            var rows = document.getElementById("<%= hid_Maker.ClientID %>").value.split("Ñ");
            for (a = 1; a < rows.length; a++) {
             var cols = rows[a].split("ÿ");
               if(cols[0]==value)
               select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
                else
                select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
                }
            }
            else if (id==4)
            {
                 if(chkvalue =="0")
                var select1 = "<select id='cmbStatus" + n + "' class='NormalText' name='cmbStatus" + n + "' style='width:100%' disabled=true >"; 
             else
                var select1 = "<select id='cmbStatus" + n + "' class='NormalText' name='cmbStatus" + n + "' style='width:100%'  >"; 
              
            var rows = document.getElementById("<%= hid_Status.ClientID %>").value.split("Ñ");
            for (a = 1; a < rows.length; a++) {
             var cols = rows[a].split("ÿ");
               if(cols[0]==value)
               select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
                else
                select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
                }
            }
             else if (id==5)
            {
               
               if(chkvalue =="0")
                var select1 = "<select id='cmbVerify" + n + "' class='NormalText' name='cmbVerify" + n + "' style='width:100%' disabled=true >";  
             else
            var select1 = "<select id='cmbVerify" + n + "' class='NormalText' name='cmbVerify" + n + "' style='width:100%'  >"; 
           
            var rows = document.getElementById("<%= hid_StatusVerify.ClientID %>").value.split("Ñ");
            for (a = 1; a < rows.length; a++) {
             var cols = rows[a].split("ÿ");
              
                select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
                    
                
                }
            }
             else if (id==6)
            {
               if(chkvalue=="0")
               var select1 = "<select id='cmbAssetMap" + n + "' class='NormalText' name='cmbAssetMap" + n + "' style='width:100%' disabled=true >"; 
             else
                var select1 = "<select id='cmbAssetMap" + n + "' class='NormalText' name='cmbAssetMap" + n + "' style='width:100%'  >"; 
           
            var rows = document.getElementById("<%= hid_AssetMap.ClientID %>").value.split("Ñ");
            for (a = 1; a < rows.length; a++) {
             var cols = rows[a].split("ÿ");
                    if(cols[0]==value)
                        select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
                    else
                        select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";   
                       
                           
                
                }
            }
            select1+="</select>"
          return select1;
        }
        function FromServer(arg, context) {

          switch (context) {

              case 2:
                  {
                     
                      document.getElementById("<%= hdn_dtls.ClientID %>").value = arg;
                      table_fill();
                      
                      break;
                  }

              case 1:
                  {
                      var Data = arg.split("Ø");
                      alert(Data[1]);
                      if (Data[0] == 0) window.open("Bank_Asset_Details_HO_verification.aspx", "_self");
                      break;
                  }

          }
        }
        

       function AddNewRow() {                          
                if (document.getElementById("<%= hdn_dtls.ClientID %>").value != "") {                      
                    row = document.getElementById("<%= hdn_dtls.ClientID %>").value.split("¥");
                    var Len = row.length;          
                    col = row[Len].split("µ");                      
                    if (col[0] != "-1" && col[1] != "-1" && col[2] != "") {
                    
                        document.getElementById("<%= hdn_dtls.ClientID %>").value += "¥µµ-1µ-1µ-1µµµµµµµµµµµµµµµµµµ";                          
                        
                    }
                    
                }
                else
                document.getElementById("<%= hdn_dtls.ClientID %>").value = "¥µµ-1µ-1µ-1µµµµµµµµµµµµµµµµµµ"; 

                //alert( document.getElementById("<%= hdn_dtls.ClientID %>").value);
                table_fill();
            }
            function chkSelectOnClick (id){
      
                    if (document.getElementById("chkSelect" + id).checked ==true){
                
                        document.getElementById("cmbVerify" + id).disabled=false;
                        document.getElementById("cmbAssetMap" + id).disabled=false;
                        document.getElementById("txtModel" + id).disabled=false;
                        document.getElementById("txtSerialNo" + id).disabled=false;
                        document.getElementById("txtRemarks" + id).disabled=false;
                    }
                    else {
                 
                        document.getElementById("cmbVerify" + id).disabled=true;
                        document.getElementById("cmbAssetMap" + id).disabled=true;
                        document.getElementById("txtModel" + id).disabled=true;
                        document.getElementById("txtSerialNo" + id).disabled=true;
                        document.getElementById("txtRemarks" + id).disabled=true;
                    }
            }
        function updateValue() {
            var NewStr = ""
           
            if (document.getElementById("<%= hdn_dtls.ClientID %>").value != "¥µµ-1µ-1µ-1µµµµµµµµµµµµµµµµµµ") {
               
                row = document.getElementById("<%= hdn_dtls.ClientID %>").value.split("¥");
               
               //alert(row.length);
               var id =0;
                for (n = 0; n < row.length ; n++) {
                   var col = row[n].split("µ");
                    id = n+1;
                       
                        if (document.getElementById("cmbVerify" + id).value!="-1"){

                           
                            var VerifyStatus = document.getElementById("cmbVerify" + id).value;
                            var Remarks = document.getElementById("txtRemarks" + id).value;

                       
                            NewStr += "¥" + col[0] + "µ" + Remarks + "µ" + VerifyStatus + "µ"  + document.getElementById("<%= hid_Brtype.ClientID %>").value;
                        }
                    
                }
                
           
            }
           
            document.getElementById("<%= hid_temp.ClientID %>").value = NewStr;
            //alert(document.getElementById("<%= hdn_dtls.ClientID %>").value);
        }
function btnSave_onclick() {
        

          if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1")
          { alert("Select Branch"); document.getElementById("<%= cmbBranch.ClientID %>").focus(); return false; }
          if (updateValue()==1){
            alert ("Atleast one data is needed for saving");
              return false;
          }
          ToServer("1Ø" + document.getElementById("<%= cmbBranch.ClientID %>").value+"Ø"+ document.getElementById("<%= hid_temp.ClientID %>").value, 1);
        }
        function RequestOnchange (){
        BranchOnChange();
        }
         function BranchOnChange() {
        
               
                if (document.getElementById("<%= cmbBranch.ClientID%>").value != "-1")
                {
                    var ToData = "2Ø" + document.getElementById("<%= cmbBranch.ClientID%>").value +"Ø"+ document.getElementById("<%= hid_Brtype.ClientID%>").value; 
                    ToServer(ToData, 2);
                }
           
            }
       
        function btnClear_onclick(){
       document.getElementById("ImgPhoto").style.display = 'none';
       document.getElementById("btnClear").style.display = 'none';
       }

    </script>
   
</head>
</html>

<br /><br />
<table style="width:20%;margin: 0px auto;"><tr><td style="width:90%"><img id="ImgPhoto" alt="" src="" style="height:300px; width:1000px;display:none;"  title="" /></td> <td style="width:10%"><input id="btnClear" style="font-family: cambria; cursor: pointer; width: 100%;display:none;"  type="button" value="Clear"  onclick="return btnClear_onclick()" /></td></tr></table>
 <table class="style1" style="width:100%;margin: 0px auto;" >
           
            
        <tr id="branch"> <td style="width:25%;">&nbsp;</td>
        <td style="width:15%; text-align:left;">Branch</td>
            <td style="width:63%">
                &nbsp; <asp:DropDownList ID="cmbBranch" class="NormalText" style="text-align: left; " runat="server" Font-Names="Cambria" text-align="top" Width="50%" Height="100%"  ForeColor="Black"  >
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    </asp:DropDownList>
                <img id='ViewAsset' src='../../Image/eswtClose2.png' onclick='viewSummary()' title='View Asset List'  style='height:20px; width:20px;  cursor:pointer;' />
                
            </td>
            
       </tr>
        <tr>
   
        <td style="width:25" colspan="3" ><asp:HiddenField ID="hdn_dtls" runat="server" />
                <asp:HiddenField ID="hdn_check" runat="server" />
                <asp:HiddenField ID="hid_temp" runat="server" /><asp:Panel ID="pnlRequest" runat="server" >
                </asp:Panel></td>
                
        </tr>
        
       <tr>
            <td style="text-align:center;" colspan="5"><br />
                <asp:HiddenField ID="hid_Category" 
                runat="server" />
                <asp:HiddenField ID="hid_ImgPhoto" 
                runat="server" />
                <asp:HiddenField ID="hid_Item" runat="server" />
                <asp:HiddenField ID="hid_Maker" runat="server" />
                <asp:HiddenField ID="hid_Status" runat="server" />
                <asp:HiddenField ID="hid_Brtype" runat="server" />
                <asp:HiddenField ID="hid_AssetMap" runat="server" />
                <asp:HiddenField ID="hid_StatusVerify" runat="server" />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 8%;" 
                type="button" value="APPROVE"   onclick="return btnSave_onclick()" onclick="return btnSave_onclick()" />&nbsp;
                 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />&nbsp;
                </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

