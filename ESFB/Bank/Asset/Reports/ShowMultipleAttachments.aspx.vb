﻿Imports System.Data
Partial Class ShowMultipleAttachments
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim dt As New DataTable
            Dim RequestID As Integer = CInt(GF.Decrypt(Request.QueryString.[Get]("RequestID")))
            dt = DB.ExecuteDataSet("SELECT val,content_Type,isnull(case when filename='' then 'Attachment' else filename end,'Attachment') FileName  FROM DMS_ESFB.dbo.BK_AT_Attachment where Asset_Id=" & RequestID & "").Tables(0)
            If dt IsNot Nothing Then
                If dt.Rows.Count > 0 Then
                    Dim bytes() As Byte = CType(dt.Rows(0)(0), Byte())
                    Response.Buffer = True
                    Response.Charset = ""
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.ContentType = dt.Rows(0)(1).ToString()
                    Response.AddHeader("content-disposition", "attachment;filename=" + dt.Rows(0)(2).ToString().Replace(" ", ""))
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()
                End If

            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected() Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
