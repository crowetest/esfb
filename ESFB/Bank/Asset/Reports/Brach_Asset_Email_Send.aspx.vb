﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Brach_Asset_Email_Send
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1322) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Asset Details Send"
            'hid_Empcode.Value = CStr(Session("UserID"))
            Dim BranchID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("BranchId")))
            Dim Branch As String = CStr(GF.Decrypt(Request.QueryString.Get("Branch")))
            Dim Email As String = CStr(GF.Decrypt(Request.QueryString.Get("Email")))
            Dim Brflag As String = CStr(GF.Decrypt(Request.QueryString.Get("Branchflag")))

            txtbranch.Text = Branch
            txtbranchcode.Text = CStr(BranchID)
            txtemail.Text = Email
            hdn_RetailFlg.Value = Brflag
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")

            txtemail.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
       
    End Sub
#End Region

    Private Sub initializeControls()
        txtbranch.text = ""
        txtbranchcode.text = ""
        txtemail.text = ""
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'Branch + "Ø" + trans_branch_id + "Ø" + Asset_ID + "Ø" + Descr;      
            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim Branchcode As Integer = CInt(Data(1))
            Dim Branch As String = CStr(Data(2))
            Dim email As String = CStr(Data(3))
            Dim Details As String = CStr(Data(4))
            Dim Brflag As String = CStr(Data(5))

            Dim UserID As String = Session("UserID").ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
                Params(0).Value = Branchcode
                Params(1) = New SqlParameter("@BRANCH", SqlDbType.VarChar)
                Params(1).Value = Branch
                Params(2) = New SqlParameter("@EMAIL", SqlDbType.VarChar, 5000)
                Params(2).Value = email
                Params(3) = New SqlParameter("@Message", SqlDbType.VarChar, 5000)
                Params(3).Value = Details
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = CInt(UserID)
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@brflag", SqlDbType.VarChar, 1)
                Params(7).Value = Brflag
                DB.ExecuteNonQuery("SP_BK_AT_PENDING_DETAILS_SEND", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
                'REQUEST_ID = CInt(Params(9).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "'); ")
            cl_script1.Append("        window.open('Brach_Asset_Email_Send.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            If ErrorFlag = 0 Then
                initializeControls()
            End If



        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
