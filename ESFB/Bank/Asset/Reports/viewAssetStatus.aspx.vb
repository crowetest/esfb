﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewAssetStatus
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim BranchID As Integer
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")
        BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))
        If BranchID = -1 Then

            RH.Heading(Session("FirmName"), tb, "ASSET STATUS REPORT -ALL BRANCH", 100)
        Else
            Dim BranchName As String = GN.GetBranch_Name(BranchID)
            RH.Heading(Session("FirmName"), tb, "ASSET STATUS REPORT " + BranchName + " BRANCH", 100)
        End If
        
        Dim RowBG As Integer = 0
        Dim DR As DataRow
        Dim DT As New DataTable
        DT = GetAssetDetails(BranchID)
        Dim TRHead As New TableRow
        Dim colWidth As Integer = 95 - ((DT.Columns.Count - 2) * 10)
        Dim TRHead_00 As New TableCell
        Dim TRHead_02 As New TableCell

        TRHead_00.BorderWidth = "1"
        TRHead_00.BorderColor = Drawing.Color.Silver
        TRHead_00.BorderStyle = BorderStyle.Solid
        RH.InsertColumn(TRHead, TRHead_02, 5, 2, "#")
        RH.InsertColumn(TRHead, TRHead_00, colWidth, 0, "Item")
        For i = 2 To DT.Columns.Count - 1
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_01 As New TableCell
            TRHead_01.BorderWidth = "1"
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderStyle = BorderStyle.Solid
            RH.InsertColumn(TRHead, TRHead_01, 10, 2, DT.Columns(i).ColumnName)
        Next
        Dim wdth As Integer = 0
        Dim aln As String
        Dim Tot As Integer
        Dim dispText As String
        Dim total((DT.Columns.Count - 2)) As Integer
        tb.Controls.Add(TRHead)
        Dim StatusID As Integer
        Dim J As Integer = 0
        For Each DR In DT.Rows
            Dim TR01 As New TableRow
            Dim TR01_02 As New TableCell
            J += 1
            RH.InsertColumn(TR01, TR01_02, 5, 2, J.ToString())
            For i = 1 To DT.Columns.Count - 1
                TR01.BackColor = Drawing.Color.WhiteSmoke
                Dim TR01_01 As New TableCell

                TR01_01.BorderWidth = "1"
                TR01_01.BorderColor = Drawing.Color.Silver
                TR01_01.BorderStyle = BorderStyle.Solid
                Tot = 0

                If i = 1 Then
                    wdth = colWidth
                    aln = 0
                    dispText = DR(i).ToString()
                Else
                    wdth = 10
                    aln = 2
                    If (IsDBNull(total(i - 2))) Then
                        total(i - 2) = 0
                    End If
                    Select Case CInt(i)
                       
                        Case 2
                            StatusID = 1
                        Case 3
                            StatusID = 2
                        Case 4
                            StatusID = 3
                        Case 5
                            StatusID = 4
                        Case 6
                            StatusID = 5
                    End Select

                    If (Not IsDBNull(DR(i))) Then
                        dispText = "<a href = 'ViewAssetDetails.aspx?ItemID=" + GN.Encrypt(DR(0).ToString()) + "&BranchID=" + GN.Encrypt(BranchID.ToString()) + "&StatusID=" + GN.Encrypt(StatusID.ToString()) + "' style='text-align:right; cursor:pointer;' title='View Item' target='_blank'>" & DR(i).ToString() & "</a>"
                        Tot = CInt(DR(i))
                    Else
                        dispText = "-"
                    End If
                    total(i - 2) += Tot
                End If
                RH.InsertColumn(TR01, TR01_01, wdth, aln, dispText)
            Next
            tb.Controls.Add(TR01)
        Next
        Dim TRHead_End As New TableRow
        TRHead_End.Style.Add("Font-Weight", "Bold")
        Dim TRHead_End_00 As New TableCell
        Dim TRHead_End_02 As New TableCell
        TRHead_End_00.BorderWidth = "1"
        TRHead_End_00.BorderColor = Drawing.Color.Silver
        TRHead_End_00.BorderStyle = BorderStyle.Solid
        RH.InsertColumn(TRHead_End, TRHead_End_02, 5, 0, "")
        RH.InsertColumn(TRHead_End, TRHead_End_00, colWidth, 0, "Total")
        For i = 2 To DT.Columns.Count - 1
            TRHead_End.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_End_01 As New TableCell
            TRHead_End_01.BorderWidth = "1"
            TRHead_End_01.BorderColor = Drawing.Color.Silver
            TRHead_End_01.BorderStyle = BorderStyle.Solid
            If (total(i - 2) = 0) Then
                dispText = "-"
            Else
                dispText = total(i - 2)
            End If
            RH.InsertColumn(TRHead_End, TRHead_End_01, 10, 2, dispText)
        Next
        tb.Controls.Add(TRHead_End)
        RH.BlankRow(tb, 20)
        pnDisplay.Controls.Add(tb)
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)

        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub
    Public Function GetAssetDetails(ByVal BranchID As Integer) As DataTable
        Try
            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@BranchID", BranchID)}
            Return DB.ExecuteDataSet("SP_BK_AT_ASSET_DETAILS", Parameters).Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Class
