﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewAssetSummary
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Dim BranchID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("BranchID")))
        Dim DTHead As New DataTable
        Dim DT As New DataTable
        RH.Heading(CStr(Session("FirmName")), tb, "List Of Assets", 100)
        Dim RowBG As Integer = 0
        tb.Attributes.Add("width", "100%")
        RH.BlankRow(tb, 6)

        Dim TRHead1 As New TableRow
        TRHead1.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07 As New TableCell
        RH.InsertColumn(TRHead1, TRHead1_00, 10, 2, "Sl No.")
        RH.InsertColumn(TRHead1, TRHead1_01, 20, 0, "Category")
        RH.InsertColumn(TRHead1, TRHead1_02, 15, 0, "Item")
        RH.InsertColumn(TRHead1, TRHead1_03, 15, 0, "Make")
        RH.InsertColumn(TRHead1, TRHead1_04, 10, 0, "Model")
        RH.InsertColumn(TRHead1, TRHead1_05, 10, 0, "SerialNo")
        RH.InsertColumn(TRHead1, TRHead1_06, 10, 0, "Asset Status")
        RH.InsertColumn(TRHead1, TRHead1_07, 10, 0, "Approval/Updation pending By")
        tb.Controls.Add(TRHead1)

        Dim sqlpart As String = ""
        If BranchID > 0 Then
            sqlpart = "and a.branch_id=" & BranchID & " "
        
        End If
        DT = DB.ExecuteDataSet("select c.category_name,b.item_name,d.make_name,[model],[serial_no],e.status_name,case when verify_flg in(1,3) then 'Branch'  when verify_flg=2 then 'HO' else '' end ,[Asset_tag],[Branch_id],c.[category_id],a.[item_id],a.[make_id],[Delivery_dt],[asset_id],[Entered_Emp_name],[department],[department_id],[Asset_status],[Branch_Verified_By],[Branch_verified_on],[HO_Verified_By],[HO_verified_on],[verify_flg],[change_dt],[remarks],[ho_remarks] from [BK_AT_details] a,BK_AT_item_master b, " & _
                " BK_AT_category_master c,BK_AT_make_master d,BK_AT_status_master e where a.item_id=b.item_id and b.category_id=c.category_id and a.make_id=d.make_id and a.asset_status=e.status_id " & sqlpart & " ").Tables(0)
        Dim dr As DataRow
        Dim n As Integer = 1
        For Each dr In DT.Rows
            Dim TRHead2 As New TableRow
            TRHead2.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08 As New TableCell
            RH.InsertColumn(TRHead2, TRHead2_00, 10, 2, n)
            RH.InsertColumn(TRHead2, TRHead2_02, 20, 0, dr(0))
            RH.InsertColumn(TRHead2, TRHead2_03, 15, 0, dr(1))
            RH.InsertColumn(TRHead2, TRHead2_04, 15, 0, dr(2))
            RH.InsertColumn(TRHead2, TRHead2_05, 10, 0, dr(3))
            RH.InsertColumn(TRHead2, TRHead2_06, 10, 0, dr(4))
            RH.InsertColumn(TRHead2, TRHead2_07, 10, 0, dr(5))
            RH.InsertColumn(TRHead2, TRHead2_08, 10, 0, "<a href = 'ViewAssetApprovalPendingDetails.aspx?AssetID=" + GF.Encrypt(dr(13).ToString()) + "&BranchID=" + GF.Encrypt(dr(8).ToString()) + "' style='text-align:right; cursor:pointer;' title='View Item' target='_blank'>" & dr(6).ToString() & "</a>")
            tb.Controls.Add(TRHead2)
            n = n + 1
        Next
        pnDisplay.Controls.Add(tb)
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)
        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
