﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewAssetApprovalPendingDetails
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Dim BranchID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("BranchID")))
        Dim AssetID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("AssetID")))


        Dim DTHead As New DataTable
        Dim DT As New DataTable

        RH.Heading(CStr(Session("FirmName")), tb, "List Of Assets -Pending Approval", 100)
        Dim RowBG As Integer = 0
        tb.Attributes.Add("width", "100%")
        RH.BlankRow(tb, 6)

        Dim TRHead1 As New TableRow
        TRHead1.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10 As New TableCell
        RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "Sl No.")
        RH.InsertColumn(TRHead1, TRHead1_01, 15, 0, "Asset Tag")
        RH.InsertColumn(TRHead1, TRHead1_02, 10, 0, "Make")
        RH.InsertColumn(TRHead1, TRHead1_03, 10, 0, "Model")
        RH.InsertColumn(TRHead1, TRHead1_04, 10, 0, "Serial No")
        RH.InsertColumn(TRHead1, TRHead1_05, 10, 0, "Asset Map ID")
        RH.InsertColumn(TRHead1, TRHead1_06, 5, 0, "Current Status")
        RH.InsertColumn(TRHead1, TRHead1_07, 10, 0, "New Model")
        RH.InsertColumn(TRHead1, TRHead1_08, 10, 0, "New Serial No")
        RH.InsertColumn(TRHead1, TRHead1_09, 10, 0, "New Asset Map ID")
        RH.InsertColumn(TRHead1, TRHead1_10, 5, 0, "New Status")

        tb.Controls.Add(TRHead1)
      
        DT = DB.ExecuteDataSet("select a.ASSET_TAG,d.Make_Name,a.MODEL,a.serial_no,isnull(x.asset_tag,''),g.status_name,n.MODEL,n.serial_no,isnull(y.asset_tag,''),k.status_name,a.asset_id from [BK_AT_details_daily] a left join BK_AT_details x on a.mapassetid=x.asset_id,[BK_AT_details] n   left join BK_AT_details y on n.mapassetid=y.asset_id ,BK_AT_item_master b,  BK_AT_category_master c,BK_AT_make_master d,BK_AT_status_master g,BK_AT_status_master k where a.asset_id=n.asset_id and n.asset_status=k.status_id and a.item_id=b.item_id and b.category_id=c.category_id and a.make_id=d.make_id and a.asset_status=g.status_id  and a.branch_id= " & BranchID & "  and a.asset_id = " & AssetID.ToString & " ").Tables(0)
        Dim dr As DataRow
        Dim n As Integer = 1
        For Each dr In DT.Rows
            Dim TRHead2 As New TableRow
            TRHead2.BackColor = Drawing.Color.White
           
            Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08, TRHead2_09, TRHead2_10 As New TableCell
            TRHead2_07.BackColor = Drawing.Color.LightYellow
            TRHead2_08.BackColor = Drawing.Color.LightYellow
            TRHead2_09.BackColor = Drawing.Color.LightYellow
            TRHead2_10.BackColor = Drawing.Color.LightYellow
            TRHead2_07.ForeColor = Drawing.Color.Red
            TRHead2_08.ForeColor = Drawing.Color.Red
            TRHead2_09.ForeColor = Drawing.Color.Red
            TRHead2_10.ForeColor = Drawing.Color.Red
            RH.InsertColumn(TRHead2, TRHead2_00, 5, 2, n)
            RH.InsertColumn(TRHead2, TRHead2_01, 15, 0, dr(0))
            RH.InsertColumn(TRHead2, TRHead2_02, 10, 0, dr(1))
            RH.InsertColumn(TRHead2, TRHead2_03, 10, 0, dr(2))
            RH.InsertColumn(TRHead2, TRHead2_04, 10, 0, dr(3))
            RH.InsertColumn(TRHead2, TRHead2_05, 10, 0, dr(4))
            RH.InsertColumn(TRHead2, TRHead2_06, 5, 0, dr(5))
            RH.InsertColumn(TRHead2, TRHead2_07, 10, 0, dr(6))
            RH.InsertColumn(TRHead2, TRHead2_08, 10, 0, dr(7))
            RH.InsertColumn(TRHead2, TRHead2_09, 10, 0, dr(8))
            RH.InsertColumn(TRHead2, TRHead2_10, 5, 0, dr(9))
            tb.Controls.Add(TRHead2)
            n = n + 1
        Next

        pnDisplay.Controls.Add(tb)
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)

        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
