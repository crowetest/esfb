﻿Imports System.Data
Partial Class Bank_Asset_Report
    Inherits System.Web.UI.Page
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Asset Status"

        GN.ComboFill(cmbBranch, GetBranch(CInt(Session("BranchID"))), 0, 1)
      
        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "DisplaySettings();", True)
    End Sub
    Public Function GetBranch(id As Integer) As DataTable
        Dim strval As String
        If id > 1000 Then
            Return DB.ExecuteDataSet("select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id=1 and branch_id=" & id & " order by 2").Tables(0)
        Else
            Return DB.ExecuteDataSet("select '-1' as branchid,' ------SELECT------' as branch Union All select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id > 0 order by 2").Tables(0)
        End If

    End Function
End Class
