﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewAssetpendingclose
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")

        Dim DTHead As New DataTable
        Dim DT As New DataTable
        RH.Heading(CStr(Session("FirmName")), tb, "Pending Approval List", 100)
        Dim RowBG As Integer = 0
        tb.Attributes.Add("width", "100%")
        RH.BlankRow(tb, 6)

        Dim TRHead1 As New TableRow
        TRHead1.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06 As New TableCell
        RH.InsertColumn(TRHead1, TRHead1_00, 10, 2, "Sl No.")
        RH.InsertColumn(TRHead1, TRHead1_01, 20, 0, "BranchID")
        RH.InsertColumn(TRHead1, TRHead1_02, 30, 0, "Branch Name")
        RH.InsertColumn(TRHead1, TRHead1_03, 10, 0, "Branch level pending count")
        RH.InsertColumn(TRHead1, TRHead1_04, 10, 0, "HO level pending count")
        RH.InsertColumn(TRHead1, TRHead1_05, 10, 0, "Send To")
        RH.InsertColumn(TRHead1, TRHead1_06, 10, 0, "Send To")
        tb.Controls.Add(TRHead1)
        Dim BranchID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("BranchID")))
        Dim sqlpart As String = ""
        Dim Branchcount As Integer = 0
        Dim Headofficecount As Integer = 0
        Dim Headoffice As String = ""
        Dim Headofficecode As String = ""
        Dim Hemail As String = ""

        If BranchID > 0 Then
            sqlpart = "and a.branch_id=" & BranchID & " "

        End If
        DT = DB.ExecuteDataSet("SELECT a.branch_id,b.branch_name,sum(case when verify_flg in (1,3) then 1 else 0 end) as 'Branch Level Pending',sum(case when verify_flg=2 then 1 else 0 end) as'HO Level Pending',b.email  FROM  bk_at_details a,branch_master b " & _
                " where  a.branch_id=b.branch_id and verify_flg in (1,2,3) group by a.branch_id,b.branch_name,b.email " + sqlpart).Tables(0)
        Dim dr As DataRow
        Dim n As Integer = 1
        For Each dr In DT.Rows
            Dim TRHead2 As New TableRow
            TRHead2.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06 As New TableCell
            If (CInt(dr(0).ToString()) = 0) Then
                Headoffice = dr(1).ToString()
                Headofficecode = dr(0).ToString()
                Hemail = dr(4).ToString()

            End If
            RH.InsertColumn(TRHead2, TRHead2_00, 10, 2, n)
            RH.InsertColumn(TRHead2, TRHead2_01, 20, 0, dr(0))
            RH.InsertColumn(TRHead2, TRHead2_02, 30, 0, dr(1))
            RH.InsertColumn(TRHead2, TRHead2_03, 10, 0, "<a href = 'ViewAssetBrachList.aspx?BranchID=" + GF.Encrypt(dr(0).ToString()) + "&Flag=" + GF.Encrypt(1) + "' style='text-align:right; cursor:pointer;' title='View Asset Details' target='_blank'>" & dr(2).ToString() & "</a>")
            RH.InsertColumn(TRHead2, TRHead2_04, 10, 0, "<a href = 'ViewAssetBrachList.aspx?BranchID=" + GF.Encrypt(dr(0).ToString()) + "&Flag=" + GF.Encrypt(2) + "' style='text-align:right; cursor:pointer;' title='View Asset Details' target='_blank'>" & dr(3).ToString() & "</a>")
            RH.InsertColumn(TRHead2, TRHead2_05, 10, 0, "<a href = 'Brach_Asset_Email_Send.aspx?BranchID=" + GF.Encrypt(dr(0).ToString()) + "&Branch=" + GF.Encrypt(dr(1).ToString()) + "&Email=" + GF.Encrypt(dr(4).ToString()) + "&Branchflag=" + GF.Encrypt(0) + "' style='text-align:right; cursor:pointer;' title='View Asset Details' target='_blank' >  Branch" & "</a>")
            RH.InsertColumn(TRHead2, TRHead2_06, 10, 0, "<a href = 'Brach_Asset_Email_Send.aspx?BranchID=" + GF.Encrypt(Headofficecode) + "&Branch=" + GF.Encrypt(Headoffice) + "&Email=" + GF.Encrypt(Hemail) + "&Branchflag=" + GF.Encrypt(1) + "' style='text-align:right; cursor:pointer;' title='View Asset Details' target='_blank' >  HO " & "</a>")

            tb.Controls.Add(TRHead2)
            Branchcount = Branchcount + dr(2).ToString()
            Headofficecount = Headofficecount + dr(3).ToString()
            n = n + 1
        Next
        pnDisplay.Controls.Add(tb)
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)
        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
