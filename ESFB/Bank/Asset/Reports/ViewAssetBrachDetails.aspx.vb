﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewAssetHistory
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Dim DTHead As New DataTable
        Dim DT As New DataTable

        Dim ID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("BranchID")))
        'Dim ID As Integer = CInt(Request.QueryString.Get("BranchID"))

        DTHead = DB.ExecuteDataSet("select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id=1 and branch_id=" & ID & " order by 2").Tables(0)
        Dim Assettag As String = DTHead.Rows(0)(1).ToString

        RH.Heading(CStr(Session("FirmName")), tb, "Branch Assets Details" + " - " + Assettag, 100)
        Dim RowBG As Integer = 0
        tb.Attributes.Add("width", "100%")
        RH.BlankRow(tb, 6)

        Dim TRHead1 As New TableRow
        TRHead1.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10, TRHead1_11, TRHead1_12, TRHead1_13, TRHead1_14 As New TableCell
        RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "Sl No.")
        RH.InsertColumn(TRHead1, TRHead1_01, 8, 0, "Category")
        RH.InsertColumn(TRHead1, TRHead1_02, 8, 0, "Item")
        RH.InsertColumn(TRHead1, TRHead1_03, 8, 0, "Make")
        RH.InsertColumn(TRHead1, TRHead1_04, 8, 0, "Model")
        RH.InsertColumn(TRHead1, TRHead1_05, 6, 0, "SerialNo")
        RH.InsertColumn(TRHead1, TRHead1_06, 6, 0, "Asset Status")
        RH.InsertColumn(TRHead1, TRHead1_07, 8, 0, "Branch Updated By")
        RH.InsertColumn(TRHead1, TRHead1_08, 6, 0, "Branch Updated On")
        RH.InsertColumn(TRHead1, TRHead1_09, 8, 0, "Branch Remarks")
        RH.InsertColumn(TRHead1, TRHead1_10, 8, 0, "HO Updated By")
        RH.InsertColumn(TRHead1, TRHead1_11, 6, 0, "HO Updated On")
        RH.InsertColumn(TRHead1, TRHead1_12, 6, 0, "HO Approval Status")
        RH.InsertColumn(TRHead1, TRHead1_13, 8, 0, "HO Remarks")


        tb.Controls.Add(TRHead1)
        
        DT = DB.ExecuteDataSet("select isnull(c.category_name,''),isnull(b.item_name,''),isnull(d.make_name,''),isnull([model],''),isnull([serial_no],'')," & _
                               " isnull(convert(varchar,[Entered_By])+' - '+[Entered_Emp_name] ,''),isnull(convert(varchar,[Branch_Verified_By])+' - '+k.emp_name ,''),isnull([Branch_verified_on] ,''), " & _
                               " isnull([remarks] ,''),isnull(convert(varchar,[ho_Verified_By])+' - '+l.emp_name ,''),isnull([HO_verified_on] ,''),isnull([ho_remarks] ,''),isnull(g.status_name ,''),a.[Asset_ID],a.[Branch_id],c.[category_id],a.[item_id],a.[make_id],[Delivery_dt],[department],a.[department_id],  " & _
                               " [verify_flg],[change_dt],case when a.Ho_approved_flg=1 then 'Approved' when a.Ho_approved_flg=2 then 'Rejected' else 'Approval pending' end from [BK_AT_details_tmp] a left join  emp_master k on a.branch_verified_by =k.emp_code left join  emp_master l on a.ho_verified_by =l.emp_code ,BK_AT_item_master b, " & _
                " BK_AT_category_master c,BK_AT_make_master d,BK_AT_status_master g where a.item_id=b.item_id and b.category_id=c.category_id and a.make_id=d.make_id and a.Asset_status=g.status_id and a.Branch_id=" & ID & " order by change_dt").Tables(0)
        Dim dr As DataRow
        Dim n As Integer = 1
        For Each dr In DT.Rows
            Dim TRHead2 As New TableRow
            TRHead2.BackColor = Drawing.Color.WhiteSmoke
            

            Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08, TRHead2_09, TRHead2_10, TRHead2_11, TRHead2_12, TRHead2_13, TRHead2_14 As New TableCell
            RH.InsertColumn(TRHead2, TRHead2_00, 5, 2, n)
            RH.InsertColumn(TRHead2, TRHead2_01, 8, 0, dr(0))
            RH.InsertColumn(TRHead2, TRHead2_02, 8, 0, dr(1))
            RH.InsertColumn(TRHead2, TRHead2_03, 8, 0, dr(2))
            RH.InsertColumn(TRHead2, TRHead2_04, 8, 0, dr(3))
            RH.InsertColumn(TRHead2, TRHead2_05, 6, 0, dr(4))
            RH.InsertColumn(TRHead2, TRHead2_06, 6, 0, dr(12))
            RH.InsertColumn(TRHead2, TRHead2_07, 8, 0, dr(6))
            RH.InsertColumn(TRHead2, TRHead2_08, 6, 0, dr(7))
            RH.InsertColumn(TRHead2, TRHead2_09, 8, 0, dr(8))
            RH.InsertColumn(TRHead2, TRHead2_10, 8, 0, dr(9))
            RH.InsertColumn(TRHead2, TRHead2_11, 6, 0, dr(10))
            RH.InsertColumn(TRHead2, TRHead2_12, 6, 0, dr(23))
            RH.InsertColumn(TRHead2, TRHead2_13, 7, 0, dr(11))
            tb.Controls.Add(TRHead2)
            n = n + 1
        Next

        pnDisplay.Controls.Add(tb)
    End Sub


    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)
        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
