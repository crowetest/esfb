﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Bank_Asset_New_Reg_Branch
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1366) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "New Asset Registration- Branch"
            ' Me.CEFromDate.StartDate = CDate(Session("TraDt"))

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0

            Dim StrVal As String = ""

           
            DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER  where branch_id=" & CInt(Session("BranchID")) & "").Tables(0)
            hdn_RetailFlg.Value = "1"
            GN.ComboFill(cmbBranch, DT, 0, 1)
            DT = DB.ExecuteDataSet("select department_ID,department_name from department_master " & _
                                  " where status_id = 1 and department_id=0 order by department_Name").Tables(0)

            GN.ComboFill(cmbDepartment, DT, 0, 1)

            DT = DB.ExecuteDataSet("select -1 as category_ID,' -----Select-----' as category_name union all select category_ID,category_name from BK_AT_category_master where status_id = 1 order by category_Name").Tables(0)
            GN.ComboFill(cmbCategory, DT, 0, 1)

            DT = DB.ExecuteDataSet("select -1 as make_ID,' -----Select-----' as make_name union all select make_ID,make_name from BK_AT_make_master where status_id = 1 order by make_Name").Tables(0)
            GN.ComboFill(cmbMake, DT, 0, 1)







            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            Me.cmbBranch.Attributes.Add("onchange", "return BranchOnChange()")

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")

            cmbCategory.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim EmpID As Integer = CInt(Data(1))

        If CInt(Data(0)) = 1 Then
            DT = DB.ExecuteDataSet("select -1 as item_ID,' -----Select-----' as item_name union all select item_ID,item_name from BK_AT_item_master where status_id = 1 and category_id=" & Data(1).ToString & " order by item_Name").Tables(0)
            If (DT.Rows.Count > 0) Then
                Dim StrVal1 As String = ""

                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal1 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal1 += "Ñ"
                    End If
                Next

                Dim StrVal2 As String = ""
                CallBackReturn = StrVal1

            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf CInt(Data(0)) = 2 Then
            Dim BranchID As Integer = CInt(Data(1))
            If BranchID > 100 Then
                DT = DB.ExecuteDataSet("select department_ID,department_name from department_master " & _
                                   " where status_id = 1 and department_id=0 order by department_Name").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select department_ID,department_name from department_master " & _
                                   " where status_id = 1 and department_id=0 order by department_Name").Tables(0)
            End If

            If (DT.Rows.Count > 0) Then
                Dim StrVal1 As String = ""

                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal1 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal1 += "Ñ"
                    End If
                Next

                Dim StrVal2 As String = ""
                CallBackReturn = StrVal1

            Else
                CallBackReturn = "ØØ"
            End If


        End If
    End Sub
#End Region

    Private Sub initializeControls()

        cmbCategory.Text = "-1"
        cmbMake.Text = "-1"
        cmbItem.Text = "-1"
        txtModel.Text = ""
        txtSerialNo.Text = ""
        txteffdate.Text = ""


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim myFile As HttpPostedFile = fup1.PostedFile
            Dim FileLen As Integer = myFile.ContentLength
            Dim FileName As String = ""
            If (FileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(FileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, FileLen)
            End If
            Dim Result As String
            Dim UserID As String = Session("UserID").ToString()

            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim Category_ID As Integer = CInt(Data(1))
            Dim Branch As Integer = CInt(Data(8))
            Dim Item_ID As Integer = CInt(Data(2))
            Dim Make_ID As Integer = CInt(Data(3))
            Dim Type As String = CStr(hdn_RetailFlg.Value)
            Dim Model As String = CStr(Data(4))
            Dim SerialNo As String = CStr(Data(5))
            Dim Remarks As String = CStr(Data(6))

            Dim Effdate As Date = CDate(IIf(Data(7) = "", "01/01/1900", Data(7)))
            Dim warrantstart As Date = CDate(IIf(Data(10) = "", "01/01/1900", Data(10)))
            Dim warrantend As Date = CDate(IIf(Data(11) = "", "01/01/1900", Data(11)))
            Dim Department_ID As Integer = CInt(Data(9))



            Try
                Dim Params(17) As SqlParameter
                Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
                Params(0).Value = Branch
                Params(1) = New SqlParameter("@Category_ID", SqlDbType.Int)
                Params(1).Value = Category_ID
                Params(2) = New SqlParameter("@Item_ID", SqlDbType.Int)
                Params(2).Value = Item_ID
                Params(3) = New SqlParameter("@Make_ID", SqlDbType.Int)
                Params(3).Value = Make_ID
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = UserID
                Params(5) = New SqlParameter("@Model", SqlDbType.NVarChar, 100)
                Params(5).Value = Model
                Params(6) = New SqlParameter("@SerialNo", SqlDbType.NVarChar, 100)
                Params(6).Value = SerialNo
                Params(7) = New SqlParameter("@Remarks", SqlDbType.NVarChar, 100)
                Params(7).Value = Remarks
                Params(8) = New SqlParameter("@Type", SqlDbType.Int)
                Params(8).Value = Type
                Params(9) = New SqlParameter("@eff_date", SqlDbType.Date)
                Params(9).Value = Effdate
                Params(10) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(10).Direction = ParameterDirection.Output
                Params(11) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(11).Direction = ParameterDirection.Output
                Params(12) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
                Params(12).Value = AttachImg
                Params(13) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                Params(13).Value = ContentType
                Params(14) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                Params(14).Value = FileName
                Params(15) = New SqlParameter("@Department_ID", SqlDbType.Int)
                Params(15).Value = Department_ID
                Params(16) = New SqlParameter("@warrant_start", SqlDbType.Date)
                Params(16).Value = warrantstart
                Params(17) = New SqlParameter("@warrant_end", SqlDbType.Date)
                Params(17).Value = warrantend

                DB.ExecuteNonQuery("SP_BK_AT_NEW_REG_branch", Params)
                ErrorFlag = CInt(Params(10).Value)
                Message = CStr(Params(11).Value)
            Catch ex1 As Exception
                Message = ex1.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("        window.open('Bank_Asset_New_Reg_branch.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)





        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
