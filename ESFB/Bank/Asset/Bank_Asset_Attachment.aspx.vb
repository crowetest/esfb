﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class Bank_Asset_Attachment
    Inherits System.Web.UI.Page
    Dim EN As New Enrollment
    Dim Asset_id As Integer
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            Asset_id = CInt(GF.Decrypt(Request.QueryString.Get("RequestID")))
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim FileName As String
            Dim myFile As HttpPostedFile = fup1.PostedFile
            Dim nFileLen As Integer = myFile.ContentLength
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, nFileLen)
            End If
            Dim Result As String
            Try
                Result = Asset_Attachment(Asset_id, AttachImg, ContentType, FileName)
            Catch
                Result = "1ØError"
            End Try
            Dim arr() = Result.Split(CChar("Ø"))
            If CDbl(arr(0)) = 0 Then
                cl_script1.Append("         alert('" + arr(1) + "');window.open('Bank_Asset_Details.aspx','_self');")
            Else
                cl_script1.Append("         alert('" + arr(1) + "');")
            End If
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Private Function Asset_Attachment(ByVal AssetId As Integer, ByVal Attachment As Byte(), ByVal ContentType As String, ByVal FileName As String) As String
        Dim ErrorFlag As Integer = 0
        Dim Message As String = ""
        Try
            Dim Params(5) As SqlParameter
            Params(0) = New SqlParameter("@Asset_ID", SqlDbType.Int)
            Params(0).Value = AssetId
            Params(1) = New SqlParameter("@Attachment", SqlDbType.Image)
            Params(1).Value = Attachment
            Params(2) = New SqlParameter("@ContentType", SqlDbType.VarChar, 150)
            Params(2).Value = ContentType
            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(3).Direction = ParameterDirection.Output
            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(4).Direction = ParameterDirection.Output
            Params(5) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
            Params(5).Value = FileName
            DB.ExecuteNonQuery("SP_BK_AT_ATTACHMENT", Params)
            ErrorFlag = CInt(Params(3).Value)
            Message = CStr(Params(4).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

        End Try
        Return ErrorFlag.ToString() + "Ø" + Message
    End Function
End Class
