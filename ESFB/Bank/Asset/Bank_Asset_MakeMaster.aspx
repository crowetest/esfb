﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true"  CodeFile="Bank_Asset_MakeMaster.aspx.vb" Inherits="Bank_Asset_MakeMaster" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
         .fileUpload{
    width:255px;    
    font-size:11px;
    color:#000000;
    border:solid;
    border-width:1px;
    border-color:#7f9db9;    
    height:17px;
    }
            .ScrollClass
        {           
            text-align:center; color:#E31E24;
            background-color:#EBDDDD;
            height:20px;
           
        }
        .style2
     {
         width: 17%;
     }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
         
         function windows_onload()
            {
            
                tablefill_Make();
                
            }
        
         function FromServer(arg, context) 
         {
         if (context == 1) 
         {
            var Data = arg.split("Ø");
            alert(Data[1]);
            window.open("Bank_Asset_MakeMaster.aspx", "_self");
          }
         
         }
       

          
          function btnSave_onclick() 
          { 
              if (document.getElementById("<%= txtMakeName.ClientID %>").value == "") { alert("Enter Make Name"); document.getElementById("<%= txtMakeName.ClientID %>").focus(); return false; }
              var MakeName=document.getElementById("<%= txtMakeName.ClientID %>").value;
               row = document.getElementById("<%= hdnValue.ClientID %>").value.split("¥");  
                    for (n = 0; n <= row.length - 1; n++) {                    
                        col = row[n].split("µ"); 
                        var ExistVal = col[1].toLowerCase();
                        var Val = MakeName.toLowerCase();
                        ExistVal = ExistVal.replace(/([\s]+)/g, '');
                        Val = Val.replace(/([\s]+)/g, '');
                       
                        if (ExistVal == Val){
                        alert("Manufacture Already Exists");return false;
                        }
                     }
                         
              var ToData = "1Ø" + MakeName;
              ToServer(ToData, 1); 
              
        }
         
         function tablefill_Make()
            {
                document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
            
                var row_bg = 0;
                var tab = "";
                if (document.getElementById("<%= hdnValue.ClientID %>").value != "") {
                    tab += "<div style='background-color:#A34747;height:300px;width:50%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
                
                    tab += "<tr class=mainhead>";
                    tab += "<td style='width:5%;text-align:center;' >SLNo</td>";                
                    tab += "<td style='width:45%;text-align:left' >Manufacturer Name</td>";                 
                    tab += "</tr>";                    
               
                
                    row = document.getElementById("<%= hdnValue.ClientID %>").value.split("¥");  
                    for (n = 0; n <= row.length - 1; n++) {                    
                        col = row[n].split("µ");  
                                    
                        if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class=sub_first>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class=sub_second>";
                        }
                        i = n+1 ;                   
                        tab += "<td style='width:5%;text-align:center;' >" + i + "</td>";
                        tab += "<td style='width:45%;text-align:left;' >" + col[1] + "</td>";                    
  
                        tab += "</tr>"; 
                    } 
                }   
                tab += "</table></div>";            
                document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
            
            
            }
          
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }    
          
       
    </script>
   
</head>
</html>

<br /><br />

 <table class="style1" style="width:80%;margin: 0px auto;" >
           
        <tr id=""> <td style="width:5%;"></td><td style="width:18%"></td>
        <td style="text-align:right;" class="style2">Manufacturer Name&nbsp;&nbsp;&nbsp;</td>
            <td colspan="2">
                &nbsp; &nbsp;<asp:TextBox ID="txtMakeName" runat="server"  
                    style=" font-family:Cambria;font-size:10pt;" Width="50%" class="NormalText" 
                    MaxLength="50" />
            </td>
       </tr>
           
       
           
        <tr> <td style="width:5%;">&nbsp;</td><td style="width:18%">&nbsp;</td>
        <td style="text-align:left;" class="style2">&nbsp;</td>
            <td style="width:50%">
                &nbsp;</td>
            <td style="width:15%;">&nbsp;</td>
       </tr>
           
       
           
        <tr> <td colspan="5" style="text-align:center;">
                <asp:Panel ID="pnlDtls" runat="server" Width="100%" >
                </asp:Panel>
            </td>
       </tr>
           
       
           
        <tr> <td colspan="5" style="text-align:right;">
                &nbsp;</td>
       </tr>
       
       <tr>
            <td style="text-align:center;" colspan="5"><br />
                <input id="btnSave" style="font-family: Cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SAVE" onclick="return btnSave_onclick()" />&nbsp;
                 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
                </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

