﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CustomerIdentification.aspx.vb" Inherits="CustomerIdentification" EnableEventValidation="false"   %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
   
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
   <style>
      #loadingmsg   
      {
      width:60%;
      background: #fff; 
      padding: 0px;
      position: fixed;     
      z-index: 100;
      margin-left: 5%;
      margin-bottom: -25%;   
      margin-top: -5%;     
      }
      #loadingover {
      background: black;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }

   </style>
   <style type="text/css" media="print">
   @media print {
   .noprint{
      display: none !important;
   }
}

   </style>
    <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
      var Exp_Flag=0;
      var Min_Char=0;
        function AlphaNumericCheck(e)//------------function to check whether a value is alpha numeric
        {   
            var valid = (e.which >= 48 && e.which <= 57) || (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || (e.which == 8) || (e.which == 13) || (e.which == 32) || (e.which == 0);
    
            if (!valid) {
                e.preventDefault();
            }
        }

        function FromServer(arg, context) {
            switch (context) 
            {               
                 case 1:
                 {                    
                    document.getElementById("<%= hdnView.ClientID %>").value=arg;
                    FillData();
                    break;
                 }  
                 case 2:
                 {        
                    showSummary(arg);
                    break;
                 }              
            }
        }

        function FillData()
        {
            document.getElementById("btnSearch").disabled = false;
            document.getElementById("<%= pnlView.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = ""; 
            tab += "<div style='width:100%; height:auto;  margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            //tab += "<div style='width:100%; overflow:scroll; height:275px; text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr style='height:30px;'>";           
            tab += "<td style='width:5%;text-align:center;font-weight:bold; font-size:11pt; border:1px solid #FFF;'>SINo</td>";
            tab += "<td style='width:10%;text-align:center;font-weight:bold;font-size:11pt; border:1px solid #FFF;'>Branch Name</td>";
            tab += "<td style='width:10%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #FFF;'>New CIF</td>";
            tab += "<td style='width:15%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #FFF;'>Name</td>";
            tab += "<td style='width:10%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #FFF;'>Dob</td>";
            tab += "<td style='width:10%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #FFF;'>PAN No</td>";     
            tab += "<td style='width:10%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #FFF;'>Adhaar No</td>";            
            tab += "<td style='width:15%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #FFF;'>Address</td>";
            tab += "<td style='width:15%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #FFF;'>Mobile No</td>";
            tab += "</tr>";  
            var row;           
            if (document.getElementById("<%= hdnView.ClientID %>").value != "") {
                row = document.getElementById("<%= hdnView.ClientID %>").value.split("¥");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class='sub_first' style='height:25px;' >";
                    }
                    else {
                        row_bg = 0; 
                        tab += "<tr class='sub_first' style='height:25px;'>";
                    }                    
                    tab += "<td style='width:5%;text-align:center' >" + n + "</td>";
                    tab += "<td style='width:10%;text-align:center' >" + col[1] + "</td>";  
                    tab += "<td style='width:10%;text-align:left' ><a href='' onclick='return ClientOnClick("+ col[4] +")'>" + col[4] + "</a></td>"; 
                    tab += "<td style='width:15%;text-align:left' >" + col[6] + "</td>";   
                    tab += "<td style='width:10%;text-align:left'>" + col[7] + "</td>";                   
                    tab += "<td style='width:10%;text-align:left'>" + col[8] + "</td>";                      
                    tab += "<td style='width:10%;text-align:left'>" + col[9] + "</td>";                    
                    tab += "<td style='width:15%;text-align:left'>" + col[10]  + "</td>";
                    tab += "<td style='width:15%;text-align:left'>" + col[3] + "</td>";
                    tab += "</tr>"; 
                }
                 
            }         
            
            tab += "</table></div>"; 
            document.getElementById("<%= pnlView.ClientID %>").innerHTML = tab;
            
        }

       function ClientOnClick(ClientID)
       {
            var Data = "2Ø" + ClientID;
            ToServer(Data, 2);
            return false;
       }
     

      function showSummary(DispData)
      {
          document.getElementById("btnSearch").disabled = false;
         var row_bg = 0;
            var tab = "";
           if (DispData != "") {
            var Data=DispData.split("Ø");
            var CustDtl=Data[0].split("µ");
            tab += "<div style='width:100%; height:auto;  margin: 0px auto;' class=sub_first><img style='float:right; padding-right:5px;width:30px;height:30px; cursor:pointer;' src='../Image/close.png' onclick='closeLoading()'>&nbsp;&nbsp;<img style='float:right; padding-right:5px;width:30px;height:30px;cursor:pointer;' src='../Image/Print.png' onclick='PrintPanel()'></div>";
            tab += "<div id='printData' style='width:100%; overflow:auto; height:550px; text-align:center;margin: 0px auto;' align='center' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr style='height:30px;';>"; 
            tab+="<td colspan=10 style='text-align:center;font-size:11pt;'><b>CLIENT DETAILS</b></td></tr>";
            tab += "<tr class='sub_first';>";           
           
            tab += "<tr  class='sub_first';>"; 
            tab += "<td style='width:20%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>Branch </td>";
            tab += "<td style='width:30%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>"+CustDtl[0]+"</td>";     
            tab += "<td style='width:20%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>Profile ClientID</td>"; 
            tab += "<td style='width:30%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>"+CustDtl[4]+"</td>"; 
            tab += "</tr>";  
            tab += "<tr  class='sub_first';>"; 
            tab += "<td style='width:20%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>Name</td>";  
            tab += "<td style='width:30%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>"+CustDtl[6]+"</td>";  
            tab += "<td style='width:20%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>Mobile No</td>";  
            tab += "<td style='width:30%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>"+CustDtl[3]+"</td>";  
            tab += "</tr>";
            tab += "<tr  class='sub_first';>"; 
            tab += "<td style='width:20%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>PAN No</td>";  
            tab += "<td style='width:30%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>"+CustDtl[8]+"</td>";  
            tab += "<td style='width:20%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>Adhaar No</td>";  
            tab += "<td style='width:30%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>"+CustDtl[9]+"</td>";  
            tab += "</tr>";
            tab += "<tr  class='sub_first';>"; 
            tab += "<td style='width:20%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>Address</td>";  
            tab += "<td style='width:30%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>"+CustDtl[10]+"</td>";  
            tab += "<td style='width:20%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'></td>";  
            tab += "<td style='width:30%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'></td>";  
            tab += "</tr>";

                      tab += "</table>";
            
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr style='height:30px;' >"; 
            tab+="<td colspan=9 style='text-align:center;font-size:11pt;'><b>PRODUCT DETAILS</b></td></tr>";
            tab += "<tr style='height:30px;' class='mainhead';>";           
            tab += "<td style='width:5%;text-align:center;font-weight:bold; font-size:11pt; border:1px solid #EEB8A6;'>SINo</td>";
            tab += "<td style='width:60%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>Type of A/c</td>";
            tab += "<td style='width:15%;text-align:center;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>Profile A/c No</td>";
            tab += "<td style='width:10%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>A/c open date</td>";
            tab += "<td style='width:10%;text-align:center;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>A/c Close date</td>";          
            tab += "</tr>";  
            var row;
            row = Data[1].split("¥");
            for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr style='height:25px;' class='sub_first';>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr style='height:25px;' class='sub_first';>";
                    }
                    tab += "<td style='width:5%;text-align:center;font-weight:bold; font-size:11pt; border:1px solid #EEB8A6;'>" + n + "</td>";
                    tab += "<td style='width:60%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>" + col[11] + "</td>";
                    tab += "<td style='width:15%;text-align:center;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>" + col[6] + "</td>";
                    tab += "<td style='width:10%;text-align:left;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>" + col[8] + "</td>";
                    tab += "<td style='width:10%;text-align:center;font-weight:bold;font-size:11pt; border:1px solid #EEB8A6;'>" + col[9] + "</td>";

               
                    tab += "</tr>"; 
                }
                 
            }  
           
            tab += "</table></div>"; 
            showLoading(tab);
            
      }

      function btnOkOnClick()
      {
            closeLoading() ;
            var Data = "1Ø" + Exp_Flag + "Ø" + document.getElementById("<%= hdnData.ClientID %>").value;
            ToServer(Data, 1);
      }

      function PrintPanel()
      {
         var panel = document.getElementById("printData");
         var printWindow = window.open('', '', 'height=400,width=800,location=0,status=0');
         printWindow.document.write('<html><head><title>ESAF</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () {
             printWindow.print();
         }, 500);
         return false;
      }
      function btnExit_onclick() 
      {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
      }
        function showLoading(Msg) {
            document.getElementById('loadingmsg').style.display = 'block';
            document.getElementById('loadingover').style.display = 'block';
            document.getElementById("loadingmsg").innerHTML = Msg;
        }
        function closeLoading() {
            document.getElementById('loadingmsg').style.display = 'none';
            document.getElementById('loadingover').style.display = 'none';
        }
       
      
function btnSearch_onclick() {
              
    document.getElementById("btnSearch").disabled = true;
            
    var Name = document.getElementById("<%= txtName.ClientID%>").value;
    var Address = document.getElementById("<%= txtAddress.ClientID%>").value;
    var PAN = document.getElementById("<%= txtPAN.ClientID%>").value;
    var Aadhar = document.getElementById("<%= txtAadhar.ClientID%>").value;


    var Namelength = Name.length;
    var Addresslength = Address.length;
    var PANlength = PAN.length;
    var Aadharlength = Aadhar.length;


        if (Name == "" && Address == "" && PAN == "" && Aadhar == "")
            {
                alert("Enter Search Value"); document.getElementById("<%= txtName.ClientID%>").focus();return false;
        }
    if (Namelength < 5 && Namelength != 0)
        { alert("Enter atleast 5 characters"); document.getElementById("<%= txtName.ClientID%>").focus(); return false; }
    if (Addresslength < 10 && Addresslength != 0)
    { alert("Enter atleast 10 characters"); document.getElementById("<%= txtAddress.ClientID%>").focus(); return false; }
    if (PANlength < 10 && PANlength != 0)
    { alert("Enter Valid PAN (10 characters)"); document.getElementById("<%= txtPAN.ClientID%>").focus(); return false; }
    if (Aadharlength < 12 && Aadharlength != 0)
    { alert("Enter Valid Aadhar (12 characters)"); document.getElementById("<%= txtAadhar.ClientID%>").focus(); return false; }

   
           
    var ToData = "1Ø" + Name + "Ø" + Address + "Ø" + PAN + "Ø" + Aadhar  + "Ø" +  document.getElementById("<%= cmbType.ClientID%>").value; 
    ToServer(ToData, 1);

}

        </script>
    <style type="text/css">
    
     
    </style>
</head>
</html>
    <table style="width:100%">

         <tr>
             <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>
            <td style="width:66% ;text-align:left;" colspan="4">
                &nbsp;&nbsp;<div id='loadingmsg' style="text-align:center; display: none; "></div>
                <div id='loadingover' style='display: none;'></div></td>
            
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>
             <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>

         </tr>


         <tr>
              <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>
            <td style="width:8% ;text-align:left;">
                Name       Search By &nbsp;&nbsp;</td>
            <td style="width:25% ;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbType" class="NormalText" style="text-align: left;" runat="server" Font-Names="Cambria" 
                    Width="60%" ForeColor="Black">
                    <asp:ListItem Value="1" selected="True" >Start with</asp:ListItem>
                    <asp:ListItem Value="2" >End With </asp:ListItem>
                    <asp:ListItem Value="3" >Anywhere </asp:ListItem>
                </asp:DropDownList>
               
            </td>
             <td style="width:8% ;text-align:left;">
                Name&nbsp;&nbsp;</td>
            <td style="width:25% ;">
                &nbsp;&nbsp;<asp:TextBox ID="txtName" runat="server" Width="79%" style="height:28px;" ></asp:TextBox>
            </td>
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>
             <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>
        </tr>

         <tr>
              <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>
            <td style="width:8% ;text-align:left;">
                PAN&nbsp;&nbsp;</td>
            <td style="width:25% ;">
                &nbsp;&nbsp;<asp:TextBox ID="txtPAN" runat="server" Width="79%" style="height:28px;" ></asp:TextBox>
               
            </td>
            <td style="width:8% ;text-align:left;">
                Aadhar No&nbsp;&nbsp;</td>
            <td style="width:25% ;">
                &nbsp;&nbsp;<asp:TextBox ID="txtAadhar" runat="server" Width="79%" style="height:28px;" ></asp:TextBox>
            </td>
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>
             <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>
        </tr>
         <tr>
              <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>
            <td style="width:8% ;text-align:left;">
                &nbsp;Address&nbsp;&nbsp;</td>
            <td style="width:25% ;">
                &nbsp;&nbsp;<asp:TextBox ID="txtAddress" runat="server" Width="79%" style="height:28px;" ></asp:TextBox>
               
            </td>
            
             <td style="width:8% ;text-align:left;">
                 &nbsp;</td>
            <td style="width:25% ;">
                &nbsp;&nbsp;</td>
               <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>

         </tr>
          <tr>
                <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>
            <td style="width:66% ;text-align:center;" colspan="4">
                <input id="btnSearch" style="font-family: cambria; cursor: pointer; width:10%; height:30px;" 
                type="button" value="SEARCH" onclick="return btnSearch_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width:10%; height:30px;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
             <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>
        </tr>

       <tr>
             <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>
            <td style="width:66% ;text-align:left;" colspan="4">
                &nbsp;&nbsp;</td>
            
              <td style="width:12% ;text-align:left;">
                &nbsp;&nbsp;</td>
             <td style="width:5% ;text-align:left;">
                &nbsp;&nbsp;</td>

         </tr>
         
          <tr>
            <td style="text-align:center; " colspan="8">
                <asp:Panel ID="pnlView" runat="server" style="width:90%; text-align:center; margin:0px auto;">
                    </asp:Panel>
              </td>
        </tr>
       
      
      
        <tr>
            <td style="text-align:center;" colspan="8">
                <br />
                &nbsp;
                &nbsp;&nbsp;
                    <asp:HiddenField ID="hdnView" runat="server" />
                    <asp:HiddenField ID="hdnData" runat="server" />
                    <asp:HiddenField ID="hdnType" runat="server" />
            </td>
        </tr>
    </table>   

</asp:Content>

