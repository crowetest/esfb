﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Configuration
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq
Imports System.IO
Imports System.Net
Imports System.Web.Services
Imports System.Web.Script.Serialization

Partial Class BrNetBranchRequestCloseBatchConfirm
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim DTExcel As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim values As New DataTable
    Dim RequestID, UserID As Integer

#Region "Page Load & Dispose"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "BRNET Branch Request Batch Confirm"

        If GF.FormAccess(CInt(Session("UserID")), 1235) = False Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If

        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        If Not IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.cmbBatch.Attributes.Add("onchange", "return BatchOnChange()")
        End If

    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
    Sub DeleteFilesFromFolder(ByVal Folder As String)
        If Directory.Exists(Folder) Then
            For Each _file As String In Directory.GetFiles(Folder)
                File.Delete(_file)
            Next
            For Each _folder As String In Directory.GetDirectories(Folder)

                DeleteFilesFromFolder(_folder)
            Next

        End If

    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1 'Fill Vet Request For Approve              
                DT = GF.GetQueryResult("select -1,'---Select---' union all select batch_id,batch_name from ESFB.dbo.APP_BRNET_REQUEST_BATCH where status_id=0")
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2
                Dim BatchID As Integer = CInt(Data(1))
                Dim StrVal As String = ""
                DT = GF.GetQueryResult("select c.app_request_id,c.request_no,j.app_name,e.App_Type_name,case when old_branch=-1 then '' else old_branch_name+(convert(varchar,old_branch)) end as oldBranch,case when d.to_branch=-1 then '' else to_branch_name+'-'+convert(varchar,d.to_branch) end as To_branch,old_role_name as oldRole,g.App_role_name, c.Emp_Name+convert(varchar,c.emp_code),c.department,c.Designation ,d.mail_id as email,d.mobile as mobile,cast(y.created_by as varchar) + ' - ' + cast(y.created_on as varchar) as Created_By, c.app_type_id, x.ca_status_dtl,x.gl_status_dtl,x.batch_dtl_id,x.batch_id,x.cif,x.remarks,c.created_branch_name+'-' +convert(varchar,c.created_branch) CrBranch,case when g.brnet_cifflag is null then 0 else g.brnet_cifflag end as flag,case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESCCO' else 'LBS' end Entity, " +
                    " case when c.created_branch between 6000 and 9000 then " +
                    " (case when c.existUser_ID is null or c.existUser_ID='' then upper(substring(replace(replace(Emp_Name,' ',''),'.',''),1,3))+substring(convert(varchar,c.created_branch),1,1)+'00'+convert(varchar,d.emp_code) else upper(c.existUser_ID) end)  " +
                    " else " +
                    " (case when c.existUser_ID is null or c.existUser_ID='' then upper(substring(replace(replace(Emp_Name,' ',''),'.',''),1,3))+convert(varchar,d.emp_code) else upper(c.existUser_ID) end  ) " +
                    " end as UserID " +
                    " from ESFB.dbo.app_level k, ESFB.dbo.APP_BRNET_REQUEST_BATCH_dtl x, esfb.dbo.APP_BRNET_REQUEST_BATCH y, ESFB.dbo.app_request_master c,ESFB.dbo.app_master j,ESFB.dbo.app_type_master e, ESFB.dbo.app_dtl_profile d left join ESFB.dbo.app_role_master g on d.Role_id=g.App_role where j.app_id=c.app_id and k.app_id=c.app_id and c.App_type_id=e.App_type_id and c.App_request_id=d.App_request_id and c.level_id=k.level_id  and c.app_id in (10) and brnet_status is null and c.HO_Approved_status =1 and x.app_request_id=c.app_request_id and c.closed_status is null and c.batch_id is not null and c.batch_id=x.batch_id and x.batch_id=y.batch_id and c.emp_code=x.emp_code and y.status_id=0 and x.batch_id=" & BatchID & " order by c.ho_approved_on")
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += DR(0).ToString & "µ" & DR(1).ToString & "µ" & DR(2).ToString & "µ" & DR(3).ToString & "µ" & DR(4).ToString & "µ" & DR(5).ToString & "µ" & DR(6).ToString & "µ" & DR(7).ToString & "µ" & DR(8).ToString & "µ" & DR(9).ToString & "µ" & DR(10).ToString & "µ" & DR(11).ToString & "µ" & DR(12).ToString & "µ" & DR(13).ToString & "µ" & DR(14).ToString & "µ" & DR(15).ToString & "µ" & DR(16).ToString & "µ" & DR(17).ToString & "µ" & DR(18).ToString & "µ" & DR(19).ToString & "µ" & DR(20).ToString & "µ" & DR(21).ToString & "µ" & DR(22).ToString & "µ" & DR(23).ToString & "µ" & DR(24).ToString & "¥"
                        '         CA                    GL                      Batch_dtl_ID            BatchID                  CIF ID                 Remarks                 AppTypeID       IsReject, Reason
                        StrVal += DR(15).ToString & "µ" & DR(16).ToString & "µ" & DR(17).ToString & "µ" & DR(18).ToString & "µ" & DR(19).ToString & "µ" & DR(20).ToString & "µ" & DR(14).ToString & "µ0µ¥"
                    Next
                End If
                DT = DB.ExecuteDataSet("select distinct y.remarks from ESFB.dbo.app_request_master c, esfb.dbo.APP_BRNET_REQUEST_BATCH y where c.app_id in (10) and c.brnet_status is null and c.HO_Approved_status =1 and  c.closed_status is null and c.batch_id is not null and c.batch_id=y.batch_id and y.status_id=0").Tables(0)
                CallBackReturn += "¶" + DT.Rows(0)(0).ToString + "¶" + StrVal
            Case 3
                Dim Remarks As String = CStr(Data(1))
                Dim dataval As String = CStr(Data(2))
                Dim Mobileno As String = ""
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim BranchID As Integer = CInt(Session("BranchID"))
                Dim mob As String = ""
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Dim StrUrl As String
                Dim RetStr() As String
                Try
                    Dim Params(5) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
                    Params(1).Value = BranchID
                    Params(2) = New SqlParameter("@EMPDtl", SqlDbType.VarChar)
                    Params(2).Value = dataval
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@COMMON_REMARKS", SqlDbType.VarChar, 300)
                    Params(5).Value = Remarks.ToString
                    DB.ExecuteNonQuery("SP_BRNET_VERIFY_BATCH", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)

                    If ErrorFlag = 0 Then
                        Dim Mobiles() As String = Message.Split(CChar("¥"))
                        If Mobiles IsNot Nothing Then
                            For index As Integer = 0 To Mobiles.Length - 2
                                Mobileno = "91" + Mobiles(index)
                                'Mobileno = "918589975866"
                                DT = DB.ExecuteDataSet("SELECT sender,username,password FROM ESFB.dbo.SMS_PORTAL WHERE STATUS_ID =1").Tables(0)
                                If DT.Rows.Count > 0 Then
                                    Dim Sender As String = ""
                                    Dim UserName As String = ""
                                    Dim PassWord As String = ""
                                    Sender = DT.Rows(0).Item(0).ToString()
                                    UserName = DT.Rows(0).Item(1).ToString()
                                    PassWord = DT.Rows(0).Item(2).ToString()
                                    StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "Your Request has been rejected by Br.Net Team. " & "&GSM=" & Mobileno & ""
                                    RetStr = WEB_Request_Response(StrUrl, 1)
                                End If
                            Next
                        End If
                    End If
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘUpdated Successfully"
        End Select
    End Sub
#End Region

#Region "Function"
    Public Shared Function WEB_Request_Response(ByVal Request As String, ByVal Request_type As Integer) As String()

        'Added On 22-10-2020 by 40013 - Changed request from IT
        Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
        Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString


        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        If Request_type = 1 Then
            delimiters = {"<", ">", ">/", "</", "/"}
        Else
            delimiters = {"=", " "}
        End If
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)
        Return parts.ToArray()
    End Function
#End Region
End Class
