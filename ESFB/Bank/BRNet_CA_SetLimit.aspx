﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="BRNet_CA_SetLimit.aspx.vb" Inherits="BRNet_CA_SetLimit" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style2
        {
            width: 11%;
        }
    </style>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

        function window_onload() {
            document.getElementById("rowplnShow").style.display = "none";
            ToServer("1ʘ", 1);
        }

        function FromServer(Arg, Context) 
        {
            switch(Context)
            {
                case 1: //  Employee Fill On Change Of Department
                {                     
                    if(Arg != "")
                    {
                        ComboFill(Arg, "<%= cmbBatch.ClientID %>");
                    }
                    break;
                }
                case 2: //  Employee Fill On Change Of Department
                {   
                    var Data = Arg;                  
                    if(Data != "")
                    {
                        document.getElementById("<%= hdnExistingDtl.ClientID %>").value = Data;
                        document.getElementById("<%= hdnBatch.ClientID %>").value = document.getElementById("<%= cmbBatch.ClientID %>").value;
                        DiplayTable();
                    }
                    break;
                }
                case 3:
                {
                    var Data = Arg.split("ʘ");
                    alert(Data[1]);
                    if (Data[0] == 0) {
                        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
                    }
                    break;
                }
            }
        }

        function BatchOnChange()
        {
            var BatchNo = document.getElementById("<%= cmbBatch.ClientID %>").value;
            ToServer("2ʘ" + BatchNo, 2);
        }

        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ř");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("Ĉ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

        function DiplayTable() 
        {
            var Tab = "";
            Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
            Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            Tab += "<tr class='sub_second';>";
            Tab += "<td style='width:100%;text-align:center'>BATCH DETAILS</td></tr>";
            Tab += "</table></div>"; 
            Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
            Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            Tab += "<tr class='sub_second';>";
            Tab += "<td style='width:5%;text-align:center'>SlNO</td>";
            Tab += "<td style='width:25%;text-align:left'>CIF</td>";
            Tab += "<td style='width:10%;text-align:center'>EMP CODE</td>";
            Tab += "<td style='width:35%;text-align:left'>TITLE</td>";
            Tab += "<td style='width:25%;text-align:left'>ACCOUNT NO</td></tr>";
            Tab += "</table></div>";           
            Tab += "<div id='ScrollDiv' style='width:100%; height:100px; overflow-y:margin: 0px auto;' class=mainhead>";
            Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            if (document.getElementById("<%= hdnExistingDtl.ClientID %>").value != "") {
                document.getElementById("rowplnShow").style.display = "";
                var HidArg = document.getElementById("<%= hdnExistingDtl.ClientID %>").value.split("®");
                var RowCount = HidArg.length - 1;
                var j;
                var Total = 0;
                var row_bg1 = 0;
                var SlNo = 0;
                for (j = 0; j < RowCount; j++) {
                    if (row_bg1 == 0) {
                        row_bg1 = 1;
                        Tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg1 = 0;
                        Tab += "<tr class='sub_second';>";
                    }
                    var HidArg1 = HidArg[j].split("Æ");
                    //Sl No, CIF ID, EMP CODE, TITLE
                    SlNo = SlNo + 1;
                    Tab += "<td style='width:5%;text-align:center'>" + SlNo + "</td>";
                    Tab += "<td style='width:25%;text-align:left'>" + HidArg1[0] + "</td>";
                    Tab += "<td style='width:10%;text-align:center'>" + HidArg1[1] + "</td>";
                    Tab += "<td style='width:35%;text-align:left'>" + HidArg1[2] + "</td>";
                    Tab += "<td style='width:25%;text-align:left'>" + HidArg1[3] + "</td></tr>";
                }
                if (row_bg1 == 0)
                    Tab += "<tr style='background-color:OldLace'></tr>";
                else
                    Tab += "<tr style='background-color:Wheat'></tr>";
                Tab += "</table></div>";
                document.getElementById("<%= pnlExistingData.ClientID %>").innerHTML = Tab;
            }
        }

        function btnConfirm_onclick()
        {
            var BatchNo = document.getElementById("<%= cmbBatch.ClientID %>").value;
            ToServer("3ʘ"+ BatchNo, 3);
        }

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
    // <![CDATA[
    return window_onload()
    // ]]>
        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 100%; margin: 0px auto;">
        <tr id="Tr3">
            <td style="width: 25%;">
            </td>
            <td style="text-align: left;" class="style2">
            </td>
            <td>
                &nbsp; &nbsp;<input id="filename" type="text" style="font-family: sans-serif; cursor: pointer;
                    width: 30%; border: none;" />
            </td>
        </tr>
        <tr id="Tr1">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="text-align: left;" class="style2">
                Select Batch
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbBatch" runat="server" Height="16px" Width="317px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="rowplnShow">
            <td style="width: 25%;" colspan="3">
                <asp:Panel ID="pnlExistingData" runat="server" Width="100%">
                </asp:Panel>
            </td>
        </tr>
        <tr id="branch">
            <td style="width: 25%;">
                &nbsp;</td>
            <td style="text-align: left;" class="style2">
                &nbsp;</td>
            <td style="width: 63%">
                &nbsp;&nbsp;<input id="btnConfirm" onclick="return btnConfirm_onclick()" 
                    style="font-family: cambria; cursor: pointer; width: 10%" type="button" 
                    value="CONFIRM" />&nbsp;
                &nbsp; &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer;
                    width: 10%;" type="button" value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <asp:HiddenField ID="hdnExistingDtl" runat="server" />
                <asp:HiddenField ID="hdnUploadDtl" runat="server" />
                 <asp:HiddenField ID="hdnBatch" runat="server" />
            </td>
        </tr>
    </table>
    <configuration>
    <system.web>
    <compilaton debug="true" targetFramework="4.0"></compilaton>
    </system.web>
    </configuration>
</asp:Content>
