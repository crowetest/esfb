﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="BranchRequest_other.aspx.vb" Inherits="BranchRequest_other" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
            function RequestOnchange() {
        
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Cug").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                document.getElementById("<%= txtContactNo.ClientID %>").value="";
                document.getElementById("<%= txtEmailid.ClientID %>").value="";
           
            }
            function RoleOnChange() {
        
      
                if (document.getElementById("<%= cmbRole.ClientID%>").value == 1)
                {
                    document.getElementById("Teller").style.display = "";
                }
                else{

                    document.getElementById("Teller").style.display = "none";
                }
           
            }
            function OldTransferOnChange() {
        
                if (document.getElementById("<%= cmbTransBranchOld.ClientID%>").value != "-1")
        {
            var ToData = "4Ø" + document.getElementById("<%= cmbTransBranchOld.ClientID%>").value ; 
            ToServer(ToData, 4);
        }
              
           
    }
    function OldRoleOnChange() {
        
      
        if (document.getElementById("<%= cmbRoleOld.ClientID%>").value != "-1")
                {
                    var ToData = "5Ø" + document.getElementById("<%= cmbRoleOld.ClientID%>").value +"Ø" +document.getElementById("<%= cmbCategory.ClientID%>").value; 
                    ToServer(ToData, 5);
                }
           
            }
            function CategoryOnChange() {
                ClearCombo("<%= cmbType.ClientID %>");
                ClearCombo("<%= cmbRole.ClientID%>");
                ClearCombo("<%= cmbRoleOld.ClientID%>");
                document.getElementById("Transfer").style.display = "none";
                document.getElementById("Cug").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").value = "";
          document.getElementById("<%= txtEmpName.ClientID%>").value = "";
                document.getElementById("<%= txtCUG.ClientID%>").value = "";
                document.getElementById("<%= txtDepartment.ClientID%>").value = "";
                document.getElementById("<%= txtDesignation.ClientID%>").value = "";
                document.getElementById("<%= txtDateOfJoin.ClientID%>").value = "";
                document.getElementById("<%= txtReportingTo.ClientID%>").value = "";
                document.getElementById("<%= txtEmpStatus.ClientID%>").value = "";
                document.getElementById("<%= txtContactNo.ClientID%>").value = "";
                document.getElementById("<%= txtEmailid.ClientID%>").value = "";
                if (document.getElementById("<%= cmbCategory.ClientID %>").value =="-1" ){
                    document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
           
          }
          else{
              document.getElementById("<%= txtEmpCode.ClientID%>").disabled=false;
                }
                document.getElementById("<%= cmbType.ClientID%>").value ="-1";
          var Groupid = document.getElementById("<%= cmbCategory.ClientID %>").value;
                if (Groupid > 0) {
                    var ToData = "2Ø" + Groupid; 
                    ToServer(ToData, 2);
                }
            }
            function TypeOnChange() { 
          document.getElementById("<%= txtEmpCode.ClientID%>").disabled=false;
          document.getElementById("<%= txtEmpCode.ClientID%>").value = "";
          document.getElementById("<%= txtEmpName.ClientID%>").value = "";
          document.getElementById("<%= txtCUG.ClientID%>").value = "";
          document.getElementById("<%= txtDepartment.ClientID%>").value = "";
          document.getElementById("<%= txtDesignation.ClientID%>").value = "";
          document.getElementById("<%= txtDateOfJoin.ClientID%>").value = "";
          document.getElementById("<%= txtReportingTo.ClientID%>").value = "";
          document.getElementById("<%= txtEmpStatus.ClientID%>").value = "";
          document.getElementById("<%= txtContactNo.ClientID%>").value = "";
          document.getElementById("<%= txtEmailid.ClientID%>").value = "";
                document.getElementById("DL").style.display = "none";


          if (document.getElementById("<%= cmbCategory.ClientID %>").value =="-1" ){
              document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                }
                else{
                    document.getElementById("<%= txtEmpCode.ClientID%>").disabled=false;
                }
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false;
          document.getElementById("<%= txtEmpCode.ClientID%>").value="";
          document.getElementById("<%= txtEmpName.ClientID%>").value="";
          var Type1 = document.getElementById("<%= cmbType.ClientID %>").value;
                
          if (document.getElementById("<%= cmbCategory.ClientID %>").value ==1 ){//profile
              document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                    document.getElementById("EmpCode").style.display = ""; 
                    document.getElementById("Name").style.display = "";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    
                    document.getElementById("OldProfID").style.display = "none";
                    document.getElementById("NewProfID").style.display = "none";
                    if (Type1 == 1) {
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Role").style.display = "";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                    
                    document.getElementById("OldProfID").style.display = "none";
                    document.getElementById("NewProfID").style.display = "";
                    }
                    else if (Type1 == 2) {
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                       
                    }
                    else if (Type1 == 3) {
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                         document.getElementById("Transfer1").style.display = "";
                         document.getElementById("Transfer").style.display = "";
                         document.getElementById("Teller").style.display = "none";
                       
            
                     }
                     else if (Type1 == 26 || Type1 ==20  || Type1 ==21  || Type1 ==22   || Type1==34 ){
                        
                         document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                        document.getElementById("Role").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                        document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                        document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                        EmployeeOnChange();
            
                        document.getElementById("OldProfID").style.display = "none";
                        document.getElementById("NewProfID").style.display = "none";         
                    }
                    else if (Type1 == 27 ){
                        
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Role").style.display = "";
                        document.getElementById("Role1").style.display = "";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                       
                        document.getElementById("OldProfID").style.display = "";
                        document.getElementById("NewProfID").style.display = "";
            
                    }
                    else if (Type1 == 47 ){
                        
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                             document.getElementById("Role").style.display = "none";
                             document.getElementById("Role1").style.display = "none";
                             document.getElementById("Transfer").style.display = "none";
                             document.getElementById("Transfer1").style.display = "none";
                             document.getElementById("Teller").style.display = "none";
                       
                        document.getElementById("OldProfID").style.display = "none";
                        document.getElementById("NewProfID").style.display = "none";
            
                         }
                    else  {
                        document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false;
                    }
                     
                    
          }
                else if (document.getElementById("<%= cmbCategory.ClientID %>").value ==9 ){//Servosys
                    document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                    document.getElementById("EmpCode").style.display = ""; 
                    document.getElementById("Name").style.display = "";
                    document.getElementById("FolderName").style.display = "none";
                    document.getElementById("Storage").style.display = "none";
                    document.getElementById("Access").style.display = "none";
                    document.getElementById("Role").style.display = "none";
                    document.getElementById("Role1").style.display = "none";
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                   
                    if (Type1 == 41) {
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Role").style.display = "";
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                    }
                    else if (Type1 == 45) {
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                        document.getElementById("Role1").style.display = "none";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                       
                    }
                    else if (Type1 == 42) {
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                        document.getElementById("Transfer1").style.display = "";
                        document.getElementById("Transfer").style.display = "";
                        document.getElementById("Teller").style.display = "none";
                       
            
                    }
                    else if (Type1 == 44 || Type1 == 43){
                        
                        document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                         document.getElementById("Role").style.display = "none";
                         document.getElementById("Transfer").style.display = "none";
                         document.getElementById("Role1").style.display = "none";
                         document.getElementById("Transfer1").style.display = "none";
                         document.getElementById("Teller").style.display = "none";
                         document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                        document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                         EmployeeOnChange();
            
                     }
                     else if (Type1 == 46 ){
                        
                         document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                        document.getElementById("Role").style.display = "";
                        document.getElementById("Role1").style.display = "";
                        document.getElementById("Transfer").style.display = "none";
                        document.getElementById("Transfer1").style.display = "none";
                        document.getElementById("Teller").style.display = "none";
                       
            
                     }
                    
                    else  {
                        document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false;
                    }
                     
                    
}
          else if   (document.getElementById("<%= cmbCategory.ClientID %>").value ==2 || document.getElementById("<%= cmbCategory.ClientID %>").value ==4 ){//AD/EMAIL
             
                document.getElementById("Role").style.display = "none";
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("Cug").style.display = "";
                if (Type1 == 20 || Type1 ==31 || Type1==32 || Type1==33  ){
                    document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                            document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                            EmployeeOnChange();
                }
                else if (Type1 ==36 || Type1 ==37 ||Type1 ==38){
                    document.getElementById("DL").style.display = "";
                }
        }
        else  if (document.getElementById("<%= cmbCategory.ClientID %>").value == 5 ){ //Share Folder
                    
                document.getElementById("FolderName").style.display = "";
                document.getElementById("Storage").style.display = "";
                document.getElementById("Access").style.display = "";
                if (Type1 == 13) {//Share folder
                    document.getElementById("Transfer").style.display = "none";
                    document.getElementById("Transfer1").style.display = "none";
                    document.getElementById("Teller").style.display = "none";
                       
                    document.getElementById("<%= txtFolderName.ClientID%>").value = "";
                            document.getElementById("<%= txtFolderName.ClientID%>").disabled = false; 
                        
                        }
                        else if (Type1 == 14) { //own folder
                            document.getElementById("Transfer").style.display = "none";
                            document.getElementById("Teller").style.display = "none";
                            document.getElementById("<%= txtFolderName.ClientID%>").value = document.getElementById("<%= txtEmpName.ClientID%>").value;
                            document.getElementById("<%= txtFolderName.ClientID%>").disabled = true; 
                       
                        }
        document.getElementById("EmpCode").style.display = ""; 
        document.getElementById("Name").style.display = "";
        document.getElementById("Cug").style.display = "";
        document.getElementById("Role").style.display = "none";
        document.getElementById("Role1").style.display = "none";
        document.getElementById("Teller").style.display = "none";
        document.getElementById("Transfer").style.display = "none";
        document.getElementById("Transfer1").style.display = "none";
        document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
    }
    else  if (document.getElementById("<%= cmbCategory.ClientID %>").value == 3  ){ //AOCO
            document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true;        
            document.getElementById("EmpCode").style.display = ""; 
            document.getElementById("Name").style.display = "";
            document.getElementById("Cug").style.display = "";
            if  (Type1 ==17){
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = ""; 
                document.getElementById("Transfer1").style.display = "";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            }
            else if  (Type1 ==7){
                document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                document.getElementById("Role").style.display = "";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            }
            else if  (Type1 ==22) //unlock application
            {
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false; 
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
                document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                EmployeeOnChange();
            }
            else
            {
                document.getElementById("Role").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            }
            
}
else  if (document.getElementById("<%= cmbCategory.ClientID %>").value == 7  ){ //RLOS
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=true; 
    if (Type1 ==23)
    {
        document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
             
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("Cug").style.display = "";
                document.getElementById("Role").style.display = "";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            }
            else  if (Type1 ==35) //unlock application
            {
                document.getElementById("<%= cmbRole.ClientID%>").disabled = true; 
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").disabled=false; 
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("Cug").style.display = "";
                document.getElementById("Role").style.display = "none";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
                document.getElementById("<%= txtEmpCode.ClientID%>").value =  document.getElementById("<%= hid_Empcode.ClientID%>").value;
                document.getElementById("<%= txtEmpCode.ClientID%>").disabled=true;
                EmployeeOnChange();
                
            }
            else{
                document.getElementById("<%= cmbRole.ClientID%>").disabled = true;
                document.getElementById("EmpCode").style.display = ""; 
                document.getElementById("Name").style.display = "";
                document.getElementById("Cug").style.display = "";
                document.getElementById("Role").style.display = "";
                document.getElementById("Teller").style.display = "none";
                document.getElementById("Transfer").style.display = "none"; 
                document.getElementById("Transfer1").style.display = "none";
                document.getElementById("Role1").style.display = "none";
                document.getElementById("FolderName").style.display = "none";
                document.getElementById("Storage").style.display = "none";
                document.getElementById("Access").style.display = "none";
                document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            }
                    
           
    }
    else  if (document.getElementById("<%= cmbCategory.ClientID %>").value == 6 ){ //Cross Fraud
    document.getElementById("<%= cmbRole.ClientID%>").disabled = false; 
                    
            document.getElementById("EmpCode").style.display = ""; 
            document.getElementById("Name").style.display = "";
            document.getElementById("Cug").style.display = "";
            document.getElementById("Role").style.display = "";
            document.getElementById("Teller").style.display = "none";
            document.getElementById("Transfer").style.display = "none"; 
            document.getElementById("Transfer1").style.display = "none";
            document.getElementById("Role1").style.display = "none";
            document.getElementById("FolderName").style.display = "none";
            document.getElementById("Storage").style.display = "none";
            document.getElementById("Access").style.display = "none";
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
        }

        else { //
            document.getElementById("EmpCode").style.display = ""; 
            document.getElementById("Name").style.display = "";
            document.getElementById("Cug").style.display = "";
            document.getElementById("Role").style.display = "none";
            document.getElementById("Teller").style.display = "none";
            document.getElementById("Transfer").style.display = "none";
            document.getElementById("FolderName").style.display = "none";
            document.getElementById("Transfer1").style.display = "none";
            document.getElementById("Role1").style.display = "none";
            document.getElementById("Storage").style.display = "none";
            document.getElementById("Access").style.display = "none";
            document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
        }
}
       
function ComboFill(data, ddlName) {
    document.getElementById(ddlName).options.length = 0;
    var rows = data.split("Ñ");
       
    for (a = 0; a < rows.length; a++) {
        var cols = rows[a].split("ÿ");
        var option1 = document.createElement("OPTION");
        option1.value = cols[0];
        option1.text = cols[1];
        document.getElementById(ddlName).add(option1);
    }
}
function ClearCombo(control) {
    document.getElementById(control).options.length = 0;
    var option1 = document.createElement("OPTION");
    option1.value = -1;
    option1.text = " -----Select-----";
    document.getElementById(control).add(option1);
}
function FromServer(arg, context) {
    if (context == 1) {
        var Data = arg.split("Ø");
        if (arg == "ØØ") 
        {
            alert("Invalid Employee Code");
            document.getElementById("<%= cmbRole.ClientID%>").value = "-1";
              
            document.getElementById("<%= txtEmpCode.ClientID%>").value = "";
            document.getElementById("<%= txtEmpName.ClientID%>").value = "";
            document.getElementById("<%= txtCUG.ClientID%>").value = "";
            document.getElementById("<%= txtDepartment.ClientID%>").value = "";
            document.getElementById("<%= txtDesignation.ClientID%>").value = "";
            document.getElementById("<%= txtDateOfJoin.ClientID%>").value = "";
            document.getElementById("<%= txtReportingTo.ClientID%>").value = "";
            document.getElementById("<%= txtEmpStatus.ClientID%>").value = "";
            document.getElementById("<%= txtContactNo.ClientID%>").value = "";
            document.getElementById("<%= txtEmailid.ClientID%>").value = "";
            document.getElementById("<%= lblRequestMail.ClientID%>").value = "";
        }
        else
        { 
            document.getElementById("<%= txtEmpName.ClientID%>").value = Data[1];
            document.getElementById("<%= txtContactNo.ClientID%>").value = Data[3];
            if(document.getElementById("<%= cmbCategory.ClientID%>").value ==4 && document.getElementById("<%= cmbType.ClientID%>").value == 10 )
            {
                    
                document.getElementById("<%= lblRequestMail.ClientID%>").innerText = (Data[1].toString().replace(" ",".")).toString().replace(" ","") +"@esafbank.com";    
            }

            else{
                document.getElementById("<%= lblRequestMail.ClientID%>").innerText = "";
                   
            }
                    
            document.getElementById("<%= txtEmailid.ClientID%>").value = Data[2];
            document.getElementById("<%= txtCUG.ClientID%>").value = Data[4];
            document.getElementById("<%= txtDepartment.ClientID%>").value =  Data[5];
            document.getElementById("<%= txtDesignation.ClientID%>").value =  Data[6];
            document.getElementById("<%= txtDateOfJoin.ClientID%>").value =  Data[7];
            document.getElementById("<%= txtReportingTo.ClientID%>").value =   Data[9];
            document.getElementById("<%= txtEmpStatus.ClientID%>").value =  Data[10];
            document.getElementById("<%= cmbRole.ClientID%>").value = (Data[11] =="") ? -1 : Data[11];

            document.getElementById("<%= txtFolderName.ClientID%>").value = ((document.getElementById("<%= cmbCategory.ClientID %>").value == 5 && document.getElementById("<%= cmbType.ClientID%>").value == 14)) ? Data[1] : "";
                
        }
    }
    else if (context == 2) {
        var Data = arg.split("|");
        ComboFill(Data[0], "<%= cmbType.ClientID%>");
        ComboFill(Data[1], "<%= cmbRole.ClientID%>");
        ComboFill(Data[1], "<%= cmbRoleOld.ClientID%>");
    }
    else if (context == 4) {
        var Data = arg;
        ComboFill(Data, "<%= cmbTransBranch.ClientID%>");
       

        }
        else if (context == 5) {
            var Data = arg;
            ComboFill(Data, "<%= cmbRole.ClientID%>");

        }
}
function btnExit_onclick() {
    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
}

function RequestOnClick() {   
       
    if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1") 
    {
        alert("Select Branch");
        document.getElementById("<%= cmbBranch.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= cmbCategory.ClientID%>").value == "-1") 
    {
        alert("Select Application Type");
        document.getElementById("<%= cmbCategory.ClientID%>").focus();
            return false;
        }
      
        if (document.getElementById("<%= cmbType.ClientID %>").value == "-1") 
    {
        alert("Select Type");
        document.getElementById("<%= cmbType.ClientID %>").focus();
            return false;
        }
        
        if (document.getElementById("<%= txtServiceRequestNo.ClientID%>").value == "") 
    {
            if (document.getElementById("<%= cmbCategory.ClientID%>").value != 9 && document.getElementById("<%= cmbCategory.ClientID%>").value != 2 && document.getElementById("<%= cmbCategory.ClientID%>").value != 4 && (document.getElementById("<%= cmbCategory.ClientID%>").value != 1  || (document.getElementById("<%= cmbCategory.ClientID%>").value == 1 && document.getElementById("<%= cmbType.ClientID%>").value == 9)) &&  (document.getElementById("<%= cmbCategory.ClientID%>").value != 3  || (document.getElementById("<%= cmbCategory.ClientID%>").value == 3 && document.getElementById("<%= cmbType.ClientID%>").value == 22))  && (document.getElementById("<%= cmbCategory.ClientID%>").value !=7  || (document.getElementById("<%= cmbCategory.ClientID%>").value == 7 && document.getElementById("<%= cmbType.ClientID%>").value == 35))) {
                        alert("Enter Your Service Request No");
                        document.getElementById("<%= txtServiceRequestNo.ClientID%>").focus();
                    return false;
                }
            }
        
            if (document.getElementById("txtDetails").value == "" && document.getElementById("<%= cmbType.ClientID %>").value =="2")
    {
        alert("Enter Description");
        document.getElementById("txtDetails").focus();
        return false;
    }  
    if (document.getElementById("<%= cmbType.ClientID %>").value =="27" && document.getElementById("<%= cmbRoleOld.ClientID%>").value =="-1" )
    {
        alert("Select Current Role");
        document.getElementById("<%= cmbRoleOld.ClientID%>").focus();
            return false;
        }  
    if ((document.getElementById("<%= cmbType.ClientID %>").value =="1" || document.getElementById("<%= cmbType.ClientID %>").value =="7"  || document.getElementById("<%= cmbType.ClientID %>").value =="23"  || document.getElementById("<%= cmbType.ClientID %>").value =="41" ) && document.getElementById("<%= cmbRole.ClientID%>").value =="-1" )
    {
        alert("Select Role");
        document.getElementById("<%= cmbRole.ClientID%>").focus();
            return false;
        }  
   
     
        if ((document.getElementById("<%= cmbType.ClientID %>").value =="3" ||document.getElementById("<%= cmbType.ClientID %>").value =="17") && document.getElementById("<%= cmbTransBranchOld.ClientID%>").value =="-1" )
    {
        alert("Select Current Branch");
        document.getElementById("<%= cmbRoleOld.ClientID%>").focus();
            return false;
        }  
        if ((document.getElementById("<%= cmbType.ClientID %>").value =="3" ||document.getElementById("<%= cmbType.ClientID %>").value =="17") && document.getElementById("<%= cmbTransBranch.ClientID%>").value =="-1" )
    {
        alert("Select New Branch");
        document.getElementById("<%= cmbRole.ClientID%>").focus();
            return false;
        }  
        if ((document.getElementById("<%= cmbCategory.ClientID%>").value == "1" && (document.getElementById("<%= cmbType.ClientID%>").value == "1" ||document.getElementById("<%= cmbType.ClientID%>").value == "27")) && document.getElementById("<%= cmbRole.ClientID%>").value == "1" && document.getElementById("<%= cmbTeller.ClientID%>").value == "-1") 
    {
        alert("Select Teller Type");
        document.getElementById("<%= cmbTeller.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= cmbCategory.ClientID %>").value ==1 &&  document.getElementById("<%= txtEmpCode.ClientID%>").value == "")
    {
        alert("Enter Emp Code");
        document.getElementById("<%= txtEmpCode.ClientID%>").focus();
            return false;
        }
        if(document.getElementById("<%= cmbCategory.ClientID %>").value == 5){
        if (document.getElementById("<%= txtFolderName.ClientID%>").value == "") 
                    {
                        alert("Enter Folder Name ");
                        document.getElementById("<%= txtFolderName.ClientID%>").focus();
                        return false;
                    }
                    if (document.getElementById("<%= cmbStorage.ClientID%>").value == "-1") 
                    {
                        alert("Select Storage limit");
                        document.getElementById("<%= cmbStorage.ClientID%>").focus();
                        return false;
                    }
                    if (document.getElementById("<%= cmbAccess.ClientID%>").value == "") 
                    {
                        alert("Select  Access Type");
                        document.getElementById("<%= cmbAccess.ClientID%>").focus();
                        return false;
                    }
                }
                
                if (document.getElementById("<%= txtContactNo.ClientID %>").value== "") 
    {
        alert("Enter Contact Number");
        document.getElementById("<%= txtContactNo.ClientID %>").focus();
            return false;
        }
               
               
        if (document.getElementById("<%= cmbCategory.ClientID%>").value == "2")
    {
        if (document.getElementById("<%= txtCUG.ClientID%>").value == "" || (document.getElementById("<%= txtCUG.ClientID%>").value).length<10) 
                        {
                            alert("Enter CUG Number");
                            document.getElementById("<%= txtCUG.ClientID%>").focus();
                        return false;
                    }
                }

      
                if (document.getElementById("<%= txtEmailid.ClientID %>").value == "") 
    {
        alert("Enter Email Id ");
        document.getElementById("<%= txtEmailid.ClientID %>").focus();
            return false;
        }
                   
    if ((document.getElementById("<%= cmbType.ClientID %>").value =="36" || document.getElementById("<%= cmbType.ClientID %>").value =="37" || document.getElementById("<%= cmbType.ClientID %>").value =="38") && document.getElementById("<%= cmbCategory.ClientID%>").value =="4" && document.getElementById("<%= txtDL.ClientID%>").value =="")
    {
        alert("Enter Distribution List");
        document.getElementById("<%= txtDL.ClientID%>").focus();
        return false;
    } 

    var DL =  document.getElementById("<%= txtDL.ClientID%>").value; 
    var Branch	= document.getElementById("<%= cmbBranch.ClientID %>").value;
    var APP_ID	= document.getElementById("<%= cmbCategory.ClientID%>").value;
    var trans_branch_id	= document.getElementById("<%= cmbTransBranch.ClientID%>").value;
 
    var TYPE_ID	= document.getElementById("<%= cmbType.ClientID %>").value;
    var CUG =document.getElementById("<%= txtCUG.ClientID%>").value;
    var Descr	= document.getElementById("txtDetails").value; 
    var contact=document.getElementById("<%= txtContactNo.ClientID %>").value;
        var emailid=document.getElementById("<%= txtEmailid.ClientID %>").value;
    var Emp_code=document.getElementById("<%= txtEmpCode.ClientID%>").value;
    var OldRoleID =document.getElementById("<%= cmbRoleOld.ClientID%>").value;
    var OldTransferID =document.getElementById("<%= cmbTransBranchOld.ClientID%>").value;
    var RoleID =document.getElementById("<%= cmbRole.ClientID%>").value;
    var Tellertype =document.getElementById("<%= cmbTeller.ClientID %>").value;
    var FolderName =document.getElementById("<%= txtFolderName.ClientID%>").value;
    var Storage =document.getElementById("<%= cmbStorage.ClientID%>").value;
    var Accesstype =document.getElementById("<%= cmbAccess.ClientID%>").value;
    var NewMailID =document.getElementById("<%= lblRequestMail.ClientID%>").innerText ;
    var ServiceRequestNo	= document.getElementById("<%= txtServiceRequestNo.ClientID%>").value;

     //Added by Vidya for profile old and new user id ---begin
        
        var oldProfUserID = "";
        var newProfUserID = "";
        if(APP_ID == 1){
            if(TYPE_ID == 27){
                oldProfUserID = document.getElementById("<%= txtOldProfID.ClientID%>").value;
                newProfUserID = document.getElementById("<%= txtNewProfID.ClientID%>").value;
                if(oldProfUserID == ""){
                    alert("Enter Old Profile User ID");
                    document.getElementById("<%= txtOldProfID.ClientID %>").focus();
                    return false;
                }
                if(newProfUserID == ""){
                    alert("Enter New Profile User ID");
                    document.getElementById("<%= txtNewProfID.ClientID %>").focus();
                    return false;
                }
            }
        }
        //Added by Vidya for profile old and new user id ---end


    document.getElementById("<%= hdnValue.ClientID %>").value = "1Ø" + Branch + "Ø" + APP_ID + "Ø" + trans_branch_id + "Ø" + TYPE_ID+ "Ø" + Descr + "Ø" + contact + "Ø" + emailid + "Ø" + Emp_code + "Ø" + RoleID +"Ø" + CUG + "Ø" + Tellertype + "Ø" + FolderName + "Ø" + Storage + "Ø" + Accesstype + "Ø" + NewMailID + "Ø" + ServiceRequestNo + "Ø" + OldRoleID + "Ø" +OldTransferID + "Ø" +DL + "Ø" + oldProfUserID + "Ø" + newProfUserID;       
     
}
function EmployeeOnChange() 
{   var APP_ID	= document.getElementById("<%= cmbCategory.ClientID%>").value;
        var EmpCode = document.getElementById("<%= txtEmpCode.ClientID%>").value;
        var TYPE_ID	= document.getElementById("<%= cmbType.ClientID %>").value;
        
        if (EmpCode != "")
            ToServer("1Ø" + EmpCode +"Ø" + document.getElementById("<%= cmbCategory.ClientID%>").value + "Ø" +TYPE_ID, 1);
            }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return window_onload()
            // ]]>
        </script>
    </head>
    </html>
   
    <br />
    <br />
    <table class="style1" style="width: 80%; margin: 0px auto;">
        <tr id="branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black" >
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
        <tr id="App">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Application
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbCategory" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Type">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Type
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbType" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
        <tr id="ServiceRequestNo">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
               Service Now Ticket No
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtServiceRequestNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15"  />
            </td>
        </tr>
        <tr id="EmpCode">
            <td style="width: 25%;">
                &nbsp;
                   <asp:HiddenField ID="hid_Empcode" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
               Employee Code
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="20%" class="NormalText" MaxLength="50" onkeypress='return NumericCheck(event)'  />
            </td>
        </tr>
        <tr id="Name">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Emp Name
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpName" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="60%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
            </td>
        </tr>
         <tr  id="Dep">
        <td style="width:25%;">
                   <asp:HiddenField ID="hid_Dtls" runat="server" />
                   </td>
            <td style="width:12% ; text-align:left;">
                Department</td>
            <td style="width:63% ;text-align:left;">
               &nbsp; &nbsp;<asp:TextBox ID="txtDepartment" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr  id="Des">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Designation</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesignation" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr  id="Doj">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Date of Join</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtDateOfJoin" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr id="ReportingTo">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Reporting Officer</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtReportingTo" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
         <tr id="Status">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Employee Status</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpStatus" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>

         <tr  id="Cug">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                CUG</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtCUG" runat="server" Width="60%" class="NormalTextBox" onkeypress='return NumericCheck(event)' MaxLength="10" ></asp:TextBox>
                </td>
        </tr>
        <tr id="Role1">
            <td style="width: 25%;">
                &nbsp;</td>
            <td style="width: 12%; text-align: left;">
                Old Role</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbRoleOld" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                <span id = "OldProfID" style="display:none;">
                    &nbsp; &nbsp; Profile ID
                    &nbsp; &nbsp;<asp:TextBox ID="txtOldProfID" runat="server" Width="30%" class="NormalTextBox" MaxLength="30"></asp:TextBox>
                </span>
            </td>
        </tr>
        <tr id="Role">
            <td style="width: 25%;">
                &nbsp;</td>
            <td style="width: 12%; text-align: left;">
                New Role</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbRole" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                <span id = "NewProfID" style="display:none;" >
                    &nbsp; &nbsp; Profile ID
                    &nbsp; &nbsp;<asp:TextBox ID="txtNewProfID" runat="server" Width="30%" class="NormalTextBox" MaxLength="30"></asp:TextBox>
                </span>
            </td>
        </tr>

        <tr id="Teller">
            <td style="width: 25%;">
                &nbsp;</td>
            <td style="width: 12%; text-align: left;">
                Teller Type</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbTeller" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                     <asp:ListItem Value="1"> Head Teller </asp:ListItem>
                     <asp:ListItem Value="2"> Cashier</asp:ListItem>
                    <asp:ListItem Value="3"> Both</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Transfer1">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Transfer From</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbTransBranchOld" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black" >
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
         <tr id="Transfer">
            <td style="width: 25%;">
                &nbsp;</td>
            <td style="width: 12%; text-align: left;">
                Transfer To </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbTransBranch" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Tr1">
            <td style="width: 25%;">
                <asp:HiddenField ID="hid_Post" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Description
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<textarea id="txtDetails" class="NormalText" cols="20" name="S1" rows="3" onkeypress='return TextAreaCheck(event)' 
                    maxlength="1000" style="width: 70%"></textarea>
            </td>
        </tr>
        <tr  id="FolderName">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Folder Name</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtFolderName" runat="server" Width="60%" class="NormalTextBox" onkeypress='return AlphaNumericCheck(event)' MaxLength="25" ></asp:TextBox>
                </td>
        </tr>
         <tr  id="Storage">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Storage Requirement</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbStorage" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                     <asp:ListItem Value="1"> 1 GB </asp:ListItem>
                     <asp:ListItem Value="2"> 2 GB</asp:ListItem>
                    <asp:ListItem Value="3"> 3 GB</asp:ListItem>
                    <asp:ListItem Value="4"> 4 GB</asp:ListItem>
                    <asp:ListItem Value="5"> 5 GB</asp:ListItem>
                    <asp:ListItem Value="6"> 6 GB</asp:ListItem>
                    <asp:ListItem Value="7"> 7 GB</asp:ListItem>
                    <asp:ListItem Value="8"> 8 GB</asp:ListItem>
                     <asp:ListItem Value="9"> 9 GB</asp:ListItem>
                     <asp:ListItem Value="10"> 10 GB</asp:ListItem>
                </asp:DropDownList>
                </td>
        </tr>
         <tr  id="DL">
            <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Distribution List</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtDL" runat="server" Width="60%" class="NormalTextBox"  MaxLength="100" ></asp:TextBox>
                </td>
        </tr>
         <tr  id="Access">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Access Type</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbAccess" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                     <asp:ListItem Value="1"> Read </asp:ListItem>
                     <asp:ListItem Value="2"> Write</asp:ListItem>
                    <asp:ListItem Value="3"> Both </asp:ListItem>
                    </asp:DropDownList>
                </td>
        </tr>
        <tr id="contactno">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Contact No.
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtContactNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="10" onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr id="Email">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; align: left;">
                Official
                Email Id
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmailid" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" MaxLength="50" />
                <asp:Label ID="lblRequestMail" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:HiddenField ID="hdn_RetailFlg" runat="server" />
                <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
