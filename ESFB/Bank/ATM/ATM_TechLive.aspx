﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="ATM_TechLive.aspx.vb" Inherits="ATM_TechLive" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %> 
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
            function RequestOnchange() {
        
 
          
               document.getElementById("<%= txtMail.ClientID%>").value="";
               document.getElementById("<%= txtRemarks.ClientID %>").value="";
               document.getElementById("<%= txtContactNo.ClientID%>").value ="";
               
           }
            function window_onload(){
            ATMOnChange();
            }

   
           
       
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ñ");
       
                for (a = 0; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function ClearCombo(control) {
                document.getElementById(control).options.length = 0;
                var option1 = document.createElement("OPTION");
                option1.value = -1;
                option1.text = " -----Select-----";
                document.getElementById(control).add(option1);
            }
            function FromServer(arg, context) {
                 var Data = arg.split("Ø");
                  if (context == 2) {
                 if (Data[0]!="")
                 {  
                        document.getElementById("<%= txtBranch.ClientID%>").value=Data[1];
                         document.getElementById("<%= txtSiteName.ClientID%>").value=Data[2];
                        document.getElementById("<%= txtCust1.ClientID%>").value = Data[3];
                        document.getElementById("<%= txtCust1Mail.ClientID%>").value = Data[4];
                        document.getElementById("<%= txtCust1ContactNo.ClientID%>").value = Data[5];
                        document.getElementById("<%= txtEmpName.ClientID%>").value = Data[6];
                        document.getElementById("<%= txtMail.ClientID%>").value = Data[7];   
                        document.getElementById("<%= txtContactNo.ClientID%>").value = Data[8]; 
                        document.getElementById("<%= txtdt.ClientID%>").focus(); 
                    } 
                    else
                    {
                        document.getElementById("<%= txtBranch.ClientID%>").value="";
                        document.getElementById("<%= txtSiteName.ClientID%>").value="";
                        document.getElementById("<%= txtCust1.ClientID%>").value = "";
                        document.getElementById("<%= txtCust1Mail.ClientID%>").value = "";
                        document.getElementById("<%= txtCust1ContactNo.ClientID%>").value ="";
                        document.getElementById("<%= txtEmpName.ClientID%>").value = "";
                        document.getElementById("<%= txtMail.ClientID%>").value = "";   
                        document.getElementById("<%= txtContactNo.ClientID%>").value = ""; 
                       
                    }  
                    
                }
       
             }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

    function RequestOnClick() {   
       
     if (document.getElementById("<%= cmbATM.ClientID%>").value == "-1") 
        {
            alert("Select ATM");
            document.getElementById("<%= cmbATM.ClientID%>").focus();
            return false;
        }
        
         
     if (document.getElementById("<%= txtdt.ClientID%>").value == "") 
        {
            alert("Select date");
            document.getElementById("<%= txtdt.ClientID%>").focus();
            return false;
        }      
       
        
    var TMKey_ID	= document.getElementById("<%= cmbATM.ClientID%>").value;
   
   
    
    var Remarks	= document.getElementById("<%= txtRemarks.ClientID%>").value;
    var TechLiveDate = document.getElementById("<%= txtdt.ClientID%>").value;

    document.getElementById("<%= hdnValue.ClientID %>").value = "1Ø" + TMKey_ID + "Ø" +  TechLiveDate + "Ø" + Remarks ;       
}

 
           
            function ATMOnChange() {
       
               
                if (document.getElementById("<%= cmbATM.ClientID%>").value != "")
                {
                    var ToData = "2Ø" + document.getElementById("<%= cmbATM.ClientID%>").value; 

                   
                    ToServer(ToData, 2);
                }
           
            }

            
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return window_onload()
            // ]]>
        </script>
    </head>
    </html>
   
    <br />
    <br />
       <table class="style1" style="width: 80%; margin: 0px auto;">
         
        <tr id="Tr2">
            <td style="width: 20%;">
            </td>
            <td style="width: 17%; text-align: left;">
                ATM</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbATM" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Tr5">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
               Branch</td> <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranch" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
            </td>
        </tr>
        <tr id="Tr4">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
               Site Name</td> <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtSiteName" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
            </td>
        </tr>
          
        <tr id="EmpCode">
            <td style="width: 20%;">
                &nbsp;
                   <asp:HiddenField ID="hid_Empcode" runat="server" />
            </td>
            <td style="width: 17%; text-align: left;">
               Custodian 1
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCust1" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="50" onkeypress='return NumericCheck(event)' ReadOnly="true" />
            </td>
        </tr>
       
         
        <tr id="mail">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
               Custodian 1 E-mail</td><td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCust1Mail" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="15" ReadOnly="true"  />
            </td>
        </tr>
        <tr id="ContactNo">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
               Custodian 1 Contact No</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCust1ContactNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="15" ReadOnly="true" />
            </td>
        </tr>
        
         
        
       
        <tr id="Tr9">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
                Custodian2
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpName" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
            </td>
        </tr>
       
        <tr id="Tr12">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
               Custodian 2 E-mail</td><td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtMail" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="15" ReadOnly="true"  />
            </td>
        </tr>
        <tr id="Tr13">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
                Custodian 2 Contact No</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtContactNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="15" ReadOnly="true"   />
            </td>
        </tr>
        <tr id="Tr3">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
               Tech Live Date</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtdt" class="NormalText" runat="server" Width="40%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtdt" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
        </tr>
        <tr id="Tr1">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
                Remarks</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtRemarks" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="300"  />
            
            </td>
        </tr>
        
        <tr id="Img">
        <td style="width:25%;"><asp:HiddenField 
                    ID="hdn_Post" runat="server" />
                </td>
            <td style="width:12% ; text-align:left;">
                &nbsp;</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;
                </td>

        </tr>
      
      
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:HiddenField ID="hdn_RetailFlg" runat="server" />
                <asp:Button ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                    &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
