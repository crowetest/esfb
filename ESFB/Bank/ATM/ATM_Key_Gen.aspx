﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="ATM_Key_Gen.aspx.vb" Inherits="ATM_Key_Gen" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %> 
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
            function RequestOnchange() {
        
 
               document.getElementById("<%= txtPIN.ClientID%>").value="";
               document.getElementById("<%= txtMail.ClientID%>").value="";
               document.getElementById("<%= txtRemarks.ClientID %>").value="";
               document.getElementById("<%= txtContactNo.ClientID%>").value ="";
               document.getElementById("<%= txtRefNo.ClientID%>").value ="";
           }
            
     function viewSummary()
        {
           var BranchID = document.getElementById("<%= cmbATM.ClientID %>").value;
           if (BranchID !="-1")
           {
            window.open("Reports/ViewATMHistory.aspx?TR_ID=" + btoa(BranchID)); }
            
            return false;
        }
          
       
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ñ");
       
                for (a = 0; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function ClearCombo(control) {
                document.getElementById(control).options.length = 0;
                var option1 = document.createElement("OPTION");
                option1.value = -1;
                option1.text = " -----Select-----";
                document.getElementById(control).add(option1);
            }
            function FromServer(arg, context) {
                 var Data = arg.split("Ø");
                 if (context == 1) {
                    if (arg == "ØØ") 
                    {
                        alert("Invalid Employee Code");
                         
                        document.getElementById("<%= txtEmpCode.ClientID%>").value = "";
                        document.getElementById("<%= txtEmpName.ClientID%>").value = "";
                        document.getElementById("<%= txtDepartment.ClientID%>").value = "";
                        document.getElementById("<%= txtDesignation.ClientID%>").value = "";
                        document.getElementById("<%= txtContactNo.ClientID%>").value = "";
                        document.getElementById("<%= txtMail.ClientID%>").value = "";
            
                    }
                    else
                    { 
           
                       
                        document.getElementById("<%= txtEmpName.ClientID%>").value = Data[1];
                        document.getElementById("<%= txtDepartment.ClientID%>").value = Data[2];
                        document.getElementById("<%= txtDesignation.ClientID%>").value = Data[3];
                        document.getElementById("<%= txtContactNo.ClientID%>").value = Data[4];
                        document.getElementById("<%= txtMail.ClientID%>").value = Data[5];
                        
                    }
                }
                else if (context == 2) {
                   
                document.getElementById("<%= txtBranch.ClientID%>").value=Data[1];
                document.getElementById("<%= txtPIN.ClientID%>").focus();
                }
       
             }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

    function RequestOnClick() {   
      
     if (document.getElementById("<%= cmbATM.ClientID%>").value == "-1") 
        {
            alert("Select ATM");
            document.getElementById("<%= cmbATM.ClientID%>").focus();
            return false;
        }
        
               
       
            if (document.getElementById("<%= txtPIN.ClientID%>").value == "") 
        {
       
            alert("Enter PIN");
            document.getElementById("<%= txtPIN.ClientID%>").focus();
            return false;
        }
        var PIN =document.getElementById("<%= txtPIN.ClientID%>").value;
        
        if (document.getElementById("<%= txtEmpcode.ClientID%>").value == "") 
        {
       
              alert("Enter Custodian 1 employee code");
              document.getElementById("<%= txtEmpcode.ClientID%>").focus();
              return false;
        }
        if (document.getElementById("<%= txtMail.ClientID%>").value == "") 
        {
       
              alert("Enter Mail ID");
              document.getElementById("<%= txtMail.ClientID%>").focus();
              return false;
        }
         if (document.getElementById("<%= txtContactNo.ClientID%>").value == "") 
        {
       
              alert("Enter Contact No");
              document.getElementById("<%= txtContactNo.ClientID%>").focus();
              return false;
        }
    var ATM	= document.getElementById("<%= cmbATM.ClientID%>").value;
    var PIN	= document.getElementById("<%= txtPIN.ClientID%>").value;
    var RefNo	= document.getElementById("<%= txtRefNo.ClientID%>").value;
    var Empcode	= document.getElementById("<%= txtEmpcode.ClientID%>").value;
    var Mail	= document.getElementById("<%= txtMail.ClientID%>").value;
    var ContactNo	= document.getElementById("<%= txtContactNo.ClientID%>").value;
    var Remarks	= document.getElementById("<%= txtRemarks.ClientID%>").value;
    

    document.getElementById("<%= hdnValue.ClientID %>").value = "1Ø" + ATM + "Ø" + PIN + "Ø" + RefNo + "Ø" + Empcode + "Ø" + Mail + "Ø" + ContactNo + "Ø" + Remarks; 
       
}
            function window_onload(){
                
            }
            function ATMOnChange() {
               
               
                if (document.getElementById("<%= cmbATM.ClientID%>").value != "-1")
                {
                    var ToData = "2Ø" + document.getElementById("<%= cmbATM.ClientID%>").value; 
                    ToServer(ToData, 2);
                }
           
            }

            function EmployeeOnChange() 
            {   
                    
                    var EmpCode = document.getElementById("<%= txtEmpCode.ClientID%>").value;
                    
        
                    if (EmpCode != "")
                        ToServer("1Ø" + EmpCode , 1);
                        }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return window_onload()
            // ]]>
        </script>
    </head>
    </html>
   
    <br />
    <br />
       <table class="style1" style="width: 80%; margin: 0px auto;">
         
        <tr id="Tr2">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                ATM</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbATM" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
                <img id='ViewAsset' src='../../Image/eswtClose2.png' onclick='viewSummary()' title='View ATM history'  style='height:20px; width:20px;  cursor:pointer;' /></td>
        </tr>
        <tr id="Tr5">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
               Branch</td> <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranch" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
            </td>
        </tr>
         <tr id="Tr3">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                PIN set 1<td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtPIN" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="32"   onkeypress='return AlphaNumericCheck(event)' />
            </td>
        </tr>
         <tr id="Tr4" style="display:none;">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
               Reference No</td><td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtRefNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15"  onkeypress='return NumericCheck(event)'  />
            </td>
        </tr>   
        <tr id="EmpCode">
            <td style="width: 25%;">
                &nbsp;
                   <asp:HiddenField ID="hid_Empcode" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
               Employee Code
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="20%" class="NormalText" MaxLength="50" onkeypress='return NumericCheck(event)'  />
            </td>
        </tr>
        <tr id="Name">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Emp Name
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpName" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
            </td>
        </tr>
         <tr  id="Dep">
        <td style="width:25%;">
                   <asp:HiddenField ID="hid_Dtls" runat="server" />
                   </td>
            <td style="width:12% ; text-align:left;">
                Department</td>
            <td style="width:63% ;text-align:left;">
               &nbsp; &nbsp;<asp:TextBox ID="txtDepartment" runat="server" Width="50%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr  id="Des">
        <td style="width:25%;">
                </td>
            <td style="width:12% ; text-align:left;">
                Designation</td>
            <td style="width:63% ;text-align:left;">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesignation" runat="server" Width="50%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr id="mail">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                E-mail<td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtMail" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15"  />
            </td>
        </tr>
        <tr id="ContactNo">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Contact No</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtContactNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15"  />
            </td>
        </tr>
        <tr id="Tr1">
            <td style="width: 25%;">
               
            </td>
            <td style="width: 12%; text-align: left;">
                Remarks
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtRemarks" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="100"  />
            &nbsp;</td>
        </tr>
        
       
        <tr id="Img">
        <td style="width:25%;"><asp:HiddenField 
                    ID="hdn_Post" runat="server" />
                </td>
            <td style="width:12% ; text-align:left;">
                &nbsp;</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;
                </td>

        </tr>
      
      
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:HiddenField ID="hdn_RetailFlg" runat="server" />
                <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
