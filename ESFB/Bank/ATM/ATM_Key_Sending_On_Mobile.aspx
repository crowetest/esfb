﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="ATM_Key_Sending_On_Mobile.aspx.vb" Inherits="ATM_Key_Sending_On_Mobile" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %> 
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
        .style2
        {
            width: 25%;
            height: 30px;
        }
        .style3
        {
            width: 12%;
            height: 30px;
        }
        .style4
        {
            width: 63%;
            height: 30px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
            function RequestOnchange() {
               document.getElementById("<%= txtPIN.ClientID%>").value="";
               document.getElementById("<%= txtMail.ClientID%>").value="";
               document.getElementById("<%= txtContactNo.ClientID%>").value ="";
           }
       function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ñ");
       
                for (a = 0; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function ClearCombo(control) {
                document.getElementById(control).options.length = 0;
                var option1 = document.createElement("OPTION");
                option1.value = -1;
                option1.text = " -----Select-----";
                document.getElementById(control).add(option1);
            }
            function FromServer(arg, context) {
//            debugger;
                 var Data = arg.split("Ø");
                 if (context == 1) {
                    if (arg == "ØØ") 
                    {
                        alert("Invalid Employee Code");
                         
                        document.getElementById("<%= txtEmpCode.ClientID%>").value = "";
                        document.getElementById("<%= txtEmpName.ClientID%>").value = "";
                        document.getElementById("<%= txtContactNo.ClientID%>").value = "";
                        document.getElementById("<%= txtMail.ClientID%>").value = "";
            
                    }
                    else
                    { 
                        if ( document.getElementById("<%= hid_PrevEmpcode.ClientID%>").value  !=Data[0]){
                            document.getElementById("<%= txtEmpName.ClientID%>").value = Data[1];
                            document.getElementById("<%= txtContactNo.ClientID%>").value = Data[4];
                            document.getElementById("<%= txtMail.ClientID%>").value = Data[5];
                           

                            }
                            else
                            {
                            alert("Custodian1 and custodian2 should not be equal!");
                            document.getElementById("<%= txtEmpCode.ClientID%>").value = "";
                            document.getElementById("<%= txtEmpName.ClientID%>").value = "";
                            document.getElementById("<%= txtContactNo.ClientID%>").value = "";
                            document.getElementById("<%= txtMail.ClientID%>").value = "";
                            document.getElementById("<%= txtEmpcode.ClientID%>").focus();
                            }
                        
                    }
                }
                else if (context == 2) {
                 if (Data[0]!="")
                 {  
                        document.getElementById("<%= txtBranch.ClientID%>").value=Data[1];
                        document.getElementById("<%= txtCust1.ClientID%>").value = Data[3];
                        document.getElementById("<%= txtCust1Mail.ClientID%>").value = Data[4];
                        document.getElementById("<%= txtCust1ContactNo.ClientID%>").value = Data[5];
                        document.getElementById("<%= txtCreatedBy.ClientID%>").value = Data[7];   
                        document.getElementById("<%= txtCreatedOn.ClientID%>").value = Data[8]; 
                        document.getElementById("<%= txtcust1PIN.ClientID%>").value = Data[10]; 
                        document.getElementById("<%= txtEmp1.ClientID%>").value = Data[13];
                        document.getElementById("<%= hid_PrevEmpcode.ClientID%>").value=Data[11]; 
                        document.getElementById("<%= txtEmpcode.ClientID%>").value = Data[14];
                        document.getElementById("<%= txtEmpName.ClientID%>").value =Data[15];
                        document.getElementById("<%= txtMail.ClientID%>").value = Data[16];
                        document.getElementById("<%= txtContactNo.ClientID%>").value =Data[17];
                         document.getElementById("<%= txtPIN.ClientID%>").value = Data[18]; 

                    } 
                    else
                    {
                        document.getElementById("<%= txtBranch.ClientID%>").value="";
                        document.getElementById("<%= txtCust1.ClientID%>").value = "";
                        document.getElementById("<%= txtCust1Mail.ClientID%>").value = "";
                        document.getElementById("<%= txtCust1ContactNo.ClientID%>").value ="";
                        document.getElementById("<%= txtCreatedBy.ClientID%>").value = "";   
                        document.getElementById("<%= txtCreatedOn.ClientID%>").value = ""; 
                        document.getElementById("<%= txtcust1PIN.ClientID%>").value = ""; 
                        document.getElementById("<%= txtEmpcode.ClientID%>").value = "";
                        document.getElementById("<%= txtEmpcode.ClientID%>").value ="";
                        document.getElementById("<%= txtEmpName.ClientID%>").value="";
                        document.getElementById("<%= txtMail.ClientID%>").value = "";
                        document.getElementById("<%= txtContactNo.ClientID%>").value ="";
                        document.getElementById("<%= txtPIN.ClientID%>").value = ""; 

                    }  
                    
                }
       
             }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

    function RequestOnClick() {   

     if (document.getElementById("<%= cmbATM.ClientID%>").value == "-1") 
        {
            alert("Select ATM");
            document.getElementById("<%= cmbATM.ClientID%>").focus();
            return false;
        }

        if (document.getElementById("<%= txtPIN.ClientID%>").value == "") 
        {
       
            alert("Enter PIN");
            document.getElementById("<%= txtPIN.ClientID%>").focus();
            return false;
        }
        var PIN =document.getElementById("<%= txtPIN.ClientID%>").value;
       
        if (document.getElementById("<%= txtEmpcode.ClientID%>").value == "") 
        {
       
              alert("Enter Custodian 1 employee code");
              document.getElementById("<%= txtEmpcode.ClientID%>").focus();
              return false;
        }
        if (document.getElementById("<%= txtMail.ClientID%>").value == "") 
        {
       
              alert("Enter Mail ID");
              document.getElementById("<%= txtMail.ClientID%>").focus();
              return false;
        }
         if (document.getElementById("<%= txtContactNo.ClientID%>").value == "") 
        {
       
              alert("Enter Contact No");
              document.getElementById("<%= txtContactNo.ClientID%>").focus();
              return false;
        }
        
    var TMKey_ID	= document.getElementById("<%= cmbATM.ClientID%>").value;
    var PIN2	= document.getElementById("<%= txtPIN.ClientID%>").value;
    var PIN1	= document.getElementById("<%= txtCust1PIN.ClientID%>").value;
    var Empcode = document.getElementById("<%= txtEmpcode.ClientID%>").value;
    var Mail	= document.getElementById("<%= txtMail.ClientID%>").value;
    var ContactNo1	= document.getElementById("<%= txtContactNo.ClientID%>").value;
    var ContactNo2	= document.getElementById("<%= txtCust1ContactNo.ClientID%>").value;
     var Empcode1	= document.getElementById("<%= txtEmp1.ClientID%>").value;

    if(isNaN(ContactNo1)){
         alert("Contact No is not valid");
              document.getElementById("<%=txtContactNo.ClientID%>").focus();
              return false;
     }
     if(isNaN(ContactNo2)){
         alert("Contact No is not valid");
              document.getElementById("<%= txtCust1ContactNo.ClientID%>").focus();
              return false;
    }
   if(ContactNo1.length != 10){
        alert("Enter a valid Contact");
        document.getElementById("<%=txtContactNo.ClientID%>").focus();
        return false;
   }
    if(ContactNo2.length != 10){
             alert("Enter a valid Contact");
              document.getElementById("<%= txtCust1ContactNo.ClientID%>").focus();
              return false;
   }
    document.getElementById("<%=hdnValue.ClientID %>").value = "1Ø" + TMKey_ID + "Ø" + PIN2 + "Ø" + Empcode + "Ø" + Mail + "Ø" + ContactNo2 + "Ø" + PIN1  + "Ø" + ContactNo1 + "Ø" + Empcode1 ;       

}


            function window_onload(){
                
            }
            function ATMOnChange() {        
              // debugger;
                if (document.getElementById("<%= cmbATM.ClientID%>").value != "-1")
                {
                    var ToData = "2Ø" + document.getElementById("<%= cmbATM.ClientID%>").value; 
                    ToServer(ToData, 2);
                }
           
            }

            function EmployeeOnChange() 
            {   
                    
                    var EmpCode = document.getElementById("<%= txtEmpCode.ClientID%>").value;
                    
        
                    if (EmpCode != "")
                        ToServer("1Ø" + EmpCode , 1);
              }

             function viewSummary()
        {
           var BranchID = document.getElementById("<%= cmbATM.ClientID %>").value;
           if (BranchID !="-1")
           {
            window.open("Reports/ViewATMPinResent.aspx?TR_ID=" + btoa(BranchID)); }
            
            return false;
        }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return window_onload()
            // ]]>
        </script>
    </head>
    </html>
   
    <br />
    <br />
       <table class="style1" style="width: 80%; margin: 0px auto;">
         
        <tr id="Tr2">
            <td style="width: 20%;">
            </td>
            <td style="width: 17%; text-align: left;">
                ATM</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbATM" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
                <img id="ViewAsset" onclick="viewSummary()" src="../../Image/eswtClose2.png" 
                    style="height:20px; width:20px;  cursor:pointer;" title="View ATM history" /></td>
        </tr>
        <tr id="Tr5">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
               Branch</td> <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranch" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
            </td>
        </tr>
         <tr id="Tr3" style="display:none">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
                PIN set 1<td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCust1PIN" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="32"  ReadOnly="true"/>
            </td>
        </tr>
         <tr id="Tr4">
            <td style="width: 20%;">
                &nbsp;
                   <asp:HiddenField ID="hid_PrevEmpcode" runat="server" />
            </td>
            <td style="width: 17%; text-align: left;">
               Employee Code</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmp1" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="15"  
                     ReadOnly="true"/>
            </td>
           
        </tr>   
        <tr id="EmpCode">
            <td style="width: 20%;">
                &nbsp;
                   <asp:HiddenField ID="hid_Empcode" runat="server" />
            </td>
            <td style="width: 17%; text-align: left;">
               Custodian 1
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCust1" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="50" onkeypress='return NumericCheck(event)' ReadOnly="true" />
            </td>
        </tr>
       
         
        <tr id="mail">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
               Custodian 1
                E-mail<td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCust1Mail" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="15" ReadOnly="true"  />
            </td>
        </tr>
        <tr id="ContactNo">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
               Custodian 1 Contact No</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox 
                    ID="txtCust1ContactNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalTextBox" MaxLength="10"   onkeypress='return NumericCheck(event)'/>
            </td>
        </tr>
        <tr id="Tr7" style="display:none">
            <td style="width: 20%;">
               
            </td>
            <td style="width: 17%; text-align: left;">
                Created By
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCreatedBy" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="100" ReadOnly="true"  />
            &nbsp;</td>
        </tr>
        <tr id="Tr15" style="display:none">
            <td style="width: 20%;">
               
            </td>
            <td style="width: 17%; text-align: left;">
                Created on
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCreatedOn" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="100" ReadOnly="true"  />
            &nbsp;</td>
        </tr>

        <tr><td></td>
        <td></td> <td style="width: 63%">
                &nbsp; &nbsp;&nbsp;<asp:Button ID="btnsend1" runat="server" Text="SEND" 
                    Font-Names="Cambria" Style="cursor: pointer;"
                    Width="12%" /></td>
        </tr>

        <tr id="Tr6" style="display:none">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
                PIN set 2</td><td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtPIN" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="32" 
                    ReadOnly="True"  />
            </td>
        </tr>
           
        <tr id="Tr8">
            <td style="width: 20%;">
                &nbsp;
                   <asp:HiddenField ID="HiddenField1" runat="server" />
            </td>
            <td style="width: 17%; text-align: left;">
               Employee Code
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpcode" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="20%" class="ReadOnlyTextBox" MaxLength="50" 
                    onkeypress='return NumericCheck(event)' ReadOnly="True"  />
            </td>
        </tr>
        <tr id="Tr9">
            <td style="width: 20%;">
                &nbsp;
                 <asp:HiddenField ID="HiddenField2" runat="server" />
            </td>
            <td style="width: 17%; text-align: left;">
                Emp Name
               
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpName" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="ReadOnlyTextBox" MaxLength="500" ReadOnly="true" />
                    
            </td>
        </tr>
                
        <tr id="Tr12">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
                E-mail<td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtMail" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%"  class="ReadOnlyTextBox" MaxLength="15" 
                    ReadOnly="True"  />
            </td>
        </tr>
        <tr id="Tr13">
            <td style="width: 20%;">
                &nbsp;
            </td>
            <td style="width: 17%; text-align: left;">
                Contact No</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtContactNo" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%"  class="NormalTextBox" MaxLength="10"  />
            </td>
        </tr>
       <tr id="Img">
        <td class="style2"><asp:HiddenField 
                    ID="hdn_Post" runat="server" />
                </td>
            <td style="text-align:left;" class="style3">
                </td>
            <td style="text-align:left;" class="style4">
                &nbsp;&nbsp;
                <asp:Button ID="btnSend2" runat="server" Text="SEND" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="12%" />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 12%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" /></td>

        </tr>
      
      
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:HiddenField ID="hdn_RetailFlg" runat="server" />
                &nbsp;
                    &nbsp;
                &nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <configuration>
    <system.web>
    <compilaton debug="true" targetFramework="4.0"></compilaton>
    </system.web>
</configuration>
</asp:Content>
