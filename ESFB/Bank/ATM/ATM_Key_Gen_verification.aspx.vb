﻿
Imports System.Data
Imports System.Data.SqlClient


Imports System.Collections
Imports System.Configuration

Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq


Imports System.IO
Imports System.Net
Imports System.Web.Services
Imports System.Web.Script.Serialization
Partial Class ATM_Key_Gen_verification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1386) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "ATM TM Key Verification"


            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0

            Dim StrVal As String = ""


            DT = DB.ExecuteDataSet("select -1 as Tr_ID,' -----Select-----' as atm_ID union all   select tmkey_id,a.atm_id from atm_key_master a,atm_master b where a.tr_id=b.tr_id and a.status_id=1 order by 2 ").Tables(0)

            GN.ComboFill(cmbATM, DT, 0, 1)

            Me.cmbATM.Attributes.Add("onchange", "return ATMOnChange()")
            Me.txtEmpcode.Attributes.Add("onchange", "EmployeeOnChange()")

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            Me.btnReject.Attributes.Add("onclick", "return RejectOnClick()")
            cmbATM.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim EmpID As Integer = CInt(Data(1))

        If CInt(Data(0)) = 1 Then
            DT = DB.ExecuteDataSet("select distinct a.emp_code,a.emp_name,department_name,designation_name,cug_no,Official_mail_id " & _
                   " from  Emp_master a,emp_profile b,department_master c,designation_master d " & _
                   " where a.emp_code=b.emp_code and a.department_id=c.Department_ID and a.designation_id=d.designation_id  " & _
                   " and a.status_id=1 and  a.emp_code=" + CInt(Data(1)).ToString + " ").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString()

            Else
                CallBackReturn = "ØØ"
            End If

        ElseIf CInt(Data(0)) = 2 Then
            Dim TMKEY_ID As Integer = CInt(Data(1))

            DT = DB.ExecuteDataSet("select tmkey_id,c.branch_name,a.created_refer_no" & _
                    " ,f.emp_name+'( '+convert(varchar,f.emp_code)+' )' as custodian1,custodian1_mail,custodian1_contactno " & _
                    " ,remarks ,d.emp_name+'( '+convert(varchar,d.emp_code)+' )',a.created_on ,b.tr_id,a.PIN1,a.custodian1,b.site_name from ATM_key_Master a left join emp_list d on a.created_by=d.emp_code left join emp_list e on a.verified_by=e.emp_code" & _
                    " left join emp_list f on a.custodian1=f.emp_code  " & _
                    " ,atm_master b,branch_master c  where a.tr_id=b.tr_id and b.branch_id=c.branch_id and a.status_id=1 and tmkey_id=" & TMKEY_ID & "").Tables(0)


            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString() + "Ø" + DT.Rows(0)(6).ToString() + "Ø" + DT.Rows(0)(7).ToString() + "Ø" + DT.Rows(0)(8).ToString() + "Ø" + DT.Rows(0)(9).ToString() + "Ø" + DT.Rows(0)(10).ToString() + "Ø" + DT.Rows(0)(11).ToString() + "Ø" + DT.Rows(0)(12).ToString()

            Else
                CallBackReturn = "ØØ"
            End If


        End If
    End Sub
#End Region

    Private Sub initializeControls()

        cmbATM.Text = "-1"
        txtMail.Text = ""
        txtContactNo.Text = ""
        txtPIN.Text = ""
        txtRefNo.Text = ""
        txtRemarks.Text = ""
        txtEmpcode.Text = ""


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing

            Dim Result As String
            Dim UserID As String = Session("UserID").ToString()
            Dim TR_ID As Integer
            Dim Empcode As Integer
            Dim Mail As String
            Dim Mobile1 As String
            Dim Mobile2 As String
            Dim PIN1 As String
            Dim PIN2 As String
            Dim RefNo As String
            Dim RefNo1 As String
            Dim Remarks As String
            Dim Mobileno1 As String
            Dim Mobileno2 As String
            Dim Mail1 As String


            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            If Data(0) = 1 Then
                TR_ID = CInt(Data(1))
                Empcode = CInt(Data(4))
                Mail = CStr(Data(5))
                Mobileno1 = CStr(Data(6))
                PIN2 = CStr(Data(2))
                RefNo = CStr(Data(3))
                Remarks = CStr(Data(7))
                PIN1 = CStr(Data(8))
                Mobileno2 = CStr(Data(9))
                Mail1 = CStr(Data(10))
            Else
                TR_ID = CInt(Data(1))
                Empcode = 0
                Mail = ""
                PIN1 = ""
                PIN2 = ""
                RefNo = ""
                Remarks = CStr(Data(2))
                Mobileno1 = ""
                Mobileno2 = ""
                Mail1 = ""
            End If
            Try
                Dim Params(11) As SqlParameter
                Params(0) = New SqlParameter("@TMKEY_ID", SqlDbType.Int)
                Params(0).Value = TR_ID
                Params(1) = New SqlParameter("@Empcode", SqlDbType.Int)
                Params(1).Value = Empcode
                Params(2) = New SqlParameter("@Mail", SqlDbType.VarChar, 100)
                Params(2).Value = Mail
                Params(3) = New SqlParameter("@ContactNo", SqlDbType.VarChar, 20)
                Params(3).Value = Mobileno2
                Params(4) = New SqlParameter("@PIN", SqlDbType.VarChar, 32)
                Params(4).Value = PIN2
                Params(5) = New SqlParameter("@RefNo", SqlDbType.VarChar, 10)
                Params(5).Value = ""
                Params(6) = New SqlParameter("@Remarks", SqlDbType.NVarChar, 1000)
                Params(6).Value = Remarks
                Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(8).Direction = ParameterDirection.Output
                Params(9) = New SqlParameter("@TYPE", SqlDbType.TinyInt)
                Params(9).Value = Data(0)
                Params(10) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(10).Value = UserID
                Params(11) = New SqlParameter("@Mail1", SqlDbType.VarChar, 500)
                Params(11).Value = Mail1
                DB.ExecuteNonQuery("[SP_ATM_KEY_REG_VERIFY]", Params)
                ErrorFlag = CInt(Params(7).Value)
                Message = CStr(Params(8).Value)
                Dim RetStr() As String

                Dim MsgId As String

                Dim Count As Integer = 0
                Dim DT As New DataTable
                Dim StrUrl As String
                If ErrorFlag = 0 And Data(0) = 1 Then
                    If (Mobileno1 <> "" And Mobileno2 <> "") Then
                        ' Mobile1 = "918589929284" '+ Mobileno1.ToString()
                        Mobile1 = "91" + Mobileno1.ToString()
                        Mobile2 = "91" + Mobileno2.ToString()
                        DT = DB.ExecuteDataSet("SELECT sender,username,password FROM ESFB.dbo.SMS_PORTAL WHERE STATUS_ID =1").Tables(0)
                        If DT.Rows.Count > 0 Then
                            Dim SenderName As String = ""
                            Dim UserName As String = ""
                            Dim PassWord As String = ""
                            SenderName = DT.Rows(0).Item(0).ToString()
                            UserName = DT.Rows(0).Item(1).ToString()
                            PassWord = DT.Rows(0).Item(2).ToString()

                            'Modified on 09-oct-2020 by 40013 as per mail request 
                            'StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & SenderName & "&SMSText=" & "Custodian 1 PIN is " & PIN1 & "&GSM=" & Mobile1.ToString
                            StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & SenderName & "&SMSText=" & "ESAF  Bank TM Key 1 is " & PIN1 & "&GSM=" & Mobile1.ToString

                            RetStr = WEB_Request_Response(StrUrl, 1)
                            'StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & SenderName & "&SMSText=" & "Custodian 2 PIN is " & PIN2 & "&GSM=" & Mobile2.ToString
                            StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & SenderName & "&SMSText=" & "ESAF Bank TM Key 2 is " & PIN2 & "&GSM=" & Mobile2.ToString

                            RetStr = WEB_Request_Response(StrUrl, 1)
                        End If

                    Else
                        MsgBox("Conact numbers is null")


                    End If

                End If
            Catch ex1 As Exception
                Message = ex1.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("        window.open('ATM_Key_Gen_verification.aspx', '_self');")


            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)





        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try

    End Sub
    Public Shared Function WEB_Request_Response(Request As String, Request_type As Integer) As String()

        'Added On 22-10-2020 by 40013 - Changed request from IT
        Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
        Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString

        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        If Request_type = 1 Then
            delimiters = {"<", ">", ">/", "</", "/"}
        Else
            delimiters = {"=", " "}
        End If
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)


        Return parts.ToArray()


    End Function

    Protected Sub btnReject_Click(sender As Object, e As System.EventArgs) Handles btnReject.Click
        Try

            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing

            Dim Result As String
            Dim UserID As String = Session("UserID").ToString()
            Dim TR_ID As Integer
            Dim Empcode As Integer
            Dim Mail As String
            Dim Mobile1 As String
            Dim Mobile2 As String
            Dim PIN1 As String
            Dim PIN2 As String
            Dim RefNo As String
            Dim Remarks As String
            Dim Mobileno1 As String
            Dim Mobileno2 As String


            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))

            TR_ID = CInt(Data(1))
            Empcode = 0
            Mail = ""
            PIN1 = ""
            PIN2 = ""
            RefNo = ""
            Remarks = CStr(Data(7))
            Mobileno1 = ""
            Mobileno2 = ""



            Try
                Dim Params(10) As SqlParameter
                Params(0) = New SqlParameter("@TMKEY_ID", SqlDbType.Int)
                Params(0).Value = TR_ID
                Params(1) = New SqlParameter("@Empcode", SqlDbType.Int)
                Params(1).Value = Empcode
                Params(2) = New SqlParameter("@Mail", SqlDbType.VarChar, 100)
                Params(2).Value = Mail
                Params(3) = New SqlParameter("@ContactNo", SqlDbType.VarChar, 20)
                Params(3).Value = Mobileno2
                Params(4) = New SqlParameter("@PIN", SqlDbType.VarChar, 32)
                Params(4).Value = PIN2
                Params(5) = New SqlParameter("@RefNo", SqlDbType.VarChar, 10)
                Params(5).Value = RefNo
                Params(6) = New SqlParameter("@Remarks", SqlDbType.NVarChar, 1000)
                Params(6).Value = Remarks
                Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(8).Direction = ParameterDirection.Output
                Params(9) = New SqlParameter("@TYPE", SqlDbType.TinyInt)
                Params(9).Value = Data(0)
                Params(10) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(10).Value = UserID
                DB.ExecuteNonQuery("[SP_ATM_KEY_REG_VERIFY]", Params)
                ErrorFlag = CInt(Params(7).Value)
                Message = CStr(Params(8).Value)




            Catch ex1 As Exception
                Message = ex1.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try






            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("        window.open('ATM_Key_Gen_verification.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)





        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
