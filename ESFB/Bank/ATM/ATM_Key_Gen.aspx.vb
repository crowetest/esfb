﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ATM_Key_Gen
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1385) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "ATM TM Key Generation"


            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0

            Dim StrVal As String = ""


            DT = DB.ExecuteDataSet("select -1 as Tr_ID,' -----Select-----' as atm_ID union all   select distinct Tr_ID,atm_ID+'('+convert(varchar,branch_id)+')' from ATM_Master  order by 2 ").Tables(0)
            
            GN.ComboFill(cmbATM, DT, 0, 1)

            Me.cmbATM.Attributes.Add("onchange", "return ATMOnChange()")
            Me.txtEmpCode.Attributes.Add("onchange", "EmployeeOnChange()")
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            cmbATM.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim EmpID As Integer = CInt(Data(1))

        If CInt(Data(0)) = 1 Then
            DT = DB.ExecuteDataSet("select distinct a.emp_code,a.emp_name,department_name,designation_name,cug_no,Official_mail_id " & _
                   " from  Emp_master a,emp_profile b,department_master c,designation_master d " & _
                   " where a.emp_code=b.emp_code and a.department_id=c.Department_ID and a.designation_id=d.designation_id  " & _
                   " and a.status_id=1 and  a.emp_code=" + CInt(Data(1)).ToString + "").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString()

            Else
                CallBackReturn = "ØØ"
            End If
           
        ElseIf CInt(Data(0)) = 2 Then
            Dim TR_ID As Integer = CInt(Data(1))

            DT = DB.ExecuteDataSet("select atm_id,site_name,branch_name from  ATM_Master a,branch_master b where a.branch_id=b.branch_id and tr_id=" & TR_ID & "").Tables(0)
        

            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString()

            Else
                CallBackReturn = "ØØ"
            End If


        End If
    End Sub
#End Region

    Private Sub initializeControls()

        cmbATM.Text = "-1"
        txtMail.Text = ""
        txtContactNo.Text = ""
        txtPIN.Text = ""
        txtRefNo.Text = ""
        txtRemarks.Text = ""

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            
            Dim Result As String
            Dim UserID As String = Session("UserID").ToString()

            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim TR_ID As Integer = CInt(Data(1))
            Dim Empcode As Integer = CInt(Data(4))
            Dim Mail As String = CStr(Data(5))
            Dim ContactNo As String = CStr(Data(6))
            Dim PIN As String = CStr(Data(2))
            Dim RefNo As String = CStr(Data(3))
            Dim Remarks As String = CStr(Data(7))




            Try
                Dim Params(9) As SqlParameter
                Params(0) = New SqlParameter("@TR_ID", SqlDbType.Int)
                Params(0).Value = TR_ID
                Params(1) = New SqlParameter("@Empcode", SqlDbType.Int)
                Params(1).Value = Empcode
                Params(2) = New SqlParameter("@Mail", SqlDbType.VarChar, 100)
                Params(2).Value = Mail
                Params(3) = New SqlParameter("@ContactNo", SqlDbType.VarChar, 20)
                Params(3).Value = ContactNo
                Params(4) = New SqlParameter("@PIN", SqlDbType.VarChar, 32)
                Params(4).Value = PIN
                Params(5) = New SqlParameter("@RefNo", SqlDbType.VarChar, 10)
                Params(5).Value = ""
                Params(6) = New SqlParameter("@Remarks", SqlDbType.NVarChar, 1000)
                Params(6).Value = Remarks
                Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(8).Direction = ParameterDirection.Output
                Params(9) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(9).Value = UserID
              
                DB.ExecuteNonQuery("[SP_ATM_KEY_REG]", Params)
                ErrorFlag = CInt(Params(7).Value)
                Message = CStr(Params(8).Value)
            Catch ex1 As Exception
                Message = ex1.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("        window.open('ATM_Key_Gen.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)





        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
