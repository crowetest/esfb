﻿
Imports System.Data
Imports System.Data.SqlClient


Imports System.Collections
Imports System.Configuration

Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq


Imports System.IO
Imports System.Net
Imports System.Web.Services
Imports System.Web.Script.Serialization
Partial Class ATM_TechLive
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1387) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "ATM Tech Live Updation"


            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0

            Dim StrVal As String = ""


            DT = DB.ExecuteDataSet("select tmkey_id,a.atm_id+'('+convert(varchar,b.branch_id)+')' from atm_key_master a,atm_master b where a.tr_id=b.tr_id and a.status_id in (2,6) and b.branch_id=" & CInt(Session("BranchID")) & " order by 2 ").Tables(0)

            GN.ComboFill(cmbATM, DT, 0, 1)



            Me.cmbATM.Attributes.Add("onchange", "return ATMOnChange()")
           
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")

            cmbATM.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim EmpID As Integer = CInt(Data(1))

       If CInt(Data(0)) = 2 Then
            Dim TMKEY_ID As Integer = CInt(Data(1))

            DT = DB.ExecuteDataSet("select tmkey_id,c.branch_name,b.site_name" & _
                    " ,f.emp_name+'( '+convert(varchar,f.emp_code)+' )' as custodian1,custodian1_mail,custodian1_contactno " & _
                    " ,d.emp_name+'( '+convert(varchar,d.emp_code)+' )' as custodian2,custodian2_mail,custodian2_contactno " & _
                    "  from ATM_key_Master a left join emp_list d on a.custodian2=d.emp_code" & _
                    " left join emp_list f on a.custodian1=f.emp_code  " & _
                    " ,atm_master b,branch_master c  where b.branch_id=c.branch_id and a.status_id in (2,6) and tmkey_id=" & TMKEY_ID & "").Tables(0)


            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString() + "Ø" + DT.Rows(0)(6).ToString() + "Ø" + DT.Rows(0)(7).ToString() + "Ø" + DT.Rows(0)(8).ToString()

            Else
                CallBackReturn = "ØØ"
            End If


        End If
    End Sub
#End Region

    Private Sub initializeControls()

        cmbATM.Text = "-1"
        txtMail.Text = ""
        txtContactNo.Text = ""
       
        txtRemarks.Text = ""

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing


            Dim UserID As String = Session("UserID").ToString()
            Dim TR_ID As Integer
            Dim Remarks As String
            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))

            TR_ID = CInt(Data(1))
            Remarks = CStr(Data(3))
            Dim TechLiveDate As Date = CDate(IIf(Data(2) = "", "01/01/1900", Data(2)))
            


            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@TMKEY_ID", SqlDbType.Int)
                Params(0).Value = TR_ID
                Params(1) = New SqlParameter("@Remarks", SqlDbType.NVarChar, 1000)
                Params(1).Value = Remarks
                Params(2) = New SqlParameter("@TechLiveDate", SqlDbType.Date)
                Params(2).Value = TechLiveDate
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(5).Value = UserID
                DB.ExecuteNonQuery("[SP_ATM_TECH_LIVE]", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
               
            Catch ex1 As Exception
                Message = ex1.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try






            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("        window.open('ATM_TechLive.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)





        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try

    End Sub
    Public Shared Function WEB_Request_Response(Request As String, Request_type As Integer) As String()

        'Added On 22-10-2020 by 40013 - Changed request from IT
        Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
        Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString

        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        If Request_type = 1 Then
            delimiters = {"<", ">", ">/", "</", "/"}
        Else
            delimiters = {"=", " "}
        End If
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)


        Return parts.ToArray()


    End Function

  
End Class
