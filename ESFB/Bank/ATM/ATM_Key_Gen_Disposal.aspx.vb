﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ATM_Key_Gen_Disposal
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If GF.FormAccess(CInt(Session("UserID")), 1296) = False And CInt(Session("BranchID")) <> 0 Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            DT = DB.ExecuteDataSet("select tmkey_id,a.atm_id,b.site_name,c.branch_name,d.emp_name+'( '+convert(varchar,d.emp_code)+' )',a.created_on,e.emp_name+'( '+convert(varchar,e.emp_code)+' )',a.verified_on " & _
                    " ,f.emp_name+'( '+convert(varchar,f.emp_code)+' )' as custodian1,custodian1_mail,custodian1_contactno " & _
                    " ,g.emp_name+'( '+convert(varchar,g.emp_code)+' )' as custodian2,custodian2_mail,custodian2_contactno,'Entered Remarks :'+remarks +' <br/>'+'Verified Remarks :'+verified_remarks,b.tr_id,a.verified_refer_no " & _
                    " from ATM_key_Master a left join emp_list d on a.created_by=d.emp_code left join emp_list e on a.verified_by=e.emp_code" & _
                    " left join emp_list f on a.custodian1=f.emp_code   left join emp_list g on a.custodian2=g.emp_code  " & _
                    " ,atm_master b,branch_master c  where a.tr_id=b.tr_id and b.branch_id=c.branch_id and a.status_id=8").Tables(0)

            Me.Master.subtitle = "ATM Key Disposal"

            Dim StrAttendance As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString & "µ" & DT.Rows(n)(16).ToString

                If n < DT.Rows.Count - 1 Then
                    StrAttendance += "¥"
                End If
            Next

            hid_dtls.Value = StrAttendance
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim dataval As String = CStr(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))

        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try

            Dim Params(3) As SqlParameter
            Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@AppDtl", SqlDbType.VarChar)
            Params(1).Value = dataval.Substring(1)
            Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(2).Direction = ParameterDirection.Output
            Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(3).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_TRACKER_BRD_APPROVAL", Params)
            ErrorFlag = CInt(Params(2).Value)
            Message = CStr(Params(3).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message



    End Sub
#End Region

End Class
