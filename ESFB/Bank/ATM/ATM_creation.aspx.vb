﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ATM_creation
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1393) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "New ATM Registration"
            Me.CEFromDate.StartDate = CDate(Session("TraDt"))

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            DT = DB.ExecuteDataSet("select -1 as Branch_ID,' -----Select-----' as Branch_Name union all   select distinct Branch_ID,Branch_Name from BRANCH_MASTER order by 2 ").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)

            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0

            Dim StrVal As String = ""


            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")


        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim EmpID As Integer = CInt(Data(1))

        If CInt(Data(0)) = 1 Then
            DT = DB.ExecuteDataSet("select -1 as item_ID,' -----Select-----' as item_name union all select item_ID,item_name from BK_AT_item_master where status_id = 1 and category_id=" & Data(1).ToString & " order by item_Name").Tables(0)
            If (DT.Rows.Count > 0) Then
                Dim StrVal1 As String = ""

                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal1 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal1 += "Ñ"
                    End If
                Next

                Dim StrVal2 As String = ""
                CallBackReturn = StrVal1

            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf CInt(Data(0)) = 2 Then
            Dim BranchID As Integer = CInt(Data(1))
            If BranchID > 100 Then
                DT = DB.ExecuteDataSet("select department_ID,department_name from department_master " & _
                                   " where status_id = 1 and department_id=0 order by department_Name").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select department_ID,department_name from department_master " & _
                                   " where status_id = 1 and department_id=0 order by department_Name").Tables(0)
            End If

            If (DT.Rows.Count > 0) Then
                Dim StrVal1 As String = ""

                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal1 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal1 += "Ñ"
                    End If
                Next

                Dim StrVal2 As String = ""
                CallBackReturn = StrVal1

            Else
                CallBackReturn = "ØØ"
            End If


        End If
    End Sub
#End Region

    Private Sub initializeControls()

        cmbBranch.Text = "-1"
        txtAtm.Text = ""
        txtSite.Text = ""
        txteffdate.Text = ""


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing

            Dim Result As String
            Dim UserID As String = Session("UserID").ToString()

            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim ATM_ID As String = CStr(Data(1))
            Dim Branch As Integer = CInt(Data(4))
            Dim SiteName As String = CStr(Data(2))
            'Dim Make_ID As Integer = CInt(Data(3))



            Dim Effdate As Date = CDate(IIf(Data(3) = "", "01/01/1900", Data(3)))



            Try
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
                Params(0).Value = Branch
                Params(1) = New SqlParameter("@ATM_ID", SqlDbType.VarChar, 100)
                Params(1).Value = ATM_ID
                Params(2) = New SqlParameter("@Site_Name", SqlDbType.VarChar, 100)
                Params(2).Value = SiteName
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = UserID
                Params(4) = New SqlParameter("@delivery_date", SqlDbType.Date)
                Params(4).Value = Effdate
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
               
               

                DB.ExecuteNonQuery("SP_ATM_NEW_REG", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
            Catch ex1 As Exception
                Message = ex1.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("        window.open('ATM_Creation.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)





        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
