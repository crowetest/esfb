﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="ATM_TechLive_Approval.aspx.vb" Inherits="ATM_TechLive_Approval" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function table_fill() {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:1%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:10%;text-align:left' >ATM ID</td>";
            tab += "<td style='width:10%;text-align:left' >Site Name</td>";
            tab += "<td style='width:10%;text-align:left'>Branch</td>"; 
            tab += "<td style='width:10%;text-align:left'>Tech Live Date</td>"; 
            tab += "<td style='width:10%;text-align:left' >Tech Live Entered By</td>";
            tab += "<td style='width:10%;text-align:left'>Tech Live Entered On</td>";
            tab += "<td style='width:10%;text-align:left'>Branch Remarks</td>";
            tab += "<td style='width:10%;text-align:center'>Select</td>";
            tab += "<td style='width:21%;text-align:center'>Remarks</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                    

                    tab += "<td style='width:1%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:10%;text-align:left' >" + col[1] + " <img id='ViewAttachment' src='../../Image/eswtClose2.png' onclick='viewHistory("+ col[8] +")' title='View History'  style='height:20px; width:20px;  cursor:pointer;' /> </td>";             
                    tab += "<td style='width:10%;text-align:left' >" + col[2] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[4] + "</td>"; 
                    tab += "<td style='width:10%;text-align:left' >" + col[5] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:10%;text-align:center'>" + col[7] + "</td>";
                   
                    var select = "<select id='cmb" + i + "' class='NormalText' style='width:99%;' name='cmb" + i + "' >";
                    select += "<option value='-1'>Select</option>";
                    select += "<option value='1'>Approved</option>";
                    select += "<option value='2'>Rejected</option>";

                    tab += "<td style='width:10%;text-align:left'>" + select + "</td>";
                    var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)'  ></textarea>";

                    tab += "<td style='width:21%;text-align:left'>" + txtBox + "</td>";


                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;


            //--------------------- Clearing Data ------------------------//


        }
       
        function UpdateValue() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("No pending Approval");
                return false;
            }

            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
             document.getElementById("<%= hid_temp.ClientID %>").value  ="1Ø" ;
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");
               var i=n+1;
                
                  if (document.getElementById("cmb" + i).value != "-1") {
                    if (document.getElementById("txtRemarks" + i).value == "" && document.getElementById("cmb" + i).value=="2") {
                        alert("Enter Rejected Reason");
                        document.getElementById("txtRemarks" + i).focus();
                        return false;
                    }
                    
                    
                    document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[0]  + "µ"  + document.getElementById("cmb" + i).value + "µ" + document.getElementById("txtRemarks" + i).value  ;
                }
            }

            return true;
        }
        function FromServer(arg, context) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("ATM_TechLive_Approval.aspx", "_self");
        }
        function btnApprove_onclick() {
            var ret = UpdateValue();
            if (ret == 0) return false;
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                alert("Select Any Request for Approval");
                return false;
            }

            var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
            var Data = "1Ø" + strempcode;
            ToServer(Data, 1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
       function viewHistory(ID)
        {
           
           
            window.open("Reports/ViewATMHistory.aspx?TR_ID=" + btoa(ID));
            
            return false;
        }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

