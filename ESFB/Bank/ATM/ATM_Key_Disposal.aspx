﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  CodeFile="ATM_Key_Disposal.aspx.vb" Inherits="ATM_Key_Disposal" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
     <style type="text/css">
    #Button
    {
        width:100%;
        height:40px;
        font-weight:bold;
        line-height:40px;
        text-align:center;
        border-top-left-radius: 25px;
	    border-top-right-radius: 25px;
	    border-bottom-left-radius: 25px;
	    border-bottom-right-radius: 25px;
        cursor:pointer;
        background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
        color:#E0E0E0;
    }
        
    #Button:hover
    {
        background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        color:#036;
    }        
</style>
    <script language="javascript" type="text/javascript">
       function table_fill() {
       
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:1%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:7%;text-align:left' >ATM ID</td>";
            tab += "<td style='width:6%;text-align:left' >Site Name</td>";
            tab += "<td style='width:6%;text-align:left'>Branch</td>"; 
             tab += "<td style='width:4%;text-align:left'>PIN ReferNo</td>"; 
            tab += "<td style='width:7%;text-align:left' >Entered By</td>";
            tab += "<td style='width:6%;text-align:left'>Entered On</td>";
            tab += "<td style='width:6%;text-align:left'>Verified By</td>";
            tab += "<td style='width:6%;text-align:left'>Verified On</td>";
            tab += "<td style='width:6%;text-align:left'>Custodian1</td>";
            tab += "<td style='width:6%;text-align:left'>Custodian1 Mail</td>";
            tab += "<td style='width:5%;text-align:left'>Custodian1 Mobile No</td>";
            tab += "<td style='width:6%;text-align:left'>Custodian2</td>";
            tab += "<td style='width:6%;text-align:left'>Custodian2 Mail</td>";
            tab += "<td style='width:5%;text-align:left'>Custodian2 Mobile No</td>";
            tab += "<td style='width:7%;text-align:left'>Remarks</td>";
            tab += "<td style='width:4%;text-align:center'>Save</td>";
            tab += "<td style='width:7%;text-align:center'>Remarks</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                    

                    tab += "<td style='width:1%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:7%;text-align:left' >" + col[1] + " <img id='ViewAttachment' src='../../Image/eswtClose2.png' onclick='viewHistory("+ col[15] +")' title='View History'  style='height:20px; width:20px;  cursor:pointer;' /> </td>";
                    tab += "<td style='width:6%;text-align:left' >" + col[2] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[3] + "</td>";  
                     tab += "<td style='width:4%;text-align:left'>" + col[16] + "</td>";  
                    tab += "<td style='width:7%;text-align:left' >" + col[4] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[8] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[9] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[10] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[11] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[12] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[13] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[14] + "</td>";
           

                    var LinkText = "Dispose";
                    tab += "<td style='width:4%; text-align:center;  padding-left:5px; ' onclick=Startval(" + col[0] + "," + col[15] + "," + i + ")><div id='Button' >" + LinkText + "</div></td>";

                   


                    var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)' ></textarea>";

                    tab += "<td style='width:7%;text-align:left'>" + txtBox + "</td>";


                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
            ToServer("1Ø" , 1);

            //--------------------- Clearing Data ------------------------//


        }
       
        function Startval(trkey_id, tr_ID, i) {
      
        
            if (document.getElementById("txtRemarks" + i).value == "") {
                alert("Enter The Reason For Disposal");
                document.getElementById("txtRemarks" + i).focus();
            }
            else {
            
                var Remarks = document.getElementById("txtRemarks" + i).value;
                
                if (confirm("Are you sure to Dispose This ?") == 1) {
                    var ToData = "1Ø" +  "Ø" + "¥" + trkey_id + "µ" + tr_ID + "µ" + Remarks + "µ1";
                    alert(ToData);
                    ToServer("1", 1);
                    alert(100);
                }
            }
            
           
        }
       function viewHistory(ID)
        {
           
           
            window.open("Reports/ViewATMHistory.aspx?TR_ID=" + btoa(ID));
            
            return false;
        }
        function FromServer(arg, context) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("ATM_Key_Disposal.aspx", "_self");
        }
        
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
       
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
<configuration><system.web><compilation  debug="true" targetframework="4.0" ></compilation></system.web></configuration>
</asp:Content>

