﻿Imports System.Data
Partial Class ATM_Disposal_Report
    Inherits System.Web.UI.Page
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1396) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Asset Disposal Details"

            GN.ComboFill(cmbBranch, GetBranch(CInt(Session("BranchID"))), 0, 1)

            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "DisplaySettings();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetBranch(id As Integer) As DataTable
        Dim strval As String
        If id > 1000 Then
            Return DB.ExecuteDataSet("select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id=1 and branch_id=" & id & " order by 2").Tables(0)
        Else

            Return DB.ExecuteDataSet("select '-1' as branchid,' ------SELECT------' as branch Union All select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id > 0 and branch_id in (select distinct branch_id from atm_key_master a,atm_master b  where a.atm_id=b.atm_id and a.status_id=10) order by 2").Tables(0)
        End If

    End Function
End Class
