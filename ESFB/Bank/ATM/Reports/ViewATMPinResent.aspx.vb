﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewATMPinResent
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Dim DTHead As New DataTable
        Dim DT As New DataTable

        Dim TR_ID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("TR_ID")))
        'Dim TR_ID As Integer = CInt(Request.QueryString.Get("TR_ID"))
        'TR_ID = 19
        DTHead = DB.ExecuteDataSet("select a.atm_id,site_name,branch_name,a.branch_id from [atm_master] a,branch_master b,ATM_Key_Master AT where a.branch_id=b.branch_id and a.ATM_ID=AT.atm_id and  AT.TMKEY_id=" & TR_ID & " ").Tables(0)
        Dim ATM_ID As String = DTHead.Rows(0)(0).ToString
        Dim Sitename As String = DTHead.Rows(0)(1).ToString
        Dim Branch As String = DTHead.Rows(0)(2).ToString
        Dim BranchID As String = DTHead.Rows(0)(3).ToString



        RH.Heading(CStr(Session("FirmName")), tb, "ATM TMKEY Pin Recent Details " + " - " + ATM_ID + "               (Branch:-" + Branch + "(" + BranchID + ")" + "-" + Sitename + ")", 100)
        Dim RowBG As Integer = 0
        tb.Attributes.Add("width", "100%")
        RH.BlankRow(tb, 6)

        Dim TRHead1 As New TableRow
        TRHead1.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10, TRHead1_11, TRHead1_12, TRHead1_13 As New TableCell
        RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "Sl No.")
        RH.InsertColumn(TRHead1, TRHead1_01, 8, 0, "ATM_ID ")
        RH.InsertColumn(TRHead1, TRHead1_02, 8, 0, "Pin Send BY")
        RH.InsertColumn(TRHead1, TRHead1_03, 8, 0, "Pin Send On")
        RH.InsertColumn(TRHead1, TRHead1_04, 8, 0, "Pin Send To")
        RH.InsertColumn(TRHead1, TRHead1_05, 8, 0, "Custodian Type")
        RH.InsertColumn(TRHead1, TRHead1_06, 8, 0, "Custodian Name")
        RH.InsertColumn(TRHead1, TRHead1_07, 8, 0, "Custodian Email")
        RH.InsertColumn(TRHead1, TRHead1_08, 8, 0, "Verified On")

        tb.Controls.Add(TRHead1)

        DT = DB.ExecuteDataSet("select atm_id,f.emp_name+'( '+convert(varchar,f.emp_code)+' )' as sendby,send_on,send_number,send_flag , case when send_flag=1 then custodian1_mail else custodian2_mail end as Cusemail,g.emp_name+'( '+convert(varchar,g.emp_code)+' )' as cus,h.emp_name+'( '+convert(varchar,h.emp_code)+' )' as cus,verified_on  " & _
                            " from ATM_Key_Sending_Details AS Ks left join ATM_Key_Master KM on ks.tm_keyid=KM.TMKEY_id  left join emp_list f on Ks.send_by=f.emp_code left join emp_list g on KM.custodian1= g.emp_code left join  emp_list h on KM.custodian2 = h.emp_code  where Ks.tm_keyid=" & TR_ID.ToString & "").Tables(0)

        Dim dr As DataRow
        Dim n As Integer = 1
        For Each dr In DT.Rows
            Dim TRHead2 As New TableRow
            TRHead2.BackColor = Drawing.Color.WhiteSmoke


            Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08, TRHead2_09, TRHead2_10, TRHead2_11, TRHead2_12, TRHead2_13 As New TableCell
            RH.InsertColumn(TRHead2, TRHead2_00, 5, 2, n)
            RH.InsertColumn(TRHead2, TRHead2_01, 8, 0, IIf(dr(0) = "", "", dr(0)))
            RH.InsertColumn(TRHead2, TRHead2_02, 8, 0, IIf(CStr(dr(1)) = "", "", dr(1)))
            RH.InsertColumn(TRHead2, TRHead2_03, 8, 0, IIf(CStr(dr(2)) = "", "", dr(2)))
            RH.InsertColumn(TRHead2, TRHead2_04, 8, 0, IIf(CStr(dr(3)) = "", "", dr(3)))
           
            If CStr(dr(4)) = 1 Then
                RH.InsertColumn(TRHead2, TRHead2_05, 8, 0, IIf(CStr(dr(4)) = "", "", "Custodian 1"))
                RH.InsertColumn(TRHead2, TRHead2_06, 8, 0, IIf(CStr(dr(6)) = "", "", dr(6)))
            Else
                RH.InsertColumn(TRHead2, TRHead2_05, 8, 0, IIf(CStr(dr(4)) = "", "", "Custodian 2"))
                RH.InsertColumn(TRHead2, TRHead2_06, 8, 0, IIf(CStr(dr(7)) = "", "", dr(7)))
            End If
            RH.InsertColumn(TRHead2, TRHead2_07, 8, 0, IIf(CStr(dr(5)) = "", "", dr(5)))
            RH.InsertColumn(TRHead2, TRHead2_08, 8, 0, IIf(CStr(dr(8)) = "", "", dr(8)))
                tb.Controls.Add(TRHead2)
                n = n + 1
        Next

        pnDisplay.Controls.Add(tb)
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)
        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
