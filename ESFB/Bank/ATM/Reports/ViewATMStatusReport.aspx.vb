﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewATMStatusReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Dim DTHead As New DataTable
        Dim DT As New DataTable

        Dim StatusId As Integer = CInt(GF.Decrypt(Request.QueryString.Get("StatusID")))
        DTHead = DB.ExecuteDataSet("select status_name from [ATM_KEY_status_Master] where status_id=" & StatusId & " ").Tables(0)
        Dim StatusName As String = DTHead.Rows(0)(0).ToString
        'Dim Sitename As String = DTHead.Rows(0)(1).ToString
        'Dim Branch As String = DTHead.Rows(0)(2).ToString



        RH.Heading(CStr(Session("FirmName")), tb, "ATM TMKEY Status" + " - " + StatusName + " Details ", 100)
        'Dim RowBG As Integer = 0
        'tb.Attributes.Add("width", "100%")
        'RH.BlankRow(tb, 6)
        If (StatusId <= 3) Then
            Dim TRHead1 As New TableRow
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10, TRHead1_11, TRHead1_12, TRHead1_13 As New TableCell
            RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead1, TRHead1_01, 8, 0, "ATM ID ")
            RH.InsertColumn(TRHead1, TRHead1_02, 8, 0, "Created On")
            RH.InsertColumn(TRHead1, TRHead1_03, 8, 0, "Reference No")
            RH.InsertColumn(TRHead1, TRHead1_04, 8, 0, "Custodian1")
            RH.InsertColumn(TRHead1, TRHead1_05, 6, 0, "Custodian1 MailID")
            RH.InsertColumn(TRHead1, TRHead1_06, 8, 0, "Custodian1 Contact No")
            RH.InsertColumn(TRHead1, TRHead1_07, 8, 0, "Verified By")
            RH.InsertColumn(TRHead1, TRHead1_08, 6, 0, "Verified On")
            RH.InsertColumn(TRHead1, TRHead1_09, 6, 0, "Reference No")
            RH.InsertColumn(TRHead1, TRHead1_10, 8, 0, "Custodian2")
            RH.InsertColumn(TRHead1, TRHead1_11, 6, 0, "Custodian2 MailID")
            RH.InsertColumn(TRHead1, TRHead1_12, 8, 0, "Custodian1 Contact No")
            'RH.InsertColumn(TRHead1, TRHead1_13, 7, 0, "Status")
            tb.Controls.Add(TRHead1)
        ElseIf (StatusId = 4) Then
            Dim TRHead1 As New TableRow
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10, TRHead1_11, TRHead1_12, TRHead1_13 As New TableCell
            RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead1, TRHead1_01, 8, 0, "ATM ID ")
            RH.InsertColumn(TRHead1, TRHead1_02, 8, 0, "Created On")
            RH.InsertColumn(TRHead1, TRHead1_03, 8, 0, "Reference No")
            RH.InsertColumn(TRHead1, TRHead1_04, 8, 0, "Custodian1")
            RH.InsertColumn(TRHead1, TRHead1_05, 6, 0, "Custodian2")
            RH.InsertColumn(TRHead1, TRHead1_06, 8, 0, "Verified By")
            RH.InsertColumn(TRHead1, TRHead1_07, 8, 0, "Verified On")
            RH.InsertColumn(TRHead1, TRHead1_08, 6, 0, "Techlive Date")
            RH.InsertColumn(TRHead1, TRHead1_09, 6, 0, "Techlive Entered By")
            RH.InsertColumn(TRHead1, TRHead1_10, 8, 0, "Techlive entered on ")
            'RH.InsertColumn(TRHead1, TRHead1_12, 8, 0, "Custodian1 Contact No")
            'RH.InsertColumn(TRHead1, TRHead1_11, 6, 0, "Status")

            'RH.InsertColumn(TRHead1, TRHead1_13, 7, 0, "Status")
            tb.Controls.Add(TRHead1)
        ElseIf (StatusId = 5 Or StatusId = 6) Then
            Dim TRHead1 As New TableRow
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10, TRHead1_11, TRHead1_12, TRHead1_13, TRHead1_14 As New TableCell
            RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead1, TRHead1_01, 8, 0, "ATM ID ")
            RH.InsertColumn(TRHead1, TRHead1_02, 8, 0, "Created On")
            RH.InsertColumn(TRHead1, TRHead1_03, 8, 0, "Reference No")
            RH.InsertColumn(TRHead1, TRHead1_04, 8, 0, "Custodian1")
            RH.InsertColumn(TRHead1, TRHead1_05, 6, 0, "Custodian2")
            RH.InsertColumn(TRHead1, TRHead1_06, 8, 0, "Verified By")
            RH.InsertColumn(TRHead1, TRHead1_07, 8, 0, "Verified On")
            RH.InsertColumn(TRHead1, TRHead1_08, 6, 0, "Techlive Date")
            RH.InsertColumn(TRHead1, TRHead1_09, 6, 0, "Techlive Entered By")
            RH.InsertColumn(TRHead1, TRHead1_10, 8, 0, "Techlive entered on ")
            RH.InsertColumn(TRHead1, TRHead1_11, 8, 0, "Techlive Approved/Rejected by")
            RH.InsertColumn(TRHead1, TRHead1_12, 8, 0, "Techlive Approved/Rejected on")
            RH.InsertColumn(TRHead1, TRHead1_13, 8, 0, "Remarks")
            'RH.InsertColumn(TRHead1, TRHead1_14, 6, 0, "Status")
            tb.Controls.Add(TRHead1)
        ElseIf (StatusId = 7) Then
            Dim TRHead1 As New TableRow
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10, TRHead1_11, TRHead1_12, TRHead1_13, TRHead1_14 As New TableCell
            RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead1, TRHead1_01, 8, 0, "ATM ID ")
            RH.InsertColumn(TRHead1, TRHead1_02, 8, 0, "Created On")
            RH.InsertColumn(TRHead1, TRHead1_03, 8, 0, "Reference No")
            RH.InsertColumn(TRHead1, TRHead1_04, 8, 0, "Custodian1")
            RH.InsertColumn(TRHead1, TRHead1_05, 6, 0, "Custodian2")
            RH.InsertColumn(TRHead1, TRHead1_06, 8, 0, "Verified By")
            RH.InsertColumn(TRHead1, TRHead1_07, 8, 0, "Verified On")
            RH.InsertColumn(TRHead1, TRHead1_08, 6, 0, "Techlive Date")
            RH.InsertColumn(TRHead1, TRHead1_09, 8, 0, "Techlive Approved by")
            RH.InsertColumn(TRHead1, TRHead1_10, 8, 0, "Techlive Approved on")
            RH.InsertColumn(TRHead1, TRHead1_11, 8, 0, "Cashlive date")
            RH.InsertColumn(TRHead1, TRHead1_12, 6, 0, "Cashlive Entered By")
            RH.InsertColumn(TRHead1, TRHead1_13, 8, 0, "Cashlive entered on ")
            'RH.InsertColumn(TRHead1, TRHead1_14, 6, 0, "Status")
            tb.Controls.Add(TRHead1)
        ElseIf (StatusId = 8 Or StatusId = 9) Then
            Dim TRHead1 As New TableRow
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10, TRHead1_11, TRHead1_12, TRHead1_13, TRHead1_14 As New TableCell
            RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead1, TRHead1_01, 8, 0, "ATM ID ")
            RH.InsertColumn(TRHead1, TRHead1_02, 8, 0, "Created On")
            RH.InsertColumn(TRHead1, TRHead1_03, 8, 0, "Reference No")
            RH.InsertColumn(TRHead1, TRHead1_04, 8, 0, "Verified By")
            RH.InsertColumn(TRHead1, TRHead1_05, 8, 0, "Verified On")
            RH.InsertColumn(TRHead1, TRHead1_06, 6, 0, "Techlive Date")
            RH.InsertColumn(TRHead1, TRHead1_07, 8, 0, "Techlive Approved by")
            RH.InsertColumn(TRHead1, TRHead1_08, 8, 0, "Techlive Approved on")
            RH.InsertColumn(TRHead1, TRHead1_09, 8, 0, "Cashlive date")
            RH.InsertColumn(TRHead1, TRHead1_10, 6, 0, "Cashlive Entered By")
            RH.InsertColumn(TRHead1, TRHead1_11, 8, 0, "Cashlive entered on ")
            RH.InsertColumn(TRHead1, TRHead1_12, 8, 0, "Cashlive Approved/Rejected By")
            RH.InsertColumn(TRHead1, TRHead1_13, 6, 0, "Cashlive Approved/Rejected on")
            RH.InsertColumn(TRHead1, TRHead1_14, 6, 0, "Remarks")
            tb.Controls.Add(TRHead1)

        ElseIf (StatusId = 10) Then
            Dim TRHead1 As New TableRow
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10, TRHead1_11, TRHead1_12, TRHead1_13 As New TableCell
            RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead1, TRHead1_01, 8, 0, "ATM ID ")
            RH.InsertColumn(TRHead1, TRHead1_02, 8, 0, "Created On")
            RH.InsertColumn(TRHead1, TRHead1_03, 8, 0, "Reference No")
            RH.InsertColumn(TRHead1, TRHead1_04, 8, 0, "Custodian1")
            RH.InsertColumn(TRHead1, TRHead1_05, 8, 0, "Custodian2")
            RH.InsertColumn(TRHead1, TRHead1_06, 8, 0, "Verified By")
            RH.InsertColumn(TRHead1, TRHead1_07, 8, 0, "Verified On")
            RH.InsertColumn(TRHead1, TRHead1_08, 6, 0, "Techlive Date")
            RH.InsertColumn(TRHead1, TRHead1_09, 8, 0, "Cashlive date")
            RH.InsertColumn(TRHead1, TRHead1_10, 6, 0, "Disposed By")
            RH.InsertColumn(TRHead1, TRHead1_11, 8, 0, "Disposed on")
            RH.InsertColumn(TRHead1, TRHead1_12, 6, 0, "Remarks")
            tb.Controls.Add(TRHead1)

        End If


        DT = DB.ExecuteDataSet("select b.atm_id as col0,a.created_on as col1,a.created_refer_no as col2,f.emp_name+'( '+convert(varchar,f.emp_code)+' )' as custodian1col3,custodian1_mail as col4,custodian1_contactno as col5," & _
                                " isnull(e.emp_name+'( '+convert(varchar,e.emp_code)+' )','') as col6, isnull(a.verified_on,'') as col7 " & _
                                " , isnull(a.verified_refer_no,'')as col8 " & _
                                " , isnull(g.emp_name+'( '+convert(varchar,g.emp_code)+' )','') as custodian2col9, isnull(custodian2_mail,'') as col10, isnull(custodian2_contactno,'') as col11 " & _
                                " , isnull(k.status_name,'') as col12, isnull('Entered Remarks :'+remarks +' <br/>'+'Verified Remarks :'+verified_remarks,'') as col13,b.tr_id as col14 " & _
                                " , isnull(a.techlive_date,'') as techlivedatecol15,isnull(techlive_entered_on,'') as techenteredcol16,l.emp_name+'( '+convert(varchar,l.emp_code)+' )' as techlivebycol17 " & _
                                " , isnull(cashlive_date ,'') as cashlivedatecol18,isnull(cashlive_Entered_on,'') as cashliveoncol19,m.emp_name+'( '+convert(varchar,m.emp_code)+' )' as cashlivebycol20 " & _
                                " , isnull(cashlive_approved_on,'') as cashliveapprovedoncol21,n.emp_name + '( '+convert(varchar,n.emp_code)+' )' as cashliveaprbycol22,techlive_remarks as col23,techlive_approved_remarks as col24" & _
                                " , isnull(techlive_approved_on,'') as col25,cashlive_approved_remarks as col26,isnull(disposed_on,'') as disposedoncol27,isnull(disposed_remarks,'') as disposalremarkscol28,o.emp_name+'( '+convert(varchar,o.emp_code)+' )' as disposedbycol29,p.emp_name+'( '+convert(varchar,p.emp_code)+' )' as tecapprovedbycol30 from ATM_key_Master a left join emp_list d on a.created_by=d.emp_code left join emp_list e on a.verified_by=e.emp_code" & _
                                " left join emp_list f on a.custodian1=f.emp_code left join emp_list g on a.custodian2=g.emp_code left join  emp_list l on a.techlive_entered_By=l.emp_code  " & _
                                " left join emp_list m on a.cashlive_entered_by=m.emp_code left join  emp_list n on a.cashlive_approved_by=n.emp_code left join emp_list o on a.disposed_by= o.emp_code  " & _
                                " left join emp_list p on a.techlive_approved_by=p.emp_code " & _
                                " ,atm_master b,branch_master c,atm_key_status_master k where a.tr_id=b.tr_id and b.branch_id=c.branch_id and a.status_id=k.status_id and a.status_id=" & StatusId.ToString & "").Tables(0)
        Dim dr As DataRow
        Dim n As Integer = 1
        If (StatusId <= 3) Then
            For Each dr In DT.Rows
                Dim TRHead2 As New TableRow
                TRHead2.BackColor = Drawing.Color.WhiteSmoke


                Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08, TRHead2_09, TRHead2_10, TRHead2_11, TRHead2_12, TRHead2_13, TRHead2_14 As New TableCell
                RH.InsertColumn(TRHead2, TRHead2_00, 5, 2, n)
                RH.InsertColumn(TRHead2, TRHead2_01, 8, 0, IIf(dr(0) = "", "", dr(0)))
                RH.InsertColumn(TRHead2, TRHead2_02, 8, 0, IIf(CStr(dr(1)) = "", "", dr(1)))
                RH.InsertColumn(TRHead2, TRHead2_03, 8, 0, IIf(CStr(dr(2)) = "", "", dr(2)))
                RH.InsertColumn(TRHead2, TRHead2_04, 8, 0, IIf(CStr(dr(3)) = "", "", dr(3)))
                RH.InsertColumn(TRHead2, TRHead2_05, 6, 0, IIf(CStr(dr(4)) = "", "", dr(4)))
                RH.InsertColumn(TRHead2, TRHead2_06, 8, 0, IIf(CStr(dr(5)) = "", "", dr(5)))
                RH.InsertColumn(TRHead2, TRHead2_07, 8, 0, IIf(CStr(dr(6)) = "", "", dr(6)))
                RH.InsertColumn(TRHead2, TRHead2_08, 6, 0, IIf(CStr(dr(7)) = "01-01-1900", "", dr(7)))
                RH.InsertColumn(TRHead2, TRHead2_09, 6, 0, IIf(CStr(dr(8)) = "", "", dr(8)))
                RH.InsertColumn(TRHead2, TRHead2_10, 8, 0, IIf(CStr(dr(9)) = "", "", dr(9)))
                RH.InsertColumn(TRHead2, TRHead2_11, 6, 0, IIf(CStr(dr(10)) = "", "", dr(10)))
                RH.InsertColumn(TRHead2, TRHead2_12, 8, 0, IIf(CStr(dr(11)) = "", "", dr(11)))
                tb.Controls.Add(TRHead2)
                n = n + 1
            Next
        ElseIf StatusId = 4 Then
            For Each dr In DT.Rows
                Dim TRHead2 As New TableRow
                TRHead2.BackColor = Drawing.Color.WhiteSmoke

                Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08, TRHead2_09, TRHead2_10, TRHead2_11, TRHead2_12, TRHead2_13, TRHead2_14 As New TableCell
                RH.InsertColumn(TRHead2, TRHead2_00, 5, 2, n)
                RH.InsertColumn(TRHead2, TRHead2_01, 8, 0, IIf(dr(0) = "", "", dr(0)))
                RH.InsertColumn(TRHead2, TRHead2_02, 8, 0, IIf(CStr(dr(1)) = "", "", dr(1)))
                RH.InsertColumn(TRHead2, TRHead2_03, 8, 0, IIf(CStr(dr(2)) = "", "", dr(2)))
                RH.InsertColumn(TRHead2, TRHead2_04, 8, 0, IIf(CStr(dr(3)) = "", "", dr(3)))
                RH.InsertColumn(TRHead2, TRHead2_05, 6, 0, IIf(CStr(dr(9)) = "", "", dr(9)))
                RH.InsertColumn(TRHead2, TRHead2_06, 8, 0, IIf(CStr(dr(6)) = "", "", dr(6)))
                RH.InsertColumn(TRHead2, TRHead2_07, 8, 0, IIf(CStr(dr(7)) = "", "", dr(7)))
                RH.InsertColumn(TRHead2, TRHead2_08, 6, 0, IIf(CStr(dr(15)) = "", "", dr(15)))
                RH.InsertColumn(TRHead2, TRHead2_09, 6, 0, IIf(CStr(dr(17)) = "", "", dr(17)))
                RH.InsertColumn(TRHead2, TRHead2_10, 8, 0, IIf(CStr(dr(16)) = "", "", dr(16)))

                tb.Controls.Add(TRHead2)
                n = n + 1
            Next
        ElseIf (StatusId = 5 Or StatusId = 6) Then
            For Each dr In DT.Rows
                Dim TRHead2 As New TableRow

                Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08, TRHead2_09, TRHead2_10, TRHead2_11, TRHead2_12, TRHead2_13, TRHead2_14 As New TableCell
                RH.InsertColumn(TRHead2, TRHead2_00, 5, 2, n)
                RH.InsertColumn(TRHead2, TRHead2_01, 8, 0, IIf(dr(0) = "", "", dr(0)))
                RH.InsertColumn(TRHead2, TRHead2_02, 8, 0, IIf(CStr(dr(1)) = "", "", dr(1)))
                RH.InsertColumn(TRHead2, TRHead2_03, 8, 0, IIf(CStr(dr(2)) = "", "", dr(2)))
                RH.InsertColumn(TRHead2, TRHead2_04, 8, 0, IIf(CStr(dr(3)) = "", "", dr(3)))
                RH.InsertColumn(TRHead2, TRHead2_05, 6, 0, IIf(CStr(dr(9)) = "", "", dr(9)))
                RH.InsertColumn(TRHead2, TRHead2_06, 8, 0, IIf(CStr(dr(6)) = "", "", dr(6)))
                RH.InsertColumn(TRHead2, TRHead2_07, 8, 0, IIf(CStr(dr(7)) = "", "", dr(7)))
                RH.InsertColumn(TRHead2, TRHead2_08, 6, 0, IIf(CStr(dr(15)) = "", "", dr(15)))
                RH.InsertColumn(TRHead2, TRHead2_09, 6, 0, IIf(CStr(dr(17)) = "", "", dr(17)))
                RH.InsertColumn(TRHead2, TRHead2_10, 8, 0, IIf(CStr(dr(16)) = "", "", dr(16)))

                RH.InsertColumn(TRHead2, TRHead2_11, 8, 0, IIf(CStr(dr(30)) = "", "", dr(30)))
                RH.InsertColumn(TRHead2, TRHead2_12, 8, 0, IIf(CStr(dr(25)) = "", "", dr(25)))
                RH.InsertColumn(TRHead2, TRHead2_13, 8, 0, IIf(CStr(dr(24)) = "", "", dr(24)))
                tb.Controls.Add(TRHead2)
                n = n + 1
            Next
        ElseIf (StatusId = 7) Then
            For Each dr In DT.Rows
                Dim TRHead2 As New TableRow
                TRHead2.BackColor = Drawing.Color.WhiteSmoke

                Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08, TRHead2_09, TRHead2_10, TRHead2_11, TRHead2_12, TRHead2_13, TRHead2_14 As New TableCell
                RH.InsertColumn(TRHead2, TRHead2_00, 5, 2, n)
                RH.InsertColumn(TRHead2, TRHead2_01, 8, 0, IIf(dr(0) = "", "", dr(0)))
                RH.InsertColumn(TRHead2, TRHead2_02, 8, 0, IIf(CStr(dr(1)) = "", "", dr(1)))
                RH.InsertColumn(TRHead2, TRHead2_03, 8, 0, IIf(CStr(dr(2)) = "", "", dr(2)))
                RH.InsertColumn(TRHead2, TRHead2_04, 8, 0, IIf(CStr(dr(3)) = "", "", dr(3)))
                RH.InsertColumn(TRHead2, TRHead2_05, 6, 0, IIf(CStr(dr(4)) = "", "", dr(9)))
                RH.InsertColumn(TRHead2, TRHead2_06, 8, 0, IIf(CStr(dr(6)) = "", "", dr(6)))
                RH.InsertColumn(TRHead2, TRHead2_07, 8, 0, IIf(CStr(dr(7)) = "", "", dr(7)))
                RH.InsertColumn(TRHead2, TRHead2_08, 6, 0, IIf(CStr(dr(15)) = "", "", dr(15)))
                RH.InsertColumn(TRHead2, TRHead2_09, 8, 0, IIf(CStr(dr(30)) = "", "", dr(30)))
                RH.InsertColumn(TRHead2, TRHead2_10, 8, 0, IIf(CStr(dr(25)) = "", "", dr(25)))
                RH.InsertColumn(TRHead2, TRHead2_11, 8, 0, IIf(CStr(dr(18)) = "", "", dr(18)))
                RH.InsertColumn(TRHead2, TRHead2_12, 6, 0, IIf(CStr(dr(20)) = "", "", dr(20)))
                RH.InsertColumn(TRHead2, TRHead2_13, 8, 0, IIf(CStr(dr(19)) = "", "", dr(19)))
                tb.Controls.Add(TRHead2)
                n = n + 1
            Next
        ElseIf (StatusId = 8 Or StatusId = 9) Then
            For Each dr In DT.Rows
                Dim TRHead2 As New TableRow
                TRHead2.BackColor = Drawing.Color.WhiteSmoke

                Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08, TRHead2_09, TRHead2_10, TRHead2_11, TRHead2_12, TRHead2_13, TRHead2_14, TRHead2_15, TRHead2_16 As New TableCell
                RH.InsertColumn(TRHead2, TRHead2_00, 5, 2, n)
                RH.InsertColumn(TRHead2, TRHead2_01, 8, 0, IIf(dr(0) = "", "", dr(0)))
                RH.InsertColumn(TRHead2, TRHead2_02, 8, 0, IIf(CStr(dr(1)) = "", "", dr(1)))
                RH.InsertColumn(TRHead2, TRHead2_03, 8, 0, IIf(CStr(dr(2)) = "", "", dr(2)))
                RH.InsertColumn(TRHead2, TRHead2_04, 8, 0, IIf(CStr(dr(6)) = "", "", dr(6)))
                RH.InsertColumn(TRHead2, TRHead2_05, 8, 0, IIf(CStr(dr(7)) = "", "", dr(7)))
                RH.InsertColumn(TRHead2, TRHead2_06, 6, 0, IIf(CStr(dr(15)) = "", "", dr(15)))
                RH.InsertColumn(TRHead2, TRHead2_07, 8, 0, IIf(CStr(dr(30)) = "", "", dr(30)))
                RH.InsertColumn(TRHead2, TRHead2_08, 8, 0, IIf(CStr(dr(25)) = "", "", dr(25)))
                RH.InsertColumn(TRHead2, TRHead2_09, 8, 0, IIf(CStr(dr(18)) = "", "", dr(18)))
                RH.InsertColumn(TRHead2, TRHead2_10, 6, 0, IIf(CStr(dr(20)) = "", "", dr(20)))
                RH.InsertColumn(TRHead2, TRHead2_11, 8, 0, IIf(CStr(dr(19)) = "", "", dr(19)))
                RH.InsertColumn(TRHead2, TRHead2_12, 8, 0, IIf(CStr(dr(22)) = "", "", dr(22)))
                RH.InsertColumn(TRHead2, TRHead2_13, 6, 0, IIf(CStr(dr(21)) = "", "", dr(21)))
                RH.InsertColumn(TRHead2, TRHead2_14, 6, 0, IIf(CStr(dr(26)) = "", "", dr(26)))

                tb.Controls.Add(TRHead2)
                n = n + 1
            Next
        ElseIf (StatusId = 10) Then
            For Each dr In DT.Rows
                Dim TRHead2 As New TableRow
                TRHead2.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08, TRHead2_09, TRHead2_10, TRHead2_11, TRHead2_12, TRHead2_13, TRHead2_14 As New TableCell
                RH.InsertColumn(TRHead2, TRHead2_00, 5, 2, n)
                RH.InsertColumn(TRHead2, TRHead2_01, 8, 0, IIf(dr(0) = "", "", dr(0)))
                RH.InsertColumn(TRHead2, TRHead2_02, 8, 0, IIf(CStr(dr(1)) = "", "", dr(1)))
                RH.InsertColumn(TRHead2, TRHead2_03, 8, 0, IIf(CStr(dr(2)) = "", "", dr(2)))
                RH.InsertColumn(TRHead2, TRHead2_04, 8, 0, IIf(CStr(dr(3)) = "", "", dr(3)))
                RH.InsertColumn(TRHead2, TRHead2_05, 6, 0, IIf(CStr(dr(4)) = "", "", dr(4)))
                RH.InsertColumn(TRHead2, TRHead2_06, 8, 0, IIf(CStr(dr(9)) = "", "", dr(9)))
                RH.InsertColumn(TRHead2, TRHead2_07, 8, 0, IIf(CStr(dr(6)) = "", "", dr(6)))
                RH.InsertColumn(TRHead2, TRHead2_08, 8, 0, IIf(CStr(dr(7)) = "", "", dr(7)))
                RH.InsertColumn(TRHead2, TRHead2_09, 6, 0, IIf(CStr(dr(15)) = "", "", dr(15)))
                RH.InsertColumn(TRHead2, TRHead2_10, 8, 0, IIf(CStr(dr(18)) = "", "", dr(18)))
                RH.InsertColumn(TRHead2, TRHead2_11, 6, 0, IIf(CStr(dr(29)) = "", "", dr(29)))
                RH.InsertColumn(TRHead2, TRHead2_12, 8, 0, IIf(CStr(dr(27)) = "", "", dr(27)))
                RH.InsertColumn(TRHead2, TRHead2_13, 6, 0, IIf(CStr(dr(28)) = "", "", dr(28)))
                tb.Controls.Add(TRHead2)
                n = n + 1
            Next
        End If
        pnDisplay.Controls.Add(tb)
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)
        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
