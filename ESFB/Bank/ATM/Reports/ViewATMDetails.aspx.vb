﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewATMDetails
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim WebTools As New WebApp.Tools
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Dim DTHead As New DataTable



        RH.Heading(CStr(Session("FirmName")), tb, "ATM Details", 100)
        Dim RowBG As Integer = 0
        tb.Attributes.Add("width", "100%")
        RH.BlankRow(tb, 6)

        Dim TRHead1 As New TableRow
        TRHead1.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06 As New TableCell
        RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "Sl No.")
        RH.InsertColumn(TRHead1, TRHead1_01, 15, 0, "ATM_ID")
        RH.InsertColumn(TRHead1, TRHead1_02, 15, 0, "Delivered On")
        RH.InsertColumn(TRHead1, TRHead1_03, 15, 0, "Branch")
        RH.InsertColumn(TRHead1, TRHead1_04, 10, 0, "BranchCode")
        RH.InsertColumn(TRHead1, TRHead1_05, 25, 0, "Site")
        RH.InsertColumn(TRHead1, TRHead1_06, 15, 0, "TMKey Status")

        tb.Controls.Add(TRHead1)

        DT = DB.ExecuteDataSet("select a.atm_id as ATM_ID,isnull(delivery_date,'') as DELIVERY_DT,branch_name as BRANCH_NAME,a.branch_id as BRANCH_ID,site_name as SITE,case when TM_KEY_flg=1 then 'TMKEY generated' else '' end AS TM_KEY_STATUS  from [atm_master] a,branch_master b where a.branch_id=b.branch_id ").Tables(0)
        Dim dr As DataRow
        Dim n As Integer = 1
        For Each dr In DT.Rows
            Dim TRHead2 As New TableRow
            TRHead2.BackColor = Drawing.Color.WhiteSmoke


            Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08, TRHead2_09, TRHead2_10, TRHead2_11, TRHead2_12, TRHead2_13, TRHead2_14, TRHead2_15, TRHead2_16, TRHead2_17 As New TableCell
            RH.InsertColumn(TRHead2, TRHead2_00, 5, 2, n)
            RH.InsertColumn(TRHead2, TRHead2_01, 15, 0, IIf(dr(0) = "", "", dr(0)))
            RH.InsertColumn(TRHead2, TRHead2_02, 15, 0, IIf(CStr(dr(1)) = "01-01-1900", "", dr(1)))
            RH.InsertColumn(TRHead2, TRHead2_03, 15, 0, IIf(CStr(dr(2)) = "", "", dr(2)))
            RH.InsertColumn(TRHead2, TRHead2_04, 10, 0, IIf(CStr(dr(3)) = "", "", dr(3)))
            RH.InsertColumn(TRHead2, TRHead2_05, 25, 0, IIf(CStr(dr(4)) = "", "", dr(4)))
            RH.InsertColumn(TRHead2, TRHead2_06, 15, 0, IIf(CStr(dr(5)) = "", "", dr(5)))
           
            tb.Controls.Add(TRHead2)
            n = n + 1
        Next
        RH.BlankRow(tb, 9)
        pnDisplay.Controls.Add(tb)
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        WebTools.ExporttoExcel(DT, "ATM_History")
    End Sub

End Class
