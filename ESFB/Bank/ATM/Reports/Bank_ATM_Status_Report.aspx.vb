﻿Imports System.Data
Partial Class Bank_ATM_Status_Report
    Inherits System.Web.UI.Page
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1394) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "ATM STATUS DETAILS REPORT"

            GN.ComboFill(cmbBranch, GetBranch(CInt(Session("StatusId"))), 0, 1)

            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "DisplaySettings();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetBranch(id As Integer) As DataTable
        Dim strval As String
        If id > 1000 Then
            Return DB.ExecuteDataSet("select status_id as statusid,upper(status_name) as status from ATM_KEY_status_Master order by status_id").Tables(0)
        Else
            Return DB.ExecuteDataSet("select '-1' as statusid,' ------SELECT------' as status Union All select status_id as statusid,upper(status_name) as status from ATM_KEY_status_Master where status_id > 0  order by statusid").Tables(0)
        End If

    End Function
End Class
