﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewATMHistory
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Dim DT_main As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Dim DTHead As New DataTable

        Dim DT As New DataTable
        Dim TR_ID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("TR_ID")))

      



        DTHead = DB.ExecuteDataSet("select atm_id,site_name,branch_name from [atm_master] a,branch_master b where a.branch_id=b.branch_id and a.tr_id=" & TR_ID & " ").Tables(0)
        Dim ATM_ID As String = DTHead.Rows(0)(0).ToString
        Dim Sitename As String = DTHead.Rows(0)(1).ToString
        Dim Branch As String = DTHead.Rows(0)(2).ToString



        RH.Heading(CStr(Session("FirmName")), tb, "ATM TMKEY History" + " - " + ATM_ID + "               (Branch:-" + Branch + "-" + Sitename + ")", 150)
        Dim RowBG As Integer = 0
        tb.Attributes.Add("width", "100%")
        RH.BlankRow(tb, 6)

        Dim TRHead1 As New TableRow
        TRHead1.BackColor = Drawing.Color.WhiteSmoke
        Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10, TRHead1_11, TRHead1_12, TRHead1_13, TRHead1_14, TRHead1_15, TRHead1_16, TRHead1_17, TRHead1_18, TRHead1_19, TRHead1_20, TRHead1_21 As New TableCell
        RH.InsertColumn(TRHead1, TRHead1_00, 2, 2, "Sl No.")
        RH.InsertColumn(TRHead1, TRHead1_01, 6, 0, "Created By ")
        RH.InsertColumn(TRHead1, TRHead1_02, 4, 0, "Created On")
        RH.InsertColumn(TRHead1, TRHead1_03, 4, 0, "Reference No1")
        RH.InsertColumn(TRHead1, TRHead1_04, 6, 0, "Custodian1")
        RH.InsertColumn(TRHead1, TRHead1_05, 6, 0, "Custodian1 MailID")
        RH.InsertColumn(TRHead1, TRHead1_06, 4, 0, "Custodian1 Contact No")
        RH.InsertColumn(TRHead1, TRHead1_07, 4, 0, "Verified By")
        RH.InsertColumn(TRHead1, TRHead1_08, 4, 0, "Verified On")
        RH.InsertColumn(TRHead1, TRHead1_09, 4, 0, "Reference No2")
        RH.InsertColumn(TRHead1, TRHead1_10, 6, 0, "Custodian2")
        RH.InsertColumn(TRHead1, TRHead1_11, 6, 0, "Custodian2 MailID")
        RH.InsertColumn(TRHead1, TRHead1_12, 4, 0, "Custodian2 Contact No")
        RH.InsertColumn(TRHead1, TRHead1_13, 4, 0, "Tech Live Date ")
        RH.InsertColumn(TRHead1, TRHead1_14, 4, 0, "Tech Live By")
        RH.InsertColumn(TRHead1, TRHead1_15, 4, 0, "Tech Live Approved  Date ")
        RH.InsertColumn(TRHead1, TRHead1_16, 4, 0, "Tech Live Approved By")
        RH.InsertColumn(TRHead1, TRHead1_17, 4, 0, "Cash Live Date ")
        RH.InsertColumn(TRHead1, TRHead1_18, 4, 0, "Cash Live By")
        RH.InsertColumn(TRHead1, TRHead1_19, 4, 0, "Cash Live Approved Date ")
        RH.InsertColumn(TRHead1, TRHead1_20, 4, 0, "Cash Live Approved By")
        RH.InsertColumn(TRHead1, TRHead1_21, 8, 0, "Status")

        tb.Controls.Add(TRHead1)

        DT_main = DB.ExecuteDataSet("select d.emp_name+'( '+convert(varchar,d.emp_code)+' )' as Created_By,a.created_on as CREATED_ON,a.created_refer_no as REFER_NO1, " & _
                     " f.emp_name+'( '+convert(varchar,f.emp_code)+' )' as custodian1,custodian1_mail,custodian1_contactno, isnull(e.emp_name+'( '+convert(varchar,e.emp_code)+' )','') as verified_by  " & _
                     " , isnull(a.verified_on,'') as verified_on , isnull(a.verified_refer_no,'')  as REFER_NO2, " & _
                     " isnull(g.emp_name+'( '+convert(varchar,g.emp_code)+' )','') as custodian2, isnull(custodian2_mail,'') as custodian2_mail, isnull(custodian2_contactno,'') as custodian2_contactno, " & _
                     " case when convert(varchar,isnull(a.techlive_date,''),104)='01.01.1900'then '' else convert(varchar,isnull(a.techlive_date,''),104) end as techlive_date " & _
                     " ,isnull(convert(varchar,isnull(a.techlive_entered_by,'')),'') as techlive_entered_by, " & _
                     " case when convert(varchar,isnull(a.techlive_approved_on,''),104)='01.01.1900'then '' else convert(varchar,isnull(a.techlive_approved_on,''),104) end as techlive_approved_on " & _
                     " ,isnull(convert(varchar,isnull(a.techlive_approved_by,'')),''),  " & _
                     " case when convert(varchar,isnull(a.Cashlive_date,''),104)='01.01.1900'then '' else convert(varchar,isnull(a.Cashlive_date,''),104) end as Cashlive_date " & _
                     " ,isnull(convert(varchar,a.Cashlive_entered_by),'') as Cashlive_entered_by, " & _
                     " case when convert(varchar,isnull(a.Cashlive_approved_on,''),104)='01.01.1900'then '' else convert(varchar,isnull(a.Cashlive_approved_on,''),104) end as Cashlive_approved_on " & _
                     " ,isnull(convert(varchar,isnull(a.Cashlive_approved_by,'')),'') as Cashlive_approved_by " & _
                     " , isnull(k.status_name,'') as STATUS,isnull('Entered Remarks :'+remarks +''+'Verified Remarks :'+verified_remarks,''),b.tr_id from ATM_key_Master a left join emp_list d on a.created_by=d.emp_code left join emp_list e on a.verified_by=e.emp_code left join emp_list f on a.custodian1=f.emp_code   " & _
                     " left join emp_list g on a.custodian2=g.emp_code   ,atm_master b,branch_master c,atm_key_status_master k where a.tr_id=b.tr_id and b.branch_id=c.branch_id and " & _
                     " a.status_id=k.status_id and a.tr_id=" & TR_ID.ToString & " order by a.status_id ").Tables(0)
        Dim dr As DataRow
        Dim n As Integer = 1
        For Each dr In DT_main.Rows
            Dim TRHead2 As New TableRow
            TRHead2.BackColor = Drawing.Color.WhiteSmoke


            Dim TRHead2_00, TRHead2_01, TRHead2_02, TRHead2_03, TRHead2_04, TRHead2_05, TRHead2_06, TRHead2_07, TRHead2_08, TRHead2_09, TRHead2_10, TRHead2_11, TRHead2_12, TRHead2_13, TRHead2_14, TRHead2_15, TRHead2_16, TRHead2_17, TRHead2_18, TRHead2_19, TRHead2_20, TRHead2_21 As New TableCell
            RH.InsertColumn(TRHead2, TRHead2_00, 2, 2, n)
            RH.InsertColumn(TRHead2, TRHead2_01, 6, 0, IIf(CStr(dr(0)) = "", "", dr(0)))
            RH.InsertColumn(TRHead2, TRHead2_02, 4, 0, IIf(CStr(dr(1)) = "", "", dr(1)))
            RH.InsertColumn(TRHead2, TRHead2_03, 4, 0, IIf(CStr(dr(2)) = "", "", dr(2)))
            RH.InsertColumn(TRHead2, TRHead2_04, 6, 0, IIf(CStr(dr(3)) = "", "", dr(3)))
            RH.InsertColumn(TRHead2, TRHead2_05, 6, 0, IIf(CStr(dr(4)) = "", "", dr(4)))
            RH.InsertColumn(TRHead2, TRHead2_06, 4, 0, IIf(CStr(dr(5)) = "", "", dr(5)))
            RH.InsertColumn(TRHead2, TRHead2_07, 4, 0, IIf(CStr(dr(6)) = "", "", dr(6)))
            RH.InsertColumn(TRHead2, TRHead2_08, 4, 0, IIf(CStr(dr(7)) = "", "", dr(7)))
            RH.InsertColumn(TRHead2, TRHead2_09, 4, 0, IIf(CStr(dr(8)) = "", "", dr(8)))
            RH.InsertColumn(TRHead2, TRHead2_10, 6, 0, IIf(CStr(dr(9)) = "", "", dr(9)))
            RH.InsertColumn(TRHead2, TRHead2_11, 6, 0, IIf(CStr(dr(10)) = "", "", dr(10)))
            RH.InsertColumn(TRHead2, TRHead2_12, 4, 0, IIf(CStr(dr(11)) = "", "", dr(11)))
            RH.InsertColumn(TRHead2, TRHead2_13, 4, 0, IIf(CStr(dr(12)) = "", "", dr(12)))
            RH.InsertColumn(TRHead2, TRHead2_14, 4, 0, IIf(CStr(dr(13)) = "", "", dr(13)))
            RH.InsertColumn(TRHead2, TRHead2_15, 4, 0, IIf(CStr(dr(14)) = "", "", dr(14)))
            RH.InsertColumn(TRHead2, TRHead2_16, 4, 0, IIf(CStr(dr(15)) = "", "", dr(15)))
            RH.InsertColumn(TRHead2, TRHead2_17, 4, 0, IIf(CStr(dr(16)) = "", "", dr(16)))
            RH.InsertColumn(TRHead2, TRHead2_18, 4, 0, IIf(CStr(dr(17)) = "", "", dr(17)))
            RH.InsertColumn(TRHead2, TRHead2_19, 4, 0, IIf(CStr(dr(18)) = "", "", dr(18)))
            RH.InsertColumn(TRHead2, TRHead2_20, 4, 0, IIf(CStr(dr(19)) = "", "", dr(19)))
            RH.InsertColumn(TRHead2, TRHead2_21, 8, 0, IIf(CStr(dr(20)) = "", "", dr(20)))
            tb.Controls.Add(TRHead2)
            n = n + 1
        Next
        RH.BlankRow(tb, 10)
        pnDisplay.Controls.Add(tb)
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        WebTools.ExporttoExcel(DT_main, "ATM_History")
    End Sub

End Class
