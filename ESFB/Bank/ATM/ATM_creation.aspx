﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="ATM_creation.aspx.vb" Inherits="ATM_creation" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %> 
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
        
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

    function RequestOnClick() {   
       
     if (document.getElementById("<%= cmbBranch.ClientID%>").value == "-1") 
        {
            alert("Select Branch");
            document.getElementById("<%= cmbBranch.ClientID%>").focus();
            return false;
        }
        
               
            var atm	= document.getElementById("<%= txtAtm.ClientID%>").value;
            var Effdate	= document.getElementById("<%= txteffdate.ClientID%>").value;
            var site	= document.getElementById("<%= txtSite.ClientID%>").value;
            var Branch	= document.getElementById("<%= cmbBranch.ClientID%>").value;
    

            document.getElementById("<%= hdnValue.ClientID %>").value = "1Ø" + atm + "Ø" + site + "Ø" + Effdate + "Ø" + Branch;       
}
            function window_onload(){
                
            }
           function viewSummary()
           {
         
            window.open("Reports/ViewATMDetails.aspx"); 
            
            return false;
            }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return window_onload()
            // ]]>
        </script>
    </head>
    </html>
   
    <br />
    <br />
       <table class="style1" style="width: 80%; margin: 0px auto;">
       <tr id="model">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                ATM ID
                     <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtAtm" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15" onkeypress='return AlphaNumericCheck(event)' />
                <img id='ViewAsset' src='../../Image/eswtClose2.png' onclick='viewSummary()' title='View ATM List'  style='height:20px; width:20px;  cursor:pointer;' /></td>
        </tr>
         <tr id="Branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>   
       
        
        <tr id="serialno">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="width: 12%; text-align: left;">
                Site Name</td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtSite" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="15"  />
            </td>
        </tr>
        
         
        <tr id="Tr5">
            <td style="width: 25%;">
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
            <td style="width: 12%; align: left;">
                Delivery Date

            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:TextBox ID="txteffdate" class="NormalText" runat="server" Width="40%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CEFromDate" runat="server" TargetControlID="txteffdate" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
        </tr>
       
       
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
