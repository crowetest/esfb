﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports System.Configuration
Imports System
Imports System.Text

Partial Class BrNetPriliminaryCheck
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim WebTools As New WebApp.Tools
    Dim values As New DataTable
    Dim RequestID, UserID As Integer

#Region "Page Load & Dispose"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If GF.FormAccess(CInt(Session("UserID")), 1251) = False Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If

        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        If Not IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.cmbBatch.Attributes.Add("onchange", "return BatchOnChange()")
        End If

    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
    Sub DeleteFilesFromFolder(ByVal Folder As String)
        If Directory.Exists(Folder) Then
            For Each _file As String In Directory.GetFiles(Folder)
                File.Delete(_file)
            Next
            For Each _folder As String In Directory.GetDirectories(Folder)

                DeleteFilesFromFolder(_folder)
            Next

        End If

    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        UserID = CInt(Session("UserID"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1 'Fill Vet Request For Approve              
                DT = GF.GetQueryResult("select -1,'---Select---' union all select batch_id,batch_name from ESFB.dbo.APP_BRNET_REQUEST_BATCH where status_id=0")
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2
                Dim BatchID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("SELECT Stuff((select upper(SUBSTRING(b.account_title, 1, 3))+convert(varchar,b.emp_code) + ', ' AS 'data()' from ESFB.dbo.app_request_master a, ESFB.dbo.APP_BRNET_REQUEST_BATCH_DTL b where a.app_id=10 and a.app_type_id=50 and b.batch_id=" & BatchID & " and a.app_request_id=b.app_request_id FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,0,N'')")
                If Not DT Is Nothing Then
                    If DT.Rows.Count > 0 Then
                        CallBackReturn += DT.Rows(0)(0).ToString + "Ĉ"
                    Else
                        CallBackReturn += "Ĉ"
                    End If
                End If
                

                DT = GF.GetQueryResult("SELECT Stuff((select upper(SUBSTRING(b.account_title, 1, 3))+convert(varchar,b.emp_code) + ', ' AS 'data()' from ESFB.dbo.app_request_master a, ESFB.dbo.APP_BRNET_REQUEST_BATCH_DTL b where a.app_id=10 and a.app_type_id=51 and b.batch_id=" & BatchID & " and a.app_request_id=b.app_request_id FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,0,N'')")
                If Not DT Is Nothing Then
                    If DT.Rows.Count > 0 Then
                        CallBackReturn += DT.Rows(0)(0).ToString + "Ĉ"
                    Else
                        CallBackReturn += "Ĉ"
                    End If
                End If
                
                DT = GF.GetQueryResult("SELECT Stuff((select upper(SUBSTRING(b.account_title, 1, 3))+convert(varchar,b.emp_code) + ', ' AS 'data()' from ESFB.dbo.app_request_master a, ESFB.dbo.APP_BRNET_REQUEST_BATCH_DTL b where a.app_id=10 and a.app_type_id=52 and b.batch_id=" & BatchID & " and a.app_request_id=b.app_request_id FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,0,N'')")
                If Not DT Is Nothing Then
                    If DT.Rows.Count > 0 Then
                        CallBackReturn += DT.Rows(0)(0).ToString + "Ĉ"
                    Else
                        CallBackReturn += "Ĉ"
                    End If
                End If

        End Select
    End Sub
#End Region


End Class
