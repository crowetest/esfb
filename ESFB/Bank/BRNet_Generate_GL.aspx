﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="BRNet_Generate_GL.aspx.vb" Inherits="BRNet_Generate_GL" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style2
        {
            width: 11%;
        }
        
        #Button
        {
             width:100%;
            height:40px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
            color:#E0E0E0;
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
            color:#036;
        } 
    </style>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

        function window_onload() {
            document.getElementById("rowplnShow").style.display = "none";
            ToServer("1ʘ", 1);
        }

        function FromServer(Arg, Context) 
        {
            switch(Context)
            {
                case 1: //  Employee Fill On Change Of Department
                {                     
                    if(Arg != "")
                    {
                        ComboFill(Arg, "<%= cmbBatch.ClientID %>");
                    }
                    break;
                }
                case 2: //  Employee Fill On Change Of Department
                {   
                    var Data = Arg;                  
                    if(Data != "")
                    {
                        document.getElementById("<%= hdnExistingDtl.ClientID %>").value = Data;
                        document.getElementById("<%= hdnBatch.ClientID %>").value = document.getElementById("<%= cmbBatch.ClientID %>").value;
                        DiplayTable();
                    }
                    break;
                }
                case 3:
                {
                    var Data = Arg.split("ʘ");
                    alert(Data[1]);
                    var BatchNo = document.getElementById("<%= cmbBatch.ClientID %>").value;
                    ToServer("2ʘ" + BatchNo, 2);
                    break;
                }
                case 4:
                {
                    var Data = Arg.split("ʘ");
                    if(Data[0] == 0)
                    {
                        var BatchNo = document.getElementById("<%= cmbBatch.ClientID %>").value;
                        ToServer("2ʘ" + BatchNo, 2);
                    }
                    else if(Data[0] == 2)
                    {
                        alert(Data[1]);
                        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
                    }
                    else{
                        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
                    }
                    break;
                }
                case 5:
                {
                    var Data = Arg.split("ʘ");
                    alert(Data[1]);                    
                    break;
                }
            }
        }

        function BatchOnChange()
        {
            var BatchNo = document.getElementById("<%= cmbBatch.ClientID %>").value;
            ToServer("2ʘ" + BatchNo, 2);
        }

        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ř");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("Ĉ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

        function DiplayTable() 
        {            
            var Tab = "";
            Tab += "<div style='width:60%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
            Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            Tab += "<tr class='sub_second';>";
            Tab += "<td style='width:60%;text-align:center'>BATCH DETAILS</td></tr>";
            Tab += "</table></div>"; 
            Tab += "<div style='width:60%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            Tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            Tab += "<tr style='background-color:#efe1ef;'>";
            Tab += "<td style='width:5%;text-align:center'>SlNO</td>";
            Tab += "<td style='width:25%;text-align:center'>GL Number</td>";
            Tab += "<td style='width:35%;text-align:left'>Name Of the Employee</td>";
            Tab += "<td style='width:20%;text-align:center'>Officer ID</td>";
            Tab += "<td style='width:15%;text-align:center'>Confirmation</td></tr>";
            
            if (document.getElementById("<%= hdnExistingDtl.ClientID %>").value != "") {
                document.getElementById("rowplnShow").style.display = "";
                var HidArg = document.getElementById("<%= hdnExistingDtl.ClientID %>").value.split("®");
                var RowCount = HidArg.length - 1;
                var j;
                var Total = 0;
                var row_bg1 = 0;
                var SlNo = 0;
                for (j = 0; j < RowCount; j++) {
                    if (row_bg1 == 0) {
                        row_bg1 = 1;
                        Tab += "<tr style='background-color:#FBEFFB;'>";
                    }
                    else {
                        row_bg1 = 0;
                        Tab += "<tr style='background-color:#FFF;'>";
                    }
                    var HidArg1 = HidArg[j].split("Æ");
                    //Sl No, CIF ID, EMP CODE, TITLE
                    SlNo = SlNo + 1;
                    Tab += "<td style='width:5%;text-align:center'>" + SlNo + "</td>";
                    Tab += "<td style='width:25%;text-align:center'>" + HidArg1[0] + "</td>";
                    Tab += "<td style='width:35%;text-align:left'>" + HidArg1[1] + "</td>";
                    Tab += "<td style='width:20%;text-align:center'>" + HidArg1[2] + "</td>";
                    var LinkText = "OK";
                    Tab += "<td style='width:15%; text-align:center;  padding-left:5px; ' onclick=btnOk_onclick('" + HidArg1[0] + "','" + HidArg1[3] + "')><div id='Button' >" + LinkText + "</div></td></tr>";                  
                }
                if (row_bg1 == 0)
                    Tab += "<tr style='background-color:OldLace'></tr>";
                else
                    Tab += "<tr style='background-color:Wheat'></tr>";
                Tab += "</table></div></div>";
                document.getElementById("<%= pnlExistingData.ClientID %>").innerHTML = Tab;
            }
        }

        function btnGenerate_onclick()
        {
            var BatchNo = document.getElementById("<%= cmbBatch.ClientID %>").value;
            ToServer("3ʘ"+ BatchNo, 3);
        }

        function btnOk_onclick(glNo,dtlID)
        {
            var BatchNo = document.getElementById("<%= cmbBatch.ClientID %>").value;
            ToServer("4ʘ" + glNo + "ʘ" + dtlID + "ʘ" + BatchNo, 4);
        }

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        function btnUpdate_onclick() {
            if (confirm("Are you sure to Update this GLNo as Last Created GL Number") == 1) {
                var GlNo = document.getElementById("<%= txtGlno.ClientID %>").value;
                ToServer("5ʘ" + GlNo, 5);
            }
        }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
    // <![CDATA[
    return window_onload()
    // ]]>
        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 100%; margin: 0px auto;">
        <tr id="Tr3">
            <td style="width: 25%;">
            </td>
            <td style="text-align: left;" class="style2">
                Last Updated GL</td>
            <td>
                &nbsp; &nbsp;
                <asp:TextBox ID="txtGlno" runat="server" Width="317px"></asp:TextBox>
            &nbsp;<input id="btnUpdate" 
                    style="font-family: cambria; cursor: pointer; width: 10%" type="button" 
                    value="UPDATE" onclick="return btnUpdate_onclick()" /></td>
        </tr>
        <tr id="Tr3">
            <td style="width: 25%;">
                &nbsp;</td>
            <td style="text-align: left;" class="style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr id="Tr1">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="text-align: left;" class="style2">
                Select Batch
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbBatch" runat="server" Height="16px" Width="317px">
                </asp:DropDownList>
            &nbsp;<input id="btnGenerate" onclick="return btnGenerate_onclick()" 
                    style="font-family: cambria; cursor: pointer; width: 10%" type="button" 
                    value="GENERATE" /></td>
        </tr>
        <tr id="rowplnShow">
            <td style="width: 25%;" colspan="3">
                <asp:Panel ID="pnlExistingData" runat="server" Width="100%">
                </asp:Panel>
            </td>
        </tr>
        <tr id="branch">
            <td style="width: 25%;">
                &nbsp;</td>
            <td style="text-align: left;" class="style2">
                &nbsp;</td>
            <td style="width: 63%">
                &nbsp;&nbsp;<input id="btnConfirm" onclick="return btnConfirm_onclick()" 
                    style="font-family: cambria; cursor: pointer; width: 10%" type="button" 
                    value="CONFIRM" />&nbsp;
                &nbsp; &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer;
                    width: 10%;" type="button" value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <asp:HiddenField ID="hdnExistingDtl" runat="server" />
                <asp:HiddenField ID="hdnUploadDtl" runat="server" />
                 <asp:HiddenField ID="hdnBatch" runat="server" />
            </td>
        </tr>
    </table>
    <configuration>
    <system.web>
    <compilaton debug="true" targetFramework="4.0"></compilaton>
    </system.web>
    </configuration>
</asp:Content>
