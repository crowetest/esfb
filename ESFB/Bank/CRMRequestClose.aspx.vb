﻿
Imports System.Data
Imports System.Data.SqlClient


Imports System.Collections
Imports System.Configuration

Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq


Imports System.IO
Imports System.Net
Imports System.Web.Services
Imports System.Web.Script.Serialization
Partial Class CRMRequestClose
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
    Dim Mobileno As String
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1291) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            DT = DB.ExecuteDataSet("select c.app_request_id,c.app_id,j.app_name,c.created_branch_name +'-' +convert(varchar,c.created_branch) branchName,d.emp_code, c.Emp_Name,e.App_Type_name,d.Remarks,d.mail_id as email,d.mobile as mobile ,c.department, c.Designation ,k.Order_ID ,c.level_id,K.LEVEL_NAME,e.app_type_id ,'HO App Dt :'+convert(varchar,c.ho_approved_on)+' Reporting By : '+isnull(reporting_name,'')+'('+convert(varchar,isnull(reporting_to,''))+') HO:'+isnull(q.name,'')+'('+convert(varchar,isnull(q.emp_code,''))+')' as Approved_By ,c.request_no, case when (c.app_id=14 and c.app_type_id in(68,70,107)) then 'Esaf@123' else '' end as passwd,case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESMACO' else 'LBS' end Entity,(SELECT Stuff((select cr.category + ' = ' + cr.type_name + ', ' AS 'data()' from ESFB.dbo.App_Crm_Request_all cr where cr.app_request_id=c.app_request_id FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,0,N'')) TypeName,case when c.app_type_id in(73,76) then 1 else case when c.app_type_id=74 then 3 else case when c.app_type_id=75 then 2 else 0 end end end Category from ESFB.dbo.app_level k ,ESFB.dbo.app_request_master c  WITH (NOLOCK) left join esfb.dbo.app_level_email q on c.ho_approved_by=q.emp_code,ESFB.dbo.app_master j,ESFB.dbo.app_type_master e , ESFB.dbo.app_dtl_profile d  where j.app_id=c.app_id and k.app_id=c.app_id and c.App_type_id=e.App_type_id and c.App_request_id=d.App_request_id  and c.HO_Approved_status =1 and c.level_id=k.level_id  and c.app_id=14 and c.closed_status is null order by c.ho_approved_on").Tables(0)
            'DT = DB.ExecuteDataSet("select c.app_request_id,c.app_id,j.app_name,c.created_branch_name +'-' +convert(varchar,c.created_branch) branchName,d.emp_code, c.Emp_Name,e.App_Type_name,d.Remarks,d.mail_id as email,d.mobile as mobile ,c.department, c.Designation ,k.Order_ID ,c.level_id,K.LEVEL_NAME,e.app_type_id ,'HO App Dt :'+convert(varchar,c.ho_approved_on)+' Reporting By : '+isnull(reporting_name,'')+'('+convert(varchar,isnull(reporting_to,''))+') HO:'+isnull(q.name,'')+'('+convert(varchar,isnull(q.emp_code,''))+')' as Approved_By ,c.request_no, case when (c.app_id=14 and c.app_type_id in(68,70,107)) then upper(substring(replace(c.Emp_Name,' ',''),1,1))+'es!f'+lower(substring(replace(c.Emp_Name,' ',''),3,1))+convert(varchar,c.app_id+100+datepart(day,getdate())+datepart(month,getdate())) else '' end as passwd,case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESMACO' else 'LBS' end Entity,(SELECT Stuff((select cr.category + ' = ' + cr.type_name + ', ' AS 'data()' from ESFB.dbo.App_Crm_Request_all cr where cr.app_request_id=c.app_request_id FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,0,N'')) TypeName,case when c.app_type_id in(73,76) then 1 else case when c.app_type_id=74 then 3 else case when c.app_type_id=75 then 2 else 0 end end end Category from ESFB.dbo.app_level k ,ESFB.dbo.app_request_master c  WITH (NOLOCK) left join esfb.dbo.app_level_email q on c.ho_approved_by=q.emp_code,ESFB.dbo.app_master j,ESFB.dbo.app_type_master e , ESFB.dbo.app_dtl_profile d  where j.app_id=c.app_id and k.app_id=c.app_id and c.App_type_id=e.App_type_id and c.App_request_id=d.App_request_id  and c.HO_Approved_status =1 and c.level_id=k.level_id  and c.app_id=14 and c.closed_status is null order by c.ho_approved_on").Tables(0)

            Me.Master.subtitle = "CRM Request Closing"

            Dim decryptval As String
            Dim StrAttendance As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                decryptval = GF.Encrypt(DT.Rows(n)(0).ToString)
                StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString & "µ" & DT.Rows(n)(16).ToString & "µ" & DT.Rows(n)(17).ToString & "µ" & DT.Rows(n)(18).ToString & "µ" & DT.Rows(n)(19).ToString & "µ" & DT.Rows(n)(20).ToString & "µ" & decryptval.ToString & "µ" & DT.Rows(n)(21).ToString
                If n < DT.Rows.Count - 1 Then
                    StrAttendance += "¥"
                End If
            Next

            hid_dtls.Value = StrAttendance
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim Mobile As String = CStr(Data(1))
        Dim dataval As String = CStr(Data(2))
        Dim Passwd As String = CStr(Data(3))
        Dim Status As Integer = CInt(Data(4))
        Dim Remarks As String = CStr(Data(5))
        Dim Category As Integer = CInt(Data(6))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim Mobileno As String = ""

        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try

            Dim Params(5) As SqlParameter
            Params(0) = New SqlParameter("@userID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(1).Value = BranchID
            Params(2) = New SqlParameter("@EMPDtl", SqlDbType.VarChar)
            Params(2).Value = dataval.Substring(1)
            Params(3) = New SqlParameter("@Category", SqlDbType.Int)
            Params(3).Value = Category
            Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(4).Direction = ParameterDirection.Output
            Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(5).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_APP_CRM_CLOSE", Params)
            ErrorFlag = CInt(Params(4).Value)
            Message = CStr(Params(5).Value)

            Dim RetStr() As String

            Dim MsgId As String

            Dim Count As Integer = 0
            Dim DT As New DataTable
            Dim StrUrl As String

            If ErrorFlag = 0 Then
                If Mobile <> "" Then
                    Mobileno = "91" + Mobile.ToString()
                    DT = DB.ExecuteDataSet("SELECT sender,username,password FROM ESFB.dbo.SMS_PORTAL WHERE STATUS_ID =1").Tables(0)
                    If DT.Rows.Count > 0 Then
                        Dim Sender As String = ""
                        Dim UserName As String = ""
                        Dim PassWord As String = ""
                        Sender = DT.Rows(0).Item(0).ToString()
                        UserName = DT.Rows(0).Item(1).ToString()
                        PassWord = DT.Rows(0).Item(2).ToString()
                        If Passwd <> "" And Status = 1 Then
                            StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "CRM - Your Login password is " & Passwd & "  " & Remarks & "&GSM=" & Mobileno & ""
                        ElseIf Passwd = "" And Status = 1 Then
                            StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "CRM - Your Request has been updated. " & "&GSM=" & Mobileno & ""
                        Else
                            StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "CRM - Your Request has been rejected. " & "&GSM=" & Mobileno & ""
                        End If
                        RetStr = WEB_Request_Response(StrUrl, 1)


                    End If



                End If
            End If
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString



    End Sub
#End Region
#Region "Function"

    Public Shared Function WEB_Request_Response(ByVal Request As String, ByVal Request_type As Integer) As String()

        'Added On 22-10-2020 by 40013 - Changed request from IT
        Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
        Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString

        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        If Request_type = 1 Then
            delimiters = {"<", ">", ">/", "</", "/"}
        Else
            delimiters = {"=", " "}
        End If
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)


        Return parts.ToArray()


    End Function
#End Region
End Class
