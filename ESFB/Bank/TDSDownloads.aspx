﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="TDSDownloads.aspx.vb" Inherits="TDSDownloads" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ESAF</title>
	<!-- BOOTSTRAP STYLES-->
    <link HRef="Style/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link HRef="Style/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link HRef="Style/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link HRef="Style/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
     <script src="../../Script/Calculations.js" type="text/javascript"></script>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        .pn1
        {
            margin-top :-350px;
        }
    </style>
  <script language="javascript" type="text/javascript">
      function window_onload() {

          var tab = "<div><br/><br/></div>";
         
          //document.getElementById("lblName").innerHTML = document.getElementById("<%= hid_User.ClientID %>").value;
          
          table_fill_Finance();
      }

      

      

      function table_fill_Finance() {
          
         
          document.getElementById("lblCaption").innerHTML = "<b style ='font-size:15pt;'>Finance Management</b>";
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          
          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          tab += "<tr><td colspan='3'><img src='../Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a HRef='TDS/"+ document.getElementById("<%= hid_branch.ClientID %>").value+".zip' target='_blank'><b>TDS</b></a></td></tr>";
        

          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }
      
      function FromServer(arg, context) {

          switch (context) {
              case 1:
                  {
                      document.getElementById("<%= hid_Finance.ClientID %>").value = arg; 
                      table_fill_Finance();
                      break;
                  }
             
          }

      }
      function TDS() {
         
          document.getElementById("<%= hid_Type.ClientID %>").value = 1;
          table_fill_Finance();
      }
      
    </script>
    </head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            
  <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">&nbsp; <a HRef="../home.aspx" class="btn btn-danger square-btn-adjust">Home</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                    
                     <h4><label id="lblCaption" style="font-size:12pt;font-weight:normal;color:#E31E24;"></label><asp:HiddenField ID="hid_branch" runat="server" />
                        
                        <asp:HiddenField ID="hid_User" runat="server" />
                        <asp:HiddenField ID="hid_EmpCode" runat="server" />
                        <asp:HiddenField ID="hid_Leave" runat="server" />
                        <asp:HiddenField ID="hid_Promotion" runat="server" />
                        <asp:HiddenField ID="hid_Finance" runat="server" />
                        <asp:HiddenField ID="hid_dtls" runat="server" />
                        <asp:HiddenField ID="hdnSubID" runat="server" />
                        <asp:HiddenField ID="hdnFromDt" runat="server" />
                        <asp:HiddenField ID="hdnToDt" runat="server" />
                        
                        <asp:HiddenField ID="hid_Transfer" runat="server" />
                        <asp:HiddenField ID="hid_Type" runat="server" />
                        <asp:HiddenField ID="hid_Job" runat="server" />
                        <asp:HiddenField ID="hid_Compliant" runat="server" />
                        </h4>   
                        
                                               
                <br /><asp:Panel ID="pnlDtls" runat="server"></asp:Panel>
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <Finance />
                              
                           
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <!-- METISMENU SCRIPTS -->
    
    
   
    </form>
    
   
</body>
</html>
</asp:Content>