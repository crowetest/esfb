﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="KitRestrictionRemoveRequest.aspx.vb" Inherits="KitRestrictionRemoveRequest" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
     <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10pt;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10pt;color:#476C91;
        }
    </style>

    <script src="../Script/Validations.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
       
        function AlphaNumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = (e.which >= 48 && e.which <= 57) || (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || (e.which == 8) || (e.which == 13) || (e.which == 32) || (e.which == 0);

            if (!valid) {
                e.preventDefault();
            }
        }
        function NumericDotCheck(e)//------------function to check whether a value is alpha numeric
        {
            // el - this, e - event
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                    if (unicode == 37 || unicode == 38) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (unicode == 46) {
                        var parts = el.value.split('.');
                        if (parts.length == 2)
                            return false;
                        return true;
                    }
                }
            }
            else {
                return true;
            }
        }
        function NumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = ((e.which >= 48 && e.which <= 57) || (e.keyWhich == 9) || (e.keyCode == 9) || (e.keyWhich == 8) || (e.keyCode == 8));
            //alert(valid);
            if (!valid) {
                e.preventDefault();
            }

        }
        function FromServer(Arg, Context) {
            var Data = Arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("KitRestrictionRemoveRequest.aspx", "_self");
        }
        function btnApprove_onclick() {
            var ret = SaveData();
            if (ret==false) 
            {
                return false;
            }
           
          
            var strempcode = document.getElementById("<%= hid_temp.ClientID%>").value;
            var Data = "1Ø" + strempcode;
            ToServer(Data, 1);
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function DeleteRow(id) {
            
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            var NewStr = "";
             for (n =1; n <= row.length - 1 ; n++) {
                    
                
                    
                     
                     if (id != n) {
                        

                         NewStr += "¥" + row[n];
                     }
                     else {
                         
                         var CIF = document.getElementById("txtCIF" + n).value;
                         var AcNO = document.getElementById("txtAcNo" + n).value;;
                         var Name = document.getElementById("txtName" + n).value;
                         var Newgen = document.getElementById("txtNewgen" + n).value;


                         var TranID = (document.getElementById("txtTranID" + n).value == "") ? 0 : document.getElementById("txtTranID" + n).value;
                         var strval = "¥" + CIF + "µ" + AcNO + "µ" + Name + "µ" + Newgen + "µ" + TranID + "µ1";
                        
                         if (TranID == 0 || TranID == "") {
                             NewStr += "";
                             
                         }

                         else if (TranID > 0) {
                             NewStr += strval;
                            
                             
                         }
                         else {

                             NewStr = "¥µµµµµ";
                            
                         }


                     }

                 }

             document.getElementById("<%= hid_dtls.ClientID %>").value = NewStr;
            
             if (document.getElementById("<%= hid_dtls.ClientID %>").value == "¥µµµµ0µ1" || document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                document.getElementById("<%= hid_dtls.ClientID %>").value = "¥µµµµµ";
            }
                table_fill();
        }
        function updateValue(id) {
            var NewStr = ""
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "¥µµµµµ") {
               
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                
               
                for (n = 1; n <= row.length - 1 ; n++) {
                   
                    if (id == n) {
                       
                        var CIF = document.getElementById("txtCIF" + id).value;
                        var AcNO = document.getElementById("txtAcNo" + id).value;;
                        var Name = document.getElementById("txtName" + id).value;
                        var Newgen = document.getElementById("txtNewgen" + id).value;


                        var TranID = (document.getElementById("txtTranID" + id).value == "") ? 0 : document.getElementById("txtTranID" + id).value;
                       
                        NewStr += "¥" + CIF + "µ" + AcNO + "µ" + Name + "µ" + Newgen + "µ" + TranID + "µ0";

                    }
                    else {
                       
                        NewStr += "¥" + row[n];
                    }
                }
                
            }
            else {
                
                var CIF = document.getElementById("txtCIF1").value;
                var AcNO = document.getElementById("txtAcNo1").value;;
                var Name = document.getElementById("txtName1").value;
                var Newgen = document.getElementById("txtNewgen1").value;
               
                var TranID = (document.getElementById("txtTranID1").value == "") ? 0 : document.getElementById("txtTranID1").value; 
                
                NewStr += "¥" + CIF + "µ" + AcNO + "µ" + Name + "µ" + Newgen + "µ" + TranID + "µ0";
            }
           
            document.getElementById("<%= hid_dtls.ClientID %>").value = NewStr;
            
        }
        function SaveData() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "" || document.getElementById("<%= hid_dtls.ClientID %>").value == "¥µµµµ") {
                alert("Enter any cheque details");
                return false;
            }
            var NewStr = ""
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "¥µµµµµ") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");


                for (n = 1; n <= row.length - 1 ; n++) {
                    if (row[n] != "µµµµµ") {
                        col = row[n].split("µ");
                        if (col[0] == "" || col[1] == "" || col[2] == "" ||  col[3] == "") {
                            alert("Enter empty field");
                            document.getElementById("txtCIF" + n).focus();
                            return false;

                        }
                        NewStr += "¥" + row[n];
                    }
                }

            }
          

            document.getElementById("<%= hid_temp.ClientID%>").value = NewStr;
            return true;
        }

        function CreateNewRow(e, val) {
            
            var n = (window.Event) ? e.which : e.keyCode;
            
            if (n == 13 || n == 9) {
                
                updateValue(val);
                AddNewRow();
            }
        }
        function table_fill() {
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                document.getElementById("<%= pnFamily.ClientID %>").style.display = '';
             var row_bg = 0;
             var tab = "";
             tab += "<div style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
             tab += "<tr height=20px;  class='tblQal'>";
             tab += "<td style='width:5%;text-align:center' >#</td>";
             tab += "<td style='width:20%;text-align:left' >CIF</td>";
             tab += "<td style='width:30%;text-align:left' >A/C Number</td>";
             tab += "<td style='width:35%;text-align:left' >Cust Name</td>";
             tab += "<td style='width:30%;text-align:left'>NewgenWorkItem</td>";
             tab += "<td style='width:1%;text-align:left; display:none;'></td>";
             tab += "<td style='width:9%;text-align:left'>Del</td>";

             tab += "</tr>";
             tab += "</table></div>";
             tab += "<div id='ScrollDiv' style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

             row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var i = 0;
            for (n = 1; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                if (col[5] != 1) {
                    var i = i+1;
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }

                    tab += "<td style='width:5%;text-align:center' >" + i + "</td>";

                    

                    
                    if (col[0] == "") {
                        var txtBox = "<input id='txtCIF" + n + "' name='txtCIF" + n + "' type='Text' style='width:100%;' value='" + col[0] + "' class='NormalText' onchange='updateValue(" + n + ")' maxlength='12' onkeypress='return NumericCheck(event)'  />";
                    }
                    else
                        var txtBox = "<input id='txtCIF" + n + "' name='txtCIF" + n + "' type='Text' disabled=true style='width:100%;' value='" + col[0] + "' class='NormalText' onchange='updateValue(" + n + ")' maxlength='12' onkeypress='return NumericCheck(event)'  />";

                        tab += "<td style='width:20%;text-align:left' >" + txtBox + "</td>";
                    

                    
                    if (col[1] == "") {
                        var txtBox = "<input id='txtAcNo" + n + "' name='txtAcNo" + n + "' type='Text' style='width:100%;' value='" + col[1] + "' class='NormalText' onchange='updateValue(" + n + ")' maxlength='14' onkeypress='return NumericCheck(event)'  />";
                    }
                        else
                        var txtBox = "<input id='txtAcNo" + n + "' name='txtAcNo" + n + "' type='Text' disabled=true style='width:100%;' value='" + col[1] + "' class='NormalText' onchange='updateValue(" + n + ")' maxlength='14' onkeypress='return NumericCheck(event)'  />";

                        tab += "<td style='width:30%;text-align:left' >" + txtBox + "</td>";
                    
                    
                    
                    if (col[2] == "") {
                        var txtBox = "<input id='txtName" + n + "' name='txtName" + n + "' type='Text' value='" + col[2] + "' style='width:100%;' class='NormalText' maxlength='100' />";
                    }
                        else
                        var txtBox = "<input id='txtName" + n + "' name='txtName" + n + "' type='Text' disabled=true value='" + col[2] + "' style='width:100%;' class='NormalText' maxlength='100' />";

                        tab += "<td style='width:35%;text-align:left' >" + txtBox + "</td>";

                   

                    
                        if (col[3] == "") {
                            var txtBox = "<input id='txtNewgen" + n + "' name='txtNewgen" + n + "' type='Text' value='" + col[3] + "' style='width:100%;' class='NormalText' maxlength='100' onchange='updateValue(" + n + ")' onkeypress='return AlphaNumericCheck(event)' onchange='updateValue(" + n + ")'  onkeydown='CreateNewRow(event," + n + ")' />";
                        
                    }
                        else
                            var txtBox = "<input id='txtNewgen" + n + "' name='txtNewgen" + n + "' type='Text' disabled=true  value='" + col[3] + "' style='width:100%;' class='NormalText' maxlength='100' onchange='updateValue(" + n + ")' onkeypress='return AlphaNumericCheck(event)' onchange='updateValue(" + n + ")'  onkeydown='CreateNewRow(event," + n + ")' />";

                        tab += "<td style='width:30%;text-align:left' >" + txtBox + "</td>";

                   
                    
                    
                    if (col[4] == "") {
                        var txtBox = "<input id='txtTranID" + n + "' name='txtTranID" + n + "' type='Text' value='" + col[4] + "' style='width:100%;' class='NormalText' maxlength='100' onkeypress='return NumericCheck(event)' '/>";
                    }
                    else
                        var txtBox = "<input id='txtTranID" + n + "' name='txtTranID" + n + "' type='Text' disabled=true value='" + col[4] + "' style='width:100%;' class='NormalText' maxlength='100' onkeypress='return NumericCheck(event)' '/>";

               

                    tab += "<td style='width:1%;text-align:left; display:none;'>" + txtBox + "</td>";
                    
                    tab += "<td style='width:9%;text-align:center' onclick=DeleteRow('" + n + "')><img  src='../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                    tab += "</tr>";
                }
            }

            tab += "</table></div></div></div>";
            document.getElementById("<%= pnFamily.ClientID %>").innerHTML = tab;
            
            setTimeout(function () {
                document.getElementById("txtCIF" + (n - 1)).focus().select();
            }, 4);
        }
        else
            document.getElementById("<%= pnFamily.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }

        function AddNewRow() {
           
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var Len = row.length - 1;
                col = row[Len].split("µ");
                if (col[0] != "" && col[1] != "" && col[2] != "" && col[3] != "") {

                    document.getElementById("<%= hid_dtls.ClientID %>").value += "¥µµµµµ";

                }
            }
            else {
                
                document.getElementById("<%= hid_dtls.ClientID%>").value = "¥µµµµµ";
            }
               
                table_fill();
        }
       
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />
     <div class="ScrollClass">
        <b></b><hr  style="color: #E31E24; margin-top: 0px;" />
    </div>
    <br />
    <br />
    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnFamily" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;"   colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
               
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="UPDATE"  onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

