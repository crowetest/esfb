﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="BranchCTSUpdation.aspx.vb" Inherits="BranchCTSUpdation" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
   
   
   
   
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 1; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function ClearCombo(control) {
        document.getElementById(control).options.length = 0;
        var option1 = document.createElement("OPTION");
        option1.value = -1;
        option1.text = " -----Select-----";
        document.getElementById(control).add(option1);
    }
    function FromServer(arg, context) {
        if (context == 1) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("BranchCTSUpdation.aspx", "_self");
        }
        
    }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

    function RequestOnClick() {    
        if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1") 
        {
            alert("Select Branch");
            document.getElementById("<%= cmbBranch.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%= fup1.ClientID%>").value == "") {
            alert("Attach File"); document.getElementById("<%= fup1.ClientID%>").focus(); return false;
        }
        document.getElementById("<%= hdnValue.ClientID %>").value = "1Ø" + document.getElementById("<%= cmbBranch.ClientID %>").value;
            
    }

    function AddMoreImages() {
        if (!document.getElementById && !document.createElement)
            return false;
        var fileUploadarea = document.getElementById("fileUploadarea");
        if (!fileUploadarea)
            return false;
        var newLine = document.createElement("br");
        fileUploadarea.appendChild(newLine);
        var newFile = document.createElement("input");
        newFile.type = "file";
        newFile.setAttribute("class", "fileUpload");
            
        if (!AddMoreImages.lastAssignedId)
            AddMoreImages.lastAssignedId = 100;
        newFile.setAttribute("id", "FileUpload" + AddMoreImages.lastAssignedId);
        newFile.setAttribute("name", "FileUpload" + AddMoreImages.lastAssignedId);
        var div = document.createElement("div");
        div.appendChild(newFile);
        div.setAttribute("id", "div" + AddMoreImages.lastAssignedId);
        fileUploadarea.appendChild(div);
        AddMoreImages.lastAssignedId++;
    }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    </html>
    
    <br />
    <br />
    <table class="style1" style="width: 80%; margin: 0px auto;">
        <tr id="branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
             
        <tr id="Tr7">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Attachment If any
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;
                <div id="fileUploadarea">
                    <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" /><br />
                </div>
                <br />
                <div>
                    <input style="display: block;" id="btnAddMoreFiles" type="button" value="Add more images"
                        onclick="AddMoreImages();" /><br />
                </div>
            </td>
            <td style="width: 12%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 20%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:Button ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
                <asp:HiddenField ID="hdnAnnexture" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
