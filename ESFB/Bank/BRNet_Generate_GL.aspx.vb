﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports System.Configuration
Imports System
Imports System.Text

Partial Class BRNet_Generate_GL
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim DTExcel As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim values As New DataTable
    Dim RequestID, UserID As Integer

#Region "Page Load & Dispose"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If GF.FormAccess(CInt(Session("UserID")), 1240) = False Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        If Not IsPostBack Then
            DT = DB.ExecuteDataSet("select glno from ESFB.dbo.App_brnet_glno_parameter").Tables(0)
            Me.txtGlno.Text = CStr(DT.Rows(0)(0))

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.cmbBatch.Attributes.Add("onchange", "return BatchOnChange()")
        End If
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
    Sub DeleteFilesFromFolder(ByVal Folder As String)
        If Directory.Exists(Folder) Then
            For Each _file As String In Directory.GetFiles(Folder)
                File.Delete(_file)
            Next
            For Each _folder As String In Directory.GetDirectories(Folder)
                DeleteFilesFromFolder(_folder)
            Next
        End If
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        UserID = CInt(Session("UserID"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1 'Fill Vet Request For Approve              
                DT = GF.GetQueryResult("select -1, '-----Select Batch-----' union all select distinct b.batch_id, b.batch_name from ESFB.dbo.App_brnet_request_batch_dtl a, ESFB.dbo.App_brnet_request_batch b where a.gl_status_dtl in(1,2) and a.batch_id=b.batch_id and b.status_id=1 and b.gl_status=0")
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2
                Dim BatchID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select glno+'Æ'+upper(account_title)+' ('+ convert(varchar(12),emp_code)+ ')' +'Æ'+convert(varchar(12),emp_code)+'Æ'+convert(varchar(20),batch_dtl_id) from ESFB.dbo.App_brnet_request_batch_dtl a, ESFB.dbo.App_brnet_request_batch b where a.batch_id= " & BatchID & " and a.gl_status_dtl=2 and a.batch_id=b.batch_id and a.glno is not null and b.status_id=1 and b.gl_status=0")
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += DR(0).ToString() + "®"
                    Next
                End If
            Case 3
                Dim BatchID As Integer = CInt(Data(1))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(3) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@BatchID", SqlDbType.VarChar)
                    Params(1).Value = BatchID
                    Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(2).Direction = ParameterDirection.Output
                    Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(3).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_BRNET_GENERATE_GL", Params)
                    ErrorFlag = CInt(Params(2).Value)
                    Message = CStr(Params(3).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
            Case 4
                Dim GLNumber As String = CStr(Data(1))
                Dim DtlID As Integer = CInt(Data(2))
                Dim BatchID As Integer = CInt(Data(3))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(4) As SqlParameter
                    Params(0) = New SqlParameter("@GLNo", SqlDbType.VarChar, 100)
                    Params(0).Value = GLNumber
                    Params(1) = New SqlParameter("@DtlID", SqlDbType.Int)
                    Params(1).Value = DtlID
                    Params(2) = New SqlParameter("@BatchID", SqlDbType.Int)
                    Params(2).Value = BatchID
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_BRNET_UPDATE_GL", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
            Case 5
                Try
                    Dim GlNo As Integer = CInt(Data(1))
                    Dim Err As Integer = DB.ExecuteNonQuery("update ESFB.dbo.App_brnet_glno_parameter set glno=" & GlNo & "")
                    If Err = 1 Then
                        CallBackReturn = CStr(0) + "ʘ" + "Last GL Updated Successffully"
                    End If
                Catch ex As Exception
                  
                    Response.Redirect("~/CatchException.aspx?ErrorNo=1")
                End Try
        End Select
    End Sub
#End Region

End Class
