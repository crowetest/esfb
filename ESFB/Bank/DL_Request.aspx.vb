﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class DL_Request
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1209) = False Or CInt(Session("BranchID")) > 1000 Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "DL Request Creation"
            hid_Empcode.Value = CStr(Session("UserID"))
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0
            DT = DB.ExecuteDataSet("select '-1' as id,'------Select--------' union all  select distinct dl_ID,dl_Name from dl_MASTER ").Tables(0)
            GN.ComboFill(cmbDL, DT, 0, 1)

            DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER  where branch_id=" & CInt(Session("BranchID")) & "").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
            cmbBranch.SelectedIndex = 0
            If CInt(Session("BranchID")) < 100 Then
                DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER  ").Tables(0)
                hdn_RetailFlg.Value = "1"
            ElseIf CInt(Session("BranchID")) > 50000 Then
                DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER  ").Tables(0)
                hdn_RetailFlg.Value = "0"
            Else
                DT = DB.ExecuteDataSet(" select distinct Branch_ID,Branch_Name from BRANCH_MASTER ").Tables(0)
                hdn_RetailFlg.Value = "1"
            End If
          

            cmbBranch.SelectedIndex = 0

            'If hdn_RetailFlg.Value = "0" Then 'usb
            DT = DB.ExecuteDataSet("select '-1' as id,'------Select--------' union all select app_id,app_name from ESFB.dbo.app_master where app_id=4").Tables(0)
            'Else
            'DT = DB.ExecuteDataSet("select '-1' as id,'------Select--------' union all select app_id,app_name from app_master ").Tables(0)
            'End If

            GN.ComboFill(cmbCategory, DT, 0, 1)
            'DT = DB.ExecuteDataSet("select * from (select '-1' as app_role,'------Select--------' as app_role_name union all select app_role,app_role_name from ESFB.dbo.app_role_master where status_id=1 and  sfb_status=1 )A order by 2 ").Tables(0)
            'GN.ComboFill(cmbRoleOld, DT, 0, 1)
            'cmbRole.SelectedIndex = 0


            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            Me.cmbType.Attributes.Add("onchange", "return TypeOnChange()")
           
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "RequestOnchange();", True)

            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            txtEmpCode.Attributes.Add("onchange", "EmployeeOnChange()")
            cmbCategory.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim EmpID As Integer = CInt(Data(1))
        Dim strval As String
        Dim strfld As String
        Dim strfld1 As String
        Dim strwhere As String
        If CInt(Data(0)) = 1 Then
            If CInt(Data(2)) > 0 Then 'Profile
                strval = " left join ESFB.dbo.App_dtl_profile g on a.emp_code=g.emp_code  left join ESFB.dbo.app_request_master h on g.App_request_id=h.App_request_id and h.App_id=" & Data(2).ToString & " and h.App_type_id=" & Data(3).ToString & "  and h.Approved_By is null"
                strfld = "b.mobile,"
                strwhere = ""
                strfld1 = " ,g.Role_id"
                'ElseIf CInt(Data(2)) = 2 Then
                '    strval = " left join App_dtl_IT g on a.emp_code=g.emp_code  left join  app_request_master h on g.App_request_id=h.App_request_id and h.App_id=" & Data(2).ToString & " and h.App_type_id=" & Data(3).ToString & "  and h.Approved_By is null"
                '    strfld = "g.cug,"
                '    strwhere = "" '"and h.App_id=1 and h.App_type_id=2 and h.Approved_By is null"
                '    strfld1 = " ,-1 "
                'ElseIf CInt(Data(2)) = 3 Then
                '    strval = " left join App_dtl_IT g on a.emp_code=g.emp_code  left join app_request_master h on g.App_request_id=h.App_request_id and h.App_id=" & Data(2).ToString & " and h.App_type_id=" & Data(3).ToString & "  and h.Approved_By is null"
                '    strfld = "g.cug,"
                '    strwhere = "" '"and h.App_id=1 and h.App_type_id=3 and h.Approved_By is null"
                '    strfld1 = " ,-1 "
            Else
                strval = ""
                strfld = ""
                strfld1 = ""
            End If
            DT = DB.ExecuteDataSet("select distinct a.emp_code,a.emp_name,Official_mail_id," & strfld.ToString & " " & _
                    " cug_no,department_name,designation_name,convert(varchar,a.date_of_join,106),a.reporting_to, " & _
                    " e.emp_name + '('+ convert(varchar,a.reporting_to)+')'  as reportingEmpCode,Emp_status " & strfld1.ToString & " from " & _
                    " Emp_master a " & strval.ToString & ",emp_profile b,department_master c,designation_master d,Emp_master e, " & _
                    " Employee_status f where a.emp_code=b.emp_code and a.department_id=c.Department_ID and a.designation_id=d.designation_id  " & _
                    " and a.reporting_to=e.emp_code and a.emp_status_id=f.emp_status_id  and a.status_id=1 and  a.emp_code=" + CInt(Data(1)).ToString + " " & strwhere.ToString).Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString() + "Ø" + DT.Rows(0)(6).ToString() + "Ø" + DT.Rows(0)(7).ToString() + "Ø" + DT.Rows(0)(8).ToString() + "Ø" + DT.Rows(0)(9).ToString() + "Ø" + DT.Rows(0)(10).ToString() + "Ø" + DT.Rows(0)(11).ToString()

            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf CInt(Data(0)) = 2 Then
            DT = DB.ExecuteDataSet("select  '-1' as id,' -----Select------' as Name union all  select app_type_id,app_type_name from ESFB.dbo.App_Type_master where status_id=1  and app_type_id in (36,37,38) and app_id=" + Data(1).ToString()).Tables(0)
            If (DT.Rows.Count > 0) Then
                Dim StrVal1 As String = ""

                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal1 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal1 += "Ñ"
                    End If
                Next

                Dim StrVal2 As String = ""
                CallBackReturn = StrVal1 + "|"
                DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name )A  order By app_role_name").Tables(0)
                If (DT.Rows.Count > 0) Then
                    For n As Integer = 0 To DT.Rows.Count - 1
                        StrVal2 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                        If n < DT.Rows.Count - 1 Then
                            StrVal2 += "Ñ"
                        End If
                    Next
                    CallBackReturn += StrVal2
                End If
            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf CInt(Data(0)) = 5 Then
            Dim StrVal2 As String = ""
            If CInt(Data(1)) = 1 Then
                DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name union all  select app_role,app_role_name from ESFB.dbo.App_role_master where status_id=1 and sfb_status=1  and  app_id=" + Data(2).ToString() + ")A  order By app_role_name").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select * from (select  '-1' as app_role,' -----Select------' as app_role_name union all  select app_role,app_role_name from ESFB.dbo.App_role_master where status_id=1  and sfb_status=1  and app_role<>" + Data(1).ToString() + " and app_id=" + Data(2).ToString() + ")A  order By app_role_name").Tables(0)
            End If

            If (DT.Rows.Count > 0) Then
                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal2 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal2 += "Ñ"
                    End If
                Next
                CallBackReturn += StrVal2
            End If
        ElseIf CInt(Data(0)) = 4 Then
            Dim StrVal2 As String = ""
            DT = DB.ExecuteDataSet("select * from (select  '-1' as Branch_ID,' -----Select------' as Branch_Name union all  select distinct Branch_ID,Branch_Name from BRANCH_MASTER  where branch_id<>" & Data(1).ToString() & ")A  order By Branch_Name").Tables(0)
            If (DT.Rows.Count > 0) Then
                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal2 += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal2 += "Ñ"
                    End If
                Next
                CallBackReturn += StrVal2
            End If
        End If
    End Sub
#End Region

    Private Sub initializeControls()
        cmbBranch.Focus()
        cmbCategory.Text = "-1"
        cmbType.Text = "-1"
        txtEmpCode.Text = ""
        txtEmpName.Text = ""
        'txtdetails.Text = ""
        txtContactNo.Text = ""
        txtEmailid.Text = ""
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            '"1Ø" + Branch + "Ø" + APP_ID + "Ø" + TYPE_ID+ "Ø" + Descr + "Ø" + contact + "Ø" + emailid + "Ø" + 
            'Emp_code + "Ø" +  CUG + "Ø" +  DL  + "Ø" +DLID  + "Ø" +DLNAME  ;       
            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim Branch As Integer = CInt(Data(1))
            Dim REQUEST_ID As Integer = 0
            Dim APP_ID As Integer = CInt(Data(2))
            Dim TYPE_ID As Integer = CInt(Data(3))
            Dim Descr As String = CStr(Data(4))
            Dim Contactno As String = CStr(Data(5))
            Dim EmailId As String = CStr(Data(6))
            Dim EMP_CODE As Integer = CInt(Data(7))
            Dim CUG As String = CStr(Data(8))
            Dim DL As String = CStr(Data(9))
            Dim DLID As Integer = CInt(Data(10))
            Dim DLName As String = CStr(Data(11))

            Dim UserID As String = Session("UserID").ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try


                Dim Params(14) As SqlParameter
                Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
                Params(0).Value = Branch
                Params(1) = New SqlParameter("@EMP_CODE", SqlDbType.Int)
                Params(1).Value = EMP_CODE
                Params(2) = New SqlParameter("@DESCR", SqlDbType.VarChar, 5000)
                Params(2).Value = Descr
                Params(3) = New SqlParameter("@APP_ID", SqlDbType.Int)
                Params(3).Value = APP_ID
                Params(4) = New SqlParameter("@TYPE_ID", SqlDbType.Int)
                Params(4).Value = TYPE_ID
                Params(5) = New SqlParameter("@DLID", SqlDbType.Int)
                Params(5).Value = DLID
                Params(6) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(6).Value = UserID
                Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(8).Direction = ParameterDirection.Output
                Params(9) = New SqlParameter("@REQUEST_ID", SqlDbType.Int)
                Params(9).Direction = ParameterDirection.Output
                Params(10) = New SqlParameter("@CONTACTNO", SqlDbType.VarChar, 15)
                Params(10).Value = Contactno
                Params(11) = New SqlParameter("@CUG", SqlDbType.VarChar, 15)
                Params(11).Value = CUG
                Params(12) = New SqlParameter("@EMAIL", SqlDbType.VarChar, 100)
                Params(12).Value = EmailId
                Params(13) = New SqlParameter("@DL", SqlDbType.VarChar, 100)
                Params(13).Value = DL
                Params(14) = New SqlParameter("@DLName", SqlDbType.VarChar, 100)
                Params(14).Value = DLName
                
                DB.ExecuteNonQuery("SP_APP_REQUEST_DL", Params)
                ErrorFlag = CInt(Params(7).Value)
                Message = CStr(Params(8).Value)
                'REQUEST_ID = CInt(Params(9).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "'); ")
            cl_script1.Append("        window.open('DL_Request.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            If ErrorFlag = 0 Then
                initializeControls()
            End If



        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
