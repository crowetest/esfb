﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports System.Configuration
Imports System
Imports System.Text

Partial Class BRNet_Upload_CA
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim DTExcel As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim values As New DataTable
    Dim RequestID, UserID As Integer

#Region "Page Load & Dispose"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "file Uploading"

        If GF.FormAccess(CInt(Session("UserID")), 1237) = False Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If

        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        If Not IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.cmbBatch.Attributes.Add("onchange", "return BatchOnChange()")
        End If

    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
    Sub DeleteFilesFromFolder(ByVal Folder As String)
        If Directory.Exists(Folder) Then
            For Each _file As String In Directory.GetFiles(Folder)
                File.Delete(_file)
            Next
            For Each _folder As String In Directory.GetDirectories(Folder)

                DeleteFilesFromFolder(_folder)
            Next

        End If

    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        CallBackReturn = ""
        Select Case CInt(Data(0))
            Case 1 'Fill Vet Request For Approve              
                DT = GF.GetQueryResult("select -1,'---Select---' union select distinct b.batch_id, batch_name from ESFB.dbo.App_brnet_request_batch_dtl a, ESFB.dbo.App_brnet_request_batch b where a.ca_status_dtl=1 and a.acno is null and a.batch_id=b.batch_id and b.status_id=1 and ca_status=0")
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2
                Dim BatchID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select convert(varchar(12),a.cif)+'Æ'+convert(varchar(12),a.emp_code)+'Æ'+upper(a.account_title) from ESFB.dbo.App_brnet_request_batch_dtl a, ESFB.dbo.App_brnet_request_batch b where a.batch_id=" & BatchID & " and a.ca_status_dtl=1 and a.acno is null and a.batch_id=b.batch_id and b.status_id=1 and ca_status=0")
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += DR(0).ToString() + "®"
                    Next
                End If
        End Select
    End Sub
#End Region

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpload.Click
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim msg As String = ""
        Dim connectionString As String = ""
        Dim Batch As Integer = CInt(Me.hdnBatch.Value)
        UserID = CInt(Session("UserID"))

        If Me.fdpFOCollection.HasFile = False Then
            Dim cl_script0 As New System.Text.StringBuilder
            cl_script0.Append("         alert('Please browse excel');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
            Exit Sub
        Else
            Dim strFileType As String = Path.GetExtension(fdpFOCollection.FileName).ToLower()
            Dim fileName As String = Path.GetFileName(fdpFOCollection.PostedFile.FileName)
            Dim fileExtension As String = Path.GetExtension(fdpFOCollection.PostedFile.FileName)

            File.Delete((MapPath(fileName)))
            Dim fileLocation As String = Server.MapPath(fileName)
            fdpFOCollection.SaveAs(fileLocation)

            If fileExtension = ".xls" Then
                If Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE") = "x86" Then
                    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=YES;"""
                Else
                    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                End If
            Else
                Dim cl_script0 As New System.Text.StringBuilder
                cl_script0.Append("         alert('Uploaded Only Excel file with Extention .xls, Ex::sample.xls ');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
                Exit Sub

            End If
            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT [BC Agent CIF],[Emp Code],[Account Number] FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            'TODO - Convert to Another DT without Null Details

            Dim DTExcel As New DataTable
            DTExcel = dtExcelRecords.Select("[BC Agent CIF] is not null").CopyToDataTable

            Try
                Dim Parameters As System.Data.SqlClient.SqlParameter() = {
                New System.Data.SqlClient.SqlParameter("@TableData", DTExcel),
                New System.Data.SqlClient.SqlParameter("@Batch", Batch),
                New System.Data.SqlClient.SqlParameter("@UserID", UserID),
                New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0),
                New System.Data.SqlClient.SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)}

                Parameters(3).Direction = ParameterDirection.Output
                Parameters(4).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_UPDATE_FOCOLLECTION_CA", Parameters)

                ErrorFlag = CInt(Parameters(3).Value)
                Message = CStr(Parameters(4).Value)

                Dim cl_script1 As New System.Text.StringBuilder
                cl_script1.Append("         alert('" + Message.ToString + "');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Dim cl_script1 As New System.Text.StringBuilder
                cl_script1.Append("         alert('" + Message.ToString + "');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
        End If
    End Sub
End Class
