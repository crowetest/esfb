﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ViewSignature
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DB As New MS_SQL.Connect
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))

        If (CInt(Data(0)) = 2) Then
            Dim DT As New DataTable
            Dim DT1 As New DataTable
            Dim GN As New GeneralFunctions
           
            CallBackReturn = ""
            DT1 = DB.ExecuteDataSet("select * from (select  distinct branch_id,branch_name,aa.emp_code,emp_name,e_signature,content_type,case when cc.cug_no is null then cc.mobile else cc.cug_no end as mobile,STATUS,stat from ( " & _
                        " select 1 as stat,a.tranid,a.branch_id,c.branch_name,a.emp_code,d.emp_name,a.approved_on,case when status=0 then 'Pending Approval' when (status=1 and assign_status=4 and transfer_branch is not null) then 'Previous Staff' when (status=1 and assign_status=4 and  transfer_branch is  null) then 'Assigned New branch ' + convert(varchar,transfer_branch) +'and waiting for approval'  else '' end  " & _
                        "  as status,e_signature,e.content_type,h.branch_name+'( '+convert(varchar,h.branch_id)+' )' as transfer_to from branch_Signature a left join branch_master h on a.transfer_branch=h.branch_id,designation_master b,branch_master c,emp_master d,DMS_ESFB.dbo.Emp_Signature  e where  a.post_id=b.designation_id and a.branch_id=c.branch_id and  " & _
                        " a.branch_id=" + Data(2).ToString() + " and a.post_id=" + Data(1).ToString() + " And status = 1 And a.emp_code = d.emp_code And a.tranid = e.tranid " & _
                        " UNION ALL " & _
                        "   select 2 as stat,a.tranid,a.branch_id,c.branch_name,a.emp_code,d.emp_name,max(a.approved_on),case when status=0 then 'Pending Approval' when (status=1 and assign_status=4 and transfer_branch is not null) then 'Previous Staff' when (status=1 and assign_status=3) then 'Suspended' when (status=1 and assign_status=4 and  transfer_branch is  null) then 'Assigned New branch ' + convert(varchar,transfer_branch) +'and waiting for approval'  else '' end " & _
                        "   as status,e.e_signature,e.content_type,h.branch_name+'( '+convert(varchar,h.branch_id)+' )' as transfer_to  from branch_Signature_his a left join branch_master h on a.transfer_branch=h.branch_id,designation_master b,branch_master c,emp_master d,DMS_ESFB.dbo.Emp_Signature  e where  a.post_id=b.designation_id and a.branch_id=c.branch_id and  " & _
                        "  a.branch_id=" + Data(2).ToString() + " and a.post_id=" + Data(1).ToString() + " And status = 1 and assign_status in (4,3)  and  a.emp_code = d.emp_code And a.tranid = e.tranid " & _
                        "  group by a.tranid,assign_status,status,a.branch_id,c.branch_name,a.emp_code,d.emp_name,status,e.e_signature,e.content_type,h.branch_name,h.branch_id,transfer_branch )AA left join emp_profile cc on aa.emp_code=cc.emp_code )BB order by stat").Tables(0)
                If DT1.Rows.Count > 0 Then

                For n As Integer = 0 To DT1.Rows.Count - 1

                    CallBackReturn += DT1.Rows(n)(0).ToString & "µ" & DT1.Rows(n)(1).ToString & "µ" & DT1.Rows(n)(2).ToString & "µ" & DT1.Rows(n)(3).ToString & "µ" & DT1.Rows(n)(4).ToString & "µ" & DT1.Rows(n)(6).ToString & "µ" & DT1.Rows(n)(7).ToString
                    Dim bytes As Byte() = DirectCast(DT1.Rows(n)(4), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                    CallBackReturn += "µ" & Convert.ToString("data:" + DT1.Rows(n)(5).ToString + ";base64,") & base64String

                    If n < DT1.Rows.Count - 1 Then
                        CallBackReturn += "¥"
                    End If
                Next
                End If
            
        ElseIf (CInt(Data(0)) = 3) Then
            Dim DT As New DataTable

            Dim GN As New GeneralFunctions
            DT = DB.ExecuteDataSet("select a.branch_id,b.state_name,c.district_name from branch_master a,state_master b,district_master c where a.state_id=b.state_id  and a.district_id=c.district_id and a.branch_id=" + Data(1).ToString()).Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString()
                CallBackReturn += "Ø"

            Else
                CallBackReturn = "ØØ"
            End If
        End If
    End Sub
#End Region
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            If GN.FormAccess(CInt(Session("UserID")), 1111) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "View Signature"
            DT = DB.ExecuteDataSet(" select '-1' as ID, '-------- Select --------' union all select distinct Branch_ID,convert(varchar,Branch_ID)+' - '+Branch_Name from BRANCH_MASTER  where status_id=1").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
            cmbBranch.SelectedIndex = 0
            DT = DB.ExecuteDataSet(" select '-1' as ID, '-------- Select --------' union all select distinct designation_ID,designation_Name from designation_MASTER  where designation_id in (5,6)").Tables(0)
            GN.ComboFill(cmbPost, DT, 0, 1)
            cmbBranch.SelectedIndex = 0
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- Register Client Side Functions ---//
            cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            cmbPost.Attributes.Add("onchange", "PostOnChange()")


        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
    End Sub
#End Region


End Class
