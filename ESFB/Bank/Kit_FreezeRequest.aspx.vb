﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Kit_FreezeRequest
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim AT As New Attendance
    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1160) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)



            If CInt(Session("BranchID").ToString()) > 100 Then
                Me.Master.subtitle = " Freeze Request"
                hid_status.Value = "Send Request"
                DT = DB.ExecuteDataSet("select tranid,cif,acno,name,newgen_no,c.branch_name,'HO Approved for Loan processing' as pending from ESS_Newgen_Restrict_Removal a,emp_master b,branch_master c where a.created_by=b.emp_code and a.branch_id=c.branch_id and a.branch_id= " & Session("BranchID").ToString() & "  and (ho_approved_status=1 and freeze_request_status is null)").Tables(0)
            Else
                hid_status.Value = "Freezed"
                Me.Master.subtitle = " Freeze Request Pending"
                DT = DB.ExecuteDataSet("select tranid,cif,acno,name,newgen_no,c.branch_name,'HO_Freeze pending' as pending from ESS_Newgen_Restrict_Removal a,emp_master b ,branch_master c where a.created_by=b.emp_code and a.branch_id=c.branch_id and  (ho_approved_status=1 and freeze_request_status =1 and closed_status is null)").Tables(0)
            End If

            Dim StrAttendance As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString
                If n < DT.Rows.Count - 1 Then
                    StrAttendance += "¥"
                End If
            Next


            hid_dtls.Value = StrAttendance
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim dataval As String = CStr(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))



        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try

            Dim Params(4) As SqlParameter
            Params(0) = New SqlParameter("@userID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(1).Value = BranchID
            Params(2) = New SqlParameter("@EMPDtl", SqlDbType.VarChar, 5000)
            Params(2).Value = dataval.Substring(1)
            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(3).Direction = ParameterDirection.Output
            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(4).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_ESS_FREEZE_REQUEST", Params)
            ErrorFlag = CInt(Params(3).Value)
            Message = CStr(Params(4).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString



    End Sub
#End Region

End Class
