﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CustomerIdentification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        If Not IsPostBack Then
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Try
                'If GF.FormAccess(CInt(Session("UserID")), 2) = False Then
                '    Response.Redirect("~/AccessDenied.aspx", False)
                '    Return
                'End If

                Me.Master.subtitle = "Profile Customer Identification"
                If Not IsPostBack Then
                    
                    'DT = DB.ExecuteDataSet("SELECT CAST(TYPEID AS VARCHAR)+'~'+CAST(ACTYPE_FLAG AS VARCHAR)+'~'+CAST(MIN_CHAR AS VARCHAR)+'~'+CAST(NumericFlag AS VARCHAR)+'~'+CAST(Max_Length AS VARCHAR),TYPENAME FROM CLIENT_SEARCH_TYPE WHERE STATUS_ID=1 ORDER BY TYPEID").Tables(0)
                    'GF.ComboFill(cmbSearchBy, DT, 0, 1)

                End If
                txtName.Focus()
            Catch ex As Exception
                If Response.IsRequestBeingRedirected Then
                    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
                End If
            End Try
        End If
        'cmbSearchBy.Attributes.Add("onchange", "return SearchByOnChange()")
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then

            Dim Name As String = CStr(Data(1))
            Dim Address As String = CStr(Data(2))
            Dim PAN As String = CStr(Data(3))
            Dim Aadhar As String = CStr(Data(4))
            Dim type As Integer = CInt(Data(5))

            Dim sqlStrDecl As String = ""
            Dim SQL As String = ""
            If Name <> "" And type = 1 Then
                sqlStrDecl += " and replace(name,' ','')  like '" & Replace(Name, " ", "") & "%' "

            ElseIf Name <> "" And type = 2 Then
                sqlStrDecl += " and replace(name,' ','')  like '%" & Replace(Name, " ", "") & "' "

            ElseIf Name <> "" And type = 3 Then
                sqlStrDecl += " and replace(name,' ','')  like '%" & Replace(Name, " ", "") & "%' "
            End If
            If Address <> "" Then
                sqlStrDecl += " and  replace(addr1+addr2,' ','') like '%" & Replace(Address, " ", "") & "%' "
            End If
            If PAN <> "" Then
                sqlStrDecl += " and PAN= '" & PAN & "' "
            End If
            If Aadhar <> "" Then
                sqlStrDecl += " and aadhar ='" & Aadhar & "' "
            End If


            Dim sqlStr As String = "select  distinct branchid,branchname,dscid,phone,cif,oldcif,name,dob,PAN,aadhar,addr1+addr2,dscname from SBA where 1=1 "

            SQL = sqlStr + sqlStrDecl + " order by name"
            DT = DB.ExecuteDataSet(SQL).Tables(0)
            CallBackReturn = ""
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + StrConv(DR(1).ToString(), VbStrConv.ProperCase) + "µ" + DR(2).ToString() + "µ" + StrConv(DR(3).ToString(), VbStrConv.ProperCase) + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + StrConv(DR(6).ToString(), VbStrConv.ProperCase) + "µ" + DR(7).ToString() + "µ" + DR(8).ToString() + "µ" + DR(9).ToString() + "µ" + StrConv(DR(10).ToString(), VbStrConv.ProperCase) + "µ" + StrConv(DR(11).ToString(), VbStrConv.ProperCase)
            Next
            ElseIf CInt(Data(0)) = 2 Then
                Dim ClientID As String = Data(1)
                CallBackReturn = ""
                DT = DB.ExecuteDataSet("select  distinct branchid,branchname,dscid,phone,cif,oldcif,name,dob,PAN,aadhar,addr1+addr2,dscname from SBA where cif=" & Data(1).ToString & "").Tables(0)

                CallBackReturn += DT.Rows(0)(0).ToString() + "µ" + StrConv(DT.Rows(0)(1).ToString(), VbStrConv.ProperCase) + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + StrConv(DT.Rows(0)(7).ToString(), VbStrConv.ProperCase) + "µ" + DT.Rows(0)(8).ToString() + "µ" + DT.Rows(0)(9).ToString() + "µ" + DT.Rows(0)(10).ToString() + "µ" + StrConv(DT.Rows(0)(11).ToString().ToString(), VbStrConv.ProperCase) + "Ø"

                DT = DB.ExecuteDataSet("select  branchid,branchname,dscid,phone,cif,oldcif,acno,name,convert(varchar,open_date,106),convert(varchar,closed_date,106),Product,Product_Descr  from CAA where cif='" + ClientID + "' " & _
                            " union all " & _
                            " select  branchid,branchname,dscid,phone,cif,oldcif,acno,name,convert(varchar,open_date,106),convert(varchar,closed_date,106),Product,Product_Descr from SBA  where cif='" + ClientID + "' " & _
                            " union all " & _
                            " select  branchid,branchname,dscid,phone,cif,oldcif,acno,name,convert(varchar,open_date,106),convert(varchar,closed_date,106),Product,Product_Descr from TD  where cif='" + ClientID + "'").Tables(0)
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "¥" + DR(0).ToString() + "µ" + StrConv(DR(1).ToString(), VbStrConv.ProperCase) + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString() + "µ" + StrConv(DR(7).ToString(), VbStrConv.ProperCase) + "µ" + DR(8).ToString() + "µ" + DR(9).ToString() + "µ" + DR(10).ToString() + "µ" + StrConv(DR(11).ToString(), VbStrConv.ProperCase)
                Next


            End If
    End Sub
End Class
