﻿Imports System.Data
Partial Class HREmployeeExit
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DB As New MS_SQL.Connect
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim Leave As New Leave
        If (CInt(Data(0)) = 1) Then
            Dim DT As New DataTable
            Dim GN As New GeneralFunctions
            DT = GN.GetUserDetails(CInt(Data(1))).Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(6).ToString()
            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf (CInt(Data(0)) = 2) Then
            CallBackReturn = EN.HREmployeeExit(CInt(Data(1)), Data(3), CInt(Data(2)), CDate(Data(4)), CInt(Session("UserID")))
        End If
    End Sub
#End Region
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1213) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim DT As New DataTable
            Me.Master.subtitle = "HR Employee Exit"
            DT = DB.ExecuteDataSet("select -1 as Status_ID,' -----Select-----' as Status union all select Status_ID,Status from Emp_Status where status_id <>1 order by 1").Tables(0)
            GN.ComboFill(cmbType, DT, 0, 1)
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- Register Client Side Functions ---//
            txtEmpCode.Attributes.Add("onchange", "EmployeeOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
    End Sub
#End Region
End Class
