﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="SRDepartmentMaster.aspx.vb" Inherits="SRDepartmentMaster" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript"> 

            function window_onload() {
                document.getElementById("rHCode").style.display = "none";
                document.getElementById("rHName").style.display = "none";
                document.getElementById("rOffMail").style.display = "none";
                document.getElementById("rSCode").style.display = "none";
                document.getElementById("rSName").style.display = "none";

                document.getElementById("<%= txtEmpCode.ClientID %>").value="";
                document.getElementById("<%= txtEmpName.ClientID %>").value="";
                document.getElementById("<%= txtMailID.ClientID %>").value="";
                document.getElementById("<%= txtSubCode.ClientID %>").value="";
                document.getElementById("<%= txtSubName.ClientID %>").value=""; 
            }

            function DeptOnChange() {   
                var DeptID = document.getElementById("<%= cmbDept.ClientID %>").value;
                ToServer("1ʘ" + DeptID, 1);  
            }

            function HeadOnChange()
            {
                var HeadCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
                ToServer("4ʘ" + HeadCode, 4);  
            }

            function SubHeadOnChange()
            {
                var HeadCode = document.getElementById("<%= txtSubCode.ClientID %>").value;
                ToServer("4ʘ" + HeadCode, 5);
            }

            function FromServer(Arg, Context) 
            {
                switch(Context)
                {
                    case 1: // Change Of Department
                    {                     
                        if(Arg != "")
                        {
                            document.getElementById("rHCode").style.display = "none";
                            document.getElementById("rHName").style.display = "none";
                            document.getElementById("rOffMail").style.display = "none";
                            document.getElementById("rSCode").style.display = "none";
                            document.getElementById("rSName").style.display = "none";
                            document.getElementById("<%= hdnExistingDtl.ClientID %>").value=Arg;
                            DiplayTable();
                        }
                        else{
                            document.getElementById("rHCode").style.display = "";
                            document.getElementById("rHName").style.display = "";
                            document.getElementById("rOffMail").style.display = "";
                            document.getElementById("rSCode").style.display = "";
                            document.getElementById("rSName").style.display = "";
                            
                            document.getElementById("<%= txtEmpCode.ClientID %>").value="";
                            document.getElementById("<%= txtEmpName.ClientID %>").value="";
                            document.getElementById("<%= txtMailID.ClientID %>").value="";
                            document.getElementById("<%= txtSubCode.ClientID %>").value="";
                            document.getElementById("<%= txtSubName.ClientID %>").value="";

                            document.getElementById("rplnShow").style.display = "none";
                        }
                        break;
                    }
                    case 2:
                    {
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        var DeptID = document.getElementById("<%= cmbDept.ClientID %>").value;
                        ToServer("1ʘ" + DeptID, 1);  
                        break;
                    }
                    case 3:
                    {
                        document.getElementById("rHCode").style.display = "";
                        document.getElementById("rHName").style.display = "";
                        document.getElementById("rOffMail").style.display = "";
                        document.getElementById("rSCode").style.display = "";
                        document.getElementById("rSName").style.display = "";
                        document.getElementById("rplnShow").style.display = "none";
                        var HidArg = Arg.split("Æ");

                        document.getElementById("<%= txtEmpCode.ClientID %>").value=HidArg[0];
                        document.getElementById("<%= txtEmpName.ClientID %>").value=HidArg[1];
                        document.getElementById("<%= txtMailID.ClientID %>").value=HidArg[2];
                        document.getElementById("<%= txtSubCode.ClientID %>").value=HidArg[3];
                        document.getElementById("<%= txtSubName.ClientID %>").value=HidArg[4];   
                        break;                     
                    }
                    case 4:
                    {
                        document.getElementById("<%= txtEmpName.ClientID %>").value=Arg;
                        break;
                    }
                    case 5:
                    {
                        document.getElementById("<%= txtSubName.ClientID %>").value=Arg;
                        break;
                    }
                    case 6:
                    {
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        var DeptID = document.getElementById("<%= cmbDept.ClientID %>").value;
                        ToServer("1ʘ" + DeptID, 1); 
                        break;
                    }
                }
            }

            function DiplayTable() 
            {            
                var Tab = "";
                Tab += "<div style='width:70%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:70%;text-align:center'>DEPARTMENT DETAILS</td></tr>";
                Tab += "</table></div>"; 
                Tab += "<div style='width:70%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
                Tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr style='background-color:#efe1ef;'>";
                Tab += "<td style='width:5%;text-align:center'>SlNo</td>";
                Tab += "<td style='width:10%;text-align:center'>Dept Head EmpCode</td>";
                Tab += "<td style='width:20%;text-align:left'>Dept Head EmpName</td>";
                Tab += "<td style='width:15%;text-align:left'>Official MailID</td>";
                Tab += "<td style='width:10%;text-align:center'>Substitude Head Code</td>";
                Tab += "<td style='width:20%;text-align:left'>Substitude Head Name</td>";
                Tab += "<td style='width:10%;text-align:center'>Delete</td>";
                Tab += "<td style='width:10%;text-align:center'>Edit</td></tr>";
            
                if (document.getElementById("<%= hdnExistingDtl.ClientID %>").value != "") {
                    document.getElementById("rplnShow").style.display = "";
                    var HidArg1 = document.getElementById("<%= hdnExistingDtl.ClientID %>").value.split("Æ");
                    
                    var Total = 0;
                    var row_bg1 = 0;
                    var SlNo = 0;
                  
                    if (row_bg1 == 0) {
                        row_bg1 = 1;
                        Tab += "<tr style='background-color:#FBEFFB;'>";
                    }
                    else {
                        row_bg1 = 0;
                        Tab += "<tr style='background-color:#FFF;'>";
                    }
                    
                    SlNo = SlNo + 1;
                    Tab += "<td style='width:5%;text-align:center'>" + SlNo + "</td>";
                    Tab += "<td style='width:10%;text-align:center'>" + HidArg1[0] + "</td>";
                    Tab += "<td style='width:20%;text-align:left'>" + HidArg1[1] + "</td>";
                    Tab += "<td style='width:15%;text-align:left'>" + HidArg1[2] + "</td>";
                    Tab += "<td style='width:10%;text-align:center'>" + HidArg1[3] + "</td>";
                    Tab += "<td style='width:20%;text-align:left'>" + HidArg1[4] + "</td>";
                    Tab += "<td style='width:10%;text-align:center' onclick=DeleteRowDocument()><img  src='../Image/delete.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";  
                    Tab += "<td style='width:10%;text-align:center' onclick=EditRowDocument()><img  src='../Image/Edit.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Edit'/></td></tr>";                                  
                   
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div></div>";
                    document.getElementById("<%= pnlExistingData.ClientID %>").innerHTML = Tab;
                }
            }

            function DeleteRowDocument() {
                if (confirm("Are you sure to Delete This ") == 1) {
                    var DeptID = document.getElementById("<%= cmbDept.ClientID %>").value;
                    ToServer("2ʘ" + DeptID, 2);  
                }
            }

            function EditRowDocument() {
                var DeptID = document.getElementById("<%= cmbDept.ClientID %>").value;
                ToServer("3ʘ" + DeptID, 3);
            }

            function btnExit_onclick() 
            {
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }

            function btnAdd_onclick() {                
                if (document.getElementById("<%= cmbDept.ClientID %>").value == "-1") 
                {
                    alert("Select Department");
                    document.getElementById("<%= cmbDept.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "") 
                {
                    alert("Enter Department Head Code");
                    document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= txtMailID.ClientID %>").value == "") 
                {
                    alert("Enter Mail ID");
                    document.getElementById("<%= txtMailID.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= txtMailID.ClientID %>").value == "") 
                {
                    alert("Enter Mail ID");
                    document.getElementById("<%= txtMailID.ClientID %>").focus();
                    return false;
                }
                var DeptID = document.getElementById("<%= cmbDept.ClientID %>").value;
                var HeadCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
                var MailID = document.getElementById("<%= txtMailID.ClientID %>").value;
                var SubCode = document.getElementById("<%= txtSubCode.ClientID %>").value;

                if (confirm("Are you sure to Save This ") == 1) {                    
                    ToServer("6ʘ" + DeptID + "ʘ" + HeadCode + "ʘ" + MailID + "ʘ" + SubCode, 6);  
                }
            }

        </script>
    </head>
    </html>
    <br />
    <table class="style1" style="width: 100%">
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Select&nbsp; Department
            </td>
            <td style="width: 63%;">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbDept" class="NormalText" runat="server" Font-Names="Cambria"
                    Width="61%" ForeColor="Black">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="rHCode">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Department Head Code
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Width="30%" 
                    MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr id="rHName">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Department Head Name
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpName" class="ReadOnlyTextBox" ReadOnly="True"
                    runat="server" Width="30%" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr id="rOffMail">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Official Email ID
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtMailID" runat="server" Width="30%" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr id="rSCode">
            <td style="width: 25%; height: 25px;">
            </td>
            <td style="width: 12%; text-align: left; height: 25px;">
                Substitude Head Code
            </td>
            <td style="width: 63%; height: 25px;">
                &nbsp; &nbsp;<asp:TextBox ID="txtSubCode" runat="server" Width="30%" 
                    MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr id="rSName">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Substitude Name
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtSubName" class="ReadOnlyTextBox" ReadOnly="True"
                    runat="server" Width="30%" MaxLength="100"></asp:TextBox>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input id="btnAdd" onclick="return btnAdd_onclick()" 
                    style="font-family: cambria; cursor: pointer; width: 8%; height: 5%;" type="button" 
                    value="SAVE" onclick="return btnAdd_onclick()" /></td>
        </tr>
        <tr id="rplnShow">
            <td colspan="3">
                <asp:Panel ID="pnlExistingData" runat="server" Width="100%">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 63%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 8%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />
            </td>
        </tr>
    </table>
        <asp:HiddenField ID="hdnExistingDtl" runat="server" />
    <br />
    <br />
</asp:Content>
