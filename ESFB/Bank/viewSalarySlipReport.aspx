﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="viewSalarySlipReport.aspx.vb" Inherits="viewSalarySlipReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
</head>
<body style="background-color:Black;">
 <script language="javascript" type="text/javascript">
     function PrintPanel() {
         var panel = document.getElementById("<%=pnDisplay.ClientID %>");
         var printWindow = window.open('', '', 'height=400,width=800');
         printWindow.document.write('<html><head><title>ESAF</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () {
             printWindow.print();
         }, 500);
         return false;
     }
     function Exitform() {
         if (document.getElementById("<%= hdnFormID.ClientID %>").value == "1") {
             window.open("SalrySlipReport.aspx", "_self");
         }
         else {
             window.open("ConsolidatedSalrySlipReport.aspx", "_self");
         }
         return false;
     }
     function table_fill() {
         
         if (document.getElementById("<%= hdnDtls.ClientID %>").value == "") {
             document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none';

         }
         else {
            
                 document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
                 var row_bg = 0;
                 var tab = "";
                
                    
                 col = document.getElementById("<%= hdnDtls.ClientID %>").value.split("~");

                 tab += "<div style='text-align:center;margin:0px auto; width:100%; background-color:white; border:1px solid gray;'><table  style='width:100%;margin: 0px auto;color:Gray;font-family:Cambria;font-size:12px; border-bottom:1px solid gray;' >"
        
                    tab +="<tr> <td style='width:100%;text-align:center;color:Maroon;font-size:18px;font-weight:bold ' colspan='5'>ESAF Swasraya Multistate Agro Co-operative  Society Ltd</td></tr>"
                    tab += "<tr> <td style='width:100%;text-align:center;' colspan='5'><i>Ebenezer Complex, Post Office Road, Mannuthy P O, Thrissur</i></td></tr>"
                    tab += "<tr> <td style='width:100%;text-align:center;font-size:18px;font-weight:bold;color:Black;' colspan='5'>Payslip for the month of &nbsp;" + document.getElementById("<%= hdnMonth.ClientID %>").value + "</td></tr>"
                    tab +="<tr> <td style='width:100%;text-align:center;font-size:18px;font-weight:bold;color:Black;' colspan='5'></td></tr></table>"
                    tab += "<table  style='width:100%;margin: 0px auto;color:Gray;font-family:Cambria;font-size:12px;' ><tr> <td style='width:60%; text-align:left;' colspan='3'>"
                    tab += "<table style='width:100%;text-align:left;position: relative; left: -1px;bottom:-5px;'>"
                    tab +="<tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;Name</td><td style='width:60%;text-align:left;' colspan='2'>"+ col[1] +"</td></tr>"
                    tab += " <tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;Employee Code</td><td style='width:60%;text-align:left;' colspan='2'>" + col[0] + "</td></tr>"
                    tab += "<tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;Designation</td><td style='width:60%;text-align:left;' colspan='2'>" + col[5] + "</td></tr>"
                    tab += "<tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;Department</td><td style='width:60%;text-align:left;' colspan='2'>" + col[3] + "</td></tr>"
                    tab += "<tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;Location</td><td style='width:60%;text-align:left;' colspan='2'>" + col[4] + "</td></tr>"
                    tab += "<tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;Days of Working</td><td style='width:60%;text-align:left;' colspan='2'>" + col[9] + " days</td></tr>"
                    tab +="</table>"
                    tab +="</td>"
                    tab +="<td style='width:40%; text-align:left;' colspan='2'>"
                    tab += "<table style='width:100%;text-align:left;position: relative; left: -12px; bottom:-5px;'>"
                    tab += "<tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;Bank Name</td><td style='width:60%;text-align:left;' colspan='2'>" + col[39] + "</td></tr>"
                    tab += " <tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;Account No</td><td style='width:60%;text-align:left;' colspan='2'>" + col[38] + "</td></tr>"
                    tab += " <tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;EPF No.</td><td style='width:60%;text-align:left;' colspan='2'>" + col[42] + "</td></tr>"
                    tab += " <tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;ESIC No.</td><td style='width:60%;text-align:left;' colspan='2'>" + "" + "</td></tr>"
                    tab += "<tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;PAN No.</td><td style='width:60%;text-align:left;' colspan='2'>" + col[43] + "</td></tr>"
                    tab += "<tr><td style='width:40%;text-align:left;font-size:14px;'>&nbsp; &nbsp;LOP</td><td style='width:60%;text-align:left;' colspan='2'>" + col[10] + " days</td></tr>"
                    tab +="</table>"
                    tab +="</td></tr>"
                    tab +=" <tr> <td style='width:30%; text-align:left;' >"
            tab +=" <table style='width:109%;text-align:left;border-right:1px solid Gray;border-top:1px solid Gray;border-bottom:1px solid Gray;position: relative; left: -5px;'>"
            tab +=" <tr><td style='text-align:left;font-size:14px;border-bottom:1px solid Gray;'>&nbsp; &nbsp;EARNINGS</td></tr>"
            tab +=" <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Basic Pay</td></tr>"
            tab +=" <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Dearness Allowance</td></tr>"
            tab +=" <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;House Rent Allowance</td></tr>"
            tab +=" <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Special  Allowance</td></tr>"
            tab +=" <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Local  Allowance</td></tr>"
            tab +=" <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Medical Allowance</td></tr>"
            tab +=" <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Hospitality Allowance</td></tr>"
            tab +=" <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Performance Allowance</td></tr>"
            tab +=" <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Conveyance</td></tr>"
            tab +=" <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Other Allowance</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Field Allowance</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Metro Allowance</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;border-top:1px solid Gray;font-weight:bold;font-size:15px;'>&nbsp; &nbsp;Total Earnings</td></tr>"
            tab +=" </table></td>"

 tab +=" <td style='width:15%; text-align:left;' >"
            tab +=" <table style='width:106%;text-align:left;border-right:1px solid Gray;border-top:1px solid Gray;border-bottom:1px solid Gray;position: relative; left: -5px;'>"
            tab += " <tr><td style='text-align:center;font-size:14px;border-bottom:1px solid Gray;'>&nbsp; &nbsp;FULL</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[11] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[12] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[13] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[14] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[15] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[16] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[17] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[18] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[19] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[20] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[21] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[46] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;border-top:1px solid Gray;font-weight:bold;font-size:15px;'>&nbsp; &nbsp;"+ col[22] +"</td></tr>"
            tab +=" </table></td>"
  tab +=" <td style='width:15%; text-align:left;' >"
            tab +=" <table style='width:109%;text-align:left;border-right:1px solid Gray;border-top:1px solid Gray;border-bottom:1px solid Gray;position: relative; left: -7px;'>"
            tab += " <tr><td style='text-align:center;font-size:14px;border-bottom:1px solid Gray;'>&nbsp; &nbsp;ACTUAL</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[11] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[12] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[13] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[14] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[15] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[16] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[17] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[18] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[19] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[20] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[21] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[46] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;border-top:1px solid Gray;font-weight:bold;font-size:15px;'>&nbsp; &nbsp;"+ col[22] +"</td></tr>"
            tab +=" </table></td>"

 
  tab +=" <td style='width:25%; text-align:left;' >"
            tab +=" <table style='width:111%;text-align:left;border-right:1px solid Gray;border-top:1px solid Gray;border-bottom:1px solid Gray;position: relative; left: -7px;'>"
            tab += " <tr><td style='text-align:left;font-size:14px;border-bottom:1px solid Gray;'>&nbsp; &nbsp;&nbsp; &nbsp;DEDUCTIONS</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;EPF Contribution</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;ESIC Contribution</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;S & E Scheme</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;ESWT 1%</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;Charity Fund 1%</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;Staff Advance</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;Staff Loan</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;Vehicle Loan</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;Professional Tax</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;ROP</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;Staff  Welfare Fund</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;Medical Insurance</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;Telephone</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;&nbsp; &nbsp;Other Deductions</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;</td></tr>"
            tab += " <tr><td style='text-align:left;font-size:14px;border-top:1px solid Gray;font-weight:bold;font-size:15px;'>&nbsp; &nbsp;&nbsp; &nbsp;Total Deductions</td></tr>"
            tab +=" </table></td>"
   tab +=" <td style='width:15%; text-align:left;' >"
            tab +=" <table style='width:100.7%;text-align:left;border-right:1px solid Gray;border-top:1px solid Gray;border-bottom:1px solid Gray;position: relative; left:4px;'>"
            tab += " <tr><td style=';text-align:center;font-size:14px;border-bottom:1px solid Gray;'>&nbsp; &nbsp;ACTUAL</td></tr>"
            tab +=" <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;"+ col[23] +"</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[24] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[25] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[26] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[27] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[28] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[29] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[30] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[44] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[34] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[31] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[32] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[33] + "</td></tr>"
            tab += " <tr><td style=';text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[45] + "</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;</td></tr>"
            tab += " <tr><td style='text-align:right;font-size:14px;border-top:1px solid Gray;font-weight:bold;font-size:15px;'>&nbsp; &nbsp;"+ col[35] +"</td></tr>"
            tab +=" </table></td>"

            tab += " </tr>" 
            if(col[36]!=0)
                tab += " <tr><td style='text-align:left;font-size:14px;'>&nbsp; &nbsp;Other Pay(Arrear)</td><td style='text-align:right;font-size:14px;'>&nbsp; &nbsp;" + col[36] + "</td></tr>"
            tab += "</table> "
            tab += "<span><b>Net Pay for the month : &nbsp; &nbsp;&nbsp; &nbsp;RS." + col[37] + "</b></span></div>";       
            
                
             
             }
             
             document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;
         
     }


     function PrintPanel() {
         var panel = document.getElementById("<%=pnDisplay.ClientID %>");
         var printWindow = window.open('', '', 'menubar=yes,height=400,width=800');
         printWindow.document.write('<html><head><title>ESAF</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () {
             printWindow.print();
         }, 500);
         return false;
     }
     </script>
    <form id="form1" runat="server">    
    <div style="width:60%; background-color:white; min-height:750px; margin:0px auto;">
        <div style="text-align:right ;margin:0px auto; width:95%; background-color:white; ">
            <asp:HiddenField ID="hdnDtls" runat="server" />
            <asp:HiddenField ID="hdnFormID" runat="server" />
            <asp:ImageButton ID="cmd_Back" style="text-align:left ;float:left; padding-top:5px;" runat="server" Height="35px" Width="35px" ImageAlign="AbsMiddle" ImageUrl="~/Image/back.png" ToolTip="Back"/>
            <asp:HiddenField ID="hdnMonth" runat="server" />
            <asp:ImageButton ID="cmd_Print" style="text-align:right ;" runat="server" Height="40px" Width="40px" ImageAlign="AbsMiddle" ImageUrl="~/Image/print.png" ToolTip="Click to Print"/>
            &nbsp;&nbsp;</div>
        <br style="background-color:white";/>
        <div style="text-align:center;margin:0px auto; width:80%; background-color:white; ">   
            <asp:Panel ID="pnDisplay" runat="server" Font-Names="Cambria,Franklin Gothic Medium" Width="100%">
            </asp:Panel>
        </div>
    </div>
    </form>
</body>
</html>
