﻿Option Strict On
Option Explicit On

Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Configuration

Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq

Imports System.IO
Imports System.Net
Imports System.Web.Services
Imports System.Web.Script.Serialization
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports System
Imports System.Text
Imports System.Data.OleDb



Partial Class ServosysRequestClose_Others
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
    Dim Mobileno As String
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1255) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            '<Summary>Changed by 40020 on 08-Feb-2021</Summary>
            '<Comments>Procedure call added</Comments>
            'DT = DB.ExecuteDataSet("select c.app_request_id,c.app_type_id,j.app_name,c.created_branch_name +'-' +convert(varchar,c.created_branch) createdBranch,d.emp_code, c.Emp_Name,e.App_Type_name,d.Remarks,g.App_role_name,case when d.to_branch=-1 then '' else d.to_branch_name+' - ' + convert(varchar,d.to_branch) + case when y.usb_code is null then '' else', DSC = ' + isnull(y.usb_name+' - '+convert(varchar,y.usb_code),'') end end as To_branch,d.mail_id as email,d.mobile as mobile ,c.department, c.Designation ,k.Order_ID ,c.level_id,K.LEVEL_NAME,c.folder_name,c.storage_in_gb ,case when c.access_type =1 then 'Read' when  c.access_type =2 then 'Write'  when  c.access_type =3 then 'R & W' else '' end  ,RequestMailID,e.app_type_id  ,'HO App Dt :'+convert(varchar,c.ho_approved_on)+' Reporting By : '+isnull(reporting_name,'') +'('+convert(varchar,isnull(reporting_to,''))+') HO:'+isnull(q.name,'')+'('+convert(varchar,isnull(q.emp_code,''))+')' as Approved_By , c.request_no, case when (c.app_type_id=43) then 'Esaf@123'  else '' end as passwd,case when DB_ID=1 then 'ESFB' when DB_ID=2  then 'ESMACO' else 'LBS' end,case when old_branch=-1 then '' else old_branch_name+ ' - ' +(convert(varchar,old_branch)) + case when x.usb_code is null then '' else ', DSC = ' + isnull(x.usb_name+' - '+convert(varchar,x.usb_code),'') end end as oldBranch, old_role_name  as oldRole,''  as TellerType, d.remarks,isnull(x.usb_name+' - '+convert(varchar,x.usb_code),'') MappedBranch,case when c.Handoverto is null or c.Handoverto=0 then '' else c.Handoverto end HandOver from ESFB.dbo.app_level k, ESFB.dbo.app_request_master c  WITH (NOLOCK) left join esfb.dbo.app_level_email q on c.ho_approved_by=q.emp_code left join esfb.dbo.App_Merged_Branch_Master x on c.created_branch=x.retail_brcode, ESFB.dbo.app_master j,ESFB.dbo.app_type_master e , ESFB.dbo.app_dtl_profile d left join ESFB.dbo.app_role_master g on d.Role_id=g.App_role left join esfb.dbo.App_Merged_Branch_Master y on d.to_branch=y.retail_brcode where j.app_id=c.app_id and k.app_id=c.app_id and c.App_type_id=e.App_type_id and c.App_request_id=d.App_request_id  and c.HO_Approved_status =1 and c.level_id=k.level_id  and c.app_id in (11)  and c.closed_status is null order by c.ho_approved_on").Tables(0)
            Dim Parameters1 As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@AppID", 11),
                                                            New System.Data.SqlClient.SqlParameter("@LevelID", 1)}
            DT = DB.ExecuteDataSet("SP_APP_GET_REQUESTS", Parameters1).Tables(0)

            Me.Master.subtitle = "M-BANK Branch Request Closing-Others"

            Dim decryptval As String
            Dim StrAttendance As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                decryptval = GF.Encrypt(DT.Rows(n)(0).ToString)
                StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString & "µ" & DT.Rows(n)(16).ToString & "µ" & DT.Rows(n)(17).ToString & "µ" & DT.Rows(n)(18).ToString & "µ" & DT.Rows(n)(19).ToString & "µ" & DT.Rows(n)(20).ToString & "µ" & DT.Rows(n)(21).ToString & "µ" & DT.Rows(n)(22).ToString & "µ" & DT.Rows(n)(23).ToString & "µ" & DT.Rows(n)(24).ToString & "µ" & DT.Rows(n)(25).ToString & "µ" & DT.Rows(n)(26).ToString & "µ" & DT.Rows(n)(27).ToString & "µ" & DT.Rows(n)(28).ToString & "µ" & DT.Rows(n)(29).ToString & "µ" & decryptval.ToString & "µ" & DT.Rows(n)(30).ToString & "µ" & DT.Rows(n)(31).ToString & "µ" & DT.Rows(n)(32).ToString
                If n < DT.Rows.Count - 1 Then
                    StrAttendance += "¥"
                End If
            Next

            hid_dtls.Value = StrAttendance
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim Mobile As String = CStr(Data(1))
        Dim dataval As String = CStr(Data(2))
        Dim Passwd As String = CStr(Data(3))
        Dim Status As Integer = CInt(Data(4))
        Dim Remarks As String = CStr(Data(5))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim Mobileno As String = ""
        Dim App_Type_ID As Integer = CInt(Data(6))

        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try

            Dim Params(4) As SqlParameter
            Params(0) = New SqlParameter("@userID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(1).Value = BranchID
            Params(2) = New SqlParameter("@EMPDtl", SqlDbType.VarChar)
            Params(2).Value = dataval.Substring(1)
            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(3).Direction = ParameterDirection.Output
            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(4).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_APP_APPROVAL_CLOSE_SERVOSYS", Params)
            ErrorFlag = CInt(Params(3).Value)
            Message = CStr(Params(4).Value)

            Dim RetStr() As String

            Dim MsgId As String

            Dim Count As Integer = 0
            Dim DT As New DataTable
            Dim StrUrl As String

            If ErrorFlag = 0 Then
                Dim SMSFlg As Integer = CInt(GF.GetESFBQueryResult("ESFB011", "").Rows(0)(0))

                If (SMSFlg = 1) Then

                    If Mobile <> "" Then
                        Mobileno = "91" + Mobile.ToString()
                        'DT = DB.ExecuteDataSet("SELECT sender,username,password FROM ESFB.dbo.SMS_PORTAL WHERE STATUS_ID =1").Tables(0)
                        'If DT.Rows.Count > 0 Then
                        '    Dim Sender As String = ""
                        '    Dim UserName As String = ""
                        '    Dim PassWord As String = ""
                        '    Sender = DT.Rows(0).Item(0).ToString()
                        '    UserName = DT.Rows(0).Item(1).ToString()
                        '    PassWord = DT.Rows(0).Item(2).ToString()
                        Dim parameters As String = ""
                        Dim smsId As Integer
                        If Status = 1 And App_Type_ID = 58 Then
                            '  StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "CASA User ID created successfully-Your Login password is Esaf@123  " & Remarks & "&GSM=" & Mobileno & ".Please change the password immediately to avoid blocking."
                            smsId = 57
                            parameters = "Esaf@123^" + Remarks
                        ElseIf Status = 1 And App_Type_ID = 60 Then
                            ' StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "CASA User ID Transferred successfully. " & "&GSM=" & Mobileno & ""
                            smsId = 31
                        ElseIf Status = 1 And App_Type_ID = 59 Then
                            'StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "CASA User ID Disabled successfully.  " & "&GSM=" & Mobileno & ""
                            smsId = 39
                        ElseIf Status = 2 And App_Type_ID = 58 Then
                            'StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "CASA New User ID request is rejected. " & Remarks & "&GSM=" & Mobileno & ""
                            smsId = 46
                        ElseIf Status = 2 And App_Type_ID = 60 Then
                            'StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "CASA User ID Transfer request is rejected. " & "&GSM=" & Mobileno & ""
                            smsId = 53
                        ElseIf Status = 2 And App_Type_ID = 59 Then
                            'StrUrl = "https://api.infobip.com/api/v3/sendsms/plain?user=" & UserName & "&password=" & PassWord & "&sender=" & Sender & "&SMSText=" & "CASA User ID Disabled  request is rejected.  " & "&GSM=" & Mobileno & ""
                            smsId = 42
                        End If

                        'RetStr = WEB_Request_Response(StrUrl, 1)
                        '//----------------New SMS-----------------//

                        Dim otpStat As Integer = 0
                        RetStr = GF.WEB_Request_Response(Mobileno, parameters, smsId, otpStat)
                        '//----------------End of New SMS----------//
                        ' End If
                    End If

                End If

            End If
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString



    End Sub
#End Region
#Region "Function"

    Public Shared Function WEB_Request_Response(Request As String, Request_type As Integer) As String()

        'Added On 22-10-2020 by 40013 - Changed request from IT
        Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
        Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString

        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        If Request_type = 1 Then
            delimiters = {"<", ">", ">/", "</", "/"}
        Else
            delimiters = {"=", " "}
        End If
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)


        Return parts.ToArray()


    End Function
#End Region

   
    Protected Sub btnDownL_Click(sender As Object, e As EventArgs) Handles btnDownL.Click
        Try
            DT = GF.GetQueryResult("select ROW_NUMBER() OVER(ORDER BY a.app_request_id ASC) AS SRNO,a.emp_code USERNAME, a.emp_name NTUSERNAME, a.created_branch [MF OFFICEID], a.created_branch_name [MF OFFICENAME],c.app_role_name [MF ROLE], '' as [CASA OFFICEID], '' as [CASA OFFICENAME], '' as [CASA ROLE] from app_request_master a, app_dtl_profile b left join app_role_master c on b.role_id=c.app_role where a.app_id=11 and a.app_type_id=58 and closed_status is null and a.HO_Approved_status=1 and a.app_request_id=b.app_request_id")

            WebTools.ExporttoExcel(DT, "SERVOSYS_MB_" & CStr(Now.Date()))
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
End Class
