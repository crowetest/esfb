﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ChangeSignature.aspx.vb" Inherits="ChangeSignature" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
    <script src="../../Script/Calculations.js" language="javascript" type="text/javascript"> </script> 
  <script language="javascript" type="text/javascript">
      function btnExit_onclick() 
      {
          window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
      }
      function EmployeeOnChange() 
      {
         
          if (document.getElementById("<%= cmbBranch.ClientID%>").value != "-1" && document.getElementById("<%= cmbPost.ClientID%>").value!="-1")
          {
              var EmpCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
              document.getElementById("<%= lblPrevBranch.ClientID%>").textContent = "";
          if (EmpCode != "")
              ToServer("1Ø" + EmpCode + "Ø" + document.getElementById("<%= cmbBranch.ClientID%>").value + "Ø" + document.getElementById("<%= cmbPost.ClientID%>").value, 1);

          }
          else {
              alert("Select Branch");
              return false;
          }
      }
      
      function PostOnChange() {
          if ( document.getElementById("<%= cmbBranch.ClientID%>").value != "-1")
          {
              cleardata();
              var PostID = document.getElementById("<%= cmbPost.ClientID%>").value;
              document.getElementById("<%= cmbNewPost.ClientID%>").value = document.getElementById("<%= cmbPost.ClientID%>").value;
              document.getElementById("<%= hdn_Post.ClientID%>").value = PostID;
              document.getElementById("<%= hid_Dtls.ClientID%>").value = "";
              document.getElementById("<%= lblPrevBranch.ClientID%>").textContent = "";
              table_fill();
              if (PostID != "")
                  ToServer("2Ø" + PostID + "Ø" + document.getElementById("<%= cmbBranch.ClientID%>").value, 2);
          }
          else

          {
              alert("Select Branch");
              return false;
          }
       }
      function cleardata() {
        
          document.getElementById("<%= txtName.ClientID %>").value = "";
          document.getElementById("<%= txtEmpCode.ClientID %>").value = "";
          
          document.getElementById("<%= txtDepartment.ClientID %>").value = "";
          document.getElementById("<%= txtDesignation.ClientID %>").value = "";
          document.getElementById("<%= txtDateOfJoin.ClientID %>").value = "";
          document.getElementById("<%= txtEmpCode.ClientID %>").value = "";
          document.getElementById("<%= hdn_TranID.ClientID%>").value = "";
          document.getElementById("ImgPhoto").setAttribute('src', "");
          document.getElementById("<%= txtEmpCode.ClientID %>").disabled = false;
          document.getElementById("Transfer").style.display = "none";
          document.getElementById("Post").style.display = "none";
          document.getElementById("Type").style.display = "none";
          document.getElementById("Descr").style.display = "none";
          document.getElementById("ExistStaff").style.display = "none";
          document.getElementById("Img").style.display = "";
      
         
      }
      function TypeOnChange() {
          document.getElementById("<%= hid_Dtls.ClientID%>").value = "";
          document.getElementById("<%= lblPrevBranch.ClientID%>").textContent = "";
          table_fill();
          if (document.getElementById("<%= cmbType.ClientID%>").value == 2) {
              document.getElementById("Transfer").style.display = "";
              document.getElementById("Post").style.display = "";
              document.getElementById("Descr").style.display = "";
              document.getElementById("Img").style.display = "";
              document.getElementById("ExistStaff").style.display = "";
              ToServer("6Ø" + document.getElementById("<%= cmbBranch.ClientID%>").value, 6);
           }
           else {
              document.getElementById("Transfer").style.display = "none";
              document.getElementById("Post").style.display = "none";
              document.getElementById("Descr").style.display = "";
              document.getElementById("Img").style.display = "none";
              document.getElementById("ExistStaff").style.display = "none";
           }
       }
      function FromServer(Arg, Context) {
         
          switch (Context) {
            
              case 1:
                  {
                      cleardata();
                      var Data1 = Arg.split("|");
                      if (Data1[0] == "ØØ") {
                          alert("Invalid Employee Code"); document.getElementById("<%= txtName.ClientID %>").value = "";
                          document.getElementById("<%= txtEmpCode.ClientID %>").value = "";
                          
                          document.getElementById("<%= txtDepartment.ClientID %>").value = "";
                          document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                          document.getElementById("<%= txtDateOfJoin.ClientID %>").value = "";
                          document.getElementById("<%= txtEmpCode.ClientID %>").value = "";
                          document.getElementById("ImgPhoto").setAttribute('src', "");
                          document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                          document.getElementById("<%= hid_Dtls.ClientID%>").value = Data1[1];
                          document.getElementById("<%= hdn_activestaff.ClientID%>").value = 0;
                          table_fill();
                          return false;
                      }
                      var Data = Data1[0].split("Ø");
                      document.getElementById("<%= txtName.ClientID %>").value = Data[1];
                      document.getElementById("<%= txtEmpCode.ClientID %>").value = Data[0];
                      document.getElementById("<%= txtDepartment.ClientID %>").value = Data[2];
                      document.getElementById("<%= txtDesignation.ClientID %>").value = Data[3];
                      document.getElementById("<%= txtDateOfJoin.ClientID %>").value = Data[4];
                      document.getElementById("<%= hdn_TranID.ClientID%>").value = "";
                      if (Data[5] != "") {
                          document.getElementById("<%= lblPrevBranch.ClientID%>").textContent = "current Branch " + Data[5];
                      }
                      else
                          document.getElementById("<%= lblPrevBranch.ClientID%>").textContent = "";

                      document.getElementById("<%= hdn_Image.ClientID%>").value = (Data[7] != "" ? 1 : 0);
                      document.getElementById("ImgPhoto").setAttribute('src', Data[7]);
                      document.getElementById("<%= hid_Dtls.ClientID%>").value = Data1[1];
                      document.getElementById("Transfer").style.display = "none";
                      document.getElementById("Post").style.display = "none";
                      document.getElementById("Type").style.display = "none";
                      
                      
                      var Datanew = Data1[1].split("µ");
                      document.getElementById("<%= hdn_activestaff.ClientID%>").value = ((Data[6] != "" && Datanew[6] == 1) ? 1 : 0);
                      table_fill();
                      break;
                  }
              case 2:
                  {
                      cleardata();
                      var Data1 = Arg.split("|");
                      if (Data1[0] == "ØØ") {
                          document.getElementById("<%= txtName.ClientID %>").value = "";
                          document.getElementById("<%= txtEmpCode.ClientID %>").value = "";
                          document.getElementById("<%= txtDepartment.ClientID %>").value = "";
                          document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                          document.getElementById("<%= txtDateOfJoin.ClientID %>").value = "";
                          document.getElementById("<%= txtEmpCode.ClientID %>").value = "";
                          document.getElementById("ImgPhoto").setAttribute('src', "");
                          document.getElementById("<%= hdn_activestaff.ClientID%>").value = 0;
                          document.getElementById("<%= txtEmpCode.ClientID %>").disabled = false;
                          document.getElementById("<%= txtEmpCode.ClientID %>").disabled = false;
                          document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                          document.getElementById("<%= hid_Dtls.ClientID%>").value = Data1[1];

                          table_fill();
                          return false;
                      }
                      var Data = Data1[0].split("Ø");
                      document.getElementById("<%= txtEmpCode.ClientID %>").disabled = true;
                      document.getElementById("<%= txtEmpCode.ClientID%>").value = Data[0];
                      document.getElementById("<%= txtName.ClientID %>").value = Data[1];
                      document.getElementById("<%= txtDepartment.ClientID %>").value = Data[2];
                      document.getElementById("<%= txtDesignation.ClientID %>").value = Data[3];
                      document.getElementById("<%= txtDateOfJoin.ClientID %>").value = Data[4];
                      document.getElementById("<%= lblPrevBranch.ClientID%>").value = " ";
                      document.getElementById("ImgPhoto").setAttribute('src', Data[6]);
                      document.getElementById("<%= hdn_Image.ClientID%>").value = (Data[6] != "" ? 1 : 0);
                      document.getElementById("<%= txtEmpCode.ClientID %>").disabled = true;
                      document.getElementById("<%= hid_Dtls.ClientID%>").value = Data1[1];
                      var Datanew = Data1[0].split("µ");
                      document.getElementById("<%= hdn_TranID.ClientID%>").value = Data[5];
                      document.getElementById("Type").style.display = "";
                      document.getElementById("<%= hdn_activestaff.ClientID%>").value = ((Data[5] != "" && Datanew[6] == 1) ? 1 : 0);
                      table_fill();
                      break;
                  }
              
              case 3:
                  {
                      
                      if (Arg == "ØØ") {
                          document.getElementById("<%= txtState.ClientID%>").value = "";
                          document.getElementById("<%= txtDistrict.ClientID%>").value = "";
                          return false;
                      }
                      var Data = Arg.split("Ø");
                      document.getElementById("<%= txtState.ClientID%>").value = Data[1];
                      document.getElementById("<%= txtDistrict.ClientID%>").value = Data[2];
                     
                      break;
                  }
              case 5:
                  {

                      if (Arg == "ØØ") {
                          document.getElementById("<%= txtNewExistStaff.ClientID%>").value = "Not Assigned";
                          
                          return false;
                      }
                     
                      document.getElementById("<%= txtNewExistStaff.ClientID%>").value = Arg;
                      

                      break;
                  }
              case 6:
                  {

                     
                      ComboFill(Arg, "<%= cmbNewBranch.ClientID%>");
                      break;
                  }
          }

      }
      function ComboFill(data, ddlName) {
          document.getElementById(ddlName).options.length = 0;
          var rows = data.split("Ñ");
          for (a = 1; a < rows.length; a++) {
              var cols = rows[a].split("ÿ");
              var option1 = document.createElement("OPTION");
              option1.value = cols[0];
              option1.text = cols[1];
              document.getElementById(ddlName).add(option1);
          }
      }
      function ClearCombo(control) {
          document.getElementById(control).options.length = 0;
          var option1 = document.createElement("OPTION");
          option1.value = -1;
          option1.text = " -----Select-----";
          document.getElementById(control).add(option1);
      }
      function BranchOnChange() {
          cleardata();
          document.getElementById("<%= hid_Dtls.ClientID%>").value = "";

          table_fill();
          if (document.getElementById("<%= cmbBranch.ClientID%>").value != "-1") {
              document.getElementById("<%= cmbPost.ClientID%>").value = "-1";
              document.getElementById("<%= txtEmpCode.ClientID %>").value = "";
              var BranchID = document.getElementById("<%= cmbBranch.ClientID%>").value;
              document.getElementById("<%= hdn_Branch.ClientID%>").value = BranchID;
              if (BranchID != "")
                  ToServer("3Ø" + BranchID,3);
          }
      }
      function NewBranchOnChange() {
          
          if (document.getElementById("<%= cmbNewBranch.ClientID%>").value != "-1") {
             
              var BranchID = document.getElementById("<%= cmbNewBranch.ClientID%>").value;
              
              if (BranchID != "")
                  ToServer("5Ø" + BranchID + "Ø" + document.getElementById("<%= cmbPost.ClientID%>").value, 5);
          }
      }
      function RequestOnClick() {
          document.getElementById("<%= hdn_NewBranch.ClientID%>").value = 0;
          document.getElementById("<%= hdn_NewPost.ClientID%>").value = 0;
          if (document.getElementById("<%= cmbBranch.ClientID %>").value == "-1") {
         alert("Select Branch");
         document.getElementById("<%= cmbBranch.ClientID %>").focus();
            return false;
          }
          if (document.getElementById("<%= cmbPost.ClientID%>").value == "-1") {
              alert("Select Post");
              document.getElementById("<%= cmbPost.ClientID%>").focus();
         return false;
        }
          if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "") {

              alert("Enter Employee Code");
              document.getElementById("<%= txtEmpCode.ClientID%>").focus();
              return false;
          }
          if (document.getElementById("<%= fup1.ClientID%>").value == "" && document.getElementById("<%= hdn_Image.ClientID%>").value == 0 && document.getElementById("<%= cmbType.ClientID%>").value != "3") {
              alert("Attach Signature"); document.getElementById("<%= fup1.ClientID%>").focus(); return false;
          }
          if (document.getElementById("<%= hid_Dtls.ClientID%>").value != "") {
              if (document.getElementById("<%= cmbType.ClientID%>").value == "-1" && document.getElementById("<%= hdn_activestaff.ClientID%>").value == 1) {
                    alert("Select Type");
                    document.getElementById("<%= cmbType.ClientID%>").focus();
                    return false;
              }
              if (document.getElementById("<%= cmbType.ClientID%>").value == "2" && document.getElementById("<%= cmbNewBranch.ClientID%>").value == "-1") {
                    alert("Select To Be Transfer Branch");
                    document.getElementById("<%= cmbNewBranch.ClientID%>").focus();
                    return false;
              }
              if (document.getElementById("<%= cmbType.ClientID%>").value == "2" && document.getElementById("<%= cmbNewPost.ClientID%>").value == "-1") {
                  alert("Select the new post");
                  document.getElementById("<%= cmbNewPost.ClientID%>").focus();
                    return false;
                }
              if (document.getElementById("<%= cmbType.ClientID%>").value == "3" && document.getElementById("txtDetails").value == "") {
                  alert("Enter the Reason");
                  document.getElementById("txtDetails").focus();
                  return false;
              }
              else
              {
                  document.getElementById("<%= hid_Details.ClientID%>").value = document.getElementById("txtDetails").value;
              }
             
          }
          document.getElementById("<%= hdn_NewBranch.ClientID%>").value = (document.getElementById("<%= cmbNewBranch.ClientID%>").value == "-1" ? document.getElementById("<%= cmbBranch.ClientID%>").value : document.getElementById("<%= cmbNewBranch.ClientID%>").value);
          document.getElementById("<%= hdn_NewPost.ClientID%>").value = (document.getElementById("<%= cmbNewPost.ClientID%>").value == "-1" ? document.getElementById("<%= cmbPost.ClientID%>").value : document.getElementById("<%= cmbNewPost.ClientID%>").value); 
          document.getElementById("<%= hdn_Empcode.ClientID%>").value = document.getElementById("<%= txtEmpCode.ClientID%>").value;
      }
     // function delete_row(Roleid,Emp_Code,POSTID) {
     //     if (confirm("Are You sure to Edit?") == 1) {
     //         window.open("ChangeSignature_Status.aspx?TranID=" + btoa(Roleid) + " &Emp_Code=" + Emp_Code + " &PostID=" + POSTID, "_self");
     //    }

     //}
      function table_fill() {
          
          if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
              document.getElementById("<%= pnHistory.ClientID %>").style.display = 'none';
          }
          else {
              document.getElementById("<%= pnHistory.ClientID %>").style.display = '';
              var row_bg = 0;
              var tab = "";
              tab += "<div style='width:100%; background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto; align=center;color:#FFF;font-family:cambria; font-size:10pt;'>HISTORY</div>"
              tab += "<div style='width:100%; height:170px;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#FFF;' align='center'>";
              tab += "<tr class=mainhead>";
              tab += "<td style='width:5%;text-align:center'>No</td>";
              tab += "<td style='width:10%;text-align:left' >Branch</td>";
              tab += "<td style='width:10%;text-align:left;'>Emp_code</td>";
              tab += "<td style='width:20%;text-align:left'>Emp Name</td>";
              tab += "<td style='width:50%;text-align:left'>Designation</td>";
              tab += "<td style='width:5%;text-align:left'>Status</td>";
              tab += "</tr>";

              row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

              for (n = 0; n <= row.length - 1; n++) {
                  col = row[n].split("µ");
                  if (document.getElementById("<%= hid_Dtls.ClientID %>").value != "") {
                      if (row_bg == 0) {
                          row_bg = 1;
                          tab += "<tr class=sub_first>";
                      }
                      else {
                          row_bg = 0;
                          tab += "<tr class=sub_second>";
                      }
                      i = n + 1;

                      
                      tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                      tab += "<td style='width:10%;text-align:left' >" + col[1] + "</td>";
                      tab += "<td style='width:20%;text-align:left' >" + col[2] + "</td>";
                      tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>";
                      tab += "<td style='width:50%;text-align:left'>" + col[4] + "</td>";
                      tab += "<td style='width:5%;text-align:center' >" + col[5] + "</td>";
                      tab += "</tr>";
                  }
              }
              tab += "</table><div><div>";
            
              document.getElementById("<%= pnHistory.ClientID %>").innerHTML = tab;

              //--------------------- Clearing Data ------------------------

          }
        }

// ]]>
  </script><br />
    <table align="center" style="width:80%;margin: 0px auto;">
         <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                Branch</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbBranch" runat="server" class="NormalText" Font-Names="Cambria" ForeColor="Black" Style="text-align: left;" Width="50%">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                State
                </td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtState" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                District
                </td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtDistrict" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
                Post</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbPost" runat="server" class="NormalText" Font-Names="Cambria" ForeColor="Black" Style="text-align: left;" Width="50%">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"><asp:HiddenField 
                    ID="hdn_Empcode" runat="server" />
                </td>
            <td style="width:12% ; text-align:left;">
                Employee Code&nbsp;
                </td>
            <td style="width:63% ;text-align:left;">    
                &nbsp;&nbsp;<asp:TextBox ID="txtEmpCode" runat="server" Width="20%" class="NormalText" 
                    MaxLength="5" onkeypress="NumericCheck(event)" ></asp:TextBox>
                <img id="ImgPhoto" alt="" src="" style="height:50px; width:100px;" title="" />
                <asp:Label ID="lblPrevBranch" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
        <td style="width:25%;"><asp:HiddenField 
                    ID="hdn_TranID" runat="server" />
                </td>
            <td style="width:12% ; text-align:left;">
                Name
                </td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtName" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
       
        <tr>
        <td style="width:25%;">
                   <asp:HiddenField ID="hid_Dtls" runat="server" />
                   </td>
            <td style="width:12% ; text-align:left;">
                Department</td>
            <td style="width:63% ;text-align:left;">
               &nbsp;&nbsp;<asp:TextBox ID="txtDepartment" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"><asp:HiddenField 
                    ID="hdn_Image" runat="server" />
                </td>
            <td style="width:12% ; text-align:left;">
                Designation</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtDesignation" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
        <tr>
        <td style="width:25%;"><asp:HiddenField 
                    ID="hdn_Branch" runat="server" />
                </td>
            <td style="width:12% ; text-align:left;">
                Date of Join</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtDateOfJoin" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
       <tr id="Type" style="display:none;">
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
               
                Type</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbType" runat="server" class="NormalText" Font-Names="Cambria" ForeColor="Black" Style="text-align: left;" Width="50%">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="2"> Transfer </asp:ListItem>
                    <asp:ListItem Value="3"> Suspended </asp:ListItem>
                </asp:DropDownList>
                </td>
        </tr>
        <tr id="Transfer" style="display:none;">
        <td style="width:25%;">
                <asp:HiddenField ID="hid_Details" runat="server" />
            </td>
            <td style="width:12% ; text-align:left;">
                Trasfer Branch</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbNewBranch" runat="server" class="NormalText" Font-Names="Cambria" ForeColor="Black" Style="text-align: left;" Width="50%">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
                </td>
        </tr>
         <tr id="ExistStaff">
        <td style="width:25%;">
                <asp:HiddenField 
                    ID="hdn_activestaff" runat="server" />
                </td>
            <td style="width:12% ; text-align:left;">
               Current Staff
                </td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtNewExistStaff" runat="server" Width="60%" class="ReadOnlyTextBox" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
          <tr id="Post">
        <td style="width:25%;"></td>
            <td style="width:12% ; text-align:left;">
                
                New Post</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbNewPost" runat="server" class="NormalText" Font-Names="Cambria" ForeColor="Black" Style="text-align: left;" Width="50%">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                   </asp:DropDownList>
                </td>
        </tr>
         <tr id="Descr" style="display:none;">
            <td style="width: 25%;">
                <asp:HiddenField ID="hdn_NewBranch" runat="server" />
                <asp:HiddenField ID="hdn_NewPost" runat="server" />
            </td>
            <td style="width: 12%; text-align: left;">
                Description
            </td>
            <td style="width: 63%">
                &nbsp;&nbsp;<textarea id="txtDetails" class="NormalText" cols="20" name="S1" rows="3" maxlength="1000" style="width: 70%" onkeypress='return TextAreaCheck(event)' ></textarea>
            </td>
        </tr>
        <tr id="Img">
        <td style="width:25%;"><asp:HiddenField 
                    ID="hdn_Post" runat="server" />
                </td>
            <td style="width:12% ; text-align:left;">
                Select Signature</td>
            <td style="width:63% ;text-align:left;">
                &nbsp;&nbsp;<input id="fup1" type="file" runat="server" 
                style="font-family: Cambria; font-size: 10.5pt" />
                </td>

        </tr>
        
        <tr>
       <td style="width:100%;" colspan="3">
       <asp:Panel ID="pnHistory" runat="server">
            </asp:Panel>
       </td>
       </tr>
        <tr>
        
         <td style="text-align:center;" colspan="3"> <br /><br /> <asp:Button 
                ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" style="cursor:pointer;"
                 Width="10%" /> &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
        
    </table>
  <br /><br />
</asp:Content>

