﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Partial Class UploadBranchUser
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim DTExcel As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim values As New DataTable
#Region "Page Load & Dispose"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "file Uploading"
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


    End Sub


    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
    Sub DeleteFilesFromFolder(ByVal Folder As String)
        If Directory.Exists(Folder) Then
            For Each _file As String In Directory.GetFiles(Folder)
                File.Delete(_file)
            Next
            For Each _folder As String In Directory.GetDirectories(Folder)

                DeleteFilesFromFolder(_folder)
            Next

        End If

    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
    End Sub
#End Region

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpload.Click
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim today As Date = DateTime.Now
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim msg As String = ""
        Dim connectionString As String = ""

        If (FileUpload1.PostedFile.FileName <> "") Then

            Dim fileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)



            Dim fileExtension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)
            Dim fileType As String = Path.GetExtension(FileUpload1.PostedFile.ContentType)
            Dim fileSize As String = ""
            Dim fileCreatedDate As String = ""

            'Dim filesize As Integer = Path.GetExtension(FileUpload1.PostedFile.ContentLength)
            Dim fileLocation As String = Server.MapPath("../" & fileName)

            DeleteFilesFromFolder(fileLocation)
            FileUpload1.SaveAs(fileLocation)

            Dim infoReader As System.IO.FileInfo
            infoReader = My.Computer.FileSystem.GetFileInfo(fileLocation)
            'MsgBox("File was created on " & infoReader.CreationTime)

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            'con.Open()

            Dim dt As New DataTable()
            Dim col As New DataColumn("test")
            col.DataType = System.Type.[GetType]("System.String")
            dt.Columns.Add(col)

            Dim aa As String() = File.ReadAllLines(fileLocation)
            For Each item As String In aa
                Dim dr As DataRow = dt.NewRow()
                dr(0) = item.ToString()
                dt.Rows.Add(dr)
            Next
            Dim StrCAFDetails As String = ""

            Dim count As Integer
            Dim val As Integer

            values.Columns.Add("User_ID", GetType(String), Nothing)  '0
            values.Columns.Add("User_Code", GetType(String), Nothing)  '1
            values.Columns.Add("User_Name", GetType(String), Nothing) '2
            values.Columns.Add("User_Class", GetType(String), Nothing) '3   
            values.Columns.Add("Cashier", GetType(String), Nothing) '4
            values.Columns.Add("HeadTeller", GetType(String), Nothing) '5
            values.Columns.Add("TellerID", GetType(String), Nothing) '6
            values.Columns.Add("Branch_Code", GetType(String), Nothing) '7
            values.Columns.Add("Service_Centre_ID", GetType(String), Nothing) '8
            values.Columns.Add("Last_Signed_On", GetType(String), Nothing)  '9
            values.Columns.Add("UserStatus", GetType(String), Nothing)  '10
            values.Columns.Add("Description", GetType(String), Nothing)  '11
            values.Columns.Add("Manual_Revoke_Status", GetType(String), Nothing) '12
            values.Columns.Add("Manual_Status", GetType(String), Nothing) '13
            values.Columns.Add("Revoke_Reason", GetType(String), Nothing) '14
            values.Columns.Add("RDescription", GetType(String), Nothing) '15
            values.Columns.Add("EnterBy", GetType(String), Nothing)
            values.Columns.Add("EnterDate", GetType(String), Nothing)

            Dim lent As Integer
            Dim LastVal As Integer
            Dim PrePos As Integer
            Dim BalanceLen As Integer
            For count = 1 To dt.Rows.Count - 1

                Dim row = values.NewRow()
                If Len(dt.Rows(count)(0).ToString) >= 126 Then
                    Dim test As String = (Replace(dt.Rows(count)(0).ToString, " ", ""))

                    If Len(Replace(dt.Rows(count)(0).ToString, " ", "")) > 7 Then
                        If Replace(dt.Rows(count)(0).ToString.Substring(0, 4), " ", "") <> "" And Replace(dt.Rows(count)(0).ToString.Substring(0, 4), " ", "") <> "====" And Replace(dt.Rows(count)(0).ToString.Substring(0, 10), " ", "") <> "ESAFSmall" And dt.Rows(count)(0).ToString.Substring(0, 6) <> "Branch" And dt.Rows(count)(0).ToString.Substring(0, 4) <> "User" And dt.Rows(count)(0).ToString.Substring(0, 2) <> "ID" And dt.Rows(count)(0).ToString.Substring(0, 6) <> "      " Then

                            'Length of String in Each Row
                            lent = Len(dt.Rows(count)(0).ToString)

                            If lent <= 18 Then '51
                                row(0) = ""
                            Else
                                Dim r1 As String = Replace(dt.Rows(count)(0).ToString.Substring(0, 18), " ", "")
                                row(0) = Replace(dt.Rows(count)(0).ToString.Substring(0, 18), " ", "") 'User_ID
                            End If

                            row(1) = "0" 'UserCode

                            If lent <= 18 Then '18
                                row(2) = ""
                            Else
                                Dim r2 As String = Replace(dt.Rows(count)(0).ToString.Substring(18, 32), " ", "")
                                row(2) = Replace(dt.Rows(count)(0).ToString.Substring(18, 32), " ", "")  'User_Name
                            End If


                            If lent <= 50 Then '50
                                row(3) = ""
                            Else
                                LastVal = lent - 50
                                Dim r3 As String = Replace(dt.Rows(count)(0).ToString.Substring(50, 16), " ", "")
                                row(3) = Replace(dt.Rows(count)(0).ToString.Substring(50, 16), " ", "")  'User_Class
                            End If

                            If lent <= 66 Then
                                row(4) = ""
                            Else
                                LastVal = lent - 63
                                Dim r4 As String = Replace(dt.Rows(count)(0).ToString.Substring(66, 8), " ", "")
                                row(4) = Replace(dt.Rows(count)(0).ToString.Substring(66, 8), " ", "")  'Cashier
                            End If

                            If lent <= 74 Then
                                row(5) = ""
                            Else
                                LastVal = lent - 72
                                Dim r5 As String = Replace(dt.Rows(count)(0).ToString.Substring(74, 8), " ", "")
                                row(5) = Replace(dt.Rows(count)(0).ToString.Substring(74, 8), " ", "")  'HeadTeller
                            End If

                            If lent <= 82 Then
                                row(6) = ""
                            Else
                                LastVal = lent - 80
                                Dim r6 As String = Replace(dt.Rows(count)(0).ToString.Substring(82, 19), " ", "")
                                row(6) = Replace(dt.Rows(count)(0).ToString.Substring(82, 19), " ", "")  'TellerID
                            End If

                        If lent <= 101 Then
                            row(7) = ""
                        Else
                            LastVal = lent - 100
                                Dim r7 As String = Replace(dt.Rows(count)(0).ToString.Substring(101, 15), " ", "")
                                row(7) = Replace(dt.Rows(count)(0).ToString.Substring(101, 15), " ", "")  'Branch_Code
                        End If

                            If lent <= 116 Then
                                row(8) = "0"
                            Else
                                LastVal = lent - 113
                                Dim r8 As String = Replace(dt.Rows(count)(0).ToString.Substring(116, 22), " ", "")
                                If (r8 = "") Then
                                    row(8) = "0"
                                Else
                                    row(8) = Replace(dt.Rows(count)(0).ToString.Substring(116, 22), " ", "")  'Service_Centre_ID
                                End If
                            End If

                            If lent <= 138 Then
                                row(9) = ""
                            Else
                                LastVal = lent - 138
                                Dim r9 As String = Replace(dt.Rows(count)(0).ToString.Substring(138, 23), " ", "")
                                If (r9 = "") Then
                                    row(9) = "01/Jan/1900"
                                Else
                                    row(9) = CDate(Replace(dt.Rows(count)(0).ToString.Substring(138, 23), " ", "")).ToString("dd/MMM/yyyy")  'Last_Signed_On
                                End If

                            End If

                            If lent <= 161 Then
                                row(10) = ""
                            Else
                                LastVal = lent - 156
                                Dim r10 As String = Replace(dt.Rows(count)(0).ToString.Substring(161, 5), " ", "")
                                If (r10 = "") Then
                                    row(10) = "0"
                                Else
                                    row(10) = Replace(dt.Rows(count)(0).ToString.Substring(161, 5), " ", "") 'UserStatus
                                End If

                            End If

                            If lent <= 166 Then
                                row(11) = ""
                            Else
                                If (lent < 187) Then
                                    LastVal = lent - 166
                                Else
                                    LastVal = 21
                                End If
                                Dim r11 As String = Replace(dt.Rows(count)(0).ToString.Substring(166, LastVal), " ", "")
                                row(11) = Replace(dt.Rows(count)(0).ToString.Substring(166, LastVal), " ", "")  'Description
                            End If

                            If lent <= 187 Then
                                row(12) = ""
                            Else
                                If (lent < 203) Then
                                    LastVal = lent - 187
                                Else
                                    LastVal = 16
                                End If
                                Dim r12 As String = Replace(dt.Rows(count)(0).ToString.Substring(187, LastVal), " ", "")
                                row(12) = Replace(dt.Rows(count)(0).ToString.Substring(187, LastVal), " ", "")  'Manual_Revoke_Status

                            End If

                            If lent <= 203 Then
                                row(13) = ""
                            Else
                                If (lent < 206) Then
                                    LastVal = lent - 203
                                Else
                                    LastVal = 3
                                End If
                                Dim r13 As String = Replace(dt.Rows(count)(0).ToString.Substring(203, LastVal), " ", "")
                                If (r13 = "") Then
                                    row(13) = "0"
                                Else
                                    row(13) = Replace(dt.Rows(count)(0).ToString.Substring(203, LastVal), " ", "")  'Manual_Status
                                End If
                            End If

                            If lent <= 206 Then
                                row(14) = ""
                            Else
                                If (lent < 215) Then
                                    LastVal = lent - 206
                                Else
                                    LastVal = 9
                                End If

                                Dim r14 As String = Replace(dt.Rows(count)(0).ToString.Substring(206, LastVal), " ", "")
                                row(14) = Replace(dt.Rows(count)(0).ToString.Substring(206, LastVal), " ", "")  'Revoke_Reason
                            End If

                            If lent <= 215 Then '215
                                row(15) = ""  'RDescription                                       
                            Else
                                LastVal = lent - 215
                                Dim r15 As String = Replace(dt.Rows(count)(0).ToString.Substring(215, LastVal), " ", "")
                                row(15) = Replace(dt.Rows(count)(0).ToString.Substring(214, LastVal), " ", "")  'RDescription 
                            End If
                            row(16) = UserID 'EnterBy
                            row(17) = today.ToString("dd/MMM/yyyy") 'EnterDate
                            values.Rows.Add(row)
                    End If
                        End If
                End If
            Next

            Try
                Dim Parameters As System.Data.SqlClient.SqlParameter() = {
                New System.Data.SqlClient.SqlParameter("@TableData", values),
                New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0),
                New System.Data.SqlClient.SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)}

                Parameters(1).Direction = ParameterDirection.Output
                Parameters(2).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_UPDATE_USER_DETAILS", Parameters)

                ErrorFlag = CInt(Parameters(1).Value)
                Message = CStr(Parameters(2).Value)

                Dim cl_script1 As New System.Text.StringBuilder
                cl_script1.Append("         alert('" + Message.ToString + "');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
        Else
            Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_srpt1.Append("alert('Select File For uploading');reset();")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
        End If
    End Sub


End Class
