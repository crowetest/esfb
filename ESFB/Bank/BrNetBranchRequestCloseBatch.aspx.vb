﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Configuration
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq
Imports System.IO
Imports System.Net
Imports System.Web.Services
Imports System.Web.Script.Serialization
Partial Class BrNetBranchRequestCloseBatch
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing

#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1233) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            DT = DB.ExecuteDataSet("select c.app_request_id,c.request_no,j.app_name,e.App_Type_name,case when old_branch=-1 then '' else old_branch_name+(convert(varchar,old_branch)) end as oldBranch, " & _
                " case when d.to_branch=-1 then '' else to_branch_name+'-' +convert(varchar,d.to_branch) end as To_branch,old_role_name  as oldRole,g.App_role_name,c.Emp_Name+convert(varchar,c.emp_code),c.department,  " & _
                " c.Designation ,d.mail_id as email,d.mobile as mobile,'HO App Dt :'+convert(varchar,c.ho_approved_on)+' Reporting By : '+isnull(reporting_name,'')+'('+convert(varchar,isnull(reporting_to,''))+') HO:'+isnull(q.name,'')+'('+convert(varchar,isnull(q.emp_code,''))+')' as Approved_By, " +
                " c.app_type_id,c.created_branch_name+'-' +convert(varchar,c.created_branch),case when g.brnet_cifflag is null then 0 else (case when c.created_branch between 6000 and 9000 then 0 else g.brnet_cifflag end) end as flag,case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESCCO' when DB_ID=3 then 'LBS' when DB_ID=4 then 'LLMS' when DB_ID=5 then 'BC' end Entity  " & _
                " from ESFB.dbo.app_level k ,ESFB.dbo.app_request_master c  WITH (NOLOCK)  left join esfb.dbo.app_level_email q on c.ho_approved_by=q.emp_code,ESFB.dbo.app_master j,ESFB.dbo.app_type_master e ,    " & _
                " ESFB.dbo.app_dtl_profile d  left join ESFB.dbo.app_role_master g on d.Role_id=g.App_role  where   " & _
                " j.app_id=c.app_id and k.app_id=c.app_id and c.App_type_id=e.App_type_id and c.App_request_id=d.App_request_id  " & _
                " and c.level_id=k.level_id  and c.app_id in (10)  and brnet_status is null and c.HO_Approved_status =1 and  c.closed_status is null and batch_id is null order by c.ho_approved_on ").Tables(0)

            Me.Master.subtitle = "BRNET Branch Request Batch Creation"

            Dim decryptval As String
            Dim StrAttendance As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                decryptval = GF.Encrypt(DT.Rows(n)(0).ToString)
                StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString & "µ" & DT.Rows(n)(16).ToString & "µ" & DT.Rows(n)(17).ToString & "µ0"
                If n < DT.Rows.Count - 1 Then
                    StrAttendance += "¥"
                End If
            Next

            hid_dtls.Value = StrAttendance
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim Remarks As String = CStr(Data(0))
        Dim dataval As String = CStr(Data(1))

        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))



        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try

            Dim Params(5) As SqlParameter
            Params(0) = New SqlParameter("@userID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(1).Value = BranchID
            Params(2) = New SqlParameter("@EMPDtl", SqlDbType.VarChar)
            Params(2).Value = dataval.Substring(1)
            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(3).Direction = ParameterDirection.Output
            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(4).Direction = ParameterDirection.Output
            Params(5) = New SqlParameter("@COMMON_REMARKS", SqlDbType.VarChar, 300)
            Params(5).Value = Remarks.ToString
            DB.ExecuteNonQuery("SP_APP_APPROVAL_CLOSE_BATCH", Params)
            ErrorFlag = CInt(Params(3).Value)
            Message = CStr(Params(4).Value)

            Dim RetStr() As String

            Dim MsgId As String
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString
           
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
    End Sub
#End Region
#Region "Function"

    Public Shared Function WEB_Request_Response(Request As String, Request_type As Integer) As String()

        'Added On 22-10-2020 by 40013 - Changed request from IT
        Dim strData() As String = Request.Split({"&GSM"}, StringSplitOptions.RemoveEmptyEntries)
        Request = strData(0).ToString + " ESAF Bank&GSM" + strData(1).ToString

        Dim dataString As String = ""
        Dim ss As String = ""
        Dim delimiters() As String
        Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create(New Uri(Request)), HttpWebRequest)
        Using httpResponse As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
            Using stream As Stream = httpResponse.GetResponseStream()
                dataString = (New StreamReader(stream)).ReadToEnd()
            End Using
        End Using
        ss = dataString
        If Request_type = 1 Then
            delimiters = {"<", ">", ">/", "</", "/"}
        Else
            delimiters = {"=", " "}
        End If
        Dim parts() As String = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)


        Return parts.ToArray()


    End Function
#End Region

End Class
