﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="AssetBranchStatus.aspx.vb" Inherits="AssetBranchStatus" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
     <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload 
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10pt;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10pt;color:#476C91;
        }
    </style>

    <script src="../Script/Validations.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
       
        function AlphaNumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = (e.which >= 48 && e.which <= 57) || (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || (e.which == 8) || (e.which == 13) || (e.which == 32) || (e.which == 0);

            if (!valid) {
                e.preventDefault();
            }
        }
        function NumericDotCheck(e)//------------function to check whether a value is alpha numeric
        {
            // el - this, e - event
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                    if (unicode == 37 || unicode == 38) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (unicode == 46) {
                        var parts = el.value.split('.');
                        if (parts.length == 2)
                            return false;
                        return true;
                    }
                }
            }
            else {
                return true;
            }
        }
        function NumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = ((e.which >= 48 && e.which <= 57) || (e.keyWhich == 9) || (e.keyCode == 9) || (e.keyWhich == 8) || (e.keyCode == 8));
            //alert(valid);
            if (!valid) {
                e.preventDefault();
            }

        }
        function FromServer(Arg, Context) {
            var Data = Arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("AssetBranchStatus.aspx", "_self");
        }
        function btnApprove_onclick() {
            var ret = SaveData();
            if (ret==false) 
            {
                return false;
            }
           
          
            var strempcode = document.getElementById("<%= hid_temp.ClientID%>").value;
            var Data = "1Ø" + strempcode;
            ToServer(Data, 1);
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function DeleteRow(id) {
            
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            
            var NewStr = "";
            var TranID = "";
             for (n =1; n <= row.length - 1 ; n++) {
                 
                 if (id != n) {
                    
                         NewStr += "¥" + row[n];
                     }
                 else {
                    

                    
                         if (row[n] != "µµµµµµ") {
                             col = row[n].split("µ");
                             
                             var serialNo = col[2];;
                             var asset_id = col[0];
                             var status = col[1];
                             var Repair_status = col[4];
                             var Remarks = col[3];
                             var TranID = col[5];

                             var strval = "¥" + asset_id + "µ" + status + "µ" + serialNo + "µ" + Remarks + "µ" + Repair_status + "µ" + TranID + "µ1";
                         }
                         else {
                             var strval = "";
                         }

                         

                        
                         if (TranID == 0 || TranID == "") {
                            
                             NewStr += "";
                       
                         }
                         
                         else if  (TranID > 0 )
                         {
                            
                             NewStr += strval;
                             }
                         else {
                             
                             NewStr = "¥µµµµµµ";
                        }

                   
                 }

             }
             
             if (n-1 == id) {
                 NewStr += "¥µµµµµµ";
             }
            
             
             document.getElementById("<%= hid_dtls.ClientID %>").value = NewStr; 
         
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "¥µµµµµµ1" || document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                document.getElementById("<%= hid_dtls.ClientID %>").value = "¥µµµµµµ";
            }
           
                table_fill();
        }
        function updateValue(id) {
            var NewStr = ""
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "¥µµµµµµ") {
               
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                
               
                for (n = 1; n <= row.length - 1 ; n++) {
                   
                    if (id == n) {
                        
                            var serialNo = document.getElementById("txtserialNo" + id).value;
                            var asset_id = document.getElementById("cmbasset_id" + id).options[document.getElementById("cmbasset_id" + id).selectedIndex].text;
                            var status = document.getElementById("cmbstatus" + id).options[document.getElementById("cmbstatus" + id).selectedIndex].text;
                            var Repair_status = document.getElementById("cmbRepair_status" + id).options[document.getElementById("cmbRepair_status" + id).selectedIndex].text;
                            var Remarks = document.getElementById("txtRemarks" + id).value;
                            var TranID = (document.getElementById("txtTranID" + id).value == "") ? 0 : document.getElementById("txtTranID" + id).value;

                            NewStr += "¥" + asset_id + "µ" + status + "µ" + serialNo + "µ" + Remarks + "µ" + Repair_status + "µ" + TranID + "µ0";
                        

                    }
                    else {
                       
                        NewStr += "¥" + row[n];
                    }
                }
                
            }
            else {
                
                var serialNo = document.getElementById("txtserialNo" + id).value;
                var asset_id = document.getElementById("cmbasset_id" + id).options[document.getElementById("cmbasset_id" + id).selectedIndex].text;
                var status = document.getElementById("cmbstatus" + id).options[document.getElementById("cmbstatus" + id).selectedIndex].text;
                var Repair_status = document.getElementById("cmbRepair_status" + id).options[document.getElementById("cmbRepair_status" + id).selectedIndex].text;
                var Remarks = document.getElementById("txtRemarks" + id).value;
                var TranID = (document.getElementById("txtTranID" + id).value == "") ? 0 : document.getElementById("txtTranID" + id).value;

                                
                NewStr += "¥" + asset_id + "µ" + status + "µ" + serialNo + "µ" + Remarks + "µ" + Repair_status + "µ" + TranID + "µ0";
            }
           
            document.getElementById("<%= hid_dtls.ClientID %>").value = NewStr;
            
        }
        function SaveData() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            //if (document.getElementById("<%= hid_dtls.ClientID %>").value == "" || document.getElementById("<%= hid_dtls.ClientID %>").value == "¥µµµµµ") {
              //  alert("Enter any cheque details");
              //  return false;
           // }
            var NewStr = ""
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "¥µµµµµµ") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                

                for (n = 1; n <= row.length - 1 ; n++) {
                    if (row[n] != "µµµµµµ") {
                        col = row[n].split("µ");
                        
                        if (col[0] != " ---------Select---------" && (col[1] == " ---------Select---------" || col[2] == "" || col[3] == "" || col[4] == "---------Select---------")) {
                            if (col[0] != "No Damage") {
                                alert("Enter empty field");
                                document.getElementById("cmbasset_id" + n).focus();
                                return false;
                            }


                        }

                        if (col[0] == "-1") {
                            alert("Select Asset Type");
                            document.getElementById("cmbasset_id" + n).focus();
                            return false;

                        }
                        NewStr += "¥" + row[n];
                    }
                }

            }
          

            document.getElementById("<%= hid_temp.ClientID%>").value = NewStr;
            
            return true;
        }

        function CreateNewRow(e, val) {
            
            var n = (window.Event) ? e.which : e.keyCode;
            
            if (n == 13 || n == 9) {
                
                updateValue(val);
                AddNewRow();
            }
        }
        function table_fill() {
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                document.getElementById("<%= pnFamily.ClientID %>").style.display = '';
             var row_bg = 0;
             var tab = "";
             tab += "<div style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
             tab += "<tr height=20px;  class='tblQal'>";
             tab += "<td style='width:5%;text-align:center' >#</td>";
             tab += "<td style='width:20%;text-align:left' >Asset</td>";
             tab += "<td style='width:10%;text-align:left' >status</td>";
             tab += "<td style='width:25%;text-align:left' >serialNo</td>";
             tab += "<td style='width:20%;text-align:left'>Remarks</td>";
             tab += "<td style='width:10%;text-align:left'>Repair_status</td>";
             tab += "<td style='width:1%;text-align:left; display:none;'></td>";
             tab += "<td style='width:9%;text-align:left'></td>";

             tab += "</tr>";
             tab += "</table></div>";
             tab += "<div id='ScrollDiv' style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

             row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var i = 0;
            for (n = 1; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                if (col[6] != 1) {
                    var i = i+1;
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }

                    tab += "<td style='width:5%;text-align:center' >" + i + "</td>";
                    
                    if (col[0] == "") {
                        var select = "<select id='cmbasset_id" + i + "' class='NormalText' name='cmbasset_id" + i + "' onchange='updateValue(" + i + ")' style='width:100%'>";

                        var rows = document.getElementById("<%= hid_Asset.ClientID%>").value.split("Ñ");
                        for (a = 1; a < rows.length; a++) {
                            var cols = rows[a].split("ÿ");
                            if (cols[0] == -1)
                                select += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                            else
                                select += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";

                        }
                        select += "</select>";
                        tab += "<td style='width:20%;text-align:left' >" + select + "</td>";
                    }
                    else{
                        tab += "<td style='width:20%;text-align:left' >" + col[0] + "</td>";
                    }
                    if (col[1] == "") {
                        var select1 = "<select id='cmbstatus" + i + "' class='NormalText' name='cmbstatus" + i + "' onchange='updateValue(" + i + ")' style='width:100%'>";


                        select1 += "<option value='-1' selected=true> ---------Select---------</option>";

                        select1 += "<option value='1' >Partially Damaged</option>";
                        select1 += "<option value='2' >Fully Damaged</option>";


                        select1 += "</select>";
                        tab += "<td style='width:10%;text-align:left' >" + select1 + "</td>";
                    }
                    else {
                        tab += "<td style='width:10%;text-align:left' >" + col[1] + "</td>";
                    }
                    if (col[1] == "") {
                        var txtBox = "<input id='txtserialNo" + i + "' name='txtserialNo" + n + "' type='Text' style='width:99%;' value='" + col[0] + "' class='NormalText'  maxlength='100' onchange='updateValue(" + i + ")' onkeypress='return AlphaNumericCheck(event)'  />";
                        tab += "<td style='width:25%;text-align:left' >" + txtBox + "</td>";
                    }
                    else
                        tab += "<td style='width:25%;text-align:left' >" + col[2] + "</td>";
                    //}
                    //else {
                    //    tab += "<td style='width:30%;text-align:left'>" + col[0] + "</td>";
                    //}
                    //if (col[1] == "") {
                    if (col[1] == "") {
                        var txtBox = "<input id='txtRemarks" + i + "' name='txtRemarks" + i + "' type='Text' value='' style='width:99%;' class='NormalText' maxlength='100' onchange='updateValue(" + i + ")' onkeypress='return AlphaNumericCheck(event)' />";
                        tab += "<td style='width:20%;text-align:left' >" + txtBox + "</td>";
                    }
                    else
                        tab += "<td style='width:20%;text-align:left' >" + col[3] + "</td>";

                    if (col[1] == "") {
                        var select1 = "<select id='cmbRepair_status" + i + "' class='NormalText' name='cmbRepair_status" + i + "' style='width:100%' onchange='updateValue(" + i + ")' onkeydown='CreateNewRow(event," + i + ")'>";


                        select1 += "<option value='-1' selected=true > ---------Select---------</option>";

                        select1 += "<option value='1'  >Repaired</option>";

                        select1 += "<option value='2' >Not Repaired</option>";




                        select1 += "</select>";
                        tab += "<td style='width:10%;text-align:left' >" + select1 + "</td>";
                    }
                    else
                        tab += "<td style='width:10%;text-align:left' >" + col[4] + "</td>";

                    var txtBox = "<input id='txtTranID" + i + "' name='txtTranID" + i + "' type='Text' value='" + col[5] + "' style='width:99%;' class='NormalText' maxlength='100' onkeypress='return NumericCheck(event)'/>";
                    tab += "<td style='width:1%;text-align:left; display:none;'>" + txtBox + "</td>";
                   
                    tab += "<td style='width:9%;text-align:center' onclick=DeleteRow('" + i + "')><img  src='../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                    tab += "</tr>";
                }
            }

                tab += "</table></div></div></div>";
            
            document.getElementById("<%= pnFamily.ClientID %>").innerHTML = tab;
               
            document.getElementById("cmbasset_id" + i).focus();
        }
        else
            document.getElementById("<%= pnFamily.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }

        function AddNewRow() {
            
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var Len = row.length - 1;
                col = row[Len].split("µ");
                if (col[0] != "" && col[1] != "" && col[2] != "" && col[3] != "") {

                    document.getElementById("<%= hid_dtls.ClientID %>").value += "¥µµµµµµ";

                }
            }
            else {
                
                document.getElementById("<%= hid_dtls.ClientID%>").value = "¥µµµµµµ";
            }
               
                table_fill();
        }
       
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />
     
    <br />
    <br />
    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnFamily" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <asp:HiddenField ID="hid_Asset" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="UPDATE"  onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

