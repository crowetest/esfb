﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ServiceRequestHOApproval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1185) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Dim UserID As Integer = CInt(Session("UserID").ToString())


            DT = DB.ExecuteDataSet("select sr_request_id,sr_ticket_no,branch_name+'-'+convert(varchar,branch_id),app_name,app_type_name," & _
                    " a.department_name,issue,proposed_solution,comments," & _
                    " cug,mail_id,convert(varchar,eff_date,106),on_behalf_of,db_id,created_name+'('+convert(varchar,created_by) +')'," & _
                    " convert(varchar,created_on,106),case when DB_ID=1 then 'ESFB' when DB_ID=2 then 'ESMACO' else 'LBS' end " & _
                    " ,approved_name +'-'+ convert(varchar,approved_by) +'('+convert(varchar,approved_on,106)+')',convert(varchar,approved_on,106),g.val,isnull (a.urls_required,'')AS URL from ESFB.dbo.sr_request_master a left join DMS_ESFB.dbo.SR_Attachment g on a.sr_request_id=g.requestid,ESFB.dbo.sr_app_master b,ESFB.dbo.sr_app_type_master c where a.app_id=b.app_id and a.app_type_id=c.app_type_id " & _
                    "  and (ho_approved_status is null and approved_status=1) and " + UserID.ToString() + " = case when a.app_id=10 then 50354 else " + UserID.ToString() + " end and " + UserID.ToString() + " <> case when a.app_id<>10 then 50354 else 0 end ").Tables(0)

            '''' Outside Access (Bank Applications) HO Approval only by CISO - 50354 Venkitaraman


            Me.Master.subtitle = "Service Request HO Approval"




            Dim StrAttendance As String = ""
            Dim Strattachment As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1

                StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString & "µ" & DT.Rows(n)(16).ToString & "µ" & DT.Rows(n)(17).ToString & "µ" & DT.Rows(n)(18).ToString & "µ" & DT.Rows(n)(19).ToString & "µ" & DT.Rows(n)(20).ToString
                If n < DT.Rows.Count - 1 Then
                    StrAttendance += "¥"
                End If
            Next


            hid_dtls.Value = StrAttendance
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim dataval As String = CStr(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))



        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try

            Dim Params(5) As SqlParameter
            Params(0) = New SqlParameter("@userID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(1).Value = BranchID
            Params(2) = New SqlParameter("@OrderID", SqlDbType.Int)
            Params(2).Value = 1
            Params(3) = New SqlParameter("@EMPDtl", SqlDbType.VarChar)
            Params(3).Value = dataval.Substring(1)
            Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(4).Direction = ParameterDirection.Output
            Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(5).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_SR_APP_APPROVAL", Params)
            ErrorFlag = CInt(Params(4).Value)
            Message = CStr(Params(5).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + TypeID.ToString

    End Sub
#End Region



End Class
