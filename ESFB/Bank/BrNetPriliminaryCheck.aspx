﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="BrNetPriliminaryCheck.aspx.vb" Inherits="BrNetPriliminaryCheck" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

        function window_onload() {
            document.getElementById("rowNew").style.display = "none";
            document.getElementById("rowDel").style.display = "none";
            document.getElementById("rowTran").style.display = "none";
            ToServer("1ʘ", 1);
        }

        function FromServer(Arg, Context) 
        {
            switch(Context)
            {
                case 1: //  
                {                     
                    if(Arg != "")
                    {
                        ComboFill(Arg, "<%= cmbBatch.ClientID %>");
                    }
                    break;
                }
                case 2: //  
                {   
                    var Data = Arg.split("Ĉ");
                    if(Data[0] != "")
                    {
                        document.getElementById("rowNew").style.display = "";
                        document.getElementById("txtNewUser").value = Data[0];
                    }
                    else
                        document.getElementById("rowNew").style.display = "none";
                    if(Data[1] != "")
                    {
                        document.getElementById("rowDel").style.display = "";
                        document.getElementById("txtDelUser").value = Data[1];
                    }
                    else
                        document.getElementById("rowDel").style.display = "none";
                    if(Data[2] != "")
                    {
                        document.getElementById("rowTran").style.display = "";
                        document.getElementById("txtTraUser").value = Data[2]; 
                    }
                    else
                        document.getElementById("rowTran").style.display = "none";
                    break;
                }
            }
        }

        function BatchOnChange()
        {
            var BatchNo = document.getElementById("<%= cmbBatch.ClientID %>").value;
            ToServer("2ʘ"+ BatchNo, 2);
        }

        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ř");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("Ĉ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
    // <![CDATA[
    return window_onload()
    // ]]>
        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 100%; margin: 0px auto;">
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="text-align: left;" class="style2">
            </td>
            <td>
                &nbsp; &nbsp;<input id="filename" type="text" style="font-family: sans-serif; cursor: pointer;
                    width: 30%; border: none;" />
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="text-align: left;" class="style2">
                Select Batch
            </td>
            <td style="width: 63%">
                &nbsp;<asp:DropDownList ID="cmbBatch" runat="server" Height="16px" 
                    Width="414px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="text-align: left;" class="style2">
                &nbsp;
            </td>
            <td style="width: 63%">                 
            </td>
        </tr>
        <tr id="rowNew">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="text-align: left;" class="style2">
                New User
            </td>
            <td style="width: 63%">
                <textarea name="message" id="txtNewUser" rows="3"  cols="50" onkeypress='return TextAreaCheck(event)' ></textarea>
            </td>
        </tr>
        <tr id="rowDel">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="text-align: left;" class="style2">
                Disable/Delete User
            </td>
            <td style="width: 63%">
               <textarea name="message" id="txtDelUser" rows="3"  cols="50" onkeypress='return TextAreaCheck(event)' ></textarea>
            </td>
        </tr>
        <tr id="rowTran">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="text-align: left;" class="style2">
                Transfer User
            </td>
            <td style="width: 63%">
                <textarea name="message" id="txtTraUser" rows="3" cols="50" onkeypress='return TextAreaCheck(event)'  ></textarea>
            </td>
        </tr>
        <tr id="branch">
            <td style="width: 25%;">
                &nbsp;
            </td>
            <td style="text-align: left;" class="style2">
                &nbsp;
            </td>
            <td style="width: 63%">
                &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;"
                    type="button" value="EXIT" onclick="return btnExit_onclick()" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
            </td>
        </tr>
    </table>
    <configuration>
    <system.web>
    <compilaton debug="true" targetFramework="4.0"></compilaton>
    </system.web>
    </configuration>
</asp:Content>
