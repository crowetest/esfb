﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ChangeSignature
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DB As New MS_SQL.Connect
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))

        If (CInt(Data(0)) = 1) Then
            Dim DT As New DataTable
            Dim DT1 As New DataTable
            Dim GN As New GeneralFunctions
            DT = DB.ExecuteDataSet("select d.emp_code,d.emp_name,Department_name,Designation_name,CONVERT(VARCHAR(11),Date_Of_Join,106) as doj,convert(varchar,a.branch_id)+' - '+c.branch_name,A.tranid " & _
                    " from Emp_List d left join  branch_signature a  on a.emp_code=d.emp_code and a.status=1 and a.assign_status in (1,2) left join branch_master c on a.branch_id=c.branch_id where d.emp_code=" & Data(1).ToString & "").Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString() + "Ø" + DT.Rows(0)(6).ToString()
                CallBackReturn += "Ø"
                DT1 = DB.ExecuteDataSet("select top 1 E_Signature,content_type from DMS_ESFB.dbo.Emp_Signature where Emp_Code=" + Data(1).ToString() + " order by tranid desc").Tables(0)
                If DT1.Rows.Count > 0 Then
                    Dim bytes As Byte() = DirectCast(DT1.Rows(0)(0), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                    CallBackReturn += Convert.ToString("data:" + DT1.Rows(0)(1).ToString + ";base64,") & base64String
                Else
                    CallBackReturn += ""
                End If
                CallBackReturn += "|"
                DT1 = DB.ExecuteDataSet("select a.tranid,a.branch_id,c.branch_name,d.emp_name+convert(varchar,a.emp_code),b.designation_name,case when status=0 then 'Pending Approval' when (status=1 and assign_status=4 and transfer_branch is not null) then 'Previous Staff' when (status=1 and assign_status=4 and  transfer_branch is  null) then 'Assigned New branch ' + convert(varchar,transfer_branch) +'and waiting for approval'  else '' end,status from branch_Signature a,emp_master d,designation_master b,branch_master c where  a.post_id=b.designation_id and a.branch_id=c.branch_id and (status=0 or (status=1 and assign_status=4))  and  a.branch_id=" + Data(2).ToString() + " and  a.emp_code=d.emp_code  and a.post_id=" + Data(3).ToString()).Tables(0)
                If DT1.Rows.Count > 0 Then

                    For n As Integer = 0 To DT1.Rows.Count - 1
                        CallBackReturn += DT1.Rows(n)(0).ToString & "µ" & DT1.Rows(n)(1).ToString & "µ" & DT1.Rows(n)(2).ToString & "µ" & DT1.Rows(n)(3).ToString & "µ" & DT1.Rows(n)(4).ToString & "µ" & DT1.Rows(n)(5).ToString & "µ" & DT1.Rows(n)(6).ToString

                        If n < DT1.Rows.Count - 1 Then
                            CallBackReturn += "¥"
                        End If
                    Next

                End If

            Else
                CallBackReturn = "ØØ|"
                DT1 = DB.ExecuteDataSet("select a.tranid,a.branch_id,c.branch_name,d.emp_name+convert(varchar,a.emp_code),b.designation_name,case when status=0 then 'Pending Approval' when (status=1 and assign_status=4 and transfer_branch is not null) then 'Previous Staff' when (status=1 and assign_status=4 and  transfer_branch is  null) then 'Assigned New branch ' + convert(varchar,transfer_branch) +'and waiting for approval'  else '' end,status from branch_Signature a,emp_master d,designation_master b,branch_master c where  a.post_id=b.designation_id and a.branch_id=c.branch_id and (status=0 or (status=1 and assign_status=4))  and  a.branch_id=" + Data(2).ToString() + " and  a.emp_code=d.emp_code  and a.post_id=" + Data(3).ToString()).Tables(0)
                If DT1.Rows.Count > 0 Then

                    For n As Integer = 0 To DT1.Rows.Count - 1
                        CallBackReturn += DT1.Rows(n)(0).ToString & "µ" & DT1.Rows(n)(1).ToString & "µ" & DT1.Rows(n)(2).ToString & "µ" & DT1.Rows(n)(3).ToString & "µ" & DT1.Rows(n)(4).ToString & "µ" & DT1.Rows(n)(5).ToString & "µ" & DT1.Rows(n)(6).ToString

                        If n < DT1.Rows.Count - 1 Then
                            CallBackReturn += "¥"
                        End If
                    Next

                End If

            End If
        ElseIf (CInt(Data(0)) = 2) Then
            Dim DT As New DataTable
            Dim DT1 As New DataTable
            Dim GN As New GeneralFunctions
            Dim TRANID As String
            DT = DB.ExecuteDataSet("select a.emp_code,d.emp_name,Department_name,Designation_name,CONVERT(VARCHAR(11),Date_Of_Join,106) as doj,tranid " & _
                    " from branch_signature a left join Emp_List d on a.emp_code=d.emp_code  where  a.branch_id=" + Data(2).ToString() + " and a.emp_code is not null and a.status=1 and a.assign_status in (1,2) and a.post_id=" + Data(1).ToString()).Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString()

                CallBackReturn += "Ø"
                DT1 = DB.ExecuteDataSet("select top 1 E_Signature,content_type from DMS_ESFB.dbo.Emp_Signature where tranid=" + DT.Rows(0)(5).ToString()).Tables(0)
                If DT1.Rows.Count > 0 Then
                    Dim bytes As Byte() = DirectCast(DT1.Rows(0)(0), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                    CallBackReturn += Convert.ToString("data:" + DT1.Rows(0)(1).ToString + ";base64,") & base64String
                Else
                    CallBackReturn += ""
                End If
                CallBackReturn += "|"
                DT1 = DB.ExecuteDataSet("select a.tranid,a.branch_id,c.branch_name,d.emp_name+convert(varchar,a.emp_code),b.designation_name,case when status=0 then 'Pending Approval' when (status=1 and assign_status=4 and transfer_branch is not null) then 'Previous Staff' when (status=1 and assign_status=4 and  transfer_branch is  null) then 'Assigned New branch ' + convert(varchar,transfer_branch) +'and waiting for approval'  else '' end,status from branch_Signature a,emp_master d,designation_master b,branch_master c where  a.post_id=b.designation_id and a.branch_id=c.branch_id and (status=0 or (status=1 and assign_status=4))  and  a.branch_id=" + Data(2).ToString() + " and  a.emp_code=d.emp_code  and a.post_id=" + Data(1).ToString()).Tables(0)
                If DT1.Rows.Count > 0 Then

                    For n As Integer = 0 To DT1.Rows.Count - 1
                        CallBackReturn += DT1.Rows(n)(0).ToString & "µ" & DT1.Rows(n)(1).ToString & "µ" & DT1.Rows(n)(2).ToString & "µ" & DT1.Rows(n)(3).ToString & "µ" & DT1.Rows(n)(4).ToString & "µ" & DT1.Rows(n)(5).ToString & "µ" & DT1.Rows(n)(6).ToString

                        If n < DT1.Rows.Count - 1 Then
                            CallBackReturn += "¥"
                        End If
                    Next

                End If
            Else
                CallBackReturn = "ØØ|"
                DT1 = DB.ExecuteDataSet("select a.tranid,a.branch_id,c.branch_name,d.emp_name+convert(varchar,a.emp_code),b.designation_name,case when status=0 then 'Pending Approval' when (status=1 and assign_status=4 and transfer_branch is not null) then 'Previous Staff' when (status=1 and assign_status=4 and  transfer_branch is  null) then 'Assigned New branch ' + convert(varchar,transfer_branch) +'and waiting for approval'  else '' end,status from branch_Signature a,emp_master d,designation_master b,branch_master c where  a.post_id=b.designation_id and a.branch_id=c.branch_id and (status=0 or (status=1 and assign_status=4))  and  a.branch_id=" + Data(2).ToString() + " and  a.emp_code=d.emp_code  and a.post_id=" + Data(1).ToString()).Tables(0)
                If DT1.Rows.Count > 0 Then

                    For n As Integer = 0 To DT1.Rows.Count - 1
                        CallBackReturn += DT1.Rows(n)(0).ToString & "µ" & DT1.Rows(n)(1).ToString & "µ" & DT1.Rows(n)(2).ToString & "µ" & DT1.Rows(n)(3).ToString & "µ" & DT1.Rows(n)(4).ToString & "µ" & DT1.Rows(n)(5).ToString & "µ" & DT1.Rows(n)(6).ToString

                        If n < DT1.Rows.Count - 1 Then
                            CallBackReturn += "¥"
                        End If
                    Next

                End If

            End If
        ElseIf (CInt(Data(0)) = 3) Then
            Dim DT As New DataTable

            Dim GN As New GeneralFunctions
            DT = DB.ExecuteDataSet("select a.branch_id,b.state_name,c.district_name from branch_master a,state_master b,district_master c where a.state_id=b.state_id  and a.district_id=c.district_id and a.branch_id=" + Data(1).ToString()).Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString()
                CallBackReturn += "Ø"

            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf (CInt(Data(0)) = 4) Then '----Delete 

            Try

                Dim DT As New DataTable
                Dim StrVal As String
                Dim RetVal As Integer = DB.ExecuteNonQuery("UPDATE ESS_employee_signature set emp_code=NULL where tranID=" & Data(1).ToString & "")
                If RetVal = 1 Then
                    DT = DB.ExecuteDataSet("select a.tranid,a.branch_id,c.branch_name,a.emp_code,b.designation_name from branch_Signature a,designation_master b,branch_master c where a.branch_id <>" & Data(2).ToString() & "  and a.post_id=b.designation_id and a.branch_id=c.branch_id and a.Emp_Code=" + Data(3).ToString()).Tables(0)
                    If DT.Rows.Count > 0 Then

                        For n As Integer = 0 To DT.Rows.Count - 1
                            StrVal += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString
                            If n < DT.Rows.Count - 1 Then
                                StrVal += "¥"
                            End If
                        Next
                    End If
                    CallBackReturn = "1~" + "Deleted Successfully" + "|" + StrVal
                Else
                    CallBackReturn = "2~" + "Error Occured"
                End If

            Catch ex As Exception
                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try
        ElseIf (CInt(Data(0)) = 5) Then
            Dim DT As New DataTable

            Dim GN As New GeneralFunctions
            DT = DB.ExecuteDataSet("select d.emp_name+' - ' +convert(varchar,a.emp_code) from branch_Signature a,emp_master d where   status=1  and  a.branch_id=" + Data(1).ToString() + " and  a.emp_code=d.emp_code  and a.post_id=" + Data(2).ToString()).Tables(0)
            If (DT.Rows.Count > 0) Then
                CallBackReturn = DT.Rows(0)(0).ToString()


            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf (CInt(Data(0)) = 6) Then
            Dim DT As New DataTable


            DT = DB.ExecuteDataSet(" select '-1' as ID, '-------- Select --------' union all select distinct Branch_ID,convert(varchar,Branch_ID)+' - '+Branch_Name from BRANCH_MASTER  where status_id=1 and branch_id <> " & Data(1).ToString & "").Tables(0) 'and branch_id<50000
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub
#End Region
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            If GN.FormAccess(CInt(Session("UserID")), 1111) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Change Signature"
            'and Branch_ID<50000
            DT = DB.ExecuteDataSet(" select * from (select '-1' as ID, '-------- Select --------' as Name union all select distinct Branch_ID,convert(varchar,Branch_ID)+' - '+Branch_Name  as Name from BRANCH_MASTER  where status_id=1)AA  order by 2").Tables(0) 'and branch_id<50000
            GN.ComboFill(cmbBranch, DT, 0, 1)
            cmbBranch.SelectedIndex = 0
            DT = DB.ExecuteDataSet(" select * from ( select '-1' as ID, '-------- Select --------'  as Name union all select distinct Branch_ID,convert(varchar,Branch_ID)+' - '+Branch_Name  as Name from BRANCH_MASTER  where status_id=1)AA  order by 2 ").Tables(0) 'and branch_id<50000
            GN.ComboFill(cmbNewBranch, DT, 0, 1)
            cmbNewBranch.SelectedIndex = 0
            DT = DB.ExecuteDataSet(" select '-1' as ID, '-------- Select --------' union all select distinct designation_ID,designation_Name from designation_MASTER  where designation_id in (5,6)").Tables(0)
            GN.ComboFill(cmbPost, DT, 0, 1)
            cmbPost.SelectedIndex = 0

            GN.ComboFill(cmbNewPost, DT, 0, 1)
            cmbNewPost.SelectedIndex = 0
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            '/--- Register Client Side Functions ---//

            cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            cmbNewBranch.Attributes.Add("onchange", "NewBranchOnChange()")
            cmbPost.Attributes.Add("onchange", "PostOnChange()")
            txtEmpCode.Attributes.Add("onchange", "EmployeeOnChange()")
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
            cmbType.Attributes.Add("onchange", "TypeOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
    End Sub
#End Region

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim myFile As HttpPostedFile = fup1.PostedFile
            Dim nFileLen As Integer = myFile.ContentLength
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                AttachImg = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, nFileLen)
            End If
            Dim Result As String
            Dim UserID As String = Session("UserID").ToString()


            Dim BranchID As Integer = CInt(Me.hdn_Branch.Value)
            Dim PostID As Integer = CInt(Me.hdn_Post.Value)
            Dim EmpCode As String = CStr(Me.hdn_Empcode.Value)
            Dim NewBranch As Integer = CInt(Me.hdn_NewBranch.Value)
            Dim NewPost As Integer = CInt(Me.hdn_NewPost.Value)
            Dim Type As Integer = CInt(Me.cmbType.SelectedValue)
            Dim IPAddress As String = GeneralFunctions.getMACIPAddress
            Dim Descr As String = CStr(Me.hid_Details.Value)
            Dim TranID As Integer = CInt(IIf(Me.hdn_TranID.Value = "", 0, Me.hdn_TranID.Value))

            Try
                Dim Params(13) As SqlParameter
                Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(0).Value = BranchID
                Params(1) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
                Params(1).Value = AttachImg
                Params(2) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                Params(2).Value = ContentType
                Params(3) = New SqlParameter("@PostID", SqlDbType.Int)
                Params(3).Value = PostID
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = UserID
                Params(5) = New SqlParameter("@NewBranchID", SqlDbType.Int)
                Params(5).Value = NewBranch
                Params(6) = New SqlParameter("@NewPostID", SqlDbType.Int)
                Params(6).Value = NewPost
                Params(7) = New SqlParameter("@Type", SqlDbType.Int)
                Params(7).Value = Type
                Params(8) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(8).Value = EmpCode
                Params(9) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(9).Direction = ParameterDirection.Output
                Params(10) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(10).Direction = ParameterDirection.Output
                Params(11) = New SqlParameter("@IPAddress", SqlDbType.VarChar, 200)
                Params(11).Value = IPAddress
                Params(12) = New SqlParameter("@Descr", SqlDbType.VarChar, 5000)
                Params(12).Value = Descr.ToString
                Params(13) = New SqlParameter("@TRANID", SqlDbType.Int)
                Params(13).Value = TranID
                DB.ExecuteNonQuery("SP_SIGNATURE_UPDATION", Params)
                ErrorFlag = CInt(Params(9).Value)
                Message = CStr(Params(10).Value)
            Catch ex1 As Exception
                Message = ex1.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("        window.open('ChangeSignature.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)

           



        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
