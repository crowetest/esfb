﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  CodeFile="BrNetBranchRequestCloseBatch.aspx.vb" Inherits="BrNetBranchRequestCloseBatch" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <style type="text/css">
    #Button
    {
        width:100%;
        height:40px;
        font-weight:bold;
        line-height:40px;
        text-align:center;
        border-top-left-radius: 25px;
	    border-top-right-radius: 25px;
	    border-bottom-left-radius: 25px;
	    border-bottom-right-radius: 25px;
        cursor:pointer;
        background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
        color:#E0E0E0;
    }
        
    #Button:hover
    {
        background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        color:#036;
    }        
</style>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function table_fill() {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
           
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr style='background-color:#efe1ef;'>";
            tab += "<td style='width:1%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:5%;text-align:left'>Request No</td>";
            tab += "<td style='width:5%;text-align:left'>Created Branch</td>";
            tab += "<td style='width:7%;text-align:left'>Emp_Name</td>";
            tab += "<td style='width:4%;text-align:left' >Entity</td>";
            tab += "<td style='width:5%;text-align:left' >Application</td>";
            tab += "<td style='width:5%;text-align:left'>Type</td>";            
            tab += "<td style='width:7%;text-align:left' >FromBranch</td>";
            tab += "<td style='width:7%;text-align:left' >ToBranch</td>";
            tab += "<td style='width:7%;text-align:left'>New Role</td>";
            tab += "<td style='width:7%;text-align:left'>PrevRole</td>";
            tab += "<td style='width:5%;text-align:left'>Dep_Name</td>";
            tab += "<td style='width:5%;text-align:left'>Desg_Name</td>";            
            tab += "<td style='width:4%;text-align:left'>Mobile</td>";
            tab += "<td style='width:4%;text-align:left'>Approved By</td>";
            tab += "<td style='width:4%;text-align:center'>Batch Select</br><input type='checkbox' id='chkBASelectAll'  onclick='SelectOnClick(-1,1,0)' /></td>";
            tab += "<td style='width:7%;text-align:center'>CIF</td>";           
            tab += "<td style='width:2%;text-align:center'>CA</td>";
            tab += "<td style='width:2%;text-align:center'>GL</td>";
            tab += "<td style='width:7%;text-align:center'>Remarks</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr style='background-color:#FBEFFB;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr style='background-color:#FFF;'>";
                    }
                    i = n + 1;
                    //c.app_request_id,d.app_dtl_id,j.app_name,h.branch_name,d.emp_code, f.Emp_Name,e.App_Type_name,d.Remarks,g.App_role_name,i.branch_name as To_branch

                    tab += "<td style='width:1%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:5%;text-align:left;word-break: break-all;'>" + col[15] + "</td>";
                    //tab += "<td style='width:7%;text-align:left'>" + col[8] + "</td>";

                     var txtBox1 = "<textarea id='txtEmpName" + col[0] + "' name='txtEmpName" + col[0] + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)'  onchange='updateEmpName(" + col[0] + ")' >" + col[8] + "</textarea>";
                    tab += "<td style='width:5%;text-align:center'>"+ txtBox1 +"</td>";

                    tab += "<td style='width:4%;text-align:left' >" + col[17] + "</td>";
                    tab += "<td style='width:5%;text-align:left' >" + col[2] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[3] + "</td>"; 
                    tab += "<td style='width:7%;text-align:left;word-break: break-all;' >" + col[4] + "</td>";                   
                    tab += "<td style='width:7%;text-align:left;word-break: break-all;' >" + col[5] + "</td>";                    
                    tab += "<td style='width:7%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[9] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[10] + "</td>";                    
                    tab += "<td style='width:4%;text-align:left'>" + col[12] + "</td>";//14
                    tab += "<td style='width:4%;text-align:left'>" + col[13] + " By</td>";
                 
                    tab += "<td style='width:4%;text-align:center'><input type='checkbox' id='chkSelectBA" + col[0] + "' onclick='SelectOnClick(" + col[0] + ",2," + col[14] + ")'  /></td>";
                    var txtBox2 = "<textarea id='txtCIF" + col[0] + "' name='txtCIF" + col[0] + "' style='width:99%; float:left;' maxlength='14' onkeypress='return NumericCheck(event)'  disabled=true ></textarea>";
                    tab += "<td style='width:7%;text-align:center'>" + txtBox2 + "</td>";                    
                    tab += "<td style='width:2%;text-align:center'><input type='checkbox' id='chkSelectCA" + col[0] + "'  disabled=true onclick='SelectOnClick(" + col[0] + ",2," + col[14] + ")'  /></td>";
                    tab += "<td style='width:2%;text-align:center'><input type='checkbox' id='chkSelectGL" + col[0] + "'  disabled=true onclick='SelectOnClick(" + col[0] + ",4," + col[14] + ")'  /></td>";
                    var txtBox3 = "<textarea id='txtRemarks" + col[0] + "' name='txtRemarks" + col[0] + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)' ></textarea>";
                    tab += "<td style='width:7%;text-align:center'>"+ txtBox3 +"</td>";
                    tab += "</tr>";
                }
            }
           
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//

        }
        function SelectOnClick(val, type,apptype) {    

                if (type == 1) //All
                {
                    row = document.getElementById("<%= hid_dtls.ClientID%>").value.split("¥");
                   
                    for (n = 0; n <= row.length - 1; n++)
                    {
                        i = n + 1;
                        col = row[n].split("µ");   
                        if (document.getElementById("chkBASelectAll").checked == true)

                        {
                            if (col[14] ==50 && col[16] == 1)
                            {
//                                document.getElementById("chkSelectBA" + col[0]).disabled = false;
                                document.getElementById("chkSelectCA" + col[0]).disabled = false;
                                document.getElementById("chkSelectGL" + col[0]).disabled = false;
                                document.getElementById("txtCIF" + col[0]).disabled = false;
                                document.getElementById("chkSelectBA" + col[0]).checked = true;
                            }
                        }    
                        else
                        {
//                            document.getElementById("chkSelectBA" + col[0]).disabled = true;
                            document.getElementById("chkSelectCA" + col[0]).disabled = true;
                            document.getElementById("chkSelectGL" + col[0]).disabled = true;
                            document.getElementById("txtCIF" + col[0]).disabled = true;
                            document.getElementById("chkSelectBA" + col[0]).checked = false;

                        }
                    }
                }
                if (type == 2) //BA 
                {
                    
                    var TickType = true;
                    if (document.getElementById("chkSelectBA" + val).checked == true) 
                        TickType = false; 
                    else 
                        TickType = true;;
                  
                    document.getElementById("chkSelectCA" + val).disabled = TickType;
                    document.getElementById("chkSelectGL" + val).disabled = TickType;
                    document.getElementById("txtCIF" + val).disabled = TickType;
                    if (document.getElementById("chkSelectBA" + val).checked == false)
                        document.getElementById("chkBASelectAll").checked = false;
                }


////            if (val < 0) {
////                
////                if (type == 1) 
////                {
////                    row = document.getElementById("<%= hid_dtls.ClientID%>").value.split("¥");

////                        for (n = 0; n <= row.length - 1; n++)
////                        {
////                            i = n + 1;
////                            col = row[n].split("µ");                          
////                            
////                            document.getElementById("chkSelectBA" + col[0]).checked = document.getElementById("chkBASelectAll").checked;
////                            if (document.getElementById("chkSelectBA" + col[0]).checked == false) {    
////                                                      
////                                document.getElementById("chkSelectCA" + col[0]).checked = false;
////                                document.getElementById("chkSelectGL" + col[0]).checked = false;
////                            }
////                            if (document.getElementById("chkSelectBA" + col[0]).checked == true && col[14] ==50 && col[16] ==1) {                         
////                                document.getElementById("chkSelectCA" + col[0]).disabled = false;
////                                document.getElementById("chkSelectGL" + col[0]).disabled = false;
////                                document.getElementById("txtCIF" + col[0]).disabled = false;
////                            }
////                            else {                                
////                                document.getElementById("chkSelectCA" + col[0]).disabled = true;
////                                document.getElementById("chkSelectGL" + col[0]).disabled = true;
////                                document.getElementById("txtCIF" + col[0]).disabled = true;
////                            }
////                        }
////                    }
////                }
////                else {
////               
////              
////                    if (document.getElementById("chkSelectBA" + val).checked == false) {
////                        document.getElementById("chkBASelectAll").checked = false;
////                    }
////                    if (document.getElementById("chkSelectBA" + val).checked == true && apptype == 50 && col[16] ==1) {     
////                        document.getElementById("chkSelectCA" + val).disabled = false;
////                        document.getElementById("chkSelectGL" + val).disabled = false;
////                        document.getElementById("txtCIF" + val).disabled = false;
////                    }
////                    else if (document.getElementById("chkSelectBA" + val).checked == false && apptype == 50 && col[16] ==1) {
////                        document.getElementById("chkSelectCA" + val).disabled = true;
////                        document.getElementById("chkSelectGL" + val).disabled = true;
////                        document.getElementById("txtCIF" + val).disabled = true;
////                    }
////                    else {  
////                        document.getElementById("chkSelectCA" + val).disabled = true;
////                        document.getElementById("chkSelectGL" + val).disabled = true;
////                        document.getElementById("txtCIF" + val).disabled = true;

////                    }                    
////                }        
        }
        function window_onload() {
            table_fill();
           
        }
        function updateEmpName(RequestID)
        {   
            var Newstr = "";    
            var row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            var txtName = document.getElementById("txtEmpName" + RequestID).value;
            for (n = 0; n <= row.length - 1; n++)
            {               
                col = row[n].split("µ");
                if(RequestID == col[0])                    
                    Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ" + col[7] + "µ" + txtName + "µ" + col[9] + "µ" + col[10] + "µ" + col[11] + "µ" + col[12] + "µ" + col[13] + "µ" + col[14] + "µ" + col[15] + "µ" + col[16] + "µ" + col[17] + "µ1";                    
                else
                    Newstr += row[n];

                if(n < row.length - 1) 
                    Newstr += "¥";                
            }

            document.getElementById("<%= hid_dtls.ClientID %>").value = Newstr;
        }
        function UpdateValue() {                  
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("No Requests are existing ");
                return false;
            }
            document.getElementById("<%= hid_temp.ClientID %>").value = "";

            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");

                var CIF = "";
                if(document.getElementById("txtCIF" + col[0]).disabled ==false)
                    CIF = document.getElementById("txtCIF" + col[0]).value;
                
                var txtRemarks = document.getElementById("txtRemarks" + col[0]).value;

                var CA = (document.getElementById("chkSelectCA" + col[0]).checked) ? 1 : 0;
                var GL = (document.getElementById("chkSelectGL" + col[0]).checked) ? 1 : 0;
                
                if (document.getElementById("chkSelectBA" + col[0]).checked == 1) 
                {
                    if (col[14] == 50 && col[16] ==1 && CIF.length != "12") {                        
                        alert("CIF length should be 12 digits ");
                        document.getElementById("txtCIF" + col[0]).focus();
                        return false;
                    }
                    else if (col[14] == 50 && col[16] ==1 && CA == 0 && GL == 0) {
                        alert("Please select GL|CA");
                        document.getElementById("txtCIF" + col[0]).focus();
                        return false;
                    }
                    else {
                        document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[0] + "µ" + txtRemarks + "µ" + CA + "µ" + GL + "µ" + CIF + "µ" + col[8] + "µ" + col[18];
                    }   
                }
            }            
            return true;
        }
        function FromServer(arg, context) {
        
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("BrnetBranchRequestCloseBatch.aspx", "_self");
        }
        
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnSave_onclick(){                    
            var ret = UpdateValue();
            if (ret == 0) return false;
            if (confirm("Are you sure to Save This ") == 1) {
                var ToData = document.getElementById("txtCommonRemarks").value + "Ø" + document.getElementById("<%= hid_temp.ClientID %>").value;
                   
                ToServer(ToData, 1);
            }
        }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr> 
            <td colspan="3"><textarea id="txtCommonRemarks" name="txtCommonRemarks" style="width:99%; float:left;" maxlength="300" onkeypress='return TextAreaCheck(event)' ></textarea>
            </td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />    <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="SAVE" onclick="return btnSave_onclick()" />              <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />
                <br />
                &nbsp;
                &nbsp;
                </td>
        </tr>
    </table>    
<br /><br />
    <configuration>
    <system.web>
    <compilaton debug="true" targetFramework="4.0"></compilaton>
    </system.web>
</configuration>
</asp:Content>

