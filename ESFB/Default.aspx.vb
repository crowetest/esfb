Imports System.Data
Imports System.Data.SqlClient
Partial Class Login
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim Flag As Boolean = False
    Dim DT, DT1, DTNew, DTDATE As New DataTable
    Dim wrongPwdCount As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.txt_user_id.Focus()
        'DT1 = DB.ExecuteDataSet("select count(*) from ESWT.dbo.election_date where getdate()>=Election_start_date and  getdate()<= Election_end_date").Tables(0)

        'If CInt(DT1.Rows(0)(0)) > 0 Then
        '    Flag = True
        'Else

        '    Flag = False

        'End If
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload('" & Flag & "');", True)

        Session.Clear()
        cmd_login.Attributes.Add("onclick", "return LoginOnClick()")
        Me.txt_password.Attributes.Add("onchange", "return PwdOnChange()")
    End Sub
    Protected Sub cmd_login_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_login.Click
        'If txt_user_id.Text <> "" And txt_password.Text <> "" Then
        If Me.hdnUserID.Value <> "" And Me.hdnPswd.Value <> "" Then
            'If ValidateUser(Me.txt_user_id.Text, Me.txt_password.Text) Then
            If ValidateUser(Me.hdnUserID.Value, Decrypt(Me.hdnPswd.Value)) Then
                Try
                    DT = DB.ExecuteDataSet("select text_value from GENERAL_PARAMETERS where param_id = 2").Tables(0)
                    Session("FirmName") = CStr(DT.Rows(0)(0))
                    DT = DB.ExecuteDataSet("select isnull(Module_id,0) from Guest_Login where emp_code =" + Me.txt_user_id.Text).Tables(0)
                    If DT.Rows.Count > 0 Then
                        Session("GuestModuleID") = CStr(DT.Rows(0)(0))
                    Else
                        Session("GuestModuleID") = -1
                    End If

                    Dim UserID As Integer = CInt(Session("UserID"))

                    DT = DB.ExecuteDataSet("select DATEPART(d, GETDATE())").Tables(0)

                    'If DT.Rows(0).Item(0) > 15 And DT.Rows(0).Item(0) <= 20 Then
                    'DT = DB.ExecuteDataSet("select count(*) from emp_attend_his where emp_code=" & UserID & " and (Exp_Remarks='TRR' or Exp_Remarks='NI' or Exp_Remarks='CLR' or Exp_Remarks='SLR' or Exp_Remarks='PLR' or Exp_Remarks='MLR'   or Exp_Remarks='PTLR'   or Exp_Remarks='CLHR'  or Exp_Remarks='NIH' or Exp_Remarks='SLHR' or Exp_Remarks='PLHR' or Exp_Remarks='NMM'  or Exp_Remarks='NME' or Exp_Remarks='NM' or Exp_Remarks='NMMR' or Exp_Remarks='NMER') and Tra_Dt between Format(DATEADD(DAY,20,EOMONTH(convert(date,getdate(),105),-2)), 'dd-MMM-yyyy') and Format(getdate(), 'dd-MMM-yyyy')").Tables(0)
                    'If DT.Rows(0).Item(0) > 0 Then
                    '    Response.Redirect("Popup.aspx")
                    'End If


                    'End If
                    Dim PwdExpiry As Integer = 0
                    DTDATE = DB.ExecuteDataSet("SELECT DATEDIFF(DAY,MAX(CHANGED_ON),GETDATE()) FROM EMP_PASSWORD_HISTORY WHERE emp_code= " + UserID.ToString()).Tables(0)
                    Dim days = CInt(DTDATE.Rows(0)(0).ToString())
                    If days > 60 Then
                        PwdExpiry = 1
                    End If

                    'If IsNumeric(Me.txt_password.Text) Then
                    '    ''If (Me.txt_password.Text = "Esaf123") Then
                    '    Response.Redirect("ChangePassword.aspx?ID=2")
                    'ElseIf Session("GuestModuleID") > 0 Then
                    '    Response.Redirect("Home.aspx?ModuleID=" & Session("GuestModuleID"))
                    'Else
                    '    Response.Redirect("Portal.aspx")
                    'End If
                    Dim hidSum As String = Me.hidSum.Value
                    If IsNumeric(Me.hdnPswd.Value) Then
                        Response.Redirect("ChangePassword.aspx?ID=2")
                    ElseIf PwdExpiry = 1 Then
                        Response.Redirect("ChangePassword.aspx?ID=3")
                    ElseIf Session("GuestModuleID") > 0 Then
                        Response.Redirect("Home.aspx?ModuleID=" & Session("GuestModuleID"))
                    ElseIf hidSum = "false" Then
                        Me.lblMessage.Text = "Incorrect sum !"
                    Else
                        If days = 60 Then
                            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                            cl_script1.Append("         alert('Your password will expire today. Please change your password.');")
                            cl_script1.Append("        window.open('Portal.aspx', '_self');")
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
                            Return
                        ElseIf days = 59 Then
                            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                            cl_script1.Append("         alert('Your password will expire tomorrow. Please change your password.');")
                            cl_script1.Append("        window.open('Portal.aspx', '_self');")
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
                            Return
                        Else
                            Response.Redirect("Portal.aspx")
                        End If
                        'Response.Redirect("Portal.aspx")
                    End If
                Catch ex As Exception
                    Me.lblMessage.Text = "Invalid Configuration !"
                End Try
            Else
                If Me.hdnWrongPwd.Value <> "" Then
                    wrongPwdCount = CInt(Me.hdnWrongPwd.Value) + CInt(1)
                Else
                    wrongPwdCount = 1
                End If
                Me.hdnWrongPwd.Value = wrongPwdCount.ToString()
                If CInt(Me.hdnWrongPwd.Value) >= 3 Then
                    Dim Message As String = Nothing
                    Dim ErrorFlag As Integer = 0
                    Try
                        Dim Params(2) As SqlParameter
                        Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                        Params(0).Value = Me.hdnUserID.Value
                        Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(1).Direction = ParameterDirection.Output
                        Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(2).Direction = ParameterDirection.Output
                        DB.ExecuteNonQuery("SP_Lock_Password", Params)
                        ErrorFlag = CInt(Params(1).Value)
                        Message = CStr(Params(2).Value)
                    Catch ex As Exception
                        Message = ex.Message.ToString
                        ErrorFlag = 1
                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                    End Try
                    Me.lblMessage.Text = "Your account is locked !!!"
                End If
                DT = DB.ExecuteDataSet("SELECT PASS_WORD FROM EMP_MASTER WHERE EMP_CODE = " + Me.hdnUserID.Value).Tables(0)
                If DT.Rows(0)(0).ToString() = "" Then
                    Me.hdnUserID.Value = ""
                    Me.hdnPswd.Value = ""
                    Me.lblMessage.Text = "Your account is locked !!!"
                Else
                    Me.hdnUserID.Value = ""
                    Me.hdnPswd.Value = ""
                    Me.lblMessage.Text = "Invalid User Credentials !"
                End If


                'Me.txt_user_id.Text = ""
                'Me.txt_password.Text = ""
                'Me.lblMessage.Text = "Invalid User Credentials !"
                'Me.txt_user_id.Focus()
            End If
        Else
            'Response.Redirect("Portal.aspx")
            Me.lblMessage.Text = "Enter UserID and Password!"
            Me.txt_user_id.Focus()
        End If
        cmd_login.Attributes.Add("onclick", "return LoginOnClick()")
        Me.txt_password.Attributes.Add("onchange", "return PwdOnChange()")
    End Sub
    Public Function ValidateUser(ByVal username As String, ByVal passwd As String) As Boolean
        Dim UserID As String
        Try
            UserID = CStr(Me.txt_user_id.Text)
        Catch ex As Exception
            Return False
        End Try
        Dim userExists As Boolean
        userExists = False
        Dim DT As New DataTable
        'Dim strPass As String = EncryptDecrypt.Encrypt(Me.txt_password.Text.Trim(), Me.txt_user_id.Text.Trim())
        'Dim strPass As String = EncryptDecrypt.Encrypt(Me.hdnPswd.Value.Trim(), Me.txt_user_id.Text.Trim())
        'DT = DB.ExecuteDataSet("SELECT a.emp_name,b.branch_id,b.branch_name,getdate(),a.post_id,a.department_id from EMP_MASTER a,BRANCH_MASTER b where a.emp_code = " & UserID & "  and a.Pass_word = '" & EncryptDecrypt.Encrypt(Me.hdnPswd.Value.Trim(), Me.txt_user_id.Text.Trim()) & "' and a.branch_id = b.branch_id and a.status_id = 1").Tables(0)
        'DT = DB.ExecuteDataSet("SELECT a.emp_name,b.branch_id,b.branch_name,getdate(),a.post_id,a.department_id from EMP_MASTER a,BRANCH_MASTER b where a.emp_code = " & UserID & "   and a.branch_id = b.branch_id and a.status_id = 1").Tables(0)

        ' Modified on 13-oct-2020 by 40013 for VAPT changes
        Dim strPass As String = EncryptDecrypt.Encrypt(passwd, Me.txt_user_id.Text.Trim())
        Dim Params(1) As SqlParameter
        Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
        Params(0).Value = UserID
        Params(1) = New SqlParameter("@Password", SqlDbType.VarChar)
        Params(1).Value = strPass
        DT = DB.ExecuteDataSet("SELECT a.emp_name,b.branch_id,b.branch_name,getdate(),a.post_id,a.department_id from EMP_MASTER a,BRANCH_MASTER b where a.emp_code =@EmpCode  and a.Pass_word =@Password and a.branch_id = b.branch_id and a.status_id = 1", Params, 1).Tables(0)
        'DT = DB.ExecuteDataSet("SELECT a.emp_name,b.branch_id,b.branch_name,getdate(),a.post_id,a.department_id from EMP_MASTER a,BRANCH_MASTER b where a.emp_code =@EmpCode   and a.branch_id = b.branch_id and a.status_id = 1", Params, 1).Tables(0)

        If DT.Rows.Count > 0 Then
            userExists = True
            Session("UserID") = CStr(UserID)
            Session("UserName") = CStr(DT.Rows(0)(0))
            Session("BranchID") = CStr(DT.Rows(0)(1))
            Session("BranchName") = CStr(DT.Rows(0)(2))
            Session("TraDt") = CDate(DT.Rows(0)(3)).ToString("dd MMM yyyy")
            Session("Post_ID") = CStr(DT.Rows(0)(4))
            Session("DepartmentID") = CStr(DT.Rows(0)(5))
        End If
        Return userExists
    End Function
    Public Function Decrypt(ByVal cipherText As String) As String
        Dim base64Decoded As String
        Dim data() As Byte
        data = System.Convert.FromBase64String(cipherText)
        base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data)
        Return base64Decoded
    End Function
End Class
