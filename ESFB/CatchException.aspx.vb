﻿Partial Class AccessDenied
    Inherits System.Web.UI.Page
    Dim Flg As Integer = 0
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim ErrorNo As Integer = Request.QueryString.Get("ErrorNo")
        lblMessage.Text = "An Unexpected Error Occured.  Error Code  is " + ErrorNo.ToString()
        If Not Request.QueryString.Get("Flg") = Nothing Then
            Flg = Request.QueryString.Get("Flg")
        End If
        hdnFlg.Value = Flg
    End Sub
End Class
