﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="Downloads.aspx.vb" Inherits="Downloads" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ESAF</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="Style/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="Style/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="Style/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="Style/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
     <script src="../../Script/Calculations.js" type="text/javascript"></script>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script src="Script/jquery.min.js" type="text/javascript"><
    </script>
    <style type="text/css">
        .pn1
        {
            margin-top :-350px;
        }
    </style>
  <script language="javascript" type="text/javascript">
      var row_Length;
      function window_onload() {
          var tab = "<div><br/><br/></div>";
          if (document.getElementById("<%= hid_Card.ClientID %>").value == 2) {

              table_fill_cards();
          }
          document.getElementById("<%= pnlImg.ClientID %>").innerHTML = tab;

      }
//      function btn_Display() {

//          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';

//          var row_bg = 0;
//          var tab = "";
//          tab += "<div style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
//          tab += "<table style='width:80%;margin:0px auto;font-family:'cambria';' align='center'>";
//          if (document.getElementById("<%= hid_Display.ClientID %>").value != "") {
//              row1 = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
//              var ShowQuali = 0;
//              for (m = 0; m <= row1.length - 1; m++) {
//                  col2 = row1[m].split("µ");
//                  if (ShowQuali == 0) {

//                      tab += "<tr><td style='width:10%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
//                      tab += "<td style='width:20%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
//                      tab += "<td style='width:70%;height:10px; text-align:left;font-weight:bold;' >Description</td></tr>";
//                      ShowQuali = 1;

//                  }

//                  tab += "<tr><td style='width:30%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
//                  if (col2[4] == 1) {
//                      tab += "<td style='width:40%;height:10px; text-align:left;' ><a style='width:50%;'  style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";

//                  }
//                  else {
//                      tab += "<td style='width:50%;height:10px; text-align:left;' ><a style='width:50%;'  style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";

//                  }


//                  tab += "<td style='width:100%;height:10px; text-align:left;' >" + col2[5] + "</td>";

//                  tab += " &nbsp &nbsp &nbsp<td style = 'width:5%;'><br><input  type ='button' onclick='Delete_onclick(" + col2[0] + ")' id ='btnExpand' value='Delete'></td></tr>";
//              }

//              tab += "<tr><td colspan='3'></td></tr>";


//          }
//          tab += "</table></div>";
//          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
//      }
      function btn_Display() {
          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          var row_bg = 0;
//          var tab = "";
//          tab += "<div style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
//          tab += "<table style='width:80%;margin:0px auto;font-family:'cambria';' align='center'>";
          if (document.getElementById("<%= hid_Display.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:10%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:70%;height:10px; text-align:left;font-weight:bold;' >Description</td></tr>";
                      ShowQuali = 1;

                  }

                  tab += "<tr><td style='width:10%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                  if (col2[4] == 1) {
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a style='width:50%;' href='Help/viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";

                  }
                  else {
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a style='width:50%;' href='Help/ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";

                  }


                  tab += "<td style='width:100%;height:10px; text-align:left;' >" + col2[5] + "</td>";
              }

          }
          var row_Length = document.getElementById("<%= hid_Rowlength.ClientID %>").value;
          var fid = document.getElementById("<%= hid_Fid.ClientID %>").value;
          for (n = 0; n <= row_Length - 1; n++) {
              $('.hrTr').hide();
          }
          $("#tr_" + fid).show();
          document.getElementById('btn_'+ fid).innerHTML = tab;
      }
      function btn_Expand(DAT, VAL) {
         
          document.getElementById("<%= hid_Fid.ClientID %>").value = DAT;
          document.getElementById("<%= hid_DeptId.ClientID %>").value = VAL;
          var toData = "15Ø" + DAT + "Ø" + VAL
          
                    ToServer(toData,15);
              }
      function table_fill_HR() {

          document.getElementById("lblCaption").innerHTML = 'HR Management';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {            
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRMS%20Video1.mp4' target='_blank'>HRMS Video1.mp4</a><b></b></a></td></tr>";
              tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRMS%20Video2.mp4' target='_blank'>HRMS Video2.mp4</a><b></b></a></td></tr>";
              tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRMS%20Video3.mp4' target='_blank'>HRMS Video3.mp4</a><b></b></a></td></tr>";
              tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRMS%20Video4.mp4' target='_blank'>HRMS Video4.mp4</a><b></b></a></td></tr>";

              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");
                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",1)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  document.getElementById("<%= hid_Rowlength.ClientID %>").value = m + 1;
//                  alert("tr"+m);
                                                     }
              tab += "<tr><td colspan='3'></td></tr>";
                                                                                     }

              tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//
      }
      function table_fill_Risk() {

          document.getElementById("lblCaption").innerHTML = 'Risk Management';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
             if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");

                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",2)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                 // tab += "<div style='width: 50%';><tr><td style = 'width:5%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",2)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
              }

              tab += "<tr><td colspan='3'></td></tr>";
          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//
     }
      function table_fill_Treasury() {

          document.getElementById("lblCaption").innerHTML = 'Treasury';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;
          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");
                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",9)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";

                  //tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",9)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr id='tr_" + col2[0] + "' style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  document.getElementById("<%= hid_Rowlength.ClientID %>").value = m + 1;
              }
              tab += "<tr><td colspan='3'></td></tr>";
          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//
      }
      function table_fill_Retail() {
          document.getElementById("lblCaption").innerHTML = 'Credit Monitoring & Administration Management';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;
          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");
                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",7)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
//                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",7)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "' style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  document.getElementById("<%= hid_Rowlength.ClientID %>").value = m + 1;
              }
              tab += "<tr><td colspan='3'></td></tr>";
          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

           //--------------------- Clearing Data ------------------------//


      }
      function table_fill_IT() {
          document.getElementById("lblCaption").innerHTML = 'IT Management';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;
          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");
//                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",8)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "' style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",8)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";

                  document.getElementById("<%= hid_Rowlength.ClientID %>").value = m + 1;
              }
              tab += "<tr><td colspan='3'></td></tr>";
          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//
      }
      function table_fill_cards() {

          document.getElementById("lblCaption").innerHTML = 'Cards and Payments';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");
                  //                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",8)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "' style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",13)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";

                  document.getElementById("<%= hid_Rowlength.ClientID %>").value = m + 1;
              }
              tab += "<tr><td colspan='3'></td></tr>";
          }
//          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Dispute.pptx' target='_blank'><b>Dispute.pptx</b></a></td></tr>";
//          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/ARBITRATION format.docx'target='_blank'><b>Arbitration Request Form </b></a></td></tr>";
//          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/ESAF Shortage Format New.xlsx'target='_blank'><b>ESAF Shortage Format</b></a></td></tr>";


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//
      }
      function table_fill_Audit() {
          document.getElementById("lblCaption").innerHTML = 'Audit Management';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;
          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");
 //                 tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",3)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "' style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",3)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";

                  document.getElementById("<%= hid_Rowlength.ClientID %>").value = m + 1;
              }
              tab += "<tr><td colspan='3'></td></tr>";
          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function table_fill_HD() {

        document.getElementById("lblCaption").innerHTML = 'HelpDesk Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");
         //         tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",4)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "' style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",4)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";

                  document.getElementById("<%= hid_Rowlength.ClientID %>").value = m + 1;
              }
              tab += "<tr><td colspan='3'></td></tr>";
          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function table_fill_Compliance() {

        document.getElementById("lblCaption").innerHTML = 'Compliance Management';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;
          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");
        //          tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",5)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "' style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",5)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";

                  document.getElementById("<%= hid_Rowlength.ClientID %>").value = m + 1;
              }
              tab += "<tr><td colspan='3'></td></tr>";
          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function table_fill_OP() {
         document.getElementById("lblCaption").innerHTML = 'Branch Operations';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;
          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");

                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",6)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                 // tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",6)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "' style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  document.getElementById("<%= hid_Rowlength.ClientID %>").value = m + 1;
              }
              tab += "<tr><td colspan='3'></td></tr>";
          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function table_fill_Foriegn() {
          document.getElementById("lblCaption").innerHTML = 'Foreign Exchange';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;
          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");

                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",10)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
//                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",10)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "' style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
              }
              tab += "<tr><td colspan='3'></td></tr>";
          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function table_fill_Digital() {

          document.getElementById("lblCaption").innerHTML = 'Digital Banking and Alternate Channels ';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");

                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",11)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
//                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",11)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  document.getElementById("<%= hid_Rowlength.ClientID %>").value = m + 1;

              }
              tab += "<tr><td colspan='3'></td></tr>";
          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
          //--------------------- Clearing Data ------------------------//
      }
      function table_fill_customer() {

          document.getElementById("lblCaption").innerHTML = 'Customer Service Quality ';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:200%;'><table style='width:50%; font-size:10pt;'>";
          if (document.getElementById("<%= hid_Hrfolder.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hrfolder.ClientID %>").value.split("¥");
              var ShowQuali = 0;
            
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("^");

                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",12)' id ='btnExpand' value='+'></td><td style='width:20%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  //                  tab += "<div style='width: 50%';><tr><td style = 'width:1%;'><br><input ' type ='button'   onclick='btn_Expand(" + col2[0] + ",11)' id ='btnExpand' value='+'></td><td style='width:5%;height:10px; text-align:left;font-weight:bold;'><br>" + col2[1] + "</td></tr><tr class = 'hrTr' id='tr_" + col2[0] + "'  style='width:5%;height:10px;' ><td style='width:5%'></td><td id='btn_" + col2[0] + "' style='width:30%';></td></tr></div>";
                  document.getElementById("<%= hid_Rowlength.ClientID %>").value = m + 1;

              }
              tab += "<tr><td colspan='3'></td></tr>";
          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
          //--------------------- Clearing Data ------------------------//
      }
      function table_fill_Election() {
          document.getElementById("lblCaption").innerHTML = 'Election Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='3'>No files uploaded in Election Management</td></tr>";


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;


      }
      function table_fill_OFMA() {
          document.getElementById("lblCaption").innerHTML = 'OFMA';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/OFMA.pdf' target='_blank'><b>User Manual</b></a></td></tr>";
        


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;


      }
      function table_fill_CompanyPolicies() {
          document.getElementById("lblCaption").innerHTML = 'Company Policies';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=1' target='_blank'><b>Microfinance and Credit Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=2' target='_blank'><b>Loan Rescheduling Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=3' target='_blank'><b>Client Privacy Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=4' target='_blank'><b>Client Protection Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=5' target='_blank'><b>Grievance  Redressal Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=6' target='_blank'><b>I T Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=7' target='_blank'><b>Product Guidelines</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=8' target='_blank'><b>Operational Manual</b></a></td></tr>";

          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function table_fill_LivelihoodSurvey() {
          document.getElementById("lblCaption").innerHTML = 'Livelihood Services';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/LivelihoodSurveyFormat.pdf' target='_blank'><b>Identification Survey Form </b></a></td></tr>";
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function FromServer(arg, context) {
          document.getElementById("<%= hid_Hrfolder.ClientID %>").value = arg;

          if (context == 1)
                  {
                      table_fill_HR();
                  }
                  else if (context == 2)
                  {
                      table_fill_Risk();
                     
                  }
                  else if (context == 3)
                  {
                      
                      table_fill_Audit();
                  }
                  else if (context == 4)
                  {
                      
                      table_fill_HD();
                  }
                  else if (context == 5)
                  {
                     
                      table_fill_Compliance();
                  }
                  else if (context == 6)
                  {
                      table_fill_OP();
                  }
                  else if (context == 7)
                  {
                      table_fill_Retail();
                  }
                  else if (context == 8) {
               
                      table_fill_IT();
                  }
                  else if (context ==9)
                  {
                      table_fill_Treasury();
                  }
                  else if (context == 15) {
                  //alert(arg);
                  document.getElementById("<%= hid_Display.ClientID %>").value = arg;
                      btn_Display();
                  }
                  else if (context == 10)
                  {
                      table_fill_Foriegn();
                  }
                  else if (context == 11) {
                      table_fill_Digital();
                  }
                  else if (context == 12) {
                      table_fill_customer();
                  }
                  else if (context == 13) {
                 
                      table_fill_cards();
                  }
      }
      function HR() {
          document.getElementById("<%= hid_DeptId.ClientID %>").value = 1;
          ToServer("2Ø1", 1);
      }
      function Risk() {
          document.getElementById("<%= hid_DeptId.ClientID %>").value = 2;
          ToServer("2Ø2" , 2); 

      }

      function Audit() {

          document.getElementById("<%= hid_DeptId.ClientID %>").value = 3;
          ToServer("2Ø3", 3);

      }
      function HelpDesk() {


          document.getElementById("<%= hid_DeptId.ClientID %>").value = 4;
          ToServer("2Ø4", 4);

      }
      function Compliance() {

          document.getElementById("<%= hid_DeptId.ClientID %>").value = 5;
          ToServer("2Ø5", 5);
      }
      function Operations() {

          document.getElementById("<%= hid_DeptId.ClientID %>").value = 6;
          ToServer("2Ø6" , 6); 

      }
      function Retail() {
          document.getElementById("<%= hid_DeptId.ClientID %>").value = 7;
          ToServer("2Ø7", 7); 

      }
      function IT() {
          document.getElementById("<%= hid_DeptId.ClientID %>").value = 8;
          ToServer("2Ø8", 8);

      }
      function Treasury() {
          document.getElementById("<%= hid_DeptId.ClientID %>").value = 9;
          ToServer("2Ø9", 9);

      }
   
      function Foriegn() {
          //          document.getElementById("<%= hid_DeptId.ClientID %>").value = 10;
          //          ToServer("2Ø10", 10);


          document.getElementById("lblCaption").innerHTML = 'Foreign Exchange';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";

          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Foreign Exchange  Purchase Application [SFB_05_27_09_17].pdf' target='_blank'><b>Application for purchase of Foreign Exchange</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/currency features.zip' target='_blank'><b>Currency Features</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Cash Memo.pdf' target='_blank'><b>Cash Memo</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Encashment Certificate new.pdf' target='_blank'><b>Encashment Certificate new</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Form_A2.pdf' target='_blank'><b>Form_A2</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/LRS Application cum declaration.pdf' target='_blank'><b>LRS Application cum declaration</b></a></td></tr>";


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;


      }
      function Digital() {
          document.getElementById("<%= hid_DeptId.ClientID %>").value = 11;
          ToServer("2Ø11" , 11);

      }
      function Customer() {
                  
          document.getElementById("<%= hid_DeptId.ClientID %>").value = 12;
          ToServer("2Ø12", 12);

      }
      function Cards() {

          document.getElementById("<%= hid_DeptId.ClientID %>").value = 13;
          ToServer("2Ø13", 13);

      }
      function NPS() {


          document.getElementById("<%= hid_DeptId.ClientID %>").value = 6;


          table_fill_NPS();
      }
      function OFMA() {


          document.getElementById("<%= hid_DeptId.ClientID %>").value = 6;


          table_fill_OFMA();
      }
     

      function Election() {
          

          document.getElementById("<%= hid_Type.ClientID %>").value = 7;


          table_fill_Election();
      }

      function CompanyPolicies() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 8;
          table_fill_CompanyPolicies();
      }
      function LivelihoodSurvey() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 9;
          table_fill_LivelihoodSurvey();
      }

      function Legal() {

          document.getElementById("lblCaption").innerHTML = 'Legal Department';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";

          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Legal/Documentation_Manual.aspx' target='_blank'><b>Documentation Manual</b></a></td></tr>";
          
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

      }
  </script>
    </head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            
  <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">&nbsp; <a href="home.aspx" class="btn btn-danger square-btn-adjust">Back</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                <asp:Panel ID="pnlImg" runat="server"></asp:Panel>
                    
					</li>
				
					
                    <li onclick ="HR()"  >
                        <a  href="#"> HR Management <span style="float:right;"> &raquo;</span></a>
                    </li>
                     <li  onclick ="Audit()" >
                        <a  href="#"> Audit Management <span style="float:right;"> &raquo;</span></a>
                    </li>
                    <li  onclick ="HelpDesk()" style="top: 2px; left: 0px">
                        <a  href="#"> HelpDesk Management <span style="float:right;"> &raquo;</span></a>
                    </li>
				    <li  onclick ="Compliance()" >
                        <a   href="#"> Compliance Management <span style="float:right;"> &raquo;</span></a>
                    </li>	
                     <li  onclick ="Operations()" >
                        <a   href="#"> Branch Operations <span style="float:right;"> &raquo;</span></a>
                    </li>
                    <li  onclick ="Foriegn()" >
                        <a   href="#"> Foreign Exchange <span style="float:right;"> &raquo;</span></a>
                    </li>
                    <li  onclick ="Risk()" >
                        <a   href="#"> Risk Management <span style="float:right;"> &raquo;</span></a>
                    </li>
                    <li  onclick ="Retail()" >
                        <a   href="#"> Retail Management <span style="float:right;"> &raquo;</span></a>
                    </li>
                    <li  onclick ="IT()" >
                        <a   href="#"> IT <span style="float:right;"> &raquo;</span></a>
                    </li>
                    <li  onclick ="Treasury()" >
                        <a   href="#"> Treasury <span style="float:right;"> &raquo;</span></a>
                    </li>
                    <li  onclick ="Digital()" >
                        <a   href="#"> Digital Banking and Alternate Channels<span style="float:right;"> &raquo;</span></a>
                    </li>
                    <li  onclick ="Customer()" >
                        <a   href="#"> Customer Service Quality<span style="float:right;"> &raquo;</span></a>
                    </li>
                       <li  onclick ="Cards()" >
                        <a   href="#"> Cards and Payments<span style="float:right;"> &raquo;</span></a>
                    </li>
                       <li  onclick ="Legal()" >
                        <a   href="#"> Legal Department<span style="float:right;"> &raquo;</span></a>
                    </li>
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                    <h3 style='width:100%;color:#E31E24;font-size:larger;'><b>Document Downloads</h3><br/><br/><h5>Choose the download that you want </h5>
                     <h4><label id="lblCaption" style="font-size:12pt;font-weight:normal;color:#E31E24;"></label><asp:HiddenField ID="hid_image" runat="server" />
                        <asp:HiddenField ID="hid_User" runat="server" />
                        <asp:HiddenField ID="hid_EmpCode" runat="server" />
                        <asp:HiddenField ID="hid_Leave" runat="server" />
                        <asp:HiddenField ID="hid_Promotion" runat="server" />
                        <asp:HiddenField ID="hid_HR" runat="server" />
                        <asp:HiddenField ID="hid_dtls" runat="server" />
                        <asp:HiddenField ID="hdnSubID" runat="server" />
                        <asp:HiddenField ID="hdnFromDt" runat="server" />
                        <asp:HiddenField ID="hdnToDt" runat="server" />
                        
                        <asp:HiddenField ID="hid_Transfer" runat="server" />
                        <asp:HiddenField ID="hid_Type" runat="server" />
                        <asp:HiddenField ID="hid_Job" runat="server" />
                        <asp:HiddenField ID="hid_Compliant" runat="server" />
                        <asp:HiddenField ID="hid_Risk" runat="server" />
                        <asp:HiddenField ID="hid_Audit" runat="server" />
                         <asp:HiddenField ID="hid_Foreign" runat="server" />
                        <asp:HiddenField ID="hid_Hd" runat="server" />
                        <asp:HiddenField ID="hid_Com" runat="server" />
                        <asp:HiddenField ID="hid_Op" runat="server" />
                        <asp:HiddenField ID="hid_for" runat="server" />
                        <asp:HiddenField ID="hid_IT" runat="server" />
                        <asp:HiddenField ID="hid_Treasury" runat="server" />
                        <asp:HiddenField ID="hid_Hrfolder" runat="server" />
                        <asp:HiddenField ID="hid_DeptId" runat="server" />         
                        <asp:HiddenField ID="hid_Display" runat="server" />
                        <asp:HiddenField ID="hid_Fid" runat="server" />
                            <asp:HiddenField ID="hid_Card" runat="server" />
                        <asp:HiddenField ID="hid_Rowlength" runat="server" />
                        </h4>   
                        
                                               
                <br /><asp:Panel ID="pnlDtls" runat="server"></asp:Panel>
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                              
                           
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <!-- METISMENU SCRIPTS -->
    
    
   
    </form>
    
   
</body>
</html>
</asp:Content>
