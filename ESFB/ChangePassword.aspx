﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChangePassword.aspx.vb" Inherits="ChangePassword" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Change Password</title>
<link href="Style/Style.css" type="text/css" rel="Stylesheet"/>
<style type="text/css">
body 
{
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #B21C32;
	background-color:#b21c32;
	line-height: 18px;
	height: 100%;
	margin-top: 15px;
}
        .style1 
	{
            width: 100%;
        }
        .style2 
        {
        	width: 25%;
        }
        .style3
        {
            font-size: x-large;
            font-family: Cambria;
            color: #000066;
        }
        .style6
        {
            width: 25%;
            font-size: x-large;
            font-family: Cambria;
            color: #000066;
        }
        .style7
        {
            font-family: Cambria;
            font-size: medium;
            text-align: left;
        }
        .style12
        {
        	width: 25%;
            height: 23px;
            text-align: left;
            font-family: Georgia;
        }
        .style13
        {
            width: 12%;
            height: 23px;
            text-align: right;
            font-family: Georgia;
        }
        .style15
        {
            width: 25%;
            height: 30px;
            text-align: center;
            font-family: Georgia;
        }
        .style16
        {
            width: 12%;
            height: 29px;
            text-align: right;
            font-family: Cambria;
        }
        .style17
        {
        	width: 25%;
            height: 30px;
            text-align: right;
            font-family: Cambria;
        }
        .style18
        {
            width: 25%;
            height: 30px;
            text-align: left;
        }
        .style19
        {
            font-family: "Times New Roman", Times, serif;
            font-size: medium;
           
        }
        .style20
        {
            width: 25%;
            height: 29px;
            text-align: center;
            color: #999999;
            font-family: "Times New Roman", Times, serif;
            font-size: small;
        }
        VeryPoorStrength
        {
            background: Red;
            color:white;
            font-weight:bold;
        }
        .WeakStrength
        {
            background: Red;
            color:White;
            font-weight:bold;
        }
        .AverageStrength
        {
            background: orange;
            color:black;
            font-weight:bold;
        }
        .GoodStrength
        {
            background: blue;
            color:White;
            font-weight:bold;
        }
        .ExcellentStrength
        {
            background: Green;
            color:White;
            font-weight:bold;
        }
        .BarBorder
        {
            border-style: solid;
            border-width: 0px;
            width: 180px;
            padding:0px;
        }
        .BarIndicator
        {
	        height:1px;
        }
        .style21
        {
            width: 12%;
            height: 25px;
            text-align: right;
            font-family: Cambria;
        }
        .style22
        {
            width: 25%;
            height: 25px;
            text-align: left;
        }
        .style26
        {
            width: 25%;
            height: 29px;
            text-align: center;
        }
        .style27
        {
            width: 12%;
            height: 26px;
            text-align: right;
            font-family: Georgia;
        }
        .style28
        {
            width: 25%;
            height: 26px;
            text-align: left;
        }
        .style29
        {
            font-family: Georgia;
            font-size: small;
        }
        .style30
        {
            font-family: Georgia;
        }
    </style>
<script type="text/javascript" src="Script/jquery-1.2.6.min.js"></script>
<script src="Script/Validations.js" language="javascript" type="text/javascript"> </script>  
<script language="javascript" type="text/javascript">
    // <!CDATA[


    function isValid() {       
        if (document.getElementById("<%= txtUserID.ClientID %>").value == "")
        {document.getElementById("<%= lblErrorMessage.ClientID %>").innerHTML = "UserID can't be Empty!!!"; document.getElementById("<%= txtUserID.ClientID %>").focus(); return false; }
        if (document.getElementById("<%= txtCurrentPassword.ClientID %>").value.trim() == "")
        { document.getElementById("<%= lblErrorMessage.ClientID %>").innerHTML = "CurrentPassword can't be Empty!!!"; document.getElementById("<%= txtCurrentPassword.ClientID %>").focus(); return false; }
        if (document.getElementById("<%= txtNewPassword.ClientID %>").value.trim() == "")
        { document.getElementById("<%= lblErrorMessage.ClientID %>").innerHTML = "NewPassword can't be Empty!!!"; document.getElementById("<%= txtNewPassword.ClientID %>").focus(); return false; }
        if (document.getElementById("<%= txtConfirmPassword.ClientID %>").value.trim() == "")
        { document.getElementById("<%= lblErrorMessage.ClientID %>").innerHTML = "ConfirmPassword can't be Empty!!!"; document.getElementById("<%= txtConfirmPassword.ClientID %>").focus(); return false; }
        if (document.getElementById("<%= txtNewPassword.ClientID %>").value != document.getElementById("<%= txtConfirmPassword.ClientID %>").value)
        { document.getElementById("<%= lblErrorMessage.ClientID %>").innerHTML = "Passwords Do not Match"; document.getElementById("<%= txtConfirmPassword.ClientID %>").focus(); return false; }
        var Len = document.getElementById("<%= txtNewPassword.ClientID %>").value.toString().length;
        if (Len > 20)
        { document.getElementById("<%= lblErrorMessage.ClientID %>").innerHTML = "Maximum Length is Twenty"; document.getElementById("<%= txtConfirmPassword.ClientID %>").focus();  return false; }
        if (!ValidPassword())
            return false;
        //        document.getElementById("<%= hdnValue.ClientID %>").value = document.getElementById("<%= txtUserID.ClientID %>").value + "ử" + document.getElementById("<%= txtCurrentPassword.ClientID %>").value + "ử" + document.getElementById("<%= txtNewPassword.ClientID %>").value;
        document.getElementById("<%= hdnValue.ClientID %>").value = document.getElementById("<%= txtUserID.ClientID %>").value + "ử" + document.getElementById("<%= txtCurrentPassword.ClientID %>").value + "ử" + document.getElementById("<%= hidNewPwd.ClientID %>").value;
       
    }

    function ValidPassword() {
        if (document.getElementById("<%= hidNewPwd.ClientID %>").value != "") {
            var pwFlg = 0;
            var regex = new RegExp("(?=.{6,12})");
            var b = regex.test(document.getElementById("<%= hidNewPwd.ClientID %>").value);
            if (b) {
                pwFlg = pwFlg + 1
            }
            regex = new RegExp("(?=.*[a-z])");
            b = regex.test(document.getElementById("<%= hidNewPwd.ClientID %>").value);
            if (b == true) {
                pwFlg = pwFlg + 1
            }
            regex = new RegExp("(?=.*[A-Z])");
            b = regex.test(document.getElementById("<%= hidNewPwd.ClientID %>").value);
            if (b == true) {
                pwFlg = pwFlg + 1
            }
            regex = new RegExp("(?=.*[0-9])");
            b = regex.test(document.getElementById("<%= hidNewPwd.ClientID %>").value);
            if (b == true) {
                pwFlg = pwFlg + 1
            }
            regex = new RegExp("(?=.*[!~`*^@#$%])");
            b = regex.test(document.getElementById("<%= hidNewPwd.ClientID %>").value);
            if (b == true) {
                pwFlg = pwFlg + 1
            }

            if (pwFlg < 4) {
                alert("Password does not meet policies");
                document.getElementById("<%= txtNewPassword.ClientID %>").select();
                return false;
            }
            else return true;
        }

    }
    $('#form1').disableAutoFill();
    function NewPwdOnChange() {
        document.getElementById("<%= hidNewPwd.ClientID %>").value = document.getElementById("<%= txtNewPassword.ClientID %>").value;
        document.getElementById("<%= txtNewPassword.ClientID %>").value = "00000000000000000000";
    }
    function ConfPwdOnChange() {
        document.getElementById("<%= hidNewPwd.ClientID %>").value = document.getElementById("<%= txtNewPassword.ClientID %>").value;
        document.getElementById("<%= txtNewPassword.ClientID %>").value = "00000000000000000000";
        document.getElementById("<%= hidConfPwd.ClientID %>").value = document.getElementById("<%= txtConfirmPassword.ClientID %>").value;
        document.getElementById("<%= txtConfirmPassword.ClientID %>").value = "00000000000000000000";
    }
    function btnConfirm_Click() {
        document.getElementById("<%= hidNewPwd.ClientID %>").value = document.getElementById("<%= txtNewPassword.ClientID %>").value;
        document.getElementById("<%= txtNewPassword.ClientID %>").value = "00000000000000000000";
        document.getElementById("<%= hidConfPwd.ClientID %>").value = document.getElementById("<%= txtConfirmPassword.ClientID %>").value;
        document.getElementById("<%= txtConfirmPassword.ClientID %>").value = "00000000000000000000";
        document.getElementById("<%= btnSave.ClientID %>").disabled = false;
    }

    function btnNo_Click() {
        document.getElementById("<%= hidNewPwd.ClientID %>").value = "";
        document.getElementById("<%= txtNewPassword.ClientID %>").value = "";
        document.getElementById("<%= hidConfPwd.ClientID %>").value = "";
        document.getElementById("<%= txtConfirmPassword.ClientID %>").value = "";
        document.getElementById("<%= txtCurrentPassword.ClientID %>").value = "";
        document.getElementById("<%= btnSave.ClientID %>").disabled = true;
    }
    // ]]>
</script>
</head>
<body style="background-color:#E31E24">
<form id="form1" runat="server">
<div style="height:150px; "><img src="Image/Logo.png" style="width:450px; height:150px;">
</div>
<div id="MainContents" style="background-color:White; height:370px; width:100%">
    <table align="center" class="style1">
        <tr>
            <td class="style6" colspan="2">
                &nbsp;</td>
            <td class="style2">                <asp:HiddenField ID="hdnValue" runat="server" />
                &nbsp;</td>
            <td class="style2">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style6" colspan="3">
                &nbsp;Change Password</td>
            <td class="style2">
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2" colspan="2">
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <%--<td class="style2" colspan="3">--%>
            <td class="style2" colspan="3" id="head" runat="server">
                <span class="style7">Note: you can&#39;t reuse your old password once you change it!</span> </td>
            <td class="style2">
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr style="border-top-style: solid; border-top-width: thin; border-top-color: #C0C0C0">
            <td class="style2" colspan="5" 
                    
                style="border-top-style: solid; border-top-width: thin; border-top-color: #C0C0C0">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style27">
                </td>
            <td class="style27">
                <span class="style29">User ID</span><span> :</span></td>
            <td class="style28">
                <asp:TextBox ID="txtUserID" runat="server" Width="90%" MaxLength="5" onkeypress="return NumericCheck();"></asp:TextBox>
            </td>
            <td class="style28">
                </td>
            <td class="style28">
                </td>
        </tr>
        <tr>
            <td class="style13" colspan="2">
                Current Password/OTP : </td>
            <td class="style12">
                <asp:TextBox ID="txtCurrentPassword" runat="server" Width="90%" MaxLength="15" 
                    TextMode="Password"></asp:TextBox>
                </td>
            <td class="style12">
                <span class="style19">
            <a href="PasswordHelp.aspx" target="_self">Tips 
                for a Secure Password</a></span> 
                </td>
            <td class="style12">
                </td>
        </tr>
        <tr>
            <td class="style13" colspan="2">
                New Password :</td>
                <td class="style12">
                    <%--<asp:TextBox ID="txtNewPassword" runat="server" Width="90%" MaxLength="20" 
                        TextMode="Password" ></asp:TextBox>    
                   <asp:PasswordStrength ID="PasswordStrength1" TargetControlID="txtNewPassword" StrengthIndicatorType="BarIndicator" PrefixText="Strength:" HelpStatusLabelID="lblhelp" PreferredPasswordLength="6" MinimumLowerCaseCharacters=1 MinimumUpperCaseCharacters=1
                    MinimumNumericCharacters="1" MinimumSymbolCharacters="1" BarBorderCssClass="BarBorder" BarIndicatorCssClass="BarIndicator" CalculationWeightings="25;25;25;25" TextStrengthDescriptionStyles="VeryPoorStrength;WeakStrength;
                    AverageStrength;GoodStrength;ExcellentStrength" runat="server"   /> --%>
                    <asp:TextBox ID="txtNewPassword"  runat="server" Width="90%" MaxLength="20" 
                        autocomplete="new-password" type="password" onfocus="this.removeAttribute('readonly');"></asp:TextBox>    
                   <asp:PasswordStrength ID="PasswordStrength1" TargetControlID="txtNewPassword" StrengthIndicatorType="BarIndicator" PrefixText="Strength:" HelpStatusLabelID="lblhelp" PreferredPasswordLength="8" MinimumLowerCaseCharacters="1" MinimumUpperCaseCharacters="1"
                    MinimumNumericCharacters="1" MinimumSymbolCharacters="1" RequiresUpperAndLowerCaseCharacters="true" BarBorderCssClass="BarBorder" BarIndicatorCssClass="BarIndicator" CalculationWeightings="25;25;25;25" TextStrengthDescriptionStyles="VeryPoorStrength;WeakStrength;
                    AverageStrength;GoodStrength;ExcellentStrength" runat="server"   /> 
                    </td>
            <td class="style12">  
               
                &nbsp;</td>
            <td class="style12">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="2">
                Confirm Password : </td>
            <td class="style12">
                <asp:TextBox ID="txtConfirmPassword" runat="server" Width="90%" MaxLength="20" 
                    TextMode="Password"></asp:TextBox>
                </td>
            <td class="style12" colspan="2">
               
                <asp:Label ID="lblhelp" runat="server"></asp:Label>
            </td>
        </tr>
         <tr>
            <td class="style13" colspan="2">
                Do you want to change your password ?
            </td>
            <td class="style12" >
                <%--<asp:Button ID="btnConfirmPassword" runat="server" Text="Yes" Width="9%" 
                    style="height: 26px" />
                <asp:Button ID="btnNoPassword" runat="server" Text="No" Width="9%" 
                    style="height: 26px" />--%>
                <input type="button" id="btnConfirmPassword" onclick="btnConfirm_Click()" style="width:9%;height:26px;" value="YES" />
                <input type="button" id="btnNoPassword" onclick="btnNo_Click()" style="width:9%;height:26px;" value="NO" />
            </td>
        </tr>
        <tr>
            <td class="style15" colspan="2">
                </td>
            <td class="style15">
                <asp:Label ID="lblErrorMessage" runat="server" Font-Names="Cambria" 
                    Font-Size="11pt" ForeColor="Red"></asp:Label>
            </td>
            <td class="style18">
                </td>
            <td class="style18">
                </td>
        </tr>
        <tr>
            <td class="style17" colspan="2">
                <asp:Button ID="btnSave" runat="server" Text="Save" Width="25%" 
                    onclick="btnSave_Click" style="height: 26px" />
            </td>
            <td class="style18">
                <asp:Button ID="btnClose" runat="server" Text="Close" Width="25%" 
                    onclick="btnClose_Click" style="height: 26px" />
            </td>
            <td class="style18">
                </td>
            <td class="style18">
                <asp:HiddenField ID="HiddenField1" runat="server" />
                <asp:HiddenField ID="hidNewPwd" runat="server" />
                <asp:HiddenField ID="hidConfPwd" runat="server" />
                </td>
        </tr>
    </table>
</div>
<div id="FooterContainer" style="float:right;">
</div>
</form>
</body>
</html>

