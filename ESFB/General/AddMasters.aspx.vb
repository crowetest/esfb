﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class General_AddMasters
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Disposed"
    Protected Sub General_AddMasters_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 187) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Me.Master.subtitle = "Master Maintenance"
            If Not IsPostBack Then
                DT = GF.GetQueryResult("select -1,'---NEW---' zone_name union all  select zone_id,zone_name from zone_master order by zone_name")
                GF.ComboFill(cmbHead, DT, 0, 1)
                Me.txtCode.Text = CStr(102)
                Me.txtName.Text = CStr("George K John")
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.cmbType.Attributes.Add("onchange", "return TypeOnChange()")
            Me.cmbHead.Attributes.Add("onchange", "return HeadOnChange()")
            Me.txtCode.Attributes.Add("onchange", "return CodeOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub General_AddMasters_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim SQL As String = ""
        Dim SQL1 As String = ""
        Select Case CInt(Data(0))
            Case 1
                Dim TypeID As Integer = CInt(Data(1))
                Select Case TypeID
                    Case 1
                        SQL = "select -1,'---NEW---' zone_name union all  select zone_id,zone_name from zone_master order by zone_name"
                        SQL1 = "select -1,'---SELECT---' zone_name union all  select zone_id,zone_name from zone_master order by zone_name"
                    Case 2
                        SQL = "select -1,'---NEW---' region_name union all  select region_id,region_name from region_master order by region_name"
                        SQL1 = "select -1,'---SELECT---' zone_name union all  select zone_id,zone_name from zone_master order by zone_name"
                    Case 3
                        SQL = "select -1,'---NEW---' area_name union all  select area_id,area_name from area_master order by area_name"
                        SQL1 = "select -1,'---SELECT---' region_name union all  select region_id,region_name from region_master order by region_name"
                End Select
                DT = GF.GetQueryResult(SQL)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
                CallBackReturn += "Ø"
                DT = GF.GetQueryResult(SQL1)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2
                Dim TypeID As Integer = CInt(Data(1))
                Dim HeadID As Integer = CInt(Data(2))
                Select Case TypeID
                    Case 1
                        SQL = "select zone_head,emp_name,a.zone_name,a.zone_id,isnull(a.Email_ID,'') from zone_master a left outer join emp_master b on ( a.zone_head = b.emp_code) where a.zone_id = " & HeadID & ""
                    Case 2
                        SQL = "select region_head,emp_name,a.region_name,a.zone_id,isnull(a.Email_ID,'') from region_master a left outer join emp_master b on ( a.region_head = b.emp_code) where a.region_id = " & HeadID & ""
                    Case 3
                        SQL = "select area_head,emp_name,a.area_name,a.region_id,isnull(a.Email_ID,'')  from area_master a left outer join emp_master b on (  a.area_head = b.emp_code) where a.area_id = " & HeadID & ""
                End Select
                DT = GF.GetQueryResult(SQL)
                If DT.Rows.Count > 0 Then
                    CallBackReturn = DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString()
                End If
            Case 3
                Dim EmpCode As Integer = CInt(Data(1))
                SQL = "select emp_name from emp_master where emp_code=" & EmpCode & " and status_id = 1"
                DT = GF.GetQueryResult(SQL)
                If DT.Rows.Count > 0 Then
                    CallBackReturn = DT.Rows(0)(0).ToString()
                End If
            Case 4
                '"4Ø" + CategoryID + "Ø" + TypeID + "Ø" + SubTypeID + "Ø" + SubTypeName + "Ø" + HeadCode + "Ø" + Flag;
                Dim CategoryID As Integer = CInt(Data(1))   'Region/Area/Territory
                Dim TypeID As Integer = CInt(Data(2))
                Dim SubType As Integer = CInt(Data(3))
                Dim SubTypeName As String = CStr(Data(4))
                Dim EmpCode As Integer = CInt(Data(5))
                Dim Flag As Integer = CInt(Data(6))
                Dim EmailID As String = Data(7)
                Dim ErrorFlag As Integer = 0
                Dim Message As String = Nothing
                Try
                    Dim Params(9) As SqlParameter
                    Params(0) = New SqlParameter("@CategoryID", SqlDbType.Int)
                    Params(0).Value = CategoryID
                    Params(1) = New SqlParameter("@TypeID", SqlDbType.Int)
                    Params(1).Value = TypeID
                    Params(2) = New SqlParameter("@SubType", SqlDbType.Int)
                    Params(2).Value = SubType
                    Params(3) = New SqlParameter("@SubTypeName", SqlDbType.VarChar, 50)
                    Params(3).Value = SubTypeName
                    Params(4) = New SqlParameter("@EmpCode", SqlDbType.Int)
                    Params(4).Value = EmpCode
                    Params(5) = New SqlParameter("@Flag", SqlDbType.Int)
                    Params(5).Value = Flag
                    Params(6) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(6).Value = CInt(Session("UserID"))
                    Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(7).Direction = ParameterDirection.Output
                    Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(8).Direction = ParameterDirection.Output
                    Params(9) = New SqlParameter("@EmailID", SqlDbType.VarChar, 50)
                    Params(9).Value = EmailID
                    DB.ExecuteNonQuery("SP_MASTER_MAINTENANCE", Params)
                    Message = CStr(Params(8).Value)
                    ErrorFlag = CInt(Params(7).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString() + "ʘ" + Message.ToString()
        End Select
    End Sub
#End Region

End Class
