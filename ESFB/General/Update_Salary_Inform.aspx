﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Update_Salary_Inform.aspx.vb" Inherits="Update_Salary_Inform" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head>
<title></title>
<link href="../Style/Style.css" rel="stylesheet" type="text/css" />
<link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
<link href="../Style/datepicker.css" rel="stylesheet"/>
<link href="../Style/jquery.datepick.css" rel="stylesheet"/>

<script type="text/javascript" src="../Script/jquery.js"></script>
<script type="text/javascript" src="../Script/jquery.datepick.js"></script>
<script src="../Script/Validations.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date').datepick();
    });
</script>

<script language="javascript" type="text/javascript">
   
    function table_fill() {
            document.getElementById("<%= pnlDisplay.ClientID %>").style.display = ''; 
            var monthNames = ["January", "February", "March", "April", "May", "June",  "July", "August", "September", "October", "November", "December"];
            var d = new Date();
            var mo=monthNames[new Date().getMonth()];
            document.getElementById("<%= hid_Month.ClientID %>").value=new Date().getMonth()+1;
            document.getElementById("<%= hid_Year.ClientID %>").value=new Date().getFullYear();
            var y=new Date().getFullYear();
            var titl='Salary Input Sheet for the Month ' + mo + ' ' + y ;
            var row_bg = 0;
            var tab = "";
            
             tab += "<div style='width:100%;background-color:#A34747; height:auto; overflow:auto; margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
            
            tab += "<tr height=100px;  class='mainhead'>"; 
            tab += "<td style='width:93%;text-align:center' ><b>" + titl + " </b></td>";
            tab += "<td style='width:7%;text-align:center'><a href='Birthday_Entries.aspx?' style='text-align:right;' target='_blank' >"  + "HELP"+ "</td>";
            tab += "</tr>";
            tab += "<div style='width:100%;background-color:#A34747; height:auto; overflow:auto; margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
            tab += "<tr height=100px;  class='sub_first'>"; 
            tab += "<td style='width:1%;text-align:center; ' ></td>";
            tab += "<td style='width:16%;text-align:center; ' ></td>";
            tab += "<td style='width:0%;text-align:center; ' ></td>";
            tab += "<td style='width:0%;text-align:center; ' ></td>";
            tab += "<td style='width:25%;text-align:center; ' >STATUS</td>";
            tab += "<td style='width:0%;text-align:center; ' ></td>";
            tab += "<td style='width:0%;text-align:center; ' ></td>";
            tab += "<td style='width:15%;text-align:center; ' >LEAVE</td>";
            tab += "<td style='width:0%;text-align:center; ' ></td>";
            tab += "<td style='width:0%;text-align:center; ' ></td>";
            tab += "<td style='width:16%;text-align:center; ' >ARREARS'</td>";
            tab += "<td style='width:0%;text-align:center; ' ></td>";
            tab += "<td style='width:0%;text-align:center; ' ></td>";
            tab += "<td style='width:16%;text-align:center; ' >ADDITIONAL PAYMENT</td>";
            tab += "<td style='width:0%;text-align:center; ' ></td>";
            tab += "<td style='width:0%;text-align:center; ' ></td>";
            tab += "<td style='width:12%;text-align:center; ' >BANK DETAILS</td>";
            
            tab += "<td style='width:2%;text-align:center; ' ></td>";
            tab += "</tr>";

            tab += "<div style='width:100%;background-color:#A34747; height:auto; overflow:auto; margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
            tab += "<tr height=100px;  class='mainhead'>";
            tab += "<td style='width:2%;text-align:center' >Slno</td>";
            tab += "<td style='width:3%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:7%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:4%;text-align:center' >Employee Status</td>";
            tab += "<td style='width:0%;text-align:center' ></td>";
            tab += "<td style='width:0%;text-align:center' ></td>";
            tab += "<td style='width:8%;text-align:center' >Current Status</td>";            
            tab += "<td style='width:6%;text-align:center' >Date</td>";
            tab += "<td style='width:11%;text-align:center' >Remark</td>";
            tab += "<td style='width:0%;text-align:center' ></td>";
            tab += "<td style='width:0%;text-align:center' ></td>";
            tab += "<td style='width:2%;text-align:center' >LOP</td>";
            tab += "<td style='width:2%;text-align:center' >CL</td>";
            tab += "<td style='width:2%;text-align:center' >ML/PL</td>";
            tab += "<td style='width:2%;text-align:center' >PL</td>";
            tab += "<td style='width:2%;text-align:center' >SL</td>";
            tab += "<td style='width:2%;text-align:center' >ESI</td>";
            tab += "<td style='width:3%;text-align:center' >Others</td>";
            tab += "<td style='width:0%;text-align:center' ></td>";
            tab += "<td style='width:0%;text-align:center' ></td>";
            tab += "<td style='width:5%;text-align:center' >Amount</td>";
            tab += "<td style='width:11%;text-align:center' >Remark</td>";
            tab += "<td style='width:0%;text-align:center' ></td>";
             tab += "<td style='width:0%;text-align:center' ></td>";
            tab += "<td style='width:5%;text-align:center' >Amount</td>";
            tab += "<td style='width:11%;text-align:center' >Remark</td>";
            tab += "<td style='width:0%;text-align:center' ></td>";
            tab += "<td style='width:0%;text-align:center' ></td>";
            tab += "<td style='width:6%;text-align:center' >IFSC CODE</td>";
            tab += "<td style='width:6%;text-align:center' >Account No.</td>";
            
            
           
            
            
            
            
            tab += "</tr>";
            var row = document.getElementById("<%= hid_Value.ClientID %>").value.split("¥"); 
            var i=0;
            for (n = 0; n <= row.length - 2; n++) {
                var col = row[n].split("~");
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr  class='sub_first';>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class='sub_first';>";
                }
                i=i+1;
                tab += "<td style='width:2%;text-align:center' >" + i + "</td>";
                var txtEmpCode= "<input type=text id='txtEmpCode" + i + "' name='txtEmpCode" + i + "'   style='width:99%;text-align:center;background-color:#FFFFF0;border:none; ' class='NormalText' ReadOnly='true' value=" + col[0] + " />";
                tab += "<td style='width:3%;text-align:center;border:none;' >"+ txtEmpCode +"</td>";
                tab += "<td style='width:7%;text-align:left' class='NormalText' >" + col[1] + "</td>";
                tab += "<td style='width:4%;text-align:center' >" + col[19] + "</td>";




                var txtCLL= "<input type=text id='txtCLL" + i + "' name='txtCLL" + i + "'   style='width:0%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[19] +" MaxLength='50' disabled='true'  class='NormalText' />";
                tab += "<td style='width:0%;text-align:center' >"+ txtCLL +"</td>";                
                var txtCLB= "<input type=text id='txtCLB" + i + "' name='txtCLB" + i + "'   style='width:0%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[25] +" MaxLength='50' disabled='true'  class='NormalText' />";
                tab += "<td style='width:0%;text-align:center' >"+ txtCLB +"</td>";




                var sel=-1;
                var select = "<select id='cmbType" + i + "' name='cmbType" + i + "'  style='width:99%;text-align:left;background-color:#FFFFF0;border:none;'  >";
                if (col[4]==0) {
                    if(col[18]==4 )
                        sel=2;
                    else if (col[18]==2 )
                        sel=3;
                    else
                        sel=-1;
                }
                else
                    sel=col[4];
                if (sel==-1)
                    select += "<option value=-1 selected=true>-----Select------</option>";
                else
                    select += "<option value=-1>-----Select------</option>";
                if (sel==1)
                    select += "<option value=1 selected=true>New Recurite</option>";
                else
                    select += "<option value=1>New Recurite</option>";
                if (sel==4)
                    select += "<option value=4 selected=true>Confirmed</option>";
                else
                    select += "<option value=4>Confirmed</option>";
                if (sel==5)
                    select += "<option value=5 selected=true>Promotion</option>";
                else
                    select += "<option value=5>Promotion</option>";
                if (sel==6)
                    select += "<option value=6 selected=true>Transfer</option>";
                else
                    select += "<option value=6>Transfer</option>";
                if (sel==7)
                    select += "<option value=7 selected=true>Resigned</option>";
                else
                    select += "<option value=7>Resigned</option>";
                tab += "<td style='width:8%;text-align:left'>" + select + "</td>";
               
               



                if (col[5]!='1/1/1900')
                    var txtDate= "<input type=text id='txtDate" + i + "' name='txtDate" + i + "'   style='width:99%;text-align:center;background-color:#FFFFF0; border:none;' value="+ col[5] +" ReadOnly='true'  class='date' onblur=DateOnChange(" + i + ") />";
                else
                        var txtDate= "<input type=text id='txtDate" + i + "' name='txtDate" + i + "'   style='width:99%;text-align:center; background-color:#FFFFF0;border:none;'  ReadOnly='true'  class='date' onblur=DateOnChange(" + i + ") />";
                     
                tab += "<td style='width:6%;text-align:center' >"+ txtDate +"</td>";

                if(col[30]=='null')
                    var txtStatus_Remark="<textarea id='txtStatus_Remark" + i + "' name='txtStatus_Remark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;'  maxlength='1000' onkeypress='return TextAreaCheck(event)'  ></textarea>";
                else
                    var txtStatus_Remark="<textarea id='txtStatus_Remark" + i + "' name='txtStatus_Remark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;'   maxlength='1000' onkeypress='return TextAreaCheck(event)' >"+ col[30] +"</textarea>";
                tab += "<td style='width:11%;text-align:center' >"+ txtStatus_Remark +"</td>";
                
                
                var txtPLL= "<input type=text id='txtPLL" + i + "' name='txtPLL" + i + "'   style='width:0%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[21] +" MaxLength='50' disabled='true'  class='NormalText' />";
                tab += "<td style='width:0%;text-align:center' >"+ txtPLL +"</td>";
                var txtPLB= "<input type=text id='txtPLB" + i + "' name='txPLB" + i + "'   style='width:0%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[27] +" MaxLength='50' disabled='true'  class='NormalText' />";
                tab += "<td style='width:0%;text-align:center' >"+ txtPLB +"</td>";
                
                if (col[6]==0)
                    if (col[24]==0)
                        var txtLOP= "<input type=text id='txtLOP" + i + "' name='txtLOP" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none; '   class='NormalText' MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                    else
                        var txtLOP= "<input type=text id='txtLOP" + i + "' name='txtLOP" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none; ' value="+ col[24] +"  class='NormalText' MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                else
                    var txtLOP= "<input type=text id='txtLOP" + i + "' name='txtLOP" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none; ' value="+ col[6] +" class='NormalText' MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                tab += "<td style='width:2%;text-align:right' >"+ txtLOP +"</td>";
                
                if (col[7]==0)
                    if (col[20]==0)
                        var txtCL= "<input type=text id='txtCL" + i + "' name='txtCL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'  MaxLength='2' onkeypress='return NumericCheck(event)' />";
                    else
                        var txtCL= "<input type=text id='txtCL" + i + "' name='txtCL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' value="+ col[20] +"  MaxLength='2' onkeypress='return NumericCheck(event)' />";

                else
                    var txtCL= "<input type=text id='txtCL" + i + "' name='txtCL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' value="+ col[7] +"  MaxLength='2' onkeypress='return NumericCheck(event)' />";
                tab += "<td style='width:2%;text-align:center' >"+ txtCL +"</td>";
                if (col[8]==0)
                    if (col[23]==0)
                        var txtML= "<input type=text id='txtML" + i + "' name='txtML" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'    MaxLength='2' onkeypress='return NumericCheck(event)' />";
                    else
                        var txtML= "<input type=text id='txtML" + i + "' name='txtML" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' value="+ col[23] +"   MaxLength='2' onkeypress='return NumericCheck(event)' />";
                else
                    var txtML= "<input type=text id='txtML" + i + "' name='txtML" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'  value="+ col[8] +" MaxLength='2' onkeypress='return NumericCheck(event)' />";
                tab += "<td style='width:2%;text-align:center' >"+ txtML +"</td>";

                if (col[9]==0)
                    if (col[22]==0)
                        var txtPL= "<input type=text id='txtPL" + i + "' name='txtPL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'   MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                    else
                        var txtPL= "<input type=text id='txtPL" + i + "' name='txtPL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' value="+ col[22] +"  MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                else
                    var txtPL= "<input type=text id='txtPL" + i + "' name='txtPL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' value="+ col[9] +" MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                tab += "<td style='width:2%;text-align:center' >"+ txtPL +"</td>";

                if (col[10]==0)
                    if (col[22]==0)
                        var txtSL= "<input type=text id='txtSL" + i + "' name='txtSL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'   MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                    else
                        var txtSL= "<input type=text id='txtSL" + i + "' name='txtSL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' value="+ col[21] +"  MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                else
                    var txtSL= "<input type=text id='txtSL" + i + "' name='txtSL" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' value="+ col[10] +" MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                tab += "<td style='width:2%;text-align:center' >"+ txtSL +"</td>";

                if (col[11]==0)
                    var txtESI= "<input type=text id='txtESI" + i + "' name='txtESI" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'  MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                else
                    var txtESI= "<input type=text id='txtESI" + i + "' name='txtESI" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' value="+ col[11] +" MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                tab += "<td style='width:2%;text-align:center' >"+ txtESI +"</td>";

                if (col[12]==0)
                    var txtOthers= "<input type=text id='txtOthers" + i + "' name='txtOthers" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'  MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                else
                    var txtOthers= "<input type=text id='txtOthers" + i + "' name='txtOthers" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' value="+ col[12] +" MaxLength='2' onkeypress='return NumericCheck(event)'  />";
                tab += "<td style='width:3%;text-align:center' >"+ txtOthers +"</td>";


                
                var txtLOPL= "<input type=text id='txtLOPL" + i + "' name='txtLOPL" + i + "'   style='width:0%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[23] +" MaxLength='50' disabled='true'  class='NormalText' />";
                tab += "<td style='width:0%;text-align:center' >"+ txtLOPL +"</td>";
                var txtLOPB= "<input type=text id='txtLOPB" + i + "' name='txtLOPB" + i + "'   style='width:0%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[29] +" MaxLength='50' disabled='true'  class='NormalText' />";
                tab += "<td style='width:0%;text-align:center' >"+ txtLOPB +"</td>";



                if (col[13]==0)
                    var txtArrear= "<input type=text id='txtArrear" + i + "' name='txtArrear" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText'  MaxLength='8' onkeypress='return NumericCheck(event)'   />";
                else
                    var txtArrear= "<input type=text id='txtArrear" + i + "' name='txtArrear" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' class='NormalText' value="+ col[13] +" MaxLength='8' onkeypress='return NumericCheck(event)'   />";
                tab += "<td style='width:5%;text-align:center' >"+ txtArrear +"</td>";

                if(col[14]=='null')
                    var txtA_Remark="<textarea id='txtA_Remark" + i + "' name='txtA_Remark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;' onblur=Arrear_RemarkUpdation("+ i +") maxlength='1000' onkeypress='return TextAreaCheck(event)' ></textarea>";
                else
                    var txtA_Remark="<textarea id='txtA_Remark" + i + "' name='txtA_Remark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;'  onblur=Arrear_RemarkUpdation("+ i +") maxlength='1000' onkeypress='return TextAreaCheck(event)' >"+ col[14] +"</textarea>";
                tab += "<td style='width:11%;text-align:center' >"+ txtA_Remark +"</td>";


                
                var txtSLB= "<input type=text id='txtSLB" + i + "' name='txtSLB" + i + "'   style='width:0%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[26] +" MaxLength='50' disabled='true'  class='NormalText' />";
                tab += "<td style='width:0%;text-align:center' >"+ txtSLB +"</td>";
                var txtSLL= "<input type=text id='txtSLL" + i + "' name='txtSLL" + i + "'   style='width:0%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[20] +" MaxLength='50' disabled='true'  class='NormalText' />";
                tab += "<td style='width:0%;text-align:center' >"+ txtSLL +"</td>";



                if (col[15]==0)
                    var txtPayment= "<input type=text id='txtPayment" + i + "' name='txtPayment" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'  MaxLength='8' class='NormalText' onkeypress='return NumericCheck(event)'  />";
                else
                    var txtPayment= "<input type=text id='txtPayment" + i + "' name='txtPayment" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' value="+ col[15] +" MaxLength='8' class='NormalText' onkeypress='return NumericCheck(event)'  />";
                tab += "<td style='width:5%;text-align:center' >"+ txtPayment +"</td>";
                if(col[16]=='null')
                    var txtP_Remark= "<textarea id='txtP_Remark" + i + "' name='txtP_Remark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;' onblur=Payment_RemarkUpdation("+ i +")  maxlength='1000' onkeypress='return TextAreaCheck(event)' ></textarea>";
                else
                    var txtP_Remark= "<textarea id='txtP_Remark" + i + "' name='txtP_Remark" + i + "' style='width:99%; float:left;background-color:#FFFFF0;border:none;resize:none;' onblur=Payment_RemarkUpdation("+ i +")  maxlength='1000' onkeypress='return TextAreaCheck(event)' >"+ col[16] +"</textarea>";
                tab += "<td style='width:11%;text-align:center' >"+ txtP_Remark +"</td>";



                var txtMPL= "<input type=text id='txtMPL" + i + "' name='txtMPL" + i + "'   style='width:0%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[22] +" MaxLength='50' disabled='true'  class='NormalText' />";
                tab += "<td style='width:0%;text-align:center' >"+ txtMPL +"</td>";
               var txtMPB= "<input type=text id='txtMPB" + i + "' name='txtMPB" + i + "'   style='width:0%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[28] +" MaxLength='50' disabled='true'  class='NormalText' />";
                tab += "<td style='width:0%;text-align:center' >"+ txtMPB +"</td>";



                if (col[17]==1){
                    var txtIFSC= "<input type=text id='txtIFSC" + i + "' name='txtIFSC" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;' onblur= AllowIFSC(" + i +")  value="+ col[2] +"  MaxLength='11'  disabled='true' ' class='NormalText' />";
                    tab += "<td style='width:6%;text-align:center' >"+ txtIFSC +"</td>";
                    
                    var txtAcNo= "<input type=text id='txtAcNo" + i + "' name='txtAcNo" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'   value="+ col[3] +" MaxLength='50' disabled='true'  class='NormalText' />";
                    tab += "<td style='width:6%;text-align:center' >"+ txtAcNo +"</td>";
                }
                else{
                    if(col[2]=='' || col[2]=='null')
                        var txtIFSC= "<input type=text id='txtIFSC" + i + "' name='txtIFSC" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'  onblur= AllowIFSC(" + i +")  MaxLength='11' class='NormalText' />";
                    else
                        var txtIFSC= "<input type=text id='txtIFSC" + i + "' name='txtIFSC" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'  onblur= AllowIFSC(" + i +")  value="+ col[2] +" MaxLength='11' class='NormalText' />";
                    tab += "<td style='width:6%;text-align:center' >"+ txtIFSC +"</td>";
                    if(col[3]=='' || col[3]=='null')
                        var txtAcNo= "<input type=text id='txtAcNo" + i + "' name='txtAcNo" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'  MaxLength='50' class='NormalText' />";
                    else
                        var txtAcNo= "<input type=text id='txtAcNo" + i + "' name='txtAcNo" + i + "'   style='width:99%;text-align:right;background-color:#FFFFF0;border:none;'  value="+ col[3] +"  MaxLength='50' class='NormalText' />";
                    tab += "<td style='width:6%;text-align:center' >"+ txtAcNo +"</td>";
                }

                
               
               
                
                
                tab += "</tr>";
            }
            
            document.getElementById("<%= pnlDisplay.ClientID %>").innerHTML = tab;
        }

    function Arrear_RemarkUpdation(i){alert(document.getElementById("txtL" + i).value);
        if (document.getElementById("txtA_Remark" + i).value=='' && document.getElementById("txtArrear" + i).value!='') {
            alert("Enter Arrear Remark");
            document.getElementById("txtA_Remark" + i).focus();
        }
    }


    function Payment_RemarkUpdation(i){
        if (document.getElementById("txtP_Remark" + i).value=='' && document.getElementById("txtPayment" + i).value!='') {
            alert("Enter Payment Remark");
            document.getElementById("txtP_Remark" + i).focus();
        }
    }

    function DateOnChange(i){
//        if (document.getElementById("cmbType" + i).value!=-1 && document.getElementById("cmbType" + i).value!=-1 document.getElementById("txtDate" + i).value==''){
//            alert("Enter Date");
//            document.getElementById("cmbType" + i).focus();
//        }

    }


    function LOPTaken(i){
        if (document.getElementById("txtLOP" + i).value!=''){
            
        }
    }



     function FromServer(arg, context) {
        if (context == 1) {
            var Data = arg.split("~");
            if (Data[0]==0){
                alert(Data[1]);
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
                return false;
            }
        }
        else if (context == 2) {
            var Data = arg.split("~");
            if (Data[0]==1){
                alert("Submitted Successfully !");
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
            if (Data[0]==3){
                alert(Data[1]);
                return false;
            }

        }
    }

    function SubmitOnClick(){
        var Conf = confirm("Unless and until all the updations of salary informations in this month are completed do not submit .Do you want to continue ?");
        if (Conf == true) {
            var BranchId=document.getElementById("<%= txtBranchId.ClientID %>").value;
            ToServer("2Ø" + BranchId + "Ø" + document.getElementById("<%= hid_Month.ClientID %>").value + "Ø" + document.getElementById("<%= hid_Year.ClientID %>").value , 2);
        }
    }


    function SaveOnClick(){
        var Conf = confirm("Are you sure to save the salary information details ?");
        if (Conf == true) {
            var BranchId=document.getElementById("<%= txtBranchId.ClientID %>").value;
            document.getElementById("<%= hid_Data.ClientID %>").value='';
            var Empcode='';
            var EmpType=null;
            var TDate='';
            var LOP=0;
            var CL=0;
            var ML=0;
            var PL=0;
            var SL=0;
            var ESI=0;
            var Others=0;
            var Arrear=0;
            var Arrear_Remark=null;
            var Payment=0;
            var Payment_Remark=null;
            var IFSC=null;
            var Account_No=null;
            var Status_Remark=null;
            var RowLength=document.getElementById("<%= hid_Item.ClientID %>").value-1;
            var n=0;
            for (n = 1; n <= RowLength+1; n++) {
//                validation
                if (document.getElementById("txtA_Remark" + n).value=='' && document.getElementById("txtArrear" + n).value!='') {
                    alert("Enter Arrear Remark");
                    document.getElementById("txtArrear" + n).focus();
                    return false;
                }
                if (document.getElementById("txtP_Remark" + n).value=='' && document.getElementById("txtPayment" + n).value!='') {
                    alert("Enter Payment Remark");
                    document.getElementById("txtPayment" + n).focus();
                    return false;
                }
//                if (document.getElementById("cmbType" + i).disabled=='false'  && document.getElementById("cmbType" + i).value!=-1 )
//                    if(document.getElementById("txtDate" + n).value==''){
//                        alert("Enter Date");
//                        document.getElementById("cmbType" + n).focus();
//                        return false;
//                    }
//                }

               
               
                Empcode=document.getElementById("txtEmpCode" + n ).value;
                EmpType=document.getElementById("cmbType" + n ).value;
                if (document.getElementById("txtDate" + n).value!='') {
                    var DT=document.getElementById("txtDate" + n).value.split("/");
                    var userday = DT[0];
                    var usermonth = DT[1];
                    var useryear = DT[2];
                    var lenmonth=usermonth.length;
                    if (usermonth.substr(0, 1)==0)
                        var res = usermonth.substr(1, lenmonth);
                    else
                        var res =usermonth;
                    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                    TDate = userday + "-" + months[res - 1] + "-" + useryear; 
                }
                else
                    TDate='';alert(2);
                if (document.getElementById("txtStatus_Remark" + n).value!='')
                    Status_Remark=document.getElementById("txtStatus_Remark" + n ).value;
                else
                    Status_Remark=null;alert(1);
                if (document.getElementById("txtLOP" + n).value!='')
                    LOP=document.getElementById("txtLOP" + n ).value;
                else 
                    LOP=0;                   
                if (document.getElementById("txtCL" + n).value!='')
                    CL=document.getElementById("txtCL" + n ).value;
                else
                    CL=0;
                if (document.getElementById("txtML" + n).value!='')
                    ML=document.getElementById("txtML" + n ).value;
                else
                    ML=0;
                if (document.getElementById("txtPL" + n).value!='')
                    PL=document.getElementById("txtPL" + n ).value;
                else
                    PL=0;
                if (document.getElementById("txtSL" + n).value!='')
                    SL=document.getElementById("txtSL" + n ).value;
                else
                    SL=0;
                if (document.getElementById("txtESI" + n).value!='')
                    ESI=document.getElementById("txtESI" + n ).value;
                else
                    ESI=0;
                if (document.getElementById("txtOthers" + n).value!='')
                    Others=document.getElementById("txtOthers" + n ).value;
                else
                    Others=0;
                if (document.getElementById("txtArrear" + n).value!='')
                    Arrear=document.getElementById("txtArrear" + n ).value;
                else
                    Arrear=0;
                if (document.getElementById("txtA_Remark" + n).value!='')
                    Arrear_Remark=document.getElementById("txtA_Remark" + n ).value;
                else
                    Arrear_Remark=null;
                if (document.getElementById("txtPayment" + n).value!='')
                    Payment=document.getElementById("txtPayment" + n ).value;
                else
                    Payment=0;
                if (document.getElementById("txtP_Remark" + n).value!='')
                    Payment_Remark=document.getElementById("txtP_Remark"  + n ).value;
                else
                    Payment_Remark=null;

                if (document.getElementById("txtIFSC" + n).value!='')
                    IFSC=document.getElementById("txtIFSC"  + n ).value;
                if (document.getElementById("txtAcNo" + n).value!='')
                    Account_No=document.getElementById("txtAcNo"  + n ).value;

                document.getElementById("<%= hid_Data.ClientID %>").value+= Empcode + "ÿ" + EmpType  + "ÿ" + TDate + "ÿ"  +  LOP  + "ÿ"  + CL + "ÿ"  + ML + "ÿ"  + PL + "ÿ" + SL + "ÿ" + ESI + "ÿ" + Others + "ÿ" + Arrear +  "ÿ"  + Arrear_Remark + "ÿ" + Payment + "ÿ" + Payment_Remark + "ÿ" + IFSC + "ÿ" + Account_No + "ÿ" + Status_Remark + "Ñ";
                
             } 
            
            ToServer("1Ø" + BranchId + "Ø" + document.getElementById("<%= hid_Data.ClientID %>").value + "Ø" + document.getElementById("<%= hid_Month.ClientID %>").value + "Ø" +  document.getElementById("<%= hid_Year.ClientID %>").value, 1);
        }
    }



     function AllowIFSC(i) {
            var ifsc = document.getElementById("txtIFSC" + i).value;
            var reg = /[A-Z|a-z][a-zA-Z0-9]{7}$/;

            if (ifsc.match(reg)) {
                return true;
            }
            else {
                alert("You Entered Wrong IFSC Code \n\n ------ or------ \n\n IFSC code should be count 11 \n\n-> Starting 4 should be only alphabets[A-Z] \n\n-> Remaining 7 should be accepting only alphanumeric");
                document.getElementById("txtIFSC" + i).focus();
                return false;
            }

        }


    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }


</script>
</head>
 <table style="width: 100%;margin: 0px auto">
    <tr>
        <td colspan="5" style="width:100%; height: 23px;"></td>
    </tr>
    <tr>
        <td style="width:10%;"></td>
        <td style="width:15%;"></td>
        <td style="width:10%; text-align: left;font-family:Cambria;">
            Branch Code</td>
        <td  style="width:45%;">
                <asp:TextBox ID="txtBranchId" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="40%" MaxLength="5"  disabled="true"  />
        </td>
        <td style="width:10%;"></td>
    </tr>
    <tr>
        <td style="width:10%;"></td>
        <td style="width:15%;"></td>
        <td style="width:10%; text-align: left;font-family:Cambria;">
            Branch Name</td>
        <td  style="width:45%;">
                <asp:TextBox ID="txtBranch" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="40%" MaxLength="50" disabled="true"   />
        </td>
        <td style="width:10%;"></td>
    </tr>
    <tr>
        <td colspan="5" style="width:100%; height: 12px;"></td>
    </tr>
    <tr>
        <td colspan="5" ">
            <asp:Panel ID="pnlDisplay" runat="server">
            </asp:Panel>
        </td>
        
    </tr>
    <tr>
        <td colspan="5" style="width:100%; height: 12px;"></td>
    </tr>
    <tr>
        <td style="text-align: center; font-family:Cambria;" colspan="5">
           <input id="btnSave" style="font-family: cambria; cursor: pointer; width:6%; height: 25px;" 
                type="button" value="SAVE"  onclick="return SaveOnClick()" />                 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%; height: 25px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
               <asp:HiddenField ID="hid_Value" runat="server" />  
               <asp:HiddenField ID="hid_Item" runat="server" />
               <asp:HiddenField ID="hid_Data" runat="server" />
               <asp:HiddenField ID="hid_Month" runat="server" />
               <asp:HiddenField ID="hid_Year" runat="server" />
            <input id="btnSubmit" style="font-family: cambria; cursor: pointer; width:6%; height: 25px;" 
                type="button" value="SUBMIT"  onclick="return SubmitOnClick()" />
        </td>
    </tr>
 </table>
</asp:Content>

