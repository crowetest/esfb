﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Meeting_Details.aspx.vb" Inherits="Meeting_Details"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
		<link rel="stylesheet" href="../../Style/bootstrap-multiselect.css" type="text/css" />
		<script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-multiselect_EMP.js"></script>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
     
   
    <script language="javascript" type="text/javascript">
    var Meeting_ID = 0;
          
        var FileSelected = 0;
    function MeetingDateOnChange()
    {
        var MeetingDt     = document.getElementById("<%= txtDateOfMeeting.ClientID %>").value;
     
        var Data = "2Ø"+ MeetingDt ;      
        ToServer(Data, 2);
            
    }
     function cursorwait(e) {
            document.body.className = 'wait';}
     function cursordefault(e) {
            document.body.className = 'default';}
      
    function btnExit_onclick() 
    {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }

    function btnSave_onclick() {
    var newstr="";
    row = document.getElementById("<%= hdnMeetingDtl.ClientID %>").value.split("¥");
    if(row == "" || row == "¥µ-1µ-1µ-1µ"){
        alert("Enter data in meeting attendee details panel");
        return false;
    }
    var NewStr=""
    for (n = 1; n <= row.length-1 ; n++) {     
        var Qualcol=row[n].split("µ");     
        if(Qualcol[0] == ""){
            alert("Enter all details in meeting attendee details panel");
            return false;
        }
        if(Qualcol[1] == -1){
            alert("Enter all details in meeting attendee details panel");
            return false;
        }else{
            if(Qualcol[1] != 1){
                if(Qualcol[2] == -1){
            alert("Enter all details in meeting attendee details panel");
                    return false;
                }
                if(Qualcol[3] == -1){
            alert("Enter all details in meeting attendee details panel");
                    return false;
                }
            }
        }
        if((Qualcol[0]!="" && Qualcol[1]!="-1"  && Qualcol[2]!="-1" && Qualcol[3]!="-1")||(Qualcol[0]!="" && Qualcol[1]!="-1"))
        {                         
                NewStr+="¥"+Qualcol[0]+"µ"+Qualcol[1]+"µ"+Qualcol[2]+ "µ" +Qualcol[3];
        }               
    }
               

    var MeetingDt     = document.getElementById("<%= txtDateOfMeeting.ClientID %>").value;
    if(MeetingDt=="")
    {
            alert("Select Date of Meeting");
            document.getElementById("<%= txtDateOfMeeting.ClientID %>").focus();
            return false;
    } 
               
    var newstr="";
    row = document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value.split("¥");
    if(row == "" || row == "¥µµµ-1µ"){
        alert("Enter details of meeting discussion");
        return false;
    }
    for (n = 1; n <= row.length-1 ; n++) {     
        var Qualcol=row[n].split("µ");   
        if(Qualcol[0]==""){
            alert("Enter all details in meeting discussion points panel");
            return false;
        }
        if(Qualcol[1]==""){
            alert("Enter all details in meeting discussion points panel");
            return false;
        }
        if(Qualcol[2]==""){
            alert("Enter all details in meeting discussion points panel");
            return false;
        }
        if(Qualcol[3]==-1){
            alert("Enter all details in meeting discussion points panel");
            return false;
        }
          
        if(Qualcol[0]!="" && Qualcol[1]!=""  && Qualcol[3]!="-1")
        {                         
                newstr+="¥"+Qualcol[0]+"µ"+Qualcol[1]+"µ"+Qualcol[2]+ "µ" +Qualcol[3];

        }
        else if(Qualcol[0]!="" && Qualcol[1]=="")
        {
                    alert("Please Fill the Follow up action");
                    return false;

        }
        else if (Qualcol[0]!="" && Qualcol[1]!=""  &&  Qualcol[3]=="-1")
        {
                    alert("Please Select Status");
                    return false;
        }
    }
     if (FileSelected == 0) 
        {
            alert("Kindly add attachment");
            document.getElementById("<%= fup1.ClientID %>").focus();
            return false;
        }
    
    var Data = "1Ø"+ Meeting_ID + "Ø" + MeetingDt   + "Ø" + NewStr + "Ø" + newstr;
//    cursorwait();
//    document.getElementById("<%= btnSave.ClientID %>").disabled=true;
    
    document.getElementById("<%= hid_Data.ClientID %>").value = Data;
//    ToServer(Data, 1);
            
    }

    function FromServer(Arg, Context) 
    {
          switch (Context)
           {
            
               case 1:
               {
                    var Data = Arg.split("Ø");
                    cursordefault();
                    alert(Data[1]);
                    document.getElementById("btnSave").disabled=false;
                    if (Data[0] == 0) 
                    window.open('Meeting_Details.aspx','_self');
                    break;
               }       
               case 2:
               {
              
                    var NewStr = "";
                    var arr = Arg.split("~");
                   
                    if(arr[1]=="")
                        Meeting_ID=0;
                    else
                        Meeting_ID = arr[1];
                  
                    document.getElementById("<%= hidAttach.ClientID %>").value=arr[4];
                    document.getElementById("<%= hidMeetingID.ClientID %>").value=Meeting_ID;
                    document.getElementById("<%= hdnMeetingDtl.ClientID %>").value=arr[2];
                    table_fill();
                    document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value=arr[3];
                    table_fillDisscussion();
                    break;         
                }  
             }
          } 

        function AddNewRow() 
        {            
                if(document.getElementById("<%= txtDateOfMeeting.ClientID %>").value  =="")
                {
                    alert("Select Meeting Date First");
                    return false;
                }
                if (document.getElementById("<%= hdnMeetingDtl.ClientID %>").value != "")
                {                   
                    row = document.getElementById("<%= hdnMeetingDtl.ClientID %>").value.split("¥");
                     var Len = row.length - 1;          
                    col = row[Len].split("µ");  
                    if( (col[0] != "" && col[1] != "-1" && col[2] != "-1" && col[3] != "-1") || (col[0] != "" && col[1] == "1"))
                    {
              
                        document.getElementById("<%= hdnMeetingDtl.ClientID %>").value += "¥µ-1µ-1µ-1µ";                          
                        
                    }
                }             
                else
                document.getElementById("<%= hdnMeetingDtl.ClientID %>").value = "¥µ-1µ-1µ-1µ"; 
                
                table_fill();
                
        }
        function AddNewRowDisscussion() 
        {            
            if(document.getElementById("<%= txtDateOfMeeting.ClientID %>").value  =="")
            {
                alert("Select Meeting Date First");
                return false;
            }
            if (document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value != "")
            {                
                row = document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value.split("¥");
                var Len = row.length - 1;          
                col = row[Len].split("µ");  
               
                if(col[0]!="" && col[1]=="")
                {
                            alert("Please Fill the Follow up action");
                            return false;

                }
                else if (col[0]!="" && col[1]!=""  &&  col[3]=="-1")
                {
                            alert("Please Select Status");
                            return false;

                }
                else if((col[0] != "" && col[1] != "" && col[2] != "" && col[3] != "-1") || (col[0] != "" && col[1] != "" && col[2] == "" && col[3] != "-1"))
                {
                    document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value += "¥µµµ-1µ";                          
                  
                 }
              }
                             
            else
            
            {
         
            document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value = "¥µµµ-1µ";
            } 
            table_fillDisscussion();
        }
        function updateValue(obj,id,value_)
        {  
        var Obj_;
            if (obj ==1 )
            {
                Obj_ ="cmbStaffCustomer"+id.toString(); 
            }
            else if(obj ==2)
            {
                 Obj_ ="cmbSeniorCitizen"+id.toString(); 
            }
            else if(obj ==3)
            {
                 Obj_ ="cmbWomen"+id.toString();     
            }
            document.getElementById(Obj_).value = value_;
            row = document.getElementById("<%= hdnMeetingDtl.ClientID %>").value.split("¥");
             var NewStr="";
             if (row.length != 0)
             {
                 for (n = 1; n <= row.length-1; n++)
                 {

                      if(id==n)
                      {    
                            var AttendeeName=document.getElementById("txtAttendee"+id).value;
                            var StaffCustomer = document.getElementById("cmbStaffCustomer"+id).options[document.getElementById("cmbStaffCustomer"+id).selectedIndex].text;
                            var SeniorCitizen = document.getElementById("cmbSeniorCitizen"+id).options[document.getElementById("cmbSeniorCitizen"+id).selectedIndex].text;
                            var Womenn = document.getElementById("cmbWomen"+id).options[document.getElementById("cmbWomen"+id).selectedIndex].text;
                             NewStr+="¥"+AttendeeName+"µ"+document.getElementById("cmbStaffCustomer"+id).value+"µ"+document.getElementById("cmbSeniorCitizen"+id).value+"µ"+document.getElementById("cmbWomen"+id).value+"µ"+StaffCustomer+"µ"+SeniorCitizen+"µ"+Womenn;
                           
                       }
                       else
                       {
                            NewStr+="¥"+row[n];
                       }
                   
                  }
                 
            document.getElementById("<%= hdnMeetingDtl.ClientID %>").value=NewStr;

            }

        }
        function updateValueDisscussion(id)
        {  
       
             row = document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value.split("¥");
             var newStr=""
              if (row.length != 0)
             {
             for (n = 1; n <= row.length-1 ; n++) {
             if(id==n)
             {
        
                    var DisscussionPoint=document.getElementById("txtDisscussion"+id).value;
                    var FollowupAction=document.getElementById("txtFollow"+id).value;
                    var Remarks=document.getElementById("txtRemark"+id).value;
                    var StatusValue = document.getElementById("cmbStatus"+id).options[document.getElementById("cmbStatus"+id).selectedIndex].text;
                    newStr+="¥"+DisscussionPoint+"µ"+FollowupAction+"µ"+Remarks+"µ"+document.getElementById("cmbStatus"+id).value+"µ"+StatusValue;
             }
            else
            {
                    newStr+="¥"+row[n];
            }
           
            }
             document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value=newStr;
        }
        }
        function DeleteRow(id)
        {
           
                row = document.getElementById("<%= hdnMeetingDtl.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                    if(id!=n)                  
                        NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnMeetingDtl.ClientID %>").value=NewStr;
                if(document.getElementById("<%= hdnMeetingDtl.ClientID %>").value=="")
                document.getElementById("<%= hdnMeetingDtl.ClientID %>").value= "¥µ-1µ-1µ-1µ";
                table_fill();
        }
        function DeleteRowDisscussion(id)
        {
           
                row = document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value.split("¥");
                var newStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id!=n)                  
                     newStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value=newStr;
                if(document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value=="")
                document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value= "¥µµµ-1µ";
                table_fillDisscussion();
        }
        function DisableColumn(value_,row_no){
            if(value_==1){
                document.getElementById("cmbSeniorCitizen"+row_no).disabled=true;
                document.getElementById("cmbWomen"+row_no).disabled=true;
                document.getElementById("cmbSeniorCitizen"+row_no).value="-1";
                document.getElementById("cmbWomen"+row_no).value ="-1";             }
            else
            {
               document.getElementById("cmbSeniorCitizen"+row_no).disabled =false;
               document.getElementById("cmbWomen"+row_no).disabled=false;
            }
       }
     
       function table_fill() 
       {
         if (document.getElementById("<%= hdnMeetingDtl.ClientID %>").value != "") 
         {
            document.getElementById("<%= pnMeetingDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>"; 
            tab += "<td style='width:35%;text-align:center'><b>Attendee Name</b></td>"; 
            tab += "<td style='width:20%;text-align:center'><b>Category</b></td>";   
            tab += "<td style='width:15%;text-align:center'><b>Senior citizen</b></td>";
            tab += "<td style='width:15%;text-align:center'><b>Women </b></td>";    
            tab += "<td style='width:10%;text-align:center'></td>";  
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            row = document.getElementById("<%= hdnMeetingDtl.ClientID %>").value.split("¥");
            for (n = 1; n <= row.length - 1; n++) 
            {
                 col = row[n].split("µ");   
                 if (row_bg == 0) 
                 {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  else 
                  {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";  
                    if(col[0]!="")
                    {
                    tab += "<td style='width:35%;text-align:left'>" + col[0] + "</td>";
                      
                    }
                    else
                    {
                    var txtAttendeeName = "<input id='txtAttendee" + n + "' name='txtAttendee" + n + "' type='Text' TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%; word-wrap:'break-word;'maxlength='50' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)'  />";
                    tab += "<td style='width:35%;text-align:left' >"+ txtAttendeeName +"</td>";
                       
                    }

                    if(col[1]==-1)
                        var select1 = "<select id='cmbStaffCustomer" + n + "' class='NormalText' name='cmbStaffCustomer" + n + "' style='width:100%' onchange='DisableColumn(this.options[this.selectedIndex].value," + n +") ;updateValue(" +  1 + ","+ n +",this.value)'  >";
                    else
                        var select1 = "<select id='cmbStaffCustomer" + n + "' class='NormalText' name='cmbStaffCustomer" + n + "' style='width:100%'  disabled=true onchange='DisableColumn(this.options[this.selectedIndex].value," + n +") ;updateValue(" +  1 + ","+ n +",this.value)'  >";

                    if(col[1]==-1)
                        select1 += "<option value='-1' selected=true>-----Select-----</option>";
                    else
                         select1 += "<option value='-1'>-----Select-----</option>";
                    if(col[1]==1)
                        select1 += "<option value='1' selected=true>Staff</option>";
                    else
                        select1 += "<option value='1'>Staff</option>";
                    if(col[1]==2)
                        select1 += "<option value='2' selected=true>Customer</option>";
                    else
                        select1 += "<option value='2'>Customer</option>";
                    
                    tab += "<td style='width:20%;text-align:left'>" + select1 + "</td>";


                    if((col[1]==1) || (col[2]!=-1))
                        var select2 = "<select id='cmbSeniorCitizen" + n + "' class='NormalText' name='cmbSeniorCitizen" + n + "'  style='width:100%'  disabled=true  onchange='updateValue(" +  2 + ","+ n +",this.value)' >";
                    else
                        var select2 = "<select id='cmbSeniorCitizen" + n + "' class='NormalText' name='cmbSeniorCitizen" + n + "' style='width:100%' onchange='updateValue(" +  2 + ","+ n +",this.value)'   >";

                   if(col[2]==-1)
                        select2 += "<option value='-1' selected=true>-----Select-----</option>";
                    else
                        select2 += "<option value='-1'>-----Select-----</option>";
                    if(col[2]==1)
                        select2 += "<option value='1' selected=true>Yes</option>";
                    else
                        select2 += "<option value='1'>Yes</option>";
                    if(col[2]==2)
                        select2 += "<option value='2' selected=true>No</option>";
                    else
                        select2 += "<option value='2'>No</option>";
                                                    
                    tab += "<td style='width:15%;text-align:left'>" + select2 + "</td>";


                     if((col[1]==1) || (col[3]!=-1))
                        var select3 = "<select id='cmbWomen" + n + "' class='NormalText' name='cmbWomen" +n + "' style='width:100%'  disabled=true onchange='updateValue(" +  3 + ","+ n +",this.value)' onkeydown='CreateNewRow(event," + n + ")' >";
                     else
                         var select3 = "<select id='cmbWomen" + n + "' class='NormalText' name='cmbWomen" +n + "' style='width:100%' onchange='updateValue(" + 3 + ","+ n +",this.value)' onkeydown='CreateNewRow(event," + n + ")' >";
                                    
                    if(col[3]==-1)
                        select3 += "<option value='-1' selected=true>-----Select-----</option>";
                    else
                        select3 += "<option value='-1'>-----Select-----</option>";
                    
                    if(col[3]==1)
                        select3 += "<option value='1' selected=true>Yes</option>";
                    else
                        select3 += "<option value='1'>Yes</option>";
                    if(col[3]==2)
                        select3 += "<option value='2' selected=true>No</option>";
                    else
                        select3 += "<option value='2'>No</option>";
                    tab += "<td style='width:15%;text-align:left '>" + select3 + "</td>";

                    if( (col[0] == "" && col[1] == "-1" && col[2] == "-1" && col[3] == "-1") || (col[0] == "" && col[1] == "1"))
                    {
                      tab +="<td style='width:10%;text-align:left'></td>";
                     
                    }
                    else
                    {
                       tab += "<td style='width:10%;text-align:center' onclick=DeleteRow('" + n + "')><img  src='../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                        
                    }
                    tab += "</tr>";
                }
                tab += "</table></div></div></div>";
                document.getElementById("<%= pnMeetingDtl.ClientID %>").innerHTML = tab;
                setTimeout(function() {
                document.getElementById("txtAttendee"+(n-1)).focus().select();
                }, 4);
                var attach = document.getElementById("<%= hidAttach.ClientID %>").value;
                if(attach == 1)
                    document.getElementById("<%= btnView.ClientID %>").style.display = '';
                else
                    document.getElementById("<%= btnView.ClientID %>").style.display = "none";
                }
                else{
                    document.getElementById("<%= pnMeetingDtl.ClientID %>").style.display = 'none';
                }
                //--------------------- Clearing Data ------------------------//
        }
        function  table_fillDisscussion()
        {
            if (document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value != "") 
            {
            document.getElementById("<%= pnDisscussionDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>"; 
            tab += "<td style='width:30%;text-align:center'><b>Disscussion Point</b></td>"; 
            tab += "<td style='width:35%;text-align:center'><b>Follow up Action</b></td>";   
            tab += "<td style='width:15%;text-align:center'><b>Remarks</b></td>";
            tab += "<td style='width:10%;text-align:center'><b>Status </b></td>";    
            tab += "<td style='width:10%;text-align:center'></td>";  
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            row = document.getElementById("<%= hdnDisscussionDtl.ClientID %>").value.split("¥");
           
            for (n = 1; n <= row.length - 1; n++) 
            {
                 col = row[n].split("µ");   
                 if (row_bg == 0) 
                 {
                    row_bg = 1;
                    tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                 }
                 else 
                 {
                    row_bg = 0;
                    tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                 }
                 tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";  
                 if(col[0]!="")
                 {
                    tab += "<td style='width:30%;text-align:left'>" + col[0] + "</td>";
                      
                 }
                 else
                 {
                    var txtDisscussionPoint = "<input id='txtDisscussion" + n + "' name='txtDisscussion" + n + "' type='Text' TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%; word-wrap:'break-word;' class='NormalText' maxlength='1000' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+ n +")'  />";                                            
                    tab += "<td style='width:30%;text-align:left'>" + txtDisscussionPoint + "</td>";
                       
                 }
                 if(col[1]!="")
                 {
                    tab += "<td style='width:35%;text-align:left'>" + col[1] + "</td>";
                 }
                 else
                 {
                    var txtFollowupAction = "<input id='txtFollow" + n + "' name='txtFollow" + n + "' type='Text' TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%; word-wrap:'break-word;' class='NormalText' maxlength='1000' onkeypress='return TextAreaCheck(event)' onchange='updateValueDisscussion("+ n +")' />";                                            
                    tab += "<td style='width:35%;text-align:left'>" + txtFollowupAction + "</td>";
                 }
                 if(col[1]!="")
                 {
                    tab += "<td style='width:15%;text-align:left'>" + col[2] + "</td>";
                 }
                 else
                 {
                     var txtRemarks = "<input id='txtRemark" + n + "' name='txtRemark" + n + "' type='Text' TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%; word-wrap:'break-word;' class='NormalText' maxlength='1000' onkeypress='return TextAreaCheck(event)' onchange='updateValueDisscussion("+ n +")' />";                                            
                     tab += "<td style='width:15%;text-align:left'>" + txtRemarks + "</td>";
                 }
                 if(col[3]==-1)
                    var select1 = "<select id='cmbStatus" + n.toString() + "' class='NormalText' name='cmbStatus" + n.toString() + "' style='width:100%' onchange='updateValueDisscussion("+ n +")'    >";
                else
                    var select1 = "<select id='cmbStatus" + n.toString() + "' class='NormalText' name='cmbStatus" + n.toString() + "' style='width:100%'  onchange='updateValueDisscussion("+ n +")'  disabled='true'  >";

                    if(col[3]==-1)
                        select1 += "<option value='-1' selected=true>-----Select-----</option>";
                    else
                         select1 += "<option value='-1'>-----Select-----</option>";
                    if(col[3]==1)
                        select1 += "<option value='1' selected=true>Open</option>";
                    else
                        select1 += "<option value='1'>Open</option>";
                    if(col[3]==2)
                        select1 += "<option value='2' selected=true>Closed</option>";
                    else
                        select1 += "<option value='2'>Closed</option>";
                                                    
                    tab += "<td style='width:10%;text-align:left'>" + select1 + "</td>";
                    if((col[0]=="" && col[1]=="" && col[2]=="" && col[3]=="-1") || (col[0]=="" && col[1]==""  && col[3] == "-1"))
                    {
                        tab +="<td style='width:10%;text-align:left'></td>";
                    }
                    else
                    {
                        tab += "<td style='width:10%;text-align:center' onclick=DeleteRowDisscussion('" + n + "')><img  src='../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                       
                    }
                     tab += "</tr>";
                }
                tab += "</table></div></div></div>";
                document.getElementById("<%= pnDisscussionDtl.ClientID %>").innerHTML = tab;

                setTimeout(function() {
                    document.getElementById("txtDisscussion"+(n-1)).focus().select();
                    }, 4);
            }
            else
            document.getElementById("<%= pnDisscussionDtl.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//
        }
       function CreateNewRow(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValue(obj,id,val);
                AddNewRow();                          
            }
         }
         function CreateNewRowDisscussion(e, val) {
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValueDisscussion(val);
                AddNewRowDisscussion() ;                         
             }
         }

         
    function ViewAttachment() 
    {
        var MeetingID	= document.getElementById("<%= hidMeetingID.ClientID %>").value;
        if (MeetingID > 0)
            window.open("ShowFormat.aspx?MeetingID=" + btoa(MeetingID) + "");
        return false;
    }
    
    function validate_new_fileupload()
    {
        var fileName = document.getElementById("<%= fup1.ClientID %>").value;
        FileSelected = 1;
        return true;
    }
    </script>
    <br />
    <div>
    <table align="center" style="width:80%; margin: 0px auto;">
               <tr>
                    <td style="width: 40%;text-align:right;font-size: 10pt;">Date of Meeting</td>
                    <td style="width: 1%;text-align:right;"></td>
                    <td style="width: 40%;text-align:left;"><asp:TextBox ID="txtDateOfMeeting"  runat="server"  class="NormalText" Width="50%" ReadOnly="true"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender2" Format="dd MMM yyyy"  runat="server" Enabled="True" TargetControlID="txtDateOfMeeting"></asp:CalendarExtender></td>
                    <td style="width: 19%;text-align:right;"></td>
            </tr>
            <tr>
                    <td style="width:12% ;text-align:center;" colspan="4" class="mainhead" >
                    <strong>MEETING ATTENDEE DETAILS</strong>
                    <img src="../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer;  padding-right:10px;" onclick="AddNewRow()" title="Add New"/>
                    <b style="color:red;">*</b>
                    </td>
            </tr> 
            <tr style="text-align:center;">
                    <td  colspan="4" style="text-align:center;">
                    <asp:Panel ID="pnMeetingDtl" runat="server">
                    </asp:Panel>
                    </td>
           </tr>
           <tr>
           
                <td style="width:12% ;text-align:center;" colspan="4" class="mainhead">
                <strong>MEETING DISSCUSSION POINTS</strong>
                <img src="../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer;  padding-right:10px;" onclick="AddNewRowDisscussion()" title="Add New"/>
                <b style="color:red;">*</b>
                </td>
           
         </tr> 
         <tr style="text-align:center;">
              <td  colspan="4" style="text-align:center;">
              <asp:Panel ID="pnDisscussionDtl" runat="server">
              </asp:Panel>
             </td>
        </tr>
        <tr id="Tr7">
            <td style="width: 40%;text-align:right;font-size: 10pt;">
                Attachment If any
            </td>
            <td style="width: 1%;text-align:right;"></td>
            <td style="width: 40%;text-align:left;">
                &nbsp; &nbsp;
                <div id="fileUploadarea">
                    <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" />
                &nbsp; &nbsp;<asp:ImageButton ID="btnView" runat="server" Height="18px" Width="18px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" style="display:none" /><br />
                </div>
            </td>
            <td style="width: 19%;text-align:right;"><br /></td>
        </tr>
        <tr>
            <td><br /></td>
        </tr>
        <tr style="text-align:center;">
                <td  colspan="4" style="text-align:center;">
                <%--<input id="btnSave" type="button" value="SAVE" onclick="return btnSave_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;--%>
                <asp:Button ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                <input id="btnExit" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />
                
                <asp:HiddenField 
                    ID="hdnStaffCustomer" runat="server" />
                <asp:HiddenField 
                    ID="hdnSeniorCitizen" runat="server" />
                <asp:HiddenField 
                    ID="hdnWomen" runat="server" />
                <asp:HiddenField 
                    ID="hdnMeetingDtl" runat="server" />
                <asp:HiddenField 
                    ID="hdnStatus" runat="server" />
                <asp:HiddenField 
                    ID="hdnDisscussionDtl" runat="server" />
                <asp:HiddenField 
                    ID="hid_Data" runat="server" />
                <asp:HiddenField 
                    ID="hidAttach" runat="server" />
                <asp:HiddenField 
                    ID="hidMeetingID" runat="server" />
               </td>
         </tr>
    </table>
    </div> 
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    </asp:Content>
