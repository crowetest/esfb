﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true" CodeFile="Correct_Branch_Address.aspx.vb" Inherits="Correct_Branch_Address" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
            font-size :x-large;
            font-family :Cambria;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;1
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        </style>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:HiddenField ID="hid_dtls" runat="server" />
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
        function window_onload() {  
            var BranchID = document.getElementById("<%= hdnBranch.ClientID %>").value; 
            ToServer("1Ø"+ BranchID, 1);
        }
        function btnSave_onclick() {
            var BranchID = document.getElementById("<%= hdnBranch.ClientID %>").value;
            if(document.getElementById("<%= txtAdd1.ClientID %>").value == "")
            {
                alert("Please Enter Address 1");
                document.getElementById("<%= txtAdd1.ClientID %>").focus();
                return false;
            }
            var Add1 = document.getElementById("<%= txtAdd1.ClientID %>").value;  
            if(document.getElementById("<%= txtAdd2.ClientID %>").value == "")
            {
                alert("Please Enter Address 2");
                document.getElementById("<%= txtAdd2.ClientID %>").focus();
                return false;
            }  
            var Add2 = document.getElementById("<%= txtAdd2.ClientID %>").value;
            if(document.getElementById("<%= txtAdd3.ClientID %>").value == "")
            {
                alert("Please Enter Address 3");
                document.getElementById("<%= txtAdd3.ClientID %>").focus();
                return false;
            } 
            var Add3 = document.getElementById("<%= txtAdd3.ClientID %>").value;
            if(document.getElementById("<%= txtCity.ClientID %>").value == "")
            {
                alert("Please Enter City");
                document.getElementById("<%= txtCity.ClientID %>").focus();
                return false;
            }
            var City = document.getElementById("<%= txtCity.ClientID %>").value;
            if(document.getElementById("<%= txtPin.ClientID %>").value == "")
            {
                alert("Please Enter PIN");
                document.getElementById("<%= txtPin.ClientID %>").focus();
                return false;
            }
            var Pin = document.getElementById("<%= txtPin.ClientID %>").value; 
            var SaveData = "2Ø" + BranchID + "Ø" + Add1 + "Ø" + Add2 + "Ø" + Add3 + "Ø" + City + "Ø" + Pin
            ToServer(SaveData, 2);
        }
        function FromServer(Arg, Context) {            
            switch (Context) 
            {                
                case 1:
                {
                    var Data = Arg;
                    if(Data != "Œ")
                    {                        
                        var BranchDtl = Data.split("µ");  
                        document.getElementById("<%= txtBranchID.ClientID %>").value=BranchDtl[0];    
                        document.getElementById("<%= txtBranchName.ClientID %>").value=BranchDtl[1];    
                        document.getElementById("<%= txtRegion.ClientID %>").value=BranchDtl[2];    
                        document.getElementById("<%= txtDistrict.ClientID %>").value=BranchDtl[3];
                        document.getElementById("<%= txtState.ClientID %>").value=BranchDtl[4];  
                        document.getElementById("<%= lblState.ClientID %>").innerHTML=BranchDtl[5];  
                        document.getElementById("<%= txtAdd1.ClientID %>").value=BranchDtl[6];    
                        document.getElementById("<%= txtAdd2.ClientID %>").value=BranchDtl[7];
                        document.getElementById("<%= txtAdd3.ClientID %>").value=BranchDtl[8];
                        document.getElementById("<%= txtCity.ClientID %>").value=BranchDtl[9];
                        document.getElementById("<%= txtPin.ClientID %>").value=BranchDtl[10];                           
                    } 
                    else{
                        alert("You have already updated the data");
                        window.open("<%= Page.ResolveUrl("~")%>Portal.aspx", "_self");
                    }             
                    break;
                }
                case 2:
                {
                    var Data = Arg.split("ʘ");
                    alert(Data[1]);
                    if(Data[0] == 0)
                    {
                        window.open("<%= Page.ResolveUrl("~")%>Portal.aspx", "_self");
                    }
                    break;
                }
            }
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Portal.aspx", "_self");
        } 
        
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 80%; margin: 0px auto; font-family: Cambria;
        font-size: large;">
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch ID
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranchID" runat="server" class="ReadOnlyTextBox"
                    ReadOnly="true" Style="font-family: Cambria; font-size: 10pt;" Width="50%" MaxLength="50" />
            </td>
        </tr>
        <tr id="rowSName">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch Name
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranchName" runat="server" class="ReadOnlyTextBox"
                    ReadOnly="true" Style="font-family: Cambria; font-size: 10pt;" Width="50%" MaxLength="50" />
            </td>
        </tr>
        <tr id="rowClient">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Region
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtRegion" runat="server" class="ReadOnlyTextBox"
                    ReadOnly="true" Style="font-family: Cambria; font-size: 10pt;" Width="50%" MaxLength="50" />
            </td>
        </tr>
        <tr id="rowCName">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                District
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDistrict" runat="server" class="ReadOnlyTextBox"
                    ReadOnly="true" Style="font-family: Cambria; font-size: 10pt;" Width="50%" MaxLength="50" />
            </td>
        </tr>
        <tr id="rowCClassify">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                State
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtState" runat="server" class="ReadOnlyTextBox"
                    ReadOnly="true" Style="font-family: Cambria; font-size: 10pt;" Width="50%" MaxLength="50" />
                &nbsp;<asp:Label ID="lblState" runat="server" Font-Bold="True" ForeColor="#3333CC"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Address 1
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtAdd1" runat="server" class="NormalText" Style="font-family: Cambria;
                    font-size: 10pt; resize: none;" Width="50%" MaxLength="40" />
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Address 2
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtAdd2" runat="server" class="NormalText" Style="font-family: Cambria;
                    font-size: 10pt; resize: none;" Width="50%" MaxLength="40" />
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Address 3
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtAdd3" runat="server" class="NormalText" Style="font-family: Cambria;
                    font-size: 10pt; resize: none;" Width="50%" MaxLength="40" />
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                City (Location/Town)
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCity" class="NormalText" runat="server" Width="50%" MaxLength="40"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                PIN
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtPin" runat="server" Style="font-family: Cambria;
                    font-size: 10pt;" Width="50%" class="NormalText" MaxLength="6" onkeypress='return NumericCheck(event)' />
            </td>
        </tr>
        <tr>
            <td style="width: 25%;" colspan="3">
                <br />
            </td>
        </tr>
        <tr>
            <td style="width: 25%;" colspan="3">
                <br />
            </td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="3">
                    <br />
                    <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                        value="SAVE" onclick="return btnSave_onclick()" />&nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                        value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnBranch" runat="server" />
    <br />
    <br />
</asp:Content>
