﻿Imports System.Data
Imports System.IO
Imports System.Net
Imports System.Data.SqlClient
Partial Class CSQ_EMS_ATTACH
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Extra Mile Service"
        Session("Attach") = ""
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Session.Remove("Attach")
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim UserID As String = Session("UserID").ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim Attach_ID As Integer = 0
            Dim NoofAttachments As Integer = 0
            Dim FileName As String = ""
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    If (nFileLen > 0) Then
                        NoofAttachments += 1
                    End If
                Next
            End If
            If (NoofAttachments = 0) Then
                Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_srpt1.Append("alert('Please Attach the File');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                Exit Sub
            End If
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                    End If
                Next
            End If
            Dim vOut As String = Convert.ToBase64String(AttachImg)
            Session.Remove("Attach")
            Session("Attach") = vOut + "µ" + ContentType + "µ" + FileName
            Session("Attach2") = vOut + "µ" + ContentType + "µ" + FileName
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()

            System.Threading.Thread.Sleep(300)
            cl_script1.Append("         alert('Uploaded Successfully');window.close();window.opener.setValue('" + vOut + "µ" + ContentType + "µ" + FileName + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End If
        End Try
    End Sub
End Class
