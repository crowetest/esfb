﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CSQ_Return
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1368) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim UserID As String = CStr(Session("UserID"))
            DT = DB.ExecuteDataSet("select b.branch_id,b.branch_name from emp_master a inner join branch_master b on a.branch_id = b.branch_id where a.emp_code = '" + UserID + "'").Tables(0)
            Me.hidBid.Value = DT.Rows(0)(0).ToString()
            Me.hidBName.Value = DT.Rows(0)(1).ToString()
            Dim QS1 As String = ""
            DT1 = DB.ExecuteDataSet("SELECT QUESTION_ID,QUESTION FROM CSQ_QUESTION_MASTER WHERE SECTION_ID = 1 AND STATUS_ID = 1 ORDER BY QUESTION_ID").Tables(0)
            For n As Integer = 0 To DT1.Rows.Count - 1
                QS1 += DT1.Rows(n)(0).ToString() + "ÿ" + DT1.Rows(n)(1).ToString() + "Ñ"
            Next
            Me.hidQS1.Value = QS1
            Dim QS2 As String = ""
            DT2 = DB.ExecuteDataSet("SELECT QUESTION_ID,QUESTION FROM CSQ_QUESTION_MASTER WHERE SECTION_ID = 2 AND STATUS_ID = 1 ORDER BY QUESTION_ID").Tables(0)
            For m As Integer = 0 To DT2.Rows.Count - 1
                QS2 += DT2.Rows(m)(0).ToString() + "ÿ" + DT2.Rows(m)(1).ToString() + "Ñ"
            Next
            Me.hidQS2.Value = QS2
            'DT3 = DB.ExecuteDataSet("SELECT QUARTER_NAME,START_DATE,REPORT_FILING_PERIOD,ID FROM CSQ_FREQ_SETTINGS WHERE ID = (SELECT MAX(ID) FROM CSQ_FREQ_SETTINGS)").Tables(0)
            DT3 = DB.ExecuteDataSet("SELECT QUARTER_NAME,START_DATE,REPORT_FILING_PERIOD,ID,convert(date,DATEADD(day,DATEDIFF(day, 0,START_DATE ),REPORT_FILING_PERIOD)) as END_DATE,case when getdate() between start_date and convert(date,DATEADD(day,DATEDIFF(day, 0,START_DATE ),REPORT_FILING_PERIOD)) then 1 else 0 end as date_status FROM CSQ_FREQ_SETTINGS WHERE ID = (SELECT MAX(ID) FROM CSQ_FREQ_SETTINGS) ").Tables(0)
            Me.hidQDetails.Value = DT3.Rows(0)(0).ToString() + "ÿ" + DT3.Rows(0)(1).ToString() + "ÿ" + DT3.Rows(0)(2).ToString() + "ÿ" + DT3.Rows(0)(3).ToString() + "ÿ" + DT3.Rows(0)(4).ToString() + "ÿ" + DT3.Rows(0)(5).ToString()
            DT4 = DB.ExecuteDataSet("select * from csq_entry where branch_id = " + DT.Rows(0)(0).ToString() + " and status_id <> 3 and freq_id = " + DT3.Rows(0)(3).ToString()).Tables(0)
            If DT4.Rows.Count = 0 Then
                Me.hidDataHis.Value = CStr(1)
            Else
                Me.hidDataHis.Value = CStr(0)
            End If
            Me.Master.subtitle = "Quaterly Returns On Customer Service"
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", " tablefill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim Branch_ID As Integer = CInt(Data(1).ToString())
            Dim Created_By As Integer = CInt(Session("UserID"))
            Dim Freq_ID As Integer = CInt(Data(2).ToString())
            Dim Ans1 As String = Data(3).ToString()
            Dim Ans2 As String = Data(4).ToString()
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Try
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@Branch_ID", SqlDbType.Int)
                Params(0).Value = Branch_ID
                Params(1) = New SqlParameter("@Created_By", SqlDbType.Int)
                Params(1).Value = Created_By
                Params(2) = New SqlParameter("@Freq_ID", SqlDbType.Int)
                Params(2).Value = Freq_ID
                Params(3) = New SqlParameter("@Ans1", SqlDbType.VarChar, 5000)
                Params(3).Value = Ans1
                Params(4) = New SqlParameter("@Ans2", SqlDbType.VarChar, 5000)
                Params(4).Value = Ans2
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@Status_ID", SqlDbType.Int)
                Params(7).Value = 1 'status_id 1 for save
                DB.ExecuteNonQuery("SP_CSQ_RETURN_SAVE", Params)
                Message = CStr(Params(6).Value)
                ErrorFlag = CInt(Params(5).Value)
            Catch ex As Exception
                Message = Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
        End If
    End Sub
End Class