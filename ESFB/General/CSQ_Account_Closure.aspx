﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CSQ_Account_Closure.aspx.vb" Inherits="CSQ_Account_Closure" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 25%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    var close_len;
    window.onload = function () {
        AddRow(0);
    }
    function FromServer(arg, context) {             
        if (context == 1) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0){
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");        
            }        
        }
        else if (context == 2) {                
             
        }
    }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
    function btnSave_onclick() {
           var Close_His = "";
           var QDetails = document.getElementById("<%= hidQDetails.ClientID %>").value.split("ÿ"); 
            var quarter = QDetails[3].toString();
           for(var i = 1; i <= close_len; i++){
                if(document.getElementById("customerName_"+(i)).value == -1){
                    
                }
                else{
                    Close_His += document.getElementById("customerName_"+(i)).value ;
                    Close_His += "µ";
                    Close_His += document.getElementById("accountNumber_"+(i)).value ; 
                    Close_His += "µ";
                    Close_His += document.getElementById("closureDate_"+(i)).value ; 
                    Close_His += "µ";
                    Close_His += document.getElementById("cmbReason_"+(i)).value ; 
                    Close_His += "µ";
                    Close_His += document.getElementById("remarks_"+(i)).value ; 
                    Close_His += "¥"; 
                    if(document.getElementById("customerName_"+(i)).value == ""){
                        alert("Enter Customer Name");
                        return false;
                    }
                    if(document.getElementById("accountNumber_"+(i)).value == ""){
                        alert("Enter Account Number");
                        return false;
                    }
                    if(document.getElementById("closureDate_"+(i)).value == ""){
                        alert("Enter Closure Date");
                        return false;
                    }
                    if(document.getElementById("cmbReason_"+(i)).value == -1){
                        alert("Select Reason");
                        return false;
                    }
                }
           }
           var ToData = "1Ø" + Close_His + "Ø" + quarter;
	       ToServer(ToData, 1);
    }
    function TableFill(j){
        var CloseData = "";
//        CloseData = document.getElementById("<%= hidCloseData.ClientID %>").value.split("Ñ");
        document.getElementById("<%= pnlClosure.ClientID %>").style.display = '';
        var row_bg = 0;
        var tab = "";            
        tab += "<div style='width:85%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'>";
        tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center' id='ClosureTable'>";
        tab += "<tr class=mainhead style='height:35px;'>";
        tab += "<td style='width:19%;text-align:center'>Customer Name</td>";
        tab += "<td style='width:19%;text-align:center' >Account Number</td>";
        tab += "<td style='width:19%;text-align:center;'>Date Of Closure</td>";
        tab += "<td style='width:19%;text-align:center;' >Reason</td>";
        tab += "<td style='width:19%;text-align:center;'>Remarks</td>";
        tab += "<td style='width:4%;text-align:center;'></td>";
        tab += "</tr>";
        for(var k=1;k<=j;k++){
//            var CloseRow = CloseData[k].split("ÿ"); 
            tab += "<tr class=sub_first style ='height:30px;'>";
            tab += "<td style='width:19%;text-align:center'><input type='text' id = 'customerName_" + k + "' style='width:95%;height:90%;text-align:left;'></input></td>";
            tab += "<td style='width:19%;text-align:center'><input type='text' id = 'accountNumber_" + k + "' style='width:95%;height:90%;text-align:left;'></input></td>";
            tab += "<td style='width:19%;text-align:center'><input type='date' name='closureDate_"+k+"' id='closureDate_"+k+"' max='2019-12-30' style='width:95%;height:90%;' onkeydown='return false'</td>";
            var select = "<select id='cmbReason_"+k+"' class='NormalText' name='cmbReason_"+k+"' style='width:99%;height:90%'>";
            tab += "<td style='width:19%;text-align:center'>"+select+"</td>";
            tab += "<td style='width:19%;text-align:center'><textarea id = 'remarks_" + k + "' style='width:99%;text-align:left;' onkeypress='return TextAreaCheck(event)' ></textarea></td>";
            tab += "<td style='width:5%;text-align:center' onclick=AddRow("+k+")><img id='Add"+k+"' src='../Image/addd1.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;display:none;' title='ADD'/></td>";
            tab += "</tr>";
        }
        close_len = j; 
        tab += "</table></div>";
        document.getElementById("<%= pnlClosure.ClientID %>").innerHTML = tab;  
        document.getElementById("Add"+j).style.display = '';
        for(var k=1;k<=j;k++){
            var reason = document.getElementById("<%= hidReason.ClientID %>").value;
            ComboFill(reason,"cmbReason_"+k);
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd
            } 
            if(mm<10){
                mm='0'+mm
            } 
            today = yyyy+'-'+mm+'-'+dd;
            document.getElementById("closureDate_"+k).setAttribute("max", today);
        }
        try{
            var rowData = document.getElementById("<%= hdnCloseHis.ClientID %>").value.split("¥");
            for(var i = 1;i<=rowData.length;i++){
                var cData = rowData[i].toString();
                var closeData = cData.split("µ");
                for(var j = 1;j<=closeData.length;j++){
                    document.getElementById("customerName_"+i).value = closeData[0];
                    document.getElementById("accountNumber_"+i).value = closeData[1];
                    document.getElementById("closureDate_"+i).value = closeData[2];
                    document.getElementById("cmbReason_"+i).value = closeData[3];
                    document.getElementById("remarks_"+i).value = closeData[4];
                }
            }
        }catch(e){}
        var dataHis = document.getElementById("<%= hidDataHis.ClientID %>").value;
        if(dataHis[0] == '0'){
//            document.getElementById("<%= pnlClosure.ClientID %>").innerHTML = "<p align='center'>Already submitted for this month</p>";
            document.getElementById("btnSave").style.display = "none";
            document.getElementById("<%= pnlClosure.ClientID %>").style.display = 'none';
        }
        var QDetails = document.getElementById("<%= hidQDetails.ClientID %>").value.split("ÿ"); 
        if (QDetails[5] == 0){
//            document.getElementById("<%= pnlClosure.ClientID %>").innerHTML = "<p align='center'>Already submitted for this month</p>";
            document.getElementById("btnSave").style.display = "none";
            document.getElementById("<%= pnlClosure.ClientID %>").style.display = 'none';
        }
    }
    function AddRow(i){
        if(i!=0){
            if( document.getElementById("customerName_"+i).value != ""){
                updateValue(i);
                TableFill(i+1);
            }
            else{
                if( document.getElementById("customerName_"+i).value == ""){
                    alert("Enter Customer Name");
                    return false;
                }
                if( document.getElementById("accountNumber_"+i).value == ""){
                    alert("Enter Account Number");
                    return false;
                }
                if( document.getElementById("closureDate_"+i).value == ""){
                    alert("Select Closure Date");
                    return false;
                }
                if( document.getElementById("cmbReason_"+i).value == -1){
                    alert("Select Reason");
                    return false;
                }
            }
        }
        else{            
            updateValue(i);
            TableFill(i+1);
        }
    }
    function updateValue(id){
        row = document.getElementById("<%= hdnCloseHis.ClientID %>").value.split("¥");
        var NewStr=""
        for (i = 1; i <= id ; i++) {
                var customerName = document.getElementById("customerName_"+i).value;
                var accountNumber = document.getElementById("accountNumber_"+i).value;
                var closeDate = document.getElementById("closureDate_"+i).value;
                var reason = document.getElementById("cmbReason_"+i).value;
                var remarks = document.getElementById("remarks_"+i).value;
                NewStr+="¥" + customerName + "µ" + accountNumber + "µ" + closeDate + "µ" + reason + "µ" + remarks;
        }
        document.getElementById("<%= hdnCloseHis.ClientID %>").value=NewStr;
    }
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 0; a < rows.length-1; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function ClearCombo(control) {
        document.getElementById(control).options.length = 0;
        var option1 = document.createElement("OPTION");
        option1.value = -1;
        option1.text = " -----Select-----";
        document.getElementById(control).add(option1);
    } 
</script>
   
</head>
</html>
<br />
<br />
<br
 <table class="style1" style="width:80%;margin: 0px auto;">
    <tr> 
        <td style="width:10%;">
            <asp:HiddenField ID="hidData" runat="server" />
            <asp:HiddenField ID="hidReason" runat="server" />
            <asp:HiddenField ID="hidCloseData" runat="server" />
            <asp:HiddenField ID="hdnCloseHis" runat="server" />
            <asp:HiddenField ID="hidQDetails" runat="server" />
            <asp:HiddenField ID="hidDataHis" runat="server" />
        </td>
        <td style="width:80%; text-align:left;">
        </td>
        <td style="width:10%">
        </td>
    </tr>
    <tr>
        <td style="width:10%;"></td>
        <td style="width:80%;">
            <asp:Panel ID="pnlClosure" runat="server">
            </asp:Panel>
        </td>
        <td style="width:10%;"></td>
    </tr>    
</table>
<table class="style1" style="width:80%;margin: 0px auto;">   
    <tr>
        <td style="width:100%;" colspan="4">
        </td>
    </tr>
    <tr>
        <td style="text-align:center;" colspan="4">
            <br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="SAVE" onclick="return btnSave_onclick()"/>&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
        </td>
    </tr>
</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

