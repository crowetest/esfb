﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CSQ_EMS
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim EditFlag As Integer = 0
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1369) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If

            Me.Master.subtitle = "Extra Mile Service"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim EmpCode = CInt(Session("UserID"))
            Dim CHANNEL As String = ""

            DT = GF.GetQueryResult("SELECT -1,'--SELECT--' CHANNEL UNION ALL SELECT CHANNEL_ID,CHANNEL FROM CSQ_EMS_CHANNEL ORDER BY 2")
            For n As Integer = 0 To DT.Rows.Count - 1
                CHANNEL += DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString() + "Ñ"
            Next
            Me.hidChannel.Value = CHANNEL
            Me.hidEMSDetails.Value = ""
            Me.hdnEMSHis.Value = ""
            Dim UserID As String = CStr(Session("UserID"))
            DT1 = DB.ExecuteDataSet("SELECT QUARTER_NAME, START_DATE, FILING_PERIOD,QUARTER_ID,CONVERT(DATE,DATEADD(DAY,DATEDIFF(DAY,0,START_DATE),FILING_PERIOD)) AS END_DATE,CASE WHEN GETDATE() BETWEEN START_DATE AND CONVERT(DATE,DATEADD(DAY,DATEDIFF(DAY,0,START_DATE),FILING_PERIOD)) THEN 1 ELSE 0 END AS DATE_STATUS FROM CSQ_EMS_SETTINGS WHERE QUARTER_ID = (SELECT MAX(QUARTER_ID) FROM CSQ_EMS_SETTINGS)").Tables(0)
            Me.hidEMSDetails.Value = DT1.Rows(0)(0).ToString() + "ÿ" + DT1.Rows(0)(1).ToString() + "ÿ" + DT1.Rows(0)(2).ToString() + "ÿ" + DT1.Rows(0)(3).ToString() + "ÿ" + DT1.Rows(0)(4).ToString() + "ÿ" + DT1.Rows(0)(5).ToString()
            DT2 = DB.ExecuteDataSet("SELECT a.*,b.attachment,b.content_type,b.attach_file_name FROM CSQ_EMS_ENTRY a inner join dms_esfb.dbo.csq_ems b on a.quarterID=b.Quarterid and a.branchid=b.branch_id and a.entryID=b.EntryID WHERE a.BRANCHID = (SELECT BRANCH_ID FROM EMP_MASTER WHERE EMP_CODE = " + UserID + ") AND a.QUARTERID = " + DT1.Rows(0)(3).ToString()).Tables(0)
            If DT2.Rows.Count = 0 Then
                Me.hidDataHis.Value = CStr(1)
                Me.hidRowCount.Value = CStr(0)
                EditFlag = 0
            Else
                Me.hidDataHis.Value = CStr(0)
                Me.hidRowCount.Value = CStr(DT2.Rows.Count)
                EditFlag = 1
            End If
            Dim QS1 As String = ""
            For n As Integer = 0 To DT2.Rows.Count - 1
                QS1 += "¥" + DT2.Rows(n)(3).ToString() + "µ" + DT2.Rows(n)(4).ToString() + "µ" + DT2.Rows(n)(5).ToString() + "µ" + DT2.Rows(n)(6).ToString() + "µ" + System.Convert.ToBase64String(CType(DT2.Rows(n)(10), Byte())) + "µ" + DT2.Rows(n)(11).ToString() + "µ" + DT2.Rows(n)(12).ToString() + "µ" + DT2.Rows(n)(9).ToString()
            Next
            Me.hdnEMSHis.Value = QS1
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim EMSData() As String = Data(1).ToString().Split(CChar("¥"))
            Dim QuarterID As Integer = CInt(Data(2).ToString())
            For n As Integer = 0 To EMSData.Length - 2
                Dim ContentType As String = ""
                Dim AttachImg As Byte() = Nothing
                Dim FileName As String = ""
                Dim EMSDetail() As String = EMSData(n).Split(CChar("µ"))
                ContentType = EMSDetail(5)
                AttachImg = System.Convert.FromBase64String(EMSDetail(4))
                FileName = EMSDetail(6)
                Dim ChannelID As Integer = CInt(EMSDetail(0))
                Dim CustomerName As String = EMSDetail(1)
                Dim CustomerAcNo As String = EMSDetail(2)
                Dim CustomerMobile As String = EMSDetail(3)
                Dim Remarks As String = EMSDetail(7)
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(13) As SqlParameter
                    Params(0) = New SqlParameter("@SubmittedBy", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@EMSData", SqlDbType.VarChar)
                    Params(1).Value = ""
                    Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(2).Direction = ParameterDirection.Output
                    Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@QuarterId", SqlDbType.Int)
                    Params(4).Value = QuarterID
                    Params(5) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                    Params(5).Value = AttachImg
                    Params(6) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                    Params(6).Value = ContentType
                    Params(7) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                    Params(7).Value = FileName
                    Params(8) = New SqlParameter("@ChannelID", SqlDbType.Int)
                    Params(8).Value = ChannelID
                    Params(9) = New SqlParameter("@CustomerName", SqlDbType.VarChar, 100)
                    Params(9).Value = CustomerName
                    Params(10) = New SqlParameter("@CustomerAcNo", SqlDbType.VarChar, 50)
                    Params(10).Value = CustomerAcNo
                    Params(11) = New SqlParameter("@CustomerMobile", SqlDbType.VarChar, 20)
                    Params(11).Value = CustomerMobile
                    Params(12) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                    Params(12).Value = Remarks
                    Params(13) = New SqlParameter("@EditFlag", SqlDbType.Int)
                    Params(13).Value = EditFlag
                    DB.ExecuteNonQuery("SP_CSQ_EMS_SAVE", Params)
                    ErrorFlag = CInt(Params(2).Value)
                    Message = CStr(Params(3).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                EditFlag = 0
                CallBackReturn = ErrorFlag.ToString + "Ø" + Message
            Next

        ElseIf CInt(Data(0)) = 2 Then
        End If
    End Sub
#End Region
End Class
