﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Update_Salary_Inform
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GF As New GeneralFunctions
#Region "Page Load & Disposed"
    Protected Sub Birthday_Submit_Break_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            txtBranchId.Text = CStr(Session("BranchID"))
            txtBranch.Text = CStr(Session("BranchName"))
            Dim Mn As Integer = Month(Now.Date())
            Dim ye As Integer = Year(Now.Date())
            DT = DB.ExecuteDataSet("select * from salary_inform where branch_id=" & CInt(txtBranchId.Text) & " and month_id=" & Mn & " and YEAR_ID=" & ye & " and status_id=1").Tables(0)
            If DT.Rows.Count <> 0 Then
                Dim cl_script1 As New System.Text.StringBuilder
                cl_script1.Append("         alert(' The salary information already submited in this month ');")
                cl_script1.Append("         window.open('../Home.aspx','_self'); ")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)
                Exit Sub
            End If
            hid_Item.Value = ""
            hid_Value.Value = ""
            Dim i As Integer = 0

            Dim DTT As New DataTable
            DT = DB.ExecuteDataSet("SELECT E.EMP_CODE,E.EMP_NAME,isnull(D.IFSC_CODE,''),isnull(D.ACNO,''),   isnull(D.EMP_TYPE,'') ,ISNULL(D.EMP_TYPE_DATE,''),isnull(D.LOP,0),isnull(D.CL,0),isnull(D.ML,0),isnull(D.PL,0),isnull(D.SL,0), " & _
                 " isnull(D.ESI,0),isnull(D.OTHERS,0),isnull(D.ARREAR,0),ISNULL( D.ARREAR_REMARK,''),isnull(D.ADDITIONAL_PAYMENTS,0),ISNULL(D.PAYMENT_REMARK ,'') ,E.Emp_status_id,S.EMP_STATUS FROM EMPLOYEE_STATUS S, EMP_MASTER E  LEFT OUTER JOIN  " & _
                 " (SELECT D.IFSC_CODE,D.ACNO,D.EMP_CODE,D.EMP_TYPE,D.EMP_TYPE_DATE,D.LOP,D.CL,D.ML,D.PL,D.SL,D.ESI,D.OTHERS,D.ARREAR, D.ARREAR_REMARK, D.ADDITIONAL_PAYMENTS, D.PAYMENT_REMARK FROM SALARY_INFORM S, " & _
                 " SALARY_INFORM_DTLS D WHERE S.SALARY_ID=D.SALARY_ID AND  S.BRANCH_ID=" & CInt(txtBranchId.Text) & " AND S.MONTH_ID=" & Mn & " AND S.YEAR_ID=" & ye & ")D ON  E.EMP_CODE=D.EMP_CODE WHERE " & _
                 " S.EMP_STATUS_ID=E.EMP_STATUS_ID AND E.BRANCH_ID=" & CInt(txtBranchId.Text) & " AND E.STATUS_ID=1 ORDER BY E.EMP_CODE").Tables(0)
            If DT.Rows.Count <> 0 Then
                hid_Item.Value = DT.Rows.Count.ToString
                While i < DT.Rows.Count
                    Dim Emp_type As String = Nothing
                    If CStr(DT.Rows(i)(4)) = "0" Then
                        Emp_type = "-1"
                    Else
                        Emp_type = CStr(DT.Rows(i)(4))
                    End If
                    'BANK DETAILS
                    DTT = DB.ExecuteDataSet(" SELECT isnull(IFSC_CODE,''),isnull(ACNO,'') FROM SALARY_BANK_DETAILS WHERE EMP_CODE=" & CInt(DT.Rows(i)(0)) & "  ").Tables(0)
                    If CStr(DTT.Rows(0)(0)) <> "" Then
                        hid_Value.Value += CStr(DT.Rows(i)(0)) & "~" & CStr(DT.Rows(i)(1)) & "~" & CStr(DTT.Rows(0)(0)) & "~" & CStr(DTT.Rows(0)(1)) & "~" & CStr(Emp_type) & "~" & CStr(DT.Rows(i)(5)) & "~" & CStr(DT.Rows(i)(6)) & "~" & CStr(DT.Rows(i)(7)) & "~" & CStr(DT.Rows(i)(8)) & "~" & CStr(DT.Rows(i)(9)) & "~" & CStr(DT.Rows(i)(10)) & "~" & CStr(DT.Rows(i)(11)) & "~" & CStr(DT.Rows(i)(12)) & "~" & CStr(DT.Rows(i)(13)) & "~" & CStr(DT.Rows(i)(14)) & "~" & CStr(DT.Rows(i)(15)) & "~" & CStr(DT.Rows(i)(16)) & "~1~" & CStr(DT.Rows(i)(17)) & "~" & CStr(DT.Rows(i)(18))
                    Else
                        hid_Value.Value += CStr(DT.Rows(i)(0)) & "~" & CStr(DT.Rows(i)(1)) & "~" & CStr(DT.Rows(i)(2)) & "~" & CStr(DT.Rows(i)(3)) & "~" & CStr(Emp_type) & "~" & CStr(DT.Rows(i)(5)) & "~" & CStr(DT.Rows(i)(6)) & "~" & CStr(DT.Rows(i)(7)) & "~" & CStr(DT.Rows(i)(8)) & "~" & CStr(DT.Rows(i)(9)) & "~" & CStr(DT.Rows(i)(10)) & "~" & CStr(DT.Rows(i)(11)) & "~" & CStr(DT.Rows(i)(12)) & "~" & CStr(DT.Rows(i)(13)) & "~" & CStr(DT.Rows(i)(14)) & "~" & CStr(DT.Rows(i)(15)) & "~" & CStr(DT.Rows(i)(16)) & "~0~" & CStr(DT.Rows(i)(17)) & "~" & CStr(DT.Rows(i)(18))
                    End If
                    'LEAVE TAKEN
                    Dim da As Integer = 30
                    Dim FromDt As New DateTime(CDate(Session("TraDt")).Year, CDate(Session("TraDt")).Month, 1)
                    If CDate(Session("TraDt")).Month = 1 Or CDate(Session("TraDt")).Month = 3 Or CDate(Session("TraDt")).Month = 5 Or CDate(Session("TraDt")).Month = 7 Or CDate(Session("TraDt")).Month = 8 Or CDate(Session("TraDt")).Month = 10 Or CDate(Session("TraDt")).Month = 12 Then
                        da = 31
                    ElseIf CDate(Session("TraDt")).Month = 2 Then
                        da = 28
                    End If
                    Dim ToDt As New DateTime(CDate(Session("TraDt")).Year, CDate(Session("TraDt")).Month, da)
                    DTT = DB.ExecuteDataSet("SELECT ISNULL(SUM(CASE WHEN Leave_Type=1 THEN  Leave_Days ELSE 0 END),0),ISNULL(SUM(CASE WHEN Leave_Type=2 THEN  Leave_Days ELSE 0 END),0)," & _
                    " ISNULL(SUM(CASE WHEN Leave_Type=3 THEN  Leave_Days ELSE 0 END),0),case when e.gender_id=1 then ISNULL(SUM(CASE WHEN Leave_Type=5 THEN  Leave_Days ELSE 0 END),0) else " & _
                    " ISNULL(SUM(CASE WHEN Leave_Type=4 THEN  Leave_Days ELSE 0 END),0) end,ISNULL(SUM(CASE WHEN Leave_Type=6 THEN  Leave_Days ELSE 0 END),0)  " & _
                    "  FROM EMP_LEAVE_REQUEST L,Emp_master E  " & _
                    " WHERE L.emp_code=E.emp_code and L.Emp_Code =" & CInt(DT.Rows(i)(0)) & " AND  L.STATUS_ID<5  " & _
                    " and DATEADD(day, DATEDIFF(day, 0, Leave_From), 0) Between '" + FromDt.ToString("MM/dd/yyyy") + "' and '" + ToDt.ToString("MM/dd/yyyy") + "' group by e.gender_id    ").Tables(0)
                    If DTT.Rows.Count <> 0 Then
                        hid_Value.Value += "~" & CStr(DTT.Rows(0)(0)) & "~" & CStr(DTT.Rows(0)(1)) & "~" & CStr(DTT.Rows(0)(2)) & "~" & CStr(DTT.Rows(0)(3)) & "~" & CStr(DTT.Rows(0)(4))
                    Else
                        hid_Value.Value += "~~~~~"
                    End If
                    DTT = DB.ExecuteDataSet(" SELECT L.CL_Bal ,L.SL_Bal ,L.PL_Bal , CASE WHEN E.GENDER_ID=1 THEN 3 ELSE 90 END, 365 AS LOP FROM EMP_LEAVE_BALANCE L ,EMP_MASTER E WHERE L.EMP_CODE=E.EMP_CODE AND L.Emp_Code =" & CInt(DT.Rows(i)(0)) & "").Tables(0)
                    If DTT.Rows.Count <> 0 Then
                        hid_Value.Value += "~" & CStr(DTT.Rows(0)(0)) & "~" & CStr(DTT.Rows(0)(1)) & "~" & CStr(DTT.Rows(0)(2)) & "~" & CStr(DTT.Rows(0)(3)) & "~" & CStr(DTT.Rows(0)(4))
                    End If
                    DTT = DB.ExecuteDataSet("SELECT ISNULL(D.STATUS_REMARK ,'')  FROM SALARY_INFORM S,SALARY_INFORM_DTLS D WHERE S.SALARY_ID=D.SALARY_ID AND " & _
                        "   S.BRANCH_ID=" & CInt(txtBranchId.Text) & " AND S.MONTH_ID=" & Mn & " AND S.YEAR_ID=" & ye & " AND  D.EMP_CODE=" & CInt(DT.Rows(i)(0)) & "").Tables(0)
                    If DTT.Rows.Count <> 0 Then
                        hid_Value.Value += "~" & CStr(DTT.Rows(0)(0))
                    Else
                        hid_Value.Value += "~"
                    End If
                    hid_Value.Value += "¥"
                    i = i + 1
                End While
            End If

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub Birthday_Submit_Break_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub

#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim ErrorFlag As Integer = 0
            Dim Message As String = ""
            Dim BranchId As Integer = CInt(Data(1))
            Dim Hid_data As String = CStr(Data(2))
            Dim MonthId As Integer = CInt(Data(3))
            Dim YearId As Integer = CInt(Data(4))
            Dim UserID As Integer = CInt(Session("UserID"))
            Try
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(0).Value = BranchId
                Params(1) = New SqlParameter("@hid_Data", SqlDbType.VarChar,5000)
                Params(1).Value = Hid_data
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = CInt(UserID)
                Params(5) = New SqlParameter("@Month", SqlDbType.Int)
                Params(5).Value = CInt(MonthId)
                Params(6) = New SqlParameter("@yearId", SqlDbType.Int)
                Params(6).Value = CInt(YearId)
                DB.ExecuteNonQuery("SP_UPDATE_SALARY_INFORM", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "~" + Message
        ElseIf CInt(Data(0)) = 2 Then
            Dim BranchId As Integer = CInt(Data(1))
            Dim Month As Integer = CInt(Data(2))
            Dim YearId As Integer = CInt(Data(3))
            DT = DB.ExecuteDataSet("SELECT * FROM SALARY_INFORM S,SALARY_INFORM_DTLS D WHERE S.SALARY_ID=D.SALARY_ID AND (IFSC_CODE = 'null' OR ACNO ='null') AND  branch_id=" & BranchId & " and month_id=" & Month & " and year_id=" & YearId & " ").Tables(0)
            If DT.Rows.Count <> 0 Then
                CallBackReturn = "3~Update IFSC CODE/ACNO"
            Else
                Dim IntVal As Integer = 0
                IntVal = DB.ExecuteNonQuery("UPDATE SALARY_INFORM set status_id=1 where branch_id=" & BranchId & " and month_id=" & Month & " and year_id=" & YearId & " ")
                CallBackReturn = CStr(IntVal) + "~"
            End If
        ElseIf CInt(Data(0)) = 3 Then   'CL 
            Dim Empcode As Integer = CInt(Data(1))
            Dim Month_id As Integer = CInt(Data(2))

            Dim CLCount As Double = 0
            Dim SLCount As Double = 0
            Dim PLCount As Double = 0
            Dim LOPCount As Double = 365
            Dim MLCount As Double = 85
            Dim CLTaken As Double = 0
            Dim SLTaken As Double = 0
            Dim PLTaken As Double = 0
            Dim LOPTaken As Double = 0
            Dim MLTaken As Double = 0
            DT = DB.ExecuteDataSet(" SELECT CL_Bal ,SL_Bal ,PL_Bal  FROM EMP_LEAVE_BALANCE WHERE Emp_Code =" & Empcode & "").Tables(0)
            If DT.Rows.Count <> 0 Then
                CLCount = CInt(DT.Rows(0)(0))
                SLCount = CInt(DT.Rows(0)(1))
                PLCount = CInt(DT.Rows(0)(2))
            End If
            DT = DB.ExecuteDataSet("SELECT ISNULL(SUM(CASE WHEN Leave_Type=1 THEN  Leave_Days ELSE 0 END),0),ISNULL(SUM(CASE WHEN Leave_Type=2 THEN  Leave_Days ELSE 0 END),0),ISNULL(SUM(CASE WHEN Leave_Type=3 THEN  Leave_Days ELSE 0 END),0)  FROM EMP_LEAVE_REQUEST   " & _
                " WHERE Emp_Code =" & Empcode & " AND  STATUS_ID<5 and Leave_From >='1-01-2016' ").Tables(0)
            If DT.Rows.Count <> 0 Then
                CLTaken = CInt(DT.Rows(0)(0))
                SLTaken = CInt(DT.Rows(0)(1))
                PLTaken = CInt(DT.Rows(0)(2))
            End If
            DT = DB.ExecuteDataSet("select sum(d.lop),sum(d.ml) from salary_inform s , salary_inform_dtls d  where s.salary_id=d.salary_id and d.emp_code=" & Empcode & " and s. month_id=").Tables(0)
            If DT.Rows.Count <> 0 Then

            End If
            CLCount = CLCount - CLTaken
            SLCount = SLCount - SLTaken
            PLCount = PLCount - PLTaken
        End If
    End Sub
#End Region


    
End Class
