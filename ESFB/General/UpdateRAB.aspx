﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="UpdateRAB.aspx.vb" Inherits="General_UpdateRAB" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
        </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

            function window_onload() {
                var ID = document.getElementById("<%= cmbCategory.ClientID %>").value;
                document.getElementById("<%= hdnData.ClientID %>").value = "";
                document.getElementById("<%= hdnUpData.ClientID %>").value = "";
                if(ID == 1)
                {
                    document.getElementById("rowRegion").style.display = "";
                    document.getElementById("rowArea").style.display = "none";
                    document.getElementById("rowBranch").style.display = "none";
                }
                else if(ID == 2)
                {
                    document.getElementById("rowRegion").style.display = "none";
                    document.getElementById("rowArea").style.display = "";
                    document.getElementById("rowBranch").style.display = "none";
                }
                else if(ID == 3)
                {
                    document.getElementById("rowRegion").style.display = "none";
                    document.getElementById("rowArea").style.display = "none";
                    document.getElementById("rowBranch").style.display = "";
                }
                ToServer("1ʘ" + ID, 1);
            }

            function CategoryOnChange()
            {
                var ID = document.getElementById("<%= cmbCategory.ClientID %>").value;
                document.getElementById("<%= hdnData.ClientID %>").value = "";
                document.getElementById("<%= hdnUpData.ClientID %>").value = "";
                if(ID == 1)
                {
                    document.getElementById("rowRegion").style.display = "";
                    document.getElementById("rowArea").style.display = "none";
                    document.getElementById("rowBranch").style.display = "none";
                }
                else if(ID == 2)
                {
                    document.getElementById("rowRegion").style.display = "none";
                    document.getElementById("rowArea").style.display = "";
                    document.getElementById("rowBranch").style.display = "none";
                }
                else if(ID == 3)
                {
                    document.getElementById("rowRegion").style.display = "none";
                    document.getElementById("rowArea").style.display = "none";
                    document.getElementById("rowBranch").style.display = "";
                }
                ToServer("1ʘ" + ID, 1);
            }

            function btnSave_onclick() {
                var ID = document.getElementById("<%= cmbCategory.ClientID %>").value;
                var ChangeData = "";

                var row = document.getElementById("<%= hdnUpData.ClientID %>").value.split("ʘ");
                var Length = row.length - 1;
                document.getElementById("<%= hdnData.ClientID %>").value = "";
                for (n = 0; n < Length; n++) {
                    col = row[n].split("Æ");
                    if (col[5] == 1)                     
                    {
                        if(ID==3)
                            document.getElementById("<%= hdnData.ClientID %>").value += col[0] + "Æ" + col[2]+ "Æ" + col[4]+ "Æ" + col[6] + "¶";
                        else
                            document.getElementById("<%= hdnData.ClientID %>").value += col[0] + "Æ" + col[2] + "¶";
                    }
                }              
                ChangeData = document.getElementById("<%= hdnData.ClientID %>").value;
                ToServer("2ʘ" + ID + "ʘ" + ChangeData, 2);
            }

            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {   
                        if(Arg != "")
                        {
                            document.getElementById("<%= hdnData.ClientID %>").value = Arg;
                            var ID = document.getElementById("<%= cmbCategory.ClientID %>").value;
                            if(ID == 1)
                            {
                                DisplayRegion();
                            }
                            else if(ID == 2)
                            {
                                DisplayArea();
                            }
                            else if(ID == 3)
                            {
                                DisplayBranch();
                            }
                        }
                        break;
                    }
                    case 2:// Confirmation
                    {
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        if (Data[0] == "0") window.open("UpdateRAB.aspx", "_self");
                        break;
                    }
                }
            }

            function DisplayRegion()
            {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>REGION</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:20%;text-align:left'>Region&nbsp;Name</td>";
                Tab += "<td style='width:20%;text-align:left'>Tertiary&nbsp;Name</td>";
                Tab += "<td style='width:25%;text-align:center'>Region&nbsp;Head</td>";
                Tab += "<td style='width:25%;text-align:left'>Name</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                if (document.getElementById("<%= hdnData.ClientID %>").value != "") {
                    var HidArg = document.getElementById("<%= hdnData.ClientID %>").value.split("¶");

                    document.getElementById("<%= hdnUpData.ClientID %>").value = HidArg[0];
                    document.getElementById("rowRegion").style.display = "";                
                    var j;
                    var Total = 0;
                    var row_bg1 = 0;
                    var SlNo = 0;

                    var HidArg1 = document.getElementById("<%= hdnUpData.ClientID %>").value.split("ʘ");
                    var RowCount = HidArg1.length - 1;
                    for (j = 0; j < RowCount; j++) {
                        if (row_bg1 == 0) {
                            row_bg1 = 1;
                            Tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg1 = 0;
                            Tab += "<tr class='sub_second';>";
                        }
                        SlNo = SlNo + 1;
                        var RegionData = HidArg1[j].split("Æ");
                        Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                        Tab += "<td style='width:20%;text-align:left'>" + RegionData[1] + "</td>";

                        var select = "<select id='cmbRegion" + RegionData[0] + "' class='NormalText' name='cmbRegion" + RegionData[0] + "' style='width:100%' onchange='UpdateValueData(" + RegionData[0] + ",1)'>";
                        
                        var rows = HidArg[1].split("Ř"); //Zonal Details
                        for (a = 1; a < rows.length; a++) {
                            var cols = rows[a].split("Ĉ");
                            if (cols[0] == RegionData[2])
                                select += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                            else
                                select += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
                        }
                        
                        Tab += "<td style='width:20%;text-align:center'>" + select + "</td>";
                        Tab += "<td style='width:25%;text-align:center'>" + RegionData[3] + "</td>";
                        Tab += "<td style='width:25%;text-align:left'>" + RegionData[4] + "</td></tr>";
                    }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div>";
                    document.getElementById("<%= pnRegion.ClientID %>").innerHTML = Tab;
                }
                else
                    document.getElementById("rowRegion").style.display = "none";
                    fix_onChange_editable_elements();
            }
                                
            function DisplayArea()
            {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>AREA</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:20%;text-align:left'>Area&nbsp;Name</td>";
                Tab += "<td style='width:20%;text-align:left'>Region&nbsp;Name</td>";
                Tab += "<td style='width:25%;text-align:center'>Area&nbsp;Head</td>";
                Tab += "<td style='width:25%;text-align:left'>Name</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                if (document.getElementById("<%= hdnData.ClientID %>").value != "") {
                    var HidArg = document.getElementById("<%= hdnData.ClientID %>").value.split("¶");

                    document.getElementById("<%= hdnUpData.ClientID %>").value = HidArg[0];
                    document.getElementById("rowArea").style.display = "";                    
                    var j;
                    var Total = 0;
                    var row_bg1 = 0;
                    var SlNo = 0;

                    var HidArg1 = document.getElementById("<%= hdnUpData.ClientID %>").value.split("ʘ");
                    var RowCount = HidArg1.length - 1;
                    for (j = 0; j < RowCount; j++) {
                        if (row_bg1 == 0) {
                            row_bg1 = 1;
                            Tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg1 = 0;
                            Tab += "<tr class='sub_second';>";
                        }
                        SlNo = SlNo + 1;
                        var AreaData = HidArg1[j].split("Æ");
                        Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                        Tab += "<td style='width:20%;text-align:left'>" + AreaData[1] + "</td>";

                        var select = "<select id='cmbArea" + AreaData[0] + "' class='NormalText' name='cmbArea" + AreaData[0] + "' style='width:100%' onchange='UpdateValueData(" + AreaData[0] + ",2)'>";
                        
                        var rows = HidArg[1].split("Ř"); //Region Details
                        for (a = 1; a < rows.length; a++) {
                            var cols = rows[a].split("Ĉ");
                            if (cols[0] == AreaData[2])
                                select += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                            else
                                select += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
                        }
                        
                        Tab += "<td style='width:20%;text-align:center'>" + select + "</td>";
                        Tab += "<td style='width:25%;text-align:center'>" + AreaData[3] + "</td>";
                        Tab += "<td style='width:25%;text-align:left'>" + AreaData[4] + "</td></tr>";
                    }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div>";
                    document.getElementById("<%= pnArea.ClientID %>").innerHTML = Tab;
                }
                else
                    document.getElementById("rowArea").style.display = "none";
                    fix_onChange_editable_elements();
            }

            function DisplayBranch()
            {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>BRANCH</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:5%;text-align:center'>Branch&nbsp;ID</td>";
                Tab += "<td style='width:15%;text-align:left'>Branch&nbsp;Name</td>";
                Tab += "<td style='width:20%;text-align:left'>Area&nbsp;Name</td>";
                Tab += "<td style='width:20%;text-align:left'>Branch&nbsp;Head</td>";
                Tab += "<td style='width:30%;text-align:left'>Address</td>";
                Tab += "<td style='width:10%;text-align:left'>Phone Number</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                if (document.getElementById("<%= hdnData.ClientID %>").value != "") {
                    var HidArg = document.getElementById("<%= hdnData.ClientID %>").value.split("¶");

                    document.getElementById("<%= hdnUpData.ClientID %>").value = HidArg[0];
                    document.getElementById("rowBranch").style.display = "";                    
                    var j;
                    var Total = 0;
                    var row_bg1 = 0;
                    var SlNo = 0;

                    var HidArg1 = document.getElementById("<%= hdnUpData.ClientID %>").value.split("ʘ");
                    var RowCount = HidArg1.length - 1;
                    for (j = 0; j < RowCount; j++) {
                        if (row_bg1 == 0) {
                            row_bg1 = 1;
                            Tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg1 = 0;
                            Tab += "<tr class='sub_second';>";
                        }
                        SlNo = SlNo + 1;
                        var BranchData = HidArg1[j].split("Æ");
                        Tab += "<td style='width:5%;text-align:center'>" + BranchData[0] + "</td>";
                        Tab += "<td style='width:15%;text-align:left'>" + BranchData[1] + "</td>";

                        var select = "<select id='cmbBranch" + BranchData[0] + "' class='NormalText' name='cmbBranch" + BranchData[0] + "' style='width:100%' onchange='UpdateValueData(" + BranchData[0] + ",3)'>";
                        
                        var rows = HidArg[1].split("Ř"); //Area Details
                        for (a = 1; a < rows.length; a++) {
                            var cols = rows[a].split("Ĉ");
                            if (cols[0] == BranchData[2])
                                select += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                            else
                                select += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
                        }
                        
                        Tab += "<td style='width:20%;text-align:center'>" + select + "</td>";
                        Tab += "<td style='width:20%;text-align:left'>" + BranchData[3] + "</td>";
                        Tab += "<td style='width:30%;text-align:left'><div class='editable' id='txtAddress"+BranchData[0]+"' contenteditable='true'; onchange='UpdateValueData(" + BranchData[0] + ",3)'>" + BranchData[4] + "</div></td>";
                        Tab += "<td style='width:10%;text-align:left'><div class='editable' id='txtPhone"+BranchData[0]+"' contenteditable='true'; onchange='UpdateValueData(" + BranchData[0] + ",3)'>" + BranchData[6] + "</div></td></tr>";
                    }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div>";
                    document.getElementById("<%= pnBranch.ClientID %>").innerHTML = Tab;
                }
                else
                    document.getElementById("rowBranch").style.display = "none";
            fix_onChange_editable_elements();
            }
                
            function UpdateValueData(Val,ID) {          
                var i = 0;
                var Newstr = "";
                var cmbName = "cmbBranch";
                if(ID == 1)
                    cmbName = "cmbRegion";
                else if(ID ==2)
                    cmbName = "cmbArea";
                var row = document.getElementById("<%= hdnUpData.ClientID %>").value.split("ʘ");
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("Æ");
                    if (col[0] == Val) { 
                    if(ID==3)
                        Newstr += col[0] + "Æ" + col[1] + "Æ" + document.getElementById(cmbName + Val).value + "Æ" + col[3] + "Æ" + document.getElementById("txtAddress"+ Val).innerHTML + "Æ1Æ" + document.getElementById("txtPhone"+ Val).innerHTML + "ʘ";
                    else
                        Newstr += col[0] + "Æ" + col[1] + "Æ" + document.getElementById(cmbName + Val).value + "Æ" + col[3] + "Æ" + col[4] + "Æ1ʘ";
                    }
                    else
                        Newstr += row[n] + "ʘ";
                }
                document.getElementById("<%= hdnUpData.ClientID %>").value = Newstr;
            }

            function btnExit_onclick() {
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
function fix_onChange_editable_elements()
{
  var tags = document.querySelectorAll('[contenteditable=true][onChange]');//(requires FF 3.1+, Safari 3.1+, IE8+)
  for (var i=tags.length-1; i>=0; i--) if (typeof(tags[i].onblur)!='function')
  {
    tags[i].onfocus = function()
    {
      this.data_orig=this.innerHTML;
    };
    tags[i].onblur = function()
    {
      if (this.innerHTML != this.data_orig)
        this.onchange();
      delete this.data_orig;
    };
  }
}

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <br />
    <br />
    <table align="center" style="width: 80%; margin: 0px auto; font-family: Cambria;">
        <tr>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 25%; text-align: left;">
                Select Category
            </td>
            <td style="width: 25%; text-align: left;">
                <asp:DropDownList ID="cmbCategory" Width="100%" runat="server" Font-Names="Cambria">
                    <asp:ListItem Value="1">Region</asp:ListItem>
                    <asp:ListItem Value="2">Area</asp:ListItem>
                    <asp:ListItem Value="3">Branch</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr id="rowRegion">
            <td style="text-align: left;" colspan="4">
                <asp:Panel ID="pnRegion" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr id="rowArea">
            <td style="text-align: left;" colspan="4">
                <asp:Panel ID="pnArea" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr id="rowBranch">
            <td style="text-align: left;" colspan="4">
                <asp:Panel ID="pnBranch" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
            <td style="text-align: center;" colspan="2">
                <input id="btnSave" style="font-family: cambria; width: 15%; cursor: pointer;" type="button"
                    value="SAVE" onclick="return btnSave_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;
                <input id="btnExit" onclick="return btnExit_onclick()" style="font-family: cambria;
                    width: 15%; cursor: pointer;" type="button" value="EXIT" />
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnData" runat="server" />
    <asp:HiddenField ID="hdnUpData" runat="server" />
    <br />
</asp:Content>
