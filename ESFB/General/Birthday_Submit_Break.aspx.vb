﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Birthday_Submit_Break
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing
    Dim GF As New GeneralFunctions
#Region "Page Load & Disposed"
    Protected Sub Birthday_Submit_Break_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.txtBranchId.Attributes.Add("onchange", "return BranchOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub Birthday_Submit_Break_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub

#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Try
            Dim Data() As String = eventArgument.Split(CChar("Ø"))
            If CInt(Data(0)) = 1 Then
                DT = DB.ExecuteDataSet("select branch_name from brmaster where branch_id=" & CInt(Data(1)) & "").Tables(0)
                If DT.Rows.Count <> 0 Then
                    CallBackReturn = "1~" + CStr(DT.Rows(0)(0))
                Else
                    CallBackReturn = "2~"
                End If
            ElseIf CInt(Data(0)) = 2 Then
                Dim IntVal As Integer = 0
                IntVal = DB.ExecuteNonQuery("UPDATE BIRTHDAY_SUBMIT set  STATUS_ID=0 WHERE BRANCH_ID=" & CInt(Data(1)) & "")
                CallBackReturn = CStr(IntVal)
            ElseIf CInt(Data(0)) = 3 Then
                DT = DB.ExecuteDataSet("SELECT status_id FROM BIRTHDAY_SUBMIT WHERE BRANCH_ID=" & CInt(Data(1)) & "").Tables(0)
                If DT.Rows.Count <> 0 Then
                    If CInt(DT.Rows(0)(0)) = 1 Then
                        CallBackReturn = "1"
                    Else
                        CallBackReturn = "2"
                    End If
                Else
                    CallBackReturn = "3"
                End If
            End If
        Catch ex As Exception
          
            Response.Redirect("~/CatchException.aspx?ErrorNo=1")
        End Try
    End Sub
#End Region


End Class
