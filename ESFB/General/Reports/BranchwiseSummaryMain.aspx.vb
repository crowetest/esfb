﻿Imports System.Data

Partial Class BranchwiseSummaryMain
    Inherits System.Web.UI.Page

    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim Meeting_Dt As Date
    Dim UserID As Integer
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim DB As New MS_SQL.Connect

    Dim WebTools As New WebApp.Tools

#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Dim AdminFlag As Boolean = False
        Try

            Me.Master.subtitle = "Customer Meeting Summary "
            UserID = CInt(Session("UserID"))
            BranchID = CInt(Session("BranchID"))
            If Not IsPostBack Then
                DT = GMASTER.GetEmpRoleList(UserID)
                DT = DB.ExecuteDataSet("select '-11' as branchid,' ------ALL------' as branch Union All select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id = 1  and -1 < Branch_ID and Branch_ID < 50000  order by 2").Tables(0)
                GF.ComboFill(cmbBranch, DT, 0, 1)
            End If
           
            Me.txtYear.Text = Year(Date.Now)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "Initialize();", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
        DB.dispose()
    End Sub

#End Region



End Class


