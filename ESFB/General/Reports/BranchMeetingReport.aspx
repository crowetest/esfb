﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="BranchMeetingReport.aspx.vb" Inherits="BranchMeetingReport"  %>

<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
    <tr>
            <td style="width:15%;">
                Branch</td>
           <td  style="width:85%;">
                 <asp:DropDownList ID="cmdBranch" class="NormalText" runat="server" Font-Names="Cambria" AutoPostBack="true"
         Width="90%" ForeColor="Black" AppendDataBoundItems="True">
                </asp:DropDownList>
                </td>
           
        </tr>
 
        <tr>
            <td style="width:15%;">
                Date</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbDate" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black" AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">---AS ON DATE---</asp:ListItem>
              
                </asp:DropDownList></td>
           
        </tr>
         <tr>
            <td style="width:30%;">
               <%-- Discussion Status--%></td>
            <td  style="width:85%;">
             <asp:TextBox ID="txtCode" runat="server"  class="NormalText"
                        style=" font-family:Cambria;font-size:10pt;" Width="40%" 
                    MaxLength="50" Visible='false'  />
                
                <asp:DropDownList ID="CmbStatus" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black" AppendDataBoundItems="True" Visible='false'>
                    <asp:ListItem Value="-1">---ALL---</asp:ListItem>
                   <%-- <asp:ListItem Value="1">OPEN</asp:ListItem>
                    <asp:ListItem Value="2">CLOSED</asp:ListItem>--%>
                </asp:DropDownList>
                 
                
                
                </td>
           
        </tr>

        <tr id = "dateTr" style="display:none">
            <td style="width:15%;">
                Meeting Date</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmdMeetDate" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black" AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">---SELECT---</asp:ListItem>
              
                </asp:DropDownList>
                &nbsp; &nbsp;<asp:ImageButton ID="btnAttachView" runat="server" Height="18px" Width="18px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" style="display:none" /><br />
                </td>
           
        </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
              
           
              
              <asp:Button ID="btnView" runat="server" Text="VIEW" Font-Names="Cambria" 
                    style="margin-bottom: 0px" Width="8%" />
                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 10%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
    
      
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
                <asp:HiddenField ID="hidMeeting" runat="server" />
                <asp:HiddenField ID="hidDateID" runat="server" />
                <asp:HiddenField ID="hidAttach" runat="server" />
                <asp:HiddenField ID="hidMeetingID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
     
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }
   
    function BranchOnChange()
    {
        
        var BranchID    = document.getElementById("<%= cmdBranch.ClientID %>").value;       
        var ToData      = "1Ø" + BranchID ;
        ToServer(ToData,1);
              
    }
    function DateOnChange()
    {
        var DateID    = document.getElementById("<%= cmbDate.ClientID %>").value;     
        document.getElementById("<%= hidDateID.ClientID %>").value=DateID; 
        if(DateID == -1){
            alert("Attachments will not be available if date is not selected.");
            document.getElementById("dateTr").style.display = 'none';
        }else{
            var BranchID    = document.getElementById("<%= cmdBranch.ClientID %>").value;  
            var DateID      = document.getElementById("<%= cmbDate.ClientID %>").value; 
            var ToData = "3Ø" + BranchID + "Ø"+ DateID ;
            ToServer(ToData,3);
        }
    }
    function MeetingOnChange(){
        var meetingId = document.getElementById("<%= cmdMeetDate.ClientID %>").value; 
        if(meetingId == -1){
            alert("Attachments will not be available if meeting date is not selected.");
            document.getElementById("<%= btnAttachView.ClientID %>").style.display = "none";
        }else{
            var ToData = "4Ø" + meetingId;
            ToServer(ToData,4);
        }
    }
    function btnView_onclick()
    {    
        var BranchID    = document.getElementById("<%= cmdBranch.ClientID %>").value;  
        var DateID      = document.getElementById("<%= cmbDate.ClientID %>").value; 
        if(DateID == -1){
            alert("Attachments will not be available if date is not selected.");
        }else {
            if(MeetingID == -1){
                alert("Attachments will not be available if meeting date is not selected.");
            }
        }
        var MeetingID      = document.getElementById("<%= cmdMeetDate.ClientID %>").value; 
        var ToData = "2Ø" + BranchID + "Ø"+ DateID + "Ø"+ StatusId;
        ToServer(ToData,2);              
    }
   function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
       
     function FromServer(arg, context) {

         switch (context) {
            case 1:
              
                ComboFill(arg, "<%= cmbDate.ClientID %>");
                break;
          
            case 2:
              ComboFill(arg, "<%= cmdBranch.ClientID %>");
                break;
            case 3:
                var Data = arg.split("Ø");
                if(Data[1] == 1){
                    document.getElementById("dateTr").style.display = '';
                    ComboFill(Data[0], "<%= cmdMeetDate.ClientID %>");
                }
                break;
            case 4 :           
                var Data = arg.split("Ø");     
                document.getElementById("<%= hidAttach.ClientID %>").value=Data[0];
                document.getElementById("<%= hidMeetingID.ClientID %>").value=Data[1];    
                var attach = document.getElementById("<%= hidAttach.ClientID %>").value;
                if(attach == 1){
                    document.getElementById("<%= btnAttachView.ClientID %>").style.display = '';
                }else{
                    document.getElementById("<%= btnAttachView.ClientID %>").style.display = "none";
                }

            }
        }

          
    function ViewAttachment() 
    {
        var MeetingID	= document.getElementById("<%= hidMeetingID.ClientID %>").value;
        if (MeetingID > 0)
            window.open("../ShowFormat.aspx?MeetingID=" + btoa(MeetingID) + "");
        return false;
    }
    </script>
</asp:Content>

