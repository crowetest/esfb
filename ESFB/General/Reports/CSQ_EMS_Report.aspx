﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="CSQ_EMS_Report.aspx.vb" Inherits="CSQ_EMS_Report"  %>

<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%;">
                Quarter</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbQuarter" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black" AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">---SELECT---</asp:ListItem>              
                </asp:DropDownList></td>          
        </tr>
    <tr>
            <td style="width:15%;">
                Branch</td>
           <td  style="width:85%;">
                 <asp:DropDownList ID="cmdBranch" class="NormalText" runat="server" Font-Names="Cambria" AutoPostBack="true"
         Width="90%" ForeColor="Black" AppendDataBoundItems="True">
                </asp:DropDownList>
                </td>           
        </tr> 
        <tr><td><br /></td></tr>
        <tr>
        <br />
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">              
              <asp:Button ID="btnView" runat="server" Text="Download Attachments" Font-Names="Cambria" 
                    style="margin-bottom: 0px" Width="25%" />            
              <asp:Button ID="btnExcel" runat="server" Text="View Report" Font-Names="Cambria" 
                    style="margin-bottom: 0px" Width="20%" />
                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 15%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>      
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">     
        function btnExit_onclick(){
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
        }   
        function btnView_onclick(){    
            var BranchID    = document.getElementById("<%= cmdBranch.ClientID %>").value;  
            var QuarterID = document.getElementById("<%= cmbQuarter.ClientID %>").value;  
            if (QuarterID == -1) {
                alert('Select Quarter');
                return false;
            }
            var ToData = "2Ø" + BranchID + "Ø" + QuarterID;
            ToServer(ToData,2);              
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }       
        function FromServer(arg, context) {
         switch (context) {
            case 1:              
                ComboFill(arg, "<%= cmbQuarter.ClientID %>");
                break;          
            case 2:
                ComboFill(arg, "<%= cmdBranch.ClientID %>");
                break;   
             }
        }       
    </script>
</asp:Content>

