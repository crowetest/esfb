﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class BranchMeetingReport
    Inherits System.Web.UI.Page

    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim Meeting_Dt As Date
    Dim UserID As Integer
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim DB As New MS_SQL.Connect
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools


#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try

            Me.Master.subtitle = "Customer Meeting  Report"
            UserID = CInt(Session("UserID"))
            BranchID = CInt(Session("BranchID"))
            DT = GMASTER.GetEmpRoleList(UserID)
            For Each DR In DT.Rows
                If DR(2) = 26 Then
                    AdminFlag = True
                    Exit For
                End If
            Next

            If Not IsPostBack Then

                If AdminFlag = False Then
                    strWhere = " And branch_id = " & BranchID & ""
                    DT = DB.ExecuteDataSet("select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id = 1  " & strWhere & " and Branch_ID =" & BranchID & " order by 2").Tables(0)

                Else
                    DT = DB.ExecuteDataSet("select '-11' as branchid,' ------ALL------' as branch Union All select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id = 1  and -1 < Branch_ID and Branch_ID < 50000  order by 2").Tables(0)

                End If
                GF.ComboFill(cmdBranch, DT, 0, 1)

                If AdminFlag = False Then
                    DT = DB.ExecuteDataSet("SELECT '-1' AS MeetingDt,'AS ON DATE' AS Meeting_Dt1 UNION ALL Select Distinct Convert(varchar, DateAdd(dd, -Day(Meeting_Dt) + 1, Meeting_Dt))  as MeetingDt, Convert(char(3), Meeting_Dt, 0)+ ' ' + CONVERT(varchar, YEAR(Meeting_Dt))  as Meeting_Dt1 from MEETING_MASTER where Branch_ID =" & BranchID & " ").Tables(0)

                Else

                    DT = DB.ExecuteDataSet("SELECT '-1' AS MeetingDt,'AS ON DATE' AS Meeting_Dt1 UNION ALL Select Distinct Convert(varchar, DateAdd(dd, -Day(Meeting_Dt) + 1, Meeting_Dt))  as MeetingDt, Convert(char(3), Meeting_Dt, 0)+ ' ' + CONVERT(varchar, YEAR(Meeting_Dt))  as Meeting_Dt1 from MEETING_MASTER").Tables(0)
                End If
                GF.ComboFill(cmbDate, DT, 0, 1)

            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.cmdBranch.Attributes.Add("onchange", "return BranchOnChange()")
            Me.cmbDate.Attributes.Add("onchange", "return DateOnChange()")
            Me.cmdMeetDate.Attributes.Add("onchange", "return MeetingOnChange()")
            Me.btnAttachView.Attributes.Add("onclick", "return ViewAttachment()")
            Me.btnView.Attributes.Add("onclick", "return btnView_onclick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub

#End Region
#Region "Events"
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click

        Try
            Dim SqlStr As String = ""
           
            Dim Data As String = CStr(Me.cmbDate.SelectedValue)
            Dim DiscussionStatus As Integer = CInt(CmbStatus.SelectedValue)
            Dim Branch As Integer = CInt(Me.cmdBranch.SelectedValue)
            Dim MeetingID As String = Me.cmdMeetDate.SelectedValue
            Dim StrWhere As String = " 1 = 1 "
            If Data <> "-1" Then 'select  DATE
                Dim FDate, ToDate As Date
                Dim FromDt, ToDt As String
                Data = CDate(Data).ToString("dd MMM yyyy")
                FDate = Data
                ToDate = DateAdd(DateInterval.Month, 1, FDate)
                FromDt = "CONVERT(date, '" & FDate & "',103)"
                ToDt = "CONVERT(date, '" & ToDate & "',103)"

                StrWhere += " and DATEADD(day,DATEDIFF(day, 0, mm.MEETING_DT ),0)>=" & FromDt & " and DATEADD(day,DATEDIFF(day, 0, mm.MEETING_DT ),0)<=" & ToDt

            End If
            If Branch <> -11 Then
                StrWhere += " AND mm.Branch_ID = " & Branch
            End If
            If DiscussionStatus <> -1 Then
                StrWhere += " AND MD.StatusValue = " & DiscussionStatus
            End If
            If MeetingID <> "-1" Then
                StrWhere += " AND mm.Meeting_Id = " & MeetingID
            End If

            SqlStr = " SELECT ROW_NUMBER() over( order by MM.Meeting_ID) as SL_No ,BM.Branch_Name AS [Branch Name], BM.Branch_ID as [Branch ID], AM.Area_Name as [Area Name] ,convert(varchar(11),mm.MEETING_DT,6) as [Meeting date],MAD.TOTAL_ATTENDEES as [Total Attendees], MAD.STAFF as [No.of Staff], MAD.CUSTOMER as [No.of Customers], MAD.SC AS [No.of Senior Citizen],MAD.women as [No.of Women]" +
            " ,MD.[Discussion Points],MD.[Follow Up Actions],MD.[Remarks],EM.EMP_NAME as [Submitted By],convert(varchar(11),MM.SUBMITTED_ON,6) as [Submitted date] FROM MEETING_MASTER MM left JOIN EMP_MASTER EM ON EM.EMP_CODE = MM.SUBMITTED_BY INNER JOIN BRANCH_MASTER BM ON BM.Branch_ID = MM.Branch_ID" +
            " LEFT JOIN AREA_MASTER AM ON AM.Area_ID = BM.Area_ID LEFT JOIN (  select MAD.Meeting_ID,isnull(A.SC,0)  as SC ,isnull(B.women,0) as women , isnull(C.STAFF,0) as STAFF,  isnull(D.CUSTOMER,0) as CUSTOMER ,  isnull(C.STAFF,0)+  isnull(D.CUSTOMER,0) AS TOTAL_ATTENDEES   from MEETING_ATTENDEE_DTL MAD " +
            " LEFT JOIN (select Meeting_ID, COUNT(*) as SC from MEETING_ATTENDEE_DTL where Senior_citizen=1 group by Meeting_ID) A ON MAD.Meeting_ID = A.Meeting_ID" +
            " LEFT JOIN (select Meeting_ID, COUNT(*) as women from MEETING_ATTENDEE_DTL where Women=1 group by Meeting_ID) B ON MAD.Meeting_ID = B.Meeting_ID  " +
            " LEFT JOIN (select Meeting_ID, COUNT(*) as STAFF from MEETING_ATTENDEE_DTL where Category=1 group by Meeting_ID) C ON MAD.Meeting_ID = C.Meeting_ID " +
            " LEFT JOIN (select Meeting_ID, COUNT(*) as CUSTOMER from MEETING_ATTENDEE_DTL where Category=2 group by Meeting_ID) D ON MAD.Meeting_ID = D.Meeting_ID  " +
            " GROUP BY MAD.Meeting_ID,A.SC,B.women,C.STAFF,D.CUSTOMER ) MAD ON MM.Meeting_ID= MAD.Meeting_ID" +
            " LEFT JOIN (SELECT MEETING_ID, STUFF(" +
            " (select ',' + CONVERT(VARCHAR(2),ROW_NUMBER() OVER ( ORDER BY MEETING_ID))+'. ' + MD.Discussion +CASE WHEN StatusValue = 1 THEN '(OPEN)' ELSE '(CLOSE)' END  from MEETING_DISCUSSION_DTL MD where MD.meeting_id = MM.meeting_id For XML PATH ('')" +
            " ), 1, 1, '') AS [Discussion Points],STUFF(" +
            " ( select ',' + CONVERT(VARCHAR(2),ROW_NUMBER() OVER ( ORDER BY MEETING_ID))+'. ' + MD.Follow_up_action  from MEETING_DISCUSSION_DTL MD where MD.meeting_id = MM.meeting_id For XML PATH ('')" +
            " ), 1, 1, '') AS [Follow Up Actions]," +
            "STUFF( ( select ',' + CONVERT(VARCHAR(2),ROW_NUMBER() OVER ( ORDER BY MEETING_ID))+'. ' + MD.Remarks " +
            " from MEETING_DISCUSSION_DTL MD where MD.meeting_id = MM.meeting_id For XML PATH ('') ), 1, 1, '') AS [Remarks]" +
            " FROM MEETING_DISCUSSION_DTL MM " +
            " GROUP BY MEETING_ID) md ON MM.Meeting_ID=md.Meeting_ID WHERE " & StrWhere & " GROUP BY MM.Meeting_ID,BM.BRANCH_ID,BM.Branch_Name,AM.Area_Name,MM.Meeting_Dt,MAD.TOTAL_ATTENDEES,MAD.STAFF,MAD.CUSTOMER,MAD.SC,MAD.women,MD.[Discussion Points],MD.[Follow Up Actions],MD.[Remarks],EM.EMP_NAME,MM.SUBMITTED_ON"


            DT = DB.ExecuteDataSet(SqlStr).Tables(0)

            WebTools.ExporttoExcel(DT, "Branch Meeting Report")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
  
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            DT = DB.ExecuteDataSet("SELECT '-1' AS MeetingDt,'AS ON DATE' AS Meeting_Dt1 UNION ALL Select Distinct Convert(varchar, DateAdd(dd, -Day(Meeting_Dt) + 1, Meeting_Dt))  as MeetingDt, Convert(char(3), Meeting_Dt, 0)+ ' ' + CONVERT(varchar, YEAR(Meeting_Dt))  as Meeting_Dt1 from MEETING_MASTER where Branch_ID =" + Data(1).ToString()).Tables(0)

            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 2 Then
        ElseIf CInt(Data(0)) = 3 Then
            Dim SqlStr As String = ""
            Dim Dates As String = Data(2).ToString()
            Dim Branch As Integer = CInt(Data(1).ToString())
            Dim StrWhere As String = " 1 = 1 "
            If Dates <> "-1" Then 'select  DATE
                Dim FDate, ToDate As Date
                Dim FromDt, ToDt As String
                Dates = CDate(Dates).ToString("dd MMM yyyy")
                FDate = Dates
                ToDate = DateAdd(DateInterval.Month, 1, FDate)
                FromDt = "CONVERT(date, '" & FDate & "')"
                ToDt = "CONVERT(date, '" & ToDate & "')"

                StrWhere += " and CONVERT(DATE,DATEADD(day,DATEDIFF(day, 0, MEETING_DT ),0))>=" & FromDt & " and CONVERT(DATE,DATEADD(day,DATEDIFF(day, 0, MEETING_DT ),0))<=" & ToDt

            End If
            If Branch <> -11 Then
                StrWhere += " AND Branch_ID = " & Branch
            End If
            SqlStr = "select -1 as Meeting_ID,'---ALL---' as Meeting_Dt union all select Meeting_ID,convert(varchar(10),meeting_Dt,103) from meeting_master where " + StrWhere
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            If DT.Rows.Count > 0 Then
                hidMeeting.Value = CStr(1)
            End If
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            CallBackReturn += "Ø" + hidMeeting.Value
        ElseIf CInt(Data(0)) = 4 Then
            Dim Sql As String = ""
            Dim meetingID As String = Data(1).ToString()
            Sql = "select count(*) from DMS_ESFB.dbo.MEETING_ATTACH where meeting_id = " + meetingID
            DT = GF.GetQueryResult(Sql)
            For Each DR In DT.Rows
                CallBackReturn += DR(0).ToString()
            Next
            CallBackReturn += "Ø" + meetingID
        End If
    End Sub
#End Region

    'Protected Sub cmdBranch_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmdBranch.SelectedIndexChanged
    '    Dim StrWheres As String = " where 1 = 1 "
    '    If AdminFlag = False Then
    '        DT = DB.ExecuteDataSet("SELECT '-1' AS MeetingDt,'AS ON DATE' AS Meeting_Dt1 UNION ALL Select Distinct Convert(varchar, DateAdd(dd, -Day(Meeting_Dt) + 1, Meeting_Dt))  as MeetingDt, Convert(char(3), Meeting_Dt, 0)+ ' ' + CONVERT(varchar, YEAR(Meeting_Dt))  as Meeting_Dt1 from MEETING_MASTER where Branch_ID =" & BranchID & " ").Tables(0)

    '    Else
    '        If cmdBranch.SelectedValue <> -11 Then
    '            BranchID = cmdBranch.SelectedValue
    '            StrWheres += " and branch_id = " & BranchID & ""
    '        End If

    '        DT = DB.ExecuteDataSet("SELECT '-1' AS MeetingDt,'AS ON DATE' AS Meeting_Dt1 UNION ALL Select Distinct Convert(varchar, DateAdd(dd, -Day(Meeting_Dt) + 1, Meeting_Dt))  as MeetingDt, Convert(char(3), Meeting_Dt, 0)+ ' ' + CONVERT(varchar, YEAR(Meeting_Dt))  as Meeting_Dt1 from MEETING_MASTER " & StrWheres & "").Tables(0)
    '    End If
    '    GF.ComboFill(cmbDate, DT, 0, 1)
    'End Sub

End Class

