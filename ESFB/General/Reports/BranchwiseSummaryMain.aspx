﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="BranchwiseSummaryMain.aspx.vb" Inherits="BranchwiseSummaryMain"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <script src="../../Script/Validations.js" type="text/javascript"></script>

     <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
    </asp:ToolkitScriptManager>
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
    <tr>
            <td style="width:15%;">
                Branch</td>
           <td  style="width:85%;">
                 <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black" AppendDataBoundItems="True">
                </asp:DropDownList>
                </td>
           
        </tr>
 
        <tr>

           <td style="width:15%;">
                Year</td>
                        <td  style="width:85%;">
                            <asp:TextBox ID="txtYear" runat="server" 
                    Width="90%" class="NormalText" MaxLength="4"  onkeypress='return NumericCheck(event)'></asp:TextBox>
                        </td>
     
        </tr>
         <tr>
            <td style="width:30%;">
                Month </td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbMonth" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black" AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">---ALL---</asp:ListItem>
                    <asp:ListItem Value="1">January</asp:ListItem>
                    <asp:ListItem Value="2">February</asp:ListItem>
                    <asp:ListItem Value="3">March</asp:ListItem>
                    <asp:ListItem Value="4">April</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">June</asp:ListItem>
                    <asp:ListItem Value="7">July</asp:ListItem>
                    <asp:ListItem Value="8">August</asp:ListItem>
                    <asp:ListItem Value="9">September</asp:ListItem>
                    <asp:ListItem Value="10">October</asp:ListItem>
                    <asp:ListItem Value="11">November</asp:ListItem>
                    <asp:ListItem Value="12">December</asp:ListItem>
                </asp:DropDownList></td>
           
        </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
              
             <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;

                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 10%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
          
      
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
     
        function btnExit_onclick() 
    {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }
     function btnView_onclick() 
        {
       
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
            var Year = document.getElementById("<%= txtYear.ClientID %>").value;
            var MonthID = document.getElementById("<%= cmbMonth.ClientID %>").value;
            window.open("ViewBranchwiseSummaryMain.aspx?BranchID=" + BranchID + "&MonthID=" + MonthID + "&Year=" +Year+ "", "_self");        
        }        
   

       
    </script>
  

    
  
</asp:Content>

