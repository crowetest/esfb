﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class View_CSQ_Detail_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTEXCEL As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim ViewType As Integer
            ViewType = CInt(Request.QueryString.Get("ViewType"))
            Dim Freq = Request.QueryString.Get("Quarter_ID")
            Dim strwhere As String = " where b.freq_id = " + Freq
            Dim branchID = Request.QueryString.Get("Branch_Code")
            If branchID <> -1 Then
                strwhere += " and b.branch_id = " + branchID
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RH.Heading(Session("FirmName"), tb, "Quarterly Returns On Customer Service", 100)
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid

            TRHead_00.Font.Bold = True
            TRHead_01.Font.Bold = True
            TRHead_02.Font.Bold = True
            TRHead_03.Font.Bold = True
            TRHead_04.Font.Bold = True
            TRHead_05.Font.Bold = True
            TRHead_06.Font.Bold = True
            TRHead_07.Font.Bold = True
            TRHead_08.Font.Bold = True
            TRHead_09.Font.Bold = True
            TRHead_10.Font.Bold = True
            TRHead_11.Font.Bold = True
            TRHead_12.Font.Bold = True

            RH.AddColumn(TRHead, TRHead_00, 4, 4, "c", "Branch Code")
            RH.AddColumn(TRHead, TRHead_01, 8, 8, "c", "Branch Name")
            RH.AddColumn(TRHead, TRHead_02, 8, 8, "c", "Number of complaints pending in the beginning of quarter")
            RH.AddColumn(TRHead, TRHead_03, 8, 8, "c", "Number of complaints received during the quarter")
            RH.AddColumn(TRHead, TRHead_04, 8, 8, "c", "Number of complaints Resolved")
            RH.AddColumn(TRHead, TRHead_05, 8, 8, "c", "Number of Complaints Pending at the end of the quarter")
            RH.AddColumn(TRHead, TRHead_06, 8, 8, "c", "Number of complaints received vide Complaint Register")
            RH.AddColumn(TRHead, TRHead_07, 8, 8, "c", "Number of Complaints received vide Complaint Box")
            RH.AddColumn(TRHead, TRHead_08, 8, 8, "c", "Whether the Customer Service Committee Register is updated for last three months (Quarter)")
            RH.AddColumn(TRHead, TRHead_09, 8, 8, "c", "Whether all the mandatory Notices have been displayed in the Notice Board ")
            RH.AddColumn(TRHead, TRHead_10, 8, 8, "c", "Whether the Branch was opened on time every day and ready for Customer service at 9 AM")
            RH.AddColumn(TRHead, TRHead_11, 8, 8, "c", "Whether all inward letters and emails are properly recorded and responded ")
            RH.AddColumn(TRHead, TRHead_12, 8, 8, "c", "Whether all the circulars issued from the Head Office are properly discussed with the team and implemented properly")

            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer


            DT = DB.ExecuteDataSet("DECLARE @cols AS NVARCHAR(MAX), @query  AS NVARCHAR(MAX) select @cols = STUFF((SELECT  ',' + QUOTENAME(QUESTION_ID) from CSQ_ENTRY_DTL group by question_id order by question_id FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)'),1,1,'') set @query = 'SELECT branch_id,branch_name,'+@cols+' from ( select b.branch_id,c.branch_name,a.QUESTION_ID,case when a.COMPLAINTS_COUNT is null then a.CONFIRM_STATUS else a.COMPLAINTS_COUNT end as COMPLAINTS_COUNT from CSQ_ENTRY_DTL a inner join csq_entry b on a.entry_id = b.entry_id inner join branch_master c on c.branch_id = b.branch_id" + strwhere + ") x pivot (max(COMPLAINTS_COUNT) for QUESTION_ID in ('+@cols+')) p ' execute(@query)").Tables(0)


            DTEXCEL = DB.ExecuteDataSet("DECLARE @cols AS NVARCHAR(MAX), @query  AS NVARCHAR(MAX) select @cols = STUFF((SELECT  ',' + QUOTENAME(QUESTION_ID) from CSQ_ENTRY_DTL group by question_id order by question_id FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)'),1,1,'') set @query = 'select branch_id as [Branch Code],branch_name as [Branch Name], [1] as [Number of complaints pending in the beginning of quarter],[2] as [Number of complaints received during the quarter],[3] as [Number of complaints Resolved],[4] as [Number of Complaints Pending at the end of the quarter],[5] as [Number of complaints received vide Complaint Register],[6] as [Number of Complaints received vide Complaint Box],case when [7] = 1 then ''YES'' when [7] = 2 then ''NO'' else '''' end as [Whether the Customer Service Committee Register is updated for last three months (Quarter)],case when [8] = 1 then ''YES'' when [8] = 2 then ''NO'' else '''' end as [Whether all the mandatory Notices have been displayed in the Notice Board ],case when [9] = 1 then ''YES'' when [9] = 2 then ''NO'' else '''' end as [Whether the Branch was opened on time every day and ready for Customer service at 9 AM],case when [10] = 1 then ''YES'' when [10] = 2 then ''NO'' else '''' end as [Whether all inward letters and emails are properly recorded and responded ],case when [11] = 1 then ''YES'' when [11] = 2 then ''NO'' else '''' end as [Whether all the circulars issued from the Head Office are properly discussed with the team and implemented properly] from (SELECT branch_id,branch_name,'+@cols+' from ( select b.branch_id,c.branch_name,a.QUESTION_ID,case when a.COMPLAINTS_COUNT is null then a.CONFIRM_STATUS else a.COMPLAINTS_COUNT end as COMPLAINTS_COUNT from CSQ_ENTRY_DTL a inner join csq_entry b on a.entry_id = b.entry_id inner join branch_master c on c.branch_id = b.branch_id where b.freq_id = " + Freq + ") x pivot (max(COMPLAINTS_COUNT) for QUESTION_ID in ('+@cols+')) p )a' execute(@query)").Tables(0)

            If ViewType = 1 Then
                Export_Excel_Click()
            End If

            Dim createddate As String = ""

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid


                RH.AddColumn(TR3, TR3_00, 4, 4, "c", DR(0).ToString())
                RH.AddColumn(TR3, TR3_01, 8, 8, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_02, 8, 8, "c", DR(2).ToString())
                RH.AddColumn(TR3, TR3_03, 8, 8, "c", DR(3).ToString())
                RH.AddColumn(TR3, TR3_04, 8, 8, "c", DR(4).ToString())
                RH.AddColumn(TR3, TR3_05, 8, 8, "c", DR(5).ToString())
                RH.AddColumn(TR3, TR3_06, 8, 8, "c", DR(6).ToString())
                RH.AddColumn(TR3, TR3_07, 8, 8, "c", DR(7).ToString())
                If DR(8).ToString() = 1 Then
                    RH.AddColumn(TR3, TR3_08, 8, 8, "c", "YES")
                ElseIf DR(8).ToString() = 2 Then
                    RH.AddColumn(TR3, TR3_08, 8, 8, "c", "NO")
                Else
                    RH.AddColumn(TR3, TR3_08, 8, 8, "c", "")
                End If
                If DR(9).ToString() = 1 Then
                    RH.AddColumn(TR3, TR3_09, 8, 8, "c", "YES")
                ElseIf DR(9).ToString() = 2 Then
                    RH.AddColumn(TR3, TR3_09, 8, 8, "c", "NO")
                Else
                    RH.AddColumn(TR3, TR3_09, 8, 8, "c", "")
                End If
                If DR(10).ToString() = 1 Then
                    RH.AddColumn(TR3, TR3_10, 8, 8, "c", "YES")
                ElseIf DR(10).ToString() = 2 Then
                    RH.AddColumn(TR3, TR3_10, 8, 8, "c", "NO")
                Else
                    RH.AddColumn(TR3, TR3_10, 8, 8, "c", "")
                End If
                If DR(11).ToString() = 1 Then
                    RH.AddColumn(TR3, TR3_11, 8, 8, "c", "YES")
                ElseIf DR(11).ToString() = 2 Then
                    RH.AddColumn(TR3, TR3_11, 8, 8, "c", "NO")
                Else
                    RH.AddColumn(TR3, TR3_11, 8, 8, "c", "")
                End If
                If DR(12).ToString() = 1 Then
                    RH.AddColumn(TR3, TR3_12, 8, 8, "c", "YES")
                ElseIf DR(12).ToString() = 2 Then
                    RH.AddColumn(TR3, TR3_12, 8, 8, "c", "NO")
                Else
                    RH.AddColumn(TR3, TR3_12, 8, 8, "c", "")
                End If
                

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=CSQReport.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Quarterly Returns On Customer Service"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Protected Sub Export_Excel_Click()
        Try
            Dim HeaderText As String
            HeaderText = "Quarterly Returns On Customer Service"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
