﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ExcelExport.aspx.vb" Inherits="Operations_ExcelExport" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
<div>
    <table style="width: 50%; margin:0px auto; text-align:center;">
        <tr>
            <td style="width: 40%"
                &nbsp;</td>
            <td  style="width: 60%">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            </td>
        </tr>
        <tr>
            <td  style="width: 40%;text-align:right ;">
                Select&nbsp;&nbsp;</td>
            <td  style="width: 60%">
                <asp:DropDownList ID="cmbName" runat="server" Height="25px" Width="60%" class="NormalText">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="rowFrom" style="display:none;">
            <td id="colFrom" style="width: 40%;;text-align:right ;">
                From&nbsp;&nbsp;</td>
            <td  style="width: 60%">
                <asp:TextBox ID="txtStartDt" class="NormalText" runat="server" 
                    Width="30%" onkeypress="return false" ReadOnly="True" ></asp:TextBox>
             <asp:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtStartDt" Format="dd MMM yyyy"></asp:CalendarExtender></td>
        </tr>
        <tr id="rowTo"  style="display:none;">
            <td  style="width: 40%;;text-align:right ;">
                To&nbsp;&nbsp;</td>
            <td  style="width: 60%">
                <asp:TextBox ID="txtToDt" class="NormalText" runat="server" 
                    Width="30%" onkeypress="return false" ReadOnly="True" ></asp:TextBox>
             <asp:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtToDt" Format="dd MMM yyyy"></asp:CalendarExtender></td>
        </tr>
        <tr>
            <td  style="width: 40%">
                &nbsp;</td>
            <td  style="width: 60%">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center;">
                <asp:HiddenField ID="hdnFromDt" runat="server" />
                <asp:HiddenField ID="hdnToDt" runat="server" />
                <asp:Button ID="btnExport" runat="server" Text="EXPORT" Font-Names="Cambria" 
                    Font-Size="10pt" Width="10%" />
&nbsp;
                <input id="btn_Exit" type="button" value="EXIT" 
                    style="font-family: Cambria; font-size: 10pt; width: 10%" onclick="return btn_Exit_onclick()" /></td>
        </tr>
    </table>
    </div> 
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btn_Exit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function NameOnChange()
        {
          var Arr=document.getElementById("<%= cmbName.ClientID %>").value.split("^");
          var DateRange =Arr[2];         
          if (DateRange ==1)
          {
            document.getElementById("colFrom").innerHTML="From&nbsp;&nbsp;"
            document.getElementById("rowFrom").style.display="";
            document.getElementById("rowTo").style.display="";         
          }
          else if (DateRange ==2)
          {
            document.getElementById("colFrom").innerHTML="As On&nbsp;&nbsp;"
            document.getElementById("rowFrom").style.display="";
            document.getElementById("rowTo").style.display="none";   
          }
          else
          {
            document.getElementById("rowFrom").style.display="none";
            document.getElementById("rowTo").style.display="none";   
          }
        }
        function ExportOnClick()
        {
          var Val=document.getElementById("<%= cmbName.ClientID %>").value;
          if(Val=="-1")
          {
            alert("Select report");document.getElementById("<%= cmbName.ClientID %>").focus();return false;
          }
          var Arr=Val.split("^");
          var DateRange =Arr[2];         
          if (DateRange ==1)
          {
                if(document.getElementById("<%= txtStartDt.ClientID %>").value=="")
                {
                    alert("Select From Date");return false;
                }
                if(document.getElementById("<%= txtToDt.ClientID %>").value=="")
                {
                    alert("Select To Date");return false;
                }
                document.getElementById("<%= hdnFromDt.ClientID %>").value=document.getElementById("<%= txtStartDt.ClientID %>").value;
                document.getElementById("<%= hdnToDt.ClientID %>").value=document.getElementById("<%= txtToDt.ClientID %>").value;
          }
          else if (DateRange ==2)
          {
           if(document.getElementById("<%= txtStartDt.ClientID %>").value=="")
                {
                    alert("Select  Date");return false;
                }
            document.getElementById("<%= hdnFromDt.ClientID %>").value=document.getElementById("<%= txtStartDt.ClientID %>").value;
          }
        
        }

// ]]>
    </script>
</asp:Content>

