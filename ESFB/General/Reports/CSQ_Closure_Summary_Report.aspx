﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="CSQ_Closure_Summary_Report.aspx.vb" Inherits="CSQ_Closure_Summary_Report"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

<br />
<table align="center" style="width: 40%; text-align:center; margin:0px auto;">
    <tr>
        <td style="width:15%;">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
        </td>
        <td  style="width:85%;">
        </td>
    </tr>      
    <tr>           
        <td style="width:15%; text-align:left;">
            Month
        </td>
        <td style="width:85%">
            <asp:DropDownList ID="cmbQuarter" class="NormalText" runat="server" Font-Names="Cambria" Width="86%" ForeColor="Black">
                
            </asp:DropDownList>
        </td> 
    </tr>    
    <tr>
        <td style="width:15%; text-align:left;"></td>
        <td style="width:85%">
            <br />           
            <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 20%" type="button" value="VIEW"   onclick="return btnView_onclick()" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input id="cmd_Export_Excel" style="font-family: Cambria; cursor: pointer; width: 22%;" type="button" value="VIEW EXCEL" onclick="return btnExcelView_onclick()" />
            &nbsp;&nbsp;
            <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 20%" type="button" value="EXIT"  onclick="return btnExit_onclick()" />
        </td>
        <td style="width:100%; height: 18px; text-align:center;"></td>
    </tr>       
    <tr>
        <td style="width:15%;">&nbsp;</td>
        <td  style="width:85%;">
            <asp:HiddenField ID="hdnReportID" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="width:15%;">&nbsp;</td>
        <td  style="width:85%;">               
            <asp:HiddenField ID="hdnValue" runat="server" />
        </td>               
    </tr>
</table>
<script language="javascript" type="text/javascript">
    function FromServer(arg, context) {
                
    }
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 1; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function btnView_onclick() 
    {
        var Quarter_ID = document.getElementById("<%= cmbQuarter.ClientID %>").value;
        if(Quarter_ID == -1)
        {
            alert("Select Month");
        }
        else
        {
            window.open("View_CSQ_Closure_Summary_Report.aspx?ViewType=-1 &Quarter_ID=" + Quarter_ID +" ", "_self");  
        }
    }   
    function btnExcelView_onclick() 
    {
        var Quarter_ID = document.getElementById("<%= cmbQuarter.ClientID %>").value;
        if(Quarter_ID == -1)
        {
            alert("Select Quarter");
        }
        else
        {
            window.open("View_CSQ_Closure_Summary_Report.aspx?ViewType=1 &Quarter_ID=" + Quarter_ID +" ", "_self");  
        }
    }    
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }  
</script>
</asp:Content>

