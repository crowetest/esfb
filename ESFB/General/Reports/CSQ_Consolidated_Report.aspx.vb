﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class CSQ_Consolidated_Report
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim TraDt As Date
    Dim DB As New MS_SQL.Connect
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1383) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Submission Report"
            If Not IsPostBack Then
                DT = GF.GetQueryResult("SELECT -1,'--SELECT--' Quarter UNION ALL SELECT ID,QUARTER_NAME FROM CSQ_FREQ_SETTINGS ORDER BY 2")
                GF.ComboFill(cmbQuarter, DT, 0, 1)
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.cmbReport.Attributes.Add("onchange", "return ReportOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub
#End Region
#Region "Events"
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim Report As Integer = CInt(Data(1))
            Dim QS1 As String = ""
            If Report = 1 Then
                DT = GF.GetQueryResult("SELECT -1,'--SELECT--' Quarter UNION ALL SELECT ID,QUARTER_NAME FROM CSQ_FREQ_SETTINGS ORDER BY 2")
                For n As Integer = 0 To DT.Rows.Count - 1
                    QS1 += DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString() + "Ñ"
                Next
            Else
                DT = GF.GetQueryResult("SELECT -1,'--SELECT--' MONTH UNION ALL SELECT ID,MONTH FROM CSQ_ACCOUNT_CLOSURE_FREQ_SETTINGS ORDER BY 1")
                For n As Integer = 0 To DT.Rows.Count - 1
                    QS1 += DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString() + "Ñ"
                Next
            End If
            CallBackReturn = QS1
        End If
    End Sub
#End Region
End Class

