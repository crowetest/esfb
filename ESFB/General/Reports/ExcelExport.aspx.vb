﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Operations_ExcelExport
    Inherits System.Web.UI.Page
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim Form_id As Integer
    Dim WebTools As New WebApp.Tools
    Dim AuditTypeID As Integer
    Dim ModuleID As Integer = 0
    Dim RptID As Integer = 1
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            Dim HeaderText As String
            Dim Data() As String = cmbName.SelectedValue.Split("^")
            Dim FormID As Integer = Data(0)
            Dim DateRange As Integer = Data(2)
            Dim Query As String = Data(1)
            If DateRange = 1 Then
                Dim sqlStr = "DECLARE @FromDT datetime; DECLARE @ToDT datetime; SET @FromDT='" + CDate(hdnFromDt.Value).ToString("MM/dd/yyyy") + "' SET @ToDT='" + CDate(hdnToDt.Value).ToString("MM/dd/yyyy") + "' " + Query
                DT = GF.GetQueryResult(sqlStr)
            ElseIf DateRange = 2 Then
                Dim sqlStr = "DECLARE @FromDT datetime; SET @FromDT='" + CDate(hdnFromDt.Value).ToString("MM/dd/yyyy") + "' " + Query
                DT = GF.GetQueryResult(sqlStr)
            Else
                DT = GF.GetQueryResult(Query)
            End If
            HeaderText = cmbName.SelectedItem.Text
            WebTools.ExporttoExcel(DT, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub ExcelExport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        try
            ModuleID = CInt(Request.QueryString.Get("ModuleID"))
            Me.cmbName.Attributes.Add("onchange", "NameOnChange()")
            Me.btnExport.Attributes.Add("onclick", "return ExportOnClick()")
            Me.txtStartDt_CalendarExtender.SelectedDate = CDate(Session("TraDt")).AddDays((CDate(Session("TraDt")).Day - 1) * -1)
            txtToDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            If Not IsNothing(Request.QueryString.Get("RptID")) Then
                RptID = CInt(Request.QueryString.Get("RptID"))
            End If
            If Not IsPostBack Then
                If RptID = 1 Then
                    DT = GF.GetQueryResult("select '-1' as val,'------Select------' as Descr union all select convert(varchar(10),Form_id ) + '^' + query+'^'+cast(DateRange as varchar(2)),Name from tbl_export where status=1 and module_id=" + ModuleID.ToString())
                    GF.ComboFill(cmbName, DT, 0, 1)
                Else
                    DT = GF.GetQueryResult("select '-1' as val,'------Select------' as Descr union all select convert(varchar(10),Form_id ) + '^' + query+'^'+cast(DateRange as varchar(2)),Name from tbl_export where status=1 and module_id=" + ModuleID.ToString() + " and form_id in(select form_id from tbl_export_dtl where emp_code=" + Session("UserID") + ")")
                    GF.ComboFill(cmbName, DT, 0, 1)
                End If
             
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
