﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class View_CSQ_Consolidated_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTEXCEL As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim ViewType As Integer
            ViewType = CInt(Request.QueryString.Get("ViewType"))
            Dim Freq = Request.QueryString.Get("Quarter_ID")
            Dim Report = Request.QueryString.Get("Report")
            Me.hdnReportID.Value = Report
            Dim strwhere As String = ""
            If Report = 1 Then
                strwhere = " where a.freq_id = " + Freq
            ElseIf Report = 2 Then
                strwhere = " where a.freqid = " + Freq
            End If

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            If Report = 1 Then
                RH.Heading(Session("FirmName"), tb, "Quarterly Return Submission Report", 100)
            ElseIf Report = 2 Then
                RH.Heading(Session("FirmName"), tb, "Account Closure Submission Report", 100)
            End If
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid

            TRHead_00.Font.Bold = True
            TRHead_01.Font.Bold = True
            TRHead_02.Font.Bold = True
            TRHead_03.Font.Bold = True

            RH.AddColumn(TRHead, TRHead_00, 10, 10, "c", "Sl.No")
            RH.AddColumn(TRHead, TRHead_01, 20, 20, "c", "Branch Code")
            RH.AddColumn(TRHead, TRHead_02, 35, 35, "c", "Branch Name")
            RH.AddColumn(TRHead, TRHead_03, 35, 35, "c", "Submitted On")

            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer

            If Report = 1 Then
                DT = DB.ExecuteDataSet("SELECT A.BRANCH_ID, B.BRANCH_NAME, CONVERT(VARCHAR(11),FORMAT(A.CREATEDDATE ,'dd-MMM-yyyy')) FROM CSQ_ENTRY A INNER JOIN BRANCH_MASTER B ON A.BRANCH_ID = B.BRANCH_ID " + strwhere + " ORDER BY A.BRANCH_ID").Tables(0)
                DTEXCEL = DB.ExecuteDataSet("SELECT A.BRANCH_ID AS [Branch Code], B.BRANCH_NAME AS [Branch Name], CONVERT(VARCHAR(11),FORMAT(A.CREATEDDATE ,'dd-MMM-yyyy')) AS [Submitted On] FROM CSQ_ENTRY A INNER JOIN BRANCH_MASTER B ON A.BRANCH_ID = B.BRANCH_ID " + strwhere + " ORDER BY A.BRANCH_ID").Tables(0)
            ElseIf Report = 2 Then
                DT = DB.ExecuteDataSet("SELECT A.BRANCHID, B.BRANCH_NAME, CONVERT(VARCHAR(11),FORMAT(A.CREATEDDATE ,'dd-MMM-yyyy')) FROM CSQ_ACCOUNT_CLOSURE_ENTRY A INNER JOIN BRANCH_MASTER B ON A.BRANCHID = B.BRANCH_ID " + strwhere + " GROUP BY A.BRANCHID, B.BRANCH_NAME, A.CREATEDDATE ORDER BY A.BRANCHID").Tables(0)
                DTEXCEL = DB.ExecuteDataSet("SELECT A.BRANCHID AS [Branch Code], B.BRANCH_NAME AS [Branch Name], CONVERT(VARCHAR(11),FORMAT(A.CREATEDDATE ,'dd-MMM-yyyy')) AS [Submitted On] FROM CSQ_ACCOUNT_CLOSURE_ENTRY A INNER JOIN BRANCH_MASTER B ON A.BRANCHID = B.BRANCH_ID " + strwhere + " GROUP BY A.BRANCHID, B.BRANCH_NAME, A.CREATEDDATE ORDER BY A.BRANCHID").Tables(0)
            End If
            If ViewType = 1 Then
                Export_Excel_Click()
            End If

            Dim createddate As String = ""

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid


                RH.AddColumn(TR3, TR3_00, 10, 10, "c", i)
                RH.AddColumn(TR3, TR3_01, 20, 20, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 35, 35, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 35, 35, "l", DR(2).ToString())


                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=ACReasonsReport.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Account Closure Reasons Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Protected Sub Export_Excel_Click()
        Try
            Dim HeaderText As String
            Dim report = Me.hdnReportID.Value
            If report = 1 Then
                HeaderText = "Quarterly Return Submission Report"
            ElseIf report = 2 Then
                HeaderText = "Account Closure Submission Report"
            End If
            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
