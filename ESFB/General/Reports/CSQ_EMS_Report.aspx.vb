﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Imports Ionic.Zip

Partial Class CSQ_EMS_Report
    Inherits System.Web.UI.Page

    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT, DT1 As New DataTable
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim Meeting_Dt As Date
    Dim UserID As Integer
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim DB As New MS_SQL.Connect
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools


#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            Me.Master.subtitle = "Extra Mile Service Report"
            UserID = CInt(Session("UserID"))
            BranchID = CInt(Session("BranchID"))
            If Not IsPostBack Then
                If BranchID <> 0 Then
                    DT = DB.ExecuteDataSet("SELECT BRANCH_ID AS BRANCHID,UPPER(BRANCH_NAME) AS BRANCH FROM BRANCH_MASTER WHERE STATUS_ID = 1  " & strWhere & " and Branch_ID =" & BranchID & " order by 2").Tables(0)
                Else
                    DT = DB.ExecuteDataSet("SELECT '-11' AS BRANCHID,' ------ALL------' AS BRANCH UNION All SELECT BRANCH_ID AS BRANCHID,UPPER(BRANCH_NAME) AS BRANCH FROM BRANCH_MASTER WHERE STATUS_ID = 1  and -1 < BRANCH_ID and BRANCH_ID < 50000  order by 2").Tables(0)
                End If
                GF.ComboFill(cmdBranch, DT, 0, 1)
                DT = DB.ExecuteDataSet("SELECT '-1' AS QUARTER_ID,'-----SELECT-----'AS QUARTER_NAME UNION ALL SELECT QUARTER_ID,QUARTER_NAME FROM CSQ_EMS_SETTINGS ORDER BY QUARTER_ID").Tables(0)
                GF.ComboFill(cmbQuarter, DT, 0, 1)
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.btnView.Attributes.Add("onclick", "return btnView_onclick()")

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub

#End Region
#Region "Events"
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            Dim SqlStr As String = ""
            Dim Quarter As String = CStr(Me.cmbQuarter.SelectedValue)
            Dim Branch As Integer = CInt(Me.cmdBranch.SelectedValue)
            Dim StrWhere As String = " where 1 = 1 "
            If Branch <> -11 Then
                StrWhere += " AND a.BranchID = " & Branch.ToString()
            End If
            If Quarter <> -1 Then
                StrWhere += " and a.quarterid = " & Quarter
            End If
            SqlStr = "SELECT ROW_NUMBER() OVER( ORDER BY A.ENTRYID) as ENTRYID, B.QUARTER_NAME AS QUARTER, A.BRANCHID AS [BRANCH CODE], C.BRANCH_NAME AS [BRANCH NAME], A.CUSTOMERNAME AS [CUSTOMER NAME], A.CUSTOMERACNO AS [CUSTOMER ACCOUNT NO], A.CUSTOMERMOBILE AS [CUSTOMER MOBILE] FROM CSQ_EMS_ENTRY A INNER JOIN CSQ_EMS_SETTINGS B ON B.QUARTER_ID = A.QUARTERID INNER JOIN BRANCH_MASTER C ON C.BRANCH_ID = A.BRANCHID INNER JOIN CSQ_EMS_CHANNEL D ON D.CHANNEL_ID = A.CHANNELID " + StrWhere + " ORDER BY A.BRANCHID"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim zip As New ZipFile()
            Dim bytes As Byte() = Nothing
            Dim Files As String = "C:/Files"
            zip.AlternateEncodingUsage = ZipOption.AsNecessary
            If (DT.Rows.Count > 0) Then
                DT1 = DB.ExecuteDataSet("SELECT C.ATTACHMENT, C.CONTENT_TYPE, C.ATTACH_FILE_NAME, B.BRANCH_NAME, c.attach_id,C.SUBMITTEDON, B.BRANCH_ID FROM DMS_ESFB.DBO.CSQ_EMS C INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = C.BRANCH_ID INNER JOIN CSQ_EMS_ENTRY A ON A.ENTRYID = C.ATTACH_ID " + StrWhere).Tables(0)
                For m As Integer = 0 To DT1.Rows.Count - 1
                    bytes = CType(DT1.Rows(m)(0), Byte())
                    Dim dt As String = DT1.Rows(m)(6).ToString() + "_" + DT1.Rows(m)(5).ToString() + "_" + m.ToString() + "_" + DT1.Rows(m)(2).ToString()
                    zip.AddEntry(dt, bytes)
                Next
            End If

            Response.Clear()
            Response.BufferOutput = False
            Dim zipName As String = String.Format("ExtraMileService{0}.zip", "")
            Response.ContentType = "application/x-zip-compressed"
            Response.AddHeader("content-disposition", "attachment; filename=" + zipName)
            zip.Save(Response.OutputStream)



            HttpContext.Current.Response.Flush()
            HttpContext.Current.Response.SuppressContent = True
            HttpContext.Current.ApplicationInstance.CompleteRequest()

            'Response.[End]()
            'HttpContext.Current.Response.End()

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnViewExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        Try
            Dim SqlStr As String = ""
            Dim Quarter As String = CStr(Me.cmbQuarter.SelectedValue)
            Dim Branch As Integer = CInt(Me.cmdBranch.SelectedValue)
            Dim StrWhere As String = " where 1 = 1 "
            If Branch <> -11 Then
                StrWhere += " AND a.BranchID = " & Branch.ToString()
            End If
            If Quarter <> -1 Then
                StrWhere += " and a.quarterid = " & Quarter
            End If
            SqlStr = "SELECT B.QUARTER_NAME AS QUARTER, A.BRANCHID AS [BRANCH CODE], C.BRANCH_NAME AS [BRANCH NAME], D.CHANNEL AS [CHANNEL], A.CUSTOMERNAME AS [CUSTOMER NAME], A.CUSTOMERACNO AS [CUSTOMER ACCOUNT NO], A.CUSTOMERMOBILE AS [CUSTOMER MOBILE], A.REMARKS AS REMARKS , F.EMP_NAME AS [SUBMITTED BY], A.SUBMITTEDDATE AS [SUBMITTED ON] FROM CSQ_EMS_ENTRY A INNER JOIN CSQ_EMS_SETTINGS B ON B.QUARTER_ID = A.QUARTERID INNER JOIN BRANCH_MASTER C ON C.BRANCH_ID = A.BRANCHID INNER JOIN CSQ_EMS_CHANNEL D ON D.CHANNEL_ID = A.CHANNELID INNER JOIN EMP_MASTER F ON F.EMP_CODE = A.SUBMITTEDBY " + StrWhere + " ORDER BY A.BRANCHID"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            WebTools.ExporttoExcel(DT, "Extra_Mile_Service_Report")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            DT = DB.ExecuteDataSet("SELECT '-1' AS MeetingDt,'AS ON DATE' AS Meeting_Dt1 UNION ALL Select Distinct Convert(varchar, DateAdd(dd, -Day(Meeting_Dt) + 1, Meeting_Dt))  as MeetingDt, Convert(char(3), Meeting_Dt, 0)+ ' ' + CONVERT(varchar, YEAR(Meeting_Dt))  as Meeting_Dt1 from MEETING_MASTER where Branch_ID =" + Data(1).ToString()).Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next

        End If
    End Sub
#End Region

End Class

