﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CSQ_Account_Closure
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1369) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return

            End If

            Me.Master.subtitle = "CSQ Account Closure"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim EmpCode = CInt(Session("UserID"))
            Dim RSN As String = ""
            DT = GF.GetQueryResult("SELECT -1,'--SELECT--' REASON UNION ALL SELECT REASON_ID,REASON FROM CSQ_CLOSURE_REASONS ORDER BY 2")
            For n As Integer = 0 To DT.Rows.Count - 1
                RSN += DT.Rows(n)(0).ToString() + "ÿ" + DT.Rows(n)(1).ToString() + "Ñ"
            Next
            Me.hidReason.Value = RSN
            Dim UserID As String = CStr(Session("UserID"))
            DT2 = DB.ExecuteDataSet("select b.branch_id,b.branch_name from emp_master a inner join branch_master b on a.branch_id = b.branch_id where a.emp_code = '" + UserID + "'").Tables(0)


            DT3 = DB.ExecuteDataSet("SELECT MONTH,START_DATE,REPORT_FILING_PERIOD,ID,convert(date,DATEADD(day,DATEDIFF(day, 0,START_DATE ),10)) as END_DATE,case when getdate() between start_date and convert(date,DATEADD(day,DATEDIFF(day, 0,START_DATE ),report_filing_period)) then 1 else 0 end as date_status FROM [CSQ_ACCOUNT_CLOSURE_FREQ_SETTINGS] WHERE ID = (SELECT MAX(ID) FROM [CSQ_ACCOUNT_CLOSURE_FREQ_SETTINGS]) ").Tables(0)
            Me.hidQDetails.Value = DT3.Rows(0)(0).ToString() + "ÿ" + DT3.Rows(0)(1).ToString() + "ÿ" + DT3.Rows(0)(2).ToString() + "ÿ" + DT3.Rows(0)(3).ToString() + "ÿ" + DT3.Rows(0)(4).ToString() + "ÿ" + DT3.Rows(0)(5).ToString()
            DT4 = DB.ExecuteDataSet("select * from csq_account_closure_entry where branchid = " + DT2.Rows(0)(0).ToString() + " and freqid = " + DT3.Rows(0)(3).ToString()).Tables(0)
            If DT4.Rows.Count = 0 Then
                Me.hidDataHis.Value = CStr(1)
            Else
                Me.hidDataHis.Value = CStr(0)
            End If

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim CloseData As String = CStr(Data(1))
            Dim FreqId As Integer = CInt(Data(2).ToString())
            If CloseData = "¥" Then
                CloseData = ""
            End If
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@Created_By", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@CloseData", SqlDbType.VarChar, 5000)
                Params(1).Value = CloseData
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@FreqId", SqlDbType.Int)
                Params(4).Value = FreqId
                DB.ExecuteNonQuery("SP_CSQ_ACCOUNT_CLOSURE_SAVE", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then


        End If


    End Sub
#End Region
End Class
