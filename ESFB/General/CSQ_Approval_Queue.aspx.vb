﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CSQ_Approval_Queue
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1370) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "CSQ Approval Queue"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim EmpCode = CInt(Session("UserID"))
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
            If Not IsPostBack Then
                'DT = DB.ExecuteDataSet("SELECT A.BRANCH_ID,B.BRANCH_NAME,CONVERT(DATE,A.CREATED_DATE) FROM CSQ_ENTRY A INNER JOIN BRANCH_MASTER B ON A.BRANCH_ID = B.BRANCH_ID WHERE A.HO_VERIFY_BY IS NULL AND A.FREQ_ID = 1 AND A.STATUS_ID = 1").Tables(0)
                DT = DB.ExecuteDataSet("SELECT A.BRANCH_ID,B.BRANCH_NAME,A.CREATED_DATE FROM CSQ_ENTRY A INNER JOIN BRANCH_MASTER B ON A.BRANCH_ID = B.BRANCH_ID WHERE A.HO_VERIFY_BY IS NULL AND A.FREQ_ID = (SELECT MAX(ID) FROM CSQ_FREQ_SETTINGS) AND A.STATUS_ID = 1").Tables(0)
                Dim i As Integer = 0
                For Each dtr In DT.Rows
                    Me.hidQueueData.Value += DT.Rows(i)(0).ToString() + "ÿ" + DT.Rows(i)(1).ToString() + "ÿ" + DT.Rows(i)(2).ToString() + "Ñ"
                    i += 1
                Next
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EmpID As Integer = CInt(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
           
        ElseIf CInt(Data(0)) = 2 Then
            
        ElseIf CInt(Data(0)) = 3 Then
           
        ElseIf CInt(Data(0)) = 4 Then
            
        End If
    End Sub
#End Region
End Class
