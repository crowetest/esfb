﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CSQ_Return.aspx.vb" Inherits="CSQ_Return" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script src="../Script/jquery.min.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);          
        }        
        .Button:hover
        {           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);            
            color:#801424;
        }                   
     .bg
     {
         background-color:#FFF;
     }
     .style1
        {
            width: 40%;
            height: 104px;
        }           
    </style>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/jquery.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">       
        window.onload = function () {
            
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) {
                    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");    
                }
            }
            if(context == 2){
                
            }
            if(context == 3){
                      
            }
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
        function white_space(field)
        {
            if(field.value.length==1){
                field.value = (field.value).replace(' ','');
            }
        }
        function NumericCheck(e)//------------function to check whether a value is alpha numeric
        {
            var valid = ((e.which >= 48 && e.which <= 57) || (e.keyWhich == 9) || (e.keyCode == 9) || (e.keyWhich == 8) || (e.keyCode == 8));
            //alert(valid);
            if (!valid) {
                e.preventDefault();
            }

        }
        function DecimalCheck(e,el)
        {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                    if (unicode == 37 || unicode == 38) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (unicode == 46) {
                        var parts = el.value.split('.');
                        if (parts.length == 2)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            else {
                return true;
            }
        }
        function tablefill(){   
            var QDetails = document.getElementById("<%= hidQDetails.ClientID %>").value.split("ÿ");
            
                 
            var bName = document.getElementById("<%= hidBName.ClientID %>").value;
            var bid = document.getElementById("<%= hidBid.ClientID %>").value;
            document.getElementById("<%= txtBranchName.ClientID %>").value = bName;
            document.getElementById("<%= txtBranchCode.ClientID %>").value = bid;
            document.getElementById("<%= txtQuarterName.ClientID %>").value = QDetails[0].toString();

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd
            } 
            if(mm<10){
                mm='0'+mm
            } 
            today = dd + '/' + mm + '/' + yyyy;

            document.getElementById("<%= txtDate.ClientID %>").value = today;
            var QS1 = document.getElementById("<%= hidQS1.ClientID %>").value.split("Ñ");
            var Count1 = QS1.length-1;
            if(Count1 > 0){
                document.getElementById("<%= pnlSec1.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div>";
                tab += "<table style='width:100%'>";
                tab += "<tr>";
                tab += "<td>";
                tab += "<div class='mainhead' style='width:75%; height:auto; padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
                tab += "<table id = 'x' style='width:100%; margin:0px auto;font-family:cambria;line-height:20px;background-color:#EEB8A6;' align='center'>";
                tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;height:35px;' align='left' class='sec2'>";
                tab += "<td style='text-align:center;width:15%;font-weight:bold;' class='NormalText'>Sl No</td>";
                tab += "<td style='text-align:center;width:40%;font-weight:bold;' class='NormalText'>Customer Complaint Particulars</td>";
                tab += "<td style='text-align:center;width:15%;font-weight:bold;' class='NormalText'>Complaints (Nos)</td>";
                tab += "<td style='text-align:center;width:30%;font-weight:bold;' class='NormalText'>Remarks / Action Taken</td>";                
                tab += "</tr>";
                for(var i=0;i<Count1;i++){
                    var qData1 = QS1[i].toString().split("ÿ");
                    tab += "<tr style='width:100%;font-family:cambria;height:10%;' class=sub_first align='left'>";
                    tab += "<td style='text-align:center;width:15%;text-align:center;' class='NormalText' id='slno_"+qData1[0]+"'>"+(i+1)+"</td>";
                    tab += "<td style='text-align:center;width:40%;text-align:left;padding-left:2%;' class='NormalText' id='Question_"+qData1[0]+"'>"+qData1[1]+"</td>";
                    tab += "<td style='text-align:center;width:15%;text-align:left;' class='NormalText'>";
                    tab += "<input type='text' id='Complaints_"+qData1[0]+"' onkeypress='return NumericCheck(event)'/>";
                    tab += "</td>";
                    tab += "<td style='text-align:center;width:30%;text-align:left;' class='NormalText'>";
                    tab += "<textarea id='Remarks_"+qData1[0]+"' name='Remarks_"+qData1[0]+"'type='Text' style='width:99%;'  maxlength='300' onkeypress='return TextAreaCheck(event)' ></textarea>";
                    tab += "</td>";
                    tab += "</tr>";
                }
                tab += "</table>";
                tab += "</div>";
                tab += "</td>";
                tab += "</tr>";
                tab += "</table>";
                tab += "</div>";
                document.getElementById("<%= pnlSec1.ClientID %>").innerHTML = tab;
            }
            var QS2 = document.getElementById("<%= hidQS2.ClientID %>").value.split("Ñ");
            var Count2 = QS2.length-1;
            if(Count2 > 0){
                document.getElementById("<%= pnlSec2.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div>";
                tab += "<table style='width:100%'>";
                tab += "<tr>";
                tab += "<td>";
                tab += "<div class='mainhead' style='width:75%; height:auto; padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
                tab += "<table id = 'x' style='width:100%; margin:0px auto;font-family:cambria;line-height:20px;background-color:#EEB8A6;' align='center'>";
                tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;height:35px;' align='left' class='sec2'>";
                tab += "<td style='text-align:center;width:70%;font-weight:bold;' class='NormalText'>Confirmation on Customer Service Quality Particulars</td>";
                tab += "<td style='text-align:center;width:30%;font-weight:bold;' class='NormalText'>YES / NO</td>";
                tab += "</tr>";
                for(var i=0;i<Count2;i++){
                    var qData2 = QS2[i].toString().split("ÿ");
                    tab += "<tr style='width:100%;font-family:cambria;height:50px;' class=sub_first align='left'>";
                    tab += "<td style='text-align:left;width:80%;padding-left:2%;' class='NormalText' id='Question_"+qData2[0]+"'>"+qData2[1]+"</td>";
                    var select = "<select  id='Answer_"+qData2[0]+"' name='Answer_"+qData2[0]+"' style='width:99%;'>";
                    select += "<option value=-1>---Select---</option>";
                    select += "<option value=1>YES</option>";
                    select += "<option value=2>NO</option>";
                    tab += "<td style='text-align:center;width:20%;' class='NormalText'>"+select+"</td>";                    
                    tab += "</tr>";
                }
                tab += "</table>";
                tab += "</div>";
                tab += "</td>";
                tab += "</tr>";
                tab += "</table>";
                tab += "</div>";
                document.getElementById("<%= pnlSec2.ClientID %>").innerHTML = tab;
            }
            if (QDetails[5] == 0){
                document.getElementById("<%= pnlSec1.ClientID %>").style.display = 'none';
                document.getElementById("<%= pnlSec2.ClientID %>").style.display = 'none';
                document.getElementById("divSection1").style.display = 'none';
            }
            var dataHis = document.getElementById("<%= hidDataHis.ClientID %>").value;
            if(dataHis[0] == '0'){
                document.getElementById("<%= pnlSec1.ClientID %>").style.display = 'none';
                document.getElementById("<%= pnlSec2.ClientID %>").style.display = 'none';
                document.getElementById("divSection1").style.display = 'none';
            }
        }
        function Saveonclick(){
            var branchID = document.getElementById("<%= txtBranchCode.ClientID %>").value;   
            var QDetails = document.getElementById("<%= hidQDetails.ClientID %>").value.split("ÿ"); 
            var quarter = QDetails[3].toString();
            var QS1 = document.getElementById("<%= hidQS1.ClientID %>").value.split("Ñ");
            var Count1 = QS1.length-1;
            var Ans1 = "";
            if(Count1 > 0){
                for(var i=0;i<Count1;i++){
                    var qData1 = QS1[i].toString().split("ÿ");
                    var complaint = document.getElementById("Complaints_"+qData1[0]).value;
                    var remarks = document.getElementById("Remarks_"+qData1[0]).value; 
                    Ans1 += qData1[0] + "ÿ" + complaint + "ÿ" + remarks + "Ñ";
                }
            }
            var QS2 = document.getElementById("<%= hidQS2.ClientID %>").value.split("Ñ");
            var Count2 = QS2.length-1;
            var Ans2 = "";
            if(Count2 > 0){
                for(var i=0;i<Count2;i++){
                    var qData2 = QS2[i].toString().split("ÿ");
                    var selected = document.getElementById("Answer_"+qData2[0]).value; 
                    Ans2 += qData2[0] + "ÿ" + selected + "Ñ";
                }
            }
            var saveData = "1Ø" + branchID.toString() + "Ø" + quarter.toString() + "Ø" + Ans1.toString() + "Ø" + Ans2.toString();
            ToServer(saveData,1); 
        }
    </script>
</head>
</html>
<br />
<div id="parentTable" style="width:90%; height:auto; margin:0px auto; background-color:#EEB8A6;">
    <br />
    <div id = "divSection1" class = "sec1" style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table class="style1" style="width:70%;margin: 0px auto;height:auto;line-height:35px;">
            <tr> 
                <td style="width:20%;">
                    <asp:HiddenField ID="hidData" runat="server" />
                    <asp:HiddenField ID="hidBName" runat="server" />
                    <asp:HiddenField ID="hidQName" runat="server" />
                    <asp:HiddenField ID="hidQDetails" runat="server" />
                    <asp:HiddenField ID="hidDataHis" runat="server" />
                    <asp:HiddenField ID="hidBid" runat="server" />
                    <asp:HiddenField ID="hidQS1" runat="server" />
                    <asp:HiddenField ID="hidQS2" runat="server" />
                </td>
                <td style="width:30%; text-align:left;">
                </td>
                <td style="width:20%">
                </td>
                <td style="width:30%">
                </td>
            </tr>
            <tr> 
                <td>
                    Name of the RBO :
                </td>
                <td>
                    <asp:TextBox ID="txtBranchName" class="ReadOnlyTextBox" runat="server" Width="87%" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    Quarter :
                </td>
                <td>
                    <asp:TextBox ID="txtQuarterName" class="ReadOnlyTextBox" runat="server" Width="87%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr> 
                <td>
                    Branch Code :
                </td>
                <td>
                    <asp:TextBox ID="txtBranchCode" class="ReadOnlyTextBox" runat="server" Width="87%" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    Date :
                </td>
                <td>
                    <asp:TextBox ID="txtDate" class="ReadOnlyTextBox" runat="server" Width="87%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table ID="tblSec1" style="width:90%;height:90px;margin:0px auto;">            
            <tr id="Tr1">
                <td style="text-align: center;">
                    <asp:Panel ID="pnlSec1" Style="width: 100%;  text-align: left; float: left;" runat="server">               

                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />
        <br />
        <table ID="tblSec2" style="width:90%;height:90px;margin:0px auto;">            
            <tr id="Tr2">
                <td style="text-align: center;">
                    <asp:Panel ID="PnlSec2" Style="width: 100%;  text-align: left; float: left;" runat="server">               

                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br /><br />    
    </div>
    <div style="width:80%;height:auto; padding-left:10px;margin:0px auto;">
        <div style="text-align:center; height: 63px;"><br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="SAVE" onclick="return Saveonclick()"/>       
            &nbsp;
            &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
        </div>
    </div>
</div>   
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

