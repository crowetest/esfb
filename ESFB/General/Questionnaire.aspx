﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Questionnaire.aspx.vb" Inherits="Questionnaire"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
		<link rel="stylesheet" href="../../Style/bootstrap-multiselect.css" type="text/css" />
		<script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-multiselect_EMP.js"></script>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
     
   
    <script language="javascript" type="text/javascript">
    var Admin=0;
     function BranchOnChange(AdminFlag)
    {
    
            Admin=AdminFlag;
      panelView();
              
    }
      function QuestionOnChange(AdminFlag)
    {
      
            Admin=AdminFlag;
      panelView();
              
    }
      function FromDtOnChange(AdminFlag)
    {
         
     Admin=AdminFlag;
         panelView(AdminFlag);
              
    }
     function ToDtOnChange(AdminFlag)
    {
       
       Admin=AdminFlag;
        panelView(AdminFlag);
              
    }
   


   
    function panelView(AdminFlag)
    {
        if(AdminFlag==0)
        {
              var FromDt    = document.getElementById("<%= txtFromDt.ClientID %>").value;  
          
              var BranchID    = document.getElementById("<%= cmbBranch.ClientID %>").value;
              var ToDt    = document.getElementById("<%= txtToDt.ClientID %>").value; 
                if (BranchID != -1 && FromDt !="" && ToDt !="")
                {
         
                var Data="1Ø" + BranchID+ "Ø" +FromDt+ "Ø" +ToDt+ "Ø" +"-1";
                ToServer(Data, 1);
                }

                else{
                document.getElementById("<%= hdnQuestion.ClientID %>").value='';
                }
         
          
         
         
        }
        else
        {
            var FromDt    = document.getElementById("<%= txtFromDt.ClientID %>").value;  
            
            var BranchID    = document.getElementById("<%= cmbBranch.ClientID %>").value;
          
            var ToDt    = document.getElementById("<%= txtToDt.ClientID %>").value; 
            
           var QuestionID = document.getElementById("<%= cmbQuestion.ClientID %>").value; 
                  if (BranchID != -1 && FromDt !="" && ToDt !="" && QuestionID !="") 
                  {
              
                         var Data="1Ø" + BranchID+ "Ø" +FromDt+ "Ø" +ToDt+ "Ø" +QuestionID;
               
                         ToServer(Data, 1);
                
                  }
                   else{
                   document.getElementById("<%= hdnQuestion.ClientID %>").value='';
                   }
            

        }
          
                 
    }

     function cursorwait(e) {
            document.body.className = 'wait';}
     function cursordefault(e) {
            document.body.className = 'default';}
      
    function btnExit_onclick() 
    {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }

    function btnSave_onclick() {           
    var newstr="";
    row = document.getElementById("<%= hdnQuestion.ClientID %>").value.split("¥");

    var BranchID    = document.getElementById("<%= cmbBranch.ClientID %>").value;
    for (n = 1; n <= row.length-1 ; n++) {     
        var Question=row[n].split("µ");  
    

        newstr+="¥"+ Question[5]+"µ"+Question[3]+"µ"+Question[4];

    }
   
    var Data = "2Ø"+ BranchID + "Ø"  + newstr ;
    cursorwait();
    document.getElementById("btnSave").disabled=true;
    
    ToServer(Data, 2);
            
    }

    function FromServer(Arg, Context) 
    {
          switch (Context)
           {
            
               case 1:
               {
                    var NewStr = "";
                    var arr = Arg.split("~");
                   
                   
                    document.getElementById("<%= hdnQuestion.ClientID %>").value=arr[1];
                    table_fill();
                    break;       
               }       
               case 2:
                {
                    var Data = Arg.split("Ø");
                    cursordefault();
                    alert(Data[1]);
                    document.getElementById("btnSave").disabled=false;
                    if (Data[0] == 0) 
                    window.open('Questionnaire.aspx','_self');
                    break;
               }       
             }
          } 
    function updateValueDisscussion(id)
        {  
          
             row = document.getElementById("<%= hdnQuestion.ClientID %>").value.split("¥");
             var newStr=""
             if (row.length != 0)
             {
                 for (n = 1; n <= row.length-1 ; n++) {
                 var Question=row[n].split("µ");
                 if(id==n)
                 {
                        
                        var StatusValue = document.getElementById("cmbAnswer"+id).options[document.getElementById("cmbAnswer"+id).selectedIndex].text;
                        var Remarks=document.getElementById("txtRemark"+id).value;
                        newStr+="¥"+Question[0]+"µ"+Question[1]+"µ"+Question[2]+"µ"+ document.getElementById("cmbAnswer"+id).value+"µ"+Remarks+"µ"+Question[5];
                 }
                else
                {
                
                        newStr+="¥"+row[n];
                }
           
            }
            document.getElementById("<%= hdnQuestion.ClientID %>").value=newStr;
        }
        }
        function  table_fill()
        {
       
            if (document.getElementById("<%= hdnQuestion.ClientID %>").value != "") 
            {
            document.getElementById("<%= pnQuestion.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>"; 
            tab += "<td style='width:15%;text-align:center'><b>Branch</b></td>"; 
            tab += "<td style='width:0%;text-align:center;display:none'><b>Question_ID</b></td>"; 
            tab += "<td style='width:40%;text-align:center' ><b>Questions</b></td>"; 
            tab += "<td style='width:15%;text-align:center'><b>Response Date</b></td>";   
            tab += "<td style='width:5%;text-align:center'><b>Answer</b></td>";
            tab += "<td style='width:20%;text-align:center'><b>Remark </b></td>";    
 
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            row = document.getElementById("<%= hdnQuestion.ClientID %>").value.split("¥");
        
            for (n = 1; n <= row.length - 1; n++) 
            {
                 col = row[n].split("µ");  
                
                 if (row_bg == 0) 
                 {
                    row_bg = 1;
                    tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                 }
                 else 
                 {
                    row_bg = 0;
                    tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                 }
                 tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";  
                 tab += "<td style='width:15%;text-align:left'>" + col[0] + "</td>";               
                 tab += "<td style='width:0%;text-align:left;display:none'>" + col[5] + "</td>";
                tab += "<td style='width:40%;text-align:left'>" + col[1] + "</td>";
                tab += "<td style='width:15%;text-align:left'>" + col[2] + "</td>";

               
               if (Admin == 0)
               {
                 if(col[3]==-1)
                    var select1 = "<select id='cmbAnswer" + n.toString() + "' class='NormalText' name='cmbAnswer" + n.toString() + "' style='width:100%' onchange='updateValueDisscussion("+ n +")'    >";
                else
                    var select1 = "<select id='cmbAnswer" + n.toString() + "' class='NormalText' name='cmbAnswer" + n.toString() + "' style='width:100%'  onchange='updateValueDisscussion("+ n +")' disable='True'   >";

                if(col[3]==-1)
                    select1 += "<option value='-1' selected=true>-----Select-----</option>";
                else
                    select1 += "<option value='-1'>-----Select-----</option>";
                if(col[3]==1)
                    select1 += "<option value='1' selected=true>Yes</option>";
                else
                    select1 += "<option value='1'>Yes</option>";
                if(col[3]==2)
                    select1 += "<option value='2' selected=true>No</option>";
                else
                    select1 += "<option value='2'>No</option>";
                                                    
                tab += "<td style='width:5%;text-align:left'>" + select1 + "</td>";

                    var txtRemarks = "<input id='txtRemark" + n + "' name='txtRemark" + n + "' type='Text' value='"+ col[4] +"' TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%; word-wrap:'break-word;' class='NormalText' maxlength='1000' onkeypress='return TextAreaCheck(event)' onchange='updateValueDisscussion("+ n +")' />";                                            
                    tab += "<td style='width:20%;text-align:left'>" + txtRemarks + "</td>";
                }
                else
                {
                    if( col[3]==1)
                        tab += "<td style='width:5%;text-align:left'>Yes</td>";
                    else if( col[3]==2)
                        tab += "<td style='width:5%;text-align:left'>No</td>";
                    else
                        tab += "<td style='width:5%;text-align:left'></td>";

                    tab += "<td style='width:20%;text-align:left'>" + col[4] + "</td>";

                }
  
                tab += "</tr>";
            }
                tab += "</table></div></div></div>";
                document.getElementById("<%= pnQuestion.ClientID %>").innerHTML = tab;


                setTimeout(function() {
                    document.getElementById("txtBranch"+(n-1)).focus().select();
                    }, 4);
            }
            else
            document.getElementById("<%= pnQuestion.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//
        }
     

    </script>
    <br />
    <div>
    <table align="center" style="width:80%; margin: 0px auto;">
            <tr>
          <td style="width: 40%;text-align:right;font-size: 10pt;">Branch</td>
           
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="50%" ForeColor="Black" AppendDataBoundItems="True">                
                </asp:DropDownList></td>
                 <td style="width: 19%;text-align:right;"></td>
        </tr>

        <tr>
             <td style="width: 40%;text-align:right;font-size: 10pt;">From</td>
            
             <td style="width: 40%;text-align:left;">
                <asp:TextBox ID="txtFromDt" runat="server" class="NormalText" Width="50%"  
                    ReadOnly="true"></asp:TextBox> <asp:CalendarExtender ID="CalendarExtender2" Format="dd MMM yyyy"  runat="server" Enabled="True" TargetControlID="txtFromDt"></asp:CalendarExtender>

                </td>
                 <td style="width: 19%;text-align:right;"></td>
        </tr>
        <tr>
            <td style="width: 40%;text-align:right;font-size: 10pt;">To</td>
            
             <td style="width: 40%;text-align:left;">
                <asp:TextBox ID="txtToDt" runat="server" class="NormalText" Width="50%"  
                    ReadOnly="true"></asp:TextBox> 
                <asp:CalendarExtender ID="CalendarExtender1" Format="dd MMM yyyy"  runat="server" Enabled="True" TargetControlID="txtToDt"></asp:CalendarExtender></td>
            <td style="width: 19%;text-align:right;"></td>

        </tr>
          <tr>
          <td style="width: 40%;text-align:right;font-size: 10pt;"></td>
           
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbQuestion" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="50%" ForeColor="Black" AppendDataBoundItems="True">                
                </asp:DropDownList></td>
                 <td style="width: 19%;text-align:right;"></td>
        </tr>
            <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
           <tr>
           
                <td style="width:12% ;text-align:center;" colspan="4" class="mainhead">
                <strong>Questionnaire</strong>
               
                <b style="color:red;">*</b>
                </td>
           
         </tr> 
         <tr style="text-align:center;">
              <td  colspan="4" style="text-align:center;">
              <asp:Panel ID="pnQuestion" runat="server">
              </asp:Panel>
             </td>
        </tr>
        <tr style="text-align:center;">
                <td  colspan="4" style="text-align:center;">
                <input id="btnSave" type="button" value="SAVE" onclick="return btnSave_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;
                <input id="btnExit" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />
                
              
                <asp:HiddenField 
                    ID="hdnStatus" runat="server" />
                <asp:HiddenField 
                    ID="hdnQuestion" runat="server" />
                
               </td>
         </tr>
    </table>
    </div> 
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    </asp:Content>
