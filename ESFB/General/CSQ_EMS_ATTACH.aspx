﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CSQ_EMS_ATTACH.aspx.vb" Inherits="CSQ_EMS_ATTACH" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
      #loadingmsg   
      {
      width:55%;
      background: transparent; 
      padding: 0px;
      position: fixed;     
      z-index: 100;
      margin-left: 0%;
      margin-bottom: 0%;   
      margin-top: 0%;  
      color:#FFF;   
      }
      #loadingover {
      background: black;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }

   </style>
    <title></title>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function showLoading() {
            document.getElementById('loadingmsg').style.display = 'block';
            document.getElementById('loadingover').style.display = 'block';
        }
        function closeLoading() {
            document.getElementById('loadingmsg').style.display = 'none';
            document.getElementById('loadingover').style.display = 'none';
        }
        function onUpload() {
            var file = document.querySelector('input[type=file]').files[0];
            var size = Math.abs(file.size);
            if (((size / 1024) > 4096)) {
                alert("File size should be less than 4 mb");
                document.getElementById("<%= FileUpload1.ClientID %>").value = "";
                document.getElementById("<%= FileUpload1.ClientID %>").focus();
                return false;
            }
        }

// ]]>
    </script>
</head>
</html> 
<div align="center" style="width:100%;">
    <br />
    <br />
    <table align="center" style="width: 40%; margin:0px auto;">
        <tr>
            <td style="width:50%;text-align:center;">
                Select File</td>
            <td style="width:50%">
                <asp:FileUpload ID="FileUpload1" runat="server" Font-Names="Cambria" onchange="onUpload()"/><asp:Label ID="filesize" runat="server" style="color:red;font-size:small;">File size should be less than 4 mb</asp:Label>
                <asp:HiddenField ID="hidData" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width: 100%;" colspan="2"> <div id='loadingmsg' style="text-align:center; display: none;margin:0px auto; ">                             
                    <img alt="progress" src="../image/Progress.gif" style="width:60px;height:60px;text-align:center; background-color:#FFF;"/> &nbsp;&nbsp;Uploading Please wait....                    
          <br /><asp:Label ID="lblWait" runat="server" style="color:#FFF"></asp:Label></div><div id='loadingover' style='display: none;'></div></td>
        </tr>
        <tr>
            <td style="width: 100%;" colspan="2">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="2">
                <asp:Button ID="btnSave" runat="server" Text="UPLOAD" Style="font-family: cambria;
                    cursor: pointer; width: 70px;" />
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 70px;" type="button"
                    value="EXIT" onclick="window.close()" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>

