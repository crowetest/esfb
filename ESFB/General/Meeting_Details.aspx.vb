﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class Meeting_Details
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim DT As New DataTable
    Dim SINo As Integer

    Dim UserID As Integer
    Dim BranchID As Integer

    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing

#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Customer Meeting Details"
            UserID = CInt(Session("UserID"))
            BranchID = CInt(Session("BranchID"))

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            '/--- Register Client Side Functions ---//
            Me.txtDateOfMeeting.Attributes.Add("onchange", "return MeetingDateOnChange()")
            Me.btnSave.Attributes.Add("onclick", "return btnSave_onclick()")
            Me.btnView.Attributes.Add("onclick", "return ViewAttachment()")
            fup1.Attributes.Add("onchange", "return validate_new_fileupload()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try


    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent


        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim MeetingID As Integer = Data(1)
            Dim MeetingDt As Date = CDate(Data(2))
            Dim MeetingDtl As String = Data(3)
            If MeetingDtl <> "" Then
                MeetingDtl = MeetingDtl.Substring(1)
            End If
            Dim DisscussionDtl As String = Data(4)
            If DisscussionDtl <> "" Then
                DisscussionDtl = DisscussionDtl.Substring(1)
            End If
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@MeetingID", SqlDbType.Int)
                Params(0).Value = MeetingID
                Params(1) = New SqlParameter("@MeetingDt", SqlDbType.Date)
                Params(1).Value = MeetingDt
                Params(2) = New SqlParameter("@MeetingDtl", SqlDbType.VarChar, 4000)
                Params(2).Value = MeetingDtl
                Params(3) = New SqlParameter("@DisscussionDtl", SqlDbType.VarChar, 4000)
                Params(3).Value = DisscussionDtl
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = CInt(Session("UserID"))
                Params(5) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(5).Value = CInt(Session("BranchID"))
                Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(7).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_MEETING_DETAILS", Params)
                ErrorFlag = CInt(Params(6).Value)
                Message = CStr(Params(7).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            DT = New DataTable
            Dim Sql As String
            Dim MeetingDt As String = Data(1).ToString()
            Dim MeetingID As String = CStr(0)
            CallBackReturn = "~"
            Sql = "select  Meeting_ID FROM MEETING_MASTER MM WHERE  MM.Meeting_Dt= CONVERT(DATE, '" & MeetingDt & "',105) and MM.Branch_ID = " & BranchID & ""
            DT = GN.GetQueryResult(Sql)
            For Each DR In DT.Rows
                CallBackReturn += DR(0).ToString()
                MeetingID = DR(0).ToString()
            Next
            CallBackReturn += "~"
            Sql = "select  Attendee_Name,Category,Senior_citizen,Women from MEETING_MASTER MM INNER JOIN MEETING_ATTENDEE_DTL MAD ON MM.Meeting_ID = MAD.Meeting_ID WHERE  MM.Meeting_Dt= CONVERT(DATE, '" & MeetingDt & "',105) and MM.Branch_ID = " & BranchID & ""
            DT = GN.GetQueryResult(Sql)
            For Each DR In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
            Next
            CallBackReturn += "~"
            Sql = "select  Discussion,Follow_up_action,Remarks,StatusValue from MEETING_MASTER MM INNER JOIN MEETING_DISCUSSION_DTL MAD ON MM.Meeting_ID = MAD.Meeting_ID WHERE MM.Meeting_Dt = CONVERT(DATE, '" & MeetingDt & "',105) and MM.Branch_ID = " & BranchID & ""
            DT = GN.GetQueryResult(Sql)
            For Each DR In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString()
            Next
            CallBackReturn += "~"
            If MeetingID <> "0" Then
                Sql = "select count(*) from DMS_ESFB.dbo.MEETING_ATTACH where meeting_id = " + MeetingID
                DT = GN.GetQueryResult(Sql)
                For Each DR In DT.Rows
                    CallBackReturn += DR(0).ToString()
                Next
            Else
                CallBackReturn += "0"
            End If
            
        End If
    End Sub

#End Region
    Private Sub initializeControls()
        txtDateOfMeeting.Focus()
        txtDateOfMeeting.Text = ""
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim Data As String() = hid_Data.Value.Split(CChar("Ø"))
            Dim MeetingID As Integer = Data(1)
            Dim MeetingDt As Date = CDate(Data(2))
            Dim MeetingDtl As String = Data(3)
            If MeetingDtl <> "" Then
                MeetingDtl = MeetingDtl.Substring(1)
            End If
            Dim DisscussionDtl As String = Data(4)
            If DisscussionDtl <> "" Then
                DisscussionDtl = DisscussionDtl.Substring(1)
            End If
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim MeetID As Integer = 0
            Dim NoofAttachments As Integer = 0
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        NoofAttachments += 1
                    End If
                Next
            End If
            Try
                Dim Params(8) As SqlParameter
                Params(0) = New SqlParameter("@MeetingID", SqlDbType.Int)
                Params(0).Value = MeetingID
                Params(1) = New SqlParameter("@MeetingDt", SqlDbType.Date)
                Params(1).Value = MeetingDt
                Params(2) = New SqlParameter("@MeetingDtl", SqlDbType.VarChar, 4000)
                Params(2).Value = MeetingDtl
                Params(3) = New SqlParameter("@DisscussionDtl", SqlDbType.VarChar, 4000)
                Params(3).Value = DisscussionDtl
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = CInt(Session("UserID"))
                Params(5) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(5).Value = CInt(Session("BranchID"))
                Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@MeetID", SqlDbType.Int)
                Params(8).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_MEETING_DETAILS", Params)
                ErrorFlag = CInt(Params(6).Value)
                Message = CStr(Params(7).Value)
                MeetID = CStr(Params(8).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            If ErrorFlag = 0 Then
                initializeControls()
            End If
            If MeetID > 0 And hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                        Dim Params(3) As SqlParameter
                        Params(0) = New SqlParameter("@MeetingID", SqlDbType.Int)
                        Params(0).Value = MeetID
                        Params(1) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                        Params(1).Value = AttachImg
                        Params(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                        Params(2).Value = ContentType
                        Params(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                        Params(3).Value = FileName
                        DB.ExecuteNonQuery("SP_MEETING_ATTACH", Params)
                    End If

                Next
            End If


            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub

End Class

