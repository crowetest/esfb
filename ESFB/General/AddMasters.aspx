﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AddMasters.aspx.vb" Inherits="General_AddMasters" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head>
    <title></title>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
     <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function TypeOnChange()
        {
            var TypeID = document.getElementById("<%= cmbType.ClientID %>").value;
            document.getElementById("<%= cmbSubType.ClientID %>").disabled = false;
            if (TypeID == 1) {
                document.getElementById("<%= lblHead.ClientID %>").innerHTML = "Territory";
                document.getElementById("<%= lblNewHead.ClientID %>").innerHTML = "Territory Name";
                document.getElementById("row1").style.display = "none";
                document.getElementById("col1").innerHTML = "";
            }
            if (TypeID == 2) {
                document.getElementById("<%= lblHead.ClientID %>").innerHTML = "Region";
                document.getElementById("<%= lblNewHead.ClientID %>").innerHTML = "Region Name";
                document.getElementById("row1").style.display = "";
                document.getElementById("col1").innerHTML = "Territory";
            }
            if (TypeID == 3) {
                document.getElementById("<%= lblHead.ClientID %>").innerHTML = "Area";
                document.getElementById("<%= lblNewHead.ClientID %>").innerHTML = "Area Name";
                document.getElementById("row1").style.display = "";
                document.getElementById("col1").innerHTML = "Region";
            }
            var Data = "1Ø" + TypeID;
            ToServer(Data, 1);
        }

        function HeadOnChange() 
        {
            var TypeID = document.getElementById("<%= cmbType.ClientID %>").value;
            var HeadID = document.getElementById("<%= cmbHead.ClientID %>").value;
            var SubType = document.getElementById("<%= cmbSubType.ClientID %>").value;
            if (HeadID == "-1") {
                document.getElementById("<%= txtHeadName.ClientID %>").value = ""; //document.getElementById("<%= cmbHead.ClientID %>").options[document.getElementById("<%= cmbHead.ClientID %>").selectedIndex].text; 
                document.getElementById("<%= txtCode.ClientID %>").value = "102";
                document.getElementById("<%= txtName.ClientID %>").value = "George K John";
                document.getElementById("<%= cmbSubType.ClientID %>").disabled = false;
                document.getElementById("<%= cmbSubType.ClientID %>").value = -1;
            }
            else 
            {
                var Data = "2Ø" + TypeID + "Ø" + HeadID ;
                ToServer(Data, 2);
            }
        }

        function CodeOnChange() 
        {
            var Code = document.getElementById("<%= txtCode.ClientID %>").value;
            if (Code > 0) {
                var Data = "3Ø" + Code;
                ToServer(Data, 3);
            }
            else {
                alert("Enter Correct EmpCode");
                return false;
            }
        }

        function SaveOnClick() 
        {
            var CategoryID  = document.getElementById("<%= cmbType.ClientID %>").value;
            var SubTypeID   = document.getElementById("<%= cmbHead.ClientID %>").value;
            var SubTypeName = document.getElementById("<%= txtHeadName.ClientID %>").value;
            var HeadCode    = document.getElementById("<%= txtCode.ClientID %>").value;
            var TypeID = document.getElementById("<%= cmbSubType.ClientID %>").value;
            if (TypeID == "")
                TypeID = '-1';
            var Flag = 0;
            if (SubTypeName == "")
            { alert("Enter  Name"); document.getElementById("<%= txtHeadName.ClientID %>").focus(); return false; }
            if (HeadCode == "")
            { alert("Enter Head EmpCode"); document.getElementById("<%= txtCode.ClientID %>").focus(); return false; }
            if (SubTypeID != -1) 
            {
                Flag = 1;
//                if (TypeID == "-1")
//                { alert("Select Territory/Region"); document.getElementById("<%= cmbSubType.ClientID %>").focus(); return false; }
            }
            var EmailID = document.getElementById("<%= txtEmail.ClientID %>").value;
            if (EmailID != "") {
                var ret = checkEmail(document.getElementById("<%= txtEmail.ClientID %>")); if (ret == false) {return false; }
            }
            else {
                alert("Enter  EmailID"); document.getElementById("<%= txtEmail.ClientID %>").focus(); return false;
            }
            var Data = "4Ø" + CategoryID + "Ø" + TypeID + "Ø" + SubTypeID + "Ø" + SubTypeName + "Ø" + HeadCode + "Ø" + Flag + "Ø" + EmailID;
            ToServer(Data, 4);
        }

        function FromServer(arg, context) 
        {
            switch (context) 
            {
                case 1: //  Existing Fill On Change of Type
                    {
                        var Data = arg.split("Ø");
                        ComboFill(Data[0], "<%= cmbHead.ClientID %>");
                        ComboFill(Data[1], "<%= cmbSubType.ClientID %>");
                        var HeadID = document.getElementById("<%= cmbHead.ClientID %>").value;
                        if (HeadID == -1) {
                            document.getElementById("<%= txtHeadName.ClientID %>").value = "";
                            document.getElementById("<%= txtCode.ClientID %>").value = "102";
                            document.getElementById("<%= txtName.ClientID %>").value = "George K John";
                        }
                        break;
                    }
                case 2:
                    {
                        var Data = arg.split("Ø"); //3644ØAnand K B
                        document.getElementById("<%= txtCode.ClientID %>").value = Data[0];
                        document.getElementById("<%= txtName.ClientID %>").value = Data[1];
                        document.getElementById("<%= txtHeadName.ClientID %>").value = Data[2];
                        document.getElementById("<%= cmbSubType.ClientID %>").value = Data[3];
                        document.getElementById("<%= txtEmail.ClientID %>").value = Data[4];
                        document.getElementById("<%= cmbSubType.ClientID %>").disabled = true;
                        break;
                    }
                case 3:
                    {
                        document.getElementById("<%= txtName.ClientID %>").value = arg;
                        break;
                    }
                case 4:
                    {
                        var Data = arg.split("ʘ");
                        alert(Data[1]);
                        if (Data[0] == 0) {
                            window.open('AddMasters.aspx','_self');
                        }
                    }
            }
        }


        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

// ]]>
    </script>

</head>
    <table style="width: 80%;margin: 0px auto">
        <tr>
            <td style="width:40%; text-align: right;font-family:Cambria;">
                Select Type</td>
            <td   style="width:5%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <asp:DropDownList ID="cmbType" class="NormalText" runat="server" Width="40%">
                                            <asp:ListItem Value="1">TERRITORY</asp:ListItem> 
                        <asp:ListItem Value="2">REGION</asp:ListItem> 
                        <asp:ListItem Value="3">AREA</asp:ListItem> 

                    </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width:40%; text-align: right;font-family:Cambria;">
                <asp:Label ID="lblHead" runat="server" Text="Territory"></asp:Label>
            </td>
            <td   style="width:5%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <asp:DropDownList ID="cmbHead" class="NormalText" runat="server" Width="40%">
                                        </asp:DropDownList>
            </td>
        </tr>
        <tr id="row1" style="display:none;">
            <td id = "col1" style="width:40%; text-align: right;font-family:Cambria;">
                Territory</td>
            <td   style="width:5%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <asp:DropDownList ID="cmbSubType" class="NormalText" runat="server" Width="40%">
                                        </asp:DropDownList>
            </td>
        </tr>
        <tr >
            <td style="width:40%; text-align: right;font-family:Cambria;">
                <asp:Label ID="lblNewHead" runat="server" Text="Territory Name"></asp:Label>
            </td>
            <td   style="width:5%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <asp:TextBox ID="txtHeadName" runat="server"  class="NormalText"
                        style=" font-family:Cambria;font-size:10pt;" Width="40%" MaxLength="50"   />
            </td>
        </tr>
        <tr>
            <td style="width:40%; text-align: right;font-family:Cambria;">
                <asp:Label ID="lblHeadCode" runat="server" Text="Head (Emp Code)"></asp:Label>
            </td>
            <td   style="width:5%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <asp:TextBox ID="txtCode" runat="server"  class="NormalText"
                        style=" font-family:Cambria;font-size:10pt;" Width="40%" 
                    MaxLength="50"   />
            </td>
        </tr>
        <tr>
            <td style="width:40%; text-align: right;font-family:Cambria;">
                Head
                Name</td>
            <td   style="width:5%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <asp:TextBox ID="txtName" runat="server"  class="NormalText"
                        style=" font-family:Cambria;font-size:10pt;" Width="40%" 
                    MaxLength="50"   />
            </td>
        </tr>
        <tr>
            <td style="width:40%; text-align: right;font-family:Cambria;">
                Email ID</td>
            <td   style="width:5%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <asp:TextBox ID="txtEmail" runat="server"  
                        style=" font-family:Cambria;font-size:10pt;" Width="40%" 
                    MaxLength="50"   />
            </td>
        </tr>
        <tr>
            <td style="width:40%; text-align: right;font-family:Cambria;">
                &nbsp;</td>
            <td   style="width:5%;">
                &nbsp;</td>
            <td  style="width:55%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center; font-family:Cambria;" colspan="3">
           <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="SAVE"  onclick="return SaveOnClick()" />
                 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnType" runat="server" />
</asp:Content>

