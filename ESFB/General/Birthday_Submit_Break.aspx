﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Birthday_Submit_Break.aspx.vb" Inherits="Birthday_Submit_Break" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head>
<title></title>
<link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">


    function BranchOnChange() {
        if(document.getElementById("<%= txtBranchId.ClientID %>").value!=""){
            BranchId=document.getElementById("<%= txtBranchId.ClientID %>").value;
            ToServer("1Ø" + BranchId, 1);
        }
    }



     function FromServer(arg, context) {
        if (context == 1) {
            var Data = arg.split("~");
            if (Data[0]==1){
                document.getElementById("<%= txtBranch.ClientID %>").value=Data[1];
                BranchId=document.getElementById("<%= txtBranchId.ClientID %>").value;
                ToServer("3Ø" + BranchId, 3);
            }
            else {
                alert("Invalid Branch Code");
                document.getElementById("<%= txtBranchId.ClientID %>").value="";
                document.getElementById("<%= txtBranch.ClientID %>").value="";
                document.getElementById("<%= txtBranchId.ClientID %>").focus();
                return false();
            }
        }
        else if (context == 2) {
            if (arg==1 ){
                alert("Submission is sucessfully withdrawed");
                window.open("Birthday_Submit_Break.aspx", "_self");
            }
        }
        else if (context == 3) {
            if (arg==2 ){
                alert("Submission withdrawal of this branch is already done.");
                document.getElementById("<%= txtBranchId.ClientID %>").value="";
                document.getElementById("<%= txtBranch.ClientID %>").value="";
                document.getElementById("<%= txtBranchId.ClientID %>").focus();
                return false();
            }
            else if (arg==3 ){
                alert("Submission is not done.");
                document.getElementById("<%= txtBranchId.ClientID %>").value="";
                document.getElementById("<%= txtBranch.ClientID %>").value="";
                document.getElementById("<%= txtBranchId.ClientID %>").focus();
                return false();
            }
        }
    }


    function WithDrawOnClick(){
        var Conf = confirm("Are you sure to withdraw the submission?");
        if (Conf == true) {
            BranchId=document.getElementById("<%= txtBranchId.ClientID %>").value;
            ToServer("2Ø" + BranchId, 2);
        }
    }



    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }


</script>
</head>
 <table style="width: 80%;margin: 0px auto">
    <tr>
        <td colspan="5" style="width:100%; height: 23px;"></td>
    </tr>
    <tr>
        <td style="width:10%;"></td>
        <td style="width:15%;"></td>
        <td style="width:10%; text-align: left;font-family:Cambria;">
            Branch Code</td>
        <td  style="width:45%;">
                <asp:TextBox ID="txtBranchId" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="40%" MaxLength="5" onkeypress='return NumericCheck(event)'  />
        </td>
        <td style="width:10%;"></td>
    </tr>
    <tr>
        <td style="width:10%;"></td>
        <td style="width:15%;"></td>
        <td style="width:10%; text-align: left;font-family:Cambria;">
            Branch Name</td>
        <td  style="width:45%;">
                <asp:TextBox ID="txtBranch" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="40%" MaxLength="50" disabled="true"   />
        </td>
        <td style="width:10%;"></td>
    </tr>
    <tr>
        <td colspan="5" style="width:100%; height: 23px;"></td>
    </tr>
    <tr>
        <td style="text-align: center; font-family:Cambria;" colspan="5">
           <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="WITHDRAWAL"  onclick="return WithDrawOnClick()" />                 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
        </td>
    </tr>
 </table>
</asp:Content>

