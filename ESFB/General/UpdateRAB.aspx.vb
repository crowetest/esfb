﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class General_UpdateRAB
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable

#Region "Page Load & Dispose"
    Protected Sub Election_EditElectionBooth_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 186) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Shuffle Location"

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim DR As DataRow
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        'Dim UserID As Integer = CInt(Session("UserID"))
        Select Case CInt(Data(0))
            Case 1
                Dim ID As Integer = CInt(Data(1))
                CallBackReturn = ""
                If (ID = 1) Then 'Region
                    DT = GF.GetQueryResult("select convert(varchar(10),r.Region_ID)+'Æ'+upper(r.Region_Name)+'Æ'+convert(varchar(10),r.Zone_ID)+'Æ'+convert(varchar(10),isnull(r.Region_head,''))+'Æ'+isnull(upper(e.Emp_Name),'')+'Æ0' from REGION_MASTER r left outer join EMP_MASTER e on r.Region_head=e.Emp_Code order by r.Region_Name")
                    If DT.Rows.Count > 0 Then
                        For Each DR In DT.Rows
                            CallBackReturn += CStr(DR(0)) + "ʘ"
                        Next
                    End If
                    CallBackReturn += "¶"
                    DT = GF.GetQueryResult("select Zone_ID,upper(Zone_Name) from Zone_Master")
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                ElseIf (ID = 2) Then 'Area
                    DT = GF.GetQueryResult("select convert(varchar(10),a.Area_ID)+'Æ'+upper(a.Area_Name)+'Æ'+convert(varchar(10),a.Region_ID)+'Æ'+convert(varchar(10),isnull(a.Area_Head,''))+'Æ'+isnull(upper(e.Emp_Name),'')+'Æ0' from AREA_MASTER a left outer join EMP_MASTER e on a.Area_Head=e.Emp_Code order by a.Area_Name")
                    If DT.Rows.Count > 0 Then
                        For Each DR In DT.Rows
                            CallBackReturn += CStr(DR(0)) + "ʘ"
                        Next
                    End If
                    CallBackReturn += "¶"
                    DT = GF.GetQueryResult("select Region_ID,upper(Region_Name) from REGION_MASTER")
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                ElseIf (ID = 3) Then 'Branch
                    DT = GF.GetQueryResult("select convert(varchar(10),b.Branch_ID)+'Æ'+upper(b.Branch_Name)+'Æ'+convert(varchar(10),b.Area_ID)+'Æ'+isnull(upper(e.Emp_Name),'')+'('+convert(varchar(10),isnull(b.Branch_Head,''))+')'+'Æ'+isnull(b.Address,'Nil')+'Æ0Æ'+isnull(b.Phone,'Nil') from BRANCH_MASTER b left outer join EMP_MASTER e on b.Branch_Head=e.Emp_Code where b.Branch_ID > 100 order by b.Branch_ID")
                    If DT.Rows.Count > 0 Then
                        For Each DR In DT.Rows
                            CallBackReturn += CStr(DR(0)) + "ʘ"
                        Next
                    End If
                    CallBackReturn += "¶"
                    DT = GF.GetQueryResult("select Area_ID,upper(Area_Name) from AREA_MASTER")
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2
                Dim ID As Integer = CInt(Data(1))
                Dim ChangeData As String = Data(2)
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(4) As SqlParameter
                    Params(0) = New SqlParameter("@ChangeID", SqlDbType.Int)
                    Params(0).Value = ID
                    Params(1) = New SqlParameter("@ChangeData", SqlDbType.VarChar)
                    Params(1).Value = ChangeData
                    Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(2).Value = UserID
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 500)
                    Params(4).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_SHUFFLE_LOCATION", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region

    Private Function DR(p1 As Integer) As Object
        Throw New NotImplementedException
    End Function

End Class
