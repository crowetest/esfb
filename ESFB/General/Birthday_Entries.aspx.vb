﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Birthday_Entries
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Disposed"
    Protected Sub General_Birthday_Entries_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Update Birthday"
            txtBranchId.Text = CStr(Session("BranchID"))
            txtBranch.Text = CStr(Session("BranchName"))
            Dim t As Integer = 0
            Me.txtDOB_CalendarExtender.EndDate = CDate(Session("TraDt"))
            Dim i As Integer = 0
            hid_Value.Value = ""
            DT = DB.ExecuteDataSet("SELECT E.EMP_CODE,E.EMP_NAME,isnull(P.DOB,'') FROM EMP_MASTER E , EMP_PROFILE P WHERE E.EMP_CODE=P.EMP_CODE and e.status_id=1 and e.department_id=" & CInt(Session("DepartmentID")) & " order by e.emp_name").Tables(0)
            If DT.Rows.Count <> 0 Then
                While i < DT.Rows.Count
                    hid_Value.Value += CStr(DT.Rows(i)(0)) & "~" & CStr(DT.Rows(i)(1)) & "~" & Format(DT.Rows(i)(2), "dd MMM yyyy")
                    hid_Value.Value += "¥"
                    i = i + 1
                End While
            End If
            DT = DB.ExecuteDataSet("SELECT * FROM BIRTHDAY_SUBMIT WHERE BRANCH_ID=" & CInt(Session("DepartmentID")) & " and status_id=1 and BRANCH_FLAG=0").Tables(0)
            If DT.Rows.Count <> 0 Then
                hid_Submit.Value = CStr(1)
            Else
                hid_Submit.Value = CStr(0)
            End If

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
            Me.txtDOB.Attributes.Add("onchange", "return DOBOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub General_Birthday_Entries_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Try
            Dim Data() As String = eventArgument.Split(CChar("Ø"))
            If CInt(Data(0)) = 1 Then
                DT = DB.ExecuteDataSet("SELECT E.EMP_NAME,isnull(P.DOB,'') FROM EMP_MASTER E , EMP_PROFILE P WHERE E.EMP_CODE=P.EMP_CODE AND  E.EMP_CODE=" & CInt(Data(1)) & "").Tables(0)
                If DT.Rows.Count <> 0 Then
                    CallBackReturn = "1~" + CStr(DT.Rows(0)(0)) + "~" + Format(DT.Rows(0)(1), "dd MMM yyyy")
                End If
            ElseIf CInt(Data(0)) = 2 Then
                Dim IntVal As Integer = 0
                Dim dob As Date = CDate(Data(2))
                IntVal = DB.ExecuteNonQuery("UPDATE EMP_PROFILE SET DOB='" & dob.Date & "' WHERE EMP_CODE=" & CInt(Data(1)) & "")
                CallBackReturn = CStr(IntVal)
            ElseIf CInt(Data(0)) = 3 Then
                Dim Branch_Id As Integer = CInt(Session("DepartmentID"))
                Dim UserId As Integer = CInt(Session("UserID"))
                Dim IntVal As Integer = 0
                DT = DB.ExecuteDataSet("SELECT * FROM BIRTHDAY_SUBMIT WHERE BRANCH_ID=" & Branch_Id & "").Tables(0)
                If DT.Rows.Count <> 0 Then
                    IntVal = DB.ExecuteNonQuery("UPDATE BIRTHDAY_SUBMIT set SUBMIT_BY=" & UserId & " , SUBMIT_DT=getdate(), STATUS_ID=1,BRANCH_FLAG=0 WHERE BRANCH_ID=" & Branch_Id & "")
                    CallBackReturn = CStr(IntVal)
                Else
                    IntVal = DB.ExecuteNonQuery("INSERT INTO BIRTHDAY_SUBMIT ( BRANCH_ID,SUBMIT_BY,SUBMIT_DT,STATUS_ID,BRANCH_FLAG) VALUES(" & Branch_Id & "," & UserId & ",GETDATE(),1,0)")
                    CallBackReturn = CStr(IntVal)
                End If
            End If
        Catch ex As Exception
          
            Response.Redirect("~/CatchException.aspx?ErrorNo=1")
        End Try

    End Sub
#End Region

End Class
