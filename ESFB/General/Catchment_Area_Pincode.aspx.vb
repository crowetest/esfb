﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Catchment_Area_Pincode
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1429) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return

            End If
            Dim UserId As Integer
            UserId = CInt(Session("UserID"))
            DT = DB.ExecuteDataSet("select b.branch_id as value,b.Branch_name as branch_name  from emp_master a inner join branch_master b on a.Branch_id=b.Branch_id where emp_code=" & UserId & "").Tables(0)
            GF.ComboFill(cmbBranch, DT, 0, 1)
            Dim n As Integer = 0
            'Me.btnSave.Attributes.Add("onclick", "return btnSave_onclick(n)")

            Me.Master.subtitle = "Catchment Area Pincode"
            '--//---------- Script Registrations -----------//--
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            '/--- For Call Back ---//
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    'Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
    '    DT.Dispose()
    '    DT_EMP.Dispose()
    '    DTTS.Dispose()
    '    DB.dispose()
    '    GC.Collect()
    'End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim StrData As String = Nothing

        If CInt(Data(0)) = 1 Then
            Dim BranchId As Integer = CInt(Me.cmbBranch.SelectedValue)

            Dim StringData As String = CStr(Data(1))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            Try
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@BranchId", SqlDbType.Int)
                Params(0).Value = BranchId
                Params(1) = New SqlParameter("@StringData", SqlDbType.VarChar, 5000)
                Params(1).Value = StringData
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_CSQ_PIN_MAPPING", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
        End If
        If CInt(Data(0)) = 2 Then 'document display

            DT = DB.ExecuteDataSet("select a.Location,a.pincode from ESFB.dbo.CSQ_PINCODE_MAPPING a inner join emp_master b on a.BranchId=b.branch_id  where b.emp_code=" & UserID & "").Tables(0)

            For n As Integer = 0 To DT.Rows.Count - 1

                StrData += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString
                'If n <= DT.Rows.Count Then
                If n < DT.Rows.Count - 1 Then
                    StrData += "¥"
                End If
            Next
            CallBackReturn = StrData.ToString
        End If
        If CInt(Data(0)) = 3 Then 'delete entry
            Dim BranchId As Integer = CInt(Me.cmbBranch.SelectedValue)
            Dim checkFlag As Integer = CInt(Data(1))
            Dim pincode As String = CStr(Data(2))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            'DT = DB.ExecuteDataSet("select * from ESFB.dbo.CSQ_PINCODE_MAPPING  where pincode=" & Data(1) & "").Tables(0)
            Try
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@BranchId", SqlDbType.Int)
                Params(0).Value = BranchId
                Params(1) = New SqlParameter("@checkFlag", SqlDbType.Int)
                Params(1).Value = checkFlag
                Params(2) = New SqlParameter("@pincode", SqlDbType.VarChar, 200)
                Params(2).Value = pincode
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_CSQ_PIN_MAPPING", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try

            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
        End If


    End Sub
#End Region
End Class
