﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CSQ_Approval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1350) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim UserID As String = CStr(Session("UserID"))
            Dim BranchID As Integer = CInt(Request.QueryString.Get("BranchID"))
            DT = DB.ExecuteDataSet("SELECT A.BRANCH_ID,B.BRANCH_NAME,A.CREATED_DATE,C.QUARTER_NAME, A.FREQ_ID FROM CSQ_ENTRY A INNER JOIN BRANCH_MASTER B ON B.BRANCH_ID = A.BRANCH_ID INNER JOIN CSQ_FREQ_SETTINGS C ON A.FREQ_ID = C.ID WHERE A.FREQ_ID = (SELECT MAX(ID) FROM CSQ_FREQ_SETTINGS) and A.BRANCH_ID = " + BranchID.ToString()).Tables(0)
            Me.hidEntryData.Value = DT.Rows(0)(0).ToString() + "Ñ" + DT.Rows(0)(1).ToString() + "Ñ" + DT.Rows(0)(2).ToString() + "Ñ" + DT.Rows(0)(3).ToString()
            Dim QS1 As String = ""
            DT1 = DB.ExecuteDataSet("SELECT A.QUESTION_ID,B.QUESTION,A.COMPLAINTS_COUNT,A.REMARKS FROM CSQ_ENTRY C INNER JOIN CSQ_ENTRY_DTL A ON A.ENTRY_ID = C.ENTRY_ID INNER JOIN CSQ_QUESTION_MASTER B ON B.QUESTION_ID = A.QUESTION_ID WHERE C.BRANCH_ID = " + BranchID.ToString() + " AND SECTION_ID = 1 AND B.STATUS_ID = 1 AND C.FREQ_ID = " + DT.Rows(0)(4).ToString() + " ORDER BY A.QUESTION_ID").Tables(0)
            For n As Integer = 0 To DT1.Rows.Count - 1
                QS1 += DT1.Rows(n)(0).ToString() + "ÿ" + DT1.Rows(n)(1).ToString() + "ÿ" + DT1.Rows(n)(2).ToString() + "ÿ" + DT1.Rows(n)(3).ToString() + "Ñ"
            Next
            Me.hidQS1.Value = QS1
            Dim QS2 As String = ""
            DT2 = DB.ExecuteDataSet("SELECT A.QUESTION_ID,B.QUESTION,A.CONFIRM_STATUS FROM CSQ_ENTRY C INNER JOIN CSQ_ENTRY_DTL A ON A.ENTRY_ID = C.ENTRY_ID INNER JOIN CSQ_QUESTION_MASTER B ON B.QUESTION_ID = A.QUESTION_ID WHERE C.BRANCH_ID = " + BranchID.ToString() + " AND SECTION_ID = 2 AND B.STATUS_ID = 1 AND C.FREQ_ID = " + DT.Rows(0)(4).ToString() + " ORDER BY A.QUESTION_ID").Tables(0)
            For m As Integer = 0 To DT2.Rows.Count - 1
                QS2 += DT2.Rows(m)(0).ToString() + "ÿ" + DT2.Rows(m)(1).ToString() + "ÿ" + DT2.Rows(m)(2).ToString() + "Ñ"
            Next
            Me.hidQS2.Value = QS2
            Me.Master.subtitle = "Quaterly Returns On Customer Service"
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", " tablefill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim Branch_ID As Integer = CInt(Data(1).ToString())
            Dim Status_ID As Integer = CInt(Data(2).ToString())
            Dim HO_Remarks As String = Data(3).ToString()
            Dim HO_Verify_By As Integer = CInt(Session("UserID"))
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@Branch_ID", SqlDbType.Int)
                Params(0).Value = Branch_ID
                Params(1) = New SqlParameter("@HO_Verify_By", SqlDbType.Int)
                Params(1).Value = HO_Verify_By
                Params(2) = New SqlParameter("@HO_Remarks", SqlDbType.VarChar, 1000)
                Params(2).Value = HO_Remarks
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@Status_ID", SqlDbType.Int)
                Params(5).Value = Status_ID 'status_id 2 for approve,3 for reject
                DB.ExecuteNonQuery("SP_CSQ_RETURN_SAVE", Params)
                Message = CStr(Params(4).Value)
                ErrorFlag = CInt(Params(3).Value)
            Catch ex As Exception
                Message = Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
        End If
        If CInt(Data(0)) = 2 Then

        End If
        If CInt(Data(0)) = 3 Then

        End If
    End Sub
End Class
