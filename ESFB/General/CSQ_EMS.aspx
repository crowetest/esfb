﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CSQ_EMS.aspx.vb" Inherits="CSQ_EMS" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 25%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var close_len;
        var k = 0;
        
    window.onload = function () {
       k=document.getElementById("<%= hidRowCount.ClientID %>").value;
       if (k==0)
        AddRow(0);
        else 
        {

         TableFill(k);
//        AddRow(k);
        }
    }
    function FromServer(arg, context) {             
        if (context == 1) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0){
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");        
            }        

        }
        else if (context == 2) {
        }
    }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
    function btnSave_onclick() {
        var EMS_His = "";
        var EMSDetails = document.getElementById("<%= hidEMSDetails.ClientID %>").value.split("ÿ");
        var quarter = EMSDetails[3].toString();
        for (var i = 1; i <= close_len; i++) {
            EMS_His += document.getElementById("cmbChannel_" + i).value;
            EMS_His += "µ";
            EMS_His += document.getElementById("customerName_" + i).value;
            EMS_His += "µ";
            EMS_His += document.getElementById("customerAccount_" + i).value;
            EMS_His += "µ";
            EMS_His += document.getElementById("customerMobile_" + i).value;
            EMS_His += "µ";
            EMS_His += document.getElementById("attach_" + i).value;
            EMS_His += "µ";
            EMS_His += document.getElementById("remarks_" + i).value;
            EMS_His += "¥";
            if (document.getElementById("cmbChannel_" + i).value == -1) {
                alert("Select channel");
                return false;
            }
            if (document.getElementById("customerName_" + i).value == "") {
                alert("Enter customer name");
                return false;
            }
            if (document.getElementById("customerAccount_" + i).value == "") {
                alert("Enter customer account number");
                return false;
            }
            if (document.getElementById("customerMobile_" + i).value == "") {
                alert("Enter customer mobile number");
                return false;
            }
            if (document.getElementById("attach_" + i).value == "") {
                alert("Add attachment");
                return false;
            }
        }
        var ToData = "1Ø" + EMS_His + "Ø" + quarter;
	    ToServer(ToData, 1);
    }
    function TableFill(j){
        var CloseData = "";
        document.getElementById("<%= pnlEMS.ClientID %>").style.display = '';
        var row_bg = 0;
        var tab = "";            
        tab += "<div style='width:85%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'>";
        tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center' id='ClosureTable'>";
        tab += "<tr class=mainhead style='height:35px;'>";
        tab += "<td style='width:5%;text-align:center'>Sl.No</td>";
        tab += "<td style='width:15%;text-align:center'>Channel</td>";
        tab += "<td style='width:15%;text-align:center'>Customer Name</td>";
        tab += "<td style='width:15%;text-align:center' >Customer Account Number</td>";
        tab += "<td style='width:15%;text-align:center;'>Customer Mobile Number</td>";
        tab += "<td style='width:20%;text-align:center;'>Remarks</td>";
        tab += "<td style='width:10%;text-align:center;' >Attachment</td>";
        tab += "<td style='width:5%;text-align:center;'></td>";
        tab += "</tr>";
        for(var k=1;k<=j;k++){
            tab += "<tr class=sub_first style ='height:30px;'>";
            tab += "<td style='width:5%;text-align:center'>" + k + "</td>";
            var select = "<select id='cmbChannel_" + k + "' class='NormalText' name='cmbChannel_" + k + "' style='width:99%;height:90%'>";
            tab += "<td style='width:15%;text-align:center'>" + select + "</td>";
            tab += "<td style='width:15%;text-align:center'><input type='text' id = 'customerName_" + k + "' style='width:95%;height:90%;text-align:left;' onkeypress='AlphaCheck(event)'></input></td>";
            tab += "<td style='width:15%;text-align:center'><input type='text' id = 'customerAccount_" + k + "' style='width:95%;height:90%;text-align:left;' maxlength='20' onkeypress='NumericCheck(event)'></input></td>";
            tab += "<td style='width:15%;text-align:center'><input type='text' id = 'customerMobile_" + k + "' style='width:95%;height:90%;text-align:left;' maxlength='10' onkeypress='NumericCheck(event)'></input></td>";
            tab += "<td style='width:20%;text-align:center'><input type='text' id = 'remarks_" + k + "' style='width:95%;height:90%;text-align:left;' maxlength='500'></input></td>";
            var button = "<input type='button' id='btnAttach_" + k + "' onclick ='return AddAttachment(" + k + ");' value='Add Attachment'>  ";
            button += "<input type='button' id='btnEdit_" + k + "' onclick ='return AddAttachment(" + k + ");' value='Edit' style='width:40%;display:none;'>  ";
            button += "<input type='text' id='attach_"+k+"' style='display:none;'></input>";
            tab += "<td style='width:10%;text-align:center'>" + button + "</td>";
            tab += "<td style='width:5%;text-align:center' onclick=AddRow("+k+")><img id='Add"+k+"' src='../Image/addd1.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;display:none;' title='ADD'/></td>";
            tab += "</tr>";
        }
        close_len = j; 
        tab += "</table></div>";
        document.getElementById("<%= pnlEMS.ClientID %>").innerHTML = tab;  
        document.getElementById("Add"+j).style.display = '';
        for(var k=1;k<=j;k++){
            var channel = document.getElementById("<%= hidChannel.ClientID %>").value;
            ComboFill(channel,"cmbChannel_"+k);
        }
        try{
            var rowData = document.getElementById("<%= hdnEMSHis.ClientID %>").value.split("¥");
            for(var i = 1;i<=rowData.length;i++){
                var cData = rowData[i].toString();
                var closeData = cData.split("µ");
                    document.getElementById("cmbChannel_" + i).value = closeData[0];
                    document.getElementById("customerName_" + i).value = closeData[1];
                    document.getElementById("customerAccount_" + i).value = closeData[2];
                    document.getElementById("customerMobile_" + i).value = closeData[3];
                    document.getElementById("remarks_" + i).value = closeData[7];
                    var attachData = closeData[4] + "µ" + closeData[5] + "µ" + closeData[6];
                    document.getElementById("attach_" + i).value = attachData;
                    if (attachData != "") {
                        document.getElementById("btnAttach_" + i).style.display = "none";
                        document.getElementById("btnEdit_" + i).style.display = "";
                    }
            }
        }catch(e){} 
        var QDetails = document.getElementById("<%= hidEMSDetails.ClientID %>").value.split("ÿ"); 
        if (QDetails[5] == 0){
            document.getElementById("<%= pnlEMS.ClientID %>").style.display = 'none';
        }
        
//        var dataHis = document.getElementById("<%= hidDataHis.ClientID %>").value;     
////        if(dataHis[0] == '0'){
//            document.getElementById("<%= pnlEMS.ClientID %>").innerHTML = "<p align='center'>Already submitted for this quarter</p>";
//            document.getElementById("btnSave").style.display = "none";
////            document.getElementById("<%= pnlEMS.ClientID %>").style.display = 'none';
//        }
    }
        function AddAttachment(x) {
                    var win = window.open("<%= Page.ResolveUrl("~")%>General/CSQ_EMS_ATTACH.aspx", "_blank", "Height=400px,Width:100px,Left=300,Top=300,center:yes");
            k = x;
        }
        function setValue(x) {
            document.getElementById("attach_" + k).value = x;
            if (document.getElementById("attach_" + k).value != "") {
                document.getElementById("btnAttach_" + k).style.display = "none";
                document.getElementById("btnEdit_" + k).style.display = "";
            }
        }
    function AddRow(i){
        if(i!=0){            
            if( document.getElementById("cmbChannel_"+i).value == -1){
                alert("Select Channel");
                return false;
            }else if( document.getElementById("customerName_"+i).value == ""){
                alert("Enter Customer Name");
                return false;
            }else if( document.getElementById("customerAccount_"+i).value == ""){
                alert("Enter Customer Account Number");
                return false;
            }else if( document.getElementById("customerMobile_"+i).value == ""){
                alert("Enter Customer Mobile Number");
                return false;
            } else if (document.getElementById("attach_" + i).value == "") {
                alert("Add Attachment");
                return false;
            } else {
                updateValue(i);
                TableFill(i + 1);
            }
        }
        else{            
            updateValue(i);
            TableFill(i+1);
        }
    }
    function updateValue(id){
        row = document.getElementById("<%= hdnEMSHis.ClientID %>").value.split("¥");
        var NewStr=""
        for (i = 1; i <= id ; i++) {
            var channelID = document.getElementById("cmbChannel_" + i).value;
            var customerName = document.getElementById("customerName_" + i).value;
            var customerAccount = document.getElementById("customerAccount_" + i).value;
            var customerMobile = document.getElementById("customerMobile_" + i).value;
            var remarks = document.getElementById("remarks_" + i).value;
            var Attach = "";
            if (i == 0) {
                Attach = "";
            } else {
                Attach = document.getElementById("attach_" + i).value;
            }
            NewStr += "¥" + channelID + "µ" + customerName + "µ" + customerAccount + "µ" + customerMobile + "µ" + Attach + "µ" + remarks;
        }
        document.getElementById("<%= hdnEMSHis.ClientID %>").value=NewStr;
    }
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 0; a < rows.length-1; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function ClearCombo(control) {
        document.getElementById(control).options.length = 0;
        var option1 = document.createElement("OPTION");
        option1.value = -1;
        option1.text = " -----Select-----";
        document.getElementById(control).add(option1);
    }
    function AlphaCheck(e)//------------function to check whether a value is alpha
    {
        if (/[a-zA-Z. ]/.test(e.key)) {

        } else {
            e.preventDefault();
        }
    }
</script>
   
</head>
</html>
<br />
<br />
<br
 <table class="style1" style="width:80%;margin: 0px auto;">
    <tr> 
        <td style="width:10%;">
            <asp:HiddenField ID="hidData" runat="server" />
            <asp:HiddenField ID="hidChannel" runat="server" />
            <asp:HiddenField ID="hidEMSData" runat="server" />
            <asp:HiddenField ID="hdnEMSHis" runat="server" />
            <asp:HiddenField ID="hidEMSDetails" runat="server" />
            <asp:HiddenField ID="hidDataHis" runat="server" />
            <asp:HiddenField ID="hidSaveData" runat="server" />
            <asp:HiddenField ID="hidRowCount" runat="server" />
        </td>
        <td style="width:80%; text-align:left;">
        </td>
        <td style="width:10%">
        </td>
    </tr>
    <tr>
        <td style="width:10%;"></td>
        <td style="width:80%;">
            <asp:Panel ID="pnlEMS" runat="server">
            </asp:Panel>
        </td>
        <td style="width:10%;"></td>
    </tr>    
</table>
<table class="style1" style="width:80%;margin: 0px auto;">   
    <tr>
        <td style="width:100%;" colspan="4">
        </td>
    </tr>
    <tr>
        <td style="text-align:center;" colspan="4">
            <br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="SAVE" onclick="return btnSave_onclick()"/>&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
        </td>
    </tr>
</table> 

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

