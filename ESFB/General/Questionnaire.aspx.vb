﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class Questionnaire
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim DT As New DataTable
    Dim SINo As Integer
    Dim GMASTER As New Master
    Dim UserID As Integer
    Dim BranchID As Integer
    Dim AdminFlag As Integer = 0
    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing

#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            Me.Master.subtitle = "BRANCH RESPONSE"
            UserID = CInt(Session("UserID"))
            BranchID = CInt(Session("BranchID"))
            DT = GMASTER.GetEmpRoleList(UserID)
            For Each DR In DT.Rows
                If DR(2) = 26 Then
                    AdminFlag = 1
                    Exit For
                End If
            Next

            If Not IsPostBack Then

                If AdminFlag = 0 Then
                    strWhere = " And branch_id = " & BranchID & ""
                    DT = DB.ExecuteDataSet("select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id = 1  " & strWhere & " and  Branch_ID =" & BranchID & " order by 2").Tables(0)

                Else
                    DT = DB.ExecuteDataSet("select '-11' as branchid,' ---------------ALL---------------' as branch Union All select branch_id as branchid,upper(Branch_Name) as branch from BRANCH_MASTER where status_id = 1  and -1 < Branch_ID and Branch_ID < 50000  order by 2").Tables(0)

                End If
                GN.ComboFill(cmbBranch, DT, 0, 1)
                
                If AdminFlag = 1 Then
                    DT = DB.ExecuteDataSet("SELECT '-1' AS Questionid,'--------------- ALL QUESTIONS ---------------' AS Question_Dt1 UNION ALL Select Question_id,convert (varchar(18),Question_Date ,106) + '   |   '  + Question  from Question_master").Tables(0)
                    GN.ComboFill(cmbQuestion, DT, 0, 1)

                Else
                    Me.cmbQuestion.Visible = False

                End If

            End If

                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
                Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

                '/--- Register Client Side Functions ---//
                If AdminFlag = 1 Then
                    Me.cmbBranch.Attributes.Add("onchange", "return  BranchOnChange(1)")
                    Me.txtFromDt.Attributes.Add("onchange", "return  FromDtOnChange(1)")
                    Me.txtToDt.Attributes.Add("onchange", "return  ToDtOnChange(1)")
                    Me.cmbQuestion.Attributes.Add("onchange", "return  QuestionOnChange(1)")
                Else
                    Me.cmbBranch.Attributes.Add("onchange", "return  BranchOnChange(0)")
                    Me.txtFromDt.Attributes.Add("onchange", "return  FromDtOnChange(0)")
                Me.txtToDt.Attributes.Add("onchange", "return  ToDtOnChange(0)")
                Me.cmbQuestion.Attributes.Add("onchange", "return  QuestionOnChange(0)")
                End If

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try


    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent


        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 2 Then
            Dim BranchID As Integer = Data(1)
            Dim QuestionDtl As String = Data(2)
            If QuestionDtl <> "" Then
                QuestionDtl = QuestionDtl.Substring(1)
            End If
          
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(0).Value = BranchID

                Params(1) = New SqlParameter("@QuestionDtl", SqlDbType.VarChar, 4000)
                Params(1).Value = QuestionDtl
               
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_QUESTIONNAIRE", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 1 Then
            DT = New DataTable
            Dim Sql As String
            Dim StrWhr As String
            Dim StrWhre As String
            Dim BranchID As String = Data(1).ToString()
            Dim FromDt As String = Data(2).ToString()
            Dim ToDt As String = Data(3).ToString()
            Dim QuestionID As String = Data(4).ToString()
            StrWhr = " And 1 = 1 "
            StrWhre = " And 2 = 2 "
            ''CallBackReturn = "~"
            ''Sql = "  select  Question_ID FROM Question_master QM WHERE  QM.Question_Date between CONVERT(DATE,    '" & FromDt & "' ,105)  and  CONVERT(DATE,    '" & ToDt & "' ,105)"

            ''DT = GN.GetQueryResult(Sql)
            ''For Each DR In DT.Rows
            ''    CallBackReturn += DR(0).ToString()
            ''Next

            CallBackReturn += "~"
            ' Sql = "select QM.question,QE.Branch_ID from Question_entry QE inner join Question_Master QM on QE.Question_ID=QM.Question_id WHERE  QM.Question_Date between  CONVERT(DATE,    '" & FromDt & "' ,105)  and  CONVERT(DATE,    '" & ToDt & "' ,105)"
            If BranchID <> -11 Then
                StrWhr += " AND QB.Branch_ID = " & BranchID
            End If
            If QuestionID <> -1 Then
                StrWhre += "  and QB.question_id=  " & QuestionID
            End If
            Sql = "SELECT QB.Branch_name,QB.question,isnull(CONVERT (varchar(11),QE.Qstn_entry_Date,105),GETDATE()) as Response_Date,isnull(QE.Entry,-1) AS Entry,isnull(Qe.Remarks,'') AS Remarks,QB.Question_ID FROM (SELECT * FROM Question_Master QM,  Branch_master BM) QB LEFT JOIN Question_entry QE ON QB.Branch_id=QE.Branch_id and QE.Question_ID=QB.Question_id where QB.status_id = 1 and QB.Question_Date between CONVERT(DATE,    '" & FromDt & "' ,105)  and  CONVERT(DATE,    '" & ToDt & "' ,105) and -1 < QB.Branch_ID and QB.Branch_ID < 50000 " + StrWhr + StrWhre
            'Sql = "SELECT QB.Branch_name,QB.question,QB.Question_Date,isnull(QE.Entry,-1) AS Entry,isnull(Qe.Remarks,'') AS Remarks,QB.Question_ID FROM (SELECT * FROM Question_Master QM,  Branch_master BM)QB LEFT JOIN Question_entry QE ON QB.Branch_id=QE.Branch_id and QE.Question_ID=QB.Question_id where QB.status_id = 1  and -1 < QB.Branch_ID and QB.Branch_ID < 50000 " + StrWhr
            DT = GN.GetQueryResult(Sql)
            For Each DR In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString()
            Next
        End If
    End Sub

#End Region


End Class

