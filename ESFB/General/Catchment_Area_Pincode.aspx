﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Catchment_Area_Pincode.aspx.vb" Inherits="Catchment_Area_Pincode" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 25%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    var close_len;
    var br_len;
    window.onload = function()
    {
         AddRow(0);
         PinDisplay();
    }
    function PinDisplay()
    {
    var ToData = "2Ø";
    ToServer(ToData,2);


    }
    function btnSave_onclick()
    {
        var br_len=document.getElementById("<%= hidData.ClientID %>").value;
        var newstrExp="";
         var Br_His = "";

        for(var i = 1; i <= br_len; i++)
        {
            var Location=document.getElementById("txtLoc"+i).value;
            var Pincode=document.getElementById("txtPin"+i).value;
            newstrExp+=Location+"µ"+Pincode+"¥";
            var ToData = "1Ø" + newstrExp;
        }
        ToServer(ToData,1);

    }
    function FromServer(arg, context) 
    {             
        if (context == 1) 
        {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0)
            {
//                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self"); 
                            window.open("Catchment_Area_Pincode.aspx", "_self"); 
       
            }        
        }
        else if (context == 2)
        {    
            btnDisplay(arg);
        }
                else if (context == 3)
        {    
         
            alert("Deleted Successfully");
            window.open("Catchment_Area_Pincode.aspx", "_self"); 
        }

    }
      function Delete_onclick(pid) {
    document.getElementById("<%= hid_pkid.ClientID %>").value =pid;
    var checkFlag=1;
    todata = "3Ø"+ checkFlag + "Ø"+ pid ;
    ToServer(todata,3);
        }
        function btnDisplay(arg) {

        document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
        document.getElementById("<%= hidStringData.ClientID %>").value=arg;
         var row_bg = 0;
            var tab = "";
            tab += "<div style='width:60%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:60%;margin:0px auto;font-family:'cambria';' align='center'>";
        if (document.getElementById("<%= hidStringData.ClientID %>").value != "") {
            row1 = document.getElementById("<%= hidStringData.ClientID %>").value.split("¥");
            var ShowQuali = 0;
            for (m = 0; m <= row1.length - 1; m++) {
                col2 = row1[m].split("µ");
                if (ShowQuali == 0) {

                    tab += "<tr><td style='width:10%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                    tab += "<td style='width:20%;height:10px; text-align:left;font-weight:bold;' >Location</td>";
                    tab += "<td style='width:40%;height:10px; text-align:left;font-weight:bold;' >Pincode</td></tr>";
                    ShowQuali = 1;

                }

                tab += "<tr><td style='width:10%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                    tab += "<td style='width:20%;height:20px; text-align:left;' ><a style='width:50%;'  style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[0] + "</a></td>";
                    tab += "<td style='width:40%;height:20px; text-align:left;' ><a style='width:50%;'  style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[1] + "</a></td>";           
                tab +=" &nbsp &nbsp &nbsp<td style = 'width:5%;'><br><input  type ='button' onclick='Delete_onclick(\""+ col2[0].toString() + "\")' id ='btnExpand' value='Delete'></td></tr>";
            }

            tab += "<tr><td colspan='3'></td></tr>";


        }
        tab += "</table></div>";
         document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
    }

    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
    function NumericCheck(evt)
    { 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        { 
            return false; 
        }
        return true; 
    } 
     function DeleteRow(i)
     {
            row = document.getElementById("<%= hdnBranchHis.ClientID %>").value.split("¥");
            var NewStr=""
            for (n = 1; n <= row.length ; n++) 
            {
                if(i!=n)
                {
                    try
                    {
                        
                    var Location = document.getElementById("txtLoc"+n).value;
                    var Pincode = document.getElementById("txtPin"+n).value;
                    NewStr+="¥"+Location+"µ"+Pincode;        
                    }
                    catch(e)
                    {
                    }
                }
                else
                {
                    NewStr+="¥"+row[n];
                }
            }
            document.getElementById("<%= hdnBranchHis.ClientID %>").value=NewStr;
            TableFill(row.length-1);
        }
      function updateValue(id)
      {
            row = document.getElementById("<%= hdnBranchHis.ClientID %>").value.split("¥");
            var NewStr=""
            for (n = 1; n <= id ; n++)
             {
                if(id==n)
                {
                    var Location = document.getElementById("txtLoc"+n).value;
                    var Pincode = document.getElementById("txtPin"+n).value;
                    NewStr+="¥"+Location+"µ"+Pincode;
                }
                else
                {                    
                    NewStr+="¥"+row[n];
                }
            }
            document.getElementById("<%= hdnBranchHis.ClientID %>").value=NewStr;
        }
        function AddRow(i)
        {
            if(i!=0)
            {
                if( document.getElementById("txtLoc"+i).value !='')
                {
                    updateValue(i);
                    TableFill(i+1);
                }
            }
            else
            {
                updateValue(i);
                TableFill(i+1);
            }
        }


       function TableFill(j)
       {
            document.getElementById("<%= pnlBranchHistory.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:95%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'>";
            tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center' id='BranchHistory'>";
            tab += "<tr class=mainhead>";
            tab += "<td style='width:7%;text-align:center'>Sl.No</td>";
            tab += "<td style='width:24%;text-align:center;'>Location</td>";
            tab += "<td style='width:24%;text-align:center;'>Pincode</td>";
            tab += "<td style='width:5%;'></td>";
            tab += "<td style='width:5%;'></td>";
            tab += "</tr>";
            for(var k=1;k<=j;k++){
                tab += "<tr class=sub_first style ='height:20px;'>";
                tab += "<td style='width:7%;text-align:center'>"+k+"</td>";
                tab += "<td style='width:24%;text-align:center;'><input type='text' name='txtLoc"+k+"' id='txtLoc"+k+"' type='Text' style='width:99%;' class='NormalText' onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/></td>";
                tab += "<td style='width:24%;text-align:center;' ><input type='text' name='txtPin"+k+"' id='txtPin"+k+"' style='width:99%;' class='NormalText'onkeypress='return NumericCheck(event)' maxlength = '6'></td>";
                tab += "<td style='width:5%;text-align:center' onclick=AddRow("+k+")><img id='Add"+k+"' src='../Image/addd1.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;display:none;' title='ADD'/></td>"
                tab += "<td style='width:5%;text-align:center' onclick=DeleteRow("+k+")><img id='Delete"+k+"' src='../Image/delete.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='DELETE'/></td>"
                tab += "</tr>";
                document.getElementById("<%= hidData.ClientID %>").value=k;
            }
            br_len = j; 
            tab += "</table></div>";
            document.getElementById("<%= pnlBranchHistory.ClientID %>").innerHTML = tab;
            document.getElementById("Add"+j).style.display = '';
            if(j==1)
            {
                document.getElementById("Delete1").style.display = 'none';
            }
           
            try
            {
                var rowData = document.getElementById("<%= hdnBranchHis.ClientID %>").value.split("¥");
                for(var i = 1;i<=rowData.length;i++)
                {
                    var bData = rowData[i].toString();
                    var branchData = bData.split("µ");
                    for(var j = 1;j<=branchData.length;j++)
                    {
                        document.getElementById("txtLoc"+i).value = branchData[0];
                        document.getElementById("txtPin"+i).value = branchData[1];
                    }
                }
            }catch(e){}
        }
    function ComboFill(data, ddlName) 
    {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 0; a < rows.length-1; a++) 
        {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
   
</script>
   
</head>
</html>
<br />
<br />
<div style="width:60%; height:auto; overflow:auto;margin: 0px auto;" class="mainhead">
 <table class="style1" style="width:80%;margin: 0px auto;"> 
        <tr>
         <td style="width: 20%; text-align: right;">
                Branch Code&nbsp;&nbsp;<b style="color: red;">*</b>
            </td>
        <td> <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="80%" ForeColor="Black">
                        </asp:DropDownList></td>
        <tr style="text-align:center">
        <td  colspan="4" style="text-align:center;">
            <asp:Panel ID="pnlBranchHistory" runat="server">
            </asp:Panel>
            </td>
        </tr>

</table>
</div>
<table class="style1" style="width:80%;margin: 0px auto;">   
<%--    <tr>
        <td style="width:100%;" colspan="4">
        </td>
    </tr>--%>
    <tr>
        <td style="text-align:center;" colspan="4">
            <br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="SAVE" onclick="return btnSave_onclick()"/>&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
        </td>
    </tr>
      <asp:Panel ID="pnlDtls" runat="server"></asp:Panel>

      <tr> 
        <td style="width:10%;">
            <asp:HiddenField ID="hidData" runat="server" />
            <asp:HiddenField ID="hdnBranchHis" runat="server" />
            <asp:HiddenField ID="hidCloseData" runat="server" />
            <asp:HiddenField ID="hid_pkid" runat="server" />
            <asp:HiddenField ID="hid_Display" runat="server" />
            <asp:HiddenField ID="hidStringData" runat="server" />
             <asp:HiddenField ID="hdnExperience" runat="server" />
        </td>
        <td style="width:80%; text-align:left;">
        </td>
        <td style="width:10%">
        </td>
    </tr>
</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

