﻿Imports System.Data
Partial Class Leave_ShowVetFormat
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim MeetingID As Integer = CInt(GF.Decrypt(Request.QueryString.[Get]("MeetingID")))
            DT = DB.ExecuteDataSet("select ATTACHMENT,CONTENT_TYPE,isnull(case when ATTACH_FILE_NAME='' then 'Attachment' else ATTACH_FILE_NAME end,'Attachment') FileName from DMS_ESFB.dbo.MEETING_ATTACH where Meeting_ID=" & MeetingID & "").Tables(0)
            If DT IsNot Nothing Then
                Dim bytes() As Byte = CType(DT.Rows(0)(0), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = DT.Rows(0)(1).ToString()
                Response.AddHeader("content-disposition", "attachment;filename=" + DT.Rows(0)(2).ToString.Replace(" ", ""))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
