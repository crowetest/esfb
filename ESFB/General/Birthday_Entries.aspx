﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Birthday_Entries.aspx.vb" Inherits="Birthday_Entries" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
<head>
<title></title>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
     <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">



        function SubmitOnClick() {
            if (document.getElementById("<%= hid_Submit.ClientID %>").value==1) {
                alert("Birthday details already submitted. If you need any updation contact HR Team(Mobile No.+919349030252)");
                return false;
            }
            else {
                var Conf = confirm("Unless and until all the corrections of DOB are completed do not submit .Do you want to continue ?");
                if (Conf == true) {
                    Conf = confirm("I hereby declare that the information furnished above is true to the best of my knowledge.");
                    if (Conf == true) {
                        ToServer("3Ø", 3); 
                    }
                }
            }
        }


        function SaveOnClick() {
            if(document.getElementById("<%= txtEmpId.ClientID %>").value==""){
                alert("Select Employee");
                return false;
            }
            else {
                var EmpCode = document.getElementById("<%= txtEmpId.ClientID %>").value;
                var DOB = document.getElementById("<%= txtDOB.ClientID %>").value;
                ToServer("2Ø" + EmpCode + "Ø" + DOB, 2);
            }
        }




        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Portal.aspx", "_self");
        }


        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("~");
                document.getElementById("<%= txtEmpName.ClientID %>").value = Data[1];
                document.getElementById("<%= txtDOB.ClientID %>").value = Data[2];
            }
            else if (context == 2) {
                if (arg==1 ){
                    alert("Saved Successfully !");
                    window.open("Birthday_Entries.aspx", "_self");
                }
            }
            else if (context == 3) {
                if (arg==1 ){
                    alert("Submitted Successfully !");
                    window.open("<%= Page.ResolveUrl("~")%>Portal.aspx", "_self");
                    return false;
                }
            }
        }



        function DOBOnChange(){
            var dates = document.getElementById("<%=txtDOB.ClientID%>").value.split(" ");
            var userday = dates[0];
            var usermonth = dates[1];
            var useryear = dates[2];
            var dob = new Date(useryear+"/"+usermonth+"/"+userday)  
            // dob year
            var dobYear = dob.getFullYear();
            // dob month
            var dobMonth = dob.getMonth() + 1;
            //dob date
            var dobDate = dob.getDate();

	        //today
            //date today string format 
            var today = new Date(); // i.e wed 04 may 2016 15:12:09 GMT
            //todays year
            var todayYear = today.getFullYear();
            // today month
            var todayMonth = today.getMonth();
            //today date
            var todayDate = today.getDate();           

            var yearsDiff = todayYear - dobYear ;
            var age;
            var yearsDiff = todayYear - dobYear ;
            var age;
            if ( todayMonth < dobMonth ) 
            { 
                age = yearsDiff - 1; 
            }
            else if ( todayMonth > dobMonth ) 
            {
                age = yearsDiff ; 
            }
            var CurAge = age;
            if(CurAge < 18 ){
                alert("Date Of Birth is not correct"); 
                document.getElementById("<%= txtDOB.ClientID %>").value="";
            }
        }



        function edit_row(EmpID) {
            if (document.getElementById("<%= hid_Submit.ClientID %>").value==1)
                alert("Birthday details already submitted. If you need any updation contact HR Team(Mobile No.+919349030252)");
            else {
                document.getElementById("<%= txtEmpId.ClientID %>").value = EmpID;
                ToServer("1Ø" + EmpID, 1);
            }

        }




        function table_fill() {
            document.getElementById("<%= pnlDisplay.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";

            tab += "<div style='width:70%;background-color:#A34747; height:auto; overflow:auto; margin: 0px auto;' align='center'><table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
            tab += "<tr height=100px;  class='mainhead'>";
            tab += "<td style='width:5%;text-align:center' ></td>";
            tab += "<td style='width:10%;text-align:center' >Employee Code</td>";
            tab += "<td style='width:60%;text-align:center' >Employee Name</td>";
            tab += "<td style='width:20%;text-align:center' >DOB</td>";
            tab += "<td style='width:5%;text-align:center' ></td>";
            tab += "</tr>";
            var row = document.getElementById("<%= hid_Value.ClientID %>").value.split("¥"); 
            var i=0;
            for (n = 0; n <= row.length - 2; n++) {
                var col = row[n].split("~");
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr  class='sub_first';>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class='sub_second';>";
                }
                i=i+1;
                tab += "<td style='width:5%;height:5%;text-align:center' >" + i + "</td>";
                tab += "<td style='width:10%;text-align:left' >" + col[0] + "</td>";
                tab += "<td style='width:60%;text-align:left' >" + col[1] + "</td>";
                tab += "<td style='width:20%;text-align:left' >" + col[2] + "</td>";
                tab += "<td style='width:5%;text-align:center' onclick=edit_row(" + col[0] + ")><img  src='../Image/Edit.png' style='align:middle;cursor:pointer; height:20px; width:20px ;'title='Edit' /></td>";                 
                    
                tab += "</tr>";
            }
            document.getElementById("<%= pnlDisplay.ClientID %>").innerHTML = tab;
        }
    </script>
</head>
 <table style="width: 100%;margin: 0px auto">
    <tr>
        <td style="width:10%;"></td>
        <td style="width:10%;"></td>
        <td style="width:10%; text-align: left;font-family:Cambria;">Branch Code</td>
        <td style="width:30%;">
            &nbsp;&nbsp;<asp:TextBox ID="txtBranchId" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="40%" MaxLength="50" disabled="true"  /></td>
        <td style="width:10%; text-align: left;font-family:Cambria;">Employee Code</td>
        <td  style="width:30%;">
            <asp:TextBox ID="txtEmpId" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="40%" MaxLength="50" disabled="true"    />
        </td>
        
    </tr>
    <tr>
        <td style="width:10%;"></td>
        <td style="width:10%;"></td>
        <td style="width:10%; text-align: left;font-family:Cambria;">
            Branch Name</td>
        <td style="width:30%;">
            &nbsp;&nbsp;<asp:TextBox ID="txtBranch" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="40%" MaxLength="50" disabled="true"   />
        </td>
        <td style="width:10%; text-align: left;font-family:Cambria;">Employee Name</td>
        <td  style="width:30%;">
            <asp:TextBox ID="txtEmpName" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="40%" MaxLength="50" disabled="true"   />
        </td>
        
    </tr>
    
    <tr>
        <td style="width:10%;"></td>
        <td style="width:10%;"></td>
        <td style="width:10%;"></td>
        <td style="width:30%;"></td>
        <td style="width:10%; text-align: left;font-family:Cambria;">
            DOB</td>
        <td style="width:30%">
            <asp:TextBox ID="txtDOB" runat="server" class="NormalText" Width="25%" ReadOnly="true"   ></asp:TextBox> 
            <asp:CalendarExtender ID="txtDOB_CalendarExtender" runat="server" TargetControlID="txtDOB"  
                    Format="dd MMM yyyy"></asp:CalendarExtender>
        </td>
        
    </tr>
    <tr>
        <td colspan="6" style="width:100%; height: 12px;"></td>
    </tr>
    <tr>
        <td style="width:10%;"></td>
        <td style="text-align: center; font-family:Cambria;" colspan="5">
           <input id="btnSave" style="font-family: cambria; cursor: pointer; width:6%; height: 25px;" 
                type="button" value="SAVE"  onclick="return SaveOnClick()" />                 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%; height: 25px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
            <asp:HiddenField ID="hid_Value" runat="server" />
        </td>
        
    </tr>
     <tr>
        <td colspan="6" style="width:100%; height: 12px;"></td>
    </tr>
    <tr>
        <td style="width:10%;"></td>
        <td colspan="5" ">
            <asp:Panel ID="pnlDisplay" runat="server">
            </asp:Panel>
        </td>
        
    </tr>
    <tr>
        <td colspan="6" style="width:100%; height: 12px;"></td>
    </tr>
    <tr>
        <td style="text-align: center; font-family:Cambria;" colspan="6">
           <input id="Button1" style="font-family: cambria; cursor: pointer; width:6%; height: 25px;" 
                type="button" value="SUBMIT"  onclick="return SubmitOnClick()" />
           <asp:HiddenField ID="hid_Submit" runat="server" />  
        </td>
        
    </tr>
   
    

 </table>
</asp:Content>

