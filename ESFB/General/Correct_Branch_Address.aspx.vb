﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Correct_Branch_Address
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim BranchID As Integer

#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Update Branch Address"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            BranchID = CInt(Me.Session("BranchID"))
            Me.hdnBranch.Value = CStr(BranchID)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Branch Details 
                BranchID = CInt(Data(1))
                DT = GN.GetQueryResult("select a.BRANCH_ID,a.BRANCH_NAME,r.Region_Name,d.District_Name,s.State_Name,a.State_Abbr,a.ADD1,a.ADD2,a.ADD3,a.CITY, a.PIN from branch_master a, region_master r, DISTRICT_MASTER d,STATE_MASTER s where a.Region_ID=r.Region_ID and a.District_ID=d.District_ID and a.State_ID=s.State_ID and a.Update_ID=0 and a.BRANCH_ID=" & BranchID & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "µ" + DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + DT.Rows(0)(7).ToString() + "µ" + DT.Rows(0)(8).ToString() + "µ" + DT.Rows(0)(9).ToString() + "µ" + DT.Rows(0)(10).ToString()
                Else
                    CallBackReturn += "Œ"
                End If
            Case 2
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                BranchID = CInt(Data(1))
                Dim Add1 As String = CStr(Data(2))
                Dim Add2 As String = CStr(Data(3))
                Dim Add3 As String = CStr(Data(4))
                Dim City As String = CStr(Data(5))
                Dim Pin As String = CStr(Data(6))
                Try
                    Dim Params(7) As SqlParameter
                    Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
                    Params(0).Value = BranchID
                    Params(1) = New SqlParameter("@Add1", SqlDbType.VarChar, 100)
                    Params(1).Value = Add1
                    Params(2) = New SqlParameter("@Add2", SqlDbType.VarChar, 100)
                    Params(2).Value = Add2
                    Params(3) = New SqlParameter("@Add3", SqlDbType.VarChar, 100)
                    Params(3).Value = Add3
                    Params(4) = New SqlParameter("@City", SqlDbType.VarChar, 100)
                    Params(4).Value = City
                    Params(5) = New SqlParameter("@Pin", SqlDbType.VarChar, 100)
                    Params(5).Value = Pin
                    Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 250)
                    Params(7).Direction = ParameterDirection.Output

                    DB.ExecuteNonQuery("SP_BRANCH_DETAILS_UPDATE", Params)
                    ErrorFlag = CInt(Params(6).Value)
                    Message = CStr(Params(7).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region

End Class
