﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BRDRequestApproval
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If GF.FormAccess(CInt(Session("UserID")), 1318) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//

            DT = DB.ExecuteDataSet("SELECT DedupeID, replace(convert(char(11),NewCif_CreatedDt,113),' ','-'), New_Cif,replace(convert(char(11),DupeCif_CreatedDt,113),' ','-'),Dupe_Cif,MakerN,replace(convert(char(11),MakerNDt,113),' ','-'),CheckerN,replace(convert(char(11),CheckerNDt,113),' ','-') FROM dedupe_cust_verification where MakerN is not null and MakerNDt is not null and CheckerN is not null and CheckerNDt is not null and Branch is null and BranchDt is null and NewCif_brcode=" & CInt(Session("BranchID")) & " and Status_ID=2").Tables(0)

            Me.Master.subtitle = "Branch Dedupe Verification"

            Dim StrDtl As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrDtl += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString
                If n < DT.Rows.Count - 1 Then
                    StrDtl += "¥"
                End If
            Next

            hid_dtls.Value = StrDtl
    
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

End Class
