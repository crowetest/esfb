﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="SearchCustomer.aspx.vb" Inherits="SearchCustomer" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../Style/Style.css" />
    <title></title>   
</head>
</html> 

<br />

<div>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
</div>     
            
<div align="center">
<table align="center" >
    

    <tr>
        <td  > 
            <asp:Label ID="lblCustomerName" cssclass="labelstyle1" runat="server" Text="Customer Name"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtCustomerName" cssclass="Txtstyle2" runat="server" ></asp:TextBox>
        </td>
        <td></td>
        <td > 
        <asp:Label ID="lblMobileNo" cssclass="labelstyle1" runat="server" Text="Mobile Number"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtMobile"  cssclass="Txtstyle2" runat="server" ></asp:TextBox>
        </td>                 
    </tr>       
      
    <tr>
        <td class="style2" > 
        <asp:Label ID="lblAadhaarNumber" cssclass="labelstyle1" runat="server" Text="Aadhaar Number"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtAadhaarNumber"  cssclass="Txtstyle2" runat="server" ></asp:TextBox></td>
        <td></td>   <td > 

        <asp:Label ID="lblPanNumber" cssclass="labelstyle1" runat="server" Text="Pan Number"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtPanNumber"  cssclass="Txtstyle2" runat="server" ></asp:TextBox></td>
    </tr>      

    <tr>
        <td class="style2" > 
        <asp:Label ID="lblDrivingLicense" cssclass="labelstyle1" runat="server" Text="Driving License"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtDrivingLicense"  cssclass="Txtstyle2" runat="server" ></asp:TextBox></td>
        <td></td>     <td > 
        <asp:Label ID="lblVotersID" cssclass="labelstyle1" runat="server" Text="Voters ID"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtVotersID"  cssclass="Txtstyle2" runat="server" ></asp:TextBox></td>
    </tr>

    <tr>           
        <td class="style2" > 
        <asp:Label ID="lblPassportNo" cssclass="labelstyle1" runat="server" Text="Passport No"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtPassportNo"  cssclass="Txtstyle2" runat="server" ></asp:TextBox></td>
        <td></td>     <td > 
        <asp:Label ID="lblDOB" cssclass="labelstyle1" runat="server" Text="Date Of Birth"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtDOB"  cssclass="Txtstyle2"  runat="server" ></asp:TextBox></td>
           
    </tr>
    <tr>
             
        <td class="style2" >  
        <asp:Label ID="lblEmail" cssclass="labelstyle1" runat="server" Text="Email Address"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtEmail" cssclass="Txtstyle2" runat="server" ></asp:TextBox></td>
        <td></td>  <td >   <asp:Label ID="lblFatherName" cssclass="labelstyle1" runat="server" Text="Father’s Name"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtFatherName" cssclass="Txtstyle2" runat="server" ></asp:TextBox></td>
    </tr>

    <tr>
            
        <td class="style2" >  
        <asp:Label ID="lblHusbandName" cssclass="labelstyle1" runat="server" Text="Husband’s Name"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtHusbandName" cssclass="Txtstyle2" runat="server" ></asp:TextBox></td>
        <td></td>    <td >   <asp:Label ID="lblMotherName" cssclass="labelstyle1" runat="server" Text="Mother’s Name"></asp:Label>
        </td>
        <td >
        <asp:TextBox ID="txtMotherName" cssclass="Txtstyle2" runat="server" ></asp:TextBox></td>
    </tr>

</table>
  
</div>
<div align="center" style="width:100%;">

    <table align="center">

        <tr>
        <td  >  
        <input id="btnView"   class="buttonstyle1"    type="button" value="VIEW"   onclick="return btnView_onclick()"  />&nbsp;&nbsp;&nbsp;
        <input id="btnClear"  type="button" value="CLEAR"   class="buttonstyle1"  onclick="return btnClear_onclick()" />&nbsp;&nbsp;&nbsp;
        <input id="btnExit"   type="button" value="EXIT"   class="buttonstyle1"  onclick="return btnExit_onclick()" />
                
        </td>
      
        </tr>
    </table>
 
</div>

   <%-- <script language="javascript" type="text/javascript">
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
       
     
      function btnClear_onclick() 
        {
           document.getElementById("<%= cmbEmp.ClientID %>").value="";
          
        }
      
        function btnView_onclick() 
        {
           var Emp_ID=document.getElementById("<%= cmbEmp.ClientID %>").value;
           window.open("Report/SearchCustomerView.aspx?Emp_ID="+ btoa(Emp_ID) +"");

        }

        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = "ALL";
            document.getElementById(control).add(option1);
        }
    </script>--%>
</asp:Content>

