﻿Imports System.Data
Partial Class SearchCustomer
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim ReportID As Integer = 1
    Dim LocationID As Integer = 0
    Dim DB As New MS_SQL.Connect
    Dim GMASTER As New Master
    Dim UserID As Integer
    Dim DTT As New DataTable
    Dim PeriodID As Integer
    Dim StatusID As Integer
    Dim LevelID As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1228) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            'Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            'Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
    
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        DT1.Dispose()
        GC.Collect()
    End Sub


    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""

        If CInt(Data(0)) = 1 Then
            

        ElseIf CInt(Data(0)) = 3 Then
            PeriodID = CInt(Data(1))
            DT = DB.ExecuteDataSet("select -1 as code,'---select----' as empname UNION ALL select ahm.APPRAISEE_ID as code,em.EMP_NAME as empname from Appraisal_Heads_Master ahm " & _
                                    " LEFT join emp_master em on em.Emp_Code = ahm.APPRAISEE_ID" & _
                                    " where ahm.PERIOD_ID = " & PeriodID & " ").Tables(0)
        ElseIf CInt(Data(0)) = 4 Then
            LevelID = CInt(Data(1))
            UserID = CInt(Session("UserID"))
            If LevelID = 1 Then
                DT = DB.ExecuteDataSet("select -1 as code,'---select----' as empname UNION ALL select ahm.APPRAISEE_ID as code,em.EMP_NAME as empname from Appraisal_Heads_Master ahm " & _
                                        " LEFT join emp_master em on em.Emp_Code = ahm.APPRAISEE_ID" & _
                                        " where ahm.PERIOD_ID = " & PeriodID & " and ahm.Reporting_officer_id = " & UserID & " and ahm.STATUS >= 1 ").Tables(0)
            ElseIf LevelID = 2 Then
                DT = DB.ExecuteDataSet("select -1 as code,'---select----' as empname UNION ALL select ahm.APPRAISEE_ID as code,em.EMP_NAME as empname from Appraisal_Heads_Master ahm " & _
                                        " LEFT join emp_master em on em.Emp_Code = ahm.APPRAISEE_ID" & _
                                        " where ahm.PERIOD_ID = " & PeriodID & " and ahm.reviewing_head_id = " & UserID & " and ahm.STATUS >= 2").Tables(0)
            ElseIf LevelID = 3 Then
                DT = DB.ExecuteDataSet("select -1 as code,'---select----' as empname UNION ALL select ahm.APPRAISEE_ID as code,em.EMP_NAME as empname from Appraisal_Heads_Master ahm " & _
                                        " LEFT join emp_master em on em.Emp_Code = ahm.APPRAISEE_ID" & _
                                        " where ahm.PERIOD_ID = " & PeriodID & " and ahm.functional_head_id = " & UserID & " and ahm.STATUS >= 3").Tables(0)
            End If
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next

          
        End If
    End Sub
End Class
