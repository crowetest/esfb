﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Cust_Verification_Branch.aspx.vb" Inherits="Cust_Verification_Branch" EnableEventValidation="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<style> table.solid-table, .solid-table td {   border: 1px solid green;   border-collapse: collapse; } 

.NormalText
{
    font-family:Cambria;
    font-size:10pt;    
}
*                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
*                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
</style>
<head runat="server">
    <title></title>
</head>
<body>
<script language="javascript" type="text/javascript">
      function window_onload() {
          document.getElementById("rowCustDtl").style.display = "none";    
          document.getElementById("<%= hdnDocDtl.ClientID %>").value = "";  
          document.getElementById("rowDedupe").style.display = ""; 
          document.getElementById("rowDeactivate").style.display = "none"; 
          document.getElementById("rowRemark").style.display = "none"; 
          document.getElementById("rowUpload").style.display = "none";    
          ToServer("1ʘ", 1);
      }

      function YesOnClick()
      {
            if(document.getElementById("<%= radYes.ClientID %>").checked == true)
            {
                  document.getElementById("rowDedupe").style.display = ""; 
                  document.getElementById("rowDeactivate").style.display = ""; 
                  document.getElementById("rowRemark").style.display = ""; 
                  document.getElementById("rowUpload").style.display = "";   
            }
      }

      function NoOnClick()
      {
            if(document.getElementById("<%= radNo.ClientID %>").checked == true)
            {
                  document.getElementById("rowDedupe").style.display = ""; 
                  document.getElementById("rowDeactivate").style.display = "none"; 
                  document.getElementById("rowRemark").style.display = ""; 
                  document.getElementById("rowUpload").style.display = "none";   
            }
      }
        function AddDraftDoc() {
            if (!document.getElementById && !document.createElement)
                return false;
            var fileUploadarea = document.getElementById("fileDraftArea");
            if (!fileUploadarea)
                return false;
            var newLine = document.createElement("br");
            fileUploadarea.appendChild(newLine);
            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("class", "fileUpload");

            if (!AddDraftDoc.lastAssignedId)
                AddDraftDoc.lastAssignedId = 100;
            newFile.setAttribute("id", "FileUpload" + AddDraftDoc.lastAssignedId);
            newFile.setAttribute("name", "FileUpload" + AddDraftDoc.lastAssignedId);
            var div = document.createElement("div");
            div.appendChild(newFile);
            div.setAttribute("id", "div" + AddDraftDoc.lastAssignedId);
            fileUploadarea.appendChild(div);
            AddDraftDoc.lastAssignedId++;
        }
      function FromServer(Arg, Context) {     
          switch (Context) {
                case 1:
                {                    
                    var Data = Arg.split("£");                    
                    document.getElementById("<%= hdnDocDtl.ClientID %>").value = Data[0];    
                    ComboFill(Data[1], "<%= cmbCIF.ClientID %>");            
                    DiplayTable();
                    break;
                }
                case 4: // Approve Confirmation
                {
                    var Data = Arg;
                    if (Data != "") {
                        alert("OK");
                        window.open("Branch_Verification.aspx", "_self");
                    }
                    break;
                }
          }
      }
      function DiplayTable() {
          debugger;                                            
            var Tab = "";
            Tab += "<div id='ScrollDiv' style='width:100%; background-color:black; height:auto; overflow:auto; margin: 0px auto;'>";            
            if (document.getElementById("<%= hdnDocDtl.ClientID %>").value != "") {
                document.getElementById("rowCustDtl").style.display = "";

                var SplitData = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("£"); 
                var HidArg = SplitData[0].split("®");
                document.getElementById("<%= hdnCifDtl.ClientID %>").value = SplitData[1];
              
                var number = HidArg[32];
                var cast = number.toString(8).split('');   

                Tab += "<div style='width:100%;' class=mainhead>";
                Tab += "<table style='width:100%; font-family:'cambria';' align='center'>";
                Tab += "<tr style='height:30px;'>";
                Tab += "<th style='width:100%; text-align:center' bgcolor='#B90A0A' colspan='16'><font size='4' color='white'>DEDUPE CONFIRMATION</font></th>";
                Tab += "</tr>";
                Tab += "<tr style='height:30px;' bgcolor='white'>";
                Tab += "<th style='width:3%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>Open Date</font></th>";
                Tab += "<th style='width:6%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>CIF</font></th>";
                Tab += "<th style='width:7%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>Name</font></th>";
                Tab += "<th style='width:7%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>Branch</font></th>";
                Tab += "<th style='width:6%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>DOB</font></th>";
                Tab += "<th style='width:6%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>PIN</font></th>";
                Tab += "<th style='width:6%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>Mobile</font></th>";
                Tab += "<th style='width:7%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>Email</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>PAN</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>DL</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>Passport</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>VotersID</font></th>";
                Tab += "<th style='width:6%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>Aadhar</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>Father</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>Mother</font></th>";
                Tab += "<th style='width:10%; text-align:lefft' bgcolor='#081a6a'><font size='4' color='white'>Address</font></th>";
                Tab += "</tr>";

                Tab += "<tr style='height:200px;' bgcolor='white'>";
                Tab += "<th style='width:3%; text-align:center'><font size='2' color='black'>" + HidArg[0] + "</font></th>";
                Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[1] + "</font></th>";
                Tab += "<th style='width:7%; text-align:left'><font size='2' color='black'>" + HidArg[2] + "</font></th>";
                Tab += "<th style='width:7%; text-align:center'><font size='2' color='black'>" + HidArg[3] + "</font></th>";
                if (cast[0] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[4] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[4] + "</font></th>";
                if (cast[1] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[5] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[5] + "</font></th>";
                if (cast[2] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[6] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[6] + "</font></th>";
                if (cast[3] == 1)
                    Tab += "<th style='width:7%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[7] + "</font></th>";
                else
                    Tab += "<th style='width:7%; text-align:left'><font size='2' color='black'>" + HidArg[7] + "</font></th>";
                if (cast[4] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[8] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[8] + "</font></th>";
                if (cast[5] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[9] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[9] + "</font></th>";
                if (cast[6] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[10] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[10] + "</font></th>";
                Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[11] + "</font></th>";
                Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[12] + "</font></th>";
                Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[13] + "</font></th>";
                if (cast[7] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[14] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[14] + "</font></th>";
                Tab += "<th style='width:10%; text-align:left'><font size='2' color='black'>" + HidArg[15] + "</font></th>";
                Tab += "</tr>";

                Tab += "<tr style='height:200px;' bgcolor='white'>";
                Tab += "<th style='width:3%; text-align:center'><font size='2' color='black'>" + HidArg[16] + "</font></th>";
                Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[17] + "</font></th>";
                Tab += "<th style='width:7%; text-align:left'><font size='2' color='black'>" + HidArg[18] + "</font></th>";             
                Tab += "<th style='width:7%; text-align:center'><font size='2' color='black'>" + HidArg[19] + "</font></th>";
                if (cast[0] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[20] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[20] + "</font></th>";
                if (cast[1] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[21] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[21] + "</font></th>";
                if (cast[2] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[22] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[22] + "</font></th>";
                if (cast[3] == 1)
                    Tab += "<th style='width:7%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[23] + "</font></th>";
                else
                    Tab += "<th style='width:7%; text-align:left'><font size='2' color='black'>" + HidArg[23] + "</font></th>";
                if (cast[4] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[24] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[24] + "</font></th>";
                if (cast[5] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[25] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[25] + "</font></th>";
                if (cast[6] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[26] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[26] + "</font></th>";
                Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[27] + "</font></th>";
                Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[28] + "</font></th>";
                Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[29] + "</font></th>";
                if (cast[7] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[30] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[30] + "</font></th>";
                Tab += "<th style='width:10%; text-align:left'><font size='2' color='black'>" + HidArg[31] + "</font></th>";
                Tab += "</tr>";
                Tab += "<tr style='height:30px;' bgcolor='#e6e6ff'>";                
                Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Maker : " + HidArg[37] + "</font></label></td>";
                Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Date : " + HidArg[38] + "</font></label></td>";
                Tab += "<td style='width:34%; text-align:center;' colspan='6'><label><font size='4' color='black'>Status : " + HidArg[39] + "</font></label></td>";
                Tab += "</tr>";                  

                if(HidArg[49]==0)
                {          
                    if(HidArg[43]>0)
                    {
                        Tab += "<tr style='height:30px;' bgcolor='#e6e6ff'>";                                  
                        Tab += "<td style='width:23%; text-align:center;' colspan='2'><label><font size='4' color='black'>Checker : " + HidArg[43] + "</font></label></td>";
                        Tab += "<td style='width:23%; text-align:center;' colspan='3'><label><font size='4' color='black'>Date : " + HidArg[44] + "</font></label></td>";
                        Tab += "<td style='width:24%; text-align:center;' colspan='5'><label><font size='4' color='black'>Status : " + HidArg[45] + "</font></label></td>";
                        Tab += "<td style='width:30%; text-align:center;' colspan='6'><label><font size='4' color='black'>Remarks : " + HidArg[48] + "</font></label></td>"; 
                        Tab += "</tr>";                    
                    }
                    else
                    {  
                        Tab += "<tr style='height:30px;' bgcolor='#e6e6ff'>";  
                        Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Checker : " + HidArg[40] + "</font></label></td>";
                        Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Date : " + HidArg[41] + "</font></label></td>";
                        Tab += "<td style='width:34%; text-align:center;' colspan='6'><label><font size='4' color='black'>Status : " + HidArg[42] + "</font></label></td>";
                        Tab += "</tr>";                                      
                    }
                }
                else {                    
                    Tab += "<tr style='height:30px;' bgcolor='#e6e6ff'>";  
                    Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Checker : " + HidArg[40] + "</font></label></td>";
                    Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Date : " + HidArg[41] + "</font></label></td>";
                    Tab += "<td style='width:34%; text-align:center;' colspan='6'><label><font size='4' color='black'>Status : " + HidArg[42] + "</font></label></td>";
                    Tab += "</tr>";

                    Tab += "<tr style='height:30px;' bgcolor='#66cdaa'>";                                  
                    Tab += "<td style='width:23%; text-align:center;' colspan='2'><label><font size='4' color='black'>RPT Maker : " + HidArg[49] + "</font></label></td>";
                    Tab += "<td style='width:23%; text-align:center;' colspan='3'><label><font size='4' color='black'>Date : " + HidArg[50] + "</font></label></td>";
                    Tab += "<td style='width:24%; text-align:center;' colspan='5'><label><font size='4' color='black'>Status : " + HidArg[51] + "</font></label></td>";
                    Tab += "<td style='width:30%; text-align:center;' colspan='6'><label><font size='4' color='black'>Remarks : " + HidArg[52] + "</font></label></td>"; 
                    Tab += "</tr>";

                    if (HidArg[53] > 0) {
                        Tab += "<tr style='height:30px;' bgcolor='#66cdaa'>";
                        Tab += "<td style='width:23%; text-align:center;' colspan='2'><label><font size='4' color='black'>RPT Checker : " + HidArg[53] + "</font></label></td>";
                        Tab += "<td style='width:23%; text-align:center;' colspan='3'><label><font size='4' color='black'>Date : " + HidArg[54] + "</font></label></td>";
                        Tab += "<td style='width:24%; text-align:center;' colspan='4'><label><font size='4' color='black'>Status : " + HidArg[55] + "</font></label></td>";
                        Tab += "<td style='width:30%; text-align:center;' colspan='6'><label><font size='4' color='black'>Remarks : " + HidArg[56] + "</font></label></td>";
                        Tab += "</tr>";
                    }
                }
                
                Tab += "</table></div>";
                document.getElementById("<%= pnlCustDtl.ClientID %>").innerHTML = Tab;
            }
      } 
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ř");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("Ĉ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function SaveOnClick() {            

            if (document.getElementById("<%= radYes.ClientID %>").checked != true && document.getElementById("<%= radNo.ClientID %>").checked != true)
                alert("Please Mark Dedupe Yes/No");
            else
            {
                var HidArg = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("®");
                document.getElementById("<%= hdnDedupeID.ClientID %>").value = HidArg[47];
                document.getElementById("<%= hdnCif1.ClientID %>").value = HidArg[1];
                document.getElementById("<%= hdnCif2.ClientID %>").value = HidArg[17]; 
                document.getElementById("<%= hdnUpdateID.ClientID %>").value = HidArg[33]; 
                if(document.getElementById("<%= radNo.ClientID %>").checked == true) // Dedupe No
                {
                    var AppStatus = 2;
                    var StatusID = 4;
                    if(document.getElementById("<%= txtRemark.ClientID %>").value == "")
                    {
                        alert("Please enter Remaks");
                        document.getElementById("<%= txtRemark.ClientID %>").focus();
                        return false;
                    }
                    document.getElementById("<%= hdnRemark.ClientID %>").value = document.getElementById("<%= txtRemark.ClientID %>").value;
                    document.getElementById("<%= hdnDeactiveCif.ClientID %>").value = 0;                   
                    document.getElementById("<%= hdnAppStatus.ClientID %>").value = 2;
                    document.getElementById("<%= hdnStatusID.ClientID %>").value = 4;                    
                }
                else{ //dedupe Yes
                    if (document.getElementById("<%= fupAttachment.ClientID %>").value == "") {
                        alert("Please Upload Documents");
                        document.getElementById("<%= fupAttachment.ClientID %>").focus();
                        return false;
                    }
                    if(document.getElementById("<%= txtRemark.ClientID %>").value == "")
                    {
                        alert("Please enter Remaks");
                        document.getElementById("<%= txtRemark.ClientID %>").focus();
                        return false;
                    }
                    if(document.getElementById("<%= cmbCif.ClientID %>").value == "-1")
                    {
                        alert("Please Select CIF");
                        document.getElementById("<%= cmbCif.ClientID %>").focus();
                        return false;
                    }
                    document.getElementById("<%= hdnRemark.ClientID %>").value = document.getElementById("<%= txtRemark.ClientID %>").value;
                    document.getElementById("<%= hdnDeactiveCif.ClientID %>").value = document.getElementById("<%= cmbCif.ClientID %>").value;                   
                    document.getElementById("<%= hdnAppStatus.ClientID %>").value = 1;
                    document.getElementById("<%= hdnStatusID.ClientID %>").value = 3;                    
                }                
            }             
        }
      function btnExit_onclick() {
              ToServer("4ʘ", 4);
      }

// ]]>
  </script>
  <script language="javascript" type="text/javascript" for="window" event="onload">
    // <![CDATA[
    return window_onload()
    // ]]>
  </script>
  <br />    
    <form id="form1" runat="server">
    <div>    
    <table align="center" style="width: 95%; margin: 0px auto;">                
                <tr id="rowCustDtl">
                    <td style="text-align: center;" colspan="3" class="style2">
                        &nbsp;
                    </td>
                </tr>                              
                <tr>
                    <td style="text-align: center;" colspan="3" class="style2">
                        <asp:Panel ID="pnlCustDtl" runat="server">
                        </asp:Panel>
                    </td>
                </tr>
                <tr id="rowDedupe">
                    <td style="width: 30%; text-align: right;">
                        Dedupe&nbsp;&nbsp;&nbsp;&nbsp;</td>
                   <td style="text-align: left;">
                        <asp:RadioButton ID="radYes" runat="server" GroupName="dedupe" Text="Yes" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="radNo" runat="server" GroupName="dedupe" Text="No" />
                    </td>
                    <td style="width: 30%; text-align: center;">
                        &nbsp;</td>
                </tr>                
                <tr id="rowDeactivate">
                    <td style="width: 30%; text-align: right;">
                        CIF to be deactivated&nbsp;&nbsp;&nbsp;&nbsp;</td>
                   <td style="text-align: left;">
                        <asp:DropDownList ID="cmbCIF" Width="100%" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 30%; text-align: center;">
                        &nbsp;
                    </td>
                </tr>                
                <tr id="rowRemark">
                    <td style="width: 30%; text-align: right;">
                        Remarks&nbsp;&nbsp;&nbsp;&nbsp;</td>
                   <td style="text-align: center;">
                        <asp:TextBox ID="txtRemark" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 30%; text-align: center;">
                        &nbsp;
                    </td>
                </tr>                
                <tr id="rowUpload">
                    <td style="width: 30%; text-align: center;">
                        &nbsp;
                    </td>
                   <td style="width: 40%; text-align: center;">
                        <div id="fileDraftArea">
                            <asp:FileUpload ID="fupAttachment" runat="server" CssClass="fileUpload" /><br />
                        </div>
                        <div>
                            <input style="display: block;" id="btnDraftDoc" type="button" value="Add More" onclick="AddDraftDoc();" /><br />
                        </div>
                    </td>
                    <td style="width: 30%; text-align: center;">
                        &nbsp;
                    </td>
                </tr>                
                <tr>
                    <td style="text-align: center;" colspan="3" class="style2">
                        <asp:Button ID="btnSave" runat="server" Style="font-family: cambria; cursor: pointer;
                            width: 67px;" Text="SUBMIT" />
                        &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>            
            <asp:HiddenField ID="hdnDocDtl" runat="server" />
            <asp:HiddenField ID="hdnCifDtl" runat="server" />

            <asp:HiddenField ID="hdnDedupeID" runat="server" />
            <asp:HiddenField ID="hdnRemark" runat="server" /> 
            <asp:HiddenField ID="hdnDeactiveCif" runat="server" /> 
            <asp:HiddenField ID="hdnCif1" runat="server" /> 
            <asp:HiddenField ID="hdnCif2" runat="server" /> 
            <asp:HiddenField ID="hdnAppStatus" runat="server" /> 
            <asp:HiddenField ID="hdnStatusID" runat="server" /> 
            <asp:HiddenField ID="hdnUpdateID" runat="server" />   
    </div>
    </form>
    <configuration>
        <system.web>
            </compilation debug="true" targetFramework="4.0">
        </system.web>
    </configuration>
</body>    
</html>
