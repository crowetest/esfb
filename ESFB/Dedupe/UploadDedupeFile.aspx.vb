﻿Imports System.Data
Imports System.IO
Imports System.Net
Imports System.Data.SqlClient
Partial Class UploadDedupeFile
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Upload CAF/UPD File"
        Me.btnSave.Attributes.Add("onclick", "return showLoading()")
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim FilePath As String
            Dim FileName As String
            If Me.FileUpload1.HasFile = False Then
                Dim cl_script0 As New System.Text.StringBuilder
                cl_script0.Append("         alert('Please Browse Text File');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
                Exit Sub
            Else
                FileName = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName)
                FileUpload1.SaveAs(Server.MapPath("" + FileName))
                FilePath = Server.MapPath("" + FileName)
            End If
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim NoofAttachments As Integer = 0
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    If (nFileLen > 0) Then
                        NoofAttachments += 1
                    End If
                Next
            End If
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                        Dim Params(3) As SqlParameter
                        Params(0) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                        Params(0).Value = AttachImg
                        Params(1) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                        Params(1).Value = ContentType
                        Params(2) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                        Params(2).Value = FileName
                        Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                        Params(3).Value = CInt(Session("UserID"))
                        DB.ExecuteNonQuery("SP_DEDUPE_ATTACH", Params)
                    End If
                Next
            End If

            ReadTextFile(FilePath) '--//-- Reading Data From Text File Saved in Mentioned Path
            SaveData(FileName) '--//-- Saving Data into DB from Datatable Generated from Text File

            'File.Delete(FilePath)
        Catch ex As Exception
            Response.Redirect("~/CatchException.aspx?ErrorNo=1")
        End Try
    End Sub
    Private Sub ReadTextFile(ByVal FilePath As String)
        Dim Data As String = Nothing
        DT.Columns.Add("RowData1", Type.GetType("System.String"))
        Using Reader As New Microsoft.VisualBasic.FileIO.TextFieldParser(FilePath)
            Reader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
            Reader.SetDelimiters("|||||||||||||||||||||")
            Dim currentRow As String()
            Dim RowID As Integer = 0
            While Not Reader.EndOfData
                Try
                    currentRow = Reader.ReadFields()
                    Data += currentRow(0) + "~"
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException

                End Try
                RowID += 1
            End While
            hidData.Value = Data
        End Using
    End Sub
    Private Sub SaveData(ByVal FileName As String)
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim Data As String = hidData.Value
        Try
            Dim Params(3) As SqlParameter
            Params(0) = New SqlParameter("@DedupeDtl", Data)
            Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(1).Value = CInt(Session("UserID"))
            Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(2).Direction = ParameterDirection.Output
            Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(3).Direction = ParameterDirection.Output
            DB.ExecuteDataSetLongTime("SP_DEDUPE_FILE_UPLOAD", Params)
            ErrorFlag = CInt(Params(2).Value)
            Message = CStr(Params(3).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        If ErrorFlag = 0 Then
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Else
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        End If
    End Sub
End Class
