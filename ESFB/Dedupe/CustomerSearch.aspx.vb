﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Dedupe_CustomerSearch
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1466) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Me.Master.subtitle = "Customer Search"

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Me.txtDOB.Attributes.Add("onchange", "return DateOnChange();")
            Me.btnView.Attributes.Add("onclick", "return SaveOnClick();")

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
            End If
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
    End Sub

#Region "CallBack"

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
    End Sub

#End Region

#Region "Confirm"
    Protected Sub btnView_onclick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click

        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim SearchID As Integer = 0
        Dim UserID As Integer = CInt(Session("UserID"))

        Try
            Dim Name As String = CStr(Me.txtCustomerName.Text.Trim())
            'Dim DOB As String = If(Me.hdnDate.Value.Trim().Length > 0, Date.ParseExact(Convert.ToDateTime(Me.hdnDate.Value), "dd-MM-yyyy", Nothing).ToString("dd-MM-yyyy"), Nothing)
            Dim DOB As String = If(Me.hdnDate.Value.Trim().Length > 0, Convert.ToDateTime(Me.hdnDate.Value).ToString("dd-MM-yyyy"), Nothing)
            Dim Address As String = CStr(Me.txtAddress.Text.Trim())
            Dim PIN As String = Me.txtPin.Text.Trim()
            Dim MobileNo As String = If(Me.txtMobile.Text.Trim().Length > 0, CStr(Me.txtCountryCode.Text.Trim() + "-" + Me.txtMobile.Text.Trim()), "")
            Dim Email As String = CStr(Me.txtEmail.Text.Trim())
            Dim AadharNo As String = CStr(Me.txtAadhaarNumber.Text.Trim())
            Dim PanNo As String = CStr(Me.txtPanNumber.Text.Trim())
            Dim DrivingLicense As String = CStr(Me.txtDrivingLicense.Text.Trim())
            Dim VotersID As String = CStr(Me.txtVotersID.Text.Trim())
            Dim PassportNo As String = CStr(Me.txtPassportNo.Text.Trim())
            Dim FatherName As String = CStr(Me.txtFatherName.Text.Trim())
            Dim SpouseName As String = CStr(Me.txtSpouseName.Text.Trim())
            Dim MotherName As String = CStr(Me.txtMotherName.Text.Trim())

            Dim Params(17) As SqlParameter
            Params(0) = New SqlParameter("@Customer_Name", SqlDbType.VarChar, 200)
            Params(0).Value = Name
            Params(1) = New SqlParameter("@Date_Of_Birth", SqlDbType.VarChar, 20)
            Params(1).Value = DOB
            Params(2) = New SqlParameter("@Address", SqlDbType.VarChar, 1000)
            Params(2).Value = Address
            Params(3) = New SqlParameter("@PIN", SqlDbType.VarChar, 20)
            Params(3).Value = PIN
            Params(4) = New SqlParameter("@Mobile_Number", SqlDbType.VarChar, 20)
            Params(4).Value = MobileNo
            Params(5) = New SqlParameter("@Customer_Email", SqlDbType.VarChar, 200)
            Params(5).Value = Email
            Params(6) = New SqlParameter("@Aadhar_Number", SqlDbType.VarChar, 20)
            Params(6).Value = AadharNo
            Params(7) = New SqlParameter("@Pan_Number", SqlDbType.VarChar, 20)
            Params(7).Value = PanNo
            Params(8) = New SqlParameter("@Driving_License", SqlDbType.VarChar, 20)
            Params(8).Value = DrivingLicense
            Params(9) = New SqlParameter("@Voters_ID", SqlDbType.VarChar, 20)
            Params(9).Value = VotersID
            Params(10) = New SqlParameter("@Passport_Number", SqlDbType.VarChar, 20)
            Params(10).Value = PassportNo
            Params(11) = New SqlParameter("@Father_Name", SqlDbType.VarChar, 200)
            Params(11).Value = FatherName
            Params(12) = New SqlParameter("@Spouse_Name", SqlDbType.VarChar, 200)
            Params(12).Value = SpouseName
            Params(13) = New SqlParameter("@Mother_Name", SqlDbType.VarChar, 200)
            Params(13).Value = MotherName
            Params(14) = New SqlParameter("@USER_ID", SqlDbType.Int)
            Params(14).Value = UserID
            Params(15) = New SqlParameter("@SEARCH_ID", SqlDbType.Int)
            Params(15).Direction = ParameterDirection.Output
            Params(16) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(16).Direction = ParameterDirection.Output
            Params(17) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 2000)
            Params(17).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_CUSTOMER_SEARCH_DATA", Params)

            SearchID = CInt(Params(15).Value)
            ErrorFlag = CInt(Params(16).Value)
            Message = CStr(Params(17).Value)

            If ErrorFlag = 1 Then
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString(), False)
            Else
                Response.Redirect("~/Dedupe/Reports/SearchCustomerView.aspx?USER_ID=" + GN.Encrypt(UserID) + "&SEARCH_ID=" + GN.Encrypt(SearchID), False)
            End If

        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString(), False)
        End Try

    End Sub

#End Region

End Class
