﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Cust_Verification_CheckerBNo
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT1, DT2 As New DataTable

#Region "Page Load & Dispose"
    Protected Sub Customer_Verification_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
        DT1.Dispose()
        DT2.Dispose()
    End Sub

    Protected Sub Customer_Verification_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1320) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim NextCount As Integer = 1
        Dim PreCount As Integer = 0
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim TotCnt As Integer = 0
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Cust Details
                Try
                    Dim Params(10) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@UpdateInt", SqlDbType.Int)
                    Params(1).Value = 0
                    Params(2) = New SqlParameter("@CifOne", SqlDbType.VarChar, 50)
                    Params(2).Value = ""
                    Params(3) = New SqlParameter("@CifTwo", SqlDbType.VarChar, 50)
                    Params(3).Value = ""
                    Params(4) = New SqlParameter("@RowNum", SqlDbType.Int)
                    Params(4).Value = 0
                    Params(5) = New SqlParameter("@SaveID", SqlDbType.Int)
                    Params(5).Value = 0
                    Params(6) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(6).Value = 0
                    Params(7) = New SqlParameter("@DedupeID", SqlDbType.Int)
                    Params(7).Value = 0
                    Params(8) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                    Params(8).Value = ""
                    Params(9) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(9).Direction = ParameterDirection.Output
                    Params(10) = New SqlParameter("@TotCount", SqlDbType.Int)
                    Params(10).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_DEDUPE_VERIFICATION_CHECKERBYES", Params)
                    ErrorFlag = CInt(Params(9).Value)
                    TotCnt = CInt(Params(10).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                If (ErrorFlag = 0) Then
                    'TO - Add 2 column for updating Empcode and Random Number - Upto 25
                    DT = GF.GetQueryResult("select top 1 replace(convert(char(11),a.NewCif_CreatedDt,113),' ','-'),a.New_Cif,a.NewCif_Name,'('+convert(varchar,a.NewCif_brcode)+') '+isnull(b.Branch_Name,''),replace(convert(char(11),a.NewCif_DOB,113),' ','-'),a.NewCif_Pin,a.NewCif_Mob,a.NewCif_Email,a.NewCif_Pan,a.NewCif_Dlicense,a.NewCif_Passport,a.NewCif_VoterID,a.NewCif_Aadhar,a.NewCif_Father,a.NewCif_Mother,a.NewCif_Address,replace(convert(char(11),a.DupeCif_CreatedDt,113),' ','-'),a.Dupe_Cif,a.DupeCif_Name,'('+ convert(varchar,a.DupeCif_brcode)+') '+isnull(c.Branch_Name,''),replace(convert(char(11),a.DupeCif_DOB,113),' ','-'),a.DupeCif_Pin,a.DupeCif_Mob,a.DupeCif_Email,a.DupeCif_Pan,a.DupeCif_Dlicense,a.DupeCif_Passport,a.DupeCif_VoterID,a.DupeCif_Aadhar,a.DupeCif_Father,a.DupeCif_Mother,a.DupeCif_Address,a.EqualStatus,isnull(a.UpdateID,0),a.CheckerBYFlg,a.MakerN,replace(convert(char(11),a.MakerNDt,113),' ','-'),case when a.MakerNFlg=1 then 'DEDUPE YES' when a.MakerNFlg=2 then 'DEDUPE NO' else '' end,a.CheckerN,replace(convert(char(11),a.CheckerNDt,113),' ','-'),case when a.CheckerNFlg=1 then 'DEDUPE YES' when a.CheckerNFlg=2 then 'DEDUPE NO' else '' end,isnull(a.CheckerBN,0),replace(convert(char(11),a.CheckerBNDt,113),' ','-'),case when a.CheckerBNFlg=1 then 'DEDUPE YES' when a.CheckerBNFlg=2 then 'DEDUPE NO' else '' end, isnull(a.Branch,0),replace(convert(char(11),a.BranchDt,113),' ','-'),case when a.BranchFlg=1 then 'DEDUPE YES' when a.BranchFlg=2 then 'DEDUPE NO' else '' end,BranchRemark,a.status_id,a.DedupeID,isnull(a.MakerBY,0),replace(convert(char(11),a.MakerBYDt,113),' ','-'),case when a.MakerBYFlg=1 then 'DEDUPE YES' when a.MakerBYFlg=2 then 'DEDUPE NO' when a.MakerBYFlg=3 then 'REJECT' else '' end, MakerRemark,DeactivateCif from dedupe_cust_verification a left join branch_master b on a.NewCif_brcode=b.Branch_ID left join branch_master c on a.DupeCif_brcode=c.Branch_ID where a.MakerN is not null and a.MakerNDt is not null and a.EmpCode=" & UserID & " and a.status_id=5 and a.CheckerN is not null and a.CheckerNDt is not null and a.Branch is not null and a.BranchDt is not null order by a.UpdateID")
                    If DT.Rows.Count > 0 Then
                        If TotCnt = 1 Then
                            NextCount = 0
                        End If
                        CallBackReturn += DT.Rows(0)(0).ToString + "®" + DT.Rows(0)(1).ToString + "®" + DT.Rows(0)(2).ToString + "®" + DT.Rows(0)(3).ToString + "®" + DT.Rows(0)(4).ToString + "®" + DT.Rows(0)(5).ToString + "®" + DT.Rows(0)(6).ToString + "®" + DT.Rows(0)(7).ToString + "®" + DT.Rows(0)(8).ToString + "®" + DT.Rows(0)(9).ToString + "®" + DT.Rows(0)(10).ToString + "®" + DT.Rows(0)(11).ToString + "®" + DT.Rows(0)(12).ToString + "®" + DT.Rows(0)(13).ToString + "®" + DT.Rows(0)(14).ToString + "®" + DT.Rows(0)(15).ToString + "®" + DT.Rows(0)(16).ToString + "®" + DT.Rows(0)(17).ToString + "®" + DT.Rows(0)(18).ToString + "®" + DT.Rows(0)(19).ToString + "®" + DT.Rows(0)(20).ToString + "®" + DT.Rows(0)(21).ToString + "®" + DT.Rows(0)(22).ToString + "®" + DT.Rows(0)(23).ToString + "®" + DT.Rows(0)(24).ToString + "®" + DT.Rows(0)(25).ToString + "®" + DT.Rows(0)(26).ToString + "®" + DT.Rows(0)(27).ToString + "®" + DT.Rows(0)(28).ToString + "®" + DT.Rows(0)(29).ToString + "®" + DT.Rows(0)(30).ToString + "®" + DT.Rows(0)(31).ToString + "®" + DT.Rows(0)(32).ToString + "®" + DT.Rows(0)(33).ToString + "®" + DT.Rows(0)(34).ToString + "®" + NextCount.ToString + "®" + PreCount.ToString + "®" + DT.Rows(0)(35).ToString + "®" + DT.Rows(0)(36).ToString + "®" + DT.Rows(0)(37).ToString + "®" + DT.Rows(0)(38).ToString + "®" + DT.Rows(0)(39).ToString + "®" + DT.Rows(0)(40).ToString + "®" + DT.Rows(0)(41).ToString + "®" + DT.Rows(0)(42).ToString + "®" + DT.Rows(0)(43).ToString + "®" + DT.Rows(0)(44).ToString + "®" + DT.Rows(0)(45).ToString + "®" + DT.Rows(0)(46).ToString + "®" + DT.Rows(0)(47).ToString + "®" + DT.Rows(0)(48).ToString + "®" + DT.Rows(0)(49).ToString + "®" + DT.Rows(0)(50).ToString + "®" + DT.Rows(0)(51).ToString + "®" + DT.Rows(0)(52).ToString + "®" + DT.Rows(0)(53).ToString + "®" + DT.Rows(0)(54).ToString
                        CallBackReturn += "£"
                        DT2 = GF.GetQueryResult("select verid,dedupeid,dedupefile_name from dedupe_cust_attachments where dedupeid=" & CInt(DT.Rows(0)(49)) & "")
                        If DT2.Rows.Count > 0 Then
                            For Each DR In DT2.Rows
                                CallBackReturn += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "Ĉ"
                            Next
                        End If
                    End If
                End If
            Case 2
                Try
                    Dim Params(10) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@UpdateInt", SqlDbType.Int)
                    Params(1).Value = 1
                    Params(2) = New SqlParameter("@CifOne", SqlDbType.VarChar, 50)
                    Params(2).Value = CStr(Data(2))
                    Params(3) = New SqlParameter("@CifTwo", SqlDbType.VarChar, 50)
                    Params(3).Value = CStr(Data(3))
                    Params(4) = New SqlParameter("@RowNum", SqlDbType.Int)
                    Params(4).Value = CInt(Data(1))
                    Params(5) = New SqlParameter("@SaveID", SqlDbType.Int)
                    Params(5).Value = CInt(Data(4))
                    Params(6) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(6).Value = CInt(Data(6))
                    Params(7) = New SqlParameter("@DedupeID", SqlDbType.Int)
                    Params(7).Value = CInt(Data(7))
                    Params(8) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                    Params(8).Value = CStr(Data(8))
                    Params(9) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(9).Direction = ParameterDirection.Output
                    Params(10) = New SqlParameter("@TotCount", SqlDbType.Int)
                    Params(10).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_DEDUPE_VERIFICATION_CHECKERBYES", Params)
                    ErrorFlag = CInt(Params(9).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                If (ErrorFlag = 0) Then
                    Dim Str As String = "select replace(convert(char(11),a.NewCif_CreatedDt,113),' ','-'),a.New_Cif,a.NewCif_Name,'('+convert(varchar,a.NewCif_brcode)+') '+isnull(b.Branch_Name,''),replace(convert(char(11),a.NewCif_DOB,113),' ','-'),a.NewCif_Pin,a.NewCif_Mob,a.NewCif_Email,a.NewCif_Pan,a.NewCif_Dlicense,a.NewCif_Passport,a.NewCif_VoterID,a.NewCif_Aadhar,a.NewCif_Father,a.NewCif_Mother,a.NewCif_Address,replace(convert(char(11),a.DupeCif_CreatedDt,113),' ','-'),a.Dupe_Cif,a.DupeCif_Name,'('+ convert(varchar,a.DupeCif_brcode)+') '+isnull(c.Branch_Name,''),replace(convert(char(11),a.DupeCif_DOB,113),' ','-'),a.DupeCif_Pin,a.DupeCif_Mob,a.DupeCif_Email,a.DupeCif_Pan,a.DupeCif_Dlicense,a.DupeCif_Passport,a.DupeCif_VoterID,a.DupeCif_Aadhar,a.DupeCif_Father,a.DupeCif_Mother,a.DupeCif_Address,a.EqualStatus,isnull(a.UpdateID,0),a.CheckerBYFlg,a.MakerN,replace(convert(char(11),a.MakerNDt,113),' ','-'),case when a.MakerNFlg=1 then 'DEDUPE YES' when a.MakerNFlg=2 then 'DEDUPE NO' else '' end,a.CheckerN,replace(convert(char(11),a.CheckerNDt,113),' ','-'),case when a.CheckerNFlg=1 then 'DEDUPE YES' when a.CheckerNFlg=2 then 'DEDUPE NO' else '' end,isnull(a.CheckerBN,0),replace(convert(char(11),a.CheckerBNDt,113),' ','-'),case when a.CheckerBNFlg=1 then 'DEDUPE YES' when a.CheckerBNFlg=2 then 'DEDUPE NO' else '' end, isnull(a.Branch,0),replace(convert(char(11),a.BranchDt,113),' ','-'),case when a.BranchFlg=1 then 'DEDUPE YES' when a.BranchFlg=2 then 'DEDUPE NO' else '' end,BranchRemark,a.status_id,a.DedupeID,isnull(a.MakerBY,0),replace(convert(char(11),a.MakerBYDt,113),' ','-'),case when a.MakerBYFlg=1 then 'DEDUPE YES' when a.MakerBYFlg=2 then 'DEDUPE NO' when a.MakerBYFlg=3 then 'REJECT' else '' end, MakerRemark,DeactivateCif from dedupe_cust_verification a left join branch_master b on a.NewCif_brcode=b.Branch_ID left join branch_master c on a.DupeCif_brcode=c.Branch_ID where a.MakerN is not null and a.MakerNDt is not null and a.UpdateID=" & Data(5) & " and a.Empcode= " & UserID & " and a.CheckerN is not null and a.CheckerNDt is not null and a.Branch is not null and a.BranchDt is not null"
                    DT = GF.GetQueryResult(Str)
                    DT1 = GF.GetQueryResult("select count(*) from dedupe_cust_verification where UpdateID > " & Data(5) & " and EmpCode = " & UserID & "")
                    DT2 = GF.GetQueryResult("select count(*) from dedupe_cust_verification where UpdateID < " & Data(5) & "and EmpCode = " & UserID & "")
                    If DT.Rows.Count > 0 Then

                        NextCount = CInt(DT1.Rows(0)(0))
                        PreCount = CInt(DT2.Rows(0)(0))

                        CallBackReturn += DT.Rows(0)(0).ToString + "®" + DT.Rows(0)(1).ToString + "®" + DT.Rows(0)(2).ToString + "®" + DT.Rows(0)(3).ToString + "®" + DT.Rows(0)(4).ToString + "®" + DT.Rows(0)(5).ToString + "®" + DT.Rows(0)(6).ToString + "®" + DT.Rows(0)(7).ToString + "®" + DT.Rows(0)(8).ToString + "®" + DT.Rows(0)(9).ToString + "®" + DT.Rows(0)(10).ToString + "®" + DT.Rows(0)(11).ToString + "®" + DT.Rows(0)(12).ToString + "®" + DT.Rows(0)(13).ToString + "®" + DT.Rows(0)(14).ToString + "®" + DT.Rows(0)(15).ToString + "®" + DT.Rows(0)(16).ToString + "®" + DT.Rows(0)(17).ToString + "®" + DT.Rows(0)(18).ToString + "®" + DT.Rows(0)(19).ToString + "®" + DT.Rows(0)(20).ToString + "®" + DT.Rows(0)(21).ToString + "®" + DT.Rows(0)(22).ToString + "®" + DT.Rows(0)(23).ToString + "®" + DT.Rows(0)(24).ToString + "®" + DT.Rows(0)(25).ToString + "®" + DT.Rows(0)(26).ToString + "®" + DT.Rows(0)(27).ToString + "®" + DT.Rows(0)(28).ToString + "®" + DT.Rows(0)(29).ToString + "®" + DT.Rows(0)(30).ToString + "®" + DT.Rows(0)(31).ToString + "®" + DT.Rows(0)(32).ToString + "®" + DT.Rows(0)(33).ToString + "®" + DT.Rows(0)(34).ToString + "®" + NextCount.ToString + "®" + PreCount.ToString + "®" + DT.Rows(0)(35).ToString + "®" + DT.Rows(0)(36).ToString + "®" + DT.Rows(0)(37).ToString + "®" + DT.Rows(0)(38).ToString + "®" + DT.Rows(0)(39).ToString + "®" + DT.Rows(0)(40).ToString + "®" + DT.Rows(0)(41).ToString + "®" + DT.Rows(0)(42).ToString + "®" + DT.Rows(0)(43).ToString + "®" + DT.Rows(0)(44).ToString + "®" + DT.Rows(0)(45).ToString + "®" + DT.Rows(0)(46).ToString + "®" + DT.Rows(0)(47).ToString + "®" + DT.Rows(0)(48).ToString + "®" + DT.Rows(0)(49).ToString + "®" + DT.Rows(0)(50).ToString + "®" + DT.Rows(0)(51).ToString + "®" + DT.Rows(0)(52).ToString + "®" + DT.Rows(0)(53).ToString + "®" + DT.Rows(0)(54).ToString
                        CallBackReturn += "£"
                        DT2 = GF.GetQueryResult("select verid,dedupeid,dedupefile_name from dedupe_cust_attachments where dedupeid=" & CInt(DT.Rows(0)(49)) & "")
                        If DT2.Rows.Count > 0 Then
                            For Each DR In DT2.Rows
                                CallBackReturn += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "Ĉ"
                            Next
                        End If
                    End If
                End If
            Case 3
                Try
                    Dim Params(10) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@UpdateInt", SqlDbType.Int)
                    Params(1).Value = 2
                    Params(2) = New SqlParameter("@CifOne", SqlDbType.VarChar, 50)
                    Params(2).Value = CStr(Data(2))
                    Params(3) = New SqlParameter("@CifTwo", SqlDbType.VarChar, 50)
                    Params(3).Value = CStr(Data(3))
                    Params(4) = New SqlParameter("@RowNum", SqlDbType.Int)
                    Params(4).Value = CInt(Data(1))
                    Params(5) = New SqlParameter("@SaveID", SqlDbType.Int)
                    Params(5).Value = CInt(Data(4))
                    Params(6) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(6).Value = CInt(Data(5))
                    Params(7) = New SqlParameter("@DedupeID", SqlDbType.Int)
                    Params(7).Value = CInt(Data(6))
                    Params(8) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                    Params(8).Value = CStr(Data(7))
                    Params(9) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(9).Direction = ParameterDirection.Output
                    Params(10) = New SqlParameter("@TotCount", SqlDbType.Int)
                    Params(10).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_DEDUPE_VERIFICATION_CHECKERBYES", Params)
                    ErrorFlag = CInt(Params(9).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                If (ErrorFlag = 0) Then
                    CallBackReturn += "Updated Successfully"
                End If
            Case 4
                Try
                    Dim Params(10) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@UpdateInt", SqlDbType.Int)
                    Params(1).Value = 3
                    Params(2) = New SqlParameter("@CifOne", SqlDbType.VarChar, 50)
                    Params(2).Value = ""
                    Params(3) = New SqlParameter("@CifTwo", SqlDbType.VarChar, 50)
                    Params(3).Value = ""
                    Params(4) = New SqlParameter("@RowNum", SqlDbType.Int)
                    Params(4).Value = 0
                    Params(5) = New SqlParameter("@SaveID", SqlDbType.Int)
                    Params(5).Value = 0
                    Params(6) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(6).Value = 0
                    Params(7) = New SqlParameter("@DedupeID", SqlDbType.Int)
                    Params(7).Value = 0
                    Params(8) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                    Params(8).Value = ""
                    Params(9) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(9).Direction = ParameterDirection.Output
                    Params(10) = New SqlParameter("@TotCount", SqlDbType.Int)
                    Params(10).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_DEDUPE_VERIFICATION_CHECKERBYES", Params)
                    ErrorFlag = CInt(Params(9).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                If (ErrorFlag = 0) Then
                    CallBackReturn += "Ok"
                End If
        End Select
    End Sub
#End Region

End Class
