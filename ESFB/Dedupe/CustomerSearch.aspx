﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="CustomerSearch.aspx.vb" Inherits="Dedupe_CustomerSearch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .NormalText
        {
            font-family: Cambria;
            font-size: 10pt;
        }
    </style>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <br />
    <div>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    </div>
    <div>
        <table align="center" style="width: 50%; text-align: center; margin: 0px auto;">
            <tr>
                <td>
                    <asp:Label ID="lblCustomerName" class="NormalText" runat="server">Customer Name<span style="color:red"> *</span></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCustomerName" class="NormalText" runat="server" autocomplete="off"
                        MaxLength="200"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblDOB" class="NormalText" runat="server">Date Of Birth<span style="color:red"> *</span></asp:Label>
                </td>
                <td>
                    <asp:HiddenField ID="hdnDate" runat="server" />
                    <asp:TextBox ID="txtDOB" class="NormalText" runat="server" autocomplete="off" ReadOnly="true"></asp:TextBox>
                    <asp:CalendarExtender ID="CEFromDate" runat="server" Enabled="True" EnabledOnClient="true"
                        OnClientShowing="setStartDate" TargetControlID="txtDOB" Format="dd MMM yyyy">
                    </asp:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblAddress" class="NormalText" runat="server">Address<span style="color:red"> *</span></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtAddress" class="NormalText" runat="server" autocomplete="off"
                        MaxLength="1000" TextMode="MultiLine" onkeypress="TextAreaCheck(event)" 
                        Width="219px"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblPin" class="NormalText" runat="server">PIN Code<span style="color:red"> *</span></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtPin" class="NormalText" runat="server" autocomplete="off" MaxLength="6"
                        onkeypress="NumericCheck(event)"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFatherName" class="NormalText" runat="server">Father’s Name<span style="color:red"> *</span></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFatherName" class="NormalText" runat="server" autocomplete="off"
                        MaxLength="200"></asp:TextBox>
                </td>
                <td class="style2">
                    <asp:Label ID="lblSpouseName" class="NormalText" runat="server">Spouse's Name<span style="color:red"> *</span></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSpouseName" class="NormalText" runat="server" autocomplete="off"
                        MaxLength="200"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMotherName" class="NormalText" runat="server">Mother’s Name<span style="color:red"> *</span></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtMotherName" class="NormalText" runat="server" autocomplete="off"
                        MaxLength="200"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblMobile" class="NormalText" runat="server" Text="Mobile Number"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCountryCode" class="NormalText" runat="server" autocomplete="off"
                        onkeypress="NumericCheck(event)" MaxLength="5" Text ="91" Width="61px" onchange="CountryCodeOnChange()"></asp:TextBox>
                    <asp:TextBox ID="txtMobile" class="NormalText" runat="server" autocomplete="off"
                        onkeypress="NumericCheck(event)" MaxLength="10" Width="148px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblEmail" class="NormalText" runat="server" Text="Email Address"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" class="NormalText" runat="server" autocomplete="off" MaxLength="200"></asp:TextBox>
                </td>
                <td class="style2">
                    <asp:Label ID="lblAadhaarNumber" class="NormalText" runat="server" Text="Aadhar Number"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtAadhaarNumber" class="NormalText" runat="server" autocomplete="off"
                        onkeypress="NumericCheck(event)" MaxLength="12"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPanNumber" class="NormalText" runat="server" Text="Pan Number"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtPanNumber" class="NormalText" runat="server" autocomplete="off"
                        MaxLength="10"></asp:TextBox>
                </td>
                <td class="style2">
                    <asp:Label ID="lblDrivingLicense" class="NormalText" runat="server" Text="Driving License"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDrivingLicense" class="NormalText" runat="server" autocomplete="off"
                        MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblVotersID" class="NormalText" runat="server" Text="Voters ID"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtVotersID" class="NormalText" runat="server" autocomplete="off"
                        MaxLength="20"></asp:TextBox>
                </td>
                <td class="style2">
                    <asp:Label ID="lblPassportNo" class="NormalText" runat="server" Text="Passport No"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtPassportNo" class="NormalText" runat="server" autocomplete="off"
                        MaxLength="20"></asp:TextBox>
                </td>
            </tr>
        </table>
        <div align="center" style="padding-top: 20px">
            <asp:Button ID="btnView" runat="server" Style="font-family: cambria; cursor: pointer;
                width: 67px;" type="button" Text="VIEW" />&nbsp;&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                value="EXIT" onclick="return btnExit_onclick()" />
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function btnExit_onclick() {

            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        function setStartDate(sender, args) {                    
            var Today = new Date();
            sender._endDate = Today;            
        }

        function DateOnChange()
        {
            document.getElementById("<%= hdnDate.ClientID %>").value=document.getElementById("<%= txtDOB.ClientID %>").value;
        }

        function CountryCodeOnChange()
        {
            if(document.getElementById("<%= txtCountryCode.ClientID %>").value.trim() == "")
            {
                document.getElementById("<%= txtCountryCode.ClientID %>").value="91"
            }
        }

        function SaveOnClick() {  

                if(document.getElementById("<%= txtCustomerName.ClientID %>").value == "")
                {
                    alert("Please Enter Customer Name");
                    document.getElementById("<%= txtCustomerName.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtDOB.ClientID %>").value == "")
                {
                    alert("Please Enter Date of Birth");
                    document.getElementById("<%= txtDOB.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtAddress.ClientID %>").value == "")
                {
                    alert("Please Enter Address");
                    document.getElementById("<%= txtAddress.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtPin.ClientID %>").value == "")
                {
                    alert("Please Enter PIN Code");
                    document.getElementById("<%= txtPin.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtFatherName.ClientID %>").value == "")
                {
                    alert("Please Enter Father Name");
                    document.getElementById("<%= txtFatherName.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtSpouseName.ClientID %>").value == "")
                {
                    alert("Please Enter Spouse Name");
                    document.getElementById("<%= txtSpouseName.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtMotherName.ClientID %>").value == "")
                {
                    alert("Please Enter Mother Name");
                    document.getElementById("<%= txtMotherName.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtPin.ClientID %>").value != "")
                {
                    var pin= document.getElementById("<%= txtPin.ClientID %>").value;
                    if(pin.length<6)
                    {
                        alert("Please Enter a Valid PIN Code");
                        document.getElementById("<%= txtPin.ClientID %>").focus();
                        return false;
                    }
                }                
                if(document.getElementById("<%= txtMobile.ClientID %>").value!="")
                { 
                    var mobileNo= document.getElementById("<%= txtMobile.ClientID %>").value; 
                    if(mobileNo.length<10)
                    {
                        alert("Please Enter a Valid Mobile Number")
                        return false;
                    }
                } 
                if(document.getElementById("<%= txtEmail.ClientID %>").value != "")
                {
                    var ret= checkEmail(document.getElementById("<%= txtEmail.ClientID %>"));
                    if(ret==false)
                    {
                        return false;
                    }
                } 
                if(document.getElementById("<%= txtAadhaarNumber.ClientID %>").value!="")
                { 
                    var aadharNo= document.getElementById("<%= txtAadhaarNumber.ClientID %>").value; 
                    if(aadharNo.length!=12)
                    {
                        alert("Please Enter a Valid Aadhar Number")
                        return false;
                    }
                    else
                    {
                        if(validate(aadharNo)==false)
                        {
                            alert("Please Enter a Valid Aadhar Number")
                            return false;
                        }
                    }
                }     
                if(document.getElementById("<%= txtPanNumber.ClientID %>").value != "")
                {
                    var panVal = document.getElementById("<%= txtPanNumber.ClientID %>").value;
                    var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;

                    if(regpan.test(panVal)){
                        return true;
                    } else {
                        alert("Please Enter a Valid PAN Number")
                        document.getElementById("<%= txtPanNumber.ClientID %>").focus();
                        return false;
                    }
                }
                if((document.getElementById("<%= txtAadhaarNumber.ClientID %>").value == "") && (document.getElementById("<%= txtPanNumber.ClientID %>").value == "") && (document.getElementById("<%= txtDrivingLicense.ClientID %>").value == "") && (document.getElementById("<%= txtVotersID.ClientID %>").value == "") && (document.getElementById("<%= txtPassportNo.ClientID %>").value == ""))        
                {
                    alert("Please Enter Atleast 1 KYC");
                    return false;
                } 
        }

        // multiplication table d
        var d = [
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            [1, 2, 3, 4, 0, 6, 7, 8, 9, 5],
            [2, 3, 4, 0, 1, 7, 8, 9, 5, 6],
            [3, 4, 0, 1, 2, 8, 9, 5, 6, 7],
            [4, 0, 1, 2, 3, 9, 5, 6, 7, 8],
            [5, 9, 8, 7, 6, 0, 4, 3, 2, 1],
            [6, 5, 9, 8, 7, 1, 0, 4, 3, 2],
            [7, 6, 5, 9, 8, 2, 1, 0, 4, 3],
            [8, 7, 6, 5, 9, 3, 2, 1, 0, 4],
            [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
        ];

        // permutation table p
        var p = [
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            [1, 5, 7, 6, 2, 8, 3, 0, 9, 4],
            [5, 8, 0, 3, 7, 9, 6, 1, 4, 2],
            [8, 9, 1, 6, 0, 4, 3, 5, 2, 7],
            [9, 4, 5, 3, 1, 2, 6, 8, 7, 0],
            [4, 2, 8, 6, 5, 7, 3, 9, 0, 1],
            [2, 7, 9, 3, 8, 0, 6, 4, 1, 5],
            [7, 0, 4, 6, 9, 1, 3, 2, 5, 8]
        ];

        // inverse table inv
        var inv = [0, 4, 3, 2, 1, 5, 6, 7, 8, 9];

        // converts string or number to an array and inverts it
        function invArray(array) {

            if (Object.prototype.toString.call(array) === "[object Number]") {
                array = String(array);
            }

            if (Object.prototype.toString.call(array) === "[object String]") {
                array = array.split("").map(Number);
            }

            return array.reverse();

        }

        // generates checksum
        function generate(array) {

            var c = 0;
            var invertedArray = invArray(array);

            for (var i = 0; i < invertedArray.length; i++) {
                c = d[c][p[((i + 1) % 8)][invertedArray[i]]];
            }

            return inv[c];
        }

        // validates checksum
        function validate(array) {
            var c = 0;
            var invertedArray = invArray(array);

            for (var i = 0; i < invertedArray.length; i++) {
                c = d[c][p[(i % 8)][invertedArray[i]]];
            }

            return (c === 0);
        }
    </script>
</asp:Content>
