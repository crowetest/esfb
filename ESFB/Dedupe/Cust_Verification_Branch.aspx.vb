﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Cust_Verification_Branch
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT1, DT2 As New DataTable
    Dim DedupeID As New Integer
#Region "Page Load & Dispose"
    Protected Sub Customer_Verification_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
        DT1.Dispose()
        DT2.Dispose()
    End Sub

    Protected Sub Customer_Verification_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1318) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            DedupeID = CInt(Request.QueryString.Get("DedupeID"))
            Me.hdnDedupeID.Value = CStr(DedupeID)

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.btnSave.Attributes.Add("onclick", "return SaveOnClick()")
            Me.radYes.Attributes.Add("onclick", "return YesOnClick()")
            Me.radNo.Attributes.Add("onclick", "return NoOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim NextCount As Integer = 1
        Dim PreCount As Integer = 0
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim TotCnt As Integer = 0
        Dim DedupeID As Integer = CInt(Me.hdnDedupeID.Value)
        Select Case CInt(Data(0))
            Case 1 'Fill Cust Details
                Try
                    Dim Params(12) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@UpdateInt", SqlDbType.Int)
                    Params(1).Value = 0
                    Params(2) = New SqlParameter("@CifOne", SqlDbType.VarChar, 50)
                    Params(2).Value = ""
                    Params(3) = New SqlParameter("@CifTwo", SqlDbType.VarChar, 50)
                    Params(3).Value = ""
                    Params(4) = New SqlParameter("@RowNum", SqlDbType.Int)
                    Params(4).Value = 0
                    Params(5) = New SqlParameter("@SaveID", SqlDbType.Int)
                    Params(5).Value = 0
                    Params(6) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(6).Value = 0
                    Params(7) = New SqlParameter("@BranchID", SqlDbType.Int)
                    Params(7).Value = BranchID
                    Params(8) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                    Params(8).Value = ""
                    Params(9) = New SqlParameter("@DedupeID", SqlDbType.Int)
                    Params(9).Value = DedupeID
                    Params(10) = New SqlParameter("@DeactivateCif", SqlDbType.Float)
                    Params(10).Value = 0
                    Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(11).Direction = ParameterDirection.Output
                    Params(12) = New SqlParameter("@TotCount", SqlDbType.Int)
                    Params(12).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_DEDUPE_VERIFICATION_BRANCH", Params)
                    ErrorFlag = CInt(Params(11).Value)
                    TotCnt = CInt(Params(12).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                If (ErrorFlag = 0) Then
                    DT = GF.GetQueryResult("select top 1 replace(convert(char(11),a.NewCif_CreatedDt,113),' ','-'),a.New_Cif,a.NewCif_Name,'('+convert(varchar,a.NewCif_brcode)+') '+isnull(b.Branch_Name,''),replace(convert(char(11),a.NewCif_DOB,113),' ','-'),a.NewCif_Pin,a.NewCif_Mob,a.NewCif_Email,a.NewCif_Pan,a.NewCif_Dlicense,a.NewCif_Passport,a.NewCif_VoterID,a.NewCif_Aadhar,a.NewCif_Father,a.NewCif_Mother,a.NewCif_Address,replace(convert(char(11),a.DupeCif_CreatedDt,113),' ','-'),a.Dupe_Cif,a.DupeCif_Name,'('+ convert(varchar,a.DupeCif_brcode)+') '+isnull(c.Branch_Name,''),replace(convert(char(11),a.DupeCif_DOB,113),' ','-'),a.DupeCif_Pin,a.DupeCif_Mob,a.DupeCif_Email,a.DupeCif_Pan,a.DupeCif_Dlicense,a.DupeCif_Passport,a.DupeCif_VoterID,a.DupeCif_Aadhar,a.DupeCif_Father,a.DupeCif_Mother,a.DupeCif_Address,a.EqualStatus,isnull(a.UpdateID,0),a.BranchFlg,a.MakerN,replace(convert(char(11),a.MakerNDt,113),' ','-'),case when a.MakerNFlg=1 then 'DEDUPE YES' when a.MakerNFlg=2 then 'DEDUPE NO' else '' end,a.CheckerN,replace(convert(char(11),a.CheckerNDt,113),' ','-'),case when a.CheckerNFlg=1 then 'DEDUPE YES' when a.CheckerNFlg=2 then 'DEDUPE NO' else '' end,isnull(a.CheckerBN,0),replace(convert(char(11),a.CheckerBNDt,113),' ','-'),case when a.CheckerBNFlg=1 then 'DEDUPE YES' when a.CheckerBNFlg=2 then 'DEDUPE NO' else '' end,a.status_id,a.DedupeID,a.CheckerRemark,isnull(a.MakerBY,0),replace(convert(char(11),a.MakerBYDt,113),' ','-'),case when a.MakerBYFlg=1 then 'DEDUPE YES' when a.MakerBYFlg=2 then 'DEDUPE NO' when a.MakerBYFlg=3 then 'RETURN TO BRANCH' else '' end, MakerRemark, isnull(a.CheckerBY,0),replace(convert(char(11),a.CheckerBYDt,113),' ','-'),case when a.CheckerBYFlg=1 then 'DEDUPE YES' when a.CheckerBYFlg=2 then 'DEDUPE NO' when a.CheckerBYFlg=3 then 'RETURN TO BRANCH' else '' end, CheckerRemark from dedupe_cust_verification a left join branch_master b on a.NewCif_brcode=b.Branch_ID left join branch_master c on a.DupeCif_brcode=c.Branch_ID where a.MakerN is not null and a.MakerNDt is not null and a.EmpCode=" & UserID & " and a.NewCif_brcode=" & BranchID & " and a.status_id=2 and a.CheckerN is not null and a.CheckerNDt is not null and a.Branch is null and a.BranchDt is null order by UpdateID")
                    If DT.Rows.Count > 0 Then
                        If TotCnt = 1 Then
                            NextCount = 0
                        End If
                        CallBackReturn += DT.Rows(0)(0).ToString + "®" + DT.Rows(0)(1).ToString + "®" + DT.Rows(0)(2).ToString + "®" + DT.Rows(0)(3).ToString + "®" + DT.Rows(0)(4).ToString + "®" + DT.Rows(0)(5).ToString + "®" + DT.Rows(0)(6).ToString + "®" + DT.Rows(0)(7).ToString + "®" + DT.Rows(0)(8).ToString + "®" + DT.Rows(0)(9).ToString + "®" + DT.Rows(0)(10).ToString + "®" + DT.Rows(0)(11).ToString + "®" + DT.Rows(0)(12).ToString + "®" + DT.Rows(0)(13).ToString + "®" + DT.Rows(0)(14).ToString + "®" + DT.Rows(0)(15).ToString + "®" + DT.Rows(0)(16).ToString + "®" + DT.Rows(0)(17).ToString + "®" + DT.Rows(0)(18).ToString + "®" + DT.Rows(0)(19).ToString + "®" + DT.Rows(0)(20).ToString + "®" + DT.Rows(0)(21).ToString + "®" + DT.Rows(0)(22).ToString + "®" + DT.Rows(0)(23).ToString + "®" + DT.Rows(0)(24).ToString + "®" + DT.Rows(0)(25).ToString + "®" + DT.Rows(0)(26).ToString + "®" + DT.Rows(0)(27).ToString + "®" + DT.Rows(0)(28).ToString + "®" + DT.Rows(0)(29).ToString + "®" + DT.Rows(0)(30).ToString + "®" + DT.Rows(0)(31).ToString + "®" + DT.Rows(0)(32).ToString + "®" + DT.Rows(0)(33).ToString + "®" + DT.Rows(0)(34).ToString + "®" + NextCount.ToString + "®" + PreCount.ToString + "®" + DT.Rows(0)(35).ToString + "®" + DT.Rows(0)(36).ToString + "®" + DT.Rows(0)(37).ToString + "®" + DT.Rows(0)(38).ToString + "®" + DT.Rows(0)(39).ToString + "®" + DT.Rows(0)(40).ToString + "®" + DT.Rows(0)(41).ToString + "®" + DT.Rows(0)(42).ToString + "®" + DT.Rows(0)(43).ToString + "®" + DT.Rows(0)(44).ToString + "®" + DT.Rows(0)(45).ToString + "®" + DT.Rows(0)(46).ToString + "®" + DT.Rows(0)(47).ToString + "®" + DT.Rows(0)(48).ToString + "®" + DT.Rows(0)(49).ToString + "®" + DT.Rows(0)(50).ToString + "®" + DT.Rows(0)(51).ToString + "®" + DT.Rows(0)(52).ToString + "®" + DT.Rows(0)(53).ToString + "®" + DT.Rows(0)(54).ToString
                        CallBackReturn += "£"
                        CallBackReturn += "Ř-1ĈSelect CIFŘ" + DT.Rows(0)(1).ToString + "Ĉ" + DT.Rows(0)(1).ToString + "Ř" + DT.Rows(0)(17).ToString + "Ĉ" + DT.Rows(0)(17).ToString
                    End If
                End If
            Case 4
                Try
                    Dim Params(12) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@UpdateInt", SqlDbType.Int)
                    Params(1).Value = 3
                    Params(2) = New SqlParameter("@CifOne", SqlDbType.VarChar, 50)
                    Params(2).Value = ""
                    Params(3) = New SqlParameter("@CifTwo", SqlDbType.VarChar, 50)
                    Params(3).Value = ""
                    Params(4) = New SqlParameter("@RowNum", SqlDbType.Int)
                    Params(4).Value = 0
                    Params(5) = New SqlParameter("@SaveID", SqlDbType.Int)
                    Params(5).Value = 0
                    Params(6) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(6).Value = 0
                    Params(7) = New SqlParameter("@BranchID", SqlDbType.Int)
                    Params(7).Value = BranchID
                    Params(8) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                    Params(8).Value = ""
                    Params(9) = New SqlParameter("@DedupeID", SqlDbType.Int)
                    Params(9).Value = 0
                    Params(10) = New SqlParameter("@DeactivateCif", SqlDbType.Float)
                    Params(10).Value = 0
                    Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(11).Direction = ParameterDirection.Output
                    Params(12) = New SqlParameter("@TotCount", SqlDbType.Int)
                    Params(12).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_DEDUPE_VERIFICATION_BRANCH", Params)
                    ErrorFlag = CInt(Params(11).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                If (ErrorFlag = 0) Then
                    CallBackReturn += "Ok"
                End If
        End Select
    End Sub
#End Region

#Region "Confirm"
    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim DedupeID As Integer = CInt(Me.hdnDedupeID.Value)
        Dim UpdateID As Integer = CInt(Me.hdnUpdateID.Value)
        Dim StatusID As Integer = CInt(Me.hdnStatusID.Value)
        Dim ApproveID As Integer = CInt(Me.hdnAppStatus.Value)
        Try
            If ApproveID = 2 Then

                Dim Params(12) As SqlParameter
                Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(0).Value = UserID
                Params(1) = New SqlParameter("@UpdateInt", SqlDbType.Int)
                Params(1).Value = 2
                Params(2) = New SqlParameter("@CifOne", SqlDbType.VarChar, 50)
                Params(2).Value = Me.hdnCif1.Value
                Params(3) = New SqlParameter("@CifTwo", SqlDbType.VarChar, 50)
                Params(3).Value = Me.hdnCif2.Value
                Params(4) = New SqlParameter("@RowNum", SqlDbType.Int)
                Params(4).Value = UpdateID
                Params(5) = New SqlParameter("@SaveID", SqlDbType.Int)
                Params(5).Value = ApproveID
                Params(6) = New SqlParameter("@Status", SqlDbType.Int)
                Params(6).Value = StatusID
                Params(7) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(7).Value = BranchID
                Params(8) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                Params(8).Value = CStr(Me.hdnRemark.Value)
                Params(9) = New SqlParameter("@DedupeID", SqlDbType.Int)
                Params(9).Value = CInt(Me.hdnDedupeID.Value)
                Params(10) = New SqlParameter("@DeactivateCif", SqlDbType.VarChar, 50)
                Params(10).Value = ""
                Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(11).Direction = ParameterDirection.Output
                Params(12) = New SqlParameter("@TotCount", SqlDbType.Int)
                Params(12).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_DEDUPE_VERIFICATION_BRANCH", Params)
                ErrorFlag = CInt(Params(11).Value)
            Else
                Dim ContentType As String = ""
                Dim AttachImg As Byte() = Nothing
                Dim hfc As HttpFileCollection
                hfc = Request.Files

                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        Dim FileLength As Integer = CInt(myFile.ContentLength) 'Convert into bytes
                        If FileLength > 4000000 Then
                            Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                            cl_srpt1.Append("alert('Please Check the Size of attached file');")
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                            Exit Sub
                        End If
                        Dim FileExtension As String = Path.GetExtension(myFile.FileName)
                        If Not (FileExtension = ".xls" Or FileExtension = ".xlsx" Or FileExtension = ".jpg" Or FileExtension = ".jpeg" Or FileExtension = ".doc" Or FileExtension = ".docx" Or FileExtension = ".zip" Or FileExtension = ".pdf" Or FileExtension = ".PDF" Or FileExtension = ".XLS" Or FileExtension = ".XLSX" Or FileExtension = ".JPG" Or FileExtension = ".JPEG" Or FileExtension = ".DOC" Or FileExtension = ".DOCX" Or FileExtension = ".ZIP") Then
                            Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                            cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                            Exit Sub
                        End If
                    End If
                Next
                Try
                    Dim Params(12) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@UpdateInt", SqlDbType.Int)
                    Params(1).Value = 4
                    Params(2) = New SqlParameter("@CifOne", SqlDbType.VarChar, 50)
                    Params(2).Value = Me.hdnCif1.Value
                    Params(3) = New SqlParameter("@CifTwo", SqlDbType.VarChar, 50)
                    Params(3).Value = Me.hdnCif2.Value
                    Params(4) = New SqlParameter("@RowNum", SqlDbType.Int)
                    Params(4).Value = UpdateID
                    Params(5) = New SqlParameter("@SaveID", SqlDbType.Int)
                    Params(5).Value = ApproveID
                    Params(6) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(6).Value = StatusID
                    Params(7) = New SqlParameter("@BranchID", SqlDbType.Int)
                    Params(7).Value = BranchID
                    Params(8) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
                    Params(8).Value = CStr(Me.hdnRemark.Value)
                    Params(9) = New SqlParameter("@DedupeID", SqlDbType.Int)
                    Params(9).Value = DedupeID
                    Params(10) = New SqlParameter("@DeactivateCif", SqlDbType.VarChar, 50)
                    Params(10).Value = Me.hdnDeactiveCif.Value
                    Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(11).Direction = ParameterDirection.Output
                    Params(12) = New SqlParameter("@TotCount", SqlDbType.Int)
                    Params(12).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_DEDUPE_VERIFICATION_BRANCH", Params)
                    ErrorFlag = CInt(Params(11).Value)
                    If CInt(Me.hdnDedupeID.Value) > 0 And ErrorFlag = 0 And hfc.Count > 0 Then
                        For i = 0 To hfc.Count - 1
                            If i = 0 Then

                                DB.ExecuteNonQuery("delete from dedupe_cust_attachments where dedupeid ='" & DedupeID & "'")

                            End If

                            Dim myFile As HttpPostedFile = hfc(i)
                            Dim nFileLen As Integer = myFile.ContentLength
                            Dim FileName As String = ""
                            If (nFileLen > 0) Then
                                ContentType = myFile.ContentType
                                FileName = myFile.FileName
                                AttachImg = New Byte(nFileLen - 1) {}
                                myFile.InputStream.Read(AttachImg, 0, nFileLen)

                                Dim Param(5) As SqlParameter
                                Param(0) = New SqlParameter("@DedupeID", SqlDbType.Int)
                                Param(0).Value = DedupeID
                                Param(1) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
                                Param(1).Value = AttachImg
                                Param(2) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                                Param(2).Value = ContentType
                                Param(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                                Param(3).Value = FileName
                                Param(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                                Param(4).Direction = ParameterDirection.Output
                                Param(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                                Param(5).Direction = ParameterDirection.Output
                                DB.ExecuteNonQuery("SP_DEDUPE_BRANCH_ATTACH", Param)
                            End If
                        Next
                    End If
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
            End If
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("alert('Updated Successfully');")
            cl_script1.Append("window.open('Branch_Verification.aspx','_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

End Class
