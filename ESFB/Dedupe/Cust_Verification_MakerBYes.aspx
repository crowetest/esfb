﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Cust_Verification_MakerBYes.aspx.vb"
    Inherits="Cust_Verification_Branch" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style>
    table.solid-table, .solid-table td
    {
        border: 1px solid green;
        border-collapse: collapse;
    }
</style>
<head runat="server">
    <title></title>
</head>
<body>
    <script language="javascript" type="text/javascript">
      function window_onload() {
          document.getElementById("rowCustDtl").style.display = "none";    
          document.getElementById("<%= hdnDocDtl.ClientID %>").value = "";     
          ToServer("1ʘ", 1);
      }
      function FromServer(Arg, Context) {     
          switch (Context) {
                case 1:
                {                    
                    var Data = Arg;                    
                    document.getElementById("<%= hdnDocDtl.ClientID %>").value = Data;                   
                    DiplayTable();
                    break;
                }
                case 2: // Approve Confirmation
                {
                    var Data = Arg;
                    if (Data != "") {
                        alert(Data);
                        window.open("Cust_Verification_MakerBYes.aspx", "_self");
                    }
                    break;
                }
                case 4: // Approve Confirmation
                {
                    var Data = Arg;
                    if (Data != "") {
                        alert("OK");
                        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");

                    }
                    break;
                }
          }
      }
      function DiplayTable() {   
      debugger;                                         
            var Tab = "";
            Tab += "<div id='ScrollDiv' style='width:100%; background-color:black; height:auto; overflow:auto; margin: 0px auto;'>";            
            if (document.getElementById("<%= hdnDocDtl.ClientID %>").value != "") {
                document.getElementById("rowCustDtl").style.display = "";
                                
                var Data = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("£");
                var HidArg = Data[0].split("®"); 
                document.getElementById("<%= hdnAttachDtl.ClientID %>").value = Data[1];
                var number = HidArg[32];
                var cast = number.toString(8).split('');   

                Tab += "<div style='width:100%;' class=mainhead>";
                Tab += "<table style='width:100%; font-family:'cambria';' align='center'>";
                Tab += "<tr style='height:30px;'>";
                Tab += "<th style='width:100%; text-align:center' bgcolor='#B90A0A' colspan='16'><font size='4' color='white'>RETURN FROM BRANCH - DEDUPE YES</font></th>";
                Tab += "</tr>";
                Tab += "<tr style='height:30px;' bgcolor='white'>";
                Tab += "<th style='width:5%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>Open Date</font></th>";
                Tab += "<th style='width:6%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>CIF</font></th>";
                Tab += "<th style='width:7%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>Name</font></th>";
                Tab += "<th style='width:5%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>Branch</font></th>";
                Tab += "<th style='width:6%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>DOB</font></th>";
                Tab += "<th style='width:6%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>PIN</font></th>";
                Tab += "<th style='width:6%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>Mobile</font></th>";
                Tab += "<th style='width:7%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>Email</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>PAN</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>DL</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>Passport</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>VotersID</font></th>";
                Tab += "<th style='width:6%; text-align:center' bgcolor='#081a6a'><font size='4' color='white'>Aadhar</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>Father</font></th>";
                Tab += "<th style='width:6%; text-align:left' bgcolor='#081a6a'><font size='4' color='white'>Mother</font></th>";
                Tab += "<th style='width:10%; text-align:lefft' bgcolor='#081a6a'><font size='4' color='white'>Address</font></th>";
                Tab += "</tr>";

                Tab += "<tr style='height:200px;' bgcolor='white'>";
                Tab += "<th style='width:5%; text-align:center'><font size='2' color='black'>" + HidArg[0] + "</font></th>";
                Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[1] + "</font></th>";
                Tab += "<th style='width:7%; text-align:left'><font size='2' color='black'>" + HidArg[2] + "</font></th>";
                Tab += "<th style='width:5%; text-align:center' bgcolor='#C0C0C0'><font size='2' color='black'>" + HidArg[3] + "</font></th>";
                if (cast[0] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[4] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[4] + "</font></th>";
                if (cast[1] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[5] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[5] + "</font></th>";
                if (cast[2] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[6] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[6] + "</font></th>";
                if (cast[3] == 1)
                    Tab += "<th style='width:7%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[7] + "</font></th>";
                else
                    Tab += "<th style='width:7%; text-align:left'><font size='2' color='black'>" + HidArg[7] + "</font></th>";
                if (cast[4] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[8] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[8] + "</font></th>";
                if (cast[5] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[9] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[9] + "</font></th>";
                if (cast[6] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[10] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[10] + "</font></th>";
                Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[11] + "</font></th>";
                Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[12] + "</font></th>";
                Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[13] + "</font></th>";
                if (cast[7] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[14] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[14] + "</font></th>";
                Tab += "<th style='width:10%; text-align:left'><font size='2' color='black'>" + HidArg[15] + "</font></th>";
                Tab += "</tr>";

                Tab += "<tr style='height:200px;' bgcolor='white'>";
                Tab += "<th style='width:5%; text-align:center'><font size='2' color='black'>" + HidArg[16] + "</font></th>";
                Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[17] + "</font></th>";
                Tab += "<th style='width:7%; text-align:left'><font size='2' color='black'>" + HidArg[18] + "</font></th>";             
                Tab += "<th style='width:5%; text-align:center'><font size='2' color='black'>" + HidArg[19] + "</font></th>";
                if (cast[0] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[20] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[20] + "</font></th>";
                if (cast[1] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[21] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[21] + "</font></th>";
                if (cast[2] == 1)
                    Tab += "<th style='width:6%; text-align:center' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[22] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[22] + "</font></th>";
                if (cast[3] == 1)
                    Tab += "<th style='width:7%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[23] + "</font></th>";
                else
                    Tab += "<th style='width:7%; text-align:left'><font size='2' color='black'>" + HidArg[23] + "</font></th>";
                if (cast[4] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[24] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[24] + "</font></th>";
                if (cast[5] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[25] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[25] + "</font></th>";
                if (cast[6] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[26] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[26] + "</font></th>";
                Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[27] + "</font></th>";
                Tab += "<th style='width:6%; text-align:center'><font size='2' color='black'>" + HidArg[28] + "</font></th>";
                Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[29] + "</font></th>";
                if (cast[7] == 1)
                    Tab += "<th style='width:6%; text-align:left' bgcolor='#ff9999'><font size='2' color='black'>" + HidArg[30] + "</font></th>";
                else
                    Tab += "<th style='width:6%; text-align:left'><font size='2' color='black'>" + HidArg[30] + "</font></th>";
                Tab += "<th style='width:10%; text-align:left'><font size='2' color='black'>" + HidArg[31] + "</font></th>";
                Tab += "</tr>";
                Tab += "<tr style='height:30px;' bgcolor='#e6e6ff'>";                
                Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Maker : " + HidArg[37] + "</font></label></td>";
                Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Date : " + HidArg[38] + "</font></label></td>";
                Tab += "<td style='width:34%; text-align:center;' colspan='6'><label><font size='4' color='black'>Status : " + HidArg[39] + "</font></label></td>";
                Tab += "</tr>";
                Tab += "<tr style='height:30px;' bgcolor='#e6e6ff'>";                
                Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Checker : " + HidArg[40] + "</font></label></td>";
                Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Date : " + HidArg[41] + "</font></label></td>";
                Tab += "<td style='width:34%; text-align:center;' colspan='6'><label><font size='4' color='black'>Status : " + HidArg[42] + "</font></label></td>";
                Tab += "</tr>";
                if(HidArg[43] > 0)
                {                   
                    Tab += "<tr style='height:30px;' bgcolor='#e6e6ff'>";                
                    Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Branch No Return Checker : " + HidArg[43] + "</font></label></td>";
                    Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Date : " + HidArg[44] + "</font></label></td>";
                    Tab += "<td style='width:34%; text-align:center;' colspan='6'><label><font size='4' color='black'>Status : " + HidArg[45] + "</font></label></td>";
                    Tab += "</tr>";
                }                
                Tab += "<tr style='height:30px;' bgcolor='#e6e6ff'>";                
                Tab += "<td style='width:15%; text-align:center;' colspan='2'><label><font size='4' color='black'>Branch : " + HidArg[46] + "</font></label></td>";
                Tab += "<td style='width:20%; text-align:center;' colspan='3'><label><font size='4' color='black'>Date : " + HidArg[47] + "</font></label></td>";
                Tab += "<td style='width:32%; text-align:center;' colspan='5'><label><font size='4' color='black'>Status : " + HidArg[48] + "</font></label></td>";
                Tab += "<td style='width:33%; text-align:center;' colspan='6'><label><font size='4' color='black'>Remarks : " + HidArg[49] + "</font></label></td>";
                Tab += "</tr>";
                if(HidArg[53] > 0 && HidArg[50] == 12)
                { 
                    Tab += "<tr style='height:30px;' bgcolor='#b9c6c2'>";                
                    Tab += "<td style='width:15%; text-align:center;' colspan='2'><label><font size='4' color='black'>RPT Checker : " + HidArg[53] + "</font></label></td>";
                    Tab += "<td style='width:20%; text-align:center;' colspan='3'><label><font size='4' color='black'>Date : " + HidArg[54] + "</font></label></td>";
                    Tab += "<td style='width:32%; text-align:center;' colspan='5'><label><font size='4' color='black'>Status : " + HidArg[55] + "</font></label></td>";
                    Tab += "<td style='width:33%; text-align:center;' colspan='6'><label><font size='4' color='black'>Remarks : " + HidArg[56] + "</font></label></td>";
                    Tab += "</tr>";
                }
                if(HidArg[52] > 0)
                {
                    Tab += "<tr style='height:30px;' bgcolor='white'>";
                    Tab += "<td style='width:33%; text-align:center;' colspan='4'></td>";
                    Tab += "<td style='width:33%; text-align:center;' colspan='1'>Deactivate CIF</td>";
                    Tab += "<td tyle='width:33%; text-align:center;' colspan='5'>" + HidArg[52] + "</td>";   
                    Tab += "<td style='width:34%; text-align:center;' colspan='6'></td>";
                    Tab += "</tr>";
                }
                if (document.getElementById("<%= hdnAttachDtl.ClientID %>").value != "") {
                    row = document.getElementById("<%= hdnAttachDtl.ClientID %>").value.split("Ĉ");
                    for (n = 0; n < row.length - 1; n++) {
                        col = row[n].split("µ");
                        i = n + 1;
                         
                        Tab += "<tr style='height:30px;' bgcolor='white'>";
                        Tab += "<td style='width:33%; text-align:center;' colspan='4'></td>";
                        Tab += "<td style='width:33%; text-align:center;' colspan='1'>" + i + "</td>";
                        Tab += "<td tyle='width:33%; text-align:center;' colspan='5'><a href='ShowDedupeFormat.aspx?VerID=" + col[0] + "&DedupeID=" + col[1] + "'>" + col[2] + "</a></td>";   
                        Tab += "<td style='width:34%; text-align:center;' colspan='6'></td>";
                        Tab += "</tr>";
                    }
                }                

                Tab += "<tr style='height:30px;' bgcolor='white'>";
                Tab += "<td style='width:33%; text-align:center;' colspan='5'><label>Dedupe</font></label></td>";
                if (HidArg[50] == 3) //StatusID = 3 <- Branch Yes
                {
                    Tab += "<td style='width:25%; text-align:center;' colspan='5'><label><input type='radio' checked id='radYes" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>Yes</font></label></td>";
                    //Tab += "<td style='width:17%; text-align:center;' colspan='3'><label><input type='radio' id='radNo" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>No</font></label></td>";
                    Tab += "<td style='width:25%; text-align:center;' colspan='6'><label><input type='radio' id='radRej" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>Return to Branch</font></label></td>";
                }
                else if (HidArg[50] == 12) //StatusID = 12 <- Return From Checker
                {
                    Tab += "<td style='width:25%; text-align:center;' colspan='5'><label><input type='radio' id='radYes" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>Yes</font></label></td>";
                    //Tab += "<td style='width:17%; text-align:center;' colspan='3'><label><input type='radio' id='radNo" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>No</font></label></td>";
                    Tab += "<td style='width:25%; text-align:center;' colspan='6'><label><input type='radio' checked id='radRej" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>Return to Branch</font></label></td>";
                }
                else
                {
                    if (HidArg[34] == 1)
                    {
                        Tab += "<td style='width:16%; text-align:center;' colspan='5'><label><input type='radio' checked id='radYes" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>Yes</font></label></td>";
                        //Tab += "<td style='width:17%; text-align:center;' colspan='3'><label><input type='radio' id='radNo" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>No</font></label></td>";
                        Tab += "<td style='width:17%; text-align:center;' colspan='6'><label><input type='radio' id='radRej" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>Return to Branch</font></label></td>";
                    }
                    else if (HidArg[34] == 2){
                        Tab += "<td style='width:16%; text-align:center;' colspan='5'><label><input type='radio' id='radYes" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>Yes</font></label></td>";
                        //Tab += "<td style='width:17%; text-align:center;' colspan='3'><label><input type='radio' checked id='radNo" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>No</font></label></td>";
                        Tab += "<td style='width:17%; text-align:center;' colspan='6'><label><input type='radio' id='radRej" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>Return to Branch</font></label></td>";
                    }
                    else {
                        Tab += "<td style='width:16%; text-align:center;' colspan='5'><label><input type='radio' id='radYes" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>Yes</font></label></td>";
                        //Tab += "<td style='width:17%; text-align:center;' colspan='3'><label><input type='radio' id='radNo" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>No</font></label></td>";
                        Tab += "<td style='width:17%; text-align:center;' colspan='6'><label><input type='radio' checked id='radRej" + HidArg[33] + "' name='rad" + HidArg[33] + "'/><font size='4' color='black'>Return to Branch</font></label></td>";
                    }
                }
                Tab += "</tr>";   
                Tab += "<tr style='height:30%;' bgcolor='white'>";
                var txtBox = "<textarea id='txtRemarks" + HidArg[33] + "' name='txtRemarks" + HidArg[33] + "' style='width:99%; float:left;' maxlength='300' ></textarea>";
                Tab += "<td style='width:33%; text-align:center;' colspan='5'><label><font size='4' color='black'>Enter Remarks If any</font></label></td>";
                Tab += "<td style='width:33%; text-align:left;' colspan='11'>" + txtBox + "</td>";
                Tab += "</tr>";             
                Tab += "<tr style='height:30%;' bgcolor='white'>";
                if (HidArg[35] > 0) {
                    if (HidArg[36] == 0) {
                        Tab += "<td style='width:5%;text-align:center' colspan='8'></td>";
                        Tab += "<td style='width:5%;text-align:right' colspan='8'><u><a href='#' title='Next' onclick='NextOnClick(" + HidArg[1] + "," + HidArg[17] + "," + HidArg[33] + "," + HidArg[51] +")'>Next<img id='imgMk' src='../Image/DedupeNext.PNG' title='Next' Height='15px' Width='15px' style='cursor:pointer;'></img></a></u></td></tr>";
                    }
                    else {
                        Tab += "<td style='width:5%;text-align:left' colspan='8'><u><a href='#' title='Previous' onclick='PreviousOnClick(" + HidArg[1] + "," + HidArg[17] + "," + HidArg[33] + "," + HidArg[51] +")'><img id='imgMk' src='../Image/DedupePre.PNG' title='Previouss' Height='15px' Width='15px' style='cursor:pointer;'></img>Previous</a></u></td>";
                        Tab += "<td style='width:5%;text-align:right' colspan='8'><u><a href='#' title='Next' onclick='NextOnClick(" + HidArg[1] + "," + HidArg[17] + "," + HidArg[33] + "," + HidArg[51] +")'>Next<img id='imgMk' src='../Image/DedupeNext.PNG' title='Next' Height='15px' Width='15px' style='cursor:pointer;'></img></a></u></td></tr>";
                    }
                }
                else {
                    if (HidArg[36] > 0) {
                        Tab += "<td style='width:5%;text-align:left' colspan='8'><u><a href='#' title='Previous' onclick='PreviousOnClick(" + HidArg[1] + "," + HidArg[17] + "," + HidArg[33] + "," + HidArg[51] +")'><img id='imgMk' src='../Image/DedupePre.PNG' title='Previouss' Height='15px' Width='15px' style='cursor:pointer;'></img>Previous</a></u></td>";
                        Tab += "<td style='width:5%;text-align:right' colspan='8'></td></tr>";
                    }
                    else {
                        Tab += "<td style='width:5%;text-align:center'></td>";
                        Tab += "<td style='width:5%;text-align:center'></td></tr>";
                    }
                }
                Tab += "<td style='width:45%;text-align:center'></td>";
                Tab += "</table></div>";
                document.getElementById("<%= pnlCustDtl.ClientID %>").innerHTML = Tab;
            }
      }      
//      function NextOnClick(CIfOne, CifTwo, RowNum, DedupeID)
//      {  
//            var Data = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("£");
//            var HidArg = Data[0].split("®"); 
//            var SaveVal = 0
//            var NextId = RowNum+1;
//            var AppStatus = 0;
//            var StatusID = 0;
//            var Remarks = document.getElementById("txtRemarks" + RowNum).value; 
//            if (document.getElementById("radYes" + RowNum).checked != true && document.getElementById("radNo" + RowNum).checked != true && document.getElementById("radRej" + HidArg[33]).checked != true)
//            {
//                alert("Please Mark Dedupe Yes/No/Reject");
//                return false;
//            }
//            else
//            {
//                if (document.getElementById("radYes" + RowNum).checked == true)            
//                    AppStatus=1;
//                else if (document.getElementById("radNo" + RowNum).checked == true)
//                    AppStatus=2;   
//                else
//                {
//                    if(Remarks == "")
//                    {
//                        alert("Please Enter Reject Reason");
//                        return false;
//                    }
//                    AppStatus=3;
//                } 
//                StatusID = 5;
//                ToServer("2ʘ" + RowNum + "ʘ" + CIfOne + "ʘ" + CifTwo + "ʘ" + AppStatus + "ʘ" + NextId + "ʘ" + StatusID + "ʘ" + DedupeID + "ʘ" + Remarks, 1);
//            }
//      }
//      function PreviousOnClick(CIfOne, CifTwo, RowNum, DedupeID)
//      {    
//            var Data = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("£");
//            var HidArg = Data[0].split("®"); 
//            var SaveVal = 0
//            var AppStatus = 0;
//            var StatusID = 0;
//            var Remarks = document.getElementById("txtRemarks" + RowNum).value; 
//            if (document.getElementById("radYes" + RowNum).checked != true && document.getElementById("radNo" + RowNum).checked != true && document.getElementById("radRej" + HidArg[33]).checked != true)
//                alert("Please Mark Dedupe Yes/No/Reject");
//            else
//            {
//                if (document.getElementById("radYes" + RowNum).checked == true)
//                    AppStatus=1;
//                else if (document.getElementById("radNo" + RowNum).checked == true)
//                    AppStatus=2;   
//                else{
//                    if(Remarks == "")
//                        alert("Please Enter Reject Reason");
//                    AppStatus=3;
//                } 
//                StatusID = 5;
//                var NextId = RowNum-1; 

//                ToServer("2ʘ" + RowNum + "ʘ" + CIfOne + "ʘ" + CifTwo + "ʘ" + AppStatus + "ʘ" + NextId + "ʘ" + StatusID + "ʘ" + DedupeID + "ʘ" + Remarks, 1);   
//            }         
//      }
      function btnSave_onclick() {     
            var Data = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("£");
            var HidArg = Data[0].split("®"); 
            var Remarks = document.getElementById("txtRemarks" + HidArg[33]).value;
            if (document.getElementById("radYes" + HidArg[33]).checked != true && document.getElementById("radRej" + HidArg[33]).checked != true)
            {
                alert("Please Mark Dedupe Yes/Return");
                return false;
            }
            else
            {
                var AppStatus = 0;
                var StatusID = 0;
                if (document.getElementById("radYes" + HidArg[33]).checked == true)
                {
                    AppStatus=1;
                    StatusID = 5;
                }
                else if(document.getElementById("radRej" + HidArg[33]).checked == true)
                {
                    if(Remarks == "")
                    {
                        alert("Please Enter Return Reason");
                        return false;
                    }
                    AppStatus= 3; // Return
                    StatusID = 2;
                }
                                
                ToServer("3ʘ" + HidArg[33] + "ʘ" + HidArg[1] + "ʘ" + HidArg[17]  + "ʘ" + AppStatus + "ʘ" + StatusID + "ʘ" + HidArg[51] + "ʘ" + Remarks, 2);
            }
      }
      function btnExit_onclick() {
              ToServer("4ʘ", 4);
      }

// ]]>

    </script>
    <script language="javascript" type="text/javascript" for="window" event="onload">
    // <![CDATA[
    return window_onload()
    // ]]>
    </script>
    <br />
    <form id="form1" runat="server">
    <div>
        <table align="center" style="width: 95%; margin: 0px auto;">
            <tr id="rowCustDtl">
                <td style="text-align: center;" colspan="2" class="style2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="2" class="style2">
                    <asp:Panel ID="pnlCustDtl" runat="server">
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="2" class="style2">
                    <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                        value="SUBMIT" onclick="return btnSave_onclick()" /> <input
                            id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hdnDocDtl" runat="server" />
        <asp:HiddenField ID="hdnSaveDtl" runat="server" />
        <asp:HiddenField ID="hdnAttachDtl" runat="server" />
    </div>
    </form>
    <configuration>
        <system.web>
            </compilation debug="true" targetFramework="4.0">
        </system.web>
    </configuration>
</body>
</html>
