﻿Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.Drawing.Printing
Imports System.Drawing
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Diagnostics
Partial Class SearchCustomerView
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim pageIndex As Integer = 0
    Dim streams As IList(Of Stream)
    Dim rptds As ReportDataSource
    Dim rptds1 As ReportDataSource
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then

                'Dim USER_ID As Integer
                'USER_ID = CInt(GF.Decrypt(Request.QueryString.Get("USER_ID")))
                'Dim SEARCH_ID As Integer
                'SEARCH_ID = CInt(GF.Decrypt(Request.QueryString.Get("SEARCH_ID")))

                'Dim Params(6) As SqlParameter
                'Params(0) = New SqlParameter("@SEARCH_ID", SqlDbType.Int)
                'Params(0).Value = SEARCH_ID
                'Params(1) = New SqlParameter("@USER_ID", SqlDbType.Int)
                'Params(1).Value = USER_ID
                'Params(2) = New SqlParameter("@CustomerCount", SqlDbType.Int)
                'Params(2).Direction = ParameterDirection.Output
                'Params(3) = New SqlParameter("@CustomerDtl", SqlDbType.NVarChar, -1)
                'Params(3).Direction = ParameterDirection.Output
                'Params(4) = New SqlParameter("@SearchDtl", SqlDbType.VarChar, 1000)
                'Params(4).Direction = ParameterDirection.Output
                'Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                'Params(5).Direction = ParameterDirection.Output
                'Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 2000)
                'Params(6).Direction = ParameterDirection.Output

                'DB.ExecuteNonQuery("SP_CUSTOMER_SEARCH", Params)

                'Dim CustomerCount As Integer = CInt(Params(2).Value)
                'Dim ErrorFlag As Integer = CInt(Params(5).Value)
                'Dim Message As String = CStr(Params(6).Value)

                'If ErrorFlag = 1 Then
                '    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString(), False)
                'Else

                '    Dim SearchDtl As String = CStr(Params(4).Value)

                '    Dim ds1 As New DataTable()
                '    Dim col_UserID As DataColumn = ds1.Columns.Add("USER_ID", GetType(String))
                '    Dim col_UserName As DataColumn = ds1.Columns.Add("User_Name", GetType(String))
                '    Dim col_SearchOn As DataColumn = ds1.Columns.Add("Search_On", GetType(String))
                '    Dim col_DataAsOn As DataColumn = ds1.Columns.Add("DataAsOn", GetType(String))
                '    Dim col_Search_Name As DataColumn = ds1.Columns.Add("Customer_Name", GetType(String))
                '    Dim col_DOB As DataColumn = ds1.Columns.Add("Date_Of_Birth", GetType(String))
                '    Dim col_Address As DataColumn = ds1.Columns.Add("Address", GetType(String))
                '    Dim col_Address_Pincode As DataColumn = ds1.Columns.Add("Address_Pincode", GetType(String))
                '    Dim col_Mobile As DataColumn = ds1.Columns.Add("Mobile_Number", GetType(String))
                '    Dim col_Email As DataColumn = ds1.Columns.Add("Customer_Email", GetType(String))
                '    Dim col_AadharNo As DataColumn = ds1.Columns.Add("Aadhar_Number", GetType(String))
                '    Dim col_PanNo As DataColumn = ds1.Columns.Add("Pan_Number", GetType(String))
                '    Dim col_VotersID As DataColumn = ds1.Columns.Add("Voters_ID", GetType(String))
                '    Dim col_DrivingLicense As DataColumn = ds1.Columns.Add("Driving_License", GetType(String))
                '    Dim col_PassportNo As DataColumn = ds1.Columns.Add("Passport_Number", GetType(String))
                '    Dim col_FatherName As DataColumn = ds1.Columns.Add("Father_Name", GetType(String))
                '    Dim col_SpouseName As DataColumn = ds1.Columns.Add("Spouse_Name", GetType(String))
                '    Dim col_MotherName As DataColumn = ds1.Columns.Add("Mother_Name", GetType(String))

                '    Dim split1() As String = SearchDtl.Split(CChar("|"))

                '    Dim row1 As DataRow = ds1.NewRow()

                '    row1.SetField(col_Search_Name, split1(0))
                '    row1.SetField(col_DOB, split1(1))
                '    row1.SetField(col_Address, split1(2))
                '    row1.SetField(col_Address_Pincode, split1(3))
                '    row1.SetField(col_Mobile, split1(4))
                '    row1.SetField(col_Email, split1(5))
                '    row1.SetField(col_AadharNo, split1(6))
                '    row1.SetField(col_PanNo, split1(7))
                '    row1.SetField(col_VotersID, split1(8))
                '    row1.SetField(col_DrivingLicense, split1(9))
                '    row1.SetField(col_PassportNo, split1(10))
                '    row1.SetField(col_FatherName, split1(11))
                '    row1.SetField(col_SpouseName, split1(12))
                '    row1.SetField(col_MotherName, split1(13))
                '    row1.SetField(col_UserID, split1(14))
                '    row1.SetField(col_UserName, split1(15))
                '    row1.SetField(col_SearchOn, split1(16))
                '    row1.SetField(col_DataAsOn, split1(17))

                '    ds1.Rows.Add(row1)

                '    rptds1 = New ReportDataSource("DedupeDataSet", ds1)
                '    rptds1.Name = "SearchDetails"

                '    Dim ds As New DataTable()

                '    If (CustomerCount > 0) Then

                '        Dim CustomerDtl As String = CStr(Params(3).Value)
                '        Dim col_Branch_Code As DataColumn = ds.Columns.Add("Branch_Code", GetType(String))
                '        Dim col_Branch_Name As DataColumn = ds.Columns.Add("Branch_Name", GetType(String))
                '        Dim col_Customer_Number As DataColumn = ds.Columns.Add("Customer_Number", GetType(String))
                '        Dim col_Customer_Name As DataColumn = ds.Columns.Add("Customer_Name", GetType(String))
                '        Dim col_Address1 As DataColumn = ds.Columns.Add("Address1", GetType(String))
                '        Dim col_Address1_Pincode As DataColumn = ds.Columns.Add("Address1_Pincode", GetType(String))
                '        Dim col_Address2 As DataColumn = ds.Columns.Add("Address2", GetType(String))
                '        Dim col_Address2_Pincode As DataColumn = ds.Columns.Add("Address2_Pincode", GetType(String))
                '        Dim col_Date_Of_Birth As DataColumn = ds.Columns.Add("Date_Of_Birth", GetType(String))
                '        Dim col_Mobile_Number As DataColumn = ds.Columns.Add("Mobile_Number", GetType(String))
                '        Dim col_Customer_Email As DataColumn = ds.Columns.Add("Customer_Email", GetType(String))
                '        Dim col_Father_Name As DataColumn = ds.Columns.Add("Father_Name", GetType(String))
                '        Dim col_Spouse_Name As DataColumn = ds.Columns.Add("Spouse_Name", GetType(String))
                '        Dim col_Mother_Name As DataColumn = ds.Columns.Add("Mother_Name", GetType(String))
                '        Dim col_Aadhar_Number As DataColumn = ds.Columns.Add("Aadhar_Number", GetType(String))
                '        Dim col_Pan_Number As DataColumn = ds.Columns.Add("Pan_Number", GetType(String))
                '        Dim col_Voters_ID As DataColumn = ds.Columns.Add("Voters_ID", GetType(String))
                '        Dim col_Driving_License As DataColumn = ds.Columns.Add("Driving_License", GetType(String))
                '        Dim col_Passport_Number As DataColumn = ds.Columns.Add("Passport_Number", GetType(String))

                '        Dim lines() As String = CustomerDtl.Split(CChar("~"))

                '        For Each line As String In lines
                '            Dim split() As String = line.Split(CChar("|"))
                '            Dim row As DataRow = ds.NewRow()

                '            row.SetField(col_Branch_Code, split(0))
                '            row.SetField(col_Branch_Name, split(1))
                '            row.SetField(col_Customer_Number, split(2))
                '            row.SetField(col_Customer_Name, split(3))
                '            row.SetField(col_Address1, split(4))
                '            row.SetField(col_Address1_Pincode, split(5))
                '            row.SetField(col_Address2, split(6))
                '            row.SetField(col_Address2_Pincode, split(7))
                '            row.SetField(col_Date_Of_Birth, split(8))
                '            row.SetField(col_Mobile_Number, split(9))
                '            row.SetField(col_Customer_Email, split(10))
                '            row.SetField(col_Father_Name, split(11))
                '            row.SetField(col_Spouse_Name, split(12))
                '            row.SetField(col_Mother_Name, split(13))
                '            row.SetField(col_Aadhar_Number, split(14))
                '            row.SetField(col_Pan_Number, split(15))
                '            row.SetField(col_Voters_ID, split(16))
                '            row.SetField(col_Driving_License, split(17))
                '            row.SetField(col_Passport_Number, split(18))

                '            ds.Rows.Add(row)
                '        Next

                '        rptds = New ReportDataSource("DedupeDataSet", ds)
                '        rptds.Name = "CustomerSearchResult"

                '    End If

                '    If CustomerCount > 0 Then
                '        ReportViewer1.ProcessingMode = ProcessingMode.Local
                '        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Dedupe/Reports/CustomerSearchReport.rdlc")
                '        ReportViewer1.LocalReport.DataSources.Clear()
                '        ReportViewer1.LocalReport.DataSources.Add(rptds)
                '        ReportViewer1.LocalReport.DataSources.Add(rptds1)
                '        ReportViewer1.LocalReport.Refresh()
                '        Show_PDF(1)
                '    Else
                '        ReportViewer2.ProcessingMode = ProcessingMode.Local
                '        ReportViewer2.LocalReport.ReportPath = Server.MapPath("~/Dedupe/Reports/CustomerSearchReport_None.rdlc")
                '        ReportViewer2.LocalReport.DataSources.Clear()
                '        ReportViewer2.LocalReport.DataSources.Add(rptds1)
                '        ReportViewer2.LocalReport.Refresh()
                '        Show_PDF(2)
                '    End If

                'End If

                Dim USER_ID As Integer
                USER_ID = CInt(GF.Decrypt(Request.QueryString.Get("USER_ID")))
                Dim SEARCH_ID As Integer
                SEARCH_ID = CInt(GF.Decrypt(Request.QueryString.Get("SEARCH_ID")))

                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@SEARCH_ID", SqlDbType.Int)
                Params(0).Value = SEARCH_ID
                Params(1) = New SqlParameter("@USER_ID", SqlDbType.Int)
                Params(1).Value = USER_ID
                Params(2) = New SqlParameter("@SearchDtl", SqlDbType.VarChar, 1000)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 2000)
                Params(4).Direction = ParameterDirection.Output

                Dim ds As New DataTable()
                ds = DB.ExecuteDataSet("SP_CUSTOMER_SEARCH", Params).Tables(0)

                Dim CustomerCount As Integer = ds.Rows.Count
                Dim ErrorFlag As Integer = CInt(Params(3).Value)
                Dim Message As String = CStr(Params(4).Value)

                If ErrorFlag = 1 Then
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString(), False)
                Else

                    Dim SearchDtl As String = CStr(Params(2).Value)

                    Dim ds1 As New DataTable()
                    Dim col_UserID As DataColumn = ds1.Columns.Add("USER_ID", GetType(String))
                    Dim col_UserName As DataColumn = ds1.Columns.Add("User_Name", GetType(String))
                    Dim col_SearchOn As DataColumn = ds1.Columns.Add("Search_On", GetType(String))
                    Dim col_DataAsOn As DataColumn = ds1.Columns.Add("DataAsOn", GetType(String))
                    Dim col_Search_Name As DataColumn = ds1.Columns.Add("Customer_Name", GetType(String))
                    Dim col_DOB As DataColumn = ds1.Columns.Add("Date_Of_Birth", GetType(String))
                    Dim col_Address As DataColumn = ds1.Columns.Add("Address", GetType(String))
                    Dim col_Address_Pincode As DataColumn = ds1.Columns.Add("Address_Pincode", GetType(String))
                    Dim col_Mobile As DataColumn = ds1.Columns.Add("Mobile_Number", GetType(String))
                    Dim col_Email As DataColumn = ds1.Columns.Add("Customer_Email", GetType(String))
                    Dim col_AadharNo As DataColumn = ds1.Columns.Add("Aadhar_Number", GetType(String))
                    Dim col_PanNo As DataColumn = ds1.Columns.Add("Pan_Number", GetType(String))
                    Dim col_VotersID As DataColumn = ds1.Columns.Add("Voters_ID", GetType(String))
                    Dim col_DrivingLicense As DataColumn = ds1.Columns.Add("Driving_License", GetType(String))
                    Dim col_PassportNo As DataColumn = ds1.Columns.Add("Passport_Number", GetType(String))
                    Dim col_FatherName As DataColumn = ds1.Columns.Add("Father_Name", GetType(String))
                    Dim col_SpouseName As DataColumn = ds1.Columns.Add("Spouse_Name", GetType(String))
                    Dim col_MotherName As DataColumn = ds1.Columns.Add("Mother_Name", GetType(String))

                    Dim split1() As String = SearchDtl.Split(CChar("|"))

                    Dim row1 As DataRow = ds1.NewRow()

                    row1.SetField(col_Search_Name, split1(0))
                    row1.SetField(col_DOB, split1(1))
                    row1.SetField(col_Address, split1(2))
                    row1.SetField(col_Address_Pincode, split1(3))
                    row1.SetField(col_Mobile, split1(4))
                    row1.SetField(col_Email, split1(5))
                    row1.SetField(col_AadharNo, split1(6))
                    row1.SetField(col_PanNo, split1(7))
                    row1.SetField(col_VotersID, split1(8))
                    row1.SetField(col_DrivingLicense, split1(9))
                    row1.SetField(col_PassportNo, split1(10))
                    row1.SetField(col_FatherName, split1(11))
                    row1.SetField(col_SpouseName, split1(12))
                    row1.SetField(col_MotherName, split1(13))
                    row1.SetField(col_UserID, split1(14))
                    row1.SetField(col_UserName, split1(15))
                    row1.SetField(col_SearchOn, split1(16))
                    row1.SetField(col_DataAsOn, split1(17))

                    ds1.Rows.Add(row1)

                    rptds1 = New ReportDataSource("DedupeDataSet", ds1)
                    rptds1.Name = "SearchDetails"

                    If (CustomerCount > 0) Then

                        rptds = New ReportDataSource("DedupeDataSet", ds)
                        rptds.Name = "CustomerSearchResult"

                    End If

                    If CustomerCount > 0 Then
                        ReportViewer1.ProcessingMode = ProcessingMode.Local
                        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Dedupe/Reports/CustomerSearchReport.rdlc")
                        ReportViewer1.LocalReport.DataSources.Clear()
                        ReportViewer1.LocalReport.DataSources.Add(rptds)
                        ReportViewer1.LocalReport.DataSources.Add(rptds1)
                        ReportViewer1.LocalReport.Refresh()
                        Show_PDF(1)
                    Else
                        ReportViewer2.ProcessingMode = ProcessingMode.Local
                        ReportViewer2.LocalReport.ReportPath = Server.MapPath("~/Dedupe/Reports/CustomerSearchReport_None.rdlc")
                        ReportViewer2.LocalReport.DataSources.Clear()
                        ReportViewer2.LocalReport.DataSources.Add(rptds1)
                        ReportViewer2.LocalReport.Refresh()
                        Show_PDF(2)
                    End If

                End If

            End If
        Catch ex As Exception
            Response.Redirect("~/CatchException.aspx?ErrorNo=0", False)
        End Try
    End Sub
    Private Sub Show_PDF(reportType As Integer)
        Try
            'Dim deviceInfo As String = "<DeviceInfo>" + _
            '                            "  <OutputFormat>EMF</OutputFormat>" + _
            '                            "  <PageWidth>10.7in</PageWidth>" + _
            '                            "  <PageHeight>11in</PageHeight>" + _
            '                            "  <MarginTop>0.25in</MarginTop>" + _
            '                            "  <MarginLeft>0.25in</MarginLeft>" + _
            '                            "  <MarginRight>0.25in</MarginRight>" + _
            '                            "  <MarginBottom>0.25in</MarginBottom>" + _
            '                            "</DeviceInfo>"
            Dim deviceInfo As String = "<DeviceInfo>" + _
                                        "  <OutputFormat>EMF</OutputFormat>" + _
                                        "  <PageWidth>11.69in</PageWidth>" + _
                                        "  <PageHeight>8.27in</PageHeight>" + _
                                        "  <MarginTop>0.25in</MarginTop>" + _
                                        "  <MarginLeft>0.25in</MarginLeft>" + _
                                        "  <MarginRight>0.25in</MarginRight>" + _
                                        "  <MarginBottom>0.25in</MarginBottom>" + _
                                        "</DeviceInfo>"

            Dim warnings As Warning()
            Dim streamids As String()
            Dim mimeType As String = "application/Report.pdf"
            Dim encoding As String = String.Empty
            Dim filenameExtension As String = String.Empty
            Dim pdfContent As Byte()
            If (reportType = 1) Then
                pdfContent = ReportViewer1.LocalReport.Render("PDF", deviceInfo, mimeType, encoding, filenameExtension, streamids, warnings)
            Else
                pdfContent = ReportViewer2.LocalReport.Render("PDF", deviceInfo, mimeType, encoding, filenameExtension, streamids, warnings)
            End If
            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "Report.pdf")
            Response.BinaryWrite(pdfContent)
            Response.Flush()
        Catch ex As Exception
            MsgBox("Exception", MsgBoxStyle.Information, "")
            Response.Redirect("../../CatchException.aspx?ErrorNo=0")
        End Try

    End Sub
    Protected Sub PrintDoc()
        Export(ReportViewer1.LocalReport)
        pageIndex = 0
        Print()
    End Sub

    Private Sub Export(ByVal report As LocalReport)
        'Dim deviceInfo As String = "<DeviceInfo>" + _
        '                          "  <OutputFormat>EMF</OutputFormat>" + _
        '                          "  <PageWidth>8.5in</PageWidth>" + _
        '                          "  <PageHeight>11in</PageHeight>" + _
        '                          "  <MarginTop>0.25in</MarginTop>" + _
        '                          "  <MarginLeft>0.25in</MarginLeft>" + _
        '                          "  <MarginRight>0.25in</MarginRight>" + _
        '                          "  <MarginBottom>0.25in</MarginBottom>" + _
        '                          "</DeviceInfo>"
        Dim deviceInfo As String = "<DeviceInfo>" + _
                                   "  <OutputFormat>EMF</OutputFormat>" + _
                                   "  <PageWidth>11.69in</PageWidth>" + _
                                   "  <PageHeight>8.27in</PageHeight>" + _
                                   "  <MarginTop>0.25in</MarginTop>" + _
                                   "  <MarginLeft>0.25in</MarginLeft>" + _
                                   "  <MarginRight>0.25in</MarginRight>" + _
                                   "  <MarginBottom>0.25in</MarginBottom>" + _
                                   "</DeviceInfo>"
        Dim warnings() As Warning
        streams = New List(Of Stream)
        report.Render("Image", deviceInfo, AddressOf CreateStream, warnings)
        For Each stream As Stream In streams
            stream.Position = 0
        Next
    End Sub

    Private Function CreateStream(ByVal name As String, ByVal fileNameExtension As String, ByVal encoding As Encoding, ByVal mimeType As String, ByVal willSeek As Boolean) As Stream
        Dim stream As Stream = New FileStream((Server.MapPath("~/Files/") _
                        + (name + ("." + fileNameExtension))), FileMode.Create)
        streams.Add(stream)
        Return stream
    End Function

    Private Sub PrintPage(ByVal sender As Object, ByVal ev As PrintPageEventArgs)
        Dim pageImage As Metafile = New Metafile(streams(pageIndex))
        ev.Graphics.DrawImage(pageImage, ev.PageBounds)
        pageIndex = (pageIndex + 1)
        ev.HasMorePages = (pageIndex < streams.Count)
    End Sub

    Private Overloads Sub Print()
        If ((streams Is Nothing) _
                    OrElse (streams.Count = 0)) Then
            Return
        End If

        Dim printDoc As PrintDocument = New PrintDocument
        AddHandler printDoc.PrintPage, AddressOf Me.PrintPage
        printDoc.Print()
    End Sub

End Class
