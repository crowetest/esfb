﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" EnableEventValidation="false" CodeFile="BranchPendingReport.aspx.vb" Inherits="BranchPendingReport" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
        .style1
        {
            width: 15%;
        }
        .style2
        {
            width: 18%;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
           
           
            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        
            function btnShow_onclick() {
               
                var frmdate=document.getElementById("<%= txtStartDt.ClientID %>").value;
                var todate=document.getElementById("<%= txtToDt.ClientID %>").value;  
               

                window.open("ViewBranchPendingReport.aspx?frmdate=" + btoa(frmdate) + "&todate=" + btoa(todate) + "&StatusID=2", "_blank");
            }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
            <br />
    <div style="width: 80%; margin: 0px auto;">
        <table class="style1" style="width: 100%; top: 350px auto;">
            <tr class="style1">
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 12%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: justify; width: 22%;" class="style2">
                    Account Open From date &nbsp; &nbsp;
                </td>
                <td style="text-align: left; width: 22%;">
                    <asp:TextBox ID="txtStartDt" class="NormalText" runat="server" Width="98%" onkeypress="return false"
                        ReadOnly="True"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtStartDt" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
                <td style="text-align: left; margin: 0px auto; width: 12%;">
                </td>
                <td style="text-align: left; margin: 0px auto; width: 22%;">
                    Account Open To date &nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 22%;">
                    <asp:TextBox ID="txtToDt" class="NormalText" runat="server" Width="98%" onkeypress="return false"
                        ReadOnly="True"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtToDt" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 12%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 12%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                     &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="5">
                    <br />
                    <br />
                    <input id="btnShow" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                        value="SHOW" onclick="return btnShow_onclick()" onclick="return btnShow_onclick()" onclick="return btnShow_onclick()" />
                    &nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                        value="EXIT" onclick="return btnExit_onclick()" /></td>
            </tr>
        </table>
    </div>
            <asp:HiddenField ID="hdnApplication" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
