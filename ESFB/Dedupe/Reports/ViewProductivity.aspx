﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="ViewProductivity.aspx.vb" Inherits="ViewStaffRequestCloseStatus" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <br />
    <table align="center" style="width: 60%; text-align: center; margin: 0px auto;">
        <tr align="center">
            <td style="width: 15%; height: 18px; text-align: center;" colspan="4">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </ajaxToolkit:ToolkitScriptManager>
                    <input id="radEmp" type="radio" name="wise" value ="Employee Wise" onclick="return radEmp_onclick()"> Employee Wise &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input id="radDate" type="radio" name="wise" value ="Date Wise" onclick="return radDate_onclick()"> Date Wise               
            </td>
        </tr>
        <tr align="center">
            <td style="width: 15%; height: 18px; text-align: center;" colspan="4">
                    &nbsp;</td>
        </tr>
        <tr id="rowEmp">
            <td style="width: 15%;">
            </td>
            <td style="width: 15%;">
                Employee code
            </td>
            <td style="width: 55%;">
                <asp:TextBox ID="txtEmpcode" runat="server" Style="font-family: Cambria; font-size: 10pt;"
                    Width="50%" MaxLength="50" onkeypress='return NumericCheck(event)' />
            </td>
            <td style="width: 15%;">
            </td>
        </tr>
        <tr id="rowDate">
            <td style="width: 15%;">
            </td>
            <td style="width: 15%;">
               From Date
            </td>
            <td style="width: 55%;">
                <asp:TextBox ID="txtFrom" runat="server" class="NormalText" Width="50%" ReadOnly="true"></asp:TextBox>
                <asp:CalendarExtender ID="txtFrom_Extender" runat="server" TargetControlID="txtFrom"
                    Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>
            <td style="width: 15%">
               
            </td>
        </tr>
        <tr id="rowDateTo">
            <td style="width: 15%;">
            </td>
            <td style="width: 15%;">
               To Date
            </td>
            <td style="width: 55%;">
                <asp:TextBox ID="txtTo" runat="server" class="NormalText" Width="50%" ReadOnly="true"></asp:TextBox>
                <asp:CalendarExtender ID="txtTo_Extender" runat="server" TargetControlID="txtTo"
                    Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>
            <td style="width: 15%">
               
            </td>
        </tr>
        <tr id="rowDate">
            <td style="width: 15%;">
                &nbsp;</td>
            <td style="width: 15%;">
                &nbsp;</td>
            <td style="width: 55%;">
                &nbsp;</td>
            <td style="width: 15%">
             <asp:HiddenField ID="hdnReportID" runat="server" />
                <asp:HiddenField ID="hdnPostID" runat="server" />
                <asp:HiddenField ID="hdnLocationID" runat="server" />
                <asp:HiddenField ID="hdnName" runat="server" />
                <asp:HiddenField ID="hdnAdmin" runat="server" />
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 15%; height: 18px; text-align: center;" colspan="4">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                    value="VIEW" onclick="return btnView_onclick()" onclick="return btnView_onclick()" />&nbsp;&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">

        function window_onload() {

            document.getElementById("rowEmp").style.display = "none";
            document.getElementById("rowDate").style.display = "none";
             document.getElementById("rowDateTo").style.display = "none";
            
           
        }   

        function btnExit_onclick() {            
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        function btnView_onclick() {
            var CheckStatus = 0;
            if (document.getElementById("radEmp").checked == true) {
                var EmpCode = document.getElementById("<%= txtEmpcode.ClientID%>").value     
                var From = document.getElementById("<%= txtFrom.ClientID %>").value;  
                var To = document.getElementById("<%= txtTo.ClientID %>").value;                 
                CheckStatus = 1;
            }
            else {
                var From = document.getElementById("<%= txtFrom.ClientID %>").value;
                var To = document.getElementById("<%= txtTo.ClientID %>").value; 
                CheckStatus = 0;
            }
            window.open("ViewProductivity_Dtl.aspx?EmpCode=" + btoa(EmpCode) + " &Dateval=" + From + " &Status=" + CheckStatus + " &DateValTo=" + To , "_blank");

        }

        function radEmp_onclick() {            
            if (document.getElementById("<%= hdnAdmin.ClientID %>").value==0)
            {
              document.getElementById("<%= txtEmpcode.ClientID%>").disabled="true";
            }
            document.getElementById("rowEmp").style.display = "";
            document.getElementById("rowDate").style.display = "";
            document.getElementById("rowDateTo").style.display = "";
        }

        function radDate_onclick() {            
            document.getElementById("<%= txtFrom.ClientID %>").value = "";
            document.getElementById("<%= txtTo.ClientID %>").value = "";
            document.getElementById("rowEmp").style.display = "none";
            document.getElementById("rowDate").style.display = "";
            document.getElementById("rowDateTo").style.display = "";
            
        }

    </script>
</asp:Content>
