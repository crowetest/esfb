﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class BranchPendingReport
    Inherits System.Web.UI.Page
    'Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "DEDUPE 'YES' IDENTIFIED - PENDING AT BRANCH LEVEL"

        If GF.FormAccess(CInt(Session("UserID")), 1441) = False And CInt(Session("BranchID")) < 100 Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
    End Sub
#End Region

    
End Class
