﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class ViewStaffRequestCloseStatus
    Inherits System.Web.UI.Page

    Dim DT As New DataTable
    Dim GN As New GeneralFunctions

    Dim ReportID As Integer = 1
    Dim LocationID As Integer = 0
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim UserID As Integer = CInt(Session("UserID"))
            DT = DB.ExecuteDataSet("select count(dedupe_team_leader) from Dedupe_Team_master where dedupe_team_leader=" & UserID & "").Tables(0)
            If DT.Rows(0)(0) > "0" Then
                Me.hdnAdmin.Value = "1"
            Else
                Me.hdnAdmin.Value = "0"
                Me.txtEmpcode.Text = CInt(Session("UserID"))
            End If
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub


End Class
