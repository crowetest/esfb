﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewCKYCRpt_Dtl
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT, DT2 As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim StrFromDate As String = ""
            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(GF.Decrypt(Request.QueryString.Get("frmdate")))
            End If
            Dim StrToDate As String = ""
            If CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrToDate = CStr(GF.Decrypt(Request.QueryString.Get("todate")))
            End If

            Dim StrQuery As String = ""

            If CStr(Request.QueryString.Get("frmdate")) <> "" And CStr(Request.QueryString.Get("todate")) <> "" Then
                StrQuery += " where DATEADD(day,DATEDIFF(day, 0,tots.dt ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
            End If

            Dim SqlStr As String = ""
            'StrFromDate
            SqlStr = "select tots.dt,sum(tots.tot) tot,sum(tots.StatNull) StatNull,sum(tots.Stat1) Stat1,sum(tots.Stat2) Stat2,sum(tots.Stat10) Stat10,sum(tots.Stat3) Stat3,sum(tots.Stat4) Stat4,sum(tots.Stat5) Stat5,sum(tots.Stat11) Stat11,sum(tots.Stat12) Stat12,sum(tots.Stat13) Stat13  from ( " & _
                        "select NewCif_CreatedDt dt,count(New_Cif) tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification group by NewCif_CreatedDt " & _
            "union all " & _
                        "select NewCif_CreatedDt dt,0 tot, count(New_Cif) StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id is null group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, count(New_Cif) Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=1 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, count(New_Cif) Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=2 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, count(New_Cif) Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=10 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, count(New_Cif) Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=3 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, count(New_Cif) Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=4 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, count(New_Cif) Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=5 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, count(New_Cif) Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=11 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, count(New_Cif) Stat12, 0 Stat13 from dedupe_cust_verification where status_id=12 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, count(New_Cif) Stat13 from dedupe_cust_verification where status_id=13 group by NewCif_CreatedDt) tots " & StrQuery & " group by tots.dt order by 1"

            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            DT2 = DB.ExecuteDataSet("select status_id,status_name from Dedupe_Status_Master order by dedupe").Tables(0)
            Dim b As Integer = DT.Rows.Count

            RH.Heading(Session("FirmName"), tb, "Verification Dashboard", 150)

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRHead_1 As New TableRow

            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"
            TRHead_1_08.BorderWidth = "1"
            TRHead_1_09.BorderWidth = "1"
            TRHead_1_10.BorderWidth = "1"
            TRHead_1_11.BorderWidth = "1"
            TRHead_1_12.BorderWidth = "1"

            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver
            TRHead_1_08.BorderColor = Drawing.Color.Silver
            TRHead_1_09.BorderColor = Drawing.Color.Silver
            TRHead_1_10.BorderColor = Drawing.Color.Silver
            TRHead_1_11.BorderColor = Drawing.Color.Silver
            TRHead_1_12.BorderColor = Drawing.Color.Silver

            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid
            TRHead_1_08.BorderStyle = BorderStyle.Solid
            TRHead_1_09.BorderStyle = BorderStyle.Solid
            TRHead_1_10.BorderStyle = BorderStyle.Solid
            TRHead_1_11.BorderStyle = BorderStyle.Solid
            TRHead_1_12.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead_1, TRHead_1_00, 3, 3, "c", "Sl No")
            RH.AddColumn(TRHead_1, TRHead_1_01, 6, 6, "c", "Account Open Date")
            RH.AddColumn(TRHead_1, TRHead_1_02, 6, 6, "c", "Total")
            RH.AddColumn(TRHead_1, TRHead_1_03, 8, 8, "c", "NEW - DEDUPE PENDING AT MAKER LEVEL")
            RH.AddColumn(TRHead_1, TRHead_1_04, 8, 8, "c", DT2.Rows(0)(1))
            RH.AddColumn(TRHead_1, TRHead_1_05, 8, 8, "c", DT2.Rows(1)(1))
            RH.AddColumn(TRHead_1, TRHead_1_06, 8, 8, "c", DT2.Rows(2)(1))
            RH.AddColumn(TRHead_1, TRHead_1_07, 8, 8, "c", DT2.Rows(3)(1))
            RH.AddColumn(TRHead_1, TRHead_1_08, 9, 9, "c", DT2.Rows(4)(1))
            RH.AddColumn(TRHead_1, TRHead_1_09, 9, 9, "c", DT2.Rows(5)(1))
            RH.AddColumn(TRHead_1, TRHead_1_10, 9, 9, "c", DT2.Rows(6)(1))
            RH.AddColumn(TRHead_1, TRHead_1_11, 9, 9, "c", DT2.Rows(7)(1))
            RH.AddColumn(TRHead_1, TRHead_1_12, 9, 9, "c", DT2.Rows(8)(1))

            tb.Controls.Add(TRHead_1)
            Dim i As Integer = 0
            For Each DR In DT.Rows
                i = i + 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 3, 3, "c", i.ToString())
                RH.AddColumn(TR3, TR3_01, 6, 6, "c", CDate(DR(0)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_02, 6, 6, "c", DR(1).ToString())
                If CInt(DR(2)) = 0 Then
                    RH.AddColumn(TR3, TR3_03, 8, 8, "c", DR(2).ToString())
                Else
                    RH.AddColumn(TR3, TR3_03, 8, 8, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=0' style='text-align:right;' target='_blank'>" + DR(2).ToString() + "</a>")
                End If
                If CInt(DR(3)) = 0 Then
                    RH.AddColumn(TR3, TR3_04, 8, 8, "c", DR(3).ToString())
                Else
                    RH.AddColumn(TR3, TR3_04, 8, 8, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=1' style='text-align:right;' target='_blank'>" + DR(3).ToString() + "</a>")
                End If
                If CInt(DR(4)) = 0 Then
                    RH.AddColumn(TR3, TR3_05, 8, 8, "c", DR(4).ToString())
                Else
                    RH.AddColumn(TR3, TR3_05, 8, 8, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=2' style='text-align:right;' target='_blank'>" + DR(4).ToString() + "</a>")
                End If
                If CInt(DR(5)) = 0 Then
                    RH.AddColumn(TR3, TR3_06, 8, 8, "c", DR(5).ToString())
                Else
                    RH.AddColumn(TR3, TR3_06, 8, 8, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=10' style='text-align:right;' target='_blank'>" + DR(5).ToString() + "</a>")
                End If
                If CInt(DR(6)) = 0 Then
                    RH.AddColumn(TR3, TR3_07, 8, 8, "c", DR(6).ToString())
                Else
                    RH.AddColumn(TR3, TR3_07, 8, 8, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=3' style='text-align:right;' target='_blank'>" + DR(6).ToString() + "</a>")
                End If
                If CInt(DR(7)) = 0 Then
                    RH.AddColumn(TR3, TR3_08, 9, 9, "c", DR(7).ToString())
                Else
                    RH.AddColumn(TR3, TR3_08, 9, 9, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=4' style='text-align:right;' target='_blank'>" + DR(7).ToString() + "</a>")
                End If
                If CInt(DR(8)) = 0 Then
                    RH.AddColumn(TR3, TR3_09, 9, 9, "c", DR(8).ToString())
                Else
                    RH.AddColumn(TR3, TR3_09, 9, 9, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=5' style='text-align:right;' target='_blank'>" + DR(8).ToString() + "</a>")
                End If
                If CInt(DR(9)) = 0 Then
                    RH.AddColumn(TR3, TR3_10, 9, 9, "c", DR(9).ToString())
                Else
                    RH.AddColumn(TR3, TR3_10, 9, 9, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=11' style='text-align:right;' target='_blank'>" + DR(9).ToString() + "</a>")
                End If
                If CInt(DR(10)) = 0 Then
                    RH.AddColumn(TR3, TR3_11, 9, 9, "c", DR(10).ToString())
                Else
                    RH.AddColumn(TR3, TR3_11, 9, 9, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=12' style='text-align:right;' target='_blank'>" + DR(10).ToString() + "</a>")
                End If
                If CInt(DR(11)) = 0 Then
                    RH.AddColumn(TR3, TR3_12, 9, 9, "c", DR(11).ToString())
                Else
                    RH.AddColumn(TR3, TR3_12, 9, 9, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=13' style='text-align:right;' target='_blank'>" + DR(11).ToString() + "</a>")
                End If

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        Try
            Dim StrFromDate As String = ""
            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(GF.Decrypt(Request.QueryString.Get("frmdate")))
            End If
            Dim StrToDate As String = ""
            If CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrToDate = CStr(GF.Decrypt(Request.QueryString.Get("todate")))
            End If

            Dim StrQuery As String = ""

            If CStr(Request.QueryString.Get("frmdate")) <> "" And CStr(Request.QueryString.Get("todate")) <> "" Then
                StrQuery += " where DATEADD(day,DATEDIFF(day, 0,tots.dt ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
            End If
            Dim SqlStr As String = ""
            'StrFromDate
            SqlStr = "select tots.dt as [Account Open Date],sum(tots.tot)  as Toatal ,sum(tots.StatNull)  as [NEW - DEDUPE PENDING AT MAKER LEVEL] ,sum(tots.Stat1)  as[NEW - DEDUPE YES/NO FROM MAKER - PENDING AT CHECKER LEVEL] , " & _
             "   sum(tots.Stat2)  as [DEDUPE YES IDENTIFIED - PENDING AT BRANCH LEVEL] ,sum(tots.Stat10)  as [DEDUPE NO FROM MAKER - CONFIRMED AT CHECKER LEVEL] ,sum(tots.Stat3)  as [DEDUPE YES FROM BRANCH - PENDING AT RPT MAKER LEVEL] ,sum(tots.Stat4)  as [DEDUPE NO FROM BRANCH - PENDING AT CHECKER LEVEL] ,sum(tots.Stat5)  as [DEDUPE YES FROM RPT MAKER - PENDING AT RPT CHECKER LEVEL] ,sum(tots.Stat11)  as [DEDUPE NO FROM BRANCH - CONFIRMED AT CHECKER LEVELL] ,sum(tots.Stat12)  as [RETURN TO RPT MAKER - CONFIRMED AT RPT CHECKER LEVEL] ,sum(tots.Stat13)  as [DEDUPE RECTIFIED - CONFIRMED AT RPT CHECKER LEVEL] from ( " & _
                        "select NewCif_CreatedDt dt,count(New_Cif) tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification group by NewCif_CreatedDt " & _
            "union all " & _
                        "select NewCif_CreatedDt dt,0 tot, count(New_Cif) StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id is null group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, count(New_Cif) Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=1 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, count(New_Cif) Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=2 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, count(New_Cif) Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=10 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, count(New_Cif) Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=3 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, count(New_Cif) Stat4, 0 Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=4 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, count(New_Cif) Stat5, 0 Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=5 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, count(New_Cif) Stat11, 0 Stat12, 0 Stat13 from dedupe_cust_verification where status_id=11 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, count(New_Cif) Stat12, 0 Stat13 from dedupe_cust_verification where status_id=12 group by NewCif_CreatedDt " & _
            "union all  " & _
                        "select NewCif_CreatedDt dt,0 tot, 0 StatNull, 0 Stat1, 0 Stat2, 0 Stat10, 0 Stat3, 0 Stat4, 0 Stat5, 0 Stat11, 0 Stat12, count(New_Cif) Stat13 from dedupe_cust_verification where status_id=13 group by NewCif_CreatedDt) tots " & StrQuery & " group by tots.dt order by 1"

            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            WebTools.ExporttoExcel(DT, "Ticket")
            'Response.ContentType = "Ticket/pdf"
            'Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            'Dim sw As New StringWriter()
            'Dim hw As New HtmlTextWriter(sw)
            'pnDisplay.RenderControl(hw)
            'Dim sr As New StringReader(sw.ToString())
            'Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            'Dim htmlparser As New HTMLWorker(pdfDoc)
            'PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            'pdfDoc.Open()
            'htmlparser.Parse(sr)
            'pdfDoc.Close()
            'Response.Write(pdfDoc)
            'Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
