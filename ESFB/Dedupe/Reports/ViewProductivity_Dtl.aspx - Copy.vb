﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewCKYCRpt_Dtl
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT As New DataTable
    Dim DTExcel As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim Stat As Integer = CInt(Request.QueryString.Get("Status"))
            Dim SqlStr As String = ""
            Dim StrDateFrom As String = ""
            Dim StrDateTo As String = ""
            If (Stat = 1) Then
                Dim EmpCode As String
                If CStr(Request.QueryString.Get("Dateval")) <> "1" Then
                    StrDateFrom = CStr(Request.QueryString.Get("Dateval"))
                End If
                If CStr(Request.QueryString.Get("DatevalTo")) <> "1" Then
                    StrDateTo = CStr(Request.QueryString.Get("DatevalTo"))
                End If
                EmpCode = CStr(GF.Decrypt(Request.QueryString.Get("EmpCode")))
                ' SqlStr = "select a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name, count(*) MankerNew, 0 MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.MakerN=b.Emp_Code and a.MakerN=" & EmpCode & " group by a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name"
                'union all select a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name, 0 MankerNew, count(*) MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.MakerBY=b.Emp_Code and a.MakerBY=" & EmpCode & " group by a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name union all select a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, count(*) CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerN=b.Emp_Code and a.CheckerN=" & EmpCode & " group by a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name union all select a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, 0 CheckerNew,  count(*) CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerBN=b.Emp_Code and a.CheckerBN=" & EmpCode & " group by a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name union all select a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo, count(*) CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerBY=b.Emp_Code and a.CheckerBY=" & EmpCode & " group by a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name"
                SqlStr = "select cast(a.newcif_createdDt as date) as [Account open date],cast(a.MakerNDt as date) as [verified date], b.Emp_Code, b.Emp_Name, count(*) MankerNew, 0 MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.MakerN=b.Emp_Code and a.MakerN=" & EmpCode & " and cast(a.MakerNDt as date) between '" & StrDateFrom & "' and '" & StrDateTo & "' group by cast(a.MakerNDt as date),cast(a.newcif_createdDt as date), b.Emp_Code, b.Emp_Name " +
                " union all select cast(a.newcif_createdDt as date) as [Account open date],cast(a.MakerBYDt as date) as [verified date], b.Emp_Code, b.Emp_Name, 0 MankerNew, count(*) MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.MakerBY=b.Emp_Code and a.MakerBY=" & EmpCode & " and cast(a.MakerBYDt as date) between '" & StrDateFrom & "' and '" & StrDateTo & "' group by cast(a.MakerBYDt as date),cast(a.newcif_createdDt as date), b.Emp_Code, b.Emp_Name " +
                " union all select cast(a.newcif_createdDt as date) as [Account open date],cast(a.checkerNDt as date) as [verified date], b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, count(*) CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerN=b.Emp_Code and a.CheckerN=" & EmpCode & " and  cast(a.checkerNDt as date) between '" & StrDateFrom & "' and '" & StrDateTo & "'  group by cast(a.checkerNDt as date),cast(a.newcif_createdDt as date), b.Emp_Code, b.Emp_Name " +
               " union all select cast(a.newcif_createdDt as date) as [Account open date],cast(a.checkerBNDt as date) as [verified date], b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, 0 CheckerNew,  count(*) CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerBN=b.Emp_Code and a.CheckerBN=" & EmpCode & " and cast(a.checkerBNDt as date) between '" & StrDateFrom & "' and '" & StrDateTo & "' group by cast(a.checkerBNDt as date),cast(a.newcif_createdDt as date), b.Emp_Code, b.Emp_Name " +
               " union all select cast(a.newcif_createdDt as date) as [Account open date],cast(a.checkerBYDt as date) as [verified date], b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo, count(*) CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerBY=b.Emp_Code and a.CheckerBY=" & EmpCode & " and cast(a.checkerBYDt as date)  between '" & StrDateFrom & "' and '" & StrDateTo & "'  group by cast(a.checkerBYDt as date),cast(a.newcif_createdDt as date), b.Emp_Code, b.Emp_Name "


                RH.Heading(Session("FirmName"), tb, "Productivity Dashboard", 150)
            Else
                If CStr(Request.QueryString.Get("Dateval")) <> "1" Then
                    StrDateFrom = CStr(Request.QueryString.Get("Dateval"))
                End If
                If CStr(Request.QueryString.Get("DatevalTo")) <> "1" Then
                    StrDateTo = CStr(Request.QueryString.Get("DatevalTo"))
                End If
                'SqlStr = "select a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name, count(a.MakerN) MankerNew, 0 MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.MakerN=b.Emp_Code and convert(date,a.MakerNDt)='" & StrDate & "' group by a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name union all select a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name, 0 MankerNew, count(a.MakerBY) MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.MakerBY=b.Emp_Code and convert(date,a.MakerBYDt)='" & StrDate & "' group by a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name union all select a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, count(a.CheckerN) CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerN=b.Emp_Code and convert(date,a.CheckerNDt)='" & StrDate & "' group by a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name union all select a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, 0 CheckerNew,  count(a.CheckerBN) CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerBN=b.Emp_Code and convert(date,a.CheckerBNDt)='" & StrDate & "' group by a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name union all select a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo, count(a.CheckerBY) CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerBY=b.Emp_Code and convert(date,a.CheckerBYDt)='" & StrDate & "' group by a.NewCif_CreatedDt, b.Emp_Code, b.Emp_Name"
                SqlStr = " select convert(date,a.newcif_createdDt) as [Account open date],convert(date,a.MakerNDt) as [verified date], b.Emp_Code, b.Emp_Name, count(*) MankerNew, 0 MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.MakerN=b.Emp_Code and convert(date,a.MakerNDt) between '" & StrDateFrom & "' and '" & StrDateTo & "' group by convert(date,a.MakerNDt),convert(date,a.newcif_createdDt), b.Emp_Code, b.Emp_Name " +
 " union all select convert(date,a.newcif_createdDt) as [Account open date],convert(date,a.MakerBYDt)  as [verified date], b.Emp_Code, b.Emp_Name, 0 MankerNew, count(*) MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.MakerBY=b.Emp_Code and convert(date,a.MakerBYDt) between '" & StrDateFrom & "' and '" & StrDateTo & "' group by convert(date,a.MakerBYDt),convert(date,a.newcif_createdDt), b.Emp_Code, b.Emp_Name " +
" union all select convert(date,a.newcif_createdDt) as [Account open date],convert(date,a.checkerNDt) as [verified date], b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, count(*) CheckerNew, 0 CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerN=b.Emp_Code  and convert(date,a.checkerNDt)  between '" & StrDateFrom & "' and '" & StrDateTo & "'  group by convert(date,a.checkerNDt),convert(date,a.newcif_createdDt), b.Emp_Code, b.Emp_Name " +
" union all select convert(date,a.newcif_createdDt) as [Account open date],convert(date,a.checkerBNDt) as [verified date], b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, 0 CheckerNew,  count(*) CheckerBrachNo,0 CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerBN=b.Emp_Code  and convert(date,a.checkerBNDt) between '" & StrDateFrom & "' and '" & StrDateTo & "' group by convert(date,a.checkerBNDt),convert(date,a.newcif_createdDt), b.Emp_Code, b.Emp_Name " +
" union all select convert(date,a.newcif_createdDt) as [Account open date],convert(date,a.checkerBYDt) as [verified date], b.Emp_Code, b.Emp_Name, 0 MankerNew, 0 MakerBranchYes, 0 CheckerNew, 0 CheckerBrachNo, count(*) CheckerBranchYes  from dedupe_cust_verification a, emp_master b where a.CheckerBY=b.Emp_Code  and convert(date,a.checkerBYDt) between '" & StrDateFrom & "' and '" & StrDateTo & "'  group by convert(date,a.checkerBYDt),convert(date,a.newcif_createdDt), b.Emp_Code, b.Emp_Name"

                RH.Heading(Session("FirmName"), tb, "Productivity Dashboard from " + StrDateFrom + " To " + StrDateTo, 150)
            End If

            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim b As Integer = DT.Rows.Count



            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRHead_1 As New TableRow

            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09 As New TableCell
            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"
            TRHead_1_08.BorderWidth = "1"
            TRHead_1_09.BorderWidth = "1"

            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver
            TRHead_1_08.BorderColor = Drawing.Color.Silver
            TRHead_1_09.BorderColor = Drawing.Color.Silver

            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid
            TRHead_1_08.BorderStyle = BorderStyle.Solid
            TRHead_1_09.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead_1, TRHead_1_00, 5, 5, "c", "Sl No")
            RH.AddColumn(TRHead_1, TRHead_1_01, 15, 15, "c", "Account open Date")
            RH.AddColumn(TRHead_1, TRHead_1_02, 15, 15, "c", "Verified Date")
            RH.AddColumn(TRHead_1, TRHead_1_03, 5, 5, "c", "Verified EmpCode")
            RH.AddColumn(TRHead_1, TRHead_1_04, 10, 10, "c", "Verified EmpName")
            RH.AddColumn(TRHead_1, TRHead_1_05, 10, 10, "c", "MakerNew")
            RH.AddColumn(TRHead_1, TRHead_1_06, 10, 10, "c", "MakerBranchYes")
            RH.AddColumn(TRHead_1, TRHead_1_07, 10, 10, "c", "CheckerNew")
            RH.AddColumn(TRHead_1, TRHead_1_08, 10, 10, "c", "CheckerBranchNo")
            RH.AddColumn(TRHead_1, TRHead_1_09, 10, 10, "c", "CheckerBranchYes")

            tb.Controls.Add(TRHead_1)
            Dim i As Integer = 0
            For Each DR In DT.Rows
                i = i + 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", i.ToString())
                RH.AddColumn(TR3, TR3_01, 15, 15, "c", CDate(DR(0)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_02, 15, 15, "c", CDate(DR(1)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_03, 5, 5, "c", DR(2))
                RH.AddColumn(TR3, TR3_04, 10, 10, "c", DR(3))
                RH.AddColumn(TR3, TR3_05, 10, 10, "c", DR(4))
                RH.AddColumn(TR3, TR3_06, 10, 10, "c", DR(5))
                RH.AddColumn(TR3, TR3_07, 10, 10, "c", DR(6))
                RH.AddColumn(TR3, TR3_08, 10, 10, "c", DR(7))
                RH.AddColumn(TR3, TR3_09, 10, 10, "c", DR(8))

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    'Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
    '    Try
    '        Response.ContentType = "Ticket/pdf"
    '        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
    '        Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '        Dim sw As New StringWriter()
    '        Dim hw As New HtmlTextWriter(sw)
    '        pnDisplay.RenderControl(hw)
    '        Dim sr As New StringReader(sw.ToString())
    '        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
    '        Dim htmlparser As New HTMLWorker(pdfDoc)
    '        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
    '        pdfDoc.Open()
    '        htmlparser.Parse(sr)
    '        pdfDoc.Close()
    '        Response.Write(pdfDoc)
    '        Response.[End]()
    '    Catch ex As Exception
    '        If Response.IsRequestBeingRedirected Then
    '            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
    '            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
    '        End If
    '    End Try
    'End Sub

    'Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
    '    Try
    '        WebTools.ExporttoExcel(DT, "RejectedList")
    '    Catch ex As Exception
    '        If Response.IsRequestBeingRedirected Then
    '            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
    '            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
    '        End If
    '    End Try
    'End Sub

    'Protected Sub cmd_Text_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Text.Click
    '    Try
    '        Dim str As New StringBuilder()
    '        str.Append("CIF ID")
    '        str.AppendLine()
    '        str.Append("-----------------------------------------")
    '        str.AppendLine()
    '        For i As Integer = 0 To DT.Rows.Count - 1
    '            For j As Integer = 0 To DT.Columns.Count - 1
    '                str.Append(DT.Rows(i)(j).ToString())
    '            Next
    '            str.AppendLine()
    '        Next

    '        Response.Clear()
    '        Response.ClearHeaders()
    '        Page.Response.ClearContent()
    '        Response.AddHeader("content-disposition", "attachment;filename=FileName.txt")
    '        Response.Charset = ""
    '        Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '        Response.ContentType = "text/plain"

    '        Response.Write(str.ToString())
    '        Response.[End]()
    '    Catch ex As Exception
    '        If Response.IsRequestBeingRedirected Then
    '            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
    '            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
    '        End If
    '    End Try
    'End Sub
End Class
