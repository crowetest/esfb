﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Filter_Application
    Inherits System.Web.UI.Page
    'Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "COPS DASHBOARD"

        If GF.FormAccess(CInt(Session("UserID")), 1346) = False And CInt(Session("BranchID")) < 100 Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        'Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        'Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        'If Not IsPostBack Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        'End If
    End Sub
#End Region

    '#Region "Call Back"
    '    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
    '        Return CallBackReturn
    '    End Function

    '    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
    '        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
    '        Dim DR As DataRow
    '        Select Case CInt(Data(0))
    '            Case 1 'Fill Application
    '                DT = GF.GetQueryResult("select -1, 'ALL' union all select application_id,application_name from app_application_master where status_id=1")
    '                If DT.Rows.Count > 0 Then
    '                    For Each DR In DT.Rows
    '                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
    '                    Next
    '                End If
    '        End Select
    '    End Sub
    '#End Region


End Class
