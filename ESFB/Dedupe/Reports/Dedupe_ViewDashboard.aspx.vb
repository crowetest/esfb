﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewCKYCRpt_Dtl
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT, DT2 As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim StrFromDate As String = ""
            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(GF.Decrypt(Request.QueryString.Get("frmdate")))
            End If
            Dim StrToDate As String = ""
            If CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrToDate = CStr(GF.Decrypt(Request.QueryString.Get("todate")))
            End If
            Dim StatusID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("status")))
            Dim StrQuery As String = ""
            Dim StrSubQuery As String = ""
            Dim HeadStr As String = ""

            If CStr(Request.QueryString.Get("frmdate")) <> "" And CStr(Request.QueryString.Get("todate")) <> "" Then
                StrQuery += " where DATEADD(day,DATEDIFF(day, 0,tots.dt ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
                If StatusID = 1 Then 'Dedupe Rectified
                    HeadStr += "DEDUPE RECTIFIED"
                Else ' Dedupe No
                    HeadStr += "'DEDUPE NO' CONFIRMED"
                End If
            End If

            Dim SqlStr As String = ""
            'StrFromDate
            SqlStr = "select tots.dt,sum(tots.Stat10) Stat10, sum(tots.Stat11) Stat11,sum(tots.Stat13) Stat13 from ( select NewCif_CreatedDt dt, count(New_Cif) Stat10, 0 Stat11, 0 Stat13 from dedupe_cust_verification where status_id=10 group by NewCif_CreatedDt union all  select NewCif_CreatedDt dt,0 Stat10, count(New_Cif) Stat11, 0 Stat13 from dedupe_cust_verification where status_id=11 group by NewCif_CreatedDt union all  select NewCif_CreatedDt dt,0 Stat10, 0 Stat11, count(New_Cif) Stat13 from dedupe_cust_verification where status_id=13 group by NewCif_CreatedDt) tots " & StrQuery & " group by tots.dt order by 1"

            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            DT2 = DB.ExecuteDataSet("select status_id,status_name from Dedupe_Status_Master where status_id in(10,11,13) order by dedupe").Tables(0)
            Dim b As Integer = DT.Rows.Count

            RH.Heading(Session("FirmName"), tb, "Verification Dashboard", 150)

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            If StatusID = 2 Then 'Dedupe No Confirmed
                Dim TRHead_1 As New TableRow

                Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03 As New TableCell

                TRHead_1_00.BorderWidth = "1"
                TRHead_1_01.BorderWidth = "1"
                TRHead_1_02.BorderWidth = "1"
                TRHead_1_03.BorderWidth = "1"

                TRHead_1_00.BorderColor = Drawing.Color.Silver
                TRHead_1_01.BorderColor = Drawing.Color.Silver
                TRHead_1_02.BorderColor = Drawing.Color.Silver
                TRHead_1_03.BorderColor = Drawing.Color.Silver

                TRHead_1_00.BorderStyle = BorderStyle.Solid
                TRHead_1_01.BorderStyle = BorderStyle.Solid
                TRHead_1_02.BorderStyle = BorderStyle.Solid
                TRHead_1_03.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TRHead_1, TRHead_1_00, 10, 10, "c", "Sl No")
                RH.AddColumn(TRHead_1, TRHead_1_01, 30, 30, "c", "Account Open Date")
                RH.AddColumn(TRHead_1, TRHead_1_02, 30, 30, "c", DT2.Rows(0)(1))
                RH.AddColumn(TRHead_1, TRHead_1_03, 30, 30, "c", DT2.Rows(1)(1))

                tb.Controls.Add(TRHead_1)
                Dim i As Integer = 0
                For Each DR In DT.Rows
                    i = i + 1
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid
                    Dim TR3_00, TR3_01, TR3_02, TR3_03 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"

                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver

                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TR3, TR3_00, 10, 10, "c", i.ToString())
                    RH.AddColumn(TR3, TR3_01, 30, 30, "c", CDate(DR(0)).ToString("dd/MMM/yyyy"))
                    If CInt(DR(1)) = 0 Then
                        RH.AddColumn(TR3, TR3_02, 30, 30, "c", DR(1).ToString())
                    Else
                        RH.AddColumn(TR3, TR3_02, 30, 30, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=10' style='text-align:right;' target='_blank'>" + DR(1).ToString() + "</a>")
                    End If
                    If CInt(DR(2)) = 0 Then
                        RH.AddColumn(TR3, TR3_03, 30, 30, "c", DR(2).ToString())
                    Else
                        RH.AddColumn(TR3, TR3_03, 30, 30, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=11' style='text-align:right;' target='_blank'>" + DR(2).ToString() + "</a>")
                    End If
                    tb.Controls.Add(TR3)
                Next
            Else
                Dim TRHead_1 As New TableRow

                Dim TRHead_1_00, TRHead_1_01, TRHead_1_02 As New TableCell

                TRHead_1_00.BorderWidth = "1"
                TRHead_1_01.BorderWidth = "1"
                TRHead_1_02.BorderWidth = "1"

                TRHead_1_00.BorderColor = Drawing.Color.Silver
                TRHead_1_01.BorderColor = Drawing.Color.Silver
                TRHead_1_02.BorderColor = Drawing.Color.Silver

                TRHead_1_00.BorderStyle = BorderStyle.Solid
                TRHead_1_01.BorderStyle = BorderStyle.Solid
                TRHead_1_02.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TRHead_1, TRHead_1_00, 10, 10, "c", "Sl No")
                RH.AddColumn(TRHead_1, TRHead_1_01, 45, 45, "c", "Account Open Date")
                RH.AddColumn(TRHead_1, TRHead_1_02, 45, 45, "c", DT2.Rows(2)(1))

                tb.Controls.Add(TRHead_1)
                Dim i As Integer = 0
                For Each DR In DT.Rows
                    i = i + 1
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid
                    Dim TR3_00, TR3_01, TR3_02 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"

                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver

                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TR3, TR3_00, 10, 10, "c", i.ToString())
                    RH.AddColumn(TR3, TR3_01, 45, 45, "c", CDate(DR(0)).ToString("dd/MMM/yyyy"))
                    If CInt(DR(3)) = 0 Then
                        RH.AddColumn(TR3, TR3_02, 45, 45, "c", DR(3).ToString())
                    Else
                        RH.AddColumn(TR3, TR3_02, 45, 45, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=13' style='text-align:right;' target='_blank'>" + DR(3).ToString() + "</a>")
                    End If
                    tb.Controls.Add(TR3)
                Next
            End If
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        Try
            Response.ContentType = "Ticket/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
