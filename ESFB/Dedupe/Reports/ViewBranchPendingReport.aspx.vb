﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewBranchPendingReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb, tb2 As New Table
    Dim DT, DT2 As New DataTable
    Dim streams As IList(Of Stream)
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Export_Excel_Click()


    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub
    Protected Sub Export_Excel_Click()
        Try
            Dim StrFromDate As String = ""
            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(GF.Decrypt(Request.QueryString.Get("frmdate")))
            End If
            Dim StrToDate As String = ""
            If CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrToDate = CStr(GF.Decrypt(Request.QueryString.Get("todate")))
            End If
            Dim StatusID As String = CStr(2)

            Dim StrQuery As String = ""

            If CStr(Request.QueryString.Get("frmdate")) <> "" Then
                StrQuery += "  and DATEADD(day,DATEDIFF(day, 0,a.NewCif_CreatedDt),0) between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "' and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
            End If

            Dim SqlStr As String = ""

            SqlStr = "select a.dedupeid as [Dedupe ID],a.newCif_createddt as [New CIF Created Date],a.new_cif as [New CIF],a.newcif_name as [New CIF Name],a.newcif_brcode as [New CIF Branch Code],b.branch_name as [New CIF Branch Name], a.newCif_dob as [New CIF DOB],a.newcif_pin as [New CIF Pin],a.newcif_mob as [New CIF Mob], a.newcif_email as [New CIF Email], a.newcif_pan as [New CIF Pan], a.NewCif_Dlicense as [New CIF Driving License],a.newcif_passport as [New CIF Passport], a.newcif_voterid as [New CIF Voter ID], a.newcif_aadhar as [New CIF Aadhar], a.newcif_father as [New CIF Father],a.newcif_mother as [New CIF Mother], a.newcif_address as [New CIF Address], a.dupecif_createddt as [Dupe CIF Created Date], a.dupe_cif as [Dupe CIF],a.dupecif_brcode as [Dupee CIF Branch Name], a.dupecif_dob as [Dupe CIF DOB], a.dupecif_pin as [Dupe CIF Pin],a.dupecif_mob as [Dupe CIF Mobile],a.dupecif_email as [Dupe CIF Email], a.dupecif_pan as [Dupe CIF Pan],a.dupecif_dlicense as [Dupe CIF Driving license],a.dupecif_passport as [Dupe CIF Passport],a.dupecif_voterid as [Dupe CIF Voter ID],a.dupecif_aadhar as [Dupe CIF Aadhar],a.dupecif_father as [Dupe CIF Father],a.dupecif_mother as [Dupe CIF Mother],a.dupecif_address as [Dupe CIF Address],a.equalstatus as [Equal Status],a.makern as [Maker N],a.makerndt as [Maker N Date],a.makernflg as [Maker N Flag],a.makerby as [Maker By],a.makerbydt as [Maker By Date],a.makerbyflg as [Maker By Flag],a.checkern as [Checker N],a.checkerndt as [Checker N Date],a.checkernflg as [Checker N Flag], a.checkerbn AS [Checker BN], a.checkerbndt as [Checker BN Date],a.checkerbnflg as [Checker BN Flag],a.checkerby as [Checker By],a.checkerbydt as [Checker By Date],a.checkerbyflg as [Checker By Flag],a.branch as [Branch],a.branchdt as [Branch Date],a.branchflg as [Branch Flag],a.status_id as [Status ID],a.updateid as [Update ID],a.empcode as [Emp Code],a.makerremark as [Maker Remark],a.checkerremark as [Checker Remark],a.branchremark as [Branch Remark],a.deactivatecif as [Deactivate CIF] from dedupe_cust_verification a left join branch_master b on b.branch_id = a.newcif_brcode left join branch_master c on c.branch_id = a.dupecif_brcode where a.status_id = 2 " + StrQuery + ""

            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            WebTools.ExporttoExcel(DT, "Dedupe Excel Report")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
