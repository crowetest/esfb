﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  
CodeFile="Branch_Verification.aspx.vb" Inherits="BRDRequestApproval" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function table_fill() {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:11%;text-align:left' >New CIF Open Date</td>";
            tab += "<td style='width:11%;text-align:left' >New CIF</td>";
            tab += "<td style='width:11%;text-align:left'>Dedupe CIF Open Date</td>";
            tab += "<td style='width:11%;text-align:left'>Dedupe Identified CIF</td>";
            tab += "<td style='width:11%;text-align:left'>Maker</td>";
            tab += "<td style='width:11%;text-align:left'>Maker Date</td>";
            tab += "<td style='width:11%;text-align:left'>Checker</td>";
            tab += "<td style='width:11%;text-align:left'>Checker Date</td>";
            tab += "<td style='width:10%;text-align:center'>Action</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                    //c.app_request_id,d.app_dtl_id,j.app_name,h.branch_name,d.emp_code, f.Emp_Name,e.App_Type_name,d.Remarks,g.App_role_name,i.branch_name as To_branch
                    tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:11%;text-align:left' >" + col[1] + "</td>";
                    tab += "<td style='width:11%;text-align:left' >" + col[2] + "</td>";
                    tab += "<td style='width:11%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:11%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:11%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:11%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:11%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:11%;text-align:left'>" + col[8] + "</td>"; 
                    tab += "<td style='width:10%;text-align:left'><a href='Cust_Verification_Branch.aspx?DedupeID=" + col[0] + "'>Dedupe Yes</a></td>"; 
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//

        }

        function btnApprove_onclick()
        {
            var ret = UpdateValue();
            if (ret == 0) return false;
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                alert("Select Any Request for Approval");
                return false;
            }

            var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
            var Data = "1Ø" + strempcode;
            ToServer(Data, 1);
        }               
                                
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

