<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Login" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ESFB</title>
    <link rel="shortcut icon" href="~/Image/favicon.ico" type="image/x-icon" />
    <link href="Style/PortalStyle.css" rel="stylesheet" type="text/css" />    
    <script src="Script/Encryption.js" type="text/javascript"> </script>   
    
  

<script language="javascript">
    document.onmousedown = disableclick;
    status = "Right Click Disabled";
    function disableclick(event) {
        if (event.button == 2) { 
            alert(status);
            return false;
        }
    }
</script>



<script language="javascript" type="text/javascript">
    var Flag = "True";
    function window_onload(Flag) {

//        alert(Flag);
        var x = document.getElementById("aa");
        if (Flag == "True") {
            x.style.display = "";
        }
        else{
            x.style.display = "none";
            }
    createSum();
    document.getElementById("<%= txt_user_id.ClientID %>").value = "";
    document.getElementById("<%= txt_password.ClientID %>").value = "";
    }

    function LoginOnClick() {
        document.body.style.cursor = 'wait';
        document.getElementById("<%= cmd_login.ClientID %>").enabled = false;
        var UserID = document.getElementById("<%= txt_user_id.ClientID %>").value;
        var Pswd = document.getElementById("<%= hidOrgPwd.ClientID %>").value
        if (UserID == "")
        { alert("Enter User Name"); document.getElementById("<%= txt_user_id.ClientID %>").focus(); return false; }
        if (Pswd == "")
        { alert("Enter Password"); document.getElementById("<%= txt_password.ClientID %>").focus(); return false; }
        document.getElementById("<%= hdnUserID.ClientID %>").value = UserID;
        document.getElementById("<%= hdnPswd.ClientID %>").value = Pswd;
        checkInput();
    }
    var total;
    function getRandom() {
        return Math.ceil(Math.random() * 10);
    }
    function getLeftRandom() {
        return Math.ceil(Math.random() * 10 + 10);
    }
    function getRightRandom() {
        return Math.ceil(Math.random() * 10);
    }
    function createSum() {
        var randomNum1 = getLeftRandom(),
			    randomNum2 = getRightRandom();
        var operators = [{
            sign: "+",
            method: function (a, b) { return a + b; }
        }, {
            sign: "-",
            method: function (a, b) { return a - b; }
        }];
        var selectedOperator = Math.floor(Math.random() * operators.length);
        document.getElementById('operation').innerHTML = operators[selectedOperator].sign;
        total = operators[selectedOperator].method(randomNum1, randomNum2);
        document.getElementById('txtA1').value = randomNum1;
        document.getElementById('txtA2').value = randomNum2;
    }
    function checkInput() {
        var input = document.getElementById("txtAns").value,
    	    slideSpeed = 200,
            hasInput = !!input,
            valid = hasInput && input == total;
        document.getElementById("<%= hidSum.ClientID %>").value = valid;
        $('#message').toggle(!hasInput);
        $('#success').toggle(valid);
        $('#fail').toggle(hasInput && !valid);
    }
    function PwdOnChange() {
            var string = document.getElementById("<%= txt_password.ClientID %>").value;
            var key = "<%=System.Configuration.ConfigurationManager.AppSettings("keyStr")%>";
            var encodedString = encrypt(string, key);
            <%--document.getElementById("<%= hidOrgPwd.ClientID %>").value = document.getElementById("<%= txt_password.ClientID %>").value;--%>
            document.getElementById("<%= hidOrgPwd.ClientID %>").value = encodedString;
            document.getElementById("<%= txt_password.ClientID %>").value = "00000000000000000000";
        }
 </script>
</head>
<body onload="pageInit(); width:100%; height:100%" style="padding-top:0px;">
    <form id="login_form" runat="server">
  
        <div style="height:120px; padding-top:0px; padding-top:10px; padding-bottom:10px;">
           <img src="Image/Logo.png" style="width:242px; height:62px;"/></div>
             <div style="height:20px; padding-top:0px; background-color:#034EA2; padding-top:10px; padding-bottom:10px;">
           </div>
        <div id="MainContents" style="background-color:White; height:250px; float:none;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                <tr>
                    <td style="height: 21px; text-align: center">
                        <span style="font-size: 14pt; color: #808080; font-family: Cambria">
                        <br />
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="height: 21px; text-align: left">
                        <span style="color: #0000ff; font-family: Cambria; text-decoration: underline">
                        </span>
                        
                    </td>
                </tr>        
                <tr>
                    <td style="height: 201px; text-align: center;">
                        <br /><br /><br />
                        <table border="0" align="center" width="30%">
                           
                            <tr>
                            <td style="width: 10%; height: 26px; text-align: right;"><span style="font-family: Cambria">User ID</span><span style="color: #ff0000">*&nbsp;</span></td>
                                <td colspan="2" style="width: 10%; height: 26px">
                                    <%--<asp:TextBox ID="txt_user_id" runat="server" Height="16px" MaxLength="5" TabIndex="1" placeholder="User Name" Width="80%" Font-Names="Cambria" BorderStyle="Ridge" BorderWidth="1px"></asp:TextBox>--%>
                                    <asp:TextBox ID="txt_user_id" runat="server" Height="16px" MaxLength="10" TabIndex="1" placeholder="User Name" Width="80%" Font-Names="Cambria" BorderStyle="Ridge" BorderWidth="1px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                            <td style="width: 10%; height: 26px; text-align: right;"><span style="color: #ff0000"></span><span style="font-family: Cambria">Password</span><span style="color: #ff0000">*&nbsp;</span></td>
                                
                                <td style="width: 10%; height: 26px" colspan="2">
                                    <%--<asp:TextBox ID="txt_password" runat="server" Height="16px" MaxLength="20" TabIndex="2" placeholder="Password" Width="80%" TextMode="Password" Font-Names="Cambria" BorderStyle="Ridge" BorderWidth="1px">admin</asp:TextBox>--%>
                                    <asp:TextBox ID="txt_password" runat="server" Height="16px" MaxLength="20" TabIndex="2" placeholder="Password" Width="80%" TextMode="Password" Font-Names="Cambria" BorderStyle="Ridge" BorderWidth="1px">admin</asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%; height: 26px; text-align: right;"></td>
                                <td style="width: 10%; height: 26px" colspan="2">
                                    <input id="txtA1"  type="text" onkeypress="return NumericCheck(event)" autocomplete="off" style="width:20%;font-family:Cambria;border-style:ridge;border-width:1px;height:20px;text-align:center;" readonly="readonly">
                                    <span id="operation" style="font-weight:bolder;"></span>
                                    <input id="txtA2"  type="text" onkeypress="return NumericCheck(event)" required style="width:20%;font-family:Cambria;border-style:ridge;border-width:1px;height:20px;text-align:center;" readonly="readonly"> 
                                    &nbsp;<span style="font-weight:bolder;">=</span>&nbsp;
                                    <input id="txtAns"  type="text" onkeypress="return NumericCheck(event)" required style="width:20%;font-family:Cambria;border-style:ridge;border-width:1px;height:20px;text-align:center;">   
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="3" style="height: 23px">
                                    <br />
                                    <asp:ImageButton ID="cmd_login" runat="server" Height="120px" Width="120px" ImageAlign="AbsMiddle" ImageUrl="~/Image/Login.png" ToolTip="Click to Login"/>
                                       <br />
                                     <br />
                                     <br />
                                </td>
                            </tr>
                           
                        </table>
                             
                        <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Blue" Font-Names="Cambria"></asp:Label>
                        <input id="hidMacAddress" style="width: 8px" type="hidden" runat="server" />
                        <input id="hidBranchID" style="width: 8px" type="hidden" runat="server" />
                        <input id="hidBranchName" style="width: 8px" type="hidden" runat="server" />
                        <asp:HiddenField ID="hdnUserID" runat="server" />
                        <asp:HiddenField ID="hdnPswd" runat="server" />
                        <asp:HiddenField ID="hdnValue" runat="server" />
                        <asp:HiddenField ID="hdnWrongPwd" runat="server" />
                        <asp:HiddenField ID="hidSum" runat="server" />
                        <asp:HiddenField ID="hidOrgPwd" runat="server" />
                         
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />

                    </td>
                </tr>
                <tr>
                    <td style="height: 21px; text-align: center;">
                        <span style="font-family: Cambria; font-size: 11pt;">
                           
                        </span>
                    </td>
                </tr>
            </table>
            
        </div>
        <div style="clear:both;"></div>
        <div style="text-align:right; background-color:White; height:300px;">
            <div id="FooterContainer" style="float:right; padding-top:0px;width:22%; height:100px;">
                <div id="Footer" class="warpper">
                    <div id="FooterContent">
                        <div id="QuickLinks" style="width:250px; height:100px;">
                            <ul>
<%--                                <li><a href="HRM/Attendance/EsafPunching.aspx" style="text-align:left;">Attendance</a></li>
--%>                                <li><a href="ForgotPassword.aspx" style="text-align:left;">Forgot Password / Unlock Eweb</a></li>
                                 <li id="aa"><a href="http://eweb.emfil.org/eswt/" style="text-align:left;">ESWT Election Result</a></li>

                             
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>       
        <div style="clear:both;"></div>
        <div><br /><br /><br /><br /><br /></div>
        <div style="clear:both;"></div>
        <div style="height:50px; font-family:Calibri; color:White; text-align:center;"><br /><br /><br />
            <span>Powered by ESAF</span><br />
            <span>Copyright @ 2017</span>
        </div>
    </form>
</body>
</html>
