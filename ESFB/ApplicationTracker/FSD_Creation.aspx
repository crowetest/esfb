﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" EnableEventValidation="false" CodeFile="FSD_Creation.aspx.vb" Inherits="FSD_Creation" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
 <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">   </asp:ToolkitScriptManager>

    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

            function window_onload() {   
                document.getElementById("<%= hdnTracker.ClientID %>").value=0;    
                document.getElementById("rwApp").style.display = "none";
                document.getElementById("rwPrio").style.display = "none";
                document.getElementById("rwView").style.display = "none";
                document.getElementById("rwDes").style.display = "none"; 
                document.getElementById("rwSubDt").style.display = "none"; 
                document.getElementById("rwSubBy").style.display = "none"; 
                document.getElementById("rwAppBy").style.display = "none";
                document.getElementById("rwSignBy").style.display = "none";
                document.getElementById("rwSignAppBy").style.display = "none";
                document.getElementById("rwAppOn").style.display = "none";
                document.getElementById("rwSignOn").style.display = "none";
                document.getElementById("rwSignAppOn").style.display = "none";
                document.getElementById("rwSerReq").style.display = "none";
                 document.getElementById("rwStartDt").style.display = "none";
                  document.getElementById("rwEndDt").style.display = "none";

                document.getElementById("<%= cmbApplication.ClientID %>").disabled=true;
                document.getElementById("<%= cmbPriority.ClientID %>").disabled=true;      
                ToServer("1ʘ", 1);
            }

            function TrackerOnChange()
            {
                if (document.getElementById("<%= cmbTracker.ClientID %>").value == -1)// tracker
                {
                    alert("Select Tracker");
                    document.getElementById("<%= cmbTracker.ClientID %>").focus();
                    return false;
                } 
                ToServer("2ʘ"+document.getElementById("<%= cmbTracker.ClientID %>").value, 2);
            }

            function FromServer(Arg, Context) {            
                switch (Context) {
                    case 1:
                    {
                        if(Arg != "")
                        {
                            var Data = Arg.split("¥");
                            ComboFill(Data[0], "<%= cmbTracker.ClientID %>");
                            ComboFill(Data[1], "<%= cmbApplication.ClientID %>");
                        }
                        break;
                    }
                    case 2:
                    {
                        if(Arg != "")
                        {
                            var Data = Arg.split("¥");    
                            document.getElementById("rwApp").style.display = "";
                            document.getElementById("rwPrio").style.display = "";
                            document.getElementById("rwView").style.display = "";                            
                            document.getElementById("rwDes").style.display = "";  
                            document.getElementById("rwSubDt").style.display = ""; 
                            document.getElementById("rwSubBy").style.display = ""; 
                            document.getElementById("rwAppBy").style.display = "";
                            document.getElementById("rwSignBy").style.display = "";
                            document.getElementById("rwSignAppBy").style.display = "";
                            document.getElementById("rwAppOn").style.display = "";
                            document.getElementById("rwSignOn").style.display = "";
                            document.getElementById("rwSignAppOn").style.display = "";
                            document.getElementById("rwSerReq").style.display = "";

                            document.getElementById("<%= cmbApplication.ClientID %>").value=Data[1];
                            document.getElementById("<%= cmbPriority.ClientID %>").value=Data[2];
                            document.getElementById("<%= txtBRDDes.ClientID %>").value=Data[3];                         
                            var Tab = "";
                            Tab += "<div id='ScrollDiv' style='width:100%; height:20px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                            Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                            Tab += "<tr class='sub_first';>";
                            Tab += "<td style='width:90%;text-align:left;cursor: pointer;'><a href='ShowTrackerFormat.aspx?TrackerID=" + Data[0] + "&UatID=" + Data[14] + "&StatusID=1'>" + Data[4] + "</a></td></tr>";
                            Tab += "</table></div>";
                            document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;
                            document.getElementById("<%= txtSubDt.ClientID %>").value=Data[5];
                            document.getElementById("<%= txtSubBy.ClientID %>").value=Data[6];
                            document.getElementById("<%= txtAppBy.ClientID %>").value=Data[7];
                            document.getElementById("<%= txtSignBy.ClientID %>").value=Data[8];
                            document.getElementById("<%= txtSignAppBy.ClientID %>").value=Data[9];
                            document.getElementById("<%= txtAppOn.ClientID %>").value=Data[10];
                            document.getElementById("<%= txtSignOn.ClientID %>").value=Data[11];
                            document.getElementById("<%= txtSignAppOn.ClientID %>").value=Data[12];
                            document.getElementById("<%= txtSerReq.ClientID %>").value=Data[13];

                            if (Data[15] ==1)
                            {
                            
                             document.getElementById("rwStartDt").style.display = "";
                             document.getElementById("rwEndDt").style.display = "";
                            }


                        }
                        break;
                    }
                }
            }

            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }

            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        
            function SaveOnClick() {
                if (document.getElementById("<%= cmbTracker.ClientID %>").value == -1)//Application
                {
                    alert("Select Application Tracker");
                    document.getElementById("<%= cmbTracker.ClientID %>").focus();
                    return false;
                } 
                if(document.getElementById("<%= txtRemark.ClientID %>").value == "")
                {
                    alert("Please enter Remaks");
                    document.getElementById("<%= txtRemark.ClientID %>").focus();
                    return false;
                } 
                if (document.getElementById("<%= fupFsd.ClientID %>").value == "") {
                    alert("Please Upload FSD");
                    document.getElementById("<%= fupFsd.ClientID %>").focus();
                    return false;
                }
                document.getElementById("<%= hdnTracker.ClientID %>").value = document.getElementById("<%= cmbTracker.ClientID %>").value;
            }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Select Tracker
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbTracker" class="NormalText" Style="text-align: left;" runat="server"
                            Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td id="Td1" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwApp">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Application
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbApplication" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td id="colDraft" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwPrio">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Priority
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbPriority" class="NormalText" Style="text-align: left;" runat="server"
                            Font-Names="Cambria" Width="100%" ForeColor="Black">
                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                            <asp:ListItem Value="1">LOW</asp:ListItem>
                            <asp:ListItem Value="2">MEDIUM</asp:ListItem>
                            <asp:ListItem Value="3">HIGH</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwView">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%">
                        BRD
                    </td>
                    <td style="width: 50%">
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwDes">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Brief Description about BRD
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtBRDDes" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)'  Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwSerReq">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Incident/Service Request No.</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSerReq" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwSubDt">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        BRD Submitted On
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSubDt" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwSubBy">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        BRD Submitted By
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSubBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwAppOn">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        BRD Approved On</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtAppOn" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwAppBy">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        BRD Approved By
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtAppBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwSignOn">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        BRD SignOff On</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSignOn" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwSignBy">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        BRD SignOff By
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSignBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwSignAppOn">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        BRD SignOff Approved On</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSignAppOn" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwSignAppBy">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        BRD SignOff Approved By
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSignAppBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwStartDt">
                      <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Start Date</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtStartDt" class="NormalText"  runat="server" onkeypress="return false"></asp:TextBox>
                        <asp:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" 
                           Enabled="True" Format="dd/MMM/yyyy" 
                            TargetControlID="txtStartDt">
                        </asp:CalendarExtender>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                 <tr id="rwEndDt">
                      <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        End Date</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtEndDt" class="NormalText"  runat="server" onkeypress="return false"></asp:TextBox>
                        <asp:CalendarExtender ID="txtEndDt_CalendarExtender" runat="server" 
                           Enabled="True" Format="dd/MMM/yyyy"  
                            TargetControlID="txtEndDt">
                        </asp:CalendarExtender>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 50%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwJira">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        JIRA Number</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtJira" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwUpload">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Upload FSD<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <input id="fupFsd" runat="server" cssclass="fileUpload" type="file" /></td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 50%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwRemark">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Remarks if any
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRemark" runat="server" class="NormalText" Style="font-family: Cambria; 
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)'  Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <asp:Button ID="btnSave" runat="server" Style="font-family: cambria; cursor: pointer;
                            width: 67px;" Text="SUBMIT" />
                        &nbsp;&nbsp;
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnTracker" runat="server" />
            <asp:HiddenField ID="hdnApplication" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
