﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" EnableEventValidation="false" CodeFile="Pre_UAT_Ready.aspx.vb" Inherits="Pre_UAT_Ready" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

            function window_onload() {                      
                document.getElementById("rwApp").style.display = "none";
                document.getElementById("rwPrio").style.display = "none";
                document.getElementById("rwView").style.display = "none";
                document.getElementById("rwDes").style.display = "none"; 
                document.getElementById("rwSubDt").style.display = "none"; 
                document.getElementById("rwSubBy").style.display = "none"; 
                document.getElementById("rwAppBy").style.display = "none";
                document.getElementById("rwSignBy").style.display = "none";
                document.getElementById("rwSignAppBy").style.display = "none";

                document.getElementById("rwFsd").style.display = "none";
                document.getElementById("rwFsdDt").style.display = "none";
                document.getElementById("rwFsdBy").style.display = "none";
                document.getElementById("rwFsdDesc").style.display = "none";
                document.getElementById("rwFsdSOffOn").style.display = "none";
                document.getElementById("rwFsdSOffBy").style.display = "none";
                document.getElementById("rwFsdSOffRem").style.display = "none";
                document.getElementById("rwUatDtl").style.display = "none";

                document.getElementById("rwAppOn").style.display = "none";
                document.getElementById("rwSignOn").style.display = "none";
                document.getElementById("rwSignAppOn").style.display = "none";
                document.getElementById("rwSerReq").style.display = "none";
                document.getElementById("rwJira").style.display = "none";
                                
                document.getElementById("<%= hdnApplication.ClientID %>").value="";
                document.getElementById("<%= cmbApplication.ClientID %>").disabled=true;
                document.getElementById("<%= cmbPriority.ClientID %>").disabled=true;      
                ToServer("1ʘ", 1);
            }

            function TrackerOnChange()
            {
                if (document.getElementById("<%= cmbTracker.ClientID %>").value == -1)// tracker
                {
                    alert("Select Tracker");
                    document.getElementById("<%= cmbTracker.ClientID %>").focus();
                    return false;
                } 
                ToServer("2ʘ"+document.getElementById("<%= cmbTracker.ClientID %>").value, 2);
            }

            function FromServer(Arg, Context) {                                    
                switch (Context) {
                case 1:
                    {
                        if(Arg != "")
                        {
                            var Data = Arg.split("¥");
                            ComboFill(Data[0], "<%= cmbTracker.ClientID %>");
                            ComboFill(Data[1], "<%= cmbApplication.ClientID %>");
                        }
                        break;
                    }
                    case 2:
                    {
                        if(Arg != "")
                        {
                            var AllData = Arg.split("Ř"); 
                            var Data = AllData[0].split("¥");    
                            document.getElementById("rwApp").style.display = "";
                            document.getElementById("rwPrio").style.display = "";
                            document.getElementById("rwView").style.display = "";                            
                            document.getElementById("rwDes").style.display = "";  
                            document.getElementById("rwSubDt").style.display = ""; 
                            document.getElementById("rwSubBy").style.display = ""; 
                            document.getElementById("rwAppBy").style.display = "";
                            document.getElementById("rwSignBy").style.display = "";
                            document.getElementById("rwSignAppBy").style.display = "";
                            document.getElementById("rwFsd").style.display = "";
                            document.getElementById("rwFsdDt").style.display = "";
                            document.getElementById("rwFsdBy").style.display = "";
                            document.getElementById("rwFsdDesc").style.display = "";
                            document.getElementById("rwFsdSOffOn").style.display = "";
                            document.getElementById("rwFsdSOffBy").style.display = "";
                            document.getElementById("rwFsdSOffRem").style.display = "";
                            document.getElementById("rwAppOn").style.display = "";
                            document.getElementById("rwSignOn").style.display = "";
                            document.getElementById("rwSignAppOn").style.display = "";
                            document.getElementById("rwSerReq").style.display = "";
                            document.getElementById("rwJira").style.display = "";

                            document.getElementById("<%= cmbApplication.ClientID %>").value=Data[1];
                            document.getElementById("<%= cmbPriority.ClientID %>").value=Data[2];
                            document.getElementById("<%= txtBRDDes.ClientID %>").value=Data[3];
                                                     
                            var Tab = "";
                            Tab += "<div id='ScrollDiv' style='width:100%; height:20px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                            Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                            Tab += "<tr class='sub_first';>";
                            Tab += "<td style='width:90%;text-align:left;cursor: pointer;'><a href='ShowTrackerFormat.aspx?TrackerID=" + Data[0] + "&UatID=" + Data[20] + "&StatusID=1'>" + Data[4] + "</a></td></tr>";
                            Tab += "</table></div>";
                            document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;

                            document.getElementById("<%= txtSubDt.ClientID %>").value=Data[5];
                            document.getElementById("<%= txtSubBy.ClientID %>").value=Data[6];
                            document.getElementById("<%= txtAppBy.ClientID %>").value=Data[7];
                            document.getElementById("<%= txtSignBy.ClientID %>").value=Data[8];
                            document.getElementById("<%= txtSignAppBy.ClientID %>").value=Data[9];

                            var Tab1 = "";
                            Tab1 += "<div id='ScrollDiv' style='width:100%; height:20px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                            Tab1 += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                            Tab1 += "<tr class='sub_first';>";
                            Tab1 += "<td style='width:90%;text-align:left;cursor: pointer;'><a href='ShowTrackerFormat.aspx?TrackerID=" + Data[0] + "&UatID=" + Data[21] + "&StatusID=2'>" + Data[10] + "</a></td></tr>";
                            Tab1 += "</table></div>";
                            document.getElementById("<%= pnlFsd.ClientID %>").innerHTML = Tab1;

                            document.getElementById("<%= txtFsdSubDt.ClientID %>").value=Data[11];
                            document.getElementById("<%= txtFsdSubBy.ClientID %>").value=Data[12];
                            document.getElementById("<%= txtFsdDesc.ClientID %>").value=Data[13];
                            document.getElementById("<%= txtFsdSOffOn.ClientID %>").value=Data[14];
                            document.getElementById("<%= txtFsdSOffBy.ClientID %>").value=Data[15];
                            document.getElementById("<%= txtFsdSOffDesc.ClientID %>").value=Data[16];

                            document.getElementById("<%= txtAppOn.ClientID %>").value=Data[22];
                            document.getElementById("<%= txtSignOn.ClientID %>").value=Data[23];
                            document.getElementById("<%= txtSignAppOn.ClientID %>").value=Data[24];
                            document.getElementById("<%= txtSerReq.ClientID %>").value=Data[18];
                            document.getElementById("<%= txtJira.ClientID %>").value=Data[19];

                            document.getElementById("<%= hdnApplication.ClientID %>").value="";
                            if(Data[17] > 0)
                            {
                                document.getElementById("<%= hdnApplication.ClientID %>").value= AllData[1];
                                UATDataFill();
                            }
                        }
                        break;
                    }
                    case 3:
                    {
                        var Data = Arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("Pre_UAT_Ready.aspx", "_self");
                        break;
                    }
                }
            }
            function UATDataFill()
            {
                document.getElementById("rwUatDtl").style.display = "";
                document.getElementById("<%= pnlUatDtl.ClientID %>").style.display = "";
                var tab = "";
                var row_bg = 0;
                tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
                tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr>";
                tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
                tab += "<td style='width:10%;text-align:left' >UAT Rediness Date</td>";
                tab += "<td style='width:10%;text-align:left' >UAT Ready Confirm On</td>";
                tab += "<td style='width:10%;text-align:left'>UAT Ready Confirm By</td>";
                tab += "<td style='width:20%;text-align:left'>UAT Confirm Remark</td>";
                tab += "<td style='width:10%;text-align:left'>UAT Over Confirm On</td>";
                tab += "<td style='width:10%;text-align:left'>UAT Over Confirm Name</td>";
                tab += "<td style='width:20%;text-align:left'>UAT Over Confirm Remark</td>";
                tab += "<td style='width:8%;text-align:left'>Defect</td>";
                tab += "</tr>";
                if (document.getElementById("<%= hdnApplication.ClientID %>").value != "") {
                    row = document.getElementById("<%= hdnApplication.ClientID %>").value.split("Ĉ");

                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");

                        if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class=sub_first>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class=sub_second>";
                        }
                        i = n + 1;
                        
                        tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                        tab += "<td style='width:10%;text-align:left' >" + col[2] + "</td>";
                        tab += "<td style='width:10%;text-align:left' >" + col[3] + "</td>";
                        tab += "<td style='width:10%;text-align:left'>" + col[4] + "</td>";
                        tab += "<td style='width:20%;text-align:left'>" + col[5] + "</td>";
                        tab += "<td style='width:10%;text-align:left'>" + col[6] + "</td>";
                        tab += "<td style='width:10%;text-align:left'>" + col[7] + "</td>";
                        tab += "<td style='width:20%;text-align:left'>" + col[8] + "</td>";
                        tab += "<td style='width:8%;text-align:left'><a href='ShowTrackerFormat.aspx?TrackerID=" + col[1] + "&UatID=" + col[0] + "&StatusID=3'>" + col[9] + "</a></td>";   
                        tab += "</tr>";
                    }
                }
                tab += "</table></div></div></div>";
                document.getElementById("<%= pnlUatDtl.ClientID %>").innerHTML = tab;
            }
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function setStartDate(sender, args) 
            {
                sender._startDate = new Date();
            }

            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }   
            function btnSave_onclick() {
                if (document.getElementById("<%= cmbTracker.ClientID %>").value == -1)//Application
                {
                    alert("Select Application Tracker");
                    document.getElementById("<%= cmbTracker.ClientID %>").focus();
                    return false;
                } 
                if(document.getElementById("<%= txtRemark.ClientID %>").value == "")
                {
                    alert("Please Enter Remarks");
                    document.getElementById("<%= txtRemark.ClientID %>").focus();
                    return false;
                } 
                if(document.getElementById("<%= txtReadyDt.ClientID %>").value == "")
                {
                    alert("Please Select UAT Ready Date");
                    document.getElementById("<%= txtRemark.ClientID %>").focus();
                    return false;
                } 
                ToServer("3ʘ"+document.getElementById("<%= cmbTracker.ClientID %>").value +"ʘ"+ document.getElementById("<%= txtRemark.ClientID %>").value +"ʘ"+ document.getElementById("<%= txtReadyDt.ClientID %>").value, 3);
            }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Select Tracker
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbTracker" class="NormalText" Style="text-align: left;" runat="server"
                            Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td id="Td1" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwApp">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Application
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbApplication" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td id="colDraft" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwPrio">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Priority
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbPriority" class="NormalText" Style="text-align: left;" runat="server"
                            Font-Names="Cambria" Width="100%" ForeColor="Black">
                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                            <asp:ListItem Value="1">LOW</asp:ListItem>
                            <asp:ListItem Value="2">MEDIUM</asp:ListItem>
                            <asp:ListItem Value="3">HIGH</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwView">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%">
                        BRD
                    </td>
                    <td style="width: 50%">
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwDes">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Brief Description about BRD
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtBRDDes" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)'  Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwSerReq">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Incident/Service Request No.</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSerReq" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwSubDt">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        BRD Submitted On
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSubDt" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwSubBy">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        BRD Submitted By
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSubBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwAppOn">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        BRD Approved On</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtAppOn" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwAppBy">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        BRD Approved By
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtAppBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwSignOn">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        BRD SignOff On</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSignOn" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwSignBy">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        BRD SignOff By
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSignBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwSignAppOn">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        BRD SignOff Approved On</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSignAppOn" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwSignAppBy">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        BRD SignOff Approved By
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSignAppBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 50%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwFsd">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        FSD</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:Panel ID="pnlFsd" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>                
                <tr id="rwFsdDt">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        FSD Submitted On</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtFsdSubDt" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwFsdBy">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        FSD Submitted By</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtFsdSubBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwJira">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        JIRA No.</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtJira" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwFsdDesc">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        FSD Description</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtFsdDesc" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwFsdSOffOn">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        FSD SignOff On</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtFsdSOffOn" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwFsdSOffBy">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        FSD SignOff By</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtFsdSOffBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwFsdSOffRem">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        FSD SignOff Remarks</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtFsdSOffDesc" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 50%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwUatDtl">
                    <td colspan="4">
                        <asp:Panel ID="pnlUatDtl" runat="server">
                        </asp:Panel></td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        UAT Readiness Date</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtReadyDt" runat="server" onkeypress="NumericCheck(event)"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="txtReadyDt_CalendarExtender" runat="server" 
                            Format="dd/MMM/yyyy" OnClientShowing="setStartDate" 
                            TargetControlID="txtReadyDt">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwRemark">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Remarks 
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRemark" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;<input id="btnSave" 
                            style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="SUBMIT" onclick="return btnSave_onclick()" />&nbsp;
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnTracker" runat="server" />
            <asp:HiddenField ID="hdnApplication" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
