﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class BRD_Resubmission
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "BRD Resubmission"
        If GF.FormAccess(CInt(Session("UserID")), 1299) = False And CInt(Session("BranchID")) <> 0 Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

        Me.cmbTracker.Attributes.Add("onchange", "return TrackerOnChange()")
        Me.chkModify.Attributes.Add("onclick", "return ModifyAttachment()")
        Me.btnConfirm.Attributes.Add("onclick", "return UploadOnClick()")

    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Tracker
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select app_tracker_id,app_tracker_no from app_tracker_master where approve_brdsoff_status=2 and fsd_created_status is null and created_status >=1 and convert(date,hold_upto) <= convert(date,getdate()) and created_by=" & CInt(Session("UserID")) & " union all select app_tracker_id,app_tracker_no from app_tracker_master where approved_status=3 and brd_signoff_status is null and created_status=1 and created_by=" & CInt(Session("UserID")) & "")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
                CallBackReturn += "¥"
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select application_id,application_name from app_application_master where status_id=1")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2 'Fill tracker details
                Dim TrackerID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select a.application_id,a.brd_priority,a.brd_remarks,b.brdfile_name,a.app_tracker_id,isnull(a.ServiceReq_No,''),a.app_brd_id,a.approved_on,a.approved_name,a.approved_remark,isnull(a.brd_signoff_on,''),isnull(a.brd_signoff_name,''),isnull(a.brd_signoff_remark,''),isnull(a.approve_brdsoff_on,''),isnull(a.approve_brdsoff_name,''),isnull(a.approve_brdsoff_remark,''),case when brd_signoff_status is null then 1 else 0 end Stat from app_tracker_master a, app_tracker_brd_attach b where a.app_tracker_id=" & TrackerID & " and a.app_tracker_id=b.app_tracker_id and a.app_brd_id=b.app_brd_id")
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "¥" + DT.Rows(0)(1).ToString() + "¥" + DT.Rows(0)(2).ToString() + "¥" + DT.Rows(0)(3).ToString() + "¥" + DT.Rows(0)(4).ToString() + "¥" + DT.Rows(0)(5).ToString() + "¥" + DT.Rows(0)(6).ToString() + "¥" + CDate(DT.Rows(0)(7)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(8).ToString() + "¥" + DT.Rows(0)(9).ToString() + "¥" + CDate(DT.Rows(0)(10)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(11).ToString() + "¥" + DT.Rows(0)(12).ToString() + "¥" + CDate(DT.Rows(0)(13)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(14).ToString() + "¥" + DT.Rows(0)(15).ToString() + "¥" + DT.Rows(0)(16).ToString()
                End If
            Case 3 'Resubmit
                Dim TrackerID As Integer = CInt(Data(1))
                Dim AppID As Integer = CInt(Data(2))
                Dim PriorityID As Integer = CInt(Data(3))
                Dim Remark As String = CStr(Data(4))
                Dim ReqNo As String = CStr(Data(5))

                Dim UserID As Integer = CInt(Session("UserID"))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try

                    Dim Params(7) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@TrackerID", SqlDbType.Int)
                    Params(1).Value = TrackerID
                    Params(2) = New SqlParameter("@ApplicationID", SqlDbType.Int)
                    Params(2).Value = AppID
                    Params(3) = New SqlParameter("@PriorityID", SqlDbType.Int)
                    Params(3).Value = PriorityID
                    Params(4) = New SqlParameter("@Remark", SqlDbType.VarChar, 1000)
                    Params(4).Value = Remark
                    Params(5) = New SqlParameter("@ReqNo", SqlDbType.VarChar, 50)
                    Params(5).Value = ReqNo
                    Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(7).Direction = ParameterDirection.Output

                    DB.ExecuteNonQuery("SP_TRACKER_BRD_RESUBMIT", Params)
                    ErrorFlag = CInt(Params(6).Value)
                    Message = CStr(Params(7).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End Select
    End Sub
#End Region

#Region "Confirm"
    Private Sub initializeControls()
        cmbTracker.Focus()
        cmbApplication.Text = "-1"
        cmbPriority.Text = "-1"
        txtRemark.Text = ""
        chkModify.Checked = False
    End Sub

    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Try
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim TrackerID As Integer = CInt(Me.hdnTracker.Value)
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim FileName As String = ""
            Dim myFile As HttpPostedFile = fupBrd.PostedFile
            Dim nFileLen As Integer = myFile.ContentLength
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, nFileLen)
                Dim fileExtension As String = Path.GetExtension(myFile.FileName)
                If Not (fileExtension = ".xls" Or fileExtension = ".xlsx" Or fileExtension = ".jpg" Or fileExtension = ".jpeg" Or fileExtension = ".doc" Or fileExtension = ".docx" Or fileExtension = ".zip" Or fileExtension = ".pdf" Or fileExtension = ".PDF" Or fileExtension = ".XLS" Or fileExtension = ".XLSX" Or fileExtension = ".JPG" Or fileExtension = ".JPEG" Or fileExtension = ".DOC" Or fileExtension = ".DOCX" Or fileExtension = ".ZIP") Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
            End If

            Try
                If TrackerID > 0 Then
                    Dim Param(5) As SqlParameter
                    Param(0) = New SqlParameter("@TrackerID", SqlDbType.Int)
                    Param(0).Value = TrackerID
                    Param(1) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                    Param(1).Value = AttachImg
                    Param(2) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                    Param(2).Value = ContentType
                    Param(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                    Param(3).Value = FileName
                    Param(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Param(4).Direction = ParameterDirection.Output
                    Param(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Param(5).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_TRACKER_BRD_UPDATE", Param)
                    ErrorFlag = CInt(Param(4).Value)
                    Message = CStr(Param(5).Value)
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "'); ")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

End Class
