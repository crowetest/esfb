﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true"  
    EnableEventValidation="false" CodeFile="BRD_Resubmission.aspx.vb" Inherits="BRD_Resubmission" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

            function window_onload() {   
                document.getElementById("<%= hdnTracker.ClientID %>").value=0;    
                document.getElementById("rwApp").style.display = "none";
                document.getElementById("rwPrio").style.display = "none";
                document.getElementById("rwView").style.display = "none";
                document.getElementById("rwUpload").style.display = "none";
                document.getElementById("rwDes").style.display = "none"; 

                document.getElementById("rwAppOn").style.display = "none"; 
                document.getElementById("rwAppBy").style.display = "none"; 
                document.getElementById("rwAppRemark").style.display = "none"; 
                document.getElementById("rwSoffOn").style.display = "none"; 
                document.getElementById("rwSoffBy").style.display = "none"; 
                document.getElementById("rwSoffAppOn").style.display = "none"; 
                document.getElementById("rwSoffAppBy").style.display = "none"; 
                document.getElementById("rwSoffAppRemark").style.display = "none"; 

                document.getElementById("<%= cmbApplication.ClientID %>").disabled=true;    
                document.getElementById("<%= chkModify.ClientID %>").checked=false;  
                ToServer("1ʘ", 1);
            }

            function TrackerOnChange()
            {
                if (document.getElementById("<%= cmbTracker.ClientID %>").value == -1)// tracker
                {
                    alert("Select Tracker");
                    document.getElementById("<%= cmbTracker.ClientID %>").focus();
                    return false;
                } 
                ToServer("2ʘ"+document.getElementById("<%= cmbTracker.ClientID %>").value, 2);
            }

            function FromServer(Arg, Context) {            
                switch (Context) {
                    case 1:
                    {
                        if(Arg != "")
                        {
                            var Data = Arg.split("¥");
                            ComboFill(Data[0], "<%= cmbTracker.ClientID %>");
                            ComboFill(Data[1], "<%= cmbApplication.ClientID %>");
                        }
                        break;
                    }
                    case 2:
                    {
                        if(Arg != "")
                        {
                            var Data = Arg.split("¥");    
                            document.getElementById("rwApp").style.display = "";
                            document.getElementById("rwPrio").style.display = "";
                            document.getElementById("rwView").style.display = "";
                            document.getElementById("rwUpload").style.display = "none";
                            document.getElementById("rwDes").style.display = "";  
                            document.getElementById("<%= cmbApplication.ClientID %>").value=Data[0];
                            document.getElementById("<%= cmbPriority.ClientID %>").value=Data[1];
                            document.getElementById("<%= txtRemark.ClientID %>").value=Data[2];  
                            document.getElementById("<%= txtSerReqNo.ClientID %>").value=Data[5];                         
                            var Tab = "";
                            Tab += "<div id='ScrollDiv' style='width:100%; height:20px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                            Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                            Tab += "<tr class='sub_first';>";
                            Tab += "<td style='width:90%;text-align:left;cursor: pointer;'><a href='ShowTrackerFormat.aspx?TrackerID=" + Data[4] + "&UatID=" + Data[6] + "&StatusID=1'>" + Data[3] + "</a></td></tr>";
                            Tab += "</table></div>";
                            document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;

                            document.getElementById("<%= txtAppOn.ClientID %>").value=Data[7]; 
                            document.getElementById("<%= txtAppBy.ClientID %>").value=Data[8];
                            document.getElementById("<%= txtAppRemark.ClientID %>").value=Data[9];
                            document.getElementById("<%= txtSoffOn.ClientID %>").value=Data[10];
                            document.getElementById("<%= txtSoffBy.ClientID %>").value=Data[11];
                            document.getElementById("<%= txtSoffAppOn.ClientID %>").value=Data[13];
                            document.getElementById("<%= txtSoffAppBy.ClientID %>").value=Data[14];
                            document.getElementById("<%= txtSoffAppRemark.ClientID %>").value=Data[15];

                            if(Data[16] == 1)
                            {
                                document.getElementById("rwAppOn").style.display = ""; 
                                document.getElementById("rwAppBy").style.display = ""; 
                                document.getElementById("rwAppRemark").style.display = ""; 
                                document.getElementById("rwSoffOn").style.display = "none"; 
                                document.getElementById("rwSoffBy").style.display = "none"; 
                                document.getElementById("rwSoffAppOn").style.display = "none"; 
                                document.getElementById("rwSoffAppBy").style.display = "none"; 
                                document.getElementById("rwSoffAppRemark").style.display = "none"; 
                            }
                            else{
                                document.getElementById("rwAppOn").style.display = ""; 
                                document.getElementById("rwAppBy").style.display = ""; 
                                document.getElementById("rwAppRemark").style.display = ""; 
                                document.getElementById("rwSoffOn").style.display = ""; 
                                document.getElementById("rwSoffBy").style.display = ""; 
                                document.getElementById("rwSoffAppOn").style.display = ""; 
                                document.getElementById("rwSoffAppBy").style.display = ""; 
                                document.getElementById("rwSoffAppRemark").style.display = ""; 
                            }
                        }
                        break;
                    }
                    case 3:
                    { 
                        var Data = Arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) 
                        window.open("BRD_Resubmission.aspx", "_self");
                        break;
                    }
                }
            }

            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }

            function UploadOnClick()
            {
                if (document.getElementById("<%= fupBrd.ClientID %>").value == "") {
                    alert("Please Upload BRD");
                    document.getElementById("<%= fupBrd.ClientID %>").focus();
                    return false;
                }
                document.getElementById("<%= hdnTracker.ClientID %>").value = document.getElementById("<%= cmbTracker.ClientID %>").value;
            }

            function ModifyAttachment() {
                if (document.getElementById("<%= chkModify.ClientID %>").checked == true)
                    document.getElementById("rwUpload").style.display = "";
                else
                    document.getElementById("rwUpload").style.display = "none";
            }

            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        
            function btnSave_onclick() {
                if (document.getElementById("<%= cmbTracker.ClientID %>").value == -1)//Application
                {
                    alert("Select Application Tracker");
                    document.getElementById("<%= cmbTracker.ClientID %>").focus();
                    return false;
                }   
                if (document.getElementById("<%= cmbApplication.ClientID %>").value == -1)//Application
                {
                    alert("Select Application");
                    document.getElementById("<%= cmbApplication.ClientID %>").focus();
                    return false;
                }                
                if (document.getElementById("<%= cmbPriority.ClientID %>").value == -1)//Priority
                {
                    alert("Select Priority");
                    document.getElementById("<%= cmbPriority.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtRemark.ClientID %>").value == "")
                {
                    alert("Please enter Brief Description about BRD");
                    document.getElementById("<%= txtRemark.ClientID %>").focus();
                    return false;
                }       
                var Data = document.getElementById("<%= cmbTracker.ClientID %>").value+"ʘ"+document.getElementById("<%= cmbApplication.ClientID %>").value+"ʘ"+document.getElementById("<%= cmbPriority.ClientID %>").value+"ʘ"+document.getElementById("<%= txtRemark.ClientID %>").value+"ʘ"+document.getElementById("<%= txtSerReqNo.ClientID %>").value;      
                ToServer("3ʘ" + Data, 3);
            }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Select Tracker
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbTracker" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">                            
                        </asp:DropDownList>
                    </td>
                    <td id="Td1" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwApp">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Application
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbApplication" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">                            
                        </asp:DropDownList>
                    </td>
                    <td id="colDraft" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwPrio">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Priority
                    </td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:DropDownList ID="cmbPriority" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                            <asp:ListItem Value="1">LOW</asp:ListItem>
                            <asp:ListItem Value="2">MEDIUM</asp:ListItem>
                            <asp:ListItem Value="3">HIGH</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rwView">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%">
                        Incident/Service Request no. if any</td>
                         <td style="width: 50%">
                        <asp:TextBox ID="txtSerReqNo" runat="server" Width="100%"></asp:TextBox>
                        </td>
                         <td style="width: 10%">
                             &nbsp;</td>
                </tr>            
                
                <tr id="rwAppOn">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%">
                        BRD Approved On</td>
                         <td style="width: 50%">
                        <asp:TextBox ID="txtAppOn" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                        </td>
                         <td style="width: 10%">
                             &nbsp;</td>
                </tr>            
                
                <tr id="rwAppBy">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%">
                        BRD Approved By</td>
                         <td style="width: 50%">
                        <asp:TextBox ID="txtAppBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                        </td>
                         <td style="width: 10%">
                             &nbsp;</td>
                </tr>            
                
                <tr id="rwAppRemark">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%">
                        BRD Approved Remark</td>
                         <td style="width: 50%">
                        <asp:TextBox ID="txtAppRemark" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                        </td>
                         <td style="width: 10%">
                             &nbsp;</td>
                </tr>            
                
                <tr id="rwSoffOn">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%">
                        BRD SignOff On</td>
                         <td style="width: 50%">
                        <asp:TextBox ID="txtSoffOn" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                        </td>
                         <td style="width: 10%">
                             &nbsp;</td>
                </tr>            
                
                <tr id="rwSoffBy">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%">
                        BRD SignOff By</td>
                         <td style="width: 50%">
                        <asp:TextBox ID="txtSoffBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                        </td>
                         <td style="width: 10%">
                             &nbsp;</td>
                </tr>            
                
                <tr id="rwSoffAppOn">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%">
                        BRD Signoff Approved On</td>
                         <td style="width: 50%">
                        <asp:TextBox ID="txtSoffAppOn" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                        </td>
                         <td style="width: 10%">
                             &nbsp;</td>
                </tr>            
                
                <tr id="rwSoffAppBy">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%">
                        BRD Signoff Approved By</td>
                         <td style="width: 50%">
                        <asp:TextBox ID="txtSoffAppBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                        </td>
                         <td style="width: 10%">
                             &nbsp;</td>
                </tr>            
                
                <tr id="rwSoffAppRemark">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%">
                        BRD Signoff Approved 
                        Remark</td>
                         <td style="width: 50%">
                        <asp:TextBox ID="txtSoffAppRemark" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                        </td>
                         <td style="width: 10%">
                             &nbsp;</td>
                </tr>            
                
                <tr id="rwView">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%">
                        &nbsp;</td>
                         <td style="width: 50%">
                             &nbsp;</td>
                         <td style="width: 10%">
                             &nbsp;</td>
                </tr>            
                
                <tr id="rwView">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%">
                        <asp:CheckBox ID="chkModify" runat="server" Font-Underline="True" 
                            ForeColor="#0000CC" Text="Reupload BRD" />
                    </td>
                         <td style="width: 50%">
                             <asp:Panel ID="pnlDoc" runat="server">
                             </asp:Panel>
                        </td>
                         <td style="width: 10%">
                             &nbsp;</td>
                </tr>            
                
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 50%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                
                <tr id="rwUpload">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        ReUpload BRD<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <input id="fupBrd" runat="server" cssclass="fileUpload" type="file" />
                         <asp:Button ID="btnConfirm" runat="server" Text="SaveBRD" Width="67px"  />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 50%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rwDes">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Brief Description about BRD
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRemark" runat="server" class="NormalText" Style="font-family: Cambria; 
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="SUBMIT" onclick="return btnSave_onclick()" />
                    &nbsp;&nbsp;
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnTracker" runat="server" />
            <asp:HiddenField ID="hdnApplication" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
