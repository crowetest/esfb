﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class FSD_Final_Submission
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "FSD Final Upload"
        If GF.FormAccess(CInt(Session("UserID")), 1301) = False And CInt(Session("BranchID")) <> 0 Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

        Me.cmbTracker.Attributes.Add("onchange", "return TrackerOnChange()")
        Me.btnSave.Attributes.Add("onchange", "return SaveOnChange()")
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim DR As DataRow
        'Dim DeptID As Integer = CInt(Session("DepartmentID"))
        'Dim UserID As Integer = CInt(Session("UserID"))
        Select Case CInt(Data(0))
            Case 1 'Fill Tracker
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select a.app_tracker_id,a.app_tracker_no from app_tracker_master a where a.fsd_created_status=1 and a.uat_ready_conf_status is null")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
                CallBackReturn += "¥"
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select application_id,application_name from app_application_master where status_id=1")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2 'Fill tracker details
                Dim TrackerID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select a.app_tracker_id,a.application_id,a.brd_priority,a.brd_remarks,b.brdfile_name,a.created_on,a.created_name,a.approved_name,a.brd_signoff_name,a.approve_brdsoff_name,c.fsdfile_name,a.fsd_created_on,a.fsd_created_name,a.fsd_created_remark,a.approved_on,a.brd_signoff_on,a.approve_brdsoff_on,isnull(a.ServiceReq_No,''),isnull(a.Jira_No,''),a.app_brd_id,a.app_fsd_id from app_tracker_master a, app_tracker_brd_attach b, app_tracker_fsd_attach c where a.app_tracker_id=" & TrackerID & " and a.app_tracker_id=b.app_tracker_id and a.app_tracker_id=c.app_tracker_id and b.app_tracker_id=c.app_tracker_id and a.app_brd_id=b.app_brd_id and a.app_fsd_id=c.app_fsd_id")
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "¥" + DT.Rows(0)(1).ToString() + "¥" + DT.Rows(0)(2).ToString() + "¥" + DT.Rows(0)(3).ToString() + "¥" + DT.Rows(0)(4).ToString() + "¥" + CDate(DT.Rows(0)(5)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(6).ToString() + "¥" + DT.Rows(0)(7).ToString() + "¥" + DT.Rows(0)(8).ToString() + "¥" + DT.Rows(0)(9).ToString() + "¥" + DT.Rows(0)(10).ToString() + "¥" + CDate(DT.Rows(0)(11)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(12).ToString() + "¥" + DT.Rows(0)(13).ToString() + "¥" + CDate(DT.Rows(0)(14)).ToString("dd/MMM/yyyy") + "¥" + CDate(DT.Rows(0)(15)).ToString("dd/MMM/yyyy") + "¥" + CDate(DT.Rows(0)(16)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(17).ToString() + "¥" + DT.Rows(0)(18).ToString() + "¥" + DT.Rows(0)(19).ToString() + "¥" + DT.Rows(0)(20).ToString()
                End If
        End Select
    End Sub
#End Region

#Region "Confirm"
    Private Sub initializeControls()
        cmbTracker.Focus()
        cmbApplication.Text = "-1"
        cmbPriority.Text = "-1"

    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))

        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
        Dim FileName As String = ""
        Dim myFile As HttpPostedFile = fupFsd.PostedFile
        Dim nFileLen As Integer = myFile.ContentLength
        If (nFileLen > 0) Then
            ContentType = myFile.ContentType
            FileName = myFile.FileName
            AttachImg = New Byte(nFileLen - 1) {}
            myFile.InputStream.Read(AttachImg, 0, nFileLen)
        End If

        Try

            Dim TrackerID As Integer = CInt(hdnTracker.Value)

            Dim Params(5) As SqlParameter
            Params(0) = New SqlParameter("@TrackerID", SqlDbType.Int)
            Params(0).Value = TrackerID
            Params(1) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
            Params(1).Value = AttachImg
            Params(2) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
            Params(2).Value = ContentType
            Params(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
            Params(3).Value = FileName
            Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(4).Direction = ParameterDirection.Output
            Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(5).Direction = ParameterDirection.Output
            DB.ExecuteNonQuery("SP_TRACKER_FSD_FINAL_SUBMIT", Params)
            ErrorFlag = CInt(Params(4).Value)
            Message = CStr(Params(5).Value)

        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "'); ")
        cl_script1.Append("        window.open('FSD_Final_Submission.aspx', '_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        If ErrorFlag = 0 Then
            initializeControls()
        End If
    End Sub
End Class
