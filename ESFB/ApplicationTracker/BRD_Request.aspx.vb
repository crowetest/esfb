﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class BRD_Request
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Upload BRD"

        If GF.FormAccess(CInt(Session("UserID")), 1295) = False And CInt(Session("BranchID")) <> 0 Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        If Not IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        End If
        Me.btnSave.Attributes.Add("onclick", "return SaveOnClick()")
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Application
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select application_id,application_name from app_application_master where status_id=1")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
        End Select
    End Sub
#End Region

#Region "Confirm"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim AppTrackerID As Integer = 0

        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
        Dim FileName As String = ""
        Dim myFile As HttpPostedFile = fupBrd.PostedFile
        Dim nFileLen As Integer = myFile.ContentLength
        If (nFileLen > 0) Then
            ContentType = myFile.ContentType
            FileName = myFile.FileName
            AttachImg = New Byte(nFileLen - 1) {}
            myFile.InputStream.Read(AttachImg, 0, nFileLen)
        End If

        Try
         
            Dim AppID As Integer = CInt(hdnApplication.Value)
            Dim PriorityID As Integer = CInt(Me.cmbPriority.SelectedValue)
            Dim Remark As String = CStr(Me.txtRemark.Text)
            Dim Name As String = CStr(Me.txtbrdName.Text)
            Dim ReqNo As String = CStr(Me.txtSerReqNo.Text)

            Dim Params(12) As SqlParameter
            Params(0) = New SqlParameter("@ApplicationID", SqlDbType.Int)
            Params(0).Value = AppID
            Params(1) = New SqlParameter("@PriorityID", SqlDbType.Int)
            Params(1).Value = PriorityID
            Params(2) = New SqlParameter("@Remark", SqlDbType.VarChar, 1000)
            Params(2).Value = Remark
            Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(3).Value = UserID
            Params(4) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(4).Value = BranchID
            Params(5) = New SqlParameter("@ReqNo", SqlDbType.VarChar, 50)
            Params(5).Value = ReqNo
            Params(6) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
            Params(6).Value = AttachImg
            Params(7) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
            Params(7).Value = ContentType
            Params(8) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
            Params(8).Value = FileName
            Params(9) = New SqlParameter("@AppTrackerID", SqlDbType.Int)
            Params(9).Direction = ParameterDirection.Output
            Params(10) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(10).Direction = ParameterDirection.Output
            Params(11) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(11).Direction = ParameterDirection.Output
            Params(12) = New SqlParameter("@Name", SqlDbType.VarChar, 1000)
            Params(12).Value = Name
            DB.ExecuteNonQuery("SP_TRACKER_BRD_REQUEST", Params)
            ErrorFlag = CInt(Params(10).Value)
            Message = CStr(Params(11).Value)
            AppTrackerID = CInt(Params(9).Value)

        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "'); ")
        cl_script1.Append("        window.open('BRD_Request.aspx', '_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        If ErrorFlag = 0 Then
            initializeControls()
        End If

    End Sub
    Private Sub initializeControls()
        cmbApplication.Focus()
        cmbPriority.Text = "-1"
        txtRemark.Text = ""
    End Sub
#End Region

End Class
