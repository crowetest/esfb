﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewProgressDtls
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim TrackerID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            TrackerID = CInt(GF.Decrypt(Request.QueryString.Get("TrackerID")))
            hdnReportID.Value = TrackerID.ToString()

            Dim EmpCode As Integer
            Dim EmpName As String

            Dim DT As New DataTable
            Dim DT_Sum As New DataTable

            Dim TR22 As New TableRow
            Dim TR55 As New TableRow
            Dim TR66 As New TableRow
            Dim TR77 As New TableRow
            Dim TR88 As New TableRow
            Dim TR89 As New TableRow
            Dim TR51 As New TableRow
            Dim TR99 As New TableRow

            Dim TR30, TR31, TR32, TR33, TR34, TR35, TR36, TR37, TR38, TR39, TR40, TR41, TR42, TR43, TR44, TR45, TR46, TR47, TR48, TR49, TR50, TR52, TR53, TR54, TR56, TR57, TR58, TR59, TR60, TR61, TR62, TR63 As New TableRow
            Dim SqlStr As String

            SqlStr = "select a.app_tracker_id,a.app_tracker_no,a.application_name, case when a.brd_priority=1 then 'LOW' when a.brd_priority=2 then 'MEDIUM' else 'HIGH' end Priorty, " & _
                        "a.created_on, a.created_name + ' (' +convert(varchar,a.created_by) +')' Created, a.Created_Branch_name, " & _
                        "a.Created_dept_name,a.brd_remarks,d.brdfile_name Brd,isnull(f.fsdfile_name,'') Fsd, " & _
                        "case when created_status=1 and approved_status is null then 'REPORTING LEVEL PENDING' " & _
                        "when approved_status=1 and brd_signoff_status is null then 'BRD SIGNOFF PENDING' " & _
                        "when approved_status=2 and brd_signoff_status is null then 'REJECTED BY REPORTING OFFICER' " & _
                        "when approved_status=3 and brd_signoff_status is null then 'RETURNED BY REPORTING OFFICER FOR REWORK' " & _
                        "when brd_signoff_status=1 and approve_brdsoff_status is null and created_status=1 then 'BRD SIGNOFF APPROVAL PENDING' " & _
                        "when brd_signoff_status=1 and approve_brdsoff_status is null and created_status=2 then 'BRD RESUBMITTED, SIGNOFF APPROVAL PENDING' " & _
                        "when brd_signoff_status=2 and approve_brdsoff_status is null then 'REJECTED IN BRD SIGNOFF LEVEL' " & _
                        "when approve_brdsoff_status=1 and fsd_created_status is null then 'FSD UPDATION PENDING' " & _
                        "when approve_brdsoff_status=2 and fsd_created_status is null and created_status=1 then 'HOLD BY BRD SIGNOFF APPROVAL LEVEL' " & _
                        "when approve_brdsoff_status=3 and fsd_created_status is null then 'REJECTED IN BRD SIGNOFF APPROVAL LEVEL' " & _
                        "when fsd_created_status=1 and fsd_signoff_status is null then 'FSD SIGNOFF PENDING' " & _
                        "when fsd_signoff_status=1 and uat_ready_conf_status is null then 'UAT READY CONFIRMATION PENDING' " & _
                        "when uat_ready_conf_status=1 and uat_over_conf_status is null then 'UAT OVER/COMPLETE CONFIRMATION PENDING' " & _
                        "when uat_ready_conf_status is null and uat_over_conf_status=1 and uat_signoff_status is null then 'UAT READY CONFIRMATION PENDING' " & _
                        "when uat_ready_conf_status=1 and uat_over_conf_status=2 and uat_signoff_status is null then 'UAT SIGNOFF PENDING' " & _
                        "when uat_signoff_status=1 and preprod_conf_status is null then 'PRE PRODUCTION CONFIRMATON PENDING' " & _
                        "when preprod_conf_status=1 and preprod_signoff_status is null then 'PRE PRODUCTION SIGNOFF PENDING' " & _
                        "when preprod_signoff_status=1 and prodmove_conf_status is null then 'PRODUCTION MOVE CONFIRMATON PENDING' " & _
                        "when prodmove_conf_status=1 and prodmove_signoff_status is null then 'PRODUCTION MOVE SIGNOFF PENDING'" & _
                        "when prodmove_signoff_status=1 then 'PRODUCTION MOVE SIGNOFF COMPLETE' " & _
                        "else '' end Remarks,isnull(a.approved_on,''),isnull(a.approved_name,''),isnull(a.approved_remark,''),isnull(a.brd_signoff_on,''),isnull(a.brd_signoff_name,''),isnull(a.brd_signoff_remark,''),isnull(a.approve_brdsoff_on,''),isnull(a.approve_brdsoff_name,''),isnull(a.approve_brdsoff_remark,''),isnull(a.fsd_created_on,''),isnull(a.fsd_created_name,''),isnull(a.fsd_created_remark,''),isnull(a.fsd_signoff_on,''),isnull(a.fsd_signoff_name,''),isnull(a.fsd_signoff_remark,''),isnull(a.uat_ready_conf_on,''),isnull(a.uat_ready_conf_name,''),isnull(a.uat_ready_conf_remark,''),isnull(a.uat_over_conf_on,''),isnull(a.uat_over_conf_name,''),isnull(a.uat_over_conf_remark,''),isnull(a.uat_signoff_on,''),isnull(a.uat_signoff_name,''),isnull(a.uat_signoff_remark,''),isnull(a.prodmove_conf_on,''),isnull(a.prodmove_conf_name,''),isnull(a.prodmove_conf_remark,''),isnull(a.prodmove_signoff_on,''),isnull(a.prodmove_signoff_name,''),isnull(a.prodmove_signoff_remark,''),isnull(a.preprod_conf_on,''),isnull(a.preprod_conf_name,''),isnull(a.preprod_conf_remark,''),isnull(a.preprod_signoff_on,''),isnull(a.preprod_signoff_name,''),isnull(a.preprod_signoff_remark,''),a.app_brd_id,isnull(a.app_fsd_id,0), " & _
                        "isnull(a.ServiceReq_No,''),isnull(a.Jira_No,''),(select count(*) from app_tracker_uat_dtl u where u.app_tracker_id=a.app_tracker_id) UatCnt " & _
                        "from app_tracker_master a left join app_tracker_fsd_attach f on a.app_fsd_id=f.app_fsd_id, app_tracker_brd_attach d " & _
                        "where a.app_tracker_id=d.app_tracker_id and a.app_brd_id=d.app_brd_id and a.app_tracker_id= " & TrackerID.ToString & ""

            DT = DB.ExecuteDataSet(SqlStr).Tables(0)

            RH.Heading(CStr(Session("FirmName")), tb, "APPLICATION TRACKER", 100)

            Dim DR As DataRow
            If DT.Rows.Count > 0 Then

                Dim TR55_00, TR55_01, TR55_02, TR55_03, TR55_04, TR55_05, TR55_06, TR55_07 As New TableCell

                RH.AddColumn(TR55, TR55_00, 23, 23, "l", "&nbsp;&nbsp;TRACKER NO")
                RH.AddColumn(TR55, TR55_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR55, TR55_02, 23, 23, "l", DT.Rows(0)(1).ToString())
                RH.AddColumn(TR55, TR55_03, 3, 3, "l", "")
                RH.AddColumn(TR55, TR55_04, 3, 3, "l", "")
                RH.AddColumn(TR55, TR55_05, 23, 23, "l", "&nbsp;&nbsp;CREATED ON")
                RH.AddColumn(TR55, TR55_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR55, TR55_07, 23, 23, "l", CDate(DT.Rows(0)(4)).ToString("dd/MMM/yyyy"))

                TR55_00.BackColor = Drawing.Color.WhiteSmoke
                TR55_01.BackColor = Drawing.Color.WhiteSmoke
                TR55_02.BackColor = Drawing.Color.WhiteSmoke
                TR55_05.BackColor = Drawing.Color.WhiteSmoke
                TR55_06.BackColor = Drawing.Color.WhiteSmoke
                TR55_07.BackColor = Drawing.Color.WhiteSmoke

                tb.Controls.Add(TR55)

                'RH.BlankRow(tb, 1)

                Dim TR66_00, TR66_01, TR66_02, TR66_03, TR66_04, TR66_05, TR66_06, TR66_07 As New TableCell
                RH.AddColumn(TR66, TR66_00, 23, 23, "l", "&nbsp;&nbsp;APPLICATION")
                RH.AddColumn(TR66, TR66_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR66, TR66_02, 23, 23, "l", DT.Rows(0)(2).ToString())
                RH.AddColumn(TR66, TR66_03, 3, 3, "l", "")
                RH.AddColumn(TR66, TR66_04, 3, 3, "l", "")
                RH.AddColumn(TR66, TR66_05, 23, 23, "l", "&nbsp;&nbsp;CREATED BY")
                RH.AddColumn(TR66, TR66_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR66, TR66_07, 23, 23, "l", DT.Rows(0)(5).ToString())
                TR66_00.BackColor = Drawing.Color.WhiteSmoke
                TR66_01.BackColor = Drawing.Color.WhiteSmoke
                TR66_02.BackColor = Drawing.Color.WhiteSmoke
                TR66_05.BackColor = Drawing.Color.WhiteSmoke
                TR66_06.BackColor = Drawing.Color.WhiteSmoke
                TR66_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR66)

                Dim TR77_00, TR77_01, TR77_02, TR77_03, TR77_04, TR77_05, TR77_06, TR77_07 As New TableCell
                RH.AddColumn(TR77, TR77_00, 23, 23, "l", "&nbsp;&nbsp;PRIORITY")
                RH.AddColumn(TR77, TR77_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR77, TR77_02, 23, 23, "l", DT.Rows(0)(3).ToString())
                RH.AddColumn(TR77, TR77_03, 3, 3, "l", "")
                RH.AddColumn(TR77, TR77_04, 3, 3, "l", "")
                RH.AddColumn(TR77, TR77_05, 23, 23, "l", "&nbsp;&nbsp;DEPARTMENT")
                RH.AddColumn(TR77, TR77_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR77, TR77_07, 23, 23, "l", DT.Rows(0)(7).ToString())
                TR77_00.BackColor = Drawing.Color.WhiteSmoke
                TR77_01.BackColor = Drawing.Color.WhiteSmoke
                TR77_02.BackColor = Drawing.Color.WhiteSmoke
                TR77_05.BackColor = Drawing.Color.WhiteSmoke
                TR77_06.BackColor = Drawing.Color.WhiteSmoke
                TR77_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR77)

                Dim TR88_00, TR88_01, TR88_02, TR88_03, TR88_04, TR88_05, TR88_06, TR88_07 As New TableCell
                RH.AddColumn(TR88, TR88_00, 23, 23, "l", "&nbsp;&nbsp;DESCRIPTION")
                RH.AddColumn(TR88, TR88_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR88, TR88_02, 23, 23, "l", DT.Rows(0)(8).ToString())
                RH.AddColumn(TR88, TR88_03, 3, 3, "l", "")
                RH.AddColumn(TR88, TR88_04, 3, 3, "l", "")
                RH.AddColumn(TR88, TR88_05, 23, 23, "l", "&nbsp;&nbsp;CURRENT STATUS")
                RH.AddColumn(TR88, TR88_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR88, TR88_07, 23, 23, "l", DT.Rows(0)(11).ToString())
                TR88_00.BackColor = Drawing.Color.WhiteSmoke
                TR88_01.BackColor = Drawing.Color.WhiteSmoke
                TR88_02.BackColor = Drawing.Color.WhiteSmoke
                TR88_05.BackColor = Drawing.Color.WhiteSmoke
                TR88_06.BackColor = Drawing.Color.WhiteSmoke
                TR88_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR88)

                Dim TR89_00, TR89_01, TR89_02, TR89_03, TR89_04, TR89_05, TR89_06, TR89_07 As New TableCell
                RH.AddColumn(TR89, TR89_00, 23, 23, "l", "&nbsp;&nbsp;INCIDENT/SER.REQUEST NO.")
                RH.AddColumn(TR89, TR89_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR89, TR89_02, 23, 23, "l", DT.Rows(0)(50).ToString())
                RH.AddColumn(TR89, TR89_03, 3, 3, "l", "")
                RH.AddColumn(TR89, TR89_04, 3, 3, "l", "")
                RH.AddColumn(TR89, TR89_05, 23, 23, "l", "&nbsp;&nbsp;JIRA NO.")
                RH.AddColumn(TR89, TR89_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR89, TR89_07, 23, 23, "l", DT.Rows(0)(51).ToString())
                TR89_00.BackColor = Drawing.Color.WhiteSmoke
                TR89_01.BackColor = Drawing.Color.WhiteSmoke
                TR89_02.BackColor = Drawing.Color.WhiteSmoke
                TR89_05.BackColor = Drawing.Color.WhiteSmoke
                TR89_06.BackColor = Drawing.Color.WhiteSmoke
                TR89_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR89)

                Dim TR30_00, TR30_01, TR30_02, TR30_03, TR30_04, TR30_05, TR30_06, TR30_07 As New TableCell
                RH.AddColumn(TR30, TR30_00, 23, 23, "l", "&nbsp;&nbsp;BRD")
                RH.AddColumn(TR30, TR30_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR30, TR30_02, 23, 23, "l", "<a href='../ShowTrackerFormat.aspx?TrackerID=" + DT.Rows(0)(0).ToString() + "&UatID=" + DT.Rows(0)(48).ToString() + "&StatusID=1' style='text-align:right;' target='_blank'>" + DT.Rows(0)(9).ToString() + "</a>")
                RH.AddColumn(TR30, TR30_03, 3, 3, "l", "")
                RH.AddColumn(TR30, TR30_04, 3, 3, "l", "")
                RH.AddColumn(TR30, TR30_05, 23, 23, "l", "&nbsp;&nbsp;FSD")
                RH.AddColumn(TR30, TR30_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR30, TR30_07, 23, 23, "l", "<a href='../ShowTrackerFormat.aspx?TrackerID=" + DT.Rows(0)(0).ToString() + "&UatID=" + DT.Rows(0)(49).ToString() + "&StatusID=2' style='text-align:right;' target='_blank'>" + DT.Rows(0)(10).ToString() + "</a>")
                TR30_00.BackColor = Drawing.Color.WhiteSmoke
                TR30_01.BackColor = Drawing.Color.WhiteSmoke
                TR30_02.BackColor = Drawing.Color.WhiteSmoke
                TR30_05.BackColor = Drawing.Color.WhiteSmoke
                TR30_06.BackColor = Drawing.Color.WhiteSmoke
                TR30_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR30)

                RH.DrawLine(tb, 100)
                Dim TR0 As New TableRow
                TR0.BackColor = Drawing.Color.LightSlateGray
                Dim TR0_00 As New TableCell
                RH.AddColumn(TR0, TR0_00, 100, 100, "c", "BRD APPROVED DETAILS")
                tb.Controls.Add(TR0)
                TR0_00.Font.Bold = True
                TR0_00.Font.Size = 20
                TR0_00.ForeColor = Drawing.Color.White
                RH.DrawLine(tb, 100)

                Dim TR37_00, TR37_01, TR37_02, TR37_03, TR37_04, TR37_05, TR37_06, TR37_07 As New TableCell
                RH.AddColumn(TR37, TR37_00, 23, 23, "l", "&nbsp;&nbsp;APPROVED ON")
                RH.AddColumn(TR37, TR37_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(12).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR37, TR37_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR37, TR37_02, 23, 23, "l", CDate(DT.Rows(0)(12)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR37, TR37_03, 3, 3, "l", "")
                RH.AddColumn(TR37, TR37_04, 3, 3, "l", "")
                RH.AddColumn(TR37, TR37_05, 23, 23, "l", "&nbsp;&nbsp;APPROVED BY")
                RH.AddColumn(TR37, TR37_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR37, TR37_07, 23, 23, "l", DT.Rows(0)(13).ToString())
                TR37_00.BackColor = Drawing.Color.WhiteSmoke
                TR37_01.BackColor = Drawing.Color.WhiteSmoke
                TR37_02.BackColor = Drawing.Color.WhiteSmoke
                TR37_05.BackColor = Drawing.Color.WhiteSmoke
                TR37_06.BackColor = Drawing.Color.WhiteSmoke
                TR37_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR37)


                Dim TR38_00, TR38_01, TR38_02, TR38_03, TR38_04, TR38_05, TR38_06, TR38_07 As New TableCell
                RH.AddColumn(TR38, TR38_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR38, TR38_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR38, TR38_02, 76, 76, "l", DT.Rows(0)(14).ToString())
                TR38_00.BackColor = Drawing.Color.WhiteSmoke
                TR38_01.BackColor = Drawing.Color.WhiteSmoke
                TR38_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR38)

                RH.DrawLine(tb, 100)
                Dim TR00 As New TableRow
                TR00.BackColor = Drawing.Color.LightSlateGray
                Dim TR00_00 As New TableCell
                RH.AddColumn(TR00, TR00_00, 100, 100, "c", "BRD SIGNOFF DETAILS")
                TR00_00.Font.Bold = True
                TR00_00.Font.Size = 20
                TR00_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR00)
                RH.DrawLine(tb, 100)

                Dim TR39_00, TR39_01, TR39_02, TR39_03, TR39_04, TR39_05, TR39_06, TR39_07 As New TableCell
                RH.AddColumn(TR39, TR39_00, 23, 23, "l", "&nbsp;&nbsp;SIGNOFF ON")
                RH.AddColumn(TR39, TR39_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(15).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR39, TR39_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR39, TR39_02, 23, 23, "l", CDate(DT.Rows(0)(15)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR39, TR39_03, 3, 3, "l", "")
                RH.AddColumn(TR39, TR39_04, 3, 3, "l", "")
                RH.AddColumn(TR39, TR39_05, 23, 23, "l", "&nbsp;&nbsp;SIGNOFF BY")
                RH.AddColumn(TR39, TR39_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR39, TR39_07, 23, 23, "l", DT.Rows(0)(16).ToString())
                TR39_00.BackColor = Drawing.Color.WhiteSmoke
                TR39_01.BackColor = Drawing.Color.WhiteSmoke
                TR39_02.BackColor = Drawing.Color.WhiteSmoke
                TR39_05.BackColor = Drawing.Color.WhiteSmoke
                TR39_06.BackColor = Drawing.Color.WhiteSmoke
                TR39_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR39)

                Dim TR40_00, TR40_01, TR40_02 As New TableCell
                RH.AddColumn(TR40, TR40_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR40, TR40_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR40, TR40_02, 76, 76, "l", DT.Rows(0)(17).ToString())
                TR40_00.BackColor = Drawing.Color.WhiteSmoke
                TR40_01.BackColor = Drawing.Color.WhiteSmoke
                TR40_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR40)

                RH.DrawLine(tb, 100)
                Dim TR01 As New TableRow
                TR01.BackColor = Drawing.Color.LightSlateGray
                Dim TR01_00 As New TableCell
                RH.AddColumn(TR01, TR01_00, 100, 100, "c", "BRD SIGNOFF APPROVED DETAILS")
                TR01_00.Font.Bold = True
                TR01_00.Font.Size = 20
                TR01_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR01)
                RH.DrawLine(tb, 100)

                Dim TR41_00, TR41_01, TR41_02, TR41_03, TR41_04, TR41_05, TR41_06, TR41_07 As New TableCell
                RH.AddColumn(TR41, TR41_00, 23, 23, "l", "&nbsp;&nbsp;APPROVE ON")
                RH.AddColumn(TR41, TR41_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(18).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR41, TR41_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR41, TR41_02, 23, 23, "l", CDate(DT.Rows(0)(18)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR41, TR41_03, 3, 3, "l", "")
                RH.AddColumn(TR41, TR41_04, 3, 3, "l", "")
                RH.AddColumn(TR41, TR41_05, 23, 23, "l", "&nbsp;&nbsp;APPROVE BY")
                RH.AddColumn(TR41, TR41_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR41, TR41_07, 23, 23, "l", DT.Rows(0)(19).ToString())
                TR41_00.BackColor = Drawing.Color.WhiteSmoke
                TR41_01.BackColor = Drawing.Color.WhiteSmoke
                TR41_02.BackColor = Drawing.Color.WhiteSmoke
                TR41_05.BackColor = Drawing.Color.WhiteSmoke
                TR41_06.BackColor = Drawing.Color.WhiteSmoke
                TR41_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR41)

                Dim TR42_00, TR42_01, TR42_02 As New TableCell
                RH.AddColumn(TR42, TR42_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR42, TR42_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR42, TR42_02, 76, 76, "l", DT.Rows(0)(20).ToString())
                TR42_00.BackColor = Drawing.Color.WhiteSmoke
                TR42_01.BackColor = Drawing.Color.WhiteSmoke
                TR42_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR42)

                RH.DrawLine(tb, 100)
                Dim TR02 As New TableRow
                TR02.BackColor = Drawing.Color.LightSlateGray
                Dim TR02_00 As New TableCell
                RH.AddColumn(TR02, TR02_00, 100, 100, "c", "FSD UPLOADED DETAILS")
                TR02_00.Font.Bold = True
                TR02_00.Font.Size = 20
                TR02_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR02)
                RH.DrawLine(tb, 100)

                Dim TR43_00, TR43_01, TR43_02, TR43_03, TR43_04, TR43_05, TR43_06, TR43_07 As New TableCell
                RH.AddColumn(TR43, TR43_00, 23, 23, "l", "&nbsp;&nbsp;UPLOADED ON")
                RH.AddColumn(TR43, TR43_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(21).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR43, TR43_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR43, TR43_02, 23, 23, "l", CDate(DT.Rows(0)(21)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR43, TR43_03, 3, 3, "l", "")
                RH.AddColumn(TR43, TR43_04, 3, 3, "l", "")
                RH.AddColumn(TR43, TR43_05, 23, 23, "l", "&nbsp;&nbsp;UPLOADED BY")
                RH.AddColumn(TR43, TR43_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR43, TR43_07, 23, 23, "l", DT.Rows(0)(22).ToString())
                TR43_00.BackColor = Drawing.Color.WhiteSmoke
                TR43_01.BackColor = Drawing.Color.WhiteSmoke
                TR43_02.BackColor = Drawing.Color.WhiteSmoke
                TR43_05.BackColor = Drawing.Color.WhiteSmoke
                TR43_06.BackColor = Drawing.Color.WhiteSmoke
                TR43_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR43)

                Dim TR44_00, TR44_01, TR44_02 As New TableCell
                RH.AddColumn(TR44, TR44_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR44, TR44_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR44, TR44_02, 76, 76, "l", DT.Rows(0)(23).ToString())
                TR44_00.BackColor = Drawing.Color.WhiteSmoke
                TR44_01.BackColor = Drawing.Color.WhiteSmoke
                TR44_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR44)

                RH.DrawLine(tb, 100)
                Dim TR03 As New TableRow
                TR03.BackColor = Drawing.Color.LightSlateGray
                Dim TR03_00 As New TableCell
                RH.AddColumn(TR03, TR03_00, 100, 100, "c", "FSD SIGNOFF DETAILS")
                TR03_00.Font.Bold = True
                TR03_00.Font.Size = 20
                TR03_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR03)
                RH.DrawLine(tb, 100)

                Dim TR45_00, TR45_01, TR45_02, TR45_03, TR45_04, TR45_05, TR45_06, TR45_07 As New TableCell
                RH.AddColumn(TR45, TR45_00, 23, 23, "l", "&nbsp;&nbsp;SIGNOFF ON")
                RH.AddColumn(TR45, TR45_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(24).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR45, TR45_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR45, TR45_02, 23, 23, "l", CDate(DT.Rows(0)(24)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR45, TR45_03, 3, 3, "l", "")
                RH.AddColumn(TR45, TR45_04, 3, 3, "l", "")
                RH.AddColumn(TR45, TR45_05, 23, 23, "l", "&nbsp;&nbsp;SIGNOFF BY")
                RH.AddColumn(TR45, TR45_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR45, TR45_07, 23, 23, "l", DT.Rows(0)(25).ToString())
                TR45_00.BackColor = Drawing.Color.WhiteSmoke
                TR45_01.BackColor = Drawing.Color.WhiteSmoke
                TR45_02.BackColor = Drawing.Color.WhiteSmoke
                TR45_05.BackColor = Drawing.Color.WhiteSmoke
                TR45_06.BackColor = Drawing.Color.WhiteSmoke
                TR45_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR45)

                Dim TR46_00, TR46_01, TR46_02 As New TableCell
                RH.AddColumn(TR46, TR46_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR46, TR46_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR46, TR46_02, 76, 76, "l", DT.Rows(0)(26).ToString())
                TR46_00.BackColor = Drawing.Color.WhiteSmoke
                TR46_01.BackColor = Drawing.Color.WhiteSmoke
                TR46_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR46)

                RH.DrawLine(tb, 100)
                Dim TR04 As New TableRow
                TR04.BackColor = Drawing.Color.LightSlateGray
                Dim TR04_00 As New TableCell
                RH.AddColumn(TR04, TR04_00, 100, 100, "c", "UAT READY CONFIRM DETAILS")
                TR04_00.Font.Bold = True
                TR04_00.Font.Size = 20
                TR04_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR04)
                RH.DrawLine(tb, 100)

                Dim TR47_00, TR47_01, TR47_02, TR47_03, TR47_04, TR47_05, TR47_06, TR47_07 As New TableCell
                RH.AddColumn(TR47, TR47_00, 23, 23, "l", "&nbsp;&nbsp;CONFRIM ON")
                RH.AddColumn(TR47, TR47_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(27).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR47, TR47_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR47, TR47_02, 23, 23, "l", CDate(DT.Rows(0)(27)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR47, TR47_03, 3, 3, "l", "")
                RH.AddColumn(TR47, TR47_04, 3, 3, "l", "")
                RH.AddColumn(TR47, TR47_05, 23, 23, "l", "&nbsp;&nbsp;CONFRIM BY")
                RH.AddColumn(TR47, TR47_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR47, TR47_07, 23, 23, "l", DT.Rows(0)(28).ToString())
                TR47_00.BackColor = Drawing.Color.WhiteSmoke
                TR47_01.BackColor = Drawing.Color.WhiteSmoke
                TR47_02.BackColor = Drawing.Color.WhiteSmoke
                TR47_05.BackColor = Drawing.Color.WhiteSmoke
                TR47_06.BackColor = Drawing.Color.WhiteSmoke
                TR47_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR47)

                Dim TR48_00, TR48_01, TR48_02 As New TableCell
                RH.AddColumn(TR48, TR48_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR48, TR48_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR48, TR48_02, 76, 76, "l", DT.Rows(0)(29).ToString())
                TR48_00.BackColor = Drawing.Color.WhiteSmoke
                TR48_01.BackColor = Drawing.Color.WhiteSmoke
                TR48_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR48)

                RH.DrawLine(tb, 100)
                Dim TR05 As New TableRow
                TR05.BackColor = Drawing.Color.LightSlateGray
                Dim TR05_00 As New TableCell
                RH.AddColumn(TR05, TR05_00, 100, 100, "c", "UAT OVER CONFIRM DETAILS")
                TR05_00.Font.Bold = True
                TR05_00.Font.Size = 20
                TR05_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR05)
                RH.DrawLine(tb, 100)

                Dim TR49_00, TR49_01, TR49_02, TR49_03, TR49_04, TR49_05, TR49_06, TR49_07 As New TableCell
                RH.AddColumn(TR49, TR49_00, 23, 23, "l", "&nbsp;&nbsp;OVER ON")
                RH.AddColumn(TR49, TR49_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(30).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR49, TR49_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR49, TR49_02, 23, 23, "l", CDate(DT.Rows(0)(30)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR49, TR49_03, 3, 3, "l", "")
                RH.AddColumn(TR49, TR49_04, 3, 3, "l", "")
                RH.AddColumn(TR49, TR49_05, 23, 23, "l", "&nbsp;&nbsp;OVER BY")
                RH.AddColumn(TR49, TR49_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR49, TR49_07, 23, 23, "l", DT.Rows(0)(31).ToString())
                TR49_00.BackColor = Drawing.Color.WhiteSmoke
                TR49_01.BackColor = Drawing.Color.WhiteSmoke
                TR49_02.BackColor = Drawing.Color.WhiteSmoke
                TR49_05.BackColor = Drawing.Color.WhiteSmoke
                TR49_06.BackColor = Drawing.Color.WhiteSmoke
                TR49_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR49)

                Dim TR50_00, TR50_01, TR50_02 As New TableCell
                RH.AddColumn(TR50, TR50_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR50, TR50_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR50, TR50_02, 76, 76, "l", DT.Rows(0)(32).ToString())
                TR50_00.BackColor = Drawing.Color.WhiteSmoke
                TR50_01.BackColor = Drawing.Color.WhiteSmoke
                TR50_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR50)

                If (CInt(DT.Rows(0)(52)) > 0) Then
                    RH.DrawLine(tb, 100)
                    Dim TR63_00 As New TableCell
                    RH.AddColumn(TR63, TR63_00, 100, 100, "c", "<a href='ViewUATDetails.aspx?TrackerID=" + GF.Encrypt(DT.Rows(0)(0).ToString()) + "' style='text-align:right;' target='_blank'>View UAT Details</a>")
                    TR63_00.BackColor = Drawing.Color.Aquamarine
                    tb.Controls.Add(TR63)
                End If

                RH.DrawLine(tb, 100)
                Dim TR06 As New TableRow
                TR06.BackColor = Drawing.Color.LightSlateGray
                Dim TR06_00 As New TableCell
                RH.AddColumn(TR06, TR06_00, 100, 100, "c", "UAT SIGNOFF DETAILS")
                TR06_00.Font.Bold = True
                TR06_00.Font.Size = 20
                TR06_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR06)
                RH.DrawLine(tb, 100)

                Dim TR52_00, TR52_01, TR52_02, TR52_03, TR52_04, TR52_05, TR52_06, TR52_07 As New TableCell
                RH.AddColumn(TR52, TR52_00, 23, 23, "l", "&nbsp;&nbsp;SIGNOFF ON")
                RH.AddColumn(TR52, TR52_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(33).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR52, TR52_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR52, TR52_02, 23, 23, "l", CDate(DT.Rows(0)(33)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR52, TR52_03, 3, 3, "l", "")
                RH.AddColumn(TR52, TR52_04, 3, 3, "l", "")
                RH.AddColumn(TR52, TR52_05, 23, 23, "l", "&nbsp;&nbsp;SIGNOFF BY")
                RH.AddColumn(TR52, TR52_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR52, TR52_07, 23, 23, "l", DT.Rows(0)(34).ToString())
                TR52_00.BackColor = Drawing.Color.WhiteSmoke
                TR52_01.BackColor = Drawing.Color.WhiteSmoke
                TR52_02.BackColor = Drawing.Color.WhiteSmoke
                TR52_05.BackColor = Drawing.Color.WhiteSmoke
                TR52_06.BackColor = Drawing.Color.WhiteSmoke
                TR52_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR52)

                Dim TR53_00, TR53_01, TR53_02 As New TableCell
                RH.AddColumn(TR53, TR53_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR53, TR53_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR53, TR53_02, 76, 76, "l", DT.Rows(0)(35).ToString())
                TR53_00.BackColor = Drawing.Color.WhiteSmoke
                TR53_01.BackColor = Drawing.Color.WhiteSmoke
                TR53_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR53)
                '----------------

                RH.DrawLine(tb, 100)
                Dim TR09 As New TableRow
                TR09.BackColor = Drawing.Color.LightSlateGray
                Dim TR09_00 As New TableCell
                RH.AddColumn(TR09, TR09_00, 100, 100, "c", "PRE PRODUCTION CONFIRM DETAILS")
                TR09_00.Font.Bold = True
                TR09_00.Font.Size = 20
                TR09_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR09)
                RH.DrawLine(tb, 100)

                Dim TR59_00, TR59_01, TR59_02, TR59_03, TR59_04, TR59_05, TR59_06, TR59_07 As New TableCell
                RH.AddColumn(TR59, TR59_00, 23, 23, "l", "&nbsp;&nbsp;CONFIRM ON")
                RH.AddColumn(TR59, TR59_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(33).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR59, TR59_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR59, TR59_02, 23, 23, "l", CDate(DT.Rows(0)(42)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR59, TR59_03, 3, 3, "l", "")
                RH.AddColumn(TR59, TR59_04, 3, 3, "l", "")
                RH.AddColumn(TR59, TR59_05, 23, 23, "l", "&nbsp;&nbsp;CONFIRM BY")
                RH.AddColumn(TR59, TR59_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR59, TR59_07, 23, 23, "l", DT.Rows(0)(43).ToString())
                TR59_00.BackColor = Drawing.Color.WhiteSmoke
                TR59_01.BackColor = Drawing.Color.WhiteSmoke
                TR59_02.BackColor = Drawing.Color.WhiteSmoke
                TR59_05.BackColor = Drawing.Color.WhiteSmoke
                TR59_06.BackColor = Drawing.Color.WhiteSmoke
                TR59_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR59)

                Dim TR60_00, TR60_01, TR60_02 As New TableCell
                RH.AddColumn(TR60, TR60_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR60, TR60_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR60, TR60_02, 76, 76, "l", DT.Rows(0)(44).ToString())
                TR60_00.BackColor = Drawing.Color.WhiteSmoke
                TR60_01.BackColor = Drawing.Color.WhiteSmoke
                TR60_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR60)

                RH.DrawLine(tb, 100)
                Dim TR10 As New TableRow
                TR10.BackColor = Drawing.Color.LightSlateGray
                Dim TR10_00 As New TableCell
                RH.AddColumn(TR10, TR10_00, 100, 100, "c", "PRE PRODUCTION SIGNOFF DETAILS")
                TR10_00.Font.Bold = True
                TR10_00.Font.Size = 20
                TR10_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR10)
                RH.DrawLine(tb, 100)

                Dim TR61_00, TR61_01, TR61_02, TR61_03, TR61_04, TR61_05, TR61_06, TR61_07 As New TableCell
                RH.AddColumn(TR61, TR61_00, 23, 23, "l", "&nbsp;&nbsp;SIGNOFF ON")
                RH.AddColumn(TR61, TR61_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(33).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR61, TR61_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR61, TR61_02, 23, 23, "l", CDate(DT.Rows(0)(45)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR61, TR61_03, 3, 3, "l", "")
                RH.AddColumn(TR61, TR61_04, 3, 3, "l", "")
                RH.AddColumn(TR61, TR61_05, 23, 23, "l", "&nbsp;&nbsp;SIGNOFF BY")
                RH.AddColumn(TR61, TR61_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR61, TR61_07, 23, 23, "l", DT.Rows(0)(46).ToString())
                TR61_00.BackColor = Drawing.Color.WhiteSmoke
                TR61_01.BackColor = Drawing.Color.WhiteSmoke
                TR61_02.BackColor = Drawing.Color.WhiteSmoke
                TR61_05.BackColor = Drawing.Color.WhiteSmoke
                TR61_06.BackColor = Drawing.Color.WhiteSmoke
                TR61_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR61)

                Dim TR62_00, TR62_01, TR62_02 As New TableCell
                RH.AddColumn(TR62, TR62_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR62, TR62_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR62, TR62_02, 76, 76, "l", DT.Rows(0)(47).ToString())
                TR62_00.BackColor = Drawing.Color.WhiteSmoke
                TR62_01.BackColor = Drawing.Color.WhiteSmoke
                TR62_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR62)

                '----------------
                RH.DrawLine(tb, 100)
                Dim TR07 As New TableRow
                TR07.BackColor = Drawing.Color.LightSlateGray
                Dim TR07_00 As New TableCell
                RH.AddColumn(TR07, TR07_00, 100, 100, "c", "PRODUCTION MOVE CONFIRM DETAILS")
                TR07_00.Font.Bold = True
                TR07_00.Font.Size = 20
                TR07_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR07)
                RH.DrawLine(tb, 100)

                Dim TR54_00, TR54_01, TR54_02, TR54_03, TR54_04, TR54_05, TR54_06, TR54_07 As New TableCell
                RH.AddColumn(TR54, TR54_00, 23, 23, "l", "&nbsp;&nbsp;CONFIRM ON")
                RH.AddColumn(TR54, TR54_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(36).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR54, TR54_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR54, TR54_02, 23, 23, "l", CDate(DT.Rows(0)(36)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR54, TR54_03, 3, 3, "l", "")
                RH.AddColumn(TR54, TR54_04, 3, 3, "l", "")
                RH.AddColumn(TR54, TR54_05, 23, 23, "l", "&nbsp;&nbsp;CONFIRM BY")
                RH.AddColumn(TR54, TR54_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR54, TR54_07, 23, 23, "l", DT.Rows(0)(37).ToString())
                TR54_00.BackColor = Drawing.Color.WhiteSmoke
                TR54_01.BackColor = Drawing.Color.WhiteSmoke
                TR54_02.BackColor = Drawing.Color.WhiteSmoke
                TR54_05.BackColor = Drawing.Color.WhiteSmoke
                TR54_06.BackColor = Drawing.Color.WhiteSmoke
                TR54_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR54)

                Dim TR56_00, TR56_01, TR56_02 As New TableCell
                RH.AddColumn(TR56, TR56_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR56, TR56_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR56, TR56_02, 76, 76, "l", DT.Rows(0)(38).ToString())
                TR56_00.BackColor = Drawing.Color.WhiteSmoke
                TR56_01.BackColor = Drawing.Color.WhiteSmoke
                TR56_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR56)

                RH.DrawLine(tb, 100)
                Dim TR08 As New TableRow
                TR08.BackColor = Drawing.Color.LightSlateGray
                Dim TR08_00 As New TableCell
                RH.AddColumn(TR08, TR08_00, 100, 100, "c", "PRODUCTION MOVE SIGNOFF DETAILS")
                TR08_00.Font.Bold = True
                TR08_00.Font.Size = 20
                TR08_00.ForeColor = Drawing.Color.White
                tb.Controls.Add(TR08)
                RH.DrawLine(tb, 100)

                Dim TR57_00, TR57_01, TR57_02, TR57_03, TR57_04, TR57_05, TR57_06, TR57_07 As New TableCell
                RH.AddColumn(TR57, TR57_00, 23, 23, "l", "&nbsp;&nbsp;SIGNOFF ON")
                RH.AddColumn(TR57, TR57_01, 1, 1, "c", ":&nbsp;&nbsp;")
                If DT.Rows(0)(39).ToString = "01-01-1900 00:00:00" Then
                    RH.AddColumn(TR57, TR57_02, 23, 23, "l", "")
                Else
                    RH.AddColumn(TR57, TR57_02, 23, 23, "l", CDate(DT.Rows(0)(39)).ToString("dd/MMM/yyyy"))
                End If
                RH.AddColumn(TR57, TR57_03, 3, 3, "l", "")
                RH.AddColumn(TR57, TR57_04, 3, 3, "l", "")
                RH.AddColumn(TR57, TR57_05, 23, 23, "l", "&nbsp;&nbsp;SIGNOFF BY")
                RH.AddColumn(TR57, TR57_06, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR57, TR57_07, 23, 23, "l", DT.Rows(0)(40).ToString())
                TR57_00.BackColor = Drawing.Color.WhiteSmoke
                TR57_01.BackColor = Drawing.Color.WhiteSmoke
                TR57_02.BackColor = Drawing.Color.WhiteSmoke
                TR57_05.BackColor = Drawing.Color.WhiteSmoke
                TR57_06.BackColor = Drawing.Color.WhiteSmoke
                TR57_07.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR57)

                Dim TR58_00, TR58_01, TR58_02 As New TableCell
                RH.AddColumn(TR58, TR58_00, 23, 23, "l", "&nbsp;&nbsp;REMARKS")
                RH.AddColumn(TR58, TR58_01, 1, 1, "c", ":&nbsp;&nbsp;")
                RH.AddColumn(TR58, TR58_02, 76, 76, "l", DT.Rows(0)(41).ToString())
                TR58_00.BackColor = Drawing.Color.WhiteSmoke
                TR58_01.BackColor = Drawing.Color.WhiteSmoke
                TR58_02.BackColor = Drawing.Color.WhiteSmoke
                tb.Controls.Add(TR58)

                RH.DrawLine(tb, 100)

            End If
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
