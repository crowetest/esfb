﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    EnableEventValidation="false" CodeFile="Filter_Application.aspx.vb" Inherits="Filter_Application" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
        .style1
        {
            width: 15%;
        }
        .style2
        {
            width: 18%;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {   
                document.getElementById("<%= hdnApplication.ClientID %>").value=0;           
                ToServer("1ʘ", 1);
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                        {
                            if(Arg != "")
                            {
                                ComboFill(Arg, "<%= cmbApplication.ClientID %>");
                            }
                            break;
                        }
                }
            }

            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        
            function btnShow_onclick() {
                var TicketNo=document.getElementById("<%= txtTicketNo.ClientID %>").value;                       
                var frmdate=document.getElementById("<%= txtStartDt.ClientID %>").value;                  
                var todate=document.getElementById("<%= txtToDt.ClientID %>").value;               
               
                var Empcode = document.getElementById("<%= txtEmpcode.ClientID%>").value;    
                var SRNo = document.getElementById("<%= txtSerReq.ClientID%>").value;     
                var AppID = document.getElementById("<%= cmbApplication.ClientID %>").value;   
                
                var Priority = document.getElementById("<%= cmbFlag.ClientID %>").value;  
                var Brdname = document.getElementById("<%= txtBrdname.ClientID %>").value;   
                
                  var Opendate=document.getElementById("<%= TxtOpendate.ClientID %>").value;                  
                var Enddate=document.getElementById("<%= TxtEnddate.ClientID %>").value; 

                window.open("ViewProgressStatus_Filter.aspx?TicketNo=" + btoa(TicketNo) + "&frmdate=" + btoa(frmdate) + "&todate=" + btoa(todate) + "&Empcode=" + btoa(Empcode) + "&AppID=" + btoa(AppID) + "&SRNo=" + btoa(SRNo) + "&Priority=" + btoa(Priority) + "&Brdname=" + btoa(Brdname) + "&Opendate=" + btoa(Opendate) + "&Enddate=" + btoa(Enddate), "_blank");
            }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
            <br />
    <div style="width: 80%; margin: 0px auto;">
        <table class="style1" style="width: 100%; top: 350px auto;">
            <tr class="style1">
                <td style="text-align: left; width: 22%; " class="style2">
                    TrackerNo &nbsp; &nbsp;
                </td>
                <td style="text-align: left; width: 22%;">
                    <asp:TextBox ID="txtTicketNo" runat="server" Width="98%"></asp:TextBox>
                    </td>
                <td style="text-align: left; width: 12%;">
                </td>
                <td style="text-align: left; width: 22%;">
                     Incident/ServiceRequestNo</td>
                <td style="text-align: left; width: 22%;">
                    <asp:TextBox ID="txtSerReq" runat="server" Width="98%"></asp:TextBox>
                </td>
            </tr>
            <tr class="style1">
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 12%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: justify; width: 22%;" class="style2">
                    Raised From date &nbsp; &nbsp;
                </td>
                <td style="text-align: left; width: 22%;">
                    <asp:TextBox ID="txtStartDt" class="NormalText" runat="server" Width="98%" onkeypress="return false"
                        ReadOnly="True"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtStartDt" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
                <td style="text-align: left; margin: 0px auto; width: 12%;">
                </td>
                <td style="text-align: left; margin: 0px auto; width: 22%;">
                    Raised To date &nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 22%;">
                    <asp:TextBox ID="txtToDt" class="NormalText" runat="server" Width="98%" onkeypress="return false"
                        ReadOnly="True"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtToDt" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 12%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    Application</td>
                <td style="text-align: left; width: 22%;">
                    <asp:DropDownList ID="cmbApplication" class="NormalText" Style="text-align: left;" runat="server"
                        Font-Names="Cambria" Width="98%" ForeColor="Black">
                        <asp:ListItem Value="-1"> ALL</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="text-align: left; width: 12%;">
                </td>
                <td style="text-align: left; width: 22%;">
                    BRD Submitted Employee</td>
                <td style="text-align: left; width: 22%;">
                    <asp:TextBox ID="txtEmpcode" runat="server" Style="font-family: Cambria; font-size: 10pt;"
                        Width="98%" MaxLength="50" onkeypress='return NumericCheck(event)' />
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 12%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                     &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    Prioritised/Pipelined</td>
                <td style="text-align: left; width: 22%;">
                    <asp:DropDownList ID="cmbFlag" class="NormalText" Style="text-align: left;" runat="server"
                        Font-Names="Cambria" Width="98%" ForeColor="Black">
                        <asp:ListItem Value="-1"> ALL</asp:ListItem>
                         <asp:ListItem Value="1">Prioritised</asp:ListItem>
                          <asp:ListItem Value="2"> Pipelined</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="text-align: left; width: 12%;">
                </td>
                <td style="text-align: left; width: 22%;">
                    BRD Name</td>
                <td style="text-align: left; width: 22%;">
                    <asp:TextBox ID="txtBrdname" runat="server" Style="font-family: Cambria; font-size: 10pt;"
                        Width="98%" MaxLength="50"  />
                </td>
            </tr>
             <tr>
                <td style="text-align: justify; width: 22%;" class="style2">
                    Start date &nbsp; &nbsp;
                </td>
                <td style="text-align: left; width: 22%;">
                    <asp:TextBox ID="TxtOpendate" class="NormalText" runat="server" Width="98%" onkeypress="return false"
                        ReadOnly="True"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="TxtOpendate_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="TxtOpendate" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
                <td style="text-align: left; margin: 0px auto; width: 12%;">
                </td>
                <td style="text-align: left; margin: 0px auto; width: 22%;">
                     End date &nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 22%;">
                    <asp:TextBox ID="TxtEnddate" class="NormalText" runat="server" Width="98%" onkeypress="return false"
                        ReadOnly="True"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="TxtEnddate_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="TxtEnddate" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 12%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                     &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="5">
                    <br />
                    <br />
                    <input id="btnShow" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                        value="SHOW" onclick="return btnShow_onclick()" onclick="return btnShow_onclick()" />
                    &nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                        value="EXIT" onclick="return btnExit_onclick()" /></td>
            </tr>
        </table>
    </div>
            <asp:HiddenField ID="hdnApplication" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
