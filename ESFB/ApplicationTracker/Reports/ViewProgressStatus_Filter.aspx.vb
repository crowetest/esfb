﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewProgressStatus_Filter
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Dim I As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1310) = False And CInt(Session("BranchID")) <> 0 Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim DTT As New DataTable

            Dim StrEmpCode As String = GF.Decrypt(Request.QueryString.Get("Empcode"))
            Dim StrTicketNo As String = GF.Decrypt(Request.QueryString.Get("TicketNo"))
            Dim AppID As Integer = GF.Decrypt(Request.QueryString.Get("AppID"))
            Dim SRNo As String = GF.Decrypt(Request.QueryString.Get("SRNo"))
            Dim Priority As Integer = GF.Decrypt(Request.QueryString.Get("Priority"))
            Dim Brdname As String = GF.Decrypt(Request.QueryString.Get("Brdname"))

            Dim StrFromDate As String = ""
            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(Request.QueryString.Get("frmdate"))
            End If
            Dim StrToDate As String = ""
            If CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrToDate = CStr(Request.QueryString.Get("todate"))
            End If

            Dim Opendate As String = ""
            If CStr(Request.QueryString.Get("Opendate")) <> "1" Then
                Opendate = CStr(Request.QueryString.Get("Opendate"))
            End If
            Dim Enddate As String = ""
            If CStr(Request.QueryString.Get("Enddate")) <> "1" Then
                Enddate = CStr(Request.QueryString.Get("Enddate"))
            End If

            Dim Struser As String = Session("UserID").ToString()
            Dim StrSubQry As String = ""
            Dim StrSubQry1 As String = ""
            Dim StrQuery As String = ""

            If CStr(Request.QueryString.Get("frmdate")) <> "" And CStr(Request.QueryString.Get("todate")) <> "" Then
                StrQuery += " and  DATEADD(day,DATEDIFF(day, 0,a.created_on ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
            End If

            Dim strTeamQry As String = Nothing

            If AppID <> -1 Then 'Application Name   
                StrQuery += " and a.application_id =" & AppID & ""
            End If

            If StrTicketNo <> "" Then
                StrQuery += " and a.app_tracker_no ='" & StrTicketNo & "'"
            End If

            If SRNo <> "" Then
                StrQuery += " and a.ServiceReq_No ='" & SRNo & "'"
            End If

            If StrEmpCode <> "" Then
                StrQuery += " and a.created_by ='" & StrEmpCode & "'"
            End If

            If Brdname <> "" Then
                StrQuery += " and a.BrdName ='" & Brdname & "'"
            End If

            If Priority <> -1 Then
                StrQuery += " and a.Type =" & Priority & ""
            End If

            If CStr(Request.QueryString.Get("Opendate")) <> "" Then
                StrQuery += " and  DATEADD(day,DATEDIFF(day, 0,a.Start_Dt ),0)  = '" & CDate(Opendate).ToString("MM/dd/yyyy") & "'  "
            End If

            If CStr(Request.QueryString.Get("Enddate")) <> "" Then
                StrQuery += " and  DATEADD(day,DATEDIFF(day, 0,a.End_Dt ),0)  = '" & CDate(Enddate).ToString("MM/dd/yyyy") & "'  "
            End If

            Dim SqlStr As String

            SqlStr = "select a.app_tracker_id,a.app_tracker_no,a.application_name, case when a.brd_priority=1 then 'LOW' when a.brd_priority=2 then 'MEDIUM' else 'HIGH' end Priorty, " & _
                    "a.created_on, a.created_name + ' (' +convert(varchar,a.created_by) +')' Created, a.Created_Branch_name, " & _
                    "a.Created_dept_name,a.brd_remarks,d.brdfile_name Brd,isnull(f.fsdfile_name,'') Fsd, " & _
                    "case when created_status=1 and approved_status is null then 'REPORTING LEVEL PENDING' " & _
                    "when approved_status=1 and brd_signoff_status is null then 'BRD SIGNOFF PENDING' " & _
                    "when approved_status=2 and brd_signoff_status is null then 'REJECTED BY REPORTING OFFICER' " & _
                    "when approved_status=3 and brd_signoff_status is null then 'RETURNED BY REPORTING OFFICER FOR REWORK' " & _
                    "when brd_signoff_status=1 and approve_brdsoff_status is null and created_status=1 then 'BRD SIGNOFF APPROVAL PENDING' " & _
                    "when brd_signoff_status=1 and approve_brdsoff_status is null and created_status=2 then 'BRD RESUBMITTED, SIGNOFF APPROVAL PENDING' " & _
                    "when brd_signoff_status=2 and approve_brdsoff_status is null then 'REJECTED IN BRD SIGNOFF LEVEL' " & _
                    "when approve_brdsoff_status=1 and fsd_created_status is null then 'FSD UPDATION PENDING' " & _
                    "when approve_brdsoff_status=2 and fsd_created_status is null and created_status=1 then 'HOLD BY BRD SIGNOFF APPROVAL LEVEL' " & _
                    "when approve_brdsoff_status=3 and fsd_created_status is null then 'REJECTED IN BRD SIGNOFF APPROVAL LEVEL' " & _
                    "when fsd_created_status=1 and fsd_signoff_status is null then 'FSD SIGNOFF PENDING' " & _
                    "when fsd_signoff_status=1 and uat_ready_conf_status is null then 'UAT READY CONFIRMATION PENDING' " & _
                    "when uat_ready_conf_status=1 and uat_over_conf_status is null then 'UAT OVER/COMPLETE CONFIRMATION PENDING' " & _
                    "when uat_ready_conf_status is null and uat_over_conf_status=1 and uat_signoff_status is null then 'UAT READY CONFIRMATION PENDING' " & _
                    "when uat_ready_conf_status=1 and uat_over_conf_status=2 and uat_signoff_status is null then 'UAT SIGNOFF PENDING' " & _
                    "when uat_signoff_status=1 and preprod_conf_status is null then 'PRE PRODUCTION CONFIRMATON PENDING' " & _
                    "when preprod_conf_status=1 and preprod_signoff_status is null then 'PRE PRODUCTION SIGNOFF PENDING' " & _
                    "when preprod_signoff_status=1 and prodmove_conf_status is null then 'PRODUCTION MOVE CONFIRMATON PENDING' " & _
                    "when prodmove_conf_status=1 and prodmove_signoff_status is null then 'PRODUCTION MOVE SIGNOFF PENDING' " & _
                    "when prodmove_signoff_status=1 then 'PRODUCTION MOVE SIGNOFF COMPLETE' " & _
                    "else '' end Remarks,a.app_brd_id,isnull(a.app_fsd_id,0) " & _
                    "from app_tracker_master a left join app_tracker_fsd_attach f on a.app_fsd_id=f.app_fsd_id, app_tracker_brd_attach d " & _
                    "where a.app_tracker_id=d.app_tracker_id and a.app_brd_id=d.app_brd_id " & StrQuery & " order by 5"


            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim b As Integer = DT.Rows.Count


            RH.Heading(Session("FirmName"), tb, "PROGRESS OF APPLICATION - TRACKER", 150)

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            '
            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            Dim StrUserNam As String = Nothing

            DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
            If DTT.Rows.Count > 0 Then
                StrUserNam = DTT.Rows(0)(0)
            End If
            Dim TRHead_1 As New TableRow

            RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)


            RH.BlankRow(tb, 4)
            tb.Controls.Add(TRSHead)


            'TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"
            TRHead_1_08.BorderWidth = "1"
            TRHead_1_09.BorderWidth = "1"
            TRHead_1_10.BorderWidth = "1"
            TRHead_1_11.BorderWidth = "1"

            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver
            TRHead_1_08.BorderColor = Drawing.Color.Silver
            TRHead_1_09.BorderColor = Drawing.Color.Silver
            TRHead_1_10.BorderColor = Drawing.Color.Silver
            TRHead_1_11.BorderColor = Drawing.Color.Silver

            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid
            TRHead_1_08.BorderStyle = BorderStyle.Solid
            TRHead_1_09.BorderStyle = BorderStyle.Solid
            TRHead_1_10.BorderStyle = BorderStyle.Solid
            TRHead_1_11.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead_1, TRHead_1_00, 3, 3, "l", "SlNo")
            RH.AddColumn(TRHead_1, TRHead_1_01, 8, 8, "l", "Tracker No")
            RH.AddColumn(TRHead_1, TRHead_1_02, 8, 8, "l", "Application Name")
            RH.AddColumn(TRHead_1, TRHead_1_03, 5, 5, "l", "Priority")
            RH.AddColumn(TRHead_1, TRHead_1_04, 8, 8, "c", "BRD Submitted On")
            RH.AddColumn(TRHead_1, TRHead_1_05, 8, 8, "l", "BRD Submitted By")
            RH.AddColumn(TRHead_1, TRHead_1_06, 8, 8, "l", "Branch")
            RH.AddColumn(TRHead_1, TRHead_1_07, 8, 8, "l", "Department")
            RH.AddColumn(TRHead_1, TRHead_1_08, 10, 10, "l", "BRD Description")
            RH.AddColumn(TRHead_1, TRHead_1_09, 8, 8, "l", "BRD")
            RH.AddColumn(TRHead_1, TRHead_1_10, 8, 8, "l", "FSD")
            RH.AddColumn(TRHead_1, TRHead_1_11, 18, 18, "l", "Status")

            tb.Controls.Add(TRHead_1)
            I = 0
            For Each DR In DT.Rows
                I = I + 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 3, 3, "l", I.ToString())
                RH.AddColumn(TR3, TR3_01, 8, 8, "l", "<a href='ViewProgressDtls.aspx?TrackerID=" + GF.Encrypt(DR(0).ToString()) + "' style='text-align:right;' target='_blank'>" + DR(1).ToString() + "</a>")
                RH.AddColumn(TR3, TR3_02, 8, 8, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_03, 5, 5, "c", DR(3).ToString())
                RH.AddColumn(TR3, TR3_04, 8, 8, "l", CDate(DR(4)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_05, 8, 8, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_06, 8, 8, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_07, 8, 8, "l", DR(7).ToString())
                RH.AddColumn(TR3, TR3_10, 10, 10, "l", DR(8).ToString())
                RH.AddColumn(TR3, TR3_08, 8, 8, "l", "<a href='../ShowTrackerFormat.aspx?TrackerID=" + DR(0).ToString() + "&UatID=" + DR(12).ToString() + "&StatusID=1' style='text-align:right;' target='_blank'>" + DR(9).ToString() + "</a>")
                RH.AddColumn(TR3, TR3_09, 8, 8, "l", "<a href='../ShowTrackerFormat.aspx?TrackerID=" + DR(0).ToString() + "&UatID=" + DR(13).ToString() + "&StatusID=2' style='text-align:right;' target='_blank'>" + DR(10).ToString() + "</a>")
                RH.AddColumn(TR3, TR3_11, 18, 18, "l", DR(11).ToString())

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        'Try
        WebTools.ExporttoExcel(DT, "Ticket")
        'Catch ex As Exception
        '    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
        '    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        'End Try
    End Sub
End Class
