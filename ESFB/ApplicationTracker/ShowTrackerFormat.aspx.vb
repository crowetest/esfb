﻿Imports System.Data
Partial Class Leave_ShowVetFormat
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim TrackerID As Integer = Convert.ToInt32(Request.QueryString.[Get]("TrackerID"))
            Dim StatusID As Integer = Convert.ToInt32(Request.QueryString.[Get]("StatusID"))
            Dim UatID As Integer = Convert.ToInt32(Request.QueryString.[Get]("UatID"))

            If StatusID = 1 Then
                DT = DB.ExecuteDataSet("select a.brd_attach,a.content_type,brdfile_name from app_tracker_brd_attach a where a.app_tracker_id=" & TrackerID & " and a.app_brd_id= " & UatID & "").Tables(0)
            ElseIf StatusID = 2 Then
                DT = DB.ExecuteDataSet("select a.fsd_attach,a.content_type,a.fsdfile_name from app_tracker_fsd_attach a where a.app_tracker_id=" & TrackerID & " and a.app_fsd_id= " & UatID & "").Tables(0)
            ElseIf StatusID = 3 Then
                DT = DB.ExecuteDataSet("select a.defect_attach,a.content_type,a.defectfile_name from app_tracker_defect_attach a where app_tracker_id=" & TrackerID & " and a.app_uat_id=" & UatID & "").Tables(0)
            End If

            If DT IsNot Nothing Then
                Dim bytes() As Byte = CType(DT.Rows(0)(0), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = DT.Rows(0)(1).ToString()
                Response.AddHeader("content-disposition", "attachment;filename=" + DT.Rows(0)(2).ToString().Replace(" ", ""))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
