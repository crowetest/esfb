﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  
CodeFile="BRDRequestApproval.aspx.vb" Inherits="BRDRequestApproval" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function table_fill() {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:7%;text-align:left' >TrackerNo</td>";
            tab += "<td style='width:6%;text-align:left' >Application</td>";
            tab += "<td style='width:5%;text-align:left'>Priority</td>";
            tab += "<td style='width:8%;text-align:left'>Incident/RequestNo.</td>";
            tab += "<td style='width:8%;text-align:left'>BRDSubmittedOn</td>";
            tab += "<td style='width:10%;text-align:left'>BRDSubmittedBy</td>";
            tab += "<td style='width:8%;text-align:left'>Branch</td>";
            tab += "<td style='width:8%;text-align:left'>Department</td>";
            tab += "<td style='width:10%;text-align:left'>BRD Description</td>";
            tab += "<td style='width:10%;text-align:left'>BRD</td>";      
            tab += "<td style='width:8%;text-align:center'>Action</td>";
            tab += "<td style='width:10%;text-align:center'>Remarks</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                    //c.app_request_id,d.app_dtl_id,j.app_name,h.branch_name,d.emp_code, f.Emp_Name,e.App_Type_name,d.Remarks,g.App_role_name,i.branch_name as To_branch
                    tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:6%;text-align:left' >" + col[1] + "</td>";
                    tab += "<td style='width:7%;text-align:left' >" + col[2] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[10] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[7] + "</td>"; 
                    tab += "<td style='width:10%;text-align:left'>" + col[9] + "</td>";         
                    tab += "<td style='width:10%;text-align:left'><a href='ShowTrackerFormat.aspx?TrackerID=" + col[0] + "&UatID=" + col[11] + "&StatusID=1'>" + col[8] + "</a></td>";   
                    var select = "<select id='cmb" + col[0] + "' class='NormalText' style='width:99%; float:left;' maxlength='300' name='cmb" + col[0] + "' >";
                    select += "<option value='-1'>Select</option>";
                    select += "<option value='1'>Approved</option>";
                    select += "<option value='2'>Rejected</option>";
                    select += "<option value='3'>Rework</option>";
                    tab += "<td style='width:8%;text-align:left'>" + select + "</td>";

                    var txtBox = "<textarea id='txtRemarks" + col[0] + "' name='txtRemarks" + col[0] + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)' ></textarea>";
                    tab += "<td style='width:10%;text-align:left'>" + txtBox + "</td>";


                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//

        }

        function btnApprove_onclick()
        {
            var ret = UpdateValue();
            if (ret == 0) return false;
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                alert("Select Any Request for Approval");
                return false;
            }

            var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
            var Data = "1Ø" + strempcode;
            ToServer(Data, 1);
        }

        function UpdateValue() {        
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("No pending Approval");
                return false;
            }

            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                               
                if (document.getElementById("cmb" + col[0]).value != -1) {
                    if(document.getElementById("cmb" + col[0]).value == 2)
                    {
                        if (document.getElementById("txtRemarks" + col[0]).value == "") {
                            alert("Enter Rejected Reason");
                            document.getElementById("txtRemarks" + col[0]).focus();
                            return false;
                        }
                    }
                    
                    document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[0] + "µ" + document.getElementById("cmb" + col[0]).value + "µ" + document.getElementById("txtRemarks" + col[0]).value;
                }
            }
            return true;
        }

        function FromServer(arg, context) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("BRDRequestApproval.aspx", "_self");
        }
           
             
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

