﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Prod_Move_SignOff
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT1 As New DataTable
    Dim RequestID As Integer

#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Production Move Signoff"
        If GF.FormAccess(CInt(Session("UserID")), 1309) = False And CInt(Session("BranchID")) <> 0 Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

        Me.cmbTracker.Attributes.Add("onchange", "return TrackerOnChange()")
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim DR As DataRow
        Dim StrDtl As String = ""
        Dim DeptID As Integer = CInt(Session("DepartmentID"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Select Case CInt(Data(0))
            Case 1 'Fill Tracker
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select a.app_tracker_id,a.app_tracker_no from app_tracker_master a, app_application_master b, emp_master c where prodmove_conf_status=1 and prodmove_signoff_status is null and a.created_by=c.emp_code and a.application_id=b.application_id and (b.dept1=" & DeptID & " or b.dept2=" & DeptID & " or b.dept3=" & DeptID & " or b.dept4=" & DeptID & " or b.dept5=" & DeptID & " or c.reporting_to=" & UserID & ")")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
                CallBackReturn += "¥"
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select application_id,application_name from app_application_master where status_id=1")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2 'Fill tracker details
                Dim TrackerID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select a.app_tracker_id,a.application_id,a.brd_priority,a.brd_remarks,b.brdfile_name,a.created_on,a.created_name,a.approved_name,a.brd_signoff_name,a.approve_brdsoff_name,c.fsdfile_name,a.fsd_created_on,a.fsd_created_name,a.fsd_created_remark,a.fsd_signoff_on,a.fsd_signoff_name,a.fsd_signoff_remark,a.uat_ready_conf_on,a.uat_ready_conf_name,a.uat_over_conf_on,a.uat_over_conf_name,a.uat_signoff_on,a.uat_signoff_name,a.prodmove_conf_on,a.prodmove_conf_name,a.prodmove_conf_remark,isnull(a.ServiceReq_No,''),isnull(a.Jira_No,''),a.app_brd_id,a.app_fsd_id,a.approved_on,a.brd_signoff_on,a.approve_brdsoff_on,a.preprod_conf_on,a.preprod_conf_name,a.preprod_signoff_on,a.preprod_signoff_name from app_tracker_master a, app_tracker_brd_attach b, app_tracker_fsd_attach c where a.app_tracker_id=" & TrackerID & " and a.app_tracker_id=b.app_tracker_id and a.app_tracker_id=c.app_tracker_id and b.app_tracker_id=c.app_tracker_id and a.app_brd_id=b.app_brd_id and a.app_fsd_id=c.app_fsd_id")
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "¥" + DT.Rows(0)(1).ToString() + "¥" + DT.Rows(0)(2).ToString() + "¥" + DT.Rows(0)(3).ToString() + "¥" + DT.Rows(0)(4).ToString() + "¥" + CDate(DT.Rows(0)(5)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(6).ToString() + "¥" + DT.Rows(0)(7).ToString() + "¥" + DT.Rows(0)(8).ToString() + "¥" + DT.Rows(0)(9).ToString() + "¥" + DT.Rows(0)(10).ToString() + "¥" + CDate(DT.Rows(0)(11)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(12).ToString() + "¥" + DT.Rows(0)(13).ToString() + "¥" + CDate(DT.Rows(0)(14)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(15).ToString() + "¥" + DT.Rows(0)(16).ToString() + "¥" + CDate(DT.Rows(0)(17)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(18).ToString() + "¥" + CDate(DT.Rows(0)(19)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(20).ToString() + "¥" + DT.Rows(0)(21).ToString() + "¥" + DT.Rows(0)(22).ToString() + "¥" + CDate(DT.Rows(0)(23)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(24).ToString() + "¥" + DT.Rows(0)(25).ToString() + "¥" + DT.Rows(0)(26).ToString() + "¥" + DT.Rows(0)(27).ToString() + "¥" + DT.Rows(0)(28).ToString() + "¥" + DT.Rows(0)(29).ToString() + "¥" + CDate(DT.Rows(0)(30)).ToString("dd/MMM/yyyy") + "¥" + CDate(DT.Rows(0)(31)).ToString("dd/MMM/yyyy") + "¥" + CDate(DT.Rows(0)(32)).ToString("dd/MMM/yyyy") + "¥" + CDate(DT.Rows(0)(33)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(34).ToString() + "¥" + CDate(DT.Rows(0)(35)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(36).ToString()
                End If
            Case 3
                Try

                    Dim ErrorFlag As Integer = 0
                    Dim Message As String = Nothing
                    Dim TrackerID As Integer = CInt(Data(1))
                    Dim Remark As String = CStr(Data(2))

                    Dim Params(4) As SqlParameter
                    Params(0) = New SqlParameter("@TrackerID", SqlDbType.Int)
                    Params(0).Value = TrackerID
                    Params(1) = New SqlParameter("@Remark", SqlDbType.VarChar, 1000)
                    Params(1).Value = Remark
                    Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(2).Value = UserID
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_TRACKER_PRODUCTION_SIGNOFF", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                    CallBackReturn = ErrorFlag.ToString + "Ø" + Message
                Catch ex As Exception
                    Response.Redirect("~/CatchException.aspx?ErrorNo=1")
                End Try


        End Select
    End Sub
#End Region

#Region "Confirm"
    Private Sub initializeControls()
        cmbTracker.Focus()
        cmbApplication.Text = "-1"
        cmbPriority.Text = "-1"
        txtRemark.Text = ""
    End Sub
#End Region

End Class
