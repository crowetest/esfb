﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BRDRequestApproval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions

    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If GF.FormAccess(CInt(Session("UserID")), 1296) = False And CInt(Session("BranchID")) <> 0 Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            DT = DB.ExecuteDataSet("select a.app_tracker_id,a.app_tracker_no,b.application_name, case when a.brd_priority=1 then 'LOW' when a.brd_priority=2 then 'MEDIUM' else 'HIGH' end Priorty, a.created_on, a.created_name + ' (' +convert(varchar,a.created_by) +')' Created, a.Created_Branch_name, a.Created_dept_name,d.brdfile_name,a.brd_remarks,isnull(a.ServiceReq_No,''),a.app_brd_id from app_tracker_master a, app_application_master b, emp_master c, app_tracker_brd_attach d where a.created_by=c.emp_code and a.application_id=b.application_id and a.app_tracker_id=d.app_tracker_id and a.app_brd_id=d.app_brd_id and a.approved_status is null and c.reporting_to=" & CInt(Session("UserID")) & " " +
                    "union all " +
                    " select a.app_tracker_id,a.app_tracker_no,b.application_name, case when a.brd_priority=1 then 'LOW' when a.brd_priority=2 then 'MEDIUM' else 'HIGH' end Priorty, a.created_on, a.created_name + ' (' +convert(varchar,a.created_by) +')' Created, a.Created_Branch_name, a.Created_dept_name,d.brdfile_name,a.brd_remarks,isnull(a.ServiceReq_No,''),a.app_brd_id  " +
                    " from app_tracker_master a, app_application_master b, App_Approval_Nominee c, app_tracker_brd_attach d " +
                    " where a.created_by=c.emp_code and a.application_id=b.application_id and a.app_tracker_id=d.app_tracker_id  " +
                    " and a.app_brd_id=d.app_brd_id  " +
                    " and a.approved_status is null and (c.Nom_Emp_Code = " & CInt(Session("UserID")) & " )").Tables(0)

            Me.Master.subtitle = "BRD Supervisor Review"

            Dim StrDtl As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrDtl += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString
                If n < DT.Rows.Count - 1 Then
                    StrDtl += "¥"
                End If
            Next

            hid_dtls.Value = StrDtl
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim dataval As String = CStr(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))

        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try

            Dim Params(3) As SqlParameter
            Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@AppDtl", SqlDbType.VarChar)
            Params(1).Value = dataval.Substring(1)
            Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(2).Direction = ParameterDirection.Output
            Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(3).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_TRACKER_BRD_APPROVAL", Params)
            ErrorFlag = CInt(Params(2).Value)
            Message = CStr(Params(3).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=1")
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message



    End Sub
#End Region

End Class
