﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class BRDRequestSignOffApprove
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "BRD IT SignOff"
        If GF.FormAccess(CInt(Session("UserID")), 1298) = False And CInt(Session("BranchID")) <> 0 Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

        Me.cmbTracker.Attributes.Add("onchange", "return TrackerOnChange()")
        Me.cmbStatus.Attributes.Add("onchange", "return StatusOnChange()")
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Tracker
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select app_tracker_id,app_tracker_no from app_tracker_master where approved_status=1 and brd_signoff_status=1 and prjt_approval_status=1 and  approve_brdsoff_status is null and created_status >= 1")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
                CallBackReturn += "¥"
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select application_id,application_name from app_application_master where status_id=1")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2 'Fill tracker details
                Dim TrackerID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select a.app_tracker_id,a.application_id,a.brd_priority,a.brd_remarks,b.brdfile_name,a.created_on,a.created_name,a.approved_name,a.brd_signoff_name,a.approved_on,a.brd_signoff_on,isnull(a.ServiceReq_No,''),a.app_brd_id,a.created_status,a.BrdName,a.prjt_approved_on,a.Prjt_approved_name from app_tracker_master a, app_tracker_brd_attach b where a.app_tracker_id=" & TrackerID & " and a.app_tracker_id=b.app_tracker_id and a.app_brd_id=b.app_brd_id")
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "¥" + DT.Rows(0)(1).ToString() + "¥" + DT.Rows(0)(2).ToString() + "¥" + DT.Rows(0)(3).ToString() + "¥" + DT.Rows(0)(4).ToString() + "¥" + CDate(DT.Rows(0)(5)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(6).ToString() + "¥" + DT.Rows(0)(7).ToString() + "¥" + DT.Rows(0)(8).ToString() + "¥" + CDate(DT.Rows(0)(9)).ToString("dd/MMM/yyyy") + "¥" + CDate(DT.Rows(0)(10)).ToString("dd/MMM/yyyy") + "¥" + DT.Rows(0)(11).ToString() + "¥" + DT.Rows(0)(12).ToString() + "¥" + DT.Rows(0)(13).ToString() + "¥" + DT.Rows(0)(14).ToString() + "¥" + DT.Rows(0)(15).ToString() + "¥" + DT.Rows(0)(16).ToString()
                End If
            Case 3
                Try

              
                Dim ErrorFlag As Integer = 0
                Dim Message As String = Nothing
                Dim TrackerID As Integer = CInt(Data(1))
                Dim Remark As String = CStr(Data(2))
                Dim Status As Integer = CInt(Data(3))
                Dim HoldDt As Date = CDate(Data(4))
                Dim Type As Integer = CInt(Data(5))
                Dim UserID As Integer = CInt(Session("UserID"))

                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@TrackerID", SqlDbType.Int)
                Params(0).Value = TrackerID
                Params(1) = New SqlParameter("@Remark", SqlDbType.VarChar, 1000)
                Params(1).Value = Remark
                Params(2) = New SqlParameter("@StatusID", SqlDbType.Int)
                Params(2).Value = Status
                Params(3) = New SqlParameter("@HoldDt", SqlDbType.Date)
                Params(3).Value = HoldDt
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = UserID
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@Type", SqlDbType.Int)
                Params(7).Value = Type
                DB.ExecuteNonQuery("SP_TRACKER_BRD_SIGNOFF_APPROVE", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
                    CallBackReturn = ErrorFlag.ToString + "Ø" + Message
                Catch ex As Exception
                    Response.Redirect("~/CatchException.aspx?ErrorNo=1")
                End Try
        End Select
    End Sub
#End Region

#Region "Confirm"
    Private Sub initializeControls()
        cmbTracker.Focus()
        cmbApplication.Text = "-1"
        cmbPriority.Text = "-1"
        txtRemark.Text = ""
    End Sub
#End Region

End Class
