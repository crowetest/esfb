﻿function calculateDays(frmDt,ToDt)
{
    if(frmDt!="" && ToDt!="")
    {
    var Days=getDateDiff(ToDt,frmDt,"days");
    return Days+1;
    }
}

function calcTotalIntDue(interest,PenalInterest,IntPaid,OutStanding,intDue,NetDue,totDue)
{
   var totIntDue=Math.abs(interest)+Math.abs(PenalInterest);
   document.getElementById(intDue).value=parseFloat(totIntDue).toFixed(2);
   var NetDueAmt=totIntDue-Math.abs(IntPaid);
   document.getElementById(NetDue).value=parseFloat(NetDueAmt).toFixed(2);
   var TotDueAmt=Math.abs(OutStanding)+NetDueAmt;
   document.getElementById(totDue).value=parseFloat(TotDueAmt).toFixed(2);
}

function calculateErr(totIntDue,WaverInt,LoanAmt,TotDays,PerrTextBox,AerrTextBox)
{
    if(TotDays>0 && totIntDue>0 && LoanAmt>0 )
    {
    var err=parseFloat((totIntDue)/((parseFloat(LoanAmt)*parseFloat(TotDays))/36600)).toFixed(2);
    document.getElementById(PerrTextBox).value=err;
    var errAfterWave=parseFloat((totIntDue-WaverInt)/((parseFloat(LoanAmt)*parseFloat(TotDays))/36600)).toFixed(2);
    document.getElementById(AerrTextBox).value=errAfterWave;
    }

}

