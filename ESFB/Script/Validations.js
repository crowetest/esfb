﻿
function getDateDiff(date1, date2, interval) {
    var second = 1000,
    minute = second * 60,
    hour = minute * 60,
    day = hour * 24,
    week = day * 7;
    date1 = new Date(date1).getTime();
    date2 = (date2 == 'now') ? new Date().getTime() : new Date(date2).getTime();
    var timediff = date2 - date1;
    if (isNaN(timediff)) return NaN;
    switch (interval) {
    case "years":
        return date2.getFullYear() - date1.getFullYear();
    case "months":
        return ((date2.getFullYear() * 12 + date2.getMonth()) - (date1.getFullYear() * 12 + date1.getMonth()));
    case "weeks":
        return Math.floor(timediff / week);
    case "days":
        return Math.floor(timediff / day);
    case "hours":
        return Math.floor(timediff / hour);
    case "minutes":
        return Math.floor(timediff / minute);
    case "seconds":
        return Math.floor(timediff / second);
    default:
        return undefined;
    }
}
// JScript File
function checkPhone(phone) {

    var filter = /^[0-9]{10}$/;
    var str = phone.value;

    if (str.substring(1, 0) == 0) {
        alert("Please provide a valid phone number");
        phone.select();
        return false;
    }
    if (!filter.test(phone.value)) {
        alert('Please provide a valid phone number');
        phone.select();
        return false;
    }
    else
        return true;
}
function returnFalse()//------------function to block typing
{
String.prototype.trim = function() {     return this.replace(/^\s+|\s+$/g, ""); }; 
return false;
}//--------------------------------------------------------------------------

function NumericWithDot(el, e) {
    // el - this, e - event
    var unicode = e.charCode ? e.charCode : e.keyCode;
    if (unicode != 8) {
        if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
            if (unicode == 37 || unicode == 38) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            if (unicode == 46) {
                var parts = el.value.split('.');
                if (parts.length == 2)
                    return false;
                return true;
            }
        }
    }
    else {
        return true;
    }
}

function NumericCheck(e)//------------function to check whether a value is alpha numeric
{
    var valid = ((e.which >= 48 && e.which <= 57) || (e.keyWhich == 9) || (e.keyCode == 9) || (e.keyWhich == 8) || (e.keyCode == 8));
    //alert(valid);
    if (!valid) {
        e.preventDefault();
    }

}
function AlphaNumericCheck(e)//------------function to check whether a value is alpha numeric
{

    var valid = (e.which >= 48 && e.which <= 57) || (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || (e.which == 8) ;
    
    if (!valid) {
        e.preventDefault();
    }

}

function CheckIfNumber(e) {
    var clipboardData, pastedData;
    clipboardData = e.clipboardData || window.clipboardData;
    pastedData = clipboardData.getData('Text');
    var pattern = /^\d+$/;
    if (pattern.test(pastedData) == false) {
        alert("Special Characters are not allowed");
        e.preventDefault();
    }
}


function panNoCheck(control)
{
ChangeToUpper();
var charcode = (event.which) ? event.which : event.keyCode
var len=document.getElementById(control).value.length;

if(len>=10)
{
alert("Maximum length is 10")
return false;
}
else if(len<5 || len>8)
{
if(charcode>=65 && charcode<=90)
{
return true;
}
else
{
alert("Must be an alphabet");
return false;
}
}
else
{
if((charcode>=48 && charcode<=57))
{
return true;
}
else
{
alert("Must be a number");
return false;
}
}

}

function formatAmount(control)
{
 if(Math.abs(document.getElementById(control).value)>0)
  document.getElementById(control).value= parseFloat(document.getElementById(control).value).toFixed(2);
}

function ChangeToUpper()//------------function to change the value to uppercase
{
var charcode = (event.which) ? event.which : event.keyCode
if(charcode!=null)
{
if ( (charcode>=97 && charcode<=122))
{
charcode=String.fromCharCode(charcode).toUpperCase();
event.keyCode= charcode.charCodeAt(0)
}
}
}//--------------------------------------------------------------------------

function CheckAmount(control) {
    var charcode = (event.which) ? event.which : event.keyCode
    if (!((charcode >= 48 && charcode <= 57) || charcode == 46)) {
        window.event.cancelBubble = true;
        window.event.keyCode = 0;
        return false;
    }

    var amt; var len;
    amt = document.getElementById(control).value;
    len = document.getElementById(control).value.length;
    var chrDot = (amt.charAt(len - 3));
    if (chrDot == ".") {
        if ("onselect" == false)
            return false;
    }
    if (charcode == 46) {
        if (amt.indexOf(".") != -1) {
            return false;
        }
    }

    //
    //
}



function checkEmail(email)
{
var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
if (!filter.test(email.value))
{
alert('Please provide a valid email address');
email.select();
return false;
}
else
 return true;
}

function ANMultiLine()//------------function to check whether a value is alpha numeric
{
ChangeToUpper();
var charcode = (event.which) ? event.which : event.keyCode
if (((charcode>=48 && charcode<=57) || (charcode>=65 && charcode<=90) ||
(charcode==32)|| (charcode==47)|| (charcode==37)|| (charcode==46)|| (charcode==44)|| (charcode==63)|| (charcode==59)|| (charcode==13)))
{
return true;
}
else
{
window.event.cancelBubble = true;
window.event.keyCode = 0;
return false;
}
}


function ComboFill(data, ddlName) {
    document.getElementById(ddlName).options.length = 0;
    var rows = data.split("Ř");
    for (a = 1; a < rows.length; a++) {
        var cols = rows[a].split("Ĉ");
        var option1 = document.createElement("OPTION");
        option1.value = cols[0];
        option1.text = cols[1];
        document.getElementById(ddlName).add(option1);
    }
}

function TextAreaCheck(e)//------------function to check whether a value is alpha numeric
{


    var valid = (e.which >= 48 && e.which <= 57) || (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || (e.which == 8) || (e.which == 40) || (e.which == 41) || (e.which == 44) || (e.which == 46) || (e.which == 47) || (e.which == 32) || (e.which == 45) || (e.which == 64) || (e.which == 35) || (e.which == 33);
    if (!valid) {
        e.preventDefault();
    }

}

//function print(txt, title) 
//{
//  txt = txt.replace(/\n/g,"<br />");
//  if(!title) var title = "Message title";
//  var template = "<center><div id='message_box' style='width:50%;"+
//    "border:2px outset #ccc;background-color:#eee;'>"+
//    "<div style='background-color:#5d5d92;color:#fff;font-weight:bold;"+
//     "padding-left:5px;'>%TITLE%</div>"+
//    "<div style='padding:15px;'>%TEXT%</div>"+
//    "<center><input type='button' value='    OK    ' "+
//    "onclick='document.getElementById(\"message_box\").style.display=\"none\"'"+
//    " /></center>"+
//    "</div></center>";
//    
//  var to_write = template.replace("%TITLE%", title);
//  to_write = to_write.replace("%TEXT%", txt);
//  
//  if(document.getElementById("print_area")) 
//  { //If the 'print_area' exists, just write our data into it.
//    document.getElementById("print_area").innerHTML = to_write;
//  } 
//  else 
//  { //Else create the element before writing the data.
//    var div = document.createElement("div");
//    div.setAttribute("id","print_area");
//    div.innerHTML = to_write;
//    
//    var body = document.getElementsByTagName("body")[0];
//     body.insertBefore(div,body.firstChild);
//   //OR	// body.appendChild(div);
//     //This will insert the message box at the end of the page.
//  }
//}



