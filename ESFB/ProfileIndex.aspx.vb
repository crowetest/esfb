﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class ProfileIndex
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler

    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim Leave As New Leave
    Dim CallBackReturn As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        Dim TypeID As Integer
        Dim EMP_Code As Integer
        TypeID = CInt(Request.QueryString.Get("TypeID"))
        hid_TypeID.Value = TypeID
        If TypeID = 1 Then
            EMP_Code = CInt(Session("UserID"))
        Else
            EMP_Code = CInt(GF.Decrypt(Request.QueryString.Get("EmpCode")))
        End If
        hid_EmpCode.Value = EMP_Code
        'Get Employee Details

        DT = GF.GetEmployee(EMP_Code)
        hid_dtls.Value = GF.GetEmployeeString(DT)

        hid_User.Value = CStr(DT.Rows(0).Item(1))
        DT = GF.GetPhoto(EMP_Code)
        If DT.Rows.Count > 0 Then
            Dim bytes As Byte() = DirectCast(DT.Rows(0)(0), Byte())
            Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
            hid_image.Value = Convert.ToString("data:" + DT.Rows(0)(1) + ";base64,") & base64String
        Else
            hid_image.Value = "Image/userimage.png"
        End If
        Dim DT_Quali As New DataTable
        Dim DT_Exp As New DataTable
        Dim DT_ID As New DataTable
        Dim DT_ADDR As New DataTable
        Dim DT_FAM As New DataTable
        Dim DT_LANG As New DataTable

        Dim strTS As New StringBuilder



        DT = DB.ExecuteDataSet("select convert(varchar,a.emp_code)+'µ'+dbo.udfPropercase(a.emp_name)+'µ'+dbo.udfPropercase(isnull(d.branch_Name ,''))+'µ'+ " & _
        " dbo.udfPropercase(isnull(f.Cadre_name  ,''))  +'µ'+ dbo.udfPropercase(isnull(g.Designation_Name ,'')) +'µ'+ dbo.udfPropercase(isnull(e.Department_Name ,''))+'µ'+  " & _
        " dbo.udfPropercase(isnull(convert(varchar,a.Date_Of_Join ,106) ,''))  +'µ'+ dbo.udfPropercase(isnull(convert(varchar,b.DOB,106),''))+'µ'+ isnull(b.Email,'') " & _
        " +'µ'+dbo.udfPropercase(isnull(h.Gender_Name,''))+'µ'+isnull(b.Mobile,'')+'µ'+dbo.udfPropercase(isnull(b.HouseName,'')) +'µ'+  isnull(b.LandLine,'')+'µ'+  " & _
        " dbo.udfPropercase(b.CareOf) +'µ'+ isnull(b.CareOf_Phone,'') +'µ'+ dbo.udfPropercase(b.City) +'µ'+ dbo.udfPropercase(b.location) +'µ'+ " & _
        " dbo.udfPropercase(isnull(v.MStatus,'')) +'µ'+ dbo.udfPropercase(isnull(m.Caste_name,'')) +'µ'+ dbo.udfPropercase(isnull(k.Pin_Code,'')) +'µ'+ " & _
        " dbo.udfPropercase(isnull(k.Post_Office,''))  +'µ'+ convert(varchar,convert(numeric(16,0),isnull(c.Basic_Pay,0))) +'µ'+ " & _
        " convert(varchar,convert(numeric(16,0),isnull(c.DA,0))) +'µ'+ convert(varchar,convert(numeric(16,0),isnull(c.HRA,0))) +'µ'+ " & _
        " convert(varchar,convert(numeric(16,0),isnull(c.Conveyance,0))) +'µ'+ convert(varchar,convert(numeric(16,0),isnull(c.SpecialAllowance,0))) +'µ'+ " & _
        " convert(varchar,convert(numeric(16,0),isnull(c.LocalAllowance,0))) +'µ'+ convert(varchar,convert(numeric(16,0),isnull(c.PerformanceAllowance,0))) " & _
        " +'µ'+ convert(varchar,convert(numeric(16,0),isnull(c.OtherAllowance,0)))+'µ'+ convert(varchar,convert(numeric(16,0),isnull(c.MedicalAllowance,0))) " & _
        " +'µ'+ convert(varchar,convert(numeric(16,0),isnull(c.HospitalityAllowance,0))) +'µ'+ convert(varchar,convert(numeric(16,0),isnull(c.FieldAllowance,0))) " & _
        " +'µ'+dbo.udfPropercase(isnull(j.District_Name,''))+'µ'+ dbo.udfPropercase(isnull(i.state_name,''))+'µ'+ convert(varchar,isnull(c.ESI,0))+'µ'+ " & _
        " convert(varchar,isnull(c.PF,0))+'µ'+ convert(varchar,isnull(c.SWF,0))+'µ'+ convert(varchar,isnull(c.Charity,0))+'µ'+convert(varchar,isnull(c.ESWT,0)) " & _
        " +'µ'+ dbo.udfPropercase(isnull(b.Fathers_name,''))+'µ'+ dbo.udfPropercase(isnull(b.Mothers_name,'')) +'µ'+ " & _
        " dbo.udfPropercase(isnull(t.Blood_Name,'')) +'µ'+dbo.udfPropercase(isnull(u.Language,'')) +'µ'+  isnull(case when convert(varchar,b.Marriage_Date,106) " & _
        " ='01-jan-1900' then '' else convert(varchar,b.Marriage_Date,106) end,'') +'µ'+ CASE when b.Twowheeler_Status=1 then 'YES' else " & _
        " 'NO' end +'µ'+ dbo.udfPropercase(isnull(b.Driving_License_No,''))  +'µ'+ dbo.udfPropercase(isnull(b.Cug_No,''))+'µ'+ " & _
        " dbo.udfPropercase(isnull(b.Official_mail_id,''))+'µ'+case when b.Ref_Letter_Panchayath=1 then 'YES' else 'NO' end  +'µ'+ " & _
        " case when b.Ref_Letter_Community=1 then 'YES' else 'NO' END +'µ'+ CASE when b.Ref_Letter_PoliceStation=1 then 'YES' ELSE 'NO' end +'µ'+ " & _
        " case when b.Is_Sangam_member=1 then 'YES' else 'NO' end  +'µ'+ dbo.udfPropercase(isnull(r.Family_person,''))  +'µ'+ " & _
        " dbo.udfPropercase(isnull(b.Surety_person_name,'')) +'µ'+dbo.udfPropercase(isnull(b.Surety_Address,'')) +'µ'+ " & _
        " dbo.udfPropercase(isnull(b.Surety_Phone,'')) +'µ'+ dbo.udfPropercase(isnull(s.Religion_name,'')) +'µ'+ w.Emp_Status  " & _
        " from EMP_MASTER a LEFT JOIN  Emp_Profile b ON a.Emp_Code=b.Emp_Code   LEFT JOIN CASTE_MASTER m ON  b.Caste_id =m.caste_id " & _
        " LEFT JOIN EMP_SALARY_DTL c ON b.Emp_Code=c.Emp_Code LEFT JOIN BRANCH_MASTER d ON a.Branch_ID=d.Branch_ID LEFT JOIN DEPARTMENT_MASTER e ON a.Department_ID=e.Department_ID LEFT JOIN   CADRE_MASTER f ON  a.Cadre_ID=f.Cadre_ID LEFT JOIN DESIGNATION_MASTER g ON  a.Designation_ID=g.Designation_ID " & _
        " LEFT JOIN Post_Master k ON b.Post_ID=k.Post_ID LEFT JOIN District_Master j ON k.District_ID=j.District_ID LEFT JOIN State_Master i ON j.State_ID =i.State_ID LEFT JOIN emp_master l ON a.Reporting_to=l.Emp_Code LEFT JOIN GENDER_MASTER h ON a.gender_id=h.gender_id  " & _
         " LEFT JOIN FAMILY_MASTER r ON b.Surety_id=r.Family_id LEFT JOIN RELIGION_MASTER s ON m.Religion_Id=s.Religion_id LEFT JOIN BLOOD_GROUP t ON b.Blood_Group=t.Blood_Id LEFT JOIN LANGUAGE_MASTER u ON b.Mother_tongue=u.Language_id LEFT JOIN MARITAL_STATUS v ON v.MStatus_ID=b.Marital_Status " & _
         " LEFT JOIN employee_status w ON a.emp_status_id =w.emp_status_id where a.Emp_Code = " & EMP_Code & "").Tables(0)

        hid_Employee.Value = DT.Rows(0).Item(0).ToString

        Dim LangVal As String = ""
        DT_LANG = DB.ExecuteDataSet("select b.Language from EMP_LANGUAGES_KNOWN a,LANGUAGE_MASTER b WHERE a.Language_id=b.Language_id and a.emp_code=" & EMP_Code & "").Tables(0)
        For Each dr In DT_LANG.Rows
            LangVal += "," + dr(0).ToString()

        Next
        If (LangVal.Length > 0) Then
            LangVal = LangVal.Substring(1)
        End If
        hid_Lang.Value = LangVal
        DT_Quali = DB.ExecuteDataSet("select b.QUALIFICATION_NAME,a.Qualification,c.UNIVERSITY_NAME,a.YearOFPassing from EMP_QUALIFICATION a,QUALIFICATION_MASTER b,UNIVERSITY_MASTER c where a.Qualification_ID=b.QUALIFICATION_ID and a.University_ID =c.UNIVERSITY_ID  and a.Emp_Code=" & EMP_Code & "").Tables(0)
        strTS.Clear()
        For Each dr In DT_Quali.Rows
            strTS.Append("¥")
            strTS.Append(dr.Item(0).ToString())
            strTS.Append("µ")
            strTS.Append(dr.Item(1).ToString())
            strTS.Append("µ")
            strTS.Append(dr.Item(2).ToString())
            strTS.Append("µ")
            strTS.Append(dr.Item(3).ToString())

        Next
        hid_Quali.Value = strTS.ToString
        DT_Exp = DB.ExecuteDataSet("select Company,Address,Phone,No_of_years  from EMP_EXPERIENCE_DETAILS  where Emp_Code=" & EMP_Code & "").Tables(0)
        strTS.Clear()
        For Each dr In DT_Exp.Rows
            strTS.Append("¥")
            strTS.Append(dr.Item(0).ToString())
            strTS.Append("µ")
            strTS.Append(dr.Item(1).ToString())
            strTS.Append("µ")
            strTS.Append(dr.Item(2).ToString())
            strTS.Append("µ")
            strTS.Append(dr.Item(3).ToString())

        Next
        hid_Exp.Value = strTS.ToString
        DT_FAM = DB.ExecuteDataSet("select b.Family_person,a.Person_Name,a.Age,a.Occupation  from EMP_FAMILY_DETAILS a,FAMILY_MASTER b where a.Family_id=b.Family_id and a.Emp_Code=" & EMP_Code & "").Tables(0)
        strTS.Clear()
        For Each dr In DT_FAM.Rows
            strTS.Append("¥")
            strTS.Append(dr.Item(0).ToString())
            strTS.Append("µ")
            strTS.Append(dr.Item(1).ToString())
            strTS.Append("µ")
            strTS.Append(dr.Item(2).ToString())
            strTS.Append("µ")
            strTS.Append(dr.Item(3).ToString())

        Next
        hid_Fam.Value = strTS.ToString

        DT_ADDR = DB.ExecuteDataSet("select b.Type_Name,a.Address_proof_Name  from EMP_ADDRESS_PROOF_DETAILS a,ID_Proof_Type b where a.Address_Proof_Id=b.Type_ID and a.Emp_Code=" & EMP_Code & "").Tables(0)
        strTS.Clear()
        For Each dr In DT_ADDR.Rows
            strTS.Append("¥")
            strTS.Append(dr.Item(0).ToString())
            strTS.Append("µ")
            strTS.Append(dr.Item(1).ToString())

        Next
        hid_ADProof.Value = strTS.ToString
        DT_ID = DB.ExecuteDataSet("select b.Type_Name,a.Proof_Name  from EMP_ID_PROOF_DETAILS a,ID_Proof_Type b where a.ID_Proof_Id=b.Type_ID  and a.Emp_Code=" & EMP_Code & "").Tables(0)
        strTS.Clear()
        For Each dr In DT_ID.Rows
            strTS.Append("¥")
            strTS.Append(dr.Item(0).ToString())
            strTS.Append("µ")
            strTS.Append(dr.Item(1).ToString())

        Next
        hid_IDProof.Value = strTS.ToString
        'DT = EN.Get_Transfer_Dtls(CInt(Session("UserID")))
        'strTS.Clear()
        'For Each dr In DT_Exp.Rows
        '    strTS.Append("¥")
        '    strTS.Append(dr.Item(0).ToString())
        '    strTS.Append("µ")
        '    strTS.Append(dr.Item(1).ToString())
        '    strTS.Append("µ")
        '    strTS.Append(dr.Item(2).ToString())
        '    strTS.Append("µ")
        '    strTS.Append(dr.Item(3).ToString())

        'Next
        'hid_Exp.Value = strTS.ToString

        hid_Transfer.Value = strTS.ToString
        txtFromDt.Attributes.Add("onchange", "return DateOnChange();")
        txtToDt.Attributes.Add("onchange", "return DateOnChange();")
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
    End Sub
Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim LoginUSerID As Integer = 0
        Dim EmpCode As Integer = Data(1)
        Dim StrValue As String = Nothing
        Dim strTS As New StringBuilder
        If CInt(Data(0)) = 1 Then 'Travel
            DT = DB.ExecuteDataSet("select D.location,convert(varchar,D.tour_start_date,106),convert(varchar,D.tour_end_date,106),D.purpose,case when T.status_id=0 then 'Requested'  " & _
               " when T.status_id=1 then 'Approved' when T.status_id=2 then 'Rejected' ELSE 'Cancelled' END AS tourstatus " & _
               " from [EMP_TOUR_MASTER] T,[EMP_TOUR] D,EMP_MASTER E where  T.M_request_id=D.M_request_id and T.userid=E.emp_code and userid=" + EmpCode.ToString() + " " & _
               " and (tour_from between DATEADD(month,MONTH(getdate())-1,DATEADD(year,year(getdate())-1900,0)) and DATEADD(day,-1,DATEADD(month,month(getdate()),DATEADD(year,year(getdate())-1900,0)))) " & _
               " and (tour_to between DATEADD(month,MONTH(getdate())-1,DATEADD(year,year(getdate())-1900,0)) and DATEADD(day,-1,DATEADD(month,month(getdate()),DATEADD(year,year(getdate())-1900,0)))) order by tour_from").Tables(0)
            For Each dr In DT.Rows
                strTS.Append("¥")
                strTS.Append(dr.Item(0).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(1).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(2).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(3).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(4).ToString())

            Next
            StrValue = strTS.ToString

        ElseIf CInt(Data(0)) = 2 Then

            'DT = DB.ExecuteDataSet("select convert(varchar,(a.CasualLeave -ISNULL(b.CLTaken ,0)))+'~'+ convert(varchar,(a.SickLeave  -ISNULL(b.SLTaken ,0)))+'~'+ convert(varchar,(a.PrivilegeLeave  -ISNULL(b.PLTaken,0 ))) from " & _
            '                             " (select a.Emp_Code ,b.Emp_Name,b.Branch_Name ,b.Department_Name,a.CL_Bal as CasualLeave,a.SL_Bal as SickLeave,a.PL_Bal as PrivilegeLeave " & _
            '                             " from  EMP_LEAVE_BALANCE a,emp_list b where a.emp_code=b.Emp_Code and b.Status_ID<>0 )a left outer join	(SELECT emp_code, ISNULL(SUM(CASE WHEN Leave_Type=1 THEN " & _
            '                             " Leave_Days ELSE 0 END),0)as CLTaken,ISNULL(SUM(CASE WHEN Leave_Type=2 THEN  Leave_Days ELSE 0 END),0) as SLTaken,ISNULL(SUM(CASE WHEN Leave_Type=3 THEN " & _
            '                             " Leave_Days ELSE 0 END),0) as PLTaken FROM EMP_LEAVE_REQUEST   WHERE   STATUS_ID<5 and Leave_From >=DATEADD(d, - DATEPART(dy, GETDATE()) + 1, GETDATE()) " & _
            '                             " group by Emp_Code) b on(a.Emp_Code=b.Emp_Code ) where a.emp_code=" & EmpCode & " ").Tables(0)
            StrValue = Leave.GetAvailLeaves(EmpCode)

            DT = DB.ExecuteDataSet("SELECT convert(varchar,AA.EMP_CODE)+'^'+UPPER(AA.EMP_NAME)+'^'+convert(varchar,SUM(AA.CLcount))+'^'+convert(varchar,SUM(AA.SLcount)) " & _
                   " +'^'+convert(varchar,SUM(AA.PLcount))+'^'+convert(varchar,SUM(AA.CMPcount))+'^'+convert(varchar,SUM(AA.RHcount))+'^'+convert(varchar,SUM(AA.OTHERcount)) FROM ( " & _
                   " select L.EMP_CODE,E.EMP_name,'CASUAL LEAVE' AS CL,case when T.Type_ID=1 then SUM(leave_days) ELSE 0 END as CLcount " & _
                   " ,'SICK LEAVE'  AS SL,case when T.Type_ID=2 then SUM(leave_days) ELSE 0 END as SLcount, " & _
                   " 'PRIVILEGE LEAVE'  AS PL,case when T.Type_ID=3 then SUM(leave_days) ELSE 0 END as PLcount, " & _
                   " 'COMPENSATORY'  AS CMP,case when T.Type_ID=7 then SUM(leave_days) ELSE 0 END as CMPcount, " & _
                   " 'RESTR. HOLIDAY'  AS RHL,case when T.Type_ID=8 then SUM(leave_days) ELSE 0 END as RHcount, " & _
                   " 'OTHERS'  AS OTHER,case when T.Type_ID NOT IN(1,2,3,7,8) then SUM(leave_days) ELSE 0 END as OTHERcount " & _
                   " from EMP_LEAVE_REQUEST L INNER JOIN EMP_LEAVE_REASONS R ON L.Reason_ID=R.Reason_ID  " & _
                   " INNER JOIN Emp_Leave_Type T ON L.Leave_Type=T.Type_ID INNER JOIN EMP_MASTER E ON L.Emp_Code=E.Emp_Code  WHERE L.emp_code=" & EmpCode.ToString() & " " & _
                   " and L.Leave_From Between DATEADD(month,MONTH(getdate())-1,DATEADD(year,year(getdate())-1900,0)) and DATEADD(day,-1,DATEADD(month,month(getdate()),DATEADD(year,year(getdate())-1900,0)))  AND L.Status_ID<5 " & _
                   " GROUP BY L.EMP_CODE,E.EMP_name,Type_ID,TYPE_NAME) AA GROUP BY AA.EMP_CODE,AA.EMP_NAME,AA.CL,AA.SL,AA.PL,AA.OTHER").Tables(0)
            If DT.Rows.Count > 0 Then
                StrValue += "Ø" + DT.Rows(0).Item(0)
            Else
                StrValue += "Ø^^^^^^^"
            End If
            DT = DB.ExecuteDataSet("select  CONVERT(VARCHAR(11),L.request_date,106) as reqdate,T.Type_Name, " & _
                           " CONVERT(VARCHAR(11),L.Leave_From,106) as leavefrom,CONVERT(VARCHAR(11),L.Leave_To,106) as leaveto,L.Leave_Days,R.Reason, " & _
                           " case when L.Status_ID=0 then 'Requested' WHEN L.Status_ID=1 then 'Approved' WHEN L.Status_ID=2 then 'Recommended' WHEN L.Status_ID=5 then 'Rejected' WHEN L.Status_ID=6 then 'Cancelled' ELSE '' END AS status from EMP_LEAVE_REQUEST L INNER JOIN EMP_LEAVE_REASONS R ON L.Reason_ID=R.Reason_ID  " & _
                           " INNER JOIN Emp_Leave_Type T ON L.Leave_Type=T.Type_ID INNER JOIN EMP_MASTER E ON L.Emp_Code=E.Emp_Code WHERE L.emp_code=" & EmpCode.ToString() & " " & _
                           " and L.Leave_From Between DATEADD(month,MONTH(getdate())-1,DATEADD(year,year(getdate())-1900,0)) and DATEADD(day,-1,DATEADD(month,month(getdate()),DATEADD(year,year(getdate())-1900,0)))  AND L.Status_ID<5 order by L.request_date").Tables(0)

            For Each dr In DT.Rows
                strTS.Append("¥")
                strTS.Append(dr.Item(0).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(1).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(2).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(3).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(4).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(5).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(6).ToString())
            Next
            StrValue += "Ø" + strTS.ToString
        ElseIf CInt(Data(0)) = 3 Then 'Transfer

            DT = DB.ExecuteDataSet("select f.Branch_Name as prevBranch,d.Branch_Name as Branch,g.Department_Name as prevDep ,e.Department_Name as Dep , " & _
               " h.Emp_Name as PrevReporting,b.Emp_Name as Reporting,i.Post_Name as prevPost ,c.Post_Name as Post,convert(varchar,a.Effective_Date,106) as Edate," & _
               " j.designation_Name as Prev_Desig,k.designation_Name,l.Cadre_Name as Prev_Cadre,m.Cadre_Name   from EMP_TRANSFER  a,EMP_MASTER b,EMP_POST_MASTER c, " & _
               " BRANCH_MASTER d,DEPARTMENT_MASTER e,BRANCH_MASTER f,DEPARTMENT_MASTER g,EMP_MASTER h,EMP_POST_MASTER i,Designation_master j,designation_master k,cadre_master l,cadre_master m where(a.Reporting_To = b.Emp_Code And a.EmpPost = " & _
               " c.Post_ID And a.Branch_ID = d.Branch_ID And a.Department_ID = e.Department_ID) and a.Prev_Branch_ID=f.Branch_ID and a.Prev_Department_ID =g.Department_ID " & _
               " and a.Prev_Reporting_To=h.emp_code and a.Prev_EmpPost=i.Post_ID and a.Prev_Designation_id=j.designation_id and a.Designation_id=k.designation_id " & _
               " and a.Prev_CadreID=l.Cadre_id and a.Cadreid=m.Cadre_id  and a.Emp_code=" + EmpCode.ToString() + " and a.effective_date Between DATEADD(month,MONTH(getdate())-1,DATEADD(year,year(getdate())-1900,0)) and DATEADD(day,-1,DATEADD(month,month(getdate()),DATEADD(year,year(getdate())-1900,0))) order by a.Effective_Date").Tables(0)
            For Each dr In DT.Rows
                strTS.Append("¥")
                strTS.Append(dr.Item(0).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(1).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(2).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(3).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(4).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(5).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(6).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(7).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(8).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(9).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(10).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(11).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(12).ToString())
            Next
            StrValue = strTS.ToString

        ElseIf CInt(Data(0)) = 4 Then 'Promotion
            DT = DB.ExecuteDataSet(" select convert(varchar,a.Effective_Date,106) as edate,e.Designation_Name as fromdesg,f.Designation_Name as todesg,b.Emp_Name as fromReporting,h.Emp_Name as ToReporting  " & _
            " ,c.Post_Name as fromPost,i.Post_Name as toPost,a.trans_id from EMP_PROMOTION  a,EMP_MASTER b,EMP_POST_MASTER c,BRANCH_MASTER d,DESIGNATION_MASTER  e,DESIGNATION_MASTER  f,EMP_MASTER h,EMP_POST_MASTER i " & _
            " where(a.Reporting_To_From = b.Emp_Code And a.EmpPost = c.Post_ID And a.Branch_ID = d.Branch_ID And a.Designation_ID_From = e.Designation_ID) " & _
            " and a.Designation_ID_to =f.Designation_ID and a.Reporting_To_To=h.emp_code and a.EmpPost=i.Post_ID and a.Emp_code=" + EmpCode.ToString() + "  " & _
            " and a.effective_date Between DATEADD(month,MONTH(getdate())-1,DATEADD(year,year(getdate())-1900,0)) and DATEADD(day,-1,DATEADD(month,month(getdate()),DATEADD(year,year(getdate())-1900,0)))  order by a.Effective_Date").Tables(0)
            For Each dr In DT.Rows
                strTS.Append("¥")
                strTS.Append(dr.Item(0).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(1).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(2).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(3).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(4).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(5).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(6).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(7).ToString())
                strTS.Append("µ")

            Next
            StrValue = strTS.ToString
        ElseIf CInt(Data(0)) = 5 Then 'Previous History 

            Dim FromDate, ToDate As String
            FromDate = Format(CDate(Data(3)), "dd/MMM/yyyy")
            ToDate = Format(CDate(Data(4)), "dd/MMM/yyyy")
            If CInt(Data(2)) = 2 Then 'Leave

                DT = DB.ExecuteDataSet("select  CONVERT(VARCHAR(11),L.request_date,106) as reqdate,T.Type_Name, " & _
                              " CONVERT(VARCHAR(11),L.Leave_From,106) as leavefrom,CONVERT(VARCHAR(11),L.Leave_To,106) as leaveto,L.Leave_Days,R.Reason, " & _
                              " case when L.Status_ID=0 then 'Requested' WHEN L.Status_ID=1 then 'Approved' WHEN L.Status_ID=2 then 'Recommended' WHEN L.Status_ID=5 then 'Rejected' WHEN L.Status_ID=6 then 'Cancelled' ELSE '' END AS status from EMP_LEAVE_REQUEST L INNER JOIN EMP_LEAVE_REASONS R ON L.Reason_ID=R.Reason_ID  " & _
                              " INNER JOIN Emp_Leave_Type T ON L.Leave_Type=T.Type_ID INNER JOIN EMP_MASTER E ON L.Emp_Code=E.Emp_Code WHERE L.emp_code=" & EmpCode.ToString() & " " & _
                              " and L.Leave_From Between '" & FromDate & "'  and '" & ToDate & "'  AND L.Status_ID<5 order by L.request_date").Tables(0)
                For Each dr In DT.Rows
                    strTS.Append("¥")
                    strTS.Append(dr.Item(0).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(1).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(2).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(3).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(4).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(5).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(6).ToString())
                Next
                StrValue = strTS.ToString
            ElseIf CInt(Data(2)) = 3 Then 'Travel
                DT = DB.ExecuteDataSet("select D.location,convert(varchar,D.tour_start_date,106),convert(varchar,D.tour_end_date,106),D.purpose,case when T.status_id=0 then 'Requested'  " & _
              " when T.status_id=1 then 'Approved' when T.status_id=2 then 'Rejected' ELSE 'Cancelled' END AS tourstatus " & _
              " from [EMP_TOUR_MASTER] T,[EMP_TOUR] D,EMP_MASTER E where  T.M_request_id=D.M_request_id and T.userid=E.emp_code and userid=" + EmpCode.ToString() + " " & _
              " and (tour_from between '" + FromDate + "' and '" + ToDate + "') and (tour_to between '" + FromDate + "' and '" + ToDate + "') order by tour_from").Tables(0)
                For Each dr In DT.Rows
                    strTS.Append("¥")
                    strTS.Append(dr.Item(0).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(1).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(2).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(3).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(4).ToString())

                Next
                StrValue = strTS.ToString
            ElseIf CInt(Data(2)) = 4 Then 'Transfer
                DT = DB.ExecuteDataSet("select f.Branch_Name as prevBranch,d.Branch_Name as Branch,g.Department_Name as prevDep ,e.Department_Name as Dep , " & _
                " h.Emp_Name as PrevReporting,b.Emp_Name as Reporting,i.Post_Name as prevPost ,c.Post_Name as Post,convert(varchar,a.Effective_Date,106) as Edate," & _
                " j.designation_Name as Prev_Desig,k.designation_Name,l.Cadre_Name as Prev_Cadre,m.Cadre_Name   from EMP_TRANSFER  a,EMP_MASTER b,EMP_POST_MASTER c, " & _
                " BRANCH_MASTER d,DEPARTMENT_MASTER e,BRANCH_MASTER f,DEPARTMENT_MASTER g,EMP_MASTER h,EMP_POST_MASTER i,Designation_master j,designation_master k,cadre_master l,cadre_master m where(a.Reporting_To = b.Emp_Code And a.EmpPost = " & _
                " c.Post_ID And a.Branch_ID = d.Branch_ID And a.Department_ID = e.Department_ID) and a.Prev_Branch_ID=f.Branch_ID and a.Prev_Department_ID =g.Department_ID " & _
                " and a.Prev_Reporting_To=h.emp_code and a.Prev_EmpPost=i.Post_ID and a.Prev_Designation_id=j.designation_id and a.Designation_id=k.designation_id " & _
                " and a.Prev_CadreID=l.Cadre_id and a.Cadreid=m.Cadre_id  and a.Emp_code=" + EmpCode.ToString() + " and a.effective_date between '" + FromDate + "' and '" + ToDate + "' order by a.Effective_Date").Tables(0)
                For Each dr In DT.Rows
                    strTS.Append("¥")
                    strTS.Append(dr.Item(0).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(1).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(2).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(3).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(4).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(5).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(6).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(7).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(8).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(9).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(10).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(11).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(12).ToString())
                Next
                StrValue = strTS.ToString
            ElseIf CInt(Data(2)) = 5 Then 'Promotion
                DT = DB.ExecuteDataSet(" select convert(varchar,a.Effective_Date,106) as edate,e.Designation_Name as fromdesg,f.Designation_Name as todesg,b.Emp_Name as fromReporting,h.Emp_Name as ToReporting  " & _
            " ,c.Post_Name as fromPost,i.Post_Name as toPost,a.trans_id from EMP_PROMOTION  a,EMP_MASTER b,EMP_POST_MASTER c,BRANCH_MASTER d,DESIGNATION_MASTER  e,DESIGNATION_MASTER  f,EMP_MASTER h,EMP_POST_MASTER i " & _
            " where(a.Reporting_To_From = b.Emp_Code And a.EmpPost = c.Post_ID And a.Branch_ID = d.Branch_ID And a.Designation_ID_From = e.Designation_ID) " & _
            " and a.Designation_ID_to =f.Designation_ID and a.Reporting_To_To=h.emp_code and a.EmpPost=i.Post_ID and a.Emp_code=" + EmpCode.ToString() + "  and a.effective_date between '" + FromDate + "' and '" + ToDate + "'  order by a.Effective_Date").Tables(0)
                For Each dr In DT.Rows
                    strTS.Append("¥")
                    strTS.Append(dr.Item(0).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(1).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(2).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(3).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(4).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(5).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(6).ToString())
                    strTS.Append("µ")
                    strTS.Append(dr.Item(7).ToString())
                    strTS.Append("µ")
                    
                Next
                StrValue = strTS.ToString
            
            End If
        ElseIf CInt(Data(0)) = 6 Then 'Role
            DT = DB.ExecuteDataSet("Select f.MODULE_NAME,e.MENU_NAME,d.FORM_NAME   from ROLES_ASSIGNED a,ROLE_MASTER b,ROLE_DTL c,MENU_FORMS d,MENU_GROUPS e,MODULE_MASTER f " & _
            " where(a.ROLE_ID = b.ROLE_ID And b.ROLE_ID = c.ROLE_ID And c.FORM_ID = d.FORM_ID And d.MENU_ID = e.MENU_ID And e.MODULE_ID = f.MODULE_ID)    and a.STATUS_ID=1 and a.emp_code=" + EmpCode.ToString + " order by f.MODULE_NAME,e.MENU_NAME,d.FORM_NAME ").Tables(0)
            For Each dr In DT.Rows
                strTS.Append("¥")
                strTS.Append(dr.Item(0).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(1).ToString())
                strTS.Append("µ")
                strTS.Append(dr.Item(2).ToString())

            Next
            StrValue = strTS.ToString
        ElseIf CInt(Data(0)) = 7 Then 'Complaints
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim Complaint As String = CStr(Data(2))
            Dim EmplCode As Integer = CInt(Data(1))
            Try
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@ComplaintName", SqlDbType.VarChar, 8000)
                Params(0).Value = Complaint
                Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(1).Value = EmplCode
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_HR_COMPLAINT_REGISTER", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                ErrorFlag = 1
                Message = ex.Message.ToString
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            StrValue = ErrorFlag.ToString + "ʘ" + Message.ToString
        End If
        CallBackReturn = StrValue.ToString
    End Sub
End Class
