﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient

Imports System.IO
Imports System.Net
Imports System.Text
Partial Class DeclarationUpload
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler

    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GN.FormAccess(CInt(Session("UserID")), 1170) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Me.Master.subtitle = "FATCA - CRS Declaration Form Upload"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim BranchID As Integer
            BranchID = CInt(Session("BranchID"))
            DT = DB.ExecuteDataSet(" select distinct [branch code] , [BranchName] + ' | ' + convert(varchar,cast([branch code] as int)) from FATCA_CLIENTS  where [branch code]=" & BranchID.ToString & " order by 2 ").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
            cmbBranch.SelectedIndex = 0

            DT = DB.ExecuteDataSet("Select  -1 as [Sangam ID] , '<---Select--->' AS [Sangam Name] UNION select distinct [Sangam ID] , [Sangam Name] + ' | ' + convert(varchar,cast([Sangam ID] as int)) AS [Sangam Name] from FATCA_CLIENTS A inner join FATCA_CRS B ON A.[Client ID] = B.Client_ID where [branch code]=" & BranchID.ToString & " order by 2  ").Tables(0)
            GN.ComboFill(cmbSangam, DT, 0, 1)

            Me.cmbSangam.Attributes.Add("onchange", "return SangamOnChange()")
            Me.btnSave.Attributes.Add("onclick", "return btnSave_onclick()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)

            cmbSangam.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim SangamID As String = CStr(Data(1))
            Dim StrVal As String = ""
            DT = DB.ExecuteDataSet("select [Client ID],(case when [First Name] = 'NULL' then '' else isnull(LTRIM([First Name]) ,'') end + ' ' +  case when [Middle Name] = 'NULL' then '' else isnull(LTRIM([Middle Name]) ,'') end + ' ' + case when [Last Name] = 'NULL' then '' else isnull(LTRIM([Last Name]) ,'') end)+ ' | ' +convert(varchar(30), convert(bigint,[Client ID])) from FATCA_CLIENTS A inner join FATCA_CRS B ON A.[Client ID] = B.Client_ID  " +
                " where A.[sangam ID] =" + SangamID + "  and B.Upload_Status is null " +
                " order by  (case when [First Name] = 'NULL' then '' else isnull(LTRIM([First Name]) ,'') end + ' ' +  case when [Middle Name] = 'NULL' then '' else isnull(LTRIM([Middle Name]) ,'') end + ' ' + case when [Last Name] = 'NULL' then '' else isnull(LTRIM([Last Name]) ,'') end) ").Tables(0)
            If (DT.Rows.Count > 0) Then
                For n As Integer = 0 To DT.Rows.Count - 1
                    StrVal += DT.Rows(n)(0).ToString & "ÿ" & DT.Rows(n)(1).ToString
                    If n < DT.Rows.Count - 1 Then
                        StrVal += "Ñ"
                    End If
                Next
                CallBackReturn += StrVal
            Else
                CallBackReturn = "ØØ"
            End If
           
        End If
    End Sub
#End Region
    Private Sub initializeControls()

        cmbBranch.SelectedIndex = 0
        cmbSangam.SelectedIndex = 0
        cmbClient.SelectedIndex = 0
        cmbSangam.Focus()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim ClientID As String = Me.hdnValue.Value
        Dim UserID As Integer = CInt(Session("UserID"))

        Try
            Dim AttachSign As Byte() = Nothing
            Dim NoofAttachments As Integer = 0
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim myFile As HttpPostedFile = hfc(0)
            Dim nFileLen As Integer = myFile.ContentLength
            Dim FileName As String = ""
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachSign = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachSign, 0, nFileLen)

            End If

            Dim Params(6) As SqlParameter
            Params(0) = New SqlParameter("@ClientID", SqlDbType.VarChar, 50)
            Params(0).Value = ClientID
            Params(1) = New SqlParameter("@ATTACHSIGN", SqlDbType.VarBinary)
            Params(1).Value = AttachSign
            Params(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
            Params(2).Value = ContentType
            Params(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
            Params(3).Value = FileName
            Params(4) = New SqlParameter("@User_Id", SqlDbType.Int)
            Params(4).Value = UserID
            Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(5).Direction = ParameterDirection.Output
            Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(6).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_FATCA_CRS_UPLOAD", Params)
            ErrorFlag = CInt(Params(5).Value)
            Message = CStr(Params(6).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        If ErrorFlag = 0 Then
            initializeControls()
        End If

        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "');  ")
        cl_script1.Append("        window.open('DeclarationUpload.aspx', '_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)

    End Sub
End Class
