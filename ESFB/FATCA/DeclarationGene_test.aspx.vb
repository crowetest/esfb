﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class DeclarationGene_test
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GN.FormAccess(CInt(Session("UserID")), 1170) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Me.Master.subtitle = "FATCA - CRS Declaration by Individual Customer"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim BranchID As Integer
            BranchID = 101 '' CInt(Session("BranchID"))
            DT = DB.ExecuteDataSet(" select distinct [branch code] , [BranchName] + ' | ' + convert(varchar,cast([branch code] as int)) from FATCA_CLIENTS  where [branch code]=" & BranchID.ToString & " order by 2 ").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
            cmbBranch.SelectedIndex = 0

            DT = DB.ExecuteDataSet("Select  -1 as [Sangam ID] , '----#Select#-----' AS [Sangam Name] UNION select distinct [Sangam ID] , [Sangam Name] + ' | ' + convert(varchar,cast([Sangam ID] as int)) AS [Sangam Name] from FATCA_CLIENTS where [branch code]=" & BranchID.ToString & " order by 2  ").Tables(0)
            GN.ComboFill(cmbSangam, DT, 0, 1)

            Me.cmbSangam.Attributes.Add("onchange", "return SangamOnChange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)

            cmbSangam.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim SangamID As Integer = CInt(Data(1))
            DT = DB.ExecuteDataSet("select [Title],[First Name],isnull([Middle Name],''), [Last Name], [Client ID] , [CIF], [Mobile No], [Aadhaar], [Second ID Name],[Second ID Number], [Father Name], [Spouse Name],[DateofBirth]  from FATCA_CLIENTS  where [Sangam ID] = " + SangamID.ToString() + " order by [First Name]  ").Tables(0)
            If (DT.Rows.Count > 0) Then
                For n As Integer = 0 To DT.Rows.Count - 1
                    CallBackReturn += DT.Rows(n)(0).ToString() + "Ø" + DT.Rows(n)(1).ToString() + "Ø" + DT.Rows(n)(2).ToString() + "Ø" + DT.Rows(n)(3).ToString() + "Ø" + DT.Rows(n)(4).ToString() + "Ø" + DT.Rows(n)(5).ToString() + "Ø" + DT.Rows(n)(6).ToString() + "Ø" + DT.Rows(n)(7).ToString() + "Ø" + DT.Rows(n)(8).ToString() + "Ø" + DT.Rows(n)(9).ToString() + "Ø" + DT.Rows(n)(10).ToString() + "Ø" + DT.Rows(n)(11).ToString() + "Ø" + IIf(IsDate(DT.Rows(n)(12).ToString()) = True, CDate(DT.Rows(n)(12).ToString()).ToString("dd-MMM-yyyy"), "").ToString()
                    CallBackReturn += "¥"
                Next
            Else
                CallBackReturn = "ØØ"
            End If
        ElseIf CInt(Data(0)) = 2 Then

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim ClientDtl As String = CStr(Data(1))
            Dim UserID As Integer = CInt(Session("UserID"))
            Try

                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@ClientID_Dtl", SqlDbType.VarChar)
                Params(0).Value = ClientDtl
                Params(1) = New SqlParameter("@User_Id", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_FATCA_CRS_Generation", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message

        End If
    End Sub
#End Region


End Class
