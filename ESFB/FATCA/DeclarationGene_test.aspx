﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="DeclarationGene_test.aspx.vb" Inherits="DeclarationGene_test" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 17px;
        }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
    </style>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
           
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
       
        for (a = 0; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function ClearCombo(control) {
        document.getElementById(control).options.length = 0;
        var option1 = document.createElement("OPTION");
        option1.value = -1;
        option1.text = " -----Select-----";
        document.getElementById(control).add(option1);
    }
    
    function SangamOnChange(){
        var SangamID = document.getElementById("<%= cmbSangam.ClientID %>").value;
        if (SangamID != '')
        {
            var ToData = "1Ø" + SangamID;
            ToServer(ToData, 1);
        }
    }
    function SelectUnSelectAll()
        {
        
            row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
            var i =1 ;       
            for (n = 0; n < row.length; n++) {                   
                col = row[n].split("µ");
                if (document.getElementById("MainChk").checked == true)
                    document.getElementById("chk" + i.toString()).checked = true;
                else
                    document.getElementById("chk" + i.toString()).checked = false;
                i+=1;
            }
        }
        function SingleToggle(i)
        {
            
             if (document.getElementById("chk" + i.toString()).checked == false)
                document.getElementById("MainChk").checked = false;
            
        }
    function tablefill()
    {  
        var row_bg = 0;
        var tab = "";
        tab += "<table style='width:100%;font-family:cambria;frame=box ;border: thin solid #C0C0C0';>";
        tab += "<tr  class=mainhead>";
        tab += "<td style='width:3%;text-align:center;font-size:11pt;' onclick=SelectUnSelectAll() ><input id='MainChk' type='checkbox' /></td>";
        tab += "<td style='width:2%;text-align:center'>#</td>";
        tab += "<td style='width:3%;text-align:center' >Title</td>";
        tab += "<td style='width:8%;text-align:center' >First Name</td>";
        tab += "<td style='width:8%;text-align:center' >Midddle Name</td>";
        tab += "<td style='width:8%;text-align:center' >Last Name</td>";
        tab += "<td style='width:7%;text-align:center' >Client ID</td>";
        tab += "<td style='width:7%;text-align:center' >CIF</td>";
        tab += "<td style='width:7%;text-align:center' >Mobile No</td>";
        tab += "<td style='width:7%;text-align:center' >Aadhaar</td>";
        tab += "<td style='width:7%;text-align:center' >Second ID Name</td>";
        tab += "<td style='width:7%;text-align:center' >Second ID No</td>";
        tab += "<td style='width:7%;text-align:center' >Father Name</td>";
        tab += "<td style='width:7%;text-align:center' >Spouse Name</td>";
        tab += "<td style='width:7%;text-align:center' >Date of Birth</td>";
        tab += "</tr>";
        var i=1;
        if (document.getElementById("<%= hid_Display.ClientID %>").value!="")
        {
            document.getElementById("rowSangam").style.display='';
            row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
            for (n = 0; n < row.length-1; n++) 
            {                   
                col = row[n].split("Ø");
                if (row_bg == 0) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
                    row_bg = 1;
                    tab += "<tr class=sub_first>";
                }
                else {
                    row_bg = 0;
                    tab += "<tr class=sub_second>";
                }
                var txtid = "chk" + i.toString();
                tab += "<td style='width:3%;text-align:center' onclick=SingleToggle("+i+") ><input id='"+ txtid +"' type='checkbox' /></td>";
                tab += "<td style='width:2%;text-align:center'>" + i + "</td>";
                tab += "<td style='width:3%;text-align:left'>" + col[0] + "</td>";
                tab += "<td style='width:8%;text-align:left'>" + col[1] + "</td>";
                tab += "<td style='width:8%;text-align:left'>" + col[2] + "</td>";
                tab += "<td style='width:8%;text-align:left'>" + col[3] + "</td>";
                tab += "<td style='width:7%;text-align:left'>" + col[4] + "</td>";
                tab += "<td style='width:7%;text-align:left'>" + col[5] + "</td>";
                tab += "<td style='width:7%;text-align:left'>" + col[6] + "</td>";
                tab += "<td style='width:7%;text-align:left'>" + col[7] + "</td>";
                tab += "<td style='width:7%;text-align:left'>" + col[8] + "</td>";
                tab += "<td style='width:7%;text-align:left'>" + col[9] + "</td>";
                tab += "<td style='width:7%;text-align:left'>" + col[10] + "</td>";
                tab += "<td style='width:7%;text-align:left'>" + col[11] + "</td>";
                tab += "<td style='width:7%;text-align:left'>" + col[12] + "</td>";
                tab += "</tr>";  
                i+=1;                    
            }
            tab += "</table>";
        }
        else{

                document.getElementById("rowSangam").style.display='none';
            }
        document.getElementById("<%= pnBranchList.ClientID %>").innerHTML = tab;
        //--------------------- Clearing Data ------------------------//
    }
    function FromServer(arg, context) 
    {
        switch (context) 
        {
            case 1:
                {
                    document.getElementById("<%= hid_Display.ClientID %>").value = arg;
                    tablefill();
                    break;
                }
            case 2:
                {
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    var ClientID = 0;
                    var ClientName ='';
                    var i=1;
                    if (Data[0] == 0) 
                    {
                        row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
                        for (n = 0; n < row.length-1; n++) 
                        {   
                                        
                            col = row[n].split("Ø");
                            var txtid = "chk" + i.toString();
     
                            if (document.getElementById(txtid).checked == true )
                            {
                          
                                ClientID = col[4];
                                ClientName = col[1];
                                window.open("FATCA_CRS_Declaration_test.aspx?ClientID="+ClientID+"&ClientName="+ClientName+"", "_blank");
                            }
                            i +=1;
                        }
                    }
                    break;
                }
        }
    }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

   
function btnGenerate_onclick() {
    var SangamID = document.getElementById("<%= cmbSangam.ClientID %>").value;
    if (SangamID == -1)
    {
        alert("Select Sangam");
        return false;
    }
    var i=1;
    var ToData = "2Ø" ;
    var ClientID = 0;
    var ClientName ='';
    row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
    for (n = 0; n < row.length-1; n++) 
    {                   
        col = row[n].split("Ø");
        var txtid = "chk" + i.toString();
     
        if (document.getElementById(txtid).checked == true )
        {
            ClientID = col[4];
            ClientName = col[1];
            ToData += col[4] + "¥";
        }
        i +=1;
    }
    if (ClientID ==0)
    {
        alert("Select Client");
        return false;
    }
    else
    {
        ToServer(ToData, 2);
    }
  
}

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return RequestOnchange()
            // ]]>
        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 100%; margin: 0px auto;">
        <tr id="branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Sangam">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Sangam
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbSangam" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
    
        <tr>
            <td style="text-align: center;" colspan="3">
                &nbsp;
            </td>
        </tr>
         <tr id="rowSangam" > 
            <td style="text-align:center;" colspan="3">
                <table align="center" class="style1" style="text-align:center; width:95%; margin-left: auto; margin-right: auto; margin-top: 0px; height: 34px;">
                    
                    <tr>
                        <td style="width:60%;text-align:center;" class="mainhead">
                            Member List</td>
                   
                    </tr>
                    <tr>
                        <td style="width:60%;text-align:left;">
                            <asp:Panel ID="pnBranchList" runat="server">
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:HiddenField ID="hid_Display" runat="server" />
                <input id="btnGenerate" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                    value="GENERATE" onclick="return btnGenerate_onclick()" />&nbsp;
&nbsp;
&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

