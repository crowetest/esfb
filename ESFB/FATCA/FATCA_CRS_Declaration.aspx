﻿<%@ Page Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="FATCA_CRS_Declaration.aspx.vb" Inherits="FATCA_CRS_Declaration" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ MasterType VirtualPath="~/REPORT.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
        <style type="text/css">
            .button{
                background-image: url('/image/print.png');
                cursor:pointer;
                border: none;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 773px; height: 539px">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <p style="margin-left: 520px">
            &nbsp;&nbsp
            <rsweb:ReportViewer 
            ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
            WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Height="448px" 
            Width="714px" style="margin-right: 0px" SizeToReportContent="True" 
            ShowBackButton="False" ShowExportControls="False" 
            ShowFindControls="False" ShowPageNavigationControls="False" 
            ShowRefreshButton="False" 
            ShowWaitControlCancelLink="False" ShowZoomControl="False" 
            AsyncRendering="False">
        <LocalReport ReportPath="FATCA\CRS_Declaration.rdlc"> <DataSources>
        <rsweb:ReportDataSource Name="FATCA_DS" />
        </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    </div>
    </form>
</body>
</html>
</asp:Content>
