﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" CodeFile="DeclarationUpload.aspx.vb" Inherits="DeclarationUpload" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .fileUpload
        {
            font-size: 11px;
            color: #000000;
            border: 1px solid #7f9db9;
            }
        .ScrollClass
        {
            text-align: center;
            color: #E31E24;
            background-color: #EBDDDD;
            height: 20px;
        }
        .style2
        {
            width: 25%;
            height: 23px;
        }
        .style3
        {
            width: 12%;
            height: 23px;
        }
        .style4
        {
            width: 63%;
            height: 23px;
        }
        .style5
        {
            width: 25%;
            height: 52px;
        }
        .style6
        {
            width: 12%;
            height: 52px;
        }
        .style7
        {
            width: 63%;
            height: 52px;
        }
    </style>
   
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
    
           
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
       
        for (a = 0; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function ClearCombo(control) {
        document.getElementById(control).options.length = 0;
        var option1 = document.createElement("OPTION");
        option1.value = -1;
        option1.text = " -----Select-----";
        document.getElementById(control).add(option1);
    }
    
    function SangamOnChange(){
        var SangamID = document.getElementById("<%= cmbSangam.ClientID %>").value;
        if (SangamID != '')
        {
            var ToData = "1Ø" + SangamID;
            ToServer(ToData, 1);
        }
    }
    function validate_new_fileupload()
    {
        var fileName = document.getElementById("<%= fup1.ClientID %>").value;
        var allowed_extensions = ["pdf"];
      
        var file_extension = fileName.split('.').pop().toLowerCase(); 
        var a = allowed_extensions.indexOf(file_extension);

        if (a==-1)
        {
            alert("Select .pdf file.");
            document.getElementById("<%= fup1.ClientID %>").value = "";
            FileSelected = 0;
            return false;

        }
       
        FileSelected = 1;
        return true;

    }
    function FromServer(arg, context) 
    {
        switch (context) 
        {
            case 1:
                var Data = arg;
                ComboFill(Data, "<%= cmbClient.ClientID%>");
                break;
        }
    }

    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }

   
function btnSave_onclick() {
    var SangamID = document.getElementById("<%= cmbSangam.ClientID %>").value;
    var ClientID = document.getElementById("<%= cmbClient.ClientID %>").value;

    if (SangamID == -1)
    {
        alert("Select Sangam");
        return false;
    }
    else if (ClientID == -1)
    {
        alert("Select Client");
        return false;
    }
    document.getElementById("<%= hdnValue.ClientID %>").value = ClientID;
  
}

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return RequestOnchange()
            // ]]>
        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 100%; margin: 0px auto;">
        <tr id="branch">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Branch
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;" runat="server"
                    Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Sangam">
            <td class="style2">
            </td>
            <td style="text-align: left;" class="style3">
                Sangam
            </td>
            <td class="style4">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbSangam" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
   <tr id="Client">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Client
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbClient" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>        
    
        <tr>
            <td class="style5">
            </td>
            <td style="text-align: left;" class="style6">
                Upload Form
            </td>
            <td class="style7">
                &nbsp; &nbsp;
                <div id="fileUploadarea" style="width: 70%"> &nbsp;
                <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload"  onchange = "return validate_new_fileupload()"/>
                    <%--<input id="fup1" type="file" name="fup1"  runat="server" accept=".pdf"
                style="font-family: Cambria; font-size: 10.5pt" onchange = 'validate_new_fileupload();'/>--%>
                </div>
            </td>
        </tr>
   
        <tr>
            <td style="text-align: center;" colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:HiddenField ID="hid_Display" runat="server" />
                 <asp:Button ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="10%" />&nbsp;

&nbsp;
&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

