﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="TMRAllocation.aspx.vb" Inherits="TMR_TMRAllocation" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {  
                document.getElementById("rAttach").style.display = "none"; 
             //   ToServer("1ʘ", 1);
             
            }
             function TmrOnChange()
            {
           
                if (document.getElementById("<%= cmbTmrNo.ClientID %>").value == -1)
                {
                    alert("Select TMR Number");
                    document.getElementById("<%= cmbTmrNo.ClientID %>").focus();
                    return false;
                } 
                ToServer("1ʘ" + document.getElementById("<%= cmbTmrNo.ClientID %>").value, 1);
            }
            function setStartDate(sender, args) 
            {
                sender._endDate = new Date();
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {
                        if(Arg != "")
                        {
                            var Args = Arg.split("£"); 
                            var Data = Args[0].split("¥");
                           
                            document.getElementById("<%= txtcifno.ClientID %>").value=Data[0];
                            document.getElementById("<%= txtCustname.ClientID %>").value=Data[1];
                            document.getElementById("<%= txtnoofaccnt.ClientID %>").value=Data[2];
                            document.getElementById("<%= txtaccnt.ClientID %>").value=Data[3];
                            document.getElementById("<%= txtdescription.ClientID %>").value=Data[4];
                            document.getElementById("<%= txtReadyDt.ClientID %>").value=Data[5];
                            if(Data[6]>0)
                            {
                           
                                document.getElementById("rAttach").style.display = "";
                                var Tab = "";
                                Tab += "<div id='ScrollDiv' style='width:100%; height:20px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                                Tab += "<tr class='sub_first';>";
                                Tab += "<td style='width:90%;text-align:left;cursor: pointer;'><a href='ShowRiskFormat.aspx?DocID=" + Data[6]  + "&tmrno=" + Data[10]+" '> " +Data[9] + " </a></td></tr>";
                                Tab += "</table></div>";
                                document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;
                            }
                            else{
                                document.getElementById("rAttach").style.display = "none";
                            }
                         
                         break;
                     }
                }
                 case 3:
                    {
                        var Data = Arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("TMRAllocation.aspx", "_self");
                        break;
                    }
                }
            }

             function btnSubmit_onclick() {
                if (document.getElementById("<%= cmbtmrno.ClientID %>").value == -1)
                {
                    alert("Select TmrNo");
                    document.getElementById("<%= cmbtmrno.ClientID %>").focus();
                    return false;
                }
             
               
            }
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        

           
          
        </script>

    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        TMR No</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbTmrNo" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RFVcmbTmrNo" ControlToValidate="cmbTmrNo" runat="server" ErrorMessage="Select TMR No" InitialValue="-1" ForeColor="Red" ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>

                        <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        CIF No</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:TextBox ID="txtcifno" runat="server"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Customer Name</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:TextBox ID="txtCustname" runat="server"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                  <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        No.of Accounts</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:TextBox ID="txtnoofaccnt" runat="server" onkeypress="NumericCheck(event)"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Account Number</td>
                    <td style="width: 30%; text-align: left;">                        
                          <asp:TextBox ID="txtaccnt" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine"  Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                
               <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Description about Issue
                    </td>
                    <td style="width: 30%; text-align: left;">
                        <asp:TextBox ID="txtdescription" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>

                <tr id="rAttach">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Attachment</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                  
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Upload Document, 
                        if any<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span>
                    </td>
                    <td style="width: 100%; text-align: left;">
                        <input id="fupBrd" runat="server" cssclass="fileUpload" type="file" /></td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Target Date</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:TextBox ID="txtReadyDt" runat="server" onkeypress="NumericCheck(event)"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="txtReadyDt_CalendarExtender" runat="server" 
                            Format="dd/MMM/yyyy" OnClientShowing="setStartDate" 
                            TargetControlID="txtReadyDt">
                        </ajaxToolkit:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RFVtxtReadyDt" ControlToValidate="txtReadyDt" runat="server" ErrorMessage="Select Target Date" ForeColor="Red" ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
   
               
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Bucket</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbbucket" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RFVcmbbucket" ControlToValidate="cmbbucket" runat="server" ErrorMessage="Select Bucket"  InitialValue="-1" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                 <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Sub Bucket</td>
                    <td style="width: 30%; text-align: left;">
                        <asp:TextBox ID="txtsubbucket" runat="server"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"   />
                           <asp:RequiredFieldValidator ID="RFVtxtsubbucket" ControlToValidate="txtsubbucket" runat="server" ErrorMessage="EnterSubBucket" ForeColor="Red" ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <%-- <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                       Final Bucket</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbFinalbucket" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>

                 <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Bucket 3</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbbucket3" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>--%>
              
               <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Vertical</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbvertical" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                           <asp:RequiredFieldValidator ID="RFVcmbvertical" ControlToValidate="cmbvertical" runat="server" ErrorMessage="Select Vertical"  InitialValue="-1" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
              <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Sub Vertical</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbsubvertical" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RFVcmbsubvertical" ControlToValidate="cmbsubvertical" runat="server" ErrorMessage="Select SubVertical"  InitialValue="-1" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                 <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Criticality</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbcritical" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                          <asp:RequiredFieldValidator ID="RFVcmbcritical" ControlToValidate="cmbcritical" runat="server" ErrorMessage="Select Criticality"  InitialValue="-1" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                 <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                       Next Level Employee</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:TextBox ID="txtempcode" runat="server" onkeypress="NumericCheck(event)"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                          <asp:RequiredFieldValidator ID="RFVtxtempcode" ControlToValidate="txtempcode" runat="server" ErrorMessage="Enter   Next Level Employee" ForeColor="Red" ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Credit Channel
                    </td>
                    <td style="width: 30%; text-align: left;">
                        <asp:TextBox ID="txtcredit" runat="server"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"   />
                          <asp:RequiredFieldValidator ID="RFVtxtcredit" ControlToValidate="txtcredit" runat="server" ErrorMessage="Enter Credit Channel" ForeColor="Red" ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Debit Channel</td>
                    <td style="width: 30%; text-align: left;">
                        <asp:TextBox ID="txtdebit" runat="server"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"   />
                         <asp:RequiredFieldValidator ID="RFVtxtdebit" ControlToValidate="txtdebit" runat="server" ErrorMessage="Enter Debit Channel" ForeColor="Red" ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Remarks</td>
                    <td style="width: 30%; text-align: left;">
                        <asp:TextBox ID="txtremarks" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                          <asp:RequiredFieldValidator ID="RFVtxtremarks" ControlToValidate="txtremarks" runat="server" ErrorMessage="Enter Remarks" ForeColor="Red" ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
               
               
            
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                 <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Width="67px" />
                        &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnIncident" runat="server" />
            <asp:HiddenField ID="hdnDate" runat="server" />
            <asp:HiddenField ID="hdnTime" runat="server" />
            <configuration>
    <system.web>
    <compilaton debug="true" targetFramework="4.0"></compilaton>
    </system.web>
    </configuration>
        </div>
        <br />
    </div>
</asp:Content>