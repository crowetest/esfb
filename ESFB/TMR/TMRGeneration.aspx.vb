﻿
Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class TMR_TMRGeneration
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub TMRGeneration_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub TMRGeneration_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "TMR Generation"

        If GF.FormAccess(CInt(Session("UserID")), 1468) = False Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        'Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        'If Not IsPostBack Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        'End If
        'Me.btnSave.Attributes.Add("onclick", "return SaveOnClick()")

    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
    
    End Sub
#End Region

#Region "Confirm"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
      
       Dim myFile As HttpPostedFile = fupBrd.PostedFile
            Dim nFileLen As Integer = myFile.ContentLength
            Dim FileName As String = ""
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
            FileName = myFile.FileName
            AttachImg = New Byte(nFileLen - 1) {}
            myFile.InputStream.Read(AttachImg, 0, nFileLen)
                Dim FileLength As Integer = CInt(myFile.ContentLength) 'Convert into bytes
                If FileLength > 4000000 Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('Please Check the Size of attached file');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
                Dim FileExtension As String = Path.GetExtension(myFile.FileName)
                If Not (FileExtension = ".xls" Or FileExtension = ".xlsx" Or FileExtension = ".jpg" Or FileExtension = ".jpeg" Or FileExtension = ".doc" Or FileExtension = ".docx" Or FileExtension = ".zip" Or FileExtension = ".pdf" Or FileExtension = ".PDF" Or FileExtension = ".XLS" Or FileExtension = ".XLSX" Or FileExtension = ".JPG" Or FileExtension = ".JPEG" Or FileExtension = ".DOC" Or FileExtension = ".DOCX" Or FileExtension = ".ZIP") Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
            End If

     
        Try

            Dim Issue As String = CStr(Me.txtdescription.Text)
            Dim cif As String = CStr(Me.txtcifno.Text)
            Dim IntDate As Date = CDate(Me.txtReadyDt.Text)
            Dim account As String = CStr(Me.txtaccnt.Text)
            Dim custname As String = CStr(Me.txtCustname.Text)
            Dim noofacc As Integer = CInt(Me.txtnoofaccnt.Text)

            Dim Params(11) As SqlParameter
            Params(0) = New SqlParameter("@p_Cif_No", SqlDbType.VarChar, 100)
            Params(0).Value = cif
            Params(1) = New SqlParameter("@p_Customer_Name", SqlDbType.VarChar, 100)
            Params(1).Value = custname
            Params(2) = New SqlParameter("@p_No_Of_Accounts", SqlDbType.Int)
            Params(2).Value = noofacc
            Params(3) = New SqlParameter("@p_Account_No", SqlDbType.VarChar, 1000)
            Params(3).Value = account
            Params(4) = New SqlParameter("@p_Issue_Noted", SqlDbType.VarChar, 600)
            Params(4).Value = Issue
            Params(5) = New SqlParameter("@p_Target_Date", SqlDbType.Date)
            Params(5).Value = IntDate
            Params(6) = New SqlParameter("@p_User_Id", SqlDbType.Int)
            Params(6).Value = UserID
            Params(7) = New SqlParameter("@p_File", SqlDbType.VarBinary)
            Params(7).Value = AttachImg
            Params(8) = New SqlParameter("@p_File_ype", SqlDbType.VarChar, 500)
            Params(8).Value = ContentType
            Params(9) = New SqlParameter("@p_File_Name", SqlDbType.VarChar, 500)
            Params(9).Value = FileName
          
            Params(10) = New SqlParameter("@p_ErrorStatus", SqlDbType.Int)
            Params(10).Direction = ParameterDirection.Output
            Params(11) = New SqlParameter("@p_OutputMessage", SqlDbType.VarChar, 5000)
            Params(11).Direction = ParameterDirection.Output
           
            DB.ExecuteNonQuery("SP_TMR_Generation", Params)
            ErrorFlag = CInt(Params(10).Value)
            Message = CStr(Params(11).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "'); ")
        cl_script1.Append("        window.open('TMRGeneration.aspx', '_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        If ErrorFlag = 0 Then
            initializeControls()
        End If

    End Sub
    Private Sub initializeControls()
    
    End Sub
#End Region

End Class


