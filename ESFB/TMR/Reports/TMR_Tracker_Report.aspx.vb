﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class TMR_Tracker_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT, DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                Dim reportId As String = ""
                If CStr(Request.QueryString.Get("reportId")) <> "0" Then
                    reportId = CStr(GF.Decrypt(Request.QueryString.Get("reportId")))
                End If
                Dim value As String = ""
                If CStr(Request.QueryString.Get("value")) <> "" Then
                    value = CStr(GF.Decrypt(Request.QueryString.Get("value")))
                End If
                DT = GF.GeneralFunctionForTMR("TMRRPT003", reportId.ToString + "~" + value)
                DT2 = GF.GeneralFunctionForTMR("TMRRPT001", reportId.ToString)
                DT3 = GF.GeneralFunctionForTMR("TMRRPT003", "0~" + value)

                RH.Heading(Session("FirmName"), tb, DT2.Rows(0)("reportName").ToString, 500)

                Dim DR As DataRow
                tb.Attributes.Add("width", "500")

                Dim TRSHead As New TableRow
                TRSHead.Width = "500"
                TRSHead.BorderWidth = "1"
                TRSHead.BorderStyle = BorderStyle.Solid
                TRSHead.Style.Add("background-color", "lightsteelblue")

                Dim TRHead_1 As New TableRow

                Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14, TRHead_1_15, TRHead_1_16, TRHead_1_17, TRHead_1_18, TRHead_1_19, TRHead_1_20, TRHead_1_21, TRHead_1_22, TRHead_1_23, TRHead_1_24, TRHead_1_25, TRHead_1_26, TRHead_1_27, TRHead_1_28, TRHead_1_29, TRHead_1_30, TRHead_1_31, TRHead_1_32, TRHead_1_33, TRHead_1_34, TRHead_1_35, TRHead_1_36, TRHead_1_37 As New TableCell

                TRHead_1_00.BorderWidth = "1"
                TRHead_1_01.BorderWidth = "1"
                TRHead_1_02.BorderWidth = "1"
                TRHead_1_03.BorderWidth = "1"
                TRHead_1_04.BorderWidth = "1"
                TRHead_1_05.BorderWidth = "1"
                TRHead_1_06.BorderWidth = "1"
                TRHead_1_07.BorderWidth = "1"
                TRHead_1_08.BorderWidth = "1"
                TRHead_1_09.BorderWidth = "1"
                TRHead_1_10.BorderWidth = "1"
                TRHead_1_11.BorderWidth = "1"
                TRHead_1_12.BorderWidth = "1"
                TRHead_1_13.BorderWidth = "1"
                TRHead_1_14.BorderWidth = "1"
                TRHead_1_15.BorderWidth = "1"
                TRHead_1_16.BorderWidth = "1"
                TRHead_1_17.BorderWidth = "1"
                TRHead_1_18.BorderWidth = "1"
                TRHead_1_19.BorderWidth = "1"
                TRHead_1_20.BorderWidth = "1"
                TRHead_1_21.BorderWidth = "1"
                TRHead_1_22.BorderWidth = "1"
                TRHead_1_23.BorderWidth = "1"
                TRHead_1_24.BorderWidth = "1"
                TRHead_1_25.BorderWidth = "1"
                TRHead_1_26.BorderWidth = "1"
                TRHead_1_27.BorderWidth = "1"
                TRHead_1_28.BorderWidth = "1"
                TRHead_1_29.BorderWidth = "1"
                TRHead_1_30.BorderWidth = "1"
                TRHead_1_31.BorderWidth = "1"
                TRHead_1_32.BorderWidth = "1"
                TRHead_1_33.BorderWidth = "1"
                TRHead_1_34.BorderWidth = "1"
                TRHead_1_35.BorderWidth = "1"
                TRHead_1_36.BorderWidth = "1"
                TRHead_1_37.BorderWidth = "1"

                TRHead_1_00.BorderColor = Drawing.Color.Silver
                TRHead_1_01.BorderColor = Drawing.Color.Silver
                TRHead_1_02.BorderColor = Drawing.Color.Silver
                TRHead_1_03.BorderColor = Drawing.Color.Silver
                TRHead_1_04.BorderColor = Drawing.Color.Silver
                TRHead_1_05.BorderColor = Drawing.Color.Silver
                TRHead_1_06.BorderColor = Drawing.Color.Silver
                TRHead_1_07.BorderColor = Drawing.Color.Silver
                TRHead_1_08.BorderColor = Drawing.Color.Silver
                TRHead_1_09.BorderColor = Drawing.Color.Silver
                TRHead_1_10.BorderColor = Drawing.Color.Silver
                TRHead_1_11.BorderColor = Drawing.Color.Silver
                TRHead_1_12.BorderColor = Drawing.Color.Silver
                TRHead_1_13.BorderColor = Drawing.Color.Silver
                TRHead_1_14.BorderColor = Drawing.Color.Silver
                TRHead_1_15.BorderColor = Drawing.Color.Silver
                TRHead_1_16.BorderColor = Drawing.Color.Silver
                TRHead_1_17.BorderColor = Drawing.Color.Silver
                TRHead_1_18.BorderColor = Drawing.Color.Silver
                TRHead_1_19.BorderColor = Drawing.Color.Silver
                TRHead_1_20.BorderColor = Drawing.Color.Silver
                TRHead_1_21.BorderColor = Drawing.Color.Silver
                TRHead_1_22.BorderColor = Drawing.Color.Silver
                TRHead_1_23.BorderColor = Drawing.Color.Silver
                TRHead_1_24.BorderColor = Drawing.Color.Silver
                TRHead_1_25.BorderColor = Drawing.Color.Silver
                TRHead_1_26.BorderColor = Drawing.Color.Silver
                TRHead_1_27.BorderColor = Drawing.Color.Silver
                TRHead_1_28.BorderColor = Drawing.Color.Silver
                TRHead_1_29.BorderColor = Drawing.Color.Silver
                TRHead_1_30.BorderColor = Drawing.Color.Silver
                TRHead_1_31.BorderColor = Drawing.Color.Silver
                TRHead_1_32.BorderColor = Drawing.Color.Silver
                TRHead_1_33.BorderColor = Drawing.Color.Silver
                TRHead_1_34.BorderColor = Drawing.Color.Silver
                TRHead_1_35.BorderColor = Drawing.Color.Silver
                TRHead_1_36.BorderColor = Drawing.Color.Silver
                TRHead_1_37.BorderColor = Drawing.Color.Silver

                TRHead_1_00.BorderStyle = BorderStyle.Solid
                TRHead_1_01.BorderStyle = BorderStyle.Solid
                TRHead_1_02.BorderStyle = BorderStyle.Solid
                TRHead_1_03.BorderStyle = BorderStyle.Solid
                TRHead_1_04.BorderStyle = BorderStyle.Solid
                TRHead_1_05.BorderStyle = BorderStyle.Solid
                TRHead_1_06.BorderStyle = BorderStyle.Solid
                TRHead_1_07.BorderStyle = BorderStyle.Solid
                TRHead_1_08.BorderStyle = BorderStyle.Solid
                TRHead_1_09.BorderStyle = BorderStyle.Solid
                TRHead_1_10.BorderStyle = BorderStyle.Solid
                TRHead_1_11.BorderStyle = BorderStyle.Solid
                TRHead_1_12.BorderStyle = BorderStyle.Solid
                TRHead_1_13.BorderStyle = BorderStyle.Solid
                TRHead_1_14.BorderStyle = BorderStyle.Solid
                TRHead_1_15.BorderStyle = BorderStyle.Solid
                TRHead_1_16.BorderStyle = BorderStyle.Solid
                TRHead_1_17.BorderStyle = BorderStyle.Solid
                TRHead_1_18.BorderStyle = BorderStyle.Solid
                TRHead_1_19.BorderStyle = BorderStyle.Solid
                TRHead_1_20.BorderStyle = BorderStyle.Solid
                TRHead_1_21.BorderStyle = BorderStyle.Solid
                TRHead_1_22.BorderStyle = BorderStyle.Solid
                TRHead_1_23.BorderStyle = BorderStyle.Solid
                TRHead_1_24.BorderStyle = BorderStyle.Solid
                TRHead_1_25.BorderStyle = BorderStyle.Solid
                TRHead_1_26.BorderStyle = BorderStyle.Solid
                TRHead_1_27.BorderStyle = BorderStyle.Solid
                TRHead_1_28.BorderStyle = BorderStyle.Solid
                TRHead_1_29.BorderStyle = BorderStyle.Solid
                TRHead_1_30.BorderStyle = BorderStyle.Solid
                TRHead_1_31.BorderStyle = BorderStyle.Solid
                TRHead_1_32.BorderStyle = BorderStyle.Solid
                TRHead_1_33.BorderStyle = BorderStyle.Solid
                TRHead_1_34.BorderStyle = BorderStyle.Solid
                TRHead_1_35.BorderStyle = BorderStyle.Solid
                TRHead_1_36.BorderStyle = BorderStyle.Solid
                TRHead_1_37.BorderStyle = BorderStyle.Solid


                TRHead_1_00.BackColor = Drawing.Color.LightBlue
                TRHead_1_01.BackColor = Drawing.Color.LightBlue
                TRHead_1_02.BackColor = Drawing.Color.LightBlue
                TRHead_1_03.BackColor = Drawing.Color.LightBlue
                TRHead_1_04.BackColor = Drawing.Color.LightBlue
                TRHead_1_05.BackColor = Drawing.Color.LightBlue
                TRHead_1_06.BackColor = Drawing.Color.LightBlue
                TRHead_1_07.BackColor = Drawing.Color.LightBlue
                TRHead_1_08.BackColor = Drawing.Color.LightBlue
                TRHead_1_09.BackColor = Drawing.Color.LightBlue
                TRHead_1_10.BackColor = Drawing.Color.LightBlue
                TRHead_1_11.BackColor = Drawing.Color.LightBlue
                TRHead_1_12.BackColor = Drawing.Color.LightBlue
                TRHead_1_13.BackColor = Drawing.Color.LightBlue
                TRHead_1_14.BackColor = Drawing.Color.LightBlue
                TRHead_1_15.BackColor = Drawing.Color.LightBlue
                TRHead_1_16.BackColor = Drawing.Color.LightBlue
                TRHead_1_17.BackColor = Drawing.Color.LightBlue
                TRHead_1_18.BackColor = Drawing.Color.LightBlue
                TRHead_1_19.BackColor = Drawing.Color.LightBlue
                TRHead_1_20.BackColor = Drawing.Color.LightBlue
                TRHead_1_21.BackColor = Drawing.Color.LightBlue
                TRHead_1_22.BackColor = Drawing.Color.LightBlue
                TRHead_1_23.BackColor = Drawing.Color.LightBlue
                TRHead_1_24.BackColor = Drawing.Color.LightBlue
                TRHead_1_25.BackColor = Drawing.Color.LightBlue
                TRHead_1_26.BackColor = Drawing.Color.LightBlue
                TRHead_1_27.BackColor = Drawing.Color.LightBlue
                TRHead_1_28.BackColor = Drawing.Color.LightBlue
                TRHead_1_29.BackColor = Drawing.Color.LightBlue
                TRHead_1_30.BackColor = Drawing.Color.LightBlue
                TRHead_1_31.BackColor = Drawing.Color.LightBlue
                TRHead_1_32.BackColor = Drawing.Color.LightBlue
                TRHead_1_33.BackColor = Drawing.Color.LightBlue
                TRHead_1_34.BackColor = Drawing.Color.LightBlue
                TRHead_1_35.BackColor = Drawing.Color.LightBlue
                TRHead_1_36.BackColor = Drawing.Color.LightBlue
                TRHead_1_37.BackColor = Drawing.Color.LightBlue


                RH.AddColumn(TRHead_1, TRHead_1_00, 3, 3, "c", "Sl No")
                RH.AddColumn(TRHead_1, TRHead_1_01, 6, 6, "c", DT3.Rows(0)("tmrNo"))
                RH.AddColumn(TRHead_1, TRHead_1_02, 6, 6, "c", DT3.Rows(0)("checker"))
                RH.AddColumn(TRHead_1, TRHead_1_03, 8, 8, "c", DT3.Rows(0)("criticalityName"))
                RH.AddColumn(TRHead_1, TRHead_1_04, 8, 8, "c", DT3.Rows(0)("issueNoted"))
                RH.AddColumn(TRHead_1, TRHead_1_05, 8, 8, "c", DT3.Rows(0)("noOfAccountsFrozen"))
                RH.AddColumn(TRHead_1, TRHead_1_06, 8, 8, "c", DT3.Rows(0)("accountNo"))
                RH.AddColumn(TRHead_1, TRHead_1_07, 8, 8, "c", DT3.Rows(0)("fpr"))
                RH.AddColumn(TRHead_1, TRHead_1_08, 9, 9, "c", DT3.Rows(0)("verticalName"))
                RH.AddColumn(TRHead_1, TRHead_1_09, 9, 9, "c", DT3.Rows(0)("subverticalName"))
                RH.AddColumn(TRHead_1, TRHead_1_10, 9, 9, "c", DT3.Rows(0)("creditChannel"))
                RH.AddColumn(TRHead_1, TRHead_1_11, 9, 9, "c", DT3.Rows(0)("debitChannel"))
                RH.AddColumn(TRHead_1, TRHead_1_12, 9, 9, "c", DT3.Rows(0)("subVerticalIdTwoName"))
                RH.AddColumn(TRHead_1, TRHead_1_13, 6, 6, "c", DT3.Rows(0)("bucketName"))
                RH.AddColumn(TRHead_1, TRHead_1_14, 6, 6, "c", DT3.Rows(0)("bucketIdFinalName"))
                RH.AddColumn(TRHead_1, TRHead_1_15, 8, 8, "c", DT3.Rows(0)("subBucketName"))
                RH.AddColumn(TRHead_1, TRHead_1_16, 8, 8, "c", DT3.Rows(0)("bucketIdThreeName"))
                RH.AddColumn(TRHead_1, TRHead_1_17, 8, 8, "c", DT3.Rows(0)("branchName"))
                RH.AddColumn(TRHead_1, TRHead_1_18, 8, 8, "c", DT3.Rows(0)("clusterName"))
                RH.AddColumn(TRHead_1, TRHead_1_19, 8, 8, "c", DT3.Rows(0)("receivedDate"))
                RH.AddColumn(TRHead_1, TRHead_1_20, 9, 9, "c", DT3.Rows(0)("createdMonth"))
                RH.AddColumn(TRHead_1, TRHead_1_21, 9, 9, "c", DT3.Rows(0)("allocationDate"))
                RH.AddColumn(TRHead_1, TRHead_1_22, 9, 9, "c", DT3.Rows(0)("sentToBranchDate"))
                RH.AddColumn(TRHead_1, TRHead_1_23, 9, 9, "c", DT3.Rows(0)("tat"))
                RH.AddColumn(TRHead_1, TRHead_1_24, 9, 9, "c", DT3.Rows(0)("targetDate"))
                RH.AddColumn(TRHead_1, TRHead_1_25, 8, 8, "c", DT3.Rows(0)("actualClosedDate"))
                RH.AddColumn(TRHead_1, TRHead_1_26, 9, 9, "c", DT3.Rows(0)("strObservation"))
                RH.AddColumn(TRHead_1, TRHead_1_27, 9, 9, "c", DT3.Rows(0)("noOfAccounts"))
                RH.AddColumn(TRHead_1, TRHead_1_28, 9, 9, "c", DT3.Rows(0)("investigationResult"))
                RH.AddColumn(TRHead_1, TRHead_1_29, 9, 9, "c", DT3.Rows(0)("remarks"))
                RH.AddColumn(TRHead_1, TRHead_1_30, 9, 9, "c", DT3.Rows(0)("comments"))
                RH.AddColumn(TRHead_1, TRHead_1_31, 9, 9, "c", DT3.Rows(0)("ageing"))
                RH.AddColumn(TRHead_1, TRHead_1_32, 9, 9, "c", DT3.Rows(0)("reason"))
                RH.AddColumn(TRHead_1, TRHead_1_33, 9, 9, "c", DT3.Rows(0)("status"))
                RH.AddColumn(TRHead_1, TRHead_1_34, 9, 9, "c", DT3.Rows(0)("pendingReason"))
                RH.AddColumn(TRHead_1, TRHead_1_35, 9, 9, "c", DT3.Rows(0)("departmentName"))
                RH.AddColumn(TRHead_1, TRHead_1_36, 9, 9, "c", DT3.Rows(0)("closedDate"))
                RH.AddColumn(TRHead_1, TRHead_1_37, 9, 9, "c", DT3.Rows(0)("totalDays"))


                tb.Controls.Add(TRHead_1)
                Dim i As Integer = 0
                Dim DT1 As New DataTable
                DT1 = DT
                For Each DR In DT1.Rows

                    i = i + 1
                        Dim TR3 As New TableRow
                        TR3.BorderWidth = "1"
                        TR3.BorderStyle = BorderStyle.Solid
                        Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16, TR3_17, TR3_18, TR3_19, TR3_20, TR3_21, TR3_22, TR3_23, TR3_24, TR3_25, TR3_26, TR3_27, TR3_28, TR3_29, TR3_30, TR3_31, TR3_32, TR3_33, TR3_34, TR3_35, TR3_36, TR3_37 As New TableCell

                        TR3_00.BorderWidth = "1"
                        TR3_01.BorderWidth = "1"
                        TR3_02.BorderWidth = "1"
                        TR3_03.BorderWidth = "1"
                        TR3_04.BorderWidth = "1"
                        TR3_05.BorderWidth = "1"
                        TR3_06.BorderWidth = "1"
                        TR3_07.BorderWidth = "1"
                        TR3_08.BorderWidth = "1"
                        TR3_09.BorderWidth = "1"
                        TR3_10.BorderWidth = "1"
                        TR3_11.BorderWidth = "1"
                        TR3_12.BorderWidth = "1"
                        TR3_13.BorderWidth = "1"
                        TR3_14.BorderWidth = "1"
                        TR3_15.BorderWidth = "1"
                        TR3_16.BorderWidth = "1"
                        TR3_17.BorderWidth = "1"
                        TR3_18.BorderWidth = "1"
                        TR3_19.BorderWidth = "1"
                        TR3_20.BorderWidth = "1"
                        TR3_21.BorderWidth = "1"
                        TR3_22.BorderWidth = "1"
                        TR3_23.BorderWidth = "1"
                        TR3_24.BorderWidth = "1"
                        TR3_25.BorderWidth = "1"
                        TR3_26.BorderWidth = "1"
                        TR3_27.BorderWidth = "1"
                        TR3_28.BorderWidth = "1"
                        TR3_29.BorderWidth = "1"
                        TR3_30.BorderWidth = "1"
                        TR3_31.BorderWidth = "1"
                        TR3_32.BorderWidth = "1"
                        TR3_33.BorderWidth = "1"
                        TR3_34.BorderWidth = "1"
                        TR3_35.BorderWidth = "1"
                        TR3_36.BorderWidth = "1"
                        TR3_37.BorderWidth = "1"

                        TR3_00.BorderColor = Drawing.Color.Silver
                        TR3_01.BorderColor = Drawing.Color.Silver
                        TR3_02.BorderColor = Drawing.Color.Silver
                        TR3_03.BorderColor = Drawing.Color.Silver
                        TR3_04.BorderColor = Drawing.Color.Silver
                        TR3_05.BorderColor = Drawing.Color.Silver
                        TR3_06.BorderColor = Drawing.Color.Silver
                        TR3_07.BorderColor = Drawing.Color.Silver
                        TR3_08.BorderColor = Drawing.Color.Silver
                        TR3_09.BorderColor = Drawing.Color.Silver
                        TR3_10.BorderColor = Drawing.Color.Silver
                        TR3_11.BorderColor = Drawing.Color.Silver
                        TR3_12.BorderColor = Drawing.Color.Silver
                        TR3_13.BorderColor = Drawing.Color.Silver
                        TR3_14.BorderColor = Drawing.Color.Silver
                        TR3_15.BorderColor = Drawing.Color.Silver
                        TR3_16.BorderColor = Drawing.Color.Silver
                        TR3_17.BorderColor = Drawing.Color.Silver
                        TR3_18.BorderColor = Drawing.Color.Silver
                        TR3_19.BorderColor = Drawing.Color.Silver
                        TR3_20.BorderColor = Drawing.Color.Silver
                        TR3_21.BorderColor = Drawing.Color.Silver
                        TR3_22.BorderColor = Drawing.Color.Silver
                        TR3_23.BorderColor = Drawing.Color.Silver
                        TR3_24.BorderColor = Drawing.Color.Silver
                        TR3_25.BorderColor = Drawing.Color.Silver
                        TR3_26.BorderColor = Drawing.Color.Silver
                        TR3_27.BorderColor = Drawing.Color.Silver
                        TR3_28.BorderColor = Drawing.Color.Silver
                        TR3_29.BorderColor = Drawing.Color.Silver
                        TR3_30.BorderColor = Drawing.Color.Silver
                        TR3_31.BorderColor = Drawing.Color.Silver
                        TR3_32.BorderColor = Drawing.Color.Silver
                        TR3_33.BorderColor = Drawing.Color.Silver
                        TR3_34.BorderColor = Drawing.Color.Silver
                        TR3_35.BorderColor = Drawing.Color.Silver
                        TR3_36.BorderColor = Drawing.Color.Silver
                        TR3_37.BorderColor = Drawing.Color.Silver

                        TR3_00.BorderStyle = BorderStyle.Solid
                        TR3_01.BorderStyle = BorderStyle.Solid
                        TR3_02.BorderStyle = BorderStyle.Solid
                        TR3_03.BorderStyle = BorderStyle.Solid
                        TR3_04.BorderStyle = BorderStyle.Solid
                        TR3_05.BorderStyle = BorderStyle.Solid
                        TR3_06.BorderStyle = BorderStyle.Solid
                        TR3_07.BorderStyle = BorderStyle.Solid
                        TR3_08.BorderStyle = BorderStyle.Solid
                        TR3_09.BorderStyle = BorderStyle.Solid
                        TR3_10.BorderStyle = BorderStyle.Solid
                        TR3_11.BorderStyle = BorderStyle.Solid
                        TR3_12.BorderStyle = BorderStyle.Solid
                        TR3_13.BorderStyle = BorderStyle.Solid
                        TR3_14.BorderStyle = BorderStyle.Solid
                        TR3_15.BorderStyle = BorderStyle.Solid
                        TR3_16.BorderStyle = BorderStyle.Solid
                        TR3_17.BorderStyle = BorderStyle.Solid
                        TR3_18.BorderStyle = BorderStyle.Solid
                        TR3_19.BorderStyle = BorderStyle.Solid
                        TR3_20.BorderStyle = BorderStyle.Solid
                        TR3_21.BorderStyle = BorderStyle.Solid
                        TR3_22.BorderStyle = BorderStyle.Solid
                        TR3_23.BorderStyle = BorderStyle.Solid
                        TR3_24.BorderStyle = BorderStyle.Solid
                        TR3_25.BorderStyle = BorderStyle.Solid
                        TR3_26.BorderStyle = BorderStyle.Solid
                        TR3_27.BorderStyle = BorderStyle.Solid
                        TR3_28.BorderStyle = BorderStyle.Solid
                        TR3_29.BorderStyle = BorderStyle.Solid
                        TR3_30.BorderStyle = BorderStyle.Solid
                        TR3_31.BorderStyle = BorderStyle.Solid
                        TR3_32.BorderStyle = BorderStyle.Solid
                        TR3_33.BorderStyle = BorderStyle.Solid
                        TR3_34.BorderStyle = BorderStyle.Solid
                        TR3_35.BorderStyle = BorderStyle.Solid
                        TR3_36.BorderStyle = BorderStyle.Solid
                        TR3_37.BorderStyle = BorderStyle.Solid

                        TR3_00.BackColor = Drawing.Color.LightYellow
                        TR3_01.BackColor = Drawing.Color.LightYellow
                        TR3_02.BackColor = Drawing.Color.LightYellow
                        TR3_03.BackColor = Drawing.Color.LightYellow
                        TR3_04.BackColor = Drawing.Color.LightYellow
                        TR3_05.BackColor = Drawing.Color.LightYellow
                        TR3_06.BackColor = Drawing.Color.LightYellow
                        TR3_07.BackColor = Drawing.Color.LightYellow
                        TR3_08.BackColor = Drawing.Color.LightYellow
                        TR3_09.BackColor = Drawing.Color.LightYellow
                        TR3_10.BackColor = Drawing.Color.LightYellow
                        TR3_11.BackColor = Drawing.Color.LightYellow
                        TR3_12.BackColor = Drawing.Color.LightYellow
                        TR3_13.BackColor = Drawing.Color.LightYellow
                        TR3_14.BackColor = Drawing.Color.LightYellow
                        TR3_15.BackColor = Drawing.Color.LightYellow
                        TR3_16.BackColor = Drawing.Color.LightYellow
                        TR3_17.BackColor = Drawing.Color.LightYellow
                        TR3_18.BackColor = Drawing.Color.LightYellow
                        TR3_19.BackColor = Drawing.Color.LightYellow
                        TR3_20.BackColor = Drawing.Color.LightYellow
                        TR3_21.BackColor = Drawing.Color.LightYellow
                        TR3_22.BackColor = Drawing.Color.LightYellow
                        TR3_23.BackColor = Drawing.Color.LightYellow
                        TR3_24.BackColor = Drawing.Color.LightYellow
                        TR3_25.BackColor = Drawing.Color.LightYellow
                        TR3_26.BackColor = Drawing.Color.LightYellow
                        TR3_27.BackColor = Drawing.Color.LightYellow
                        TR3_28.BackColor = Drawing.Color.LightYellow
                        TR3_29.BackColor = Drawing.Color.LightYellow
                        TR3_30.BackColor = Drawing.Color.LightYellow
                        TR3_31.BackColor = Drawing.Color.LightYellow
                        TR3_32.BackColor = Drawing.Color.LightYellow
                        TR3_33.BackColor = Drawing.Color.LightYellow
                        TR3_34.BackColor = Drawing.Color.LightYellow
                        TR3_35.BackColor = Drawing.Color.LightYellow
                        TR3_36.BackColor = Drawing.Color.LightYellow
                        TR3_37.BackColor = Drawing.Color.LightYellow

                        RH.AddColumn(TR3, TR3_00, 3, 3, "c", i.ToString())
                        ' RH.AddColumn(TR3, TR3_01, 6, 6, "c", CDate(DR(0)).ToString("dd/MMM/yyyy"))
                        ' RH.AddColumn(TR3, TR3_02, 6, 6, "c", DR(1).ToString())
                        RH.AddColumn(TR3, TR3_01, 6, 6, "c", DR("tmrNo").ToString)
                        RH.AddColumn(TR3, TR3_02, 6, 6, "c", DR("checker").ToString)
                        RH.AddColumn(TR3, TR3_03, 8, 8, "c", DR("criticalityName").ToString)
                        RH.AddColumn(TR3, TR3_04, 8, 8, "c", DR("issueNoted").ToString)
                        RH.AddColumn(TR3, TR3_05, 8, 8, "c", DR("noOfAccountsFrozen").ToString)
                        RH.AddColumn(TR3, TR3_06, 8, 8, "c", DR("accountNo").ToString)
                        RH.AddColumn(TR3, TR3_07, 8, 8, "c", IIf(IsDBNull(DR("fpr")), 0, DR("fpr")))
                        RH.AddColumn(TR3, TR3_08, 9, 9, "c", DR("verticalName").ToString)
                        RH.AddColumn(TR3, TR3_09, 9, 9, "c", DR("subverticalName").ToString)
                        RH.AddColumn(TR3, TR3_10, 9, 9, "c", DR("creditChannel").ToString)
                        RH.AddColumn(TR3, TR3_11, 9, 9, "c", DR("debitChannel").ToString)
                        RH.AddColumn(TR3, TR3_12, 9, 9, "c", DR("subVerticalIdTwoName").ToString)
                        RH.AddColumn(TR3, TR3_13, 6, 6, "c", DR("bucketName").ToString)
                        RH.AddColumn(TR3, TR3_14, 6, 6, "c", DR("bucketIdFinalName").ToString)
                        RH.AddColumn(TR3, TR3_15, 8, 8, "c", DR("subBucketName").ToString)
                        RH.AddColumn(TR3, TR3_16, 8, 8, "c", DR("bucketIdThreeName").ToString)
                        RH.AddColumn(TR3, TR3_17, 8, 8, "c", DR("branchName").ToString)
                        RH.AddColumn(TR3, TR3_18, 8, 8, "c", DR("clusterName").ToString)
                        RH.AddColumn(TR3, TR3_19, 8, 8, "c", DR("receivedDate").ToString)
                        RH.AddColumn(TR3, TR3_20, 9, 9, "c", DR("createdMonth").ToString)
                        RH.AddColumn(TR3, TR3_21, 9, 9, "c", DR("allocationDate").ToString)
                        RH.AddColumn(TR3, TR3_22, 9, 9, "c", DR("sentToBranchDate").ToString)
                        RH.AddColumn(TR3, TR3_23, 9, 9, "c", DR("tat").ToString)
                        RH.AddColumn(TR3, TR3_24, 9, 9, "c", DR("targetDate").ToString)
                        RH.AddColumn(TR3, TR3_25, 8, 8, "c", DR("actualClosedDate").ToString)
                        RH.AddColumn(TR3, TR3_26, 9, 9, "c", DR("strObservation").ToString)
                        RH.AddColumn(TR3, TR3_27, 9, 9, "c", DR("noOfAccounts").ToString)
                        RH.AddColumn(TR3, TR3_28, 9, 9, "c", DR("investigationResult").ToString)
                        RH.AddColumn(TR3, TR3_29, 9, 9, "c", DR("remarks").ToString)
                        RH.AddColumn(TR3, TR3_30, 9, 9, "c", DR("comments").ToString)
                        RH.AddColumn(TR3, TR3_31, 9, 9, "c", DR("ageing").ToString)
                        RH.AddColumn(TR3, TR3_32, 9, 9, "c", DR("reason").ToString)
                        RH.AddColumn(TR3, TR3_33, 9, 9, "c", DR("status").ToString)
                        RH.AddColumn(TR3, TR3_34, 9, 9, "c", DR("pendingReason").ToString)
                        RH.AddColumn(TR3, TR3_35, 9, 9, "c", DR("departmentName").ToString)
                        RH.AddColumn(TR3, TR3_36, 9, 9, "c", DR("closedDate").ToString)
                        RH.AddColumn(TR3, TR3_37, 9, 9, "c", DR("totalDays").ToString)

                        'If CInt(DR(2)) = 0 Then
                        '    RH.AddColumn(TR3, TR3_03, 8, 8, "c", DR(2).ToString())
                        'Else
                        '    RH.AddColumn(TR3, TR3_03, 8, 8, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=0' style='text-align:right;' target='_blank'>" + DR(2).ToString() + "</a>")
                        'End If
                        'If CInt(DR(3)) = 0 Then
                        '    RH.AddColumn(TR3, TR3_04, 8, 8, "c", DR(3).ToString())
                        'Else
                        '    RH.AddColumn(TR3, TR3_04, 8, 8, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=1' style='text-align:right;' target='_blank'>" + DR(3).ToString() + "</a>")
                        'End If
                        'If CInt(DR(4)) = 0 Then
                        '    RH.AddColumn(TR3, TR3_05, 8, 8, "c", DR(4).ToString())
                        'Else
                        '    RH.AddColumn(TR3, TR3_05, 8, 8, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=2' style='text-align:right;' target='_blank'>" + DR(4).ToString() + "</a>")
                        'End If
                        'If CInt(DR(5)) = 0 Then
                        '    RH.AddColumn(TR3, TR3_06, 8, 8, "c", DR(5).ToString())
                        'Else
                        '    RH.AddColumn(TR3, TR3_06, 8, 8, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=10' style='text-align:right;' target='_blank'>" + DR(5).ToString() + "</a>")
                        'End If
                        'If CInt(DR(6)) = 0 Then
                        '    RH.AddColumn(TR3, TR3_07, 8, 8, "c", DR(6).ToString())
                        'Else
                        '    RH.AddColumn(TR3, TR3_07, 8, 8, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=3' style='text-align:right;' target='_blank'>" + DR(6).ToString() + "</a>")
                        'End If
                        'If CInt(DR(7)) = 0 Then
                        '    RH.AddColumn(TR3, TR3_08, 9, 9, "c", DR(7).ToString())
                        'Else
                        '    RH.AddColumn(TR3, TR3_08, 9, 9, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=4' style='text-align:right;' target='_blank'>" + DR(7).ToString() + "</a>")
                        'End If
                        'If CInt(DR(8)) = 0 Then
                        '    RH.AddColumn(TR3, TR3_09, 9, 9, "c", DR(8).ToString())
                        'Else
                        '    RH.AddColumn(TR3, TR3_09, 9, 9, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=5' style='text-align:right;' target='_blank'>" + DR(8).ToString() + "</a>")
                        'End If
                        'If CInt(DR(9)) = 0 Then
                        '    RH.AddColumn(TR3, TR3_10, 9, 9, "c", DR(9).ToString())
                        'Else
                        '    RH.AddColumn(TR3, TR3_10, 9, 9, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=11' style='text-align:right;' target='_blank'>" + DR(9).ToString() + "</a>")
                        'End If
                        'If CInt(DR(10)) = 0 Then
                        '    RH.AddColumn(TR3, TR3_11, 9, 9, "c", DR(10).ToString())
                        'Else
                        '    RH.AddColumn(TR3, TR3_11, 9, 9, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=12' style='text-align:right;' target='_blank'>" + DR(10).ToString() + "</a>")
                        'End If
                        'If CInt(DR(11)) = 0 Then
                        '    RH.AddColumn(TR3, TR3_12, 9, 9, "c", DR(11).ToString())
                        'Else
                        '    RH.AddColumn(TR3, TR3_12, 9, 9, "c", "<a href='ViewDashboardDtls.aspx?frmdate=" + GF.Encrypt(DR(0)) + "&StatusID=13' style='text-align:right;' target='_blank'>" + DR(11).ToString() + "</a>")
                        'End If

                        tb.Controls.Add(TR3)

                Next
                RH.BlankRow(tb, 30)
                pnDisplay.Controls.Add(tb)
                'cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
                'cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Catch ex As Exception
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
            End Try
        End If

    End Sub


    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
        Dim aa As String
        aa = "fgfghfghgf"
        Response.Redirect("../Reports/TMR_Tracker.aspx")

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        Try
            Dim reportId As String = ""
            If CStr(Request.QueryString.Get("reportId")) <> "0" Then
                reportId = CStr(GF.Decrypt(Request.QueryString.Get("reportId")))
            End If
            Dim value As String = ""
            If CStr(Request.QueryString.Get("value")) <> "" Then
                value = CStr(GF.Decrypt(Request.QueryString.Get("value")))
            End If
            DT = GF.GeneralFunctionForTMR("TMRRPT003", reportId.ToString + "~" + value)

            WebTools.ExporttoExcel(DT, "TMR")

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
