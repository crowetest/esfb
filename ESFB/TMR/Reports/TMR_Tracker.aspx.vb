﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class TMR_Tracker
    Inherits System.Web.UI.Page
    ' Implements Web.UI.ICallbackEventHandler
    Dim DT, DTBr, DTTe, DTRo As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim StrValue As String
    Dim DR As DataRow
    Dim CallBackReturn As String = Nothing
    Dim Dep_Id As Integer
    Dim Entity As Integer

#Region "Page Load & Dispose"
    Protected Sub TMR_Tracker_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub TMR_Tracker_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "TMR TRACKER"
        If Not IsPostBack Then
            DT = GF.GeneralFunctionForTMR("TMRRPT001", "0")
            GF.ComboFill(ddlReportType, DT, 0, 1)
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub ddlReportType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReportType.SelectedIndexChanged
        hfReportPanel.Value = ""
        Dim reportId As Integer = 0
        Dim reportName As String = ""
        Dim reportPanel As String = ""
        Dim reportFields As String = ""
        Dim parametersCount As Integer = 0
        DT = GF.GeneralFunctionForTMR("TMRRPT001", ddlReportType.SelectedValue.ToString)
        reportId = CInt(DT.Rows(0)("reportId"))
        reportName = DT.Rows(0)("reportName").ToString
        reportPanel = DT.Rows(0)("reportPanel").ToString
        reportFields = DT.Rows(0)("reportFields").ToString
        parametersCount = CInt(DT.Rows(0)("parametersCount"))
        If reportPanel = "phTextBox" Then
            phTextBox.Visible = True
            phDropDown.Visible = False
            phDate.Visible = False
            lblTextBox.Text = reportFields
        ElseIf reportPanel = "phDropDown" Then
            phDropDown.Visible = True
            phTextBox.Visible = False
            phDate.Visible = False
            lbldropdown.Text = reportFields
            DT = GF.GeneralFunctionForTMR("TMRRPT002", reportId.ToString)
            GF.ComboFill(ddldropdown, DT, 0, 1)
        Else
            phDate.Visible = True
            phDropDown.Visible = False
            phTextBox.Visible = False
        End If
        hfReportPanel.Value = reportPanel
    End Sub

    Protected Sub btnShow_Click(sender As Object, e As EventArgs) Handles btnShow.Click

        Dim sddsf As String = txtStartDt.Text
        Dim value As String = ""
        If hfReportPanel.Value = "phTextBox" Then
            value = txtTextbox.Text
        ElseIf hfReportPanel.Value = "phDropDown" Then
            value = ddldropdown.SelectedValue.ToString
        Else
            value = txtStartDt.Text.ToString + "^" + txtToDt.Text.ToString

        End If
        Response.Redirect("TMR_Tracker_Report.aspx?reportId=" + GF.Encrypt(ddlReportType.SelectedValue.ToString) + "&value=" + GF.Encrypt(value))

    End Sub

    Protected Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Response.Redirect("~/Home.aspx?ErrorNo=")
    End Sub

#End Region

End Class
