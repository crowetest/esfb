﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class TMR_Tracker_Report_Dtl
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb, tb2 As New Table
    Dim DT, DT2 As New DataTable
    Dim streams As IList(Of Stream)
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim StrFromDate As String = ""
            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(GF.Decrypt(Request.QueryString.Get("frmdate")))
            End If

            Dim StatusID As Integer = CInt(Request.QueryString.Get("StatusID"))

            Dim StrQuery As String = ""

            If CStr(Request.QueryString.Get("frmdate")) <> "" Then
                If StatusID = 0 Then
                    StrQuery += " where (Status_id is null or Status_id=0) and DATEADD(day,DATEDIFF(day, 0,NewCif_CreatedDt),0)= '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'"
                Else
                    StrQuery += " where Status_id=" & StatusID & " and DATEADD(day,DATEDIFF(day, 0,NewCif_CreatedDt),0)= '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'"
                End If

            End If

            Dim SqlStr As String = ""

            SqlStr = "select replace(convert(char(11),NewCif_CreatedDt,113),' ','-'),New_Cif,NewCif_Name,NewCif_brcode,replace(convert(char(11),NewCif_DOB,113),' ','-'),NewCif_Pin,NewCif_Mob,NewCif_Email,NewCif_Pan,NewCif_Dlicense,NewCif_Passport,NewCif_VoterID,NewCif_Aadhar,NewCif_Father,NewCif_Mother,NewCif_Address,replace(convert(char(11),DupeCif_CreatedDt,113),' ','-'),Dupe_Cif,DupeCif_Name,DupeCif_brcode,replace(convert(char(11),DupeCif_DOB,113),' ','-'),DupeCif_Pin,DupeCif_Mob,DupeCif_Email,DupeCif_Pan,DupeCif_Dlicense,DupeCif_Passport,DupeCif_VoterID,DupeCif_Aadhar,DupeCif_Father,DupeCif_Mother,DupeCif_Address,EqualStatus,MakerN,replace(convert(char(11),MakerNDt,113),' ','-'), case when MakerNFlg=1 then 'DEDUPE YES' when MakerNFlg=2 then 'DEDUPE NO' else '' end MNStat,CheckerN,replace(convert(char(11),CheckerNDt,113),' ','-'), case when CheckerNFlg=1 then 'DEDUPE YES' when CheckerNFlg=2 then 'DEDUPE NO' else '' end CNStat,MakerBY,replace(convert(char(11),MakerBYDt,113),' ','-'), case when MakerBYFlg=1 then 'DEDUPE YES' when MakerBYFlg=2 then 'DEDUPE NO' when MakerBYFlg=3 then 'RETURN TO BRANCH' else '' end MBYStat,CheckerBY,replace(convert(char(11),CheckerBYDt,113),' ','-'), case when CheckerBYFlg=1 then 'DEDUPE YES'  when CheckerBYFlg=2 then 'DEDUPE NO' when CheckerBYFlg=3 then 'RETURN TO CHECKER' else '' end CBYStat,CheckerBN,replace(convert(char(11),CheckerBNDt,113),' ','-'), case when CheckerBNFlg=1 then 'DEDUPE YES' when CheckerBNFlg=2 then 'DEDUPE NO' else '' end CBNStat,Branch,replace(convert(char(11),BranchDt,113),' ','-'), case when BranchFlg=1 then 'DEDUPE YES' when BranchFlg=2 then 'DEDUPE NO' else '' end BranchStat from dedupe_cust_verification" & StrQuery & ""

            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            If StatusID = 0 Then
                DT2 = DB.ExecuteDataSet("select 'NEW - DEDUPE PENDING AT MAKER LEVEL'").Tables(0)
            Else
                DT2 = DB.ExecuteDataSet("select status_name from Dedupe_Status_Master where status_id=" & StatusID & "").Tables(0)
            End If

            Dim b As Integer = DT.Rows.Count

            RH.Heading(Session("FirmName"), tb, DT2.Rows(0)(0), 150)

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "3"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.BackColor = Drawing.Color.Blue

            Dim TRHead_1 As New TableRow

            Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14, TRHead_1_15, TRHead_1_16, TRHead_1_17, TRHead_1_18, TRHead_1_19 As New TableCell

            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"
            TRHead_1_08.BorderWidth = "1"
            TRHead_1_09.BorderWidth = "1"
            TRHead_1_10.BorderWidth = "1"
            TRHead_1_11.BorderWidth = "1"
            TRHead_1_12.BorderWidth = "1"
            TRHead_1_13.BorderWidth = "1"
            TRHead_1_14.BorderWidth = "1"
            TRHead_1_15.BorderWidth = "1"
            TRHead_1_16.BorderWidth = "1"
            TRHead_1_17.BorderWidth = "1"
            TRHead_1_18.BorderWidth = "1"
            TRHead_1_19.BorderWidth = "1"

            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver
            TRHead_1_08.BorderColor = Drawing.Color.Silver
            TRHead_1_09.BorderColor = Drawing.Color.Silver
            TRHead_1_10.BorderColor = Drawing.Color.Silver
            TRHead_1_11.BorderColor = Drawing.Color.Silver
            TRHead_1_12.BorderColor = Drawing.Color.Silver
            TRHead_1_13.BorderColor = Drawing.Color.Silver
            TRHead_1_14.BorderColor = Drawing.Color.Silver
            TRHead_1_15.BorderColor = Drawing.Color.Silver
            TRHead_1_16.BorderColor = Drawing.Color.Silver
            TRHead_1_17.BorderColor = Drawing.Color.Silver
            TRHead_1_18.BorderColor = Drawing.Color.Silver
            TRHead_1_19.BorderColor = Drawing.Color.Silver

            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid
            TRHead_1_08.BorderStyle = BorderStyle.Solid
            TRHead_1_09.BorderStyle = BorderStyle.Solid
            TRHead_1_10.BorderStyle = BorderStyle.Solid
            TRHead_1_11.BorderStyle = BorderStyle.Solid
            TRHead_1_12.BorderStyle = BorderStyle.Solid
            TRHead_1_13.BorderStyle = BorderStyle.Solid
            TRHead_1_14.BorderStyle = BorderStyle.Solid
            TRHead_1_15.BorderStyle = BorderStyle.Solid
            TRHead_1_16.BorderStyle = BorderStyle.Solid
            TRHead_1_17.BorderStyle = BorderStyle.Solid
            TRHead_1_18.BorderStyle = BorderStyle.Solid
            TRHead_1_19.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead_1, TRHead_1_00, 2, 2, "c", "Sl No")
            RH.AddColumn(TRHead_1, TRHead_1_01, 26, 26, "c", "CustomerDetails")
            RH.AddColumn(TRHead_1, TRHead_1_02, 4, 4, "c", "MakerNew")
            RH.AddColumn(TRHead_1, TRHead_1_03, 4, 4, "c", "MakerNewDate")
            RH.AddColumn(TRHead_1, TRHead_1_04, 4, 4, "c", "MakerNewStatus")
            RH.AddColumn(TRHead_1, TRHead_1_05, 4, 4, "c", "CheckerNew")
            RH.AddColumn(TRHead_1, TRHead_1_06, 4, 4, "c", "CheckerNewDate")
            RH.AddColumn(TRHead_1, TRHead_1_07, 4, 4, "c", "CheckerNewStatus")
            RH.AddColumn(TRHead_1, TRHead_1_08, 4, 4, "c", "MakerBranchYes")
            RH.AddColumn(TRHead_1, TRHead_1_09, 4, 4, "c", "MakerBranchYesDate")
            RH.AddColumn(TRHead_1, TRHead_1_10, 4, 4, "c", "MakerBranchYesStatus")
            RH.AddColumn(TRHead_1, TRHead_1_11, 4, 4, "c", "CheckerBranchYes")
            RH.AddColumn(TRHead_1, TRHead_1_12, 4, 4, "c", "CheckerBranchYesDate")
            RH.AddColumn(TRHead_1, TRHead_1_13, 4, 4, "c", "CheckerBranchYesStatus")
            RH.AddColumn(TRHead_1, TRHead_1_14, 4, 4, "c", "CheckerBranchNo")
            RH.AddColumn(TRHead_1, TRHead_1_15, 4, 4, "c", "CheckerBranchNoDate")
            RH.AddColumn(TRHead_1, TRHead_1_16, 4, 4, "c", "CheckerBranchNoStatus")
            RH.AddColumn(TRHead_1, TRHead_1_17, 4, 4, "c", "BranchDoneBy")
            RH.AddColumn(TRHead_1, TRHead_1_18, 4, 4, "c", "BranchDoneDate")
            RH.AddColumn(TRHead_1, TRHead_1_19, 4, 4, "c", "BranchDoneSatus")


            tb.Controls.Add(TRHead_1)
            Dim i As Integer = 0
            For Each DR In DT.Rows
                i = i + 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                TR3.BackColor = Drawing.Color.LightBlue
                Dim TR3_00, TR33_00, TR33_01, TR33_02, TR33_03, TR33_04, TR33_05, TR33_06, TR33_07, TR33_08, TR33_09, TR33_10, TR33_11, TR33_12, TR33_13, TR33_14, TR33_15, TR33_16, TR33_17, TR33_18, TR33_19, TR33_20, TR33_21, TR33_22, TR33_23, TR33_24, TR33_25, TR33_26, TR33_27, TR33_28, TR33_29, TR33_30, TR33_31, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16, TR3_17, TR3_18, TR3_19 As New TableCell

                TR3_00.BorderWidth = "1"
                TR33_00.BorderWidth = "1"
                TR33_01.BorderWidth = "1"
                TR33_02.BorderWidth = "1"
                TR33_03.BorderWidth = "1"
                TR33_04.BorderWidth = "1"
                TR33_05.BorderWidth = "1"
                TR33_06.BorderWidth = "1"
                TR33_07.BorderWidth = "1"
                TR33_08.BorderWidth = "1"
                TR33_09.BorderWidth = "1"
                TR33_10.BorderWidth = "1"
                TR33_11.BorderWidth = "1"
                TR33_12.BorderWidth = "1"
                TR33_13.BorderWidth = "1"
                TR33_14.BorderWidth = "1"
                TR33_15.BorderWidth = "1"
                TR33_16.BorderWidth = "1"
                TR33_17.BorderWidth = "1"
                TR33_18.BorderWidth = "1"
                TR33_19.BorderWidth = "1"
                TR33_20.BorderWidth = "1"
                TR33_21.BorderWidth = "1"
                TR33_22.BorderWidth = "1"
                TR33_23.BorderWidth = "1"
                TR33_24.BorderWidth = "1"
                TR33_25.BorderWidth = "1"
                TR33_26.BorderWidth = "1"
                TR33_27.BorderWidth = "1"
                TR33_28.BorderWidth = "1"
                TR33_29.BorderWidth = "1"
                TR33_30.BorderWidth = "1"
                TR33_31.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"
                TR3_15.BorderWidth = "1"
                TR3_16.BorderWidth = "1"
                TR3_17.BorderWidth = "1"
                TR3_18.BorderWidth = "1"
                TR3_19.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR33_00.BorderColor = Drawing.Color.Silver
                TR33_01.BorderColor = Drawing.Color.Silver
                TR33_02.BorderColor = Drawing.Color.Silver
                TR33_03.BorderColor = Drawing.Color.Silver
                TR33_04.BorderColor = Drawing.Color.Silver
                TR33_05.BorderColor = Drawing.Color.Silver
                TR33_06.BorderColor = Drawing.Color.Silver
                TR33_07.BorderColor = Drawing.Color.Silver
                TR33_08.BorderColor = Drawing.Color.Silver
                TR33_09.BorderColor = Drawing.Color.Silver
                TR33_10.BorderColor = Drawing.Color.Silver
                TR33_11.BorderColor = Drawing.Color.Silver
                TR33_12.BorderColor = Drawing.Color.Silver
                TR33_13.BorderColor = Drawing.Color.Silver
                TR33_14.BorderColor = Drawing.Color.Silver
                TR33_15.BorderColor = Drawing.Color.Silver
                TR33_16.BorderColor = Drawing.Color.Silver
                TR33_17.BorderColor = Drawing.Color.Silver
                TR33_18.BorderColor = Drawing.Color.Silver
                TR33_19.BorderColor = Drawing.Color.Silver
                TR33_20.BorderColor = Drawing.Color.Silver
                TR33_21.BorderColor = Drawing.Color.Silver
                TR33_22.BorderColor = Drawing.Color.Silver
                TR33_23.BorderColor = Drawing.Color.Silver
                TR33_24.BorderColor = Drawing.Color.Silver
                TR33_25.BorderColor = Drawing.Color.Silver
                TR33_27.BorderColor = Drawing.Color.Silver
                TR33_28.BorderColor = Drawing.Color.Silver
                TR33_29.BorderColor = Drawing.Color.Silver
                TR33_30.BorderColor = Drawing.Color.Silver
                TR33_31.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver
                TR3_15.BorderColor = Drawing.Color.Silver
                TR3_16.BorderColor = Drawing.Color.Silver
                TR3_17.BorderColor = Drawing.Color.Silver
                TR3_18.BorderColor = Drawing.Color.Silver
                TR3_19.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR33_00.BorderStyle = BorderStyle.Solid
                TR33_01.BorderStyle = BorderStyle.Solid
                TR33_02.BorderStyle = BorderStyle.Solid
                TR33_03.BorderStyle = BorderStyle.Solid
                TR33_04.BorderStyle = BorderStyle.Solid
                TR33_05.BorderStyle = BorderStyle.Solid
                TR33_06.BorderStyle = BorderStyle.Solid
                TR33_07.BorderStyle = BorderStyle.Solid
                TR33_08.BorderStyle = BorderStyle.Solid
                TR33_09.BorderStyle = BorderStyle.Solid
                TR33_10.BorderStyle = BorderStyle.Solid
                TR33_11.BorderStyle = BorderStyle.Solid
                TR33_12.BorderStyle = BorderStyle.Solid
                TR33_13.BorderStyle = BorderStyle.Solid
                TR33_14.BorderStyle = BorderStyle.Solid
                TR33_15.BorderStyle = BorderStyle.Solid
                TR33_16.BorderStyle = BorderStyle.Solid
                TR33_17.BorderStyle = BorderStyle.Solid
                TR33_18.BorderStyle = BorderStyle.Solid
                TR33_19.BorderStyle = BorderStyle.Solid
                TR33_20.BorderStyle = BorderStyle.Solid
                TR33_21.BorderStyle = BorderStyle.Solid
                TR33_22.BorderStyle = BorderStyle.Solid
                TR33_23.BorderStyle = BorderStyle.Solid
                TR33_24.BorderStyle = BorderStyle.Solid
                TR33_25.BorderStyle = BorderStyle.Solid
                TR33_26.BorderStyle = BorderStyle.Solid
                TR33_27.BorderStyle = BorderStyle.Solid
                TR33_28.BorderStyle = BorderStyle.Solid
                TR33_29.BorderStyle = BorderStyle.Solid
                TR33_30.BorderStyle = BorderStyle.Solid
                TR33_31.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid
                TR3_15.BorderStyle = BorderStyle.Solid
                TR3_16.BorderStyle = BorderStyle.Solid
                TR3_17.BorderStyle = BorderStyle.Solid
                TR3_18.BorderStyle = BorderStyle.Solid
                TR3_19.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 2, 2, "c", i.ToString())
                RH.AddColumn(TR3, TR33_00, 1, 1, "c", "OpenDate")
                RH.AddColumn(TR3, TR33_01, 2, 2, "c", "CIF")
                RH.AddColumn(TR3, TR33_02, 2, 2, "c", "Name")
                RH.AddColumn(TR3, TR33_03, 2, 2, "c", "Branch")
                RH.AddColumn(TR3, TR33_04, 1, 1, "c", "DOB")
                RH.AddColumn(TR3, TR33_05, 1, 1, "c", "PIN")
                RH.AddColumn(TR3, TR33_06, 2, 2, "c", "Mobile")
                RH.AddColumn(TR3, TR33_07, 2, 2, "c", "Email")
                RH.AddColumn(TR3, TR33_08, 2, 2, "c", "PAN")
                RH.AddColumn(TR3, TR33_09, 1, 1, "c", "DL")
                RH.AddColumn(TR3, TR33_10, 1, 1, "c", "Passport")
                RH.AddColumn(TR3, TR33_11, 1, 1, "c", "VotersID")
                RH.AddColumn(TR3, TR33_12, 2, 2, "c", "Aadhar")
                RH.AddColumn(TR3, TR33_13, 1, 1, "c", "Father")
                RH.AddColumn(TR3, TR33_14, 1, 1, "c", "Mother")
                RH.AddColumn(TR3, TR33_15, 4, 4, "c", "Address")

                RH.AddColumn(TR3, TR3_02, 4, 4, "c", DR(33).ToString())
                RH.AddColumn(TR3, TR3_02, 4, 4, "c", DR(33).ToString())
                RH.AddColumn(TR3, TR3_03, 4, 4, "c", DR(34).ToString())
                RH.AddColumn(TR3, TR3_04, 4, 4, "c", DR(35).ToString())
                RH.AddColumn(TR3, TR3_05, 4, 4, "c", DR(36).ToString())
                RH.AddColumn(TR3, TR3_06, 4, 4, "c", DR(37).ToString())
                RH.AddColumn(TR3, TR3_07, 4, 4, "c", DR(38).ToString())
                RH.AddColumn(TR3, TR3_08, 4, 4, "c", DR(39).ToString())
                RH.AddColumn(TR3, TR3_09, 4, 4, "c", DR(40).ToString())
                If (StatusID = 3) Then
                    RH.AddColumn(TR3, TR3_10, 4, 4, "c", "")
                Else
                    RH.AddColumn(TR3, TR3_10, 4, 4, "c", DR(41).ToString())
                End If
                RH.AddColumn(TR3, TR3_11, 4, 4, "c", DR(42).ToString())
                RH.AddColumn(TR3, TR3_12, 4, 4, "c", DR(43).ToString())
                If (StatusID = 5) Then
                    RH.AddColumn(TR3, TR3_13, 4, 4, "c", "")
                Else
                    RH.AddColumn(TR3, TR3_13, 4, 4, "c", DR(44).ToString())
                End If
                RH.AddColumn(TR3, TR3_14, 4, 4, "c", DR(45).ToString())
                RH.AddColumn(TR3, TR3_15, 4, 4, "c", DR(46).ToString())
                If (StatusID = 4) Then
                    RH.AddColumn(TR3, TR3_16, 4, 4, "c", "")
                Else
                    RH.AddColumn(TR3, TR3_16, 4, 4, "c", DR(47).ToString())
                End If
                RH.AddColumn(TR3, TR3_17, 4, 4, "c", DR(48).ToString())
                RH.AddColumn(TR3, TR3_18, 4, 4, "c", DR(49).ToString())
                If (StatusID = 2) Then
                    RH.AddColumn(TR3, TR3_19, 5, 5, "c", "")
                Else
                    RH.AddColumn(TR3, TR3_19, 5, 5, "c", DR(50).ToString())
                End If
                TR3_00.RowSpan = 3
                TR3_02.RowSpan = 3
                TR3_03.RowSpan = 3
                TR3_04.RowSpan = 3
                TR3_05.RowSpan = 3
                TR3_06.RowSpan = 3
                TR3_07.RowSpan = 3
                TR3_08.RowSpan = 3
                TR3_09.RowSpan = 3
                TR3_10.RowSpan = 3
                TR3_11.RowSpan = 3
                TR3_12.RowSpan = 3
                TR3_13.RowSpan = 3
                TR3_14.RowSpan = 3
                TR3_15.RowSpan = 3
                TR3_16.RowSpan = 3
                TR3_17.RowSpan = 3
                TR3_18.RowSpan = 3
                TR3_19.RowSpan = 3

                tb.Controls.Add(TR3)

                Dim TR4 As New TableRow
                TR4.BorderWidth = "1"
                TR4.BackColor = Drawing.Color.White

                Dim TR44_00, TR44_01, TR44_02, TR44_03, TR44_04, TR44_05, TR44_06, TR44_07, TR44_08, TR44_09, TR44_10, TR44_11, TR44_12, TR44_13, TR44_14, TR44_15, TR44_16 As New TableCell

                TR44_00.BorderWidth = "1"
                TR44_01.BorderWidth = "1"
                TR44_02.BorderWidth = "1"
                TR44_03.BorderWidth = "1"
                TR44_04.BorderWidth = "1"
                TR44_05.BorderWidth = "1"
                TR44_06.BorderWidth = "1"
                TR44_07.BorderWidth = "1"
                TR44_08.BorderWidth = "1"
                TR44_09.BorderWidth = "1"
                TR44_10.BorderWidth = "1"
                TR44_11.BorderWidth = "1"
                TR44_12.BorderWidth = "1"
                TR44_13.BorderWidth = "1"
                TR44_14.BorderWidth = "1"
                TR44_15.BorderWidth = "1"
                TR44_16.BorderWidth = "1"


                TR44_00.BorderColor = Drawing.Color.Silver
                TR44_01.BorderColor = Drawing.Color.Silver
                TR44_02.BorderColor = Drawing.Color.Silver
                TR44_03.BorderColor = Drawing.Color.Silver
                TR44_04.BorderColor = Drawing.Color.Silver
                TR44_05.BorderColor = Drawing.Color.Silver
                TR44_06.BorderColor = Drawing.Color.Silver
                TR44_07.BorderColor = Drawing.Color.Silver
                TR44_08.BorderColor = Drawing.Color.Silver
                TR44_09.BorderColor = Drawing.Color.Silver
                TR44_10.BorderColor = Drawing.Color.Silver
                TR44_11.BorderColor = Drawing.Color.Silver
                TR44_12.BorderColor = Drawing.Color.Silver
                TR44_13.BorderColor = Drawing.Color.Silver
                TR44_14.BorderColor = Drawing.Color.Silver
                TR44_15.BorderColor = Drawing.Color.Silver
                TR44_16.BorderColor = Drawing.Color.Silver


                TR44_00.BorderStyle = BorderStyle.Solid
                TR44_01.BorderStyle = BorderStyle.Solid
                TR44_02.BorderStyle = BorderStyle.Solid
                TR44_03.BorderStyle = BorderStyle.Solid
                TR44_04.BorderStyle = BorderStyle.Solid
                TR44_05.BorderStyle = BorderStyle.Solid
                TR44_06.BorderStyle = BorderStyle.Solid
                TR44_07.BorderStyle = BorderStyle.Solid
                TR44_08.BorderStyle = BorderStyle.Solid
                TR44_09.BorderStyle = BorderStyle.Solid
                TR44_10.BorderStyle = BorderStyle.Solid
                TR44_11.BorderStyle = BorderStyle.Solid
                TR44_12.BorderStyle = BorderStyle.Solid
                TR44_13.BorderStyle = BorderStyle.Solid
                TR44_14.BorderStyle = BorderStyle.Solid
                TR44_15.BorderStyle = BorderStyle.Solid
                TR44_16.BorderStyle = BorderStyle.Solid

                'Dim Data() As String = DR(32).Split(CChar(""))
                Dim index As Integer = 0
                Dim Data As New List(Of String)
                Dim StrNumber As String = DR(32).ToString
                While index < StrNumber.Length
                    Dim ans As Integer = CInt(Val(StrNumber(index)))
                    'Do whatever you want with ans. Here, I put it into a list.
                    Data.Add(ans)
                    index += 1
                End While

                RH.AddColumn(TR4, TR44_00, 1, 1, "c", DR(0).ToString())
                RH.AddColumn(TR4, TR44_01, 2, 2, "c", DR(1).ToString())
                RH.AddColumn(TR4, TR44_02, 2, 2, "c", DR(2).ToString())
                RH.AddColumn(TR4, TR44_03, 2, 2, "c", DR(3).ToString())
                If (CInt(Data(0)) = 1) Then
                    TR44_04.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR4, TR44_04, 1, 1, "c", DR(4).ToString())
                Else
                    TR44_04.BackColor = Drawing.Color.White
                    RH.AddColumn(TR4, TR44_04, 1, 1, "c", DR(4).ToString())
                End If
                If (CInt(Data(1)) = 1) Then
                    TR44_05.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR4, TR44_05, 1, 1, "c", DR(5).ToString())
                Else
                    TR44_05.BackColor = Drawing.Color.White
                    RH.AddColumn(TR4, TR44_05, 1, 1, "c", DR(5).ToString())
                End If
                If (CInt(Data(2)) = 1) Then
                    TR44_06.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR4, TR44_06, 2, 2, "c", DR(6).ToString())
                Else
                    TR44_06.BackColor = Drawing.Color.White
                    RH.AddColumn(TR4, TR44_06, 2, 2, "c", DR(6).ToString())
                End If
                If (CInt(Data(3)) = 1) Then
                    TR44_07.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR4, TR44_07, 2, 2, "c", DR(7).ToString())
                Else
                    TR44_07.BackColor = Drawing.Color.White
                    RH.AddColumn(TR4, TR44_07, 2, 2, "c", DR(7).ToString())
                End If
                If (CInt(Data(4)) = 1) Then
                    TR44_08.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR4, TR44_08, 2, 2, "c", DR(8).ToString())
                Else
                    TR44_08.BackColor = Drawing.Color.White
                    RH.AddColumn(TR4, TR44_08, 2, 2, "c", DR(8).ToString())
                End If
                If (CInt(Data(5)) = 1) Then
                    TR44_09.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR4, TR44_09, 1, 1, "c", DR(9).ToString())
                Else
                    TR44_09.BackColor = Drawing.Color.White
                    RH.AddColumn(TR4, TR44_09, 1, 1, "c", DR(9).ToString())
                End If
                If (CInt(Data(6)) = 1) Then
                    TR44_10.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR4, TR44_10, 1, 1, "c", DR(10).ToString())
                Else
                    TR44_10.BackColor = Drawing.Color.White
                    RH.AddColumn(TR4, TR44_10, 1, 1, "c", DR(10).ToString())
                End If
                RH.AddColumn(TR4, TR44_11, 1, 1, "c", DR(11).ToString())
                RH.AddColumn(TR4, TR44_12, 2, 2, "c", DR(12).ToString())
                RH.AddColumn(TR4, TR44_13, 1, 1, "c", DR(13).ToString())
                If (CInt(Data(7)) = 1) Then
                    TR44_14.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR4, TR44_14, 1, 1, "c", DR(14).ToString())
                Else
                    TR44_14.BackColor = Drawing.Color.White
                    RH.AddColumn(TR4, TR44_14, 1, 1, "c", DR(14).ToString())
                End If
                RH.AddColumn(TR4, TR44_15, 4, 4, "c", DR(15).ToString())

                tb.Controls.Add(TR4)
                Dim TR5 As New TableRow
                TR5.BorderWidth = "1"
                TR5.BackColor = Drawing.Color.White

                Dim TR55_00, TR55_01, TR55_02, TR55_03, TR55_04, TR55_05, TR55_06, TR55_07, TR55_08, TR55_09, TR55_10, TR55_11, TR55_12, TR55_13, TR55_14, TR55_15, TR55_16 As New TableCell

                TR55_00.BorderWidth = "1"
                TR55_01.BorderWidth = "1"
                TR55_02.BorderWidth = "1"
                TR55_03.BorderWidth = "1"
                TR55_04.BorderWidth = "1"
                TR55_05.BorderWidth = "1"
                TR55_06.BorderWidth = "1"
                TR55_07.BorderWidth = "1"
                TR55_08.BorderWidth = "1"
                TR55_09.BorderWidth = "1"
                TR55_10.BorderWidth = "1"
                TR55_11.BorderWidth = "1"
                TR55_12.BorderWidth = "1"
                TR55_13.BorderWidth = "1"
                TR55_14.BorderWidth = "1"
                TR55_15.BorderWidth = "1"
                TR55_16.BorderWidth = "1"

                TR55_00.BorderColor = Drawing.Color.Silver
                TR55_01.BorderColor = Drawing.Color.Silver
                TR55_02.BorderColor = Drawing.Color.Silver
                TR55_03.BorderColor = Drawing.Color.Silver
                TR55_04.BorderColor = Drawing.Color.Silver
                TR55_05.BorderColor = Drawing.Color.Silver
                TR55_06.BorderColor = Drawing.Color.Silver
                TR55_07.BorderColor = Drawing.Color.Silver
                TR55_08.BorderColor = Drawing.Color.Silver
                TR55_09.BorderColor = Drawing.Color.Silver
                TR55_10.BorderColor = Drawing.Color.Silver
                TR55_11.BorderColor = Drawing.Color.Silver
                TR55_12.BorderColor = Drawing.Color.Silver
                TR55_13.BorderColor = Drawing.Color.Silver
                TR55_14.BorderColor = Drawing.Color.Silver
                TR55_15.BorderColor = Drawing.Color.Silver

                TR55_00.BorderStyle = BorderStyle.Solid
                TR55_01.BorderStyle = BorderStyle.Solid
                TR55_02.BorderStyle = BorderStyle.Solid
                TR55_03.BorderStyle = BorderStyle.Solid
                TR55_04.BorderStyle = BorderStyle.Solid
                TR55_05.BorderStyle = BorderStyle.Solid
                TR55_06.BorderStyle = BorderStyle.Solid
                TR55_07.BorderStyle = BorderStyle.Solid
                TR55_08.BorderStyle = BorderStyle.Solid
                TR55_09.BorderStyle = BorderStyle.Solid
                TR55_10.BorderStyle = BorderStyle.Solid
                TR55_11.BorderStyle = BorderStyle.Solid
                TR55_12.BorderStyle = BorderStyle.Solid
                TR55_13.BorderStyle = BorderStyle.Solid
                TR55_14.BorderStyle = BorderStyle.Solid
                TR55_15.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR5, TR55_00, 1, 1, "c", DR(16).ToString())
                RH.AddColumn(TR5, TR55_01, 2, 2, "c", DR(17).ToString())
                RH.AddColumn(TR5, TR55_02, 2, 2, "c", DR(18).ToString())
                RH.AddColumn(TR5, TR55_03, 2, 2, "c", DR(19).ToString())
                If (CInt(Data(0)) = 1) Then
                    TR55_04.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR5, TR55_04, 1, 1, "c", DR(20).ToString())
                Else
                    TR55_04.BackColor = Drawing.Color.White
                    RH.AddColumn(TR5, TR55_04, 1, 1, "c", DR(20).ToString())
                End If
                If (CInt(Data(1)) = 1) Then
                    TR55_05.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR5, TR55_05, 1, 1, "c", DR(21).ToString())
                Else
                    TR55_05.BackColor = Drawing.Color.White
                    RH.AddColumn(TR5, TR55_05, 1, 1, "c", DR(21).ToString())
                End If
                If (CInt(Data(2)) = 1) Then
                    TR55_06.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR5, TR55_06, 2, 2, "c", DR(22).ToString())
                Else
                    TR55_06.BackColor = Drawing.Color.White
                    RH.AddColumn(TR5, TR55_06, 2, 2, "c", DR(22).ToString())
                End If
                If (CInt(Data(3)) = 1) Then
                    TR55_07.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR5, TR55_07, 2, 2, "c", DR(23).ToString())
                Else
                    TR55_07.BackColor = Drawing.Color.White
                    RH.AddColumn(TR5, TR55_07, 2, 2, "c", DR(23).ToString())
                End If
                If (CInt(Data(4)) = 1) Then
                    TR55_08.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR5, TR55_08, 2, 2, "c", DR(24).ToString())
                Else
                    TR55_08.BackColor = Drawing.Color.White
                    RH.AddColumn(TR5, TR55_08, 2, 2, "c", DR(24).ToString())
                End If
                If (CInt(Data(5)) = 1) Then
                    TR55_09.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR5, TR55_09, 1, 1, "c", DR(25).ToString())
                Else
                    TR55_09.BackColor = Drawing.Color.White
                    RH.AddColumn(TR5, TR55_09, 1, 1, "c", DR(25).ToString())
                End If
                If (CInt(Data(6)) = 1) Then
                    TR55_10.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR5, TR55_10, 1, 1, "c", DR(26).ToString())
                Else
                    TR55_10.BackColor = Drawing.Color.White
                    RH.AddColumn(TR5, TR55_10, 1, 1, "c", DR(26).ToString())
                End If
                RH.AddColumn(TR5, TR55_11, 1, 1, "c", DR(27).ToString())
                RH.AddColumn(TR5, TR55_12, 2, 2, "c", DR(28).ToString())
                RH.AddColumn(TR5, TR55_13, 1, 1, "c", DR(29).ToString())
                If (CInt(Data(7)) = 1) Then
                    TR55_14.BackColor = Drawing.Color.Red
                    RH.AddColumn(TR5, TR55_14, 1, 1, "c", DR(30).ToString())
                Else
                    TR55_14.BackColor = Drawing.Color.White
                    RH.AddColumn(TR5, TR55_14, 1, 1, "c", DR(30).ToString())
                End If
                RH.AddColumn(TR5, TR55_15, 4, 4, "c", DR(31).ToString())

                tb.Controls.Add(TR5)

            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        Try
            Dim StrFromDate As String = ""
            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(GF.Decrypt(Request.QueryString.Get("frmdate")))
            End If

            Dim StatusID As Integer = CInt(Request.QueryString.Get("StatusID"))

            Dim StrQuery As String = ""

            If CStr(Request.QueryString.Get("frmdate")) <> "" Then
                If StatusID = 0 Then
                    StrQuery += " where (Status_id is null or Status_id=0) and DATEADD(day,DATEDIFF(day, 0,NewCif_CreatedDt),0)= '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'"
                Else
                    StrQuery += " where Status_id=" & StatusID & " and DATEADD(day,DATEDIFF(day, 0,NewCif_CreatedDt),0)= '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'"
                End If

            End If

            Dim SqlStr As String = ""

            SqlStr = "select * from (" &
" select dedupeId,replace(convert(char(11),NewCif_CreatedDt,113),' ','-') OpenDate,New_Cif as CIF,NewCif_Name as Name," &
" NewCif_brcode as BranchCode,replace(convert(char(11),NewCif_DOB,113),' ','-') DOB,NewCif_Pin as PIN,NewCif_Mob as Mobile," &
" NewCif_Email as Email,NewCif_Pan as PAN,NewCif_Dlicense as DL,NewCif_Passport as Passport,NewCif_VoterID as VotersID," &
" NewCif_Aadhar as Aadhar,NewCif_Father as Father,NewCif_Mother as Mother,NewCif_Address as Address,MakerN as MakerNew," &
" replace(convert(char(11),MakerNDt,113),' ','-') MakerNewDate, case when MakerNFlg=1 then 'DEDUPE YES' when MakerNFlg=2 " &
" then 'DEDUPE NO' else '' end MakerNewStatus,CheckerN as CheckerNew,replace(convert(char(11),CheckerNDt,113),' ','-')CheckerNewDate," &
" case when CheckerNFlg=1 then 'DEDUPE YES' when CheckerNFlg=2 then 'DEDUPE NO' else '' end CheckerNewStatus," &
" MakerBY as MakerBranchYes,replace(convert(char(11),MakerBYDt,113),' ','-')MakerBranchYesDate, case when MakerBYFlg=1 then 'DEDUPE YES' " &
" when MakerBYFlg=2 then 'DEDUPE NO' when MakerBYFlg=3 then 'RETURN TO BRANCH' else '' end MakerBranchYesStatus," &
" CheckerBY as CheckerBranchYes,replace(convert(char(11),CheckerBYDt,113),' ','-')CheckerBranchYesDate," &
" case when CheckerBYFlg=1 then 'DEDUPE YES'  when CheckerBYFlg=2 then 'DEDUPE NO' when CheckerBYFlg=3 then 'RETURN TO CHECKER'" &
" else '' end CheckerBranchYesStatus,CheckerBN as CheckerBranchNo,replace(convert(char(11),CheckerBNDt,113),' ','-')CheckerBranchNoDate," &
" case when CheckerBNFlg=1 then 'DEDUPE YES' when CheckerBNFlg=2 then 'DEDUPE NO' else '' end CheckerBranchNoStatus," &
" Branch as BranchDoneBy,replace(convert(char(11),BranchDt,113),' ','-')BranchDoneDate, case when BranchFlg=1 " &
" then 'DEDUPE YES' when BranchFlg=2 then 'DEDUPE NO' else '' end BranchDoneSatus from dedupe_cust_verification " & StrQuery & " " &
            " union all " &
" select dedupeId,replace(convert(char(11),DupeCif_CreatedDt,113),' ','-') OpenDate,Dupe_Cif as CIF ,DupeCif_Name as Name," &
" DupeCif_brcode as BranchCode,replace(convert(char(11),DupeCif_DOB,113),' ','-') DOB,DupeCif_Pin as PIN,DupeCif_Mob as Mobile," &
" DupeCif_Email as Email,DupeCif_Pan as PAN,DupeCif_Dlicense as DL,DupeCif_Passport as Passport,DupeCif_VoterID as VotersID," &
" DupeCif_Aadhar as Aadhar,DupeCif_Father as Father,DupeCif_Mother as Mother,DupeCif_Address as Address,MakerN as MakerNew," &
" replace(convert(char(11),MakerNDt,113),' ','-') MakerNewDate, case when MakerNFlg=1 then 'DEDUPE YES' when MakerNFlg=2 " &
" then 'DEDUPE NO' else '' end MakerNewStatus,CheckerN as CheckerNew,replace(convert(char(11),CheckerNDt,113),' ','-')CheckerNewDate," &
" case when CheckerNFlg=1 then 'DEDUPE YES' when CheckerNFlg=2 then 'DEDUPE NO' else '' end CheckerNewStatus,MakerBY as MakerBranchYes," &
" replace(convert(char(11),MakerBYDt,113),' ','-')MakerBranchYesDate, case when MakerBYFlg=1 then 'DEDUPE YES' when MakerBYFlg=2 " &
" then 'DEDUPE NO' when MakerBYFlg=3 then 'RETURN TO BRANCH' else '' end MakerBranchYesStatus,CheckerBY as CheckerBranchYes," &
" replace(convert(char(11),CheckerBYDt,113),' ','-')CheckerBranchYesDate, case when CheckerBYFlg=1 then 'DEDUPE YES'  when CheckerBYFlg=2" &
" then 'DEDUPE NO' when CheckerBYFlg=3 then 'RETURN TO CHECKER' else '' end CheckerBranchYesStatus,CheckerBN as CheckerBranchNo," &
" replace(convert(char(11),CheckerBNDt,113),' ','-')CheckerBranchNoDate, case when CheckerBNFlg=1 then 'DEDUPE YES' when CheckerBNFlg=2" &
" then 'DEDUPE NO' else '' end CheckerBranchNoStatus,Branch as BranchDoneBy,replace(convert(char(11),BranchDt,113),' ','-')BranchDoneDate," &
" case when BranchFlg=1 then 'DEDUPE YES' when BranchFlg=2 then 'DEDUPE NO' else '' end BranchDoneSatus from dedupe_cust_verification " & StrQuery & " " &
" )temp order by dedupeId"

            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            WebTools.ExporttoExcel(DT, "Dedupe Excel Report")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
