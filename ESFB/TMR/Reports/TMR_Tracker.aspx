﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    EnableEventValidation="false" CodeFile="TMR_Tracker.aspx.vb" Inherits="TMR_Tracker" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
   
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
            <br />
    <div style="width: 80%; margin: 0px auto;">
        <table class="style1" style="width: 100%; top: 350px auto;">
            <tr class="style1">
                <td style="text-align: left; width: 22%;">
                    <asp:Label ID="lblReport" runat="server" Text="Select Report "></asp:Label> </td>
                <td style="text-align: left; width: 22%;">
                      
                    <asp:DropDownList ID="ddlReportType" runat="server"  class="NormalText"  Style="text-align: left;" Font-Names="Cambria" Width="100%" ForeColor="Black" AutoPostBack="true" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged" ControlToValidate="ddlReportType">   
                    </asp:DropDownList> </td>
                <td style="text-align: left; width: 22%;">
                    <asp:RequiredFieldValidator ID="RFVddlReportType" runat="server" ErrorMessage="Please Select Report Type"  ControlToValidate="ddlReportType" InitialValue="-1" ForeColor="Red" ValidationGroup="validationSubmit"></asp:RequiredFieldValidator>
                </td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
             <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    <asp:HiddenField ID="hfReportPanel" runat="server" /> &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                     &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
       <asp:PlaceHolder runat="server" ID="phDate" Visible="false">
            <tr>
                <td style="text-align: justify; width: 22%;" class="style2">
                    From date &nbsp; &nbsp;
                </td>
                <td style="text-align: left; width: 22%;">
                    <asp:TextBox ID="txtStartDt" class="NormalText" runat="server" Width="98%" onkeypress="return false"
                        ></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtStartDt" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
                <td style="text-align: left; margin: 0px auto; width: 12%;">
                </td>
                <td style="text-align: left; margin: 0px auto; width: 22%;">
                    To date &nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 22%;">
                    <asp:TextBox ID="txtToDt" class="NormalText" runat="server" Width="98%" onkeypress="return false"
                        ></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtToDt" Format="dd MMM yyyy">
                    </ajaxToolkit:CalendarExtender>
                </td>
            </tr>
            </asp:PlaceHolder>

             <asp:PlaceHolder runat="server" ID="phDropDown" Visible="false">
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    <asp:Label ID="lbldropdown" runat="server" Text="Label"></asp:Label>  </td>
                <td style="text-align: left; width: 22%;">
                    <asp:DropDownList ID="ddldropdown" runat="server"></asp:DropDownList>  </td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
              </asp:PlaceHolder>

              <asp:PlaceHolder runat="server" ID="phTextBox" Visible="false">
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    <asp:Label ID="lblTextBox" runat="server" Text="Label"></asp:Label>  </td>
                <td style="text-align: left; width: 22%;">
                    <asp:TextBox ID="txtTextbox" runat="server"></asp:TextBox></td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
              </asp:PlaceHolder>
            <tr>
                <td style="text-align: left; width: 22%;" class="style2">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 12%;">
                    &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                     &nbsp;</td>
                <td style="text-align: left; width: 22%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="5">
                    <br />
                    <br />
                 
                    <asp:Button ID="btnShow" runat="server" Text="SHOW"  ValidationGroup="validationSubmit"/>
                    &nbsp;
                     <asp:Button ID="btnExit" runat="server" Text="EXIT" />
                 </td>
               
            </tr>
        </table>
    </div>
            <asp:HiddenField ID="hdnApplication" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
