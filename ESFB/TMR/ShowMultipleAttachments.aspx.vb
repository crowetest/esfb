﻿Imports System.Data
Partial Class ShowMultipleAttachments
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim GF As New GeneralFunctions
    Dim DT As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
           

            Dim tmrno As Integer = CInt(Request.QueryString.[Get]("tmrno"))

            DT = GF.GeneralFunctionForTMR("TMR011", tmrno.ToString())
            If DT IsNot Nothing Then
                Dim tb As New Table
                tb.Attributes.Add("width", "100%")
                For Each DR As DataRow In DT.Rows
                    Dim TRHead As New TableRow
                    Dim TRHead_00 As New TableCell
                    RH.AddColumn(TRHead, TRHead_00, 100, 100, "l", "<a href='ShowRiskFormat.aspx?DocID=" + (DR(0).ToString()) + " &tmrno=" + (DR(4).ToString()) + " ' style='cursor:pointer; font-size:12pt;'>" + DR(3) + "</a>")
                    tb.Controls.Add(TRHead)
                Next
                pnDisplay.Controls.Add(tb)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
