﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class TMR_TMRAllocation
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
        Dim CallBackReturn As String = Nothing
        Dim DB As New MS_SQL.Connect
        Dim GF As New GeneralFunctions
    Dim DT, DT1, DT2, DT3, DT4 As New DataTable
        Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub TMRAllocation_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub TMRAllocation_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "TMR Allocation"

        If GF.FormAccess(CInt(Session("UserID")), 1469) = False Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
       
        If Not IsPostBack Then
            DT = GF.GeneralFunctionForTMR("TMR001", "")
            GF.ComboFill(cmbTmrNo, DT, 0, 1)
            DT1 = GF.GeneralFunctionForTMR("TMR002", "")
            GF.ComboFill(cmbbucket, DT1, 0, 1)
           
            DT2 = GF.GeneralFunctionForTMR("TMR004", "")
            GF.ComboFill(cmbcritical, DT2, 0, 1)
            DT3 = GF.GeneralFunctionForTMR("TMR005", "")
            GF.ComboFill(cmbvertical, DT3, 0, 1)
            DT4 = GF.GeneralFunctionForTMR("TMR006", "")
            GF.ComboFill(cmbsubvertical, DT4, 0, 1)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        End If

        Me.cmbTmrNo.Attributes.Add("onchange", "return TmrOnChange()")

    End Sub
#End Region

#Region "Call Back"
        Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
            Return CallBackReturn
        End Function

        Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
            Dim Data() As String = eventArgument.Split(CChar("ʘ"))
            Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))

        Dim ParameterId As String = Nothing
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill TMR Details

                Dim tmrno As String = CStr(Data(1))
                DT = GF.GeneralFunctionForTMR("TMR010", tmrno)
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)("cifno").ToString() + "¥" + DT.Rows(0)("customerName").ToString() + "¥" + DT.Rows(0)("noOfAccounts").ToString() + "¥" + DT.Rows(0)("accountNo").ToString() +
                        "¥" + DT.Rows(0)("issueNoted").ToString() + "¥" + DT.Rows(0)("targetDate").ToString() + "¥"
                End If
                DT = GF.GeneralFunctionForTMR("TMR011", tmrno.ToString() + "^1")
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "¥" + DT.Rows(0)(1).ToString() + "¥" + DT.Rows(0)(2).ToString() + "¥" + DT.Rows(0)(3).ToString() +
                        "¥" + DT.Rows(0)(4).ToString() + "¥" + DT.Rows(0)(5).ToString()
                End If
                CallBackReturn += "£"

        End Select


    End Sub
#End Region
#Region "Confirm"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing

        Dim myFile As HttpPostedFile = fupBrd.PostedFile
        Dim nFileLen As Integer = myFile.ContentLength
        Dim FileName As String = ""
        If (nFileLen > 0) Then
            ContentType = myFile.ContentType
            FileName = myFile.FileName
            AttachImg = New Byte(nFileLen - 1) {}
            myFile.InputStream.Read(AttachImg, 0, nFileLen)
            Dim FileLength As Integer = CInt(myFile.ContentLength) 'Convert into bytes
            If FileLength > 4000000 Then
                Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_srpt1.Append("alert('Please Check the Size of attached file');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                Exit Sub
            End If
            Dim FileExtension As String = Path.GetExtension(myFile.FileName)
            If Not (FileExtension = ".xls" Or FileExtension = ".xlsx" Or FileExtension = ".jpg" Or FileExtension = ".jpeg" Or FileExtension = ".doc" Or FileExtension = ".docx" Or FileExtension = ".zip" Or FileExtension = ".pdf" Or FileExtension = ".PDF" Or FileExtension = ".XLS" Or FileExtension = ".XLSX" Or FileExtension = ".JPG" Or FileExtension = ".JPEG" Or FileExtension = ".DOC" Or FileExtension = ".DOCX" Or FileExtension = ".ZIP") Then
                Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                Exit Sub
            End If
        End If

        Try

            Dim tmrnumber As Integer = CInt(cmbTmrNo.SelectedValue)
            Dim Params(27) As SqlParameter
            Params(0) = New SqlParameter("@p_TMR_No", SqlDbType.Int)
            Params(0).Value = tmrnumber
            Params(1) = New SqlParameter("@p_Cif_No", SqlDbType.VarChar, 100)
            Params(1).Value = txtcifno.Text
            Params(2) = New SqlParameter("@p_Customer_Name", SqlDbType.VarChar, 100)
            Params(2).Value = txtCustname.Text
            Params(3) = New SqlParameter("@p_No_Of_Accounts", SqlDbType.Int)
            Params(3).Value = txtnoofaccnt.Text
            Params(4) = New SqlParameter("@p_Account_No", SqlDbType.VarChar, 200)
            Params(4).Value = txtaccnt.Text
            Params(5) = New SqlParameter("@p_Issue_Noted", SqlDbType.VarChar, 600)
            Params(5).Value = txtdescription.Text
            Params(6) = New SqlParameter("@p_Target_Date", SqlDbType.Date)
            Params(6).Value = CDate(txtReadyDt.Text)
            Params(7) = New SqlParameter("@p_Bucket_Id", SqlDbType.Int)
            Params(7).Value = cmbbucket.SelectedValue
            Params(8) = New SqlParameter("@p_Vertical_Id", SqlDbType.Int)
            Params(8).Value = cmbvertical.SelectedValue
            Params(9) = New SqlParameter("@p_SubVertical_Id", SqlDbType.Int)
            Params(9).Value = cmbsubvertical.SelectedValue
            Params(10) = New SqlParameter("@p_Criticality_Id", SqlDbType.Int)
            Params(10).Value = cmbcritical.SelectedValue
            Params(11) = New SqlParameter("@p_Credit_Channel", SqlDbType.VarChar, 200)
            Params(11).Value = txtcredit.Text
            Params(12) = New SqlParameter("@p_Debit_Channel", SqlDbType.VarChar, 200)
            Params(12).Value = txtdebit.Text
            Params(13) = New SqlParameter("@p_Bucket_Id_Final", SqlDbType.Int)
            Params(13).Value = 0
            Params(14) = New SqlParameter("@p_SubBucket_Name", SqlDbType.VarChar, 200)
            Params(14).Value = txtsubbucket.Text
            Params(15) = New SqlParameter("@p_SubBucket_Id_Three", SqlDbType.Int)
            Params(15).Value = 0
            Params(16) = New SqlParameter("@p_User_Id", SqlDbType.Int)
            Params(16).Value = UserID
            Params(17) = New SqlParameter("@p_Remarks", SqlDbType.VarChar, 200)
            Params(17).Value = txtremarks.Text
            Params(18) = New SqlParameter("@p_File", SqlDbType.VarBinary)
            Params(18).Value = AttachImg
            Params(19) = New SqlParameter("@p_File_ype", SqlDbType.VarChar, 500)
            Params(19).Value = ContentType
            Params(20) = New SqlParameter("@p_File_Name", SqlDbType.VarChar, 500)
            Params(20).Value = FileName
            Params(21) = New SqlParameter("@p_ErrorStatus", SqlDbType.Int)
            Params(21).Direction = ParameterDirection.Output
            Params(22) = New SqlParameter("@p_OutputMessage", SqlDbType.VarChar, 4000)
            Params(22).Direction = ParameterDirection.Output
            Params(23) = New SqlParameter("@p_Type_Id", SqlDbType.Int)
            Params(23).Value = 1

            Params(24) = New SqlParameter("@p_SubVertical_Id_Two", SqlDbType.Int)
            Params(24).Value = cmbsubvertical.SelectedValue

            Params(25) = New SqlParameter("@p_Cluster_Id", SqlDbType.Int)
            Params(25).Value = 0
            Params(26) = New SqlParameter("@p_Status_Id", SqlDbType.Int)
            Params(26).Value = 1
            Params(27) = New SqlParameter("@p_Next_EmpCode", SqlDbType.Int)
            Params(27).Value = txtempcode.Text
            DB.ExecuteNonQuery("SP_TMR_Allocation", Params)
            ErrorFlag = CInt(Params(21).Value)
            Message = CStr(Params(22).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1

            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "'); ")
        cl_script1.Append("        window.open('TMRAllocation.aspx', '_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        If ErrorFlag = 0 Then
            initializeControls()
        End If

    End Sub
    Private Sub initializeControls()

    End Sub
#End Region
End Class


