﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class TMR_TMRResponse
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT1, DT2, DT3, DT4 As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub TMRResponse_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub TMRResponse_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "TMR Response"

        If GF.FormAccess(CInt(Session("UserID")), 1478) = False Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        Dim UserID As Integer = CInt(Session("UserID"))
        If Not IsPostBack Then
            DT = GF.GeneralFunctionForTMR("TMR020", CStr(UserID.ToString()))
            GF.ComboFill(cmbTmrNo, DT, 0, 1)


            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
        End If

        Me.cmbTmrNo.Attributes.Add("onchange", "return TmrOnChange()")

    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))

        Dim ParameterId As String = Nothing
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill TMR Details

                Dim tmrno As String = CStr(Data(1))
                DT = GF.GeneralFunctionForTMR("TMR010", tmrno)
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)("cifno").ToString() + "¥" + DT.Rows(0)("customerName").ToString() + "¥" + DT.Rows(0)("noOfAccounts").ToString() + "¥" + DT.Rows(0)("accountNo").ToString() +
                        "¥" + DT.Rows(0)("issueNoted").ToString() + "¥" + DT.Rows(0)("targetDate").ToString() + "¥" + DT.Rows(0)("bucketName").ToString() + "¥" + DT.Rows(0)("subBucketName").ToString() + "¥" +
                    DT.Rows(0)("bucketIdFinalName").ToString() + "¥" + DT.Rows(0)("bucketIdThreeName").ToString() + "¥" + DT.Rows(0)("verticalName").ToString() + "¥" + DT.Rows(0)("subverticalName").ToString() + "¥" +
                    DT.Rows(0)("criticalityName").ToString() + "¥" + DT.Rows(0)("creditChannel").ToString() + "¥" + DT.Rows(0)("debitChannel").ToString() + "¥" + DT.Rows(0)("level1Comments").ToString() + "¥"
                End If
                DT = GF.GeneralFunctionForTMR("TMR011", tmrno.ToString() + "^2")
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "¥" + DT.Rows(0)(1).ToString() + "¥" + DT.Rows(0)(2).ToString() + "¥" + DT.Rows(0)(3).ToString() +
                        "¥" + DT.Rows(0)(4).ToString() + "¥" + DT.Rows(0)(5).ToString()
                End If
                CallBackReturn += "£"

        End Select


    End Sub
#End Region
#Region "Confirm"

    Private Sub initializeControls()

    End Sub
#End Region
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Try
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files

            Dim RequestID As Integer = 0
            Dim UserID As String = Session("UserID").ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim NoofAttachments As Integer = 0
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        NoofAttachments += 1
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                    End If

                    Try
                        Dim Params(8) As SqlParameter
                        Params(0) = New SqlParameter("@p_TMR_No", SqlDbType.Int)
                        Params(0).Value = cmbTmrNo.SelectedValue
                        Params(1) = New SqlParameter("@p_User_Id", SqlDbType.Int)
                        Params(1).Value = UserID
                        Params(2) = New SqlParameter("@p_Remarks", SqlDbType.VarChar, 200)
                        Params(2).Value = txtRemarks.Text
                        Params(3) = New SqlParameter("@p_File", SqlDbType.VarBinary)
                        Params(3).Value = AttachImg
                        Params(4) = New SqlParameter("@p_File_ype", SqlDbType.VarChar, 500)
                        Params(4).Value = ContentType
                        Params(5) = New SqlParameter("@p_File_Name", SqlDbType.VarChar, 500)
                        Params(5).Value = FileName
                        Params(6) = New SqlParameter("@p_ErrorStatus", SqlDbType.Int)
                        Params(6).Direction = ParameterDirection.Output
                        Params(7) = New SqlParameter("@p_OutputMessage", SqlDbType.VarChar, 4000)
                        Params(7).Direction = ParameterDirection.Output
                        Params(8) = New SqlParameter("@p_Id", SqlDbType.Int)
                        Params(8).Value = i
                        DB.ExecuteNonQuery("SP_TMR_Response", Params)
                        ErrorFlag = CInt(Params(6).Value)
                        Message = CStr(Params(7).Value)
                    Catch ex As Exception
                        Message = ex.Message.ToString
                        ErrorFlag = 1

                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                    End Try

                    Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_script1.Append("         alert('" + Message + "'); ")
                    cl_script1.Append("        window.open('TMRResponse.aspx', '_self');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
                    If ErrorFlag = 0 Then
                        initializeControls()
                    End If

                Next
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
End Class
