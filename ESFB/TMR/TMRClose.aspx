﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="TMRClose.aspx.vb" Inherits="TMR_TMRClose" EnableEventValidation="false"%>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {  
                document.getElementById("rAttach").style.display = "none"; 
             //   ToServer("1ʘ", 1);
             
            }
             function TmrOnChange()
            {
           
                if (document.getElementById("<%= cmbTmrNo.ClientID %>").value == -1)
                {
                    alert("Select TMR Number");
                    document.getElementById("<%= cmbTmrNo.ClientID %>").focus();
                    return false;
                } 
                 document.getElementById("<%= hdnattach.ClientID %>").value=  document.getElementById("<%= cmbTmrNo.ClientID %>").value;
                ToServer("1ʘ" + document.getElementById("<%= cmbTmrNo.ClientID %>").value, 1);
            }
            function setStartDate(sender, args) 
            {
                sender._endDate = new Date();
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {
                        if(Arg != "")
                        {
                            var Args = Arg.split("£"); 
                            var Data = Args[0].split("¥");
                           
                            document.getElementById("<%= txtcifno.ClientID %>").value=Data[0];
                            document.getElementById("<%= txtCustname.ClientID %>").value=Data[1];
                            document.getElementById("<%= txtnoofaccnt.ClientID %>").value=Data[2];
                            document.getElementById("<%= txtaccnt.ClientID %>").value=Data[3];
                            document.getElementById("<%= txtdescription.ClientID %>").value=Data[4];
                            document.getElementById("<%= txtReadyDt.ClientID %>").value=Data[5];
                            document.getElementById("<%= cmbbucket.ClientID %>").value=Data[6];
                            document.getElementById("<%= txtsubbucket.ClientID %>").value=Data[7];

                            document.getElementById("<%= cmbvertical.ClientID %>").value=Data[10];
                            document.getElementById("<%= cmbsubvertical.ClientID %>").value=Data[11];
                            document.getElementById("<%= cmbcritical.ClientID %>").value=Data[12];
                            document.getElementById("<%= txtcredit.ClientID %>").value=Data[13];
                            document.getElementById("<%= txtdebit.ClientID %>").value=Data[14];
                            document.getElementById("<%= txtremarks.ClientID %>").value=Data[15];
                           
                         
                         break;
                     }
                }
                 case 3:
                    {
                        var Data = Arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("TMRAllocation.aspx", "_self");
                        break;
                    }
                }
            }
            function viewReport() {
           

           var tmrno=document.getElementById("<%= hdnattach.ClientID %>").value;
               
           if(tmrno!="")
           {
            window.open("ShowMultipleAttachments.aspx?tmrno=" + tmrno,"_blank");
        
            return false;
            }
        }
             function btnSubmit_onclick() {
                if (document.getElementById("<%= cmbtmrno.ClientID %>").value == -1)
                {
                    alert("Select TmrNo");
                    document.getElementById("<%= cmbtmrno.ClientID %>").focus();
                    return false;
                }
             
               
            }
            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        

           
          
        </script>

    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        TMR No</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbTmrNo" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RFVcmbTmrNo" ControlToValidate="cmbTmrNo" runat="server" ErrorMessage="Select TMR No" InitialValue="-1" ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>

                        <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        CIF No</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:TextBox ID="txtcifno" runat="server"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Customer Name</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:TextBox ID="txtCustname" runat="server"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                  <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        No.of Accounts</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:TextBox ID="txtnoofaccnt" runat="server" onkeypress="NumericCheck(event)"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Account Number</td>
                    <td style="width: 30%; text-align: left;">                        
                          <asp:TextBox ID="txtaccnt" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine"  Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                
               <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Description about Issue
                    </td>
                    <td style="width: 30%; text-align: left;">
                        <asp:TextBox ID="txtdescription" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                <td style="width: 10%">
                        &nbsp;
                    </td>
                  <td style="width:23%">Attachments</td>
               
              <td style='width:2%;text-align:left'><img id='ViewReport' src='../Image/attchment2.png' onclick='viewReport()' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
           
            <td style="width: 10%">
                        &nbsp;
                    </td>
              </tr>
               
                
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Target Date</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:TextBox ID="txtReadyDt" runat="server" onkeypress="NumericCheck(event)"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="txtReadyDt_CalendarExtender" runat="server" 
                            Format="dd/MMM/yyyy" OnClientShowing="setStartDate" 
                            TargetControlID="txtReadyDt">
                        </ajaxToolkit:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RFVtxtReadyDt" ControlToValidate="txtReadyDt" runat="server" ErrorMessage="Select Target Date"  ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
   
               
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Bucket</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbbucket" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RFVcmbbucket" ControlToValidate="cmbbucket" runat="server" ErrorMessage="Select Bucket"  InitialValue="-1"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                 <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Sub Bucket</td>
                    <td style="width: 30%; text-align: left;">
                        <asp:TextBox ID="txtsubbucket" runat="server"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"   />
                           <asp:RequiredFieldValidator ID="RFVtxtsubbucket" ControlToValidate="txtsubbucket" runat="server" ErrorMessage="EnterSubBucket"  ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
               
              
               <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Vertical</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbvertical" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                           <asp:RequiredFieldValidator ID="RFVcmbvertical" ControlToValidate="cmbvertical" runat="server" ErrorMessage="Select Vertical"  InitialValue="-1"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
              <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Sub Vertical</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbsubvertical" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RFVcmbsubvertical" ControlToValidate="cmbsubvertical" runat="server" ErrorMessage="Select SubVertical"  InitialValue="-1"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                 <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Criticality</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbcritical" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                          <asp:RequiredFieldValidator ID="RFVcmbcritical" ControlToValidate="cmbcritical" runat="server" ErrorMessage="Select Criticality"  InitialValue="-1"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
              
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Credit Channel
                    </td>
                    <td style="width: 30%; text-align: left;">
                        <asp:TextBox ID="txtcredit" runat="server"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"   />
                          <asp:RequiredFieldValidator ID="RFVtxtcredit" ControlToValidate="txtcredit" runat="server" ErrorMessage="Enter Credit Channel"  ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Debit Channel</td>
                    <td style="width: 30%; text-align: left;">
                        <asp:TextBox ID="txtdebit" runat="server"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"   />
                         <asp:RequiredFieldValidator ID="RFVtxtdebit" ControlToValidate="txtdebit" runat="server" ErrorMessage="Enter Debit Channel"  ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Remarks</td>
                    <td style="width: 30%; text-align: left;">
                        <asp:TextBox ID="txtremarks" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                          <asp:RequiredFieldValidator ID="RFVtxtremarks" ControlToValidate="txtremarks" runat="server" ErrorMessage="Enter Remarks"  ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                 <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                       Final Bucket</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbFinalbucket" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>

                 <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Bucket 3</td>
                    <td style="width: 30%; text-align: left;">                        
                        <asp:DropDownList ID="cmbbucket3" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                   <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Remarks</td>
                    <td style="width: 30%; text-align: left;">
                        <asp:TextBox ID="txtnewremarks" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtnewremarks" runat="server" ErrorMessage="Enter Remarks"  ></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
            
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                 <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <asp:Button ID="btnSave" runat="server" Text="APPROVE" Width="67px" />
                         &nbsp;
                        <asp:Button ID="btnreject" runat="server" Text="REJECT" Width="67px" />
                        &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
           
             <asp:HiddenField ID="hdnattach" runat="server" />
            <configuration>
    <system.web>
    <compilaton debug="true" targetFramework="4.0"></compilaton>
    </system.web>
    </configuration>
        </div>
        <br />
    </div>
</asp:Content>