﻿Imports System.Data
Partial Class Leave_ShowVetFormat
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim DocID As Integer = Convert.ToInt32(Request.QueryString.[Get]("DocID"))
            Dim tmrno As Integer = Convert.ToInt32(Request.QueryString.[Get]("tmrno"))
          
            DT = GF.GeneralFunctionForTMR("TMR012", DocID.ToString() + "^" + tmrno.ToString())
            If DT IsNot Nothing Then
                Dim bytes() As Byte = CType(DT.Rows(0)(1), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = DT.Rows(0)(2).ToString()
                Response.AddHeader("content-disposition", "attachment;filename=" + DT.Rows(0)(3).ToString().Replace(" ", ""))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
