﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="TMRAssignment.aspx.vb" Inherits="TMR_TMRAssignment" EnableEventValidation="false"%>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 23%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script src="../../Script/jquery.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        window.onload = function () {
          
         
        }
          
          function TmrOnChange()
            {
           
                if (document.getElementById("<%= cmbTmrNo.ClientID %>").value == -1)
                {
                    alert("Select TMR Number");
                    document.getElementById("<%= cmbTmrNo.ClientID %>").focus();
                    return false;
                } 

              document.getElementById("<%= hdnattach.ClientID %>").value=  document.getElementById("<%= cmbTmrNo.ClientID %>").value;
                ToServer("1ʘ" + document.getElementById("<%= cmbTmrNo.ClientID %>").value, 1);

            }

             function assignOnChange()
            {
           
                if (document.getElementById("<%= ddlassigntype.ClientID %>").value == -1)
                {
                    alert("Select Category");
                    document.getElementById("<%= ddlassigntype.ClientID %>").focus();
                    return false;
                } 
//                ToServer("2ʘ" + document.getElementById("<%= ddlassigntype.ClientID %>").value, 2);
            }
             function stateOnChange()
            {
          
                if (document.getElementById("<%= ddlstate.ClientID %>").value == -1)
                {
                    alert("Select State");
                    document.getElementById("<%= ddlstate.ClientID %>").focus();
                    return false;
                } 
                ToServer("3ʘ" + document.getElementById("<%= ddlstate.ClientID %>").value +"ʘ"+ document.getElementById("<%= ddlassigntype.ClientID %>").value, 3);
            }

             function branchOnChange()
            {
           
                if (document.getElementById("<%= cmbbranch.ClientID %>").value == -1)
                {
                    alert("Select Branch");
                    document.getElementById("<%= cmbbranch.ClientID %>").focus();
                    return false;
                } 

                  document.getElementById("<%= hdnbranch.ClientID %>").value=document.getElementById("<%= cmbbranch.ClientID %>").value;

                ToServer("4ʘ" + document.getElementById("<%= cmbbranch.ClientID %>").value, 4);
            }
        Date.prototype.toShortFormat = function () {
            var month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var day = this.getDate();
            var month_index = this.getMonth();
            var year = this.getFullYear();
            return "" + day + " " + month_names[month_index] + " " + year;
        }
        function AlphaSpaceCheck(e) {
            var inputValue = e.which;
            if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0 && inputValue != 121)) {
                e.preventDefault();
            }
        }
        function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function FromServer(Arg, context) {
                switch (context) {
                    case 1:
                    {
                        if(Arg != "")
                        {
                      
                            var Args = Arg.split("£"); 
                            var Data = Args[0].split("¥");
                          
                       
                            document.getElementById("<%= txtcifno.ClientID %>").value=Data[0];
                            document.getElementById("<%= txtCustname.ClientID %>").value=Data[1];
                            document.getElementById("<%= txtnoofaccnt.ClientID %>").value=Data[2];
                            document.getElementById("<%= txtaccnt.ClientID %>").value=Data[3];
                            document.getElementById("<%= txtdescription.ClientID %>").value=Data[4];
                            document.getElementById("<%= txtReadyDt.ClientID %>").value=Data[5];
                            document.getElementById("<%= txtbucket.ClientID %>").value=Data[6];
                            document.getElementById("<%= txtsubbucket.ClientID %>").value=Data[7];

                            document.getElementById("<%= txtvertical.ClientID %>").value=Data[10];
                            document.getElementById("<%= txtsubvertical.ClientID %>").value=Data[11];
                            document.getElementById("<%= txtcritical.ClientID %>").value=Data[12];
                            document.getElementById("<%= txtcredit.ClientID %>").value=Data[13];
                            document.getElementById("<%= txtdebit.ClientID %>").value=Data[14];
                            document.getElementById("<%= txtremarksprev.ClientID %>").value=Data[15];
                            
                       


                     }
                }
                case 3:

                        {
      
                            ComboFill(Arg, "<%= cmbbranch.ClientID %>");

                            
                            break;
                       }
                 case 4:
                            {
                           
                             var cluster = Arg;
                            document.getElementById("<%= txtcluster.ClientID %>").value=cluster;

                            break;
                            }
                 case 5:
                    {
                        var Data = Arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("TMRAssignment.aspx", "_self");
                        break;
                    }
                }
            }
        function RequestOnClick() {

      
                if (document.getElementById("<%= cmbtmrno.ClientID %>").value == -1)
                {
                    alert("Select TMR No");
                    document.getElementById("<%= cmbtmrno.ClientID %>").focus();
                    return false;
                }
               if (document.getElementById("<%= ddlassigntype.ClientID %>").value == -1)
                {
                    alert("Select Category");
                    document.getElementById("<%= ddlassigntype.ClientID %>").focus();
                    return false;
                } 
                  if (document.getElementById("<%= ddlstate.ClientID %>").value == -1)
                {
                    alert("Select State");
                    document.getElementById("<%= ddlstate.ClientID %>").focus();
                    return false;
                } 
                  if (document.getElementById("<%= cmbbranch.ClientID %>").value == -1)
                {
                    alert("Select Branch");
                    document.getElementById("<%= cmbbranch.ClientID %>").focus();
                    return false;
                } 

                  if (document.getElementById("<%= fupBrd.ClientID%>").value == "") {
            alert("Attach File"); document.getElementById("<%= fupBrd.ClientID%>").focus(); return false;
        }
        
            }

             function viewReport() {
           

           var tmrno=document.getElementById("<%= hdnattach.ClientID %>").value;
               
           if(tmrno!="")
           {
            window.open("ShowMultipleAttachments.aspx?tmrno=" + tmrno,"_blank");
        
            return false;
            }
        }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
        
    </script>   
</head>
</html>
<br />
<div  style="width:90%;margin:0px auto; background-color: #A34747;">
    <br />
    <div id = "divSection1" class = "sec1" style="width:97%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">
            <tr> 
                <td style="width:23%;">
                   
                </td>
                <td style="width:23%; text-align:left;"></td>
                  <td class="auto-style6">
                   <asp:HiddenField ID="hid_temp" runat="server" />
            </td>   
                <td style="width:23%"></td>
                <td style="width:23%"></td>
            </tr>
            <tr>    
                <td style="width:23%;">TMR No</td>
              
                   <td style="width:23%">
                    <asp:DropDownList ID="cmbTmrNo" runat="server" class="NormalText" Width="100.5%">
                     
                    </asp:DropDownList>
              
                </td>
                 <td style="width:2%"></td>
                <td style="width:23%; text-align:left;">
                    Cif No
                </td>
                 <td style="width:23%;">
                 <asp:TextBox ID="txtCifNo" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox></td>
            </tr>
            <tr>    
                <td style="width:23%;">Customer Name</td>
                <td style="width:23%;">
                   <asp:TextBox ID="txtCustname" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox>
                </td>
                 <td style="width:2%"></td>
                <td style="width:23%; text-align:left;">
                   No of Accounts
                </td>
                  <td style="width:23%;">
                   <asp:TextBox ID="txtnoofaccnt" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:23%;">Account Number</td>
                <td style="width:23%;">
                   <asp:TextBox ID="txtaccnt" class="ReadOnlyTextBox" ReadOnly="true" TextMode="MultiLine" runat="server" Width="99.3%" ></asp:TextBox>
                </td>
                 <td style="width:2%"></td>
                <td style="width:23%;">Issue</td>
                <td style="width:23%; text-align:left;">
                    <asp:TextBox ID="txtdescription" class="ReadOnlyTextBox"  TextMode="MultiLine" runat="server" ReadOnly="true" Width="99.3%" ></asp:TextBox>
                </td>
            </tr>
           
                 
               
            <tr>    
                <td style="width:23%;">Bucket</td>
                <td style="width:23%;">
                  <asp:TextBox ID="txtbucket" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox>
                </td>
                 <td style="width:2%"></td>
                 <td style="width:23%;">Sub Bucket</td>
                <td style="width:23%; text-align:left;">
                    <asp:TextBox ID="txtsubbucket" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
      
            <tr>    
                <td style="width:23%;">Vertical</td>
                 <td style="width:23%; text-align:left;">
                    <asp:TextBox ID="txtvertical" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
                 <td style="width:2%"></td>
                <td style="width:23%;">
                   Sub Vertical
                </td>
                <td style="width:23%; text-align:left;">
                    <asp:TextBox ID="txtsubvertical" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width:23%;">Criticality</td>
                <td style="width:23%">
                    <asp:TextBox ID="txtcritical" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
                 <td style="width:2%"></td>
                <td style="width:23%">
                    Credit Channel
                </td>
                <td style="width:23%">
                    <asp:TextBox ID="txtcredit" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width:23%;">Debit Channel</td>
                  <td style="width:23%">
                    <asp:TextBox ID="txtdebit" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
                 <td style="width:2%"></td>
                <td style="width:23%;">Remarks</td>
                  <td style="width:23%">
                    <asp:TextBox ID="txtremarksprev" class="ReadOnlyTextBox" runat="server" TextMode="MultiLine" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>

              <tr>   
                 
                <td style="width:23%">Attachments</td>
               
              <td style='width:2%;text-align:left'><img id='ViewReport' src='../Image/attchment2.png' onclick='viewReport()' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' ></td>";

              
                  <td style="width:2%"></td>
                 <td style="width:23%;"> Target Date</td>
                <td style="width:23%; text-align:left;">
                    <asp:TextBox ID="txtReadyDt" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
          
    <tr>
  <td style="width:23%"></td>
  <td style="width:23%"></td>
  <td style="width:2%"></td>
  <td style="width:23%"></td>
  <td style="width:23%"></td>
    </tr>
     <tr>
  <td style="width:23%"></td>
  <td style="width:23%"></td>
  <td style="width:2%"></td>
  <td style="width:23%"></td>
  <td style="width:23%"></td>
    </tr>
     </table>
 
        <br />
         
       <table style="width:90%;height:90px;margin:0px auto;">
            
            <tr>
               <td style="width:12% ;text-align:center;" colspan="5" class="mainhead" >
                    <strong>ASSIGN DETAILS</strong>
                 
                
                    </td>
                    </tr>
                    <tr>
             <td style="width:23%;"></td>
             <td style="width:23%">
                   Category
                </td>
               
                <td style="width:23%">
                   <asp:DropDownList ID="ddlassigntype" runat="server" class="NormalText" Width="100.5%">
                       
                    </asp:DropDownList>
                </td>
                 <td style="width:23%;"></td>
                   <td style="width:23%;"></td>
            </tr>
            <tr>
                <td style="width:23%;"></td>
                <td style="width:23%">
                   State
                </td>
               
                <td style="width:23%">
                     <asp:DropDownList ID="ddlstate" runat="server" class="NormalText" Width="100.5%">
                       
                    </asp:DropDownList>
                </td>
                 <td style="width:23%;"></td>
                   <td style="width:23%;"></td>
            </tr>
            <tr>
                <td style="width:23%;"></td>
                <td style="width:23%">
                    Branch
                </td>
                <td style="width:23%">
                    <asp:DropDownList ID="cmbbranch" runat="server" class="NormalText" Width="100.5%">
                      
                    </asp:DropDownList>
                </td>
                 <td style="width:23%;"></td>
                   <td style="width:23%;"></td>
                </tr>
                 <tr>
                <td style="width:23%;"></td>
                <td style="width:23%">
                    Cluster
                </td>
               
                     <td style="width:23%">
                    <asp:TextBox ID="txtcluster" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
               
                </td>
                 <td style="width:23%;"></td>
                   <td style="width:23%;"></td>
                </tr>
            <tr>
                <td style="width:23%;"></td>
                <td style="width:23%">
                    Remarks
                </td>
                <td style="width:23%">
                    <asp:TextBox ID="txtRemarks" class="NormalText" runat="server" Width="99.7%" Rows="3" MaxLength="500" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
                </td>
                 <td style="width:23%;"></td>
                   <td style="width:23%;"></td>
            </tr>
         
            <tr>
                <td style="width:23%;"></td>
                <td style="width:23%">
                    Upload Response Sheet
                </td>
                <td style="width: 23%; text-align: left;">
                        <input id="fupBrd" runat="server" cssclass="fileUpload" type="file" /></td>
                    
                 <td style="width:23%;"></td>
                   <td style="width:23%;"></td>
            </tr>
           <tr>
  <td style="width:23%"></td>
  <td style="width:23%"></td>
  <td style="width:2%"></td>
  <td style="width:23%"></td>
  <td style="width:23%"></td>
    </tr>
     <tr>
  <td style="width:23%"></td>
  <td style="width:23%"></td>
  <td style="width:2%"></td>
  <td style="width:23%"></td>
  <td style="width:23%"></td>
    </tr>
   
    </table>
     <br />
     <table style="width:90%;height:90px;margin:0px auto;">
                     <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Width="67px" />
                        &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
             

            
        </table>
           <asp:HiddenField ID="hdnbranch" runat="server" />
           <asp:HiddenField ID="hdnattach" runat="server" />
    </div>
    <br />

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</div>
</asp:Content>


