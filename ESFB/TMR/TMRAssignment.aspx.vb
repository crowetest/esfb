﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class TMR_TMRAssignment
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT1, DT2, DT3, DT4 As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub TMRAssignment_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub TMRAssignment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "TMR Assignment"

        If GF.FormAccess(CInt(Session("UserID")), 1470) = False Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        Dim UserID As Integer = CInt(Session("UserID"))
        If Not IsPostBack Then
            DT = GF.GeneralFunctionForTMR("TMR013", CStr(UserID.ToString()))
            GF.ComboFill(cmbTmrNo, DT, 0, 1)
            DT = GF.GeneralFunctionForTMR("TMR014", "")
            GF.ComboFill(ddlassigntype, DT, 0, 1)

            DT = GF.GeneralFunctionForTMR("TMR007", "")
            GF.ComboFill(ddlstate, DT, 0, 1)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
        End If

        Me.cmbTmrNo.Attributes.Add("onchange", "return TmrOnChange()")
        Me.ddlassigntype.Attributes.Add("onchange", "return assignOnChange()")
        Me.ddlstate.Attributes.Add("onchange", "return stateOnChange()")
        Me.cmbbranch.Attributes.Add("onchange", "return branchOnChange()")
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))

        Dim ParameterId As String = Nothing
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill TMR Details

                Dim tmrno As String = CStr(Data(1))
                DT = GF.GeneralFunctionForTMR("TMR010", tmrno)
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)("cifno").ToString() + "¥" + DT.Rows(0)("customerName").ToString() + "¥" + DT.Rows(0)("noOfAccounts").ToString() + "¥" + DT.Rows(0)("accountNo").ToString() +
                        "¥" + DT.Rows(0)("issueNoted").ToString() + "¥" + DT.Rows(0)("targetDate").ToString() + "¥" + DT.Rows(0)("bucketName").ToString() + "¥" + DT.Rows(0)("subBucketName").ToString() + "¥" +
                    DT.Rows(0)("bucketIdFinalName").ToString() + "¥" + DT.Rows(0)("bucketIdThreeName").ToString() + "¥" + DT.Rows(0)("verticalName").ToString() + "¥" + DT.Rows(0)("subverticalName").ToString() + "¥" +
                    DT.Rows(0)("criticalityName").ToString() + "¥" + DT.Rows(0)("creditChannel").ToString() + "¥" + DT.Rows(0)("debitChannel").ToString() + "¥" + DT.Rows(0)("level1Comments").ToString() + "¥" + tmrno + "¥"
                End If

                'DT = GF.GeneralFunctionForTMR("TMR011", tmrno.ToString())
                'If (DT.Rows.Count > 0) Then
                '    For n As Integer = 0 To DT.Rows.Count - 1
                '        CallBackReturn += DT.Rows(0)(0).ToString() + "¥" + DT.Rows(0)(1).ToString() + "¥" + DT.Rows(0)(2).ToString() + "¥" + DT.Rows(0)(3).ToString() +
                '            "¥" + DT.Rows(0)(4).ToString() + "¥" + DT.Rows(0)(5).ToString() + "¥"
                '        CallBackReturn += "Ñ"
                '    Next


                '  End If
                CallBackReturn += "£"
            Case 2 'Fill State


            Case 3 'Fill branch

                Dim state_id As String = CStr(Data(1))
                Dim category_id As String = CStr(Data(2))
                DT = GF.GeneralFunctionForTMR("TMR015", state_id + "^" + category_id)
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 4 'Fill Cluster
                Dim branch_id As String = CStr(Data(1))
                DT = GF.GeneralFunctionForTMR("TMR016", branch_id)
                If DT.Rows.Count > 0 Then

                    CallBackReturn += DT.Rows(0)(3).ToString()

                End If

            Case 5 'Fill Attachment



        End Select


    End Sub
#End Region
#Region "Confirm"

    Private Sub initializeControls()

    End Sub
#End Region


    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim Message As String = Nothing
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim ErrorFlag As Integer = 0
        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
        Dim FileName As String = ""
        Dim hfc As HttpFileCollection = Request.Files
        If hfc.Count > 0 Then
            For i = 0 To hfc.Count - 1
                Dim myFile As HttpPostedFile = hfc(i)
                Dim nFileLen As Integer = myFile.ContentLength

                If (nFileLen > 0) Then

                    ContentType = myFile.ContentType
                    FileName = myFile.FileName
                    AttachImg = New Byte(nFileLen - 1) {}
                    myFile.InputStream.Read(AttachImg, 0, nFileLen)
                    Dim FileLength As Integer = CInt(myFile.ContentLength) 'Convert into bytes
                    If FileLength > 4000000 Then
                        Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                        cl_srpt1.Append("alert('Please Check the Size of attached file');")
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                        Exit Sub
                    End If
                    Dim FileExtension As String = Path.GetExtension(myFile.FileName)
                    If Not (FileExtension = ".xls" Or FileExtension = ".xlsx" Or FileExtension = ".jpg" Or FileExtension = ".jpeg" Or FileExtension = ".doc" Or FileExtension = ".docx" Or FileExtension = ".zip" Or FileExtension = ".pdf" Or FileExtension = ".PDF" Or FileExtension = ".XLS" Or FileExtension = ".XLSX" Or FileExtension = ".JPG" Or FileExtension = ".JPEG" Or FileExtension = ".DOC" Or FileExtension = ".DOCX" Or FileExtension = ".ZIP") Then
                        Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                        cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                        Exit Sub
                    End If
                End If
            Next
        End If
        Try



            Dim Params(14) As SqlParameter
            Params(0) = New SqlParameter("@p_TMR_No", SqlDbType.Int)
            Params(0).Value = cmbTmrNo.SelectedValue
            Params(1) = New SqlParameter("@p_Category_Id", SqlDbType.Int)
            Params(1).Value = ddlassigntype.SelectedValue
            Params(2) = New SqlParameter("@p_Branch_Id", SqlDbType.Int)
            Params(2).Value = hdnbranch.Value
            Params(3) = New SqlParameter("@p_Cluster_Id", SqlDbType.Int)
            Params(3).Value = 0
            Params(4) = New SqlParameter("@p_Department_Id", SqlDbType.Int)
            Params(4).Value = 0
            Params(5) = New SqlParameter("@p_User_Id", SqlDbType.Int)
            Params(5).Value = UserID
            Params(6) = New SqlParameter("@p_Remarks", SqlDbType.VarChar, 200)
            Params(6).Value = txtRemarks.Text
            Params(7) = New SqlParameter("@p_File", SqlDbType.VarBinary)
            Params(7).Value = AttachImg
            Params(8) = New SqlParameter("@p_File_ype", SqlDbType.VarChar, 500)
            Params(8).Value = ContentType
            Params(9) = New SqlParameter("@p_File_Name", SqlDbType.VarChar, 500)
            Params(9).Value = FileName
            Params(10) = New SqlParameter("@p_ErrorStatus", SqlDbType.Int)
            Params(10).Direction = ParameterDirection.Output
            Params(11) = New SqlParameter("@p_OutputMessage", SqlDbType.VarChar, 4000)
            Params(11).Direction = ParameterDirection.Output
            Params(12) = New SqlParameter("@p_Type_Id", SqlDbType.Int)
            Params(12).Value = 1
            Params(13) = New SqlParameter("@p_Status_Id", SqlDbType.Int)
            Params(13).Value = 1
            Params(14) = New SqlParameter("@p_Next_EmpCode", SqlDbType.Int)
            Params(14).Value = 0
            DB.ExecuteNonQuery("SP_TMR_Assignment", Params)
            ErrorFlag = CInt(Params(10).Value)
            Message = CStr(Params(11).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1

            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "'); ")
        cl_script1.Append("        window.open('TMRAssignment.aspx', '_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        If ErrorFlag = 0 Then
            initializeControls()
        End If
    End Sub
End Class
