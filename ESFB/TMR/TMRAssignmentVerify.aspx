﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="TMRAssignmentVerify.aspx.vb" Inherits="TMR_TMRAssignmentVerify" %>

   <%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script src="../Script/jquery.min.js" type="text/javascript"></script>
    <style type="text/css">          
           #Button
    {
         width:100%;
        height:40px;
        font-weight:bold;
        line-height:40px;
        text-align:center;
        border-top-left-radius: 25px;
	    border-top-right-radius: 25px;
	    border-bottom-left-radius: 25px;
	    border-bottom-right-radius: 25px;
        cursor:pointer;
        background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
        color:#E0E0E0;
    }
        
    #Button:hover
    {
        background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        color:#036;
    }        
     .bg
     {
         background-color:#FFF;
     }
     .style1
        {
            width: 40%;
            height: 107px;
        }           
        .auto-style1 {
            width: 29%;
        }
        .auto-style2 {
            width: 23%;
        }
        .auto-style6 {
            width: 122px;
        }
    </style>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/jquery.js" type="text/javascript"></script>
  
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

     function SelectUnSelectAll()
        {
        
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");
            var i =1 ;       
            for (n = 0; n < row.length; n++) {                   
                 col = row[n].split("Ø");
                   
                if (document.getElementById("MainChk").checked == true)
                {
                    document.getElementById("chk" + i.toString()).checked = true;
                   
                  
                  document.getElementById("cmb" + col[0]).value=1;
                 }
                else 
                {
                    document.getElementById("chk" + i.toString()).checked = false;
                   document.getElementById("cmb" + col[0]).value=-1;
                }
                    
                i+=1;
                
            }
        }
        function SingleToggle(i)
        {
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");
            var i =1 ;       
            for (n = 0; n < row.length; n++) {                   
                 col = row[n].split("Ø");
                   
                if (document.getElementById("chk" + i.toString()).checked == false)
                {
                document.getElementById("MainChk").checked = false;
                   
                  
                  document.getElementById("cmb" + col[0]).value=-1;
                 }
               
                    
                i+=1;
                
            }
             
                
            
        }

         function Startval(Type,TmRNo, usr, status) {

          if(document.getElementById("txtRemarks" + TmRNo).value == "") {
                alert("Enter The Remarks");
                document.getElementById("txtRemarks" + TmRNo).focus();
                return;
            }
             if(document.getElementById("txtApprv" + TmRNo).value == "") {
                alert("Enter The Employee Code");
                document.getElementById("txtApprv" + TmRNo).focus();
                return;
            }
            if (status == 3 )
            {

                var Remarks = document.getElementById("txtRemarks" + TmRNo).value;

              var NextLevel= document.getElementById("txtApprv" + TmRNo).value;
                    var ToData = "1Ø" + Type + "Ø"  + TmRNo + "Ø" + usr + "Ø" + Remarks + "Ø" + status + "Ø" + NextLevel ;
                   
                    ToServer(ToData, 1);
            
           
           }
           else{
   
                var Remarks = document.getElementById("txtRemarks" + TmRNo).value;
                 
                       var NextLevel= document.getElementById("txtApprv" + TmRNo).value;
        
                     var ToData = "1Ø" + Type + "Ø"  + TmRNo + "Ø" + usr + "Ø" + Remarks + "Ø" + status + "Ø" + NextLevel ;
                   
                    ToServer(ToData, 1);
            
           } 
           
        }
        function  table_fill() {
            document.getElementById("<%= pnlbranch.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            
            tab += "<td style='width:1%;text-align:center;'>Sl No</td>";
             tab += "<td style='width:5%;text-align:left' >TMR No</td>";
            tab += "<td style='width:5%;text-align:left' >Cif No</td>";
            tab += "<td style='width:5%;text-align:left'>Customer Name</td>";
           tab += "<td style='width:5%;text-align:left'>No of Accounts</td>";
           tab += "<td style='width:10%;text-align:left'>Issue Noted</td>";
        
            tab += "<td style='width:8%;text-align:left' >Bucket</td>";
            tab += "<td style='width:5%;text-align:left'>Sub Bucket</td>";
           
       
            tab += "<td style='width:5%;text-align:left'>Vertical</td>";
            tab += "<td style='width:5%;text-align:left'>SubVertical</td>";
            tab += "<td style='width:5%;text-align:left'>Criticality</td>";
            tab += "<td style='width:5%;text-align:left'>Credit Channel</td>";
            tab += "<td style='width:5%;text-align:left'>Debit Channel</td>";
            tab += "<td style='width:5%;text-align:left'>Attachment</td>";
          
            tab += "<td style='width:10%;text-align:center'>Remarks</td>";
            tab += "<td style='width:5%;text-align:center'>Next Level</td>";
        
             tab += "<td style='width:5%;text-align:center;'>Approve</td>";
             tab += "<td style='width:5%;text-align:center;'>Reject</td>";
            tab += "</tr>";
              var i =  1;
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
               
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");
                
                for (n = 0; n < row.length-1; n++) {
                    col = row[n].split("Ø");
                   
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }


                    tab += "<td style='width:1%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:5%;text-align:left' >" + col[0] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:5%;text-align:left' >" + col[2] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[3] + "</td>";
                  
                    tab += "<td style='width:10%;text-align:left'>" + col[10] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[12] + " </td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[5] + " </td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[8] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[9] + "</td>";
                       
                        if (col[14] != "") {
                           
                      tab += "<td style='width:2%;text-align:left'><img id='ViewReport' src='../Image/attchment2.png' onclick='viewReport(" + col[14] + ","+col[18]+")' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' ></td>";
                    }
                    else {
                        tab += "<td style='width:5%;text-align:left'></td>";
                   }

                 
                    var txtBox = "<textarea id='txtRemarks" + col[0] + "' name='txtRemarks" + col[0] + "' style='width:80%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)' ></textarea>";

                    tab += "<td style='width:10%;text-align:left'>" + txtBox + "</td>";
                      var txtapprv = "<textarea id='txtApprv" + col[0] + "' name='txtApprv" + col[0] + "' style='width:80%; float:left;' maxlength='100'  ></textarea>";

                   tab += "<td style='width:5%;text-align:left'>" + txtapprv + "</td>";
                   

                        var LinkText = "Approve";

                        tab += "<td style='width:5%; text-align:center;  padding-left:5px; ' onclick=Startval(1,'" + col[0] + "','" + col[13] + "',2)><img id='imgMk' src='../Image/approve.PNG' title='Approve' Height='20px' Width='20px' style='cursor:pointer;'/></td>";

                        var LinkText = "Reject";
                        tab += "<td style='width:5%; text-align:center;  padding-left:5px; ' onclick=Startval(1,'" + col[0] + "','" + col[13] + "',3)><img id='imgMk' src='../Image/reject.PNG' title='Reject' Height='20px' Width='20px' style='cursor:pointer;'/></td>";
                    
                    
                    tab += "</tr>";
                    i+=1;
                }
                tab += "</table></div></div></div>";
                document.getElementById("<%= pnlbranch.ClientID %>").innerHTML = tab;

                //--------------------- Clearing Data ------------------------//
            }

        }
        function viewReport(tmrno) {
           

         
           if(tmrno!="")
           {
            window.open("ShowMultipleAttachments.aspx?tmrno=" + tmrno,"_blank");
        
            return false;
            }
        }
       
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");

            for (a = 0; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                 table_fill();
                alert(Data[1]);
                
                if (Data[0] == 0) window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
                
            }

            if (context == 3) {
               
                var Data = arg;
                var Count1 = Data.length - 1;
                if (Count1 > 0) {
                    document.getElementById("<%= hid_dtls.ClientID %>").value = Data;
                  
                   
                }
            }
        }

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

    </script>
    x</head></html><br />
<div  style="width:100%; height:auto; margin:0px auto; background-color: #A34747;">
    <br />
    <br />
    <div id = "divSection1" class = "sec1" style="width:97%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
     
     
        <br />
     <table ID="tblSec1" style="width:100%;height:90px;margin:0px auto; align-content:center">            
            <tr id="Tr1">
                <td style="text-align: center;">
                    <asp:Panel ID="pnlbranch" Style="width: 100%;  text-align: left; float: left;" runat="server">               

                    </asp:Panel>
                </td>
            </tr>
          
          <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                
              
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
        </table> 

          <table class="style1" style="width: 100%; margin: 0px auto; align-content:center">
           <tr>
               <td></td>
            <td class="auto-style6">
                   <asp:HiddenField ID="hid_temp" runat="server" />
            </td>   
                <td class="auto-style1">
                   <asp:HiddenField ID="hid_dtls" runat="server" />
            </td>
            <td></td>
               </tr>  

            </table>
     <br />
        <br />
        <br />
         
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
         
     
      
        <br />
        <br />  
    </div>
   <br />
    <br />
   
</div>   
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>