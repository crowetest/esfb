﻿<%@Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  CodeFile="TMRGeneration.aspx.vb" Inherits="TMR_TMRGeneration" %>


<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            //function window_onload() {   
            
            //}
         
            function setStartDate(sender, args) 
            {
                sender._selectedDate = new Date();
            }
           <%-- function SaveOnClick() {            
                

            }
            function FromServer(Arg, Context) {
       
            }--%>
            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        
        </script>
       <%-- <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>--%>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>

           
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                
              
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        CIF No</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtcifno" runat="server" onkeypress="NumericCheck(event)" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RFVtxtcifno" runat="server" ErrorMessage="Enter CIF No" ValidationGroup="validationSubmit" ControlToValidate="txtcifno" ForeColor="Red"></asp:RequiredFieldValidator>  
                      
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Customer Name</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtCustname" runat="server"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                         
                           <asp:RequiredFieldValidator ID="RFVtxtCustname" runat="server" ErrorMessage="Enter Customer Name" ValidationGroup="validationSubmit" ControlToValidate="txtCustname" ForeColor="Red"></asp:RequiredFieldValidator>  
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                  <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        No.of Accounts</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtnoofaccnt" runat="server" onkeypress="NumericCheck(event)"  Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>                        
                          <asp:RequiredFieldValidator ID="RFVtxtnoofaccnt" runat="server" ErrorMessage="Enter No of Accounts" ValidationGroup="validationSubmit" ControlToValidate="txtnoofaccnt" ForeColor="Red"></asp:RequiredFieldValidator>  
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Account Number</td>
                    <td style="width: 50%; text-align: left;">                        
                          <asp:TextBox ID="txtaccnt" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine"  Height="53px"
                            MaxLength="200" />
                        <asp:RequiredFieldValidator ID="RFVtxtaccnt" runat="server" ErrorMessage="Enter Account No" ValidationGroup="validationSubmit" ControlToValidate="txtaccnt" ForeColor="Red"></asp:RequiredFieldValidator>  
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                
               <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Description about Issue
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtdescription" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                          <asp:RequiredFieldValidator ID="RFVtxtdescription" runat="server" ErrorMessage="Enter Description" ValidationGroup="validationSubmit" ControlToValidate="txtdescription" ForeColor="Red"></asp:RequiredFieldValidator>  
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>

                  <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 50%; text-align: left;">                        
                        &nbsp;</td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Upload Document, 
                        if any<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span>
                    </td>
                    <td style="width: 100%; text-align: left;">
                        <input id="fupBrd" runat="server" cssclass="fileUpload" type="file" /></td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Target Date</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtReadyDt" runat="server" onkeypress="NumericCheck(event)"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="txtReadyDt_CalendarExtender" runat="server" 
                            Format="dd/MMM/yyyy" OnClientShowing="setStartDate" 
                            TargetControlID="txtReadyDt">
                        </ajaxToolkit:CalendarExtender>
                         <asp:RequiredFieldValidator ID="RFVtxtReadyDt" runat="server" ErrorMessage="Enter Description  Target Date" ValidationGroup="validationSubmit" ControlToValidate="txtReadyDt" ForeColor="Red"></asp:RequiredFieldValidator>  
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Width="67px" ValidationGroup="validationSubmit" />
                        &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnaccount" runat="server" />
            <asp:HiddenField ID="hdnDate" runat="server" />
           
        </div>
        <br />
    </div>
</asp:Content> 

