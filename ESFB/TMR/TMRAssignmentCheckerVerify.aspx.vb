﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class TMR_TMRAssignmentCheckerVerify
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim DT5 As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim QS1 As String
    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1480) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            'If GF.FormAccess(CInt(Session("UserID")), 1440) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Dim UserID As String = CStr(Session("UserID"))
            Dim BranchID As Integer = CInt(Session("BranchID"))
            Dim Clusterid As Integer = 0
            Dim BM As String = ""
            Dim CH As String = ""




            Me.Master.subtitle = "TMR Assignment Checker Verify Form"
            If Not IsPostBack Then

                DT1 = GF.GeneralFunctionForTMR("TMR022", UserID.ToString())


                Dim tmrNo As String = ""
                If (DT1.Rows.Count > 0) Then

                    For n As Integer = 0 To DT1.Rows.Count - 1
                        tmrNo = DT1.Rows(n)("tmrNo").ToString()
                        QS1 += DT1.Rows(n)("tmrNo").ToString() + "Ø" + DT1.Rows(n)("cifno").ToString() + "Ø" + DT1.Rows(n)("customerName").ToString() + "Ø" + DT1.Rows(n)("noOfAccounts").ToString() + "Ø" + DT1.Rows(n)("bucketName").ToString() +
                                "Ø" + DT1.Rows(n)("verticalName").ToString() + "Ø" + DT1.Rows(n)("subverticalName").ToString() + "Ø" + DT1.Rows(n)("criticalityName").ToString() + "Ø" + DT1.Rows(n)("creditChannel").ToString() + "Ø" + DT1.Rows(n)("debitChannel").ToString() +
                                "Ø" + DT1.Rows(n)("issueNoted").ToString() + "Ø" + DT1.Rows(n)("LeveId").ToString() + "Ø" + DT1.Rows(n)("subBucketName").ToString() + "Ø" + UserID + "Ø"



                        DT = GF.GeneralFunctionForTMR("TMR011", tmrNo.ToString() + "^3")
                        If (DT.Rows.Count > 0) Then
                            QS1 += DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() +
                                "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString() + "Ø"
                        End If
                        Me.hid_dtls.Value += QS1 + "Ñ"
                    Next


                End If


            End If


            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT3.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))



        If CInt(Data(0)) = 1 Then
            Dim Type As Integer = CInt(Data(1).ToString())
            Dim TmrNo As String = CStr(Data(2).ToString())
            Dim remarks As String = CStr(Data(4).ToString())
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim Status As Integer = CInt(Data(5).ToString())



            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@p_Type_Id", SqlDbType.Int)
                Params(0).Value = Type
                Params(1) = New SqlParameter("@p_TMR_No", SqlDbType.Int)
                Params(1).Value = TmrNo
                Params(2) = New SqlParameter("@p_User_Id", SqlDbType.Int)
                Params(2).Value = UserID
                Params(3) = New SqlParameter("@p_Status_Id", SqlDbType.Int)
                Params(3).Value = Status
                Params(4) = New SqlParameter("@p_Remarks", SqlDbType.VarChar)
                Params(4).Value = remarks
                Params(5) = New SqlParameter("@p_ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@p_OutputMessage", SqlDbType.VarChar, 4000)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@p_Next_EmpCode", SqlDbType.Int)
                Params(7).Value = 0

                DB.ExecuteNonQuery("SP_TMR_Assign_Checker_Verify", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If

    End Sub
#End Region
End Class
