﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Vet_DocumentStageWiseRpt1
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim TB As New Table
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 213) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim DT, DT_DRAFT, DT_OPINION, DT_COMMT, DT_VET, DT_ARC As New DataTable

            TB.Attributes.Add("width", "100%")

            Dim RowBG As Integer = 0
            Dim DR As DataRow

            RH.Heading(CStr(Session("FirmName")), TB, "DOCUMENT STAGE WISE REPORT", 100)

            RH.BlankRow(TB, 5)
            '---------------Instance
            DT_VET = DB.ExecuteDataSet("select v.SERVICE_ID,s.SERVICE_NAME,case when v.STATUS_ID = 0 then '(PENDING APROVE BY HOD) Requested By User' else case when v.STATUS_ID=1 then '(PENDING WORK DONE BY LEGAL CONSULTANT) Approved By HOD' else case when v.STATUS_ID=2 then 'REJECTED BY HOD' else case when v.STATUS_ID=3 then '(PENDING APPROVE BY COMPLIANCE OFFICER) Work Done By Legal Consultant' else case when v.STATUS_ID=4 then '(WORK COMPLETED) Approved By Compliance Offficer' else case when v.STATUS_ID=5 then 'REJECTED BY COMPLIANCE OFFICER' end end end end end end status,count(v.SERVICE_ID),v.STATUS_ID from VET_MASTER v,  VET_SERVICE s where v.SERVICE_ID=s.SERVICE_ID and v.SERVICE_ID<>4 group by v.SERVICE_ID,s.SERVICE_NAME,v.STATUS_ID union all select v.SERVICE_ID,s.SERVICE_NAME,case when v.STATUS_ID=0 then '(PENDING APROVE BY HOD) Requested By User' else case when v.STATUS_ID=1 then 'PENDING WORK DONE BY LEGAL CONSULTANT' else case when v.STATUS_ID=2 then 'REJECTED BY HOD' else case when v.STATUS_ID=3 then 'PENDING APPROVE BY COMPLIANCE OFFICER' else case when v.STATUS_ID=4 then 'PENDING RESPONSE UPDATE BY USER' else case when v.STATUS_ID=5 then 'PENDING REMARK UPDATE BY LEGAL' else case when v.STATUS_ID=6 then 'PENDING REMARK APPROVE BY COMPLIANCE OFFICER' else case when v.STATUS_ID=7 then 'PENDING HIGHER APPROVE (SIGN OFF BY COMPLIANCE OFFICER)' else case when v.STATUS_ID=8 then 'PENDING EXECUTION(TO BE EXECUTED)' else case when v.STATUS_ID=9 then 'PENDING USER UPDATE (TO BE CHANGE)' end end end end end end end end end end status,count(v.SERVICE_ID),v.STATUS_ID from VET_MASTER v,  VET_SERVICE s where v.SERVICE_ID=s.SERVICE_ID and v.SERVICE_ID=4 and v.STATUS_ID in(0,1,2,3,4,5,6,7,8,9) group by v.SERVICE_ID,s.SERVICE_NAME,v.STATUS_ID union all select v.SERVICE_ID,s.SERVICE_NAME,case when v.STATUS_ID=10 then 'TO BE NOT EXECUTE' else case when v.STATUS_ID=11 then 'PENDING HIGHER APPROVE (TO BE HOLD)' else case when v.STATUS_ID=12 then 'PENDING UPLOADING OF EXECUTED VERSION' else case when v.STATUS_ID=13 then 'PENDING VERIFICATION OF EXECUTED VERSION' else case when v.STATUS_ID=14 then 'PENDING USER UPDATE (VERIFY NOT OK)' else case when v.STATUS_ID=15 then 'VERIFY OK' else case when v.STATUS_ID=16 then 'REJECTED BY COMPLIANCE OFFICER' else case when v.STATUS_ID=17 then 'PENDING REQUEST APPROVE BY REPORTING HEAD' else case when v.STATUS_ID=18 then 'PENDING RESPONSE APPROVE BY REPORTING HEAD' else case when v.STATUS_ID=19 then 'DROP VET' end end end end end end end end end end status,count(v.SERVICE_ID),v.STATUS_ID from VET_MASTER v,  VET_SERVICE s where v.SERVICE_ID=s.SERVICE_ID and v.SERVICE_ID=4 and v.STATUS_ID in(10,11,12,13,14,15,16,17,18,19) group by v.SERVICE_ID,s.SERVICE_NAME,v.STATUS_ID order by 1").Tables(0)

            If DT_VET.Rows.Count > 0 Then
                Dim TRVET0 As New TableRow
                TRVET0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03 As New TableCell

                TRVET0_00.BorderWidth = "1"
                TRVET0_01.BorderWidth = "1"
                TRVET0_02.BorderWidth = "1"
                TRVET0_03.BorderWidth = "1"

                TRVET0_00.BorderStyle = BorderStyle.Solid
                TRVET0_01.BorderStyle = BorderStyle.Solid
                TRVET0_02.BorderStyle = BorderStyle.Solid
                TRVET0_03.BorderStyle = BorderStyle.Solid

                TRVET0_00.BorderColor = Drawing.Color.Silver
                TRVET0_01.BorderColor = Drawing.Color.Silver
                TRVET0_02.BorderColor = Drawing.Color.Silver
                TRVET0_03.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "c", "#")
                RH.AddColumn(TRVET0, TRVET0_01, 40, 40, "l", "Service")
                RH.AddColumn(TRVET0, TRVET0_02, 40, 40, "c", "Status")
                RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "c", "Count")

                Dim i As Integer = 0
                TB.Controls.Add(TRVET0)
                RH.BlankRow(TB, 1)

                Dim ServID As Integer = 1
                Dim NextID As Integer = 0

                For Each DR In DT_VET.Rows
                    Dim TRVET55 As New TableRow

                    Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03 As New TableCell

                    TRVET55_00.BorderWidth = "1"
                    TRVET55_01.BorderWidth = "1"
                    TRVET55_02.BorderWidth = "1"
                    TRVET55_03.BorderWidth = "1"

                    TRVET55_00.BorderStyle = BorderStyle.Solid
                    TRVET55_01.BorderStyle = BorderStyle.Solid
                    TRVET55_02.BorderStyle = BorderStyle.Solid
                    TRVET55_03.BorderStyle = BorderStyle.Solid

                    TRVET55_00.BorderColor = Drawing.Color.Silver
                    TRVET55_01.BorderColor = Drawing.Color.Silver
                    TRVET55_02.BorderColor = Drawing.Color.Silver
                    TRVET55_03.BorderColor = Drawing.Color.Silver

                    ServID = DR(0)
                    If (ServID = NextID) Then
                        RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "c", "")
                        RH.AddColumn(TRVET55, TRVET55_01, 40, 40, "l", "")
                    Else
                        i = i + 1
                        RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 40, 40, "l", DR(1))
                    End If
                    RH.AddColumn(TRVET55, TRVET55_02, 40, 40, "l", DR(2))
                    RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "c", "<a href='DocumentStageWiseRpt2.aspx?ServiceID=" + DR(0).ToString() + "&StatusID=" + DR(4).ToString() + "' style='text-align:right;'>" & DR(3) & "</a>")

                    TB.Controls.Add(TRVET55)
                    NextID = ServID
                Next
            End If

            pnDisplay.Controls.Add(TB)
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub

    Protected Sub cmd_Export_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
