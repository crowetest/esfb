﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf

Partial Class Vet_VStageWiseRpt3
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim RequestID As Integer
    Dim HeadName As String
    Dim Gf As New GeneralFunctions
    Dim From As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            HeadName = CStr(Request.QueryString.Get("DocumentToDraft"))
            Dim DT, DT1, DT2 As New DataTable
            Dim DTAudit As New DataTable
            tb.Attributes.Add("width", "100%")

            DT = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,upper(e.Emp_Name),case when v.VET_FLAG=1 then 'VET AND EXECUTE' else 'EXECUTE ONLY' end,a.DOCFILE_NAME,v.EXECUTE_DT,(select ex.FILE_NAME+'¶'+CONVERT(VARCHAR(10),ex.PkId) from DMS_ESFB.dbo.VET_EXECUTE_ATTACHMENT ex where ex.VET_ID=v.VET_ID and ex.REQUEST_ID=v.REQUEST_ID),a.PkId,v.VET_ID,v.DOC_ID,v.REQUEST_ID from VET_TRANSACTION_MASTER v, VET_DOCUMENT d, EMP_MASTER e, DMS_ESFB.dbo.VET_TRANSDOC_ATTACHMENT a where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and v.EXECUTE_BY=e.Emp_Code and v.VET_ID=a.VET_ID and v.REQUEST_ID=" & RequestID & " order by v.VET_ID").Tables(0)
            RH.Heading(Session("FirmName"), tb, "VET DOCUMENT DETAILS-" + HeadName + "", 100)
            Dim RowBG As Integer = 0
            Dim DR, DR1 As DataRow
            Dim I As Integer = 0

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No")
            RH.AddColumn(TRHead, TRHead_01, 15, 15, "l", "Document Name")
            RH.AddColumn(TRHead, TRHead_02, 15, 15, "l", "Executant")
            RH.AddColumn(TRHead, TRHead_03, 15, 15, "l", "Document Type")
            RH.AddColumn(TRHead, TRHead_04, 15, 15, "l", "Attachment")
            RH.AddColumn(TRHead, TRHead_05, 10, 10, "l", "Execute Date")
            RH.AddColumn(TRHead, TRHead_06, 15, 15, "l", "Executed Version")
            'RH.AddColumn(TRHead, TRHead_07, 10, 10, "l", "Redrafted Documents")

            tb.Controls.Add(TRHead)
            For Each DR In DT.Rows
                I = I + 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString()) 'SlNO
                RH.AddColumn(TR3, TR3_01, 15, 15, "l", DR(0))
                RH.AddColumn(TR3, TR3_02, 15, 15, "l", DR(1))
                RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(2))
                RH.AddColumn(TR3, TR3_04, 15, 15, "l", "<a href='ShowVetFormat.aspx?PkID=" + DR(6).ToString + "&VetID=" + DR(7).ToString + "' style='text-align:right;' target='_blank' >" & DR(3) & "</a>")
                If IsDBNull(DR(4)) Then
                    RH.AddColumn(TR3, TR3_05, 10, 10, "l", "")
                Else
                    RH.AddColumn(TR3, TR3_05, 10, 10, "l", Format(DR(4), "dd/MMM/yyyy"))
                End If
                If IsDBNull(DR(5)) Then
                    RH.AddColumn(TR3, TR3_06, 15, 15, "l", "")
                Else
                    Dim ExeData() As String = DR(5).Split(CChar("¶"))
                    Dim ExeFile As String = ExeData(0)
                    Dim ExePkId As Integer = CInt(ExeData(1))

                    RH.AddColumn(TR3, TR3_06, 15, 15, "l", "<a href='ShowVetFormat.aspx?PkID=" + ExePkId.ToString + "&VetID=0' style='text-align:right;' target='_blank' >" & ExeFile & "</a>")
                End If

                'DT1 = DB.ExecuteDataSet("select COUNT(*) from DMS_ESFB.dbo.VET_REDRAFT_ATTACHMENT where REQUEST_ID=" & DR(9) & " and VET_ID= " & DR(7) & " and DOC_ID= " & DR(8) & "").Tables(0)
                'If DT1.Rows(0)(0) > 0 Then
                '    RH.AddColumn(TR3, TR3_07, 10, 10, "l", "<a href='DocumentStageWiseRpt4.aspx?ServiceID=4 &StatusID=4 &RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank'><img id='imgReport' src='../Image/attchment2.png' title='Redrafted Document' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                'Else
                '    RH.AddColumn(TR3, TR3_07, 10, 10, "l", "")
                'End If

                tb.Controls.Add(TR3)
            Next

            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = Gf.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub


    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = Gf.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub


End Class
