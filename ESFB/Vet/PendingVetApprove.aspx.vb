﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Vet_PendingVetApprove
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim TB As New Table
    Dim RptID As Integer
    Dim UserID As Integer
    Dim i As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 141) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            UserID = Int(Session("UserID"))

            Dim DT, DT_DRAFT, DT_OPINION, DT_COMMT, DT_VET, DT_ARC As New DataTable

            TB.Attributes.Add("width", "100%")

            Dim RowBG As Integer = 0
            Dim DR As DataRow

            RH.Heading(CStr(Session("FirmName")), TB, "PENDING APPROVAL", 100)

            Dim sql As String = "SELECT d.DRAFT_NAME,upper(v.STATEMENT_FACT),e.Emp_Name+' - '+m.Department_Name,v.REQUEST_ID FROM VET_MASTER v, VET_DRAFT d, DEPARTMENT_MASTER m, EMP_MASTER e WHERE v.DRAFT_ID=d.DRAFT_ID and v.STATUS_ID=3 and v.SERVICE_ID=1 and v.DRAFT_ID IS NOT NULL and v.REQUEST_DEPT=m.Department_ID and v.REQUEST_BY=e.Emp_Code"
            DT_DRAFT = DB.ExecuteDataSet(sql).Tables(0)

            If DT_DRAFT.Rows.Count > 0 Then
                RH.SubHeading(TB, 100, "l", "DRAFTING")
                RH.BlankRow(TB, 5)

                Dim TR0 As New TableRow

                TR0.BorderWidth = "1"
                TR0.BorderStyle = BorderStyle.Solid

                TR0.BackColor = Drawing.Color.WhiteSmoke
                Dim TR0_00, TR0_01, TR0_02, TR0_03, TR0_04 As New TableCell
                RH.AddColumn(TR0, TR0_00, 10, 10, "l", "#")
                RH.AddColumn(TR0, TR0_01, 30, 30, "l", "Document To Draft")
                RH.AddColumn(TR0, TR0_02, 30, 30, "l", "Statement of Facts")
                RH.AddColumn(TR0, TR0_03, 20, 20, "l", "Request By")
                RH.AddColumn(TR0, TR0_04, 10, 15, "c", "")

                TR0_00.BorderWidth = "1"
                TR0_01.BorderWidth = "1"
                TR0_02.BorderWidth = "1"
                TR0_03.BorderWidth = "1"
                TR0_04.BorderWidth = "1"

                TR0_00.BorderStyle = BorderStyle.Solid
                TR0_01.BorderStyle = BorderStyle.Solid
                TR0_02.BorderStyle = BorderStyle.Solid
                TR0_03.BorderStyle = BorderStyle.Solid
                TR0_04.BorderStyle = BorderStyle.Solid

                TR0_00.BorderColor = Drawing.Color.Silver
                TR0_01.BorderColor = Drawing.Color.Silver
                TR0_02.BorderColor = Drawing.Color.Silver
                TR0_03.BorderColor = Drawing.Color.Silver
                TR0_04.BorderColor = Drawing.Color.Silver

                i = 0
                TB.Controls.Add(TR0)

                RH.BlankRow(TB, 1)

                For Each DR In DT_DRAFT.Rows
                    Dim TR55 As New TableRow
                    i = i + 1
                    Dim TR55_00, TR55_01, TR55_02, TR55_03, TR55_04 As New TableCell

                    TR55_00.BorderWidth = "1"
                    TR55_01.BorderWidth = "1"
                    TR55_02.BorderWidth = "1"
                    TR55_03.BorderWidth = "1"
                    TR55_04.BorderWidth = "1"

                    TR55_00.BorderStyle = BorderStyle.Solid
                    TR55_01.BorderStyle = BorderStyle.Solid
                    TR55_02.BorderStyle = BorderStyle.Solid
                    TR55_03.BorderStyle = BorderStyle.Solid
                    TR55_04.BorderStyle = BorderStyle.Solid

                    TR55_00.BorderColor = Drawing.Color.Silver
                    TR55_01.BorderColor = Drawing.Color.Silver
                    TR55_02.BorderColor = Drawing.Color.Silver
                    TR55_03.BorderColor = Drawing.Color.Silver
                    TR55_04.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TR55, TR55_00, 10, 10, "l", i)
                    RH.AddColumn(TR55, TR55_01, 30, 30, "l", DR(0))
                    RH.AddColumn(TR55, TR55_02, 30, 30, "l", DR(1))
                    RH.AddColumn(TR55, TR55_03, 20, 20, "l", DR(2))
                    RH.AddColumn(TR55, TR55_04, 10, 15, "c", "<a href='DraftDocumentApp.aspx?RequestID=" + DR(3).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/approve.png' title='Approve' Height='20px' Width='20px' style='cursor:pointer;'/></a>")
                    TB.Controls.Add(TR55)
                Next
            End If
            '---------------------- 
            DT_OPINION = DB.ExecuteDataSet("SELECT upper(v.USER_OPINION),upper(v.STATEMENT_FACT),e.Emp_Name+' - '+d.Department_Name,v.REQUEST_ID FROM VET_MASTER v, DEPARTMENT_MASTER d, EMP_MASTER e WHERE v.STATUS_ID=3 and v.SERVICE_ID=2 and v.REQUEST_BY=e.Emp_Code and v.REQUEST_DEPT=d.Department_ID").Tables(0)

            If DT_OPINION.Rows.Count > 0 Then
                RH.BlankRow(TB, 17)
                RH.SubHeading(TB, 100, "l", "OPINION")
                RH.BlankRow(TB, 5)

                Dim TRA0 As New TableRow
                TRA0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRA0_00, TRA0_01, TRA0_02, TRA0_03, TRA0_04 As New TableCell

                TRA0_00.BorderWidth = "1"
                TRA0_01.BorderWidth = "1"
                TRA0_02.BorderWidth = "1"
                TRA0_03.BorderWidth = "1"
                TRA0_04.BorderWidth = "1"

                TRA0_00.BorderStyle = BorderStyle.Solid
                TRA0_01.BorderStyle = BorderStyle.Solid
                TRA0_02.BorderStyle = BorderStyle.Solid
                TRA0_03.BorderStyle = BorderStyle.Solid
                TRA0_04.BorderStyle = BorderStyle.Solid

                TRA0_00.BorderColor = Drawing.Color.Silver
                TRA0_01.BorderColor = Drawing.Color.Silver
                TRA0_02.BorderColor = Drawing.Color.Silver
                TRA0_03.BorderColor = Drawing.Color.Silver
                TRA0_04.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRA0, TRA0_00, 10, 10, "l", "#")
                RH.AddColumn(TRA0, TRA0_01, 30, 30, "l", "Opinion")
                RH.AddColumn(TRA0, TRA0_02, 30, 30, "l", "Statement Of Facts")
                RH.AddColumn(TRA0, TRA0_03, 20, 20, "l", "Request By")
                RH.AddColumn(TRA0, TRA0_04, 10, 10, "c", "")

                i = 0
                TB.Controls.Add(TRA0)
                RH.BlankRow(TB, 1)

                For Each DR In DT_OPINION.Rows
                    Dim TR551 As New TableRow
                    i = i + 1
                    Dim TR551_00, TR551_01, TR551_02, TR551_03, TR551_04 As New TableCell

                    TR551_00.BorderWidth = "1"
                    TR551_01.BorderWidth = "1"
                    TR551_02.BorderWidth = "1"
                    TR551_03.BorderWidth = "1"
                    TR551_04.BorderWidth = "1"

                    TR551_00.BorderStyle = BorderStyle.Solid
                    TR551_01.BorderStyle = BorderStyle.Solid
                    TR551_02.BorderStyle = BorderStyle.Solid
                    TR551_03.BorderStyle = BorderStyle.Solid
                    TR551_04.BorderStyle = BorderStyle.Solid

                    TR551_00.BorderColor = Drawing.Color.Silver
                    TR551_01.BorderColor = Drawing.Color.Silver
                    TR551_02.BorderColor = Drawing.Color.Silver
                    TR551_03.BorderColor = Drawing.Color.Silver
                    TR551_04.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TR551, TR551_00, 10, 10, "l", i)
                    RH.AddColumn(TR551, TR551_01, 30, 30, "l", DR(0))
                    RH.AddColumn(TR551, TR551_02, 30, 30, "l", DR(1))
                    RH.AddColumn(TR551, TR551_03, 20, 20, "l", DR(2))
                    RH.AddColumn(TR551, TR551_04, 10, 10, "c", "<a href='OpinionApp.aspx?RequestID=" + DR(3).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/approve.png' title='Approve' Height='20px' Width='20px' style='cursor:pointer;'/></a>")

                    TB.Controls.Add(TR551)
                Next
            End If

            '---------------
            Dim Query As String = "SELECT d.DOCUMENT_NAME,upper(v.COMT_PURPOSE),upper(v.COMT_EXPECTATION),e.Emp_Name+' - '+m.Department_Name,v.REQUEST_ID FROM VET_MASTER v, VET_DOCUMENT d, DEPARTMENT_MASTER m, EMP_MASTER e WHERE v.COMT_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and d.DOCUMENT_TYPE=1 and v.STATUS_ID=3 and v.SERVICE_ID=3 and v.COMT_DOCID is not null and v.REQUEST_DEPT=m.Department_ID and v.REQUEST_BY=e.Emp_Code"
            DT_COMMT = DB.ExecuteDataSet(Query).Tables(0)

            If DT_COMMT.Rows.Count > 0 Then
                RH.BlankRow(TB, 17)
                RH.SubHeading(TB, 100, "l", "COMMENTS ON DOCUMENTS")
                RH.BlankRow(TB, 5)

                Dim TRB0 As New TableRow
                TRB0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRB0_00, TRB0_01, TRB0_02, TRB0_03, TRB0_04, TRB0_05 As New TableCell

                TRB0_00.BorderWidth = "1"
                TRB0_01.BorderWidth = "1"
                TRB0_02.BorderWidth = "1"
                TRB0_03.BorderWidth = "1"
                TRB0_04.BorderWidth = "1"
                TRB0_05.BorderWidth = "1"

                TRB0_00.BorderStyle = BorderStyle.Solid
                TRB0_01.BorderStyle = BorderStyle.Solid
                TRB0_02.BorderStyle = BorderStyle.Solid
                TRB0_03.BorderStyle = BorderStyle.Solid
                TRB0_04.BorderStyle = BorderStyle.Solid
                TRB0_05.BorderStyle = BorderStyle.Solid

                TRB0_00.BorderColor = Drawing.Color.Silver
                TRB0_01.BorderColor = Drawing.Color.Silver
                TRB0_02.BorderColor = Drawing.Color.Silver
                TRB0_03.BorderColor = Drawing.Color.Silver
                TRB0_04.BorderColor = Drawing.Color.Silver
                TRB0_05.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRB0, TRB0_00, 10, 10, "l", "#")
                RH.AddColumn(TRB0, TRB0_01, 20, 20, "l", "Nature Of Document")
                RH.AddColumn(TRB0, TRB0_02, 20, 20, "l", "Purpose")
                RH.AddColumn(TRB0, TRB0_03, 20, 20, "l", "Expectation")
                RH.AddColumn(TRB0, TRB0_04, 20, 20, "l", "Request By")
                RH.AddColumn(TRB0, TRB0_05, 10, 10, "c", "")

                i = 0
                TB.Controls.Add(TRB0)
                RH.BlankRow(TB, 1)

                For Each DR In DT_COMMT.Rows
                    Dim TRB55 As New TableRow
                    i = i + 1
                    Dim TRB55_00, TRB55_01, TRB55_02, TRB55_03, TRB55_04, TRB55_05 As New TableCell

                    TRB55_00.BorderWidth = "1"
                    TRB55_01.BorderWidth = "1"
                    TRB55_02.BorderWidth = "1"
                    TRB55_03.BorderWidth = "1"
                    TRB55_04.BorderWidth = "1"
                    TRB55_05.BorderWidth = "1"

                    TRB55_00.BorderStyle = BorderStyle.Solid
                    TRB55_01.BorderStyle = BorderStyle.Solid
                    TRB55_02.BorderStyle = BorderStyle.Solid
                    TRB55_03.BorderStyle = BorderStyle.Solid
                    TRB55_04.BorderStyle = BorderStyle.Solid
                    TRB55_05.BorderStyle = BorderStyle.Solid

                    TRB55_00.BorderColor = Drawing.Color.Silver
                    TRB55_01.BorderColor = Drawing.Color.Silver
                    TRB55_02.BorderColor = Drawing.Color.Silver
                    TRB55_03.BorderColor = Drawing.Color.Silver
                    TRB55_04.BorderColor = Drawing.Color.Silver
                    TRB55_05.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRB55, TRB55_00, 10, 10, "l", i)
                    RH.AddColumn(TRB55, TRB55_01, 20, 20, "l", DR(0))
                    RH.AddColumn(TRB55, TRB55_02, 20, 20, "l", DR(1))
                    RH.AddColumn(TRB55, TRB55_03, 20, 20, "l", DR(2))
                    RH.AddColumn(TRB55, TRB55_04, 20, 20, "l", DR(3))
                    RH.AddColumn(TRB55, TRB55_05, 10, 10, "c", "<a href='CommentsDocumentApp.aspx?RequestID=" + DR(4).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/approve.png' title='Approve' Height='20px' Width='20px' style='cursor:pointer;'/></a>")

                    TB.Controls.Add(TRB55)
                Next
            End If

            '---------------

            DT_VET = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT),e.Emp_Name+' - '+d.Department_Name,v.REQUEST_ID,V.HIGHER_FLAG from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, DEPARTMENT_MASTER d, EMP_MASTER e where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.SERVICE_ID=4 and v.STATUS_ID=3 and v.REQUEST_DEPT=d.Department_ID and v.REQUEST_BY=e.Emp_Code").Tables(0)

            If DT_VET.Rows.Count > 0 Then
                RH.BlankRow(TB, 17)
                RH.SubHeading(TB, 100, "l", "VETTING FOR EXECUTING TRANSACTION")
                RH.BlankRow(TB, 5)

                Dim TRVETT0 As New TableRow
                TRVETT0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRVETT0_00, TRVETT0_01, TRVETT0_02, TRVETT0_03, TRVETT0_04, TRVETT0_05 As New TableCell

                TRVETT0_00.BorderWidth = "1"
                TRVETT0_01.BorderWidth = "1"
                TRVETT0_02.BorderWidth = "1"
                TRVETT0_03.BorderWidth = "1"
                TRVETT0_04.BorderWidth = "1"
                TRVETT0_05.BorderWidth = "1"

                TRVETT0_00.BorderStyle = BorderStyle.Solid
                TRVETT0_01.BorderStyle = BorderStyle.Solid
                TRVETT0_02.BorderStyle = BorderStyle.Solid
                TRVETT0_03.BorderStyle = BorderStyle.Solid
                TRVETT0_04.BorderStyle = BorderStyle.Solid
                TRVETT0_05.BorderStyle = BorderStyle.Solid

                TRVETT0_00.BorderColor = Drawing.Color.Silver
                TRVETT0_01.BorderColor = Drawing.Color.Silver
                TRVETT0_02.BorderColor = Drawing.Color.Silver
                TRVETT0_03.BorderColor = Drawing.Color.Silver
                TRVETT0_04.BorderColor = Drawing.Color.Silver
                TRVETT0_05.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRVETT0, TRVETT0_00, 10, 10, "l", "#")
                RH.AddColumn(TRVETT0, TRVETT0_01, 20, 20, "l", "Nature Of Transaction")
                RH.AddColumn(TRVETT0, TRVETT0_02, 20, 20, "l", "Other Party")
                RH.AddColumn(TRVETT0, TRVETT0_03, 20, 20, "l", "Statement Of Facts")
                RH.AddColumn(TRVETT0, TRVETT0_04, 20, 20, "l", "Request By")
                RH.AddColumn(TRVETT0, TRVETT0_05, 10, 10, "c", "")

                i = 0
                TB.Controls.Add(TRVETT0)

                RH.BlankRow(TB, 1)

                For Each DR In DT_VET.Rows
                    Dim TRVET55 As New TableRow
                    i = i + 1
                    Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05 As New TableCell

                    TRVET55_00.BorderWidth = "1"
                    TRVET55_01.BorderWidth = "1"
                    TRVET55_02.BorderWidth = "1"
                    TRVET55_03.BorderWidth = "1"
                    TRVET55_04.BorderWidth = "1"
                    TRVET55_05.BorderWidth = "1"

                    TRVET55_00.BorderStyle = BorderStyle.Solid
                    TRVET55_01.BorderStyle = BorderStyle.Solid
                    TRVET55_02.BorderStyle = BorderStyle.Solid
                    TRVET55_03.BorderStyle = BorderStyle.Solid
                    TRVET55_04.BorderStyle = BorderStyle.Solid
                    TRVET55_05.BorderStyle = BorderStyle.Solid

                    TRVET55_00.BorderColor = Drawing.Color.Silver
                    TRVET55_01.BorderColor = Drawing.Color.Silver
                    TRVET55_02.BorderColor = Drawing.Color.Silver
                    TRVET55_03.BorderColor = Drawing.Color.Silver
                    TRVET55_04.BorderColor = Drawing.Color.Silver
                    TRVET55_05.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "l", i)
                    RH.AddColumn(TRVET55, TRVET55_01, 20, 20, "l", DR(0))
                    RH.AddColumn(TRVET55, TRVET55_02, 20, 20, "l", DR(1))
                    RH.AddColumn(TRVET55, TRVET55_03, 20, 20, "l", DR(2))
                    RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", DR(3))
                    RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "c", "<a href='VetTransactionApprove.aspx?RequestID=" + DR(4).ToString() + "&StatusID=4&HigherFlag=0' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/approve.png' title='Approve' Height='20px' Width='20px' style='cursor:pointer;'/></a>")

                    TB.Controls.Add(TRVET55)
                Next
            End If

            DT_VET = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT),e.Emp_Name+' - '+d.Department_Name,v.REQUEST_ID,V.HIGHER_FLAG from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, DEPARTMENT_MASTER d, EMP_MASTER e where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.SERVICE_ID=4 and v.STATUS_ID=6 and v.REQUEST_DEPT=d.Department_ID and v.REQUEST_BY=e.Emp_Code").Tables(0)

            '------------------------
            If DT_VET.Rows.Count > 0 Then
                RH.BlankRow(TB, 17)
                RH.SubHeading(TB, 100, "l", "VET REMARK APPROVE")
                RH.BlankRow(TB, 5)

                Dim TRVET0 As New TableRow
                TRVET0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05 As New TableCell

                TRVET0_00.BorderWidth = "1"
                TRVET0_01.BorderWidth = "1"
                TRVET0_02.BorderWidth = "1"
                TRVET0_03.BorderWidth = "1"
                TRVET0_04.BorderWidth = "1"
                TRVET0_05.BorderWidth = "1"

                TRVET0_00.BorderStyle = BorderStyle.Solid
                TRVET0_01.BorderStyle = BorderStyle.Solid
                TRVET0_02.BorderStyle = BorderStyle.Solid
                TRVET0_03.BorderStyle = BorderStyle.Solid
                TRVET0_04.BorderStyle = BorderStyle.Solid
                TRVET0_05.BorderStyle = BorderStyle.Solid

                TRVET0_00.BorderColor = Drawing.Color.Silver
                TRVET0_01.BorderColor = Drawing.Color.Silver
                TRVET0_02.BorderColor = Drawing.Color.Silver
                TRVET0_03.BorderColor = Drawing.Color.Silver
                TRVET0_04.BorderColor = Drawing.Color.Silver
                TRVET0_05.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "l", "#")
                RH.AddColumn(TRVET0, TRVET0_01, 20, 20, "l", "Nature Of Transaction")
                RH.AddColumn(TRVET0, TRVET0_02, 20, 20, "l", "Other Party")
                RH.AddColumn(TRVET0, TRVET0_03, 20, 20, "l", "Statement Of Facts")
                RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "l", "Request By")
                RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "c", "")

                Dim i As Integer = 0
                TB.Controls.Add(TRVET0)
                RH.BlankRow(TB, 1)

                For Each DR In DT_VET.Rows
                    Dim TRVET55 As New TableRow
                    i = i + 1
                    Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05 As New TableCell

                    TRVET55_00.BorderWidth = "1"
                    TRVET55_01.BorderWidth = "1"
                    TRVET55_02.BorderWidth = "1"
                    TRVET55_03.BorderWidth = "1"
                    TRVET55_04.BorderWidth = "1"
                    TRVET55_05.BorderWidth = "1"

                    TRVET55_00.BorderStyle = BorderStyle.Solid
                    TRVET55_01.BorderStyle = BorderStyle.Solid
                    TRVET55_02.BorderStyle = BorderStyle.Solid
                    TRVET55_03.BorderStyle = BorderStyle.Solid
                    TRVET55_04.BorderStyle = BorderStyle.Solid
                    TRVET55_05.BorderStyle = BorderStyle.Solid

                    TRVET55_00.BorderColor = Drawing.Color.Silver
                    TRVET55_01.BorderColor = Drawing.Color.Silver
                    TRVET55_02.BorderColor = Drawing.Color.Silver
                    TRVET55_03.BorderColor = Drawing.Color.Silver
                    TRVET55_04.BorderColor = Drawing.Color.Silver
                    TRVET55_05.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "l", i)
                    RH.AddColumn(TRVET55, TRVET55_01, 20, 20, "l", DR(0))
                    RH.AddColumn(TRVET55, TRVET55_02, 20, 20, "l", DR(1))
                    RH.AddColumn(TRVET55, TRVET55_03, 20, 20, "l", DR(2))
                    RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", DR(3))
                    RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "c", "<a href='VetTransactionApprove.aspx?RequestID=" + DR(4).ToString() + "&StatusID=6&HigherFlag=" & DR(5).ToString() & "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/approve.png' title='Approve' Height='20px' Width='20px' style='cursor:pointer;'/></a>")

                    TB.Controls.Add(TRVET55)
                Next
            End If

            '------------------
            DT_ARC = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,n.NATURE_NAME,p.PARTY_NAME,e.Emp_Name+' - '+m.Department_Name,v.REQUEST_ID FROM VET_MASTER v, VET_DOCUMENT d, VET_NATURE_TRANSACTION n, VET_PARTY p, DEPARTMENT_MASTER m, EMP_MASTER e WHERE v.ARCH_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and d.DOCUMENT_TYPE=3 and v.STATUS_ID=3 and v.SERVICE_ID=5 and v.ARCH_DOCID is not null and v.REQUEST_DEPT=m.Department_ID and v.REQUEST_BY=e.Emp_Code").Tables(0)

            If DT_ARC.Rows.Count > 0 Then
                RH.BlankRow(TB, 17)
                RH.SubHeading(TB, 100, "l", "ARCHIVING OF EXECUTED DOCUMENTS")
                RH.BlankRow(TB, 5)

                Dim TRAR0 As New TableRow
                TRAR0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRAR0_00, TRAR0_01, TRAR0_02, TRAR0_03, TRAR0_04, TRAR0_05 As New TableCell

                TRAR0_00.BorderWidth = "1"
                TRAR0_01.BorderWidth = "1"
                TRAR0_02.BorderWidth = "1"
                TRAR0_03.BorderWidth = "1"
                TRAR0_04.BorderWidth = "1"
                TRAR0_05.BorderWidth = "1"

                TRAR0_00.BorderStyle = BorderStyle.Solid
                TRAR0_01.BorderStyle = BorderStyle.Solid
                TRAR0_02.BorderStyle = BorderStyle.Solid
                TRAR0_03.BorderStyle = BorderStyle.Solid
                TRAR0_04.BorderStyle = BorderStyle.Solid
                TRAR0_05.BorderStyle = BorderStyle.Solid

                TRAR0_00.BorderColor = Drawing.Color.Silver
                TRAR0_01.BorderColor = Drawing.Color.Silver
                TRAR0_02.BorderColor = Drawing.Color.Silver
                TRAR0_03.BorderColor = Drawing.Color.Silver
                TRAR0_04.BorderColor = Drawing.Color.Silver
                TRAR0_05.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRAR0, TRAR0_00, 10, 10, "l", "#")
                RH.AddColumn(TRAR0, TRAR0_01, 20, 20, "l", "Document to be Archived")
                RH.AddColumn(TRAR0, TRAR0_02, 20, 20, "l", "Nature Of Transaction")
                RH.AddColumn(TRAR0, TRAR0_03, 20, 20, "l", "Other Party")
                RH.AddColumn(TRAR0, TRAR0_04, 20, 20, "l", "Request By")
                RH.AddColumn(TRAR0, TRAR0_05, 10, 10, "c", "")

                i = 0
                TB.Controls.Add(TRAR0)

                RH.BlankRow(TB, 1)
                For Each DR In DT_ARC.Rows
                    Dim TRAR55 As New TableRow
                    i = i + 1
                    Dim TRAR55_00, TRAR55_01, TRAR55_02, TRAR55_03, TRAR55_04, TRAR55_05 As New TableCell

                    TRAR55_00.BorderWidth = "1"
                    TRAR55_01.BorderWidth = "1"
                    TRAR55_02.BorderWidth = "1"
                    TRAR55_03.BorderWidth = "1"
                    TRAR55_04.BorderWidth = "1"
                    TRAR55_05.BorderWidth = "1"

                    TRAR55_00.BorderStyle = BorderStyle.Solid
                    TRAR55_01.BorderStyle = BorderStyle.Solid
                    TRAR55_02.BorderStyle = BorderStyle.Solid
                    TRAR55_03.BorderStyle = BorderStyle.Solid
                    TRAR55_04.BorderStyle = BorderStyle.Solid
                    TRAR55_05.BorderStyle = BorderStyle.Solid

                    TRAR55_00.BorderColor = Drawing.Color.Silver
                    TRAR55_01.BorderColor = Drawing.Color.Silver
                    TRAR55_02.BorderColor = Drawing.Color.Silver
                    TRAR55_03.BorderColor = Drawing.Color.Silver
                    TRAR55_04.BorderColor = Drawing.Color.Silver
                    TRAR55_05.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRAR55, TRAR55_00, 10, 10, "l", i)
                    RH.AddColumn(TRAR55, TRAR55_01, 20, 20, "l", DR(0))
                    RH.AddColumn(TRAR55, TRAR55_02, 20, 20, "l", DR(1))
                    RH.AddColumn(TRAR55, TRAR55_03, 20, 20, "l", DR(2))
                    RH.AddColumn(TRAR55, TRAR55_04, 20, 20, "l", DR(3))
                    RH.AddColumn(TRAR55, TRAR55_05, 10, 10, "c", "<a href='ArchivingDocumentApp.aspx?RequestID=" + DR(4).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/approve.png' title='Approve' Height='20px' Width='20px' style='cursor:pointer;'/></a>")

                    TB.Controls.Add(TRAR55)
                Next
            End If
            pnDisplay.Controls.Add(TB)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
