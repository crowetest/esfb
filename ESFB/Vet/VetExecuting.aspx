﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="VetExecuting.aspx.vb" Inherits="Vet_VetExecuting" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {
                document.getElementById("colResolution").style.display = "none";
                document.getElementById("rowDoc").style.display = "none";
                ToServer("1ʘ", 1);
            }
            function DocumentOnChange() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var Document = document.getElementById("<%= cmbDocument.ClientID %>").value.split('^');
                var DocID = Document[0];
                var VetID = Document[1];
                ToServer("2ʘ" + RequestID + "ʘ" + DocID + "ʘ" + VetID, 2);
            }
            function ViewAttachment(Value) {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowFormat.aspx?RequestID=" + RequestID + "&Value=" + Value + "&Id=0");
                return false;
            }
            function btnExecute_onclick() 
            {                
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var Document = document.getElementById("<%= cmbDocument.ClientID %>").value.split('^');
                var DocID = Document[0];
                var VetID = Document[1];
                if (document.getElementById("<%= txtExecuteDt.ClientID %>").value == "") {
                    alert("Execute Date");
                    document.getElementById("<%= txtExecuteDt.ClientID %>").focus();
                    return false;
                }
                var ExecuteDt = document.getElementById("<%= txtExecuteDt.ClientID %>").value;
                ToServer("3ʘ" + RequestID + "ʘ" + DocID + "ʘ" + VetID + "ʘ" + ExecuteDt, 3);
            }
            function setStartDate(sender, args) {
                sender._endDate = new Date();
            }   
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {
                        //SBIÆ  Ratheesh P RÆ  ADFSDFSDF SDSDFSDFSDFÆ  AVAILING OF LOANÆ 0Æ  2Æ  
                        var Data = Arg.split("Æ");
                        document.getElementById("<%= txtParty.ClientID %>").value = Data[0];
                        document.getElementById("<%= txtStatement.ClientID %>").value = Data[2];
                        document.getElementById("<%= txtNature.ClientID %>").value = Data[3];
                        document.getElementById("<%= txtTerms.ClientID %>").value = Data[6];
                        if (Data[4] == 1)
                            document.getElementById("colResolution").style.display = "";
                        else
                            document.getElementById("colResolution").style.display = "none";
                        if (Data[5] > 0) {
                            ComboFill(Data[7], "<%= cmbDocument.ClientID %>");
                            DocumentOnChange();
                        }
                        break;
                    }
                case 2:
                    {
                        if (Arg != "") {
                            document.getElementById("<%= hdnDocDtl.ClientID %>").value = Arg;
                            DiplayTable();
                        }
                        else {
                            DisplayNoAttach();
                        }
                        break;
                    }
                case 3: // Confirm Execute
                    {
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        if (Data[0] == 0) {
                            if (Data[2] > 0) {
                                window.open("VetExecuting.aspx?RequestID=" + Data[3] + "", '_self');
                            }
                            else {
                                window.open("PendingExecution.aspx", '_self');
                            }
                        }
                        break;
                    }
                }
            }
            function DiplayTable() {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:45%;text-align:left'>DOCUMENT&nbsp;NAME</td>";
                Tab += "<td style='width:45%;text-align:left'>FILE&nbsp;NAME</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                if (document.getElementById("<%= hdnDocDtl.ClientID %>").value != "") {
                    document.getElementById("rowDoc").style.display = "";
                    var HidArg = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("¶");
                    var RowCount = HidArg.length - 1;
                    var j;
                    var Total = 0;
                    var row_bg1 = 0;
                    var SlNo = 0;
                    for (j = 0; j < RowCount; j++) {
                        if (row_bg1 == 0) {
                            row_bg1 = 1;
                            Tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg1 = 0;
                            Tab += "<tr class='sub_second';>";
                        }
                        var HidArg1 = HidArg[j].split("®");
                        //LOAN AGREEMENT¶  Ratheesh P R¶  1¶  1
                        SlNo = SlNo + 1;
                        Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                        Tab += "<td style='width:45%;text-align:left'>" + HidArg1[3] + "</td>";
                        Tab += "<td style='width:45%;text-align:left;cursor: pointer;'><a href='ShowVetFormat.aspx?PkID=" + HidArg1[0] + "&VetID=" + HidArg1[1] + "&DocID=" + HidArg1[2] + "'>" + HidArg1[4] + "</a></td></tr>";
                    }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div>";
                    document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;
                }
                else
                    document.getElementById("rowDoc").style.display = "none";
            }
            function DisplayNoAttach() {
                document.getElementById("rowDoc").style.display = "";
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:45%;text-align:center'>NO ANY DOCUMENTS ATTACHED - PLEASE EXECUTE THIS DOCUMENT</td></tr>";
                Tab += "</table></div>";
                document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;
            }
            function GetVetReport() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                window.open("ViewVetReport.aspx?RequestID=" + RequestID);
            }
            function btnExit_onclick() {
                window.open("PendingExecution.aspx", '_self');
            }            

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <br />
            <table align="center" style="width: 90%; margin: 0px auto;">
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Party with which transaction is plan
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtParty" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Statement Of Facts
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtStatement" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td id="colResolution" style="width: 10%">
                        <asp:ImageButton ID="cmdView" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" />
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Nature of transaction
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtNature" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        General Terms
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtTerms" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 10%; text-align: center; cursor: pointer;">
                        <asp:HyperLink 
                            ID="hlVetRpt" runat="server" Font-Underline="True" ForeColor="#3333CC">Vet Report</asp:HyperLink>
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Document For Transaction</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbDocument" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDoc">
                    <td style="text-align: right;" colspan="4">
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;</td>
                        <td style="width: 30%; text-align: right;">
                        &nbsp;</td>
                    <td style="width: 50%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Date Of Execution</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtExecuteDt" runat="server" onkeypress='NumericCheck(event)'></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="txtExecuteDt_CalendarExtender" OnClientShowing="setStartDate"
                            runat="server" TargetControlID="txtExecuteDt" Format="dd/MMM/yyyy">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <input id="btnExecute" style="font-family: cambria; cursor: pointer; width: 140px;" type="button" value="EXECUTE" onclick="return btnExecute_onclick()" />
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 140px;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocDtl" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
