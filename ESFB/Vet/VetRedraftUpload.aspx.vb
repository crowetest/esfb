﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Vet_VetRedraftUpload
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID, VetID, DocID, PkID As Integer

#Region "Page Load & Dispose"
    Protected Sub Vet_Opinion_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub Vet_Opinion_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            VetID = CInt(Request.QueryString.Get("VetID"))
            DocID = CInt(Request.QueryString.Get("DocID"))
            PkID = CInt(Request.QueryString.Get("PkID"))
            Me.hdnRequest.Value = CStr(RequestID)
            Me.Master.subtitle = "Attach Document"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.btnSave.Attributes.Add("onclick", "return SaveOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Select Case CInt(Data(0))
            Case 1
                DT = DB.ExecuteDataSet("select DOCUMENT_NAME from VET_DOCUMENT where SERVICE_ID=4 and DOCUMENT_TYPE=2 and DOCUMENT_ID=" & DocID & "").Tables(0)
                If DT.Rows.Count > 0 Then
                    CallBackReturn = CStr(DT.Rows(0)(0))
                End If
        End Select
    End Sub
#End Region

#Region "Save"
    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim UserID As Integer = CInt(Session("UserID"))

            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim FileName As String = ""
            Dim myFile As HttpPostedFile = fupAttDraft.PostedFile
            Dim nFileLen As Integer = myFile.ContentLength
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, nFileLen)
                Dim fileExtension As String = Path.GetExtension(myFile.FileName)
                If Not (fileExtension = ".xls" Or fileExtension = ".xlsx" Or fileExtension = ".jpg" Or fileExtension = ".jpeg" Or fileExtension = ".doc" Or fileExtension = ".docx" Or fileExtension = ".zip" Or fileExtension = ".pdf" Or fileExtension = ".PDF" Or fileExtension = ".XLS" Or fileExtension = ".XLSX" Or fileExtension = ".JPG" Or fileExtension = ".JPEG" Or fileExtension = ".DOC" Or fileExtension = ".DOCX" Or fileExtension = ".ZIP") Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
            End If

            Try
                If RequestID > 0 Then
                    Dim Param(7) As SqlParameter
                    Param(0) = New SqlParameter("@PkID", SqlDbType.Int)
                    Param(0).Value = PkID
                    Param(1) = New SqlParameter("@VetID", SqlDbType.Int)
                    Param(1).Value = VetID
                    Param(2) = New SqlParameter("@DocID", SqlDbType.Int)
                    Param(2).Value = DocID
                    Param(3) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                    Param(3).Value = AttachImg
                    Param(4) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                    Param(4).Value = ContentType
                    Param(5) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                    Param(5).Value = FileName
                    Param(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Param(6).Direction = ParameterDirection.Output
                    Param(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Param(7).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_REDRAFT_ATTACHMENT", Param)
                    ErrorFlag = CInt(Param(6).Value)
                    Message = CStr(Param(7).Value)
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("alert('" + Message + "');")
            cl_script1.Append("window.close();")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

End Class
