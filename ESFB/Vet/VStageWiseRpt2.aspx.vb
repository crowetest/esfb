﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Vet_VStageWiseRpt2
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim TB As New Table
    Dim ServiceID As Integer
    Dim UserID As Integer
    Dim DeptID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            ServiceID = CInt(Request.QueryString.Get("ServiceID"))

            TB.Attributes.Add("width", "100%")

            Dim RowBG As Integer = 0
            Dim DR As DataRow

            RH.Heading(CStr(Session("FirmName")), TB, "STAGE WISE REPORT", 100)
            RH.BlankRow(TB, 5)

            UserID = CInt(Session("UserID"))

            Dim DT As New DataTable
            DT = DB.ExecuteDataSet("select Department_ID from EMP_MASTER where Emp_Code=" & UserID & "").Tables(0)
            DeptID = DT.Rows(0)(0)

            If ServiceID = 1 Then 'Drafting
                Dim DT_REQ, DT_APP As New DataTable

                RH.SubHeading(TB, 100, "l", "DRAFTING")
                RH.DrawLine(TB, 5)
                RH.BlankRow(TB, 1)

                '---------------Request
                DT_REQ = DB.ExecuteDataSet("SELECT d.DRAFT_NAME,upper(v.STATEMENT_FACT),upper(e.Emp_Name),v.REQUEST_DT,v.REQUEST_ID FROM VET_MASTER v, VET_DRAFT d, EMP_MASTER e WHERE v.DRAFT_ID=d.DRAFT_ID and v.REQUEST_BY=e.Emp_Code and v.STATUS_ID=0 and v.SERVICE_ID=1 and v.DRAFT_ID IS NOT NULL and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_REQ.Rows.Count > 0 Then

                    RH.SubHeading(TB, 100, "l", "REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 20, 20, "l", "Document To Draft")
                    RH.AddColumn(TRVET0, TRVET0_02, 25, 25, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 25, 25, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "l", "Request Date")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_REQ.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 20, 20, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 25, 25, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 25, 25, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", Format(DR(3), "dd/MMM/yyyy"))

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Approved Request
                DT_APP = DB.ExecuteDataSet("SELECT d.DRAFT_NAME,upper(v.STATEMENT_FACT),UPPER(e.Emp_Name) RequestBY,v.REQUEST_DT,upper(m.Emp_Name) ApproveBy,v.REQUEST_ID FROM VET_MASTER v, VET_DRAFT d, EMP_MASTER e, EMP_MASTER m WHERE v.DRAFT_ID=d.DRAFT_ID and v.REQUEST_BY=e.Emp_Code and v.REQAPP_BY=m.Emp_Code and v.STATUS_ID=1 and v.SERVICE_ID=1 and v.DRAFT_ID IS NOT NULL and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "APPRROVED REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 15, 15, "l", "Document To Draft")
                    RH.AddColumn(TRVET0, TRVET0_02, 20, 20, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 20, 20, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 15, 15, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 20, 20, "l", "Approve By")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 15, 15, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 20, 20, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 20, 20, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "l", Format(DR(3), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_05, 20, 20, "l", DR(4))

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Rejected Request
                DT_APP = DB.ExecuteDataSet("SELECT d.DRAFT_NAME,upper(v.STATEMENT_FACT),UPPER(e.Emp_Name) RequestBY,v.REQUEST_DT,upper(m.Emp_Name) ApproveBy,upper(v.REJECT_REASON),v.REQUEST_ID FROM VET_MASTER v, VET_DRAFT d, EMP_MASTER e, EMP_MASTER m WHERE v.DRAFT_ID=d.DRAFT_ID and v.REQUEST_BY=e.Emp_Code and v.REQAPP_BY=m.Emp_Code and v.STATUS_ID=2 and v.SERVICE_ID=1 and v.DRAFT_ID IS NOT NULL and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "REJECTED REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 15, 15, "l", "Document To Draft")
                    RH.AddColumn(TRVET0, TRVET0_02, 15, 15, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 15, 15, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 15, 15, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 15, 15, "l", "Reject By")
                    RH.AddColumn(TRVET0, TRVET0_06, 15, 15, "l", "Reject Reason")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 15, 15, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 15, 15, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "l", Format(DR(3), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", DR(4))
                        RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", DR(5))

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Drafted
                DT_APP = DB.ExecuteDataSet("SELECT d.DRAFT_NAME,upper(v.STATEMENT_FACT),UPPER(e.Emp_Name) RequestBY,v.REQUEST_DT,UPPER(m.Emp_Name) DraftBy,v.VET_DT DraftDt,a.DRFILE_NAME,a.PkId,v.REQUEST_ID FROM VET_MASTER v, VET_DRAFT d, EMP_MASTER e, EMP_MASTER m, DMS_ESFB.dbo.VET_DRAFT_ATTACHMENT a WHERE v.DRAFT_ID=d.DRAFT_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.REQUEST_ID=a.REQUEST_ID and v.STATUS_ID=3 and v.SERVICE_ID=1 and v.DRAFT_ID IS NOT NULL and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "DRAFTED")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 15, 15, "l", "Document To Draft")
                    RH.AddColumn(TRVET0, TRVET0_02, 15, 15, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 15, 15, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 15, 15, "l", "Drafted By")
                    RH.AddColumn(TRVET0, TRVET0_06, 10, 10, "l", "Drafted Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 15, 15, "l", "Drafted File")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow

                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver

                        RequestID = DR(8)

                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", "")
                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 15, 15, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", DR(1))
                            RH.AddColumn(TRVET55, TRVET55_03, 15, 15, "l", DR(2))
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", Format(DR(3), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", DR(4))
                            RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", Format(DR(5), "dd/MMM/yyyy"))
                        End If
                        RH.AddColumn(TRVET55, TRVET55_07, 15, 15, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(8).ToString + "&Value=0 &Id=" + DR(7).ToString + "' style='text-align:right;' target='_blank' >" & DR(6) & "</a>")
                        NextRequest = RequestID
                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Approve Drafted
                DT_APP = DB.ExecuteDataSet("SELECT d.DRAFT_NAME,upper(v.STATEMENT_FACT),UPPER(e.Emp_Name) RequestBY,v.REQUEST_DT,UPPER(m.Emp_Name) DraftBy,v.VET_DT DraftDt,UPPER(p.Emp_Name) ApproveBy,v.APP_DT,a.DRFILE_NAME,a.PkId,v.REQUEST_ID FROM VET_MASTER v, VET_DRAFT d, EMP_MASTER e, EMP_MASTER m, EMP_MASTER p, DMS_ESFB.dbo.VET_DRAFT_ATTACHMENT a WHERE v.DRAFT_ID=d.DRAFT_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.APP_BY=p.Emp_Code and v.REQUEST_ID=a.REQUEST_ID and v.STATUS_ID=4 and v.SERVICE_ID=1 and v.DRAFT_ID IS NOT NULL and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "APPROVE DRAFTED")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Document To Draft")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Drafted By")
                    RH.AddColumn(TRVET0, TRVET0_06, 10, 10, "l", "Drafted Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Approve By")
                    RH.AddColumn(TRVET0, TRVET0_08, 10, 10, "l", "Approve Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 15, 15, "l", "Drafted File")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow

                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver

                        RequestID = DR(10)

                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_08, 10, 10, "l", "")
                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", Format(DR(3), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                            RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", Format(DR(5), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                            RH.AddColumn(TRVET55, TRVET55_08, 10, 10, "l", Format(DR(7), "dd/MMM/yyyy"))
                        End If
                        RH.AddColumn(TRVET55, TRVET55_09, 15, 15, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(10).ToString + "&Value=0 &Id=" + DR(9).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        NextRequest = RequestID
                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Reject Drafted
                DT_APP = DB.ExecuteDataSet("SELECT d.DRAFT_NAME,upper(v.STATEMENT_FACT),UPPER(e.Emp_Name) RequestBY,v.REQUEST_DT,UPPER(m.Emp_Name) DraftBy,v.VET_DT DraftDt,UPPER(p.Emp_Name) ApproveBy,v.APP_DT,a.DRFILE_NAME,a.PkId,v.REQUEST_ID,v.REJECT_REASON FROM VET_MASTER v left outer join EMP_MASTER p on v.APP_BY=p.Emp_Code, VET_DRAFT d, EMP_MASTER e, EMP_MASTER m, DMS_ESFB.dbo.VET_DRAFT_ATTACHMENT a WHERE v.DRAFT_ID=d.DRAFT_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.REQUEST_ID=a.REQUEST_ID and v.STATUS_ID=5 and v.SERVICE_ID=1 and v.DRAFT_ID IS NOT NULL and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "REJECT DRAFTED")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Document To Draft")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Drafted By")
                    RH.AddColumn(TRVET0, TRVET0_06, 7, 7, "l", "Drafted Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Reject By")
                    RH.AddColumn(TRVET0, TRVET0_08, 8, 8, "l", "Reject Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 10, 10, "l", "Reject Reason")
                    RH.AddColumn(TRVET0, TRVET0_10, 10, 10, "l", "Drafted File")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow

                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver

                        RequestID = DR(10)

                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 7, 7, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_08, 8, 8, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_09, 10, 10, "l", "")
                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", Format(DR(3), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                            RH.AddColumn(TRVET55, TRVET55_06, 7, 7, "l", Format(DR(5), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                            RH.AddColumn(TRVET55, TRVET55_08, 8, 8, "l", Format(DR(7), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_09, 10, 10, "l", DR(11))
                        End If
                        RH.AddColumn(TRVET55, TRVET55_10, 15, 15, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(10).ToString + "&Value=0 &Id=" + DR(9).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        NextRequest = RequestID
                        TB.Controls.Add(TRVET55)
                    Next
                End If
            ElseIf ServiceID = 2 Then 'Opinion
                Dim DT_REQ, DT_APP As New DataTable

                RH.SubHeading(TB, 100, "l", "OPINION")
                RH.DrawLine(TB, 5)
                RH.BlankRow(TB, 1)

                '---------------Request
                DT_REQ = DB.ExecuteDataSet("SELECT upper(v.USER_OPINION),upper(v.STATEMENT_FACT),UPPER(e.Emp_Name),v.REQUEST_DT,v.REQUEST_ID FROM VET_MASTER v, EMP_MASTER e WHERE v.REQUEST_BY=e.Emp_Code and v.STATUS_ID=0 and v.SERVICE_ID=2 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_REQ.Rows.Count > 0 Then

                    RH.SubHeading(TB, 100, "l", "REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 20, 20, "l", "Opinion Related To")
                    RH.AddColumn(TRVET0, TRVET0_02, 25, 25, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 25, 25, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "l", "Request Date")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_REQ.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 20, 20, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 25, 25, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 25, 25, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", Format(DR(3), "dd/MMM/yyyy"))

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Approved Request
                DT_APP = DB.ExecuteDataSet("SELECT upper(v.USER_OPINION),upper(v.STATEMENT_FACT),UPPER(e.Emp_Name),v.REQUEST_DT,UPPER(m.Emp_Name),v.REQUEST_ID FROM VET_MASTER v, EMP_MASTER e, EMP_MASTER m WHERE v.REQUEST_BY=e.Emp_Code and v.REQAPP_BY=m.Emp_Code and v.STATUS_ID=1 and v.SERVICE_ID=2 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "APPRROVED REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 15, 15, "l", "Opinion Related To")
                    RH.AddColumn(TRVET0, TRVET0_02, 20, 20, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 20, 20, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 15, 15, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 20, 20, "l", "Approve By")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 15, 15, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 20, 20, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 20, 20, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "l", Format(DR(3), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_05, 20, 20, "l", DR(4))

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Rejected Request
                DT_APP = DB.ExecuteDataSet("SELECT upper(v.USER_OPINION),upper(v.STATEMENT_FACT),UPPER(e.Emp_Name),v.REQUEST_DT,UPPER(m.Emp_Name),upper(v.REJECT_REASON),v.REQUEST_ID FROM VET_MASTER v, EMP_MASTER e, EMP_MASTER m WHERE v.REQUEST_BY=e.Emp_Code and v.REQAPP_BY=m.Emp_Code and v.STATUS_ID=2 and v.SERVICE_ID=2 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "REJECTED REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 15, 15, "l", "Opinion Related To")
                    RH.AddColumn(TRVET0, TRVET0_02, 15, 15, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 15, 15, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 15, 15, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 15, 15, "l", "Reject By")
                    RH.AddColumn(TRVET0, TRVET0_06, 15, 15, "l", "Reject Reason")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 15, 15, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 15, 15, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "l", Format(DR(3), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", DR(4))
                        RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", DR(5))

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ opinion entered
                DT_APP = DB.ExecuteDataSet("SELECT upper(v.USER_OPINION),upper(v.STATEMENT_FACT),UPPER(e.Emp_Name),v.REQUEST_DT,UPPER(m.Emp_Name) OpinionBy,v.VET_DT OpinionDt,a.OPINION_NAME,a.PkId,v.REQUEST_ID FROM VET_MASTER v, EMP_MASTER e, EMP_MASTER m, DMS_ESFB.dbo.VET_OPINION_ATTACHMENT a WHERE v.REQUEST_BY=e.Emp_Code and V.VET_BY=m.Emp_Code and v.REQUEST_ID=a.REQUEST_ID and v.STATUS_ID=3 and v.SERVICE_ID=2 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "OPINION ENTERED")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 15, 15, "l", "Opinion Related To")
                    RH.AddColumn(TRVET0, TRVET0_02, 15, 15, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 15, 15, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 15, 15, "l", "Opinion By")
                    RH.AddColumn(TRVET0, TRVET0_06, 10, 10, "l", "Opinion Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 15, 15, "l", "Uploaded File")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow

                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver

                        RequestID = DR(8)

                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", "")
                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 15, 15, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", DR(1))
                            RH.AddColumn(TRVET55, TRVET55_03, 15, 15, "l", DR(2))
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", Format(DR(3), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", DR(4))
                            RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", Format(DR(5), "dd/MMM/yyyy"))
                        End If
                        RH.AddColumn(TRVET55, TRVET55_07, 15, 15, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(8).ToString + "&Value=3 &Id=" + DR(7).ToString + "' style='text-align:right;' target='_blank' >" & DR(6) & "</a>")
                        NextRequest = RequestID
                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Approved Opinion
                DT_APP = DB.ExecuteDataSet("SELECT upper(v.USER_OPINION),upper(v.STATEMENT_FACT),UPPER(e.Emp_Name),v.REQUEST_DT,UPPER(m.Emp_Name) OpinionBy,v.VET_DT OpinionDt,UPPER(p.Emp_Name) ApproveBy,v.APP_DT,a.OPINION_NAME,a.PkId,v.REQUEST_ID FROM VET_MASTER v, EMP_MASTER e, EMP_MASTER m, EMP_MASTER p, DMS_ESFB.dbo.VET_OPINION_ATTACHMENT a WHERE v.REQUEST_BY=e.Emp_Code and V.VET_BY=m.Emp_Code and v.APP_BY=p.Emp_Code and v.REQUEST_ID=a.REQUEST_ID and v.STATUS_ID=4 and v.SERVICE_ID=2 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "APPROVED OPINION")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Document To Draft")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Drafted By")
                    RH.AddColumn(TRVET0, TRVET0_06, 10, 10, "l", "Drafted Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Approve By")
                    RH.AddColumn(TRVET0, TRVET0_08, 10, 10, "l", "Approve Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 15, 15, "l", "Drafted File")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow

                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver

                        RequestID = DR(10)

                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_08, 10, 10, "l", "")
                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", Format(DR(3), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                            RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", Format(DR(5), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                            RH.AddColumn(TRVET55, TRVET55_08, 10, 10, "l", Format(DR(7), "dd/MMM/yyyy"))
                        End If
                        RH.AddColumn(TRVET55, TRVET55_09, 15, 15, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(10).ToString + "&Value=0 &Id=" + DR(9).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        NextRequest = RequestID
                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Rejected Opinion
                DT_APP = DB.ExecuteDataSet("SELECT upper(v.USER_OPINION),upper(v.STATEMENT_FACT),UPPER(e.Emp_Name),v.REQUEST_DT,UPPER(m.Emp_Name) OpinionBy,v.VET_DT OpinionDt,UPPER(p.Emp_Name) ApproveBy,v.APP_DT,a.OPINION_NAME,a.PkId,v.REQUEST_ID,V.REJECT_REASON FROM VET_MASTER v, EMP_MASTER e, EMP_MASTER m, EMP_MASTER p, DMS_ESFB.dbo.VET_OPINION_ATTACHMENT a WHERE v.REQUEST_BY=e.Emp_Code and V.VET_BY=m.Emp_Code and v.APP_BY=p.Emp_Code and v.REQUEST_ID=a.REQUEST_ID and v.STATUS_ID=5 and v.SERVICE_ID=2 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "REJECTED OPINION")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Document To Draft")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Statement of Facts")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Drafted By")
                    RH.AddColumn(TRVET0, TRVET0_06, 7, 7, "l", "Drafted Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Reject By")
                    RH.AddColumn(TRVET0, TRVET0_08, 8, 8, "l", "Reject Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 10, 10, "l", "Reject Reason")
                    RH.AddColumn(TRVET0, TRVET0_10, 10, 10, "l", "Drafted File")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow

                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver

                        RequestID = DR(10)

                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 7, 7, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_08, 8, 8, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_09, 10, 10, "l", "")
                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", Format(DR(3), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                            RH.AddColumn(TRVET55, TRVET55_06, 7, 7, "l", Format(DR(5), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                            RH.AddColumn(TRVET55, TRVET55_08, 8, 8, "l", Format(DR(7), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_09, 10, 10, "l", DR(11))
                        End If
                        RH.AddColumn(TRVET55, TRVET55_10, 15, 15, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(10).ToString + "&Value=0 &Id=" + DR(9).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        NextRequest = RequestID
                        TB.Controls.Add(TRVET55)
                    Next
                End If
            ElseIf ServiceID = 3 Then 'Comments On Document
                Dim DT_REQ, DT_APP As New DataTable

                RH.SubHeading(TB, 100, "l", "COMMENTS ON DOCUMENTS")
                RH.DrawLine(TB, 11)
                RH.BlankRow(TB, 1)

                '---------------Request
                DT_REQ = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,upper(v.COMT_PURPOSE),upper(v.COMT_EXPECTATION),UPPER(e.Emp_Name),v.REQUEST_DT,v.REQUEST_ID FROM VET_MASTER v, VET_DOCUMENT d, EMP_MASTER e WHERE v.COMT_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.REQUEST_BY=e.Emp_Code and d.DOCUMENT_TYPE=1 and v.STATUS_ID=0 and v.SERVICE_ID=3 and v.COMT_DOCID is not null and v.REQUEST_DEPT=" & DeptID & " ").Tables(0)
                If DT_REQ.Rows.Count > 0 Then

                    RH.SubHeading(TB, 100, "l", "REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 20, 20, "l", "Nature Of Document")
                    RH.AddColumn(TRVET0, TRVET0_02, 20, 20, "l", "Purpose")
                    RH.AddColumn(TRVET0, TRVET0_03, 25, 25, "l", "Expectation")
                    RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request Date")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_REQ.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 20, 20, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 20, 20, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 25, 25, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", Format(DR(4), "dd/MMM/yyyy"))

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Approved Request
                DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,upper(v.COMT_PURPOSE),upper(v.COMT_EXPECTATION),UPPER(e.Emp_Name),v.REQUEST_DT,upper(m.Emp_Name),v.REQUEST_ID FROM VET_MASTER v, VET_DOCUMENT d, EMP_MASTER e, EMP_MASTER m WHERE v.COMT_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.REQUEST_BY=e.Emp_Code and v.REQAPP_BY=m.Emp_Code and d.DOCUMENT_TYPE=1 and v.STATUS_ID=1 and v.SERVICE_ID=3 and v.COMT_DOCID is not null and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "APPRROVED REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 15, 15, "l", "Nature Of Document")
                    RH.AddColumn(TRVET0, TRVET0_02, 15, 15, "l", "Purpose")
                    RH.AddColumn(TRVET0, TRVET0_03, 20, 20, "l", "Expectation")
                    RH.AddColumn(TRVET0, TRVET0_04, 15, 15, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_05, 15, 15, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_06, 15, 15, "l", "Approve By")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 15, 15, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 20, 20, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", Format(DR(4), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", DR(5))

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Rejected Request
                DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,upper(v.COMT_PURPOSE),upper(v.COMT_EXPECTATION),UPPER(e.Emp_Name),v.REQUEST_DT,upper(m.Emp_Name),v.REJECT_REASON,v.REQUEST_ID FROM VET_MASTER v, VET_DOCUMENT d, EMP_MASTER e, EMP_MASTER m WHERE v.COMT_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.REQUEST_BY=e.Emp_Code and v.REQAPP_BY=m.Emp_Code and d.DOCUMENT_TYPE=1 and v.STATUS_ID=2 and v.SERVICE_ID=3 and v.COMT_DOCID is not null and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "REJECTED REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Document")
                    RH.AddColumn(TRVET0, TRVET0_02, 20, 20, "l", "Purpose")
                    RH.AddColumn(TRVET0, TRVET0_03, 20, 20, "l", "Expectation")
                    RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_06, 10, 10, "l", "Reject By")
                    RH.AddColumn(TRVET0, TRVET0_07, 15, 15, "l", "Reject Reason")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 20, 20, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 20, 20, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", Format(DR(4), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", DR(5))
                        RH.AddColumn(TRVET55, TRVET55_07, 15, 15, "l", DR(6))

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Comments entered
                DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,upper(v.COMT_PURPOSE),upper(v.COMT_EXPECTATION),UPPER(e.Emp_Name),v.REQUEST_DT,upper(m.Emp_Name) COMMENTBY,v.VET_DT,v.REQUEST_ID FROM VET_MASTER v, VET_DOCUMENT d, EMP_MASTER e, EMP_MASTER m WHERE v.COMT_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and d.DOCUMENT_TYPE=1 and v.STATUS_ID=3 and v.SERVICE_ID=3 and v.COMT_DOCID is not null and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "COMMENTS ENTERED")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Document")
                    RH.AddColumn(TRVET0, TRVET0_02, 15, 15, "l", "Purpose")
                    RH.AddColumn(TRVET0, TRVET0_03, 20, 20, "l", "Expectation")
                    RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_06, 10, 10, "l", "Comment By")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Comment Date")
                    RH.AddColumn(TRVET0, TRVET0_08, 10, 10, "l", "Comments")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow

                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver

                        i = i + 1
                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 20, 20, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", Format(DR(4), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", DR(5))
                        RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", Format(DR(6), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_08, 10, 10, "c", "<a href='ViewCommentsDtlRpt.aspx?RequestID=" + DR(7).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Approved Comments
                DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,upper(v.COMT_PURPOSE),upper(v.COMT_EXPECTATION),UPPER(e.Emp_Name),v.REQUEST_DT,upper(m.Emp_Name) COMMENTBY,v.VET_DT,UPPER(p.Emp_Name),v.APP_DT,v.REQUEST_ID FROM VET_MASTER v, VET_DOCUMENT d, EMP_MASTER e, EMP_MASTER m, EMP_MASTER p WHERE v.COMT_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.APP_BY=p.Emp_Code and d.DOCUMENT_TYPE=1 and v.STATUS_ID=4 and v.SERVICE_ID=3 and v.COMT_DOCID is not null and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "APPROVED COMMENTS")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Document")
                    RH.AddColumn(TRVET0, TRVET0_02, 15, 15, "l", "Purpose")
                    RH.AddColumn(TRVET0, TRVET0_03, 20, 20, "l", "Expectation")
                    RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_05, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_06, 10, 10, "l", "Comment By")
                    RH.AddColumn(TRVET0, TRVET0_07, 5, 5, "l", "Comment Date")
                    RH.AddColumn(TRVET0, TRVET0_08, 10, 10, "l", "Approve By")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Approve Date")
                    RH.AddColumn(TRVET0, TRVET0_10, 5, 5, "l", "Comments")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow

                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver


                        i = i + 1
                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 20, 20, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 5, 5, "l", Format(DR(4), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", DR(5))
                        RH.AddColumn(TRVET55, TRVET55_07, 5, 5, "l", Format(DR(6), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_08, 10, 10, "l", DR(7))
                        RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "l", Format(DR(8), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "c", "<a href='ViewCommentsDtlRpt.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Rejected Comments
                DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,upper(v.COMT_PURPOSE),upper(v.COMT_EXPECTATION),UPPER(e.Emp_Name),v.REQUEST_DT,upper(m.Emp_Name) COMMENTBY,v.VET_DT,UPPER(p.Emp_Name),v.APP_DT,UPPER(v.REJECT_REASON),v.REQUEST_ID FROM VET_MASTER v, VET_DOCUMENT d, EMP_MASTER e, EMP_MASTER m, EMP_MASTER p WHERE v.COMT_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.APP_BY=p.Emp_Code and d.DOCUMENT_TYPE=1 and v.STATUS_ID=5 and v.SERVICE_ID=3 and v.COMT_DOCID is not null and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "REJECTED COMMENTS")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10, TRVET0_11 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"
                    TRVET0_11.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid
                    TRVET0_11.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver
                    TRVET0_11.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Document")
                    RH.AddColumn(TRVET0, TRVET0_02, 15, 15, "l", "Purpose")
                    RH.AddColumn(TRVET0, TRVET0_03, 20, 20, "l", "Expectation")
                    RH.AddColumn(TRVET0, TRVET0_04, 5, 5, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_05, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_06, 5, 5, "l", "Comment By")
                    RH.AddColumn(TRVET0, TRVET0_07, 5, 5, "l", "Comment Date")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Reject By")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Reject Date")
                    RH.AddColumn(TRVET0, TRVET0_10, 15, 15, "l", "Reject Reason")
                    RH.AddColumn(TRVET0, TRVET0_11, 5, 5, "l", "Comments")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow

                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10, TRVET55_11 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"
                        TRVET55_11.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid
                        TRVET55_11.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver
                        TRVET55_11.BorderColor = Drawing.Color.Silver


                        i = i + 1
                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 20, 20, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 5, 5, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 5, 5, "l", Format(DR(4), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_06, 5, 5, "l", DR(5))
                        RH.AddColumn(TRVET55, TRVET55_07, 5, 5, "l", Format(DR(6), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", DR(7))
                        RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "l", Format(DR(8), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_10, 15, 15, "l", DR(9))
                        RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "c", "<a href='ViewCommentsDtlRpt.aspx?RequestID=" + DR(10).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")

                        TB.Controls.Add(TRVET55)
                    Next
                End If

            ElseIf ServiceID = 4 Then 'Vetting For Executing Transaction
                Dim DT_REQ, DT_APP As New DataTable

                RH.SubHeading(TB, 100, "l", "VETTING FOR EXECUTING TRANSACTION")
                RH.DrawLine(TB, 11)
                RH.BlankRow(TB, 1)

                '---------------Request
                DT_REQ = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT),UPPER(e.Emp_Name),v.REQUEST_DT,v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.REQUEST_BY=e.Emp_Code and v.SERVICE_ID=4 and v.STATUS_ID in(0,17) and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_REQ.Rows.Count > 0 Then

                    RH.SubHeading(TB, 100, "l", "REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 20, 20, "l", "Nature Of Transaction")
                    RH.AddColumn(TRVET0, TRVET0_02, 20, 20, "l", "Other Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 20, 20, "l", "Statement Of Facts")
                    RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_06, 5, 5, "l", "Vet Details")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_REQ.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 20, 20, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 20, 20, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 20, 20, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", Format(DR(4), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_06, 5, 5, "c", "<a href='VStageWiseRpt3.aspx?RequestID=" + DR(5).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Approved Request
                DT_APP = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT),UPPER(e.Emp_Name),v.REQUEST_DT,UPPER(m.Emp_Name),v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER m where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.REQUEST_BY=e.Emp_Code and v.REQAPP_BY=m.Emp_Code and v.SERVICE_ID=4 and v.STATUS_ID=1 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "APPRROVED REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 15, 15, "l", "Nature Of Transaction")
                    RH.AddColumn(TRVET0, TRVET0_02, 15, 15, "l", "Other Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 15, 15, "l", "Statement Of Facts")
                    RH.AddColumn(TRVET0, TRVET0_04, 15, 15, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_05, 15, 15, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_06, 15, 15, "l", "Approve By")
                    RH.AddColumn(TRVET0, TRVET0_07, 5, 5, "l", "Vet Details")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 15, 15, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 15, 15, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", Format(DR(4), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", DR(5))
                        RH.AddColumn(TRVET55, TRVET55_07, 5, 5, "c", "<a href='VStageWiseRpt3.aspx?RequestID=" + DR(6).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Rejected Request
                DT_APP = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT),UPPER(e.Emp_Name),v.REQUEST_DT,UPPER(m.Emp_Name),UPPER(v.REJECT_REASON),v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER m where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.REQUEST_BY=e.Emp_Code and v.REQAPP_BY=m.Emp_Code and v.SERVICE_ID=4 and v.STATUS_ID=2 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "REJECTED REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Transaction")
                    RH.AddColumn(TRVET0, TRVET0_02, 20, 20, "l", "Other Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 15, 15, "l", "Statement Of Facts")
                    RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_06, 10, 10, "l", "Reject By")
                    RH.AddColumn(TRVET0, TRVET0_07, 15, 15, "l", "Reject Reason")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Vet Details")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 20, 20, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 15, 15, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", Format(DR(4), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", DR(5))
                        RH.AddColumn(TRVET55, TRVET55_07, 15, 15, "l", DR(6))
                        RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "c", "<a href='VStageWiseRpt3.aspx?RequestID=" + DR(7).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Vetting
                DT_APP = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT) Stmt,UPPER(v.GENERAL_TERM) general,UPPER(e.Emp_Name) reqBy,v.REQUEST_DT,UPPER(m.Emp_Name) vetBy,v.VET_DT,v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER m where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID=4 and v.STATUS_ID in(3,4,5,6,18) and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "VETTING")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Transaction")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Other Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Statement Of Facts")
                    RH.AddColumn(TRVET0, TRVET0_04, 25, 25, "l", "General Terms")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_06, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Vet By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Vet Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Vet Details")
                    RH.AddColumn(TRVET0, TRVET0_10, 5, 5, "l", "Comments")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 25, 25, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                        RH.AddColumn(TRVET55, TRVET55_06, 5, 5, "l", Format(DR(5), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                        RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "c", "<a href='VStageWiseRpt3.aspx?RequestID=" + DR(8).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "c", "<a href='VStageWiseRpt4.aspx?RequestID=" + DR(8).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Vetting
                DT_APP = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT) Stmt,UPPER(v.GENERAL_TERM) general,UPPER(e.Emp_Name) reqBy,v.REQUEST_DT,UPPER(m.Emp_Name) vetBy,v.VET_DT,v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER m where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID=4 and v.STATUS_ID in(16,19) and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "REJECTED VETTING")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Transaction")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Other Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Statement Of Facts")
                    RH.AddColumn(TRVET0, TRVET0_04, 25, 25, "l", "General Terms")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_06, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Vet By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Vet Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Vet Details")
                    RH.AddColumn(TRVET0, TRVET0_10, 5, 5, "l", "Comments")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 25, 25, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                        RH.AddColumn(TRVET55, TRVET55_06, 5, 5, "l", Format(DR(5), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                        RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "c", "<a href='VStageWiseRpt3.aspx?RequestID=" + DR(8).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "c", "<a href='VStageWiseRpt4.aspx?RequestID=" + DR(8).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Sign Off Vetting
                DT_APP = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT) Stmt,UPPER(v.GENERAL_TERM) general,UPPER(e.Emp_Name) reqBy,v.REQUEST_DT,UPPER(m.Emp_Name) vetBy,v.VET_DT,isnull(v.VET_NO,''),v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER m where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID=4 and v.STATUS_ID in(7) and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "SIGN OFF")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10, TRVET0_11 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"
                    TRVET0_11.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid
                    TRVET0_11.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver
                    TRVET0_11.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Transaction")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Other Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Statement Of Facts")
                    RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "l", "General Terms")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_06, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Vet By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Vet Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Vet Details")
                    RH.AddColumn(TRVET0, TRVET0_10, 5, 5, "l", "Comments")
                    RH.AddColumn(TRVET0, TRVET0_11, 5, 5, "l", "Vet No")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10, TRVET55_11 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"
                        TRVET55_11.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid
                        TRVET55_11.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver
                        TRVET55_11.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                        RH.AddColumn(TRVET55, TRVET55_06, 5, 5, "l", Format(DR(5), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                        RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "c", "<a href='VStageWiseRpt3.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "c", "<a href='VStageWiseRpt4.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        If IsDBNull(DR(8)) Then
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "")
                        Else
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "<a href='ViewVetFullReport.aspx?RequestID=" + DR(9).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        End If

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Higher Approve Vetting
                DT_APP = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT) Stmt,UPPER(v.GENERAL_TERM) general,UPPER(e.Emp_Name) reqBy,v.REQUEST_DT,UPPER(m.Emp_Name) vetBy,v.VET_DT,isnull(v.VET_NO,''),v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER m where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID=4 and v.STATUS_ID in(8,9,10,11) and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "HIGHER APPROVAL COMPLETED")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10, TRVET0_11 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"
                    TRVET0_11.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid
                    TRVET0_11.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver
                    TRVET0_11.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Transaction")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Other Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Statement Of Facts")
                    RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "l", "General Terms")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_06, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Vet By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Vet Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Vet Details")
                    RH.AddColumn(TRVET0, TRVET0_10, 5, 5, "l", "Comments")
                    RH.AddColumn(TRVET0, TRVET0_11, 5, 5, "l", "Vet No")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10, TRVET55_11 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"
                        TRVET55_11.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid
                        TRVET55_11.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver
                        TRVET55_11.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                        RH.AddColumn(TRVET55, TRVET55_06, 5, 5, "l", Format(DR(5), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                        RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "c", "<a href='VStageWiseRpt3.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "c", "<a href='VStageWiseRpt4.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        If IsDBNull(DR(8)) Then
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "")
                        Else
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "<a href='ViewVetFullReport.aspx?RequestID=" + DR(9).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        End If

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Execute Vetting
                DT_APP = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT) Stmt,UPPER(v.GENERAL_TERM) general,UPPER(e.Emp_Name) reqBy,v.REQUEST_DT,UPPER(m.Emp_Name) vetBy,v.VET_DT,isnull(v.VET_NO,''),v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER m where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID=4 and v.STATUS_ID in(12) and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "EXECUTION COMPLETE")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10, TRVET0_11 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"
                    TRVET0_11.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid
                    TRVET0_11.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver
                    TRVET0_11.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Transaction")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Other Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Statement Of Facts")
                    RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "l", "General Terms")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_06, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Vet By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Vet Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Execute Details")
                    RH.AddColumn(TRVET0, TRVET0_10, 5, 5, "l", "Comments")
                    RH.AddColumn(TRVET0, TRVET0_11, 5, 5, "l", "Vet No")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10, TRVET55_11 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"
                        TRVET55_11.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid
                        TRVET55_11.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver
                        TRVET55_11.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                        RH.AddColumn(TRVET55, TRVET55_06, 5, 5, "l", Format(DR(5), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                        RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "c", "<a href='VStageWiseRpt3.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "c", "<a href='VStageWiseRpt4.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        If IsDBNull(DR(8)) Then
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "")
                        Else
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "<a href='ViewVetFullReport.aspx?RequestID=" + DR(9).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        End If

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Uploded Exectued Version
                DT_APP = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT) Stmt,UPPER(v.GENERAL_TERM) general,UPPER(e.Emp_Name) reqBy,v.REQUEST_DT,UPPER(m.Emp_Name) vetBy,v.VET_DT,isnull(v.VET_NO,''),v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER m where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID=4 and v.STATUS_ID =13 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "EXECUTED VERSION UPLOADED")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10, TRVET0_11 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"
                    TRVET0_11.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid
                    TRVET0_11.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver
                    TRVET0_11.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Transaction")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Other Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Statement Of Facts")
                    RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "l", "General Terms")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_06, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Vet By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Vet Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Execute Details")
                    RH.AddColumn(TRVET0, TRVET0_10, 5, 5, "l", "Comments")
                    RH.AddColumn(TRVET0, TRVET0_11, 5, 5, "l", "Vet No")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10, TRVET55_11 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"
                        TRVET55_11.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid
                        TRVET55_11.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver
                        TRVET55_11.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                        RH.AddColumn(TRVET55, TRVET55_06, 5, 5, "l", Format(DR(5), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                        RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "c", "<a href='VStageWiseRpt3.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "c", "<a href='VStageWiseRpt4.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        If IsDBNull(DR(8)) Then
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "")
                        Else
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "<a href='ViewVetFullReport.aspx?RequestID=" + DR(9).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        End If

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Verified OK
                DT_APP = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT) Stmt,UPPER(v.GENERAL_TERM) general,UPPER(e.Emp_Name) reqBy,v.REQUEST_DT,UPPER(m.Emp_Name) vetBy,v.VET_DT,isnull(v.VET_NO,''),v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER m where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID=4 and v.STATUS_ID=14 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "VERIFIED OK")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10, TRVET0_11 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"
                    TRVET0_11.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid
                    TRVET0_11.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver
                    TRVET0_11.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Transaction")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Other Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Statement Of Facts")
                    RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "l", "General Terms")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_06, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Vet By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Vet Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Execute Details")
                    RH.AddColumn(TRVET0, TRVET0_10, 5, 5, "l", "Comments")
                    RH.AddColumn(TRVET0, TRVET0_11, 5, 5, "l", "Vet No")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10, TRVET55_11 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"
                        TRVET55_11.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid
                        TRVET55_11.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver
                        TRVET55_11.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                        RH.AddColumn(TRVET55, TRVET55_06, 5, 5, "l", Format(DR(5), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                        RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "c", "<a href='VStageWiseRpt3.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "c", "<a href='VStageWiseRpt4.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        If IsDBNull(DR(8)) Then
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "")
                        Else
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "<a href='ViewVetFullReport.aspx?RequestID=" + DR(9).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        End If

                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Verified NOT OK
                DT_APP = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT) Stmt,UPPER(v.GENERAL_TERM) general,UPPER(e.Emp_Name) reqBy,v.REQUEST_DT,UPPER(m.Emp_Name) vetBy,v.VET_DT,isnull(v.VET_NO,''),v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER m where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.REQUEST_BY=e.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID=4 and v.STATUS_ID=15 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "VERIFIED NOT OK")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10, TRVET0_11 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"
                    TRVET0_11.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid
                    TRVET0_11.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver
                    TRVET0_11.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Nature Of Transaction")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Other Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Statement Of Facts")
                    RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "l", "General Terms")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_06, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Vet By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Vet Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Execute Details")
                    RH.AddColumn(TRVET0, TRVET0_10, 5, 5, "l", "Comments")
                    RH.AddColumn(TRVET0, TRVET0_11, 5, 5, "l", "Vet No")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        i = i + 1
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10, TRVET55_11 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"
                        TRVET55_11.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid
                        TRVET55_11.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver
                        TRVET55_11.BorderColor = Drawing.Color.Silver

                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                        RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                        RH.AddColumn(TRVET55, TRVET55_06, 5, 5, "l", Format(DR(5), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                        RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                        RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "c", "<a href='VStageWiseRpt3.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "c", "<a href='VStageWiseRpt4.aspx?RequestID=" + DR(9).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")
                        If IsDBNull(DR(8)) Then
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "")
                        Else
                            RH.AddColumn(TRVET55, TRVET55_11, 5, 5, "l", "<a href='ViewVetFullReport.aspx?RequestID=" + DR(9).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        End If

                        TB.Controls.Add(TRVET55)
                    Next
                End If
            ElseIf ServiceID = 5 Then 'Archiving of executed document
                Dim DT_REQ, DT_APP As New DataTable

                RH.SubHeading(TB, 100, "l", "ARCHIVING OF EXECUTED DOCUMENTS REQUEST")
                RH.DrawLine(TB, 11)
                RH.BlankRow(TB, 1)

                '---------------Request
                DT_REQ = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,n.NATURE_NAME,p.PARTY_NAME,v.EXECUTE_DT,UPPER(e.Emp_Name) executant,v.REASON_NOTVET,upper(r.Emp_Name) requestBy,v.REQUEST_DT,a.FILE_NAME,v.REQUEST_ID,a.PkId FROM VET_MASTER v, VET_DOCUMENT d, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER r, DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a WHERE v.ARCH_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_BY=r.Emp_Code and v.SERVICE_ID = a.SERVICE_ID and v.REQUEST_ID=a.REQUEST_ID and d.DOCUMENT_TYPE=3 and v.STATUS_ID=0 and v.SERVICE_ID=5 and v.ARCH_DOCID is not null and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_REQ.Rows.Count > 0 Then

                    RH.SubHeading(TB, 100, "l", "REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Document")
                    RH.AddColumn(TRVET0, TRVET0_02, 15, 15, "l", "Nature")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Party")
                    RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "l", "Execute Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Executant")
                    RH.AddColumn(TRVET0, TRVET0_06, 15, 15, "l", "Reason For Archive")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 10, 10, "l", "File To Archive")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_REQ.Rows
                        Dim TRVET55 As New TableRow
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver

                        RequestID = DR(9)
                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", "")
                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", DR(1))
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                            RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "l", Format(DR(3), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                            RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", DR(5))
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                            RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                        End If

                        RH.AddColumn(TRVET55, TRVET55_09, 10, 10, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(9).ToString + "&Value=2 &Id=" + DR(10).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        NextRequest = RequestID
                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Approved Request
                DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,n.NATURE_NAME,p.PARTY_NAME,v.EXECUTE_DT,UPPER(e.Emp_Name) executant,v.REASON_NOTVET,upper(r.Emp_Name) requestBy,v.REQUEST_DT,a.FILE_NAME,v.REQUEST_ID,a.PkId,UPPER(m.Emp_Name) FROM VET_MASTER v, VET_DOCUMENT d, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER r, EMP_MASTER m, DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a WHERE v.ARCH_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_BY=r.Emp_Code and v.REQAPP_BY=m.Emp_Code and v.SERVICE_ID = a.SERVICE_ID and v.REQUEST_ID=a.REQUEST_ID and d.DOCUMENT_TYPE=3 and v.STATUS_ID=1 and v.SERVICE_ID=5 and v.ARCH_DOCID is not null and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "APPRROVED REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Document")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Nature")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Party")
                    RH.AddColumn(TRVET0, TRVET0_04, 5, 5, "l", "Execute Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Executant")
                    RH.AddColumn(TRVET0, TRVET0_06, 15, 15, "l", "Reason For Archive")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 10, 10, "l", "Approve By")
                    RH.AddColumn(TRVET0, TRVET0_10, 10, 10, "l", "File To Archive")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver

                        RequestID = DR(9)
                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 5, 5, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_09, 10, 10, "l", "")
                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                            RH.AddColumn(TRVET55, TRVET55_04, 5, 5, "l", Format(DR(3), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                            RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", DR(5))
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                            RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_09, 10, 10, "l", DR(11))
                        End If

                        RH.AddColumn(TRVET55, TRVET55_10, 10, 10, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(9).ToString + "&Value=2 &Id=" + DR(10).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        NextRequest = RequestID
                        TB.Controls.Add(TRVET55)
                    Next
                End If


                '------ Rejected Request
                DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,n.NATURE_NAME,p.PARTY_NAME,v.EXECUTE_DT,UPPER(e.Emp_Name) executant,v.REASON_NOTVET,upper(r.Emp_Name) requestBy,v.REQUEST_DT,a.FILE_NAME,v.REQUEST_ID,a.PkId,UPPER(m.Emp_Name) FROM VET_MASTER v, VET_DOCUMENT d, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER r, EMP_MASTER m, DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a WHERE v.ARCH_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_BY=r.Emp_Code and v.REQAPP_BY=m.Emp_Code and v.SERVICE_ID = a.SERVICE_ID and v.REQUEST_ID=a.REQUEST_ID and d.DOCUMENT_TYPE=3 and v.STATUS_ID=2 and v.SERVICE_ID=5 and v.ARCH_DOCID is not null and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "REJECTED REQUEST")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Document")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Nature")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Party")
                    RH.AddColumn(TRVET0, TRVET0_04, 5, 5, "l", "Execute Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Executant")
                    RH.AddColumn(TRVET0, TRVET0_06, 15, 15, "l", "Reason For Archive")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 10, 10, "l", "Rejected By")
                    RH.AddColumn(TRVET0, TRVET0_10, 10, 10, "l", "File To Archive")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver

                        RequestID = DR(9)
                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 5, 5, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_09, 10, 10, "l", "")
                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                            RH.AddColumn(TRVET55, TRVET55_04, 5, 5, "l", Format(DR(3), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                            RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", DR(5))
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                            RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_09, 10, 10, "l", DR(11))
                        End If

                        RH.AddColumn(TRVET55, TRVET55_10, 10, 10, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(9).ToString + "&Value=2 &Id=" + DR(10).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        NextRequest = RequestID
                        TB.Controls.Add(TRVET55)
                    Next
                End If

                '------ Archived 
                DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,n.NATURE_NAME,p.PARTY_NAME,v.EXECUTE_DT,UPPER(e.Emp_Name) executant,v.REASON_NOTVET,upper(r.Emp_Name) requestBy,v.REQUEST_DT,a.FILE_NAME,v.REQUEST_ID,a.PkId,UPPER(m.Emp_Name) ArchiveBy,v.VET_DT FROM VET_MASTER v, VET_DOCUMENT d, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER r, EMP_MASTER m, DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a WHERE v.ARCH_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_BY=r.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID = a.SERVICE_ID and v.REQUEST_ID=a.REQUEST_ID and d.DOCUMENT_TYPE=3 and v.STATUS_ID in(3,4) and v.SERVICE_ID=5 and v.ARCH_DOCID is not null and v.REQUEST_DEPT=" & DeptID & "").Tables(0)
                If DT_APP.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "ARCHIVE")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10, TRVET0_11 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"
                    TRVET0_11.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid
                    TRVET0_11.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver
                    TRVET0_11.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Document")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Nature")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Party")
                    RH.AddColumn(TRVET0, TRVET0_04, 5, 5, "l", "Execute Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Executant")
                    RH.AddColumn(TRVET0, TRVET0_06, 15, 15, "l", "Reason For Archive")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Archive By")
                    RH.AddColumn(TRVET0, TRVET0_10, 5, 5, "l", "Archive Date")
                    RH.AddColumn(TRVET0, TRVET0_11, 10, 10, "l", "File To Archive")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10, TRVET55_11 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"
                        TRVET55_11.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid
                        TRVET55_11.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver
                        TRVET55_11.BorderColor = Drawing.Color.Silver

                        RequestID = DR(9)
                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 5, 5, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "l", "")
                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                            RH.AddColumn(TRVET55, TRVET55_04, 5, 5, "l", Format(DR(3), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                            RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", DR(5))
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                            RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "l", DR(11))
                            RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "l", Format(DR(12), "dd/MMM/yyyy"))
                        End If

                        RH.AddColumn(TRVET55, TRVET55_11, 10, 10, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(9).ToString + "&Value=2 &Id=" + DR(10).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        NextRequest = RequestID
                        TB.Controls.Add(TRVET55)
                    Next
                End If

            End If
            pnDisplay.Controls.Add(TB)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
