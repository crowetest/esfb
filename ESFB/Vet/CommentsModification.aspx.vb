﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vet_CommentsModification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer

#Region "Page Load & Dispose"
    Protected Sub Vet_CommentsDocument_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub Vet_CommentsDocument_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            Me.hdnRequest.Value = CStr(RequestID)
            Me.Master.subtitle = "Comments On Documents"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            If Not IsPostBack Then
                DT = GF.GetQueryResult("select DOCUMENT_ID,DOCUMENT_NAME from VET_DOCUMENT where DOCUMENT_TYPE=1 and SERVICE_ID=3 and STATUS_ID=1")
                GF.ComboFill(cmbNaturDoc, DT, 0, 1)
            End If
            Me.chkModify.Attributes.Add("onclick", "return ModifyAttachment()")
            Me.btnSave.Attributes.Add("onclick", "return SaveOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Compliance For Approve
                DT = GF.GetQueryResult("SELECT CONVERT(varchar(10),v.COMT_DOCID)+'Æ'+upper(v.COMT_PURPOSE)+'Æ'+upper(v.COMT_EXPECTATION)+'Æ'+isnull((select CONVERT(varchar(10),count(a.REQUEST_ID),0) from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID),0) FROM VET_MASTER v WHERE v.STATUS_ID=2 and v.SERVICE_ID=3 and REQUEST_ID=" & RequestID & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn = DT.Rows(0)(0).ToString()
                End If
                CallBackReturn += "Æ"
                Dim RequestData() As String = CStr(DT.Rows(0)(0)).Split(CChar("Æ"))
                If CInt(RequestData(3)) > 0 Then
                    DT = GF.GetQueryResult("SELECT convert(varchar(10),PkId)+ '®' +convert(varchar(10),REQUEST_ID)+ '®' + FILE_NAME FROM DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT where SERVICE_ID=3 and REQUEST_ID=" & RequestID & "")
                    If DT.Rows.Count > 0 Then
                        For Each DR In DT.Rows
                            CallBackReturn += DR(0).ToString() + "¶"
                        Next
                    End If
                End If
        End Select
    End Sub
#End Region

#Region "Confirmation"
    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim UserID As Integer = CInt(Session("UserID"))
            DT = GF.GetQueryResult("select Department_ID from EMP_MASTER where Emp_Code = " & UserID & "")
            Dim DepartmentID As Integer = CInt(DT.Rows(0)(0))

            Dim ModifyFlag As Integer = 0
            If (Me.chkModify.Checked = True) Then
                ModifyFlag = 1
            End If
            If (CInt(Me.hdnValue.Value) = 1) Then
                ModifyFlag = 1
            End If

            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files

            Dim CommntDocID As Integer = CInt(Me.cmbNaturDoc.SelectedValue)
            Dim Purpose As String = CStr(Me.txtPurpose.Text)
            Dim Expectation As String = CStr(Me.txtExpectation.Text)
            Try
                Dim Params(8) As SqlParameter
                Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                Params(0).Value = RequestID
                Params(1) = New SqlParameter("@ComDocID", SqlDbType.Int)
                Params(1).Value = CommntDocID
                Params(2) = New SqlParameter("@Purpose", SqlDbType.VarChar, 1000)
                Params(2).Value = Purpose
                Params(3) = New SqlParameter("@Expect", SqlDbType.VarChar, 1000)
                Params(3).Value = Expectation
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = UserID
                Params(5) = New SqlParameter("@DeptID", SqlDbType.Int)
                Params(5).Value = DepartmentID
                Params(6) = New SqlParameter("@ModifyAtt", SqlDbType.Int)
                Params(6).Value = ModifyFlag
                Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(8).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_VET_MODIFY_COMMENTS", Params)
                ErrorFlag = CInt(Params(7).Value)
                Message = CStr(Params(8).Value)

                If ModifyFlag > 0 And ErrorFlag = 0 And hfc.Count > 0 Then
                    For i = 0 To hfc.Count - 1
                        Dim myFile As HttpPostedFile = hfc(i)
                        Dim nFileLen As Integer = myFile.ContentLength
                        Dim FileName As String = ""
                        If (nFileLen > 0) Then
                            ContentType = myFile.ContentType
                            FileName = myFile.FileName
                            AttachImg = New Byte(nFileLen - 1) {}
                            myFile.InputStream.Read(AttachImg, 0, nFileLen)

                            Dim Param(6) As SqlParameter
                            Param(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                            Param(0).Value = RequestID
                            Param(1) = New SqlParameter("@ServiceID", SqlDbType.Int)
                            Param(1).Value = 3
                            Param(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                            Param(2).Value = AttachImg
                            Param(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                            Param(3).Value = ContentType
                            Param(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                            Param(4).Value = FileName
                            Param(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                            Param(5).Direction = ParameterDirection.Output
                            Param(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                            Param(6).Direction = ParameterDirection.Output
                            DB.ExecuteNonQuery("SP_VET_REQUEST_ATTACHMENT", Param)
                        End If
                    Next
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("         if (window.opener != null && !window.opener.closed);{window.opener.location.reload();window.close();}window.onbeforeunload = RefreshParent;")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
#End Region
    
End Class
