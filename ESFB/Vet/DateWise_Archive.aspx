﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="DateWise_Archive.aspx.vb" Inherits="Vet_DateWise_Archive" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

            function window_onload() {
                ToServer("1ʘ", 1);
            }
            function NatureOnChange() {
                var Nature = document.getElementById("<%= cmbNature.ClientID %>").value.split("Œ");
                var ServiceID = Nature[0];
                var NatureID = Nature[1];
                ToServer("2ʘ" + ServiceID + "ʘ" + NatureID, 2);
            }
            function DocumentOnChange() {
                var Nature = document.getElementById("<%= cmbNature.ClientID %>").value.split("Œ");
                var ServiceID = Nature[0];
                var NatureID = Nature[1];
                ToServer("3ʘ" + ServiceID + "ʘ" + NatureID, 3);
            }
            function btnSave_onclick() {
                var Nature = document.getElementById("<%= cmbNature.ClientID %>").value.split("Œ");
                var ServiceID = Nature[0];
                var NatureID = Nature[1];
                var DocID = document.getElementById("<%= cmbDocument.ClientID %>").value;
                var PartyID = document.getElementById("<%= cmbParty.ClientID %>").value;
                var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
                var ToDt = document.getElementById("<%= txtToDt.ClientID %>").value;
                window.open("DateWise_Archive_Rpt.aspx?ServiceID=" + ServiceID + "&NatureID=" + NatureID + "&DocID=" + DocID + "&PartyID=" + PartyID + "&FromDT=" + FromDt + "&ToDT=" + ToDt);
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1: // Nature Of Document
                    {
                        var Data = Arg;
                        if (Data != "") {
                            ComboFill(Data, "<%= cmbNature.ClientID %>");
                            NatureOnChange();
                        }
                        break;
                    }
                    case 2: // Document
                    {
                        var Data = Arg;
                        if (Data != "") {
                            ComboFill(Data, "<%= cmbDocument.ClientID %>");
                            DocumentOnChange();
                        }
                        break;
                    }
                    case 3: // Party
                    {
                        var Data = Arg;
                        if (Data != "") {
                            ComboFill(Data, "<%= cmbParty.ClientID %>");
                        }
                        break;
                    }
                }
            }
            function btnExit_onclick() {
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <br />
    <br />
    <br />
    <br />
    <table align="center"  style="width: 100%">
        <tr>
        <td style="width: 10%; text-align: right;"></td>
            <td style="width: 30%; text-align: right;">
                Nature Of Document&nbsp;&nbsp;
            </td>
            <td style="width: 50%; text-align: left;">
                <asp:DropDownList ID="cmbNature" runat="server" Width="69%">
                </asp:DropDownList>
            </td>
            <td style="width: 10%; text-align: right;"></td>
        </tr>
        <tr>  
                  <td style="width: 10%; text-align: right;"></td>
            <td style="width: 30%; text-align: right;">
                Document&nbsp;&nbsp;</td>
            <td style="width: 50%; text-align: left;">
                <asp:DropDownList ID="cmbDocument" runat="server" Width="69%">
                </asp:DropDownList>
            </td>
            <td style="width: 10%; text-align: right;"></td>
        </tr>
        <tr>
           <td style="width: 10%; text-align: right;"></td>
            <td style="width: 30%; text-align: right;">
                Party&nbsp;&nbsp;
            </td>
            <td style="width: 50%;text-align: left;">
                <asp:DropDownList ID="cmbParty" runat="server" Width="69%">
                </asp:DropDownList>
            </td>
            <td style="width: 10%; text-align: right;"></td>
        </tr>
        <tr>
            <td style="width: 10%; text-align: right;"></td>
            <td style="width: 30%; text-align: right;">
                Execution Period From&nbsp;&nbsp;</td>
            <td style="width: 50%; text-align:left">
                <asp:TextBox ID="txtFromDt" Width="32%" runat="server"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="txtFromDt_CalendarExtender" runat="server" TargetControlID="txtFromDt"
                    Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                &nbsp;&nbsp;&nbsp;To&nbsp;&nbsp;
                <asp:TextBox ID="txtToDt" Width="32%" runat="server"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="txtToDt_CalendarExtender" runat="server" TargetControlID="txtToDt"
                    Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
            </td>
            <td style="width: 10%; text-align: right;"></td>
        </tr>
        <tr>            
               <td style="width: 10%; text-align: right;"></td>
            <td style="width: 30%; text-align: right;">
            &nbsp;
            </td>
            <td style="width: 50%; text-align: left;">
            &nbsp;
            </td>
            <td style="width: 10%; text-align: right;"></td>
        </tr>
        <tr>
            <td style="width: 10%; text-align: right;"></td>
            <td colspan="2" style="text-align: center;">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                    value="GO" onclick="return btnSave_onclick()" />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />
            </td>
            <td style="width: 10%; text-align: right;"></td>
        </tr>
    </table>
    <br />
    <br />
    <br />
</asp:Content>
