﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="ArchiveApproveHead.aspx.vb" Inherits="Vet_ArchiveApproveHead" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

        function window_onload() {
            document.getElementById("rowArchiveDoc").style.display = "none";
            document.getElementById("rowReason").style.display = "none";
            ToServer("1ʘ", 1);
        }           
        function btnApprove_onclick() {
            var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
            ToServer("2ʘ" + RequestID, 2);
        }
        function btnReject_onclick() {
            var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
            document.getElementById("btnApprove").disabled = true;
            if (document.getElementById("<%= txtRejReason.ClientID %>").value == "") {
                alert("Enter Reject Reason");
                document.getElementById("rowReason").style.display = "";
                document.getElementById("<%= txtRejReason.ClientID %>").focus();
                return false;
            }
            ToServer("3ʘ" + RequestID + "ʘ" + document.getElementById("<%= txtRejReason.ClientID %>").value, 3);
        }
        function FromServer(Arg, Context) 
        {
            switch (Context) 
            {
                case 1:
                {
                    var Data = Arg.split("Æ");
                    document.getElementById("<%= txtDocument.ClientID %>").value = Data[0];
                    if (Data[1] > 0) {
                        document.getElementById("<%= hdnDocDtl.ClientID %>").value = Data[7];
                        DiplayTable();
                    }
                    else
                        document.getElementById("rowArchiveDoc").style.display = "none";

                    document.getElementById("<%= txtNature.ClientID %>").value = Data[2];
                    document.getElementById("<%= txtParty.ClientID %>").value = Data[3];
                    document.getElementById("<%= txtEecutant.ClientID %>").value = Data[4];
                    document.getElementById("<%= txtReason.ClientID %>").value = Data[5];
                    document.getElementById("<%= txtExcuteDt.ClientID %>").value = Data[6];
                    break;
                }
                case 2: // Approve Confirmation
                {
                    var Data = Arg.split("ʘ");
                    if (Data[0] == 0) {
                        alert(Data[1]);
                        //Return to Old Report
                        if (window.opener != null && !window.opener.closed) {
                            window.opener.location.reload();
                            window.close();
                        }
                        window.onbeforeunload = RefreshParent;
                    }
                    break;
                }
                case 3: // Reject Confirmation
                {
                    var Data = Arg.split("ʘ");
                    if (Data[0] == 0) {
                        alert(Data[1]);
                        //Return to Old Report
                        if (window.opener != null && !window.opener.closed) {
                            window.opener.location.reload();
                            window.close();
                        }
                        window.onbeforeunload = RefreshParent;
                    }
                    break;
                }
            }
        }
        function DiplayTable() {
            var Tab = "";
            Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
            Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            Tab += "<tr class='sub_second';>";
            Tab += "<td style='width:5%;text-align:center'>ATTACHMENTS</td></tr>";
            Tab += "</table></div>";
            Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
            Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            Tab += "<tr>";
            Tab += "<td style='width:10%;text-align:center'>#</td>";
            Tab += "<td style='width:90%;text-align:left'>DOCUMENT&nbsp;NAME</td></tr>";
            Tab += "</table></div>";
            Tab += "<div id='ScrollDiv' style='width:100%; height:60px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
            Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            if (document.getElementById("<%= hdnDocDtl.ClientID %>").value != "") 
            {
                document.getElementById("rowArchiveDoc").style.display = "";
                var HidArg = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("¶");
                var RowCount = HidArg.length - 1;
                var j;
                var Total = 0;
                var row_bg1 = 0;
                var SlNo = 0;
                for (j = 0; j < RowCount; j++) {
                    if (row_bg1 == 0) {
                        row_bg1 = 1;
                        Tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg1 = 0;
                        Tab += "<tr class='sub_second';>";
                    }
                    var HidArg1 = HidArg[j].split("®");
                    //LOAN AGREEMENT¶  Ratheesh P R¶  1¶  1
                    SlNo = SlNo + 1;
                    Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                    Tab += "<td style='width:90%;text-align:left;cursor: pointer;'><a href='ShowFormat.aspx?RequestID=" + HidArg1[1] + "&Value=2&Id=" + HidArg1[0] + "'>" + HidArg1[2] + "</a></td></tr>";
                }
                if (row_bg1 == 0)
                    Tab += "<tr style='background-color:OldLace'></tr>";
                else
                    Tab += "<tr style='background-color:Wheat'></tr>";
                Tab += "</table></div>";
                document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;
            }
        }
        function btnExit_onclick() {
            //Return to Old Report
            if (window.opener != null && !window.opener.closed) {
                window.opener.location.reload();
                window.close();
            }
            window.onbeforeunload = RefreshParent;
        }

        </script>
    
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
</script>
</head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Document to be archived
                    </td>                    
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtDocument" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td id="colDoc" style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rowArchiveDoc">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: right;">
                        &nbsp;</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Documents Releated to
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtNature" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Name Of Opposite Party
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtParty" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Date of execution
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtExcuteDt" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Person Executed the Document
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtEecutant" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Reason for not taking the same through vetting route
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtReason" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowReason">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Reject Reason</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRejReason" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="500" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="APPROVE" onclick="return btnApprove_onclick()" />
                        <input id="btnReject" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="REJECT" onclick="return btnReject_onclick()" />
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocDtl" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
