﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="VetResponseUpdation.aspx.vb" Inherits="Vet_VetResponseUpdation" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {
                document.getElementById("colResolution").style.display = "none";
                document.getElementById("rowDocuments").style.display = "none";
                document.getElementById("rowObserve").style.display = "none";
                document.getElementById("<%= hdnObservation.ClientID %>").value = "";
                document.getElementById("<%= hdnObserveDtl.ClientID %>").value = "";
                document.getElementById("rowRespRemark").style.display = "none";

                if (document.getElementById("<%= hdnIsHOD.ClientID %>").value == 0) {
               
                    document.getElementById("btnApprove").disabled = false;
                    document.getElementById("btnClosure").disabled = false;
                    document.getElementById("btnDrop").disabled = true;
                    document.getElementById('btnApprove').value = "HIGHER APPROVAL";                 
                }
                else {
                    
                    document.getElementById("btnApprove").disabled = false;
                    document.getElementById("btnClosure").disabled = false;
                    document.getElementById("btnDrop").disabled = false;
                    document.getElementById('btnApprove').value = "CLOSURE";                   
                }
                ToServer("1^", 1);
            }
            function ViewAttachment(Value) {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowFormat.aspx?RequestID=" + RequestID + "&Value=" + Value);
                return false;
            }
            function btnApprove_onclick() {
                //For Higher Approve
               
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var Status = 1;
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                var Length = row.length - 1;
                var Count = 0;
                var TotCnt = 0;

                for (n = 0; n < Length; n++) {
                    col = row[n].split("ʘ");
                    if (col[10] == 0) {
                        //                                                              VetID           DocID          TransID
                        if (document.getElementById("<%= hdnObserveDtl.ClientID %>").value == "")
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value = col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[7] + "ʘ" + col[9] + "¶";
                        else
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[7] + "ʘ" + col[9] + "¶";
                        TotCnt += 1;
                    }
                    if (col[9] > 0)
                        Count += 1;
                }
                var ObserveDtl = document.getElementById("<%= hdnObserveDtl.ClientID %>").value;
                var IsHOD = document.getElementById("<%= hdnIsHOD.ClientID %>").value;
                var RESPONSEREMARK = document.getElementById("<%= txtAnyRemark.ClientID %>").value;
                document.getElementById("btnApprove").disabled = true;
                if (document.getElementById("<%= hdnStatus.ClientID %>").value == 4) {
                    if (TotCnt > Count) {
                        var Conf = confirm("Some Response are pending. Are you sure?");
                        if (Conf == true)
                            ToServer("2^" + RequestID + "^" + ObserveDtl + "^" + Status + "^" + IsHOD + "^" + RESPONSEREMARK, 2);
                    }
                    else
                        ToServer("2^" + RequestID + "^" + ObserveDtl + "^" + Status + "^" + IsHOD + "^" + RESPONSEREMARK, 2);
                }
                else
                    ToServer("2^" + RequestID + "^" + ObserveDtl + "^" + Status + "^" + IsHOD + "^" + RESPONSEREMARK, 2);
            }
            function btnClosure_onclick() {
                //For Closure
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var Status = 2;

                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                var Length = row.length - 1;
                var Count = 0;
                var TotCnt = 0;

                for (n = 0; n < Length; n++) {
                    col = row[n].split("ʘ");
                    if (col[10] == 0) {
                        //                                                              VetID           DocID          TransID
                        if (document.getElementById("<%= hdnObserveDtl.ClientID %>").value == "")
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value = col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[7] + "ʘ" + col[9] + "¶";
                        else
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[7] + "ʘ" + col[9] + "¶";
                        TotCnt += 1;
                    }
                    if (col[9] > 0)
                        Count += 1;
                }
                document.getElementById("btnClosure").disabled = true;
                var ObserveDtl = document.getElementById("<%= hdnObserveDtl.ClientID %>").value;
                var IsHOD = document.getElementById("<%= hdnIsHOD.ClientID %>").value;
                var RESPONSEREMARK = document.getElementById("<%= txtAnyRemark.ClientID %>").value;

                if (document.getElementById("<%= hdnStatus.ClientID %>").value == 4) {
                    if (TotCnt > Count) {
                        var Conf = confirm("Some Response are pending. Are you sure?");
                        if (Conf == true)
                            ToServer("2^" + RequestID + "^" + ObserveDtl + "^" + Status + "^" + IsHOD + "^" + RESPONSEREMARK, 2);
                    }
                    else
                        ToServer("2^" + RequestID + "^" + ObserveDtl + "^" + Status + "^" + IsHOD + "^" + RESPONSEREMARK, 2);
                }
                else
                    ToServer("2^" + RequestID + "^" + ObserveDtl + "^" + Status + "^" + IsHOD + "^" + RESPONSEREMARK, 2);
            }
            function btnDrop_onclick() {
                //For Drop
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var Status = 3;
                var IsHOD = document.getElementById("<%= hdnIsHOD.ClientID %>").value;

                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                var Length = row.length - 1;
                var Count = 0;
                var TotCnt = 0;

                for (n = 0; n < Length; n++) {
                    col = row[n].split("ʘ");
                    if (col[10] == 0) {
                        //                                                              VetID           DocID          TransID
                        if (document.getElementById("<%= hdnObserveDtl.ClientID %>").value == "")
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value = col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[7] + "ʘ" + col[9] + "¶";
                        else
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[7] + "ʘ" + col[9] + "¶";
                        TotCnt += 1;
                    }
                    if (col[9] > 0)
                        Count += 1;
                }
                document.getElementById("btnClosure").disabled = true;
                var ObserveDtl = document.getElementById("<%= hdnObserveDtl.ClientID %>").value;
                var IsHOD = document.getElementById("<%= hdnIsHOD.ClientID %>").value;
                var RESPONSEREMARK = document.getElementById("<%= txtAnyRemark.ClientID %>").value;

                var Conf = confirm("Are you sure to Drop This Veting?");
                if (Conf == true)
                    ToServer("2^" + RequestID + "^" + ObserveDtl + "^" + Status + "^" + IsHOD + "^" + RESPONSEREMARK, 2);
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                        {
                            //SBIÆ  Ratheesh P RÆ  ADFSDFSDF SDSDFSDFSDFÆ  AVAILING OF LOANÆ 0Æ  2Æ  
                            var Data = Arg.split("Æ");
                            document.getElementById("<%= txtParty.ClientID %>").value = Data[0]; //Party
                            document.getElementById("<%= txtStatement.ClientID %>").value = Data[1]; //Statement 
                            document.getElementById("<%= txtNature.ClientID %>").value = Data[2]; // Nature of transaction
                            if (Data[3] == 1) // Concerned Resolution
                                document.getElementById("colResolution").style.display = "";
                            else
                                document.getElementById("colResolution").style.display = "none";
                            if (Data[4] > 0) // Vet Document Details
                            {
                                document.getElementById("<%= hdnDocument.ClientID %>").value = Data[11]; // Vet Documents
                                document.getElementById("rowDocuments").style.display = "";
                                DataFill();
                            }
                            document.getElementById("<%= txtTerms.ClientID %>").value = Data[5];

                            // General Terms
                            if (Data[6] != "") {
                                document.getElementById("rowRemark").style.display = "";
                                document.getElementById("<%= txtRemarks.ClientID %>").value = Data[6];
                            }
                            else
                                document.getElementById("rowRemark").style.display = "none";


                            // Remarks

                            document.getElementById("<%= txtApproveBy.ClientID %>").value = Data[8]; //Approve By
                            if (Data[9] > 0) // Comments On Documents
                            {
                                document.getElementById("<%= hdnObservation.ClientID %>").value = Data[12];
                                if (document.getElementById("<%= hdnStatus.ClientID %>").value == 4)
                                    TableFill();

                                else
                                    TablesFill();
                            }

                            if (Data[10] != "") {
                                document.getElementById("rowRespRemark").style.display = "";
                                document.getElementById("<%= txtRespRem.ClientID %>").value = Data[10];
                            }
                            else
                                document.getElementById("rowRespRemark").style.display = "none";
                            break;
                        }
                    case 2: // Confirmation
                        {
                            var Data = Arg.split("ʘ");
                            alert(Data[1]);
                            if (Data[0] == 0) {
                                //Return to Old Report
                                if (window.opener != null && !window.opener.closed) {
                                    window.opener.location.reload();
                                    window.close();
                                }
                                window.onbeforeunload = RefreshParent;
                            }
                            break;
                        }

                }
            }
            function DataFill() {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>DOCUMENT TO BE VETTED/EXECUTED FOR THE TRANSACTION</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:20%;text-align:left'>DOCUMENT&nbsp;NAME</td>";
                Tab += "<td style='width:20%;text-align:left'>EXECUTANT</td>";
                Tab += "<td style='width:20%;text-align:left'>VET/EXECUTE</td>";
                Tab += "<td style='width:10%;text-align:center'>VIEW</td>";
                Tab += "<td style='width:20%;text-align:center'>REDRAFT DOCUMENT</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                var HidArg = document.getElementById("<%= hdnDocument.ClientID %>").value.split("ʘ");
                var RowCount = HidArg.length - 1;
                var j;
                var Total = 0;
                var row_bg1 = 0;
                var SlNo = 0;
                for (j = 0; j < RowCount; j++) {
                    if (row_bg1 == 0) {
                        row_bg1 = 1;
                        Tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg1 = 0;
                        Tab += "<tr class='sub_second';>";
                    }
                    var HidData = HidArg[j].split("§");
                    var HidArg1 = HidData[0].split("¶");
                    //LOAN AGREEMENT¶  Ratheesh P R¶  1¶  1
                    SlNo = SlNo + 1;
                    Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                    Tab += "<td style='width:20%;text-align:left'>" + HidArg1[0] + "</td>";
                    Tab += "<td style='width:20%;text-align:left'>" + HidArg1[1] + "</td>";
                    if (HidArg1[2] == 1)
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE AND VET</td>";
                    else
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE</td>";

                    if (HidArg1[2] == 0)
                        Tab += "<td style='width:10%;text-align:center'></td>";
                    else
                        Tab += "<td style='width:10%;text-align:center'><img id='imgReport' src='../Image/viewReport.PNG' title='View Attachment' Height='20px' Width='20px' onclick= 'ViewDocument(" + HidArg1[3] + ")' style='cursor:pointer;'/></td>";

                    if (HidArg1[2] == 0)
                        Tab += "<td style='width:20%;text-align:center'></td></tr>";
                    else
                        Tab += "<td style='width:20%;text-align:center'><a href='VetRedraftUpload.aspx?RequestID=" + HidArg1[5] + "&VetID=" + HidArg1[3] + "&DocID=" + HidArg1[4] + "&PkID=" + HidArg1[6] + "' style='text-align:right; color: red;' target='_blank'>Redraft</a></td></tr>";
                }
                if (row_bg1 == 0)
                    Tab += "<tr style='background-color:OldLace'></tr>";
                else
                    Tab += "<tr style='background-color:Wheat'></tr>";
                Tab += "</table></div>";
                document.getElementById("<%= pnlDocDtl.ClientID %>").innerHTML = Tab;
            }
            function TableFill() {
                if (document.getElementById("<%= hdnObservation.ClientID %>").value == "") {
                    document.getElementById("rowObserve").style.display = "none";
                }
                else {
                    document.getElementById("rowObserve").style.display = "";
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div style='width:100%; height:auto; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    tab += "<tr >";
                    tab += "<td style='width:5%;text-align:center' >#</td>";
                    tab += "<td style='width:10%;text-align:left'>Document For Transaction</td>";
                    tab += "<td style='width:10%;text-align:left'>Reference</td>";
                    tab += "<td style='width:15%;text-align:left'>Clause</td>";
                    tab += "<td style='width:15%;text-align:left'>Comments</td>";
                    tab += "<td style='width:15%;text-align:left'>Last Response</td>";
                    tab += "<td style='width:15%;text-align:left'>New Response</td>";
                    tab += "<td style='width:15%;text-align:left'>Remark</td>";
                    tab += "</tr>";
                    tab += "</table></div>";
                    tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    if (document.getElementById("<%= hdnObservation.ClientID %>").value != "") {

                        row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");

                        for (n = 0; n <= row.length - 2; n++) {
                            col = row[n].split("ʘ");
                            if (row_bg == 0) {
                                row_bg = 1;
                                tab += "<tr class='sub_first';>";
                            }
                            else {
                                row_bg = 0;
                                tab += "<tr class='sub_second';>";
                            }
                            i = n + 1;
                            if (col[10] == 0)
                                tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSelection" + col[2] + "' onclick='ChkPotentialOnClick(" + col[2] + ")' /></td>";
                            else
                                tab += "<td style='width:5%;text-align:center'></td>";
                            tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>";
                            tab += "<td style='width:10%;text-align:left'>" + col[4] + "</td>";
                            tab += "<td style='width:15%;text-align:left'>" + col[5] + "</td>";
                            tab += "<td style='width:15%;text-align:left'>" + col[6] + "</td>";

                            if (col[7] == "")
                                tab += "<td style='width:15%;text-align:left'>" + col[7] + "</td>";
                            else
                                tab += "<td style='width:15%;text-align:left;cursor:pointer;'>" + col[7] + "<br/><a href='ViewPrevResponseRpt.aspx?TransID=" + col[2] + "' style='text-align:right;' target='_blank' >Previous</a></td>";

                            if (col[10] == 0) {
                                var txtBox = "<textarea id='txtResponse" + col[2] + "' name='txtResponse" + col[2] + "' disabled=true style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)'  onblur='UpdateString(" + col[2] + ",1)'></textarea>";
                                tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>";
                            }
                            else
                                tab += "<td style='width:15%;text-align:left'></td>";

                            tab += "<td style='width:15%;text-align:left'>" + col[8] + "</td>";
                            tab += "</tr>";
                        }
                    }
                    tab += "</table></div>";
                    document.getElementById("<%= pnlObserve.ClientID %>").innerHTML = tab;
                }
                //--------------------- Clearing Data ------------------------//
            }
            function TablesFill() {
                if (document.getElementById("<%= hdnObservation.ClientID %>").value == "") {
                    document.getElementById("rowObserve").style.display = "none";
                }
                else {
                    document.getElementById("rowObserve").style.display = "";
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div style='width:100%; height:auto; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    tab += "<tr >";
                    tab += "<td style='width:5%;text-align:center' >#</td>";
                    tab += "<td style='width:15%;text-align:left'>Document For Transaction</td>";
                    tab += "<td style='width:15%;text-align:left'>Reference</td>";
                    tab += "<td style='width:15%;text-align:left'>Clause</td>";
                    tab += "<td style='width:15%;text-align:left'>Comments</td>";
                    tab += "<td style='width:20%;text-align:left'>Response</td>";
                    tab += "<td style='width:15%;text-align:left'>Remark</td>";
                    tab += "</tr>";
                    tab += "</table></div>";
                    tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    if (document.getElementById("<%= hdnObservation.ClientID %>").value != "") {

                        row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");

                        for (n = 0; n <= row.length - 2; n++) {
                            col = row[n].split("ʘ");
                            if (row_bg == 0) {
                                row_bg = 1;
                                tab += "<tr class='sub_first';>";
                            }
                            else {
                                row_bg = 0;
                                tab += "<tr class='sub_second';>";
                            }
                            i = n + 1;
                            if (col[10] == 0)
                                tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSelection" + col[2] + "' onclick='ChkPotentialOnClick(" + col[2] + ")' /></td>";
                            else
                                tab += "<td style='width:5%;text-align:center'></td>";
                            tab += "<td style='width:15%;text-align:left'>" + col[3] + "<br/><a href='ViewPrevResponseRpt.aspx?TransID=" + col[2] + "' style='text-align:right;' target='_blank' >Previous</a></td>";
                            tab += "<td style='width:15%;text-align:left'>" + col[4] + "</td>";
                            tab += "<td style='width:15%;text-align:left'>" + col[5] + "</td>";
                            tab += "<td style='width:15%;text-align:left'>" + col[6] + "</td>";

                            var txtBox = "<textarea id='txtResponse" + col[2] + "' name='txtResponse" + col[2] + "' disabled=true style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onblur='UpdateString(" + col[2] + ",1)'>" + col[7] + "</textarea>";
                            tab += "<td style='width:20%;text-align:left'>" + txtBox + "</td>";  

                            tab += "<td style='width:15%;text-align:left'>" + col[8] + "</td>";
                            tab += "</tr>";
                        }
                    }
                    tab += "</table></div>";
                    document.getElementById("<%= pnlObserve.ClientID %>").innerHTML = tab;
                }
                //--------------------- Clearing Data ------------------------//
            }
            function ChkPotentialOnClick(val) {
                if (document.getElementById("chkSelection" + val).checked == true) {
                    document.getElementById("txtResponse" + val).disabled = false;
                    document.getElementById("txtResponse" + val).focus();
                }
                else {
                    document.getElementById("txtResponse" + val).disabled = true;
                }
                UpdateString(val, 1);
            }
            function UpdateString(Val, Index) {
                var i = 0;
                var Newstr = "";
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("ʘ");
                    if (col[2] == Val) {
                        if (Index == 1)
                            Newstr += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[3] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "ʘ" + document.getElementById("txtResponse" + col[2]).value + "ʘ" + col[8] + "ʘ1ʘ" + col[10] + "¶";
                    }
                    else
                        Newstr += row[n] + "¶";
                }
                document.getElementById("<%= hdnObservation.ClientID %>").value = Newstr;
            }
            function ViewDocument(VetID) {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowVetFormat.aspx?RequestID=" + RequestID + "&VetID=" + VetID);
                return false;
            }
            function btnExit_onclick() {
                //Return to Old Report
                if (window.opener != null && !window.opener.closed) {
                    window.opener.location.reload();
                    window.close();
                }
                window.onbeforeunload = RefreshParent;
            }
            
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 100%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 97%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 100%; margin: 0px auto;">
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Party with which transaction is plan
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtParty" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Statement Of Facts
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtStatement" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td id="colResolution" style="width: 10%">
                        <asp:ImageButton ID="cmdView" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" />
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Nature of transaction
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtNature" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDocuments">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlDocDtl" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        General Terms
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtTerms" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Approving Authority
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtApproveBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowRemark">                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>

                    <td style="width: 20%; text-align: left;">
                        Remarks
                    </td>

                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtRemarks" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>

                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowRespRemark">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 20%; text-align: left;">
                        Responser Remark</td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtRespRem" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True"/>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 20%; text-align: left;">
                        Enter Any Remarks</td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtAnyRemark" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: right;">
                        &nbsp;
                    </td>
                    <td style="width: 60%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowObserve">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlObserve" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4">
                        <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="HIGHER APPROVAL" onclick="return btnApprove_onclick()" />
                        <input id="btnClosure" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="SEND TO LEGAL" onclick="return btnClosure_onclick()" />
                        <input id="btnDrop" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="DROP" onclick="return btnDrop_onclick()" />
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocument" runat="server" />
            <asp:HiddenField ID="hdnObservation" runat="server" />
            <asp:HiddenField ID="hdnObserveDtl" runat="server" />
            <asp:HiddenField ID="hdnIsHOD" runat="server" />
            <asp:HiddenField ID="hdnStatus" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
