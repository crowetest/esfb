﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf

Partial Class Vet_ViewVetReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim RequestID As Integer
    Dim VetNo As String
    Dim From, I As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            RequestID = CInt(Request.QueryString.Get("RequestID"))

            Dim DT, DT_DOC, DT_COM, DT_CNT As New DataTable
            Dim DTAudit As New DataTable
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            RH.Heading(Session("FirmName"), tb, "VET REPORT", 100)
            RH.BlankRow(tb, 5)
            DT = DB.ExecuteDataSet("SELECT isnull(v.VET_NO,'')Vet_No,isnull(p.PARTY_NAME,'')PARTY_NAME,isnull(v.GENERAL_TERM,'')GENERAL_TERM,isnull(e.Emp_Name ,'')HigherApprrover,isnull(e2.Emp_Name,'') RequestBy,isnull(e3.Emp_Name,'') VetBy, isnull(e4.Emp_Name,'') ApproveBy,v.STATUS_ID FROM VET_MASTER v inner join VET_PARTY p on v.PARTY_ID=p.PARTY_ID left join EMP_MASTER e on v.APPROVE_BY=e.Emp_Code left join EMP_MASTER e2 on v.REQUEST_BY=e2.Emp_Code  left join EMP_MASTER e3 on v.VET_BY=e3.Emp_Code left join EMP_MASTER e4 on v.APP_BY=e4.Emp_Code WHERE v.SERVICE_ID=4 AND v.REQUEST_ID=" & RequestID & "").Tables(0)
            If DT.Rows.Count > 0 Then
                VetNo = DT.Rows(0)(0)

                Dim TRSub As New TableRow
                Dim TRSub0, TRSub1, TRSub2 As New TableCell
                RH.AddColumn(TRSub, TRSub0, 13, 13, "l", "<body align=left ><b><font size=2.5>VET NO</b></body>")
                RH.AddColumn(TRSub, TRSub1, 2, 2, "l", ":")
                RH.AddColumn(TRSub, TRSub2, 85, 85, "l", "<body align=left ><b><font size=2.5> " & VetNo & "</b></body>")
                tb.Controls.Add(TRSub)

                RH.BlankRow(tb, 2)

                Dim TRTO As New TableRow
                Dim TRTO0, TRTO1, TRTO2 As New TableCell
                RH.AddColumn(TRTO, TRTO0, 13, 13, "l", "<body align=left ><b><font size=2.5>SUBMITTED TO</b></body>")
                RH.AddColumn(TRTO, TRTO1, 2, 2, "l", ":")
                RH.AddColumn(TRTO, TRTO2, 85, 85, "l", "<body align=left ><b><font size=2.5> " & DT.Rows(0)(3) & "</b></body>")
                tb.Controls.Add(TRTO)

                DT_DOC = DB.ExecuteDataSet("select d.DOCUMENT_NAME,t.VET_FLAG,t.VET_ID from VET_TRANSACTION_MASTER t, VET_DOCUMENT d where t.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and t.REQUEST_ID=" & RequestID & "").Tables(0)
                Dim DocName As String = Nothing
                Dim DocRow As Integer = DT_DOC.Rows.Count
                Dim Index As Integer = 1
                For Each DR In DT_DOC.Rows
                    If Index < DocRow Then
                        DocName += DR(0).ToString + ","
                    Else
                        DocName += DR(0).ToString
                    End If
                    Index = Index + 1
                Next
                Dim TRDoc As New TableRow
                Dim TRDoc0 As New TableCell
                RH.AddColumn(TRDoc, TRDoc0, 100, 100, "l", "<body align=left ><font size=2><p>This report is based on " & DocName & " of " & DT.Rows(0)(1) & " through " & DT.Rows(0)(4) & "</p></body>")
                tb.Controls.Add(TRDoc)

                DT_CNT = DB.ExecuteDataSet("select COUNT(VET_ID) from VET_TRANSACTION_MASTER where REQUEST_ID=" & RequestID & " and VET_FLAG=1").Tables(0)
                If (DT_CNT.Rows(0)(0) > 0) Then

                    Dim TRVET As New TableRow
                    Dim TRVET0, TRVET1 As New TableCell
                    RH.AddColumn(TRVET, TRVET0, 13, 13, "l", "<body align=left ><b><font size=2.5>DOCUMENTS VETTED</b></body>")
                    RH.AddColumn(TRVET, TRVET1, 2, 2, "l", ":")
                    tb.Controls.Add(TRVET)

                    RH.BlankRow(tb, 3)

                    Dim DRDOC As DataRow
                    I = 0
                    For Each DRDOC In DT_DOC.Rows
                        Dim TRVetDoc As New TableRow
                        Dim TRVetDoc0, TRVetDoc1 As New TableCell
                        I = I + 1
                        If (CInt(DRDOC(1)) = 1) Then
                            RH.AddColumn(TRVetDoc, TRVetDoc0, 5, 5, "c", I.ToString()) 'SlNO
                            RH.AddColumn(TRVetDoc, TRVetDoc1, 95, 95, "l", DRDOC(0)) 'DocName
                        End If
                        tb.Controls.Add(TRVetDoc)
                    Next
                    RH.BlankRow(tb, 3)
                End If

                Dim TRGen As New TableRow
                Dim TRGen0, TRGen1 As New TableCell
                RH.AddColumn(TRGen, TRGen0, 13, 13, "l", "<body align=left ><b><font size=2.5>GENERAL TERMS</b></body>")
                RH.AddColumn(TRGen, TRGen1, 2, 2, "l", ":")
                tb.Controls.Add(TRGen)

                Dim TRPar As New TableRow
                Dim TRPar0 As New TableCell
                RH.AddColumn(TRPar, TRPar0, 100, 100, "l", "<body align=left ><font size=2><p>" & DT.Rows(0)(2) & "</p></body>")
                tb.Controls.Add(TRPar)

                If (DT_CNT.Rows(0)(0) > 0) Then
                    Dim TRCom As New TableRow
                    Dim TRCom0 As New TableCell
                    RH.AddColumn(TRCom, TRCom0, 100, 100, "l", "<body align=left ><font size=2><p>Comments, Responses and Remarks are as follows:</p></body>")
                    tb.Controls.Add(TRCom)

                    DT_COM = DB.ExecuteDataSet("select v.VET_ID,v.DOC_ID,v.TRANS_ID,d.DOCUMENT_NAME,upper(v.REFERENCE) reference,upper(v.CLAUSE) clause,upper(v.COMMENT) comment,upper(v.RESPONSE) response,upper(v.REMARK) remark,isnull(CONVERT(varchar(10),v.SATISFY_FLAG),0) satisfy from VET_TRANSACTION_DTL v,VET_DOCUMENT d where v.DOC_ID=d.DOCUMENT_ID and v.STATUS_ID>=0 and v.REQUEST_ID=" & RequestID & "").Tables(0)
                    If (DT_COM.Rows.Count > 0) Then
                        Dim RowBG As Integer = 0
                        Dim I As Integer = 0

                        Dim TRHead As New TableRow
                        TRHead.BackColor = Drawing.Color.WhiteSmoke
                        Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07 As New TableCell

                        TRHead_00.BorderWidth = "1"
                        TRHead_01.BorderWidth = "1"
                        TRHead_02.BorderWidth = "1"
                        TRHead_03.BorderWidth = "1"
                        TRHead_04.BorderWidth = "1"
                        TRHead_05.BorderWidth = "1"
                        TRHead_06.BorderWidth = "1"
                        TRHead_07.BorderWidth = "1"

                        TRHead_00.BorderColor = Drawing.Color.Silver
                        TRHead_01.BorderColor = Drawing.Color.Silver
                        TRHead_02.BorderColor = Drawing.Color.Silver
                        TRHead_03.BorderColor = Drawing.Color.Silver
                        TRHead_04.BorderColor = Drawing.Color.Silver
                        TRHead_05.BorderColor = Drawing.Color.Silver
                        TRHead_06.BorderColor = Drawing.Color.Silver
                        TRHead_07.BorderColor = Drawing.Color.Silver

                        TRHead_00.BorderStyle = BorderStyle.Solid
                        TRHead_01.BorderStyle = BorderStyle.Solid
                        TRHead_02.BorderStyle = BorderStyle.Solid
                        TRHead_03.BorderStyle = BorderStyle.Solid
                        TRHead_04.BorderStyle = BorderStyle.Solid
                        TRHead_05.BorderStyle = BorderStyle.Solid
                        TRHead_06.BorderStyle = BorderStyle.Solid
                        TRHead_07.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No")
                        RH.AddColumn(TRHead, TRHead_01, 15, 15, "l", "Document")
                        RH.AddColumn(TRHead, TRHead_02, 15, 15, "l", "Reference")
                        RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Clause")
                        RH.AddColumn(TRHead, TRHead_04, 10, 10, "l", "Comment")
                        RH.AddColumn(TRHead, TRHead_05, 15, 15, "l", "Response")
                        RH.AddColumn(TRHead, TRHead_06, 15, 15, "l", "Remark")
                        RH.AddColumn(TRHead, TRHead_07, 15, 15, "l", "Status")
                        tb.Controls.Add(TRHead)

                        For Each DR In DT_COM.Rows
                            I = I + 1
                            Dim TR3 As New TableRow
                            TR3.BorderWidth = "1"
                            TR3.BorderStyle = BorderStyle.Solid

                            Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07 As New TableCell

                            TR3_00.BorderWidth = "1"
                            TR3_01.BorderWidth = "1"
                            TR3_02.BorderWidth = "1"
                            TR3_03.BorderWidth = "1"
                            TR3_04.BorderWidth = "1"
                            TR3_05.BorderWidth = "1"
                            TR3_06.BorderWidth = "1"
                            TR3_07.BorderWidth = "1"

                            TR3_00.BorderColor = Drawing.Color.Silver
                            TR3_01.BorderColor = Drawing.Color.Silver
                            TR3_02.BorderColor = Drawing.Color.Silver
                            TR3_03.BorderColor = Drawing.Color.Silver
                            TR3_04.BorderColor = Drawing.Color.Silver
                            TR3_05.BorderColor = Drawing.Color.Silver
                            TR3_06.BorderColor = Drawing.Color.Silver
                            TR3_07.BorderColor = Drawing.Color.Silver

                            TR3_00.BorderStyle = BorderStyle.Solid
                            TR3_01.BorderStyle = BorderStyle.Solid
                            TR3_02.BorderStyle = BorderStyle.Solid
                            TR3_03.BorderStyle = BorderStyle.Solid
                            TR3_04.BorderStyle = BorderStyle.Solid
                            TR3_05.BorderStyle = BorderStyle.Solid
                            TR3_06.BorderStyle = BorderStyle.Solid
                            TR3_07.BorderStyle = BorderStyle.Solid

                            RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString()) 'SlNO
                            RH.AddColumn(TR3, TR3_01, 15, 15, "l", DR(3)) 'Document
                            RH.AddColumn(TR3, TR3_02, 15, 15, "l", DR(4)) 'Reference                    
                            RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(5)) 'Clause
                            RH.AddColumn(TR3, TR3_04, 10, 10, "l", DR(6)) 'Comment
                            If Not IsDBNull(DR(7)) Then
                                RH.AddColumn(TR3, TR3_05, 15, 15, "l", DR(7) & "<br/><a href='ViewPrevResponseRpt.aspx?TransID=" & DR(2) & "' style='text-align:right;' target='_blank' >Previous</a>") 'Response
                            Else
                                RH.AddColumn(TR3, TR3_05, 15, 15, "l", "") 'Response
                            End If
                            If Not IsDBNull(DR(8)) Then
                                RH.AddColumn(TR3, TR3_06, 15, 15, "l", DR(8)) 'Remark
                            Else
                                RH.AddColumn(TR3, TR3_06, 15, 15, "l", "") 'Remark
                            End If
                            If (DR(9) = 0) Then
                                RH.AddColumn(TR3, TR3_07, 15, 15, "l", "NOT SATISFY")
                            Else
                                RH.AddColumn(TR3, TR3_07, 15, 15, "l", "SATISFY")
                            End If
                            tb.Controls.Add(TR3)
                        Next
                    End If

                    RH.BlankRow(tb, 3)
                    Dim TRSubmit As New TableRow
                    Dim TRSubmit0 As New TableCell
                    RH.AddColumn(TRSubmit, TRSubmit0, 100, 100, "l", "<body align=left ><font size=2>Subject to the comments, it can be executed</body>")
                    tb.Controls.Add(TRSubmit)
                End If

                RH.BlankRow(tb, 3)
                Dim TRLine As New TableRow
                Dim TRLine0 As New TableCell
                RH.AddColumn(TRLine, TRLine0, 100, 100, "c", "<body align=left ><font size=2>-----------------------------------------------------------------------------------------------------</body>")
                tb.Controls.Add(TRLine)

                RH.BlankRow(tb, 3)
                Dim TRBy As New TableRow
                Dim TRBy0 As New TableCell
                RH.AddColumn(TRBy, TRBy0, 100, 100, "c", "<body align=left ><font size=2><b>Vetted By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Verified By</b></body>")
                tb.Controls.Add(TRBy)

                RH.BlankRow(tb, 30)
                If (DT.Rows(0)(7) <> 7) Then
                    Dim TRCur As New TableRow
                    Dim TRCur0, TRCur1, TRCur2 As New TableCell
                    RH.AddColumn(TRCur, TRCur0, 35, 35, "c", "")
                    RH.AddColumn(TRCur, TRCur1, 30, 30, "l", "<body align=left ><font size=2><p>Documents in the current form</p></body>")
                    RH.AddColumn(TRCur, TRCur2, 35, 35, "c", "")
                    tb.Controls.Add(TRCur)

                    RH.BlankRow(tb, 2)
                    Dim TRExe As New TableRow
                    Dim TRExe0, TRExe1, TRExe2 As New TableCell
                    Dim Status As Integer = CInt(DT.Rows(0)(7))
                    Dim ExeType As String
                    If Status = 8 Then
                        ExeType = "To Be Executed"
                    ElseIf Status = 9 Then
                        ExeType = "To Be Changed"
                    ElseIf Status = 10 Then
                        ExeType = "Not To Be Executed"
                    ElseIf Status = 11 Then
                        ExeType = "To Be Hold"
                    End If
                    TRExe1.BorderWidth = "1"
                    TRExe1.BorderColor = Drawing.Color.Silver
                    TRExe1.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TRExe, TRExe0, 35, 35, "c", "")
                    RH.AddColumn(TRExe, TRExe1, 30, 30, "c", "<body align=left ><font size=2><b>" & ExeType & "</b></body>")
                    RH.AddColumn(TRExe, TRExe2, 35, 35, "c", "")
                    tb.Controls.Add(TRExe)

                    RH.BlankRow(tb, 20)
                    Dim TRHigh As New TableRow
                    Dim TRHigh0 As New TableCell
                    RH.AddColumn(TRHigh, TRHigh0, 100, 100, "r", "<body align=left ><font size=2><b>" & DT.Rows(0)(3) & "&nbsp;&nbsp;&nbsp;&nbsp;</b></body>")
                    tb.Controls.Add(TRHigh)
                End If
            End If
            RH.BlankRow(tb, 30)
            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Report  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub


    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub


End Class
