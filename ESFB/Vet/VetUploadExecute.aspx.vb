﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Vet_VetUploadExecute
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT_VET, DT_DOC As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub Vet_VettingTransaction_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            Me.hdnRequest.Value = CStr(RequestID)
            Me.Master.subtitle = "Upload Executed Version"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.cmdView.Attributes.Add("onclick", "return ViewAttachment(1)")
            Me.btnSave.Attributes.Add("onclick", "return SaveOnClick()")
            Me.hlVetRpt.Attributes.Add("onclick", "return GetVetReport()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Vet Request For Approve
                DT = GF.GetQueryResult("select p.PARTY_NAME+'ÆÆ'+upper(v.STATEMENT_FACT)+'Æ'+n.NATURE_NAME+'Æ'+isnull((select convert(varchar(10),case when a.CON_RESOLUTION IS NOT NULL then 1 else 0 end) from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+ isnull((select convert(varchar(10),count( m.REQUEST_ID)) from VET_TRANSACTION_MASTER m where m.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+v.GENERAL_TERM from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.STATUS_ID in(12,14) and v.SERVICE_ID=4 and v.REQUEST_ID=" & RequestID & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn = DT.Rows(0)(0).ToString() + "Æ"

                    DT_VET = GF.GetQueryResult("SELECT CONVERT(varchar(10),v.VET_ID)+'®'+CONVERT(varchar(10),v.DOC_ID)+'®'+d.DOCUMENT_NAME+'®'+isnull(case when v.VET_FLAG = 1 then (select convert(varchar(10),t.PkId)+'®'+t.DOCFILE_NAME from DMS_ESFB.dbo.VET_TRANSDOC_ATTACHMENT t where t.VET_ID=v.VET_ID and t.DOC_ID=v.DOC_ID) else '' end,'') FROM VET_TRANSACTION_MASTER v, VET_DOCUMENT d where v.DOC_ID=d.DOCUMENT_ID and REQUEST_ID=" & RequestID & " and v.STATUS_ID=2")
                    If DT_VET.Rows.Count > 0 Then
                        For Each DR In DT_VET.Rows
                            CallBackReturn += DR(0).ToString() + "¶"
                        Next
                    End If
                    CallBackReturn += "Æ"
                    DT_DOC = GF.GetQueryResult("select CONVERT(varchar(10),v.DOC_ID) +'^'+ CONVERT(varchar(10),v.VET_ID),d.DOCUMENT_NAME from VET_TRANSACTION_MASTER v, VET_DOCUMENT d where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and v.EXECUTE_DT IS NOT NULL and v.REQUEST_ID= " & RequestID & " and v.STATUS_ID=2")
                    If DT_DOC.Rows.Count > 0 Then
                        For Each DR In DT_DOC.Rows
                            CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                        Next
                    End If
                End If
        End Select
    End Sub
#End Region

#Region "Confirm"
    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim OutData As String()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim ExeCount As Integer = 0
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim Document As String() = Me.hdnVetDtl.Value.Split(CChar("^"))
            Dim DocID As Integer = CInt(Document(0))
            Dim VetID As Integer = CInt(Document(1))

            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim FileName As String = ""
            Dim myFile As HttpPostedFile = fupExecute.PostedFile
            Dim nFileLen As Integer = myFile.ContentLength
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, nFileLen)
                Dim fileExtension As String = Path.GetExtension(myFile.FileName)
                If Not (fileExtension = ".xls" Or fileExtension = ".xlsx" Or fileExtension = ".jpg" Or fileExtension = ".jpeg" Or fileExtension = ".doc" Or fileExtension = ".docx" Or fileExtension = ".zip" Or fileExtension = ".pdf" Or fileExtension = ".PDF" Or fileExtension = ".XLS" Or fileExtension = ".XLSX" Or fileExtension = ".JPG" Or fileExtension = ".JPEG" Or fileExtension = ".DOC" Or fileExtension = ".DOCX" Or fileExtension = ".ZIP") Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
            End If

            Try
                Dim Param(7) As SqlParameter
                Param(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                Param(0).Value = RequestID
                Param(1) = New SqlParameter("@VetID", SqlDbType.Int)
                Param(1).Value = VetID
                Param(2) = New SqlParameter("@DocID", SqlDbType.Int)
                Param(2).Value = DocID
                Param(3) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                Param(3).Value = AttachImg
                Param(4) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                Param(4).Value = ContentType
                Param(5) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                Param(5).Value = FileName
                Param(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Param(6).Direction = ParameterDirection.Output
                Param(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Param(7).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_VET_EXECUTE_ATTACHMENT", Param)
                OutData = CStr(Param(7).Value).Split(CChar("^"))
                Message = CStr(OutData(0))
                ExeCount = CInt(OutData(1))
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            If ExeCount > 0 Then
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_script1.Append("         alert('Confirmed Successfully');")
                cl_script1.Append(" window.open('VetUploadExecute.aspx?RequestID=" & RequestID & "','_self');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            Else
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_script1.Append("         alert('Confirmed Successfully');")
                cl_script1.Append(" window.open('../Home.aspx', '_self');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
    
End Class
