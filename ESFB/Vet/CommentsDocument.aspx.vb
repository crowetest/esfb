﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Vet_CommentsDocument
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer

#Region "Page Load & Dispose"
    Protected Sub Vet_CommentsDocument_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub Vet_CommentsDocument_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            Me.hdnRequest.Value = CStr(RequestID)
            Me.Master.subtitle = "Comments On Documents"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.btnUpload.Attributes.Add("onclick", "return UploadOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim DR As DataRow
        Select CInt(Data(0))
            Case 1 'Fill Compliance For Approve
                DT = GF.GetQueryResult("SELECT d.DOCUMENT_NAME+'Æ'+upper(v.COMT_PURPOSE)+'Æ'+upper(v.COMT_EXPECTATION)+'Æ'+isnull((select CONVERT(varchar(10),count(a.REQUEST_ID),0) from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID),0) FROM VET_MASTER v, VET_DOCUMENT d WHERE v.COMT_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and d.DOCUMENT_TYPE=1 and v.STATUS_ID in(1,5) and v.SERVICE_ID=3 and REQUEST_ID=" & RequestID & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn = DT.Rows(0)(0).ToString()
                End If
                CallBackReturn += "Æ"
                Dim RequestData() As String = CStr(DT.Rows(0)(0)).Split(CChar("Æ"))
                If CInt(RequestData(3)) > 0 Then
                    DT = GF.GetQueryResult("SELECT convert(varchar(10),PkId)+ '®' +convert(varchar(10),REQUEST_ID)+ '®' + FILE_NAME FROM DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT where SERVICE_ID=3 and REQUEST_ID=" & RequestID & "")
                    If DT.Rows.Count > 0 Then
                        For Each DR In DT.Rows
                            CallBackReturn += DR(0).ToString() + "¶"
                        Next
                    End If
                End If
            Case 2
                'Approve Confirm
                Dim RequestID As Integer = CInt(Data(1))
                Dim CommentDtl As String = CStr(Data(2))
                Dim Comments As String = ""
                Dim Reference As String = ""
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(5) As SqlParameter
                    Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(0).Value = RequestID
                    Params(1) = New SqlParameter("@ServiceID", SqlDbType.Int)
                    Params(1).Value = 3
                    Params(2) = New SqlParameter("@CommentDtl", SqlDbType.VarChar, 8000)
                    Params(2).Value = CommentDtl
                    Params(3) = New SqlParameter("@UserID", SqlDbType.VarChar, 8000)
                    Params(3).Value = CInt(Session("UserID"))
                    Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(5).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_COMMENTS", Params)
                    ErrorFlag = CInt(Params(4).Value)
                    Message = CStr(Params(5).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region

#Region "Upload Document"
    Protected Sub btnUpload_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnUpload.Click
        Try
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim UserID As Integer = CInt(Session("UserID"))
            RequestID = CInt(Me.hdnRequest.Value)
            Dim PkID As Integer = CInt(Me.hdnPkID.Value)
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim FileName As String = ""
            Dim myFile As HttpPostedFile = fupAttDraft.PostedFile
            Dim nFileLen As Integer = myFile.ContentLength
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(nFileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, nFileLen)
                Dim fileExtension As String = Path.GetExtension(myFile.FileName)
                If Not (fileExtension = ".xls" Or fileExtension = ".xlsx" Or fileExtension = ".jpg" Or fileExtension = ".jpeg" Or fileExtension = ".doc" Or fileExtension = ".docx" Or fileExtension = ".zip" Or fileExtension = ".pdf" Or fileExtension = ".PDF" Or fileExtension = ".XLS" Or fileExtension = ".XLSX" Or fileExtension = ".JPG" Or fileExtension = ".JPEG" Or fileExtension = ".DOC" Or fileExtension = ".DOCX" Or fileExtension = ".ZIP") Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
            End If

            Try
                If RequestID > 0 Then
                    Dim Param(6) As SqlParameter
                    Param(0) = New SqlParameter("@PkID", SqlDbType.Int)
                    Param(0).Value = PkID
                    Param(1) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Param(1).Value = RequestID
                    Param(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                    Param(2).Value = AttachImg
                    Param(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                    Param(3).Value = ContentType
                    Param(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                    Param(4).Value = FileName
                    Param(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Param(5).Direction = ParameterDirection.Output
                    Param(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Param(6).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_REDRAFT_COMMENTS", Param)
                    ErrorFlag = CInt(Param(5).Value)
                    Message = CStr(Param(6).Value)
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("alert('" + Message + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
    
End Class
