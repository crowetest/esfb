﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="AddNewDocuments.aspx.vb" Inherits="AddNewDocuments" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
        
    function ViewExistingDocumentsRpt() 
    {
        var ServiceID = document.getElementById("<%= cmbService.ClientID %>").value;
        var VetStatus= document.getElementById("<%= cmbSelect.ClientID %>").value;    
        window.open("ViewExistingDocumentsRpt.aspx?ServiceID=" + ServiceID +"&VetStatus= "+ VetStatus, "_blank");
    }
    function window_onload()         
    {         
        document.getElementById("rowDocu").style.display = "";
        document.getElementById("rowDocuForVet").style.display = "none";
        document.getElementById("rowNOfDocu").style.display = "none";  
        document.getElementById("rowSAT").style.display = "none";       
    }
    function ServiceOnChange() 
    {         
        document.getElementById("rowDocu").style.display = "";
        document.getElementById("rowDocuForVet").style.display = "";
        document.getElementById("rowNOfDocu").style.display = "";  
        document.getElementById("rowSAT").style.display = "";
        document.getElementById("<%= txtDocument.ClientID %>").value = "";
        document.getElementById("<%= txtNatureOfDocu.ClientID %>").value = "";
        document.getElementById("<%= txtDocuForVeting.ClientID %>").value = "";

        var ServiceID = document.getElementById("<%= cmbService.ClientID %>").value;
        if (ServiceID == 1) //Drafting
        {
            document.getElementById("rowDocu").style.display = "";
            document.getElementById("rowDocuForVet").style.display = "none";
            document.getElementById("rowNOfDocu").style.display = "none";
            document.getElementById("rowSAT").style.display = "none";
        }
        else if (ServiceID == 3)//Comments 
        {
            document.getElementById("rowDocu").style.display = "";
            document.getElementById("rowDocuForVet").style.display = "none";
            document.getElementById("rowNOfDocu").style.display = "none";
            document.getElementById("rowSAT").style.display = "none";
        }
        else if (ServiceID == 4)//Vetting
        {
            document.getElementById("rowDocu").style.display = "none ";
            document.getElementById("rowSAT").style.display = "";
            document.getElementById("rowDocuForVet").style.display = "none";
            document.getElementById("rowNOfDocu").style.display = "none";  
        }
        else if (ServiceID == 5)//Archiving
        {
            document.getElementById("rowDocuForVet").style.display = "none";
            document.getElementById("rowNOfDocu").style.display = "none";
            document.getElementById("rowSAT").style.display = "none";
        }
    }
    function SelectOnChange()
    { 
        var element=document.getElementById("<%= cmbSelect.ClientID %>").value;   
        if(element=="1")          
        {           
            document.getElementById("rowDocuForVet").style.display = "";
            document.getElementById("rowNOfDocu").style.display = "none";             
        }
        else
        {            
            document.getElementById("rowDocuForVet").style.display = "none";
            document.getElementById("rowNOfDocu").style.display = ""; 
        }          
    }
    function ViewParty() {
        window.open("ViewPartyRpt.aspx");
    }

    function btnSave_onclick() 
    {
        var Service_Id = document.getElementById("<%= cmbService.ClientID %>").value;                
        var DocumentName = document.getElementById("<%= txtDocument.ClientID %>").value;
        if(Service_Id!=4)
        {                
            if (document.getElementById("<%= txtDocument.ClientID %>").value == "")
            {
                alert("Enter Document");
                document.getElementById("<%= txtDocument.ClientID %>").focus();
                return false;
            }
        }
        else
        {
            var VetStatus= document.getElementById("<%= cmbSelect.ClientID %>").value;
            if (document.getElementById("<%= cmbSelect.ClientID %>").value == 0)
            {
            alert("Please Select Type");
            //document.getElementById("<%= txtDocument.ClientID %>").focus();
            return false;
            }                                
            if(VetStatus!=1)
            {                
                var NOD=document.getElementById("<%= txtNatureOfDocu.ClientID %>").value;
                if (document.getElementById("<%= txtNatureOfDocu.ClientID %>").value == "")
                {
                    alert("Enter Nature Of Documents");
                    document.getElementById("<%= txtNatureOfDocu.ClientID %>").focus();
                    return false;
                }
            }
            else 
            {               
                var DFV=document.getElementById("<%= txtDocuForVeting.ClientID %>").value;
                if (document.getElementById("<%= txtDocuForVeting.ClientID %>").value == "")
                {
                    alert(" Enter Document For Vetting");
                    document.getElementById("<%= txtDocuForVeting.ClientID %>").focus();
                    return false;
                }
            }                                     
        }                
        ToServer("1ʘ" + Service_Id + "ʘ" + DocumentName + "ʘ" + VetStatus + "ʘ" + NOD + "ʘ" + DFV , 1);                
    }
        function FromServer(Arg, Context) {
            switch (Context) {
                case 1:
                {
                    var Data = Arg.split("ʘ");
                    alert("Successfully Saved");
                    if (Data[0] == 0) {      
                        window.open("AddNewDocuments.aspx","_self");
                    }
                    break;
                }
            }
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }



        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    
<script language="javascript" type="text/javascript" for="window" event="onkeyup">
// <![CDATA[
return window_onkeyup()
// ]]>
</script>
<script language="javascript" type="text/javascript" for="window" event="ondblclick">
// <![CDATA[
return window_ondblclick()
// ]]>
</script>
</head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 81%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: right;">
                        &nbsp;
                    </td>                    
                    <td style="width: 50%; text-align: center; cursor:pointer;">
                        <asp:HyperLink ID="hplView" runat="server" Font-Bold="True" 
                            Font-Underline="True" ForeColor="#6600FF">Existing Documents</asp:HyperLink>
                    </td>
                    <td id="colResolution" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Services 
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbService" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="100%" ForeColor="Black" height="20px">
                        </asp:DropDownList>
                    </td>
                    <td id="colResolution" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDocu">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Enter Document
                     </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtDocument" runat="server" class="NormalText" Height="19px" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowSAT">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Select Add Type</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbSelect" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="100%" ForeColor="Black" height="19px">
                            <asp:ListItem value='0' >Select type</asp:ListItem>
                            <asp:ListItem value='2' >Nature Of Document</asp:ListItem>
                            <asp:ListItem value='1' >Document For Vetting</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 17%">
                        &nbsp;</td>
                </tr>
                <tr id="rowNOfDocu">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Enter Nature Of Document</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtNatureOfDocu" runat="server" class="NormalText" Height="19px" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rowDocuForVet">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Enter Document For Vetting</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtDocuForVeting" runat="server" class="NormalText" Height="19px" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4">
                        <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="SAVE" onclick="return btnSave_onclick()" onclick="return btnSave_onclick()" onclick="validate()" />                        
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocDtl" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
