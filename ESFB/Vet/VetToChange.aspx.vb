﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vet_VetToChange
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT_VET, DT_EMP As New DataTable
    Dim RequestID, DeptID, EmpCode As Integer
#Region "Page Load & Dispose"
    Protected Sub Vet_VettingTransaction_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            Me.hdnRequest.Value = CStr(RequestID)
            Me.Master.subtitle = "Higher Approval Of Vetting"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            If Not IsPostBack Then
                DT = GF.GetQueryResult("select Department_ID,department_name from DEPARTMENT_MASTER where Department_ID in (select Department_ID from EMP_MASTER where Status_ID = 1) order by Department_Name")
                GF.ComboFill(cmbApproveDept, DT, 0, 1)
                Dim Dept As Integer = 0
                If DT.Rows.Count > 0 Then
                    Dept = CInt(Me.cmbApproveDept.SelectedValue)
                End If
                DT = GF.GetQueryResult("select emp_code,emp_name + ' -- ' + convert(varchar(10),emp_code) from EMP_MASTER where Department_ID = " & Dept & " order by emp_name")
                GF.ComboFill(cmbApproveBy, DT, 0, 1)
            End If
            Me.cmdView.Attributes.Add("onclick", "return ViewAttachment(1)")
            Me.cmbApproveDept.Attributes.Add("onchange", "return DeptOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("^"))
        Dim DR, DRS As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Vet Request For Approve
                DT = GF.GetQueryResult("select p.PARTY_NAME+'ÆÆ'+upper(v.STATEMENT_FACT)+'Æ'+n.NATURE_NAME+'Æ'+isnull((select convert(varchar(10),case when a.CON_RESOLUTION IS NOT NULL then 1 else 0 end) from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+ isnull((select convert(varchar(10),count( m.REQUEST_ID)) from VET_TRANSACTION_MASTER m where m.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+upper(v.GENERAL_TERM)+'Æ'+UPPER(v.VET_REMARK)+'Æ'+CONVERT(varchar(10),v.APPROVE_DEPT)+'Æ'+CONVERT(varchar(10),v.APPROVE_BY)+'Æ'+ isnull((select convert(varchar(10),count( m.REQUEST_ID)) from VET_TRANSACTION_DTL m where m.REQUEST_ID=v.REQUEST_ID),0) from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.STATUS_ID =9 and v.SERVICE_ID=4 and v.REQUEST_ID=" & RequestID & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn = DT.Rows(0)(0).ToString() + "Æ"
                    DT_VET = GF.GetQueryResult("SELECT d.DOCUMENT_NAME+'¶'+CONVERT(varchar(10),v.EXECUTE_DEPT)+'¶'+CONVERT(varchar(10),v.EXECUTE_BY)+'¶'+CONVERT(varchar(10),v.VET_FLAG)+'¶'+CONVERT(varchar(10),v.VET_ID) from VET_TRANSACTION_MASTER v, VET_DOCUMENT d where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and v.REQUEST_ID=" & RequestID & "")
                    If DT_VET.Rows.Count > 0 Then
                        For Each DR In DT_VET.Rows
                            CallBackReturn += CStr(DR(0)) + "ʘ"
                        Next
                    End If
                    CallBackReturn += "Æ"
                    Dim VetData() As String = CStr(DT.Rows(0)(0)).Split(CChar("Æ"))
                    If CInt(VetData(10)) > 0 Then
                        DT = GF.GetQueryResult("select v.VET_ID,v.DOC_ID,v.TRANS_ID,d.DOCUMENT_NAME,upper(v.REFERENCE),upper(v.CLAUSE),upper(v.COMMENT),upper(v.RESPONSE),upper(v.REMARK),isnull(CONVERT(varchar(10),v.SATISFY_FLAG),0) from VET_TRANSACTION_DTL v,VET_DOCUMENT d where v.DOC_ID=d.DOCUMENT_ID and v.STATUS_ID>=1 and v.REQUEST_ID=" & RequestID & "")
                        For Each DR In DT.Rows
                            CallBackReturn += DR(0).ToString() + "ʘ" + DR(1).ToString() + "ʘ" + DR(2).ToString() + "ʘ" + DR(3).ToString() + "ʘ" + DR(4).ToString() + "ʘ" + DR(5).ToString() + "ʘ" + DR(6).ToString() + "ʘ" + DR(7).ToString() + "ʘ" + DR(8).ToString() + "ʘ" + DR(9).ToString() + "ʘ0¶"
                        Next
                    End If
                    CallBackReturn += "Æ"
                    DT = GF.GetQueryResult("select Department_ID,department_name from DEPARTMENT_MASTER where Department_ID in (select Department_ID from EMP_MASTER where Status_ID = 1) order by Department_Name")
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                        CallBackReturn += "§"
                        DT_EMP = GF.GetQueryResult("select emp_code,emp_name + ' -- ' + convert(varchar(10),emp_code) from EMP_MASTER where Department_ID = " & CInt(DR(0)) & " order by emp_name")
                        For Each DRS In DT_EMP.Rows
                            CallBackReturn += "Ř" + DRS(0).ToString() + "Ĉ" + DRS(1).ToString()
                        Next
                        CallBackReturn += "£"
                    Next
                    CallBackReturn += "Æ"
                    DT = GF.GetQueryResult("select emp_code,emp_name + ' -- ' + convert(varchar(10),emp_code) from EMP_MASTER where Department_ID = " & CInt(VetData(8)) & " order by emp_name")
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2
                Dim DeptID As Integer = CInt(Data(1))
                Dim BindVal As Integer = CInt(Data(2))
                DT = GF.GetQueryResult("select emp_code,emp_name + ' -- ' + convert(varchar(10),emp_code) from EMP_MASTER where Department_ID = " & DeptID & " order by emp_name")
                For Each DR In DT.Rows
                    CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                Next
                CallBackReturn += "Æ" + BindVal.ToString()
            Case 3
                'Department Change
                Dim DepartmentID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select emp_code,emp_name + ' -- ' + convert(varchar(10),emp_code) from EMP_MASTER where Department_ID = " & DepartmentID & " order by emp_name")
                CallBackReturn = ""
                For Each DR In DT.Rows
                    CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                Next
            Case 4
                Dim RequestID As Integer = CInt(Data(1))
                Dim GenTerm As String = CStr(Data(2))
                Dim AppDept As Integer = CInt(Data(3))
                Dim AppBy As Integer = CInt(Data(4))
                Dim ExecutantDtl As String = CStr(Data(5))
                Dim ObserveDtl As String = CStr(Data(6))
                Dim Remark As String = CStr(Data(7))
                Dim Status As Integer = CInt(Data(8))
                Dim UserID As Integer = CInt(Session("UserID"))

                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(10) As SqlParameter
                    Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(0).Value = RequestID
                    Params(1) = New SqlParameter("@Terms", SqlDbType.VarChar, 1000)
                    Params(1).Value = GenTerm
                    Params(2) = New SqlParameter("@ApproveDept", SqlDbType.Int)
                    Params(2).Value = AppDept
                    Params(3) = New SqlParameter("@ApproveBy", SqlDbType.Int)
                    Params(3).Value = AppBy
                    Params(4) = New SqlParameter("@ExecutantDtl", SqlDbType.VarChar, 5000)
                    Params(4).Value = ExecutantDtl
                    Params(5) = New SqlParameter("@ObserveDtl", SqlDbType.VarChar, 5000)
                    Params(5).Value = ObserveDtl
                    Params(6) = New SqlParameter("@Remarks", SqlDbType.VarChar, 1000)
                    Params(6).Value = Remark
                    Params(7) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(7).Value = Status
                    Params(8) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(8).Value = UserID
                    Params(9) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(9).Direction = ParameterDirection.Output
                    Params(10) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 500)
                    Params(10).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_MODIFY_SEND_VET", Params)
                    ErrorFlag = CInt(Params(9).Value)
                    Message = CStr(Params(10).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region

End Class
