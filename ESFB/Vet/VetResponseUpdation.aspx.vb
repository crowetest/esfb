﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vet_VetResponseUpdation
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT_VET, DT_SUB As New DataTable
    Dim RequestID, UserID, StatusID As Integer
#Region "Page Load & Dispose"
    Protected Sub Vet_VettingTransaction_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            Me.hdnRequest.Value = CStr(RequestID)
            StatusID = CInt(Request.QueryString.Get("StatusID"))
            Me.hdnStatus.Value = CStr(StatusID)

            UserID = CInt(Session("UserID"))
            DT = DB.ExecuteDataSet("select COUNT(Department_ID) from DEPARTMENT_MASTER where Department_Head=" & UserID & "").Tables(0)
            If DT.Rows.Count > 0 Then
                If CInt(DT.Rows(0)(0)) > 0 Then
                    Me.hdnIsHOD.Value = CStr(1)
                Else
                    Me.hdnIsHOD.Value = CStr(0)
                End If
            End If

            Me.Master.subtitle = "Update Response"

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            Me.cmdView.Attributes.Add("onclick", "return ViewAttachment(1)")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("^"))
        Dim DR As DataRow
        Dim DocData() As String
        Select Case CInt(Data(0))
            Case 1 'Fill Vet Request For Approve
                DT = GF.GetQueryResult("select p.PARTY_NAME+'Æ'+upper(v.STATEMENT_FACT)+'Æ'+n.NATURE_NAME+'Æ'+isnull((select convert(varchar(10),case when a.CON_RESOLUTION IS NOT NULL then 1 else 0 end) from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+ isnull((select convert(varchar(10),count( m.REQUEST_ID)) from VET_TRANSACTION_MASTER m where m.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+upper(v.GENERAL_TERM)+'Æ'+UPPER(case when v.APP_REMARK IS NULL then v.VET_REMARK else v.APP_REMARK end)+'Æ'+CONVERT(varchar(10),v.APPROVE_DEPT)+'Æ'+em.Emp_Name+'Æ'+ isnull((select convert(varchar(10),count( m.REQUEST_ID)) from VET_TRANSACTION_DTL m where m.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+isnull(v.RESPONSE_REMARK,'') from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER em where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.APPROVE_BY=em.Emp_Code and v.STATUS_ID= " & StatusID & " and v.SERVICE_ID=4 and v.REQUEST_ID=" & RequestID & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn = DT.Rows(0)(0).ToString() + "Æ"
                    DT_VET = GF.GetQueryResult("SELECT d.DOCUMENT_NAME+'¶'+upper(e.Emp_Name)+'¶'+CONVERT(varchar(10),v.VET_FLAG)+'¶'+CONVERT(varchar(10),v.VET_ID)+'¶'+CONVERT(varchar(10),v.DOC_ID)+'¶'+CONVERT(varchar(10),v.REQUEST_ID)+'¶'+case when V.VET_FLAG= 1 then (select convert(varchar(10), a.PkId) from DMS_ESFB.dbo.VET_TRANSDOC_ATTACHMENT a where a.VET_ID=v.VET_ID and a.DOC_ID=v.DOC_ID) else '0' end from VET_TRANSACTION_MASTER v, VET_DOCUMENT d,EMP_MASTER e where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_ID=" & RequestID & " order by v.VET_ID")
                    If DT_VET.Rows.Count > 0 Then
                        For Each DR In DT_VET.Rows
                            DocData = CStr(DR(0)).Split(CChar("¶"))
                            If CInt(DocData(6)) > 0 Then
                                CallBackReturn += CStr(DR(0)) + "§ʘ"
                            End If
                        Next
                    End If
                    CallBackReturn += "Æ"
                    Dim VetData() As String = CStr(DT.Rows(0)(0)).Split(CChar("Æ"))
                    If CInt(VetData(9)) > 0 Then
                        DT = GF.GetQueryResult("select v.VET_ID,v.DOC_ID,v.TRANS_ID,d.DOCUMENT_NAME,upper(v.REFERENCE),upper(v.CLAUSE),upper(v.COMMENT),upper(v.RESPONSE),upper(v.REMARK),isnull(CONVERT(varchar(10),v.SATISFY_FLAG),0) from VET_TRANSACTION_DTL v,VET_DOCUMENT d where v.DOC_ID=d.DOCUMENT_ID and v.STATUS_ID>=0 and v.REQUEST_ID=" & RequestID & "")
                        For Each DR In DT.Rows
                            CallBackReturn += DR(0).ToString() + "ʘ" + DR(1).ToString() + "ʘ" + DR(2).ToString() + "ʘ" + DR(3).ToString() + "ʘ" + DR(4).ToString() + "ʘ" + DR(5).ToString() + "ʘ" + DR(6).ToString() + "ʘ" + DR(7).ToString() + "ʘ" + DR(8).ToString() + "ʘ0ʘ" + DR(9).ToString() + "¶"
                        Next
                    End If
                End If
            Case 2
                Dim RequestID As Integer = CInt(Data(1))
                Dim ObserveDtl As String = CStr(Data(2))
                Dim Status As Integer = CInt(Data(3))
                Dim IsHOD As Integer = CInt(Data(4))
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim RESPONSEREMARK As String = CStr(Data(5))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(7) As SqlParameter
                    Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(0).Value = RequestID
                    Params(1) = New SqlParameter("@ObserveDtl", SqlDbType.VarChar, 5000)
                    Params(1).Value = ObserveDtl
                    Params(2) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(2).Value = Status
                    Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(3).Value = UserID
                    Params(4) = New SqlParameter("@IsHOD", SqlDbType.Int)
                    Params(4).Value = IsHOD
                    Params(5) = New SqlParameter("@RESPONSE_REMARK", SqlDbType.VarChar, 1000)
                    Params(5).Value = RESPONSEREMARK
                    Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(7).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_UPDATE_RESPONSE", Params)
                    ErrorFlag = CInt(Params(6).Value)
                    Message = CStr(Params(7).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region

End Class
