﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="AddNewParty.aspx.vb" Inherits="Vet_AddNewParty" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

            function ViewParty() {
                window.open("ViewPartyRpt.aspx");
            }
            function btnSave_onclick() {
                var Name = document.getElementById("<%= txtName.ClientID %>").value;
                var Address = document.getElementById("<%= txtAddress.ClientID %>").value;
                ToServer("1ʘ" + Name + "ʘ" + Address, 1);
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        if (Data[0] == 0) {      
                            window.open("AddNewParty.aspx","_self");
                        }
                        break;
                    }
                }
            }
            function btnExit_onclick() {
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: right;">
                        &nbsp;
                    </td>                    
                    <td style="width: 60%; text-align: center; cursor:pointer;">
                        <asp:HyperLink ID="hplView" runat="server" Font-Bold="True" 
                            Font-Underline="True" ForeColor="#6600FF">Existing Party</asp:HyperLink>
                    </td>
                    <td id="colResolution" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Party Name
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtName" Width="100%" class="NormalText" runat="server"></asp:TextBox>
                    </td>
                    <td id="colResolution" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowReason">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Address
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtAddress" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="500" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4">
                        <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="SAVE" onclick="return btnSave_onclick()" />                        
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocDtl" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
