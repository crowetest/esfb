﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AddNewDocuments
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer

#Region "Page Load & Dispose"
    Protected Sub Vet_Opinion_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub Vet_Opinion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 151) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            Me.hdnRequest.Value = CStr(RequestID)

            Me.Master.subtitle = "New Documents"

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            'Me.hplView.Attributes.Add("onclick", "return ViewParty()")
            DT = GF.GetQueryResult("select SERVICE_ID,SERVICE_NAME from VET_SERVICE where STATUS_ID=1 and SERVICE_ID <> 2")
            GF.ComboFill(cmbService, DT, 0, 1)

            Me.cmbService.Attributes.Add("onchange", "return ServiceOnChange()")
            Me.cmbSelect.Attributes.Add("onchange", "return SelectOnChange()")
            Me.hplView.Attributes.Add("onclick", "return ViewExistingDocumentsRpt()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))

        Select Case CInt(Data(0))
            Case 1
                Dim ServiceID As Integer = CInt(Data(1))

                If ServiceID = 1 Then 'Drafting


                    Dim DocName As String = CStr(Data(2))
                    Dim NatureName As String = ""

                    Dim Status As Integer = 1
                    Dim VetStatus As Integer = 0
                    Dim Message As String = Nothing
                    Dim ErrorFlag As Integer = 0

                    Try
                        Dim Params(5) As SqlParameter
                        Params(0) = New SqlParameter("@DocName", SqlDbType.VarChar, 500)
                        Params(0).Value = DocName
                        Params(1) = New SqlParameter("@NatureName", SqlDbType.VarChar, 500)
                        Params(1).Value = NatureName
                        Params(2) = New SqlParameter("@Status", SqlDbType.Int)
                        Params(2).Value = Status
                        Params(3) = New SqlParameter("@VetStatus", SqlDbType.Int)
                        Params(3).Value = VetStatus
                        Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(4).Direction = ParameterDirection.Output
                        Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(5).Direction = ParameterDirection.Output
                        DB.ExecuteNonQuery("SP_VET_NEW_DOCUMENT", Params)
                        ErrorFlag = CInt(Params(4).Value)
                        Message = CStr(Params(5).Value)
                    Catch ex As Exception
                        Message = ex.Message.ToString
                        ErrorFlag = 1
                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                    End Try
                    CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
                ElseIf ServiceID = 3 Then 'Comments

                    Dim DocName As String = CStr(Data(2))
                    Dim NatureName As String = ""

                    Dim Status As Integer = 3
                    Dim VetStatus As Integer = 0
                    Dim Message As String = Nothing
                    Dim ErrorFlag As Integer = 0

                    Try
                        Dim Params(5) As SqlParameter
                        Params(0) = New SqlParameter("@DocName", SqlDbType.VarChar, 500)
                        Params(0).Value = DocName
                        Params(1) = New SqlParameter("@NatureName", SqlDbType.VarChar, 500)
                        Params(1).Value = NatureName
                        Params(2) = New SqlParameter("@Status", SqlDbType.Int)
                        Params(2).Value = Status
                        Params(3) = New SqlParameter("@VetStatus", SqlDbType.Int)
                        Params(3).Value = VetStatus
                        Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(4).Direction = ParameterDirection.Output
                        Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(5).Direction = ParameterDirection.Output
                        DB.ExecuteNonQuery("SP_VET_NEW_DOCUMENT", Params)
                        ErrorFlag = CInt(Params(4).Value)
                        Message = CStr(Params(5).Value)
                    Catch ex As Exception
                        Message = ex.Message.ToString
                        ErrorFlag = 1
                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                    End Try
                    CallBackReturn = ErrorFlag.ToString + "ʘ" + Message


                ElseIf ServiceID = 4 Then 'Vetting for executing transactions
                    Dim VetStatus As Integer = CInt(Data(3))

                    If VetStatus = 1 Then
                        Dim DocName As String = CStr(Data(5))
                        Dim NatureName As String = ""
                        Dim Status As Integer = 4
                        Dim Message As String = Nothing
                        Dim ErrorFlag As Integer = 0

                        Try
                            Dim Params(5) As SqlParameter
                            Params(0) = New SqlParameter("@DocName", SqlDbType.VarChar, 500)
                            Params(0).Value = DocName
                            Params(1) = New SqlParameter("@NatureName", SqlDbType.VarChar, 500)
                            Params(1).Value = NatureName
                            Params(2) = New SqlParameter("@Status", SqlDbType.Int)
                            Params(2).Value = Status
                            Params(3) = New SqlParameter("@VetStatus", SqlDbType.Int)
                            Params(3).Value = VetStatus
                            Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                            Params(4).Direction = ParameterDirection.Output
                            Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                            Params(5).Direction = ParameterDirection.Output
                            DB.ExecuteNonQuery("SP_VET_NEW_DOCUMENT", Params)
                            ErrorFlag = CInt(Params(4).Value)
                            Message = CStr(Params(5).Value)
                        Catch ex As Exception
                            Message = ex.Message.ToString
                            ErrorFlag = 1
                            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                        End Try
                        CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
                    Else
                        Dim DocName As String = ""
                        Dim NatureName As String = CStr(Data(4))
                        Dim Status As Integer = 4
                        Dim Message As String = Nothing
                        Dim ErrorFlag As Integer = 0

                        Try
                            Dim Params(5) As SqlParameter
                            Params(0) = New SqlParameter("@DocName", SqlDbType.VarChar, 500)
                            Params(0).Value = DocName
                            Params(1) = New SqlParameter("@NatureName", SqlDbType.VarChar, 500)
                            Params(1).Value = NatureName
                            Params(2) = New SqlParameter("@Status", SqlDbType.Int)
                            Params(2).Value = Status
                            Params(3) = New SqlParameter("@VetStatus", SqlDbType.Int)
                            Params(3).Value = VetStatus
                            Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                            Params(4).Direction = ParameterDirection.Output
                            Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                            Params(5).Direction = ParameterDirection.Output
                            DB.ExecuteNonQuery("SP_VET_NEW_DOCUMENT", Params)
                            ErrorFlag = CInt(Params(4).Value)
                            Message = CStr(Params(5).Value)
                        Catch ex As Exception
                            Message = ex.Message.ToString
                            ErrorFlag = 1
                            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                        End Try
                        CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
                    End If
                ElseIf ServiceID = 5 Then 'Archiving

                    Dim DocName As String = CStr(Data(2))
                    Dim NatureName As String = ""
                    Dim Status As Integer = 5
                    Dim VetStatus As Integer = 0
                    Dim Message As String = Nothing
                    Dim ErrorFlag As Integer = 0

                    Try
                        Dim Params(5) As SqlParameter
                        Params(0) = New SqlParameter("@DocName", SqlDbType.VarChar, 500)
                        Params(0).Value = DocName
                        Params(1) = New SqlParameter("@NatureName", SqlDbType.VarChar, 500)
                        Params(1).Value = NatureName
                        Params(2) = New SqlParameter("@Status", SqlDbType.Int)
                        Params(2).Value = Status
                        Params(3) = New SqlParameter("@VetStatus", SqlDbType.Int)
                        Params(3).Value = VetStatus
                        Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(4).Direction = ParameterDirection.Output
                        Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(5).Direction = ParameterDirection.Output
                        DB.ExecuteNonQuery("SP_VET_NEW_DOCUMENT", Params)
                        ErrorFlag = CInt(Params(4).Value)
                        Message = CStr(Params(5).Value)
                    Catch ex As Exception
                        Message = ex.Message.ToString
                        ErrorFlag = 1
                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                    End Try
                    CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
                End If


        End Select
    End Sub
#End Region

End Class
