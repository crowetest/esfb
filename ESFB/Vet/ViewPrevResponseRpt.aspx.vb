﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf

Partial Class Vet_ViewPrevResponseRpt
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim TransID As Integer
    Dim GF As New GeneralFunctions
    Dim From As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            TransID = CInt(Request.QueryString.Get("TransID"))

            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            tb.Attributes.Add("width", "100%")

            DT = DB.ExecuteDataSet("select h.ENTER_BY,upper(e.Emp_Name),case when h.STATUS_ID=2 then 'USER' else case when h.STATUS_ID=3 then 'LEGAL' else case when h.STATUS_ID=4 then 'LEGAL HEAD' end end end level,h.ENTER_DT,upper(h.RESPONSE),upper(h.REMARK),u.UPDATE_NAME from VET_TRANSACTION_HISTORY h, EMP_MASTER e, VET_UPDATE_MASTER u where h.ENTER_BY=e.Emp_Code and h.UPDATE_ID=u.UPDATE_ID and h.STATUS_ID=h.STATUS_ID and h.TRANS_ID=" & TransID & " order by h.sl_no,h.ENTER_DT").Tables(0)
            RH.Heading(Session("FirmName"), tb, "VET OBSERVATION - PREVIOUS RESPONSE REPORT", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim I As Integer = 0

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No")
            RH.AddColumn(TRHead, TRHead_01, 5, 5, "l", "Emp.Code")
            RH.AddColumn(TRHead, TRHead_02, 15, 15, "l", "Name")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Level")
            RH.AddColumn(TRHead, TRHead_04, 10, 10, "l", "Enter Date")
            RH.AddColumn(TRHead, TRHead_05, 20, 20, "l", "Responses")
            RH.AddColumn(TRHead, TRHead_06, 20, 20, "l", "Remarks")
            RH.AddColumn(TRHead, TRHead_07, 15, 15, "l", "Status")
            tb.Controls.Add(TRHead)
            Dim EnterDate As String
            For Each DR In DT.Rows
                I = I + 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid


                RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString()) 'SlNO
                If (CInt(DR(0)) = 0) Then
                    RH.AddColumn(TR3, TR3_01, 5, 5, "l", "") 'EmpCode
                    RH.AddColumn(TR3, TR3_02, 15, 15, "l", "") 'EmpName
                Else
                    RH.AddColumn(TR3, TR3_01, 5, 5, "l", DR(0))
                    RH.AddColumn(TR3, TR3_02, 15, 15, "l", DR(1))
                End If
                RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(2)) 'Level
                If Not IsDBNull(DR(3)) Then
                    EnterDate = CDate(DR(3)).ToString("dd/MM/yyyy")
                Else
                    EnterDate = ""
                End If
                RH.AddColumn(TR3, TR3_04, 10, 10, "l", EnterDate) 'Date
                If Not IsDBNull(DR(4)) Then
                    RH.AddColumn(TR3, TR3_05, 20, 20, "l", DR(4))
                Else
                    RH.AddColumn(TR3, TR3_05, 20, 20, "l", "")
                End If
                If Not IsDBNull(DR(5)) Then
                    RH.AddColumn(TR3, TR3_06, 20, 20, "l", DR(5))
                Else
                    RH.AddColumn(TR3, TR3_06, 20, 20, "l", "")
                End If
                If Not IsDBNull(DR(6)) Then
                    RH.AddColumn(TR3, TR3_07, 15, 15, "l", DR(6))
                Else
                    RH.AddColumn(TR3, TR3_07, 15, 15, "l", "")
                End If

                tb.Controls.Add(TR3)

            Next

            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub


    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub


End Class
