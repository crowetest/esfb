﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="VetApproveHead.aspx.vb" Inherits="Vet_VetApproveHead" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {
                document.getElementById("colResolution").style.display = "none";
                document.getElementById("rowDocuments").style.display = "none";
                document.getElementById("rowReason").style.display = "none";
                if (document.getElementById("<%= hdnIsHOD.ClientID %>").value == 0) 
                {
                    document.getElementById("btnApprove").disabled = false;
                    document.getElementById("btnLegal").disabled = false;
                    document.getElementById("btnReject").disabled = true;
                }
                else 
                {
                    document.getElementById("btnApprove").disabled = true;
                    document.getElementById("btnLegal").disabled = false;
                    document.getElementById("btnReject").disabled = false;
                }
                ToServer("1ʘ", 1);
            }
            function viewAttachment(Value) 
            {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowFormat.aspx?RequestID=" + RequestID + "&Value=" + Value);
                return false;
            }
            function btnApprove_onclick() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var IsHOD = document.getElementById("<%= hdnIsHOD.ClientID %>").value;
                ToServer("2ʘ" + RequestID + "ʘ" + IsHOD, 2);
            }

            function btnLegal_onclick() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var IsHOD = document.getElementById("<%= hdnIsHOD.ClientID %>").value;
                ToServer("3ʘ" + RequestID + "ʘ" + IsHOD, 3);
            }

            function btnReject_onclick() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var IsHOD = document.getElementById("<%= hdnIsHOD.ClientID %>").value;
                document.getElementById("btnApprove").disabled = true;
                if (document.getElementById("<%= txtReason.ClientID %>").value == "") {
                    alert("Enter Reject Reason");
                    document.getElementById("rowReason").style.display = "";
                    document.getElementById("<%= txtReason.ClientID %>").focus();
                    return false;
                }
                var Conf = confirm("Are you sure?");
                if (Conf == true) 
                    ToServer("4ʘ" + RequestID + "ʘ" + document.getElementById("<%= txtReason.ClientID %>").value + "ʘ" + IsHOD, 4);
            }
            function FromServer(Arg, Context) 
            {
                switch (Context) 
                {
                    case 1:
                    {
                        //SBIÆ  Ratheesh P RÆ  ADFSDFSDF SDSDFSDFSDFÆ  AVAILING OF LOANÆ 0Æ  2Æ  
                        var Data = Arg.split("Æ");
                        document.getElementById("<%= txtParty.ClientID %>").value = Data[0];
                        document.getElementById("<%= txtStatement.ClientID %>").value = Data[1];
                        document.getElementById("<%= txtNature.ClientID %>").value = Data[2];
                        if (Data[3] == 1)
                            document.getElementById("colResolution").style.display = "";
                        else
                            document.getElementById("colResolution").style.display = "none";
                        if (Data[4] > 0) {
                            document.getElementById("<%= hdnDocument.ClientID %>").value = Data[5];
                            document.getElementById("rowDocuments").style.display = "";
                            DataFill();
                        }
                        break;
                    }
                    case 2: // Approve Confirmation
                    {
                        var Data = Arg.split("ʘ");
                        if (Data[0] == 0) {
                            alert(Data[1]);
                            //Return to Old Report
                            if (window.opener != null && !window.opener.closed) {
                                window.opener.location.reload();
                                window.close();
                            }
                            window.onbeforeunload = RefreshParent;
                        }
                        break;
                    }
                    case 3: // To Legal Confirmation
                    {
                        var Data = Arg.split("ʘ");
                        if (Data[0] == 0) {
                            alert(Data[1]);
                            //Return to Old Report
                            if (window.opener != null && !window.opener.closed) {
                                window.opener.location.reload();
                                window.close();
                            }
                            window.onbeforeunload = RefreshParent;
                        }
                        break;
                    }
                    case 4: // Reject Confirmation
                    {
                        var Data = Arg.split("ʘ");
                        if (Data[0] == 0) {
                            alert(Data[1]);
                            //Return to Old Report
                            if (window.opener != null && !window.opener.closed) {
                                window.opener.location.reload();
                                window.close();
                            }
                            window.onbeforeunload = RefreshParent;
                        }
                        break;
                    }
                }
            }
            function DataFill() 
            {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>DOCUMENT TO BE VETTED/EXECUTED FOR THE TRANSACTION</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:auto; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:30%;text-align:left'>DOCUMENT&nbsp;NAME</td>";
                Tab += "<td style='width:30%;text-align:left'>EXECUTANT</td>";
                Tab += "<td style='width:20%;text-align:left'>VET/EXECUTE</td>";
                Tab += "<td style='width:10%;text-align:center'>VIEW</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                var HidArg = document.getElementById("<%= hdnDocument.ClientID %>").value.split("Ř");
                var RowCount = HidArg.length - 1;
                var j;
                var Total = 0;
                var row_bg1 = 0;
                var SlNo = 0;
                for (j = 0; j < RowCount; j++) {
                    if (row_bg1 == 0) {
                        row_bg1 = 1;
                        Tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg1 = 0;
                        Tab += "<tr class='sub_second';>";
                    }
                    var HidArg1 = HidArg[j].split("¶");
                    //LOAN AGREEMENT¶  Ratheesh P R¶  1¶  1
                    SlNo = SlNo + 1;
                    Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                    Tab += "<td style='width:30%;text-align:left'>" + HidArg1[0] + "</td>";
                    Tab += "<td style='width:30%;text-align:left'>" + HidArg1[1] + "</td>";
                    if (HidArg1[2] == 1)
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE AND VET</td>";
                    else 
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE</td>";
                                      
                    if (HidArg1[2] == 0)
                        Tab += "<td style='width:10%;text-align:center'></td>";
                    else
                        Tab += "<td style='width:10%;text-align:center'><img id='imgReport' src='../Image/viewReport.PNG' title='View Attachment' Height='20px' Width='20px' onclick= 'ViewDocument(" + HidArg1[3] + ")' style='cursor:pointer;'/></td></tr>";
                }
                if (row_bg1 == 0)
                    Tab += "<tr style='background-color:OldLace'></tr>";
                else
                    Tab += "<tr style='background-color:Wheat'></tr>";
                Tab += "</table></div>";
                document.getElementById("<%= pnlDocDtl.ClientID %>").innerHTML = Tab;
            }
            function ViewDocument(VetID) 
            {                
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowVetFormat.aspx?RequestID=" + RequestID + "&VetID=" + VetID);
                return false;
            }
            function btnExit_onclick() {
                //Return to Old Report
                if (window.opener != null && !window.opener.closed) {
                    window.opener.location.reload();
                    window.close();
                }
                window.onbeforeunload = RefreshParent;
            }
            

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Party with which transaction is plan
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtParty" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Statement Of Facts
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtStatement" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td id="colResolution" style="width: 10%">
                        <asp:ImageButton ID="cmdView" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" />
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Nature of transaction
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtNature" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDocuments">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: center;" colspan="2">
                        <asp:Panel ID="pnlDocDtl" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowReason">                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Reject Reason</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtReason" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="500" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 187px;"
                            type="button" value="SEND TO HIGHER APPROVE" onclick="return btnApprove_onclick()" />
                        <input id="btnLegal" style="font-family: cambria; cursor: pointer; width: 187px;"
                            type="button" value="SEND TO LEGAL" onclick="return btnLegal_onclick()" />
                        <input id="btnReject" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="DROP" onclick="return btnReject_onclick()" />
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocument" runat="server" />
            <asp:HiddenField ID="hdnIsHOD" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
