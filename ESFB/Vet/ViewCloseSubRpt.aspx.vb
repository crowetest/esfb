﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf

Partial Class Vet_ViewCloseSubRpt
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim RequestID, DocID, Value, i As Integer
    Dim GF As New GeneralFunctions
    Dim From As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            DocID = CInt(Request.QueryString.Get("DocID"))

            Dim DT As New DataTable

            tb.Attributes.Add("width", "100%")

            Dim RowBG As Integer = 0
            Dim DR As DataRow

            RH.Heading(CStr(Session("FirmName")), tb, "VIEW CLOSED REQUESTS ATTACHMENTS", 100)

            If DocID = 1 Then
                Value = 0
                DT = DB.ExecuteDataSet("select convert(varchar(10),a.PkId),a.DRFILE_NAME from DMS_ESFB.dbo.VET_DRAFT_ATTACHMENT a where a.REQUEST_ID=" & RequestID & "").Tables(0)
            ElseIf DocID = 2 Then
                Value = 3
                DT = DB.ExecuteDataSet("select convert(varchar(10),a.PkId),a.OPINION_NAME from DMS_ESFB.dbo.VET_OPINION_ATTACHMENT a where a.REQUEST_ID=" & RequestID & "").Tables(0)
            End If

            If DT.Rows.Count > 0 Then
                RH.BlankRow(tb, 17)
                RH.SubHeading(tb, 100, "l", "DRAFTING")
                RH.BlankRow(tb, 5)

                Dim TRVET0 As New TableRow
                TRVET0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRVET0_00, TRVET0_01 As New TableCell

                TRVET0_00.BorderWidth = "1"
                TRVET0_01.BorderWidth = "1"

                TRVET0_00.BorderStyle = BorderStyle.Solid
                TRVET0_01.BorderStyle = BorderStyle.Solid

                TRVET0_00.BorderColor = Drawing.Color.Silver
                TRVET0_01.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRVET0, TRVET0_00, 20, 20, "c", "#")
                RH.AddColumn(TRVET0, TRVET0_01, 80, 80, "l", "Document")

                i = 0
                tb.Controls.Add(TRVET0)
                RH.BlankRow(tb, 1)

                For Each DR In DT.Rows
                    Dim TRVET55 As New TableRow
                    i = i + 1
                    Dim TRVET55_00, TRVET55_01 As New TableCell

                    TRVET55_00.BorderWidth = "1"
                    TRVET55_01.BorderWidth = "1"

                    TRVET55_00.BorderStyle = BorderStyle.Solid
                    TRVET55_01.BorderStyle = BorderStyle.Solid

                    TRVET55_00.BorderColor = Drawing.Color.Silver
                    TRVET55_01.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET55, TRVET55_00, 20, 20, "c", i)
                    RH.AddColumn(TRVET55, TRVET55_01, 80, 80, "l", "<a href='ShowFormat.aspx?RequestID=" + RequestID.ToString + "&Value=" + Value.ToString + "&Id=" + DR(0).ToString + "' style='text-align:right;' target='_blank' >" + DR(1) + "</a>")

                    tb.Controls.Add(TRVET55)
                Next
            End If

            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub


    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub


End Class
