﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    EnableEventValidation="false" CodeFile="NewRequest.aspx.vb" Inherits="Vet_NewRequest" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
        function window_onload()         
        {
            document.getElementById("<%= hdnVetDtl.ClientID %>").value = "";
            document.getElementById("<%= hdnFileName.ClientID %>").value = "";
            document.getElementById("rowDraft").style.display = "none";
            document.getElementById("rowAttDraft").style.display = "none";
            document.getElementById("rowOpinion").style.display = "none";  
            document.getElementById("rowOpinionDoc").style.display = "none";
            document.getElementById("rowDocument").style.display = "none";
            document.getElementById("rowAttComment").style.display = "none";
            document.getElementById("rowPurpose").style.display = "none";
            document.getElementById("rowExpect").style.display = "none";
            document.getElementById("rowVetParty").style.display = "none";
            document.getElementById("rowStatement").style.display = "none";
            document.getElementById("rowRegistration").style.display = "none";            
            document.getElementById("rowDocType").style.display = "none";
            document.getElementById("rowAttArchive").style.display = "none";
            document.getElementById("rowTransacction").style.display = "none";
            document.getElementById("rowDocTrnsaction").style.display = "none";
            document.getElementById("rowDocCopy").style.display = "none";
            document.getElementById("rowExecutant").style.display = "none";
            document.getElementById("rowAdd").style.display = "none";
            document.getElementById("rowPnl").style.display = "none";
            document.getElementById("rowExecuteDt").style.display = "none";
            document.getElementById("rowExecute").style.display = "none";
            document.getElementById("rowReason").style.display = "none";

            document.getElementById("<%= btnHigher.ClientID %>").style.display = "none"; 
            document.getElementById("<%= btnLegal.ClientID %>").style.display = "none";
            document.getElementById("<%= btnSave.ClientID %>").style.display = "";

            if (document.getElementById("<%= cmbService.ClientID %>").value == 1) //Drafting
            {
                document.getElementById("rowDraft").style.display = "";
                document.getElementById("rowAttDraft").style.display = "";
                document.getElementById("rowStatement").style.display = "";
            }
            else if (document.getElementById("<%= cmbService.ClientID %>").value == 2)//Opinion
            {
                document.getElementById("rowOpinion").style.display = "";
                document.getElementById("rowStatement").style.display = "";
                document.getElementById("rowOpinionDoc").style.display = "";
            }
            else if (document.getElementById("<%= cmbService.ClientID %>").value == 3)//Comments 
            {
                document.getElementById("rowDocument").style.display = "";
                document.getElementById("rowAttComment").style.display = "";
                document.getElementById("rowPurpose").style.display = "";
                document.getElementById("rowExpect").style.display = "";
            }
            else if (document.getElementById("<%= cmbService.ClientID %>").value == 4)//Vetting
            {
                document.getElementById("rowVetParty").style.display = "";
                document.getElementById("rowStatement").style.display = "";
                document.getElementById("rowRegistration").style.display = "";
                document.getElementById("rowTransacction").style.display = "";
                document.getElementById("rowDocTrnsaction").style.display = "";
                document.getElementById("rowDocCopy").style.display = "none";
                document.getElementById("rowExecutant").style.display = "";
                document.getElementById("rowAdd").style.display = "";
                document.getElementById("rowPnl").style.display = "";

                if (document.getElementById("<%= hdnIsHOD.ClientID %>").value == 0) 
                {                    
                    if (document.getElementById("<%= hdnIsReportingTo.ClientID %>").value == 1) 
                    {
                        document.getElementById("<%= btnHigher.ClientID %>").style.display = "";
                        document.getElementById("<%= btnLegal.ClientID %>").style.display = "";
                        document.getElementById("<%= btnSave.ClientID %>").style.display = "none";
                    }
                    else{
                        document.getElementById("<%= btnHigher.ClientID %>").style.display = "";
                        document.getElementById("<%= btnLegal.ClientID %>").style.display = "none";
                        document.getElementById("<%= btnSave.ClientID %>").style.display = "none";
                    }
                }
                else
                {
                    document.getElementById("<%= btnHigher.ClientID %>").style.display = "none";
                    document.getElementById("<%= btnLegal.ClientID %>").style.display = "";
                    document.getElementById("<%= btnSave.ClientID %>").style.display = "none";
                }                
            }
            else if (document.getElementById("<%= cmbService.ClientID %>").value == 5)//Archiving
            {
                document.getElementById("rowDocType").style.display = "";
                document.getElementById("rowAttArchive").style.display = "";
                document.getElementById("rowTransacction").style.display = "";
                document.getElementById("rowVetParty").style.display = "";
                document.getElementById("rowExecuteDt").style.display = "";
                document.getElementById("rowExecute").style.display = "";
                document.getElementById("rowReason").style.display = "";
            }
        }
        function ServiceOnChange() {
            document.getElementById("<%= hdnVetDtl.ClientID %>").value = "";
            document.getElementById("rowDraft").style.display = "none";
            document.getElementById("rowAttDraft").style.display = "none";
            document.getElementById("rowOpinion").style.display = "none";
            document.getElementById("rowOpinionDoc").style.display = "none";
            document.getElementById("rowDocument").style.display = "none";
            document.getElementById("rowAttComment").style.display = "none";
            document.getElementById("rowPurpose").style.display = "none";
            document.getElementById("rowExpect").style.display = "none";
            document.getElementById("rowVetParty").style.display = "none";
            document.getElementById("rowStatement").style.display = "none";
            document.getElementById("rowRegistration").style.display = "none";            
            document.getElementById("rowDocType").style.display = "none";
            document.getElementById("rowAttArchive").style.display = "none";
            document.getElementById("rowTransacction").style.display = "none";
            document.getElementById("rowDocTrnsaction").style.display = "none";
            document.getElementById("rowDocCopy").style.display = "none";
            document.getElementById("rowExecutant").style.display = "none";
            document.getElementById("rowAdd").style.display = "none";
            document.getElementById("rowPnl").style.display = "none";
            document.getElementById("rowExecuteDt").style.display = "none";
            document.getElementById("rowExecute").style.display = "none";
            document.getElementById("rowReason").style.display = "none";

            document.getElementById("<%= btnHigher.ClientID %>").style.display = "none";
            document.getElementById("<%= btnLegal.ClientID %>").style.display = "none";
            document.getElementById("<%= btnSave.ClientID %>").style.display = "";

            var ServiceID = document.getElementById("<%= cmbService.ClientID %>").value;
            if (ServiceID == 1) //Drafting
            {
                document.getElementById("rowDraft").style.display = "";
                document.getElementById("rowAttDraft").style.display = "";
                document.getElementById("rowStatement").style.display = "";
            }
            else if (ServiceID == 2)//Opinion
            {
                document.getElementById("rowOpinion").style.display = "";
                document.getElementById("rowStatement").style.display = "";
                document.getElementById("rowOpinionDoc").style.display = "";
            }
            else if (ServiceID == 3)//Comments 
            {
                document.getElementById("rowDocument").style.display = "";
                document.getElementById("rowAttComment").style.display = "";
                document.getElementById("rowPurpose").style.display = "";
                document.getElementById("rowExpect").style.display = "";
            }
            else if (ServiceID == 4)//Vetting
            {
                document.getElementById("rowVetParty").style.display = "";
                document.getElementById("rowStatement").style.display = "";
                document.getElementById("rowRegistration").style.display = "";
                document.getElementById("rowTransacction").style.display = "";
                document.getElementById("rowDocTrnsaction").style.display = "";
                document.getElementById("rowDocCopy").style.display = "none";
                document.getElementById("rowExecutant").style.display = "";
                document.getElementById("rowAdd").style.display = "";
                document.getElementById("rowPnl").style.display = "";

                if (document.getElementById("<%= hdnIsHOD.ClientID %>").value == 0) 
                {
                    if (document.getElementById("<%= hdnIsReportingTo.ClientID %>").value == 1) 
                    {
                        document.getElementById("<%= btnHigher.ClientID %>").style.display = "";
                        document.getElementById("<%= btnLegal.ClientID %>").style.display = "";
                        document.getElementById("<%= btnSave.ClientID %>").style.display = "none";
                    }
                    else{
                        document.getElementById("<%= btnHigher.ClientID %>").style.display = "";
                        document.getElementById("<%= btnLegal.ClientID %>").style.display = "none";
                        document.getElementById("<%= btnSave.ClientID %>").style.display = "none";
                    }
                }
                else
                {
                    document.getElementById("<%= btnHigher.ClientID %>").style.display = "none";
                    document.getElementById("<%= btnLegal.ClientID %>").style.display = "";
                    document.getElementById("<%= btnSave.ClientID %>").style.display = "none";
                }
            }
            else if (ServiceID == 5)//Archiving
            {
                document.getElementById("rowDocType").style.display = "";
                document.getElementById("rowAttArchive").style.display = "";
                document.getElementById("rowTransacction").style.display = "";
                document.getElementById("rowVetParty").style.display = "";
                document.getElementById("rowExecuteDt").style.display = "";
                document.getElementById("rowExecute").style.display = "";
                document.getElementById("rowReason").style.display = "";
            }
        }
        function AddDraftDoc() 
        {
            if (!document.getElementById && !document.createElement)
                return false;
            var fileUploadarea = document.getElementById("fileDraftArea");
            if (!fileUploadarea)
                return false;
            var newLine = document.createElement("br");
            fileUploadarea.appendChild(newLine);
            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("class", "fileUpload");
            
            if (!AddDraftDoc.lastAssignedId)
                AddDraftDoc.lastAssignedId = 100;
            newFile.setAttribute("id", "FileUpload" + AddDraftDoc.lastAssignedId);
            newFile.setAttribute("name", "FileUpload" + AddDraftDoc.lastAssignedId);
            var div = document.createElement("div");
            div.appendChild(newFile);
            div.setAttribute("id", "div" + AddDraftDoc.lastAssignedId);
            fileUploadarea.appendChild(div);
            AddDraftDoc.lastAssignedId++;
        }
        function AddOpinionDoc()
        {
            if (!document.getElementById && !document.createElement)
                return false;
            var fileUploadarea = document.getElementById("fileOpinionArea");
            if (!fileUploadarea)
                return false;
            var newLine = document.createElement("br");
            fileUploadarea.appendChild(newLine);
            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("class", "fileUpload");
            
            if (!AddOpinionDoc.lastAssignedId)
                AddOpinionDoc.lastAssignedId = 100;
            newFile.setAttribute("id", "FileUpload" + AddOpinionDoc.lastAssignedId);
            newFile.setAttribute("name", "FileUpload" + AddOpinionDoc.lastAssignedId);
            var div = document.createElement("div");
            div.appendChild(newFile);
            div.setAttribute("id", "div" + AddOpinionDoc.lastAssignedId);
            fileUploadarea.appendChild(div);
            AddOpinionDoc.lastAssignedId++;
        }
        function AddCommentDoc()
        {
            if (!document.getElementById && !document.createElement)
                return false;
            var fileUploadarea = document.getElementById("fileCommentArea");
            if (!fileUploadarea)
                return false;
            var newLine = document.createElement("br");
            fileUploadarea.appendChild(newLine);
            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("class", "fileUpload");
            
            if (!AddCommentDoc.lastAssignedId)
                AddCommentDoc.lastAssignedId = 100;
            newFile.setAttribute("id", "FileUpload" + AddCommentDoc.lastAssignedId);
            newFile.setAttribute("name", "FileUpload" + AddCommentDoc.lastAssignedId);
            var div = document.createElement("div");
            div.appendChild(newFile);
            div.setAttribute("id", "div" + AddCommentDoc.lastAssignedId);
            fileUploadarea.appendChild(div);
            AddCommentDoc.lastAssignedId++;
        }
        function AddMoreImages()
        {                      
            if(document.getElementById("<%= chkVet.ClientID %>").checked == true)
            {
                document.getElementById("rowDocCopy").style.display = "";                
                if (document.getElementById("<%= hdnVetDtl.ClientID %>").value != "") 
                {
                    document.getElementById("<%= fupTransaction.ClientID %>").style.display = "none";
                    if(AddMoreImages.lastAssignedId)
                    {
                        var id = Math.abs(AddMoreImages.lastAssignedId);
                        id = id - 1;
                        document.getElementById("FileUpload"+id).style.display = "none";
                    }
                    if (!document.getElementById && !document.createElement)
                    return false;
                    var fileUploadarea = document.getElementById("fileUploadarea");
                    if (!fileUploadarea)
                        return false;
                    var newLine = document.createElement("br");
                    fileUploadarea.appendChild(newLine);
                    var newFile = document.createElement("input");
                    newFile.type = "file";
                    newFile.setAttribute("class", "fileUpload");
            
                    if (!AddMoreImages.lastAssignedId)
                        AddMoreImages.lastAssignedId = 100;
                    newFile.setAttribute("id", "FileUpload" + AddMoreImages.lastAssignedId);
                    newFile.setAttribute("name", "FileUpload" + AddMoreImages.lastAssignedId);
                    newFile.setAttribute("style", 'Z-INDEX: 500; LEFT: 594px; POSITION: absolute; TOP: 425px');

                    document.getElementById("<%= hdnFileName.ClientID %>").value = "FileUpload" + AddMoreImages.lastAssignedId;

                    var div = document.createElement("div");
                    div.appendChild(newFile);

                    div.setAttribute("id", "div" + AddMoreImages.lastAssignedId);
                    div.setAttribute("style","width:0px;height:0px;")
                    fileUploadarea.appendChild(div);
                    AddMoreImages.lastAssignedId++;
                }
                else
                {
                    if(document.getElementById("<%= hdnFileName.ClientID %>").value == "")
                        document.getElementById("<%= hdnFileName.ClientID %>").value = "fupTransaction";
                }
            }
            else
            {
                document.getElementById("rowDocCopy").style.display = "none";
                document.getElementById("<%= hdnFileName.ClientID %>").value = "fupTransaction";
            }

        }
        function AddArchiveDoc()
        {
            if (!document.getElementById && !document.createElement)
                return false;
            var fileUploadarea = document.getElementById("fileArchiveArea");
            if (!fileUploadarea)
                return false;
            var newLine = document.createElement("br");
            fileUploadarea.appendChild(newLine);
            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("class", "fileUpload");
            
            if (!AddArchiveDoc.lastAssignedId)
                AddArchiveDoc.lastAssignedId = 100;
            newFile.setAttribute("id", "FileUpload" + AddArchiveDoc.lastAssignedId);
            newFile.setAttribute("name", "FileUpload" + AddArchiveDoc.lastAssignedId);
            var div = document.createElement("div");
            div.appendChild(newFile);
            div.setAttribute("id", "div" + AddArchiveDoc.lastAssignedId);
            fileUploadarea.appendChild(div);
            AddArchiveDoc.lastAssignedId++;
        }
        function ClearAdd()
        {
            document.getElementById("<%= chkVet.ClientID %>").checked = false;
            document.getElementById("rowDocCopy").style.display = "none";            
        }
        function btnAdd_onclick() 
        {   
            var DocID = document.getElementById("<%= cmbDocTransaction.ClientID %>").value;
            var DocName = document.getElementById("<%=cmbDocTransaction.ClientID%>").options[document.getElementById("<%=cmbDocTransaction.ClientID%>").selectedIndex].text;
            var DocCopy;
            if(document.getElementById("<%= chkVet.ClientID %>").checked == true)
            {   
                if(document.getElementById("<%= hdnFileName.ClientID %>").value == "fupTransaction")
                {
                    DocCopy = document.getElementById("<%= fupTransaction.ClientID %>").value;
                    if (DocCopy == "") 
                    {
                        alert("Please Upload Documeent To Be Vet"); 
                        document.getElementById("<%= fupTransaction.ClientID %>").focus(); 
                        return false;
                    }
                }
                else 
                {            
                    DocCopy = document.getElementById(document.getElementById("<%= hdnFileName.ClientID %>").value).value;
                    if (DocCopy == "") 
                    {
                        alert("Please Upload Documeent To Be Vet"); 
                        document.getElementById(document.getElementById("<%= hdnFileName.ClientID %>").value).focus(); 
                        return false;
                    }
                }
            }
            else
                DocCopy = "";         
                       
            var Dept = document.getElementById("<%= cmbExecuteDept.ClientID %>").value;
            var ExecuteBy = document.getElementById("<%= cmbExecuteBy.ClientID %>").value;
            var ExecuteName = document.getElementById("<%=cmbExecuteBy.ClientID%>").options[document.getElementById("<%=cmbExecuteBy.ClientID%>").selectedIndex].text;
            alert(document.getElementById("<%= hdnVetDtl.ClientID %>").value);
            if(document.getElementById("<%= hdnVetDtl.ClientID %>").value == "")
            {
                document.getElementById("<%= hdnVetDtl.ClientID %>").value = DocID + "®" + DocName + "®" + DocCopy + "®" + Dept + "®" + ExecuteBy + "®" + ExecuteName +"¶";
            }   
            else
            {  
                var ExistsItem = document.getElementById("<%= hdnVetDtl.ClientID %>").value.split("¶");
                var TotCount = ExistsItem.length-1;
                var i;
                for(i=0;i<TotCount;i++)
                {                    
                    var Exists=ExistsItem[i].split("®");
                                                                      
                    if(Exists[0] == DocID)
                    {
                        alert("Already Added");   
                        return false;             
                    }                  
                }               
                document.getElementById("<%= hdnVetDtl.ClientID %>").value = document.getElementById("<%= hdnVetDtl.ClientID %>").value + DocID + "®" + DocName + "®" + DocCopy + "®" + Dept + "®" + ExecuteBy + "®" + ExecuteName +"¶";
                  alert("back");
                  alert(document.getElementById("<%= hdnVetDtl.ClientID %>").value);
            }
            DiplayTable();
            ClearAdd();
        }
        function DiplayTable()
        {
            var Tab    = "";
            Tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            Tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            Tab += "<tr >";           
            Tab += "<td style='width:10%;text-align:center' >#</td>";
            Tab += "<td style='width:20%;text-align:left'>DOC NAME</td>";     
            Tab += "<td style='width:15%;text-align:left'>STATUS</td>";
            Tab += "<td style='width:25%;text-align:left'>ATTACHMENT</td>";
            Tab += "<td style='width:25%;text-align:left'>EXECUTANT</td>";  
            Tab += "<td style='width:5%;text-align:left'>DELETE</td>";
            Tab += "</tr>";
            if (document.getElementById("<%= hdnVetDtl.ClientID %>").value != "") 
            {                
                var HdnArg=document.getElementById("<%= hdnVetDtl.ClientID %>").value.split("¶");
                var RowCount = HdnArg.length-1;
                
                if (RowCount == 0)
                    document.getElementById("rowPnl").style.display = "none";
                else
                {
                    document.getElementById("rowPnl").style.display = "";
                    var row_bg1 = 0;
                    var SlNo=0;
                    var j;
                    for(j=0;j<RowCount;j++)
                    {                                          
                        var HdnArgDtl=HdnArg[j].split("®");
                        if (row_bg1 == 0)
                        {
                            row_bg1 = 1;
                            Tab += "<tr class='sub_first';>";
                        }
                        else
                        {
                            row_bg1 = 0;  
                            Tab += "<tr class='sub_second';>";             
                        }         
                        SlNo=j+1;               
                        Tab +="<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                        Tab +="<td style='width:25%;text-align:left'>" + HdnArgDtl[1] + "</td>";
                        if (HdnArgDtl[2] == "")
                            Tab +="<td style='width:15%;text-align:left'>TO BE EXECUTED</td>";
                        else
                            Tab +="<td style='width:15%;text-align:left'>TO BE EXECUTED AND VETTED</td>";
                        Tab +="<td style='width:25%;text-align:left'>" + HdnArgDtl[2] + "</td>";
                        Tab +="<td style='width:25%;text-align:left'>" + HdnArgDtl[5] + "</td>";
                        Tab +="<td style='width:5%;text-align:center' onclick=DeleteRowDocument('" + j + "')><img  src='../Image/remove.png'; style='align:middle;cursor:pointer; height:15px; width:15px;' title='Delete'/></td></tr>";
                    }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";    
                    Tab +="</table></div>";                    
                    document.getElementById("<%= pnlTransactionDtl.ClientID %>").innerHTML= Tab; 
                }                
            } 
            else
                document.getElementById("rowPnl").style.display = "none";
        }
        function DeleteRowDocument(Val)
        {
            row = document.getElementById("<%= hdnVetDtl.ClientID %>").value.split("¶");
            var NewStr = ""
            for (n = 0; n < row.length - 1; n++) {
                if (Val != n)
                    NewStr += row[n] + "¶";
            }
            document.getElementById("<%= hdnVetDtl.ClientID %>").value = NewStr;            
            DiplayTable();
            ClearAdd();
        }
        function DeptOnChange(Value)
        {
            var DeptID;            
            if(Value == 2)
            {
                DeptID = document.getElementById("<%= cmbExecuteDept.ClientID %>").value;
            }
            else if(Value == 3)
            {
                DeptID = document.getElementById("<%= cmbExeDept.ClientID %>").value;
            }
            ToServer("1ʘ" + DeptID + "ʘ" + Value, 1);
        }
        function SaveOnClick()
        {
            var ServiceID = document.getElementById("<%= cmbService.ClientID %>").value;
            if (ServiceID == 2)//Opinion
            {
                if(document.getElementById("<%= txtOpinion.ClientID %>").value == "")
                {
                    alert("Enter Opinion");
                    document.getElementById("<%= txtOpinion.ClientID %>").focus();
                    return false;
                }
            }
            else if (ServiceID == 3)//Comments 
            {
                if(document.getElementById("<%= txtPurpose.ClientID %>").value == "")
                {
                    alert("Enter Purpose");
                    document.getElementById("<%= txtPurpose.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtExpectation.ClientID %>").value == "")
                {
                    alert("Enter Expectation");
                    document.getElementById("<%= txtExpectation.ClientID %>").focus();
                    return false;
                }
            }            
            else if (ServiceID == 4)//Vetting
            {
                if (document.getElementById("<%= hdnVetDtl.ClientID %>").value == "")
                {
                    alert("Please Enter Vet/Execute Details");
                    return false;
                }
            }
            else if (ServiceID == 5)//Archiving
            {                
                if (document.getElementById("<%= fupArchiveDoc.ClientID %>").value == "") {
                    alert("Please Upload Document for Archive");
                    document.getElementById("<%= fupArchiveDoc.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtExecuteDt.ClientID %>").value == "")
                {
                    alert("Execute Date");
                    document.getElementById("<%= txtExecuteDt.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtNotVet.ClientID %>").value == "")
                {
                    alert("Enter Reason");
                    document.getElementById("<%= txtNotVet.ClientID %>").focus();
                    return false;
                }
                document.getElementById("<%= hdnExecutant.ClientID %>").value = document.getElementById("<%= cmbExeBy.ClientID %>").value;                
            }
        }
        function setStartDate(sender, args) 
        {
            sender._endDate = new Date();
        }
        function FromServer(Arg, Context) 
        {
            switch(Context)
            {
                case 1: //  Employee Fill On Change Of Department
                { 
                    var Data = Arg.split("¶");  
                    if(Data[1] == 2)
                    {
                        ComboFill(Data[0], "<%= cmbExecuteBy.ClientID %>");
                    }
                    else if(Data[1] == 3)
                    {
                        ComboFill(Data[0], "<%= cmbExeBy.ClientID %>");
                    }
                    break;
                }
            }
        }

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Services
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbService" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 50%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDraft">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Type of document to be drafted
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbDraftDoc" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowAttDraft">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Attach Relevant Document if any<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <div id="fileDraftArea">
                            <asp:FileUpload ID="fupAttDraft" runat="server" CssClass="fileUpload" /><br />
                        </div>
                        <br />
                        <div>
                            <input style="display: block;" id="btnAddMoreFiles" type="button" value="Add More"
                                onclick="AddDraftDoc();" /><br />
                        </div>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowOpinion">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Opinion related to
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtOpinion" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="500" />
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowOpinionDoc">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Attach Documents<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <div id="fileOpinionArea">
                            <asp:FileUpload ID="fupOpinion" runat="server" CssClass="fileUpload" /><br />
                        </div>
                        <br />
                        <div>
                            <input style="display: block;" id="btnAddDoc" type="button" value="Add More" onclick="AddOpinionDoc();" /><br />
                        </div>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDocument">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Nature of documents
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbNaturDoc" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowAttComment">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Attach Document for Comments<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <div id="fileCommentArea">
                            <asp:FileUpload ID="fupComment" runat="server" CssClass="fileUpload" /><br />
                        </div>
                        <br />
                        <div>
                            <input style="display: block;" id="btnCommentDoc" type="button" value="Add More"
                                onclick="AddCommentDoc();" /><br />
                        </div>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowPurpose">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Purpose for which comments is required
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtPurpose" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="500" />
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowExpect">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Expectation
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtExpectation" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="500" />
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowVetParty">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Name of the other party
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbParty" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowStatement">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Statement of facts
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtStmtFacts" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="500" />
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowRegistration">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Back up document(like board resolution, MOU, Sanction Letter etc.)</br><span style="color: #FF0000">(Maximum size is 4 MB)</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <input id="fupResolution" type="file" runat="server" cssclass="fileUpload" />
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDocType">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;Type of documents to be archived
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbDocType" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowAttArchive">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Attach Document to be archived<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span></td>
                    <td style="width: 50%; text-align: left;">
                        <div id="fileArchiveArea">
                            <asp:FileUpload ID="fupArchiveDoc" runat="server" CssClass="fileUpload" /><br />
                        </div>
                        <br />
                        <div>
                            <input style="display: block;" id="btnArchive" type="button" value="Add More" onclick="AddArchiveDoc();" /><br />
                        </div>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowTransacction">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Nature Of Transaction
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbTransaction" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 50%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDocTrnsaction">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Document For Transaction
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbDocTransaction" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%">
                        <asp:CheckBox ID="chkVet" runat="server" Text="   To be Vet" />
                    </td>
                </tr>
                <tr id="rowDocCopy">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Copy Of Document For Vet<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span></td>
                    <td style="width: 50%; height: 10%; text-align: left;">
                        <div id="fileUploadarea">
                            <input id="fupTransaction" type="file" runat="server" cssclass="fileUpload" />
                            <br />
                        </div>
                        <br />
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowExecutant">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Executant
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbExecuteDept" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="50%" ForeColor="Black">
                        </asp:DropDownList>
                        <asp:DropDownList ID="cmbExecuteBy" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="49%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowAdd">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <input id="btnAdd" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="ADD" onclick="return btnAdd_onclick()" />
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowPnl">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="text-align: center;" colspan="2">
                        <asp:Panel ID="pnlTransactionDtl" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowExecuteDt">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Date Of Execution
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtExecuteDt" runat="server" onkeypress='NumericCheck(event)'></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="txtExecuteDt_CalendarExtender" OnClientShowing="setStartDate"
                            runat="server" TargetControlID="txtExecuteDt" Format="dd/MMM/yyyy">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowExecute">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Person Executed the document
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbExeDept" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="50%" ForeColor="Black">
                        </asp:DropDownList>
                        <asp:DropDownList ID="cmbExeBy" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="49%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowReason">
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Reason for not taking the same through vetting route
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtNotVet" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="500" />
                    </td>
                    <td style="width: 15%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <asp:Button ID="btnHigher" runat="server" Text="SEND TO HIGHER APPROVE" Width="187px"
                            Font-Names="Cambria" />
                        &nbsp;<asp:Button ID="btnLegal" runat="server" Text="SEND TO LEGAL" Width="187px"
                            Font-Names="Cambria" />
                        &nbsp;<asp:Button ID="btnSave" runat="server" Text="SAVE" Width="67px" Font-Names="Cambria" />
                        &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnVetDtl" runat="server" />
            <asp:HiddenField ID="hdnExecutant" runat="server" />
            <asp:HiddenField ID="hdnFileName" runat="server" />
            <asp:HiddenField ID="hdnIsHOD" runat="server" />
            <asp:HiddenField ID="hdnIsReportingTo" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
