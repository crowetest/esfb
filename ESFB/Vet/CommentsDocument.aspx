﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="CommentsDocument.aspx.vb" Inherits="Vet_CommentsDocument" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {
                document.getElementById("colDoc").style.display = "none";
                document.getElementById("rowNewComment").style.display = "none";
                document.getElementById("rowUpload").style.display = "none";
                ToServer("1ʘ", 1);
            }
            function btnSave_onclick() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var CommentDtl = "";
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value == "") {
                    alert("Please Add New Comments");
                    return false;
                }
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    var row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");  //¥-1§-1µµµµµµ
                    for (n = 1; n < row.length - 1; n++) {
                        col = row[n].split("µ");
                        CommentDtl += col[0] + "Œ" + col[1] + "¶";
                    }
                }
                ToServer("2ʘ" + RequestID + "ʘ" + CommentDtl, 2);
            }
            function AddNewComment() {
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                    var Len = row.length - 1;
                    col = row[Len].split("µ");
                    if (col[0] != "-1" && col[1] != "") {
                        document.getElementById("<%= hdnNewDtl.ClientID %>").value += "¥µµ";
                    }
                }
                else
                    document.getElementById("<%= hdnNewDtl.ClientID %>").value = "¥µµ";
                NewCommentFill();
            }
            function NewCommentFill() {
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    document.getElementById("rowNewComment").style.display = "";
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td style='width:5%;text-align:center'>#</td>";
                    tab += "<td style='width:45%;text-align:left'>Reference</td>";
                    tab += "<td style='width:45%;text-align:left'>Comments</td>";
                    tab += "<td style='width:5%;text-align:left'></td>";
                    tab += "</tr>";
                    tab += "</table></div>";
                    tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                    row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                    for (n = 1; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                        if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class='sub_second';>";
                        }
                        // 0 - Comment 1 - Reference
                        tab += "<td style='width:5%;text-align:center' >" + n + "</td>";

                        if (col[1] != "")
                            tab += "<td style='width:45%;text-align:left' >" + col[1] + "</td>"; //Reference
                        else {
                            var txtBox = "<input id='txtReference" + n + "' name='txtReference" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500' onchange='UpdateValueDocument(" + n + ")' />";
                            tab += "<td style='width:45%;text-align:left' >" + txtBox + "</td>";
                        }

                        if (col[0] != "")
                            tab += "<td style='width:45%;text-align:left' >" + col[0] + "</td>"; //Comments
                        else {
                            var txtBox = "<input id='txtComment" + n + "' name='txtComment" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500' onchange='UpdateValueDocument(" + n + ")' onkeydown='CreateNewDocument(event," + n + ")' />";
                            tab += "<td style='width:45%;text-align:left' >" + txtBox + "</td>";
                        }

                        if (col[0] == "")
                            tab += "<td style='width:5%;text-align:left'></td>";
                        else
                            tab += "<td style='width:5%;text-align:center' onclick=DeleteRowDocument('" + n + "')><img  src='../Image/remove.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                        tab += "</tr>";
                    }
                    tab += "</table></div></div></div>";
                    document.getElementById("<%= pnlNewComment.ClientID %>").innerHTML = tab;
                    setTimeout(function () { document.getElementById("txtReference" + (n - 1)).focus().select(); }, 4);
                }
                else
                    document.getElementById("rowNewComment").style.display = "none";
            }
            function CreateNewDocument(e, val) {
                var n = (window.Event) ? e.which : e.keyCode;
                if (n == 13 || n == 9) {
                    UpdateValueDocument(val);
                    AddNewComment();
                    document.getElementById("txtReference" + val).focus();
                }
            }
            function UpdateValueDocument(id) {
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var NewStr = ""
                for (n = 1; n <= row.length - 1; n++) {
                    if (id == n) {
                        var Comment = document.getElementById("txtComment" + id).value;
                        var Reference = document.getElementById("txtReference" + id).value;
                        NewStr += "¥" + Comment + "µ" + Reference;
                    }
                    else {
                        NewStr += "¥" + row[n];
                    }
                }
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
            }
            function DeleteRowDocument(id) {
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var NewStr = ""
                for (n = 1; n <= row.length - 1; n++) {
                    if (id != n)
                        NewStr += "¥" + row[n];
                }
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value == "")
                    document.getElementById("<%= hdnNewDtl.ClientID %>").value = "¥µµ";
                NewCommentFill();
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                        {
                            var Data = Arg.split("Æ");
                            document.getElementById("<%= txtDocument.ClientID %>").value = Data[0];
                            document.getElementById("<%= txtPurpose.ClientID %>").value = Data[1];
                            document.getElementById("<%= txtExpectation.ClientID %>").value = Data[2];
                            if (Data[3] > 0) {
                                document.getElementById("<%= hdnDocDtl.ClientID %>").value = Data[4];
                                DiplayTable();
                            }
                            else
                                document.getElementById("rowCommentDoc").style.display = "none";
                            break;
                        }
                    case 2: // Approve Confirmation
                        {
                            var Data = Arg.split("ʘ");
                            alert(Data[1]);
                            if (Data[0] == 0) {
                                //Return to Old Report
                                if (window.opener != null && !window.opener.closed) {
                                    window.opener.location.reload();
                                    window.close();
                                }
                                window.onbeforeunload = RefreshParent;
                            }
                            break;
                        }
                }
            }
            function DiplayTable() {
                var Tab = "";
                Tab += "<div style='width:80%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:50%;text-align:left'>DOCUMENT&nbsp;NAME</td>";
                Tab += "<td style='width:40%;text-align:center'>REDRAFT DOCUMENT</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:80%; height:60px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                if (document.getElementById("<%= hdnDocDtl.ClientID %>").value != "") {
                    document.getElementById("rowCommentDoc").style.display = "";
                    var HidArg = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("¶");
                    var RowCount = HidArg.length - 1;
                    var j;
                    var Total = 0;
                    var row_bg1 = 0;
                    var SlNo = 0;
                    for (j = 0; j < RowCount; j++) {
                        if (row_bg1 == 0) {
                            row_bg1 = 1;
                            Tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg1 = 0;
                            Tab += "<tr class='sub_second';>";
                        }
                        var HidArg1 = HidArg[j].split("®");
                        //LOAN AGREEMENT¶  Remya Lishoy¶  1¶  1
                        SlNo = SlNo + 1;
                        Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                        Tab += "<td style='width:50%;text-align:left;cursor: pointer;'><a href='ShowFormat.aspx?RequestID=" + HidArg1[1] + "&Value=2&Id=" + HidArg1[0] + "'>" + HidArg1[2] + "</a></td>";
                        Tab += "<td style='width:40%; text-align:center; padding-left:20px; ' onclick=RedraftOnClick(" + HidArg1[0] + ")><img src='../image/redraft.png' title='ReDraft' style='height:20px; width:20px; cursor:pointer;'/></td>";
                        Tab += "</tr>";
                    }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div>";
                    document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;
                }
            }
            function RedraftOnClick(PkID) {
                document.getElementById("rowUpload").style.display = "";
                document.getElementById("<%= hdnPkID.ClientID %>").value = PkID;
                //document.getElementById("<%= lblFile.ClientID %>").innerHTML = FileName.toString().replace("$", " ");
            }
            function UploadOnClick() {
                if (document.getElementById("<%= fupAttDraft.ClientID %>").value == "") {
                    alert("Please Upload Document For Redraft");
                    document.getElementById("<%= fupAttDraft.ClientID %>").focus();
                    return false;
                }
            }
            function btnExit_onclick() {
                //Return to Old Report
                if (window.opener != null && !window.opener.closed) {
                    window.opener.location.reload();
                    window.close();
                }
                window.onbeforeunload = RefreshParent;
            }       

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Document
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtDocument" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td id="colDoc" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowCommentDoc">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: left;" colspan="2">
                        &nbsp;
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 50%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rowUpload">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Redraft Document&nbsp;
                        <asp:Label ID="lblFile" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <input id="fupAttDraft" type="file" runat="server" cssclass="fileUpload" />&nbsp;
                        <asp:ImageButton ID="btnUpload" runat="server" ImageUrl="../image/Save.png" title='Save' style='height:20px; width:20px; cursor:pointer;'/>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 50%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Purpose for which comments is required
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtPurpose" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Expectation
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtExpectation" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: center;" colspan="2">
                        <strong>ADD COMMENTS</strong>
                        <img src="../Image/addIcon.png" style="height: 25px; width: 25px; float: right; z-index: 1;
                            cursor: pointer; padding-right: 10px;" onclick="AddNewComment()" title="Add New" />
                        <div style="text-align: center">
                        </div>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowNewComment">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: center;" colspan="2">
                        <asp:Panel ID="pnlNewComment" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="SAVE" onclick="return btnSave_onclick()" />
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocDtl" runat="server" />
            <asp:HiddenField ID="hdnNewDtl" runat="server" />
            <asp:HiddenField ID="hdnPkID" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
