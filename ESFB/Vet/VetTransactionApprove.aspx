﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="VetTransactionApprove.aspx.vb" Inherits="Vet_VetTransactionApprove" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {
                document.getElementById("colResolution").style.display = "none";
                document.getElementById("rowDocuments").style.display = "none";
                document.getElementById("rowObserve").style.display = "none";
                document.getElementById("rowApp1").style.display = "none";
                document.getElementById("rowApp2").style.display = "none";
                document.getElementById("rowAddDisplay").style.display = "none";
                document.getElementById("rowReason").style.display = "none";

                document.getElementById("<%= hdnObservation.ClientID %>").value = "";
                document.getElementById("<%= hdnObserveDtl.ClientID %>").value = "";
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = "";

                if (document.getElementById("<%= hdnHigher.ClientID %>").value == 1) {
                    document.getElementById("btnSignOff").disabled = false;
                    document.getElementById("btnApprove").disabled = true;
                }
                else {
                    document.getElementById("btnSignOff").disabled = false;
                    document.getElementById("btnApprove").disabled = false;
                }
                if (document.getElementById("<%= hdnStatus.ClientID %>").value == 4)
                    document.getElementById("btnReject").disabled = false;
                else
                    document.getElementById("btnReject").disabled = true;

                ToServer("1^", 1);
            }
            function ViewAttachment(Value) {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowFormat.aspx?RequestID=" + RequestID + "&Value=" + Value);
                return false;
            }
            function DeptOnChange() {
                var DeptID;
                DeptID = document.getElementById("<%= cmbApproveDept.ClientID %>").value;
                ToServer("2^" + DeptID, 2);
            }
            function btnApprove_onclick() {
                //Approve
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var ObserveDtl = "";
                var AppStatus = document.getElementById("<%= hdnStatus.ClientID %>").value;
                document.getElementById("btnApprove").disabled = true;

                if (AppStatus == 4) {
                    ObserveDtl = document.getElementById("<%= hdnObservation.ClientID %>").value;
                    var AppDept = document.getElementById("<%= cmbApproveDept.ClientID %>").value;
                    var AppBy = document.getElementById("<%= cmbApproveBy.ClientID %>").value;
                    var Status = 1;

                    var NewObsDtl = "";
                    if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                        var row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");  //¥-1§-1µµµµµµ
                        for (n = 1; n < row.length - 1; n++) {
                            col = row[n].split("µ");
                            //           Doc + "µ" + DocID + "µ" + VetID + "µ" + DocName + "µ" + Reference + "µ" + Clause + "µ" + Comment;
                            NewObsDtl += col[2] + "ʘ" + col[1] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "¶";
                        }
                    }
                    var ApproverRemark = document.getElementById("<%= txtAppRemarks.ClientID %>").value;
                    ToServer("3^" + RequestID + "^" + AppDept + "^" + AppBy + "^" + ObserveDtl + "^" + Status + "^" + NewObsDtl + "^" + ApproverRemark, 3);
                }
                else {
                    var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                    var Length = row.length - 1;
                    for (n = 0; n < Length; n++) {
                        col = row[n].split("ʘ");
                        //                                                              VetID           DocID          TransID          Remark         SatisfyFlag
                        if (document.getElementById("<%= hdnObserveDtl.ClientID %>").value == "")
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value = col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[8] + "ʘ" + col[9] + "¶";
                        else
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[8] + "ʘ" + col[9] + "¶";
                    }
                    ObserveDtl = document.getElementById("<%= hdnObserveDtl.ClientID %>").value;

                    var NewObsDtl = "";
                    if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                        var row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");  //¥-1§-1µµµµµµ
                        for (n = 1; n < row.length - 1; n++) {
                            col = row[n].split("µ");
                            //           Doc + "µ" + DocID + "µ" + VetID + "µ" + DocName + "µ" + Reference + "µ" + Clause + "µ" + Comment;
                            NewObsDtl += col[2] + "ʘ" + col[1] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "¶";
                        }
                    }

                    var ApproverRemark = document.getElementById("<%= txtAppRemarks.ClientID %>").value;
                    var Status = 1;
                    ToServer("4^" + RequestID + "^" + ObserveDtl + "^" + ApproverRemark + "^" + Status + "^" + NewObsDtl, 4);
                }
            }
            function btnSignOff_onclick() {
                //Siign Off
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var ObserveDtl = "";
                var AppStatus = document.getElementById("<%= hdnStatus.ClientID %>").value;
                document.getElementById("btnApprove").disabled = true;
                if (AppStatus == 4) {
                    var AppDept = document.getElementById("<%= cmbApproveDept.ClientID %>").value;
                    var AppBy = document.getElementById("<%= cmbApproveBy.ClientID %>").value;
                    ObserveDtl = document.getElementById("<%= hdnObservation.ClientID %>").value;
                    var Status = 2;

                    var NewObsDtl = "";
                    if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                        var row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");  //¥-1§-1µµµµµµ
                        for (n = 1; n < row.length - 1; n++) {
                            col = row[n].split("µ");
                            //           Doc + "µ" + DocID + "µ" + VetID + "µ" + DocName + "µ" + Reference + "µ" + Clause + "µ" + Comment;
                            NewObsDtl += col[2] + "ʘ" + col[1] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "¶";
                        }
                    }
                    var ApproverRemark = document.getElementById("<%= txtAppRemarks.ClientID %>").value;
                    ToServer("3^" + RequestID + "^" + AppDept + "^" + AppBy + "^" + ObserveDtl + "^" + Status + "^" + NewObsDtl + "^" + ApproverRemark, 3);
                }
                else {
                    var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                    var Length = row.length - 1;
                    for (n = 0; n < Length; n++) {
                        col = row[n].split("ʘ");
                        if (col[10] > 0) {
                            //                                                              VetID           DocID          TransID          Remark         SatisfyFlag
                            if (document.getElementById("<%= hdnObserveDtl.ClientID %>").value == "")
                                document.getElementById("<%= hdnObserveDtl.ClientID %>").value = col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[8] + "ʘ" + col[9] + "¶";
                            else
                                document.getElementById("<%= hdnObserveDtl.ClientID %>").value += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[8] + "ʘ" + col[9] + "¶";
                        }
                    }
                    ObserveDtl = document.getElementById("<%= hdnObserveDtl.ClientID %>").value;

                    var NewObsDtl = "";
                    if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                        var row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");  //¥-1§-1µµµµµµ
                        for (n = 1; n < row.length - 1; n++) {
                            col = row[n].split("µ");
                            //           Doc + "µ" + DocID + "µ" + VetID + "µ" + DocName + "µ" + Reference + "µ" + Clause + "µ" + Comment;
                            NewObsDtl += col[2] + "ʘ" + col[1] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "¶";
                        }
                    }

                    var ApproverRemark = document.getElementById("<%= txtAppRemarks.ClientID %>").value;
                    var Status = 2;
                    ToServer("4^" + RequestID + "^" + ObserveDtl + "^" + ApproverRemark + "^" + Status + "^" + NewObsDtl, 4);
                }
            }
            function btnReject_onclick() { //Reject
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                document.getElementById("btnApprove").disabled = true;
                document.getElementById("btnSignOff").disabled = true;
                if (document.getElementById("<%= txtReason.ClientID %>").value == "") {
                    alert("Enter Reject Reason");
                    document.getElementById("rowReason").style.display = "";
                    document.getElementById("<%= txtReason.ClientID %>").focus();
                    return false;
                }
                ToServer("6^" + RequestID + "^" + document.getElementById("<%= txtReason.ClientID %>").value, 6);
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                        {
                            //SBIÆ  Ratheesh P RÆ  ADFSDFSDF SDSDFSDFSDFÆ  AVAILING OF LOANÆ 0Æ  2Æ  
                            var Data = Arg.split("Æ");
                            document.getElementById("<%= txtParty.ClientID %>").value = Data[0]; //Party
                            document.getElementById("<%= txtStatement.ClientID %>").value = Data[1]; //Statement 
                            document.getElementById("<%= txtNature.ClientID %>").value = Data[2]; // Nature of transaction
                            if (Data[3] == 1) // Concerned Resolution
                                document.getElementById("colResolution").style.display = "";
                            else
                                document.getElementById("colResolution").style.display = "none";

                            document.getElementById("<%= txtTerms.ClientID %>").value = Data[5]; // General Terms

                            if (Data[6] != "") {
                                document.getElementById("rowRemark").style.display = "";
                                document.getElementById("<%= txtRemarks.ClientID %>").value = Data[6];
                            }
                            else
                                document.getElementById("rowRemark").style.display = "none";

                            if (Data[10] != "") {
                                document.getElementById("rowRespRemark").style.display = "";
                                document.getElementById("<%= txtRespRemarks.ClientID %>").value = Data[10];                                
                            }
                            else
                                document.getElementById("rowRespRemark").style.display = "none";

                           
                            if (Data[4] > 0) // Vet Document Details
                            {
                                document.getElementById("<%= hdnDocument.ClientID %>").value = Data[11]; // Vet Documents
                                document.getElementById("rowDocuments").style.display = "";
                                DataFill();
                            }
                            var Status = document.getElementById("<%= hdnStatus.ClientID %>").value;
                            if (Status == 4) {                                
                                var VetCnt = 0;
                                var row = document.getElementById("<%= hdnDocument.ClientID %>").value.split("ʘ");
                                var RowCount = row.length - 1;
                                var j;
                                for (j = 0; j < RowCount; j++) {
                                    var col = row[j].split("¶");
                                    if (col[2] == 1)
                                        VetCnt = VetCnt + 1;
                                }
                                if (VetCnt == 0) {
                                    document.getElementById("btnApprove").disabled = true;
                                }
                                else {
                                    document.getElementById("btnApprove").disabled = false;
                                }
                                document.getElementById("rowApp1").style.display = "";
                                document.getElementById("rowApp2").style.display = "none";
                                document.getElementById("rowAddDisplay").style.display = "none";
                                document.getElementById("<%= cmbApproveDept.ClientID %>").value = Data[7]; // Approve Dept
                                ComboFill(Data[12], "<%= cmbApproveBy.ClientID %>");
                                document.getElementById("<%= cmbApproveBy.ClientID %>").value = Data[8]; //Approve By
                                
                                if (Data[9] > 0) // Comments On Documents
                                {
                                    document.getElementById("<%= hdnObservation.ClientID %>").value = Data[13];
                                    TableFill();
                                }
                                document.getElementById("<%= hdnDocDtl.ClientID %>").value = Data[14];
                            }
                            else {                                
                                document.getElementById("rowApp1").style.display = "none";
                                document.getElementById("rowApp2").style.display = "";
                                document.getElementById("rowAddDisplay").style.display = "none";
                                document.getElementById("<%= txtApproveBy.ClientID %>").value = Data[8]; //Approve By
                                if (Data[9] > 0) // Comments On Documents
                                {
                                    document.getElementById("<%= hdnObservation.ClientID %>").value = Data[12];
                                    var StatsfyCnt = 0;
                                    var TotCnt = 0;
                                    var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                                    for (n = 0; n <= row.length - 2; n++) {
                                        col = row[n].split("ʘ");
                                        TotCnt += 1;
                                        if (col[9] == 1)
                                            StatsfyCnt += 1;
                                    }
                                    if (TotCnt == StatsfyCnt)
                                        document.getElementById("btnApprove").disabled = true;
                                    else {
                                        if (document.getElementById("<%= hdnHigher.ClientID %>").value == 1)
                                            document.getElementById("btnApprove").disabled = true;
                                        else
                                            document.getElementById("btnApprove").disabled = false;
                                    }
                                    RemarkFill();
                                    DataUpdate();
                                }
                                document.getElementById("<%= hdnDocDtl.ClientID %>").value = Data[13];
                            }
                            break;
                        }
                    case 2: //  Employee Fill On Change Of Department
                        {
                            ComboFill(Arg, "<%= cmbApproveBy.ClientID %>");
                            break;
                        }
                    case 3: // Approve / Sign Off Comments
                        {
                            var Data = Arg.split("ʘ");
                            alert(Data[1]);
                            if (Data[0] == 0) {
                                //Return to Old Report
                                if (window.opener != null && !window.opener.closed) {
                                    window.opener.location.reload();
                                    if (Data[2] > 0) {
                                        if (Data[3] == 2)
                                            window.open("ViewVetReport.aspx?RequestID=" + Data[2] + "", "_self");
                                        else
                                            window.close();
                                    }
                                }
                            }
                            break;
                        }
                    case 4: // Approve / Sign Off Remarks
                        {
                            var Data = Arg.split("ʘ");
                            alert(Data[1]);
                            if (Data[0] == 0) {
                                //Return to Old Report
                                if (window.opener != null && !window.opener.closed) {
                                    window.opener.location.reload();
                                    if (Data[2] > 0) {
                                        if (Data[3] == 2)
                                            window.open("ViewVetReport.aspx?RequestID=" + Data[2] + "", "_self");
                                        else
                                            window.close();
                                    }
                                }
                            }
                            break;
                        }
                    case 5: // Delete Confirmation
                        {
                            var Data = Arg.split("ʘ");
                            if (Data[0] == 0) {
                                TableFill();
                            }
                            break;
                        }
                    case 6: // Reject
                        {
                            var Data = Arg.split("ʘ");
                            alert(Data[1]);
                            if (Data[0] == 0) {
                                //Return to Old Report
                                if (window.opener != null && !window.opener.closed) {
                                    window.opener.location.reload();
                                    window.close();
                                }
                                window.onbeforeunload = RefreshParent;
                            }
                            break;
                        }
                }
            }
            function DataFill() {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>DOCUMENT TO BE VETTED/EXECUTED FOR THE TRANSACTION</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:20%;text-align:left'>DOCUMENT&nbsp;NAME</td>";
                Tab += "<td style='width:20%;text-align:left'>EXECUTANT</td>";
                Tab += "<td style='width:20%;text-align:left'>VET/EXECUTE</td>";
                Tab += "<td style='width:10%;text-align:center'>VIEW</td>";
                Tab += "<td style='width:20%;text-align:center'>REDRAFT DOCUMENT</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                var HidArg = document.getElementById("<%= hdnDocument.ClientID %>").value.split("ʘ");

                var RowCount = HidArg.length - 1;
                var j;
                var Total = 0;
                var row_bg1 = 0;
                var SlNo = 0;
                for (j = 0; j < RowCount; j++) {
                    if (row_bg1 == 0) {
                        row_bg1 = 1;
                        Tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg1 = 0;
                        Tab += "<tr class='sub_second';>";
                    }
                    var HidArg1 = HidArg[j].split("¶");
                    //LOAN AGREEMENT¶  Ratheesh P R¶  1¶  1
                    SlNo = SlNo + 1;
                    Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                    Tab += "<td style='width:20%;text-align:left'>" + HidArg1[0] + "</td>";
                    Tab += "<td style='width:20%;text-align:left'>" + HidArg1[1] + "</td>";
                    if (HidArg1[2] == 1)
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE AND VET</td>";
                    else
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE</td>";

                    if (HidArg1[2] == 0)
                        Tab += "<td style='width:10%;text-align:center'></td>";
                    else
                        Tab += "<td style='width:10%;text-align:center'><img id='imgReport' src='../Image/viewReport.PNG' title='View Attachment' Height='20px' Width='20px' onclick= 'ViewDocument(" + HidArg1[3] + ")' style='cursor:pointer;'/></td>";

                    if (HidArg1[2] == 0)
                        Tab += "<td style='width:20%;text-align:center'></td></tr>";
                    else
                        Tab += "<td style='width:20%;text-align:center'><a href='VetRedraftUpload.aspx?RequestID=" + HidArg1[5] + "&VetID=" + HidArg1[3] + "&DocID=" + HidArg1[4] + "&PkID=" + HidArg1[6] + "' style='text-align:right; color: red;' target='_blank'>Redraft</a></td></tr>";

                }
                if (row_bg1 == 0)
                    Tab += "<tr style='background-color:OldLace'></tr>";
                else
                    Tab += "<tr style='background-color:Wheat'></tr>";
                Tab += "</table></div>";
                document.getElementById("<%= pnlDocDtl.ClientID %>").innerHTML = Tab;
            }            
            function TableFill() {
                if (document.getElementById("<%= hdnObservation.ClientID %>").value == "") {
                    document.getElementById("rowObserve").style.display = "none";
                }
                else {
                    document.getElementById("rowObserve").style.display = "";
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div style='width:100%; height:auto; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    tab += "<tr >";
                    tab += "<td style='width:5%;text-align:center' >#</td>";
                    tab += "<td style='width:20%;text-align:left'>Document For Transaction</td>";
                    tab += "<td style='width:23%;text-align:left'>Reference</td>";
                    tab += "<td style='width:23%;text-align:left'>Clause</td>";
                    tab += "<td style='width:24%;text-align:left'>Comments</td>";
                    tab += "<td style='width:5%;text-align:left'>Delete</td>";
                    tab += "</tr>";
                    tab += "</table></div>";
                    tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    if (document.getElementById("<%= hdnObservation.ClientID %>").value != "") {

                        row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");

                        for (n = 0; n <= row.length - 2; n++) {
                            col = row[n].split("ʘ");
                            if (row_bg == 0) {
                                row_bg = 1;
                                tab += "<tr class='sub_first';>";
                            }
                            else {
                                row_bg = 0;
                                tab += "<tr class='sub_second';>";
                            }
                            i = n + 1;

                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSelection" + col[2] + "' onclick='ChkPotentialOnClick(" + col[2] + ")' /></td>";
                            tab += "<td style='width:20%;text-align:left'>" + col[3] + "</td>";

                            var txtBox = "<textarea id='txtReference" + col[2] + "' name='txtReference" + col[2] + "' disabled=true style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onblur='UpdateString(" + col[2] + ",1)'>" + col[4] + "</textarea>";
                            tab += "<td style='width:23%;text-align:left'>" + txtBox + "</td>";

                            var txtBox1 = "<textarea id='txtClause" + col[2] + "' name='txtClause" + col[2] + "' disabled=true style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onblur='UpdateString(" + col[2] + ",2)'>" + col[5] + "</textarea>";
                            tab += "<td style='width:23%;text-align:left'>" + txtBox1 + "</td>";

                            var txtBox2 = "<textarea id='txtComment" + col[2] + "' name='txtComment" + col[2] + "' disabled=true style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onblur='UpdateString(" + col[2] + ",3)'>" + col[6] + "</textarea>";
                            tab += "<td style='width:24%;text-align:left'>" + txtBox2 + "</td>";

                            tab += "<td style='width:5%;text-align:center'><img src='../image/delete.png' title='Delete' style='height:25px; width:25px; cursor:pointer;' onclick='DeleteOnClick(" + col[0] + "," + col[2] + ")' /></td>";

                            tab += "</tr>";
                        }
                    }
                    tab += "</table></div>";
                    document.getElementById("<%= pnlObserve.ClientID %>").innerHTML = tab;
                }
                //--------------------- Clearing Data ------------------------//
            }
            function ChkPotentialOnClick(val) {
                if (document.getElementById("chkSelection" + val).checked == true) {
                    document.getElementById("txtReference" + val).disabled = false;
                    document.getElementById("txtClause" + val).disabled = false;
                    document.getElementById("txtComment" + val).disabled = false;
                }
                else {
                    document.getElementById("txtReference" + val).disabled = true;
                    document.getElementById("txtClause" + val).disabled = true;
                    document.getElementById("txtComment" + val).disabled = true;
                }
                UpdateString(val, 1);
                UpdateString(val, 2);
                UpdateString(val, 3);
            }
            function UpdateString(Val, Index) {
                var i = 0;
                var Newstr = "";
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("ʘ");
                    if (col[2] == Val) {
                        if (Index == 1)
                            Newstr += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[3] + "ʘ" + document.getElementById("txtReference" + col[2]).value + "ʘ" + col[5] + "ʘ" + col[6] + "¶";
                        if (Index == 2)
                            Newstr += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[3] + "ʘ" + col[4] + "ʘ" + document.getElementById("txtClause" + col[2]).value + "ʘ" + col[6] + "¶";
                        if (Index == 3)
                            Newstr += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[3] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + document.getElementById("txtComment" + col[2]).value + "¶";
                    }
                    else
                        Newstr += row[n] + "¶";
                }
                document.getElementById("<%= hdnObservation.ClientID %>").value = Newstr;
            }
            function DeleteOnClick(VetID, TransID) {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                var Newstr = "";
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("ʘ");
                    if (col[2] != TransID) {
                        Newstr += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[3] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "¶";
                    }
                }
                document.getElementById("<%= hdnObservation.ClientID %>").value = Newstr;
                var Conf = confirm("Are you sure?");
                if (Conf == true)
                    ToServer("5^" + RequestID + "^" + VetID + "^" + TransID, 5);
            }
            function AddNewComment() {
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                    var Len = row.length - 1;
                    col = row[Len].split("µ");
                    if (col[0] != "-1" && col[1] != "") {
                        document.getElementById("<%= hdnNewDtl.ClientID %>").value += "¥-1§-1µµµµµµ";
                    }
                }
                else
                    document.getElementById("<%= hdnNewDtl.ClientID %>").value = "¥-1§-1µµµµµµ";
                NewCommentFill();
            }
            function NewCommentFill() {
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    document.getElementById("rowAddDisplay").style.display = "";
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td style='width:5%;text-align:center' >#</td>";
                    tab += "<td style='width:20%;text-align:left' >Document For Transaction</td>";
                    tab += "<td style='width:23%;text-align:left'>Reference</td>";
                    tab += "<td style='width:23%;text-align:left'>Clause</td>";
                    tab += "<td style='width:24%;text-align:left'>Comments</td>";
                    tab += "<td style='width:5%;text-align:left'></td>";
                    tab += "</tr>";
                    tab += "</table></div>";
                    tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                    row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                    for (n = 1; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                        if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class='sub_second';>";
                        }
                        // 0 - id, 1 - Docid, 2 - Vetid, 3- Doc Name, 4- Reference, 5-Clause, 6- Comment
                        tab += "<td style='width:5%;text-align:center' >" + n + "</td>";

                        if (col[0] == "-1§-1") {
                            var select1 = CreateSelectDocument(n, 1, col[0]);
                            tab += "<td style='width:20%;text-align:left'>" + select1 + "</td>";
                        }
                        else
                            tab += "<td style='width:20%;text-align:left'>" + col[3] + "</td>"; //DocName

                        if (col[2] != "")
                            tab += "<td style='width:23%;text-align:left' >" + col[4] + "</td>"; //Reference
                        else {
                            var txtBox = "<input id='txtReference" + n + "' name='txtReference" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500' onchange='updateValueDocument(" + n + ")' />";
                            tab += "<td style='width:23%;text-align:left' >" + txtBox + "</td>";
                        }

                        if (col[3] != "")
                            tab += "<td style='width:23%;text-align:left' >" + col[5] + "</td>"; //Clause
                        else {
                            var txtBox = "<input id='txtClause" + n + "' name='txtClause" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500' onchange='updateValueDocument(" + n + ")' />";
                            tab += "<td style='width:23%;text-align:left' >" + txtBox + "</td>";
                        }

                        if (col[4] != "")
                            tab += "<td style='width:24%;text-align:left' >" + col[6] + "</td>"; //Comment
                        else {
                            var txtBox = "<input id='txtComment" + n + "' name='txtComment" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500' onchange='updateValueDocument(" + n + ")'  onkeydown='CreateNewDocument(event," + n + ")' />";
                            tab += "<td style='width:25%;text-align:left' >" + txtBox + "</td>";
                        }

                        if (col[0] == "-1§-1")
                            tab += "<td style='width:5%;text-align:left'></td>";
                        else
                            tab += "<td style='width:5%;text-align:center' onclick=DeleteRowDocument('" + n + "')><img  src='../Image/remove.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                        tab += "</tr>";
                    }
                    tab += "</table></div></div></div>";
                    document.getElementById("<%= pnlNewComment.ClientID %>").innerHTML = tab;

                    setTimeout(function () { document.getElementById("cmbDocument" + (n - 1)).focus().select(); }, 4);
                }
                else
                    document.getElementById("rowAddDisplay").style.display = "none";
            }
            function CreateSelectDocument(n, id, value) {
                if (id == 1) {
                    if (value == '-1§-1')
                        var select1 = "<select id='cmbDocument" + n + "' class='NormalText' name='cmbDocument" + n + "' style='width:100%' onchange='updateValueDocument(" + n + ")'  >";
                    else
                        var select1 = "<select id='cmbDocument" + n + "' class='NormalText' name='cmbDocument" + n + "' style='width:100%' disabled=true onchange='updateValueDocument(" + n + ")' >";

                    var rows = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("Ř"); //Document Details
                    for (a = 1; a < rows.length; a++) {
                        var cols = rows[a].split("Ĉ");
                        if (cols[0] == value)
                            select1 += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                        else
                            select1 += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";

                    }
                }
                select1 += "</select>"
                return select1;
            }
            function CreateNewDocument(e, val) {
                var n = (window.Event) ? e.which : e.keyCode;
                if (n == 13 || n == 9) {
                    updateValueDocument(val);
                    AddNewComment();
                }
            }
            function updateValueDocument(id) {
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var NewStr = ""
                for (n = 1; n <= row.length - 1; n++) {
                    if (id == n) {
                        var DocName = document.getElementById("cmbDocument" + id).options[document.getElementById("cmbDocument" + id).selectedIndex].text;
                        var Doc = document.getElementById("cmbDocument" + id).value
                        var Document = document.getElementById("cmbDocument" + id).value.split("§");
                        var DocID = Document[0];
                        var VetID = Document[1];
                        if (DocID == -1) {
                            alert("Select Document For Transaction");
                            document.getElementById("cmbDocument" + id).focus();
                            return false;
                        }
                        var Reference = document.getElementById("txtReference" + id).value;
                        var Clause = document.getElementById("txtClause" + id).value;
                        var Comment = document.getElementById("txtComment" + id).value;
                        NewStr += "¥" + Doc + "µ" + DocID + "µ" + VetID + "µ" + DocName + "µ" + Reference + "µ" + Clause + "µ" + Comment;
                    }
                    else {
                        NewStr += "¥" + row[n];
                    }
                }
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
            }
            function DeleteRowDocument(id) {
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var NewStr = ""
                for (n = 1; n <= row.length - 1; n++) {
                    if (id != n)
                        NewStr += "¥" + row[n];
                }
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value == "")
                    document.getElementById("<%= hdnNewDtl.ClientID %>").value = "¥-1§-1µµµµµµ";
                NewCommentFill();
            }
            //---------------------------------
            function RemarkFill() {
                if (document.getElementById("<%= hdnObservation.ClientID %>").value == "") {
                    document.getElementById("rowObserve").style.display = "none";
                }
                else {
                    document.getElementById("rowObserve").style.display = "";
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div style='width:100%; height:auto; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    tab += "<tr >";
                    tab += "<td style='width:5%;text-align:center' >#</td>";
                    tab += "<td style='width:10%;text-align:left'>Document For Transaction</td>";
                    tab += "<td style='width:10%;text-align:left'>Reference</td>";
                    tab += "<td style='width:10%;text-align:left'>Clause</td>";
                    tab += "<td style='width:15%;text-align:left'>Comments</td>";
                    tab += "<td style='width:15%;text-align:left'>Response</td>";
                    tab += "<td style='width:5%;text-align:left'>Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:15%;text-align:left'>Last Remark</td>";
                    tab += "<td style='width:15%;text-align:left'>New Remark</td>";
                    tab += "</tr>";
                    tab += "</table></div>";
                    tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    if (document.getElementById("<%= hdnObservation.ClientID %>").value != "") {

                        row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");

                        for (n = 0; n <= row.length - 2; n++) {
                            col = row[n].split("ʘ");
                            if (row_bg == 0) {
                                row_bg = 1;
                                tab += "<tr class='sub_first';>";
                            }
                            else {
                                row_bg = 0;
                                tab += "<tr class='sub_second';>";
                            }
                            i = n + 1;

                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSelection" + col[2] + "' onclick='ChkRemarkOnClick(" + col[2] + ")' /></td>";
                            tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>";
                            tab += "<td style='width:10%;text-align:left'>" + col[4] + "</td>";
                            tab += "<td style='width:10%;text-align:left'>" + col[5] + "</td>";
                            tab += "<td style='width:15%;text-align:left'>" + col[6] + "</td>";
                            if (col[7] == "")
                                tab += "<td style='width:15%;text-align:left'>" + col[7] + "</td>";
                            else
                                tab += "<td style='width:15%;text-align:left;cursor:pointer;'>" + col[7] + "<br/><a href='ViewPrevResponseRpt.aspx?TransID=" + col[2] + "' style='text-align:right;' target='_blank' >Previous</a></td>";

                            var select = "<select id='cmbStatus" + col[2] + "' name='cmbStatus" + col[2] + "' onchange='CheckStatus(" + col[2] + ")' disabled=true>";
                            select += "<option value=0>Not Satisfied</option>";
                            select += "<option value=1>Satisfied</option>";
                            tab += "<td style='width:5%;text-align:center'>" + select + "</td>";

                            tab += "<td style='width:15%;text-align:left'>" + col[8] + "</td>";

                            var txtBox = "<textarea id='txtRemark" + col[2] + "' name='txtRemark" + col[2] + "' disabled=true style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onblur='UpdateRemark(" + col[2] + ",1)'></textarea>";
                            tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>";

                            tab += "</tr>";
                        }
                    }
                    tab += "</table></div>";
                    document.getElementById("<%= pnlObserve.ClientID %>").innerHTML = tab;
                }
                //--------------------- Clearing Data ------------------------//
            }
            function DataUpdate() {
                if (document.getElementById("<%= hdnObservation.ClientID %>").value != "") {

                    row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");

                    for (n = 0; n <= row.length - 2; n++) {
                        col = row[n].split("ʘ");
                        document.getElementById("cmbStatus" + col[2]).value = col[9];
                    }
                }
            }
            function ChkRemarkOnClick(val) {
                if (document.getElementById("chkSelection" + val).checked == true) {
                    document.getElementById("txtRemark" + val).disabled = false;
                    document.getElementById("cmbStatus" + val).disabled = false;
                    document.getElementById("txtRemark" + val).focus();
                }
                else {
                    document.getElementById("txtRemark" + val).disabled = true;
                    document.getElementById("cmbStatus" + val).disabled = true;
                }
                UpdateRemark(val, 1)
            }
            function UpdateRemark(Val, Index) {
                var i = 0;
                var Newstr = "";
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("ʘ");
                    if (col[2] == Val) {
                        if (Index == 1)
                            Newstr += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[3] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "ʘ" + col[7] + "ʘ" + document.getElementById("txtRemark" + col[2]).value + "ʘ" + document.getElementById("cmbStatus" + col[2]).value + "ʘ1¶";
                    }
                    else
                        Newstr += row[n] + "¶";
                }
                document.getElementById("<%= hdnObservation.ClientID %>").value = Newstr;
            }
            function CheckStatus(Val) {
                var i = 0;
                var Newstr = "";
                var TotCnt = 0;
                var CheckCnt = 0;
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("ʘ");
                    TotCnt += 1;
                    if (col[2] == Val) {
                        Newstr += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[3] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "ʘ" + col[7] + "ʘ" + document.getElementById("txtRemark" + col[2]).value + "ʘ" + document.getElementById("cmbStatus" + col[2]).value + "ʘ1¶";
                        if (document.getElementById("cmbStatus" + col[2]).value == 1)
                            CheckCnt += 1;
                    }
                    else {
                        Newstr += row[n] + "¶";
                        if (col[9] == 1)
                            CheckCnt += 1;
                    }
                }
                document.getElementById("<%= hdnObservation.ClientID %>").value = Newstr;
                if (TotCnt == CheckCnt)
                    document.getElementById("btnApprove").disabled = true;
                else
                    document.getElementById("btnApprove").disabled = false;
            }
            function ViewDocument(VetID) {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowVetFormat.aspx?RequestID=" + RequestID + "&VetID=" + VetID);
                return false;
            }
            function GetVetReport() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                window.open("ViewVetReportDraft.aspx?RequestID=" + RequestID);
            }
            function btnExit_onclick() {
                //Return to Old Report
                if (window.opener != null && !window.opener.closed) {
                    window.opener.location.reload();
                    window.close();
                }
                window.onbeforeunload = RefreshParent;
            }            

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 100%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 97%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 100%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Party with which transaction is plan
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtParty" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%; text-align: center; cursor: pointer;"">
                        &nbsp;<asp:HyperLink ID="hlVetRpt" runat="server" Font-Underline="True" ForeColor="#3333CC">Vet Report Model</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Statement Of Facts
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtStatement" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td id="colResolution" style="width: 10%; cursor: pointer;">
                        &nbsp;<asp:ImageButton ID="cmdView" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Nature of transaction
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtNature" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDocuments">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlDocDtl" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        General Terms
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtTerms" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="500" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowApp1">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Approving Authority
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:DropDownList ID="cmbApproveDept" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="50%" ForeColor="Black">
                        </asp:DropDownList>
                        <asp:DropDownList ID="cmbApproveBy" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="49%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowApp2">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Approving Authority
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtApproveBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowRemark">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Remarks
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtRemarks" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>

                <tr id="rowRespRemark">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Responser
                        Remarks
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtRespRemarks" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 20%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 60%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: right;">
                        &nbsp;
                    </td>
                    <td style="width: 60%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowObserve">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlObserve" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowAdd">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: center;" colspan="2">
                        <strong>ADD NEW COMMENTS</strong>
                        <img src="../Image/addIcon.png" style="height: 25px; width: 25px; float: right; z-index: 1;
                            cursor: pointer; padding-right: 10px;" onclick="AddNewComment()" title="Add New" />
                        <b style="color: red;">
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowAddDisplay">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlNewComment" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Remarks If Any
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtAppRemarks" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowReason">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Reject Reason
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtReason" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 68px;"
                            type="button" value="APPROVE" onclick="return btnApprove_onclick()" />
                        <input id="btnSignOff" style="font-family: cambria; cursor: pointer; width: 68px;"
                            type="button" value="SIGN OFF" onclick="return btnSignOff_onclick()" />
                        <input id="btnReject" style="font-family: cambria; cursor: pointer; width: 68px;"
                            type="button" value="REJECT" onclick="return btnReject_onclick()" />
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 68px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnStatus" runat="server" />
            <asp:HiddenField ID="hdnHigher" runat="server" />
            <asp:HiddenField ID="hdnDocument" runat="server" />
            <asp:HiddenField ID="hdnObservation" runat="server" />
            <asp:HiddenField ID="hdnObserveDtl" runat="server" />
            <asp:HiddenField ID="hdnNewDtl" runat="server" />
            <asp:HiddenField ID="hdnDocDtl" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
