﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Vet_DocumentStageWiseRpt3
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim TB As New Table
    Dim ServiceID, RequestID As Integer
    Dim HeadName As String
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")

            ServiceID = CInt(Request.QueryString.Get("ServiceID"))
            HeadName = CStr(Request.QueryString.Get("DocumentToDraft"))
            RequestID = CInt(Request.QueryString.Get("RequestID"))

            Dim DT_VET As New DataTable

            TB.Attributes.Add("width", "100%")

            Dim RowBG As Integer = 0

            RH.Heading(CStr(Session("FirmName")), TB, "STATEMENT OF FACTS -" + HeadName + "", 100)

            RH.BlankRow(TB, 5)
            DT_VET = DB.ExecuteDataSet("SELECT upper(v.STATEMENT_FACT) FROM VET_MASTER v WHERE v.SERVICE_ID=" & ServiceID & " and v.REQUEST_ID= " & RequestID & "").Tables(0)

            If DT_VET.Rows.Count > 0 Then
                Dim TRVET0 As New TableRow
                TRVET0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRVET0_00 As New TableCell

                TRVET0_00.BorderWidth = "1"
                TRVET0_00.BorderStyle = BorderStyle.Solid
                TRVET0_00.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRVET0, TRVET0_00, 100, 100, "l", DT_VET.Rows(0)(0))

                TB.Controls.Add(TRVET0)
            End If

            pnDisplay.Controls.Add(TB)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
