﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="DraftModification.aspx.vb" Inherits="DraftModification" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

            function window_onload() {
                document.getElementById("rowDraftDoc").style.display = "none";
                document.getElementById("rowAttach").style.display = "none";
                document.getElementById("<%= hdnValue.ClientID %>").value = 0;
                ToServer("1ʘ", 1);
            }
            function ModifyAttachment() {
                if (document.getElementById("<%= chkModify.ClientID %>").checked == true)
                    document.getElementById("rowAttach").style.display = "";
                else
                    document.getElementById("rowAttach").style.display = "none";
            }
            function AddOpinionDoc() {
                if (!document.getElementById && !document.createElement)
                    return false;
                var fileUploadarea = document.getElementById("fileOpinionArea");
                if (!fileUploadarea)
                    return false;
                var newLine = document.createElement("br");
                fileUploadarea.appendChild(newLine);
                var newFile = document.createElement("input");
                newFile.type = "file";
                newFile.setAttribute("class", "fileUpload");

                if (!AddOpinionDoc.lastAssignedId)
                    AddOpinionDoc.lastAssignedId = 100;
                newFile.setAttribute("id", "FileUpload" + AddOpinionDoc.lastAssignedId);
                newFile.setAttribute("name", "FileUpload" + AddOpinionDoc.lastAssignedId);
                var div = document.createElement("div");
                div.appendChild(newFile);
                div.setAttribute("id", "div" + AddOpinionDoc.lastAssignedId);
                fileUploadarea.appendChild(div);
                AddOpinionDoc.lastAssignedId++;
            }
            function SaveOnClick() {                
                if (document.getElementById("<%= chkModify.ClientID %>").checked == true)
                {
                    if (document.getElementById("<%= fupOpinion.ClientID %>").value == "") {
                        alert("Please Upload Document");
                        document.getElementById("<%= fupOpinion.ClientID %>").focus();
                        return false;
                    }
                }
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                        {
                            var Data = Arg.split("Æ");
                            document.getElementById("<%= cmbDraftDoc.ClientID %>").value = Data[0];
                            document.getElementById("<%= txtStatement.ClientID %>").value = Data[1];
                            if (Data[2] > 0) {
                                document.getElementById("<%= hdnDocDtl.ClientID %>").value = Data[3];
                                document.getElementById("<%= hdnValue.ClientID %>").value = 0;
                                DiplayTable();
                            }
                            else {
                                document.getElementById("rowDraftDoc").style.display = "none";
                                document.getElementById("rowAttach").style.display = "";
                                document.getElementById("<%= hdnValue.ClientID %>").value = 1;
                            }
                            break;
                        }                    
                }
            }
            function DiplayTable() {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>ATTACHMENTS</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:90%;text-align:left'>DOCUMENT&nbsp;NAME</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:60px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                if (document.getElementById("<%= hdnDocDtl.ClientID %>").value != "") {
                    document.getElementById("rowDraftDoc").style.display = "";
                    var HidArg = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("¶");
                    var RowCount = HidArg.length - 1;
                    var j;
                    var Total = 0;
                    var row_bg1 = 0;
                    var SlNo = 0;
                    for (j = 0; j < RowCount; j++) {
                        if (row_bg1 == 0) {
                            row_bg1 = 1;
                            Tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg1 = 0;
                            Tab += "<tr class='sub_second';>";
                        }
                        var HidArg1 = HidArg[j].split("®");
                        //LOAN AGREEMENT¶  Ratheesh P R¶  1¶  1
                        SlNo = SlNo + 1;
                        Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                        Tab += "<td style='width:90%;text-align:left;cursor: pointer;'><a href='ShowFormat.aspx?RequestID=" + HidArg1[1] + "&Value=2&Id=" + HidArg1[0] + "'>" + HidArg1[2] + "</a></td></tr>";
                    }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div>";
                    document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;
                }
            }
            function btnExit_onclick() {
                //Return to Old Report
                if (window.opener != null && !window.opener.closed) {
                    window.opener.location.reload();
                    window.close();
                }
                window.onbeforeunload = RefreshParent;
            }

        

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Draft Related Remarks
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbDraftDoc" runat="server" class="NormalText" 
                            Font-Names="Cambria" ForeColor="Black" Width="100%">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDraftDoc">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        <asp:CheckBox ID="chkModify" runat="server" Font-Underline="True" ForeColor="#0000CC"
                            Text="Modify Attachment" />
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowAttach">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Attach Documents
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <div id="fileOpinionArea">
                            <asp:FileUpload ID="fupOpinion" runat="server" CssClass="fileUpload" />
                        </div>
                        <div>
                            <input style="display: block;" id="btnAddDoc" type="button" value="Add More" onclick="AddOpinionDoc();" /><br />
                        </div>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Statement Of Facts
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtStatement" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td id="colResolution" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <asp:Button ID="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" runat="server" Text="SAVE" />
                        &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocDtl" runat="server" />
            <asp:HiddenField ID="hdnValue" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
