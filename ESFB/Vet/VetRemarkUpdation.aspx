﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="VetRemarkUpdation.aspx.vb" Inherits="Vet_VetRemarkUpdation" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {
                document.getElementById("colResolution").style.display = "none";
                document.getElementById("rowDocuments").style.display = "none";
                document.getElementById("rowObserve").style.display = "none";
                document.getElementById("rowAddDisplay").style.display = "none";   
                document.getElementById("<%= hdnObservation.ClientID %>").value = "";
                document.getElementById("<%= hdnObserveDtl.ClientID %>").value = "";
                ToServer("1^", 1);
            }
            function ViewAttachment(Value) {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowFormat.aspx?RequestID=" + RequestID + "&Value=" + Value);
                return false;
            }
            function AddNewComment() {
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                    var Len = row.length - 1;
                    col = row[Len].split("µ");
                    if (col[0] != "-1" && col[1] != "") {
                        document.getElementById("<%= hdnNewDtl.ClientID %>").value += "¥-1§-1µµµµµµ";
                    }
                }
                else
                    document.getElementById("<%= hdnNewDtl.ClientID %>").value = "¥-1§-1µµµµµµ";
                NewCommentFill();
            }
            function NewCommentFill() {
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    document.getElementById("rowAddDisplay").style.display = "";
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td style='width:5%;text-align:center' >#</td>";
                    tab += "<td style='width:20%;text-align:left' >Document For Transaction</td>";
                    tab += "<td style='width:23%;text-align:left'>Reference</td>";
                    tab += "<td style='width:23%;text-align:left'>Clause</td>";
                    tab += "<td style='width:24%;text-align:left'>Comments</td>";
                    tab += "<td style='width:5%;text-align:left'></td>";
                    tab += "</tr>";
                    tab += "</table></div>";
                    tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                    row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                    for (n = 1; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                        if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class='sub_second';>";
                        }
                        // 0 - id, 1 - Docid, 2 - Vetid, 3- Doc Name, 4- Reference, 5-Clause, 6- Comment
                        tab += "<td style='width:5%;text-align:center' >" + n + "</td>";

                        if (col[0] == "-1§-1") {
                            var select1 = CreateSelectDocument(n, 1, col[0]);
                            tab += "<td style='width:20%;text-align:left'>" + select1 + "</td>";
                        }
                        else
                            tab += "<td style='width:20%;text-align:left'>" + col[3] + "</td>"; //DocName

                        if (col[2] != "")
                            tab += "<td style='width:23%;text-align:left' >" + col[4] + "</td>"; //Reference
                        else {
                            var txtBox = "<input id='txtReference" + n + "' name='txtReference" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500' onchange='updateValueDocument(" + n + ")' />";
                            tab += "<td style='width:23%;text-align:left' >" + txtBox + "</td>";
                        }

                        if (col[3] != "")
                            tab += "<td style='width:23%;text-align:left' >" + col[5] + "</td>"; //Clause
                        else {
                            var txtBox = "<input id='txtClause" + n + "' name='txtClause" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500' onchange='updateValueDocument(" + n + ")' />";
                            tab += "<td style='width:23%;text-align:left' >" + txtBox + "</td>";
                        }

                        if (col[4] != "")
                            tab += "<td style='width:24%;text-align:left' >" + col[6] + "</td>"; //Comment
                        else {
                            var txtBox = "<input id='txtComment" + n + "' name='txtComment" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500' onchange='updateValueDocument(" + n + ")'  onkeydown='CreateNewDocument(event," + n + ")' />";
                            tab += "<td style='width:25%;text-align:left' >" + txtBox + "</td>";
                        }

                        if (col[0] == "-1§-1")
                            tab += "<td style='width:5%;text-align:left'></td>";
                        else
                            tab += "<td style='width:5%;text-align:center' onclick=DeleteRowDocument('" + n + "')><img  src='../Image/remove.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                        tab += "</tr>";
                    }
                    tab += "</table></div></div></div>";
                    document.getElementById("<%= pnlNewComment.ClientID %>").innerHTML = tab;

                    setTimeout(function () { document.getElementById("cmbDocument" + (n - 1)).focus().select(); }, 4);
                }
                else
                    document.getElementById("rowAddDisplay").style.display = "none";
            }
            function CreateSelectDocument(n, id, value) {
                if (id == 1) {
                    if (value == '-1§-1')
                        var select1 = "<select id='cmbDocument" + n + "' class='NormalText' name='cmbDocument" + n + "' style='width:100%' onchange='updateValueDocument(" + n + ")'  >";
                    else
                        var select1 = "<select id='cmbDocument" + n + "' class='NormalText' name='cmbDocument" + n + "' style='width:100%' disabled=true onchange='updateValueDocument(" + n + ")' >";

                    var rows = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("Ř"); //Document Details
                    for (a = 1; a < rows.length; a++) {
                        var cols = rows[a].split("Ĉ");
                        if (cols[0] == value)
                            select1 += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                        else
                            select1 += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";

                    }
                }
                select1 += "</select>"
                return select1;
            }
            function CreateNewDocument(e, val) {
                var n = (window.Event) ? e.which : e.keyCode;
                if (n == 13 || n == 9) {
                    updateValueDocument(val);
                    AddNewComment();
                }
            }
            function updateValueDocument(id) {
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var NewStr = ""
                for (n = 1; n <= row.length - 1; n++) {
                    if (id == n) {
                        var DocName = document.getElementById("cmbDocument" + id).options[document.getElementById("cmbDocument" + id).selectedIndex].text;
                        var Doc = document.getElementById("cmbDocument" + id).value
                        var Document = document.getElementById("cmbDocument" + id).value.split("§");
                        var DocID = Document[0];
                        var VetID = Document[1];
                        if (DocID == -1) {
                            alert("Select Document For Transaction");
                            document.getElementById("cmbDocument" + id).focus();
                            return false;
                        }
                        var Reference = document.getElementById("txtReference" + id).value;
                        var Clause = document.getElementById("txtClause" + id).value;
                        var Comment = document.getElementById("txtComment" + id).value;
                        NewStr += "¥" + Doc + "µ" + DocID + "µ" + VetID + "µ" + DocName + "µ" + Reference + "µ" + Clause + "µ" + Comment;
                    }
                    else {
                        NewStr += "¥" + row[n];
                    }
                }
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
            }
            function DeleteRowDocument(id) {
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var NewStr = ""
                for (n = 1; n <= row.length - 1; n++) {
                    if (id != n)
                        NewStr += "¥" + row[n];
                }
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value == "")
                    document.getElementById("<%= hdnNewDtl.ClientID %>").value = "¥-1§-1µµµµµµ";
                NewCommentFill();
            }
            function btnResponse_onclick() {                
                //Send For Response
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var Status = 2;
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                var Length = row.length - 1;
                var Count = 0;
                var TotCnt = 0;
                for (n = 0; n < Length; n++) {
                    col = row[n].split("ʘ");
                    if (col[11] == 0) {
                        //                                                              VetID           DocID          TransID          Remark         SatisfyFlag
                        if (document.getElementById("<%= hdnObserveDtl.ClientID %>").value == "")
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value = col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[8] + "ʘ" + col[9] + "¶";
                        else
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[8] + "ʘ" + col[9] + "¶";
                        TotCnt += 1;
                    }
                    if (col[10] > 0)
                        Count += 1;
                }
                var ObserveDtl = document.getElementById("<%= hdnObserveDtl.ClientID %>").value;

                var NewObsDtl = "";
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    var row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");  //¥-1§-1µµµµµµ
                    for (n = 1; n < row.length - 1; n++) {
                        col = row[n].split("µ");
                        //           
                        NewObsDtl += col[1] + "®" + col[2] + "®" + col[4] + "®" + col[5] + "®" + col[6] + "¶";
                    }
                }

                document.getElementById("btnResponse").disabled = true;
                if (TotCnt > Count) {
                    var Conf = confirm("Some Remarks are pending. Are you sure?");
                    if (Conf == true)
                        ToServer("2^" + RequestID + "^" + ObserveDtl + "^" + Status + "^" + NewObsDtl, 2);
                }
                else
                    ToServer("2^" + RequestID + "^" + ObserveDtl + "^" + Status + "^" + NewObsDtl, 2);
            }
            function btnApprove_onclick()
            {
                   //Send For Approve
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var Status = 1;
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                var Length = row.length - 1;
                var Count = 0;
                var TotCnt = 0;
                for (n = 0; n < Length; n++) {
                    col = row[n].split("ʘ");
                    if (col[11] == 0) {
                        //                                                              VetID           DocID          TransID           Remark         SatisfyFlag
                        if (document.getElementById("<%= hdnObserveDtl.ClientID %>").value == "")
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value = col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[8] + "ʘ" + col[9] + "¶";
                        else
                            document.getElementById("<%= hdnObserveDtl.ClientID %>").value += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[8] + "ʘ" + col[9] + "¶";
                        TotCnt += 1;
                    }
                    if (col[10] > 0)
                        Count += 1;
                }
                var ObserveDtl = document.getElementById("<%= hdnObserveDtl.ClientID %>").value;
                document.getElementById("btnApprove").disabled = true;

                var NewObsDtl = "";
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    var row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");  //¥-1§-1µµµµµµ
                    for (n = 1; n < row.length - 1; n++) {
                        col = row[n].split("µ");
                        //           
                        NewObsDtl += col[1] + "®" + col[2] + "®" + col[4] + "®" + col[5] + "®" + col[6] + "¶";
                    }
                }

                if (TotCnt > Count) {
                    var Conf = confirm("Some Remark are pending. Are you sure?");
                    if (Conf == true)
                        ToServer("2^" + RequestID + "^" + ObserveDtl + "^" + Status + "^" + NewObsDtl, 2);
                }
                else
                    ToServer("2^" + RequestID + "^" + ObserveDtl + "^" + Status + "^" + NewObsDtl, 2);
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                        {
                            //SBIÆ  Ratheesh P RÆ  ADFSDFSDF SDSDFSDFSDFÆ  AVAILING OF LOANÆ 0Æ  2Æ  
                            var Data = Arg.split("Æ");
                            document.getElementById("<%= txtParty.ClientID %>").value = Data[0]; //Party
                            document.getElementById("<%= txtSignatory.ClientID %>").value = Data[1]; //Signatory
                            document.getElementById("<%= txtStatement.ClientID %>").value = Data[2]; //Statement 
                            document.getElementById("<%= txtNature.ClientID %>").value = Data[3]; // Nature of transaction
                            if (Data[4] == 1) // Concerned Resolution
                                document.getElementById("colResolution").style.display = "";
                            else
                                document.getElementById("colResolution").style.display = "none";
                            if (Data[5] > 0) // Vet Document Details
                            {
                                document.getElementById("<%= hdnDocument.ClientID %>").value = Data[11]; // Vet Documents
                                document.getElementById("rowDocuments").style.display = "";
                                DataFill();
                            }
                            document.getElementById("<%= txtTerms.ClientID %>").value = Data[6]; // General Terms
                            document.getElementById("<%= txtRemarks.ClientID %>").value = Data[7]; // Remarks

                            document.getElementById("<%= txtApproveBy.ClientID %>").value = Data[9]; //Approve By
                            if (Data[10] > 0) // Comments On Documents
                            {
                                document.getElementById("<%= hdnObservation.ClientID %>").value = Data[12];
                                TableFill();
                                DataUpdate();
                            }
                            document.getElementById("<%= hdnDocDtl.ClientID %>").value = Data[13];                        
                            break;
                        }
                    case 2: // Confirmation
                        {
                            var Data = Arg.split("ʘ");
                            alert(Data[1]);
                            if (Data[0] == 0) {
                                //Return to Old Report
                                if (window.opener != null && !window.opener.closed) {
                                    window.opener.location.reload();
                                    window.close();
                                }
                                window.onbeforeunload = RefreshParent;
                            }
                            break;
                        }

                }
            }
            function DataFill() {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>DOCUMENT TO BE VETTED/EXECUTED FOR THE TRANSACTION</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:20%;text-align:left'>DOCUMENT&nbsp;NAME</td>";
                Tab += "<td style='width:20%;text-align:left'>EXECUTANT</td>";
                Tab += "<td style='width:20%;text-align:left'>VET/EXECUTE</td>";
                Tab += "<td style='width:10%;text-align:center'>VIEW</td>";
                Tab += "<td style='width:20%;text-align:center'>REDRAFT DOCUMENT</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                var HidArg = document.getElementById("<%= hdnDocument.ClientID %>").value.split("ʘ");
                var RowCount = HidArg.length - 1;
                var j;
                var Total = 0;
                var row_bg1 = 0;
                var SlNo = 0;
                for (j = 0; j < RowCount; j++) {
                    if (row_bg1 == 0) {
                        row_bg1 = 1;
                        Tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg1 = 0;
                        Tab += "<tr class='sub_second';>";
                    }
                    var HidData = HidArg[j].split("§");
                    var HidArg1 = HidData[0].split("¶");
                    //LOAN AGREEMENT¶  Ratheesh P R¶  1¶  1
                    SlNo = SlNo + 1;
                    Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                    Tab += "<td style='width:20%;text-align:left'>" + HidArg1[0] + "</td>";
                    Tab += "<td style='width:20%;text-align:left'>" + HidArg1[1] + "</td>";
                    if (HidArg1[2] == 1)
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE AND VET</td>";
                    else
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE</td>";

                    if (HidArg1[2] == 0)
                        Tab += "<td style='width:10%;text-align:center'></td>";
                    else
                        Tab += "<td style='width:10%;text-align:center'><img id='imgReport' src='../Image/viewReport.PNG' title='View Attachment' Height='20px' Width='20px' onclick= 'ViewDocument(" + HidArg1[3] + ")' style='cursor:pointer;'/></td>";

                    if (HidArg1[2] == 0)
                        Tab += "<td style='width:20%;text-align:center'></td></tr>";
                    else
                        Tab += "<td style='width:20%;text-align:center'><a href='VetRedraftUpload.aspx?RequestID=" + HidArg1[5] + "&VetID=" + HidArg1[3] + "&DocID=" + HidArg1[4] + "&PkID=" + HidArg1[6] + "' style='text-align:right; color: red;' target='_blank'>Redraft</a></td></tr>";
                }
                if (row_bg1 == 0)
                    Tab += "<tr style='background-color:OldLace'></tr>";
                else
                    Tab += "<tr style='background-color:Wheat'></tr>";
                Tab += "</table></div>";
                document.getElementById("<%= pnlDocDtl.ClientID %>").innerHTML = Tab;
            }
            function TableFill() {
                if (document.getElementById("<%= hdnObservation.ClientID %>").value == "") {
                    document.getElementById("rowObserve").style.display = "none";
                }
                else {
                    document.getElementById("rowObserve").style.display = "";
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div style='width:100%; height:auto; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    tab += "<tr >";
                    tab += "<td style='width:5%;text-align:center' >#</td>";
                    tab += "<td style='width:10%;text-align:left'>Document For Transaction</td>";
                    tab += "<td style='width:10%;text-align:left'>Reference</td>";
                    tab += "<td style='width:10%;text-align:left'>Clause</td>";
                    tab += "<td style='width:15%;text-align:left'>Comments&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:15%;text-align:left'>Response</td>";
                    tab += "<td style='width:5%;text-align:left'>Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:15%;text-align:left'>Last Remark</td>";
                    tab += "<td style='width:15%;text-align:left'>New Remark</td>";                    
                    tab += "</tr>";
                    tab += "</table></div>";
                    tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    if (document.getElementById("<%= hdnObservation.ClientID %>").value != "") {

                        row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");

                        for (n = 0; n <= row.length - 2; n++) {
                            col = row[n].split("ʘ");
                            if (row_bg == 0) {
                                row_bg = 1;
                                tab += "<tr class='sub_first';>";
                            }
                            else {
                                row_bg = 0;
                                tab += "<tr class='sub_second';>";
                            }
                            i = n + 1;

                            if (col[9] == 0)
                                tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSelection" + col[2] + "' onclick='ChkPotentialOnClick(" + col[2] + ")' /></td>";
                            else
                                tab += "<td style='width:5%;text-align:center'></td>";

                            tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>";
                            tab += "<td style='width:10%;text-align:left'>" + col[4] + "</td>";
                            tab += "<td style='width:10%;text-align:left'>" + col[5] + "</td>";
                            tab += "<td style='width:15%;text-align:left'>" + col[6] + "</td>";
                            if (col[7] == "")
                                tab += "<td style='width:15%;text-align:left'>" + col[7] + "</td>";
                            else
                                tab += "<td style='width:15%;text-align:left;cursor:pointer;'>" + col[7] + "<br/><a href='ViewPrevResponseRpt.aspx?TransID=" + col[2] + "' style='text-align:right;' target='_blank' >Previous</a></td>";

                            var select = "<select id='cmbStatus" + col[2] + "' name='cmbStatus" + col[2] + "' onchange='CheckStatus(" + col[2] + ")' disabled=true>";
                            select += "<option value=0>Not Satisfied</option>";
                            select += "<option value=1>Satisfied</option>";
                            tab += "<td style='width:5%;text-align:center'>" + select + "</td>";

                            tab += "<td style='width:15%;text-align:left'>" + col[8] + "</td>";

                            var txtBox = "<textarea id='txtRemark" + col[2] + "' name='txtRemark" + col[2] + "' disabled=true style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)'  onblur='UpdateString(" + col[2] + ",1)'></textarea>";
                            tab += "<td style='width:15%;text-align:left'>" + txtBox + "</td>";                                                     

                            tab += "</tr>";
                        }
                    }
                    tab += "</table></div>";
                    document.getElementById("<%= pnlObserve.ClientID %>").innerHTML = tab;
                }
                //--------------------- Clearing Data ------------------------//
            }
            function DataUpdate() {
                if (document.getElementById("<%= hdnObservation.ClientID %>").value != "") 
                {
                    row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                    for (n = 0; n <= row.length - 2; n++) {
                        col = row[n].split("ʘ");
                        document.getElementById("cmbStatus" + col[2]).value = col[9];
                    }
                }
            }
            function ChkPotentialOnClick(val) 
            {
                if (document.getElementById("chkSelection" + val).checked == true) {
                    document.getElementById("txtRemark" + val).disabled = false;
                    document.getElementById("cmbStatus" + val).disabled = false;
                    document.getElementById("txtRemark" + val).focus();
                }
                else {
                    document.getElementById("txtRemark" + val).disabled = true;
                    document.getElementById("cmbStatus" + val).disabled = true;
                }
                UpdateString(val, 1);
            }
            function UpdateString(Val, Index) {
                var i = 0;
                var Newstr = "";
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("ʘ");
                    if (col[2] == Val) {
                        if (Index == 1)
                            Newstr += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[3] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "ʘ" + col[7] + "ʘ" + document.getElementById("txtRemark" + col[2]).value + "ʘ" + document.getElementById("cmbStatus" + col[2]).value + "ʘ1ʘ" + col[11] + "¶";
                    }
                    else
                        Newstr += row[n] + "¶";
                }
                document.getElementById("<%= hdnObservation.ClientID %>").value = Newstr;
            }
            function CheckStatus(Val) {                
                var i = 0;
                var Newstr = "";
                var TotCnt = 0;
                var CheckCnt = 0;
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("ʘ");
                    TotCnt += 1;
                    if (col[2] == Val) {
                        Newstr += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[3] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "ʘ" + col[7] + "ʘ" + document.getElementById("txtRemark" + col[2]).value + "ʘ" + document.getElementById("cmbStatus" + col[2]).value + "ʘ1ʘ" + col[11] + "¶";
                        if(document.getElementById("cmbStatus" + col[2]).value == 1)
                            CheckCnt += 1;
                    }
                    else
                    {
                        Newstr += row[n] + "¶";
                        if (col[9] == 1)
                            CheckCnt += 1;
                    }                        
                }
                document.getElementById("<%= hdnObservation.ClientID %>").value = Newstr;
                if(TotCnt == CheckCnt)
                    document.getElementById("btnResponse").disabled = true;
                else
                    document.getElementById("btnResponse").disabled = false;
            }
            function ViewDocument(VetID) {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowVetFormat.aspx?RequestID=" + RequestID + "&VetID=" + VetID);
                return false;
            }
            function ViewReDraftDoc(RequestID, VetID, DocID) {
                window.open("ShowVetFormat.aspx?RequestID=" + RequestID + "&VetID=" + VetID);
            }
            function btnExit_onclick() {
                //Return to Old Report
                if (window.opener != null && !window.opener.closed) {
                    window.opener.location.reload();
                    window.close();
                }
                window.onbeforeunload = RefreshParent;
            }           

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 100%; margin: 0px auto;">
        <br />
        <div style="width: 97%; background-color: white; margin: 0px auto; ">
            <br />
            <table align="center" style="width: 100%; margin: 0px auto;">
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Party with which transaction is plan
                    </td>
                    
                    <td style="width: 70%; text-align: left;">
                        <asp:TextBox ID="txtParty" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Signatory
                    </td>
                   
                    <td style="width: 70%; text-align: left;">
                        <asp:TextBox ID="txtSignatory" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Statement Of Facts
                    </td>
                    
                    <td style="width: 70%; text-align: left;">
                        <asp:TextBox ID="txtStatement" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td id="colResolution" style="width: 5%">
                        <asp:ImageButton ID="cmdView" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Nature of transaction
                    </td>
                    
                    <td style="width: 70%; text-align: left;">
                        <asp:TextBox ID="txtNature" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDocuments">
                <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlDocDtl" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        General Terms
                    </td>
                    
                    <td style="width: 70%; text-align: left;">
                        <asp:TextBox ID="txtTerms" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Approving Authority
                    </td>
                   
                    <td style="width: 70%; text-align: left;">
                        <asp:TextBox ID="txtApproveBy" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Remarks
                    </td>
                    
                    <td style="width: 70%; text-align: left;">
                        <asp:TextBox ID="txtRemarks" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        &nbsp;
                    </td>
                   
                    <td style="width: 70%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowObserve">
                <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlObserve" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowAddComments">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: center;" colspan="2">
                        <strong>ADD NEW COMMENTS</strong>
                        <img src="../Image/addIcon.png" style="height: 25px; width: 25px; float: right; z-index: 1;
                            cursor: pointer; padding-right: 10px;" onclick="AddNewComment()" title="Add New" />
                        </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowAddDisplay">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlNewComment" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4">
                        <input id="btnResponse" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="SEND FOR RESPONSE" onclick="return btnResponse_onclick()" />
                        <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="SEND FOR APPPROVE"  onclick="return btnApprove_onclick()" />
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocument" runat="server" />
            <asp:HiddenField ID="hdnObservation" runat="server" />
            <asp:HiddenField ID="hdnObserveDtl" runat="server" />
            <asp:HiddenField ID="hdnDocDtl" runat="server" />
            <asp:HiddenField ID="hdnNewDtl" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
