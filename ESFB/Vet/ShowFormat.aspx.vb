﻿Imports System.Data
Partial Class Leave_ShowVetFormat
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim RequestID As Integer = Convert.ToInt32(Request.QueryString.[Get]("RequestID"))
            Dim Status As Integer = Convert.ToInt32(Request.QueryString.[Get]("Value"))
            Dim Id As Integer = Convert.ToInt32(Request.QueryString.[Get]("Id"))

            If Status = 1 Then
                DT = DB.ExecuteDataSet("select a.CON_RESOLUTION,a.CONTENT_TYPE,'Attachment' from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=" & RequestID & "").Tables(0)
            ElseIf Status = 2 Then
                DT = DB.ExecuteDataSet("select a.DOC_ATTACH,a.CON_TYPE,a.FILE_NAME from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=" & RequestID & " and PkId=" & Id & "").Tables(0)
            ElseIf Status = 0 Then
                DT = DB.ExecuteDataSet("select DRAFT_ATTACH,CONTENT_TYPE,DRFILE_NAME from DMS_ESFB.dbo.VET_DRAFT_ATTACHMENT where REQUEST_ID=" & RequestID & " and PkId=" & Id & "").Tables(0)
            ElseIf Status = 3 Then
                DT = DB.ExecuteDataSet("select OPINION_ATTACH,CONTENT_TYPE,OPINION_NAME from DMS_ESFB.dbo.VET_OPINION_ATTACHMENT where REQUEST_ID=" & RequestID & " and PkId=" & Id & "").Tables(0)
            ElseIf Status = 4 Then
                DT = DB.ExecuteDataSet("select DOC_ATTACHMENT,CONTENT_TYPE,DOCFILE_NAME from DMS_ESFB.dbo.VET_REDRAFT_ATTACHMENT where REQUEST_ID=" & RequestID & " and PkId=" & Id & "").Tables(0)
            End If

            If DT IsNot Nothing Then
                Dim bytes() As Byte = CType(DT.Rows(0)(0), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = DT.Rows(0)(1).ToString()
                Response.AddHeader("content-disposition", "attachment;filename=" + DT.Rows(0)(2).ToString().Replace(" ", ""))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
