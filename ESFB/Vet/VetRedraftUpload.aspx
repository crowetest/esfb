﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="VetRedraftUpload.aspx.vb" Inherits="Vet_VetRedraftUpload" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {
                ToServer("1ʘ", 1);
            }
            function SaveOnClick() {
                if (document.getElementById("<%= fupAttDraft.ClientID %>").value == "") {
                    alert("Please Upload Redrafted Document For Vet");
                    document.getElementById("<%= fupAttDraft.ClientID %>").focus();
                    return false;
                }
            }
                       
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {
                        var Data = Arg;
                        document.getElementById("<%= txtName.ClientID %>").value = Data;                        
                        break;
                    }
                }
            }
            function btnExit_onclick() {
                //Return to Old Report
                window.close();
            }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Document Name
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtName" Width="100%" class="ReadOnlyTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td id="colResolution" style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowReason">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Upload Document
                    </td>
                    <td style="width: 60%; text-align: left;">                        
                        <input id="fupAttDraft" type="file" runat="server" cssclass="fileUpload" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4">
                        <asp:Button ID="btnSave" runat="server" Text="SAVE" Width="67px" 
                            Font-Names="Cambria"/>
                        &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
