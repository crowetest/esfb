﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="DocumentStageWiseRpt4.aspx.vb" Inherits="Vet_DocumentStageWiseRpt3" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<body style="background-color:Black;">
    <script language="javascript" type="text/javascript">
    function PrintPanel() 
    {
        var panel = document.getElementById("<%=pnDisplay.ClientID %>");
        var printWindow = window.open('', '', 'height=400,width=800');
        printWindow.document.write('<html><head><title>ESAF</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(panel.innerHTML);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        setTimeout(function () 
        {
            printWindow.print();
        }, 500);
        return false;
    }
    function exitform() {
        window.close();
    }
    </script>


    <form id="form1" runat="server">
    <div style="width:80%; background-color:white; min-height:750px; margin:0px auto;">
            <div style="text-align:right ;margin:0px auto; width:95%; background-color:white; ">
            <asp:HiddenField ID="hdnReportID" runat="server" />
            <img src="../../Image/close.png" style="width:35px;  Height:35px; text-align:left ;float:left; padding-top:5px;cursor:pointer;" title="Close"  onclick="exitform()"/>
            <asp:ImageButton ID="cmd_Print" style="text-align:right ;" runat="server" Height="40px" Width="40px" ImageAlign="AbsMiddle" ImageUrl="~/Image/print.png" ToolTip="Click to Print"/>
            &nbsp;&nbsp;<asp:ImageButton ID="cmd_Export" style="text-align:right ;" runat="server" Height="30px" Width="30px" ImageAlign="AbsMiddle" ImageUrl="~/Image/Export.png" ToolTip="Click to Export"/>
            </div>
            <br style="background-color:white"/>
            <div style="text-align:center;margin:0px auto; width:95%; background-color:white;">   
                <asp:Panel ID="pnDisplay" runat="server" Font-Names="Cambria,Franklin Gothic Medium" Width="100%">
                </asp:Panel>
            </div>
        </div>
    </form>
    <p>
        &nbsp;</p>
</body>
</html>
</asp:Content>