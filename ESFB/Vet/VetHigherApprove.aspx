﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="VetHigherApprove.aspx.vb" Inherits="Vet_VetHigherApprove" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {
                document.getElementById("colResolution").style.display = "none";
                document.getElementById("rowDocuments").style.display = "none";
                document.getElementById("rowObserve").style.display = "none";
                document.getElementById("<%= hdnObservation.ClientID %>").value = "";
                document.getElementById("<%= hdnObserveDtl.ClientID %>").value = "";
                document.getElementById("<%= hdnDepartment.ClientID %>").value = ""; // Department
                document.getElementById("<%= hdnEmployee.ClientID %>").value = "";
                document.getElementById("btnExecute").disabled = true;
                ToServer("1^", 1);
            }
            function GetEmployee(DeptID, Val) {
                ToServer("2^" + DeptID + "^" + Val, 2);
            }
            function ViewAttachment(Value) {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowFormat.aspx?RequestID=" + RequestID + "&Value=" + Value);
                return false;
            }
            function btnExecute_onclick() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                var Length = row.length - 1;
                for (n = 0; n < Length; n++) {
                    col = row[n].split("ʘ");
                    //                                                                  VetID           DocID          TransID         ApproveFlag
                    if (document.getElementById("<%= hdnObserveDtl.ClientID %>").value == "")
                        document.getElementById("<%= hdnObserveDtl.ClientID %>").value = col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[10] + "¶";
                    else
                        document.getElementById("<%= hdnObserveDtl.ClientID %>").value += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[10] + "¶";
                }
                ObserveDtl = document.getElementById("<%= hdnObserveDtl.ClientID %>").value;
                var Remark = document.getElementById("<%= txtAppRemark.ClientID %>").value;
                var Status = 1;
                var ExecutantDtl = document.getElementById("<%= hdnDocument.ClientID %>").value;

                document.getElementById("btnExecute").disabled = true;
                ToServer("4^" + RequestID + "^" + ObserveDtl + "^" + Remark + "^" + Status + "^" + ExecutantDtl, 4);
            }
            function btnNotExe_onclick() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                var Length = row.length - 1;
                for (n = 0; n < Length; n++) {
                    col = row[n].split("ʘ");
                    //                                                                  VetID           DocID          TransID         ApproveFlag
                    if (document.getElementById("<%= hdnObserveDtl.ClientID %>").value == "")
                        document.getElementById("<%= hdnObserveDtl.ClientID %>").value = col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[10] + "¶";
                    else
                        document.getElementById("<%= hdnObserveDtl.ClientID %>").value += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[10] + "¶";
                }
                ObserveDtl = document.getElementById("<%= hdnObserveDtl.ClientID %>").value;
                var Remark = document.getElementById("<%= txtAppRemark.ClientID %>").value;
                var Status = 2;
                var ExecutantDtl = document.getElementById("<%= hdnDocument.ClientID %>").value;

                document.getElementById("btnNotExe").disabled = true;
                ToServer("4^" + RequestID + "^" + ObserveDtl + "^" + Remark + "^" + Status + "^" + ExecutantDtl, 4);
            }

            function btnChange_onclick() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                var Length = row.length - 1;
                for (n = 0; n < Length; n++) {
                    col = row[n].split("ʘ");
                    //                                                                  VetID           DocID          TransID         ApproveFlag
                    if (document.getElementById("<%= hdnObserveDtl.ClientID %>").value == "")
                        document.getElementById("<%= hdnObserveDtl.ClientID %>").value = col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[10] + "¶";
                    else
                        document.getElementById("<%= hdnObserveDtl.ClientID %>").value += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[10] + "¶";
                }
                ObserveDtl = document.getElementById("<%= hdnObserveDtl.ClientID %>").value;
                var Remark = document.getElementById("<%= txtAppRemark.ClientID %>").value;
                var Status = 3;
                var ExecutantDtl = document.getElementById("<%= hdnDocument.ClientID %>").value;

                document.getElementById("btnChange").disabled = true;
                ToServer("4^" + RequestID + "^" + ObserveDtl + "^" + Remark + "^" + Status + "^" + ExecutantDtl, 4);
            }

            function btnHold_onclick() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                var Length = row.length - 1;
                for (n = 0; n < Length; n++) {
                    col = row[n].split("ʘ");
                    //                                                                  VetID           DocID          TransID         ApproveFlag
                    if (document.getElementById("<%= hdnObserveDtl.ClientID %>").value == "")
                        document.getElementById("<%= hdnObserveDtl.ClientID %>").value = col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[10] + "¶";
                    else
                        document.getElementById("<%= hdnObserveDtl.ClientID %>").value += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[10] + "¶";
                }
                ObserveDtl = document.getElementById("<%= hdnObserveDtl.ClientID %>").value;
                var Remark = document.getElementById("<%= txtAppRemark.ClientID %>").value;
                var Status = 4;
                var ExecutantDtl = document.getElementById("<%= hdnDocument.ClientID %>").value;

                document.getElementById("btnHold").disabled = true;
                ToServer("4^" + RequestID + "^" + ObserveDtl + "^" + Remark + "^" + Status + "^" + ExecutantDtl, 4);
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                        {
                            //SBIÆ  Ratheesh P RÆ  ADFSDFSDF SDSDFSDFSDFÆ  AVAILING OF LOANÆ 0Æ  2Æ  
                            var Data = Arg.split("Æ");
                            document.getElementById("<%= txtParty.ClientID %>").value = Data[0]; //Party
                            document.getElementById("<%= txtStatement.ClientID %>").value = Data[1]; //Statement 
                            document.getElementById("<%= txtNature.ClientID %>").value = Data[2]; // Nature of transaction
                            if (Data[3] == 1) // Concerned Resolution
                                document.getElementById("colResolution").style.display = "";
                            else
                                document.getElementById("colResolution").style.display = "none";
                            if (Data[4] > 0) // Vet Document Details
                            {
                                document.getElementById("<%= hdnDocument.ClientID %>").value = Data[11]; // Vet Documents                                
                                var AllData = Data[13].split("£");
                                var RowCount = AllData.length - 1;
                                var j;
                                var Total = 0;
                                var row_bg1 = 0;
                                var SlNo = 0;
                                for (j = 0; j < RowCount; j++) {
                                    var Executant = AllData[j].split("§");
                                    document.getElementById("<%= hdnDepartment.ClientID %>").value += Executant[0]; // Department
                                    document.getElementById("<%= hdnEmployee.ClientID %>").value += Executant[1]; //Employee
                                }
                                document.getElementById("rowDocuments").style.display = "";
                                DataFill();
                            }
                            document.getElementById("<%= txtTerms.ClientID %>").value = Data[5]; // General Terms

                            if (Data[6] != "") {
                                document.getElementById("rowRemark").style.display = "";
                                document.getElementById("<%= txtRemarks.ClientID %>").value = Data[6]; // Remarks
                            }
                            else
                                document.getElementById("rowRemark").style.display = "none";



                            if (Data[10] != "") {
                                document.getElementById("rowRespRemark").style.display = "";
                                document.getElementById("<%= txtRespRemarks.ClientID %>").value = Data[10];

                            }
                            else
                                document.getElementById("rowRespRemark").style.display = "none";

                            if (Data[9] > 0) // Comments On Documents
                            {
                                document.getElementById("<%= hdnObservation.ClientID %>").value = Data[12];
                                var SatisfyCnt = 0;
                                row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                                for (n = 0; n <= row.length - 2; n++) {
                                    col = row[n].split("ʘ");
                                    if (col[9] == 0)
                                        SatisfyCnt = SatisfyCnt + 1;
                                }
                                if (SatisfyCnt > 0)
                                    document.getElementById("btnExecute").disabled = true;
                                else
                                    document.getElementById("btnExecute").disabled = false;
                                TableFill();
                            }
                            else
                                document.getElementById("btnExecute").disabled = false;
                            break;
                        }
                    case 2:
                        {
                            if (Arg != "") {
                                var Data = Arg.split("Æ");
                                var val = Data[1];
                                document.getElementById("<%= hdnEmployee.ClientID %>").value = Data[0];
                                var Employee = document.getElementById("<%= hdnEmployee.ClientID %>").value; //Employee Details                    
                                ComboFill(Employee, "cmbEmployee" + val);

                                var Newstr = "";                                                                    // 0             1          2             3          4
                                var row = document.getElementById("<%= hdnDocument.ClientID %>").value.split("ʘ"); //DocName ¶ Execute Dept ¶ Exeecute By ¶ Vet Flag ¶ Vet ID ʘ
                                for (n = 0; n <= row.length - 2; n++) {
                                    col = row[n].split("¶");
                                    if (col[4] == val) {
                                        Newstr += col[0] + "¶" + document.getElementById("cmbDept" + val).value + "¶" + document.getElementById("cmbEmployee" + val).value + "¶" + col[3] + "¶" + col[4] + "ʘ";
                                    }
                                    else
                                        Newstr += row[n] + "ʘ";
                                }
                                document.getElementById("<%= hdnDocument.ClientID %>").value = Newstr;
                            }
                            break;
                        }
                    case 4: // Confirmation
                        {
                            var Data = Arg.split("ʘ");
                            alert(Data[1]);
                            if (Data[0] == 0) {
                                //Return to Old Report
                                if (window.opener != null && !window.opener.closed) {
                                    window.opener.location.reload();
                                    if (Data[2] > 0) {
                                        window.open("ViewVetReport.aspx?RequestID=" + Data[2] + "", "_self");
                                    }
                                }
                            }
                            break;
                        }
                }
            }
            function DataFill() {
                var Tab = "";
                Tab += "<div style='width:80%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>DOCUMENT TO BE VETTED/EXECUTED FOR THE TRANSACTION</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:80%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:20%;text-align:left'>DOCUMENT&nbsp;NAME</td>";
                Tab += "<td style='width:20%;text-align:left'>EXECUTANT&nbsp;DEPT</td>";
                Tab += "<td style='width:20%;text-align:left'>EXECUTANT</td>";
                Tab += "<td style='width:20%;text-align:left'>VET/EXECUTE</td>";
                Tab += "<td style='width:10%;text-align:center'>VIEW</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:80%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                var HidArg = document.getElementById("<%= hdnDocument.ClientID %>").value.split("ʘ");
                var RowCount = HidArg.length - 1;

                var i;
                var Total = 0;
                var row_bg1 = 0;
                var SlNo = 0;
                for (i = 0; i < RowCount; i++) {
                    if (row_bg1 == 0) {
                        row_bg1 = 1;
                        Tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg1 = 0;
                        Tab += "<tr class='sub_second';>";
                    }
                    var HidArg1 = HidArg[i].split("¶");
                    //LOAN AGREEMENT¶  Remya¶  1¶  1
                    SlNo = SlNo + 1;
                    Tab += "<td style='width:10%;text-align:center'><input type='checkbox' id='chkSelection" + HidArg1[4] + "' onclick='ChkPotentialOnClick(" + HidArg1[4] + ")' /></td>";
                    Tab += "<td style='width:20%;text-align:left'>" + HidArg1[0] + "</td>";

                    var select = "<select id='cmbDept" + HidArg1[4] + "' class='NormalText' name='cmbDept" + HidArg1[4] + "' style='width:100%' disabled=true onchange='UpdateEmployee(" + HidArg1[4] + ")' >";
                    var rows = document.getElementById("<%= hdnDepartment.ClientID %>").value.split("Ř"); //Department Details
                    for (a = 1; a < rows.length; a++) {
                        var cols = rows[a].split("Ĉ");
                        if (cols[0] == HidArg1[1])
                            select += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                        else
                            select += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
                    }
                    Tab += "<td style='width:20%;text-align:center'>" + select + "</td>";

                    var select1 = "<select id='cmbEmployee" + HidArg1[4] + "' class='NormalText' name='cmbEmployee" + HidArg1[4] + "' style='width:100%' disabled=true onchange='UpdateCurrentEmployee(" + HidArg1[4] + ")'>";
                    var rowEmp = document.getElementById("<%= hdnEmployee.ClientID %>").value.split("Ř"); //Employee Details
                    for (j = 1; j < rowEmp.length; j++) {
                        var colEmp = rowEmp[j].split("Ĉ");
                        if (colEmp[0] == HidArg1[2])
                            select1 += "<option value='" + colEmp[0] + "' selected=true>" + colEmp[1] + "</option>";
                        else
                            select1 += "<option value='" + colEmp[0] + "' >" + colEmp[1] + "</option>";
                    }
                    Tab += "<td style='width:20%;text-align:center'>" + select1 + "</td>";

                    if (HidArg1[3] == 1)
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE AND VET</td>";
                    else
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE</td>";

                    if (HidArg1[3] == 0)
                        Tab += "<td style='width:10%;text-align:center'></td>";
                    else
                        Tab += "<td style='width:10%;text-align:center'><img id='imgReport' src='../Image/viewReport.PNG' title='View Attachment' Height='20px' Width='20px' onclick= 'ViewDocument(" + HidArg1[4] + ")' style='cursor:pointer;'/></td></tr>";
                }
                if (row_bg1 == 0)
                    Tab += "<tr style='background-color:OldLace'></tr>";
                else
                    Tab += "<tr style='background-color:Wheat'></tr>";
                Tab += "</table></div>";
                document.getElementById("<%= pnlDocDtl.ClientID %>").innerHTML = Tab;
            }
            function ChkPotentialOnClick(val) {
                if (document.getElementById("chkSelection" + val).checked == true) {
                    document.getElementById("cmbDept" + val).disabled = false;
                    document.getElementById("cmbEmployee" + val).disabled = false;
                }
                else {
                    document.getElementById("cmbDocument" + val).disabled = true;
                    document.getElementById("cmbEmployee" + val).disabled = true;
                }
            }
            function UpdateEmployee(val) {
                var DeptID = document.getElementById("cmbDept" + val).value;
                GetEmployee(DeptID, val);
            }
            function UpdateCurrentEmployee(val) {
                var Newstr = "";                                                                    // 0             1          2             3          4
                var row = document.getElementById("<%= hdnDocument.ClientID %>").value.split("ʘ"); //DocName ¶ Execute Dept ¶ Exeecute By ¶ Vet Flag ¶ Vet ID ʘ
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("¶");
                    if (col[4] == val) {
                        Newstr += col[0] + "¶" + document.getElementById("cmbDept" + val).value + "¶" + document.getElementById("cmbEmployee" + val).value + "¶" + col[3] + "¶" + col[4] + "ʘ";
                    }
                    else
                        Newstr += row[n] + "ʘ";
                }
                document.getElementById("<%= hdnDocument.ClientID %>").value = Newstr;
            }
            function TableFill() {
                if (document.getElementById("<%= hdnObservation.ClientID %>").value == "") {
                    document.getElementById("rowObserve").style.display = "none";
                }
                else {
                    document.getElementById("rowObserve").style.display = "";
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div style='width:100%; height:auto; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    tab += "<tr >";
                    tab += "<td style='width:5%;text-align:center' >#</td>";
                    tab += "<td style='width:10%;text-align:left'>Document For Transaction</td>";
                    tab += "<td style='width:15%;text-align:left'>Reference</td>";
                    tab += "<td style='width:15%;text-align:left'>Clause</td>";
                    tab += "<td style='width:15%;text-align:left'>Comments</td>";
                    tab += "<td style='width:15%;text-align:left'>Response</td>";
                    tab += "<td style='width:15%;text-align:left'>Remark</td>";
                    tab += "<td style='width:5%;text-align:left'>Status</td>";
                    tab += "<td style='width:5%;text-align:left'>Approve</td>";
                    tab += "</tr>";
                    tab += "</table></div>";
                    tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    if (document.getElementById("<%= hdnObservation.ClientID %>").value != "") {

                        row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");

                        for (n = 0; n <= row.length - 2; n++) {
                            col = row[n].split("ʘ");
                            if (row_bg == 0) {
                                row_bg = 1;
                                tab += "<tr class='sub_first';>";
                            }
                            else {
                                row_bg = 0;
                                tab += "<tr class='sub_second';>";
                            }
                            i = n + 1;

                            tab += "<td style='width:5%;text-align:left'>" + i + "</td>";
                            tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>"; //Doc Name
                            tab += "<td style='width:15%;text-align:left'>" + col[4] + "</td>"; //Reference
                            tab += "<td style='width:15%;text-align:left'>" + col[5] + "</td>"; //Clause
                            tab += "<td style='width:15%;text-align:left'>" + col[6] + "</td>"; //Comment
                            if (col[7] == "")
                                tab += "<td style='width:15%;text-align:left'>" + col[7] + "</td>"; //Response
                            else
                                tab += "<td style='width:15%;text-align:left'>" + col[7] + "<br/><a href='ViewPrevResponseRpt.aspx?TransID=" + col[2] + "' style='text-align:right;' target='_blank' >Previous</a></td>"; //Response
                            tab += "<td style='width:15%;text-align:left'>" + col[8] + "</td>"; //Remark
                            if (col[9] == 1)//Satisfy Flag
                            {
                                tab += "<td style='width:5%;text-align:left'>SATISFIED</td>";
                                tab += "<td style='width:5%;text-align:left'></td>";
                            }
                            else {
                                tab += "<td style='width:5%;text-align:left'>NOT SATISFIED</td>";
                                tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkApprove" + col[2] + "' onclick='ChkApproveOnlick(" + col[2] + ")' /></td>";
                            }
                            tab += "</tr>";
                        }
                    }
                    tab += "</table></div>";
                    document.getElementById("<%= pnlObserve.ClientID %>").innerHTML = tab;
                }
                //--------------------- Clearing Data ------------------------//
            }
            function ChkApproveOnlick(val) {
                UpdateString(val, 1);
                if (document.getElementById("<%= hdnObservation.ClientID %>").value != "") {
                    var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                    var Length = row.length - 1;
                    var ApproveCount = 0;
                    var NotSatCount = 0;
                    for (n = 0; n < Length; n++) {
                        col = row[n].split("ʘ");
                        if (col[9] == 0)
                            NotSatCount += 1;
                        if (col[10] == 1)
                            ApproveCount += 1;
                    }
                    if (NotSatCount == ApproveCount)
                        document.getElementById("btnExecute").disabled = false;
                    else
                        document.getElementById("btnExecute").disabled = true;
                }
            }
            function UpdateString(Val, Index) {
                var i = 0;
                var Newstr = "";
                var row = document.getElementById("<%= hdnObservation.ClientID %>").value.split("¶");
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("ʘ");
                    if (col[2] == Val) {
                        if (Index == 1) {
                            if (document.getElementById("chkApprove" + col[2]).checked == true)
                                Newstr += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[3] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "ʘ" + col[7] + "ʘ" + col[8] + "ʘ" + col[9] + "ʘ1¶";
                            else
                                Newstr += col[0] + "ʘ" + col[1] + "ʘ" + col[2] + "ʘ" + col[3] + "ʘ" + col[4] + "ʘ" + col[5] + "ʘ" + col[6] + "ʘ" + col[7] + "ʘ" + col[8] + "ʘ" + col[9] + "ʘ0¶";
                        }
                    }
                    else
                        Newstr += row[n] + "¶";
                }
                document.getElementById("<%= hdnObservation.ClientID %>").value = Newstr;
            }
            function GetVetReport() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                window.open("ViewVetFullReport.aspx?RequestID=" + RequestID);
            }
            function ViewDocument(VetID) {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowVetFormat.aspx?RequestID=" + RequestID + "&VetID=" + VetID);
                return false;
            }
            function btnExit_onclick() {
                //Return to Old Report
                if (window.opener != null && !window.opener.closed) {
                    window.opener.location.reload();
                    window.close();
                }
                window.onbeforeunload = RefreshParent;
            }           

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 100%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 97%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 100%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Party with which transaction is plan
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtParty" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Statement Of Facts
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtStatement" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td id="colResolution" style="width: 10%; cursor: pointer">
                        <asp:ImageButton ID="cmdView" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Nature of transaction
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtNature" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDocuments">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlDocDtl" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        General Terms
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtTerms" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 10%; text-align: center; cursor: pointer;">
                        <asp:HyperLink ID="hlVetRpt" runat="server" Font-Underline="True" ForeColor="#3333CC">Vet Report</asp:HyperLink>
                    </td>
                </tr>
                 <tr id="rowRemark">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Remarks
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtRemarks" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                 <tr id="rowRespRemark">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 20%; text-align: left;">
                        Responser Remarks</td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtRespRemarks" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rowObserve">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlObserve" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Remarks If Any
                    </td>
                    <td style="width: 60%; text-align: left;">
                        &nbsp;<asp:TextBox ID="txtAppRemark" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; cursor: pointer" colspan="4">
                        <input id="btnExecute" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="TO BE EXECUTED" onclick="return btnExecute_onclick()" />
                        <input id="btnNotExe" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="NOT TO BE EXECUTED" onclick="return btnNotExe_onclick()" />
                        <input id="btnChange" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="TO BE CHANGED" onclick="return btnChange_onclick()" />
                        <input id="btnHold" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="HOLD" onclick="return btnHold_onclick()" />
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 170px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocument" runat="server" />
            <asp:HiddenField ID="hdnObservation" runat="server" />
            <asp:HiddenField ID="hdnObserveDtl" runat="server" />
            <asp:HiddenField ID="hdnDepartment" runat="server" />
            <asp:HiddenField ID="hdnEmployee" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
