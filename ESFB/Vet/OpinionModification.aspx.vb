﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vet_OpinionModification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer

#Region "Page Load & Dispose"
    Protected Sub Vet_Opinion_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub Vet_Opinion_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            Me.hdnRequest.Value = CStr(RequestID)

            Me.Master.subtitle = "Modify Opinion"

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            Me.chkModify.Attributes.Add("onclick", "return ModifyAttachment()")
            Me.btnSave.Attributes.Add("onclick", "return SaveOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Compliance For Approve
                DT = GF.GetQueryResult("SELECT upper(v.USER_OPINION)+'Æ'+upper(v.STATEMENT_FACT)+'Æ'+isnull((select CONVERT(varchar(10),count(a.REQUEST_ID),0) from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID),0) FROM VET_MASTER v WHERE v.STATUS_ID=2 and v.SERVICE_ID=2 and v.REQUEST_ID=" & RequestID & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn = DT.Rows(0)(0).ToString()
                End If
                CallBackReturn += "Æ"
                Dim RequestData() As String = CStr(DT.Rows(0)(0)).Split(CChar("Æ"))
                If CInt(RequestData(2)) > 0 Then
                    DT = GF.GetQueryResult("SELECT convert(varchar(10),PkId)+ '®' +convert(varchar(10),REQUEST_ID)+ '®' + FILE_NAME FROM DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT where SERVICE_ID=2 and REQUEST_ID=" & RequestID & "")
                    If DT.Rows.Count > 0 Then
                        For Each DR In DT.Rows
                            CallBackReturn += DR(0).ToString() + "¶"
                        Next
                    End If
                End If
        End Select
    End Sub
#End Region

#Region "Confirmation"
    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim UserID As Integer = CInt(Session("UserID"))
            DT = GF.GetQueryResult("select Department_ID from EMP_MASTER where Emp_Code = " & UserID & "")
            Dim DepartmentID As Integer = CInt(DT.Rows(0)(0))
            Dim Opinion As String = CStr(Me.txtOpinion.Text)
            Dim Statement As String = CStr(Me.txtStatement.Text)
            Dim ModifyFlag As Integer = 0
            If (Me.chkModify.Checked = True) Then
                ModifyFlag = 1
            End If
            If (CInt(Me.hdnValue.Value) = 1) Then
                ModifyFlag = 1
            End If

            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files

            Try
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                Params(0).Value = RequestID
                Params(1) = New SqlParameter("@Opinions", SqlDbType.VarChar, 1000)
                Params(1).Value = Opinion
                Params(2) = New SqlParameter("@Statement", SqlDbType.VarChar, 1000)
                Params(2).Value = Statement
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = UserID
                Params(4) = New SqlParameter("@DeptID", SqlDbType.Int)
                Params(4).Value = DepartmentID
                Params(5) = New SqlParameter("@ModifyAtt", SqlDbType.Int)
                Params(5).Value = ModifyFlag
                Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(7).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_VET_MODIFY_OPINION", Params)
                ErrorFlag = CInt(Params(6).Value)
                Message = CStr(Params(7).Value)

                If ModifyFlag > 0 And ErrorFlag = 0 And hfc.Count > 0 Then
                    For i = 0 To hfc.Count - 1
                        Dim myFile As HttpPostedFile = hfc(i)
                        Dim nFileLen As Integer = myFile.ContentLength
                        Dim FileName As String = ""
                        If (nFileLen > 0) Then
                            ContentType = myFile.ContentType
                            FileName = myFile.FileName
                            AttachImg = New Byte(nFileLen - 1) {}
                            myFile.InputStream.Read(AttachImg, 0, nFileLen)

                            Dim Param(6) As SqlParameter
                            Param(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                            Param(0).Value = RequestID
                            Param(1) = New SqlParameter("@ServiceID", SqlDbType.Int)
                            Param(1).Value = 2
                            Param(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                            Param(2).Value = AttachImg
                            Param(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                            Param(3).Value = ContentType
                            Param(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                            Param(4).Value = FileName
                            Param(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                            Param(5).Direction = ParameterDirection.Output
                            Param(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                            Param(6).Direction = ParameterDirection.Output
                            DB.ExecuteNonQuery("SP_VET_REQUEST_ATTACHMENT", Param)
                        End If
                    Next
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("         if (window.opener != null && !window.opener.closed);{window.opener.location.reload();window.close();}window.onbeforeunload = RefreshParent;")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
    
End Class
