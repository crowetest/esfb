﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vet_VetTransactionApprove
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT_VET, DT_SUB As New DataTable
    Dim RequestID, StatusID, HigherFlag As Integer

#Region "Page Load & Dispose"
    Protected Sub Vet_VettingTransaction_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            Me.hdnRequest.Value = CStr(RequestID)
            StatusID = CInt(Request.QueryString.Get("StatusID"))
            Me.hdnStatus.Value = CStr(StatusID)
            HigherFlag = CInt(Request.QueryString.Get("HigherFlag"))
            Me.hdnHigher.Value = CStr(HigherFlag)

            Me.Master.subtitle = "Approve Vetting for executing transactions"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            If Not IsPostBack Then
                DT = GF.GetQueryResult("select Department_ID,department_name from DEPARTMENT_MASTER where Department_ID in (select Department_ID from EMP_MASTER where Status_ID = 1) order by Department_Name")
                GF.ComboFill(cmbApproveDept, DT, 0, 1)
                Dim Dept As Integer = 0
                If DT.Rows.Count > 0 Then
                    Dept = CInt(Me.cmbApproveDept.SelectedValue)
                End If

                DT = GF.GetQueryResult("select emp_code,emp_name + ' -- ' + convert(varchar(10),emp_code) from EMP_MASTER where Department_ID = " & Dept & " order by emp_name")
                GF.ComboFill(cmbApproveBy, DT, 0, 1)
            End If

            Me.cmdView.Attributes.Add("onclick", "return ViewAttachment(1)")
            Me.cmbApproveDept.Attributes.Add("onchange", "return DeptOnChange()")
            Me.hlVetRpt.Attributes.Add("onclick", "return GetVetReport()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("^"))
        Dim DR, DR1 As DataRow
        Dim NewSub As String = Nothing
        Dim DocData() As String
        Select Case CInt(Data(0))
            Case 1 'Fill Vet Request For Approve
                If StatusID = 4 Then
                    DT = GF.GetQueryResult("select p.PARTY_NAME+'Æ'+upper(v.STATEMENT_FACT)+'Æ'+n.NATURE_NAME+'Æ'+isnull((select convert(varchar(10),case when a.CON_RESOLUTION IS NOT NULL then 1 else 0 end) from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+ isnull((select convert(varchar(10),count( m.REQUEST_ID)) from VET_TRANSACTION_MASTER m where m.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+upper(v.GENERAL_TERM)+'Æ'+UPPER(v.VET_REMARK)+'Æ'+CONVERT(varchar(10),v.APPROVE_DEPT)+'Æ'+CONVERT(varchar(10),v.APPROVE_BY)+'Æ'+ isnull((select convert(varchar(10),count( m.REQUEST_ID)) from VET_TRANSACTION_DTL m where m.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+isnull(v.RESPONSE_REMARK,'') from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.STATUS_ID=3 and v.SERVICE_ID=4 and v.REQUEST_ID=" & RequestID & "")
                    If DT.Rows.Count > 0 Then
                        CallBackReturn = DT.Rows(0)(0).ToString() + "Æ"
                        DT_VET = GF.GetQueryResult("SELECT d.DOCUMENT_NAME+'¶'+upper(e.Emp_Name)+'¶'+CONVERT(varchar(10),v.VET_FLAG)+'¶'+CONVERT(varchar(10),v.VET_ID)+'¶'+CONVERT(varchar(10),v.DOC_ID)+'¶'+CONVERT(varchar(10),v.REQUEST_ID)+'¶'+case when V.VET_FLAG= 1 then (select convert(varchar(10), a.PkId) from DMS_ESFB.dbo.VET_TRANSDOC_ATTACHMENT a where a.VET_ID=v.VET_ID and a.DOC_ID=v.DOC_ID) else '0' end from VET_TRANSACTION_MASTER v, VET_DOCUMENT d,EMP_MASTER e where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_ID=" & RequestID & " order by v.VET_ID")
                        If DT_VET.Rows.Count > 0 Then
                            For Each DR In DT_VET.Rows
                                CallBackReturn += CStr(DR(0)) + "ʘ"
                            Next
                        End If
                        CallBackReturn += "Æ"
                        Dim VetData() As String = CStr(DT.Rows(0)(0)).Split(CChar("Æ"))
                        DT = GF.GetQueryResult("select emp_code,emp_name + ' -- ' + convert(varchar(10),emp_code) from EMP_MASTER where Department_ID = " & CInt(VetData(7)) & " order by emp_name")
                        For Each DR In DT.Rows
                            CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                        Next
                        CallBackReturn += "Æ"
                        If CInt(VetData(9)) > 0 Then
                            If StatusID = 4 Then
                                DT = GF.GetQueryResult("select v.VET_ID,v.DOC_ID,v.TRANS_ID,d.DOCUMENT_NAME,v.REFERENCE,v.CLAUSE,v.COMMENT from VET_TRANSACTION_DTL v,VET_DOCUMENT d where v.DOC_ID=d.DOCUMENT_ID and v.STATUS_ID=0 and v.REQUEST_ID=" & RequestID & " order by v.TRANS_ID")
                                For Each DR In DT.Rows
                                    CallBackReturn += DR(0).ToString() + "ʘ" + DR(1).ToString() + "ʘ" + DR(2).ToString() + "ʘ" + DR(3).ToString() + "ʘ" + DR(4).ToString() + "ʘ" + DR(5).ToString() + "ʘ" + DR(6).ToString() + "¶"
                                Next
                            End If
                        End If
                        CallBackReturn += "Æ"
                        CallBackReturn += "Ř-1§-1ĈSELECT"
                        DT = GF.GetQueryResult("select CONVERT(varchar(10),v.DOC_ID) +'§'+ CONVERT(varchar(10),v.VET_ID),D.DOCUMENT_NAME from VET_TRANSACTION_MASTER v, VET_DOCUMENT d where v.DOC_ID=d.DOCUMENT_ID AND v.VET_FLAG=1 AND v.REQUEST_ID=" & RequestID & " order by v.VET_ID")
                        If DT.Rows.Count > 0 Then
                            For Each DR In DT.Rows
                                CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                            Next
                        End If
                    End If
                Else
                    DT = GF.GetQueryResult("select p.PARTY_NAME+'Æ'+upper(v.STATEMENT_FACT)+'Æ'+n.NATURE_NAME+'Æ'+isnull((select convert(varchar(10),case when a.CON_RESOLUTION IS NOT NULL then 1 else 0 end) from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+ isnull((select convert(varchar(10),count( m.REQUEST_ID)) from VET_TRANSACTION_MASTER m where m.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+upper(v.GENERAL_TERM)+'Æ'+UPPER(v.VET_REMARK)+'Æ'+CONVERT(varchar(10),v.APPROVE_DEPT)+'Æ'+em.Emp_Name+'Æ'+ isnull((select convert(varchar(10),count( m.REQUEST_ID)) from VET_TRANSACTION_DTL m where m.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+isnull(v.RESPONSE_REMARK,'') from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER em where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.APPROVE_BY=em.Emp_Code and v.STATUS_ID=6 and v.SERVICE_ID=4 and v.REQUEST_ID=" & RequestID & "")
                    If DT.Rows.Count > 0 Then
                        CallBackReturn = DT.Rows(0)(0).ToString() + "Æ"
                        DT_VET = GF.GetQueryResult("SELECT d.DOCUMENT_NAME+'¶'+upper(e.Emp_Name)+'¶'+CONVERT(varchar(10),v.VET_FLAG)+'¶'+CONVERT(varchar(10),v.VET_ID)+'¶'+CONVERT(varchar(10),v.DOC_ID)+'¶'+CONVERT(varchar(10),v.REQUEST_ID)+'¶'+case when V.VET_FLAG= 1 then (select convert(varchar(10), a.PkId) from DMS_ESFB.dbo.VET_TRANSDOC_ATTACHMENT a where a.VET_ID=v.VET_ID and a.DOC_ID=v.DOC_ID) else '0' end from VET_TRANSACTION_MASTER v, VET_DOCUMENT d,EMP_MASTER e where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_ID=" & RequestID & " order by v.VET_ID")
                        If DT_VET.Rows.Count > 0 Then
                            For Each DR In DT_VET.Rows
                                DocData = CStr(DR(0)).Split(CChar("¶"))
                                If CInt(DocData(6)) > 0 Then
                                    DT_SUB = GF.GetQueryResult("select CONVERT(varchar(10),PkId)+'¶'+DOCFILE_NAME from DMS_ESFB.dbo.VET_REDRAFT_ATTACHMENT WHERE REQUEST_ID=" & DocData(5) & " AND VET_ID=" & DocData(3) & " AND DOC_ID=" & DocData(4) & "")
                                    For Each DR1 In DT_SUB.Rows
                                        NewSub += CStr(DR1(0)) + "¥"
                                    Next
                                    CallBackReturn += CStr(DR(0)) + "§" + NewSub + "ʘ"
                                Else
                                    CallBackReturn += CStr(DR(0)) + "§ʘ"
                                End If
                            Next
                        End If
                        CallBackReturn += "Æ"
                        Dim VetData() As String = CStr(DT.Rows(0)(0)).Split(CChar("Æ"))
                        If CInt(VetData(9)) > 0 Then
                            DT = GF.GetQueryResult("select v.VET_ID,v.DOC_ID,v.TRANS_ID,d.DOCUMENT_NAME,upper(v.REFERENCE),upper(v.CLAUSE),upper(v.COMMENT),upper(v.RESPONSE),upper(v.REMARK),isnull(CONVERT(varchar(10),v.SATISFY_FLAG),0) from VET_TRANSACTION_DTL v,VET_DOCUMENT d where v.DOC_ID=d.DOCUMENT_ID and v.STATUS_ID>=1 and v.REQUEST_ID=" & RequestID & " order by v.TRANS_ID")
                            For Each DR In DT.Rows
                                CallBackReturn += DR(0).ToString() + "ʘ" + DR(1).ToString() + "ʘ" + DR(2).ToString() + "ʘ" + DR(3).ToString() + "ʘ" + DR(4).ToString() + "ʘ" + DR(5).ToString() + "ʘ" + DR(6).ToString() + "ʘ" + DR(7).ToString() + "ʘ" + DR(8).ToString() + "ʘ" + DR(9).ToString() + "ʘ0¶"
                            Next
                        End If
                        CallBackReturn += "Æ"
                        CallBackReturn += "Ř-1§-1ĈSELECT"
                        DT = GF.GetQueryResult("select CONVERT(varchar(10),v.DOC_ID) +'§'+ CONVERT(varchar(10),v.VET_ID),D.DOCUMENT_NAME from VET_TRANSACTION_MASTER v, VET_DOCUMENT d where v.DOC_ID=d.DOCUMENT_ID AND v.VET_FLAG=1 AND v.REQUEST_ID=" & RequestID & " order by v.VET_ID")
                        If DT.Rows.Count > 0 Then
                            For Each DR In DT.Rows
                                CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                            Next
                        End If
                    End If
                End If
            Case 2
                'Department Change
                Dim DepartmentID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select emp_code,emp_name + ' -- ' + convert(varchar(10),emp_code) from EMP_MASTER where Department_ID = " & DepartmentID & " order by emp_name")
                CallBackReturn = ""
                For Each DR In DT.Rows
                    CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                Next
            Case 3
                Dim RequestID As Integer = CInt(Data(1))
                Dim ApproveDept As Integer = CInt(Data(2))
                Dim ApproveBy As Integer = CInt(Data(3))
                Dim ObserveDtl As String = CStr(Data(4))
                Dim Status As Integer = CInt(Data(5))
                Dim NewOberve As String = CStr(Data(6))
                Dim AppRemark As String = CStr(Data(7))
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(9) As SqlParameter
                    Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(0).Value = RequestID
                    Params(1) = New SqlParameter("@ApproveDept", SqlDbType.Int)
                    Params(1).Value = ApproveDept
                    Params(2) = New SqlParameter("@ApproveBy", SqlDbType.Int)
                    Params(2).Value = ApproveBy
                    Params(3) = New SqlParameter("@ObserveDtl", SqlDbType.VarChar, 10000)
                    Params(3).Value = ObserveDtl
                    Params(4) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(4).Value = Status
                    Params(5) = New SqlParameter("@NewObserveDtl", SqlDbType.VarChar, 5000)
                    Params(5).Value = NewOberve
                    Params(6) = New SqlParameter("@AppRemark", SqlDbType.VarChar, 5000)
                    Params(6).Value = AppRemark
                    Params(7) = New SqlParameter("@UserID", SqlDbType.VarChar, 5000)
                    Params(7).Value = UserID
                    Params(8) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(8).Direction = ParameterDirection.Output
                    Params(9) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(9).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_APPROVE_VET", Params)
                    ErrorFlag = CInt(Params(8).Value)
                    Message = CStr(Params(9).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message + "ʘ" + RequestID.ToString + "ʘ" + Status.ToString
            Case 4
                Dim RequestID As Integer = CInt(Data(1))
                Dim ObserveDtl As String = CStr(Data(2))
                Dim AppRemark As String = CStr(Data(3))
                Dim Status As Integer = CInt(Data(4))
                Dim NewObserveDtl As String = CStr(Data(5))
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(7) As SqlParameter
                    Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(0).Value = RequestID
                    Params(1) = New SqlParameter("@ObserveDtl", SqlDbType.VarChar, 8000)
                    Params(1).Value = ObserveDtl
                    Params(2) = New SqlParameter("@AppRemark", SqlDbType.VarChar, 1000)
                    Params(2).Value = AppRemark
                    Params(3) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(3).Value = Status
                    Params(4) = New SqlParameter("@NewObserveDtl", SqlDbType.VarChar, 8000)
                    Params(4).Value = NewObserveDtl
                    Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(5).Value = UserID
                    Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(7).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_APPROVE_REMARK", Params)
                    ErrorFlag = CInt(Params(6).Value)
                    Message = CStr(Params(7).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message + "ʘ" + RequestID.ToString + "ʘ" + Status.ToString
            Case 5
                Dim RequestID As Integer = CInt(Data(1))
                Dim VetID As Integer = CInt(Data(2))
                Dim TransID As Integer = CInt(Data(3))
                Dim Err As Integer = DB.ExecuteNonQuery("delete from VET_TRANSACTION_DTL where REQUEST_ID=" & RequestID & " and VET_ID= " & VetID & " and TRANS_ID= " & TransID & "")
                If Err = 1 Then
                    CallBackReturn = CStr(0) + "ʘ" + "Successfully Deleted"
                End If
            Case 6 'Reject Confirm
                Dim RequestID As Integer = CInt(Data(1))
                Dim Reason As String = CStr(Data(2))
                Dim Err As Integer = DB.ExecuteNonQuery("update VET_MASTER set STATUS_ID=16,REJECT_REASON='" & Reason & "' where REQUEST_ID=" & RequestID & " and SERVICE_ID=4")
                If Err = 1 Then
                    CallBackReturn = CStr(0) + "ʘ" + "Rejected Successfully"
                End If
        End Select
    End Sub
#End Region

End Class
