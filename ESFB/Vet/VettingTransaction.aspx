﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="VettingTransaction.aspx.vb" Inherits="Vet_VettingTransaction" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {
                document.getElementById("colResolution").style.display = "none";
                document.getElementById("rowDocuments").style.display = "none";
                document.getElementById("rowAddDisplay").style.display = "none";
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = "";         
                ToServer("1ʘ", 1);
            }
            function ViewAttachment(Value) 
            {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowFormat.aspx?RequestID=" + RequestID + "&Value=" + Value + "&Id=0");
                return false;
            }
            function DeptOnChange() {
                var DeptID;
                DeptID = document.getElementById("<%= cmbApproveDept.ClientID %>").value;
                ToServer("2ʘ" + DeptID, 2);
            }
            function AddNewComment() {
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                    var Len = row.length - 1;
                    col = row[Len].split("µ");
                    if (col[0] != "-1" && col[1] != "") {
                        document.getElementById("<%= hdnNewDtl.ClientID %>").value += "¥-1§-1µµµµµµ";
                    }
                }
                else
                    document.getElementById("<%= hdnNewDtl.ClientID %>").value = "¥-1§-1µµµµµµ";
                NewCommentFill();
            }
            function NewCommentFill() {
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    document.getElementById("rowAddDisplay").style.display = "";
                    var row_bg = 0;
                    var tab = "";
                    tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    tab += "<tr height=20px;  class='tblQal'>";
                    tab += "<td style='width:5%;text-align:center' >#</td>";
                    tab += "<td style='width:20%;text-align:left' >Document For Transaction</td>";
                    tab += "<td style='width:23%;text-align:left'>Reference</td>";
                    tab += "<td style='width:23%;text-align:left'>Clause</td>";
                    tab += "<td style='width:24%;text-align:left'>Comments</td>";
                    tab += "<td style='width:5%;text-align:left'></td>";
                    tab += "</tr>";
                    tab += "</table></div>";
                    tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
                    tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                    row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                    for (n = 1; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                        if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class='sub_second';>";
                        }
                        // 0 - id, 1 - Docid, 2 - Vetid, 3- Doc Name, 4- Reference, 5-Clause, 6- Comment
                        tab += "<td style='width:5%;text-align:center' >" + n + "</td>";

                        if (col[0] == "-1§-1") {
                            var select1 = CreateSelectDocument(n, 1, col[0]);
                            tab += "<td style='width:20%;text-align:left'>" + select1 + "</td>";
                        }
                        else
                            tab += "<td style='width:20%;text-align:left'>" + col[3] + "</td>"; //DocName

                        if (col[2] != "")
                            tab += "<td style='width:23%;text-align:left' >" + col[4] + "</td>"; //Reference
                        else {
                            var txtBox = "<input id='txtReference" + n + "' name='txtReference" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500' onchange='updateValueDocument(" + n + ")' />";
                            tab += "<td style='width:23%;text-align:left' >" + txtBox + "</td>";
                        }

                        if (col[3] != "")
                            tab += "<td style='width:23%;text-align:left' >" + col[5] + "</td>"; //Clause
                        else {
                            var txtBox = "<input id='txtClause" + n + "' name='txtClause" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500' onchange='updateValueDocument(" + n + ")' />";
                            tab += "<td style='width:23%;text-align:left' >" + txtBox + "</td>";
                        }

                        if (col[4] != "")
                            tab += "<td style='width:24%;text-align:left' >" + col[6] + "</td>"; //Comment
                        else {
                            var txtBox = "<input id='txtComment" + n + "' name='txtComment" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500' onchange='updateValueDocument(" + n + ")'  onkeydown='CreateNewDocument(event," + n + ")' />";
                            tab += "<td style='width:25%;text-align:left' >" + txtBox + "</td>";
                        }

                        if (col[0] == "-1§-1")
                            tab += "<td style='width:5%;text-align:left'></td>";
                        else
                            tab += "<td style='width:5%;text-align:center' onclick=DeleteRowDocument('" + n + "')><img  src='../Image/remove.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                        tab += "</tr>";
                    }
                    tab += "</table></div></div></div>";
                    document.getElementById("<%= pnlNewComment.ClientID %>").innerHTML = tab;

                    setTimeout(function () { document.getElementById("cmbDocument" + (n - 1)).focus().select(); }, 4);
                }
                else
                    document.getElementById("rowAddDisplay").style.display = "none";
            }
            function CreateSelectDocument(n, id, value) {
                if (id == 1) {
                    if (value == '-1§-1')
                        var select1 = "<select id='cmbDocument" + n + "' class='NormalText' name='cmbDocument" + n + "' style='width:100%' onchange='updateValueDocument(" + n + ")'  >";
                    else
                        var select1 = "<select id='cmbDocument" + n + "' class='NormalText' name='cmbDocument" + n + "' style='width:100%' disabled=true onchange='updateValueDocument(" + n + ")' >";

                    var rows = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("Ř"); //Document Details
                    for (a = 1; a < rows.length; a++) {
                        var cols = rows[a].split("Ĉ");
                        if (cols[0] == value)
                            select1 += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                        else
                            select1 += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";

                    }
                }
                select1 += "</select>"
                return select1;
            }
            function CreateNewDocument(e, val) {
                var n = (window.Event) ? e.which : e.keyCode;
                if (n == 13 || n == 9) {
                    updateValueDocument(val);
                    AddNewComment();
                }
            }
            function updateValueDocument(id) {
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var NewStr = ""
                for (n = 1; n <= row.length - 1; n++) {
                    if (id == n) {
                        var DocName = document.getElementById("cmbDocument" + id).options[document.getElementById("cmbDocument" + id).selectedIndex].text;
                        var Doc = document.getElementById("cmbDocument" + id).value
                        var Document = document.getElementById("cmbDocument" + id).value.split("§");
                        var DocID = Document[0];
                        var VetID = Document[1];
                        if (DocID == -1) {
                            alert("Select Document For Transaction");
                            document.getElementById("cmbDocument" + id).focus();
                            return false;
                        }
                        var Reference = document.getElementById("txtReference" + id).value;
                        var Clause = document.getElementById("txtClause" + id).value;
                        var Comment = document.getElementById("txtComment" + id).value;
                        NewStr += "¥" + Doc + "µ" + DocID + "µ" + VetID + "µ" + DocName + "µ" + Reference + "µ" + Clause + "µ" + Comment;
                    }
                    else {
                        NewStr += "¥" + row[n];
                    }
                }
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
            }
            function DeleteRowDocument(id) {
                row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");
                var NewStr = ""
                for (n = 1; n <= row.length - 1; n++) {
                    if (id != n)
                        NewStr += "¥" + row[n];
                }
                document.getElementById("<%= hdnNewDtl.ClientID %>").value = NewStr;
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value == "")
                    document.getElementById("<%= hdnNewDtl.ClientID %>").value = "¥-1§-1µµµµµµ";
                NewCommentFill();
            }
            function btnApprove_onclick() {
                //Send For Approve
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value == "") {
                    if (document.getElementById("<%= txtRemarks.ClientID %>").value == "") {
                        alert("Please Enter Remarks");
                        document.getElementById("<%= txtRemarks.ClientID %>").focus();
                        return false;
                    }                    
                }
                if (document.getElementById("<%= txtTerms.ClientID %>").value == "") {
                    alert("Enter General Terms");
                    document.getElementById("<%= txtTerms.ClientID %>").focus();
                    return false;
                }                
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var Terms = document.getElementById("<%= txtTerms.ClientID %>").value;
                var AppDept = document.getElementById("<%= cmbApproveDept.ClientID %>").value;
                var AppBy = document.getElementById("<%= cmbApproveBy.ClientID %>").value;
                var Remarks = document.getElementById("<%= txtRemarks.ClientID %>").value;

                var NewObsDtl = "";
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    var row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");  //¥-1§-1µµµµµµ
                    for (n = 1; n < row.length - 1; n++) {
                        col = row[n].split("µ");
                        //           
                        NewObsDtl += col[1] + "®" + col[2] + "®" + col[4] + "®" + col[5] + "®" + col[6] + "¶";
                    }
                }
                var Status = 1;
                document.getElementById("btnApprove").disabled = true;
                ToServer("3ʘ" + RequestID + "ʘ" + Terms + "ʘ" + AppDept + "ʘ" + AppBy + "ʘ" + Remarks + "ʘ" + NewObsDtl + "ʘ" + Status, 3);
            }

            function btnResponse_onclick() {
                //Send For Response
//                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value == "") {
//                    alert("Add any comments for reponse");
//                    return false;
//                }
                if (document.getElementById("<%= txtTerms.ClientID %>").value == "") {
                    alert("Enter General Terms");
                    document.getElementById("<%= txtTerms.ClientID %>").focus();
                    return false;
                }                
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var Terms = document.getElementById("<%= txtTerms.ClientID %>").value;
                var AppDept = document.getElementById("<%= cmbApproveDept.ClientID %>").value;
                var AppBy = document.getElementById("<%= cmbApproveBy.ClientID %>").value;
                var Remarks = document.getElementById("<%= txtRemarks.ClientID %>").value;

                var NewObsDtl = "";
                if (document.getElementById("<%= hdnNewDtl.ClientID %>").value != "") {
                    var row = document.getElementById("<%= hdnNewDtl.ClientID %>").value.split("¥");  //¥-1§-1µµµµµµ
                    for (n = 1; n < row.length - 1; n++) {
                        col = row[n].split("µ");
                        //           
                        NewObsDtl += col[1] + "®" + col[2] + "®" + col[4] + "®" + col[5] + "®" + col[6] + "¶";
                    }
                }

                var Status = 2;
                document.getElementById("btnResponse").disabled = true;
                ToServer("3ʘ" + RequestID + "ʘ" + Terms + "ʘ" + AppDept + "ʘ" + AppBy + "ʘ" + Remarks + "ʘ" + NewObsDtl + "ʘ" + Status, 3);
            }
            function FromServer(Arg, Context) 
            {
                switch (Context) 
                {
                    case 1:
                    {
                        //SBIÆ  Ratheesh P RÆ  ADFSDFSDF SDSDFSDFSDFÆ  AVAILING OF LOANÆ 0Æ  2Æ  
                        var Data = Arg.split("Æ");
                        document.getElementById("<%= txtParty.ClientID %>").value = Data[0];
                        document.getElementById("<%= txtStatement.ClientID %>").value = Data[1];
                        document.getElementById("<%= txtNature.ClientID %>").value = Data[2];
                        if (Data[3] == 1)
                            document.getElementById("colResolution").style.display = "";
                        else
                            document.getElementById("colResolution").style.display = "none";
                        if (Data[4] > 0) {
                            document.getElementById("<%= hdnDocument.ClientID %>").value = Data[5];
                            document.getElementById("rowDocuments").style.display = "";
                            DataFill();
                        }
                        document.getElementById("<%= hdnDocDtl.ClientID %>").value = Data[6];                        
                        break;
                    }
                    case 2: //  Employee Fill On Change Of Department
                    {
                        ComboFill(Arg, "<%= cmbApproveBy.ClientID %>");
                        break;
                    }
                case 3: // Confirmation
                    {                        
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        if (Data[0] == 0) {
                            //Return to Old Report
                            if (window.opener != null && !window.opener.closed) {
                                window.opener.location.reload();
                                window.close();
                            }
                            window.onbeforeunload = RefreshParent;
                        }
                        break;
                    }
                }
            }
            function DataFill() 
            {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>DOCUMENT TO BE VETTED/EXECUTED FOR THE TRANSACTION</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:20%;text-align:left'>DOCUMENT&nbsp;NAME</td>";
                Tab += "<td style='width:20%;text-align:left'>EXECUTANT</td>";
                Tab += "<td style='width:20%;text-align:left'>VET/EXECUTE</td>";
                Tab += "<td style='width:10%;text-align:center'>VIEW</td>";
                Tab += "<td style='width:20%;text-align:center'>REDRAFT DOCUMENT</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                var HidArg = document.getElementById("<%= hdnDocument.ClientID %>").value.split("Ř");
                var RowCount = HidArg.length - 1;
                var j;
                var Total = 0;
                var row_bg1 = 0;
                var SlNo = 0;
                for (j = 0; j < RowCount; j++) {
                    if (row_bg1 == 0) {
                        row_bg1 = 1;
                        Tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg1 = 0;
                        Tab += "<tr class='sub_second';>";
                    }
                    var HidArg1 = HidArg[j].split("¶");
                    //LOAN AGREEMENT¶  Ratheesh P R¶  1¶  1
                    SlNo = SlNo + 1;
                    Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                    Tab += "<td style='width:20%;text-align:left'>" + HidArg1[0] + "</td>";
                    Tab += "<td style='width:20%;text-align:left'>" + HidArg1[1] + "</td>";
                    if (HidArg1[2] == 1)
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE AND VET</td>";
                    else 
                        Tab += "<td style='width:20%;text-align:left'>EXECUTE</td>";
                                      
                    if (HidArg1[2] == 0)
                        Tab += "<td style='width:10%;text-align:center'></td>";
                    else
                        Tab += "<td style='width:10%;text-align:center'><img id='imgReport' src='../Image/viewReport.PNG' Height='20px' Width='20px' onclick= 'ViewDocument(" + HidArg1[3] + ")' style='cursor:pointer;'/></td>";

                    if (HidArg1[2] == 0)
                        Tab += "<td style='width:20%;text-align:center'></td></tr>";
                    else
                        Tab += "<td style='width:20%;text-align:center'><a href='VetRedraftUpload.aspx?RequestID=" + HidArg1[5] + "&VetID=" + HidArg1[3] + "&DocID=" + HidArg1[4] + "&PkID=" + HidArg1[6] + "' style='text-align:right; color: red;' target='_blank'>Redraft</a></td></tr>";

                }
                if (row_bg1 == 0)
                    Tab += "<tr style='background-color:OldLace'></tr>";
                else
                    Tab += "<tr style='background-color:Wheat'></tr>";
                Tab += "</table></div>";
                document.getElementById("<%= pnlDocDtl.ClientID %>").innerHTML = Tab;
            }
            function ViewDocument(VetID) 
            {                
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowVetFormat.aspx?RequestID=" + RequestID + "&VetID=" + VetID);
                return false;
            }
            function btnExit_onclick(){
                //Return to Old Report
                if (window.opener != null && !window.opener.closed) {
                    window.opener.location.reload();
                    window.close();
                }
                window.onbeforeunload = RefreshParent;
            }
            
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 100%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 97%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 100%; margin: 0px auto;">
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Party with which transaction is plan
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtParty" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>                
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Statement Of Facts
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtStatement" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td id="colResolution" style="width: 10%">
                        <asp:ImageButton ID="cmdView" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" />
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Nature of transaction
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtNature" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowDocuments">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlDocDtl" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        General Terms
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtTerms" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="1000" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: right;">
                        &nbsp;
                    </td>
                    <td style="width: 60%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: center;" colspan="2">
                        <strong>ADD NEW COMMENTS</strong>
                        <img src="../Image/addIcon.png" style="height: 25px; width: 25px; float: right; z-index: 1;
                            cursor: pointer; padding-right: 10px;" onclick="AddNewComment()" title="Add New" />
                        </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowAddDisplay">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlNewComment" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: right;">
                        &nbsp;
                    </td>
                    <td style="width: 60%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Approving Authority
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:DropDownList ID="cmbApproveDept" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="50%" ForeColor="Black">
                        </asp:DropDownList>
                        <asp:DropDownList ID="cmbApproveBy" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="49%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 20%; text-align: left;">
                        Remarks
                    </td>
                    <td style="width: 60%; text-align: left;">
                        <asp:TextBox ID="txtRemarks" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="500" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <input id="btnResponse" style="font-family: cambria; cursor: pointer; width: 140px;"
                            type="button" value="SEND FOR RESPONSE" onclick="return btnResponse_onclick()" />
                        <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 140px;"
                            type="button" value="SEND FOR APPROVE" onclick="return btnApprove_onclick()" />
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 140px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocument" runat="server" />
            <asp:HiddenField ID="hdnDocDtl" runat="server" />
            <asp:HiddenField ID="hdnObservation" runat="server" />
            <asp:HiddenField ID="hdnNewDtl" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
