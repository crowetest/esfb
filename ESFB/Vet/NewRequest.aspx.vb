﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Vet_NewRequest
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim UserID As Integer
#Region "Page Load & Dispose"
    Protected Sub Vet_NewRequest_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub Vet_NewRequest_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 138) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "New Request"

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            If Not IsPostBack Then
                UserID = CInt(Session("UserID"))
                DT = DB.ExecuteDataSet("select COUNT(Department_ID) from DEPARTMENT_MASTER where Department_Head=" & UserID & "").Tables(0)
                If DT.Rows.Count > 0 Then
                    If CInt(DT.Rows(0)(0)) > 0 Then
                        Me.hdnIsHOD.Value = CStr(1)
                    Else
                        Me.hdnIsHOD.Value = CStr(0)
                    End If
                End If

                DT = DB.ExecuteDataSet("select COUNT(Emp_Code) from EMP_MASTER where Reporting_To=" & UserID & "").Tables(0)
                If DT.Rows.Count > 0 Then
                    If CInt(DT.Rows(0)(0)) > 0 Then
                        Me.hdnIsReportingTo.Value = CStr(1)
                    Else
                        Me.hdnIsReportingTo.Value = CStr(0)
                    End If
                End If

                DT = GF.GetQueryResult("select SERVICE_ID,SERVICE_NAME from VET_SERVICE where STATUS_ID=1")
                GF.ComboFill(cmbService, DT, 0, 1)

                DT = GF.GetQueryResult("select DRAFT_ID,DRAFT_NAME from VET_DRAFT where STATUS_ID=1 order by 2")
                GF.ComboFill(cmbDraftDoc, DT, 0, 1)

                DT = GF.GetQueryResult("select DOCUMENT_ID,DOCUMENT_NAME from VET_DOCUMENT where DOCUMENT_TYPE=1 and SERVICE_ID=3 and STATUS_ID=1 order by 2")
                GF.ComboFill(cmbNaturDoc, DT, 0, 1)

                DT = GF.GetQueryResult("select PARTY_ID,PARTY_NAME from VET_PARTY where STATUS_ID=1 order by 2")
                GF.ComboFill(cmbParty, DT, 0, 1)

                DT = GF.GetQueryResult("select DOCUMENT_ID,DOCUMENT_NAME from VET_DOCUMENT where DOCUMENT_TYPE=3 and SERVICE_ID=5 and STATUS_ID=1 order by 2")
                GF.ComboFill(cmbDocType, DT, 0, 1)

                DT = GF.GetQueryResult("select NATURE_ID,NATURE_NAME from VET_NATURE_TRANSACTION where STATUS_ID=1 order by 2")
                GF.ComboFill(cmbTransaction, DT, 0, 1)

                DT = GF.GetQueryResult("select DOCUMENT_ID,DOCUMENT_NAME from VET_DOCUMENT where DOCUMENT_TYPE=2 and SERVICE_ID=4 and STATUS_ID=1 order by 2")
                GF.ComboFill(cmbDocTransaction, DT, 0, 1)

                DT = GF.GetQueryResult("select Department_ID,department_name from DEPARTMENT_MASTER where Department_ID in (select Department_ID from EMP_MASTER where Status_ID = 1) order by Department_Name")
                GF.ComboFill(cmbExecuteDept, DT, 0, 1)
                Dim Dept As Integer = 0
                If DT.Rows.Count > 0 Then
                    Dept = CInt(Me.cmbExecuteDept.SelectedValue)
                End If
                DT = GF.GetQueryResult("select emp_code,emp_name + ' -- ' + convert(varchar(10),emp_code) from EMP_MASTER where Department_ID = " & Dept & " order by emp_name")
                GF.ComboFill(cmbExecuteBy, DT, 0, 1)

                DT = GF.GetQueryResult("select Department_ID,department_name from DEPARTMENT_MASTER where Department_ID in (select Department_ID from EMP_MASTER where Status_ID = 1) order by Department_Name")
                GF.ComboFill(cmbExeDept, DT, 0, 1)
                Dim Depart As Integer = 0
                If DT.Rows.Count > 0 Then
                    Depart = CInt(Me.cmbExeDept.SelectedValue)
                End If
                DT = GF.GetQueryResult("select emp_code,emp_name + ' -- ' + convert(varchar(10),emp_code) from EMP_MASTER where Department_ID = " & Depart & " order by emp_name")
                GF.ComboFill(cmbExeBy, DT, 0, 1)

            End If
            Me.cmbService.Attributes.Add("onchange", "return ServiceOnChange()")
            Me.chkVet.Attributes.Add("onclick", "return AddMoreImages()")
            Me.cmbExecuteDept.Attributes.Add("onchange", "return DeptOnChange(2)")
            Me.cmbExeDept.Attributes.Add("onchange", "return DeptOnChange(3)")
            Me.btnSave.Attributes.Add("onclick", "return SaveOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Select Case CInt(Data(0))
            Case 1 'Department Change
                Dim DepartmentID As Integer = CInt(Data(1))
                Dim Value As Integer = CInt(Data(2))
                DT = GF.GetQueryResult("select emp_code,emp_name + ' -- ' + convert(varchar(10),emp_code) from EMP_MASTER where Department_ID = " & DepartmentID & " order by emp_name")
                CallBackReturn = ""
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                Next
                CallBackReturn += "¶" + Value.ToString()
        End Select
    End Sub
#End Region

#Region "Save"
    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim RequestID As Integer = 0
            Dim IsHod As Integer = CInt(Me.hdnIsHOD.Value)
            Dim UserID As Integer = CInt(Session("UserID"))
            DT = GF.GetQueryResult("select Department_ID from EMP_MASTER where Emp_Code = " & UserID & "")
            Dim DepartmentID As Integer = CInt(DT.Rows(0)(0))

            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files

            For i = 0 To hfc.Count - 1
                Dim myFile As HttpPostedFile = hfc(i)
                Dim nFileLen As Integer = myFile.ContentLength
                Dim FileName As String = ""
                If (nFileLen > 0) Then
                    ContentType = myFile.ContentType
                    FileName = myFile.FileName
                    Dim FileLength As Integer = CInt(myFile.ContentLength) 'Convert into bytes
                    If FileLength > 4000000 Then
                        Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                        cl_srpt1.Append("alert('Please Check the Size of attached file');")
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                        Exit Sub
                    End If
                    Dim FileExtension As String = Path.GetExtension(myFile.FileName)
                    If Not (FileExtension = ".xls" Or FileExtension = ".xlsx" Or FileExtension = ".jpg" Or FileExtension = ".jpeg" Or FileExtension = ".doc" Or FileExtension = ".docx" Or FileExtension = ".zip" Or FileExtension = ".pdf" Or FileExtension = ".PDF" Or FileExtension = ".XLS" Or FileExtension = ".XLSX" Or FileExtension = ".JPG" Or FileExtension = ".JPEG" Or FileExtension = ".DOC" Or FileExtension = ".DOCX" Or FileExtension = ".ZIP") Then
                        Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                        cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                        Exit Sub
                    End If
                End If
            Next

            Dim ServiceID As Integer = CInt(Me.cmbService.SelectedValue)
            If ServiceID = 1 Then 'Drafting
                Dim ContType As String = ""
                Dim AttImg As Byte() = Nothing
                Dim DraftID As Integer = CInt(Me.cmbDraftDoc.SelectedValue)
                Dim Statement As String = CStr(Me.txtStmtFacts.Text)
                Try

                    Dim Params(8) As SqlParameter
                    Params(0) = New SqlParameter("@ServiceID", SqlDbType.Int)
                    Params(0).Value = ServiceID
                    Params(1) = New SqlParameter("@DraftID", SqlDbType.Int)
                    Params(1).Value = DraftID
                    Params(2) = New SqlParameter("@Statement", SqlDbType.VarChar, 1000)
                    Params(2).Value = Statement
                    Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(3).Value = UserID
                    Params(4) = New SqlParameter("@DeptID", SqlDbType.Int)
                    Params(4).Value = DepartmentID
                    Params(5) = New SqlParameter("@IsHOD", SqlDbType.Int)
                    Params(5).Value = IsHod
                    Params(6) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(7).Direction = ParameterDirection.Output
                    Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(8).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_REQUEST_DRAFTING", Params)
                    ErrorFlag = CInt(Params(7).Value)
                    Message = CStr(Params(8).Value)
                    RequestID = CInt(Params(6).Value)

                    If RequestID > 0 And ErrorFlag = 0 And hfc.Count > 0 Then
                        For i = 0 To hfc.Count - 1
                            Dim myFile As HttpPostedFile = hfc(i)
                            Dim nFileLen As Integer = myFile.ContentLength
                            Dim FileName As String = ""
                            If (nFileLen > 0) Then
                                ContentType = myFile.ContentType
                                FileName = myFile.FileName
                                AttachImg = New Byte(nFileLen - 1) {}
                                myFile.InputStream.Read(AttachImg, 0, nFileLen)

                                Dim Param(6) As SqlParameter
                                Param(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                                Param(0).Value = RequestID
                                Param(1) = New SqlParameter("@ServiceID", SqlDbType.Int)
                                Param(1).Value = 1
                                Param(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                                Param(2).Value = AttachImg
                                Param(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                                Param(3).Value = ContentType
                                Param(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                                Param(4).Value = FileName
                                Param(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                                Param(5).Direction = ParameterDirection.Output
                                Param(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                                Param(6).Direction = ParameterDirection.Output
                                DB.ExecuteNonQuery("SP_VET_REQUEST_ATTACHMENT", Param)
                            End If
                        Next
                    End If
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
            ElseIf ServiceID = 2 Then 'Opinion
                Dim Opinion As String = CStr(Me.txtOpinion.Text)
                Dim Statement As String = CStr(Me.txtStmtFacts.Text)
                Try
                    Dim Params(8) As SqlParameter
                    Params(0) = New SqlParameter("@ServiceID", SqlDbType.Int)
                    Params(0).Value = ServiceID
                    Params(1) = New SqlParameter("@Opinions", SqlDbType.VarChar, 1000)
                    Params(1).Value = Opinion
                    Params(2) = New SqlParameter("@Statement", SqlDbType.VarChar, 1000)
                    Params(2).Value = Statement
                    Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(3).Value = UserID
                    Params(4) = New SqlParameter("@DeptID", SqlDbType.Int)
                    Params(4).Value = DepartmentID
                    Params(5) = New SqlParameter("@IsHOD", SqlDbType.Int)
                    Params(5).Value = IsHod
                    Params(6) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(7).Direction = ParameterDirection.Output
                    Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(8).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_REQUEST_OPINION", Params)
                    ErrorFlag = CInt(Params(7).Value)
                    Message = CStr(Params(8).Value)
                    RequestID = CInt(Params(6).Value)

                    If RequestID > 0 And ErrorFlag = 0 And hfc.Count > 0 Then
                        For i = 0 To hfc.Count - 1
                            Dim myFile As HttpPostedFile = hfc(i)
                            Dim nFileLen As Integer = myFile.ContentLength
                            Dim FileName As String = ""
                            If (nFileLen > 0) Then
                                ContentType = myFile.ContentType
                                FileName = myFile.FileName
                                AttachImg = New Byte(nFileLen - 1) {}
                                myFile.InputStream.Read(AttachImg, 0, nFileLen)

                                Dim Param(6) As SqlParameter
                                Param(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                                Param(0).Value = RequestID
                                Param(1) = New SqlParameter("@ServiceID", SqlDbType.Int)
                                Param(1).Value = 2
                                Param(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                                Param(2).Value = AttachImg
                                Param(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                                Param(3).Value = ContentType
                                Param(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                                Param(4).Value = FileName
                                Param(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                                Param(5).Direction = ParameterDirection.Output
                                Param(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                                Param(6).Direction = ParameterDirection.Output
                                DB.ExecuteNonQuery("SP_VET_REQUEST_ATTACHMENT", Param)
                            End If
                        Next
                    End If
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
            ElseIf ServiceID = 3 Then 'Comments o documents
                Dim CommntDocID As Integer = CInt(Me.cmbNaturDoc.SelectedValue)
                Dim Purpose As String = CStr(Me.txtPurpose.Text)
                Dim Expectation As String = CStr(Me.txtExpectation.Text)
                Try
                    Dim Params(9) As SqlParameter
                    Params(0) = New SqlParameter("@ServiceID", SqlDbType.Int)
                    Params(0).Value = ServiceID
                    Params(1) = New SqlParameter("@ComDocID", SqlDbType.Int)
                    Params(1).Value = CommntDocID
                    Params(2) = New SqlParameter("@Purpose", SqlDbType.VarChar, 1000)
                    Params(2).Value = Purpose
                    Params(3) = New SqlParameter("@Expect", SqlDbType.VarChar, 1000)
                    Params(3).Value = Expectation
                    Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(4).Value = UserID
                    Params(5) = New SqlParameter("@DeptID", SqlDbType.Int)
                    Params(5).Value = DepartmentID
                    Params(6) = New SqlParameter("@IsHOD", SqlDbType.Int)
                    Params(6).Value = IsHod
                    Params(7) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(7).Direction = ParameterDirection.Output
                    Params(8) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(8).Direction = ParameterDirection.Output
                    Params(9) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(9).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_REQUEST_COMMENTS", Params)
                    ErrorFlag = CInt(Params(8).Value)
                    Message = CStr(Params(9).Value)
                    RequestID = CInt(Params(7).Value)

                    If RequestID > 0 And ErrorFlag = 0 And hfc.Count > 0 Then
                        For i = 0 To hfc.Count - 1
                            Dim myFile As HttpPostedFile = hfc(i)
                            Dim nFileLen As Integer = myFile.ContentLength
                            Dim FileName As String = ""
                            If (nFileLen > 0) Then
                                ContentType = myFile.ContentType
                                FileName = myFile.FileName
                                AttachImg = New Byte(nFileLen - 1) {}
                                myFile.InputStream.Read(AttachImg, 0, nFileLen)

                                Dim Param(6) As SqlParameter
                                Param(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                                Param(0).Value = RequestID
                                Param(1) = New SqlParameter("@ServiceID", SqlDbType.Int)
                                Param(1).Value = 3
                                Param(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                                Param(2).Value = AttachImg
                                Param(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                                Param(3).Value = ContentType
                                Param(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                                Param(4).Value = FileName
                                Param(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                                Param(5).Direction = ParameterDirection.Output
                                Param(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                                Param(6).Direction = ParameterDirection.Output
                                DB.ExecuteNonQuery("SP_VET_REQUEST_ATTACHMENT", Param)
                            End If
                        Next
                    End If
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
            ElseIf ServiceID = 4 Then 'Vetting for executing transactions
                Dim PartyID As Integer = CInt(Me.cmbParty.SelectedValue)
                Dim Statement As String = CStr(Me.txtStmtFacts.Text)

                ContentType = ""
                AttachImg = Nothing
                Dim myFile As HttpPostedFile = fupResolution.PostedFile
                Dim nFileLen As Integer = myFile.ContentLength
                If (nFileLen > 0) Then
                    ContentType = myFile.ContentType
                    AttachImg = New Byte(nFileLen - 1) {}
                    myFile.InputStream.Read(AttachImg, 0, nFileLen)
                End If

                Dim NatureTran As Integer = CInt(Me.cmbTransaction.SelectedValue)
                Dim VetDtl As String() = CStr(Me.hdnVetDtl.Value).Split(CChar("¶"))
                If CInt(VetDtl.Length) > 0 Then
                    Try
                        Dim Params(10) As SqlParameter
                        Params(0) = New SqlParameter("@ServiceID", SqlDbType.Int)
                        Params(0).Value = ServiceID
                        Params(1) = New SqlParameter("@PartyID", SqlDbType.Int)
                        Params(1).Value = PartyID
                        Params(2) = New SqlParameter("@Statement", SqlDbType.VarChar, 1000)
                        Params(2).Value = Statement
                        Params(3) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
                        Params(3).Value = AttachImg
                        Params(4) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                        Params(4).Value = ContentType
                        Params(5) = New SqlParameter("@NatureID", SqlDbType.Int)
                        Params(5).Value = NatureTran
                        Params(6) = New SqlParameter("@UserID", SqlDbType.Int)
                        Params(6).Value = UserID
                        Params(7) = New SqlParameter("@DeptID", SqlDbType.Int)
                        Params(7).Value = DepartmentID
                        Params(8) = New SqlParameter("@SendID", SqlDbType.Int)
                        Params(8).Value = 0
                        Params(9) = New SqlParameter("@RequestID", SqlDbType.Int)
                        Params(9).Direction = ParameterDirection.Output
                        Params(10) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(10).Direction = ParameterDirection.Output
                        Params(11) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(11).Direction = ParameterDirection.Output
                        DB.ExecuteNonQuery("SP_VET_REQUEST_VET", Params)
                        ErrorFlag = CInt(Params(10).Value)
                        Message = CStr(Params(11).Value)

                        RequestID = CInt(Params(9).Value)
                        Dim DocumentDtl As String = CStr(Me.hdnVetDtl.Value)

                        Dim DataLen As Integer = VetDtl.Length

                        If RequestID > 0 And ErrorFlag = 0 Then
                            For j = 0 To DataLen - 2
                                ContentType = ""
                                AttachImg = Nothing
                                Dim FileName As String = ""
                                Dim VetDocument As String() = VetDtl(j).Split(CChar("®"))
                                Dim DocID As Integer = CInt(VetDocument(0))
                                Dim VetFlag As Integer
                                If (VetDocument(2) = "") Then
                                    VetFlag = 0
                                Else
                                    VetFlag = 1
                                End If
                                Dim ExecuteDept As Integer = CInt(VetDocument(3))
                                Dim ExecuteBy As Integer = CInt(VetDocument(4))

                                Dim Param(9) As SqlParameter
                                Param(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                                Param(0).Value = RequestID
                                Param(1) = New SqlParameter("@DocID", SqlDbType.Int)
                                Param(1).Value = DocID
                                Param(2) = New SqlParameter("@VetFlag", SqlDbType.Int)
                                Param(2).Value = VetFlag
                                Param(3) = New SqlParameter("@ExecuteDept", SqlDbType.Int)
                                Param(3).Value = ExecuteDept
                                Param(4) = New SqlParameter("@ExecuteBy", SqlDbType.Int)
                                Param(4).Value = ExecuteBy
                                For i = 0 To hfc.Count - 1
                                    Dim myFle As HttpPostedFile = hfc(i)
                                    Dim nFileLe As Integer = myFle.ContentLength
                                    If (nFileLe > 0) Then
                                        FileName = myFle.FileName
                                        If (FileName = VetDocument(2)) Then
                                            ContentType = myFle.ContentType
                                            AttachImg = New Byte(nFileLe - 1) {}
                                            myFle.InputStream.Read(AttachImg, 0, nFileLe)
                                        End If
                                    End If
                                Next
                                Param(5) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                                Param(5).Value = AttachImg
                                Param(6) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                                Param(6).Value = ContentType
                                Param(7) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                                Param(7).Value = VetDocument(2)
                                Param(8) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                                Param(8).Direction = ParameterDirection.Output
                                Param(9) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                                Param(9).Direction = ParameterDirection.Output
                                DB.ExecuteNonQuery("SP_VET_REQUEST_DOCUMENT", Param)
                                ErrorFlag = CInt(Param(8).Value)
                                Message = CStr(Param(9).Value)
                            Next
                        End If
                    Catch ex As Exception
                        Message = ex.Message.ToString
                        ErrorFlag = 1
                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                    End Try
                End If
            ElseIf ServiceID = 5 Then 'Archiving of executed documents
                Dim PartyID As Integer = CInt(Me.cmbParty.SelectedValue)
                Dim ArchDoc As Integer = CInt(Me.cmbDocType.SelectedValue)
                Dim NatureTrans As Integer = CInt(Me.cmbTransaction.SelectedValue)
                Dim ExecuteDt As Date = CDate(Me.txtExecuteDt.Text)
                Dim ExecuteDept As Integer = CInt(Me.cmbExeDept.SelectedValue)
                Dim ExecuteBy As Integer = CInt(Me.hdnExecutant.Value)
                Dim Reason As String = CStr(Me.txtNotVet.Text)

                Try
                    Dim Params(13) As SqlParameter
                    Params(0) = New SqlParameter("@ServiceID", SqlDbType.Int)
                    Params(0).Value = ServiceID
                    Params(1) = New SqlParameter("@PartyID", SqlDbType.Int)
                    Params(1).Value = PartyID
                    Params(2) = New SqlParameter("@ArchDocID", SqlDbType.Int)
                    Params(2).Value = ArchDoc
                    Params(3) = New SqlParameter("@NatureID", SqlDbType.Int)
                    Params(3).Value = NatureTrans
                    Params(4) = New SqlParameter("@ExecuteDt", SqlDbType.Date)
                    Params(4).Value = ExecuteDt
                    Params(5) = New SqlParameter("@ExecuteDept", SqlDbType.Int)
                    Params(5).Value = ExecuteDept
                    Params(6) = New SqlParameter("@ExecuteBy", SqlDbType.Int)
                    Params(6).Value = ExecuteBy
                    Params(7) = New SqlParameter("@Reason", SqlDbType.VarChar, 1000)
                    Params(7).Value = Reason
                    Params(8) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(8).Value = UserID
                    Params(9) = New SqlParameter("@DeptID", SqlDbType.Int)
                    Params(9).Value = DepartmentID
                    Params(10) = New SqlParameter("@IsHOD", SqlDbType.Int)
                    Params(10).Value = IsHod
                    Params(11) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(11).Direction = ParameterDirection.Output
                    Params(12) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(12).Direction = ParameterDirection.Output
                    Params(13) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(13).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_REQUEST_ARCHIVE", Params)
                    ErrorFlag = CInt(Params(12).Value)
                    Message = CStr(Params(13).Value)
                    RequestID = CInt(Params(11).Value)

                    If RequestID > 0 And ErrorFlag = 0 And hfc.Count > 0 Then
                        For i = 0 To hfc.Count - 1
                            Dim myFile As HttpPostedFile = hfc(i)
                            Dim nFileLen As Integer = myFile.ContentLength
                            Dim FileName As String = ""
                            If (nFileLen > 0) Then
                                ContentType = myFile.ContentType
                                FileName = myFile.FileName
                                AttachImg = New Byte(nFileLen - 1) {}
                                myFile.InputStream.Read(AttachImg, 0, nFileLen)

                                Dim Param(6) As SqlParameter
                                Param(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                                Param(0).Value = RequestID
                                Param(1) = New SqlParameter("@ServiceID", SqlDbType.Int)
                                Param(1).Value = 5
                                Param(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                                Param(2).Value = AttachImg
                                Param(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                                Param(3).Value = ContentType
                                Param(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                                Param(4).Value = FileName
                                Param(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                                Param(5).Direction = ParameterDirection.Output
                                Param(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                                Param(6).Direction = ParameterDirection.Output
                                DB.ExecuteNonQuery("SP_VET_REQUEST_ATTACHMENT", Param)
                            End If
                        Next
                    End If
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
            End If
            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("alert('" + Message + "');")
            cl_script1.Append("window.open('NewRequest.aspx','_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Higher Approve"
    Protected Sub btnHigher_Click(sender As Object, e As System.EventArgs) Handles btnHigher.Click
        Try
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim RequestID As Integer = 0
            Dim UserID As Integer = CInt(Session("UserID"))
            DT = GF.GetQueryResult("select Department_ID from EMP_MASTER where Emp_Code = " & UserID & "")
            Dim DepartmentID As Integer = CInt(DT.Rows(0)(0))

            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files

            For i = 0 To hfc.Count - 1
                Dim myFile As HttpPostedFile = hfc(i)
                Dim nFileLen As Integer = myFile.ContentLength
                Dim FileName As String = ""
                If (nFileLen > 0) Then
                    ContentType = myFile.ContentType
                    FileName = myFile.FileName
                    Dim FileLength As Integer = CInt(myFile.ContentLength) 'Convert into bytes
                    If FileLength > 4000000 Then
                        Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                        cl_srpt1.Append("alert('Please Check the Size of attached file');")
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                        Exit Sub
                    End If
                    Dim FileExtension As String = Path.GetExtension(myFile.FileName)
                    If Not (FileExtension = ".xls" Or FileExtension = ".xlsx" Or FileExtension = ".jpg" Or FileExtension = ".jpeg" Or FileExtension = ".doc" Or FileExtension = ".docx" Or FileExtension = ".zip" Or FileExtension = ".pdf" Or FileExtension = ".PDF" Or FileExtension = ".XLS" Or FileExtension = ".XLSX" Or FileExtension = ".JPG" Or FileExtension = ".JPEG" Or FileExtension = ".DOC" Or FileExtension = ".DOCX" Or FileExtension = ".ZIP") Then
                        Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                        cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                        Exit Sub
                    End If
                End If
            Next

            Dim ServiceID As Integer = CInt(Me.cmbService.SelectedValue)
            If ServiceID = 4 Then 'Vetting for executing transactions
                Dim PartyID As Integer = CInt(Me.cmbParty.SelectedValue)
                Dim Statement As String = CStr(Me.txtStmtFacts.Text)

                ContentType = ""
                AttachImg = Nothing
                Dim myFile As HttpPostedFile = fupResolution.PostedFile
                Dim nFileLen As Integer = myFile.ContentLength
                If (nFileLen > 0) Then
                    ContentType = myFile.ContentType
                    AttachImg = New Byte(nFileLen - 1) {}
                    myFile.InputStream.Read(AttachImg, 0, nFileLen)
                End If

                Dim NatureTran As Integer = CInt(Me.cmbTransaction.SelectedValue)
                Dim VetDtl As String() = CStr(Me.hdnVetDtl.Value).Split(CChar("¶"))
                If CInt(VetDtl.Length) > 1 Then
                    Try
                        Dim Params(11) As SqlParameter
                        Params(0) = New SqlParameter("@ServiceID", SqlDbType.Int)
                        Params(0).Value = ServiceID
                        Params(1) = New SqlParameter("@PartyID", SqlDbType.Int)
                        Params(1).Value = PartyID
                        Params(2) = New SqlParameter("@Statement", SqlDbType.VarChar, 1000)
                        Params(2).Value = Statement
                        Params(3) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
                        Params(3).Value = AttachImg
                        Params(4) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                        Params(4).Value = ContentType
                        Params(5) = New SqlParameter("@NatureID", SqlDbType.Int)
                        Params(5).Value = NatureTran
                        Params(6) = New SqlParameter("@UserID", SqlDbType.Int)
                        Params(6).Value = UserID
                        Params(7) = New SqlParameter("@DeptID", SqlDbType.Int)
                        Params(7).Value = DepartmentID
                        Params(8) = New SqlParameter("@SendID", SqlDbType.Int)
                        Params(8).Value = 0
                        Params(9) = New SqlParameter("@RequestID", SqlDbType.Int)
                        Params(9).Direction = ParameterDirection.Output
                        Params(10) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(10).Direction = ParameterDirection.Output
                        Params(11) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(11).Direction = ParameterDirection.Output
                        DB.ExecuteNonQuery("SP_VET_REQUEST_VET", Params)
                        ErrorFlag = CInt(Params(10).Value)
                        Message = CStr(Params(11).Value)

                        RequestID = CInt(Params(9).Value)
                        Dim DocumentDtl As String = CStr(Me.hdnVetDtl.Value)

                        Dim DataLen As Integer = VetDtl.Length

                        If RequestID > 0 And ErrorFlag = 0 Then
                            For j = 0 To DataLen - 2
                                ContentType = ""
                                AttachImg = Nothing
                                Dim FileName As String = ""
                                Dim VetDocument As String() = VetDtl(j).Split(CChar("®"))
                                Dim DocID As Integer = CInt(VetDocument(0))
                                Dim VetFlag As Integer
                                If (VetDocument(2) = "") Then
                                    VetFlag = 0
                                Else
                                    VetFlag = 1
                                End If
                                Dim ExecuteDept As Integer = CInt(VetDocument(3))
                                Dim ExecuteBy As Integer = CInt(VetDocument(4))

                                Dim Param(9) As SqlParameter
                                Param(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                                Param(0).Value = RequestID
                                Param(1) = New SqlParameter("@DocID", SqlDbType.Int)
                                Param(1).Value = DocID
                                Param(2) = New SqlParameter("@VetFlag", SqlDbType.Int)
                                Param(2).Value = VetFlag
                                Param(3) = New SqlParameter("@ExecuteDept", SqlDbType.Int)
                                Param(3).Value = ExecuteDept
                                Param(4) = New SqlParameter("@ExecuteBy", SqlDbType.Int)
                                Param(4).Value = ExecuteBy
                                For i = 0 To hfc.Count - 1
                                    Dim myFle As HttpPostedFile = hfc(i)
                                    Dim nFileLe As Integer = myFle.ContentLength
                                    If (nFileLe > 0) Then
                                        FileName = myFle.FileName
                                        If (FileName = VetDocument(2)) Then
                                            ContentType = myFle.ContentType
                                            AttachImg = New Byte(nFileLe - 1) {}
                                            myFle.InputStream.Read(AttachImg, 0, nFileLe)
                                        End If
                                    End If
                                Next
                                Param(5) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                                Param(5).Value = AttachImg
                                Param(6) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                                Param(6).Value = ContentType
                                Param(7) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                                Param(7).Value = VetDocument(2)
                                Param(8) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                                Param(8).Direction = ParameterDirection.Output
                                Param(9) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                                Param(9).Direction = ParameterDirection.Output
                                DB.ExecuteNonQuery("SP_VET_REQUEST_DOCUMENT", Param)
                                ErrorFlag = CInt(Param(8).Value)
                                Message = CStr(Param(9).Value)
                            Next
                        End If
                    Catch ex As Exception
                        Message = ex.Message.ToString
                        ErrorFlag = 1
                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                    End Try
                    Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_script1.Append("alert('" + Message + "');")
                    cl_script1.Append("window.open('NewRequest.aspx','_self');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
                Else
                    Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_script1.Append("alert('Please Add Vet / Execute Details');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
                End If
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Send To Legal"
    Protected Sub btnLegal_Click(sender As Object, e As System.EventArgs) Handles btnLegal.Click
        Try
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim RequestID As Integer = 0
            Dim UserID As Integer = CInt(Session("UserID"))
            DT = GF.GetQueryResult("select Department_ID from EMP_MASTER where Emp_Code = " & UserID & "")
            Dim DepartmentID As Integer = CInt(DT.Rows(0)(0))

            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files

            For i = 0 To hfc.Count - 1
                Dim myFile As HttpPostedFile = hfc(i)
                Dim nFileLen As Integer = myFile.ContentLength
                Dim FileName As String = ""
                If (nFileLen > 0) Then
                    ContentType = myFile.ContentType
                    FileName = myFile.FileName
                    Dim FileLength As Integer = CInt(myFile.ContentLength) 'Convert into bytes
                    If FileLength > 4000000 Then
                        Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                        cl_srpt1.Append("alert('Please Check the Size of attached file');")
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                        Exit Sub
                    End If
                    Dim FileExtension As String = Path.GetExtension(myFile.FileName)
                    If Not (FileExtension = ".xls" Or FileExtension = ".xlsx" Or FileExtension = ".jpg" Or FileExtension = ".jpeg" Or FileExtension = ".doc" Or FileExtension = ".docx" Or FileExtension = ".zip" Or FileExtension = ".pdf" Or FileExtension = ".PDF" Or FileExtension = ".XLS" Or FileExtension = ".XLSX" Or FileExtension = ".JPG" Or FileExtension = ".JPEG" Or FileExtension = ".DOC" Or FileExtension = ".DOCX" Or FileExtension = ".ZIP") Then
                        Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                        cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                        Exit Sub
                    End If
                End If
            Next

            Dim ServiceID As Integer = CInt(Me.cmbService.SelectedValue)
            If ServiceID = 4 Then 'Vetting for executing transactions
                Dim PartyID As Integer = CInt(Me.cmbParty.SelectedValue)
                Dim Statement As String = CStr(Me.txtStmtFacts.Text)

                ContentType = ""
                AttachImg = Nothing
                Dim myFile As HttpPostedFile = fupResolution.PostedFile
                Dim nFileLen As Integer = myFile.ContentLength
                If (nFileLen > 0) Then
                    ContentType = myFile.ContentType
                    AttachImg = New Byte(nFileLen - 1) {}
                    myFile.InputStream.Read(AttachImg, 0, nFileLen)
                End If

                Dim NatureTran As Integer = CInt(Me.cmbTransaction.SelectedValue)
                Dim VetDtl As String() = CStr(Me.hdnVetDtl.Value).Split(CChar("¶"))
                If CInt(VetDtl.Length) > 1 Then
                    Try
                        Dim Params(11) As SqlParameter
                        Params(0) = New SqlParameter("@ServiceID", SqlDbType.Int)
                        Params(0).Value = ServiceID
                        Params(1) = New SqlParameter("@PartyID", SqlDbType.Int)
                        Params(1).Value = PartyID
                        Params(2) = New SqlParameter("@Statement", SqlDbType.VarChar, 1000)
                        Params(2).Value = Statement
                        Params(3) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
                        Params(3).Value = AttachImg
                        Params(4) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                        Params(4).Value = ContentType
                        Params(5) = New SqlParameter("@NatureID", SqlDbType.Int)
                        Params(5).Value = NatureTran
                        Params(6) = New SqlParameter("@UserID", SqlDbType.Int)
                        Params(6).Value = UserID
                        Params(7) = New SqlParameter("@DeptID", SqlDbType.Int)
                        Params(7).Value = DepartmentID
                        Params(8) = New SqlParameter("@SendID", SqlDbType.Int)
                        Params(8).Value = 1
                        Params(9) = New SqlParameter("@RequestID", SqlDbType.Int)
                        Params(9).Direction = ParameterDirection.Output
                        Params(10) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(10).Direction = ParameterDirection.Output
                        Params(11) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(11).Direction = ParameterDirection.Output
                        DB.ExecuteNonQuery("SP_VET_REQUEST_VET", Params)
                        ErrorFlag = CInt(Params(10).Value)
                        Message = CStr(Params(11).Value)

                        RequestID = CInt(Params(9).Value)
                        Dim DocumentDtl As String = CStr(Me.hdnVetDtl.Value)

                        Dim DataLen As Integer = VetDtl.Length

                        If RequestID > 0 And ErrorFlag = 0 Then
                            For j = 0 To DataLen - 2
                                ContentType = ""
                                AttachImg = Nothing
                                Dim FileName As String = ""
                                Dim VetDocument As String() = VetDtl(j).Split(CChar("®"))
                                Dim DocID As Integer = CInt(VetDocument(0))
                                Dim VetFlag As Integer
                                If (VetDocument(2) = "") Then
                                    VetFlag = 0
                                Else
                                    VetFlag = 1
                                End If
                                Dim ExecuteDept As Integer = CInt(VetDocument(3))
                                Dim ExecuteBy As Integer = CInt(VetDocument(4))

                                Dim Param(9) As SqlParameter
                                Param(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                                Param(0).Value = RequestID
                                Param(1) = New SqlParameter("@DocID", SqlDbType.Int)
                                Param(1).Value = DocID
                                Param(2) = New SqlParameter("@VetFlag", SqlDbType.Int)
                                Param(2).Value = VetFlag
                                Param(3) = New SqlParameter("@ExecuteDept", SqlDbType.Int)
                                Param(3).Value = ExecuteDept
                                Param(4) = New SqlParameter("@ExecuteBy", SqlDbType.Int)
                                Param(4).Value = ExecuteBy
                                For i = 0 To hfc.Count - 1
                                    Dim myFle As HttpPostedFile = hfc(i)
                                    Dim nFileLe As Integer = myFle.ContentLength
                                    If (nFileLe > 0) Then
                                        FileName = myFle.FileName
                                        If (FileName = VetDocument(2)) Then
                                            ContentType = myFle.ContentType
                                            AttachImg = New Byte(nFileLe - 1) {}
                                            myFle.InputStream.Read(AttachImg, 0, nFileLe)
                                        End If
                                    End If
                                Next
                                Param(5) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
                                Param(5).Value = AttachImg
                                Param(6) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                                Param(6).Value = ContentType
                                Param(7) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                                Param(7).Value = VetDocument(2)
                                Param(8) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                                Param(8).Direction = ParameterDirection.Output
                                Param(9) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                                Param(9).Direction = ParameterDirection.Output
                                DB.ExecuteNonQuery("SP_VET_REQUEST_DOCUMENT", Param)
                                ErrorFlag = CInt(Param(8).Value)
                                Message = CStr(Param(9).Value)
                            Next
                        End If
                    Catch ex As Exception
                        Message = ex.Message.ToString
                        ErrorFlag = 1
                        Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                    End Try
                    Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_script1.Append("alert('" + Message + "');")
                    cl_script1.Append("window.open('NewRequest.aspx','_self');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
                Else
                    Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_script1.Append("alert('Please Add Vet / Execute Details');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
                End If
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
End Class
