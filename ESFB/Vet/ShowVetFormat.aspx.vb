﻿Imports System.Data
Partial Class Leave_ShowVetFormat
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As DataTable
            Dim PkID As Integer = Convert.ToInt32(Request.QueryString.[Get]("PkID"))
            Dim VetID As Integer = Convert.ToInt32(Request.QueryString.[Get]("VetID"))
            Dim DocID As Integer = Convert.ToInt32(Request.QueryString.[Get]("DocID"))

            If VetID = 0 And DocID = 0 Then
                DT = DB.ExecuteDataSet("select EXE_ATTACHMENT,CONTENT_TYPE,FILE_NAME from DMS_ESFB.dbo.VET_EXECUTE_ATTACHMENT where PkId=" & PkID & "").Tables(0)
            ElseIf PkID = 0 Then
                DT = DB.ExecuteDataSet("SELECT DOC_ATTACHMENT,CONTENT_TYPE,DOCFILE_NAME FROM DMS_ESFB.dbo.VET_TRANSDOC_ATTACHMENT where VET_ID=" & VetID & "").Tables(0)
            Else
                DT = DB.ExecuteDataSet("SELECT DOC_ATTACHMENT,CONTENT_TYPE,DOCFILE_NAME FROM DMS_ESFB.dbo.VET_TRANSDOC_ATTACHMENT where PkId=" & PkID & " and VET_ID=" & VetID & "").Tables(0)
            End If

            If DT IsNot Nothing Then
                Dim bytes() As Byte = CType(DT.Rows(0)(0), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = DT.Rows(0)(1).ToString()
                Response.AddHeader("content-disposition", "attachment;filename=" + DT.Rows(0)(2).ToString().Replace(" ", ""))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
