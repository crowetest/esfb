﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class VetArchiveRpt2
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim TB As New Table
    Dim ServiceID, NatureID, DocID, PartyID As Integer
    Dim NatureNAme, FromDT, ToDT As String
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            ServiceID = CInt(Request.QueryString.Get("ServiceID"))
            NatureID = CInt(Request.QueryString.Get("NatureID"))
            DocID = CInt(Request.QueryString.Get("DocID"))
            PartyID = CInt(Request.QueryString.Get("PartyID"))
            FromDT = CStr(Request.QueryString.Get("FromDT"))
            ToDT = CStr(Request.QueryString.Get("ToDT"))

            Dim DT_Service As New DataTable
            DT_Service = DB.ExecuteDataSet("select SERVICE_NAME from VET_SERVICE where SERVICE_ID=" & ServiceID & " ").Tables(0)
            TB.Attributes.Add("width", "100%")

            Dim RowBG As Integer = 0
            Dim DR As DataRow

            RH.Heading(CStr(Session("FirmName")), TB, "ARCHIVED DOCUMENTS", 100)
            RH.BlankRow(TB, 5)

            If ServiceID = 5 Then 'Archiving of executed document
                Dim DT_REQ, DT_APP As New DataTable

                If (DocID = -1 And PartyID = -1) Then
                    DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,n.NATURE_NAME,p.PARTY_NAME,v.EXECUTE_DT,UPPER(e.Emp_Name) executant,v.REASON_NOTVET,upper(r.Emp_Name) requestBy,v.REQUEST_DT,a.FILE_NAME,v.REQUEST_ID,a.PkId,UPPER(m.Emp_Name) ArchiveBy,v.VET_DT FROM VET_MASTER v, VET_DOCUMENT d, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER r, EMP_MASTER m, DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a WHERE v.ARCH_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_BY=r.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID = a.SERVICE_ID and v.REQUEST_ID=a.REQUEST_ID and d.DOCUMENT_TYPE=3 and v.STATUS_ID in(3,4) and v.SERVICE_ID=5 and v.NATURE_ID=" & NatureID & " and v.EXECUTE_DT between '" & CDate(FromDT).ToString("dd/MMM/yyyy") & "' and '" & CDate(ToDT).ToString("dd/MMM/yyyy") & "' order by 3").Tables(0)
                ElseIf (DocID = -1 And PartyID <> -1) Then
                    DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,n.NATURE_NAME,p.PARTY_NAME,v.EXECUTE_DT,UPPER(e.Emp_Name) executant,v.REASON_NOTVET,upper(r.Emp_Name) requestBy,v.REQUEST_DT,a.FILE_NAME,v.REQUEST_ID,a.PkId,UPPER(m.Emp_Name) ArchiveBy,v.VET_DT FROM VET_MASTER v, VET_DOCUMENT d, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER r, EMP_MASTER m, DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a WHERE v.ARCH_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_BY=r.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID = a.SERVICE_ID and v.REQUEST_ID=a.REQUEST_ID and d.DOCUMENT_TYPE=3 and v.STATUS_ID in(3,4) and v.SERVICE_ID=5 and v.NATURE_ID=" & NatureID & " and v.EXECUTE_DT between '" & CDate(FromDT).ToString("dd/MMM/yyyy") & "' and '" & CDate(ToDT).ToString("dd/MMM/yyyy") & "' and v.PARTY_ID=" & PartyID & " order by 3").Tables(0)
                ElseIf (DocID <> -1 And PartyID = -1) Then
                    DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,n.NATURE_NAME,p.PARTY_NAME,v.EXECUTE_DT,UPPER(e.Emp_Name) executant,v.REASON_NOTVET,upper(r.Emp_Name) requestBy,v.REQUEST_DT,a.FILE_NAME,v.REQUEST_ID,a.PkId,UPPER(m.Emp_Name) ArchiveBy,v.VET_DT FROM VET_MASTER v, VET_DOCUMENT d, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER r, EMP_MASTER m, DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a WHERE v.ARCH_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_BY=r.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID = a.SERVICE_ID and v.REQUEST_ID=a.REQUEST_ID and d.DOCUMENT_TYPE=3 and v.STATUS_ID in(3,4) and v.SERVICE_ID=5 and v.NATURE_ID=" & NatureID & " and v.ARCH_DOCID= " & DocID & " and v.EXECUTE_DT between '" & CDate(FromDT).ToString("dd/MMM/yyyy") & "' and '" & CDate(ToDT).ToString("dd/MMM/yyyy") & "' order by 3").Tables(0)
                ElseIf (DocID <> -1 And PartyID <> -1) Then
                    DT_APP = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,n.NATURE_NAME,p.PARTY_NAME,v.EXECUTE_DT,UPPER(e.Emp_Name) executant,v.REASON_NOTVET,upper(r.Emp_Name) requestBy,v.REQUEST_DT,a.FILE_NAME,v.REQUEST_ID,a.PkId,UPPER(m.Emp_Name) ArchiveBy,v.VET_DT FROM VET_MASTER v, VET_DOCUMENT d, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e, EMP_MASTER r, EMP_MASTER m, DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a WHERE v.ARCH_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_BY=r.Emp_Code and v.VET_BY=m.Emp_Code and v.SERVICE_ID = a.SERVICE_ID and v.REQUEST_ID=a.REQUEST_ID and d.DOCUMENT_TYPE=3 and v.STATUS_ID in(3,4) and v.SERVICE_ID=5 and v.NATURE_ID=" & NatureID & " and v.ARCH_DOCID= " & DocID & " and v.EXECUTE_DT between '" & CDate(FromDT).ToString("dd/MMM/yyyy") & "' and '" & CDate(ToDT).ToString("dd/MMM/yyyy") & "' and v.PARTY_ID=" & PartyID & " order by 3").Tables(0)
                End If
                If DT_APP.Rows.Count > 0 Then
                    NatureNAme = DT_APP.Rows(0)(1)
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "ARCHIVE THROUGH ARCHIVE ROUTE -" + NatureNAme.ToString())
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10, TRVET0_11 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"
                    TRVET0_09.BorderWidth = "1"
                    TRVET0_10.BorderWidth = "1"
                    TRVET0_11.BorderWidth = "1"

                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid
                    TRVET0_09.BorderStyle = BorderStyle.Solid
                    TRVET0_10.BorderStyle = BorderStyle.Solid
                    TRVET0_11.BorderStyle = BorderStyle.Solid

                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver
                    TRVET0_09.BorderColor = Drawing.Color.Silver
                    TRVET0_10.BorderColor = Drawing.Color.Silver
                    TRVET0_11.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Document")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Party")
                    RH.AddColumn(TRVET0, TRVET0_04, 5, 5, "l", "Execute Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 10, 10, "l", "Executant")
                    RH.AddColumn(TRVET0, TRVET0_06, 15, 15, "l", "Reason For Archive")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Request By")
                    RH.AddColumn(TRVET0, TRVET0_08, 5, 5, "l", "Request Date")
                    RH.AddColumn(TRVET0, TRVET0_09, 5, 5, "l", "Archive By")
                    RH.AddColumn(TRVET0, TRVET0_10, 5, 5, "l", "Archive Date")
                    RH.AddColumn(TRVET0, TRVET0_11, 10, 10, "l", "File To Archive")

                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_APP.Rows
                        Dim TRVET55 As New TableRow
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10, TRVET55_11 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"
                        TRVET55_09.BorderWidth = "1"
                        TRVET55_10.BorderWidth = "1"
                        TRVET55_11.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid
                        TRVET55_09.BorderStyle = BorderStyle.Solid
                        TRVET55_10.BorderStyle = BorderStyle.Solid
                        TRVET55_11.BorderStyle = BorderStyle.Solid

                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver
                        TRVET55_09.BorderColor = Drawing.Color.Silver
                        TRVET55_10.BorderColor = Drawing.Color.Silver
                        TRVET55_11.BorderColor = Drawing.Color.Silver

                        RequestID = DR(9)
                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 5, 5, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "l", "")
                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                            RH.AddColumn(TRVET55, TRVET55_04, 5, 5, "l", Format(DR(3), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_05, 10, 10, "l", DR(4))
                            RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", DR(5))
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                            RH.AddColumn(TRVET55, TRVET55_08, 5, 5, "l", Format(DR(7), "dd/MMM/yyyy"))
                            RH.AddColumn(TRVET55, TRVET55_09, 5, 5, "l", DR(11))
                            RH.AddColumn(TRVET55, TRVET55_10, 5, 5, "l", Format(DR(12), "dd/MMM/yyyy"))
                        End If


                        RH.AddColumn(TRVET55, TRVET55_11, 10, 10, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(9).ToString + "&Value=2 &Id=" + DR(10).ToString + "' style='text-align:right;' target='_blank' >" & DR(8) & "</a>")
                        NextRequest = RequestID
                        TB.Controls.Add(TRVET55)
                    Next
                End If
            ElseIf ServiceID = 4 Then
                Dim DT_REQ1, DT_APP1 As New DataTable

                If (DocID = -1 And PartyID = -1) Then
                    DT_APP1 = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,P.PARTY_NAME,upper(e.Emp_Name),v.EXECUTE_DT,a.DOCFILE_NAME,(select ex.FILE_NAME+'¶'+CONVERT(VARCHAR(10),ex.PkId) from DMS_ESFB.dbo.VET_EXECUTE_ATTACHMENT ex where	ex.VET_ID=v.VET_ID and ex.REQUEST_ID=v.REQUEST_ID),upper(emp.Emp_Name) REQUESTBY,VM.REQUEST_DT,a.PkId,v.VET_ID,v.DOC_ID,v.REQUEST_ID from VET_TRANSACTION_MASTER v, VET_DOCUMENT d, EMP_MASTER e,EMP_MASTER emp, DMS_ESFB.dbo.VET_TRANSDOC_ATTACHMENT a ,VET_MASTER VM ,VET_PARTY p where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and v.EXECUTE_BY=e.Emp_Code and v.VET_ID=a.VET_ID and v.REQUEST_ID=vm.REQUEST_ID and VM.SERVICE_ID=d.SERVICE_ID and Vm.NATURE_ID=" & NatureID & " and v.EXECUTE_DT between '" & CDate(FromDT).ToString("dd/MMM/yyyy") & "' and '" & CDate(ToDT).ToString("dd/MMM/yyyy") & "' and VM.STATUS_ID=15 and p.PARTY_ID =VM.PARTY_ID and VM.REQUEST_BY=emp.Emp_Code order by 2").Tables(0)
                ElseIf (DocID = -1 And PartyID <> -1) Then
                    DT_APP1 = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,P.PARTY_NAME,upper(e.Emp_Name),v.EXECUTE_DT,a.DOCFILE_NAME,(select ex.FILE_NAME+'¶'+CONVERT(VARCHAR(10),ex.PkId) from DMS_ESFB.dbo.VET_EXECUTE_ATTACHMENT ex where	ex.VET_ID=v.VET_ID and ex.REQUEST_ID=v.REQUEST_ID),upper(emp.Emp_Name) REQUESTBY,VM.REQUEST_DT,a.PkId,v.VET_ID,v.DOC_ID,v.REQUEST_ID from VET_TRANSACTION_MASTER v, VET_DOCUMENT d, EMP_MASTER e,EMP_MASTER emp, DMS_ESFB.dbo.VET_TRANSDOC_ATTACHMENT a ,VET_MASTER VM ,VET_PARTY p where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and v.EXECUTE_BY=e.Emp_Code and v.VET_ID=a.VET_ID and v.REQUEST_ID=vm.REQUEST_ID and VM.SERVICE_ID=d.SERVICE_ID and Vm.NATURE_ID=" & NatureID & " and v.EXECUTE_DT between '" & CDate(FromDT).ToString("dd/MMM/yyyy") & "' and '" & CDate(ToDT).ToString("dd/MMM/yyyy") & "' and vm.PARTY_ID=" & PartyID & " and VM.STATUS_ID=15 and p.PARTY_ID =VM.PARTY_ID and VM.REQUEST_BY=emp.Emp_Code order by 2").Tables(0)
                ElseIf (DocID <> -1 And PartyID = -1) Then
                    DT_APP1 = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,P.PARTY_NAME,upper(e.Emp_Name),v.EXECUTE_DT,a.DOCFILE_NAME,(select ex.FILE_NAME+'¶'+CONVERT(VARCHAR(10),ex.PkId) from DMS_ESFB.dbo.VET_EXECUTE_ATTACHMENT ex where	ex.VET_ID=v.VET_ID and ex.REQUEST_ID=v.REQUEST_ID),upper(emp.Emp_Name) REQUESTBY,VM.REQUEST_DT,a.PkId,v.VET_ID,v.DOC_ID,v.REQUEST_ID from VET_TRANSACTION_MASTER v, VET_DOCUMENT d, EMP_MASTER e,EMP_MASTER emp, DMS_ESFB.dbo.VET_TRANSDOC_ATTACHMENT a ,VET_MASTER VM ,VET_PARTY p where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and v.EXECUTE_BY=e.Emp_Code and v.VET_ID=a.VET_ID and v.REQUEST_ID=vm.REQUEST_ID and VM.SERVICE_ID=d.SERVICE_ID and Vm.NATURE_ID=" & NatureID & " and v.DOC_ID=" & DocID & " and v.EXECUTE_DT between '" & CDate(FromDT).ToString("dd/MMM/yyyy") & "' and '" & CDate(ToDT).ToString("dd/MMM/yyyy") & "' and VM.STATUS_ID=15 and p.PARTY_ID =VM.PARTY_ID and VM.REQUEST_BY=emp.Emp_Code order by 2").Tables(0)
                ElseIf (DocID <> -1 And PartyID <> -1) Then
                    DT_APP1 = DB.ExecuteDataSet("SELECT d.DOCUMENT_NAME,P.PARTY_NAME,upper(e.Emp_Name),v.EXECUTE_DT,a.DOCFILE_NAME,(select ex.FILE_NAME+'¶'+CONVERT(VARCHAR(10),ex.PkId) from DMS_ESFB.dbo.VET_EXECUTE_ATTACHMENT ex where	ex.VET_ID=v.VET_ID and ex.REQUEST_ID=v.REQUEST_ID),upper(emp.Emp_Name) REQUESTBY,VM.REQUEST_DT,a.PkId,v.VET_ID,v.DOC_ID,v.REQUEST_ID from VET_TRANSACTION_MASTER v, VET_DOCUMENT d, EMP_MASTER e,EMP_MASTER emp, DMS_ESFB.dbo.VET_TRANSDOC_ATTACHMENT a ,VET_MASTER VM ,VET_PARTY p where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and v.EXECUTE_BY=e.Emp_Code and v.VET_ID=a.VET_ID and v.REQUEST_ID=vm.REQUEST_ID and VM.SERVICE_ID=d.SERVICE_ID and Vm.NATURE_ID=" & NatureID & " and v.DOC_ID=" & DocID & " and v.EXECUTE_DT between '" & CDate(FromDT).ToString("dd/MMM/yyyy") & "' and '" & CDate(ToDT).ToString("dd/MMM/yyyy") & "' and vm.PARTY_ID=" & PartyID & " and VM.STATUS_ID=15 and p.PARTY_ID =VM.PARTY_ID and VM.REQUEST_BY=emp.Emp_Code order by 2").Tables(0)
                End If
                ' NatureNAme = DT_APP1.Rows(0)(1)

                If DT_APP1.Rows.Count > 0 Then
                    RH.BlankRow(TB, 5)
                    RH.SubHeading(TB, 100, "l", "ARCHIVE THROUGH VETTING ROUTE ")
                    RH.BlankRow(TB, 5)

                    Dim TRVET0 As New TableRow
                    TRVET0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07, TRVET0_08, TRVET0_09, TRVET0_10, TRVET0_11 As New TableCell

                    TRVET0_00.BorderWidth = "1"
                    TRVET0_01.BorderWidth = "1"
                    TRVET0_02.BorderWidth = "1"
                    TRVET0_03.BorderWidth = "1"
                    TRVET0_04.BorderWidth = "1"
                    TRVET0_05.BorderWidth = "1"
                    TRVET0_06.BorderWidth = "1"
                    TRVET0_07.BorderWidth = "1"
                    TRVET0_08.BorderWidth = "1"


                    TRVET0_00.BorderStyle = BorderStyle.Solid
                    TRVET0_01.BorderStyle = BorderStyle.Solid
                    TRVET0_02.BorderStyle = BorderStyle.Solid
                    TRVET0_03.BorderStyle = BorderStyle.Solid
                    TRVET0_04.BorderStyle = BorderStyle.Solid
                    TRVET0_05.BorderStyle = BorderStyle.Solid
                    TRVET0_06.BorderStyle = BorderStyle.Solid
                    TRVET0_07.BorderStyle = BorderStyle.Solid
                    TRVET0_08.BorderStyle = BorderStyle.Solid


                    TRVET0_00.BorderColor = Drawing.Color.Silver
                    TRVET0_01.BorderColor = Drawing.Color.Silver
                    TRVET0_02.BorderColor = Drawing.Color.Silver
                    TRVET0_03.BorderColor = Drawing.Color.Silver
                    TRVET0_04.BorderColor = Drawing.Color.Silver
                    TRVET0_05.BorderColor = Drawing.Color.Silver
                    TRVET0_06.BorderColor = Drawing.Color.Silver
                    TRVET0_07.BorderColor = Drawing.Color.Silver
                    TRVET0_08.BorderColor = Drawing.Color.Silver


                    RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                    RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Document Name")
                    RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Party")
                    RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Executant")
                    RH.AddColumn(TRVET0, TRVET0_04, 15, 15, "l", "Execute Date")
                    RH.AddColumn(TRVET0, TRVET0_05, 15, 15, "l", "Before Execute")
                    RH.AddColumn(TRVET0, TRVET0_06, 15, 15, "l", "Executed Version")
                    RH.AddColumn(TRVET0, TRVET0_07, 10, 10, "l", "Requested By")
                    RH.AddColumn(TRVET0, TRVET0_08, 10, 10, "l", "Requested Date")


                    Dim i As Integer = 0
                    TB.Controls.Add(TRVET0)
                    RH.BlankRow(TB, 1)

                    Dim RequestID As Integer = 1
                    Dim NextRequest As Integer = 0

                    For Each DR In DT_APP1.Rows
                        Dim TRVET55 As New TableRow
                        Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07, TRVET55_08, TRVET55_09, TRVET55_10, TRVET55_11 As New TableCell

                        TRVET55_00.BorderWidth = "1"
                        TRVET55_01.BorderWidth = "1"
                        TRVET55_02.BorderWidth = "1"
                        TRVET55_03.BorderWidth = "1"
                        TRVET55_04.BorderWidth = "1"
                        TRVET55_05.BorderWidth = "1"
                        TRVET55_06.BorderWidth = "1"
                        TRVET55_07.BorderWidth = "1"
                        TRVET55_08.BorderWidth = "1"

                        TRVET55_00.BorderStyle = BorderStyle.Solid
                        TRVET55_01.BorderStyle = BorderStyle.Solid
                        TRVET55_02.BorderStyle = BorderStyle.Solid
                        TRVET55_03.BorderStyle = BorderStyle.Solid
                        TRVET55_04.BorderStyle = BorderStyle.Solid
                        TRVET55_05.BorderStyle = BorderStyle.Solid
                        TRVET55_06.BorderStyle = BorderStyle.Solid
                        TRVET55_07.BorderStyle = BorderStyle.Solid
                        TRVET55_08.BorderStyle = BorderStyle.Solid


                        TRVET55_00.BorderColor = Drawing.Color.Silver
                        TRVET55_01.BorderColor = Drawing.Color.Silver
                        TRVET55_02.BorderColor = Drawing.Color.Silver
                        TRVET55_03.BorderColor = Drawing.Color.Silver
                        TRVET55_04.BorderColor = Drawing.Color.Silver
                        TRVET55_05.BorderColor = Drawing.Color.Silver
                        TRVET55_06.BorderColor = Drawing.Color.Silver
                        TRVET55_07.BorderColor = Drawing.Color.Silver
                        TRVET55_08.BorderColor = Drawing.Color.Silver


                        RequestID = DR(9)
                        If RequestID = NextRequest Then
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", "")
                            RH.AddColumn(TRVET55, TRVET55_08, 10, 10, "l", "")


                        Else
                            i = i + 1
                            RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                            RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                            RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                            RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))

                            If IsDBNull(DR(3)) Then
                                RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "l", "")
                            Else
                                RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "l", Format(DR(3), "dd/MMM/yyyy"))
                            End If

                            RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", "<a href='ShowVetFormat.aspx?PkID=" + DR(8).ToString + "&VetID=" + DR(9).ToString + "' style='text-align:right;' target='_blank' >" & DR(4) & "</a>")

                            If IsDBNull(DR(5)) Then
                                RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", "")
                            Else
                                Dim ExeData() As String = DR(5).Split(CChar("¶"))
                                Dim ExeFile As String = ExeData(0)
                                Dim ExePkId As Integer = CInt(ExeData(1))
                                RH.AddColumn(TRVET55, TRVET55_06, 15, 15, "l", "<a href='ShowVetFormat.aspx?PkID=" + ExePkId.ToString + "&VetID=0' style='text-align:right;' target='_blank' >" & ExeFile & "</a>")
                            End If
                            RH.AddColumn(TRVET55, TRVET55_07, 10, 10, "l", DR(6))
                            If IsDBNull(DR(7)) Then
                                RH.AddColumn(TRVET55, TRVET55_08, 10, 10, "l", "")
                            Else
                                RH.AddColumn(TRVET55, TRVET55_08, 10, 10, "l", Format(DR(7), "dd/MMM/yyyy"))
                            End If
                        End If
                        TB.Controls.Add(TRVET55)
                    Next
                End If
            End If
            pnDisplay.Controls.Add(TB)
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
