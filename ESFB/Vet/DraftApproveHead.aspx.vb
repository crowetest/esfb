﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vet_DraftApproveHead
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID, UserID As Integer
#Region "Page Load & Dispose"
    Protected Sub Vet_DraftDocument_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub Vet_DraftDocument_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            Me.hdnRequest.Value = CStr(RequestID)
            Me.Master.subtitle = "Drafting"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Try
            Dim Data() As String = eventArgument.Split(CChar("ʘ"))
            UserID = CInt(Session("UserID"))
            Dim DR As DataRow
            Select Case CInt(Data(0))
                Case 1 'Fill Compliance For Approve
                    DT = GF.GetQueryResult("SELECT d.DRAFT_NAME+'Æ'+upper(v.STATEMENT_FACT)+'Æ'+isnull((select CONVERT(varchar(10),count(a.REQUEST_ID),0) from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID),0) FROM VET_MASTER v, VET_DRAFT d WHERE v.DRAFT_ID=d.DRAFT_ID and v.STATUS_ID=0 and v.SERVICE_ID=1 and v.REQUEST_ID=" & RequestID & "")
                    If DT.Rows.Count > 0 Then
                        CallBackReturn = DT.Rows(0)(0).ToString()
                    End If
                    CallBackReturn += "Æ"
                    Dim RequestData() As String = CStr(DT.Rows(0)(0)).Split(CChar("Æ"))
                    If CInt(RequestData(2)) > 0 Then
                        DT = GF.GetQueryResult("SELECT convert(varchar(10),PkId)+ '®' +convert(varchar(10),REQUEST_ID)+ '®' + FILE_NAME FROM DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT where SERVICE_ID=1 and REQUEST_ID=" & RequestID & "")
                        If DT.Rows.Count > 0 Then
                            For Each DR In DT.Rows
                                CallBackReturn += DR(0).ToString() + "¶"
                            Next
                        End If
                    End If
                Case 2 'Approve Confirm
                    Dim RequestID As Integer = CInt(Data(1))
                    Dim Err As Integer = DB.ExecuteNonQuery("update VET_MASTER set STATUS_ID=1, REQAPP_BY = " & UserID & ", REQAPP_DT = GETDATE()  where REQUEST_ID=" & RequestID & " and SERVICE_ID=1")
                    If Err = 1 Then
                        CallBackReturn = CStr(0) + "ʘ" + "Approved Successfully"
                    End If
                Case 3 'Reject Confirm
                    Dim RequestID As Integer = CInt(Data(1))
                    Dim Reason As String = CStr(Data(2))
                    Dim Err As Integer = DB.ExecuteNonQuery("update VET_MASTER set STATUS_ID=2,REJECT_REASON='" & Reason & "', REQAPP_BY = " & UserID & ", REQAPP_DT = GETDATE() where REQUEST_ID=" & RequestID & " and SERVICE_ID=1")
                    If Err = 1 Then
                        CallBackReturn = CStr(0) + "ʘ" + "Rejected Successfully"
                    End If
            End Select
        Catch ex As Exception
         
            Response.Redirect("~/CatchException.aspx?ErrorNo=1")
        End Try
    End Sub
#End Region

End Class
