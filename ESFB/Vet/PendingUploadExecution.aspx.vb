﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Vet_PendingUploadExecution
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim TB As New Table
    Dim RptID As Integer
    Dim UserID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 145) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            UserID = Int(Session("UserID"))

            Dim DT, DT_DRAFT, DT_OPINION, DT_COMMT, DT_VET, DT_ARC As New DataTable

            TB.Attributes.Add("width", "100%")

            Dim RowBG As Integer = 0
            Dim DR As DataRow

            RH.Heading(CStr(Session("FirmName")), TB, "PENDING UPLOAD EXECUTED VERSION", 100)

            RH.BlankRow(TB, 5)
            '---------------
            DT_VET = DB.ExecuteDataSet("select n.NATURE_NAME,p.PARTY_NAME,upper(v.STATEMENT_FACT),v.REQUEST_ID from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.SERVICE_ID=4 and v.STATUS_ID=12").Tables(0)

            If DT_VET.Rows.Count > 0 Then
                Dim TRVET0 As New TableRow
                TRVET0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04 As New TableCell

                TRVET0_00.BorderWidth = "1"
                TRVET0_01.BorderWidth = "1"
                TRVET0_02.BorderWidth = "1"
                TRVET0_03.BorderWidth = "1"
                TRVET0_04.BorderWidth = "1"

                TRVET0_00.BorderStyle = BorderStyle.Solid
                TRVET0_01.BorderStyle = BorderStyle.Solid
                TRVET0_02.BorderStyle = BorderStyle.Solid
                TRVET0_03.BorderStyle = BorderStyle.Solid
                TRVET0_04.BorderStyle = BorderStyle.Solid

                TRVET0_00.BorderColor = Drawing.Color.Silver
                TRVET0_01.BorderColor = Drawing.Color.Silver
                TRVET0_02.BorderColor = Drawing.Color.Silver
                TRVET0_03.BorderColor = Drawing.Color.Silver
                TRVET0_04.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "l", "#")
                RH.AddColumn(TRVET0, TRVET0_01, 25, 25, "l", "Nature Of Transaction")
                RH.AddColumn(TRVET0, TRVET0_02, 30, 30, "l", "Other Party")
                RH.AddColumn(TRVET0, TRVET0_03, 25, 25, "l", "Statement Of Facts")
                RH.AddColumn(TRVET0, TRVET0_04, 10, 10, "c", "")

                Dim i As Integer = 0
                TB.Controls.Add(TRVET0)
                RH.BlankRow(TB, 1)

                For Each DR In DT_VET.Rows
                    Dim TRVET55 As New TableRow
                    i = i + 1
                    Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04 As New TableCell

                    TRVET55_00.BorderWidth = "1"
                    TRVET55_01.BorderWidth = "1"
                    TRVET55_02.BorderWidth = "1"
                    TRVET55_03.BorderWidth = "1"
                    TRVET55_04.BorderWidth = "1"

                    TRVET55_00.BorderStyle = BorderStyle.Solid
                    TRVET55_01.BorderStyle = BorderStyle.Solid
                    TRVET55_02.BorderStyle = BorderStyle.Solid
                    TRVET55_03.BorderStyle = BorderStyle.Solid
                    TRVET55_04.BorderStyle = BorderStyle.Solid

                    TRVET55_00.BorderColor = Drawing.Color.Silver
                    TRVET55_01.BorderColor = Drawing.Color.Silver
                    TRVET55_02.BorderColor = Drawing.Color.Silver
                    TRVET55_03.BorderColor = Drawing.Color.Silver
                    TRVET55_04.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "l", i)
                    RH.AddColumn(TRVET55, TRVET55_01, 25, 25, "l", DR(0))
                    RH.AddColumn(TRVET55, TRVET55_02, 30, 30, "l", DR(1))
                    RH.AddColumn(TRVET55, TRVET55_03, 25, 25, "l", DR(2))
                    RH.AddColumn(TRVET55, TRVET55_04, 10, 10, "c", "<a href='VetUploadExecute.aspx?RequestID=" + DR(3).ToString() + "'><img id='imgReport' src='../Image/approve.png' title='Upload Executed Version' Height='20px' Width='20px' style='cursor:pointer;'/></a>")

                    TB.Controls.Add(TRVET55)
                Next
            End If
            pnDisplay.Controls.Add(TB)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
