﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vet_DateWise_Archive
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub Vet_DateWise_Archive_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub
    Protected Sub Vet_DateWise_Archive_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 247) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Archiving Of Executed Documents"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            If Not IsPostBack Then
                Dim FisrtDay As Date = CDate(New DateTime(Today.Year, Today.Month, 1))
                Me.txtFromDt.Text = FisrtDay.ToString("dd/MMM/yyyy")
                Me.txtToDt.Text = Format(Date.Now(), "dd/MMM/yyyy")
            End If
            Me.cmbNature.Attributes.Add("onchange", "return NatureOnChange()")
            cmbDocument.Attributes.Add("onchange", "return DocumentOnChange()")
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim DR As DataRow
        Select CInt(Data(0))
            Case 1 'Fill Vet Nature Of Document
                DT = GF.GetQueryResult("select convert(varchar(10),x.SERVICE_ID)+ 'Œ' +convert(varchar(10),x.NATURE_ID) Nature,x.NATURE_NAME+ '  -  ' +x.ArRoute Naturename from(SELECT V.SERVICE_ID ,V.NATURE_ID,N.NATURE_NAME,COUNT(*) ArCnt,'DIRECT ARCHIVE' ArRoute FROM VET_MASTER V,VET_NATURE_TRANSACTION  N WHERE V.SERVICE_ID=5 AND V.STATUS_ID IN(3,4) AND V.NATURE_ID=N.NATURE_ID GROUP BY V.SERVICE_ID ,V.NATURE_ID,N.NATURE_NAME UNION ALL SELECT V.SERVICE_ID ,V.NATURE_ID,N.NATURE_NAME,COUNT(*) ArCnt,'VETTING ROUTE' ArRoute FROM VET_MASTER V,VET_NATURE_TRANSACTION  N WHERE V.SERVICE_ID=4 AND V.STATUS_ID IN(15) AND V.NATURE_ID=N.NATURE_ID GROUP BY V.SERVICE_ID ,V.NATURE_ID,N.NATURE_NAME) x ORDER BY 2")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                Else
                    CallBackReturn = ""
                End If
            Case 2 'Fill Party
                Dim ServiceID As Integer = CInt(Data(1))
                Dim NatureID As Integer = CInt(Data(2))
                If ServiceID = 5 Then
                    DT = GF.GetQueryResult("select -1 DOCUMENT_ID, 'ALL' DOCUMENT_NAME union select distinct d.DOCUMENT_ID,d.DOCUMENT_NAME from VET_MASTER v, VET_DOCUMENT d where d.DOCUMENT_TYPE=3 and v.SERVICE_ID=d.SERVICE_ID and v.ARCH_DOCID=d.DOCUMENT_ID and v.STATUS_ID in(3,4) and v.SERVICE_ID=5 and v.ARCH_DOCID is not null and v.NATURE_ID=" & NatureID & " order by 1,2")
                ElseIf ServiceID = 4 Then
                    DT = GF.GetQueryResult("select -1 DOCUMENT_ID, 'ALL' DOCUMENT_NAME union select distinct d.DOCUMENT_ID,d.DOCUMENT_NAME from VET_MASTER vm,VET_TRANSACTION_MASTER v, VET_DOCUMENT d where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and vm.SERVICE_ID=d.SERVICE_ID and v.REQUEST_ID=vm.REQUEST_ID and vm.SERVICE_ID=d.SERVICE_ID and vm.NATURE_ID=" & NatureID & " and vm.STATUS_ID=15 and v.VET_FLAG=1 order by 1,2")
                End If
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                Else
                    CallBackReturn = ""
                End If
            Case 3 'Fill Party
                Dim ServiceID As Integer = CInt(Data(1))
                Dim NatureID As Integer = CInt(Data(2))
                If ServiceID = 5 Then
                    DT = GF.GetQueryResult("select -1 PARTY_ID, 'ALL' PARTY_NAME union select distinct v.PARTY_ID,p.PARTY_NAME from VET_MASTER v, VET_PARTY p where v.PARTY_ID=p.PARTY_ID and v.STATUS_ID in(3,4) and v.SERVICE_ID=5 and v.ARCH_DOCID is not null and v.NATURE_ID=" & NatureID & " order by 1,2")
                ElseIf ServiceID = 4 Then
                    DT = GF.GetQueryResult("select -1 PARTY_ID, 'ALL' PARTY_NAME union select distinct p.PARTY_ID,p.PARTY_NAME from VET_MASTER vm,VET_TRANSACTION_MASTER v, VET_PARTY p where v.REQUEST_ID=vm.REQUEST_ID and p.PARTY_ID =VM.PARTY_ID and VM.STATUS_ID=15 and v.VET_FLAG=1 and Vm.NATURE_ID=" & NatureID & " order by 1,2")
                End If
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                Else
                    CallBackReturn = ""
                End If
        End Select
    End Sub
#End Region
    
End Class
