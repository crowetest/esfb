﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="VetVerification.aspx.vb" Inherits="Vet_VetVerification" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
        .style1
        {
            text-align: left;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {
                document.getElementById("colResolution").style.display = "none";
                document.getElementById("rowDoc").style.display = "none";
                document.getElementById("rowReason").style.display = "none";
                ToServer("1ʘ", 1);
            }
            function ViewAttachment(Value) {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                if (RequestID > 0)
                    window.open("ShowFormat.aspx?RequestID=" + RequestID + "&Value=" + Value + "&Id=0");
                return false;
            }
            function btnOk_onclick() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                var Status = 1;
                var row = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("¶");
                var Length = row.length - 1;
                var Data = "";
                document.getElementById("<%= hdnData.ClientID %>").value = "";
                for (n = 0; n < Length; n++) {
                    col = row[n].split("®");
                    if (col[3] = 1) {
                        var Document = col[1].split("^")
                        document.getElementById("<%= hdnData.ClientID %>").value += col[0] + "®" + Document[1] + "®" + Document[0] + "®" + col[3] + "¶";
                    }
                }
                Data = document.getElementById("<%= hdnData.ClientID %>").value;
                var Reason = "";
                ToServer("2ʘ" + RequestID + "ʘ" + Status + "ʘ" + Data + "ʘ" + Reason, 2);
            }

            function btnNotOk_onclick() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                document.getElementById("btnOk").disabled = true;
                var Status = 2;
                var row = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("¶");
                var Length = row.length - 1;
                var Data = "";
                document.getElementById("<%= hdnData.ClientID %>").value = "";
                for (n = 0; n < Length; n++) {
                    col = row[n].split("®");
                    if (col[3] = 1) {
                        var Document = col[1].split("^")
                        document.getElementById("<%= hdnData.ClientID %>").value += col[0] + "®" + Document[1] + "®" + Document[0] + "®" + col[3] + "¶";
                    }
                }
                Data = document.getElementById("<%= hdnData.ClientID %>").value;
                var Reason = "";
                if (document.getElementById("<%= txtReason.ClientID %>").value == "") {
                    alert("Please Enter Reason");
                    document.getElementById("rowReason").style.display = "";
                    document.getElementById("<%= txtReason.ClientID %>").focus();
                    return false;
                }
                Reason = document.getElementById("<%= txtReason.ClientID %>").value;
                ToServer("2ʘ" + RequestID + "ʘ" + Status + "ʘ" + Data + "ʘ" + Reason, 2);
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                        {
                            //SBIÆ  Ratheesh P RÆ  ADFSDFSDF SDSDFSDFSDFÆ  AVAILING OF LOANÆ 0Æ  2Æ  
                            var Data = Arg.split("Æ");
                            document.getElementById("<%= txtParty.ClientID %>").value = Data[0];
                            document.getElementById("<%= txtStatement.ClientID %>").value = Data[2];
                            document.getElementById("<%= txtNature.ClientID %>").value = Data[3];
                            document.getElementById("<%= txtTerms.ClientID %>").value = Data[6];
                            if (Data[4] == 1)
                                document.getElementById("colResolution").style.display = "";
                            else
                                document.getElementById("colResolution").style.display = "none";
                            if (Data[5] > 0) {
                                document.getElementById("<%= hdnDocDtl.ClientID %>").value = Data[7];
                                if (Data[8] != "") {
                                    document.getElementById("<%= hdnExecuteDtl.ClientID %>").value = Data[8];
                                }
                                DiplayTable();
                            }
                            break;
                        }
                    case 2: // Confirmation
                        {
                            var Data = Arg.split("ʘ");
                            alert(Data[1]);
                            if (Data[0] == 0) {
                                //Return to Old Report
                                if (window.opener != null && !window.opener.closed) {
                                    window.opener.location.reload();
                                    window.close();
                                }
                                window.onbeforeunload = RefreshParent;
                            }
                            break;
                        }
                }
            }
            function DiplayTable() {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>EXECUTED VERSION</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:45%;text-align:left'>Document&nbsp;For&nbsp;Transaction</td>";
                Tab += "<td style='width:45%;text-align:left'>File&nbsp;Name</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:70%; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                if (document.getElementById("<%= hdnDocDtl.ClientID %>").value != "") {
                    document.getElementById("rowDoc").style.display = "";
                    var HidArg = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("¶");
                    var RowCount = HidArg.length - 1;
                    var j;
                    var Total = 0;
                    var row_bg1 = 0;
                    var SlNo = 0;
                    for (j = 0; j < RowCount; j++) {
                        if (row_bg1 == 0) {
                            row_bg1 = 1;
                            Tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg1 = 0;
                            Tab += "<tr class='sub_second';>";
                        }
                        var HidArg1 = HidArg[j].split("®");
                        SlNo = SlNo + 1;
                        Tab += "<td style='width:10%;text-align:center'><input type='checkbox' id='chkSelection" + HidArg1[0] + "' onclick='ChkPotentialOnClick(" + HidArg1[0] + ")' /></td>";
                        var select = "<select id='cmbDocument" + HidArg1[0] + "' class='NormalText' name='cmbDocument" + HidArg1[0] + "' style='width:100%' disabled=true onchange='UpdateValueDocument(" + HidArg1[0] + ")' >";

                        var rows = document.getElementById("<%= hdnExecuteDtl.ClientID %>").value.split("Ř"); //Document Details
                        for (a = 1; a < rows.length; a++) {
                            var cols = rows[a].split("Ĉ");
                            if (cols[0] == HidArg1[1])
                                select += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                            else
                                select += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
                        }
                        Tab += "<td style='width:45%;text-align:center'>" + select + "</td>";
                        Tab += "<td style='width:45%;text-align:left;cursor: pointer;'><a href='ShowVetFormat.aspx?PkID=" + HidArg1[0] + "&VetID=0&DocID=0'>" + HidArg1[2] + "</a></td></tr>";
                    }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div>";
                    document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;
                }
                else
                    document.getElementById("rowDoc").style.display = "none";
            }
            function ChkPotentialOnClick(val) {
                if (document.getElementById("chkSelection" + val).checked == true) {
                    document.getElementById("cmbDocument" + val).disabled = false;
                }
                else {
                    document.getElementById("cmbDocument" + val).disabled = true;
                }
                UpdateValueDocument(val)
            }
            function UpdateString(Val) {
                var i = 0;
                var Newstr = "";
                var row = document.getElementById("<%= hdnDocDtl.ClientID %>").value.split("¶");
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("®");
                    if (col[0] == Val) {
                        Newstr += col[0] + "®" + document.getElementById("cmbDocument" + col[0]).value + "®" + col[2] + "®1¶";
                    }
                    else
                        Newstr += row[n] + "¶";
                }
                document.getElementById("<%= hdnDocDtl.ClientID %>").value = Newstr;
            }
            function GetVetReport() {
                var RequestID = document.getElementById("<%= hdnRequest.ClientID %>").value;
                window.open("ViewVetReport.aspx?RequestID=" + RequestID);
            }
            function btnExit_onclick() {
                //Return to Old Report
                if (window.opener != null && !window.opener.closed) {
                    window.opener.location.reload();
                    window.close();
                }
                window.onbeforeunload = RefreshParent;
            }            

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 90%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Party with which transaction is plan
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtParty" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Statement Of Facts
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtStatement" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td id="colResolution" style="width: 10%">
                        <asp:ImageButton ID="cmdView" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle"
                            ImageUrl="~/Image/attchment2.png" ToolTip="View Attachment" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Nature of transaction
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtNature" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt;" Width="100%" MaxLength="50" ReadOnly="True" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%;" class="style1">
                        General Terms
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtTerms" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" ReadOnly="True" />
                    </td>
                    <td style="width: 10%; text-align: center; cursor: pointer;">
                        <asp:HyperLink ID="hlVetRpt" runat="server" Font-Underline="True" ForeColor="#3333CC">Vet Report</asp:HyperLink>
                    </td>
                </tr>
                <tr id="rowDoc">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr id="rowReason">
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%;" class="style1">
                        Reason
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtReason" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <input id="btnOk" style="font-family: cambria; cursor: pointer; width: 70px;" type="button"
                            value="OK" onclick="return btnOk_onclick()" />
                        <input id="btnNotOk" style="font-family: cambria; cursor: pointer; width: 70px;"
                            type="button" value="NOT OK" onclick="return btnNotOk_onclick()" />
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 70px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnRequest" runat="server" />
            <asp:HiddenField ID="hdnDocDtl" runat="server" />
            <asp:HiddenField ID="hdnExecuteDtl" runat="server" />
            <asp:HiddenField ID="hdnData" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
