﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Vet_DocumentStageWiseRpt3
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim TB As New Table
    Dim ServiceID, RequestID, StatusID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")

            ServiceID = CInt(Request.QueryString.Get("ServiceID"))
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            StatusID = CInt(Request.QueryString.Get("StatusID"))

            Dim DT_VET As New DataTable

            TB.Attributes.Add("width", "100%")

            Dim RowBG As Integer = 0

            RH.Heading(CStr(Session("FirmName")), TB, "ATTACHED DOCUMENTS", 100)

            RH.BlankRow(TB, 5)
            Dim DR As DataRow
            If StatusID = 4 Then
                DT_VET = DB.ExecuteDataSet("select PkId,DOCFILE_NAME from DMS_ESFB.dbo.VET_REDRAFT_ATTACHMENT where REQUEST_ID= " & RequestID & "").Tables(0)
            Else
                DT_VET = DB.ExecuteDataSet("select PkId,FILE_NAME from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT where SERVICE_ID=" & ServiceID & " and REQUEST_ID= " & RequestID & "").Tables(0)
            End If

            Dim i As Integer = 0
            RH.BlankRow(TB, 1)

            If DT_VET.Rows.Count > 0 Then


                For Each DR In DT_VET.Rows
                    Dim TRVET55 As New TableRow
                    i = i + 1
                    Dim TRVET55_00, TRVET55_01 As New TableCell

                    TRVET55_00.BorderWidth = "1"
                    TRVET55_01.BorderWidth = "1"

                    TRVET55_00.BorderStyle = BorderStyle.Solid
                    TRVET55_01.BorderStyle = BorderStyle.Solid

                    TRVET55_00.BorderColor = Drawing.Color.Silver
                    TRVET55_01.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "c", i)
                    If ServiceID = 4 Then
                        If StatusID = 4 Then
                            RH.AddColumn(TRVET55, TRVET55_01, 90, 90, "c", "<a href='ShowFormat.aspx?RequestID=" + RequestID.ToString + "&Value=" + StatusID.ToString + "&Id=" + DR(0).ToString + "' style='text-align:right;' target='_blank' >" & DR(1) & "</a>")
                        Else
                            RH.AddColumn(TRVET55, TRVET55_01, 90, 90, "c", "<a href='ShowFormat.aspx?RequestID=" + RequestID.ToString + "&Value=" + StatusID.ToString + "&Id=" + DR(0).ToString + "' style='text-align:right;' target='_blank' >Vew Document " & i & "</a>")
                        End If
                    Else
                        RH.AddColumn(TRVET55, TRVET55_01, 90, 90, "c", "<a href='ShowFormat.aspx?RequestID=" + RequestID.ToString + "&Value=" + StatusID.ToString + "&Id=" + DR(0).ToString + "' style='text-align:right;' target='_blank' >" & DR(1) & "</a>")
                    End If

                    TB.Controls.Add(TRVET55)
                Next
            End If
            pnDisplay.Controls.Add(TB)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
