﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Vet_ViewCloseRequestRpt
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim TB As New Table
    Dim RptID As Integer
    Dim UserID, DeptID, i As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 147) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            UserID = Int(Session("UserID"))

            Dim DT, DT_SUB, DT_DRAFT, DT_OPINION, DT_COMMT, DT_VET, DT_ARC As New DataTable

            TB.Attributes.Add("width", "100%")

            DT = DB.ExecuteDataSet("select Department_ID from EMP_MASTER where Emp_Code=" & UserID & "").Tables(0)
            DeptID = DT.Rows(0)(0)

            Dim RowBG As Integer = 0
            Dim DR, DRS As DataRow

            RH.Heading(CStr(Session("FirmName")), TB, "VIEW CLOSED REQUESTS", 100)

            '---------------Drafting
            DT_VET = DB.ExecuteDataSet("select d.DRAFT_NAME,v.STATEMENT_FACT,v.DRAFT_REMARK,v.REQUEST_ID,(select convert(varchar(10),COUNT(a.PkId)) from DMS_ESFB.dbo.VET_DRAFT_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID and a.DRAFT_ID=v.DRAFT_ID)DraftDoc from VET_MASTER v, VET_DRAFT d where v.DRAFT_ID=d.DRAFT_ID and v.SERVICE_ID=1 and v.STATUS_ID=4 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)

            If DT_VET.Rows.Count > 0 Then
                RH.BlankRow(TB, 17)
                RH.SubHeading(TB, 100, "l", "DRAFTING")
                RH.BlankRow(TB, 5)

                Dim TRVET0 As New TableRow
                TRVET0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04 As New TableCell

                TRVET0_00.BorderWidth = "1"
                TRVET0_01.BorderWidth = "1"
                TRVET0_02.BorderWidth = "1"
                TRVET0_03.BorderWidth = "1"
                TRVET0_04.BorderWidth = "1"

                TRVET0_00.BorderStyle = BorderStyle.Solid
                TRVET0_01.BorderStyle = BorderStyle.Solid
                TRVET0_02.BorderStyle = BorderStyle.Solid
                TRVET0_03.BorderStyle = BorderStyle.Solid
                TRVET0_04.BorderStyle = BorderStyle.Solid

                TRVET0_00.BorderColor = Drawing.Color.Silver
                TRVET0_01.BorderColor = Drawing.Color.Silver
                TRVET0_02.BorderColor = Drawing.Color.Silver
                TRVET0_03.BorderColor = Drawing.Color.Silver
                TRVET0_04.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "l", "#")
                RH.AddColumn(TRVET0, TRVET0_01, 25, 25, "l", "Document To Drafted")
                RH.AddColumn(TRVET0, TRVET0_02, 20, 20, "l", "Statement Of Facts")
                RH.AddColumn(TRVET0, TRVET0_03, 25, 25, "l", "Remark")
                RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "c", "Drafted Document")

                i = 0
                TB.Controls.Add(TRVET0)
                RH.BlankRow(TB, 1)

                For Each DR In DT_VET.Rows
                    Dim TRVET55 As New TableRow
                    i = i + 1
                    Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04 As New TableCell

                    TRVET55_00.BorderWidth = "1"
                    TRVET55_01.BorderWidth = "1"
                    TRVET55_02.BorderWidth = "1"
                    TRVET55_03.BorderWidth = "1"
                    TRVET55_04.BorderWidth = "1"

                    TRVET55_00.BorderStyle = BorderStyle.Solid
                    TRVET55_01.BorderStyle = BorderStyle.Solid
                    TRVET55_02.BorderStyle = BorderStyle.Solid
                    TRVET55_03.BorderStyle = BorderStyle.Solid
                    TRVET55_04.BorderStyle = BorderStyle.Solid

                    TRVET55_00.BorderColor = Drawing.Color.Silver
                    TRVET55_01.BorderColor = Drawing.Color.Silver
                    TRVET55_02.BorderColor = Drawing.Color.Silver
                    TRVET55_03.BorderColor = Drawing.Color.Silver
                    TRVET55_04.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "l", i)
                    RH.AddColumn(TRVET55, TRVET55_01, 25, 25, "l", DR(0))
                    RH.AddColumn(TRVET55, TRVET55_02, 20, 20, "l", DR(1))
                    RH.AddColumn(TRVET55, TRVET55_03, 25, 25, "l", DR(2))
                    If CInt(DR(4)) > 0 Then
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "c", "<a href='ViewCloseSubRpt.aspx?RequestID=" & DR(3) & "&DocID=1' style='text-align:right;' target='_blank' >" & DR(4) & "</a>")
                    Else
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "c", "NIL")
                    End If

                    TB.Controls.Add(TRVET55)
                Next
            End If

            '---------------Opinion
            DT_VET = DB.ExecuteDataSet("select v.USER_OPINION,v.STATEMENT_FACT,v.OPINION_REPLY,v.REQUEST_ID,(select convert(varchar(10),count(a.PkId)) from DMS_ESFB.dbo.VET_OPINION_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID)OpinionDoc from VET_MASTER v where v.SERVICE_ID=2 and v.STATUS_ID=4 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)

            If DT_VET.Rows.Count > 0 Then
                RH.BlankRow(TB, 17)
                RH.SubHeading(TB, 100, "l", "OPINION")
                RH.BlankRow(TB, 5)

                Dim TRVET0 As New TableRow
                TRVET0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04 As New TableCell

                TRVET0_00.BorderWidth = "1"
                TRVET0_01.BorderWidth = "1"
                TRVET0_02.BorderWidth = "1"
                TRVET0_03.BorderWidth = "1"
                TRVET0_04.BorderWidth = "1"

                TRVET0_00.BorderStyle = BorderStyle.Solid
                TRVET0_01.BorderStyle = BorderStyle.Solid
                TRVET0_02.BorderStyle = BorderStyle.Solid
                TRVET0_03.BorderStyle = BorderStyle.Solid
                TRVET0_04.BorderStyle = BorderStyle.Solid

                TRVET0_00.BorderColor = Drawing.Color.Silver
                TRVET0_01.BorderColor = Drawing.Color.Silver
                TRVET0_02.BorderColor = Drawing.Color.Silver
                TRVET0_03.BorderColor = Drawing.Color.Silver
                TRVET0_04.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "l", "#")
                RH.AddColumn(TRVET0, TRVET0_01, 25, 25, "l", "Opinion Related To")
                RH.AddColumn(TRVET0, TRVET0_02, 20, 20, "l", "Statement Of Facts")
                RH.AddColumn(TRVET0, TRVET0_03, 25, 25, "l", "Remark")
                RH.AddColumn(TRVET0, TRVET0_04, 20, 20, "c", "Uploaded Opinion")

                i = 0
                TB.Controls.Add(TRVET0)
                RH.BlankRow(TB, 1)

                For Each DR In DT_VET.Rows
                    Dim TRVET55 As New TableRow
                    i = i + 1
                    Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04 As New TableCell

                    TRVET55_00.BorderWidth = "1"
                    TRVET55_01.BorderWidth = "1"
                    TRVET55_02.BorderWidth = "1"
                    TRVET55_03.BorderWidth = "1"
                    TRVET55_04.BorderWidth = "1"

                    TRVET55_00.BorderStyle = BorderStyle.Solid
                    TRVET55_01.BorderStyle = BorderStyle.Solid
                    TRVET55_02.BorderStyle = BorderStyle.Solid
                    TRVET55_03.BorderStyle = BorderStyle.Solid
                    TRVET55_04.BorderStyle = BorderStyle.Solid

                    TRVET55_00.BorderColor = Drawing.Color.Silver
                    TRVET55_01.BorderColor = Drawing.Color.Silver
                    TRVET55_02.BorderColor = Drawing.Color.Silver
                    TRVET55_03.BorderColor = Drawing.Color.Silver
                    TRVET55_04.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "l", i)
                    RH.AddColumn(TRVET55, TRVET55_01, 25, 25, "l", DR(0))
                    RH.AddColumn(TRVET55, TRVET55_02, 20, 20, "l", DR(1))
                    RH.AddColumn(TRVET55, TRVET55_03, 25, 25, "l", DR(2))
                    If CInt(DR(4)) > 0 Then
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "c", "<a href='ViewCloseSubRpt.aspx?RequestID=" & DR(3) & "&DocID=2' style='text-align:right;' target='_blank' >" & DR(4) & "</a>")
                    Else
                        RH.AddColumn(TRVET55, TRVET55_04, 20, 20, "c", "NIL")
                    End If

                    TB.Controls.Add(TRVET55)
                Next
            End If

            '---------------Comments On Document
            DT_VET = DB.ExecuteDataSet("select d.DOCUMENT_NAME,v.COMT_PURPOSE,v.COMT_EXPECTATION,v.REQUEST_ID  from VET_MASTER v, VET_DOCUMENT d where v.COMT_DOCID=d.DOCUMENT_ID and d.SERVICE_ID=v.SERVICE_ID and d.DOCUMENT_TYPE=1 and v.SERVICE_ID=3 and v.STATUS_ID=4 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)

            If DT_VET.Rows.Count > 0 Then
                RH.BlankRow(TB, 17)
                RH.SubHeading(TB, 100, "l", "COMMENTS ON DOCUMENTS")
                RH.BlankRow(TB, 5)

                Dim TRVET0 As New TableRow
                TRVET0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04 As New TableCell

                TRVET0_00.BorderWidth = "1"
                TRVET0_01.BorderWidth = "1"
                TRVET0_02.BorderWidth = "1"
                TRVET0_03.BorderWidth = "1"
                TRVET0_04.BorderWidth = "1"

                TRVET0_00.BorderStyle = BorderStyle.Solid
                TRVET0_01.BorderStyle = BorderStyle.Solid
                TRVET0_02.BorderStyle = BorderStyle.Solid
                TRVET0_03.BorderStyle = BorderStyle.Solid
                TRVET0_04.BorderStyle = BorderStyle.Solid

                TRVET0_00.BorderColor = Drawing.Color.Silver
                TRVET0_01.BorderColor = Drawing.Color.Silver
                TRVET0_02.BorderColor = Drawing.Color.Silver
                TRVET0_03.BorderColor = Drawing.Color.Silver
                TRVET0_04.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRVET0, TRVET0_00, 10, 10, "l", "#")
                RH.AddColumn(TRVET0, TRVET0_01, 25, 25, "l", "Nature Of Document")
                RH.AddColumn(TRVET0, TRVET0_02, 25, 25, "l", "Purpose")
                RH.AddColumn(TRVET0, TRVET0_03, 25, 25, "l", "Expectation")
                RH.AddColumn(TRVET0, TRVET0_04, 15, 15, "c", "Comments")

                i = 0
                TB.Controls.Add(TRVET0)
                RH.BlankRow(TB, 1)

                For Each DR In DT_VET.Rows
                    Dim TRVET55 As New TableRow
                    i = i + 1
                    Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04 As New TableCell

                    TRVET55_00.BorderWidth = "1"
                    TRVET55_01.BorderWidth = "1"
                    TRVET55_02.BorderWidth = "1"
                    TRVET55_03.BorderWidth = "1"
                    TRVET55_04.BorderWidth = "1"

                    TRVET55_00.BorderStyle = BorderStyle.Solid
                    TRVET55_01.BorderStyle = BorderStyle.Solid
                    TRVET55_02.BorderStyle = BorderStyle.Solid
                    TRVET55_03.BorderStyle = BorderStyle.Solid
                    TRVET55_04.BorderStyle = BorderStyle.Solid

                    TRVET55_00.BorderColor = Drawing.Color.Silver
                    TRVET55_01.BorderColor = Drawing.Color.Silver
                    TRVET55_02.BorderColor = Drawing.Color.Silver
                    TRVET55_03.BorderColor = Drawing.Color.Silver
                    TRVET55_04.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET55, TRVET55_00, 10, 10, "l", i)
                    RH.AddColumn(TRVET55, TRVET55_01, 25, 25, "l", DR(0))
                    RH.AddColumn(TRVET55, TRVET55_02, 25, 25, "l", DR(1))
                    RH.AddColumn(TRVET55, TRVET55_03, 25, 25, "l", DR(2))
                    RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "c", "<a href='ViewCommentsDtlRpt.aspx?RequestID=" + DR(3).ToString() + "' style='text-align:right;' target='_blank' ><img id='imgReport' src='../Image/NextIcon.png' title='Next Page' Height='22px' Width='22px' style='cursor:pointer;'/></a>")

                    TB.Controls.Add(TRVET55)
                Next
            End If

            '---------------Vetting
            DT_VET = DB.ExecuteDataSet("select p.PARTY_NAME,v.STATEMENT_FACT,n.NATURE_NAME,v.GENERAL_TERM,v.VET_REMARK, case when v.STATUS_ID=15 then 'VERIFIED OK' else 'VERIFIED NOT OK' + ' - ' + v.NOTOK_REASON end Vetstatus,v.REQUEST_ID,v.STATUS_ID from VET_MASTER v, VET_PARTY p, VET_NATURE_TRANSACTION n where v.PARTY_ID=p.PARTY_ID and v.NATURE_ID=n.NATURE_ID and v.SERVICE_ID=4 and v.STATUS_ID in(14,15) and v.REQUEST_DEPT=" & DeptID & "").Tables(0)

            If DT_VET.Rows.Count > 0 Then
                RH.BlankRow(TB, 17)
                RH.SubHeading(TB, 100, "l", "VETTING")
                RH.BlankRow(TB, 5)

                Dim TRVET0 As New TableRow
                TRVET0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07 As New TableCell

                TRVET0_00.BorderWidth = "1"
                TRVET0_01.BorderWidth = "1"
                TRVET0_02.BorderWidth = "1"
                TRVET0_03.BorderWidth = "1"
                TRVET0_04.BorderWidth = "1"
                TRVET0_05.BorderWidth = "1"
                TRVET0_06.BorderWidth = "1"
                TRVET0_07.BorderWidth = "1"

                TRVET0_00.BorderStyle = BorderStyle.Solid
                TRVET0_01.BorderStyle = BorderStyle.Solid
                TRVET0_02.BorderStyle = BorderStyle.Solid
                TRVET0_03.BorderStyle = BorderStyle.Solid
                TRVET0_04.BorderStyle = BorderStyle.Solid
                TRVET0_05.BorderStyle = BorderStyle.Solid
                TRVET0_06.BorderStyle = BorderStyle.Solid
                TRVET0_07.BorderStyle = BorderStyle.Solid

                TRVET0_00.BorderColor = Drawing.Color.Silver
                TRVET0_01.BorderColor = Drawing.Color.Silver
                TRVET0_02.BorderColor = Drawing.Color.Silver
                TRVET0_03.BorderColor = Drawing.Color.Silver
                TRVET0_04.BorderColor = Drawing.Color.Silver
                TRVET0_05.BorderColor = Drawing.Color.Silver
                TRVET0_06.BorderColor = Drawing.Color.Silver
                TRVET0_07.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "l", "#")
                RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Other Party")
                RH.AddColumn(TRVET0, TRVET0_02, 10, 10, "l", "Statement Of Facts")
                RH.AddColumn(TRVET0, TRVET0_03, 10, 10, "l", "Nature Of Document")
                RH.AddColumn(TRVET0, TRVET0_04, 25, 25, "l", "General Term")
                RH.AddColumn(TRVET0, TRVET0_05, 15, 15, "l", "Vet Remark")
                RH.AddColumn(TRVET0, TRVET0_06, 20, 20, "l", "Verify Status")
                RH.AddColumn(TRVET0, TRVET0_07, 5, 5, "l", "Update")

                i = 0
                TB.Controls.Add(TRVET0)
                RH.BlankRow(TB, 1)

                For Each DR In DT_VET.Rows
                    Dim TRVET55 As New TableRow
                    i = i + 1
                    Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07 As New TableCell

                    TRVET55_00.BorderWidth = "1"
                    TRVET55_01.BorderWidth = "1"
                    TRVET55_02.BorderWidth = "1"
                    TRVET55_03.BorderWidth = "1"
                    TRVET55_04.BorderWidth = "1"
                    TRVET55_05.BorderWidth = "1"
                    TRVET55_06.BorderWidth = "1"
                    TRVET55_07.BorderWidth = "1"

                    TRVET55_00.BorderStyle = BorderStyle.Solid
                    TRVET55_01.BorderStyle = BorderStyle.Solid
                    TRVET55_02.BorderStyle = BorderStyle.Solid
                    TRVET55_03.BorderStyle = BorderStyle.Solid
                    TRVET55_04.BorderStyle = BorderStyle.Solid
                    TRVET55_05.BorderStyle = BorderStyle.Solid
                    TRVET55_06.BorderStyle = BorderStyle.Solid
                    TRVET55_07.BorderStyle = BorderStyle.Solid

                    TRVET55_00.BorderColor = Drawing.Color.Silver
                    TRVET55_01.BorderColor = Drawing.Color.Silver
                    TRVET55_02.BorderColor = Drawing.Color.Silver
                    TRVET55_03.BorderColor = Drawing.Color.Silver
                    TRVET55_04.BorderColor = Drawing.Color.Silver
                    TRVET55_05.BorderColor = Drawing.Color.Silver
                    TRVET55_06.BorderColor = Drawing.Color.Silver
                    TRVET55_07.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "l", i)
                    RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(0))
                    RH.AddColumn(TRVET55, TRVET55_02, 10, 10, "l", DR(1))
                    RH.AddColumn(TRVET55, TRVET55_03, 10, 10, "l", DR(2))
                    RH.AddColumn(TRVET55, TRVET55_04, 25, 25, "l", DR(3))
                    RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", DR(4))
                    RH.AddColumn(TRVET55, TRVET55_06, 20, 20, "l", DR(5))
                    If (DR(7) = 14) Then
                        RH.AddColumn(TRVET55, TRVET55_07, 5, 5, "c", "<a href='VetUploadExecute.aspx?RequestID=" + DR(6).ToString() + "' style='text-align:right;'><img id='imgReport' src='../Image/approve.png' title='Update' Height='20px' Width='20px' style='cursor:pointer;'/></a>")
                    Else
                        RH.AddColumn(TRVET55, TRVET55_07, 5, 5, "c", "")
                    End If

                    TB.Controls.Add(TRVET55)
                Next
            End If

            '---------------Archiving
            DT_VET = DB.ExecuteDataSet("SELECT v.REQUEST_ID,d.DOCUMENT_NAME,n.NATURE_NAME,p.PARTY_NAME,e.Emp_Name,v.REASON_NOTVET,v.EXECUTE_DT,a.FILE_NAME,a.PkId FROM VET_MASTER v, VET_DOCUMENT d, VET_NATURE_TRANSACTION n, VET_PARTY p, EMP_MASTER e,DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a WHERE v.ARCH_DOCID=d.DOCUMENT_ID and v.SERVICE_ID=d.SERVICE_ID and v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.EXECUTE_BY=e.Emp_Code and d.DOCUMENT_TYPE=3 and v.REQUEST_ID=a.REQUEST_ID and v.STATUS_ID=4 and v.SERVICE_ID=5 and v.REQUEST_DEPT=" & DeptID & "").Tables(0)

            If DT_VET.Rows.Count > 0 Then
                RH.BlankRow(TB, 17)
                RH.SubHeading(TB, 100, "l", "ARCHIVING OF EXECUTED DOCUMENTS")
                RH.BlankRow(TB, 5)

                Dim TRVET0 As New TableRow
                TRVET0.BackColor = Drawing.Color.WhiteSmoke
                Dim TRVET0_00, TRVET0_01, TRVET0_02, TRVET0_03, TRVET0_04, TRVET0_05, TRVET0_06, TRVET0_07 As New TableCell

                TRVET0_00.BorderWidth = "1"
                TRVET0_01.BorderWidth = "1"
                TRVET0_02.BorderWidth = "1"
                TRVET0_03.BorderWidth = "1"
                TRVET0_04.BorderWidth = "1"
                TRVET0_05.BorderWidth = "1"
                TRVET0_06.BorderWidth = "1"
                TRVET0_07.BorderWidth = "1"

                TRVET0_00.BorderStyle = BorderStyle.Solid
                TRVET0_01.BorderStyle = BorderStyle.Solid
                TRVET0_02.BorderStyle = BorderStyle.Solid
                TRVET0_03.BorderStyle = BorderStyle.Solid
                TRVET0_04.BorderStyle = BorderStyle.Solid
                TRVET0_05.BorderStyle = BorderStyle.Solid
                TRVET0_06.BorderStyle = BorderStyle.Solid
                TRVET0_07.BorderStyle = BorderStyle.Solid

                TRVET0_00.BorderColor = Drawing.Color.Silver
                TRVET0_01.BorderColor = Drawing.Color.Silver
                TRVET0_02.BorderColor = Drawing.Color.Silver
                TRVET0_03.BorderColor = Drawing.Color.Silver
                TRVET0_04.BorderColor = Drawing.Color.Silver
                TRVET0_05.BorderColor = Drawing.Color.Silver
                TRVET0_06.BorderColor = Drawing.Color.Silver
                TRVET0_07.BorderColor = Drawing.Color.Silver

                RH.AddColumn(TRVET0, TRVET0_00, 5, 5, "c", "#")
                RH.AddColumn(TRVET0, TRVET0_01, 10, 10, "l", "Document To Archive")
                RH.AddColumn(TRVET0, TRVET0_02, 15, 15, "l", "Nature")
                RH.AddColumn(TRVET0, TRVET0_03, 15, 15, "l", "Party")
                RH.AddColumn(TRVET0, TRVET0_04, 15, 15, "l", "Executant")
                RH.AddColumn(TRVET0, TRVET0_05, 15, 15, "l", "Not Vet Reason")
                RH.AddColumn(TRVET0, TRVET0_06, 10, 10, "l", "Execcute Date")
                RH.AddColumn(TRVET0, TRVET0_07, 15, 15, "l", "Arcchive File")

                i = 0
                TB.Controls.Add(TRVET0)
                RH.BlankRow(TB, 1)

                Dim RequestID As Integer = 1
                Dim NextRequest As Integer = 0

                For Each DR In DT_VET.Rows
                    Dim TRVET55 As New TableRow
                    i = i + 1
                    Dim TRVET55_00, TRVET55_01, TRVET55_02, TRVET55_03, TRVET55_04, TRVET55_05, TRVET55_06, TRVET55_07 As New TableCell

                    TRVET55_00.BorderWidth = "1"
                    TRVET55_01.BorderWidth = "1"
                    TRVET55_02.BorderWidth = "1"
                    TRVET55_03.BorderWidth = "1"
                    TRVET55_04.BorderWidth = "1"
                    TRVET55_05.BorderWidth = "1"
                    TRVET55_06.BorderWidth = "1"
                    TRVET55_07.BorderWidth = "1"

                    TRVET55_00.BorderStyle = BorderStyle.Solid
                    TRVET55_01.BorderStyle = BorderStyle.Solid
                    TRVET55_02.BorderStyle = BorderStyle.Solid
                    TRVET55_03.BorderStyle = BorderStyle.Solid
                    TRVET55_04.BorderStyle = BorderStyle.Solid
                    TRVET55_05.BorderStyle = BorderStyle.Solid
                    TRVET55_06.BorderStyle = BorderStyle.Solid
                    TRVET55_07.BorderStyle = BorderStyle.Solid

                    TRVET55_00.BorderColor = Drawing.Color.Silver
                    TRVET55_01.BorderColor = Drawing.Color.Silver
                    TRVET55_02.BorderColor = Drawing.Color.Silver
                    TRVET55_03.BorderColor = Drawing.Color.Silver
                    TRVET55_04.BorderColor = Drawing.Color.Silver
                    TRVET55_05.BorderColor = Drawing.Color.Silver
                    TRVET55_06.BorderColor = Drawing.Color.Silver
                    TRVET55_07.BorderColor = Drawing.Color.Silver

                    RequestID = DR(0)

                    If RequestID = NextRequest Then
                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", "")
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", "")
                        RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", "")
                        RH.AddColumn(TRVET55, TRVET55_03, 15, 15, "l", "")
                        RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "l", "")
                        RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", "")
                        RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", "")
                    Else
                        RH.AddColumn(TRVET55, TRVET55_00, 5, 5, "c", i)
                        RH.AddColumn(TRVET55, TRVET55_01, 10, 10, "l", DR(1))
                        RH.AddColumn(TRVET55, TRVET55_02, 15, 15, "l", DR(2))
                        RH.AddColumn(TRVET55, TRVET55_03, 15, 15, "l", DR(3))
                        RH.AddColumn(TRVET55, TRVET55_04, 15, 15, "l", DR(4))
                        RH.AddColumn(TRVET55, TRVET55_05, 15, 15, "l", DR(5))
                        RH.AddColumn(TRVET55, TRVET55_06, 10, 10, "l", Format(DR(6), "dd/MMM/yyyy"))
                    End If

                    RH.AddColumn(TRVET55, TRVET55_07, 15, 15, "l", "<a href='ShowFormat.aspx?RequestID=" + DR(0).ToString + "&Value=2 &Id=" + DR(8).ToString + "' style='text-align:right;' target='_blank' >" & DR(7) & "</a>")

                    NextRequest = RequestID
                    TB.Controls.Add(TRVET55)
                Next
            End If
            pnDisplay.Controls.Add(TB)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
