﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Vet_VetApproveHead
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT_VET As New DataTable
    Dim RequestID, UserID As Integer
#Region "Page Load & Dispose"
    Protected Sub Vet_VettingTransaction_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            RequestID = CInt(Request.QueryString.Get("RequestID"))
            Me.hdnRequest.Value = CStr(RequestID)
            UserID = CInt(Session("UserID"))
            DT = DB.ExecuteDataSet("select COUNT(Department_ID) from DEPARTMENT_MASTER where Department_Head=" & UserID & "").Tables(0)
            If DT.Rows.Count > 0 Then
                If CInt(DT.Rows(0)(0)) > 0 Then
                    Me.hdnIsHOD.Value = CStr(1)
                Else
                    Me.hdnIsHOD.Value = CStr(0)
                End If
            End If
            Me.Master.subtitle = "Vetting for executing transactions"

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

            Me.cmdView.Attributes.Add("onclick", "return viewAttachment(1)")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        UserID = CInt(Session("UserID"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Vet Request For Approve
                DT = GF.GetQueryResult("select p.PARTY_NAME+'Æ'+upper(v.STATEMENT_FACT)+'Æ'+n.NATURE_NAME+'Æ'+isnull((select distinct convert(varchar(10),case when a.CON_RESOLUTION IS NOT NULL then 1 else 0 end) from DMS_ESFB.dbo.VET_REQUEST_ATTACHMENT a where a.REQUEST_ID=v.REQUEST_ID),0)+'Æ'+ isnull((select convert(varchar(10),count( m.REQUEST_ID)) from VET_TRANSACTION_MASTER m where m.REQUEST_ID=v.REQUEST_ID),0) from VET_MASTER v, VET_NATURE_TRANSACTION n, VET_PARTY p where v.NATURE_ID=n.NATURE_ID and v.PARTY_ID=p.PARTY_ID and v.STATUS_ID in(0,17) and v.SERVICE_ID=4 and v.REQUEST_ID=" & RequestID & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn = DT.Rows(0)(0).ToString() + "Æ"

                    DT_VET = GF.GetQueryResult("SELECT d.DOCUMENT_NAME+'¶'+e.Emp_Name+'¶'+CONVERT(varchar(10),v.VET_FLAG)+'¶'+CONVERT(varchar(10),v.VET_ID) from VET_TRANSACTION_MASTER v, VET_DOCUMENT d,EMP_MASTER e where v.DOC_ID=d.DOCUMENT_ID and d.SERVICE_ID=4 and v.EXECUTE_BY=e.Emp_Code and v.REQUEST_ID=" & RequestID & "")
                    If DT_VET.Rows.Count > 0 Then
                        For Each DR In DT_VET.Rows
                            CallBackReturn += CStr(DR(0)) + "Ř"
                        Next
                    End If
                End If
            Case 2 'Approve Confirm
                Dim RequestID As Integer = CInt(Data(1))
                Dim IsHOD As Integer = CInt(Data(2))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(6) As SqlParameter
                    Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(0).Value = RequestID
                    Params(1) = New SqlParameter("@AppStatus", SqlDbType.Int)
                    Params(1).Value = 0
                    Params(2) = New SqlParameter("@RejectRes", SqlDbType.VarChar, 1000)
                    Params(2).Value = ""
                    Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(3).Value = UserID
                    Params(4) = New SqlParameter("@IsHOD", SqlDbType.Int)
                    Params(4).Value = IsHOD
                    Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(6).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_REQ_APPROVE_VET", Params)
                    ErrorFlag = CInt(Params(5).Value)
                    Message = CStr(Params(6).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
            Case 3
                Dim RequestID As Integer = CInt(Data(1))
                Dim IsHOD As Integer = CInt(Data(2))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(6) As SqlParameter
                    Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(0).Value = RequestID
                    Params(1) = New SqlParameter("@AppStatus", SqlDbType.Int)
                    Params(1).Value = 1
                    Params(2) = New SqlParameter("@RejectRes", SqlDbType.VarChar, 1000)
                    Params(2).Value = ""
                    Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(3).Value = UserID
                    Params(4) = New SqlParameter("@IsHOD", SqlDbType.Int)
                    Params(4).Value = IsHOD
                    Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(6).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_REQ_APPROVE_VET", Params)
                    ErrorFlag = CInt(Params(5).Value)
                    Message = CStr(Params(6).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
            Case 4 'Reject Confirm
                Dim RequestID As Integer = CInt(Data(1))
                Dim Reason As String = CStr(Data(2))
                Dim IsHOD As Integer = CInt(Data(3))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(6) As SqlParameter
                    Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                    Params(0).Value = RequestID
                    Params(1) = New SqlParameter("@AppStatus", SqlDbType.Int)
                    Params(1).Value = 2
                    Params(2) = New SqlParameter("@RejectRes", SqlDbType.VarChar, 1000)
                    Params(2).Value = Reason
                    Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(3).Value = UserID
                    Params(4) = New SqlParameter("@IsHOD", SqlDbType.Int)
                    Params(4).Value = IsHOD
                    Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(6).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_VET_REQ_APPROVE_VET", Params)
                    ErrorFlag = CInt(Params(5).Value)
                    Message = CStr(Params(6).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region

End Class
