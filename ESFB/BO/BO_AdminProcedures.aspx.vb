﻿
'Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BO_AdminProcedures
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTATTEND1 As New DataTable
    Dim DTATTEND2 As New DataTable
    Dim DTATTEND3 As New DataTable
    Dim DTATTEND4 As New DataTable
    Dim DTA1 As New DataTable
    Dim DTA2 As New DataTable
    Dim DTA3 As New DataTable
    Dim DTA4 As New DataTable
    Dim DTPREMISE As New DataTable
    Dim DTDATA6 As New DataTable
    Dim DTCOMBO As New DataTable
    Dim DTINTIMATE_FROM1 As New DataTable
    Dim DTINTIMATE_FROM2 As New DataTable
    Dim DTINTIMATE_FROM3 As New DataTable
    Dim DTINTIMATE_FROM4 As New DataTable
    Dim DTINTIMATE_1 As New DataTable
    Dim DTINTIMATE_2 As New DataTable
    Dim DTINTIMATE_3 As New DataTable
    Dim DTINTIMATE_4 As New DataTable
    Dim DTINTIMATE_TO1 As New DataTable
    Dim DTINTIMATE_TO2 As New DataTable
    Dim DTINTIMATE_TO3 As New DataTable
    Dim DTINTIMATE_TO4 As New DataTable
    Dim DT_TO_I1 As New DataTable
    Dim DT_TO_I2 As New DataTable
    Dim DT_TO_I3 As New DataTable
    Dim DT_TO_I4 As New DataTable
    Dim DTA6 As New DataTable
    Dim DTA7 As New DataTable
    Dim DTA8 As New DataTable
    Dim DTI1 As New DataTable
    Dim DTI2 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Dim BO_Master_Id As Integer
    Dim AttachImage As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1269) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Admin Procedures"
            Dim UserID As Integer = CInt(Session("UserID"))
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.hidUser.Value = CStr(UserID)
            Dim BO_Master_Id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))
            'Dim BO_Master_Id As Integer = 17
            hidmaster.Value = CStr(BO_Master_Id)
            DTINTIMATE_FROM1 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 2 AND INTIMATE_TO_TEAM = 2 AND BO_MASTER_ID = " + BO_Master_Id.ToString()).Tables(0)
            If DTINTIMATE_FROM1.Rows.Count <> 0 Then
                DTINTIMATE_1 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 2 AND INTIMATE_TO_TEAM = 2 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND ATTEND_BY_TEAM IS NOT NULL AND WORK_ONGOING = 1").Tables(0)
                If DTINTIMATE_1.Rows.Count = 0 Then
                    hidSection.Value = "3"
                    hidenable1.Value = "0"
                Else
                    hidenable1.Value = "1"
                    hidSection.Value = "3"
                End If
            End If

            DTATTEND1 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 2 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2").Tables(0)
            If DTATTEND1.Rows.Count <> 0 Then
                DTA1 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 2 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2 AND ATTEND_BY_TEAM IS NULL").Tables(0)
                If DTA1.Rows.Count <> 0 Then
                    hidAttend1.Value = "0"
                    hidIntimate3.Value = "0"
                Else
                    hidAttend1.Value = "1"
                    hidIntimate3.Value = "1"
                End If
            End If
            'DTINTIMATE_TO1 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 2 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2").Tables(0)
            'If DTINTIMATE_TO1.Rows.Count <> 0 Then
            '    DT_TO_I1 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 2 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2 AND ATTEND_BY_TEAM IS NULL").Tables(0)
            '    If DT_TO_I1.Rows.Count <> 0 Then
            '        hidIntimate3.Value = "0"
            '    Else
            '        hidIntimate3.Value = "1"
            '    End If
            'End If


            ' SECTION 4
            DTINTIMATE_FROM2 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 3 AND INTIMATE_TO_TEAM = 2 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND ATTEND_BY_TEAM IS NULL").Tables(0)
            If DTINTIMATE_FROM2.Rows.Count <> 0 Then
                DTINTIMATE_2 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 3 AND INTIMATE_TO_TEAM = 2 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND ATTEND_BY_TEAM IS NOT NULL AND WORK_ONGOING = 1").Tables(0)
                If DTINTIMATE_2.Rows.Count = 0 Then
                    hidSection.Value = "4"
                    hidenable2.Value = "0"
                Else
                    hidenable2.Value = "1"
                    hidSection.Value = "4"
                End If

            End If

            DTATTEND2 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 3 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2").Tables(0)
            If DTATTEND2.Rows.Count <> 0 Then
                DTA2 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 3 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2 AND ATTEND_BY_TEAM IS NULL").Tables(0)
                If DTA2.Rows.Count <> 0 Then
                    hidAttend2.Value = "0"
                    hidIntimate4.Value = "0"
                Else
                    hidAttend2.Value = "1"
                    hidIntimate4.Value = "1"
                End If
            End If
            'DTINTIMATE_TO2 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 3 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2").Tables(0)
            'If DTINTIMATE_TO2.Rows.Count <> 0 Then
            '    DT_TO_I2 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 3 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2 AND ATTEND_BY_TEAM IS NULL").Tables(0)
            '    If DT_TO_I2.Rows.Count <> 0 Then
            '        hidIntimate3.Value = "0"
            '    Else
            '        hidIntimate3.Value = "1"
            '    End If
            'End If


            'Section 5

            DTINTIMATE_FROM3 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 4 AND INTIMATE_TO_TEAM = 2 AND BO_MASTER_ID = " + BO_Master_Id.ToString()).Tables(0)
            If DTINTIMATE_FROM3.Rows.Count <> 0 Then
                DTINTIMATE_3 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 4 AND INTIMATE_TO_TEAM = 2 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND ATTEND_BY_TEAM IS NOT NULL AND WORK_ONGOING = 1").Tables(0)
                If DTINTIMATE_3.Rows.Count = 0 Then
                    hidSection.Value = "5"
                    hidenable3.Value = "0"
                Else
                    hidenable3.Value = "1"
                    hidSection.Value = "5"
                End If
            End If

            DTATTEND3 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 4 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2").Tables(0)
            If DTATTEND3.Rows.Count <> 0 Then
                DTA3 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 4 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2 AND ATTEND_BY_TEAM IS NULL").Tables(0)
                If DTA3.Rows.Count <> 0 Then
                    hidAttend5.Value = "0"

                Else
                    hidAttend5.Value = "1"

                End If
            End If
            DTINTIMATE_TO3 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 4 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2").Tables(0)
            If DTINTIMATE_TO3.Rows.Count <> 0 Then
                DT_TO_I3 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 5 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 2").Tables(0)
                If DT_TO_I3.Rows.Count <> 0 Then
                    hidIntimate5.Value = "1"
                Else
                    hidIntimate5.Value = "0"
                End If
            End If

            '  Section 6

            DTINTIMATE_FROM4 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 5 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND INTIMATE_TO_TEAM = 2 ").Tables(0)
            If DTINTIMATE_FROM4.Rows.Count <> 0 Then
                DTINTIMATE_4 = DB.ExecuteDataSet("SELECT * FROM BO_PREMISE_DTL WHERE  BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND prop_appr_status_id = 3 ").Tables(0)
                If DTINTIMATE_4.Rows.Count = 0 Then
                    hidenable4.Value = "0"
                Else
                    hidenable4.Value = "1"
                    hidSection.Value = "6"
                End If
            End If



            DTATTEND4 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 5 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1").Tables(0)
            If DTATTEND4.Rows.Count <> 0 Then
                DTA4 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 5 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1 AND ATTEND_BY_TEAM IS NULL").Tables(0)
                If DTA4.Rows.Count <> 0 Then
                    hidAttend6.Value = "0"
                Else
                    hidAttend6.Value = "1"
                End If
            End If
            DTINTIMATE_TO4 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 5 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1").Tables(0)
            If DTINTIMATE_TO4.Rows.Count <> 0 Then
                DT_TO_I4 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 5 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1 AND ATTEND_BY_TEAM IS NULL").Tables(0)
                If DT_TO_I4.Rows.Count <> 0 Then
                    hidIntimate6.Value = "0"
                Else
                    hidIntimate6.Value = "1"
                End If
            End If




            DT = DB.ExecuteDataSet("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 7 and Status_id = 1 order by 1").Tables(0)
            For Each b1 In DT.Rows
                Me.hidBuildingStatus.Value += b1(0).ToString() + "ÿ" + b1(1).ToString() + "Ñ"
            Next
            DT = DB.ExecuteDataSet("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 8 and Status_id = 1 order by 1").Tables(0)
            For Each b1 In DT.Rows
                Me.hidLeadStatus.Value += b1(0).ToString() + "ÿ" + b1(1).ToString() + "Ñ"
            Next
            DT = DB.ExecuteDataSet("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 9 and Status_id = 1 order by 1").Tables(0)
            For Each b1 In DT.Rows
                Me.hidPremiseStatus.Value += b1(0).ToString() + "ÿ" + b1(1).ToString() + "Ñ"
            Next
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 10 and Status_id = 1 order by 1")
            GF.ComboFill(cmbScopeOfCivilWork, DT, 0, 1)
           
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 11 and Status_id = 1 order by 1")
            GF.ComboFill(cmbProposalApproval, DT, 0, 1)
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 11 and Status_id = 1 order by 1")
            GF.ComboFill(cmbLegalClearance, DT, 0, 1)
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 11 and Status_id = 1 order by 1")
            GF.ComboFill(cmbStatusInterior, DT, 0, 1)

            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,Caption,Description from dms_ESFB.dbo.BO_BUSINESS_UPLOAD where BO_Master_Id = " + BO_Master_Id.ToString() + " And BO_Section_Id = 3 And BO_Premise_Id = 0").Tables(0)
            If DT.Rows.Count <> 0 Then
                Me.hid_Sec1_Attachment.Value = DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString()
                UploadedAttachment1.Visible = True
                'UploadedAttachment1.Attributes.Add("href", DT.Rows(0)(1).ToString())
                UploadedAttachment1.InnerText = DT.Rows(0)(2).ToString()
                Remove1.Visible = True
                fup1.Visible = False
                Upload.Visible = False
            Else
                fup1.Visible = True
                Upload.Visible = True
                UploadedAttachment1.Visible = False
                Remove1.Visible = False
            End If

            DTPREMISE = DB.ExecuteDataSet("SELECT * FROM BO_PREMISE_DTL WHERE BO_MASTER_ID = " + BO_Master_Id.ToString()).Tables(0)
            Dim PremData As String = ""
            'For Each dtprow In DTPREMISE.Rows
            For n As Integer = 0 To DTPREMISE.Rows.Count - 1
                For i As Integer = 0 To 47
                    PremData += DTPREMISE(n)(i).ToString() + "µ"
                Next
                PremData += "¥"
            Next
            Me.hidPremiseDtl.Value = PremData

            'section 5 show

            If DTPREMISE.Rows(0)(40).ToString() <> "" Then
                If CInt(DTPREMISE.Rows(0)(40)) <> -1 Then
                    Me.cmbProposalApproval.SelectedValue = CInt(DTPREMISE.Rows(0)(40))
                End If
            End If
            If DTPREMISE.Rows(0)(41).ToString() <> "" Then
                Me.txtProposalApproval.Text = CDate(DTPREMISE.Rows(0)(41).ToString())
            End If
            If DTPREMISE.Rows(0)(42).ToString() <> "" Then
                If CInt(DTPREMISE.Rows(0)(42)) <> -1 Then
                    Me.cmbLegalClearance.SelectedValue = CInt(DTPREMISE.Rows(0)(42))
                End If
            End If
            If DTPREMISE.Rows(0)(43).ToString() <> "" Then
                Me.txtLegalClearance.Text = CDate(DTPREMISE.Rows(0)(43).ToString())
            End If
            If DTPREMISE.Rows(0)(44).ToString() <> "" Then
                Me.txtLegalAgreementSigned.Text = CDate(DTPREMISE.Rows(0)(44).ToString())
            End If
            'section 6 data show

            DTDATA6 = DB.ExecuteDataSet("SELECT * FROM BO_ADMIN_PROC WHERE BO_MASTER_ID  = " + BO_Master_Id.ToString()).Tables(0)
            If DTDATA6.Rows(0)(1).ToString() <> "" Then
                If CInt(DTDATA6.Rows(0)(1)) <> -1 Then
                    Me.cmbScopeOfCivilWork.SelectedValue = CInt(DTDATA6.Rows(0)(1))
                End If
            End If

            If DTDATA6.Rows(0)(2).ToString() <> "" Then
                Me.txtLordScopeWork.Text = CDate(DTDATA6.Rows(0)(2).ToString())
            End If
            If DTDATA6.Rows(0)(3).ToString() <> "" Then
                Me.txtRentstartDate.Text = CDate(DTDATA6.Rows(0)(3).ToString())
            End If
            If DTDATA6.Rows(0)(4).ToString() <> "" Then
                Me.txtWoDate.Text = CDate(DTDATA6.Rows(0)(4).ToString())
            End If
            If DTDATA6.Rows(0)(5).ToString() <> "" Then
                Me.txtVendorDetail.Text = DTDATA6.Rows(0)(5).ToString()
            End If
            If DTDATA6.Rows(0)(6).ToString() <> "" Then
                Me.txtInteriorWorkDate.Text = CDate(DTDATA6.Rows(0)(6).ToString())
            End If
            If DTDATA6.Rows(0)(7).ToString() <> "" Then
                If CInt(DTDATA6.Rows(0)(7)) <> -1 Then
                    Me.cmbStatusInterior.SelectedValue = CInt(DTDATA6.Rows(0)(7))
                End If
            End If
            If DTDATA6.Rows(0)(8).ToString() <> "" Then
                Me.txtGenerationInstall.Text = CDate(DTDATA6.Rows(0)(8).ToString())
            End If
            If DTDATA6.Rows(0)(9).ToString() <> "" Then
                Me.txtUPSInstall.Text = CDate(DTDATA6.Rows(0)(9).ToString())
            End If
            If DTDATA6.Rows(0)(10).ToString() <> "" Then
                Me.txt3phaseConnection.Text = CDate(DTDATA6.Rows(0)(10).ToString())
            End If
            If DTDATA6.Rows(0)(11).ToString() <> "" Then
                Me.txtCCTVComplete.Text = CDate(DTDATA6.Rows(0)(11).ToString())
            End If
            If DTDATA6.Rows(0)(12).ToString() <> "" Then
                Me.txtFireAlarmComplete.Text = CDate(DTDATA6.Rows(0)(12).ToString())
            End If
            If DTDATA6.Rows(0)(13).ToString() <> "" Then
                Me.txtBrandingCompletion.Text = CDate(DTDATA6.Rows(0)(13).ToString())
            End If
            If DTDATA6.Rows(0)(14).ToString() <> "" Then
                Me.txtSRD.Text = CDate(DTDATA6.Rows(0)(14).ToString())
            End If
            If DTDATA6.Rows(0)(15).ToString() <> "" Then
                Me.txtSDL.Text = CDate(DTDATA6.Rows(0)(15).ToString())
            End If
            If DTDATA6.Rows(0)(16).ToString() <> "" Then
                Me.txtSafe.Text = CDate(DTDATA6.Rows(0)(16).ToString())
            End If
            If DTDATA6.Rows(0)(17).ToString() <> "" Then
                Me.txtAlmirah.Text = CDate(DTDATA6.Rows(0)(17).ToString())
            End If
            If DTDATA6.Rows(0)(18).ToString() <> "" Then
                Me.TextAC.Text = CDate(DTDATA6.Rows(0)(18).ToString())
            End If
            If DTDATA6.Rows(0)(19).ToString() <> "" Then
                Me.txtUPSBank.Text = CDate(DTDATA6.Rows(0)(19).ToString())
            End If
            If DTDATA6.Rows(0)(20).ToString() <> "" Then
                Me.txtUPSATM.Text = CDate(DTDATA6.Rows(0)(20).ToString())
            End If
            If DTDATA6.Rows(0)(21).ToString() <> "" Then
                Me.txtFireExtinguisher.Text = CDate(DTDATA6.Rows(0)(21).ToString())
            End If
            If DTDATA6.Rows(0)(22).ToString() <> "" Then
                Me.txtCTS_Scanner.Text = CDate(DTDATA6.Rows(0)(22).ToString())
            End If
            If DTDATA6.Rows(0)(23).ToString() <> "" Then
                Me.txtUVLamp.Text = CDate(DTDATA6.Rows(0)(23).ToString())
            End If
            If DTDATA6.Rows(0)(24).ToString() <> "" Then
                Me.txtCashCountingMachine.Text = CDate(DTDATA6.Rows(0)(24).ToString())
            End If
            If DTDATA6.Rows(0)(25).ToString() <> "" Then
                Me.txtChairs.Text = CDate(DTDATA6.Rows(0)(25).ToString())
            End If
            If DTDATA6.Rows(0)(26).ToString() <> "" Then
                Me.txtBrandingExterior.Text = CDate(DTDATA6.Rows(0)(26).ToString())
            End If
            If DTDATA6.Rows(0)(27).ToString() <> "" Then
                Me.txtBrandingInterior.Text = CDate(DTDATA6.Rows(0)(27).ToString())
            End If
            If DTDATA6.Rows(0)(28).ToString() <> "" Then
                Me.txtCCTV.Text = CDate(DTDATA6.Rows(0)(28).ToString())
            End If
            If DTDATA6.Rows(0)(29).ToString() <> "" Then
                If CInt(DTDATA6.Rows(0)(29)) <> -1 Then
                    Me.cmbCCTV.SelectedValue = CInt(DTDATA6.Rows(0)(29))
                End If
            End If
            If DTDATA6.Rows(0)(30).ToString() <> "" Then
                If CInt(DTDATA6.Rows(0)(30)) <> -1 Then
                    Me.cmbFireAlarm.SelectedValue = CInt(DTDATA6.Rows(0)(30))
                End If
            End If
            If DTDATA6.Rows(0)(31).ToString() <> "" Then
                If CInt(DTDATA6.Rows(0)(31)) <> -1 Then
                    Me.cmbBrandingCompletion.SelectedValue = CInt(DTDATA6.Rows(0)(31))
                End If
            End If

            If GF.CheckdeptUser(CInt(Session("UserID")), 2) = False Then
                hidenable1.Value = "0"
            End If

            'If DTDATA1.Rows(0)(11).ToString() <> "" Then
            '    If CInt(DTDATA1.Rows(0)(11)) <> -1 Then
            '        DT1 = DB.ExecuteDataSet("select combo_text from bo_combo_data where identify_id = 1 and combo_id = " + DTDATA1.Rows(0)(11).ToString()).Tables(0)
            '        Me.txtCommMode.Text = DT1.Rows(0)(0).ToString()
            '    End If
            'End If




            'Me.hidUser.Value = CStr(UserID)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then

            Dim Data1 As String() = CStr(Data(1)).Split(CChar("ÿ"))
            Dim Data2 As String() = CStr(Data1(0)).Split(CChar("₤"))
            Dim Data3 As String() = CStr(Data1(1)).Split(CChar("₤"))
            Dim Data4 As String() = CStr(Data1(2)).Split(CChar("₤"))
            Dim Data5 As String() = CStr(Data1(3)).Split(CChar("₤"))
            Dim Data6 As String() = CStr(Data1(4)).Split(CChar("₤"))
            Dim Data7 As String() = CStr(Data1(5)).Split(CChar("₤"))
            Dim Data8 As String() = CStr(Data1(6)).Split(CChar("₤"))
            Dim Data9 As String() = CStr(Data1(7)).Split(CChar("₤"))
            Dim Data10 As String() = CStr(Data1(8)).Split(CChar("₤"))
            Dim Data11 As String() = CStr(Data1(9)).Split(CChar("₤"))
            Dim Data12 As String() = CStr(Data1(10)).Split(CChar("₤"))
            Dim Data13 As String() = CStr(Data1(11)).Split(CChar("₤"))
            Dim Data14 As String() = CStr(Data1(12)).Split(CChar("₤"))
            Dim Data15 As String() = CStr(Data1(15)).Split(CChar("₤"))
            Dim Data16 As String() = CStr(Data1(19)).Split(CChar("₤"))
            Dim Data17 As String() = CStr(Data1(20)).Split(CChar("₤"))
            Dim Data18 As String() = CStr(Data1(21)).Split(CChar("₤"))


            Dim columnId As Integer = CInt(Data1(13))
            Dim sectionId As Integer = CInt(Data1(14))
            Dim attend As Integer = CInt(Data1(16))
            Dim intimate As Integer = CInt(Data1(17))
            Dim FinishFlag As Integer = CInt(Data1(18))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                For n As Integer = 0 To columnId - 1

                    Dim DateIntimationRecievedBusiness As Nullable(Of Date)
                    If Data2(n).ToString = "" Then
                        DateIntimationRecievedBusiness = Nothing
                    Else
                        DateIntimationRecievedBusiness = CDate(Data2(n).ToString())
                    End If

                    Dim DatePrelimnaryAgreement As Nullable(Of Date)
                    If Data3(n).ToString = "" Then
                        DatePrelimnaryAgreement = Nothing
                    Else
                        DatePrelimnaryAgreement = CDate(Data3(n).ToString())
                    End If
                    'Dim ExecutionEngineer As Integer = CInt(Data4(n))
                    Dim ExecutionEngineer As Integer
                    If Data4(n).ToString() <> "" Then
                        ExecutionEngineer = CInt(Data4(n).ToString())
                    Else
                        ExecutionEngineer = Nothing
                    End If
                    Dim DateVisitingPlan As Nullable(Of Date)
                    If Data5(n).ToString = "" Then
                        DateVisitingPlan = Nothing
                    Else
                        DateVisitingPlan = CDate(Data5(n).ToString())
                    End If

                    Dim DateVisited As Nullable(Of Date)
                    If Data6(n).ToString = "" Then
                        DateVisited = Nothing
                    Else
                        DateVisited = CDate(Data6(n).ToString())
                    End If
                    'Dim PremiseStausAdmin As Integer = CInt(Data7(n))
                    Dim PremiseStausAdmin As Integer
                    If Data7(n).ToString() <> "" Then
                        If (CInt(Data7(n).ToString()) = -1) Then
                            PremiseStausAdmin = Nothing
                        Else
                            PremiseStausAdmin = CInt(Data7(n).ToString())
                        End If
                    End If
                    Dim CommentBox As String = CStr(Data8(n))

                    Dim BulidingPresStatus As String = CStr(Data9(n))

                    Dim DateProposalToCommittee As Nullable(Of Date)
                    If Data10(n).ToString = "" Then
                        DateProposalToCommittee = Nothing
                    Else
                        DateProposalToCommittee = CDate(Data10(n).ToString())
                    End If
                    Dim AreaSqft As Integer
                    If Data11(n).ToString() <> "" Then
                        AreaSqft = CInt(Data11(n).ToString())
                    Else
                        AreaSqft = Nothing
                    End If
                    Dim RentAgreed As Integer
                    If Data12(n).ToString() <> "" Then
                        RentAgreed = CInt(Data12(n).ToString())
                    Else
                        RentAgreed = Nothing
                    End If
                    Dim Panchayat As String = CStr(Data13(n))
                    Dim Village As String = CStr(Data14(n))
                    Dim PremiseColumnNumber As Integer = CInt(Data15(n))
                    Dim BO_Master_Id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))

                    Dim Landmark As String = CStr(Data16(n))
                    Dim Address As String = CStr(Data17(n))
                    Dim Pincode As Integer
                    If Data18(n).ToString() <> "" Then
                        Pincode = CInt(Data18(n).ToString())
                    Else
                        Pincode = Nothing
                    End If
                    Dim Params(24) As SqlParameter
                    Params(0) = New SqlParameter("@IntimationRecievedBusiness_dt", SqlDbType.Date)
                    Params(0).Value = DateIntimationRecievedBusiness
                    Params(1) = New SqlParameter("@PreliAgreeSigned_dt", SqlDbType.Date)
                    Params(1).Value = DatePrelimnaryAgreement
                    Params(2) = New SqlParameter("@ExecutionEngineer", SqlDbType.Int)
                    Params(2).Value = ExecutionEngineer
                    Params(3) = New SqlParameter("@Visting_Plan_Dt", SqlDbType.Date)
                    Params(3).Value = DateVisitingPlan
                    Params(4) = New SqlParameter("@Visited_Dt", SqlDbType.Date)
                    Params(4).Value = DateVisited
                    Params(5) = New SqlParameter("@Premise_StatusId_Admin", SqlDbType.Int)
                    Params(5).Value = PremiseStausAdmin
                    Params(6) = New SqlParameter("@Admin_Comments", SqlDbType.VarChar, 500)
                    Params(6).Value = CommentBox
                    Params(7) = New SqlParameter("@Buildiing_Pres_Status", SqlDbType.VarChar, 100)
                    Params(7).Value = BulidingPresStatus
                    Params(8) = New SqlParameter("@ProposalToCommittee_dt", SqlDbType.Date)
                    Params(8).Value = DateProposalToCommittee
                    Params(9) = New SqlParameter("@Area_Sqft", SqlDbType.Int)
                    Params(9).Value = AreaSqft
                    Params(10) = New SqlParameter("@Rent_Agreed", SqlDbType.Int)
                    Params(10).Value = RentAgreed
                    Params(11) = New SqlParameter("@Panch_Corporation", SqlDbType.VarChar, 100)
                    Params(11).Value = Panchayat
                    Params(12) = New SqlParameter("@Village", SqlDbType.VarChar, 100)
                    Params(12).Value = Village
                    Params(13) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(13).Direction = ParameterDirection.Output
                    Params(14) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(14).Direction = ParameterDirection.Output
                    Params(15) = New SqlParameter("@SectionID", SqlDbType.Int)
                    Params(15).Value = sectionId
                    Params(16) = New SqlParameter("@Sl_id", SqlDbType.Int)
                    Params(16).Value = PremiseColumnNumber
                    Params(17) = New SqlParameter("@attend", SqlDbType.Int)
                    Params(17).Value = attend
                    Params(18) = New SqlParameter("@Intimate", SqlDbType.Int)
                    Params(18).Value = intimate
                    Params(19) = New SqlParameter("@FinishFlag", SqlDbType.Int)
                    Params(19).Value = FinishFlag
                    Params(20) = New SqlParameter("@userId", SqlDbType.Int)
                    Params(20).Value = Session("UserID")
                    Params(21) = New SqlParameter("@BoMasterId", SqlDbType.Int)
                    Params(21).Value = BO_Master_Id
                    Params(22) = New SqlParameter("@Landmark", SqlDbType.VarChar, 100)
                    Params(22).Value = Landmark
                    Params(23) = New SqlParameter("@address", SqlDbType.VarChar, 500)
                    Params(23).Value = Address
                    Params(24) = New SqlParameter("@pincode", SqlDbType.Int)
                    Params(24).Value = Pincode
                    DB.ExecuteNonQuery("SP_BO_ADMIN_PROCEDURES", Params)
                    ErrorFlag = CInt(Params(13).Value)
                    Message = CStr(Params(14).Value)
                Next
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()

        ElseIf CInt(Data(0)) = 2 Then
            Dim Data1 As String() = CStr(Data(1)).Split(CChar("ÿ"))
            Dim ProposalApprovalCombo As Integer = CInt(Data1(0))
            'Dim ProposalApprovalDate As Date = CDate(Data1(1))
            Dim ProposalApprovalDate As Nullable(Of Date)
            If Data1(1).ToString = "" Then
                ProposalApprovalDate = Nothing
            Else
                ProposalApprovalDate = CDate(Data1(1).ToString())
            End If

            Dim LegalClearanceCombo As Integer = CInt(Data1(2))
            'Dim LegalClearanceDate As Date = CDate(Data1(3))
            Dim LegalClearanceDate As Nullable(Of Date)
            If Data1(3).ToString = "" Then
                LegalClearanceDate = Nothing
            Else
                LegalClearanceDate = CDate(Data1(3).ToString())
            End If
            'Dim LegalAgreementSignedDate As Date = CDate(Data1(4))
            Dim LegalAgreementSignedDate As Nullable(Of Date)
            If Data1(4).ToString = "" Then
                LegalAgreementSignedDate = Nothing
            Else
                LegalAgreementSignedDate = CDate(Data1(4).ToString())
            End If
            Dim sectionId As Integer = CInt(Data1(5))
            Dim FinishFlag As Integer = CInt(Data1(6))
            Dim attend As Integer = CInt(Data1(7))
            Dim intimate As Integer = CInt(Data1(8))
            Dim BO_Master_Id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))
            ''Dim LegalAgreementAttachment As Integer = CInt(Data1(1))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(12) As SqlParameter
                Params(0) = New SqlParameter("@ProposalApprovalStatus", SqlDbType.Int)
                Params(0).Value = ProposalApprovalCombo
                Params(1) = New SqlParameter("@ProposalApproval_Dt", SqlDbType.Date)
                Params(1).Value = ProposalApprovalDate
                Params(2) = New SqlParameter("@LegalClearanceStatus", SqlDbType.Int)
                Params(2).Value = LegalClearanceCombo
                Params(3) = New SqlParameter("@LegalClearance_Dt", SqlDbType.Date)
                Params(3).Value = LegalClearanceDate
                Params(4) = New SqlParameter("@LegalAgreementSigned_Dt", SqlDbType.Date)
                Params(4).Value = LegalAgreementSignedDate
                Params(5) = New SqlParameter("@SectionID", SqlDbType.Int)
                Params(5).Value = sectionId
                Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@userId", SqlDbType.Int)
                Params(8).Value = Session("UserID")
                Params(9) = New SqlParameter("@attend", SqlDbType.Int)
                Params(9).Value = attend
                Params(10) = New SqlParameter("@Intimate", SqlDbType.Int)
                Params(10).Value = intimate
                Params(11) = New SqlParameter("@FinishFlag", SqlDbType.Int)
                Params(11).Value = FinishFlag
                Params(12) = New SqlParameter("@BoMasterId", SqlDbType.Int)
                Params(12).Value = BO_Master_Id
                DB.ExecuteNonQuery("SP_BO_ADMIN_PROCEDURES", Params)
                ErrorFlag = CInt(Params(6).Value)
                Message = CStr(Params(7).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 3 Then
            Dim Data1 As String() = CStr(Data(1)).Split(CChar("ÿ"))
            Dim ScopeOfCivilWorkStatus As Integer = CInt(Data1(0))
            'Dim LordScopeWork As Date = CDate(Data1(1))
            Dim LordScopeWork As Nullable(Of Date)
            If Data1(1).ToString = "" Then
                LordScopeWork = Nothing
            Else
                LordScopeWork = CDate(Data1(1).ToString())
            End If
            'Dim RentstartDate As Date = CDate(Data1(2))
            Dim RentstartDate As Nullable(Of Date)
            If Data1(2).ToString = "" Then
                RentstartDate = Nothing
            Else
                RentstartDate = CDate(Data1(2).ToString())
            End If
            'Dim WoDate As Date = CDate(Data1(3))
            Dim WoDate As Nullable(Of Date)
            If Data1(3).ToString = "" Then
                WoDate = Nothing
            Else
                WoDate = CDate(Data1(3).ToString())
            End If
            Dim VendorDetail As String = CStr(Data1(4))
            'Dim InteriorWorkDate As Date = CDate(Data1(5))
            Dim InteriorWorkDate As Nullable(Of Date)
            If Data1(5).ToString = "" Then
                InteriorWorkDate = Nothing
            Else
                InteriorWorkDate = CDate(Data1(5).ToString())
            End If
            'Dim InteriorWorkStatus As Integer = CInt(Data1(6))
            Dim InteriorWorkStatus As Integer
            If Data1(6).ToString() <> "" Then
                If (CInt(Data1(6).ToString()) = -1) Then
                    InteriorWorkStatus = Nothing
                Else
                    InteriorWorkStatus = CInt(Data1(6).ToString())
                End If
            End If
            'Dim GenerationInstall As Date = CDate(Data1(7))
            Dim GenerationInstall As Nullable(Of Date)
            If Data1(7).ToString = "" Then
                GenerationInstall = Nothing
            Else
                GenerationInstall = CDate(Data1(7).ToString())
            End If
            'Dim UPSInstall As Date = CDate(Data1(8))
            Dim UPSInstall As Nullable(Of Date)
            If Data1(8).ToString = "" Then
                UPSInstall = Nothing
            Else
                UPSInstall = CDate(Data1(8).ToString())
            End If
            'Dim phaseConnection As Date = CDate(Data1(9))
            Dim phaseConnection As Nullable(Of Date)
            If Data1(9).ToString = "" Then
                phaseConnection = Nothing
            Else
                phaseConnection = CDate(Data1(9).ToString())
            End If
            'Dim CCTV As Date = CDate(Data1(10))
            Dim CCTVCompletionStatus As Integer
            If Data1(10).ToString() <> "" Then
                If (CInt(Data1(10).ToString()) = -1) Then
                    CCTVCompletionStatus = Nothing
                Else
                    CCTVCompletionStatus = CInt(Data1(10).ToString())
                End If
            End If
            Dim CCTVCompletionDate As Nullable(Of Date)
            If Data1(11).ToString = "" Then
                CCTVCompletionDate = Nothing
            Else
                CCTVCompletionDate = CDate(Data1(11).ToString())
            End If
            'Dim FireAlarm As Date = CDate(Data1(11))
            Dim FireAlarmCompletionStatus As Integer
            If Data1(12).ToString() <> "" Then
                If (CInt(Data1(12).ToString()) = -1) Then
                    FireAlarmCompletionStatus = Nothing
                Else
                    FireAlarmCompletionStatus = CInt(Data1(12).ToString())
                End If
            End If
            Dim FireAlarmCompletionDate As Nullable(Of Date)
            If Data1(13).ToString = "" Then
                FireAlarmCompletionDate = Nothing
            Else
                FireAlarmCompletionDate = CDate(Data1(13).ToString())
            End If
            'Dim BrandingCompletion As Date = CDate(Data1(12))
            Dim BrandingCompletionStatus As Integer
            If Data1(14).ToString() <> "" Then
                If (CInt(Data1(14).ToString()) = -1) Then
                    BrandingCompletionStatus = Nothing
                Else
                    BrandingCompletionStatus = CInt(Data1(14).ToString())
                End If
            End If
            Dim BrandingCompletionDate As Nullable(Of Date)
            If Data1(15).ToString = "" Then
                BrandingCompletionDate = Nothing
            Else
                BrandingCompletionDate = CDate(Data1(15).ToString())
            End If
            'Dim SRD As Date = CDate(Data1(13))
            Dim SRD As Nullable(Of Date)
            If Data1(16).ToString = "" Then
                SRD = Nothing
            Else
                SRD = CDate(Data1(16).ToString())
            End If
            'Dim SDL As Date = CDate(Data1(14))
            Dim SDL As Nullable(Of Date)
            If Data1(17).ToString = "" Then
                SDL = Nothing
            Else
                SDL = CDate(Data1(17).ToString())
            End If
            'Dim Safe As Date = CDate(Data1(15))
            Dim Safe As Nullable(Of Date)
            If Data1(18).ToString = "" Then
                Safe = Nothing
            Else
                Safe = CDate(Data1(18).ToString())
            End If
            'Dim Almirah As Date = CDate(Data1(16))
            Dim Almirah As Nullable(Of Date)
            If Data1(19).ToString = "" Then
                Almirah = Nothing
            Else
                Almirah = CDate(Data1(19).ToString())
            End If
            'Dim AC As Date = CDate(Data1(17))
            Dim AC As Nullable(Of Date)
            If Data1(20).ToString = "" Then
                AC = Nothing
            Else
                AC = CDate(Data1(20).ToString())
            End If
            'Dim UPSBank As Date = CDate(Data1(18))
            Dim UPSBank As Nullable(Of Date)
            If Data1(21).ToString = "" Then
                UPSBank = Nothing
            Else
                UPSBank = CDate(Data1(21).ToString())
            End If
            'Dim UPSATM As Date = CDate(Data1(19))
            Dim UPSATM As Nullable(Of Date)
            If Data1(22).ToString = "" Then
                UPSATM = Nothing
            Else
                UPSATM = CDate(Data1(22).ToString())
            End If
            'Dim FireExtinguisher As Date = CDate(Data1(20))
            Dim FireExtinguisher As Nullable(Of Date)
            If Data1(23).ToString = "" Then
                FireExtinguisher = Nothing
            Else
                FireExtinguisher = CDate(Data1(23).ToString())
            End If
            'Dim CTS_Scanner As Date = CDate(Data1(21))
            Dim CTS_Scanner As Nullable(Of Date)
            If Data1(24).ToString = "" Then
                CTS_Scanner = Nothing
            Else
                CTS_Scanner = CDate(Data1(24).ToString())
            End If
            'Dim UVLamp As Date = CDate(Data1(22))
            Dim UVLamp As Nullable(Of Date)
            If Data1(25).ToString = "" Then
                UVLamp = Nothing
            Else
                UVLamp = CDate(Data1(25).ToString())
            End If
            'Dim CashCountingMachine As Date = CDate(Data1(23))
            Dim CashCountingMachine As Nullable(Of Date)
            If Data1(26).ToString = "" Then
                CashCountingMachine = Nothing
            Else
                CashCountingMachine = CDate(Data1(26).ToString())
            End If
            'Dim Chairs As Date = CDate(Data1(24))
            Dim Chairs As Nullable(Of Date)
            If Data1(27).ToString = "" Then
                Chairs = Nothing
            Else
                Chairs = CDate(Data1(27).ToString())
            End If
            'Dim BrandingExterior As Date = CDate(Data1(25))
            Dim BrandingExterior As Nullable(Of Date)
            If Data1(28).ToString = "" Then
                BrandingExterior = Nothing
            Else
                BrandingExterior = CDate(Data1(28).ToString())
            End If
            'Dim BrandingInterior As Date = CDate(Data1(26))
            Dim BrandingInterior As Nullable(Of Date)
            If Data1(29).ToString = "" Then
                BrandingInterior = Nothing
            Else
                BrandingInterior = CDate(Data1(29).ToString())
            End If
            'Dim CCTVDate As Date = CDate(Data1(27))
            Dim CCTVDate As Nullable(Of Date)
            If Data1(30).ToString = "" Then
                CCTVDate = Nothing
            Else
                CCTVDate = CDate(Data1(30).ToString())
            End If

            Dim sectionId As Integer = CInt(Data1(31))
            Dim FinishFlag As Integer = CInt(Data1(32))
            Dim attend As Integer = CInt(Data1(33))
            Dim intimate As Integer = CInt(Data1(34))
            Dim BO_Master_Id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(38) As SqlParameter
                Params(0) = New SqlParameter("@ScopeCivilWorkID", SqlDbType.Int)
                Params(0).Value = ScopeOfCivilWorkStatus
                Params(1) = New SqlParameter("@ScopeCivilCompDate", SqlDbType.Date)
                Params(1).Value = LordScopeWork
                Params(2) = New SqlParameter("@RentStartDate", SqlDbType.Date)
                Params(2).Value = RentstartDate
                Params(3) = New SqlParameter("@WoDate", SqlDbType.Date)
                Params(3).Value = WoDate
                Params(4) = New SqlParameter("@VenderDtls", SqlDbType.VarChar, 500)
                Params(4).Value = VendorDetail
                Params(5) = New SqlParameter("@InteriorWrkStartDate", SqlDbType.Date)
                Params(5).Value = InteriorWorkDate
                Params(6) = New SqlParameter("@GeneratorInstallDate", SqlDbType.Date)
                Params(6).Value = GenerationInstall
                Params(7) = New SqlParameter("@UpsInstallDate", SqlDbType.Date)
                Params(7).Value = UPSInstall
                Params(8) = New SqlParameter("@Phase3ConnectionDate", SqlDbType.Date)
                Params(8).Value = phaseConnection
                Params(9) = New SqlParameter("@CctvCompletDate", SqlDbType.Date)
                Params(9).Value = CCTVCompletionDate
                Params(10) = New SqlParameter("@FireAlarmCompleDate", SqlDbType.Date)
                Params(10).Value = FireAlarmCompletionDate
                Params(11) = New SqlParameter("@BrandingCompleDate", SqlDbType.Date)
                Params(11).Value = BrandingCompletionDate
                Params(12) = New SqlParameter("@SRDDeliverDate", SqlDbType.Date)
                Params(12).Value = SRD
                Params(13) = New SqlParameter("@SDLDeliverDate", SqlDbType.Date)
                Params(13).Value = SDL
                Params(14) = New SqlParameter("@SafeDeliverDate", SqlDbType.Date)
                Params(14).Value = Safe
                Params(15) = New SqlParameter("@AlmirahDeliverDate", SqlDbType.Date)
                Params(15).Value = Almirah
                Params(16) = New SqlParameter("@AcDate", SqlDbType.Date)
                Params(16).Value = AC
                Params(17) = New SqlParameter("@UPSBankDeliverDate", SqlDbType.Date)
                Params(17).Value = UPSBank
                Params(18) = New SqlParameter("@UPSATMDeliverDate", SqlDbType.Date)
                Params(18).Value = UPSATM
                Params(19) = New SqlParameter("@FireExitingDeliverDate", SqlDbType.Date)
                Params(19).Value = FireExtinguisher
                Params(20) = New SqlParameter("@CtsScannerDeliverDate", SqlDbType.Date)
                Params(20).Value = CTS_Scanner
                Params(21) = New SqlParameter("@UVLampDeliverDate", SqlDbType.Date)
                Params(21).Value = UVLamp
                Params(22) = New SqlParameter("@CashCountMachineDeliverDate", SqlDbType.Date)
                Params(22).Value = CashCountingMachine
                Params(23) = New SqlParameter("@ChairsDeliverDate", SqlDbType.Date)
                Params(23).Value = Chairs
                Params(24) = New SqlParameter("@BrandingExteriorDeliverDate", SqlDbType.Date)
                Params(24).Value = BrandingExterior
                Params(25) = New SqlParameter("@BrandingInteriorDeliverDate", SqlDbType.Date)
                Params(25).Value = BrandingInterior
                Params(26) = New SqlParameter("@CCTVDeliverDate", SqlDbType.Date)
                Params(26).Value = CCTVDate
                Params(27) = New SqlParameter("@SectionID", SqlDbType.Int)
                Params(27).Value = sectionId
                Params(28) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(28).Direction = ParameterDirection.Output
                Params(29) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(29).Direction = ParameterDirection.Output
                Params(30) = New SqlParameter("@InteriorWrkStatus", SqlDbType.Int)
                Params(30).Value = InteriorWorkStatus
                Params(31) = New SqlParameter("@CCTVCompletionStatus", SqlDbType.Int)
                Params(31).Value = CCTVCompletionStatus
                Params(32) = New SqlParameter("@FireAlarmCompletionStatus", SqlDbType.Int)
                Params(32).Value = FireAlarmCompletionStatus
                Params(33) = New SqlParameter("@BrandingCompletionStatus", SqlDbType.Int)
                Params(33).Value = BrandingCompletionStatus
                Params(34) = New SqlParameter("@userId", SqlDbType.Int)
                Params(34).Value = Session("UserID")
                Params(35) = New SqlParameter("@FinishFlag", SqlDbType.Int)
                Params(35).Value = FinishFlag
                Params(36) = New SqlParameter("@attend", SqlDbType.Int)
                Params(36).Value = attend
                Params(37) = New SqlParameter("@Intimate", SqlDbType.Int)
                Params(37).Value = intimate
                Params(38) = New SqlParameter("@BoMasterId", SqlDbType.Int)
                Params(38).Value = BO_Master_Id
                DB.ExecuteNonQuery("SP_BO_ADMIN_PROCEDURES", Params)
                ErrorFlag = CInt(Params(28).Value)
                Message = CStr(Params(29).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message

        ElseIf CInt(Data(0)) = 4 Then
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            If Data(1).ToString() <> "" Then
                BO_Master_Id = CInt(Data(1).ToString())
            End If
            Dim BO_Section_Id As Integer
            If Data(2).ToString() <> "" Then
                BO_Section_Id = CInt(Data(2).ToString())
            End If
            Dim BO_Premise_Id As Integer
            If Data(3).ToString() <> "" Then
                BO_Premise_Id = CInt(Data(3).ToString())
            End If
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@BO_Master_Id", SqlDbType.Int)
                Params(0).Value = BO_Master_Id
                Params(1) = New SqlParameter("@BO_Section_Id", SqlDbType.Int)
                Params(1).Value = BO_Section_Id
                Params(2) = New SqlParameter("@BO_Premise_Id", SqlDbType.Int)
                Params(2).Value = BO_Premise_Id
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@Remove", SqlDbType.Int)
                Params(5).Value = 1
                DB.ExecuteNonQuery("SP_BO_BUSINESS_ATTACH", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
                fup1.Visible = True
                Upload.Visible = True
                UploadedAttachment1.Visible = False
                Remove1.Visible = False
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_script1.Append("         alert('" + Message + "');")
                cl_script1.Append("         window.open('BO_AdminProcedures.aspx','_self');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
        End If
    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Upload.Click
        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
        Dim myFile As HttpPostedFile
        Dim FileLen As Integer = 0
        Dim FileName As String = ""
        If fup1.Visible = True Then
            myFile = fup1.PostedFile
            FileLen = myFile.ContentLength
            If (FileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(FileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, FileLen)
            End If
        End If
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim BO_Master_Id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))
        Try
            Dim Params(10) As SqlParameter
            Params(0) = New SqlParameter("@AttachImage", SqlDbType.Int)
            Params(0).Value = AttachImage
            Params(1) = New SqlParameter("@CaptionName", SqlDbType.VarChar, 100)
            Params(1).Value = "Lease agreement"
            Params(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
            Params(2).Value = AttachImg
            Params(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
            Params(3).Value = ContentType
            Params(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
            Params(4).Value = FileName
            Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(5).Value = CInt(Session("UserID"))
            Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(6).Direction = ParameterDirection.Output
            Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(7).Direction = ParameterDirection.Output
            Params(8) = New SqlParameter("@BO_Master_Id", SqlDbType.Int)
            Params(8).Value = BO_Master_Id
            Params(9) = New SqlParameter("@BO_Section_Id", SqlDbType.Int)
            Params(9).Value = 5
            Params(10) = New SqlParameter("@BO_Premise_Id", SqlDbType.Int)
            Params(10).Value = 0
            DB.ExecuteNonQuery("SP_BO_BUSINESS_ATTACH", Params)
            Message = CStr(Params(7).Value)
            ErrorFlag = CInt(Params(6).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "');")
        'cl_script1.Append("         window.open('BO_BusinessProcedures.aspx','_self');")
        cl_script1.Append("         window.open('BO_AdminProcedures.aspx?Bo_Master_Id='" + BO_Master_Id.ToString() + "',_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    End Sub

    Private Function bld(p1 As Integer) As Object
        Throw New NotImplementedException
    End Function

    Private Function bld() As Object
        Throw New NotImplementedException
    End Function

    Private Function b(p1 As Integer) As Object
        Throw New NotImplementedException
    End Function

End Class
