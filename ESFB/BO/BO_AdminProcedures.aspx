﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="BO_AdminProcedures.aspx.vb" Inherits="BO_AdminProcedures" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
 <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">   </asp:ToolkitScriptManager>
 <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script src="../Script/jquery-1.2.6.min.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);         
        }        
        .Button:hover
        {            
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            color:#801424;
        }                  
     .bg
     {
         background-color:#FFF;
     }
     .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
        .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }            
    </style>    

    <script language="javascript" type="text/javascript">
//    var col_id = 3;
        var DisFlag1;
        var DisFlag2;
  function  window_onload() 
  {
            enableSection(); //to show, hide, enable and disble fields            
            SectionID = $("#<%= hidSection.ClientID %>").val();
            
           var premTable = $("#<%= hidPremiseDtl.ClientID %>").val().split("¥");
          
            if(premTable.length - 1 != 0 ){
                col_id = premTable.length - 1;
            }
            else{
                col_id = 1;
            }
            TableFill_Section2();
           
            
   }

 function enableSection()
 {
           SectionID = $("#<%= hidSection.ClientID %>").val();
          
            if(SectionID == 3)
            {
               
                
                   
                if($("#<%= hidenable1.ClientID %>").val() == 1){
                    $(".Disp_Section1").attr("disabled", false);
                    $("#divSection2").show();
                    styleFlag1 = '';
                    styleFlag2 = 'display:none';
                    DisFlag1 = 'disabled';
                    
                }
                else{
                    $(".Disp_Section1").attr("disabled", true);
                    $("#divSection2").show();
                    styleFlag1 = '';
                    styleFlag2 = 'display:none';
                    DisFlag1 = 'disabled';
                    DisFlag2 = 'disabled';

                }                
            }
           
            if(SectionID == 5)
            {

                if ($("#<%= hidenable3.ClientID %>").val() == 1) {
                    styleFlag1 = 'display:none';
                    styleFlag2 = 'display:none';
                    DisFlag1 = 'disabled';
                    DisFlag2 = 'disabled';
                    $("#divSection2").show();
                    $("#divSection5").show();

                }
                else {
                    styleFlag1 = 'display:none';
                    styleFlag2 = 'display:none';
                    DisFlag1 = 'disabled';
                    DisFlag2 = 'disabled';
                    $("#divSection2").show();
                    $("#divSection5").show();
                    $("#<%= chkAttendSec5.ClientID %>").attr("disabled", true);
                    $("#<%= fup1.ClientID %>").attr("disabled", true);
                    $("#<%= Upload.ClientID %>").attr("disabled", true);
                    $("#<%= chkIntimateSec5.ClientID %>").attr("disabled", true);
                    $("#<%= cmbProposalApproval.ClientID %>").attr("disabled", true);
                    $("#<%= txtProposalApproval.ClientID %>").attr("disabled", true);
                    $("#<%= cmbLegalClearance.ClientID %>").attr("disabled", true);
                    $("#<%= txtLegalClearance.ClientID %>").attr("disabled", true);
                     $("#<%= txtLegalAgreementSigned.ClientID %>").attr("disabled", true);
                               

                }   

            }
            if (SectionID == 6) {

                if ($("#<%= hidenable4.ClientID %>").val() == 1) {
                    $(".sec5").attr("disabled", true);
                    styleFlag1 = 'display:none';
                    styleFlag2 = 'display:none';
                    DisFlag1 = 'disabled';
                    DisFlag2 = 'disabled';
                    $("#divSection2").show();
                    $("#divSection5").show();
                    $("#divSection6").show();
                    $("#attendSec5").hide();
                    $("#intimateSec5").hide();
                                         $("#<%= chkAttendSec5.ClientID %>").attr("disabled", true);
                                         $("#<%= fup1.ClientID %>").attr("disabled", true);
                                         $("#<%= Upload.ClientID %>").attr("disabled", true);
                                         $("#<%= chkIntimateSec5.ClientID %>").attr("disabled", true);
                                         $("#<%= cmbProposalApproval.ClientID %>").attr("disabled", true);
                                         $("#<%= txtProposalApproval.ClientID %>").attr("disabled", true);
                                         $("#<%= cmbLegalClearance.ClientID %>").attr("disabled", true);
                                         $("#<%= txtLegalClearance.ClientID %>").attr("disabled", true);
                                         $("#<%= txtLegalAgreementSigned.ClientID %>").attr("disabled", true);
                                         

                }
                else {
                    $(".sec5").attr("disabled", true);
                    styleFlag1 = 'display:none';
                    styleFlag2 = 'display:none';
                    DisFlag1 = 'disabled';
                    DisFlag2 = 'disabled';
                    $("#divSection2").show();
                    $("#divSection5").show();
                    $("#divSection6").show();
                    $("#attendSec5").hide();
                    $("#intimateSec5").hide();
                    $("#<%= chkAttendSec5.ClientID %>").attr("disabled", true);
                    $("#<%= fup1.ClientID %>").attr("disabled", true);
                    $("#<%= Upload.ClientID %>").attr("disabled", true);
                    $("#<%= chkIntimateSec5.ClientID %>").attr("disabled", true);
                    $("#<%= cmbProposalApproval.ClientID %>").attr("disabled", true);
                    $("#<%= txtProposalApproval.ClientID %>").attr("disabled", true);
                    $("#<%= cmbLegalClearance.ClientID %>").attr("disabled", true);
                    $("#<%= txtLegalClearance.ClientID %>").attr("disabled", true);
                    $("#<%= txtLegalAgreementSigned.ClientID %>").attr("disabled", true);
                    $(".sec6").attr("disabled", true);



                }

            }

           
            if (SectionID == "") 
            {
                alert("No pending queues");

            }
           
 }


   function FromServer(arg, context)
    {           
           if(context == 1)
           {
                var Data = arg.split("Ø");
                var errorFlag = Data[0];
                var message = Data[1];
                var master = document.getElementById("<%= hidmaster.ClientID %>").value;
                    alert(message);
                    window.open("BO_AdminProcedures.aspx?Bo_Master_Id=" + master.toString() + "", "_self"); 
                
            }
             if(context == 2)
           {
                var Data = arg.split("Ø");
                var errorFlag = Data[0];
                var message = Data[1];
                var master = document.getElementById("<%= hidmaster.ClientID %>").value;
                  alert(message);
                    window.open("BO_AdminProcedures.aspx?Bo_Master_Id=" + master.toString() + "", "_self"); 
                
            }
             if(context == 3)
           {
                var Data = arg.split("Ø");
                var errorFlag = Data[0];
                var message = Data[1];
                var master = document.getElementById("<%= hidmaster.ClientID %>").value;
                    alert(message);
                    window.open("BO_AdminProcedures.aspx?Bo_Master_Id=" + master.toString() + "", "_self");         
            }
            
       }

    function TableFill_Section2() 
       {
     
       document.getElementById("<%= pnlSection2.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div>";
            tab += "<table>";
            tab += "<tr>";
            tab += "<td>";
            tab += "<div class='CheckboxShow' style='text-align:center;"+styleFlag1+"'><br /><input type = 'checkbox' ID='chkAttendSec3'/>&nbsp;<b>Attend</b></div>";
            tab += "<div class='Checkbox4Show' style='text-align:center;"+styleFlag2+"'><br /><input type = 'checkbox' ID='chkAttendSec4'/>&nbsp;<b>Attend</b></div>";
            tab += "<div class='mainhead' style='width:100% height:auto; padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
            tab += "<table style='width:100%; margin:0px auto;font-family:cambria;line-height:20px;background-color:#EEB8A6;' align='left'>";
            tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;' align='left'>";
            tab += "<td style='text-align:center;' class='NormalText'>Particulars</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>Premise "+i+"</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section1'>";
            tab += "<td style='text-align:center;' class='NormalText'>Approximate Area (in sqft)</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtApproxArea_"+i+"' "+DisFlag1+" name='txtApproxArea_"+i+"'  type='Text' ReadOnly='true' style='width:99%;' class='NormalText'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second Disp_Section1'>";
            tab += "<td style='text-align:center;' class='NormalText'>Building status</td>";
            for(var i = 1;i<=col_id;i++){
                var select = "<select id='cmbBuildStatus_"+i+"' "+DisFlag1+" class='NormalText cmbBuildStatus' ReadOnly='true' style='width:99%;'>";        
                var cmbData = $("#<%= hidBuildingStatus.ClientID %>").val();
                var rows = cmbData.split("Ñ");
                for (var a = 0; a < rows.length-1; a++) 
                {
                    var cols = rows[a].split("ÿ");
                    select += "<option value='"+cols[0]+"'>"+cols[1]+"</option>";                    
                }
                select += "</select>";
                tab += "<td style='text-align:center;'>"+select+"</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section1'>";
            tab += "<td style='text-align:center;' class='NormalText'>Present USB Floor</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtUSBFloor_"+i+"' "+DisFlag1+" name='txtUSBFloor_"+i+"'  type='Text' style='width:99%;' ReadOnly='true' class='NormalText'/>";
                tab += "</td>";
            }
            tab += "</tr> ";
            tab += "<tr class='sub_second Disp_Section1'>";   
            tab += "<td style='text-align:center;' class='NormalText'>Present USB Area</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtUSBArea_"+i+"' "+DisFlag1+" name='txtUSBArea"+i+"'  type='Text' style='width:99%;' ReadOnly='true' class='NormalText'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section1'>";
            tab += "<td style='text-align:center;' class='NormalText'>Floor (Ground,1,2,3 floor)</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtFloor_"+i+"' "+DisFlag1+" name='txtFloor"+i+"'  type='Text' style='width:99%;' ReadOnly='true' class='NormalText'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second Disp_Section1'>";
            tab += "<td style='text-align:center;' class='NormalText'>Building No available</td>";
            for(var i = 1;i<=col_id;i++){
                select = "<select id='cmbBuildingNo_"+i+"' "+DisFlag1+" class='NormalText' name='cmbBuildingNo"+i+"' ReadOnly='true' style='width:99%;'>";      
                select += "<option value='-1'> ---Select---  </option>";
                select += "<option value='1'>Yes</option>";
                select += "<option value='2'>No</option>";
                select += "</select>";
                tab += "<td style='text-align:center;' class='NormalText'>"+select+"</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section1'>";
            tab += "<td style='text-align:center;' class='NormalText'>Building owner name</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtOwnerName_"+i+"' "+DisFlag1+" name='txtOwnerName_"+i+"'  type='Text' style='width:99%;' ReadOnly='true' class='NormalText'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second Disp_Section1'>";
            tab += "<td style='text-align:center;' class='NormalText'>contact number</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtPhNo_"+i+"' "+DisFlag1+" name='txtPhNo_"+i+"'  type='Text' style='width:99%;' ReadOnly='true' class='NormalText'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section1'>";
            tab += "<td style='text-align:center;' class='NormalText'>Rent proposed</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtPropRent_"+i+"' "+DisFlag1+" name='txtPropRent_"+i+"'  type='Text' style='width:99%;' ReadOnly='true' class='NormalText'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second Disp_Section1'>";
            tab += "<td style='text-align:center;' class='NormalText'>Availbility of ATM</td>";
            for(var i = 1;i<=col_id;i++){
                select = "<select id='cmbATM_"+i+"' "+DisFlag1+" class='NormalText' ReadOnly='true' style='width:99%;'>";      
                select += "<option value='-1'> ---Select---  </option>";
                select += "<option value='1'>Yes</option>";
                select += "<option value='2'>No</option>";
                select += "</select>";
                tab += "<td style='text-align:center;' class='NormalText'>"+select+"</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section1'>";
            tab += "<td style='text-align:center;' class='NormalText'>Strong room/Safe room required?</td>";
            for(var i = 1;i<=col_id;i++){
                select = "<select id='cmbStrongRoom_"+i+"' "+DisFlag1+" class='NormalText' ReadOnly='true' style='width:99%;'>";      
                select += "<option value='-1'> ---Select---  </option>";
                select += "<option value='1'>Yes</option>";
                select += "<option value='2'>No</option>";
                select += "</select>";
                tab += "<td style='text-align:center;' class='NormalText'>"+select+"</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second Disp_Section1'>";
            tab += "<td style='text-align:center;' class='NormalText'>Comment box</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<textarea id='txtCommentStrong_" + i + "' " + DisFlag1 + " name='txtCommentStrong_" + i + "' type='Text' ReadOnly='true' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)'></textarea>";
                tab += "</td>";
            }
            tab += "</tr>";
//            tab += "<tr class='sub_first Disp_Section1'>";
//            tab += "<td style='text-align:center;' class='NormalText'>Attachment option</td>";
//            for(var i = 1;i<=col_id;i++){
//                tab += "<td style='text-align:center;' class='NormalText'>Attachment option</td>";
//            }
//             tab += "</tr>";



//  section 3 Admin

//             tab += "<tr class='sub_second Disp_Section'>";
//            tab += "<td style='text-align:center;' class='NormalText'>Business Intimation received date</td>";
//            for(var i = 1;i<=col_id;i++){
//                tab += "<td style='text-align:center;' class='NormalText'>";
//                tab += "<input type='date' style='width:99%;' id='DateIntimationRecievedBusiness_" + i + "'"+DisFlag2+" name='DateIntimationRecievedBusiness_" + i + "'></td>";
//                tab += "</td>";
//            }

//            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>Preliminary Agreement Signed</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input type='date' style='width:99%;' id='DatePrelimnaryAgreement_" + i + "' " + DisFlag2 + " name='DatePrelimnaryAgreement_" + i + "'></td>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>Execution Engineer</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtExecutionEngineer_" + i + "' name='txtExecutionEngineer_" + i + "' " + DisFlag2 + " type='Text' style='width:99%;' class='NormalText'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>visting planning date</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input type='date' style='width:99%;' " + DisFlag2 + " id='DateVisitingPlan_" + i + "' name='DateVisitingPlan_" + i + "'></td>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>visited date</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input type='date' style='width:99%;' id='DateVisited_" + i + "' " + DisFlag2 + " name='DateVisited_" + i + "'></td>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section'>";
             tab += "<td style='text-align:center;' class='NormalText'>Premise  status updation by Admin</td>";
            for(var i = 1;i<=col_id;i++){
                var select = "<select id='cmbPremiseStausAdmin_" + i + "' " + DisFlag2 + " class='NormalText' style='width:99%;'>";  
                var cmbData = $("#<%= hidPremiseStatus.ClientID %>").val();
                var rows = cmbData.split("Ñ");
                for (var a = 0; a < rows.length-1; a++) 
                {
                    var cols = rows[a].split("ÿ");
                   
                    select += "<option value='"+cols[0]+"'>"+cols[1]+"</option>";                    
                }
                select += "</select>";
                tab += "<td style='text-align:center;'>"+select+"</td>";
            }
             tab += "</tr>";
            tab += "<tr class='sub_second Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>Comment box</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<textarea id='txtCommentBox_" + i + "' name='txtCommentBox_" + i + "' " + DisFlag2 + " type='Text' style='width:99%;' onkeypress='return TextAreaCheck(event)' maxlength='500'/></textarea>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>Present status of buiding at time of proposal</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtBulidingPresStatus_" + i + "' name='txtBulidingPresStatus_" + i + "' " + DisFlag2 + " type='Text' style='width:99%;' maxlength='100' class='NormalText'/>";
                tab += "</td>";
            }
           
             tab += "</tr>";
            tab += "<tr class='sub_second Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>Proposal to Committee from admin</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input type='date' style='width:99%;' id='DateProposalToCommittee_" + i + "' " + DisFlag2 + " name='DateProposalToCommittee_" + i + "'></td>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>Area in Sqft</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtAreaSqft_" + i + "' name='txtAreaSqft_" + i + "' " + DisFlag2 + " type='Text' style='width:99%;' class='NormalText' onkeypress='return NumericCheck(event)'/>";
                tab += "</td>";
            }
             tab += "</tr>";
            tab += "<tr class='sub_second Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>Rent Agreed</td>";
           for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtRentAgreed_" + i + "' name='txtRentAgreed_" + i + "' " + DisFlag2 + " type='Text' style='width:99%;' class='NormalText' onkeypress='return NumericCheck(event)'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>panchayath/corporation</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtPanchayat_" + i + "' name='txtPanchayat_" + i + "' " + DisFlag2 + " type='Text' style='width:99%;' maxlength='100' class='NormalText'/>";
                tab += "</td>";
            }
            
             tab += "</tr>";
            tab += "<tr class='sub_second Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'> village</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtVillage_" + i + "' name='txtVillage_" + i + "' " + DisFlag2 + " type='Text' style='width:99%;' maxlength='100' class='NormalText'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>landmark</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtLandmark_" + i + "' name='txtLandmark_" + i + "' " + DisFlag2 + " type='Text' style='width:99%;' maxlength='100' class='NormalText'/>";
                tab += "</td>";
            }
             tab += "</tr>";
              tab += "<tr class='sub_second Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>Address</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtAddress_" + i + "' name='txtAddress_" + i + "' " + DisFlag2 + " type='Text' style='width:99%;' maxlength='500' class='NormalText'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first Disp_Section'>";
            tab += "<td style='text-align:center;' class='NormalText'>pin code</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtPincode_" + i + "' name='txtPincode_" + i + "' " + DisFlag2 + " type='Text' style='width:99%;' class='NormalText' onkeypress='return NumericCheck(event)'/>";
                tab += "</td>";
            }
             tab += "</tr>";

// end section 3


//  start section 4

 tab += "<tr class='sub_second sec4'>";
            tab += "<td style='text-align:center;' class='NormalText'>Building Lead Status</td>";
//            for(var i = 1;i<=col_id;i++){
//                select = "<select id='cmbBldngLeadStatus_"+i+"' class='NormalText' style='width:99%;'>";      
//                select += "<option value='-1'> ---Select---  </option>";
//                select += "</select>";
//                tab += "<td style='text-align:center;' class='NormalText'>"+select+"</td>";
//            }

             for(var i = 1;i<=col_id;i++){
                 var select = "<select id='cmbBldngLeadStatus_" + i + "' "+ DisFlag1+" class='NormalText' ReadOnly='true' style='width:99%;'>";        
                var cmbData = $("#<%= hidLeadStatus.ClientID %>").val();
                var rows = cmbData.split("Ñ");
                for (var a = 0; a < rows.length-1; a++) 
                {
                    var cols = rows[a].split("ÿ");
                    select += "<option value='"+cols[0]+"'>"+cols[1]+"</option>";                    
                }
                select += "</select>";
                tab += "<td style='text-align:center;'>"+select+"</td>";
            }

            tab += "</tr>";
            tab += "<tr class='sub_first sec4'>";
            tab += "<td style='text-align:center;' class='NormalText'>Reason for rejection</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<textarea id='txtCommentRejReason_" + i + "' " + DisFlag1 + " name='txtCommentRejReason_" + i + "' type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)'></textarea>";
                tab += "</td>";
            }
            tab += "</tr>";

// end section 4

           
            tab += "</table>";    
            tab += "</div>";     
            tab += "</td>";            
            tab += "</tr>"; 
            tab += "</table>"; 
             tab += "<div class='Checkbox4Show' style='text-align:center;"+styleFlag2+"'><br /><input type = 'checkbox' ID='chkIntimateSec4'/>&nbsp;<b>Intimate</b></div>";
            tab += "</div>";             
             document.getElementById("<%= pnlSection2.ClientID %>").innerHTML = tab; 
//            
                 
             TableFillPremiseData();
                  

               SectionID = $("#<%= hidSection.ClientID %>").val();

              if(SectionID == 3){

	                  
                         $('#CheckboxShow').show();


                        if ($('#<%= hidAttend1.ClientID %>').val() == 0) {
                            document.getElementById("chkAttendSec3").checked = false;
                        }
                        else {
                            document.getElementById("chkAttendSec3").checked = true;
                            document.getElementById("chkAttendSec3").disabled = true;

                        }

                 }


                    if (SectionID == 5) {
                        

                          if(document.getElementById("<%= hidAttend5.ClientID %>").value== "0")
                        {
                           

                            document.getElementById("<%= chkAttendSec5.ClientID %>").checked = false;
                        }
                        else {


                            document.getElementById("<%= chkAttendSec5.ClientID %>").checked = true;
                        }

                        if (document.getElementById("<%= hidIntimate5.ClientID %>").value == "0")
                         {

                             document.getElementById("<%= chkIntimateSec5.ClientID %>").checked = false;
                        }
                        else {
                            document.getElementById("<%= chkIntimateSec5.ClientID %>").checked = true;
                        }

                 }
                  if(SectionID == 6)
                 {

                     if (document.getElementById("<%= hidAttend6.ClientID %>").value == "0") 
                     {
                         document.getElementById("<%= chkAttendSec6.ClientID %>").checked = false;
                     }
                     else 
                     {
                         document.getElementById("<%= chkAttendSec6.ClientID %>").checked = true;
                     }

                     if (document.getElementById("<%= hidIntimate6.ClientID %>").value == "0") 
                     {

                         document.getElementById("<%= chkIntimateSec6.ClientID %>").checked = false;
                     }
                     else 
                     {
                         document.getElementById("<%= chkIntimateSec6.ClientID %>").checked = true;
                     }

                 }

       }
    
    function btnSave_onclick() 
    {   
        SaveAll(0);
    } 
    function btnFinish_onclick() 
    {   
        SaveAll(1);
    } 
    function SaveAll(FinishFlag) 
    {
       
         
         /// section 3
        
         
               if(SectionID==3)
               {
                                
                 var DateIntimationRecievedBusiness = [];
                 var DatePrelimnaryAgreement     = [];
                  var ExecutionEngineer    =[];
                 var DateVisitingPlan        = [];
                 var DateVisited = [];
                 var PremiseStausAdmin       = [];
                 var CommentBox        = [];
                 var BulidingPresStatus         = [];
                 var DateProposalToCommittee         = [];
                 var AreaSqft        = [];
                 var RentAgreed        = [];
                 var Panchayat          = [];
                 var Village            = [];
                 var Landmark           = [];
                 var Address            = [];
                 var Pincode           = [];
               var PremiseColumnNumber =[];


                 for (n = 0; n <= col_id-1; n++) 
                {  

                
//                  DateIntimationRecievedBusiness[n] = document.getElementById("DateIntimationRecievedBusiness_" + (n+1)).value;
               
                  DatePrelimnaryAgreement[n]= document.getElementById("DatePrelimnaryAgreement_" + (n+1)).value;
                   ExecutionEngineer[n]= document.getElementById("txtExecutionEngineer_" + (n+1)).value;
                  DateVisitingPlan[n]= document.getElementById("DateVisitingPlan_" + (n+1)).value;
                  DateVisited[n]= document.getElementById("DateVisited_" + (n+1)).value;
                  PremiseStausAdmin[n]= document.getElementById("cmbPremiseStausAdmin_" + (n+1)).value;
                  CommentBox[n]= document.getElementById("txtCommentBox_" + (n+1)).value;
                  BulidingPresStatus[n]=document.getElementById("txtBulidingPresStatus_" + (n+1)).value;
                  DateProposalToCommittee[n]=document.getElementById("DateProposalToCommittee_" + (n+1)).value;
                  AreaSqft[n]=document.getElementById("txtAreaSqft_" + (n+1)).value;
                  RentAgreed[n]=document.getElementById("txtRentAgreed_" + (n+1)).value;
                  Panchayat[n]=document.getElementById("txtPanchayat_" + (n+1)).value;
                  Village[n] = document.getElementById("txtVillage_" + (n+1)).value;
                  Landmark[n] = document.getElementById("txtLandmark_" + (n + 1)).value;
                 
                  Address[n] = document.getElementById("txtAddress_" + (n + 1)).value;
                 
                  Pincode[n] = document.getElementById("txtPincode_" + (n + 1)).value;
                 
                  PremiseColumnNumber[n]=n+1;

              }
              var attend;
             
              var intimate=0;
                if($('#chkAttendSec3').is(":checked") == true)
                {
                    attend = "1";
                }
                else{
                    attend = "0";
                    alert("Please confirm that you are attending the section to save data");
                    return false;
                }



                var ToData = "1Ø" + DateIntimationRecievedBusiness.join('₤') + "ÿ" + DatePrelimnaryAgreement.join('₤') + "ÿ" + ExecutionEngineer.join('₤') + "ÿ" + DateVisitingPlan.join('₤') + "ÿ" + DateVisited.join('₤') + "ÿ" + PremiseStausAdmin.join('₤') + "ÿ" + CommentBox.join('₤') + "ÿ" + BulidingPresStatus.join('₤') + "ÿ" + DateProposalToCommittee.join('₤') + "ÿ" + AreaSqft.join('₤') + "ÿ" + RentAgreed.join('₤') + "ÿ" + Panchayat.join('₤') + "ÿ" + Village.join('₤') + "ÿ" + col_id + "ÿ" + SectionID + "ÿ" + PremiseColumnNumber.join('₤')
                + "ÿ" + attend + "ÿ" + intimate + "ÿ" + FinishFlag + 'ÿ' + Landmark + 'ÿ' + Address + 'ÿ' + Pincode;
              
              ToServer(ToData, 1);
         }
        
         else if(SectionID==5)
         {

     ///section 5

     
        
          
                var ProposalApprovalCombo=document.getElementById("<%= cmbProposalApproval.ClientID %>").value;
                var ProposalApprovalDate=document.getElementById("<%= txtProposalApproval.ClientID %>").value;

                var LegalClearanceCombo=document.getElementById("<%= cmbLegalClearance.ClientID %>").value;
                var LegalClearanceDate=document.getElementById("<%= txtLegalClearance.ClientID %>").value;

              
                var LegalAgreementSignedDate=document.getElementById("<%= txtLegalAgreementSigned.ClientID %>").value;

               
               
               var attend;
               var intimate;
              
                if($('#<%= chkAttendSec5.ClientID %>').is(":checked") == true)
                {
                    attend = "1";
                }
                else
                {
                    attend = "0";
                    alert("Please confirm that you are attending the section to save data");
                    return false;
                }
                
                if($('#<%= chkIntimateSec5.ClientID %>').is(":checked") == true)
                {
                    intimate = "1";
                }
                else{
                    intimate = "0";
                   
                }
              
              

               var ToData ="2Ø" + ProposalApprovalCombo + "ÿ" + ProposalApprovalDate + "ÿ" + LegalClearanceCombo + "ÿ" + LegalClearanceDate + "ÿ" + LegalAgreementSignedDate  + "ÿ" + SectionID + "ÿ" + FinishFlag + "ÿ" +  attend + "ÿ" + intimate;
              
              ToServer(ToData, 2);

         
         
         }
         else if(SectionID==6)
         {
   
///section 6
          
                var ScopeOfCivilWorkStatus=document.getElementById("<%= cmbScopeOfCivilWork.ClientID %>").value;
                var LordScopeWork=document.getElementById("<%= txtLordScopeWork.ClientID %>").value;
                var RentstartDate=document.getElementById("<%= txtRentstartDate.ClientID %>").value;
                var WoDate=document.getElementById("<%= txtWoDate.ClientID %>").value;
                var VendorDetail=document.getElementById("<%= txtVendorDetail.ClientID %>").value;
                var InteriorWorkDate=document.getElementById("<%= txtInteriorWorkDate.ClientID %>").value;
                var InteriorWorkStatus=document.getElementById("<%= cmbStatusInterior.ClientID %>").value;
                var GenerationInstall=document.getElementById("<%= txtGenerationInstall.ClientID %>").value;
                var UPSInstall=document.getElementById("<%= txtUPSInstall.ClientID %>").value;
                var phaseConnection=document.getElementById("<%= txt3phaseConnection.ClientID %>").value;
                var CCTVCompletionStatus=document.getElementById("<%= cmbCCTV.ClientID %>").value;
                var CCTVCompletionDate=document.getElementById("<%= txtCCTVComplete.ClientID %>").value;
                var FireAlarmCompletionStatus=document.getElementById("<%= cmbFireAlarm.ClientID %>").value;
                 var FireAlarmCompletionDate=document.getElementById("<%= txtFireAlarmComplete.ClientID %>").value;
                var BrandingCompletionStatus=document.getElementById("<%= cmbBrandingCompletion.ClientID %>").value;
                var BrandingCompletionDate=document.getElementById("<%= txtBrandingCompletion.ClientID %>").value;
                var SRD=document.getElementById("<%= txtSRD.ClientID %>").value;
                var SDL=document.getElementById("<%= txtSDL.ClientID %>").value;
                var Safe=document.getElementById("<%= txtSafe.ClientID %>").value;
                var Almirah=document.getElementById("<%= txtAlmirah.ClientID %>").value;
                var AC=document.getElementById("<%= TextAC.ClientID %>").value;
                var UPSBank=document.getElementById("<%= txtUPSBank.ClientID %>").value;
                var UPSATM=document.getElementById("<%= txtUPSATM.ClientID %>").value;
                var FireExtinguisher=document.getElementById("<%= txtFireExtinguisher.ClientID %>").value;
                var CTS_Scanner=document.getElementById("<%= txtCTS_Scanner.ClientID %>").value;
                var UVLamp=document.getElementById("<%= txtUVLamp.ClientID %>").value;
                var CashCountingMachine=document.getElementById("<%= txtCashCountingMachine.ClientID %>").value;
                var Chairs=document.getElementById("<%= txtChairs.ClientID %>").value;
                var BrandingExterior=document.getElementById("<%= txtBrandingExterior.ClientID %>").value;
                var BrandingInterior=document.getElementById("<%= txtBrandingInterior.ClientID %>").value;
                var CCTVDate=document.getElementById("<%= txtCCTV.ClientID %>").value;

              var attend;
               var intimate;
              
                if($('#<%= chkAttendSec6.ClientID %>').is(":checked") == true)
                {
                    attend = "1";
                }
                else
                {
                    attend = "0";
                    alert("Please confirm that you are attending the section to save data");
                    return false;
                }
                
                if($('#<%= chkAttendSec6.ClientID %>').is(":checked") == true)
                {
                    intimate = "1";
                }
                else{
                    intimate = "0";
                     alert("Please confirm that you are intimating the section to finish data");
                    return false;
                }
              
              
               var ToData ="3Ø" + ScopeOfCivilWorkStatus + "ÿ" +  LordScopeWork + "ÿ" + RentstartDate + "ÿ" + WoDate +
                "ÿ" + VendorDetail + "ÿ" + InteriorWorkDate + "ÿ" + InteriorWorkStatus + "ÿ" + GenerationInstall + "ÿ" +
                 UPSInstall + "ÿ" + phaseConnection + "ÿ" + CCTVCompletionStatus + "ÿ" + CCTVCompletionDate + "ÿ" + FireAlarmCompletionStatus 
                 + "ÿ" + FireAlarmCompletionDate + "ÿ" +  BrandingCompletionStatus + "ÿ" +  BrandingCompletionDate +  "ÿ" + SRD +
                  "ÿ" + SDL + "ÿ" +  Safe + "ÿ" + Almirah + "ÿ" + AC + "ÿ" + UPSBank + "ÿ" + UPSATM + "ÿ" + FireExtinguisher  
                  + "ÿ" + CTS_Scanner + "ÿ"  + UVLamp + "ÿ" + CashCountingMachine + "ÿ" + Chairs + "ÿ" + BrandingExterior
                   + "ÿ" + BrandingInterior + "ÿ" + CCTVDate + "ÿ" + SectionID + "ÿ" + FinishFlag + "ÿ" +  attend + "ÿ" +  intimate;
             
              ToServer(ToData, 3);


         }
         else
         {
         
         alert("nothing");
         
         }

    
}  



 function TableFillPremiseData(){

            var pTable = $("#<%= hidPremiseDtl.ClientID %>").val().split("¥");

            for( var i = 0;i<pTable.length-1;i++){
                var pData = pTable[i].split("µ");  
                          
                document.getElementById("txtApproxArea_"+(i+1)).value = pData[3];
                document.getElementById("cmbBuildStatus_"+(i+1)).value = pData[4];
                document.getElementById("txtUSBFloor_"+(i+1)).value = pData[5];
                document.getElementById("txtUSBArea_"+(i+1)).value = pData[6];
                document.getElementById("txtFloor_"+(i+1)).value = pData[7];
                document.getElementById("cmbBuildingNo_"+(i+1)).value = pData[8];
                document.getElementById("txtOwnerName_"+(i+1)).value = pData[9];
                document.getElementById("txtPhNo_"+(i+1)).value = pData[10];
                document.getElementById("txtPropRent_"+(i+1)).value = pData[11];
                document.getElementById("cmbATM_"+(i+1)).value = pData[12];
                document.getElementById("cmbStrongRoom_"+(i+1)).value = pData[13];
                document.getElementById("txtCommentStrong_"+(i+1)).value = pData[15];
 //             document.getElementById("DatePrelimnaryAgreement_"+(i+1)).value = pData[19];
  
                if ( pData[19] != "")
                    {
                        var CreateDate = new Date(pData[19]); 
                        dd = CreateDate.getDate(); if (dd<10) dd = "0"+dd;
                        mm = CreateDate.getMonth()+1; if (mm<10) mm = "0"+mm;
                        yyyy = CreateDate.getFullYear(); 
                        CreateDate = yyyy+'-'+mm+'-'+dd;
                         document.getElementById("DatePrelimnaryAgreement_"+(i+1)).value =CreateDate;
//                     alert(document.getElementById("DatePrelimnaryAgreement_"+(i+1)).value );
                     }
                document.getElementById("txtExecutionEngineer_"+(i+1)).value = pData[20];
//                document.getElementById("DateVisitingPlan_"+(i+1)).value = pData[21];
                if ( pData[21] != "")
                    {
                        var CreateDate = new Date(pData[21]); 
                        dd = CreateDate.getDate(); if (dd<10) dd = "0"+dd;
                        mm = CreateDate.getMonth()+1; if (mm<10) mm = "0"+mm;
                        yyyy = CreateDate.getFullYear(); 
                        CreateDate = yyyy+'-'+mm+'-'+dd;
                         document.getElementById("DateVisitingPlan_"+(i+1)).value =CreateDate;
                     }
//                document.getElementById("DateVisited_"+(i+1)).value = pData[22];
                if ( pData[22] != "")
                    {
                        var CreateDate = new Date(pData[22]); 
                        dd = CreateDate.getDate(); if (dd<10) dd = "0"+dd;
                        mm = CreateDate.getMonth()+1; if (mm<10) mm = "0"+mm;
                        yyyy = CreateDate.getFullYear(); 
                        CreateDate = yyyy+'-'+mm+'-'+dd;
                         document.getElementById("DateVisited_"+(i+1)).value =CreateDate;
                     }
                document.getElementById("cmbPremiseStausAdmin_"+(i+1)).value = pData[23];
                document.getElementById("txtCommentBox_"+(i+1)).value = pData[24];
                document.getElementById("txtBulidingPresStatus_"+(i+1)).value = pData[25];
//                document.getElementById("DateProposalToCommittee_"+(i+1)).value = pData[26];
                if ( pData[26] != "")
                    {
                        var CreateDate = new Date(pData[26]); 
                        dd = CreateDate.getDate(); if (dd<10) dd = "0"+dd;
                        mm = CreateDate.getMonth()+1; if (mm<10) mm = "0"+mm;
                        yyyy = CreateDate.getFullYear(); 
                        CreateDate = yyyy+'-'+mm+'-'+dd;
                         document.getElementById("DateProposalToCommittee_"+(i+1)).value =CreateDate;
                     }
                document.getElementById("txtAreaSqft_"+(i+1)).value = pData[27];
                document.getElementById("txtRentAgreed_"+(i+1)).value = pData[28];
                document.getElementById("txtPanchayat_"+(i+1)).value = pData[29];
                document.getElementById("txtVillage_"+(i+1)).value = pData[30];
                document.getElementById("txtLandmark_" + (i + 1)).value = pData[31];
                document.getElementById("txtAddress_" + (i + 1)).value = pData[32];
                document.getElementById("txtPincode_" + (i + 1)).value = pData[33];
                document.getElementById("cmbBldngLeadStatus_"+(i+1)).value = pData[36];
                document.getElementById("txtCommentRejReason_"+(i+1)).value = pData[37];


            }
        }
       
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }

        }
       
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            var master = document.getElementById("<%= hidmaster.ClientID %>").value;
            window.open("TeamView.aspx?Bo_Master_Id=" + master.toString() + "", "_self");

        }
        function ViewAttach1() {
            var BO_Section_Id = 5;
            var BO_Premise_Id = 0;
            var master = document.getElementById("<%= hidmaster.ClientID %>").value;
            window.open("ViewAttachment.aspx?BO_Master_Id=" + master.toString() + " &BO_Section_Id=" + BO_Section_Id + " &BO_Premise_Id=" + BO_Premise_Id, "_blank", "Height=400px,Width:500px,Left=300,Top=300,center:yes");
        }
        function RemoveAttach1() {
            var BO_Section_Id = 5;
            var BO_Premise_Id = 0;
            var BO_Master_Id = document.getElementById("<%= hidmaster.ClientID %>").value;
            var ToData = "4Ø" + BO_Master_Id + "Ø" + BO_Section_Id + "Ø" + BO_Premise_Id;
            ToServer(ToData, 4);
        }
    </script>
</head>
</html>
 <br />
 <div  style="width:90%;margin:0px auto; background-color:#EEB8A6;">
    <div style="width:80%;padding-left:10px;margin:0px auto;">
        <asp:HiddenField ID="hid_dtls" runat="server" />
        <asp:HiddenField ID="hdnSection3Dtl" runat="server" />
        <asp:HiddenField ID = "hid_Edit" runat="server" />
        <asp:HiddenField ID = "hidUser" runat="server" />  
        <asp:HiddenField ID = "hidPremiseStatus" runat="server" />
         <asp:HiddenField ID = "hidBuildingStatus" runat="server" />
         <asp:HiddenField ID = "hidLeadStatus" runat="server" />
        <asp:HiddenField ID = "hidPremiseDtl" runat="server" />
        <asp:HiddenField ID = "hidenable" runat="server" />
        <asp:HiddenField ID = "hidenable1" runat="server" />
        <asp:HiddenField ID = "hidenable2" runat="server" />
        <asp:HiddenField ID = "hidenable3" runat="server" />
        <asp:HiddenField ID = "hidenable4" runat="server" />
        <asp:HiddenField ID = "hidenable9" runat="server" />
        <asp:HiddenField ID = "hidSection" runat="server" />
        <asp:HiddenField ID = "hidAttend1" runat="server" />
        <asp:HiddenField ID = "hidAttend2" runat="server" />
        <asp:HiddenField ID = "hidAttend5" runat="server" />
        <asp:HiddenField ID = "hidAttend4" runat="server" />
        <asp:HiddenField ID = "hidAttend6" runat="server" />
        <asp:HiddenField ID = "hidAttend7" runat="server" />
        <asp:HiddenField ID = "hidAttend8" runat="server" />
        <asp:HiddenField ID = "hidIntimate3" runat="server" />
        <asp:HiddenField ID = "hidIntimate4" runat="server" />
        <asp:HiddenField ID = "hidIntimate5" runat="server" />
        <asp:HiddenField ID = "hidIntimate6" runat="server" />
         <asp:HiddenField ID = "hidmaster" runat="server" />
          <asp:HiddenField ID = "hid_Sec1_Attachment" runat="server" />
        <br /> 
    </div>
   
    <div id = "divSection2"  style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;display:none;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">            
            <tr id="Tr2">
                <td style="text-align: center;">
                    <asp:Panel ID="pnlSection2" Style="width: 100%;  text-align: left; float: left;" runat="server">
                       
            
                           
                    </asp:Panel>
                </td>
            </tr>
        </table> 
    </div>
   

    <br />
  
    <div id = "divSection5"  style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;display:none;">
        <br />

        <table style="width:90%;height:90px;margin:0px auto;">            
            <tr id="Tr1">
                <td style="text-align: center;" colspan="4">
               
                    <asp:Panel ID="PanelSection5" Style="width: 100%;  text-align: right; float: right;" runat="server">
                    <div id="attendSec5" style="text-align:center;"><br />
                            <asp:CheckBox ID="chkAttendSec5" runat="server"/>
                            &nbsp;<b>Attend</b>
                        </div>
                        <div class="mainhead" style='width:100%; height:auto; padding-top:0px;margin:0px auto;background-color:white;'>
                            <table style='width:100%;font-family:'cambria';' align='center'>
                                <tr>
                                    <td style='width:25%;text-align:center;'></td>
                                    <td style='width:25%;text-align:center;'></td>
                                    <td style='width:25%;text-align:center;'></td>
                                    <td style='width:25%;text-align:center;'></td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                       Proposal Approved
                                    </td>
                                   <td style='width:25%;text-align:center;'>
                                    <asp:DropDownList ID="cmbProposalApproval" class="NormalText sec5" runat="server" Width="50%">
                                    <asp:ListItem Value="-1">---------Select---------</asp:ListItem>
                                    </asp:DropDownList></td>
                                    <tr>
                                    <td style='width:25%;text-align:left;'>
                                       
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtProposalApproval" class="NormalText sec5" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtProposalApproval_CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtProposalApproval" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td></tr>
                                </tr>
                                 <tr>
                                    <td style='width:25%;text-align:left;'>
                                       Legal Clearance
                                    </td>
                                   <td style='width:25%;text-align:center;'>
                                    <asp:DropDownList ID="cmbLegalClearance" class="NormalText sec5" runat="server" Width="50%">
                                     <asp:ListItem Value="-1">---------Select---------</asp:ListItem>
                                    </asp:DropDownList></td>
                                    <tr>
                                    <td style='width:25%;text-align:left;'>
                                       
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtLegalClearance" class="NormalText sec5" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtLegalClearance_CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtLegalClearance" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td></tr>
                                </tr>
                                 <tr>
                                    <td style='width:25%;text-align:left;'>
                                       Lease Agreement Signed
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtLegalAgreementSigned" class="NormalText sec5" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtLegalAgreementSigned_CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtLegalAgreementSigned" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                 <tr>
                                    <td style='width:25%;text-align:left;'>
                                       Lease agreement attachment
                                    </td>
                                     <td style='width:25%;text-align:left;'>
                                        <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" Width="95%"/>
                                        <asp:Button ID="Upload" runat="server" Text="Upload" Font-Names="Cambria" style="cursor:pointer; Width=15%;text-align:right;" />
                                        <a id = "UploadedAttachment1" runat = "server" onclick="ViewAttach1()" width = "75%"></a>
                                        <asp:Button ID="Remove1" runat="server" Text="Remove" Font-Names="Cambria" style="cursor:pointer;" width="25%" align = "center"/>
                                    </td>
                                    </tr>
                                </tr>
                              
                            </table>
                        </div>
                         <div id="intimateSec5" style="text-align:center;"><br />
                            <asp:CheckBox ID="chkIntimateSec5" runat="server"/>
                            &nbsp;<b>intimate to business</b>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table> 
       <br />            
    </div>
     <br />     
   <div  id = "divSection6" style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;display:none;">
        <br />

        <table style="width:90%;height:90px;margin:0px auto;">            
            <tr id="List">
                <td style="text-align: center;" colspan="4">
                <div style="text-align:center;"><br />
                            <asp:CheckBox ID="chkAttendSec6"  runat="server"/>
                            &nbsp;<b>Attend</b>
                        </div>
                    <asp:Panel ID="pnlSection6" Style="width: 100%;  text-align: right; float: right;" runat="server">
                        <div class="mainhead" style='width:100%; height:auto; padding-top:0px;margin:0px auto;background-color:white;'>
                            <table style='width:100%;font-family:'cambria';' align='center'>
                                <tr>
                                    <td style='width:25%;text-align:center;'></td>
                                    <td style='width:25%;text-align:center;'></td>
                                    <td style='width:25%;text-align:center;'></td>
                                    <td style='width:25%;text-align:center;'></td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                       Scope of Civil Work
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:DropDownList ID="cmbScopeOfCivilWork" class="NormalText sec6" runat="server" Width="50%">
                                    <asp:ListItem Value="-1">---------Select---------</asp:ListItem>
                                    </asp:DropDownList>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Completion of Land Lord Scope of Work 
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                        <asp:TextBox ID="txtLordScopeWork" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtLordScopeWork_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtLordScopeWork" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                       Rent Start Date
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                        <asp:TextBox ID="txtRentstartDate" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtRentstartDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtRentstartDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        WO Date
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtWoDate" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtWoDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtWoDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Vendor Details
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                     <asp:TextBox ID="txtVendorDetail" class="NormalText sec6" runat="server" Width="50%" >
                                        </asp:TextBox>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                       Interior Work Start date
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtInteriorWorkDate" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtInteriorWorkDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtInteriorWorkDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Status of Interior Work
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:DropDownList ID="cmbStatusInterior" class="NormalText sec6" runat="server" Width="50%">
                                     <asp:ListItem Value="-1">---------Select---------</asp:ListItem>
                                    </asp:DropDownList></td>
                                    <tr>
                                    <td style='width:25%;text-align:left;'>
                                       
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtStatusInterior" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtStatusInterior_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtStatusInterior" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td></tr>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Genarator Installation
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtGenerationInstall" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtGenerationInstall_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtGenerationInstall" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                       UPS  Installation
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtUPSInstall" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtUPSInstall_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtUPSInstall" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        3 Phase Electric Connection
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txt3phaseConnection" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txt3phaseConnection_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt3phaseConnection" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        CCTV Completed
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:DropDownList ID="cmbCCTV" class="NormalText sec6" runat="server" Width="50%">
                                    <asp:ListItem Value="-1">-----SELECT-----</asp:ListItem>
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="2">NO</asp:ListItem>
                                    </asp:DropDownList>
                                    </td>
                                     <tr>
                                    <td style='width:25%;text-align:left;'>
                                       
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtCCTVComplete" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtCCTVComplete_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtCCTVComplete" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td></tr>
                                    <td style='width:25%;text-align:left;'>
                                       FIRE ALARM-Completed
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:DropDownList ID="cmbFireAlarm" class="NormalText sec6" runat="server" Width="50%">
                                    <asp:ListItem Value="-1">-----SELECT-----</asp:ListItem>
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="2">NO</asp:ListItem>
                                    </asp:DropDownList>
                                    </td>
                                    <tr>
                                    <td style='width:25%;text-align:left;'>
                                       
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtFireAlarmComplete" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtFireAlarmComplete_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtFireAlarmComplete" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td></tr>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Completion Of Branding
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:DropDownList ID="cmbBrandingCompletion" class="NormalText sec6" runat="server" Width="50%">
                                    <asp:ListItem Value="-1">-----SELECT-----</asp:ListItem>
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="2">NO</asp:ListItem>
                                    </asp:DropDownList>
                                    </td>
                                    <tr>
                                    <td style='width:25%;text-align:left;'>
                                       
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtBrandingCompletion" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtBrandingCompletion_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtBrandingCompletion" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td></tr>
                                    <td style='width:25%;text-align:left;'>
                                        SRD (strongroom door)
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtSRD" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtSRD_CalendarExtender6" runat="server" Enabled="True" TargetControlID="txtSRD" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                       SDL
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtSDL" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtSDL_CalendarExtender7" runat="server" Enabled="True" TargetControlID="txtSDL" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                       Safe
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtSafe" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtSafe_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtSafe" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Almirah
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtAlmirah" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtAlmirah_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtAlmirah" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                       AC 
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="TextAC" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="TextAC_CalendarExtender" runat="server" Enabled="True" TargetControlID="TextAC" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        UPS-BANK
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtUPSBank" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtUPSBank_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtUPSBank" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                       UPS -ATM 
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtUPSATM" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtUPSATM_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtUPSATM" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Fire Extinguisher
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtFireExtinguisher" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtFireExtinguisher_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtFireExtinguisher" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                       CTS scanner 
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtCTS_Scanner" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtCTS_Scanner_CalendarExtender14" runat="server" Enabled="True" TargetControlID="txtCTS_Scanner" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        UV Lamp
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtUVLamp" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtUVLamp_CalendarExtender15" runat="server" Enabled="True" TargetControlID="txtUVLamp" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                       Cash Counting Machine 
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtCashCountingMachine" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtCashCountingMachine_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtCashCountingMachine" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Chairs
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtChairs" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtChairs_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtChairs" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                       Branding exterior 
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtBrandingExterior" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtBrandingExterior_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtBrandingExterior" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Branding interior
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtBrandingInterior" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtBrandingInterior_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtBrandingInterior" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                      CCTV 
                                    </td>
                                    <td style='width:25%;text-align:center;'>
                                    <asp:TextBox ID="txtCCTV" class="NormalText sec6" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtCCTV_CalendarExtender20" runat="server" Enabled="True" TargetControlID="txtCCTV" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                            </table>
                        </div>
                         <div style="text-align:center;"><br />
                            <asp:CheckBox ID="chkIntimateSec6" class="NormalText sec6" runat="server"/>
                            &nbsp;<b>intimate to HR and IT</b>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />            
    </div>
    <div style="text-align:center; height: 63px;"><br />
        <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="SAVE" onclick="return btnSave_onclick()"/> 
        &nbsp;&nbsp;
        <input id="btnFinish" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="FINISH" onclick="return btnFinish_onclick()"/> 
        &nbsp;&nbsp;
        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()"/>
    </div>   
</div>
</asp:Content>
                   


