﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="BO_BusinessProcedures.aspx.vb" Inherits="BO_BusinessProcedures" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script src="../Script/jquery-1.2.6.min.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);         
        }        
        .Button:hover
        {            
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            color:#801424;
        }                  
     .bg
     {
         background-color:#FFF;
     }          
     .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
        .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        } 
        
    </style>    
    <script language="javascript" type="text/javascript">
        var col_id;
        var mode; // mode = 1 for SAVE, mode = 2 for FINISH
        var SectionID;
        var DisFlag2;
        var styleFlag2;
        var DisFlag3;
        var styleFlag3;
        var DisFlag4;
        var styleFlag4;
        var styleCheckFlag4;
        var BO_Master_Id;
        window.onload = function () {
            enableSection(); //to show, hide, enable and disble fields            
            SectionID = $("#<%= hidSection.ClientID %>").val();
            BO_Master_Id = $("#<%= hid_BO_Master_Id.ClientID %>").val();
            var premTable = $("#<%= hidPremiseDtl.ClientID %>").val().split("¥");
            if(premTable.length - 1 != 0 ){
                col_id = premTable.length - 1
            }
            else{
                col_id = 1;
            }

            TableFill();       
//            TableFillPremiseData();
        }
        function enableSection(){
            SectionID = $("#<%= hidSection.ClientID %>").val();
            if(SectionID == 1){
                $("#divSection1").show();
                if($("#<%= hidenable1.ClientID %>").val() == 1){
                    $(".sec1").attr("disabled", false);
                }
                else{
                    $(".sec1").attr("disabled", true);
                }                
            }
            if(SectionID == 2){
                $("#divSection1").show();
                $("#divSection2").show();
                styleFlag2 = '';
                styleFlag3 = 'display:none';
                styleFlag4 = 'display:none';
                styleCheckFlag4 = 'display:none';
                if($("#<%= hidenable1.ClientID %>").val() == 1){
                    $(".sec1").attr("disabled", false);
                }
                else{
                    $(".sec1").attr("disabled", true);
                }
                if($("#<%= hidenable2.ClientID %>").val() == 1){
//                    $(".sec2").attr("disabled", false);
                    DisFlag2 = '';
                    $(".sec1").attr("disabled", false);
                }
                else{
//                    $(".sec2").attr("disabled", true);
                    DisFlag2 = 'disabled';
                    $(".sec1").attr("disabled", true);
                }                
            }
            if(SectionID == 3){
                $("#divSection1").show();
                $("#divSection2").show();
                styleFlag2 = '';
                styleFlag3 = 'display:none';
                styleFlag4 = 'display:none';
                styleCheckFlag4 = 'display:none';
                if($("#<%= hidenable1.ClientID %>").val() == 1){
                    $(".sec1").attr("disabled", false);
                }
                else{
                    $(".sec1").attr("disabled", true);
                }
                if($("#<%= hidenable2.ClientID %>").val() == 1){
//                    $(".sec2").attr("disabled", false);
                    DisFlag2 = '';
                    $(".sec1").attr("disabled", false);
                }
                else{
//                    $(".sec2").attr("disabled", true);
                    DisFlag2 = 'disabled';
                    $(".sec1").attr("disabled", true);
                }   
            }
            if(SectionID == 4){
                $("#divSection1").show();
                $("#divSection2").show();
                styleFlag2 = '';
                styleFlag3 = '';
                styleFlag4 = '';
                styleCheckFlag4 = '';
                if($("#<%= hidenable1.ClientID %>").val() == 1){
                    $(".sec1").attr("disabled", false);
                }
                else{
                    $(".sec1").attr("disabled", true);
                }
                if($("#<%= hidenable2.ClientID %>").val() == 1){
                    DisFlag2 = '';
                }
                else{
                    DisFlag2 = 'disabled';
                }
                DisFlag3 = 'disabled';
                if($("#<%= hidenable4.ClientID %>").val() == 1){
                    DisFlag4 = '';
                }
                else{
                    DisFlag4 = 'disabled';
                }
            }
            if(SectionID == 9){
                $("#divSection1").show();
                $("#divSection2").show();
                styleFlag2 = '';
                styleFlag3 = '';
                styleFlag4 = '';
                styleCheckFlag4 = 'display:none';
                DisFlag4 = '';
                $("#divSection9").show();
                $(".attend9").show();
                if($("#<%= hidenable1.ClientID %>").val() == 1){
                    $(".sec1").attr("disabled", false);
                }
                else{
                    $(".sec1").attr("disabled", true);
                }
                if($("#<%= hidenable2.ClientID %>").val() == 1){
                    DisFlag2 = '';
                }
                else{
                    DisFlag2 = 'disabled';
                }
                DisFlag3 = 'disabled';
                if($("#<%= hidenable4.ClientID %>").val() == 1){
                    DisFlag4 = '';
                }
                else{
                    DisFlag4 = 'disabled';
                }    
                if($("#<%= hidenable9.ClientID %>").val() == 1){
                    $(".sec9").attr("disabled", false);
                }
                else{
                    $(".sec9").attr("disabled", true);
                }         
            }
        }
        function AddNewPremise(){
            col_id += 1;
            TableFill();
            var pTableOld = $("#<%= hidPremiseDtlOld.ClientID %>").val().split("¥");
            if(pTableOld.length>1){
                for( var i = 0;i<pTableOld.length-1;i++){
                    var pData = pTableOld[i].split("µ"); 
                    document.getElementById("txtApproxArea_"+(i+1)).value = pData[0];
                    document.getElementById("cmbBuildStatus_"+(i+1)).value = pData[1];
                    document.getElementById("txtUSBFloor_"+(i+1)).value = pData[2];
                    document.getElementById("txtUSBArea_"+(i+1)).value = pData[3];
                    document.getElementById("txtFloor_"+(i+1)).value = pData[4];
                    document.getElementById("cmbBuildingNo_"+(i+1)).value = pData[5];
                    document.getElementById("txtOwnerName_"+(i+1)).value = pData[6];
                    document.getElementById("txtPhNo_"+(i+1)).value = pData[7];
                    document.getElementById("txtPropRent_"+(i+1)).value = pData[8];
                    document.getElementById("cmbATM_"+(i+1)).value = pData[9];
                    document.getElementById("cmbStrongRoom_"+(i+1)).value = pData[10];
                    document.getElementById("cmbSafeRoom_"+(i+1)).value = pData[11];
                    document.getElementById("txtCommentStrong_"+(i+1)).value = pData[12];
                }
            }
        }
        function TableFill(){
            var approxArea;
            var cmbBuildStatus;
            var usbFloor;
            var usbArea;
            var txtFloor;
            var bldngNo;
            var ownerName;
            var phNo;
            var rent;
            var atm;
            var strngRoom;
            var comment2;
            var safeRoom;
            var passData = '';
            if(col_id !=1){
                for(var i = 1;i<col_id;i++){
                    approxArea = $("#txtApproxArea_"+(i)).val();
                    cmbBuildStatus = $("#cmbBuildStatus_"+(i)).val();
                    usbFloor = $("#txtUSBFloor_"+(i)).val();
                    usbArea = $("#txtUSBArea_"+(i)).val();
                    txtFloor = $("#txtFloor_"+(i)).val();
                    bldngNo = $("#cmbBuildingNo_"+(i)).val();
                    ownerName = $("#txtOwnerName_"+(i)).val();
                    phNo = $("#txtPhNo_"+(i)).val();
                    rent = $("#txtPropRent_"+(i)).val();
                    atm = $("#cmbATM_"+(i)).val();
                    strngRoom = $("#cmbStrongRoom_"+(i)).val();
                    safeRoom = $("#cmbSafeRoom_"+(i)).val();
                    comment2 = $("#txtCommentStrong_"+(i)).val();
                    passData += approxArea + "µ" + cmbBuildStatus + "µ" + usbFloor + "µ" + usbArea + "µ" + txtFloor + "µ" + bldngNo + "µ" + ownerName + "µ" + phNo + "µ" + rent + "µ" + atm + "µ" + strngRoom + "µ" + safeRoom + "µ" + comment2 + "¥";
                }
                document.getElementById("<%= hidPremiseDtlOld.ClientID %>").value=passData;
            }
            document.getElementById("<%= pnlSection2.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div>";
            tab += "<table>";
            tab += "<tr>";
            tab += "<td>";
//            tab += "<div style='text-align:center;'><br /><input type = 'checkbox' ID='chkAttendSec2' style='display:none;'/><b>Attend</b></div>";
            tab += "<div style='text-align:center;"+styleCheckFlag4+"'><br /><input type = 'checkbox' ID='chkAttendSec4' onchange = 'check4Click()' /><b>&nbsp;&nbsp;Attend</b><br /><br /></div>";
            tab += "<div class='mainhead' style='width:100%; height:auto; padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
            tab += "<table id = 'x' style='width:100%; margin:0px auto;font-family:cambria;line-height:20px;background-color:#EEB8A6;' align='left'>";
            
            /*----section 2 begin----*/
            tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;"+styleFlag2+"' align='left' class='sec2'>";
            tab += "<td style='text-align:center;' class='NormalText'>Particulars</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText' "+DisFlag2+">Premise "+i+"</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Approximate Area (in sqft)</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtApproxArea_"+i+"' "+DisFlag2+" name='txtApproxArea_"+i+"'  type='Text' style='width:99%;' class='NormalText sec2' onkeypress='NumericCheck(event)'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Building status</td>";
            for(var i = 1;i<=col_id;i++){
                var select = "<select id='cmbBuildStatus_"+i+"' "+DisFlag2+" class='NormalText cmbBuildStatus sec2' style='width:99%;'>";        
                var cmbData = $("#<%= hidBuildingStatus.ClientID %>").val();
                var rows = cmbData.split("Ñ");
                for (var a = 0; a < rows.length-1; a++) 
                {
                    var cols = rows[a].split("ÿ");
                    select += "<option value='"+cols[0]+"'>"+cols[1]+"</option>";                    
                }
                select += "</select>";
                tab += "<td style='text-align:center;'>"+select+"</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Present USB Floor</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtUSBFloor_"+i+"' "+DisFlag2+" name='txtUSBFloor_"+i+"'  type='Text' style='width:99%;' class='NormalText sec2'/>";
                tab += "</td>";
            }
            tab += "</tr> ";
            tab += "<tr class='sub_second sec2' style='"+styleFlag2+"'>";   
            tab += "<td style='text-align:center;' class='NormalText'>Present USB Area</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtUSBArea_"+i+"' "+DisFlag2+" name='txtUSBArea"+i+"'  type='Text' style='width:99%;' class='NormalText sec2'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Floor (0,1,2,3..)</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtFloor_"+i+"' "+DisFlag2+" name='txtFloor"+i+"'  type='Text' style='width:99%;' class='NormalText sec2' onkeypress='NumericCheck(event)'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Building No available</td>";
            for(var i = 1;i<=col_id;i++){
                select = "<select id='cmbBuildingNo_"+i+"' "+DisFlag2+" class='NormalText sec2' name='cmbBuildingNo"+i+"' style='width:99%;'>";      
                select += "<option value='-1'> ---Select---  </option>";
                select += "<option value='1'>Yes</option>";
                select += "<option value='2'>No</option>";
                select += "</select>";
                tab += "<td style='text-align:center;' class='NormalText'>"+select+"</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Building owner name</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtOwnerName_"+i+"' "+DisFlag2+" name='txtOwnerName_"+i+"'  type='Text' style='width:99%;' class='NormalText sec2' maxlength = 50/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>contact number</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtPhNo_"+i+"' "+DisFlag2+" name='txtPhNo_"+i+"'  type='Text' style='width:99%;' class='NormalText sec2' maxlength = 50 onkeypress='NumericCheck(event)'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Rent proposed</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtPropRent_"+i+"' "+DisFlag2+" name='txtPropRent_"+i+"'  type='Text' style='width:99%;' class='NormalText sec2' onkeypress='NumericCheck(event)'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Availbility of ATM</td>";
            for(var i = 1;i<=col_id;i++){
                select = "<select id='cmbATM_"+i+"' "+DisFlag2+" class='NormalText sec2' style='width:99%;'>";      
                select += "<option value='-1'> ---Select---  </option>";
                select += "<option value='1'>Yes</option>";
                select += "<option value='2'>No</option>";
                select += "</select>";
                tab += "<td style='text-align:center;' class='NormalText'>"+select+"</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Strong room required?</td>";
            for(var i = 1;i<=col_id;i++){
                select = "<select id='cmbStrongRoom_"+i+"' "+DisFlag2+" class='NormalText' style='width:99%;'>";      
                select += "<option value='-1'> ---Select---  </option>";
                select += "<option value='1'>Yes</option>";
                select += "<option value='2'>No</option>";
                select += "</select>";
                tab += "<td style='text-align:center;' class='NormalText sec2'>"+select+"</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Safe room required?</td>";
            for(var i = 1;i<=col_id;i++){
                select = "<select id='cmbSafeRoom_"+i+"' "+DisFlag2+" class='NormalText' style='width:99%;'>";      
                select += "<option value='-1'> ---Select---  </option>";
                select += "<option value='1'>Yes</option>";
                select += "<option value='2'>No</option>";
                select += "</select>";
                tab += "<td style='text-align:center;' class='NormalText sec2'>"+select+"</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Comment box</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<textarea class='NormalText sec2' " + DisFlag2 + " id='txtCommentStrong_" + i + "' name='txtCommentStrong_" + i + "' type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)'></textarea>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec2' style='"+styleFlag2+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Attachments</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText sec2'>";
                var PremData = $('#<%= hidPremData.ClientID %>').val().split("Ø");
                tab += "<ul>";
                for (var j = 0; j < PremData.length-1; j++) {
                    var rowData = PremData[j].toString().split("µ");
                    if (rowData[2] == i) {
                        tab += "<li>";
                        tab += "<a href='#/' id = 'PremAttachment_" + j + "' onclick='ViewAttach1("+i.toString()+")'>" + rowData[1].toString() + "</a>";
                        tab += "</li>";
                    }
                }
                tab += "</ul>";
                tab += "</td>";
            }
            tab += "</tr>";

            for (var i = 1; i <= col_id; i++) {
                $("#<%= cmbPremAttach.ClientID %> option[value='"+i+"']").remove();
                $('#<%= cmbPremAttach.ClientID %>').append('<option value="' + i + '">Premise ' + i + '</option>');
            }
                

            /*----section 2 end----*/


            /*----section 3 begin (Admin)----*/
            tab += "<tr class='sub_first sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Business Intimation received date</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input type='date' id='DateIntimationRecieved_"+i+"' "+DisFlag3+" name='DateIntimationRecieved_"+i+"' class='NormalText sec3'></td>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Preliminary Agreement Signed</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input type='date' id='DatePrelimnaryAgreement_"+i+"' "+DisFlag3+" name='DatePrelimnaryAgreement_"+i+"' class='NormalText sec3'></td>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Execution Engineer</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtExecutionEngineer_"+i+"' "+DisFlag3+" name='txtExecutionEngineer_"+i+"'  type='Text' style='width:99%;' class='NormalText sec3'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>visting planning date</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input type='date' id='DateVisitingPlan_"+i+"' "+DisFlag3+" name='DateVisitingPlan_"+i+"' class='NormalText sec3'></td>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_first sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>visited date</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input type='date' id='DateVisited_"+i+"' "+DisFlag3+" name='DateVisited_"+i+"' class='NormalText sec3'></td>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Premise  status updation by Admin</td>";
            for(var i = 1;i<=col_id;i++){
                var select = "<select id='cmbPremiseStausAdmin_"+i+"' "+DisFlag3+" class='NormalText sec3' style='width:99%;'>";      
                select += "<option value='-1'> ---Select---  </option>";
                select += "<option value='1'> Premises on HOLD</option>";
                select += "<option value='2'>Cancelled</option>";
                select += "<option value='3'>Suitable premises</option>";
                select += "</select>";
                tab += "<td style='text-align:center;'>"+select+"</td>";
            }
             tab += "</tr>";
            tab += "<tr class='sub_first sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Comment box</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<textarea id='txtCommentBox_" + i + "' name='txtCommentBox_" + i + "' " + DisFlag3 + " type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)' class='NormalText sec3'/></textarea>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Present status of buiding at time of proposal</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtBulidingPresStatus_"+i+"' name='txtBulidingPresStatus_"+i+"' "+DisFlag3+" type='Text' style='width:99%;' class='NormalText sec3'/>";
                tab += "</td>";
            }
           
             tab += "</tr>";
            tab += "<tr class='sub_first sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Proposal to Committee from admin</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input type='date' id='DateProposalToCommittee_"+i+"' "+DisFlag3+" name='DateProposalToCommittee_"+i+"' class='NormalText sec3'></td>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Area in Sqft</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtAreaSqft_"+i+"' name='txtAreaSqft_"+i+"' "+DisFlag3+" type='Text' style='width:99%;' class='NormalText sec3'/>";
                tab += "</td>";
            }
             tab += "</tr>";
            tab += "<tr class='sub_first sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Rent Agreed</td>";
           for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtRentAgreed_"+i+"' name='txtRentAgreed_"+i+"' "+DisFlag3+" type='Text' style='width:99%;' class='NormalText sec3'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>panchayath/corporation</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtPanchayat_"+i+"' name='txtPanchayat_"+i+"' "+DisFlag3+" type='Text' style='width:99%;' class='NormalText sec3'/>";
                tab += "</td>";
            }
            
             tab += "</tr>";
            tab += "<tr class='sub_first sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'> village</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtVillage_"+i+"' name='txtVillage_"+i+"'  type='Text' "+DisFlag3+" style='width:99%;' class='NormalText sec3'/>";
                tab += "</td>";
            }
            tab += "</tr>";
            tab += "<tr class='sub_second sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>landmark </td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtLandmark_"+i+"' name='txtLandmark_"+i+"'  type='Text' "+DisFlag3+" style='width:99%;' class='NormalText sec3'/>";
                tab += "</td>";
            }
             tab += "</tr>";
             tab += "<tr class='sub_second sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Address </td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtAddress_"+i+"' name='txtAddress_"+i+"'  type='Text' "+DisFlag3+" style='width:99%;' class='NormalText sec3'/>";
                tab += "</td>";
            }
             tab += "</tr>";
             tab += "<tr class='sub_second sec3' style='"+styleFlag3+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Pin code</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<input id='txtPin_"+i+"' name='txtPin_"+i+"'  type='Text' "+DisFlag3+" style='width:99%;' class='NormalText sec3'/>";
                tab += "</td>";
            }
             tab += "</tr>";



            /*----section 3 end (Admin)----*/


            /*----section 4 begin----*/
            tab += "<tr class='sub_first sec4' style='"+styleFlag4+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Building Lead Status</td>";
            for(var i = 1;i<=col_id;i++){
                select = "<select id='cmbBldngLeadStatus_"+i+"' class='NormalText sec4' style='width:99%;' "+DisFlag4+">";  
                var cmbData = $("#<%= hidBldngLeadStatus.ClientID %>").val();
                var rows = cmbData.split("Ñ");
                for (var a = 0; a < rows.length-1; a++) 
                {
                    var cols = rows[a].split("ÿ");
                    select += "<option value='"+cols[0]+"'>"+cols[1]+"</option>";                    
                }
                select += "</select>";
                tab += "<td style='text-align:center;'>"+select+"</td>";
            }
            tab += "</tr>";
            
            tab += "<tr class='sub_second sec4' style='"+styleFlag4+"'>";
            tab += "<td style='text-align:center;' class='NormalText'>Reason for rejection</td>";
            for(var i = 1;i<=col_id;i++){
                tab += "<td style='text-align:center;' class='NormalText'>";
                tab += "<textarea id='txtCommentRejReason_" + i + "' name='txtCommentRejReason_" + i + "' " + DisFlag4 + " type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)' class='NormalText sec4'></textarea>";
                tab += "</td>";
            }
            tab += "</tr>";
            /*----section 4 end----*/


            tab += "</table>";
            tab += "</div>";      
            tab += "</td>";            
            tab += "</tr>";
            tab += "</table>";
            tab += "<img src='../Image/Addd1.gif' style='height:18px; width:18px; float:center; z-index:1; cursor: pointer;  padding-right:10px;' onclick='AddNewPremise()' title='Add New' /> "
            
            tab += "</div>";            
            document.getElementById("<%= pnlSection2.ClientID %>").innerHTML = tab;  
            TableFillPremiseData();            
             
            SectionID = $("#<%= hidSection.ClientID %>").val();
            if(SectionID == 1){
            }
            if(SectionID == 2){
                
            }
            if(SectionID == 4){	
                $('.chkAttend4').show();
                if($('#<%= hidAttend4.ClientID %>').val()== 0){
                    document.getElementById("chkAttendSec4").checked = false;
                    document.getElementById("chkAttendSec4").disabled = false;
                }
                else{
                    document.getElementById("chkAttendSec4").checked = true;
                    document.getElementById("chkAttendSec4").disabled = true;
                }
            }
            if(SectionID == 9){
	            $('#<%= chkAttendSec6.ClientID %>').show(); 
                $('#<%= chkAttendSec7.ClientID %>').show();
                $('#<%= chkAttendSec8.ClientID %>').show();
                if($('#<%= hidAttend6.ClientID %>').val()== 0){
                    document.getElementById("chkAttendSec6").checked = false;
                    document.getElementById("chkAttendSec6").disabled = false;
                }
                else{
                    document.getElementById("chkAttendSec6").checked = true;
                    document.getElementById("chkAttendSec6").disabled = true;
                }
                if($('#<%= hidAttend7.ClientID %>').val()== 0){
                    document.getElementById("chkAttendSec7").checked = false;
                    document.getElementById("chkAttendSec7").disabled = false;
                }
                else{
                    document.getElementById("chkAttendSec7").checked = true;
                    document.getElementById("chkAttendSec7").disabled = true;
                }
                if($('#<%= hidAttend8.ClientID %>').val()== 0){
                    document.getElementById("chkAttendSec8").checked = false;
                    document.getElementById("chkAttendSec8").disabled = false;
                }
                else{
                    document.getElementById("chkAttendSec8").checked = true;
                    document.getElementById("chkAttendSec8").disabled = true;
                }
            }
        }
        
        function btnSave_onclick(){
                mode = 1;
            /*----Save Section 1 Begin----*/
            if(SectionID == 1){
                var locApproveBoard = $('#<%= txtLocApproveBoard.ClientID %>').val();
                var locRBI = $('#<%= txtLocRBI.ClientID %>').val();
                var revcievedDate = $('#<%= txtRecievedDate.ClientID %>').val();
                var f6Date = $('#<%= txtF6Date.ClientID %>').val();
                var finalRBIDate = $('#<%= txtFinalRBIDate.ClientID %>').val();
                var premiseType = $('#<%= cmbPremiseType.ClientID %>').val()
                var propLaunchDate = $('#<%= txtPropLaunchDate.ClientID %>').val();
                var bussClass = $('#<%= cmbBussClass.ClientID %>').val();
                var popCategory = $('#<%= cmbPopCategory.ClientID %>').val();
                var cluster = $('#<%= cmbCluster.ClientID %>').val();
                var branchID = $('#<%= txtBranchID.ClientID %>').val();
                var branchName = $('#<%= txtBranchName.ClientID %>').val();
                var premIdentify = $('#<%= cmbPremIdentify.ClientID %>').val();
                var intimate;
                if(premIdentify == 1){
                    intimate = 1;
                }
                else{
                    intimate = 0;
                }
                var ToData1 = "1Ø" + SectionID + "Ø" + locApproveBoard+ "Ø" + locRBI + "Ø" + revcievedDate + "Ø" + f6Date + "Ø" + finalRBIDate + "Ø" + premiseType + "Ø" + propLaunchDate + "Ø" + bussClass + "Ø" + popCategory + "Ø" + cluster + "Ø" + branchID + "Ø" + branchName + "Ø" + premIdentify + "Ø" + mode + "Ø" + intimate;
//                alert(ToData1);
                ToServer(ToData1,1);
            }
            /*----Save Section 1 End----*/
            /*----Save Section 9 Begin----*/
            if(SectionID == 9){
                var BrSealDate = $('#<%= txtBrSealDate.ClientID %>').val();
                var StationeryDate = $('#<%= txtStationeryDate.ClientID %>').val();
                var MICRCode = $('#<%= txtMICRCode.ClientID %>').val();
                var cmbMICRCode = $('#<%= cmbMICRCode.ClientID %>').val();
                var BSRCode = $('#<%= txtBSRCode.ClientID %>').val();
                var cmbBSRCode = $('#<%= cmbBSRCode.ClientID %>').val(); 
                var UserIDMapping = $('#<%= cmbUserIDMapping.ClientID %>').val();
                var TabDate = $('#<%= txtTabDate.ClientID %>').val();
                var PRNum = $('#<%= txtPRNum.ClientID %>').val();
                var SoftDate = $('#<%= txtSoftDate.ClientID %>').val();
                var InaugurationDate = $('#<%= txtInaugurationDate.ClientID %>').val();
                var attend;
                if (($('#<%= chkAttendSec6.ClientID %>').is(":checked") == true) && ($('#<%= chkAttendSec7.ClientID %>').is(":checked") == true) && ($('#<%= chkAttendSec8.ClientID %>').is(":checked") == true)) {
                    attend = "1";
                }
                else {
                    attend = "0";
                    alert("Please confirm that you are attending the section to save data");
                    return false;
                }   
                var ToData9 = "1Ø" + SectionID + "Ø" + BrSealDate + "Ø" + StationeryDate + "Ø" + MICRCode + "Ø" + cmbMICRCode + "Ø" + BSRCode + "Ø" + cmbBSRCode + "Ø" + UserIDMapping + "Ø" + TabDate + "Ø" + PRNum + "Ø" + SoftDate + "Ø" + InaugurationDate + "Ø" + mode + "Ø" + attend;
//                alert(ToData9);
                ToServer(ToData9,1);
            }
            /*----Save Section 9 End----*/
            /*----Save Section 2 Begin----*/
            if(SectionID == 2){
                var approxArea;
                var cmbBuildStatus;
                var usbFloor;
                var usbArea;
                var txtFloor;
                var bldngNo;
                var ownerName;
                var phNo;
                var rent;
                var atm;
                var strngRoom;
                var comment2;
                var safeRoom;
                var passData = '';
                for(var i = 0;i<=col_id-1;i++){
                    approxArea = $("#txtApproxArea_"+(i+1)).val();
                    cmbBuildStatus = $("#cmbBuildStatus_"+(i+1)).val();
                    usbFloor = $("#txtUSBFloor_"+(i+1)).val();
                    usbArea = $("#txtUSBArea_"+(i+1)).val();
                    txtFloor = $("#txtFloor_"+(i+1)).val();
                    bldngNo = $("#cmbBuildingNo_"+(i+1)).val();
                    ownerName = $("#txtOwnerName_"+(i+1)).val();
                    phNo = $("#txtPhNo_"+(i+1)).val();
                    rent = $("#txtPropRent_"+(i+1)).val();
                    atm = $("#cmbATM_"+(i+1)).val();
                    strngRoom = $("#cmbStrongRoom_"+(i+1)).val();
                    safeRoom = $("#cmbSafeRoom_"+(i+1)).val();
                    comment2 = $("#txtCommentStrong_"+(i+1)).val();
                    passData += (i+1) + "µ" + approxArea + "µ" + cmbBuildStatus + "µ" + usbFloor + "µ" + usbArea + "µ" + txtFloor + "µ" + bldngNo + "µ" + ownerName + "µ" + phNo + "µ" + rent + "µ" + atm + "µ" + strngRoom + "µ" + comment2 + "µ" + safeRoom + "¥";
                }
                var locApproveBoard = $('#<%= txtLocApproveBoard.ClientID %>').val();
                var locRBI = $('#<%= txtLocRBI.ClientID %>').val();
                var revcievedDate = $('#<%= txtRecievedDate.ClientID %>').val();
                var f6Date = $('#<%= txtF6Date.ClientID %>').val();
                var finalRBIDate = $('#<%= txtFinalRBIDate.ClientID %>').val();
                var premiseType = $('#<%= cmbPremiseType.ClientID %>').val()
                var propLaunchDate = $('#<%= txtPropLaunchDate.ClientID %>').val();
                var bussClass = $('#<%= cmbBussClass.ClientID %>').val();
                var popCategory = $('#<%= cmbPopCategory.ClientID %>').val();
                var cluster = $('#<%= cmbCluster.ClientID %>').val();
                var branchID = $('#<%= txtBranchID.ClientID %>').val();
                var branchName = $('#<%= txtBranchName.ClientID %>').val();
                var premIdentify = $('#<%= cmbPremIdentify.ClientID %>').val();

                var ToData2 = "1Ø" + SectionID + "Ø" + col_id + "Ø" + mode + "Ø" + passData + "Ø" + locApproveBoard+ "Ø" + locRBI + "Ø" + revcievedDate + "Ø" + f6Date + "Ø" + finalRBIDate + "Ø" + premiseType + "Ø" + propLaunchDate + "Ø" + bussClass + "Ø" + popCategory + "Ø" + cluster + "Ø" + branchID + "Ø" + branchName + "Ø" + premIdentify + "Ø" + 0;
                ToServer(ToData2,1);
            }
            /*----Save Section 2 End----*/
            /*----Save Section 4 Begin----*/
            if(SectionID == 4){
                var bldngLeadStatus;
                var comment4;
                var passData = '';
                for(var i = 0;i<=col_id-1;i++){
                    bldngLeadStatus = $("#cmbBldngLeadStatus_"+(i+1)).val();
                    comment4 = $("#txtCommentRejReason_"+(i+1)).val();
                    passData += (i+1) + "µ" + bldngLeadStatus + "µ" + comment4 + "¥";
                }
                var attend;
                if($('#chkAttendSec4').is(":checked") == true)
                {
                    attend = "1";
                }
                else{
                    attend = "0";
                    alert("Please confirm that you are attending the section to save data");
                    return false;
                }
                var ToData4 = "1Ø" + SectionID + "Ø" + col_id + "Ø" + mode + "Ø" + passData;
                ToServer(ToData4,1);
            }
            /*----Save Section 4 End----*/
        }
        function btnFinish_onclick(){
            var finishAlert = confirm("Once You Finish The Section, You Won't Be Able To Update Again!");
            if (finishAlert == true) {  
                    mode = 2;
            /*----Finish Section 1 Begin----*/
                if(SectionID == 1){            
                    var locApproveBoard = $('#<%= txtLocApproveBoard.ClientID %>').val();
                    var locRBI = $('#<%= txtLocRBI.ClientID %>').val();
                    var revcievedDate = $('#<%= txtRecievedDate.ClientID %>').val();
                    var f6Date = $('#<%= txtF6Date.ClientID %>').val();
                    var finalRBIDate = $('#<%= txtFinalRBIDate.ClientID %>').val();
                    var premiseType = $('#<%= cmbPremiseType.ClientID %>').val()
                    var propLaunchDate = $('#<%= txtPropLaunchDate.ClientID %>').val();
                    var bussClass = $('#<%= cmbBussClass.ClientID %>').val();
                    var popCategory = $('#<%= cmbPopCategory.ClientID %>').val();
                    var cluster = $('#<%= cmbCluster.ClientID %>').val();
                    var branchID = $('#<%= txtBranchID.ClientID %>').val();
                    var branchName = $('#<%= txtBranchName.ClientID %>').val();
                    var premIdentify = $('#<%= cmbPremIdentify.ClientID %>').val();
                    if(locApproveBoard == "")
                    {
                        alert("Enter Location Approved Date By Board");
                        return false;
                    }
                    if(locRBI == "")
                    {
                        alert("Enter Location Approved Date By RBI");
                        return false;
                    }
                    if(revcievedDate == "")
                    {
                        alert("Enter Recieved Date");
                        return false;
                    }
                    if(f6Date == "")
                    {
                        alert("Enter Form 6 Sent Date To RBI");
                        return false;
                    }
                    if(finalRBIDate == "")
                    {
                        alert("Enter Final Approval Recieved Date From RBI");
                        return false;
                    }
                    if(premiseType == -1)
                    {
                        alert("Select Premise Type");
                        return false;
                    }
                    if(propLaunchDate == "")
                    {
                        alert("Enter Proposed Launch Date");
                        return false;
                    }
                    if(bussClass == -1)
                    {
                        alert("Select Business Classification");
                        return false;
                    }
                    if(popCategory == -1)
                    {
                        alert("Select Population Category");
                        return false;
                    }
                    if(cluster == -1)
                    {
                        alert("Select Cluster");
                        return false;
                    }
                    if(branchID == "")
                    {
                        alert("Enter Branch ID");
                        return false;
                    }
                    if(branchName == "")
                    {
                        alert("Enter Branch Name");
                        return false;
                    }
                    if(premIdentify == -1)
                    {
                        alert("Enter Premise Identification Status");
                        return false;
                    }
                    var ToData1 = "1Ø" + sectionID + "Ø" + locApproveBoard+ "Ø" + locRBI + "Ø" + revcievedDate + "Ø" + f6Date + "Ø" + finalRBIDate + "Ø" + premiseType + "Ø" + propLaunchDate + "Ø" + bussClass + "Ø" + popCategory + "Ø" + cluster + "Ø" + branchID + "Ø" + branchName + "Ø" + premIdentify + "Ø" + mode;
                    ToServer(ToData1,1);
                }
                /*----Finish Section 1 End----*/
                /*----Finish Section 9 Begin----*/
                if(SectionID == 9){
                    var BrSealDate = $('#<%= txtBrSealDate.ClientID %>').val();
                    var StationeryDate = $('#<%= txtStationeryDate.ClientID %>').val();
                    var MICRCode = $('#<%= txtMICRCode.ClientID %>').val();
                    var cmbMICRCode = $('#<%= cmbMICRCode.ClientID %>').val();
                    var BSRCode = $('#<%= txtBSRCode.ClientID %>').val();
                    var cmbBSRCode = $('#<%= cmbBSRCode.ClientID %>').val();                
                    var UserIDMapping = $('#<%= cmbUserIDMapping.ClientID %>').val();
                    var TabDate = $('#<%= txtTabDate.ClientID %>').val();
                    var PRNum = $('#<%= txtPRNum.ClientID %>').val();
                    var SoftDate = $('#<%= txtSoftDate.ClientID %>').val();
                    var InaugurationDate = $('#<%= txtInaugurationDate.ClientID %>').val();
                    if(BrSealDate == "")
                    {
                        alert("Enter Branch Seal Delivery Date");
                        return false;
                    }
                    if(StationeryDate == "")
                    {
                        alert("Enter Stationery Delivery Date");
                        return false;
                    }
                    if(MICRCode == "")
                    {
                        alert("Enter MICR Code");
                        return false;
                    }
                    if(cmbMICRCode == -1)
                    {
                        alert("Select MICR Code Updated Status");
                        return false;
                    }
                    if(BSRCode == "")
                    {
                        alert("Enter BSR Code");
                        return false;
                    }
                    if(cmbBSRCode == -1)
                    {
                        alert("Select BSR Code Update Status");
                        return false;
                    }
                    if(UserIDMapping == -1)
                    {
                        alert("Select User ID Mapping Status");
                        return false;
                    }
                    if(TabDate == "")
                    {
                        alert("Enter Tab Delivery Date");
                        return false;
                    }
                    if(PRNum == "")
                    {
                        alert("Enter PR Number");
                        return false;
                    }
                    if(SoftDate == "")
                    {
                        alert("Enter Soft Launch Date");
                        return false;
                    }
                    if(InaugurationDate == "")
                    {
                        alert("Enter Inauguration Date");
                        return false;
                    }        
                    var attend;
                    if(($('#<%= chkAttendSec6.ClientID %>').is(":checked") == true) &&($('#<%= chkAttendSec7.ClientID %>').is(":checked") == true )&&($('#<%= chkAttendSec8.ClientID %>').is(":checked") == true ))
                    {
                        attend = "1";
                    }
                    else{
                        attend = "0";
                        alert("Please confirm that you are attending the section to save data");
                        return false;
                    }
                    var ToData9 = "1Ø" + SectionID + "Ø" + BrSealDate + "Ø" + StationeryDate + "Ø" + MICRCode + "Ø" + cmbMICRCode + "Ø" + BSRCode + "Ø" + cmbBSRCode + "Ø" + UserIDMapping + "Ø" + TabDate + "Ø" + PRNum + "Ø" + SoftDate + "Ø" + InaugurationDate + "Ø" + mode + "Ø" + attend;
                    ToServer(ToData9,1);
                }
            /*----Finish Section 9 End----*/
            /*----Finish Section 2 Begin----*/
                if(SectionID == 2){
                    var approxArea ;
                    var cmbUSBFloor;
                    var usbFloor;
                    var usbArea;
                    var txtFloor;
                    var bldngNo;
                    var ownerName;
                    var phNo;
                    var rent;
                    var atm;
                    var strngRoom;
                    var safeRoom;
                    var comment2;
                    var passData = '';
                    for(var i = 0;i<=col_id-1;i++){                        
                        approxArea = $("#txtApproxArea_"+(i+1)).val();
//                        if(approxArea == ""){
//                            alert("Enter Approximate Area For Premise "+(i+1));
//                            return false;
//                        }
                        cmbBuildStatus = $("#cmbBuildStatus_"+(i+1)).val();
//                        if(cmbBuildStatus == -1){
//                            alert("Select Building Status For Premise "+(i+1));
//                            return false;
//                        }
                        usbFloor = $("#txtUSBFloor_"+(i+1)).val();
//                        if(usbFloor == ""){
//                            alert("Enter Present USB Floor For Premise "+(i+1));
//                            return false;
//                        }
                        usbArea = $("#txtUSBArea_"+(i+1)).val();
//                        if(usbArea == ""){
//                            alert("Enter Present USB Area For Premise "+(i+1));
//                            return false;
//                        }
                        txtFloor = $("#txtFloor_"+(i+1)).val();
//                        if(txtFloor == ""){
//                            alert("Enter Floor For Premise "+(i+1));
//                            return false;
//                        }
                        bldngNo = $("#cmbBuildingNo_"+(i+1)).val();
//                        if(bldngNo == -1){
//                            alert("Select Building Number Available Status For Premise "+(i+1));
//                            return false;
//                        }
                        ownerName = $("#txtOwnerName_"+(i+1)).val();
//                        if(ownerName == ""){
//                            alert("Enter Owner Name Of Premise "+(i+1));
//                            return false;
//                        }
                        phNo = $("#txtPhNo_"+(i+1)).val();
//                        if(phNo == ""){
//                            alert("Enter Phone Number Of Premise "+(i+1));
//                            return false;
//                        }
                        rent = $("#txtPropRent_"+(i+1)).val();
//                        if(rent == ""){
//                            alert("Enter Proposed Rent For Premise "+(i+1));
//                            return false;
//                        }
                        atm = $("#cmbATM_"+(i+1)).val();
//                        if(atm == -1){
//                            alert("Select ATM Availability For Premise "+(i+1));
//                            return false;
//                        }
                        strngRoom = $("#cmbStrongRoom_"+(i+1)).val();
//                        if(strngRoom == -1){
//                            alert("Select Strong Room Requirement For Premise "+(i+1));
//                            return false;
//                        }
                        safeRoom = $("#cmbSafeRoom_"+(i+1)).val();
//                        if(safeRoom == -1){
//                            alert("Select Safe Room Requirement For Premise "+(i+1));
//                            return false;
//                        }
                        comment2 = $("#txtCommentStrong_"+(i+1)).val();
                        passData += (i+1) + "µ" + approxArea + "µ" + cmbBuildStatus + "µ" + usbFloor + "µ" + usbArea + "µ" + txtFloor + "µ" + bldngNo + "µ" + ownerName + "µ" + phNo + "µ" + rent + "µ" + atm + "µ" + strngRoom + "µ" + comment2 + "µ" + safeRoom + "¥";
                    }
                    var intimate = "1";  

                    var locApproveBoard = $('#<%= txtLocApproveBoard.ClientID %>').val();
                    var locRBI = $('#<%= txtLocRBI.ClientID %>').val();
                    var revcievedDate = $('#<%= txtRecievedDate.ClientID %>').val();
                    var f6Date = $('#<%= txtF6Date.ClientID %>').val();
                    var finalRBIDate = $('#<%= txtFinalRBIDate.ClientID %>').val();
                    var premiseType = $('#<%= cmbPremiseType.ClientID %>').val()
                    var propLaunchDate = $('#<%= txtPropLaunchDate.ClientID %>').val();
                    var bussClass = $('#<%= cmbBussClass.ClientID %>').val();
                    var popCategory = $('#<%= cmbPopCategory.ClientID %>').val();
                    var cluster = $('#<%= cmbCluster.ClientID %>').val();
                    var branchID = $('#<%= txtBranchID.ClientID %>').val();
                    var branchName = $('#<%= txtBranchName.ClientID %>').val();
                    var premIdentify = $('#<%= cmbPremIdentify.ClientID %>').val();
                    if(locApproveBoard == "")
                    {
                        alert("Enter Location Approved Date By Board");
                        return false;
                    }
                    if(locRBI == "")
                    {
                        alert("Enter Location Approved Date By RBI");
                        return false;
                    }
                    if(revcievedDate == "")
                    {
                        alert("Enter Recieved Date");
                        return false;
                    }
                    if(f6Date == "")
                    {
                        alert("Enter Form 6 Sent Date To RBI");
                        return false;
                    }
                    if(finalRBIDate == "")
                    {
                        alert("Enter Final Approval Recieved Date From RBI");
                        return false;
                    }
                    if(premiseType == -1)
                    {
                        alert("Select Premise Type");
                        return false;
                    }
                    if(propLaunchDate == "")
                    {
                        alert("Enter Proposed Launch Date");
                        return false;
                    }
                    if(bussClass == -1)
                    {
                        alert("Select Business Classification");
                        return false;
                    }
                    if(popCategory == -1)
                    {
                        alert("Select Population Category");
                        return false;
                    }
                    if(cluster == -1)
                    {
                        alert("Select Cluster");
                        return false;
                    }
                    if(branchID == "")
                    {
                        alert("Enter Branch ID");
                        return false;
                    }
                    if(branchName == "")
                    {
                        alert("Enter Branch Name");
                        return false;
                    }
                    if(premIdentify == -1)
                    {
                        alert("Enter Premise Identification Status");
                        return false;
                    }

                    var ToData2 = "1Ø" + SectionID + "Ø" + col_id + "Ø" + mode + "Ø" + passData + "Ø" + locApproveBoard+ "Ø" + locRBI + "Ø" + revcievedDate + "Ø" + f6Date + "Ø" + finalRBIDate + "Ø" + premiseType + "Ø" + propLaunchDate + "Ø" + bussClass + "Ø" + popCategory + "Ø" + cluster + "Ø" + branchID + "Ø" + branchName + "Ø" + premIdentify + "Ø" + intimate;
//                    var ToData2 = "1Ø" + SectionID + "Ø" + col_id + "Ø" + approxArea + "Ø" + cmbBuildStatus + "Ø" + usbFloor + "Ø" + usbArea + "Ø" + txtFloor + "Ø" + bldngNo + "Ø" + ownerName + "Ø" + phNo + "Ø" + rent + "Ø" + atm + "Ø" + strngRoom + "Ø" + comment2 + "Ø" + mode + "Ø" + intimate;
                
                    ToServer(ToData2,1);
                }
            /*----Finish Section 2 End----*/
            /*----Finish Section 4 Begin----*/
                if(SectionID == 4){
                    var bldngLeadStatus;
                    var comment4;
                    var passData = '';
                    for(var i = 0;i<=col_id-1;i++){
                        bldngLeadStatus = $("#cmbBldngLeadStatus_"+(i+1)).val();
                        if(bldngLeadStatus == -1){
                            alert("Select Building Lead Status For Premise "+(i+1));
                            return false;
                        }
                        comment4 = $("#txtCommentRejReason_"+(i+1)).val();
                        passData += (i+1) + "µ" + bldngLeadStatus + "µ" + comment4 + "¥";
                    }
                    var intimate = "1";
                    var attend;
                    if($('#chkAttendSec4').is(":checked") == true)
                    {
                        attend = "1";
                    }
                    else{
                        attend = "0";
                        alert("Please confirm that you are attending the section to save data");
                        return false;
                    }
                    var ToData4 = "1Ø" + SectionID + "Ø" + col_id + "Ø" + mode + "Ø" + passData + "Ø" + intimate + "Ø" + attend;
                    ToServer(ToData4,1);
                }
            /*----Finish Section 4 End----*/
            } 
            else {
                return false;
            }
        }
        function FromServer(arg, context) {           
           if(context == 1){
                var Data = arg.split("Ø");
                var errorFlag = Data[0];
                var message = Data[1];
                var mode = Data[2];//Save or Finish
                if(errorFlag == 0){
                    alert(message);
//                    if(mode == 2){
//                        $(".sec2").show();
//                        currentSection = 2;
//                    }
//                    window.open("BO_BusinessProcedures.aspx", "_self");  
                window.open("BO_BusinessProcedures.aspx?Bo_Master_Id="+ BO_Master_Id, "_self");  
                }
            }
        } 
        /*----To fill previously saved datas in premise panel----*/
        function TableFillPremiseData(){
            var pTable = $("#<%= hidPremiseDtl.ClientID %>").val().split("¥");
            for( var i = 0;i<pTable.length-1;i++){
                var pData = pTable[i].split("µ"); 
                document.getElementById("txtApproxArea_"+(i+1)).value = pData[3];
                document.getElementById("cmbBuildStatus_"+(i+1)).value = pData[4];
                document.getElementById("txtUSBFloor_"+(i+1)).value = pData[5];
                document.getElementById("txtUSBArea_"+(i+1)).value = pData[6];
                document.getElementById("txtFloor_"+(i+1)).value = pData[7];
                document.getElementById("cmbBuildingNo_"+(i+1)).value = pData[8];
                document.getElementById("txtOwnerName_"+(i+1)).value = pData[9];
                document.getElementById("txtPhNo_"+(i+1)).value = pData[10];
                document.getElementById("txtPropRent_"+(i+1)).value = pData[11];
                document.getElementById("cmbATM_"+(i+1)).value = pData[12];
                document.getElementById("cmbStrongRoom_"+(i+1)).value = pData[13];
                document.getElementById("cmbSafeRoom_"+(i+1)).value = pData[14];
                document.getElementById("txtCommentStrong_" + (i + 1)).value = pData[15];
                if (pData[19] != "") {
                    var CreateDate = new Date(pData[19]);
                    dd = CreateDate.getDate(); if (dd < 10) dd = "0" + dd;
                    mm = CreateDate.getMonth() + 1; if (mm < 10) mm = "0" + mm;
                    yyyy = CreateDate.getFullYear();
                    CreateDate = yyyy + '-' + mm + '-' + dd;
                    document.getElementById("DatePrelimnaryAgreement_" + (i + 1)).value = CreateDate;
                }
//                document.getElementById("DatePrelimnaryAgreement_"+(i+1)).value = pData[19];
                document.getElementById("txtExecutionEngineer_" + (i + 1)).value = pData[20];
                if (pData[21] != "") {
                    var CreateDate = new Date(pData[21]);
                    dd = CreateDate.getDate(); if (dd < 10) dd = "0" + dd;
                    mm = CreateDate.getMonth() + 1; if (mm < 10) mm = "0" + mm;
                    yyyy = CreateDate.getFullYear();
                    CreateDate = yyyy + '-' + mm + '-' + dd;
                    document.getElementById("DateVisitingPlan_" + (i + 1)).value = CreateDate;
                }
//                document.getElementById("DateVisitingPlan_"+(i+1)).value = pData[21];
                if (pData[22] != "") {
                    var CreateDate = new Date(pData[22]);
                    dd = CreateDate.getDate(); if (dd < 10) dd = "0" + dd;
                    mm = CreateDate.getMonth() + 1; if (mm < 10) mm = "0" + mm;
                    yyyy = CreateDate.getFullYear();
                    CreateDate = yyyy + '-' + mm + '-' + dd;
                    document.getElementById("DateVisited_" + (i + 1)).value = CreateDate;
                }
//                document.getElementById("DateVisited_"+(i+1)).value = pData[22];
                document.getElementById("cmbPremiseStausAdmin_"+(i+1)).value = pData[23];
                document.getElementById("txtCommentBox_"+(i+1)).value = pData[24];
                document.getElementById("txtBulidingPresStatus_" + (i + 1)).value = pData[25];
                if (pData[26] != "") {
                    var CreateDate = new Date(pData[26]);
                    dd = CreateDate.getDate(); if (dd < 10) dd = "0" + dd;
                    mm = CreateDate.getMonth() + 1; if (mm < 10) mm = "0" + mm;
                    yyyy = CreateDate.getFullYear();
                    CreateDate = yyyy + '-' + mm + '-' + dd;
                    document.getElementById("DateProposalToCommittee_" + (i + 1)).value = CreateDate;
                }
//                document.getElementById("DateProposalToCommittee_"+(i+1)).value = pData[26];
                document.getElementById("txtAreaSqft_"+(i+1)).value = pData[27];
                document.getElementById("txtRentAgreed_"+(i+1)).value = pData[28];
                document.getElementById("txtPanchayat_"+(i+1)).value = pData[29];
                document.getElementById("txtVillage_"+(i+1)).value = pData[30];
                document.getElementById("txtLandmark_"+(i+1)).value = pData[31];
                document.getElementById("txtAddress_"+(i+1)).value = pData[32];
                document.getElementById("txtPin_"+(i+1)).value = pData[33];
                document.getElementById("cmbBldngLeadStatus_"+(i+1)).value = pData[36];
                document.getElementById("txtCommentRejReason_"+(i+1)).value = pData[37];
            }

            

/*-----TABLE FILLL DATA FOR PREMISE WHEN ADD NEW PREMISE WITHOUT SAVING------*/


            /*------------------------------------*/

        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            //            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");  
            window.open("TeamView.aspx?Bo_Master_Id=" + BO_Master_Id, "_self");    
        }
        function ViewAttach1(i) {
            var BO_Section_Id = SectionID;
            var BO_Premise_Id;
            if (BO_Section_Id == 1) {
                BO_Premise_Id = 0;
            }
            else {
                BO_Premise_Id = i;
            }
            window.open("ViewAttachment.aspx?BO_Master_Id="+BO_Master_Id+" &BO_Section_Id="+BO_Section_Id+" &BO_Premise_Id="+BO_Premise_Id,"_blank","Height=400px,Width:500px,Left=300,Top=300,center:yes");
        }
        function RemoveAttach1(){
            var BO_Section_Id = 1;
            var BO_Premise_Id = 0;
            var ToData = "4Ø" + BO_Master_Id + "Ø" + BO_Section_Id + "Ø" + BO_Premise_Id;
            ToServer(ToData,4);
        }
        function check4Click(){
            DisFlag4 = '';
            document.getElementById('<%= hidAttend4.ClientID %>').value = "1";            
            TableFill();
            TableFillPremiseData();
        }
    </script>
</head>
</html>
 <br />
 <div  style="width:90%;margin:0px auto; background-color:#EEB8A6;">
    <div style="width:80%;padding-left:10px;margin:0px auto;">
        <asp:HiddenField ID="hidData1" runat="server" />
        <asp:HiddenField ID="hid_BO_Master_Id" runat="server" />
        <asp:HiddenField ID = "hidUser" runat="server" />
        <asp:HiddenField ID = "hidBuildingStatus" runat="server" />
        <asp:HiddenField ID = "hidBldngLeadStatus" runat="server" />
        <asp:HiddenField ID = "hidPremiseDtl" runat="server" />
        <asp:HiddenField ID = "hidPremiseDtlOld" runat="server" />
        <asp:HiddenField ID = "hidenable1" runat="server" />
        <asp:HiddenField ID = "hidenable2" runat="server" />
        <asp:HiddenField ID = "hidenable3" runat="server" />
        <asp:HiddenField ID = "hidenable4" runat="server" />
        <asp:HiddenField ID = "hidenable9" runat="server" />
        <asp:HiddenField ID = "hidSection" runat="server" />
        <asp:HiddenField ID = "hidAttend1" runat="server" />
        <asp:HiddenField ID = "hidAttend2" runat="server" />
        <asp:HiddenField ID = "hidAttend4" runat="server" />
        <asp:HiddenField ID = "hidAttend6" runat="server" />
        <asp:HiddenField ID = "hidAttend7" runat="server" />
        <asp:HiddenField ID = "hidAttend8" runat="server" />
        <asp:HiddenField ID = "hidIntimate1" runat="server" />
        <asp:HiddenField ID = "hidBtn" runat="server" />
        <asp:HiddenField ID = "hid_Value" runat="server" />
        <asp:HiddenField ID = "UploadedData" runat="server" />
        <asp:HiddenField ID = "hid_Sec1_Attachment" runat="server" />
        <asp:HiddenField ID = "hid_Sec2_Attachment" runat="server" />
        <asp:HiddenField ID = "hidPremAttach" runat="server" />
        <asp:HiddenField ID = "hidPremData" runat="server" />
        <br /> 
    </div>
    <div id = "divSection1" class = "sec1" style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">            
            <tr id="List">
                <td style="text-align: center;" colspan="4">
                    <asp:Panel ID="pnlSection1" Style="width: 100%;  text-align: right; float: right;" runat="server">
                        <div class="mainhead" style='width:100%; height:auto; padding-top:0px;margin:0px auto;background-color:white;'>
                            <table style='width:100%;font-family:cambria;' align='center'>
                                <tr>
                                    <td style='width:25%;text-align:left;'></td>
                                    <td style='width:25%;text-align:left;'></td>
                                    <td style='width:25%;text-align:left;'></td>
                                    <td style='width:25%;text-align:left;'></td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Location Approved Date By Board
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtLocApproveBoard" class="NormalText sec1" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtLocApproveBoard_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtLocApproveBoard" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Location Approved Date By RBI
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtLocRBI" class="NormalText sec1" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtLocRBI_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtLocRBI" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Recieved Date
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtRecievedDate" class="NormalText sec1" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtRecievedDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtRecievedDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                         Form 6 Sent Date To RBI 
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtF6Date" class="NormalText sec1" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtF6Date_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtF6Date" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Form 6 Attachment
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" Width="95%"/>
                                        <asp:Button ID="Upload" runat="server" Text="Upload" Font-Names="Cambria" style="cursor:pointer; Width=15%;text-align:right;" />
                                        <a id = "UploadedAttachment1" runat = "server" onclick="ViewAttach1(1)" width = "75%" ></a>
                                        <asp:Button ID="Remove1" runat="server" Text="Remove" Font-Names="Cambria" style="cursor:pointer;" width="25%" align = "center"/>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Final Approval Received Date From RBI
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtFinalRBIDate" class="NormalText sec1" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtFinalRBIDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtFinalRBIDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Commencement Mode
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtCommMode" class="NormalText sec1" runat="server" Width="79%" MaxLength="100" ReadOnly="true">
                                        </asp:TextBox>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Type
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtType" class="NormalText sec1" runat="server" Width="79%" MaxLength="100" ReadOnly="true">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Premise Type
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="cmbPremiseType" class="NormalText sec1" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                                        </asp:DropDownList> 
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Proposed Launch Date 
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtPropLaunchDate" class="NormalText sec1" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtPropLaunchDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtPropLaunchDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Business Classification
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="cmbBussClass" class="NormalText sec1" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                                        </asp:DropDownList> 
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Tier
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtTier" class="NormalText sec1" runat="server" Width="79%" MaxLength="100" ReadOnly="true">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        North East
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtNE" class="NormalText sec1" runat="server" Width="79%" MaxLength="100" ReadOnly="true">
                                        </asp:TextBox>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        LWE District
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtLWE" class="NormalText sec1" runat="server" Width="79%" MaxLength="100" ReadOnly="true">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Population Category
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="cmbPopCategory" class="NormalText sec1" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        State
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtState" class="NormalText sec1" runat="server" Width="79%" MaxLength="100" ReadOnly="true">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        District
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtDistrict" class="NormalText sec1" runat="server" Width="79%" MaxLength="100" ReadOnly="true">
                                        </asp:TextBox>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Cluster
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="cmbCluster" class="NormalText sec1" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Branch Name
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtBranchName" class="NormalText sec1" runat="server" Width="79%" MaxLength="100">
                                        </asp:TextBox>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Branch ID
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtBranchID" class="NormalText sec1" runat="server" Width="79%" MaxLength="100" onkeypress='NumericCheck(event)'>
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Premise Identification Status
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="cmbPremIdentify" class="NormalText sec1" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="2">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                    
                                    </td>
                                </tr>
                            </table>                            
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />            
    </div>
    <br />
    <div id = "divSection2" style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;display:none;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">            
            <tr id="Tr1">
                <td style="text-align: center;">
                    <asp:Panel ID="pnlSection2" Style="width: 100%;  text-align: left; float: left;" runat="server">
                       






                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />       
        <table style = "width:90%;height:90px;margin:0px auto;">
            <tr>
                <td style="width:25%;text-align:left;"></td>
                <td style="width:25%;text-align:left;"></td>
                <td style="width:25%;text-align:left;"></td>
                <td style="width:25%;text-align:left;"></td>
            </tr>
            <tr>
                <td style="width:25%;text-align:left;">
                    Select premise to add attachment
                </td>
                <td style="width:25%;text-align:left;">
                    <asp:DropDownList ID="cmbPremAttach" class="NormalText sec9" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                        <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width:25%;text-align:left;"></td>
                <td style="width:25%;text-align:left;"></td>
            </tr>
            <tr>
                <td style="width:25%;text-align:left;">
                    Attachment Category
                </td>
                <td style="width:25%;text-align:left;">
                    <asp:TextBox ID="txtCaptionName" class="NormalText sec9" runat="server" Width="79%" MaxLength="50">
                    </asp:TextBox>
                </td>
                <td style="width:25%;text-align:left;"></td>
                <td style="width:25%;text-align:left;"></td>
            </tr>
            <tr>
                <td style="width:25%;text-align:left;">
                    Select file to upload
                </td>
                <td style="width:25%;text-align:left;">
                    <asp:FileUpload ID="fup2" runat="server" CssClass="fileUpload" Width="95%"/>
                    <asp:Button ID="Upload2" runat="server" Text="Upload" Font-Names="Cambria" style="cursor:pointer; Width=15%;text-align:right;" />
                    <a id = "UploadedAttachment2" runat = "server" onclick="ViewAttach1()" width = "75%"></a>
                    <asp:Button ID="Remove2" runat="server" Text="Remove" Font-Names="Cambria" style="cursor:pointer;" width="25%" align = "center"/>
                </td>
                <td style="width:25%;text-align:left;"></td>
                <td style="width:25%;text-align:left;"></td>
            </tr>            
        </table>    
        <script type="text/javascript">
            $('#<%= cmbPremAttach.ClientID %>').change(function () {
                document.getElementById('<%= hidPremAttach.ClientID %>').value = $('#<%= cmbPremAttach.ClientID %>').val();
            });
        </script> 
    </div>
    <br />
    <div id = "divSection9" class = "sec9" style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;display:none;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">            
            <tr id="Tr2">
                <td style="text-align: center;" colspan="4">
                    <asp:Panel ID="pnlSection9" Style="width: 100%;  text-align: right; float: right;" runat="server">
                        <table style='width:100%;font-family:cambria;display:none' class = 'attend9' align='center'>
                                <tr>
                                    <td style='width:33%;text-align:left;'></td>
                                    <td style='width:34%;text-align:left;'></td>
                                    <td style='width:33%;text-align:left;'></td>
                                </tr>
                                <tr>
                                    <td style='width:33%;text-align:left;'></td>
                                    <td style='width:34%;text-align:left;'>
                                        <asp:CheckBox ID="chkAttendSec6" runat="server"/>
                                        &nbsp;<b>Attend Admin's Intimation</b>
                                    </td>
                                    <td style='width:33%;text-align:left;'></td>
                                </tr>
                                <tr>
                                    <td style='width:33%;text-align:left;'></td>
                                    <td style='width:34%;text-align:left;'>
                                        <asp:CheckBox ID="chkAttendSec7" runat="server"/>
                                        &nbsp;<b>Attend IT's Intimation</b>
                                    </td>
                                    <td style='width:33%;text-align:left;'></td>
                                </tr>
                                <tr>
                                    <td style='width:33%;text-align:left;'></td>
                                    <td style='width:34%;text-align:left;'>
                                        <asp:CheckBox ID="chkAttendSec8" runat="server"/>
                                        &nbsp;<b>Attend HR's Intimation</b>
                                    </td>
                                    <td style='width:33%;text-align:left;'></td>
                                </tr>
                        </table>
                        <br />
                        <div class="mainhead" style='width:100%; height:auto; padding-top:0px;margin:0px auto;background-color:white;'>
                            <table style='width:100%;font-family:'cambria';' align='center'>
                                <tr>
                                    <td style='width:25%;text-align:left;'></td>
                                    <td style='width:25%;text-align:left;'></td>
                                    <td style='width:25%;text-align:left;'></td>
                                    <td style='width:25%;text-align:left;'></td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Branch Seal Delivery Date
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtBrSealDate" class="NormalText sec9" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtBrSealDate_CalendarExtender"  runat="server" Enabled="True" TargetControlID="txtBrSealDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Stationery Delivery Date
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtStationeryDate" class="NormalText sec9" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtStationeryDate_CalendarExtender"  runat="server" Enabled="True" TargetControlID="txtStationeryDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        MICR Code Obtained from RBI
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtMICRCode" class="NormalText sec9" runat="server" Width="79%" MaxLength="50">
                                        </asp:TextBox>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                         Updated MICR Code in Profile ? 
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="cmbMICRCode" class="NormalText sec9" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="2">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        BSR Code Created
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtBSRCode" class="NormalText sec9" runat="server" Width="79%" MaxLength="50">
                                        </asp:TextBox>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Updated BSR Code in Profile ?
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="cmbBSRCode" class="NormalText sec9" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="2">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Completed UserID Mapping ?
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="cmbUserIDMapping" class="NormalText sec9" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> -----Select-----</asp:ListItem>
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="2">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Tab Requested Date
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtTabDate" class="NormalText sec9" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtTabDate_CalendarExtender"  runat="server" Enabled="True" TargetControlID="txtTabDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        PR Number
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtPRNum" class="NormalText sec9" runat="server" Width="79%" MaxLength="50">
                                        </asp:TextBox>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Soft Launch Date
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtSoftDate" class="NormalText sec9" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtSoftDate_CalendarExtender"  runat="server" Enabled="True" TargetControlID="txtSoftDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Date Of Inauguration
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtInaugurationDate" class="NormalText sec9" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtInaugurationDate_CalendarExtender"  runat="server" Enabled="True" TargetControlID="txtInaugurationDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />            
    </div>
    <div style="text-align:center; height: 63px;"><br />
        <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="SAVE" onclick="return btnSave_onclick()"/> 
        &nbsp;&nbsp;
        <input id="btnFinish" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="FINISH" onclick="return btnFinish_onclick()"/> 
        &nbsp;&nbsp;
        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()"/>
    </div>   
</div>
</asp:Content>

