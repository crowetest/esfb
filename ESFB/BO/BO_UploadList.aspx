﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="BO_UploadList.aspx.vb" Inherits="BO_UploadList" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);         
        }        
        .Button:hover
        {            
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            color:#801424;
        }                  
     .bg
     {
         background-color:#FFF;
     }           
    </style>    
    <script language="javascript" type="text/javascript">
        window.onload = function () {
//            TableFill();
        }
        
        function btnSave_onclick(){
            
//            ToServer(saveData ,1);
        }
        function FromServer(arg, context) {           
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
    </script>
</head>
</html>
 <br />
 <div  style="width:90%;margin:0px auto; background-color:#EEB8A6; height: 218px;">
    <div style="width:80%;padding-left:10px;margin:0px auto;">
        <asp:HiddenField ID="hidData" runat="server" />
        <asp:HiddenField ID = "hidUser" runat="server" />
        <br /> 
    </div>
    <div style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table class="style1" style="width: 80%; margin: 0px auto; height: 110px;">
            <tr id="Tr">
            <td style="width: 25%;">
            </td>
            <td style="width: 12%; text-align: left;">
                Select File
            </td>
            <td style="width: 63%">
                &nbsp; &nbsp;
                <div id="fileUploadarea">
                    <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload"/><br />
                </div>
                <br />
            </td>
            <td style="width: 12%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 20%">
                &nbsp;
            </td>
        </tr>
       
            <tr>
            <td style="text-align: center;" colspan="3">
                <br />
                <asp:Button ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" Style="cursor: pointer;"
                    Width="6%" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hdnValue" runat="server" />
                <asp:HiddenField ID="hdnAnnexture" runat="server" />
            </td>
        </tr>
        </table>
                            
    </div>
     
</div>
<br />
<br />
<br />
<br />
</asp:Content>

