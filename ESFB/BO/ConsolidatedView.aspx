﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ConsolidatedView.aspx.vb" Inherits="ConsolidatedView" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);         
        }        
        .Button:hover
        {            
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            color:#801424;
        }                  
     .bg
     {
         background-color:#FFF;
     }           
        .style1
        {
            width: 609px;
        }
        .style2
        {
            width: 652px;
        }
        .style3
        {
            width: 44%;
        }
    </style>    
    <script language="javascript" type="text/javascript">
            function table_fill() {
            document.getElementById("<%= pnUploadList.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;' class=mainhead>";
            tab += "<div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; height:auto; overflow:auto;text-align:center;margin: 0px auto;' align='center'>";
            tab += "<table style='width:100%; height:auto;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:5%;text-align:center;'>No</td>";
            tab += "<td style='width:0%;text-align:center;display:none;'>Bo_master_id</td>";
            tab += "<td style='width:20%;text-align:center'>Branch Name</td>";
            tab += "<td style='width:10%;text-align:center'>District</td>";
            tab += "<td style='width:10%;text-align:center'>State</td>";
            tab += "<td style='width:10%;text-align:center'>Tier</td>";
            tab += "<td style='width:10%;text-align:center'>BO/URC</td>";
            tab += "<td style='width:5%;text-align:center'>North East Y/N</td>";
            tab += "<td style='width:5%;text-align:center'>LWE Dist Y/N</td>";
            tab += "<td style='width:20%;text-align:center'>Commensement Mode</td>";

            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ñ");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("ÿ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
               
                    tab += "<td style='width:5%;text-align:center;'>" + col[0] + "</td>";
                    tab += "<td style='width:0%;text-align:left;display:none;'>" + col[1] + "</td>";
                    tab += "<td style='width:20%;text-align:left'><a href='TeamView.aspx?Bo_Master_Id="+ col[1] +"' target='_self'>" + col[2].toString() + "</a> </td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[8] + "</td>";
                    tab += "<td style='width:20%;text-align:left'>" + col[9] + "</td>";
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnUploadList.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//
        }
        function FromServer(arg, context) {           
           if(context == 1){
                if(arg!=''){
                    document.getElementById("<%= hid_dtls.ClientID %>").value = arg;
                    table_fill();
                }
            }
        }
      function DateOnchange() {
            var UploadId=document.getElementById("<%= cmbDate.ClientID %>").value;
            var ToData = "1Ø" + UploadId;          
            ToServer(ToData, 1);
          
        }

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
    </script>
</head>
</html>
 <br />
<%-- <div  style="width:90%;margin:0px auto; background-color:#EEB8A6; height:auto;">
--%>    <div style="width:80%;padding-left:10px;margin:0px auto; height: 22px;">
        <br /> 
    </div>
  <%--  <div style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
 --%>       <table class="style1" style="width: 80%; margin: 0px auto; height: 158px;">
            <tr> 
                  
                <td style="text-align:right; " class="style3">Upload Date</td>
                <td style="width: 1%;"></td>
                <td style="width:50%">
                    <asp:DropDownList ID="cmbDate" class="NormalText" runat="server" 
                        Font-Names="Cambria" Width="41%" 
                         ForeColor="Black" Height="19px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr> 
                <td colspan="4" class="style2"><asp:Panel ID="pnUploadList" runat="server" 
                        >
                </asp:Panel></td>
            </tr>
        

            <tr>
            <td style="text-align: center;height:auto;" colspan="3" class="style2">
                <br />
              
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 8%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hdnAnnexture" runat="server" />
            </td>
        </tr>
        </table>
                            
<%--    </div>
--%>     
</div>
<br />
<br />
<br />
<br />
</asp:Content>

