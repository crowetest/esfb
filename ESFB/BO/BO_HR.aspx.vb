﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BO_HR
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Dim DTINTIMATE As New DataTable
    Dim dtintiattnd As New DataTable
    Dim DTINTIMATE1 As New DataTable
    Dim dtattnd As New DataTable
    Dim DTINTIMATE2 As New DataTable
    Dim DTINTIMATE3 As New DataTable
    Dim Teamdtl As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1269) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "HR Procedures"
            Dim UserID As Integer = CInt(Session("UserID"))
            Me.hidUser.Value = CStr(UserID)
            Dim master_id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))
            hidmaster.Value = CStr(master_id)
            Dim count As Integer = 0
            Dim rowcount As Integer = 1
            DTINTIMATE = DB.ExecuteDataSet(" SELECT B.* FROM BO_SECTION_INTIMATE A INNER JOIN BO_INTIMATE_DTLS B ON A.SECTION_ID = B.SECTION_ID " +
                                           " WHERE A.INTIMATE_TO_TEAM =4 AND B.INTIMATE_TO_TEAM = 4 AND B.BO_MASTER_ID = '" + master_id.ToString() + "'").Tables(0)
            DTINTIMATE1 = DB.ExecuteDataSet(" SELECT B.* FROM BO_SECTION_INTIMATE A INNER JOIN BO_INTIMATE_DTLS B ON A.SECTION_ID = B.SECTION_ID " +
                                           " WHERE A.INTIMATE_TO_TEAM =4 AND B.INTIMATE_TO_TEAM = 4 AND B.BO_MASTER_ID = '" + master_id.ToString() + "' AND B.ATTEND_BY_TEAM IS null").Tables(0)
            If DTINTIMATE.Rows.Count > 0 Then
                rowcount = 0
            End If

            DT = DB.ExecuteDataSet("select * from bo_hr_proc where bo_master_id= '" + master_id.ToString() + "'").Tables(0)
            If (DT.Rows.Count > 0) Then
                DT1 = DB.ExecuteDataSet(" select distinct bhp.BO_master_id,bhp.desi_id,bcd.combo_text, " +
                                        " required, available, balance, remarks " +
                                        " from BO_hr_proc bhp " +
                                        " inner join  BO_HR_BUSI_DESI_SETTING bds on bds.desi_id=bhp.desi_id and bhp.bo_master_id='" + master_id.ToString() + "' " +
                                        " inner join BO_COMBO_DATA bcd on bcd.combo_id=bds.desi_id and identify_id=15 " +
                                        " where bhp.bo_master_id='" + master_id.ToString() + "'  and identify_id=15").Tables(0)
            Else
                DT1 = DB.ExecuteDataSet(" select bds.busi_classi_id,bds.desi_id,bcd.combo_text,bds.no_required,'0','0','NIL' from BO_COMBO_DATA bcd " +
                                       " inner join  BO_HR_BUSI_DESI_SETTING bds on bds.desi_id=bcd.combo_id and identify_id=15 " +
                                       " where(bds.busi_classi_id = 2)").Tables(0)
            End If
            hidData.Value = GF.GetBranchData(DT1)

            If DTINTIMATE1.Rows.Count = 0 And rowcount = 0 Then
                'DTINTIMATE1 = DB.ExecuteDataSet(" SELECT B.* FROM BO_SECTION_INTIMATE A INNER JOIN BO_INTIMATE_DTLS B ON A.SECTION_ID = B.SECTION_ID " +
                '                                " WHERE A.INTIMATE_TO_TEAM =4 AND B.INTIMATE_TO_TEAM = 4 AND B.BO_MASTER_ID = '" + master_id.ToString() + "' AND B.ATTEND_BY_TEAM IS not null and work_ongoing=1").Tables(0)
                'DTINTIMATE2 = DB.ExecuteDataSet(" SELECT * FROM BO_INTIMATE_DTLS " +
                '                                " WHERE section_id =8 AND BO_MASTER_ID = '" + master_id.ToString() + "' AND ATTEND_BY_TEAM IS not null ").Tables(0)
                DTINTIMATE3 = DB.ExecuteDataSet(" SELECT B.* FROM BO_SECTION_INTIMATE A INNER JOIN BO_INTIMATE_DTLS B ON A.SECTION_ID = B.SECTION_ID " +
                                                " WHERE A.INTIMATE_TO_TEAM =4 AND B.INTIMATE_TO_TEAM = 4 AND B.BO_MASTER_ID = '" + master_id.ToString() + "' AND B.ATTEND_BY_TEAM IS not null and work_ongoing=0").Tables(0)
                If DTINTIMATE3.Rows.Count <> 0 Then
                    hidenable.Value = "0"
                Else
                    'If DTINTIMATE2.Rows.Count > 0 Then
                    '    hidenable.Value = "0"
                    'Else
                    hidenable.Value = "1"
                    'End If
                End If
            Else
                If rowcount = 1 Then
                    hidenable.Value = "0"
                Else
                    hidenable.Value = "1"
                End If
            End If
            dtintiattnd = DB.ExecuteDataSet(" SELECT * FROM bo_intimate_dtls " +
                                            " WHERE INTIMATE_by_TEAM =4 AND BO_MASTER_ID = '" + master_id.ToString() + "' AND section_id=8").Tables(0)
            If dtintiattnd.Rows.Count = 0 Then

                hidintimate.Value = "0"
            Else
                hidintimate.Value = "1"
            End If
            dtattnd = DB.ExecuteDataSet(" SELECT * FROM bo_intimate_dtls " +
                                        " WHERE INTIMATE_to_TEAM =4 AND BO_MASTER_ID = '" + master_id.ToString() + "' AND attend_by_team is null").Tables(0)
            If dtattnd.Rows.Count = 0 Then
                hidattend.Value = "1"
            Else
                hidattend.Value = "0"
            End If
            If rowcount = 1 Then
                hidattend.Value = "0"
            End If
            If GF.CheckdeptUser(CInt(Session("UserID")), 4) = False Then
                hidenable.Value = "0"
            End If
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "windowload();", True)
            'GN.ComboFill(cmbReligion, DT, 0, 1)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Try
                Dim savedata As String = ""
                If Data(1).ToString() <> "undefined" Then
                    savedata = Data(1).ToString()
                    If savedata <> "" Then
                        savedata = savedata.Substring(1)
                    End If
                End If
                Dim master_id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@Data", SqlDbType.VarChar)
                Params(0).Value = savedata
                Params(1) = New SqlParameter("@BO_MASTER_ID_VALUE", SqlDbType.Int)
                Params(1).Value = master_id
                Params(2) = New SqlParameter("@UserID", SqlDbType.VarChar)
                Params(2).Value = Session("UserID")
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@Attend", SqlDbType.Int)
                Params(5).Value = CInt(Data(2))
                Params(6) = New SqlParameter("@Intimate", SqlDbType.Int)
                Params(6).Value = CInt(Data(3))
                DB.ExecuteNonQuery("SP_BO_HR_UPDATION", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn += Message + "|" + ErrorFlag.ToString()
        End If
        If CInt(Data(0)) = 2 Then
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Try
                Dim savedata As String = ""
                If Data(1).ToString() <> "undefined" Then
                    savedata = Data(1).ToString()
                    If savedata <> "" Then
                        savedata = savedata.Substring(1)
                    End If
                End If
                Dim master_id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@Data", SqlDbType.VarChar)
                Params(0).Value = savedata
                Params(1) = New SqlParameter("@BO_MASTER_ID_VALUE", SqlDbType.Int)
                Params(1).Value = master_id
                Params(2) = New SqlParameter("@UserID", SqlDbType.VarChar)
                Params(2).Value = Session("UserID")
                Params(3) = New SqlParameter("@Mode", SqlDbType.VarChar)
                Params(3).Value = 2
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@Attend", SqlDbType.Int)
                Params(6).Value = CInt(Data(2))
                Params(7) = New SqlParameter("@Intimate", SqlDbType.Int)
                Params(7).Value = CInt(Data(3))
                DB.ExecuteNonQuery("SP_BO_HR_UPDATION", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn += Message + "|" + ErrorFlag.ToString()
        End If
        If CInt(Data(0)) = 3 Then
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Try
                Dim savedata As String = ""
                If Data(1).ToString() <> "undefined" Then
                    savedata = Data(1).ToString()
                    If savedata <> "" Then
                        savedata = savedata.Substring(1)
                    End If
                End If
                Dim master_id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@Data", SqlDbType.VarChar)
                Params(0).Value = savedata
                Params(1) = New SqlParameter("@BO_MASTER_ID_VALUE", SqlDbType.Int)
                Params(1).Value = master_id
                Params(2) = New SqlParameter("@UserID", SqlDbType.VarChar)
                Params(2).Value = Session("UserID")
                Params(3) = New SqlParameter("@Mode", SqlDbType.VarChar)
                Params(3).Value = 1
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@Attend", SqlDbType.Int)
                Params(6).Value = CInt(Data(2))
                Params(7) = New SqlParameter("@Intimate", SqlDbType.Int)
                Params(7).Value = CInt(Data(3))
                DB.ExecuteNonQuery("SP_BO_HR_UPDATION", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn += Message + "|" + ErrorFlag.ToString()
        End If
    End Sub
    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click

    'End Sub
End Class
