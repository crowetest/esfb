﻿'Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BO_BusinessProcedures
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTDATA1 As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DTPREMISE As New DataTable
    Dim DTINTIMATE0 As New DataTable
    Dim DTINTIMATE1 As New DataTable
    Dim DTINTIMATE2 As New DataTable
    Dim DTINTIMATE2_1 As New DataTable
    Dim DTINTIMATE3 As New DataTable
    Dim DTINTIMATE3_1 As New DataTable
    Dim DTINTIMATE4 As New DataTable
    Dim DTINTIMATE4_1 As New DataTable
    Dim DTINTIMATE9 As New DataTable
    Dim DTINTIMATE9_1 As New DataTable
    Dim DTA1 As New DataTable
    Dim DTA2 As New DataTable
    Dim DTA4 As New DataTable
    Dim DTA6 As New DataTable
    Dim DTA7 As New DataTable
    Dim DTA8 As New DataTable
    Dim DTI1 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Dim AttachImage As Integer = 0
    Dim BO_Master_Id As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1269) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            BO_Master_Id = CInt(Request.QueryString.Get("Bo_Master_Id"))
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Business Procedures"
            Dim UserID As Integer = CInt(Session("UserID"))
            Me.hidUser.Value = CStr(UserID)
            Me.hid_BO_Master_Id.Value = BO_Master_Id
            Me.Remove1.Attributes.Add("onclick", "return RemoveAttach1()")

            DTINTIMATE0 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE BO_MASTER_ID = " + BO_Master_Id.ToString()).Tables(0)
            If DTINTIMATE0.Rows.Count = 0 Then
                'Since there is no data for section 1 in intimate_dtls, adding one intimate entry to business section1 with workongoing status = 1
                Dim ErrorFlag As Integer = 0
                Dim Message As String = Nothing
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@BO_Master_id", SqlDbType.Int)
                Params(0).Value = BO_Master_Id
                Params(1) = New SqlParameter("@Intimate_By_User", SqlDbType.Int)
                Params(1).Value = CInt(UserID)
                Params(2) = New SqlParameter("@Attend_By_User", SqlDbType.Int)
                Params(2).Value = CInt(UserID)
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@IntEntry", SqlDbType.Int)
                Params(5).Value = 1
                Params(6) = New SqlParameter("@SectionID", SqlDbType.Int)
                Params(6).Value = 1
                DB.ExecuteNonQuery("SP_BO_BUSINESS_PROCEDURES", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
                hidenable1.Value = "1"
                hidSection.Value = "1"
            Else
                DTINTIMATE1 = DB.ExecuteDataSet(" SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 0 AND INTIMATE_TO_TEAM = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND WORK_ONGOING = 1").Tables(0)
                If DTINTIMATE1.Rows.Count = 0 Then
                    hidSection.Value = "1"
                    hidenable1.Value = "0"
                Else
                    hidSection.Value = "1"
                    hidenable1.Value = "1"
                End If

                DTINTIMATE2 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 1 AND INTIMATE_TO_TEAM = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString()).Tables(0)
                If DTINTIMATE2.Rows.Count <> 0 Then
                    DTINTIMATE2_1 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 1 AND INTIMATE_TO_TEAM = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND ATTEND_BY_TEAM IS NOT NULL AND WORK_ONGOING = 1").Tables(0)
                    If DTINTIMATE2_1.Rows.Count = 0 Then
                        hidSection.Value = "2"
                        hidenable2.Value = "0"
                    Else
                        hidenable2.Value = "1"
                        hidSection.Value = "2"
                    End If
                End If

                DTINTIMATE3 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 2 AND INTIMATE_TO_TEAM = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString()).Tables(0)
                If DTINTIMATE3.Rows.Count <> 0 Then
                    DTINTIMATE3_1 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 2 AND INTIMATE_TO_TEAM = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND ATTEND_BY_TEAM IS NOT NULL AND WORK_ONGOING = 1").Tables(0)
                    If DTINTIMATE3_1.Rows.Count = 0 Then
                        hidSection.Value = "3"
                        hidenable3.Value = "0"
                    Else
                        hidenable3.Value = "0"
                        hidSection.Value = "3"
                    End If
                End If

                DTINTIMATE4 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 3 AND INTIMATE_TO_TEAM = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString()).Tables(0)
                If DTINTIMATE4.Rows.Count <> 0 Then
                    DTINTIMATE4_1 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 3 AND INTIMATE_TO_TEAM = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND ATTEND_BY_TEAM IS NOT NULL AND WORK_ONGOING = 1").Tables(0)
                    If DTINTIMATE4_1.Rows.Count = 0 Then
                        hidSection.Value = "4"
                        hidenable4.Value = "0"
                    Else
                        hidenable4.Value = "1"
                        hidSection.Value = "4"
                    End If
                End If

                DTINTIMATE9 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 4 AND INTIMATE_TO_TEAM = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString()).Tables(0)
                If DTINTIMATE9.Rows.Count <> 0 Then
                    DTINTIMATE9_1 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 4 AND INTIMATE_TO_TEAM = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND ATTEND_BY_TEAM IS NOT NULL AND WORK_ONGOING = 1").Tables(0)
                    If DTINTIMATE9_1.Rows.Count = 0 Then
                        hidenable9.Value = "1"
                        hidSection.Value = "9"
                    Else
                        hidSection.Value = "9"
                        hidenable9.Value = "0"
                    End If
                End If

                'DTINTIMATE9 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 5 AND INTIMATE_TO_TEAM = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString()).Tables(0)
                'If DTINTIMATE9.Rows.Count <> 0 Then
                '    DTINTIMATE9_1 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 5 AND INTIMATE_TO_TEAM = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + " AND ATTEND_BY_TEAM IS NOT NULL AND WORK_ONGOING = 1").Tables(0)
                '    If DTINTIMATE9_1.Rows.Count = 0 Then
                '        hidSection.Value = "9"
                '        hidenable9.Value = "0"
                '    Else
                '        hidenable9.Value = "1"
                '        hidSection.Value = "9"
                '    End If
                'End If
            End If

            DT = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1").Tables(0)
            If DT.Rows.Count <> 0 Then
                DTA2 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1 AND ATTEND_BY_TEAM IS NULL").Tables(0)
                If DTA2.Rows.Count <> 0 Then
                    hidAttend2.Value = "0"
                Else
                    hidAttend2.Value = "1"
                End If
            End If
            DT = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 3 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1").Tables(0)
            If DT.Rows.Count <> 0 Then
                DTA4 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 3 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1 AND ATTEND_BY_TEAM IS NULL").Tables(0)
                If DTA4.Rows.Count <> 0 Then
                    hidAttend4.Value = "0"
                Else
                    hidAttend4.Value = "1"
                End If
            End If
            DT = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 6 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1").Tables(0)
            If DT.Rows.Count <> 0 Then
                DTA6 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 6 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1 AND ATTEND_BY_TEAM IS NULL").Tables(0)
                If DTA6.Rows.Count <> 0 Then
                    hidAttend6.Value = "0"
                Else
                    hidAttend6.Value = "1"
                End If
            End If
            DT = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 7 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1").Tables(0)
            If DT.Rows.Count <> 0 Then
                DTA7 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 7 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1 AND ATTEND_BY_TEAM IS NULL").Tables(0)
                If DTA7.Rows.Count <> 0 Then
                    hidAttend7.Value = "0"
                Else
                    hidAttend7.Value = "1"
                End If
            End If
            DT = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 8 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1").Tables(0)
            If DT.Rows.Count <> 0 Then
                DTA8 = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 8 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1 AND ATTEND_BY_TEAM IS NULL").Tables(0)
                If DTA8.Rows.Count <> 0 Then
                    hidAttend8.Value = "0"
                Else
                    hidAttend8.Value = "1"
                End If
            End If
            'DT = DB.ExecuteDataSet("SELECT * FROM BO_INTIMATE_DTLS WHERE SECTION_ID = 1 AND BO_MASTER_ID = " + BO_Master_Id.ToString() + "AND INTIMATE_TO_TEAM = 1").Tables(0)
            'If DT.Rows.Count <> 0 Then
            '    hidIntimate1.Value = "1"
            'Else
            '    hidIntimate1.Value = "0"
            'End If

            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 3 and Status_id = 1 order by 1")
            GF.ComboFill(cmbPremiseType, DT, 0, 1)
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 4 and Status_id = 1 order by 1")
            GF.ComboFill(cmbBussClass, DT, 0, 1)
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 6 and Status_id = 1 order by 1")
            GF.ComboFill(cmbPopCategory, DT, 0, 1)
            DT = GF.GetQueryResult("select -1 Cluster_Id, '-----Select-----' Cluster_name union all select Cluster_Id,Cluster_name from cluster_master where Status_id = 1 order by 1")
            GF.ComboFill(cmbCluster, DT, 0, 1)
            DT = DB.ExecuteDataSet("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 7 and Status_id = 1 order by 1").Tables(0)
            For Each bld In DT.Rows
                Me.hidBuildingStatus.Value += bld(0).ToString() + "ÿ" + bld(1).ToString() + "Ñ"
            Next
            DT = DB.ExecuteDataSet("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 8 and Status_id = 1 order by 1").Tables(0)
            For Each bld In DT.Rows
                Me.hidBldngLeadStatus.Value += bld(0).ToString() + "ÿ" + bld(1).ToString() + "Ñ"
            Next
            'Me.hidBuildingStatus.Value = DT.ToString()
            DTDATA1 = DB.ExecuteDataSet("select * from BO_MASTER where BO_Master_Id = " + BO_Master_Id.ToString()).Tables(0)
            If DTDATA1.Rows(0)(5).ToString() <> "" Then
                Me.txtLocApproveBoard.Text = CDate(DTDATA1.Rows(0)(5).ToString())
            End If
            If DTDATA1.Rows(0)(6).ToString() <> "" Then
                Me.txtLocRBI.Text = CDate(DTDATA1.Rows(0)(6).ToString())
            End If
            If DTDATA1.Rows(0)(7).ToString() <> "" Then
                Me.txtRecievedDate.Text = CDate(DTDATA1.Rows(0)(7).ToString())
            End If
            If DTDATA1.Rows(0)(8).ToString() <> "" Then
                Me.txtF6Date.Text = CDate(DTDATA1.Rows(0)(8).ToString())
            End If
            If DTDATA1.Rows(0)(10).ToString() <> "" Then
                Me.txtFinalRBIDate.Text = CDate(DTDATA1.Rows(0)(10).ToString())
            End If
            If DTDATA1.Rows(0)(11).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(11)) <> -1 Then
                    DT1 = DB.ExecuteDataSet("select combo_text from bo_combo_data where identify_id = 1 and combo_id = " + DTDATA1.Rows(0)(11).ToString()).Tables(0)
                    Me.txtCommMode.Text = DT1.Rows(0)(0).ToString()
                End If
            End If
            If DTDATA1.Rows(0)(12).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(12)) <> -1 Then
                    DT1 = DB.ExecuteDataSet("select combo_text from bo_combo_data where identify_id = 2 and combo_id = " + DTDATA1.Rows(0)(12).ToString()).Tables(0)
                    Me.txtType.Text = DT1.Rows(0)(0).ToString()
                End If
            End If
            If DTDATA1.Rows(0)(13).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(13)) <> -1 Then
                    Me.cmbPremiseType.SelectedValue = CInt(DTDATA1.Rows(0)(13))
                End If
            End If
            If DTDATA1.Rows(0)(14).ToString() <> "" Then
                Me.txtPropLaunchDate.Text = CDate(DTDATA1.Rows(0)(14).ToString())
            End If
            If DTDATA1.Rows(0)(15).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(15)) <> -1 Then
                    Me.cmbBussClass.SelectedValue = CInt(DTDATA1.Rows(0)(15))
                End If
            End If
            If DTDATA1.Rows(0)(16).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(16)) <> -1 Then
                    DT1 = DB.ExecuteDataSet("select combo_text from bo_combo_data where identify_id = 5 and combo_id = " + DTDATA1.Rows(0)(16).ToString()).Tables(0)
                    Me.txtTier.Text = DT1.Rows(0)(0).ToString()
                End If
            End If
            If DTDATA1.Rows(0)(2).ToString() <> "" Then
                If DTDATA1.Rows(0)(2).ToString() = "Y" Then
                    Me.txtNE.Text = "Yes"
                ElseIf DTDATA1.Rows(0)(2).ToString() = "N" Then
                    Me.txtNE.Text = "No"
                End If
            End If
            If DTDATA1.Rows(0)(3).ToString() <> "" Then
                If DTDATA1.Rows(0)(3).ToString() = "Y" Then
                    Me.txtLWE.Text = "Yes"
                ElseIf DTDATA1.Rows(0)(3).ToString() = "N" Then
                    Me.txtLWE.Text = "No"
                End If
            End If
            If DTDATA1.Rows(0)(17).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(17)) <> -1 Then
                    Me.cmbPopCategory.SelectedValue = CInt(DTDATA1.Rows(0)(17))
                End If
            End If
            If DTDATA1.Rows(0)(18).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(18)) <> -1 Then
                    DT1 = DB.ExecuteDataSet("select state_name from state_master where state_id = " + DTDATA1.Rows(0)(18).ToString()).Tables(0)
                    Me.txtState.Text = DT1.Rows(0)(0).ToString()
                End If
            End If
            If DTDATA1.Rows(0)(19).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(19)) <> -1 Then
                    DT1 = DB.ExecuteDataSet("select district_name from district_master where district_id = " + DTDATA1.Rows(0)(19).ToString()).Tables(0)
                    Me.txtDistrict.Text = DT1.Rows(0)(0).ToString()
                End If
            End If
            If DTDATA1.Rows(0)(20).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(20)) <> -1 Then
                    Me.cmbCluster.SelectedValue = CInt(DTDATA1.Rows(0)(20))
                End If
            End If
            If DTDATA1.Rows(0)(21).ToString() <> "" Then
                Me.txtBranchName.Text = DTDATA1.Rows(0)(21).ToString()
            End If
            If DTDATA1.Rows(0)(22).ToString() <> "" Then
                Me.txtBranchID.Text = DTDATA1.Rows(0)(22).ToString()
            End If
            If DTDATA1.Rows(0)(23).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(23)) <> -1 Then
                    Me.cmbPremIdentify.SelectedValue = CInt(DTDATA1.Rows(0)(23))
                End If
            End If
            If DTDATA1.Rows(0)(24).ToString() <> "" Then
                Me.txtBrSealDate.Text = CDate(DTDATA1.Rows(0)(24).ToString())
            End If
            If DTDATA1.Rows(0)(25).ToString() <> "" Then
                Me.txtStationeryDate.Text = CDate(DTDATA1.Rows(0)(25).ToString())
            End If
            If DTDATA1.Rows(0)(26).ToString() <> "" Then
                Me.txtMICRCode.Text = DTDATA1.Rows(0)(26).ToString()
            End If
            If DTDATA1.Rows(0)(27).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(27)) <> -1 Then
                    Me.cmbMICRCode.SelectedValue = CInt(DTDATA1.Rows(0)(27))
                End If
            End If
            If DTDATA1.Rows(0)(28).ToString() <> "" Then
                Me.txtBSRCode.Text = DTDATA1.Rows(0)(28).ToString()
            End If
            If DTDATA1.Rows(0)(29).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(29)) <> -1 Then
                    Me.cmbBSRCode.SelectedValue = CInt(DTDATA1.Rows(0)(29))
                End If
            End If
            If DTDATA1.Rows(0)(30).ToString() <> "" Then
                If CInt(DTDATA1.Rows(0)(30)) <> -1 Then
                    Me.cmbUserIDMapping.SelectedValue = CInt(DTDATA1.Rows(0)(30))
                End If
            End If
            If DTDATA1.Rows(0)(31).ToString() <> "" Then
                Me.txtTabDate.Text = CDate(DTDATA1.Rows(0)(31).ToString())
            End If
            If DTDATA1.Rows(0)(32).ToString() <> "" Then
                Me.txtPRNum.Text = DTDATA1.Rows(0)(32).ToString()
            End If
            If DTDATA1.Rows(0)(33).ToString() <> "" Then
                Me.txtSoftDate.Text = CDate(DTDATA1.Rows(0)(33).ToString())
            End If
            If DTDATA1.Rows(0)(34).ToString() <> "" Then
                Me.txtInaugurationDate.Text = CDate(DTDATA1.Rows(0)(34).ToString())
            End If

            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,Caption,Description from dms_ESFB.dbo.BO_BUSINESS_UPLOAD where BO_Master_Id = " + BO_Master_Id.ToString() + " And BO_Section_Id = 1 And BO_Premise_Id = 0").Tables(0)
            If DT.Rows.Count <> 0 Then
                Me.hid_Sec1_Attachment.Value = DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString()
                UploadedAttachment1.Visible = True
                UploadedAttachment1.Attributes.Add("href", DT.Rows(0)(1).ToString())
                UploadedAttachment1.InnerText = DT.Rows(0)(2).ToString()
                Remove1.Visible = True
                fup1.Visible = False
                Upload.Visible = False
            Else
                fup1.Visible = True
                Upload.Visible = True
                UploadedAttachment1.Visible = False
                Remove1.Visible = False
            End If
            'fup1.Visible = False
            'Upload.Visible = True
            AttachImage = 1
            Me.hidPremData.Value = ""
            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,Caption,BO_Premise_Id from dms_ESFB.dbo.BO_BUSINESS_UPLOAD where BO_Master_Id = " + BO_Master_Id.ToString() + " And BO_Section_Id = 2").Tables(0)
            If DT.Rows.Count <> 0 Then
                For i As Integer = 0 To DT.Rows.Count - 1
                    Me.hid_Sec2_Attachment.Value = DT.Rows(i)(1).ToString() + "µ" + DT.Rows(i)(2).ToString()
                    Me.hidPremData.Value += DT.Rows(i)(1).ToString() + "µ" + DT.Rows(i)(2).ToString() + "µ" + DT.Rows(i)(3).ToString() + "Ø"
                    UploadedAttachment2.Visible = True
                    'UploadedAttachment1.Attributes.Add("href", DT.Rows(0)(1).ToString())
                    'UploadedAttachment2.InnerText = DT.Rows(0)(2).ToString()
                    Remove2.Visible = False
                    'UploadedAttachment2.Visible = False
                    fup2.Visible = True
                    Upload.Visible = True
                Next
            Else
                fup2.Visible = True
                Upload.Visible = True
                UploadedAttachment2.Visible = True
                Remove2.Visible = False
            End If

                DTPREMISE = DB.ExecuteDataSet("SELECT * FROM BO_PREMISE_DTL WHERE BO_MASTER_ID = " + BO_Master_Id.ToString()).Tables(0)
                Dim PremData As String = ""
                'For Each dtprow In DTPREMISE.Rows
                For n As Integer = 0 To DTPREMISE.Rows.Count - 1
                    For i As Integer = 0 To DTPREMISE.Columns.Count - 1
                        PremData += DTPREMISE(n)(i).ToString() + "µ"
                    Next
                    PremData += "¥"
                Next
                Me.hidPremiseDtl.Value = PremData

                If GF.CheckdeptUser(CInt(Session("UserID")), 1) = False Then
                    hidenable1.Value = "0"
                    hidenable2.Value = "0"
                    hidenable3.Value = "0"
                    hidenable4.Value = "0"
                    hidenable9.Value = "0"
                    hidSection.Value = "9"
                End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim sectionID As Integer
        If Data(1).ToString() <> "" Then
            sectionID = CInt(Data(1))
        End If
        If CInt(Data(0)) = 1 Then
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            'Dim BO_Master_Id As Integer = 10
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim Mode As Integer

            If sectionID = 1 Then
                
                Dim locApproveBoardDate As Nullable(Of Date)
                If Data(2).ToString = "" Then
                    locApproveBoardDate = Nothing
                Else
                    locApproveBoardDate = CDate(Data(2).ToString())
                End If
                Dim locRBIDate As Nullable(Of Date)
                If Data(3).ToString = "" Then
                    locRBIDate = Nothing
                Else
                    locRBIDate = CDate(Data(3).ToString())
                End If
                Dim recievedDate As Nullable(Of Date)
                If Data(4).ToString = "" Then
                    recievedDate = Nothing
                Else
                    recievedDate = CDate(Data(4).ToString())
                End If
                Dim f6Date As Nullable(Of Date)
                If Data(5).ToString = "" Then
                    f6Date = Nothing
                Else
                    f6Date = CDate(Data(5).ToString())
                End If
                Dim finalRBIDate As Nullable(Of Date)
                If Data(6).ToString = "" Then
                    finalRBIDate = Nothing
                Else
                    finalRBIDate = CDate(Data(6).ToString())
                End If
                Dim premiseType As Integer
                If Data(7).ToString() <> "" Then
                    If (CInt(Data(7).ToString()) = -1) Then
                        premiseType = Nothing
                    Else
                        premiseType = CInt(Data(7).ToString())
                    End If
                End If
                Dim propLaunchDate As Nullable(Of Date)
                If Data(8).ToString = "" Then
                    propLaunchDate = Nothing
                Else
                    propLaunchDate = CDate(Data(8).ToString())
                End If
                Dim bussClass As Integer
                If Data(9).ToString() <> "" Then
                    If (CInt(Data(9).ToString()) = -1) Then
                        bussClass = Nothing
                    Else
                        bussClass = CInt(Data(9).ToString())
                    End If
                End If
                Dim popCategory As Integer
                If Data(10).ToString() <> "" Then
                    If (CInt(Data(10).ToString()) = -1) Then
                        popCategory = Nothing
                    Else
                        popCategory = CInt(Data(10).ToString())
                    End If
                End If
                Dim cluster As Integer
                If Data(11).ToString() <> "" Then
                    If (CInt(Data(11).ToString()) = -1) Then
                        cluster = Nothing
                    Else
                        cluster = CInt(Data(11).ToString())
                    End If
                End If
                Dim branchID As Integer
                If Data(12).ToString() <> "" Then
                    branchID = CInt(Data(12).ToString())
                Else
                    branchID = Nothing
                End If
                Dim branchName As String = Data(13).ToString()
                Dim premIdentify As Integer
                If Data(14).ToString() <> "" Then
                    premIdentify = CInt(Data(14).ToString())
                End If
                'Dim sectionID As Integer
                'If Data(1).ToString() <> "" Then
                '    sectionID = CInt(Data(1).ToString())
                'End If
                If Data(15).ToString() <> "" Then
                    Mode = CInt(Data(15).ToString())
                End If
                Dim Intimate As Integer
                If Data(16).ToString() <> "" Then
                    Intimate = CInt(Data(16).ToString())
                End If
                'Dim attend As Integer
                'If Data(17).ToString() <> "" Then
                '    attend = CInt(Data(17).ToString())
                'End If
                Try
                    Dim Params(19) As SqlParameter
                    Params(0) = New SqlParameter("@BO_Master_Id", SqlDbType.Int)
                    Params(0).Value = BO_Master_Id
                    Params(1) = New SqlParameter("@Busi_Entry_User_Id", SqlDbType.Int)
                    Params(1).Value = UserID
                    Params(2) = New SqlParameter("@Loc_App_Board_Dt", SqlDbType.Date)
                    Params(2).Value = locApproveBoardDate
                    Params(3) = New SqlParameter("@Loc_App_RBI_Dt", SqlDbType.Date)
                    Params(3).Value = locRBIDate
                    Params(4) = New SqlParameter("@Rece_Dt", SqlDbType.Date)
                    Params(4).Value = recievedDate
                    Params(5) = New SqlParameter("@Form6_Send_RBI_Dt", SqlDbType.Date)
                    Params(5).Value = f6Date
                    Params(6) = New SqlParameter("@Final_App_RBI_Dt", SqlDbType.Date)
                    Params(6).Value = finalRBIDate
                    Params(7) = New SqlParameter("@Premise_type_Id", SqlDbType.Int)
                    Params(7).Value = premiseType
                    Params(8) = New SqlParameter("@Prop_Launch_Dt", SqlDbType.Date)
                    Params(8).Value = propLaunchDate
                    Params(9) = New SqlParameter("@Busi_Classi_Id", SqlDbType.Int)
                    Params(9).Value = bussClass
                    Params(10) = New SqlParameter("@Pop_Cate_Id", SqlDbType.Int)
                    Params(10).Value = popCategory
                    Params(11) = New SqlParameter("@Cluster_Id", SqlDbType.Int)
                    Params(11).Value = cluster
                    Params(12) = New SqlParameter("@Branch_Id", SqlDbType.Int)
                    Params(12).Value = branchID
                    Params(13) = New SqlParameter("@Branch_Name", SqlDbType.VarChar, 100)
                    Params(13).Value = branchName
                    Params(14) = New SqlParameter("@Premise_Identified", SqlDbType.Int)
                    Params(14).Value = premIdentify
                    Params(15) = New SqlParameter("@SectionID", SqlDbType.Int)
                    Params(15).Value = sectionID
                    Params(16) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(16).Direction = ParameterDirection.Output
                    Params(17) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(17).Direction = ParameterDirection.Output
                    Params(18) = New SqlParameter("@Intimate", SqlDbType.Int)
                    Params(18).Value = Intimate
                    Params(19) = New SqlParameter("@Mode", SqlDbType.Int)
                    Params(19).Value = Mode
                    'Params(20) = New SqlParameter("@Attend", SqlDbType.Int)
                    'Params(20).Value = attend
                    DB.ExecuteNonQuery("SP_BO_BUSINESS_PROCEDURES", Params)
                    ErrorFlag = CInt(Params(16).Value)
                    Message = CStr(Params(17).Value)
                    CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString() + "Ø" + Mode.ToString()
                Catch ex As Exception
                    Response.Redirect("~/CatchException.aspx?ErrorNo=1")
                End Try
            End If
            If sectionID = 9 Then
                Dim BrSealDate As Nullable(Of Date)
                If Data(2).ToString = "" Then
                    BrSealDate = Nothing
                Else
                    BrSealDate = CDate(Data(2).ToString())
                End If
                Dim StationeryDate As Nullable(Of Date)
                If Data(3).ToString = "" Then
                    StationeryDate = Nothing
                Else
                    StationeryDate = CDate(Data(3).ToString())
                End If
                Dim MICRCode As String = Data(4).ToString()
                Dim cmbMICRCode As Integer
                If Data(5).ToString() <> "" Then
                    cmbMICRCode = CInt(Data(5).ToString())
                End If
                Dim BSRCode As String = Data(6).ToString()
                Dim cmbBSRCode As Integer
                If Data(7).ToString() <> "" Then
                    cmbBSRCode = CInt(Data(7).ToString())
                End If
                Dim UserIDMapping As Integer
                If Data(8).ToString() <> "" Then
                    UserIDMapping = CInt(Data(8).ToString())
                End If
                Dim TabDate As Nullable(Of Date)
                If Data(9).ToString = "" Then
                    TabDate = Nothing
                Else
                    TabDate = CDate(Data(9).ToString())
                End If
                Dim PRNum As String = Data(10).ToString()
                Dim SoftDate As Nullable(Of Date)
                If Data(11).ToString = "" Then
                    SoftDate = Nothing
                Else
                    SoftDate = CDate(Data(11).ToString())
                End If
                Dim InaugurationDate As Nullable(Of Date)
                If Data(12).ToString = "" Then
                    InaugurationDate = Nothing
                Else
                    InaugurationDate = CDate(Data(12).ToString())
                End If
                If Data(13).ToString() <> "" Then
                    Mode = CInt(Data(13).ToString())
                End If
                Dim attend As Integer
                If Data(14).ToString() <> "" Then
                    attend = CInt(Data(14).ToString())
                End If
                Try
                    Dim Params(15) As SqlParameter
                    Params(0) = New SqlParameter("@BO_Master_Id", SqlDbType.Int)
                    Params(0).Value = BO_Master_Id
                    Params(1) = New SqlParameter("@Branch_Seal_Deli_Dt", SqlDbType.Date)
                    Params(1).Value = BrSealDate
                    Params(2) = New SqlParameter("@Stationary_Deli_Dt", SqlDbType.Date)
                    Params(2).Value = StationeryDate
                    Params(3) = New SqlParameter("@MICR_Code", SqlDbType.VarChar, 50)
                    Params(3).Value = MICRCode
                    Params(4) = New SqlParameter("@MICR_Update_Profile", SqlDbType.Int)
                    Params(4).Value = cmbMICRCode
                    Params(5) = New SqlParameter("@BSR_Code", SqlDbType.VarChar, 50)
                    Params(5).Value = BSRCode
                    Params(6) = New SqlParameter("@BSR_Update_Profile", SqlDbType.Int)
                    Params(6).Value = cmbBSRCode
                    Params(7) = New SqlParameter("@User_Id_Mapping", SqlDbType.Int)
                    Params(7).Value = UserIDMapping
                    Params(8) = New SqlParameter("@TAB_Req_Dt", SqlDbType.Date)
                    Params(8).Value = TabDate
                    Params(9) = New SqlParameter("@PR_No", SqlDbType.VarChar, 50)
                    Params(9).Value = PRNum
                    Params(10) = New SqlParameter("@Soft_Launch_Dt", SqlDbType.Date)
                    Params(10).Value = SoftDate
                    Params(11) = New SqlParameter("@Inaugration_Dt", SqlDbType.Date)
                    Params(11).Value = InaugurationDate
                    Params(12) = New SqlParameter("@Busi_Entry_User_Id", SqlDbType.Int)
                    Params(12).Value = UserID
                    Params(13) = New SqlParameter("@SectionID", SqlDbType.Int)
                    Params(13).Value = sectionID
                    Params(14) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(14).Direction = ParameterDirection.Output
                    Params(15) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(15).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_BO_BUSINESS_PROCEDURES", Params)
                    ErrorFlag = CInt(Params(14).Value)
                    Message = CStr(Params(15).Value)
                    CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString() + "Ø" + Mode.ToString()
                Catch ex As Exception
                    Response.Redirect("~/CatchException.aspx?ErrorNo=1")
                End Try
            End If

            If sectionID = 2 Then
                Dim colNum As Integer = CInt(Data(2).ToString())
                If Data(3).ToString() <> "" Then
                    Mode = CInt(Data(3).ToString())
                End If

                Dim locApproveBoardDate As Nullable(Of Date)
                If Data(5).ToString = "" Then
                    locApproveBoardDate = Nothing
                Else
                    locApproveBoardDate = CDate(Data(5).ToString())
                End If
                Dim locRBIDate As Nullable(Of Date)
                If Data(6).ToString = "" Then
                    locRBIDate = Nothing
                Else
                    locRBIDate = CDate(Data(6).ToString())
                End If
                Dim recievedDate As Nullable(Of Date)
                If Data(7).ToString = "" Then
                    recievedDate = Nothing
                Else
                    recievedDate = CDate(Data(7).ToString())
                End If
                Dim f6Date As Nullable(Of Date)
                If Data(8).ToString = "" Then
                    f6Date = Nothing
                Else
                    f6Date = CDate(Data(8).ToString())
                End If
                Dim finalRBIDate As Nullable(Of Date)
                If Data(9).ToString = "" Then
                    finalRBIDate = Nothing
                Else
                    finalRBIDate = CDate(Data(9).ToString())
                End If
                Dim premiseType As Integer
                If Data(10).ToString() <> "" Then
                    If (CInt(Data(10).ToString()) = -1) Then
                        premiseType = Nothing
                    Else
                        premiseType = CInt(Data(10).ToString())
                    End If
                End If
                Dim propLaunchDate As Nullable(Of Date)
                If Data(11).ToString = "" Then
                    propLaunchDate = Nothing
                Else
                    propLaunchDate = CDate(Data(11).ToString())
                End If
                Dim bussClass As Integer
                If Data(12).ToString() <> "" Then
                    If (CInt(Data(12).ToString()) = -1) Then
                        bussClass = Nothing
                    Else
                        bussClass = CInt(Data(12).ToString())
                    End If
                End If
                Dim popCategory As Integer
                If Data(13).ToString() <> "" Then
                    If (CInt(Data(13).ToString()) = -1) Then
                        popCategory = Nothing
                    Else
                        popCategory = CInt(Data(13).ToString())
                    End If
                End If
                Dim cluster As Integer
                If Data(14).ToString() <> "" Then
                    If (CInt(Data(14).ToString()) = -1) Then
                        cluster = Nothing
                    Else
                        cluster = CInt(Data(14).ToString())
                    End If
                End If
                Dim branchID As Integer
                If Data(15).ToString() <> "" Then
                    branchID = CInt(Data(15).ToString())
                Else
                    branchID = Nothing
                End If
                Dim branchName As String = Data(16).ToString()
                Dim premIdentify As Integer
                If Data(17).ToString() <> "" Then
                    premIdentify = CInt(Data(17).ToString())
                End If

                Dim Intimate As Integer
                If Data(18).ToString() <> "" Then
                    Intimate = CInt(Data(18).ToString())
                End If
                'Dim attend As Integer
                'If Data(6).ToString() <> "" Then
                '    attend = CInt(Data(6).ToString())
                'End If
                Try
                    Dim cols() As String = Data(4).Split(CChar("¥"))
                    For n As Integer = 0 To colNum - 1
                        Dim colData() As String = cols(n).Split(CChar("µ"))
                        Dim Sl_Id As Integer
                        If colData(0).ToString() <> "" Then
                            Sl_Id = CInt(colData(0).ToString())
                        End If
                        Dim Appr_Area_Sqft As Double
                        If colData(1).ToString() <> "" Then
                            Appr_Area_Sqft = CDbl(colData(1).ToString())
                        End If
                        Dim Buliding_Status_Id As Integer
                        If colData(2).ToString() <> "" Then
                            If colData(2).ToString() <> "null" Then
                                If (CInt(colData(2).ToString()) = -1) Then
                                    Buliding_Status_Id = Nothing
                                Else
                                    Buliding_Status_Id = CInt(colData(2).ToString())
                                End If
                            End If
                        End If
                        Dim Present_USB_Floor As String = colData(3).ToString()
                        Dim Present_USB_Area As String = colData(4).ToString()
                        Dim Floor_No As Integer
                        If colData(5).ToString() <> "" Then
                            Floor_No = CInt(colData(5).ToString())
                        End If
                        Dim Is_Building_No_Avail As Integer
                        If colData(6).ToString() <> "" Then
                            If colData(6).ToString() <> "null" Then
                                If (CInt(colData(6).ToString()) = -1) Then
                                    Is_Building_No_Avail = Nothing
                                Else
                                    Is_Building_No_Avail = CInt(colData(6).ToString())
                                End If
                            End If
                        End If
                        Dim Buliding_Owner_Name As String = colData(7).ToString()
                        Dim Contact_No As String = colData(8).ToString()
                        Dim Rent_Prop As Double
                        If colData(9).ToString() <> "" Then
                            Rent_Prop = CDbl(colData(9).ToString())
                        End If
                        Dim Is_ATM_Avail As Integer
                        If colData(10).ToString() <> "" Then
                            If colData(10).ToString() <> "null" Then
                                If (CInt(colData(10).ToString()) = -1) Then
                                    Is_ATM_Avail = Nothing
                                Else
                                    Is_ATM_Avail = CInt(colData(10).ToString())
                                End If
                            End If
                        End If
                        Dim Is_Strong_Room_Req As Integer
                        If colData(11).ToString() <> "" Then
                            If colData(11).ToString() <> "null" Then
                                If (CInt(colData(11).ToString()) = -1) Then
                                    Is_Strong_Room_Req = Nothing
                                Else
                                    Is_Strong_Room_Req = CInt(colData(11).ToString())
                                End If
                            End If
                        End If
                        Dim Level1_Comments As String = colData(12).ToString()
                        Dim Is_Safe_Room_Req As Integer
                        If colData(13).ToString() <> "" Then
                            If colData(13).ToString() <> "null" Then
                                If (CInt(colData(13).ToString()) = -1) Then
                                    Is_Safe_Room_Req = Nothing
                                Else
                                    Is_Safe_Room_Req = CInt(colData(13).ToString())
                                End If
                            End If
                        End If
                        Dim Level1_User_Id As Integer = UserID



                        Dim Params(33) As SqlParameter
                        Params(0) = New SqlParameter("@BO_Master_Id", SqlDbType.Int)
                        Params(0).Value = BO_Master_Id
                        Params(1) = New SqlParameter("@SectionID", SqlDbType.Int)
                        Params(1).Value = sectionID
                        Params(2) = New SqlParameter("@Sl_Id", SqlDbType.Int)
                        Params(2).Value = Sl_Id
                        Params(3) = New SqlParameter("@Appr_Area_Sqft", SqlDbType.Float)
                        Params(3).Value = Appr_Area_Sqft
                        Params(4) = New SqlParameter("@Buliding_Status_Id", SqlDbType.Int)
                        Params(4).Value = Buliding_Status_Id
                        Params(5) = New SqlParameter("@Present_USB_Floor", SqlDbType.VarChar, 50)
                        Params(5).Value = Present_USB_Floor
                        Params(6) = New SqlParameter("@Present_USB_Area", SqlDbType.VarChar, 50)
                        Params(6).Value = Present_USB_Area
                        Params(7) = New SqlParameter("@Floor_No", SqlDbType.Int)
                        Params(7).Value = Floor_No
                        Params(8) = New SqlParameter("@Is_Building_No_Avail", SqlDbType.Int)
                        Params(8).Value = Is_Building_No_Avail
                        Params(9) = New SqlParameter("@Buliding_Owner_Name", SqlDbType.VarChar, 50)
                        Params(9).Value = Buliding_Owner_Name
                        Params(10) = New SqlParameter("@Contact_No", SqlDbType.VarChar, 50)
                        Params(10).Value = Contact_No
                        Params(11) = New SqlParameter("@Rent_Prop", SqlDbType.Float)
                        Params(11).Value = Rent_Prop
                        Params(12) = New SqlParameter("@Is_ATM_Avail", SqlDbType.Int)
                        Params(12).Value = Is_ATM_Avail
                        Params(13) = New SqlParameter("@Is_Strong_Room_Req", SqlDbType.Int)
                        Params(13).Value = Is_Strong_Room_Req
                        Params(14) = New SqlParameter("@Level1_Comments", SqlDbType.VarChar, 2000)
                        Params(14).Value = Level1_Comments
                        Params(15) = New SqlParameter("@Level1_User_Id", SqlDbType.Int)
                        Params(15).Value = Level1_User_Id
                        Params(16) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(16).Direction = ParameterDirection.Output
                        Params(17) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(17).Direction = ParameterDirection.Output
                        'Params(19) = New SqlParameter("@Attend", SqlDbType.Int)
                        'Params(19).Value = attend
                        Params(18) = New SqlParameter("@Is_Safe_Room_Req", SqlDbType.Int)
                        Params(18).Value = Is_Safe_Room_Req
                        Params(19) = New SqlParameter("@Mode", SqlDbType.Int)
                        Params(19).Value = Mode
                        Params(20) = New SqlParameter("@Intimate", SqlDbType.Int)
                        Params(20).Value = 0


                        'if not sec1 finished, then finish with sec2
                        Params(21) = New SqlParameter("@Loc_App_Board_Dt", SqlDbType.Date)
                        Params(21).Value = locApproveBoardDate
                        Params(22) = New SqlParameter("@Loc_App_RBI_Dt", SqlDbType.Date)
                        Params(22).Value = locRBIDate
                        Params(23) = New SqlParameter("@Rece_Dt", SqlDbType.Date)
                        Params(23).Value = recievedDate
                        Params(24) = New SqlParameter("@Form6_Send_RBI_Dt", SqlDbType.Date)
                        Params(24).Value = f6Date
                        Params(25) = New SqlParameter("@Final_App_RBI_Dt", SqlDbType.Date)
                        Params(25).Value = finalRBIDate
                        Params(26) = New SqlParameter("@Premise_type_Id", SqlDbType.Int)
                        Params(26).Value = premiseType
                        Params(27) = New SqlParameter("@Prop_Launch_Dt", SqlDbType.Date)
                        Params(27).Value = propLaunchDate
                        Params(28) = New SqlParameter("@Busi_Classi_Id", SqlDbType.Int)
                        Params(28).Value = bussClass
                        Params(29) = New SqlParameter("@Pop_Cate_Id", SqlDbType.Int)
                        Params(29).Value = popCategory
                        Params(30) = New SqlParameter("@Cluster_Id", SqlDbType.Int)
                        Params(30).Value = cluster
                        Params(31) = New SqlParameter("@Branch_Id", SqlDbType.Int)
                        Params(31).Value = branchID
                        Params(32) = New SqlParameter("@Branch_Name", SqlDbType.VarChar, 100)
                        Params(32).Value = branchName
                        Params(33) = New SqlParameter("@Premise_Identified", SqlDbType.Int)
                        Params(33).Value = premIdentify

                        DB.ExecuteNonQuery("SP_BO_BUSINESS_PROCEDURES", Params)
                        ErrorFlag = CInt(Params(16).Value)
                        Message = CStr(Params(17).Value)
                    Next
                    CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString() + "Ø" + Mode.ToString()
                Catch ex As Exception
                    Response.Redirect("~/CatchException.aspx?ErrorNo=1")
                End Try
            End If
            If sectionID = 4 Then
                Dim colNum As Integer = CInt(Data(2).ToString())
                If Data(3).ToString() <> "" Then
                    Mode = CInt(Data(3).ToString())
                End If
                Dim Intimate As Integer
                If Mode = 1 Then
                    Intimate = 0
                Else
                    Intimate = 1
                End If
                Dim cols() As String = Data(4).Split(CChar("¥"))
                For n As Integer = 0 To colNum - 1
                    Dim colData() As String = cols(n).Split(CChar("µ"))
                    Dim Sl_Id As Integer
                    If colData(0).ToString() <> "" Then
                        Sl_Id = CInt(colData(0).ToString())
                    End If
                    Dim Buliding_Lead_Status As Integer
                    If colData(1).ToString() <> "" Then
                        If (CInt(colData(1).ToString()) = -1) Then
                            Buliding_Lead_Status = Nothing
                        Else
                            Buliding_Lead_Status = CInt(colData(1).ToString())
                        End If
                    End If

                    Dim Rej_Reason As String = colData(2).ToString()
                    Dim Level4_User_Id As Integer = UserID
                    Dim Params(9) As SqlParameter
                    Params(0) = New SqlParameter("@BO_Master_Id", SqlDbType.Int)
                    Params(0).Value = BO_Master_Id
                    Params(1) = New SqlParameter("@SectionID", SqlDbType.Int)
                    Params(1).Value = sectionID
                    Params(2) = New SqlParameter("@Sl_Id", SqlDbType.Int)
                    Params(2).Value = Sl_Id
                    Params(3) = New SqlParameter("@Building_Lead_Status_Id", SqlDbType.Int)
                    Params(3).Value = Buliding_Lead_Status
                    Params(4) = New SqlParameter("@Rej_Reason", SqlDbType.VarChar, 1000)
                    Params(4).Value = Rej_Reason
                    Params(5) = New SqlParameter("@Level4_User_Id", SqlDbType.Int)
                    Params(5).Value = UserID
                    Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(6).Direction = ParameterDirection.Output
                    Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(7).Direction = ParameterDirection.Output
                    Params(8) = New SqlParameter("@Intimate", SqlDbType.Int)
                    Params(8).Value = Intimate
                    Params(9) = New SqlParameter("@Mode", SqlDbType.Int)
                    Params(9).Value = Mode
                    DB.ExecuteNonQuery("SP_BO_BUSINESS_PROCEDURES", Params)
                    ErrorFlag = CInt(Params(6).Value)
                    Message = CStr(Params(7).Value)
                Next
                CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString() + "Ø" + Mode.ToString()
            End If
        End If
        If CInt(Data(0)) = 4 Then
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0

            If Data(1).ToString() <> "" Then
                BO_Master_Id = CInt(Data(1).ToString())
            End If
            Dim BO_Section_Id As Integer
            If Data(2).ToString() <> "" Then
                BO_Section_Id = CInt(Data(2).ToString())
            End If
            Dim BO_Premise_Id As Integer
            If Data(3).ToString() <> "" Then
                BO_Premise_Id = CInt(Data(3).ToString())
            End If
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@BO_Master_Id", SqlDbType.Int)
                Params(0).Value = BO_Master_Id
                Params(1) = New SqlParameter("@BO_Section_Id", SqlDbType.Int)
                Params(1).Value = BO_Section_Id
                Params(2) = New SqlParameter("@BO_Premise_Id", SqlDbType.Int)
                Params(2).Value = BO_Premise_Id
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@Remove", SqlDbType.Int)
                Params(5).Value = 1
                DB.ExecuteNonQuery("SP_BO_BUSINESS_ATTACH", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
                fup1.Visible = True
                Upload.Visible = True
                UploadedAttachment1.Visible = False
                Remove1.Visible = False
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_script1.Append("         alert('" + Message + "');")
                'cl_script1.Append("         window.open('BO_BusinessProcedures.aspx','_self');")
                cl_script1.Append("         window.open('BO_BusinessProcedures.aspx?Bo_Master_Id='" + BO_Master_Id.ToString(), +"'_self');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            Catch ex As Exception
                Response.Redirect("~/CatchException.aspx?ErrorNo=1")
            End Try
        End If
    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Upload.Click
        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
        Dim myFile As HttpPostedFile
        Dim FileLen As Integer = 0
        Dim FileName As String = ""
        If fup1.Visible = True Then
            myFile = fup1.PostedFile
            FileLen = myFile.ContentLength
            If (FileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(FileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, FileLen)
            End If
        End If
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        'Dim BO_Master_Id As Integer = 11
        Try
            Dim Params(10) As SqlParameter
            Params(0) = New SqlParameter("@AttachImage", SqlDbType.Int)
            Params(0).Value = AttachImage
            Params(1) = New SqlParameter("@CaptionName", SqlDbType.VarChar, 100)
            Params(1).Value = "Form 6"
            Params(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
            Params(2).Value = AttachImg
            Params(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
            Params(3).Value = ContentType
            Params(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
            Params(4).Value = FileName
            Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(5).Value = CInt(Session("UserID"))
            Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(6).Direction = ParameterDirection.Output
            Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(7).Direction = ParameterDirection.Output
            Params(8) = New SqlParameter("@BO_Master_Id", SqlDbType.Int)
            Params(8).Value = BO_Master_Id
            Params(9) = New SqlParameter("@BO_Section_Id", SqlDbType.Int)
            Params(9).Value = 1
            Params(10) = New SqlParameter("@BO_Premise_Id", SqlDbType.Int)
            Params(10).Value = 0
            DB.ExecuteNonQuery("SP_BO_BUSINESS_ATTACH", Params)
            Message = CStr(Params(7).Value)
            ErrorFlag = CInt(Params(6).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "');")
        'cl_script1.Append("         window.open('BO_BusinessProcedures.aspx','_self');")
        cl_script1.Append("         window.open('BO_BusinessProcedures.aspx?Bo_Master_Id='" + BO_Master_Id.ToString() + "',_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    End Sub
    Protected Sub btnUpload2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Upload2.Click
        Dim ContentType As String = ""
        Dim CaptionName As String = Me.txtCaptionName.Text
        Dim Premise_Id As Integer = CInt(Me.hidPremAttach.Value)
        Dim AttachImg As Byte() = Nothing
        Dim myFile As HttpPostedFile
        Dim FileLen As Integer = 0
        Dim FileName As String = ""
        If fup2.Visible = True Then
            myFile = fup2.PostedFile
            FileLen = myFile.ContentLength
            If (FileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(FileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, FileLen)
            End If
        End If
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Try
            Dim Params(10) As SqlParameter
            Params(0) = New SqlParameter("@AttachImage", SqlDbType.Int)
            Params(0).Value = AttachImage
            Params(1) = New SqlParameter("@CaptionName", SqlDbType.VarChar, 100)
            Params(1).Value = CaptionName
            Params(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
            Params(2).Value = AttachImg
            Params(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
            Params(3).Value = ContentType
            Params(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
            Params(4).Value = FileName
            Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(5).Value = CInt(Session("UserID"))
            Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(6).Direction = ParameterDirection.Output
            Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(7).Direction = ParameterDirection.Output
            Params(8) = New SqlParameter("@BO_Master_Id", SqlDbType.Int)
            Params(8).Value = BO_Master_Id
            Params(9) = New SqlParameter("@BO_Section_Id", SqlDbType.Int)
            Params(9).Value = 2
            Params(10) = New SqlParameter("@BO_Premise_Id", SqlDbType.Int)
            Params(10).Value = Premise_Id
            DB.ExecuteNonQuery("SP_BO_BUSINESS_ATTACH", Params)
            Message = CStr(Params(7).Value)
            ErrorFlag = CInt(Params(6).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "');")
        cl_script1.Append("         window.open('BO_BusinessProcedures.aspx?Bo_Master_Id='" + BO_Master_Id.ToString() + ",'_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Me.txtCaptionName.Text = ""
    End Sub

End Class
