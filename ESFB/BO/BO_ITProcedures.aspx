﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="BO_ITProcedures.aspx.vb" Inherits="BO_ITProcedures" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script src="../Script/jquery-1.2.6.min.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;<a href="../Help/">../Help/</a>
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);         
        }        
        .Button:hover
        {            
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            color:#801424;
        }                  
     .bg
     {
         background-color:#FFF;
     }          
     .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
        .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        } 
        
    </style>    
    <script language="javascript" type="text/javascript">
        var col_id;
        var DisFlag;
        function windowload() 
        {

             if (document.getElementById("<%= hidenable.ClientID %>").value == "0")
             {
                DisFlag = 'disabled';
                disableTest();
                if (document.getElementById("<%= hidattend .ClientID %>").value == "0")
                {
                    document.getElementById("<%= chkattend.ClientID %>").checked = false;
                    document.getElementById("<%= chkattend.ClientID %>").disabled = false;
                }
                else{
                document.getElementById("<%= chkattend.ClientID %>").checked = true;
                document.getElementById("<%= chkattend.ClientID %>").disabled = true;
                }
                if (document.getElementById("<%= hidintimate.ClientID %>").value == "0")
                document.getElementById("<%= chkintimate.ClientID %>").checked = false;
                else
                document.getElementById("<%= chkintimate.ClientID %>").checked = true;
             }
             else
             {
                DisFlag = '';
                if (document.getElementById("<%= hidattend .ClientID %>").value == "0")
                {
                    document.getElementById("<%= chkattend.ClientID %>").checked = false;
                    document.getElementById("<%= chkattend.ClientID %>").disabled = false;
                }
                else{
                document.getElementById("<%= chkattend.ClientID %>").checked = true;
                document.getElementById("<%= chkattend.ClientID %>").disabled = true;
                }
                if (document.getElementById("<%= hidintimate.ClientID %>").value == "0")
                document.getElementById("<%= chkintimate.ClientID %>").checked = false;
                else
                document.getElementById("<%= chkintimate.ClientID %>").checked = true;
             }
             table_fill();
             table_fill_p2p();
        }
         function disableTest(){
            $(".dis").attr("disabled",true);
            document.getElementById("section1").disabled = true;
            var nodes = document.getElementById("section1").getElementsByTagName('*');
            for(var i = 0; i < nodes.length; i++){
                nodes[i].disabled = true;
            }
//           $(".dis").attr("disabled",true);
           document.getElementById("btnExit").disabled = false;  
         }
//        window.onload = function () {
//            table_fill();
//            table_fill_p2p();
//        }
        function AddNewPremise(){
            col_id += 1;
            TableFill();
        }
         function table_fill() 
        {
         if (document.getElementById("<%= hidData.ClientID %>").value != "") {
            document.getElementById("<%= pnlData.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>"; 
            tab += "<td style='width:25%;text-align:left'>Asset</td>"; 
            tab += "<td style='width:10%;text-align:left'>Required</td>";   
            tab += "<td style='width:10%;text-align:left'>Available</td>";
            tab += "<td style='width:10%;text-align:left'>Balance</td>";
            tab += "<td style='width:10%;text-align:left'>Done?</td>";
            tab += "<td style='width:30%;text-align:left'>Remarks</td>";    
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
           
                row = document.getElementById("<%= hidData.ClientID %>").value.split("¥");
                var i = 1;
                for (n = 1; n <= row.length-1; n++) {
                    col = row[n].split("^");                    
                    if (row_bg == 0) 
                    {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else 
                    {
                         row_bg = 0;
                         tab += "<tr class=sub_second>";
                    }
                                                     
                    tab += "<td style='width:5%;text-align:center' >" + i  + "</td>";
                    tab += "<td style='width:25%;text-align:left' class='NormalText'>" + col[1] + "</td>";
                    if( col[7] == "1" )
                    {
                        if ( col[2] != "")
                        {
                            var txtQualiBox = "<input id='txtrequired" + n + "' name='txtrequired" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' value= '" +col[2]+ "' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                           
                            tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                        else
                        {
                           var txtQualiBox = "<input id='txtrequired" + n + "' name='txtrequired" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                           
                           tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                    }
                    else
                    {
                        if ( col[2] != "")
                        {
                            var txtQualiBox = "<input id='txtrequired" + n + "' disabled='true' name='txtrequired" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' value= '" +col[2]+ "' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                           
                            tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                        else
                        {
                           var txtQualiBox = "<input id='txtrequired" + n + "' disabled='true' name='txtrequired" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                           
                           tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                    }
                    if( col[7] == "1" )
                    {
                        if ( col[3] != "")
                        {
                            var txtQualiBox = "<input id='txtavail" + n + "' name='txtavail" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' value= '" +col[3]+ "' onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                        else
                        {
                            var txtQualiBox = "<input id='txtavail" + n + "' name='txtavail" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                    }
                    else
                    {
                        if ( col[3] != "")
                        {
                            var txtQualiBox = "<input id='txtavail" + n + "' name='txtavail" + n + "' disabled='true' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' value= '" +col[3]+ "' onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                        else
                        {
                            var txtQualiBox = "<input id='txtavail" + n + "' name='txtavail" + n + "' disabled='true' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                    }
                    var bal = col[2] - col[3];
                    if( col[7] == "1" )
                    {
                        if ( col[4] != "")
                        {
                            var txtQualiBox = "<input id='txtbal" + n + "' name='txtbal" + n + "' type='Text' style='width:99%;' disabled='true' class='NormalText' maxlength='50' value= '" +bal+ "' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                        else
                        {
                            var txtQualiBox = "<input id='txtbal" + n + "' name='txtbal" + n + "' type='Text' style='width:99%;' disabled='true' class='NormalText' maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                    }
                    else
                    {
                        if ( col[4] != "")
                        {
                            var txtQualiBox = "<input id='txtbal" + n + "' name='txtbal" + n + "' disabled='true' type='Text' style='width:99%;' class='NormalText' maxlength='50' value= '" +bal+ "' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                        else
                        {
                            var txtQualiBox = "<input id='txtbal" + n + "' name='txtbal" + n + "' disabled='true' type='Text' style='width:99%;' class='NormalText' maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                        }
                    }
                    if( col[7] == "1" )
                    {
                        if ( col[6] != "")
                        {
                            var select1 = "<select id='txtcharge" + n + "' class='NormalText' disabled='true' name='txtcharge" + n + "' style='width:100%' onchange='updateValue("+ n +")' >";
                            select1 += "<option value='0' selected=true>--NA--</option>";
//                            if ((col[6] == 1))
//                            select1 += "<option value='1' selected=true>Yes</option>";
//                            else
//                            select1 += "<option value='1'>Yes</option>";
//                            if (col[6] == 2)
//                            select1 += "<option value='2' selected=true>No</option>";
//                            else
//                            select1 += "<option value='2'>No</option>";
//                            tab += "<td style='width:25%;text-align:left'>" + select1 + "</td>";

//                            var txtQualiBox = "<input id='txtcharge" + n + "' name='txtcharge" + n + "' disabled='true'  type='Text' style='width:99%;' class='NormalText' maxlength='500'  value= '" +col[6]+ "' onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + select1 + "</td>";
                        }
                        else
                        {
                            var select1 = "<select id='txtcharge" + n + "' class='NormalText' disabled='true' name='txtcharge" + n + "' style='width:100%' onchange='updateValue("+ n +")' >";
                            select1 += "<option value='0' selected=true>--NA--</option>";
//                            if ((col[6] == 1))
//                            select1 += "<option value='1' selected=true>Yes</option>";
//                            else
//                            select1 += "<option value='1'>Yes</option>";
//                            if (col[6] == 2)
//                            select1 += "<option value='2' selected=true>No</option>";
//                            else
//                            select1 += "<option value='2'>No</option>";
//                            var txtQualiBox = "<input id='txtcharge" + n + "' name='txtcharge" + n + "' disabled='true' type='Text' style='width:99%;' class='NormalText' maxlength='500'  onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + select1 + "</td>";
                        }
                    }
                    else
                    {
                        if ( col[6] != "")
                        {
                            var select1 = "<select id='txtcharge" + n + "' class='NormalText' "+ DisFlag + " name='txtcharge" + n + "' style='width:100%' onchange='updateValue("+ n +")' >";
                            select1 += "<option value='-1' selected=true>--Select--</option>";
                            if ((col[6] == 0))
                            select1 += "<option value='0' selected=true>No</option>";
                            else
                            select1 += "<option value='0'>No</option>";
                            if (col[6] == 1)
                            select1 += "<option value='1' selected=true>Yes</option>";
                            else
                            select1 += "<option value='1'>Yes</option>";
//                            var txtQualiBox = "<input id='txtcharge" + n + "' name='txtcharge" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500'  value= '" +col[6]+ "' onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + select1 + "</td>";
                        }
                        else
                        {
                            var select1 = "<select id='txtcharge" + n + "' class='NormalText' "+ DisFlag + " name='txtcharge" + n + "' style='width:100%' onchange='updateValue("+ n +")' >";
                            select1 += "<option value='-1' selected=true>--Select--</option>";
                            if ((col[6] == 0))
                            select1 += "<option value='0' selected=true>No</option>";
                            else
                            select1 += "<option value='0'>No</option>";
                            if (col[6] == 1)
                            select1 += "<option value='1' selected=true>Yes</option>";
                            else
                            select1 += "<option value='1'>yes</option>";
//                            var txtQualiBox = "<input id='txtcharge" + n + "' name='txtcharge" + n + "' type='Text' style='width:99%;' class='NormalText' maxlength='500'  onchange='updateValue("+ n +")'/>";                                            
                            tab += "<td style='width:10%;text-align:left'>" + select1 + "</td>";
                        }
                    }
                    if ( col[5] != "")
                    {
                    var txtQualiBox = "<input id='txtremark" + n + "' name='txtremark" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='500'  value= '" +col[5]+ "' onchange='updateValue("+ n +")'/>";                                            
                    tab += "<td style='width:30%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    else
                    {
                        var txtQualiBox = "<input id='txtremark" + n + "' name='txtremark" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='500'  onchange='updateValue("+ n +")'/>";                                            
                        tab += "<td style='width:30%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    tab += "</tr>";
                    i++;
                }
            
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnlData.ClientID %>").innerHTML = tab;
//            setTimeout(function() {
//                document.getElementById("cmbQualification"+(n-1)).focus().select();
//                }, 4);
            }
            else
            document.getElementById("<%= pnlData.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }
        function table_fill_p2p() 
        {
         if (document.getElementById("<%= hidDatap2p.ClientID %>").value != "") {
            document.getElementById("<%= pnlDatap2p.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center'>#</td>"; 
            tab += "<td style='width:10%;text-align:left'>P2P No</td>"; 
            tab += "<td style='width:20%;text-align:left'>P2P Request Date</td>";   
            tab += "<td style='width:10%;text-align:left'>PO No</td>";
            tab += "<td style='width:20%;text-align:left'>PO Date</td>";
            tab += "<td style='width:30%;text-align:left'>Remarks</td>";
            tab += "<td style='width:5%;text-align:left'></td>";     
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
           
                row = document.getElementById("<%= hidDatap2p.ClientID %>").value.split("¥");
                var i = 1;
                for (n = 1; n <= row.length-1; n++) {
                    col = row[n].split("^");                    
                    if (row_bg == 0) 
                    {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else 
                    {
                         row_bg = 0;
                         tab += "<tr class=sub_second>";
                    }
                                                     
                    tab += "<td style='width:5%;text-align:center' >" + i  + "</td>";
                    if( col[0] != "" )
                    {
                        var txtQualiBox = "<input id='txtp2pno" + n + "' name='txtp2pno" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' value= '" +col[0]+ "' onkeypress='return NumericCheck(event)' onchange='updateValuep2p("+ n +")'/>";                           
                        tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    else
                    {
                         var txtQualiBox = "<input id='txtp2pno" + n + "' name='txtp2pno" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValuep2p("+ n +")'/>";                           
                         tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    if ( col[1] != "")
                    {
                        var CreateDate = new Date(col[1]); 
                        dd = CreateDate.getDate(); if (dd<10) dd = "0"+dd;
                        mm = CreateDate.getMonth()+1; if (mm<10) mm = "0"+mm;
                        yyyy = CreateDate.getFullYear(); 
                        CreateDate = yyyy+'-'+mm+'-'+dd;
                        var txtQualiBox = "<input id='txtp2prequired" + n + "' name='txtp2prequired" + n + "' type='date' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' value= '" +CreateDate+ "' onkeypress='return NumericCheck(event)' onchange='updateValuep2p("+ n +")'/>";                           
                        tab += "<td style='width:20%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    else
                    {
                       var txtQualiBox = "<input id='txtp2prequired" + n + "' name='txtp2prequired" + n + "' type='date' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValuep2p("+ n +")'/>";                           
                       tab += "<td style='width:20%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    if ( col[2] != "")
                    {
                        var txtQualiBox = "<input id='txtp2pavail" + n + "' name='txtp2pavail" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' value= '" +col[2]+ "' onchange='updateValuep2p("+ n +")'/>";                                            
                        tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    else
                    {
                        var txtQualiBox = "<input id='txtp2pavail" + n + "' name='txtp2pavail" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValuep2p("+ n +")'/>";                                            
                        tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    if ( col[3] != "")
                    {
                        var CreateDate = new Date(col[3]); 
                        dd = CreateDate.getDate(); if (dd<10) dd = "0"+dd;
                        mm = CreateDate.getMonth()+1; if (mm<10) mm = "0"+mm;
                        yyyy = CreateDate.getFullYear(); 
                        CreateDate = yyyy+'-'+mm+'-'+dd;
                        var txtQualiBox = "<input id='txtp2pbal" + n + "' name='txtp2pbal" + n + "' type='date' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' value= '" +CreateDate+ "' onkeypress='return NumericCheck(event)' onchange='updateValuep2p("+ n +")'/>";                                            
                        tab += "<td style='width:20%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    else
                    {
                        var txtQualiBox = "<input id='txtp2pbal" + n + "' name='txtp2pbal" + n + "' type='date' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValuep2p("+ n +")'/>";                                            
                        tab += "<td style='width:20%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    if ( col[4] != "")
                    {
                    var txtQualiBox = "<input id='txtp2premark" + n + "' name='txtp2premark" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='500'  value= '" +col[4]+ "' onchange='updateValuep2p("+ n +")'/>";                                            
                    tab += "<td style='width:30%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    else
                    {
                        var txtQualiBox = "<input id='txtp2premark" + n + "' name='txtp2premark" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='500'  onchange='updateValuep2p("+ n +")'/>";                                            
                        tab += "<td style='width:30%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    if(col[0]=="" && col[1]=="" && col[2]=="" && col[3]=="")
                    tab +="<td style='width:5%;text-align:left'></td>";
                    else
                    tab += "<td style='width:5%;text-align:center' onclick=DeleteRowp2p('" + n + "')><img  src='../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                    tab += "</tr>";
                    i++;
                }
                
            
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnlDatap2p.ClientID %>").innerHTML = tab;
//            setTimeout(function() {
//                document.getElementById("cmbQualification"+(n-1)).focus().select();
//                }, 4);
            }
            else
            if (document.getElementById("<%= hidDatap2p.ClientID %>").value == "") 
            {
                document.getElementById("<%= pnlDatap2p.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr height=20px;  class='tblQal'>";      
                tab += "<td style='width:15%;text-align:center'>#</td>"; 
                tab += "<td style='width:25%;text-align:left'>P2P No</td>"; 
                tab += "<td style='width:10%;text-align:left'>P2P Request Date</td>";   
                tab += "<td style='width:10%;text-align:left'>PO No</td>";
                tab += "<td style='width:10%;text-align:left'>PO Date</td>";
                tab += "<td style='width:30%;text-align:left'>Remarks</td>";    
                tab += "</tr>";     
                tab += "</table></div>";
            }
            document.getElementById("<%= pnlDatap2p.ClientID %>").innerHTML = tab;
            document.getElementById("<%= pnlDatap2p.ClientID %>").style.display = '';
            //--------------------- Clearing Data ------------------------//
        }
         function DeleteRowp2p(id)
        {
            if (document.getElementById("<%= hidenable.ClientID %>").value == "0")
            {
                return false;
            }
            row = document.getElementById("<%= hidDatap2p.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id!=n)                  
                     NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hidDatap2p.ClientID %>").value=NewStr;
                if(document.getElementById("<%= hidDatap2p.ClientID %>").value=="")
                document.getElementById("<%= hidDatap2p.ClientID %>").value="¥^^^^^";
                table_fill_p2p();
        }
        function updateValue(id)
        {       
                row = document.getElementById("<%= hidData.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1; n++) 
                {
                   var Qualcol = row[n].split("^");
                   if(id==n)
                   {
                           var AssetId = Qualcol[0];
                           var text = Qualcol[1];
                           var required = document.getElementById("txtrequired"+id).value;
                           var available = document.getElementById("txtavail"+id).value;
                           var diff = 0;
                           if ( available > required )
                           {
                                alert("Available value should be less than required value");
                                document.getElementById("txtavail"+id).value = 0;
                                document.getElementById("txtbal"+id).value = required;
                           }
                           else
                           {
                                diff = required - available;
                                document.getElementById("txtbal"+id).value = diff;
                           }
                           var balance=document.getElementById("txtbal"+id).value;
                           var remark=document.getElementById("txtremark"+id).value;
                           var charge=document.getElementById("txtcharge"+id).value;
                           NewStr += "¥" + AssetId.toString() + "^" + text + "^" + required.toString()+"^"+available.toString()+"^"+balance.toString()+"^"+remark+"^"+ charge.toString();
                   }
                   else
                   {
                        NewStr += "¥"+row[n];
                   }
                }
                document.getElementById("<%= hidData.ClientID %>").value=NewStr;
        }
        function updateValuep2p(id)
        {       
                row = document.getElementById("<%= hidDatap2p.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1; n++) 
                {
                   var Qualcol = row[n].split("^");
                   if(id==n)
                   {
                           var no = document.getElementById("txtp2pno"+id).value;
                           var required = document.getElementById("txtp2prequired"+id).value;
                           var available=document.getElementById("txtp2pavail"+id).value;
                           var balance=document.getElementById("txtp2pbal"+id).value;
                           var remark=document.getElementById("txtp2premark"+id).value;
                           var p2pid= Qualcol[5];
                           NewStr += "¥" + no.toString()+"^"+required.toString()+"^"+available.toString()+"^"+balance+"^"+remark+"^"+p2pid.toString();
                   }
                   else
                   {
                        NewStr += "¥"+row[n];
                   }
                }
                document.getElementById("<%= hidDatap2p.ClientID %>").value=NewStr;
        }
        function btnSave_onclick()
        {
//            if (document.getElementById("<%= txtconnectivityDate.ClientID %>").value == "")
//            { 
//                alert("Enter Connectivity Link-  InformedDate");
//                document.getElementById("<%= txtconnectivityDate.ClientID %>").focus();
//                return false;
//            }
//             if (document.getElementById("<%= drpconnectPriProvider.ClientID %>").value == "-1")
//            { 
//                alert("Enter Connectivity Primary Provider");
//                document.getElementById("<%= drpconnectPriProvider.ClientID %>").focus();
//                return false;
//            }
//             if (document.getElementById("<%= drpconnectPriStatus.ClientID %>").value == "-1")
//            { 
//                alert("Enter Connectivity Primary Status");
//                document.getElementById("<%= drpconnectPriStatus.ClientID %>").focus();
//                return false;
//            }
//             if (document.getElementById("<%= drpconnectSecProvider.ClientID %>").value == "-1")
//            { 
//                alert("Enter Connectivity Secondary Provider ");
//                document.getElementById("<%= drpconnectSecProvider.ClientID %>").focus();
//                return false;
//            }
//             if (document.getElementById("<%= drpconnectSecStatus.ClientID %>").value == "-1")
//            { 
//                alert("Enter Connectivity Secondary Status");
//                document.getElementById("<%= drpconnectSecStatus.ClientID %>").focus();
//                return false;
//            }
//             if (document.getElementById("<%= txtpricommitdate.ClientID %>").value == "")
//            { 
//                alert("Enter Connectivity Primary -Delivery Committed date");
//                document.getElementById("<%= txtpricommitdate.ClientID %>").focus();
//                return false;
//            }
//             if (document.getElementById("<%= txtseccommitdate.ClientID %>").value == "")
//            { 
//                alert("Enter Connectivity Secondary -Delivery Committed date");
//                document.getElementById("<%= txtseccommitdate.ClientID %>").focus();
//                return false;
//            }
//             if (document.getElementById("<%= txtpropinscomdate.ClientID %>").value == "")
//            { 
//                alert("Enter Proposed Installation Committed date");
//                document.getElementById("<%= txtpropinscomdate.ClientID %>").focus();
//                return false;
//            }
//             if (document.getElementById("<%= txtnteinspropcomdate.ClientID %>").value == "")
//            { 
//                alert("Enter Network Rack installation - Proposed Committed date");
//                document.getElementById("<%= txtnteinspropcomdate.ClientID %>").focus();
//                return false;
//            }
//              if (document.getElementById("<%= drpnetrackinssta.ClientID %>").value == "-1")
//            { 
//                alert("Enter Network Rack installation status");
//                document.getElementById("<%= drpnetrackinssta.ClientID %>").focus();
//                return false;
//            }
//              if (document.getElementById("<%= drpatmsts.ClientID %>").value == "-1")
//            { 
//                alert("Enter ATM sattus");
//                document.getElementById("<%= drpatmsts.ClientID %>").focus();
//                return false;
//            }
//             if (document.getElementById("<%= txtfinalcomdate.ClientID %>").value == "")
//            { 
//                alert("Enter Final Completed Date");
//                document.getElementById("<%= txtfinalcomdate.ClientID %>").focus();
//                return false;
//            }


            row = document.getElementById("<%= hidData.ClientID %>").value.split("¥");
            var NewStrasset=""
            for (n = 1; n <= row.length-1; n++) 
            {   
                var Qualcol=row[n].split("^");
//                if(Qualcol[2]=="")
//                {
//                    alert("Enter required value in asset table");
//                    return false;
//                }
//                if(Qualcol[3]=="")
//                {
//                    alert("Enter available value in asset table");
//                    return false;
//                }
//                if(Qualcol[4]=="")
//                {
//                    alert("Enter balance value in asset table");
//                    return false;
//                }     
//                 if(Qualcol[6]=="")
//                {
//                    alert("Enter charge value in asset table");
//                    return false;
//                }
                 
                NewStrasset+="¥"+Qualcol[0]+"µ"+Qualcol[2]+"µ" +Qualcol[3]+"µ"+Qualcol[4]+"µ"+Qualcol[5]+"µ"+Qualcol[6];
            }

            row = document.getElementById("<%= hidDatap2p.ClientID %>").value.split("¥");
            var NewStrp2p=""
            for (n = 1; n <= row.length-1; n++) 
            {   
                var Qualcol=row[n].split("^");
//                if(Qualcol[0]=="")
//                {
//                    alert("Enter Number in P2P table");
//                    return false;
//                }
//                if(Qualcol[1]=="")
//                {
//                    alert("Enter required value in P2P table");
//                    return false;
//                }
//                if(Qualcol[2]=="")
//                {
//                    alert("Enter available value in P2P table");
//                    return false;
//                }
//                if(Qualcol[3]=="")
//                {
//                    alert("Enter balance value in P2P table");
//                    return false;
//                }     
                if ( Qualcol[0] != "" != "" && Qualcol[1] != "" && Qualcol[2] != "" && Qualcol[3] != "")          
                NewStrp2p+="¥"+Qualcol[0]+"µ"+Qualcol[1]+"µ" +Qualcol[2]+"µ"+Qualcol[3]+"µ"+Qualcol[4]+"µ"+Qualcol[5];
            }

            var value1 = document.getElementById("<%= txtconnectivityDate.ClientID %>").value;

            var value2 = document.getElementById("<%= drpconnectPriProvider.ClientID %>").value;
            var value3 = document.getElementById("<%= drpconnectPriStatus.ClientID %>").value;
            var value4 = document.getElementById("<%= drpconnectSecProvider.ClientID %>").value;
            var value5 = document.getElementById("<%= drpconnectSecStatus.ClientID %>").value;

            var value6 = document.getElementById("<%= txtpricommitdate.ClientID %>").value;
            var value7 = document.getElementById("<%= txtseccommitdate.ClientID %>").value;
            var value8 = document.getElementById("<%= txtpropinscomdate.ClientID %>").value;
            var value9 = document.getElementById("<%= txtnteinspropcomdate.ClientID %>").value;

            var value10 = document.getElementById("<%= drpnetrackinssta.ClientID %>").value;
            var value11 = document.getElementById("<%= drpatmsts.ClientID %>").value;

            var value12 = document.getElementById("<%= txtfinalcomdate.ClientID %>").value;
            if(document.getElementById("<%= chkattend.ClientID %>").checked==false)
            {
                alert( "Please check the Attend By check box. Otherwise you cant save the data" );
                return false;
            }
            var chkattend = 0;
            if(document.getElementById("<%= chkattend.ClientID %>").checked==true)
            {
                chkattend = 1;
            }
            var chkintimate = 0;
            if(document.getElementById("<%= chkintimate.ClientID  %>").checked==true)
            {
                chkintimate = 1;
            }

            var data = "1Ø" + value1 + "Ø" + value2 + "Ø" + value3 + "Ø" + value4 + "Ø" + value5 + "Ø" + value6 + "Ø" + value7 + "Ø" + value8 + "Ø" + value9 + "Ø" + value10 + "Ø" + value11 + "Ø" + value12 + "Ø" + NewStrasset + "Ø" + NewStrp2p + "Ø"+chkattend+"Ø"+chkintimate;
            ToServer(data ,1);
        }
        function btnFinish_onclick()
        {
            if (document.getElementById("<%= txtconnectivityDate.ClientID %>").value == "")
            { 
                alert("Enter Connectivity Link-  InformedDate");
                document.getElementById("<%= txtconnectivityDate.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= drpconnectPriProvider.ClientID %>").value == "-1")
            { 
                alert("Enter Connectivity Primary Provider");
                document.getElementById("<%= drpconnectPriProvider.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= drpconnectPriStatus.ClientID %>").value == "-1")
            { 
                alert("Enter Connectivity Primary Status");
                document.getElementById("<%= drpconnectPriStatus.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= drpconnectSecProvider.ClientID %>").value == "-1")
            { 
                alert("Enter Connectivity Secondary Provider ");
                document.getElementById("<%= drpconnectSecProvider.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= drpconnectSecStatus.ClientID %>").value == "-1")
            { 
                alert("Enter Connectivity Secondary Status");
                document.getElementById("<%= drpconnectSecStatus.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= txtpricommitdate.ClientID %>").value == "")
            { 
                alert("Enter Connectivity Primary -Delivery Committed date");
                document.getElementById("<%= txtpricommitdate.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= txtseccommitdate.ClientID %>").value == "")
            { 
                alert("Enter Connectivity Secondary -Delivery Committed date");
                document.getElementById("<%= txtseccommitdate.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= txtpropinscomdate.ClientID %>").value == "")
            { 
                alert("Enter Proposed Installation Committed date");
                document.getElementById("<%= txtpropinscomdate.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= txtnteinspropcomdate.ClientID %>").value == "")
            { 
                alert("Enter Network Rack installation - Proposed Committed date");
                document.getElementById("<%= txtnteinspropcomdate.ClientID %>").focus();
                return false;
            }
              if (document.getElementById("<%= drpnetrackinssta.ClientID %>").value == "-1")
            { 
                alert("Enter Network Rack installation status");
                document.getElementById("<%= drpnetrackinssta.ClientID %>").focus();
                return false;
            }
              if (document.getElementById("<%= drpatmsts.ClientID %>").value == "-1")
            { 
                alert("Enter ATM sattus");
                document.getElementById("<%= drpatmsts.ClientID %>").focus();
                return false;
            }
             if (document.getElementById("<%= txtfinalcomdate.ClientID %>").value == "")
            { 
                alert("Enter Final Completed Date");
                document.getElementById("<%= txtfinalcomdate.ClientID %>").focus();
                return false;
            }
            if(document.getElementById("<%= chkattend.ClientID %>").checked==false)
            {
                alert( "Please check the Attend By check box. Otherwise you cant save the data" );
                return false;
            }
            if(document.getElementById("<%= chkintimate.ClientID %>").checked==false)
            {
                alert( "Please check the Intimate check box. Otherwise you cant complete the process" );
                return false;
            }
            row = document.getElementById("<%= hidData.ClientID %>").value.split("¥");
            var NewStrasset=""
            for (n = 1; n <= row.length-1; n++) 
            {   
                var Qualcol=row[n].split("^");
                if(Qualcol[2]=="")
                {
                    alert("Enter required value in asset table");
                    return false;
                }
                if(Qualcol[3]=="")
                {
                    alert("Enter available value in asset table");
                    return false;
                }
                if(Qualcol[4]=="")
                {
                    alert("Enter balance value in asset table");
                    return false;
                }     
                 if(Qualcol[6]=="")
                {
                    alert("Enter charge value in asset table");
                    return false;
                }          
                NewStrasset+="¥"+Qualcol[0]+"µ"+Qualcol[2]+"µ" +Qualcol[3]+"µ"+Qualcol[4]+"µ"+Qualcol[5]+"µ"+Qualcol[6];
            }

            row = document.getElementById("<%= hidDatap2p.ClientID %>").value.split("¥");
            var NewStrp2p=""
            for (n = 1; n <= row.length-1; n++) 
            {   
                var Qualcol=row[n].split("^");
                if(Qualcol[0]=="")
                {
                    alert("Enter Number in P2P table");
                    return false;
                }
                if(Qualcol[1]=="")
                {
                    alert("Enter required value in P2P table");
                    return false;
                }
                if(Qualcol[2]=="")
                {
                    alert("Enter available value in P2P table");
                    return false;
                }
                if(Qualcol[3]=="")
                {
                    alert("Enter balance value in P2P table");
                    return false;
                }     
                      
                NewStrp2p+="¥"+Qualcol[0]+"µ"+Qualcol[1]+"µ" +Qualcol[2]+"µ"+Qualcol[3]+"µ"+Qualcol[4]+"µ"+Qualcol[5];
            }

            var value1 = document.getElementById("<%= txtconnectivityDate.ClientID %>").value;

            var value2 = document.getElementById("<%= drpconnectPriProvider.ClientID %>").value;
            var value3 = document.getElementById("<%= drpconnectPriStatus.ClientID %>").value;
            var value4 = document.getElementById("<%= drpconnectSecProvider.ClientID %>").value;
            var value5 = document.getElementById("<%= drpconnectSecStatus.ClientID %>").value;

            var value6 = document.getElementById("<%= txtpricommitdate.ClientID %>").value;
            var value7 = document.getElementById("<%= txtseccommitdate.ClientID %>").value;
            var value8 = document.getElementById("<%= txtpropinscomdate.ClientID %>").value;
            var value9 = document.getElementById("<%= txtnteinspropcomdate.ClientID %>").value;

            var value10 = document.getElementById("<%= drpnetrackinssta.ClientID %>").value;
            var value11 = document.getElementById("<%= drpatmsts.ClientID %>").value;

            var value12 = document.getElementById("<%= txtfinalcomdate.ClientID %>").value;

            var chkattend = 0;
            if(document.getElementById("<%= chkattend.ClientID %>").checked==true)
            {
                chkattend = 1;
            }
            var chkintimate = 0;
            if(document.getElementById("<%= chkintimate.ClientID  %>").checked==true)
            {
                chkintimate = 1;
            }

            var data = "2Ø" + value1 + "Ø" + value2 + "Ø" + value3 + "Ø" + value4 + "Ø" + value5 + "Ø" + value6 + "Ø" + value7 + "Ø" + value8 + "Ø" + value9 + "Ø" + value10 + "Ø" + value11 + "Ø" + value12 + "Ø" + NewStrasset + "Ø" + NewStrp2p + "Ø"+chkattend+"Ø"+chkintimate;
            ToServer(data ,2);
        }
        function FromServer(arg, context) {           
           if(context == 1){
                var Data = arg.split("|");
                if(Data[1]==0)
                {
                    alert(Data[0]);
                    var master = document.getElementById("<%= hidmaster.ClientID %>").value;
            window.open("TeamView.aspx?Bo_Master_Id=" + master.toString() + "", "_self");
                }
                else
                {
                    alert(Data[0]);    
                }
            }
             if(context == 2){
                var Data = arg.split("|");
                if(Data[1]==0)
                {
                    alert(Data[0]);
                    var master = document.getElementById("<%= hidmaster.ClientID %>").value;
                    window.open("TeamView.aspx?Bo_Master_Id=" + master.toString() + "", "_self");
                }
                else
                {
                    alert(Data[0]);    
                }
            }
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            var master = document.getElementById("<%= hidmaster.ClientID %>").value;
            window.open("TeamView.aspx?Bo_Master_Id=" + master.toString() + "", "_self");
//            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
        function AddNewRow() {                        
            if (document.getElementById("<%= hidDatap2p.ClientID %>").value != "") {                      
                    row = document.getElementById("<%= hidDatap2p.ClientID %>").value.split("¥");
                    var Len = row.length - 1;          
                    col = row[Len].split("^");                      
                    if ( col[0] != "" != "" && col[1] != "" && col[2] != "" && col[3] != "") 
                    {
                        document.getElementById("<%= hidDatap2p.ClientID %>").value += "¥^^^^^";                         
                    }
                    else
                    {
                        if ( col[0] == "" )
                        alert("Please enter p2p no");
                        else if ( col[1] == "" )
                        alert("Please enter p2p request date");
                        else if ( col[2] == "" )
                        alert("Please enter po number");
                        else if ( col[3] == "" )
                        alert("Please enter po date");
                    }
                }
                else
                document.getElementById("<%= hidDatap2p.ClientID %>").value = "¥^^^^^";  
                table_fill_p2p();
            }
    </script>
</head>
</html>
 <br />
 <div  id = "section1" style="width:90%;margin:0px auto; background-color:#EEB8A6;">
    <div style="width:80%;padding-left:10px;margin:0px auto;">
        <asp:HiddenField ID="hidData" runat="server" />
        <asp:HiddenField ID="hidDatap2p" runat="server" />
        <asp:HiddenField ID = "hidUser" runat="server" />
        <asp:HiddenField ID = "hidenable" runat="server" />
        <asp:HiddenField ID = "hidintimate" runat="server" />
        <asp:HiddenField ID = "hidattend" runat="server" />
        <asp:HiddenField ID = "hidmaster" runat="server" />
        <br /> 
    </div>
    <div id = "divSection1" style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">            
            <tr id="List">
                <td style="text-align: center;" colspan="4">
                    <asp:Panel ID="pnlSection1" Style="width: 100%;  text-align: right; float: right;" runat="server">
                        <div class="mainhead" style='width:100%; height:auto; padding-top:0px;margin:0px auto;background-color:white;'>
                            <table style='width:100%;font-family:'cambria';' align='center'>
                             <tr>
                                <td style='width:100%;text-align:center'>
                               <asp:CheckBox ID="chkattend" runat="server"/>&nbsp;<asp:label ID="lblsupport" class="NormalText" runat="server" 
                                Width="7%" Text="Attend By"/> &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkintimate" runat="server"/>&nbsp;<asp:label ID="Label1" class="NormalText" runat="server" 
                                Width="7%" Text="Intimate"/>
                                </td>
                                </tr>
                                </table><br/>
                                <table style='width:100%;font-family:'cambria';' align='center'>
                                <tr>
                                    <td style='width:25%;text-align:left;'></td>
                                    <td style='width:25%;text-align:left;'></td>
                                    <td style='width:25%;text-align:left;'></td>
                                    <td style='width:25%;text-align:left;'></td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Connectivity Link-  InformedDate
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtconnectivityDate" class="NormalText" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtconnectivityDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtconnectivityDate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Connectivity Primary Provider
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="drpconnectPriProvider" class="NormalText" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                                        </asp:DropDownList> 
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Connectivity Primary Status
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                       <asp:DropDownList ID="drpconnectPriStatus" class="NormalText" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                                        </asp:DropDownList> 
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                         Connectivity Secondary Provider 
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="drpconnectSecProvider" class="NormalText" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Connectivity Secondary Status
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                    <asp:DropDownList ID="drpconnectSecStatus" class="NormalText" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                                        </asp:DropDownList> 
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                       Connectivity Primary -Delivery Committed date
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtpricommitdate" class="NormalText" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtpricommitdate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtpricommitdate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                       Connectivity Secondary -Delivery Committed date
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:TextBox ID="txtseccommitdate" class="NormalText" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtseccommitdate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtseccommitdate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Proposed Installation Committed date
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                         <asp:TextBox ID="txtpropinscomdate" class="NormalText" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtpropinscomdate__CalendarExtender" runat="server" Enabled="True" TargetControlID="txtpropinscomdate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        Network Rack installation - Proposed Committed date
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                         <asp:TextBox ID="txtnteinspropcomdate" class="NormalText" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtnteinspropcomdate_calender" runat="server" Enabled="True" TargetControlID="txtnteinspropcomdate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Network Rack installation status 
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="drpnetrackinssta" class="NormalText" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                                        </asp:DropDownList> 
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:25%;text-align:left;'>
                                        ATM sattus
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        <asp:DropDownList ID="drpatmsts" class="NormalText" Style="text-align: left;" runat="server" Font-Names="Cambria" Width="80%" ForeColor="Black">
                                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                                        </asp:DropDownList> 
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                        Final Completed Date
                                    </td>
                                    <td style='width:25%;text-align:left;'>
                                       <asp:TextBox ID="txtfinalcomdate" class="NormalText" runat="server" Width="50%" onkeypress="return false" >
                                        </asp:TextBox>
                                        <asp:CalendarExtender ID="txtfinalcomdate_calender" runat="server" Enabled="True" TargetControlID="txtfinalcomdate" Format="dd MMM yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />            
    </div>
    <br />
     <div style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">            
            <tr id="Tr1">
                <td style="text-align: center;" colspan="4">
                    <asp:Panel ID="pnlSection2" Style="width: 100%;  text-align: right; float: right;" runat="server">
                        <div class="mainhead" style='width:100%; height:auto; padding-top:0px;margin:0px auto;background-color:white;'>
                            <table style='width:100%;font-family:'cambria';' align='center'>
                            <tr>
                           <td style='width:100%;text-align:center;'>
                           <strong>ASSET DETAILS</strong><br />
                           </td>
                            </tr>
                                <tr>
                                    <td style='width:100%;text-align:left;'><br />
                                        <asp:Panel ID="pnlData" runat="server">
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />            
    </div>
    <br />
    <div style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">            
            <tr id="Tr2">
                <td style="text-align: center;" colspan="4">
                    <asp:Panel ID="pnlData1" Style="width: 100%;  text-align: right; float: right;" runat="server">
                        <div class="mainhead" style='width:100%; height:auto; padding-top:0px;margin:0px auto;background-color:white;'>
                            <table style='width:100%;font-family:'cambria';' align='center'>
                            <tr>
                           <td style='width:100%;text-align:center;'>
                           <strong>P2P DETAILS</strong><img src="../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer;  padding-right:10px;" onclick="AddNewRow()" title="Add New" />
                           </td>
                            </tr>
                                <tr>
                                    <td style='width:100%;text-align:left;'><br />
                                        <asp:Panel ID="pnlDatap2p" runat="server">
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />            
    </div>
    <div style="text-align:center; height: 63px;"><br />
        <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="SAVE" onclick="return btnSave_onclick()"/> 
        &nbsp;&nbsp;
        <input id="btnFinish" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="FINISH" onclick="return btnFinish_onclick()"/> 
        &nbsp;&nbsp;
        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()"/>
    </div>   
   </div>
</asp:Content>

