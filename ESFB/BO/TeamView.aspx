﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="TeamView.aspx.vb" Inherits="TeamView" EnableEventValidation="false" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);         
        }        
        .Button:hover
        {            
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            color:#801424;
        }                  
     .bg
     {
         background-color:#FFF;
     }           
        .style1
        {
            width: 609px;
        }
        .style2
        {
            width: 652px;
        }
        .style3
        {
            width: 44%;
        }
    </style>  
         <style type="text/css">
    .danger {
        border-color: #f44336;
        color: red;
        width:25%;
        height:100px;
              
    }
    .wrap 
    { 
        white-space: normal;
        width: 100px; 
    }

.danger:hover
{
    background: #f44336;
    color: white;
    width:25%;
    height:100px;
                
}
    
.button1 
{
  display: inline-block;
  display: block;
  padding: 25px 10px;
   width: 250px;
   font-size: 18px;
   font-weight: bold;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #85929e;
  border-radius: 10px;
  border: 3px solid #555555;
  white-space: normal;
}

.button1:hover 
{
    background-color:#99a3a4
}

.button1:active
{
  background-color: #85929e ;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

.button1:focus
{
  background-color:#566573;
  
} 

.sub_Third
{
           background-color:#FFFFF0; height:15px;
           font-family:Cambria;color:#47476B;font-size:5px;
}   

.mainheads
{
   border:0px;background-color:#E0B9B0; height:2px;font-size:10;color:#47476B;
} 
.alert {
  padding: 20px;
  background-color: #f44336;
  color: white;
}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}              
    </style>  
    <script language="javascript" type="text/javascript">

        function FromServer(arg, context) {           
           if(context == 1){
                var Data = arg.split("Ø");
                if(Data[0]==0){
                    alert(Data[1]);
                    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self"); 
                }
            }
        }
      
       
        function btnExit_onclick() {
//            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self"); 
            window.open("ConsolidatedView.aspx","_self");       
        }
        function table_fill()
        {
             if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
             {  
                var row_bg = 0;
                var tab = "";
//             class=mainheads
                tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' >";
                tab += "<table style='width:100%;margin:0px auto; height:auto; font-family:'cambria';font-size:5px;' align='left'>";
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                            
                for (n = 1; n <= row.length - 1; n++) 
                {

                    col = row[n].split("µ"); 
                    if (row_bg == 0) {
                        row_bg = 1;
                         tab += "<tr style='text-align:left; height:21px; padding-left:20px;'>";
//                          class='sub_first';
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr style='text-align:left; height:21px; padding-left:20px;'>";
//                         class='sub_second';
                    }
                    tab += "<td style='width:41%;text-align:center' class=NormalText ></td>"; 
                    tab += "<td style='width:0%;text-align:center;display:none;' class=NormalText >" + col[0]  + "</td>"; 
                    var  Team = col[1];
                    var btnTeam = "<input id='btnTeam" + n + "' name='btnTeam" + n + "' value='" + Team + "'    type='button' width='10%' onclick='DirectToTeamProcess("+ col[0] + ")' class='button1' />";
                    tab += "<td style='text-align: center;' >"+ btnTeam +"<br /></td> ";
                    tab += "</tr>";
                                                        
                }
                
                tab += "</table></div>";

                document.getElementById("<%= pnlView.ClientID %>").innerHTML = tab;

            }
 
     }  
     function DirectToTeamProcess (TeamId)
     {
        if (TeamId ==1) { window.open("BO_BusinessProcedures.aspx?Bo_Master_Id="+ document.getElementById("<%= hdnMasterID.ClientID %>").value, "_self"); }
        else if (TeamId ==2) { window.open("BO_AdminProcedures.aspx?Bo_Master_Id="+ document.getElementById("<%= hdnMasterID.ClientID %>").value, "_self"); }
        else if (TeamId ==3) { window.open("BO_ITProcedures.aspx?Bo_Master_Id="+ document.getElementById("<%= hdnMasterID.ClientID %>").value, "_self"); }
        else if (TeamId ==4) { window.open("BO_HR.aspx?Bo_Master_Id="+ document.getElementById("<%= hdnMasterID.ClientID %>").value, "_self"); }

     }
    </script>
</head>
</html>
 <br />
 <div  style="width:90%;margin:0px auto; height: 218px;">
<asp:HiddenField ID="hid_dtls" runat="server" />
<asp:HiddenField ID="hdnMasterID" runat="server" />
    <div style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">

<%--    <div style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />--%>
        <table  style="width: 97%; margin: 0px auto; height: 158px;">
            <tr> 
                <td ><asp:Panel ID="pnlView" runat="server"></asp:Panel></td>
            </tr>
            <tr>
            <td style="text-align: center;" >
                <br />
              
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 8%;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
            </td>
        </tr>
        </table>
                            
    </div>
     
</div>
<br />
<br />
<br />
<br />
</asp:Content>

