﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class BO_ITProcedures
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Dim DTFirst As New DataTable
    Dim DTINTIMATE As New DataTable
    Dim DTINTIMATE1 As New DataTable
    Dim dtintiattnd As New DataTable
    Dim dtattnd As New DataTable
    Dim DTINTIMATE2 As New DataTable
    Dim DTINTIMATE3 As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1269) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "IT Procedures"
            Dim UserID As Integer = CInt(Session("UserID"))
            Me.hidUser.Value = CStr(UserID)
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 12 and Status_id = 1 order by 1")
            GF.ComboFill(drpconnectPriProvider, DT, 0, 1)
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 12 and Status_id = 1 order by 1")
            GF.ComboFill(drpconnectSecProvider, DT, 0, 1)
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 13 and Status_id = 1 order by 1")
            GF.ComboFill(drpconnectPriStatus, DT, 0, 1)
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 13 and Status_id = 1 order by 1")
            GF.ComboFill(drpconnectSecStatus, DT, 0, 1)
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 13 and Status_id = 1 order by 1")
            GF.ComboFill(drpnetrackinssta, DT, 0, 1)
            DT = GF.GetQueryResult("select -1 Combo_Id, '-----Select-----' Combo_Text union all select Combo_Id,Combo_Text from BO_COMBO_DATA where Identify_Id = 13 and Status_id = 1 order by 1")
            GF.ComboFill(drpatmsts, DT, 0, 1)
            Dim master_id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))
            hidmaster.Value = CStr(master_id)
            DTFirst = DB.ExecuteDataSet(" select * FROM bo_it_proc WHERE bo_master_id = '" + master_id.ToString() + "' ").Tables(0)
            If DTFirst.Rows.Count > 0 Then
                If Not IsDBNull(DTFirst.Rows(0).Item(2)) Then
                    txtconnectivityDate.Text = CStr(DTFirst.Rows(0).Item(2))
                End If
                drpconnectPriProvider.SelectedValue = CStr(CInt(DTFirst.Rows(0).Item(3).ToString()))
                drpconnectPriStatus.Text = CStr(CInt(DTFirst.Rows(0).Item(4)))
                drpconnectSecProvider.Text = CStr(CInt(DTFirst.Rows(0).Item(5)))
                drpconnectSecStatus.Text = CStr(CInt(DTFirst.Rows(0).Item(6)))
                If Not IsDBNull(DTFirst.Rows(0).Item(7)) Then
                    txtpricommitdate.Text = CStr(DTFirst.Rows(0).Item(7))
                End If
                If Not IsDBNull(DTFirst.Rows(0).Item(8)) Then
                    txtseccommitdate.Text = CStr(DTFirst.Rows(0).Item(8))
                End If
                If Not IsDBNull(DTFirst.Rows(0).Item(9)) Then
                    txtpropinscomdate.Text = CStr(DTFirst.Rows(0).Item(9))
                End If
                If Not IsDBNull(DTFirst.Rows(0).Item(10)) Then
                    txtnteinspropcomdate.Text = CStr(DTFirst.Rows(0).Item(10))
                End If
                drpnetrackinssta.Text = CStr(CInt(DTFirst.Rows(0).Item(11)))
                drpatmsts.Text = CStr(CInt(DTFirst.Rows(0).Item(12)))
                If Not IsDBNull(DTFirst.Rows(0).Item(13)) Then
                    txtfinalcomdate.Text = CStr(DTFirst.Rows(0).Item(13))
                End If
            End If
            Dim count As Integer = 0
            Dim rowcount As Integer = 1
            DTINTIMATE = DB.ExecuteDataSet(" SELECT B.* FROM BO_SECTION_INTIMATE A INNER JOIN BO_INTIMATE_DTLS B ON A.SECTION_ID = B.SECTION_ID " +
                                           " WHERE A.INTIMATE_TO_TEAM =3 AND B.INTIMATE_TO_TEAM = 3 AND B.BO_MASTER_ID = '" + master_id.ToString() + "'").Tables(0)

            DTINTIMATE1 = DB.ExecuteDataSet(" SELECT B.* FROM BO_SECTION_INTIMATE A INNER JOIN BO_INTIMATE_DTLS B ON A.SECTION_ID = B.SECTION_ID " +
                                          " WHERE A.INTIMATE_TO_TEAM =3 AND B.INTIMATE_TO_TEAM = 3 AND B.BO_MASTER_ID = '" + master_id.ToString() + "' AND B.ATTEND_BY_TEAM IS null").Tables(0)

            If DTINTIMATE.Rows.Count > 0 Then
                rowcount = 0
            End If

            DT = DB.ExecuteDataSet(" select distinct biap.it_req_id,combo_text,req_no, " +
                                   " available_no,balance_no, " +
                                   " remarks,DoneStatus,It_Identity_Id " +
                                   " from bo_it_asset_proc biap " +
                                    " inner join bo_combo_data bcd on bcd.combo_id=biap.it_req_id and bcd.identify_id=16 " +
                                    " inner join bo_it_asset_settings bir on bir.req_id=bcd.combo_id and bcd.identify_id=16 " +
                                    " and biap.bo_master_id='" + master_id.ToString() + "'").Tables(0)
            If DT.Rows.Count > 0 Then
                hidData.Value = GF.GetBranchDataIT(DT)
            Else
                DT = DB.ExecuteDataSet(" select combo_id,combo_text,case when it_identity_id=1 then no else '0' end as required, " +
                                        " case when it_identity_id=1 then convert(int,'0') else convert(int,'0') end as available, " +
                                        " case when it_identity_id=1 then convert(int,'0') else convert(int,'0') end as balance,'NIL', " +
                                        " case when it_identity_id=2 then charge else convert(int,'0') end,It_Identity_Id from bo_combo_data bam " +
                                        " inner join bo_it_asset_settings bir on bir.req_id=bam.combo_id and bam.identify_id=16 " +
                                        " inner join bo_master bm on bm.comm_mode_id = bir.comm_mode_id and " +
                                        " bm.type_id = bir.type_id And " +
                                        " bm.pop_cate_id = bir.pop_cate_id And " +
                                        " bm.premise_type_id = bir.pre_type_id And bm.bo_master_id = '" + master_id.ToString() + "' ").Tables(0)
                hidData.Value = GF.GetBranchDataIT(DT)
            End If
            DT1 = DB.ExecuteDataSet("select p2p_no,cast(p2p_req_dt as date),po_no,cast(po_dt as date),remarks,p2p_id from bo_it_p2p where bo_master_id='" + master_id.ToString() + "' ").Tables(0)
            hidDatap2p.Value = GF.GetBranchDataITP2P(DT1)

            If DTINTIMATE1.Rows.Count = 0 And rowcount = 0 Then
                'DTINTIMATE1 = DB.ExecuteDataSet(" SELECT B.* FROM BO_SECTION_INTIMATE A INNER JOIN BO_INTIMATE_DTLS B ON A.SECTION_ID = B.SECTION_ID " +
                '                                " WHERE A.INTIMATE_TO_TEAM =3 AND B.INTIMATE_TO_TEAM = 3 AND B.BO_MASTER_ID = '" + master_id.ToString() + "' AND B.ATTEND_BY_TEAM IS not null and work_ongoing=1").Tables(0)
                'DTINTIMATE2 = DB.ExecuteDataSet(" SELECT * FROM BO_INTIMATE_DTLS " +
                '                                " WHERE section_id =7 AND BO_MASTER_ID = '" + master_id.ToString() + "' AND ATTEND_BY_TEAM IS not null ").Tables(0)
                DTINTIMATE3 = DB.ExecuteDataSet(" SELECT B.* FROM BO_SECTION_INTIMATE A INNER JOIN BO_INTIMATE_DTLS B ON A.SECTION_ID = B.SECTION_ID " +
                                                " WHERE A.INTIMATE_TO_TEAM =3 AND B.INTIMATE_TO_TEAM = 3 AND B.BO_MASTER_ID = '" + master_id.ToString() + "' AND B.ATTEND_BY_TEAM IS not null and work_ongoing=0").Tables(0)
                If DTINTIMATE3.Rows.Count <> 0 Then
                    hidenable.Value = "0"
                Else
                    'If DTINTIMATE2.Rows.Count > 0 Then
                    '    hidenable.Value = "0"
                    'Else
                    hidenable.Value = "1"
                    'End If
                End If
            Else
                If rowcount = 1 Then
                    hidenable.Value = "0"
                Else
                    hidenable.Value = "1"
                End If
            End If
            dtintiattnd = DB.ExecuteDataSet(" SELECT * FROM bo_intimate_dtls " +
                                            " WHERE INTIMATE_by_TEAM =3 AND BO_MASTER_ID = '" + master_id.ToString() + "' AND section_id=7").Tables(0)
            If dtintiattnd.Rows.Count = 0 Then

                hidintimate.Value = "0"
            Else
                hidintimate.Value = "1"
            End If
            dtattnd = DB.ExecuteDataSet(" SELECT * FROM bo_intimate_dtls " +
                                        " WHERE INTIMATE_to_TEAM =3 AND BO_MASTER_ID = '" + master_id.ToString() + "' AND attend_by_team is null").Tables(0)
            If dtattnd.Rows.Count = 0 Then
                hidattend.Value = "1"
            Else
                hidattend.Value = "0"
            End If
            If rowcount = 1 Then
                hidattend.Value = "0"
            End If
            If GF.CheckdeptUser(CInt(Session("UserID")), 3) = False Then
                hidenable.Value = "0"
            End If
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "windowload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim master_id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))
            Try
                Dim value1 As Nullable(Of Date)
                If CStr(Data(1)) <> "" Then
                    value1 = CDate(Data(1))
                End If

                Dim value2 As Integer = 0
                If CStr(Data(2)) <> "" Then
                    value2 = CInt(Data(2))
                End If

                Dim value3 As Integer = 0
                If CStr(Data(3)) <> "" Then
                    value3 = CInt(Data(2))
                End If

                Dim value4 As Integer = 0
                If CStr(Data(4)) <> "" Then
                    value4 = CInt(Data(4))
                End If

                Dim value5 As Integer = 0
                If CStr(Data(5)) <> "" Then
                    value5 = CInt(Data(5))
                End If

                Dim value6 As Nullable(Of Date)
                Dim value7 As Nullable(Of Date)
                Dim value8 As Nullable(Of Date)
                Dim value9 As Nullable(Of Date)

                If CStr(Data(6)) <> "" Then
                    value6 = CDate(Data(6))
                End If

                If CStr(Data(7)) <> "" Then
                    value7 = CDate(Data(7))
                End If

                If CStr(Data(8)) <> "" Then
                    value8 = CDate(Data(8))
                End If

                If CStr(Data(9)) <> "" Then
                    value9 = CDate(Data(9))
                End If

                Dim value10 As Integer = 0
                Dim value11 As Integer = 0

                If CStr(Data(10)) <> "" Then
                    value10 = CInt(Data(10))
                End If
                If CStr(Data(11)) <> "" Then
                    value11 = CInt(Data(11))
                End If

                Dim value12 As Nullable(Of Date)
                If CStr(Data(12)) <> "" Then
                    value12 = CDate(Data(12))
                End If

                Dim assetdata As String = ""
                If Data(13).ToString() <> "undefined" Then
                    assetdata = Data(13).ToString()
                    If assetdata <> "" Then
                        assetdata = assetdata.Substring(1)
                    End If
                End If
                Dim p2pdata As String = ""
                If Data(14).ToString() <> "undefined" Then
                    p2pdata = Data(14).ToString()
                    If p2pdata <> "" Then
                        p2pdata = p2pdata.Substring(1)
                    End If
                End If
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                Dim Params(19) As SqlParameter

                Params(0) = New SqlParameter("@BO_MASTER_ID_VALUE", SqlDbType.Int)
                Params(0).Value = master_id

                Params(1) = New SqlParameter("@value1", SqlDbType.Date)
                Params(1).Value = value1

                Params(2) = New SqlParameter("@value2", SqlDbType.Int)
                Params(2).Value = value2
                Params(3) = New SqlParameter("@value3", SqlDbType.Int)
                Params(3).Value = value3
                Params(4) = New SqlParameter("@value4", SqlDbType.Int)
                Params(4).Value = value4
                Params(5) = New SqlParameter("@value5", SqlDbType.Int)
                Params(5).Value = value5

                Params(6) = New SqlParameter("@value6", SqlDbType.Date)
                Params(6).Value = value6
                Params(7) = New SqlParameter("@value7", SqlDbType.Date)
                Params(7).Value = value7
                Params(8) = New SqlParameter("@value8", SqlDbType.Date)
                Params(8).Value = value8
                Params(9) = New SqlParameter("@value9", SqlDbType.Date)
                Params(9).Value = value9

                Params(10) = New SqlParameter("@value10", SqlDbType.Int)
                Params(10).Value = value10
                Params(11) = New SqlParameter("@value11", SqlDbType.Int)
                Params(11).Value = value11

                Params(12) = New SqlParameter("@value12", SqlDbType.Date)
                Params(12).Value = value12

                Params(13) = New SqlParameter("@Assetdata", SqlDbType.VarChar)
                Params(13).Value = assetdata

                Params(14) = New SqlParameter("@P2Pdata", SqlDbType.VarChar)
                Params(14).Value = p2pdata

                Params(15) = New SqlParameter("@UserID", SqlDbType.VarChar)
                Params(15).Value = Session("UserID")

                Params(16) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(16).Direction = ParameterDirection.Output
                Params(17) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(17).Direction = ParameterDirection.Output

                Params(18) = New SqlParameter("@Attend", SqlDbType.Int)
                Params(18).Value = CInt(Data(15))
                Params(19) = New SqlParameter("@Intimate", SqlDbType.Int)
                Params(19).Value = CInt(Data(16))

                DB.ExecuteNonQuery("SP_BO_IT_UPDATION", Params)
                ErrorFlag = CInt(Params(16).Value)
                Message = CStr(Params(17).Value)

            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn += Message + "|" + ErrorFlag.ToString()
        End If

        If CInt(Data(0)) = 2 Then
            Dim ErrorFlag As Integer = 0
            Dim Message As String = Nothing
            Dim master_id As Integer = CInt(Request.QueryString.Get("Bo_Master_Id"))
            Try
                Dim value1 As Nullable(Of Date)
                If CStr(Data(1)) <> "" Then
                    value1 = CDate(Data(1))
                End If

                Dim value2 As Integer = 0
                If CStr(Data(2)) <> "" Then
                    value2 = CInt(Data(2))
                End If

                Dim value3 As Integer = 0
                If CStr(Data(3)) <> "" Then
                    value3 = CInt(Data(2))
                End If

                Dim value4 As Integer = 0
                If CStr(Data(4)) <> "" Then
                    value4 = CInt(Data(4))
                End If

                Dim value5 As Integer = 0
                If CStr(Data(5)) <> "" Then
                    value5 = CInt(Data(5))
                End If

                Dim value6 As Nullable(Of Date)
                Dim value7 As Nullable(Of Date)
                Dim value8 As Nullable(Of Date)
                Dim value9 As Nullable(Of Date)

                If CStr(Data(6)) <> "" Then
                    value6 = CDate(Data(6))
                End If

                If CStr(Data(7)) <> "" Then
                    value7 = CDate(Data(7))
                End If

                If CStr(Data(8)) <> "" Then
                    value8 = CDate(Data(8))
                End If

                If CStr(Data(9)) <> "" Then
                    value9 = CDate(Data(9))
                End If

                Dim value10 As Integer = 0
                Dim value11 As Integer = 0

                If CStr(Data(10)) <> "" Then
                    value10 = CInt(Data(10))
                End If
                If CStr(Data(11)) <> "" Then
                    value11 = CInt(Data(11))
                End If

                Dim value12 As Nullable(Of Date)
                If CStr(Data(12)) <> "" Then
                    value12 = CDate(Data(12))
                End If

                Dim assetdata As String = ""
                If Data(13).ToString() <> "undefined" Then
                    assetdata = Data(13).ToString()
                    If assetdata <> "" Then
                        assetdata = assetdata.Substring(1)
                    End If
                End If
                Dim p2pdata As String = ""
                If Data(14).ToString() <> "undefined" Then
                    p2pdata = Data(14).ToString()
                    If p2pdata <> "" Then
                        p2pdata = p2pdata.Substring(1)
                    End If
                End If
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                Dim Params(20) As SqlParameter

                Params(0) = New SqlParameter("@BO_MASTER_ID_VALUE", SqlDbType.Int)
                Params(0).Value = master_id

                Params(1) = New SqlParameter("@value1", SqlDbType.Date)
                Params(1).Value = value1

                Params(2) = New SqlParameter("@value2", SqlDbType.Int)
                Params(2).Value = value2
                Params(3) = New SqlParameter("@value3", SqlDbType.Int)
                Params(3).Value = value3
                Params(4) = New SqlParameter("@value4", SqlDbType.Int)
                Params(4).Value = value4
                Params(5) = New SqlParameter("@value5", SqlDbType.Int)
                Params(5).Value = value5

                Params(6) = New SqlParameter("@value6", SqlDbType.Date)
                Params(6).Value = value6
                Params(7) = New SqlParameter("@value7", SqlDbType.Date)
                Params(7).Value = value7
                Params(8) = New SqlParameter("@value8", SqlDbType.Date)
                Params(8).Value = value8
                Params(9) = New SqlParameter("@value9", SqlDbType.Date)
                Params(9).Value = value9

                Params(10) = New SqlParameter("@value10", SqlDbType.Int)
                Params(10).Value = value10
                Params(11) = New SqlParameter("@value11", SqlDbType.Int)
                Params(11).Value = value11

                Params(12) = New SqlParameter("@value12", SqlDbType.Date)
                Params(12).Value = value12

                Params(13) = New SqlParameter("@Assetdata", SqlDbType.VarChar)
                Params(13).Value = assetdata

                Params(14) = New SqlParameter("@P2Pdata", SqlDbType.VarChar)
                Params(14).Value = p2pdata

                Params(15) = New SqlParameter("@UserID", SqlDbType.VarChar)
                Params(15).Value = Session("UserID")

                Params(16) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(16).Direction = ParameterDirection.Output
                Params(17) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(17).Direction = ParameterDirection.Output

                Params(18) = New SqlParameter("@Attend", SqlDbType.Int)
                Params(18).Value = CInt(Data(15))
                Params(19) = New SqlParameter("@Intimate", SqlDbType.Int)
                Params(19).Value = CInt(Data(16))

                Params(20) = New SqlParameter("@Mode", SqlDbType.VarChar)
                Params(20).Value = 2

                DB.ExecuteNonQuery("SP_BO_IT_UPDATION", Params)
                ErrorFlag = CInt(Params(16).Value)
                Message = CStr(Params(17).Value)

            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn += Message + "|" + ErrorFlag.ToString()
        End If
    End Sub
    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click

    'End Sub
End Class
