﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Text

Partial Class BO_UploadList
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1277) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Upload List"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim IntPost As Integer = 0
            Dim IntDep As Integer = 0
            Me.btnSave.Attributes.Add("onclick", "return RequestOnClick()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent


    End Sub
#End Region

  
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim msg As String = ""
        Dim connectionString As String = ""
        Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
        Dim branch_id As String = Data(0)
        'Dim DateValue As Date = CDate(Data(1))
        Dim user As String = Session("UserID").ToString()
        If Me.fup1.HasFile = False Then
            Dim cl_script0 As New System.Text.StringBuilder
            cl_script0.Append("         alert('Please browse excel');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
            Exit Sub
        Else
            Dim fileName As String = Path.GetFileName(fup1.PostedFile.FileName)
            Dim fileExtension As String = Path.GetExtension(fup1.PostedFile.FileName)
            Dim fileLocation As String = Server.MapPath("../" & fileName)
            fup1.SaveAs(fileLocation)

            If fileExtension = ".xls" Then
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=NO;"""
            ElseIf fileExtension = ".xlsx" Then
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties='Excel 12.0 Xml;HDR=NO'"
            Else
                Dim cl_script0 As New System.Text.StringBuilder
                cl_script0.Append("         alert('Uploaded Only Excel file with Extention .xls or .xlsx ');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
                Exit Sub

            End If
            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con

            Dim values As New DataTable
            values.Columns.Add("[No]", GetType(String))
            values.Columns.Add("[Branch Name]", GetType(String))
            values.Columns.Add("[District‎]", GetType(String))
            values.Columns.Add("[STATE]", GetType(String))
            values.Columns.Add("[Tier]", GetType(String))
            values.Columns.Add("[URC Y/N]", GetType(String))
            values.Columns.Add("[North East Y/N]", GetType(String))
            values.Columns.Add("[LWE Dist Y/N]", GetType(String))
            values.Columns.Add("[New/Conversion]", GetType(String))
     
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            File.Delete(fileLocation)
            Dim StrErr As String = ""
            For count = 1 To dtExcelRecords.Rows.Count - 1
                Dim row = values.NewRow()
               
                row(0) = dtExcelRecords.Rows(count).Item(0).ToString()
                row(1) = dtExcelRecords.Rows(count).Item(1).ToString()
                row(2) = dtExcelRecords.Rows(count).Item(2).ToString()
                row(3) = dtExcelRecords.Rows(count).Item(3).ToString()
                row(4) = dtExcelRecords.Rows(count).Item(4).ToString()
                row(5) = dtExcelRecords.Rows(count).Item(5).ToString()
                row(6) = dtExcelRecords.Rows(count).Item(6).ToString()
                row(7) = dtExcelRecords.Rows(count).Item(7).ToString()
                row(8) = dtExcelRecords.Rows(count).Item(8).ToString()
               
                values.Rows.Add(row)
            Next
            con.Close()
            Try
                'Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TableData", dtExcelRecords), New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0), New System.Data.SqlClient.SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000), New System.Data.SqlClient.SqlParameter("@Date", hdnDate.Value)}
                Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TableData", values),
                                                                          New System.Data.SqlClient.SqlParameter("@UserId", 0),
                                                                          New System.Data.SqlClient.SqlParameter("@ErrorStatus", 0),
                                                                          New System.Data.SqlClient.SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)}
                Parameters(2).Direction = ParameterDirection.Output
                Parameters(3).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_BO_UPLOAD_LIST", Parameters)
                ErrorFlag = CInt(Parameters(2).Value)
                Message = CStr(Parameters(3).Value)


                Dim cl_script1 As New System.Text.StringBuilder
                cl_script1.Append("         alert('" + Message.ToString + "');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)


            Catch ex As Exception
                MsgBox("Exceptional Error Occurred.Please Inform Application Team")
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
        End If
    End Sub
End Class
