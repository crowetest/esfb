﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Text

Partial Class ConsolidatedView
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions

    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1278) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Consolidated View"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//

            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
       
            DT = DB.ExecuteDataSet(" select Upload_Id,  CONVERT(varchar, day(Upload_Dt)) +'-'+datename(MONTH,Upload_Dt) +'-'+CAST(YEAR(Upload_Dt) AS VARCHAR(4)) from bo_upload_list").Tables(0)
            GN.ComboFill(cmbDate, DT, 0, 1)
            Me.cmbDate.Attributes.Add("onchange", "return DateOnchange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim Upload_Id As Integer
            Upload_Id = CInt(Data(1))
            DT = DB.ExecuteDataSet("select ROW_NUMBER() OVER(ORDER BY bo_master_id) , bo_master_id,Branch_Name,District_Name AS DISTRICT, State_Name  AS STATE, D.COMBO_TEXT AS TIER , E.COMBO_TEXT  AS TYPE , " +
                 "                 North_East as [North East Y/N], LWE_Dist as [LWE Dist Y/N], F.COMBO_TEXT  AS [COMMENSEMENT MODE] " +
                 "                 from bo_master A " +
                 "                 inner join District_Master B on A.DisCtrict_Id = B.District_Id  " +
                 "                 inner join State_Master C on A.State_id = C.State_id " +
                 "                 INNER JOIN BO_COMBO_DATA D ON D.COMBO_ID = A.TIER_ID AND D.IDENTIFY_ID = 5 " +
                 "                 INNER JOIN BO_COMBO_DATA E ON E.COMBO_ID = A.TYPE_ID AND E.IDENTIFY_ID = 2 " +
                 "                 INNER JOIN BO_COMBO_DATA F ON F.COMBO_ID = A.TYPE_ID AND F.IDENTIFY_ID = 1 " +
                 "                 where upload_id = " + Upload_Id.ToString() + "").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString() + "ÿ" + DR(2).ToString() + "ÿ" + DR(3).ToString() + "ÿ" + DR(4).ToString() + "ÿ" + DR(5).ToString() + "ÿ" + DR(6).ToString() + "ÿ" + DR(7).ToString() + "ÿ" + DR(8).ToString() + "ÿ" + DR(9).ToString()
            Next
        End If

    End Sub
#End Region


End Class
