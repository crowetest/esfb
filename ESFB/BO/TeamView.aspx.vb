﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Text

Partial Class TeamView
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_PRD As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim Bo_Master_Id As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
           
            Me.Master.subtitle = "Team View"

            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim DTT As New DataTable
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Bo_Master_Id = CInt(Request.QueryString.Get("Bo_Master_Id"))

            DT = DB.ExecuteDataSet("select team_id, team_name from bo_team_master where status_id = 1").Tables(0)
            Dim StrUpload As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrUpload += "¥" & DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString
        Next
            hid_dtls.Value = StrUpload
            hdnMasterID.Value = CStr(Bo_Master_Id)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent


    End Sub
#End Region

End Class
