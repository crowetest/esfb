﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="BO_HR.aspx.vb" Inherits="BO_HR" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/jquery-1.2.6.min.js" type="text/javascript"></script>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);         
        }        
        .Button:hover
        {            
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);
            color:#801424;
        }                  
     .bg
     {
         background-color:#FFF;
     }           
    </style>    
    <script language="javascript" type="text/javascript">
        var DisFlag;
        function windowload() 
        {
             if (document.getElementById("<%= hidenable.ClientID %>").value == "0")
             {
                DisFlag = 'disabled';
                disableTest();
                if (document.getElementById("<%= hidattend .ClientID %>").value == "0")
                {
                    document.getElementById("<%= chkattend.ClientID %>").checked = false;
                    document.getElementById("<%= chkattend.ClientID %>").disabled = false;
                }
                else
                {
                document.getElementById("<%= chkattend.ClientID %>").checked = true;
                document.getElementById("<%= chkattend.ClientID %>").disabled = true;
                }
                if (document.getElementById("<%= hidintimate.ClientID %>").value == "0")
                document.getElementById("<%= chkintimate.ClientID %>").checked = false;
                else
                document.getElementById("<%= chkintimate.ClientID %>").checked = true;
             }
             else
             {
                DisFlag = '';
                if (document.getElementById("<%= hidattend .ClientID %>").value == "0")
                {
                    document.getElementById("<%= chkattend.ClientID %>").checked = false;
                    document.getElementById("<%= chkattend.ClientID %>").disabled = false;
                }
                else
                {
                document.getElementById("<%= chkattend.ClientID %>").checked = true;
                document.getElementById("<%= chkattend.ClientID %>").disabled = true;
                }
                if (document.getElementById("<%= hidintimate.ClientID %>").value == "0")
                document.getElementById("<%= chkintimate.ClientID %>").checked = false;
                else
                document.getElementById("<%= chkintimate.ClientID %>").checked = true;
             }
             table_fill();
        }
         function disableTest(){
            $(".dis").attr("disabled",true);
            document.getElementById("section1").disabled = true;
            var nodes = document.getElementById("section1").getElementsByTagName('*');
            for(var i = 0; i < nodes.length; i++){
                nodes[i].disabled = true;
            }
            document.getElementById("btnExit").disabled = false;  
         }
        function table_fill() 
        {
         if (document.getElementById("<%= hidData.ClientID %>").value != "") {
            document.getElementById("<%= pnlData.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:15%;text-align:center'>#</td>"; 
            tab += "<td style='width:25%;text-align:left'>Designation</td>"; 
            tab += "<td style='width:10%;text-align:left'>Required</td>";   
            tab += "<td style='width:10%;text-align:left'>Available</td>";
            tab += "<td style='width:10%;text-align:left'>Balance</td>";
            tab += "<td style='width:30%;text-align:left'>Remarks</td>";    
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
           
                row = document.getElementById("<%= hidData.ClientID %>").value.split("¥");
                var i = 1;
                for (n = 1; n <= row.length-1; n++) {
                    col = row[n].split("^");                    
                    if (row_bg == 0) 
                    {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else 
                    {
                         row_bg = 0;
                         tab += "<tr class=sub_second>";
                    }
                                                     
                    tab += "<td style='width:15%;text-align:center' >" + i  + "</td>";
                    tab += "<td style='width:25%;text-align:left' class='NormalText'>" + col[2] + "</td>";
                    if ( col[3] != "")
                    {
                        var txtQualiBox = "<input id='txtrequired" + n + "' name='txtrequired" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' value= '" +col[3]+ "' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                           
                        tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    else
                    {
                       var txtQualiBox = "<input id='txtrequired" + n + "' name='txtrequired" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                           
                       tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    if ( col[4] != "")
                    {
                        var txtQualiBox = "<input id='txtavail" + n + "' name='txtavail" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' value= '" +col[4]+ "' onchange='updateValue("+ n +")'/>";                                            
                        tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    else
                    {
                        var txtQualiBox = "<input id='txtavail" + n + "' name='txtavail" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                                            
                        tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    var bal = col[3] - col[4];
                    if ( col[5] != "")
                    {
                        var txtQualiBox = "<input id='txtbal" + n + "' name='txtbal" + n + "' type='Text' disabled='true' style='width:99%;' class='NormalText' maxlength='50' value= '" +bal+ "' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                                            
                        tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    else
                    {
                        var txtQualiBox = "<input id='txtbal" + n + "' name='txtQuali" + n + "' disabled='true' type='Text' style='width:99%;' class='NormalText' maxlength='50' onkeypress='return NumericCheck(event)' onchange='updateValue("+ n +")'/>";                                            
                        tab += "<td style='width:10%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    if ( col[6] != "")
                    {
                    var txtQualiBox = "<input id='txtremark" + n + "' name='txtremark" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='500'  value= '" +col[6]+ "' onchange='updateValue("+ n +")'/>";                                            
                    tab += "<td style='width:30%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    else
                    {
                        var txtQualiBox = "<input id='txtremark" + n + "' name='txtremark" + n + "' type='Text' style='width:99%;' class='NormalText' "+ DisFlag + " maxlength='500'  onchange='updateValue("+ n +")'/>";                                            
                        tab += "<td style='width:30%;text-align:left'>" + txtQualiBox + "</td>";
                    }
                    tab += "</tr>";
                    i++;
                }
            
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnlData.ClientID %>").innerHTML = tab;
//            setTimeout(function() {
//                document.getElementById("cmbQualification"+(n-1)).focus().select();
//                }, 4);
            }
            else
            document.getElementById("<%= pnlData.ClientID %>").style.display = 'none';
            //--------------------- Clearing Data ------------------------//


        }
         function updateValue(id)
        {       
                row = document.getElementById("<%= hidData.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1; n++) 
                {
                   var Qualcol = row[n].split("^");
                   if(id==n)
                   {
                           var masterid = Qualcol[0];
                           var desi  = Qualcol[1];
                           var text = Qualcol[2];
                           var required = document.getElementById("txtrequired"+id).value;
                           var available=document.getElementById("txtavail"+id).value;
                           var diff = 0;
                           if ( available > required )
                           {
                                alert(" Available value should be less than required value");
                                document.getElementById("txtavail"+id).value = 0;
                                document.getElementById("txtbal"+id).value = required;
                           }
                           else
                           {
                                diff = required - available;
                                document.getElementById("txtbal"+id).value = diff;
                           }
                           var balance=document.getElementById("txtbal"+id).value;
                           var remark=document.getElementById("txtremark"+id).value;
                           NewStr += "¥" + masterid.toString()+"^"+desi.toString()+"^"+text.toString()+"^"+required+"^"+available+"^"+balance+"^"+remark;
                   }
                   else
                   {
                        NewStr += "¥"+row[n];
                   }
                }
                document.getElementById("<%= hidData.ClientID %>").value=NewStr;
        }
        function btnIntimate_onclick()
        {
             var newstr="";
            row = document.getElementById("<%= hidData.ClientID %>").value.split("¥");

             var NewStr=""
                for (n = 1; n <= row.length-1; n++) 
                {   
                    var Qualcol=row[n].split("^");
                    if(Qualcol[3]=="")
                    {
                        alert("Enter required value in all fields");
                        return false;
                    }
                    if(Qualcol[4]=="")
                    {
                        alert("Enter available value in all fields");
                        return false;
                    }
                    if(Qualcol[5]=="")
                    {
                        alert("Enter balance value in all fields");
                        return false;
                    }     
                      
                    NewStr+="¥"+Qualcol[0]+"µ"+Qualcol[1]+"µ"+Qualcol[2]+"µ" +Qualcol[3]+"µ"+Qualcol[4]+"µ"+Qualcol[5]+"µ"+Qualcol[6];
                }
                var chknominee = 0;
                if(document.getElementById("chkNominee").checked==true)
                {
                    chkNominee = 1;
                }
                var data = "3Ø" + NewStr+"Ø"+chkNominee;
                ToServer(data ,3);
        }
        function btnFinish_onclick()
        {
            var newstr="";
            row = document.getElementById("<%= hidData.ClientID %>").value.split("¥");

             var NewStr=""
                for (n = 1; n <= row.length-1; n++) 
                {   
                    var Qualcol=row[n].split("^");
                    if(Qualcol[3]=="")
                    {
                        alert("Enter required value in all fields");
                        return false;
                    }
                    if(Qualcol[4]=="")
                    {
                        alert("Enter available value in all fields");
                        return false;
                    }
                    if(Qualcol[5]=="")
                    {
                        alert("Enter balance value in all fields");
                        return false;
                    }     
                      
                    NewStr+="¥"+Qualcol[0]+"µ"+Qualcol[1]+"µ"+Qualcol[2]+"µ" +Qualcol[3]+"µ"+Qualcol[4]+"µ"+Qualcol[5]+"µ"+Qualcol[6];
                }
                if(document.getElementById("<%= chkattend.ClientID %>").checked==false)
                {
                    alert( "Please check the Attend By check box. Otherwise you cant save the data" );
                    return false;
                }
                if(document.getElementById("<%= chkintimate.ClientID %>").checked==false)
                {
                    alert( "Please check the Intimate check box. Otherwise you cant complete the process" );
                    return false;
                }
                var chkattend = 0;
                if(document.getElementById("<%= chkattend.ClientID %>").checked==true)
                {
                    chkattend = 1;
                }
                var chkintimate = 0;
                if(document.getElementById("<%= chkintimate.ClientID  %>").checked==true)
                {
                    chkintimate = 1;
                }
                var data = "2Ø" + NewStr+"Ø"+chkattend+"Ø"+chkintimate;
                ToServer(data ,2);
        }
        function btnSave_onclick()
        {
             var newstr="";
             row = document.getElementById("<%= hidData.ClientID %>").value.split("¥");

             var NewStr=""
                for (n = 1; n <= row.length-1; n++) 
                {   
                    var Qualcol=row[n].split("^");
//                    if(Qualcol[3]=="")
//                    {
//                        alert("Enter required value");
//                        return false;
//                    }
//                    if(Qualcol[4]=="")
//                    {
//                        alert("Enter available value");
//                        return false;
//                    }
//                    if(Qualcol[5]=="")
//                    {
//                        alert("Enter balance value");
//                        return false;
//                    }     
                      
                    NewStr+="¥"+Qualcol[0]+"µ"+Qualcol[1]+"µ"+Qualcol[2]+"µ" +Qualcol[3]+"µ"+Qualcol[4]+"µ"+Qualcol[5]+"µ"+Qualcol[6];
                }

                if(document.getElementById("<%= chkattend.ClientID %>").checked==false)
                {
                    alert( "Please check the Attend By check box. Otherwise you cant save the data" );
                    return false;
                }

                var chkattend = 0;
                if(document.getElementById("<%= chkattend.ClientID %>").checked==true)
                {
                    chkattend = 1;
                }
                var chkintimate = 0;
                if(document.getElementById("<%= chkintimate.ClientID  %>").checked==true)
                {
                    chkintimate = 1;
                }

                var data = "1Ø" + NewStr+"Ø"+chkattend+"Ø"+chkintimate;
                ToServer(data ,1);
        }
        function FromServer(arg, context) {           
           if(context == 1)
           {
                var Data = arg.split("|");
                if(Data[1]==0)
                {
                    alert(Data[0]);
                     var master = document.getElementById("<%= hidmaster.ClientID %>").value;
            window.open("TeamView.aspx?Bo_Master_Id=" + master.toString() + "", "_self");
                }
                else
                {
                    alert(Data[0]);
                }
            }
            if(context == 2)
           {
                var Data = arg.split("|");
                if(Data[1]==0)
                {
                    alert(Data[0]);
                     var master = document.getElementById("<%= hidmaster.ClientID %>").value;
            window.open("TeamView.aspx?Bo_Master_Id=" + master.toString() + "", "_self"); 
                }
                else
                {
                    alert(Data[0]);
                }
            }
           if(context == 3)
           {
                var Data = arg.split("|");
                if(Data[1]==0)
                {
                    alert(Data[0]);
                    var master = document.getElementById("<%= hidmaster.ClientID %>").value;
                    window.open("TeamView.aspx?Bo_Master_Id=" + master.toString() + "", "_self");
                }
                else
                {
                    alert(Data[0]);
                }
            }
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            var master = document.getElementById("<%= hidmaster.ClientID %>").value;
            window.open("TeamView.aspx?Bo_Master_Id=" + master.toString() + "", "_self");
//            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
    </script>
</head>
</html>
 <br />
 <div id = "section1" style="width:90%;margin:0px auto; background-color:#EEB8A6;">
    <div style="width:80%;padding-left:10px;margin:0px auto;">
        <asp:HiddenField ID="hidData" runat="server" />
        <asp:HiddenField ID = "hidUser" runat="server" />
        <asp:HiddenField ID = "hidenable" runat="server" />
        <asp:HiddenField ID = "hidintimate" runat="server" />
        <asp:HiddenField ID = "hidattend" runat="server" />
        <asp:HiddenField ID = "hidmaster" runat="server" />
        <br /> 
    </div>
    <div style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">            
            <tr id="List">
                <td style="text-align: center;" colspan="4">
                    <asp:CheckBox ID="chkattend" runat="server"/>&nbsp;<asp:label ID="lblsupport" class="NormalText" runat="server" 
                    Width="7%" Text="Attend By"/> &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkintimate" runat="server"/>&nbsp;<asp:label ID="Label1" class="NormalText" runat="server" 
                    Width="7%" Text="Intimate"/>
                    <asp:Panel ID="pnlSection1" Style="width: 100%;  text-align: right; float: right;" runat="server">
                        <div class="mainhead" style='width:100%; height:auto; padding-top:0px;margin:0px auto;background-color:white;'>
                            <table style='width:100%;font-family:'cambria';' align='center'>
                                <tr>
                                    <td style='width:100%;text-align:left;'>
                                        <asp:Panel ID="pnlData" runat="server">
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />            
    </div>
    <div style="text-align:center; height: 63px;"><br />
        <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="SAVE" onclick="return btnSave_onclick()"/> 
        &nbsp;&nbsp;
        <input id="btnFinish" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="FINISH" onclick="return btnFinish_onclick()"/> 
        &nbsp;&nbsp;
        <input id="btnIntermediate" style="font-family: cambria; display:none; cursor: pointer; width:8%;" type="button" value="INTIMATE" onclick="return btnIntimate_onclick()"/> 
        &nbsp;&nbsp;
        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()"/>
    </div>   
</div>
</asp:Content>

