﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="Portal.aspx.vb" Inherits="Portal" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ESFB</title>
    <link rel="shortcut icon" href="~/Image/favicon.ico" type="image/x-icon" />
    <link href="Style/PortalStyle.css" rel="stylesheet" type="text/css" />
    <script src="Script/SpryTabbedPanels.js" type="text/javascript"></script>
    <link href="Style/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />

    
<script language="javascript">
    document.onmousedown = disableclick;
    status = "Right Click Disabled";
    function disableclick(event) {
        if (event.button == 2) {
            alert(status);
            return false;
        }
    }
</script>


    <script language="javascript" type="text/javascript">
        function window_onload() {       
            if (document.getElementById("<%= hidPost.ClientID %>").value == 5) {
                
                document.getElementById("Branch_Address").style.display = '';                
            }
            else {
               
                document.getElementById("Branch_Address").style.display = 'none'; 
            }
            if (document.getElementById("<%= hidStaffFlag.ClientID %>").value == document.getElementById("<%= hidUserID.ClientID %>").value) {
                document.getElementById("Birthday_Update").style.display = '';
                
            }
            else {
                document.getElementById("Birthday_Update").style.display = 'none';

            }
//           var days = document.getElementById("<%= hidDays.ClientID %>").value;
           // document.getElementById('NewsFrame').src = "PortalSupports/i_news.html";

            table_fill();
        }
        function table_fill() {
            document.getElementById("<%= pnlProfile.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "<div style='width:100%;font-family:cambria;'><table style='width:100%;' class='tble'>";
            tab += "<tr>";
            tab += "<td style='width:40%; border-radius:20px;'>";
            tab += "<img src='" + document.getElementById("<%= hid_image.ClientID %>").value + "' style='border-radius:25px; height:70px; width:70px; border:2px solid #FFF;' ></a></td>";

            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var ShowFlag = 0;
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("^");
                    if (ShowFlag == 0) {
                        ShowFlag = 1;
                        tab += "<td colspan='2' style='width:95%;color:#FF6600; font-weight:bold;font-size:9pt;'>&nbsp;&nbsp;&nbsp;&nbsp; Welcome <b style='font-size:11pt;color:#476C91;'>" + col[1] + "</b>.</td></tr>";
                    }
                    tab += "<tr><td style='width:40%;height:10px; text-align:left;' class='profileHead'>&nbsp;&nbsp;Emp.Code&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                    tab += "<td style='width:57%;height:10px; text-align:left; color:#FF6600; font-weight:bold;' class='profileValue' colspan='2'>&nbsp;&nbsp;" + col[0] + "</td></tr>";

                    tab += "<tr><td style='width:40%;height:10px; text-align:left;' class='profileHead'>&nbsp;&nbsp;Department&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                    tab += "<td style='width:57%;height:10px; text-align:left; color:#FF6600; font-weight:bold;' class='profileValue' colspan='2'>&nbsp;&nbsp;" + col[2] + "</td></tr>";

                    tab += "<tr><td style='width:40%;height:10px; text-align:left;' class='profileHead'>&nbsp;&nbsp;Location&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                    tab += "<td style='width:57%;height:10px; text-align:left; color:#FF6600; font-weight:bold;' class='profileValue' colspan='2'>&nbsp;&nbsp;" + col[3] + "</td></tr>";

                    tab += "<tr><td style='width:40%;height:10px; text-align:left;' class='profileHead'>&nbsp;&nbsp;Designation&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                    tab += "<td style='width:57%;height:10px; text-align:left; color:#FF6600; font-weight:bold;' class='profileValue' colspan='2'>&nbsp;&nbsp;" + col[4] + "</td></tr>";

                    tab += "<tr><td style='width:40%;height:10px; text-align:left;' class='profileHead'>&nbsp;&nbsp;Reporting To&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                    tab += "<td style='width:57%;height:10px; text-align:left; color:#FF6600; font-weight:bold;' class='profileValue' colspan='2'>&nbsp;&nbsp;" + col[5] + "</td></tr>";

                    tab += "<tr><td style='width:40%;height:10px; text-align:left;' class='profileHead'>&nbsp;&nbsp;Reporting E-mail&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                    tab += "<td style='width:57%;height:10px; text-align:left; color:#FF6600; font-weight:bold;' class='profileValue' colspan='2'>&nbsp;&nbsp;" + col[8] + "</td></tr>";

                    tab += "<tr><td style='width:40%;height:10px; text-align:left;' class='profileHead'>&nbsp;&nbsp;Reporting Mobile&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:3%;height:10px; text-align:left;'>:</td>";
                    tab += "<td style='width:57%;height:10px; text-align:left; color:#FF6600; font-weight:bold;' class='profileValue' colspan='2'>&nbsp;&nbsp;" + col[7] + "</td></tr>";

                    tab += "<tr><td style='width:40%;height:10px; text-align:left;' class='profileHead'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:3%;height:10px; text-align:left;'></td>";
                    tab += "<td style='width:57%;height:10px; text-align:left; color:#FF6600; font-weight:bold;' colspan='2'>&nbsp;&nbsp;</td></tr>";

                    tab += "<tr><td style='width:40%;height:10px; text-align:left;' class='profileHead'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    tab += "<td style='width:3%;height:10px; text-align:left;'></td>";
                    tab += "<td style='width:57%;height:10px; text-align:left; color:#FF6600; font-weight:bold;' colspan='2'>&nbsp;&nbsp;<a href='ProfileIndex.aspx?TypeID=1'><b style='font-size:9pt;color:#476C91;'>View More >></b> </a></td></tr>";

                      tab += "<tr><td colspan='3'>&nbsp;&nbsp;&nbsp;</td></tr>";

                }
            }
            tab += "</table></div>";

            document.getElementById("<%= pnlProfile.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//


        }

      
    </script>
</head>
<body onload="pageInit(); width:100%; height:100%">
    <form id="login_form" runat="server">
    <div id="HeadContents" style="width: 100%;">
        <div style="height: 80px; padding-top: 10px; padding-bottom: 10px; float: left; width: 550px;">
            <img src="Image/Logo.png" style="width: 242px; height: 62px;"></div>
        <div style="height: 80px; padding-top: 10px; padding-bottom: 10px; padding-right: 30px;
            text-align: right;">
            <img id="btnSignOut" src="<%= Page.ResolveUrl("~")%>Image/SignOut.png" onclick="window.open('Default.aspx','_self');" />
        </div>
    </div>
     <div style="height:20px; padding-top:0px; background-color:#034EA2; padding-top:10px; padding-bottom:10px;">
           </div>
    <div id="MainContents" style="background-color: White; height: 600px;">
        <div style="float: left; padding-left: 30px; width: 26%;">
            <div class="Box" style="width: 100%;">
                
                <div id="ChairmansMessage">
                   
                </div>
            </div>
        </div>
           <div style="float: left; width: 22%;">
            <div class="Box" style="width: 100%;">
              
                <div id="NewsScroll">
                    <iframe id="NewsFrame" frameborder="0" scrolling="yes" width="280"
                        height="350"></iframe>
                </div>
            </div>
        </div>
        <div style="float: left; width: 25%;">
            <div class="Box" style="width: 100%;">
                <h2>
                    Profile</h2>
                <asp:Panel ID="pnlProfile" runat="server">
                </asp:Panel>
                <div id="Birthday_Update" style="color: #0000FF; display:none;">
                    <asp:ImageButton ID="cmd_Update" runat="server" Height="60px" Width="60px" ImageAlign="AbsMiddle"
                        ImageUrl="~/Image/birthCake.gif" ToolTip="Update Birthday" />&nbsp;&nbsp;Update
                    BirthDay
                </div>
                <br />
                <div id="Branch_Address" style="color: brown; font-family:Arial; display=none;">
                    <asp:ImageButton ID="btnBrAddress" runat="server" Height="13px" Width="50px" ImageAlign="AbsMiddle"
                        ImageUrl="~/Image/UPDATE.gif" ToolTip="Update Branch Address" />&nbsp;&nbsp;<b>Branch Address</b>
                </div>
            </div>
        </div>
        <div id="FooterContainer" style="float: right; padding-top: 10px; width: 22%;">
            <div id="Footer" class="warpper">
                <div id="FooterContent">
                    <div id="QuickLinks">
                        <h3 style="padding-left: 30px;">
                            Quick Links
                        </h3>
                        <ul>
                         <li ><a href="Home.aspx?ModuleID=103">Retail Liabilities</a></li>
                          <li ><a href="Home.aspx?ModuleID=25">ATM Management</a></li>
                            <li ><a href="Home.aspx?ModuleID=3">Audit Management</a></li>
                            <li><a href="Home.aspx?ModuleID=2">HR Management</a></li>
                            <li><a href="Home.aspx?ModuleID=4">HelpDesk Management</a></li>
                            <li><a href="Home.aspx?ModuleID=6">Operations Management</a></li>
                            <li><a href="Home.aspx?ModuleID=21">Cards & Payments</a></li>
                            <li><a href="Home.aspx?ModuleID=10">Legal Documentation</a></li>
                            <li><a href="Home.aspx?ModuleID=23">Business Management</a></li>
                            <li><a href="Home.aspx?ModuleID=100">Information Security</a></li>
                            <li  style="width:auto"><a href="Home.aspx?ModuleID=24">Banking Outlet Commencement</a></li>
                            <li><a href="Home.aspx?ModuleID=101">Customer Service Quality</a></li>
                           <li><a href="Home.aspx?ModuleID=102">Compliance Management</a></li>
                           <li><a href="Home.aspx?ModuleID=104">Transaction Monitoring</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hid_image" runat="server" />
        <asp:HiddenField ID="hid_dtls" runat="server" />
        <asp:HiddenField ID="hidPost" runat="server" />
        <asp:HiddenField ID="hidStaffFlag" runat="server" />
    </div>
    <div id="AddressTabs" style="float: right;">
        <h3 style="color: White">
            Contact Address</h3>
        <div id="TabbedPanels1" class="TabbedPanels">
            <ul class="TabbedPanelsTabGroup">
                <li class="TabbedPanelsTab" tabindex="0">Corporate Office</li>
                <li class="TabbedPanelsTab" tabindex="0">Corporate Annex</li>
                <li class="TabbedPanelsTab" tabindex="0">Registered Office</li>
            </ul>
            <div class="TabbedPanelsContentGroup">
                <div class="TabbedPanelsContent">
                    <strong></strong>Hephzibah Complex,Mannuthy<br />
                    Thrissur – 680651<br />
                </div>
                <div class="TabbedPanelsContent">
                    <strong></strong>Plot No.9,Ram Krishna Society,Gorewada Road,Katol Road P O
                    <br />
                    Nagpur<br />
                </div>
                <div class="TabbedPanelsContent">
                    <strong></strong>#5/A,Fifth Floor,No. 8&9,Gangadeeswara Koil Street Purasawalkam<br />
                    Chennai- 600084<br />
                </div>
            </div>
        </div>
    </div>
    <div style="height: 10px;">
        <asp:HiddenField ID="hidUserID" runat="server" />
        <asp:HiddenField ID="hidPoll" runat="server" />
        <asp:HiddenField ID="hidDays" runat="server" />
    </div>
    <script type="text/javascript">
        var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
    </script>
    </form>
</body>
</html>
</asp:Content>
