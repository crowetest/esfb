﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports iTextSharp.text

Partial Class ViewAttachment
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim RequestID As Integer
    Dim GN As New GeneralFunctions
    Dim DT As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
        cmd_Back.Attributes.Add("onclick", "return Exitform()")
        RequestID = Request.QueryString.Get("RequestID")
        DT = DB.ExecuteDataSet("SELECT ATTACHMENT,content_Type,Attach_file_name FROM DMS_ESFB.dbo.hr_policies where PKID=" & RequestID.ToString() & " and status_id<>0").Tables(0)
        If DT IsNot Nothing Then
            If DT.Rows.Count > 0 Then
                Dim bytes() As Byte = CType(DT.Rows(0)(0), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = DT.Rows(0)(1)
                Response.AddHeader("content-disposition", "inline")
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If
        End If
       
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)

        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
