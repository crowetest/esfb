﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Help_PolicyUpload
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim Dep_Id As Integer
    Dim GF As New GeneralFunctions
    Dim Action_ID As Integer = 0
    Dim Index As Integer = 0
    Dim AttachImage As Integer = 0


#Region "Page Load & Dispose"

    Private Property StrDoc As String

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'If GN.FormAccess(CInt(Session("UserID")), 177) = False Then
        'Response.Redirect("~/AccessDenied.aspx", False)
        'Return
        'End If
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        Me.Master.subtitle = "Policy Document Upload"
        Dim UserID As Integer = CInt(Session("UserID"))
        If Not IsNothing(Request.QueryString("Index")) Then
            Index = CInt(Request.QueryString("Index"))
            Action_ID = CInt(Request.QueryString("Action_ID"))
        End If
        If Not IsPostBack Then
            DT1 = DB.ExecuteDataSet("select Emp_Code from Roles_Assigned where Role_ID=3").Tables(0)
            Dim nCount As Integer = DT1.Rows.Count
            Dim nIndex As Integer = 0
            Dim DeptUser As Boolean = False
            For n As Integer = 0 To DT1.Rows.Count - 1
                'StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString & "µ" & DT.Rows(n)(16).ToString & "µ" & DT.Rows(n)(17).ToString & "µ" & DT.Rows(n)(18).ToString & "µ" & DT.Rows(n)(19).ToString & "µ" & DT.Rows(n)(20).ToString & "µ" & DT.Rows(n)(21).ToString & "µ" & DT.Rows(n)(22).ToString & "µ" & DT.Rows(n)(23).ToString & "µ" & DT.Rows(n)(24).ToString
                If UserID = CInt(DT1.Rows(n)(0)) Then
                    DeptUser = True
                End If
            Next
            If (DeptUser = True) Then
                DT = DB.ExecuteDataSet("select -1 as Dep_ID,' --select--' as Dep_Name Union all select Dep_ID as id,Dep_Name as name from Policy_Heads_Master phm").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select Dep_ID as id,Dep_Name as name from Policy_Heads_Master phm" & _
                                       " LEFT JOIN ROLE_MASTER rm on rm.Role_ID=phm.Associated_Role_ID" & _
                                       " LEFT JOIN ROLES_ASSIGNED ra on ra.Role_ID = rm.Role_ID" & _
                                       " where (ra.Emp_Code = " & UserID & ")").Tables(0)
            End If
            GN.ComboFill(cmbdept, DT, 0, 1)
            If (DT.Rows.Count = 1) Then

                DT = DB.ExecuteDataSet("select -1 as FOLDER_ID,' --select--' as FOLDER_NAME Union all select distinct  a.folder_id as value ,a.folder_name as value from hr_folder a inner join  dms_ESFB.dbo.hr_policies b on a.folder_id = b.folder_id   where a.dept_id= " + DT.Rows(0)(0).ToString + " and status =1").Tables(0)
                GN.ComboFill(cmbfolder, DT, 0, 1)
            End If
            Me.hiddenBit.Value = CStr(-1)
            Me.btnSave.Attributes.Add("onclick", "return saveOnclick()")
            'Me.btnbrowse.Attributes.Add("onclick", "return GoOnclick()")
            'Me.btnGo.Attributes.Add("onclick", "return GoOnclick()")
            hid_Value.Value = ""
            btnbrowse.Visible = False
            chkView.Enabled = False
            chkDownLoad.Enabled = False
            btnCancel.Visible = False
            If Action_ID = 1 Then
                Me.Master.subtitle = "Edit Policy Documents"
                DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,Caption,Description,Dep_ID,Effective_dt from dms_ESFB.dbo .hr_policies where PKID = " & Index & "").Tables(0)
                drpAction.SelectedIndex = Action_ID
                drpAction.Enabled = False
                cmbdept.SelectedValue = DT.Rows(0)(4).ToString()
                cmbdept.Enabled = False
                txtCaption.Text = DT.Rows(0)(2).ToString()
                txtComments.Text = DT.Rows(0)(3).ToString()
                txtEffDate.Text = DT.Rows(0)(5).ToString()
                lblAttach.Visible = True
                fup1.Visible = False
                hid_Value.Value = CStr(Action_ID)
                btnbrowse.Visible = True
                AttachImage = 1
                btnCancel.Visible = True
            End If
        End If
        Me.btnSave.Attributes.Add("onclick", "return saveOnclick()")
        Me.cmbdept.Attributes.Add("onchange", "return DeptOnChange()")
        'Me.txtComments.Attributes.Add("onclick", "return DesOnclick()")
    End Sub

    Protected Sub Help_PolicyUpload_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "confirmation"
    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
        Dim myFile As HttpPostedFile
        Dim FileLen As Integer = 0
        Dim FileName As String = ""
        If fup1.Visible = True Then
            myFile = fup1.PostedFile
            FileLen = myFile.ContentLength
            If (FileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                AttachImg = New Byte(FileLen - 1) {}
                myFile.InputStream.Read(AttachImg, 0, FileLen)
            End If
        End If
        Dim Caption As String = Me.txtCaption.Text
        Dim Descr As String = Me.txtComments.Text
        Dim FOLDER_ID As Integer = CInt(Me.hiddenfolder.Value)
        Dim FLAG_BIT As Integer = CInt(Me.hiddenBit.Value)
        Dim TEXT_DATA As String = Me.txtfolder.Text
        Dim FOL_NAME As String = Me.hid_folder.Value

        ' Dim Dept_Id As Integer = Dep_Id
        Dim Dept_Id As Integer = CInt(Me.cmbdept.SelectedValue)

        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim ViewFlag As Integer
        If Me.chkView.Checked = True And Me.chkDownLoad.Checked = False Then
            ViewFlag = 1
        ElseIf Me.chkView.Checked = False And Me.chkDownLoad.Checked = True Then
            ViewFlag = 2
        ElseIf Me.chkView.Checked = True And Me.chkDownLoad.Checked = True Then
            ViewFlag = 3
        End If
        Dim EffDate As Nullable(Of Date)
        If Me.txtEffDate.Text = "" Then
            EffDate = Nothing
        Else
            EffDate = CDate(Me.txtEffDate.Text)
        End If
        If drpAction.SelectedIndex = 0 Then
            Index = 0
        End If
        Try
            Dim Params(16) As SqlParameter
            Params(0) = New SqlParameter("@ActionID", SqlDbType.Int)
            Params(0).Value = Index
            Params(1) = New SqlParameter("@AttachImage", SqlDbType.Int)
            Params(1).Value = AttachImage
            Params(2) = New SqlParameter("@CaptionName", SqlDbType.VarChar, 100)
            Params(2).Value = Caption
            Params(3) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
            Params(3).Value = AttachImg
            Params(4) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
            Params(4).Value = ContentType
            Params(5) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
            Params(5).Value = FileName
            Params(6) = New SqlParameter("@ViewFlag", SqlDbType.Int)
            Params(6).Value = ViewFlag
            Params(7) = New SqlParameter("@EffDate", SqlDbType.Date)
            Params(7).Value = EffDate
            Params(8) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(8).Value = CInt(Session("UserID"))
            Params(9) = New SqlParameter("@Descr", SqlDbType.VarChar)
            Params(9).Value = Descr
            Params(10) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(10).Direction = ParameterDirection.Output
            Params(11) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(11).Direction = ParameterDirection.Output
            Params(12) = New SqlParameter("@Department", SqlDbType.Int)
            Params(12).Value = Dept_Id
            Params(13) = New SqlParameter("@FOLDER_ID", SqlDbType.Int)
            Params(13).Value = FOLDER_ID
            Params(14) = New SqlParameter("@FLAG_BIT ", SqlDbType.Int)
            Params(14).Value = FLAG_BIT
            Params(15) = New SqlParameter("@TEXT_DATA ", SqlDbType.VarChar, 50)
            Params(15).Value = TEXT_DATA
            Params(16) = New SqlParameter("@FOL_NAME ", SqlDbType.VarChar, 50)
            Params(16).Value = FOL_NAME
            DB.ExecuteNonQuery("SP_HR_POLICY_ATTACH", Params)
            Message = CStr(Params(11).Value)
            ErrorFlag = CInt(Params(10).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "');")
        cl_script1.Append("         window.open('PolicyUpload.aspx','_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""

        If CInt(Data(0)) = 1 Then
            Dim FolderDtl As String = ""
            Dim StrDoc As String = ""
            txtCaption.Text = Data(1)
            'DT = DB.ExecuteDataSet(" select -1 as FOLDER_ID,' ---Select---' as FOLDER_NAME Union select FOLDER_ID as value, FOLDER_NAME from HR_FOLDER where dept_id= " + Data(1)).Tables(0)
            DT = DB.ExecuteDataSet("select -1 as FOLDER_ID,' --select--' as FOLDER_NAME Union all select distinct  a.folder_id as value ,a.folder_name as value from hr_folder a inner join  dms_ESFB.dbo.hr_policies b on a.folder_id = b.folder_id   where a.dept_id= " + Data(1) + " and status =1").Tables(0)
            For Each DR As DataRow In DT.Rows
                FolderDtl += DR(0).ToString() + "µ" + DR(1).ToString() + "¥"
            Next
            CallBackReturn += txtCaption.Text + "Ø" + FolderDtl
        End If
        If CInt(Data(0)) = 2 Then
            txtComments.Text = Data(1)
            CallBackReturn = txtComments.Text
        End If
        If CInt(Data(0)) = 3 Then
            txtEffDate.Text = Data(1)
            CallBackReturn = txtEffDate.Text
        End If
        If CInt(Data(0)) = 4 Then
            Dim Folder As String = CStr(Data(1).ToString)
            Dim Dep As Integer = CInt(Data(2).ToString)
            DT = DB.ExecuteDataSet("delete from HR_FOLDER where FOLDER_ID = " + Folder.ToString() + "and dept_id =" + Dep.ToString() + " and status =1").Tables(0)
        End If
        If CInt(Data(0)) = 5 Then
            Dim Dep As String = CStr(Data(1).ToString)
            Dim Folder As Integer = CInt(Data(2).ToString)
            DT = DB.ExecuteDataSet("select PKID,ATTACHMENT,content_Type,Caption,View_Flag,Description from dms_ESFB.dbo.hr_policies where Dep_ID=" + Data(2).ToString() + "  and FOLDER_ID= " + Data(1).ToString() + " and status_id=1 and effective_dt<=DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)

            For n As Integer = 0 To DT.Rows.Count - 1
                StrDoc += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString
                'If n <= DT.Rows.Count Then
                If n < DT.Rows.Count - 1 Then
                    StrDoc += "¥"
                End If

            Next
            CallBackReturn += StrDoc.ToString
        End If
        If CInt(Data(0)) = 6 Then
            Dim pkid As Integer = CInt(Data(1))
            Dim err As Integer = DB.ExecuteNonQuery("delete from dms_ESFB.dbo.HR_POLICIES where Pkid =" & pkid & " and status_id=1 ")
        End If
    End Sub
    'Protected Sub GoOnclick(sender As Object, e As System.EventArgs) Handles btnGo.Click
    '    Dim nIndex As Integer = CInt(drpAction.SelectedValue)
    '    Dim Dep_ID As Integer = CInt(cmbdept.SelectedValue)
    '    If nIndex <> 0 And Dep_ID <> 0 Then
    '        Response.Redirect("Policy_Edt_Del_Frm.aspx?Dep_ID=" + Dep_ID.ToString + "&Action_ID=" + nIndex.ToString, False)
    '    End If
    'End Sub
    Protected Sub BrowseOnclick(sender As Object, e As System.EventArgs) Handles btnbrowse.Click
        lblAttach.Visible = True
        fup1.Visible = True
        AttachImage = 0
        hid_Value.Value = ""
        btnbrowse.Visible = False
    End Sub
    Protected Sub CancelOnclick(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        btnCancel.Visible = False
        cmbdept.Enabled = True
        drpAction.Enabled = True
        btnbrowse.Visible = False
        fup1.Visible = True
        AttachImage = 0
        hid_Value.Value = ""
        txtCaption.Text = ""
        txtComments.Text = ""
        txtEffDate.Text = ""
        Index = 0
        drpAction.SelectedIndex = 0
        'Me.Master.subtitle = "Edit Policy Documents"
    End Sub
#End Region
End Class
