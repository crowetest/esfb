﻿Imports System.Data

Partial Class HRPolicies
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim RequestID As Integer = 1
        Dim SIno As Integer = 0
        Dim dt As DataTable = DB.ExecuteDataSet("select PKID,ATTACHMENT,content_Type,Caption,View_Flag,Description from dms_ESFB.dbo .hr_policies where status_id=1 and effective_dt<=DATEADD(day, DATEDIFF(day, 0, getdate()), 0)").Tables(0)
        If dt IsNot Nothing Then
            Dim tb As New Table
            tb.Attributes.Add("width", "80%")
            RH.Heading(Session("FirmName").ToString(), tb, "HR POLICIES", 100)

            Dim TRHead As New TableRow
            Dim TRHead_00, TRHead_01, TRHead_02 As New TableCell
            TRHead_00.BackColor = Drawing.Color.WhiteSmoke
            TRHead_01.BackColor = Drawing.Color.WhiteSmoke
            TRHead_02.BackColor = Drawing.Color.WhiteSmoke
            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver



            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "<b>SI No</b>")
            RH.AddColumn(TRHead, TRHead_01, 25, 25, "l", "<b>Caption</b>")

            RH.AddColumn(TRHead, TRHead_02, 70, 70, "l", "<b>Description</b>")
            tb.Controls.Add(TRHead)

            For Each DR As DataRow In dt.Rows
                SIno += 1
                Dim TR As New TableRow
                Dim TR_00, TR_01, TR_02 As New TableCell
                TR_00.BackColor = Drawing.Color.WhiteSmoke
                TR_01.BackColor = Drawing.Color.WhiteSmoke
                TR_02.BackColor = Drawing.Color.WhiteSmoke
                TR_00.BorderWidth = "1"
                TR_01.BorderWidth = "1"
                TR_02.BorderWidth = "1"


                TR_00.BorderColor = Drawing.Color.Silver
                TR_01.BorderColor = Drawing.Color.Silver
                TR_02.BorderColor = Drawing.Color.Silver



                TR_00.BorderStyle = BorderStyle.Solid
                TR_01.BorderStyle = BorderStyle.Solid
                TR_02.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR, TR_00, 5, 5, "c", SIno.ToString())
                If (DR(4) = 1) Then
                    RH.AddColumn(TR, TR_01, 25, 25, "l", "<a href='viewAttachment.aspx?RequestID=" + DR(0).ToString() + " &RptID=1  ' style='cursor:pointer; font-size:12pt;' target='_blank'>" + DR(3) + "</a>")
                Else
                    RH.AddColumn(TR, TR_01, 25, 25, "l", "<a href='ShowAttachment.aspx?RequestID=" + DR(0).ToString() + " &RptID=2 ' style='cursor:pointer; font-size:12pt;'>" + DR(3) + "</a>")
                End If
                RH.AddColumn(TR, TR_02, 70, 70, "l", DR(5).ToString())
                tb.Controls.Add(TR)
            Next
            pnDisplay.Controls.Add(tb)
        End If

    End Sub
End Class
