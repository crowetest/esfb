﻿<%@ Page EnableEventValidation="false" Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="PolicyUpload.aspx.vb" Inherits="Help_PolicyUpload" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <script src="../Script/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var today = new date();
    document.getElementById("<%=hid_date .ClientID %>").value = today;
  
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("¥");
        for (a = 0; a < rows.length-1; a++) {
            var cols = rows[a].split("µ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function Delete_onclick(pid) {
   

    document.getElementById("<%= hid_pkid.ClientID %>").value =pid;
    todata = "6Ø"+pid
    ToServer(todata,4);
        }
    function btnDisplay() {

        document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';

         var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:60%;margin:0px auto;font-family:'cambria';' align='center'>";
        if (document.getElementById("<%= hid_Display.ClientID %>").value != "") {
            row1 = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
            var ShowQuali = 0;
            for (m = 0; m <= row1.length - 1; m++) {
                col2 = row1[m].split("µ");
                if (ShowQuali == 0) {

                    tab += "<tr><td style='width:10%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                    tab += "<td style='width:20%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                    tab += "<td style='width:70%;height:10px; text-align:left;font-weight:bold;' >Description</td></tr>";
                    ShowQuali = 1;

                }

                tab += "<tr><td style='width:10%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                if (col2[4] == 1) {
                    tab += "<td style='width:20%;height:10px; text-align:left;' ><a style='width:50%;'  style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";

                }
                else {
                    tab += "<td style='width:20%;height:10px; text-align:left;' ><a style='width:50%;'  style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";

                }


                tab += "<td style='width:100%;height:10px; text-align:left;' >" + col2[5] + "</td>";
               
                tab +=" &nbsp &nbsp &nbsp<td style = 'width:5%;'><br><input  type ='button' onclick='Delete_onclick("+ col2[0] + ")' id ='btnExpand' value='Delete'></td></tr>";
            }

            tab += "<tr><td colspan='3'></td></tr>";


        }
        tab += "</table></div>";
         document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
    }

    function saveOnclick() {

        var Caption = document.getElementById("<%= txtCaption.ClientID %>").value;
        var Attachment = document.getElementById("<%= fup1.ClientID %>").value;
        var Data = Attachment.split(".");
        var FileType = Data[1];
        var Dep_Id = document.getElementById("<%= cmbdept.ClientID %>").value;
        var EffDate = document.getElementById("<%= txtEffDate.ClientID %>").value;
        var txt_Data = document.getElementById("<%=txtfolder .ClientID %>").value;
        var folderid = document.getElementById("<%=cmbfolder .ClientID %>").value;
        if (Dep_Id == -1) {
                alert("Select department");
                document.getElementById("<%= cmbdept.ClientID %>").focus();
                return false;
            }
        if (folderid == -1 && document.getElementById("<%=hiddenBit.ClientID %>").value !=0  ) {
        alert("Select Folder");
        document.getElementById("<%=cmbfolder .ClientID %>").focus();
                return false;
        }
        else {
            document.getElementById("<%=hiddenfolder .ClientID %>").value = folderid;
        }
        var drop = document.getElementById("<%=cmbfolder.ClientID %>");
        var selectedText = drop.options[drop.selectedIndex].text;
        document.getElementById("<%=hid_folder.ClientID %>").value = selectedText;

        if (document.getElementById("<%=hiddenBit.ClientID %>").value != 1) {
            if (EffDate == "") {
                alert("Select Date");
                document.getElementById("<%= txtEffDate.ClientID %>").focus();
                return false;
            }
            if (Caption == "") {
                alert("Enter Caption");
                document.getElementById("<%= txtCaption.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= hid_Value.ClientID %>").value == 0) {
                if (Attachment == "") {
                    alert("Browse File");
                    document.getElementById("<%= fup1.ClientID %>").focus();
                    return false;
                }
            }
            if (document.getElementById("<%= chkView.ClientID %>").checked == false && document.getElementById("<%= chkDownLoad.ClientID %>").checked == false) {
                alert("Please Check Atleast One Option");
                return false;
            }
             if(document.getElementById("<%=hiddenBit.ClientID %>").value==0){
                if(!(document.getElementById("<%=txtfolder.ClientID %>").value)){
                 alert("Enter Folder Name  ");
                  return false;
                }
            }


        }
        if(document.getElementById("<%=hiddenBit.ClientID %>").value == 1){
                if(!(document.getElementById("<%=txtfolder.ClientID %>").value)){
                 alert("Enter Folder Name  ");
                  return false;
                    }
                }
    }

    function setStartDate(sender, args) {
        sender._startDate = new Date();
    }
    function DeptOnChange() 
    {
            var Dep_ID = document.getElementById("<%= cmbdept.ClientID %>").value;
            var ToData = "1Ø" + Dep_ID  ;
            ToServer(ToData, 1);

    }
    function GoOnclick()
    {        
    var Dep_ID=document.getElementById("<%= cmbdept.ClientID %>").value;
    var folderid = document.getElementById("<%=cmbfolder .ClientID %>").value;
    if(Dep_ID == -1){
    alert("select department");
    document.getElementById("<%=cmbdept .ClientID %>").focus();
    }
    if (folderid < 0){
        alert("select folder");
        document.getElementById("<%=cmbfolder .ClientID %>").focus();
        folderid==null
    }
      if (folderid > 0 ){
        var nIndex = document.getElementById("<%= drpAction.ClientID %>").value
        toData = "5Ø" + folderid + "Ø" + Dep_ID
                ToServer(toData, 3);
      }
      
    }
    function onChangecombo() 
    {
        var nIndex = document.getElementById("<%= drpAction.ClientID %>").value;
           var Dep_ID = document.getElementById("<%= cmbdept.ClientID %>").value;
           if(nIndex == "0") {
                $('.trDrop').show()
                 window.open("PolicyUpload.aspx", "_self");
                 document.getElementById("btnBrowse").style.display = 'none';
                document.getElementById("<%= lbldept.ClientID %>").style.display = '';
                document.getElementById("<%= cmbdept.ClientID %>").style.display = '';
                document.getElementById("<%= lblCaption.ClientID %>").style.display = ''; 
                document.getElementById("<%= txtCaption.ClientID %>").style.display = ''; 
                document.getElementById("<%= lblAttach.ClientID %>").style.display = '';
                document.getElementById("<%= fup1.ClientID %>").style.display = '';
                document.getElementById("<%= lblEffectDate.ClientID %>").style.display = '';
                document.getElementById("<%= DropDownList1.ClientID %>").style.display = ''; 
                document.getElementById("<%= txtEffDate.ClientID %>").style.display = ''; 
                document.getElementById("<%= lbldec.ClientID %>").style.display = ''; 
                document.getElementById("<%= txtComments.ClientID %>").style.display = ''; 
                document.getElementById("<%= chkView.ClientID %>").style.display = '';
                document.getElementById("<%= chkDownLoad.ClientID %>").style.display = ''; 
                document.getElementById("<%= btnSave.ClientID %>").style.display = '';
                document.getElementById("<%= lblcheck.ClientID %>").style.display = '';
                document.getElementById("<%= lblDwnload.ClientID %>").style.display = '';
                document.getElementById("<%= btnGo.ClientID %>").style.display = 'none';
               
               
             }
            else if (nIndex == 1) {

                $('.trDrop').hide();
                document.getElementById("<%= hiddenBit.ClientID %>").value = 2;
                var hidBit = document.getElementById("<%= hiddenBit.ClientID %>").value;
                document.getElementById("<%= lbldept.ClientID %>").style.display = '';
                document.getElementById("<%= btnDelete.ClientID %>").style.display = 'none';
                document.getElementById("<%= cmbdept.ClientID %>").style.display = '';
                document.getElementById("<%= lblCaption.ClientID %>").style.display = 'none'; 
                document.getElementById("<%= txtCaption.ClientID %>").style.display = 'none'; 
                document.getElementById("<%= lblAttach.ClientID %>").style.display = 'none'; 
                document.getElementById("<%= fup1.ClientID %>").style.display = 'none';
                document.getElementById("<%= lblEffectDate.ClientID %>").style.display = 'none';
                document.getElementById("<%= DropDownList1.ClientID %>").style.display = 'none';
                document.getElementById("<%= txtEffDate.ClientID %>").style.display = 'none'; 
                document.getElementById("<%= lbldec.ClientID %>").style.display = 'none'; 
                document.getElementById("<%= txtComments.ClientID %>").style.display = 'none'; 
                document.getElementById("<%= chkView.ClientID %>").style.display = 'none';
                document.getElementById("<%= chkDownLoad.ClientID %>").style.display = 'none';
                document.getElementById("<%= btnSave.ClientID %>").style.display = 'none';
                document.getElementById("<%= lblcheck.ClientID %>").style.display = 'none';
                document.getElementById("<%= lblDwnload.ClientID %>").style.display = 'none';
                document.getElementById("<%= btnGo.ClientID %>").style.display = 'none';
                var Dep_Id = document.getElementById("<%= cmbdept.ClientID %>").value;
                var folderid = document.getElementById("<%=cmbfolder .ClientID %>").value;
                document.getElementById("btnBrowse").style.display = '';
                document.getElementById(" btnExit").style.display = '';
                }

    }
     function FromServer(Arg, Context) {
        
            switch (Context) {

                case 1:
                    {
                        var Data = Arg.split("Ø");
                        ComboFill(Data[1], "<%= cmbfolder.ClientID %>");

                        
                    }  
                case 2:
                    {   

                        break;
                    }
                case 3:
                    {
                        document.getElementById("<%= hid_Display.ClientID %>").value = Arg;
                        btnDisplay();

                        break;
                    }  
               case 4 :
                   {   
                        alert("document deleted successfully");
                         window.open("PolicyUpload.aspx", "_self");
                        break;
                   } 
            }
        }
         function validate_file()
    {
        var fileName = document.getElementById("<%= fup1.ClientID %>").value;
            var file    = document.querySelector('input[type=file]').files[0];
            var size = Math.abs(file.size);

            if (((size/1000)>4000))
            {
                alert("File size should be less than 4 mb");
                document.getElementById("<%= fup1.ClientID %>").value = "";
                document.getElementById("<%= fup1.ClientID %>").focus();
                return false;
            }
          else
            return true;

    }
        function folderonchange() 
            {
            var skillsSelect = document.getElementById("<%= cmbfolder.ClientID %>");
            var selectedText = skillsSelect.options[skillsSelect.selectedIndex].text;
            document.getElementById("<%=txtfolder.ClientID %>").value = selectedText;
            document.getElementById("<%=hid_folder.ClientID %>").value = selectedText;

        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
function generaterow()
            {
                document.getElementById("<%=hiddenBit.ClientID %>").value =0;
               var data = document.getElementById("<%=DropDownList1.ClientID %>").value
               document.getElementById("txtBox").style.display = '';

              if (data == 1) {
               document.getElementById("<%= cmbfolder.ClientID %>").disabled=false;
                  var drop = document.getElementById("<%=cmbfolder.ClientID %>");
                  var selectedText = drop.options[drop.selectedIndex].text;
                  document.getElementById("<%=txtfolder.ClientID %>").value = selectedText;
                  document.getElementById("<%=hiddenBit.ClientID %>").value = 1;
                  document.getElementById("<%= lblEffectDate.ClientID %>").style.display = 'none';
                  document.getElementById("<%= txtEffDate.ClientID %>").style.display = 'none';
                  var display = document.getElementById("<%=hiddenBit.ClientID %>").value;
                  document.getElementById("<%= lblCaption.ClientID %>").style.display = 'none';
                  document.getElementById("<%= txtCaption.ClientID %>").style.display = 'none';
                  document.getElementById("<%= lblAttach.ClientID %>").style.display = 'none';
                  document.getElementById("<%= fup1.ClientID %>").style.display = 'none';
                  document.getElementById("<%= lbldec.ClientID %>").style.display = 'none';
                  document.getElementById("<%= txtComments.ClientID %>").style.display = 'none';
                  document.getElementById("<%= chkView.ClientID %>").style.display = 'none';
                  document.getElementById("<%= chkDownLoad.ClientID %>").style.display = 'none';
                  document.getElementById("<%= lblcheck.ClientID %>").style.display = 'none';
                  document.getElementById("<%= lblDwnload.ClientID %>").style.display = 'none';
                  document.getElementById("<%= btnGo.ClientID %>").style.display = 'none';
//                  if(  document.getElementById("<%= cmbfolder.ClientID %>").value == -1){
//                  alert("Select Folder To be Edited ");
//                  }
              }
              if (data == 0) {
                  document.getElementById("<%=hiddenBit.ClientID %>").value = 0;
                  document.getElementById("<%= cmbfolder.ClientID %>").disabled=true;
                  document.getElementById("txtBox").style.display = '';
                  document.getElementById("<%= lblCaption.ClientID %>").style.display = '';
                  document.getElementById("<%= txtfolder.ClientID %>").value = '';
                  document.getElementById("<%= txtCaption.ClientID %>").style.display = '';
                  document.getElementById("<%= lblAttach.ClientID %>").style.display = '';
                  document.getElementById("<%= fup1.ClientID %>").style.display = '';
                  document.getElementById("<%= lblEffectDate.ClientID %>").style.display = '';
                  document.getElementById("<%= txtEffDate.ClientID %>").style.display = '';
                  document.getElementById("<%= lbldec.ClientID %>").style.display = '';
                  document.getElementById("<%= txtComments.ClientID %>").style.display = '';
                  document.getElementById("<%= chkView.ClientID %>").style.display = '';
                  document.getElementById("<%= chkDownLoad.ClientID %>").style.display = '';
                  document.getElementById("<%= btnSave.ClientID %>").style.display = '';
                  document.getElementById("<%= lblcheck.ClientID %>").style.display = '';
                  document.getElementById("<%= lblDwnload.ClientID %>").style.display = '';
                  document.getElementById("<%= btnGo.ClientID %>").style.display = 'none';
              }
              if (data == -1) {
                  
                  $('.trfol').hide();
                  document.getElementById("<%=hiddenBit.ClientID %>").value = -1;
                   document.getElementById("<%= cmbfolder.ClientID %>").disabled=false;
                   document.getElementById("<%= lblEffectDate.ClientID %>").style.display = '';
                  document.getElementById("<%= txtEffDate.ClientID %>").style.display = '';
                  document.getElementById("<%= lblCaption.ClientID %>").style.display = '';
                  document.getElementById("<%= txtCaption.ClientID %>").style.display = '';
                  document.getElementById("<%= lblAttach.ClientID %>").style.display = '';
                  document.getElementById("<%= fup1.ClientID %>").style.display = '';
                  document.getElementById("<%= lbldec.ClientID %>").style.display = '';
                  document.getElementById("<%= txtfolder.ClientID %>").value = '';
                  document.getElementById("<%= txtComments.ClientID %>").style.display = '';
                  document.getElementById("<%= chkView.ClientID %>").style.display = '';
                  document.getElementById("<%= chkDownLoad.ClientID %>").style.display = '';
                  document.getElementById("<%= btnSave.ClientID %>").style.display = '';
                  document.getElementById("<%= lblcheck.ClientID %>").style.display = '';
                  document.getElementById("<%= lblDwnload.ClientID %>").style.display = '';
                  document.getElementById("<%= btnGo.ClientID %>").style.display = 'none';
              }
            }

</script>
<table class="style1" style="width:60%;margin: 0px auto;" >
           
        <tr id="branch"> <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">&nbsp;</td>
            <td colspan="2">
               <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
            </td>
            
       </tr>
       <tr>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">
                Action</td>
            <td style ="width:25%;">
                <asp:DropDownList ID="drpAction" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="95%" ForeColor="Black" onchange="return onChangecombo()">
                    <asp:ListItem Value="0" Text="Add New Item"></asp:ListItem>
                   <asp:ListItem Value="1" Text="Delete"></asp:ListItem>
                </asp:DropDownList></td>
       </tr>
       <tr>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">
                <asp:label ID="lbldept" Text="Department" runat="server"/></td>
            <td style ="width:25%;">
                <asp:DropDownList ID="cmbdept" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="95%" ForeColor="Black">
                </asp:DropDownList></td>
        </tr>


        <tr>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">
                <asp:label ID="Label1" Text="Folder" runat="server"/></td>
            <td  style ="width:25%;">
                <asp:DropDownList ID="cmbfolder" class="NormalText" runat="server"  Font-Names="Cambria" 
                    Width="95%" ForeColor="Black" onchange="folderonchange()">
                </asp:DropDownList></td>
        </tr>
         <tr class='trDrop'>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">Add/Edit Folder</td>
            <td style ="width:25%;">
                <asp:DropDownList ID="DropDownList1"  class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="95%" ForeColor="Black" onchange="generaterow()">
                     <asp:ListItem  Value="-1" Text="--select--"></asp:ListItem>
                    <asp:ListItem  Value="0" Text="Add"></asp:ListItem>
                    <asp:ListItem  Value="1" Text="Edit"></asp:ListItem>
                    </asp:DropDownList></td>
                    
       </tr>
                 <tr class='trfol' id="txtBox" style="display:none">
               <td style ="width:25%;"></td>
                 <td style ="width:25%;"></td>
            <td  style ="width:25%;">
                <asp:TextBox id="txtfolder" type="text" style="width:95%; font-family: Cambria;" 
                    runat="server" /></td>
            <td style ="width:25%;">
                &nbsp;</td>
                </tr>





         <tr>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">
                 <asp:label ID="lblCaption" Text="Caption" runat="server"/></td>
            <td style ="width:25%;">
                <asp:TextBox id="txtCaption" type="text" style="width:95%; font-family: Cambria;" 
                    runat="server" /></td>
            <td style ="width:25%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style ="width:25%;">
                &nbsp;</td>
            <td >
                 <asp:label ID="lblAttach" Text="Attachment" runat="server"/></td>
            <td style ="width:25%;">
                <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" onchange="validate_file()" Width="95%"/>
                <asp:Button ID="btnbrowse" runat="server" Text="For browsing new file click here" Font-Names="Cambria" style="cursor:pointer;"
                 Width="100%"/></td>
            <td style ="width:25%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">
                 <asp:label ID="lblEffectDate" Text="Effective Date" runat="server"/></td>
            <td style ="width:25%;">
                    <asp:TextBox ID="txtEffDate"  runat="server" class="NormalText" Width="95%"></asp:TextBox> 
                         <asp:CalendarExtender ID="CalendarExtender1" runat="server" 
                        TargetControlID="txtEffDate"  Format="dd/MMM/yyyy" onclientshowing="setStartDate">
                    </asp:CalendarExtender>
                        </td>
            <td style ="width:25%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">
                 <asp:label ID="lbldec" Text="Brief Description" runat="server"/></td></td>
            <td style ="width:25%;">
                <asp:TextBox ID="txtComments" runat="server" class="NormalText" Width="95%" TextMode="MultiLine"></asp:TextBox> 
            </td>
            <td style ="width:25%;">
                &nbsp;</td>
        </tr>
               

        <tr>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">
                    &nbsp;</td>
            <td style ="width:25%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style ="width:25%;">
                &nbsp;</td>
            <td colspan="2" style="text-align:center;">
                <asp:CheckBox ID="chkView" runat="server" Checked="True"/>
                <asp:label ID="lblcheck" Text="View" runat="server"/>
                &nbsp;&nbsp;
                <asp:CheckBox ID="chkDownLoad" runat="server" Checked="True"/>
                <asp:label ID="lblDwnload" Text="DownLoad" runat="server"/>
                </td>
            <td style ="width:25%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">
                &nbsp;</td>
            <td style ="width:25%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style ="width:25%;">
                &nbsp;</td>
            <td colspan="2" style="text-align:center;">

         
          <asp:Button 
                ID="btnDelete" runat="server" Text="browse" Font-Names="Cambria" style="cursor:pointer; display:none"
                 Width="15%"  />
            &nbsp;
            <asp:Button 
                ID="btnGo" runat="server" Text="GO" Font-Names="Cambria" style="cursor:pointer; display:none"
                 Width="15%" />
            &nbsp;
             <input id="btnBrowse" style="font-family: cambria; display:none;    cursor: pointer; width:15%; height: 25px;" 
                type="button" value="BROWSE" onclick="return GoOnclick()" />
            &nbsp;
            <asp:Button 
                ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" style="cursor:pointer;"
                 Width="15%" />
            &nbsp;
  
            <input id="btnExit" style="font-family: cambria;  cursor: pointer; width:15%; height: 25px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
            &nbsp;
             <input id="btnBack" style="font-family: cambria; display:none;  cursor: pointer; width:15%; height: 25px;" 
                type="button"  value="BACK" onclick="window.location.href=window.location.href" />
            &nbsp;
            <asp:Button 
                ID="btnCancel" runat="server" Text="CANCEL" Font-Names="Cambria" style="cursor:pointer;"
                Width="18%" />   
                </td>
            <td style ="width:25%;">
                &nbsp;</td>
        </tr>
        


</table>
  <asp:Panel ID="pnlDtls" runat="server"></asp:Panel>


        <asp:HiddenField ID="hid_Value" runat="server"  />
        <asp:HiddenField ID="hiddenBit" runat="server" />
        <asp:HiddenField ID="hiddenText" runat="server"   />
         <asp:HiddenField ID="hiddenfolder" runat="server" />
         <asp:HiddenField ID="hid_folder" runat="server" />
            <asp:HiddenField ID="hid_date" runat="server" />
            <asp:HiddenField ID="hid_Display" runat="server" />
            <asp:HiddenField ID="hid_pkid" runat="server" />

</asp:Content>

