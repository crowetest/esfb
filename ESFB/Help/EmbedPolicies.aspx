﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="EmbedPolicies.aspx.vb" Inherits="Help_EmbedPolicies" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function showDetails() {
            var PDFURL = "";
            if (document.getElementById("<%= hidRptID.ClientID %>").value == 1)
                PDFURL = "MFCreditPolicy.swf";
            else if (document.getElementById("<%= hidRptID.ClientID %>").value == 2)
                PDFURL = "LOANRESCHEDULINGPOLICYESAF.swf";
            else if (document.getElementById("<%= hidRptID.ClientID %>").value == 3)
                PDFURL = "ClientPrivacyPolicy.swf";
            else if (document.getElementById("<%= hidRptID.ClientID %>").value == 4)
                PDFURL = "ClientProtectionPolicy.swf";
            else if (document.getElementById("<%= hidRptID.ClientID %>").value == 5)
                PDFURL = "GrievanceRedressalPolicy.swf";
            else if (document.getElementById("<%= hidRptID.ClientID %>").value == 6)
                PDFURL = "ITPolicy.swf"
            else if (document.getElementById("<%= hidRptID.ClientID %>").value == 7)
                PDFURL = "ProductGuidlines.swf";
            else if (document.getElementById("<%= hidRptID.ClientID %>").value == 8)
                PDFURL = "EMFILOPMANUEL.swf";


            document.getElementById("emb1").src = PDFURL;
            document.getElementById("emb1").style.height = (screen.height) + 'px';
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <embed id="emb1" style="width:100%"></embed>
        <asp:HiddenField ID="hidRptID" runat="server" />
    </div>
    </form>
</body>
</html>
</asp:Content>