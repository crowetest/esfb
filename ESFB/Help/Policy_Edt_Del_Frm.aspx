﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="Policy_Edt_Del_Frm.aspx.vb" Inherits="Policy_Edt_Del_Frm" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ESAF</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../Style/bootstrap.css" rel="stylesheet" type="text/css" />
     <!-- FONTAWESOME STYLES-->
    <link href="../Style/font-awesome.css" rel="stylesheet" type="text/css" />
     <!-- MORRIS CHART STYLES-->
    <link href="../Style/morris-0.4.3.min.css" rel="stylesheet" type="text/css" />
        <!-- CUSTOM STYLES-->
    <link href="../Style/custom.css" rel="stylesheet" type="text/css" />
     <!-- GOOGLE FONTS-->
    <script src="../../Script/Calculations.js" type="text/javascript"></script>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        .pn1
        {
            margin-top :-350px;
        }
    </style>
  <script language="javascript" type="text/javascript">
      function window_onload() {

          var tab = "<div><br/><br/></div>";
          document.getElementById("<%= pnlImg.ClientID %>").innerHTML = tab;
          document.getElementById("<%= btnSave.ClientID %>").style.display = 'none';
      }

      function table_fill_HR()
      {

          window_onload();
          document.getElementById("lblCaption").innerHTML = 'HR Management';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          
          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>HR Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_HR.ClientID %>").value != "") {            
              row1 = document.getElementById("<%= hid_HR.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {
                      
                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Description</td>";
                      tab += "<td style='width:26%;height:10px; text-align:left;font-weight:bold;' >Select Data</td></tr>";
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;height:10px; text-align:left;'>" + (m+1) + "</td>";
                    if(col2[4]  == 1)
                        tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                    else
                        tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";

                    tab += "<td style='width:30%;height:10px; text-align:left;' >" + col2[5] + "</td>";
                    tab += "<td style='width:26%;text-align:left;'><input type='radio' id='radio_enable_0' name='radioname' onclick='return RadioOnClick(" + col2[0] + ")'></td>";
                  
              }
              
              tab += "<tr><td colspan='3'></td></tr>";

          }
          tab += "</table></div>";

          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
          if (document.getElementById("<%= hid_HR.ClientID %>").value != "")
          {            
              row2 = document.getElementById("<%= hid_HR.ClientID %>").value.split("¥");
              if(row2.length != 0)
              {
                  document.getElementById("<%= btnSave.ClientID %>").style.display = '';
              }
          }
      }
      function table_fill_Risk()
      {

          window_onload();
          document.getElementById("lblCaption").innerHTML = 'Risk Management';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;


          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>Risk Management Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_Risk.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Risk.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Description</td>";
                      tab += "<td style='width:26%;height:10px; text-align:left;font-weight:bold;' >Select Data</td></tr>";
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                  if (col2[4] == 1)
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                  else
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";

                  tab += "<td style='width:30%;height:10px; text-align:left;' >" + col2[5] + "</td>";
                  tab += "<td style='width:26%;text-align:left;'><input type='radio' id='radio_enable_0' name='radioname' onclick='return RadioOnClick(" + col2[0] + ")'></td>";

              }

              tab += "<tr><td colspan='3'></td></tr>";

          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
          if (document.getElementById("<%= hid_Risk.ClientID %>").value != "")
          {            
              row2 = document.getElementById("<%= hid_Risk.ClientID %>").value.split("¥");
              if(row2.length != 0)
              {
                  document.getElementById("<%= btnSave.ClientID %>").style.display = '';
             }
         }
      }
      function table_fill_Audit()
      {
          window_onload();
          document.getElementById("lblCaption").innerHTML = 'Audit Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>Audit Management Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_Audit.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Audit.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Description</td>";
                      tab += "<td style='width:26%;height:10px; text-align:left;font-weight:bold;' >Select Data</td></tr>";
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                  if (col2[4] == 1)
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                  else
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";

                  tab += "<td style='width:30%;height:10px; text-align:left;' >" + col2[5] + "</td>";
                  tab += "<td style='width:26%;text-align:left;'><input type='radio' id='radio_enable_0' name='radioname' onclick='return RadioOnClick(" + col2[0] + ")'></td>";

              }

              tab += "<tr><td colspan='3'></td></tr>";

          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
          if (document.getElementById("<%= hid_Audit.ClientID %>").value != "") {
              row2 = document.getElementById("<%= hid_Audit.ClientID %>").value.split("¥");
              if (row2.length != 0) {
                  document.getElementById("<%= btnSave.ClientID %>").style.display = '';
              }
          }
      }
      
      function table_fill_HD() {

        window_onload();
        document.getElementById("lblCaption").innerHTML = 'HelpDesk Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>HelpDesk Management Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_Hd.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hd.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Description</td>";
                      tab += "<td style='width:26%;height:10px; text-align:left;font-weight:bold;' >Select Data</td></tr>";
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                  if (col2[4] == 1)
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                  else
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";

                  tab += "<td style='width:30%;height:10px; text-align:left;' >" + col2[5] + "</td>";
                  tab += "<td style='width:26%;text-align:left;'><input type='radio' id='radio_enable_0' name='radioname' onclick='return RadioOnClick(" + col2[0] + ")'></td>";

              }

              tab += "<tr><td colspan='3'></td></tr>";

          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
          if (document.getElementById("<%= hid_Hd.ClientID %>").value != "") {
              row2 = document.getElementById("<%= hid_Hd.ClientID %>").value.split("¥");
              if (row2.length != 0) {
                  document.getElementById("<%= btnSave.ClientID %>").style.display = '';
              }
          }
      }
      
      function table_fill_Compliance() {

        window_onload();
        document.getElementById("lblCaption").innerHTML = 'Compliance Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>Compliance Management Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_Com.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Com.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Description</td>";
                      tab += "<td style='width:26%;height:10px; text-align:left;font-weight:bold;' >Select Data</td></tr>";
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                  if (col2[4] == 1)
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                  else
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";

                  tab += "<td style='width:30%;height:10px; text-align:left;' >" + col2[5] + "</td>";
                  tab += "<td style='width:26%;text-align:left;'><input type='radio' id='radio_enable_0' name='radioname' onclick='return RadioOnClick(" + col2[0] + ")'></td>";

              }

              tab += "<tr><td colspan='3'></td></tr>";

          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
          if (document.getElementById("<%= hid_Com.ClientID %>").value != "") {
              row2 = document.getElementById("<%= hid_Com.ClientID %>").value.split("¥");
              if (row2.length != 0) {
                  document.getElementById("<%= btnSave.ClientID %>").style.display = '';
              }
          }
      }

      function table_fill_OP() {
         window_onload();
         document.getElementById("lblCaption").innerHTML = 'Operation Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>OPeration Management Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_Op.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Op.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:30%;height:10px; text-align:left;font-weight:bold;' >Description</td>";
                      tab += "<td style='width:26%;height:10px; text-align:left;font-weight:bold;' >Select Data</td></tr>";
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:14%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                  if (col2[4] == 1)
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                  else
                      tab += "<td style='width:30%;height:10px; text-align:left;' ><a href='ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";

                  tab += "<td style='width:30%;height:10px; text-align:left;' >" + col2[5] + "</td>";
                  tab += "<td style='width:26%;text-align:left;'><input type='radio' id='radio_enable_0' name='radioname' onclick='return RadioOnClick(" + col2[0] + ")'></td>";

              }

              tab += "<tr><td colspan='3'></td></tr>";

          }
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
          if (document.getElementById("<%= hid_Op.ClientID %>").value != "") {
              row2 = document.getElementById("<%= hid_Op.ClientID %>").value.split("¥");
              if (row2.length != 0) {
                  document.getElementById("<%= btnSave.ClientID %>").style.display = '';
              }
          }
      }
      function Foriegn() {

          document.getElementById("lblCaption").innerHTML = 'Foreign Exchange';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";

          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Foreign Exchange  Purchase Application [SFB_05_27_09_17].pdf' target='_blank'><b>Application for purchase of Foreign Exchange</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/currency features.zip' target='_blank'><b>Currency Features</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Cash Memo.pdf' target='_blank'><b>Cash Memo</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Encashment Certificate new.pdf' target='_blank'><b>Encashment Certificate new</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Form_A2.pdf' target='_blank'><b>Form_A2</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/LRS Application cum declaration.pdf' target='_blank'><b>LRS Application cum declaration</b></a></td></tr>";


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function table_fill_NPS() {
          document.getElementById("lblCaption").innerHTML = 'NPS Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          //tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRHelp.pptx' target='_blank'><b>Help</b></a></td></tr>";
          tab += "<tr><td colspan='3'>No files uploaded in NPS Management</td></tr>";


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;


      }
      function table_fill_Election() {
          document.getElementById("lblCaption").innerHTML = 'Election Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          //tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRHelp.pptx' target='_blank'><b>Help</b></a></td></tr>";
          tab += "<tr><td colspan='3'>No files uploaded in Election Management</td></tr>";


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;


      }
      function table_fill_OFMA() {
          document.getElementById("lblCaption").innerHTML = 'OFMA';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/OFMA.pdf' target='_blank'><b>User Manual</b></a></td></tr>";
        


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;


      }
      function table_fill_CompanyPolicies() {
          document.getElementById("lblCaption").innerHTML = 'Company Policies';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=1' target='_blank'><b>Microfinance and Credit Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=2' target='_blank'><b>Loan Rescheduling Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=3' target='_blank'><b>Client Privacy Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=4' target='_blank'><b>Client Protection Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=5' target='_blank'><b>Grievance  Redressal Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=6' target='_blank'><b>I T Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=7' target='_blank'><b>Product Guidelines</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=8' target='_blank'><b>Operational Manual</b></a></td></tr>";

          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function table_fill_LivelihoodSurvey() {
          document.getElementById("lblCaption").innerHTML = 'Livelihood Services';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/LivelihoodSurveyFormat.pdf' target='_blank'><b>Identification Survey Form </b></a></td></tr>";
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function FromServer(arg, context) {

          switch (context) {
              case 1:
                  {
                      document.getElementById("<%= hid_HR.ClientID %>").value = arg;
                      document.getElementById("<%= btnSave.ClientID %>").style.display = '';
                      break;
                  }
              case 2:
                  {
                      document.getElementById("<%= hid_Audit.ClientID %>").value = arg;
                      document.getElementById("<%= btnSave.ClientID %>").style.display = '';
                      break;
                  }
              case 3:
                  {
                      document.getElementById("<%= hid_Hd.ClientID %>").value = arg;
                      document.getElementById("<%= btnSave.ClientID %>").style.display = '';
                      break;
                  }
             case 4:
                  {
                      document.getElementById("<%= hid_Com.ClientID %>").value = arg;
                      document.getElementById("<%= btnSave.ClientID %>").style.display = '';
                      break;
                  }
             case 5:
                  {
                      document.getElementById("<%= hid_Op.ClientID %>").value = arg;
                      document.getElementById("<%= btnSave.ClientID %>").style.display = '';
                      break;
                  }
              case 11:
                  {
                      document.getElementById("<%= hid_Risk.ClientID %>").value = arg;
                      document.getElementById("<%= btnSave.ClientID %>").style.display = '';
                      break;
                  }
              case 12:
                  {
                      var Data = arg.split("|");
                      alert(Data)
                      break;
                  }
          }

      }
      function HR() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 1;
          ToServer("1Ø" , 1);
      }
      function Audit() {
          document.getElementById("<%= hid_Audit.ClientID %>").value = 2;
          ToServer("2Ø", 2);
      }
      function HelpDesk() {
          document.getElementById("<%= hid_Hd.ClientID %>").value = 3;
          ToServer("3Ø", 3);
      }
      function Compliance() {
          document.getElementById("<%= hid_Com.ClientID %>").value = 4;
          ToServer("4Ø", 4);
      }
      function Operations() {
          document.getElementById("<%= hid_Op.ClientID %>").value = 5;
          ToServer("5Ø", 5);
      }
      function NPS() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 6;
          table_fill_NPS();
      }
      function OFMA() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 6;
          table_fill_OFMA();
      }
     

      function Election() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 7;
          table_fill_Election();
      }

      function CompanyPolicies() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 8;
          table_fill_CompanyPolicies();
      }
      function LivelihoodSurvey() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 9;
          table_fill_LivelihoodSurvey();
      }
      function Risk() {
          document.getElementById("<%= hid_Risk.ClientID %>").value = 11;
          ToServer("11Ø", 11);
      }
      function RadioOnClick(ID) {
          document.getElementById("<%= HidSelIndex.ClientID %>").value = ID;
      }
      function saveOnclick() 
      {
       
            if (document.getElementById("<%= HidSelIndex.ClientID %>").value == 0) 
            { 
                alert("Select data"); 
                return false; 
            }
            else
            {
                return true;
            }

    }

    </script>
    </head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            
  <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">&nbsp; <a href="PolicyUpload.aspx" class="btn btn-danger square-btn-adjust">Back</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                <asp:Panel ID="pnlImg" runat="server"></asp:Panel>
                    
					</li>
				
					<div id ="IdHR" runat="server">
                    <li onclick ="HR()"  >
                        <a  href="#"> HR Management <span style="float:right;"> &raquo;</span></a>
                    </li>
                    </div>
                    <div id ="IdAudit" runat="server">
                     <li  onclick ="Audit()" >
                        <a  href="#"> Audit Management <span style="float:right;"> &raquo;</span></a>
                    </li>
                     </div>
                    <div id ="Idhelp" runat="server">
                    <li  onclick ="HelpDesk()" style="top: 2px; left: 0px">
                        <a  href="#"> HelpDesk Management <span style="float:right;"> &raquo;</span></a>
                    </li>
                    </div>
                    <div id ="Idcomp" runat="server">
				    <li  onclick ="Compliance()" >
                        <a   href="#"> Compliance Management <span style="float:right;"> &raquo;</span></a>
                    </li>
                    </div>
                    <div id ="IdOpe" runat="server">	
                     <li  onclick ="Operations()" >
                        <a   href="#"> Operation Management <span style="float:right;"> &raquo;</span></a>
                    </li>
                    </div>	
                    <%--<li  onclick ="NPS()" >
                        <a   href="#"> NPS Management <span style="float:right;"> &raquo;</span></a>
                    </li> 
                    <li  onclick ="Election()" >
                        <a   href="#"> Election Management <span style="float:right;"> &raquo;</span></a>
                    </li> 
                    <li  onclick ="OFMA()" >
                        <a   href="#"> OFMA <span style="float:right;"> &raquo;</span></a>
                    </li> 
                    <li  onclick ="CompanyPolicies()" >
                        <a   href="#"> Company Policies <span style="float:right;"> &raquo;</span></a>
                    </li> 
                     <li  onclick ="LivelihoodSurvey()" >
                        <a   href="#"> Livelihood Services <span style="float:right;"> &raquo;</span></a>
                    </li>--%>
                    <div id ="Idfor" runat="server">	
                    <li  onclick ="Foriegn()" >
                        <a   href="#"> Foreign Exchange <span style="float:right;"> &raquo;</span></a>
                    </li>
                    </div>
                    <div id ="IdRisk" runat="server">
                    <li  onclick ="Risk()" >
                        <a   href="#"> Risk Management <span style="float:right;"> &raquo;</span></a>
                    </li>
                    </div>
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h4><label id="lblCaption" style="font-size:12pt;font-weight:normal;color:#E31E24;"></label><asp:HiddenField ID="hid_image" runat="server" />
                        
                        <asp:HiddenField ID="hid_User" runat="server" />
                        <asp:HiddenField ID="hid_EmpCode" runat="server" />
                        <asp:HiddenField ID="hid_Leave" runat="server" />
                        <asp:HiddenField ID="hid_Promotion" runat="server" />
                        <asp:HiddenField ID="hid_HR" runat="server" />
                        <asp:HiddenField ID="hid_dtls" runat="server" />
                        <asp:HiddenField ID="hdnSubID" runat="server" />
                        <asp:HiddenField ID="hdnFromDt" runat="server" />
                        <asp:HiddenField ID="hdnToDt" runat="server" />
                        
                        <asp:HiddenField ID="hid_Transfer" runat="server" />
                        <asp:HiddenField ID="hid_Type" runat="server" />
                        <asp:HiddenField ID="hid_Job" runat="server" />
                        <asp:HiddenField ID="hid_Compliant" runat="server" />
                        <asp:HiddenField ID="hid_Risk" runat="server" />
                        <asp:HiddenField ID="hid_Audit" runat="server" />
                        <asp:HiddenField ID="hid_Hd" runat="server" />
                        <asp:HiddenField ID="hid_Com" runat="server" />
                        <asp:HiddenField ID="hid_Op" runat="server" />
                        <asp:HiddenField ID="hid_for" runat="server" />
                        <asp:HiddenField ID="HiddenDel" runat="server" />
                        <asp:HiddenField ID="HiddenEd" runat="server" />
                        <asp:HiddenField ID="HidSelIndex" runat="server" />
                        <asp:HiddenField ID="HidActionId" runat="server" />
                        </h4>   
                        
                                               
                <br /><asp:Panel ID="pnlDtls" runat="server"></asp:Panel>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                &nbsp;
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                &nbsp;
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                &nbsp;
                </div>
                </div>         
                <div class="row">
                <div class="col-md-12">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" style="cursor:pointer;"
                Width="15%"/>
                &nbsp;
                </div>
                </div>                  
                 <!-- /. ROW  -->
                  <hr />           
            </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <!-- METISMENU SCRIPTS -->
    </form>
    
   
</body>
</html>
</asp:Content>
