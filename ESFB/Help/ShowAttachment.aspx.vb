﻿Imports System.Data
Imports System.IO

Partial Class ShowAttachment
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RptID As Integer = 1
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim RequestID As Integer = Convert.ToInt32(Request.QueryString.[Get]("RequestID"))
        Dim RptID As Integer = Convert.ToInt32(Request.QueryString.[Get]("RptID"))
        Dim dt As DataTable
        dt = DB.ExecuteDataSet("SELECT ATTACHMENT,content_Type,Attach_file_name FROM DMS_ESFB.dbo.hr_policies where PKID=" & RequestID & " and status_id<>0").Tables(0)

        If dt IsNot Nothing Then
            If RptID = 1 Then
                Dim bytes() As Byte = CType(dt.Rows(0)(0), Byte())
                ' Me.ifm.Attributes("src") = String.Format("pdfdisplay.aspx?ref={0}#toolbar=0", bytes)
                Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                Image1.ImageUrl = "data:image/png;base64," & base64String
                Image1.Visible = True

                ' ifm.Attributes("src") = "data:image/jpg;base64," + Convert.ToBase64String(bytes)
            Else
                If dt.Rows.Count > 0 Then
                    Dim bytes() As Byte = CType(dt.Rows(0)(0), Byte())
                    Response.Buffer = True
                    Response.Charset = ""
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.ContentType = dt.Rows(0)(1).ToString()
                    Response.AddHeader("content-disposition", "attachment;filename=" + dt.Rows(0)(2).ToString().Replace(" ", ""))
                    'Response.AddHeader("content-disposition", dt.Rows(0)(2).ToString()) ' ----------  OPenwith Dialogbox 
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()
                End If

            End If

        End If
    End Sub
End Class
