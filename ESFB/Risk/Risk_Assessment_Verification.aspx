﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    EnableEventValidation="false" CodeFile="Risk_Assessment_Verification.aspx.vb" Inherits="Incident_Verification" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {  
                document.getElementById("rAttach").style.display = "none"; 
                ToServer("1ʘ", 1);
            }
            function RiskOnChange()
            {
                if (document.getElementById("<%= cmbRiskNo.ClientID %>").value == -1)
                {
                    alert("Select Risk Number");
                    document.getElementById("<%= cmbRiskNo.ClientID %>").focus();
                    return false;
                } 
                ToServer("2ʘ" + document.getElementById("<%= cmbRiskNo.ClientID %>").value, 2);
            }
            function setStartDate(sender, args) 
            {
                sender._endDate = new Date();
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {
                        if(Arg != "")
                        {
                            ComboFill(Arg, "<%= cmbRiskNo.ClientID %>");
                        }
                        break;
                    }
                    case 2:
                    {
                        if(Arg != "")
                        {                           
                            var Data = Arg.split("¥");
                            document.getElementById("<%= txtDate.ClientID %>").value=Data[1];
                            document.getElementById("<%= txtTime.ClientID %>").value=Data[2];
                            document.getElementById("<%= txtLocation.ClientID %>").value=Data[3];
                            document.getElementById("<%= txtEmpCode.ClientID %>").value=Data[4];
                            document.getElementById("<%= txtName.ClientID %>").value=Data[5];
                            document.getElementById("<%= txtDept.ClientID %>").value=Data[6];
                            document.getElementById("<%= txtDesig.ClientID %>").value=Data[7];
                            document.getElementById("<%= txtCug.ClientID %>").value=Data[8];
                            document.getElementById("<%= txtEmail.ClientID %>").value=Data[9];
                            document.getElementById("<%= txtRemark.ClientID %>").value=Data[10]; 
                            document.getElementById("<%= txtRequirment.ClientID %>").value=Data[11];
                            document.getElementById("<%= txtInfo.ClientID %>").value=Data[12];
                            document.getElementById("<%= txtRisk.ClientID %>").value=Data[13];
                            document.getElementById("<%= txtSecurity.ClientID %>").value=Data[14]; 
                            document.getElementById("<%= txtCisoCmt.ClientID %>").value=Data[15];
                            if(Data[17] > 0)
                            {
                                document.getElementById("rAttach").style.display = ""; 
                                var Tab = "";
                                Tab += "<div id='ScrollDiv' style='width:100%; height:20px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                                Tab += "<tr class='sub_first';>";
                                Tab += "<td style='width:90%;text-align:left;cursor: pointer;'><a href='ShowRiskFormat.aspx?IncidentID=" + Data[0] + "&IntID=" + Data[17] + "&StatusID=2'>" + Data[16] + "</a></td></tr>";
                                Tab += "</table></div>";
                                document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;
                            }
                            else{
                                document.getElementById("rAttach").style.display = "none"; 
                            }

                        }
                        break;
                    }
                    case 3:
                    {
                        var Data = Arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("Risk_Assessment_Verification.aspx", "_self");
                        break;
                    }
                }
            }

            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        
            function btnSubmit_onclick() { //Approve
                if (document.getElementById("<%= cmbRiskNo.ClientID %>").value == -1)
                {
                    alert("Select Risk No");
                    document.getElementById("<%= cmbRiskNo.ClientID %>").focus();
                    return false;
                }
                var RiskID=document.getElementById("<%= cmbRiskNo.ClientID %>").value
                var Info = document.getElementById("<%= txtInfo.ClientID %>").value;
                var Risk =  document.getElementById("<%= txtRisk.ClientID %>").value; 
                var Security = document.getElementById("<%= txtSecurity.ClientID %>").value;
                var CisoCmt = document.getElementById("<%= txtCisoCmt.ClientID %>").value;                 
                ToServer("3ʘ" + RiskID + "ʘ" + Info + "ʘ" + Risk + "ʘ" + Security + "ʘ" + CisoCmt + "ʘ2", 3);
            }

            function btnDeclined_onclick() { //Decline
                if (document.getElementById("<%= cmbRiskNo.ClientID %>").value == -1)
                {
                    alert("Select Risk No");
                    document.getElementById("<%= cmbRiskNo.ClientID %>").focus();
                    return false;
                }
                var RiskID=document.getElementById("<%= cmbRiskNo.ClientID %>").value
                var Info = document.getElementById("<%= txtInfo.ClientID %>").value;
                var Risk =  document.getElementById("<%= txtRisk.ClientID %>").value; 
                var Security = document.getElementById("<%= txtSecurity.ClientID %>").value;
                var CisoCmt = document.getElementById("<%= txtCisoCmt.ClientID %>").value;                 
                ToServer("3ʘ" + RiskID + "ʘ" + Info + "ʘ" + Risk + "ʘ" + Security + "ʘ" + CisoCmt + "ʘ3", 3);
            }
             function btnonhold_onclick() { //onhold
                if (document.getElementById("<%= cmbRiskNo.ClientID %>").value == -1)
                {
                    alert("Select Risk No");
                    document.getElementById("<%= cmbRiskNo.ClientID %>").focus();
                    return false;
                }
                var RiskID=document.getElementById("<%= cmbRiskNo.ClientID %>").value
                var Info = document.getElementById("<%= txtInfo.ClientID %>").value;
                var Risk =  document.getElementById("<%= txtRisk.ClientID %>").value; 
                var Security = document.getElementById("<%= txtSecurity.ClientID %>").value;
                var CisoCmt = document.getElementById("<%= txtCisoCmt.ClientID %>").value;                 
                ToServer("3ʘ" + RiskID + "ʘ" + Info + "ʘ" + Risk + "ʘ" + Security + "ʘ" + CisoCmt + "ʘ4", 3);
            }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Risk No</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:DropDownList ID="cmbRiskNo" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Date</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtDate" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Time</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtTime" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Location</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtLocation" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Employee Code</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtEmpCode" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Employee Name</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtName" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Employee Department</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtDept" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Designation</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtDesig" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        CUG No.</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtCug" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Official Email ID</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtEmail" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rAttach">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Attachment</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Risk Assessment Request Details
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRemark" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Business Requirement</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRequirment" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Information Security Comments</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtInfo" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Risk Foreseen</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRisk" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Security Controls Suggested</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSecurity" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        CISO Comments, if any</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtCisoCmt" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <input id="btnSubmit"  
                            style="font-family: cambria; cursor: pointer; width: 8%;" type="button" 
                            value="APPROVE" onclick="return btnSubmit_onclick()" />
                        <input id="btnDeclined"  
                            style="font-family: cambria; cursor: pointer; width: 8%;" type="button" 
                            value="DECLINE" onclick="return btnDeclined_onclick()" />
                            <input id="btnOnhold"  
                            style="font-family: cambria; cursor: pointer; width: 8%;" type="button" 
                            value="WORKING" onclick="return btnonhold_onclick()" />
                            &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnIncident" runat="server" />
            <asp:HiddenField ID="hdnDate" runat="server" />
            <asp:HiddenField ID="hdnTime" runat="server" />
            <configuration>
    <system.web>
    <compilaton debug="true" targetFramework="4.0"></compilaton>
    </system.web>
    </configuration>
        </div>
        <br />
    </div>
</asp:Content>
