﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    EnableEventValidation="false" CodeFile="Security_Exception_Request.aspx.vb" Inherits="Security_Exception_Request" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {   
//                document.getElementById("<%= hdnIncident.ClientID %>").value=0;           
//                ToServer("1ʘ", 1);
            }
            function AddDraftDoc() {
                if (!document.getElementById && !document.createElement)
                    return false;
                var fileUploadarea = document.getElementById("fileDraftArea");
                if (!fileUploadarea)
                    return false;
                var newLine = document.createElement("br");
                fileUploadarea.appendChild(newLine);
                var newFile = document.createElement("input");
                newFile.type = "file";
                newFile.setAttribute("class", "fileUpload");

                if (!AddDraftDoc.lastAssignedId)
                    AddDraftDoc.lastAssignedId = 100;
                newFile.setAttribute("id", "FileUpload" + AddDraftDoc.lastAssignedId);
                newFile.setAttribute("name", "FileUpload" + AddDraftDoc.lastAssignedId);
                var div = document.createElement("div");
                div.appendChild(newFile);
                div.setAttribute("id", "div" + AddDraftDoc.lastAssignedId);
                fileUploadarea.appendChild(div);
                AddDraftDoc.lastAssignedId++;
            }
            function EmployeeOnChange()
            {
                if (document.getElementById("<%= txtEmpCode.ClientID %>").value == "")
                {
                    alert("Select Employee Code");
                    document.getElementById("<%= txtEmpCode.ClientID %>").focus();
                    return false;
                } 
                ToServer("1ʘ" + document.getElementById("<%= txtEmpCode.ClientID %>").value, 1);
            }
            function setStartDate(sender, args) 
            {
                sender._endDate = new Date();
            }
            function SaveOnClick() {  
                if (document.getElementById("<%= fupAttachment.ClientID %>").value == "") {
                    alert("Please Upload Documents");
                    document.getElementById("<%= fupAttachment.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtRemark.ClientID %>").value == "")
                {
                    alert("Please enter Description about Incident");
                    document.getElementById("<%= txtRemark.ClientID %>").focus();
                    return false;
                }
                var today=new Date();
              var Hour = today.getHours();
                var Min  = today.getMinutes();
                var Sec  = today.getSeconds();

                var date=today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                document.getElementById("<%= hdnDate.ClientID %>").value=date;

                document.getElementById("<%= hdnTime.ClientID %>").value =  Hour + " : HH " + Min + " : MM " + Sec + " : SS ";
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {
                        if(Arg != "")
                        {
                            var Data = Arg.split("¥");
                            document.getElementById("<%= txtLocation.ClientID %>").value=Data[0];
                            document.getElementById("<%= txtName.ClientID %>").value=Data[1];
                            document.getElementById("<%= txtDept.ClientID %>").value=Data[2];
                            document.getElementById("<%= txtDesig.ClientID %>").value=Data[3];
                            document.getElementById("<%= txtCug.ClientID %>").value=Data[4];
                            document.getElementById("<%= txtEmail.ClientID %>").value=Data[5];
                        }
                        break;
                    }
                }
            }

            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">                
<%--                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Date</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtReadyDt" runat="server" onkeypress="NumericCheck(event)"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="txtReadyDt_CalendarExtender" runat="server" 
                            Format="dd/MMM/yyyy" OnClientShowing="setStartDate" 
                            TargetControlID="txtReadyDt">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Time</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtHour" runat="server" MaxLength=2 Width="26px" onkeypress='return NumericCheck(event)'></asp:TextBox>
                        <asp:Label ID="lblH" runat="server" Text=" : HH "></asp:Label>
                        <asp:TextBox ID="txtMinu" runat="server" MaxLength=2 Width="26px"  onkeypress='return NumericCheck(event)'></asp:TextBox>
                        <asp:Label ID="lblM" runat="server" Text=" : MM "></asp:Label>
                        <asp:TextBox ID="txtSec" runat="server" MaxLength=2 Width="26px"  onkeypress='return NumericCheck(event)'></asp:TextBox>
                        <asp:Label ID="lblS" runat="server" Text=" : SS "></asp:Label>
                        <asp:DropDownList ID="cmbDay" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="9%" ForeColor="Black">
                            <asp:ListItem Value="1">AM</asp:ListItem>
                            <asp:ListItem Value="2">PM</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>--%>                
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        On Behalf of
                        Employee Code</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtEmpCode" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="25%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Employee Name</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtName" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Employee Department</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtDept" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Designation</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtDesig" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Location</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtLocation" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        CUG No.</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtCug" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Official Email ID</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtEmail" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        &nbsp;</td>
                    <td style="width: 50%; text-align: left;">                        
                        &nbsp;</td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        EVP Approval 
                        &amp; Other Attachments<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span>
                    </td>
                    <td style="width: 100%; text-align: left;">
                        <div id="fileDraftArea">
                            <asp:FileUpload ID="fupAttachment" runat="server" CssClass="fileUpload" />
                            <br />
                        </div>
                        <div>
                            <input id="btnDraftDoc" onclick="AddDraftDoc();" style="display: block;" 
                                type="button" value="Add More" /></div>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Security Exception Request Details
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRemark" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Business Requirement</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRequirement" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Time Line For Exception</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtTimeLine" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Width="67px" />
                        &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnIncident" runat="server" />
            <asp:HiddenField ID="hdnDate" runat="server" />
            <asp:HiddenField ID="hdnTime" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
