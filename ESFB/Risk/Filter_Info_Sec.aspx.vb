﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Filter_Info_Sec
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Dim I As Integer
    Dim TrackerID As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim Type As Integer = GF.Decrypt(Request.QueryString.Get("Type"))
            Dim Status As String = GF.Decrypt(Request.QueryString.Get("Status"))

            Dim StrFromDate As String = ""

            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(GF.Decrypt(Request.QueryString.Get("frmdate")))
            End If
            Dim StrToDate As String = ""

            If CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrToDate = CStr(GF.Decrypt(Request.QueryString.Get("todate")))
            End If

            Dim DTT As New DataTable

            Dim Struser As String = Session("UserID").ToString()
            Dim StrSubQry As String = ""
            Dim StrSubQry1 As String = ""
            Dim StrQuery As String = ""

            If Type = 1 Then 'Requested Queue
                If Status = 5 Then 'Open
                    StrQuery += " where a.status_id=1"

                Else
                    StrQuery += " where a.status_id=1"
                End If
            ElseIf Type = 2 Then 'Verified
                If Status = 2 Then 'Approved

                    StrQuery += " and a.status_id=2"
                ElseIf Status = 6 Then 'Working/OnHold
                    StrQuery += " where a.status_id=4"
                Else
                    StrQuery += " where a.status_id in(2,4)"
                End If
                Else
                StrQuery += " where a.status_id in(1,2,4)"
                End If


            DT1 = DB.ExecuteDataSet("select Emp_Code from roles_assigned where emp_code ='" + Struser + "')").Tables(0)
            If (DT1.Rows.Count > 0) Then

                If CStr(Request.QueryString.Get("frmdate")) <> "" And CStr(Request.QueryString.Get("todate")) <> "" Then
                    StrQuery += " and  DATEADD(day,DATEDIFF(day, 0,a.enter_on ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
                End If
            Else
                If CStr(Request.QueryString.Get("frmdate")) <> "" And CStr(Request.QueryString.Get("todate")) <> "" Then
                    StrQuery += "and a.emp_code=" + Struser + " and  DATEADD(day,DATEDIFF(day, 0,a.enter_on ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
                End If

            End If
            If CStr(Request.QueryString.Get("frmdate")) <> "" And CStr(Request.QueryString.Get("todate")) <> "" Then
                StrQuery += " and  DATEADD(day,DATEDIFF(day, 0,a.enter_on ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
            End If

            Dim strTeamQry As String = Nothing

            Dim SqlStr As String

            If Type = 1 Then
                SqlStr = "select a.Incident_No,b.Int_TypeName,replace(convert(char(11),a.Int_DT,113),' ','-'),a.Int_Time,a.Int_Location,a.emp_code,a.emp_name,a.dept_name,a.designation,a.cug_no,a.off_email,a.int_remarks,a.Incident_ID,c.int_id,c.intfile_name,a.enter_by,replace(convert(char(11),a.enter_on,113),' ','-') from risk_security_incident a left join risk_incident_type_master b on a.Int_TypeID=b.Int_TypeID left join risk_incident_attach c on a.Incident_ID=c.Incident_ID " & StrQuery & ""

                DT = DB.ExecuteDataSet(SqlStr).Tables(0)
                Dim b As Integer = DT.Rows.Count

                RH.Heading(Session("FirmName"), tb, "INFORMATION SECURITY INCIDENTS REQUEST", 150)

                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")

                '
                Dim TRSHead As New TableRow
                TRSHead.Width = "100"
                TRSHead.BorderWidth = "1"
                TRSHead.BorderStyle = BorderStyle.Solid
                TRSHead.Style.Add("background-color", "lightsteelblue")

                Dim TRSHead_00 As New TableCell
                TRSHead_00.BorderWidth = "1"
                TRSHead_00.BorderColor = Drawing.Color.Silver
                TRSHead_00.BorderStyle = BorderStyle.Solid
                Dim StrUserNam As String = Nothing

                DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
                If DTT.Rows.Count > 0 Then
                    StrUserNam = DTT.Rows(0)(0)
                End If
                Dim TRHead_1 As New TableRow

                RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)


                RH.BlankRow(tb, 4)
                tb.Controls.Add(TRSHead)


                'TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14, TRHead_1_15 As New TableCell

                TRHead_1_00.BorderWidth = "1"
                TRHead_1_01.BorderWidth = "1"
                TRHead_1_02.BorderWidth = "1"
                TRHead_1_03.BorderWidth = "1"
                TRHead_1_04.BorderWidth = "1"
                TRHead_1_05.BorderWidth = "1"
                TRHead_1_06.BorderWidth = "1"
                TRHead_1_07.BorderWidth = "1"
                TRHead_1_08.BorderWidth = "1"
                TRHead_1_09.BorderWidth = "1"
                TRHead_1_10.BorderWidth = "1"
                TRHead_1_11.BorderWidth = "1"
                TRHead_1_12.BorderWidth = "1"
                TRHead_1_13.BorderWidth = "1"
                TRHead_1_14.BorderWidth = "1"
                TRHead_1_15.BorderWidth = "1"

                TRHead_1_00.BorderColor = Drawing.Color.Silver
                TRHead_1_01.BorderColor = Drawing.Color.Silver
                TRHead_1_02.BorderColor = Drawing.Color.Silver
                TRHead_1_03.BorderColor = Drawing.Color.Silver
                TRHead_1_04.BorderColor = Drawing.Color.Silver
                TRHead_1_05.BorderColor = Drawing.Color.Silver
                TRHead_1_06.BorderColor = Drawing.Color.Silver
                TRHead_1_07.BorderColor = Drawing.Color.Silver
                TRHead_1_08.BorderColor = Drawing.Color.Silver
                TRHead_1_09.BorderColor = Drawing.Color.Silver
                TRHead_1_10.BorderColor = Drawing.Color.Silver
                TRHead_1_11.BorderColor = Drawing.Color.Silver
                TRHead_1_12.BorderColor = Drawing.Color.Silver
                TRHead_1_13.BorderColor = Drawing.Color.Silver
                TRHead_1_14.BorderColor = Drawing.Color.Silver
                TRHead_1_15.BorderColor = Drawing.Color.Silver

                TRHead_1_00.BorderStyle = BorderStyle.Solid
                TRHead_1_01.BorderStyle = BorderStyle.Solid
                TRHead_1_02.BorderStyle = BorderStyle.Solid
                TRHead_1_03.BorderStyle = BorderStyle.Solid
                TRHead_1_04.BorderStyle = BorderStyle.Solid
                TRHead_1_05.BorderStyle = BorderStyle.Solid
                TRHead_1_06.BorderStyle = BorderStyle.Solid
                TRHead_1_07.BorderStyle = BorderStyle.Solid
                TRHead_1_08.BorderStyle = BorderStyle.Solid
                TRHead_1_09.BorderStyle = BorderStyle.Solid
                TRHead_1_10.BorderStyle = BorderStyle.Solid
                TRHead_1_11.BorderStyle = BorderStyle.Solid
                TRHead_1_12.BorderStyle = BorderStyle.Solid
                TRHead_1_13.BorderStyle = BorderStyle.Solid
                TRHead_1_14.BorderStyle = BorderStyle.Solid
                TRHead_1_15.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TRHead_1, TRHead_1_00, 2, 2, "c", "SlNo")
                RH.AddColumn(TRHead_1, TRHead_1_01, 8, 8, "c", "Incident No")
                RH.AddColumn(TRHead_1, TRHead_1_02, 8, 8, "c", "Incident Type")
                RH.AddColumn(TRHead_1, TRHead_1_03, 4, 4, "l", "Date")
                RH.AddColumn(TRHead_1, TRHead_1_04, 8, 8, "l", "Time")
                RH.AddColumn(TRHead_1, TRHead_1_05, 8, 8, "c", "Location")
                RH.AddColumn(TRHead_1, TRHead_1_06, 6, 6, "l", "EmpCode")
                RH.AddColumn(TRHead_1, TRHead_1_07, 6, 6, "l", "Emp Name")
                RH.AddColumn(TRHead_1, TRHead_1_08, 6, 6, "l", "Department")
                RH.AddColumn(TRHead_1, TRHead_1_09, 6, 6, "l", "Designation")
                RH.AddColumn(TRHead_1, TRHead_1_10, 6, 6, "l", "Cug")
                RH.AddColumn(TRHead_1, TRHead_1_11, 8, 8, "l", "Official Mail")
                RH.AddColumn(TRHead_1, TRHead_1_12, 8, 8, "l", "Incident Description")
                RH.AddColumn(TRHead_1, TRHead_1_13, 8, 8, "l", "Attachment")
                RH.AddColumn(TRHead_1, TRHead_1_14, 4, 4, "l", "RequestedBy")
                RH.AddColumn(TRHead_1, TRHead_1_15, 4, 4, "l", "RequestedOn")

                tb.Controls.Add(TRHead_1)
                I = 0
                For Each DR In DT.Rows
                    I = I + 1
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid
                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"
                    TR3_06.BorderWidth = "1"
                    TR3_07.BorderWidth = "1"
                    TR3_08.BorderWidth = "1"
                    TR3_09.BorderWidth = "1"
                    TR3_10.BorderWidth = "1"
                    TR3_11.BorderWidth = "1"
                    TR3_12.BorderWidth = "1"
                    TR3_13.BorderWidth = "1"
                    TR3_14.BorderWidth = "1"
                    TR3_15.BorderWidth = "1"

                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver
                    TR3_06.BorderColor = Drawing.Color.Silver
                    TR3_07.BorderColor = Drawing.Color.Silver
                    TR3_08.BorderColor = Drawing.Color.Silver
                    TR3_09.BorderColor = Drawing.Color.Silver
                    TR3_10.BorderColor = Drawing.Color.Silver
                    TR3_11.BorderColor = Drawing.Color.Silver
                    TR3_12.BorderColor = Drawing.Color.Silver
                    TR3_13.BorderColor = Drawing.Color.Silver
                    TR3_14.BorderColor = Drawing.Color.Silver
                    TR3_15.BorderColor = Drawing.Color.Silver

                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid
                    TR3_06.BorderStyle = BorderStyle.Solid
                    TR3_07.BorderStyle = BorderStyle.Solid
                    TR3_08.BorderStyle = BorderStyle.Solid
                    TR3_09.BorderStyle = BorderStyle.Solid
                    TR3_10.BorderStyle = BorderStyle.Solid
                    TR3_11.BorderStyle = BorderStyle.Solid
                    TR3_12.BorderStyle = BorderStyle.Solid
                    TR3_13.BorderStyle = BorderStyle.Solid
                    TR3_14.BorderStyle = BorderStyle.Solid
                    TR3_15.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TR3, TR3_00, 2, 2, "c", I.ToString())
                    RH.AddColumn(TR3, TR3_01, 8, 8, "c", DR(0).ToString())
                    RH.AddColumn(TR3, TR3_02, 8, 8, "c", DR(1).ToString())
                    RH.AddColumn(TR3, TR3_03, 4, 4, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_04, 8, 8, "l", DR(3).ToString())
                    RH.AddColumn(TR3, TR3_05, 8, 8, "c", DR(4).ToString())
                    RH.AddColumn(TR3, TR3_06, 6, 6, "l", DR(5).ToString())
                    RH.AddColumn(TR3, TR3_07, 6, 6, "l", DR(6).ToString())
                    RH.AddColumn(TR3, TR3_08, 6, 6, "l", DR(7).ToString())
                    RH.AddColumn(TR3, TR3_09, 6, 6, "l", DR(8).ToString())
                    RH.AddColumn(TR3, TR3_10, 6, 6, "l", DR(9).ToString())
                    RH.AddColumn(TR3, TR3_11, 8, 8, "l", DR(10).ToString())
                    RH.AddColumn(TR3, TR3_12, 8, 8, "l", DR(11).ToString())
                    RH.AddColumn(TR3, TR3_13, 8, 8, "l", "<a href='ShowRiskFormat.aspx?IncidentID=" + DR(12).ToString() + "&IntID=" + DR(13).ToString() + "&StatusID=1' style='text-align:right;' target='_blank'>" + DR(14).ToString() + "</a>")
                    RH.AddColumn(TR3, TR3_14, 4, 4, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_15, 4, 4, "l", DR(16).ToString())

                    tb.Controls.Add(TR3)

                Next
                RH.BlankRow(tb, 30)
                pnDisplay.Controls.Add(tb)
            Else
                SqlStr = "select a.Incident_No,b.Int_TypeName,replace(convert(char(11),a.Int_DT,113),' ','-'),a.Int_Time,a.Int_Location,a.emp_code,a.emp_name,a.dept_name,a.designation,a.cug_no,a.off_email,a.int_remarks,a.Incident_ID,c.int_id,c.intfile_name,a.enter_by,replace(convert(char(11),a.enter_on,113),' ','-'),a.int_priority,a.init_action,a.business_imp,a.analysis_dtl,a.recovery_action,a.root_cause,a.prevention_action,a.lesson_learnt,a.disp_action,a.verify_by,replace(convert(char(11),a.verify_on,113),' ','-') from risk_security_incident a left join risk_incident_type_master b  on a.Int_TypeID=b.Int_TypeID left join risk_incident_attach c on a.Incident_ID=c.Incident_ID " & StrQuery & ""

                DT = DB.ExecuteDataSet(SqlStr).Tables(0)
                Dim b As Integer = DT.Rows.Count

                RH.Heading(Session("FirmName"), tb, "INFORMATION SECURITY INCIDENTS", 150)

                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")

                '
                Dim TRSHead As New TableRow
                TRSHead.Width = "100"
                TRSHead.BorderWidth = "1"
                TRSHead.BorderStyle = BorderStyle.Solid
                TRSHead.Style.Add("background-color", "lightsteelblue")

                Dim TRSHead_00 As New TableCell
                TRSHead_00.BorderWidth = "1"
                TRSHead_00.BorderColor = Drawing.Color.Silver
                TRSHead_00.BorderStyle = BorderStyle.Solid
                Dim StrUserNam As String = Nothing

                DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
                If DTT.Rows.Count > 0 Then
                    StrUserNam = DTT.Rows(0)(0)
                End If
                Dim TRHead_1 As New TableRow

                RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)


                RH.BlankRow(tb, 4)
                tb.Controls.Add(TRSHead)


                'TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14, TRHead_1_15, TRHead_1_16, TRHead_1_17, TRHead_1_18, TRHead_1_19, TRHead_1_20, TRHead_1_21, TRHead_1_22, TRHead_1_23, TRHead_1_24, TRHead_1_25, TRHead_1_26 As New TableCell

                TRHead_1_00.BorderWidth = "1"
                TRHead_1_01.BorderWidth = "1"
                TRHead_1_02.BorderWidth = "1"
                TRHead_1_03.BorderWidth = "1"
                TRHead_1_04.BorderWidth = "1"
                TRHead_1_05.BorderWidth = "1"
                TRHead_1_06.BorderWidth = "1"
                TRHead_1_07.BorderWidth = "1"
                TRHead_1_08.BorderWidth = "1"
                TRHead_1_09.BorderWidth = "1"
                TRHead_1_10.BorderWidth = "1"
                TRHead_1_11.BorderWidth = "1"
                TRHead_1_12.BorderWidth = "1"
                TRHead_1_13.BorderWidth = "1"
                TRHead_1_14.BorderWidth = "1"
                TRHead_1_15.BorderWidth = "1"
                TRHead_1_16.BorderWidth = "1"
                TRHead_1_17.BorderWidth = "1"
                TRHead_1_18.BorderWidth = "1"
                TRHead_1_19.BorderWidth = "1"
                TRHead_1_20.BorderWidth = "1"
                TRHead_1_21.BorderWidth = "1"
                TRHead_1_22.BorderWidth = "1"
                TRHead_1_23.BorderWidth = "1"
                TRHead_1_24.BorderWidth = "1"
                TRHead_1_25.BorderWidth = "1"
                TRHead_1_26.BorderWidth = "1"

                TRHead_1_00.BorderColor = Drawing.Color.Silver
                TRHead_1_01.BorderColor = Drawing.Color.Silver
                TRHead_1_02.BorderColor = Drawing.Color.Silver
                TRHead_1_03.BorderColor = Drawing.Color.Silver
                TRHead_1_04.BorderColor = Drawing.Color.Silver
                TRHead_1_05.BorderColor = Drawing.Color.Silver
                TRHead_1_06.BorderColor = Drawing.Color.Silver
                TRHead_1_07.BorderColor = Drawing.Color.Silver
                TRHead_1_08.BorderColor = Drawing.Color.Silver
                TRHead_1_09.BorderColor = Drawing.Color.Silver
                TRHead_1_10.BorderColor = Drawing.Color.Silver
                TRHead_1_11.BorderColor = Drawing.Color.Silver
                TRHead_1_12.BorderColor = Drawing.Color.Silver
                TRHead_1_13.BorderColor = Drawing.Color.Silver
                TRHead_1_14.BorderColor = Drawing.Color.Silver
                TRHead_1_15.BorderColor = Drawing.Color.Silver
                TRHead_1_16.BorderColor = Drawing.Color.Silver
                TRHead_1_17.BorderColor = Drawing.Color.Silver
                TRHead_1_18.BorderColor = Drawing.Color.Silver
                TRHead_1_19.BorderColor = Drawing.Color.Silver
                TRHead_1_20.BorderColor = Drawing.Color.Silver
                TRHead_1_21.BorderColor = Drawing.Color.Silver
                TRHead_1_22.BorderColor = Drawing.Color.Silver
                TRHead_1_23.BorderColor = Drawing.Color.Silver
                TRHead_1_24.BorderColor = Drawing.Color.Silver
                TRHead_1_25.BorderColor = Drawing.Color.Silver
                TRHead_1_26.BorderColor = Drawing.Color.Silver

                TRHead_1_00.BorderStyle = BorderStyle.Solid
                TRHead_1_01.BorderStyle = BorderStyle.Solid
                TRHead_1_02.BorderStyle = BorderStyle.Solid
                TRHead_1_03.BorderStyle = BorderStyle.Solid
                TRHead_1_04.BorderStyle = BorderStyle.Solid
                TRHead_1_05.BorderStyle = BorderStyle.Solid
                TRHead_1_06.BorderStyle = BorderStyle.Solid
                TRHead_1_07.BorderStyle = BorderStyle.Solid
                TRHead_1_08.BorderStyle = BorderStyle.Solid
                TRHead_1_09.BorderStyle = BorderStyle.Solid
                TRHead_1_10.BorderStyle = BorderStyle.Solid
                TRHead_1_11.BorderStyle = BorderStyle.Solid
                TRHead_1_12.BorderStyle = BorderStyle.Solid
                TRHead_1_13.BorderStyle = BorderStyle.Solid
                TRHead_1_14.BorderStyle = BorderStyle.Solid
                TRHead_1_15.BorderStyle = BorderStyle.Solid
                TRHead_1_16.BorderStyle = BorderStyle.Solid
                TRHead_1_17.BorderStyle = BorderStyle.Solid
                TRHead_1_18.BorderStyle = BorderStyle.Solid
                TRHead_1_19.BorderStyle = BorderStyle.Solid
                TRHead_1_20.BorderStyle = BorderStyle.Solid
                TRHead_1_21.BorderStyle = BorderStyle.Solid
                TRHead_1_22.BorderStyle = BorderStyle.Solid
                TRHead_1_23.BorderStyle = BorderStyle.Solid
                TRHead_1_24.BorderStyle = BorderStyle.Solid
                TRHead_1_25.BorderStyle = BorderStyle.Solid
                TRHead_1_26.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TRHead_1, TRHead_1_00, 1, 1, "c", "SlNo")
                RH.AddColumn(TRHead_1, TRHead_1_01, 4, 4, "c", "Incident No")
                RH.AddColumn(TRHead_1, TRHead_1_02, 4, 4, "c", "Incident Type")
                RH.AddColumn(TRHead_1, TRHead_1_03, 3, 3, "l", "Date")
                RH.AddColumn(TRHead_1, TRHead_1_04, 3, 3, "l", "Time")
                RH.AddColumn(TRHead_1, TRHead_1_05, 4, 4, "c", "Location")
                RH.AddColumn(TRHead_1, TRHead_1_06, 4, 4, "l", "EmpCode")
                RH.AddColumn(TRHead_1, TRHead_1_07, 4, 4, "l", "Emp Name")
                RH.AddColumn(TRHead_1, TRHead_1_08, 4, 4, "l", "Department")
                RH.AddColumn(TRHead_1, TRHead_1_09, 4, 4, "l", "Designation")
                RH.AddColumn(TRHead_1, TRHead_1_10, 4, 4, "l", "Cug")
                RH.AddColumn(TRHead_1, TRHead_1_11, 4, 4, "l", "Official Mail")
                RH.AddColumn(TRHead_1, TRHead_1_12, 4, 4, "l", "Incident Description")
                RH.AddColumn(TRHead_1, TRHead_1_13, 4, 4, "l", "Attachment")
                RH.AddColumn(TRHead_1, TRHead_1_14, 4, 4, "l", "RequestBy")
                RH.AddColumn(TRHead_1, TRHead_1_15, 4, 4, "l", "RequestOn")
                RH.AddColumn(TRHead_1, TRHead_1_16, 2, 2, "l", "Priority")
                RH.AddColumn(TRHead_1, TRHead_1_17, 4, 4, "l", "Initial Action")
                RH.AddColumn(TRHead_1, TRHead_1_18, 4, 4, "l", "Business Impact")
                RH.AddColumn(TRHead_1, TRHead_1_19, 4, 4, "l", "Detailed Analysis")
                RH.AddColumn(TRHead_1, TRHead_1_20, 4, 4, "l", "Recovery Action")
                RH.AddColumn(TRHead_1, TRHead_1_21, 4, 4, "l", "Root Cause")
                RH.AddColumn(TRHead_1, TRHead_1_22, 4, 4, "l", "Preventive Action")
                RH.AddColumn(TRHead_1, TRHead_1_23, 4, 4, "l", "Lesson Learnt")
                RH.AddColumn(TRHead_1, TRHead_1_24, 4, 4, "l", "Disciplinary Action")
                RH.AddColumn(TRHead_1, TRHead_1_25, 3, 3, "l", "Verified By")
                RH.AddColumn(TRHead_1, TRHead_1_26, 4, 4, "l", "Verified On")

                tb.Controls.Add(TRHead_1)
                I = 0
                For Each DR In DT.Rows
                    I = I + 1
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid
                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16, TR3_17, TR3_18, TR3_19, TR3_20, TR3_21, TR3_22, TR3_23, TR3_24, TR3_25, TR3_26 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"
                    TR3_06.BorderWidth = "1"
                    TR3_07.BorderWidth = "1"
                    TR3_08.BorderWidth = "1"
                    TR3_09.BorderWidth = "1"
                    TR3_10.BorderWidth = "1"
                    TR3_11.BorderWidth = "1"
                    TR3_12.BorderWidth = "1"
                    TR3_13.BorderWidth = "1"
                    TR3_14.BorderWidth = "1"
                    TR3_15.BorderWidth = "1"
                    TR3_16.BorderWidth = "1"
                    TR3_17.BorderWidth = "1"
                    TR3_18.BorderWidth = "1"
                    TR3_19.BorderWidth = "1"
                    TR3_20.BorderWidth = "1"
                    TR3_21.BorderWidth = "1"
                    TR3_22.BorderWidth = "1"
                    TR3_23.BorderWidth = "1"
                    TR3_24.BorderWidth = "1"
                    TR3_25.BorderWidth = "1"
                    TR3_26.BorderWidth = "1"

                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver
                    TR3_06.BorderColor = Drawing.Color.Silver
                    TR3_07.BorderColor = Drawing.Color.Silver
                    TR3_08.BorderColor = Drawing.Color.Silver
                    TR3_09.BorderColor = Drawing.Color.Silver
                    TR3_10.BorderColor = Drawing.Color.Silver
                    TR3_11.BorderColor = Drawing.Color.Silver
                    TR3_12.BorderColor = Drawing.Color.Silver
                    TR3_13.BorderColor = Drawing.Color.Silver
                    TR3_14.BorderColor = Drawing.Color.Silver
                    TR3_15.BorderColor = Drawing.Color.Silver
                    TR3_16.BorderColor = Drawing.Color.Silver
                    TR3_17.BorderColor = Drawing.Color.Silver
                    TR3_18.BorderColor = Drawing.Color.Silver
                    TR3_19.BorderColor = Drawing.Color.Silver
                    TR3_20.BorderColor = Drawing.Color.Silver
                    TR3_21.BorderColor = Drawing.Color.Silver
                    TR3_22.BorderColor = Drawing.Color.Silver
                    TR3_23.BorderColor = Drawing.Color.Silver
                    TR3_24.BorderColor = Drawing.Color.Silver
                    TR3_25.BorderColor = Drawing.Color.Silver
                    TR3_26.BorderColor = Drawing.Color.Silver

                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid
                    TR3_06.BorderStyle = BorderStyle.Solid
                    TR3_07.BorderStyle = BorderStyle.Solid
                    TR3_08.BorderStyle = BorderStyle.Solid
                    TR3_09.BorderStyle = BorderStyle.Solid
                    TR3_10.BorderStyle = BorderStyle.Solid
                    TR3_11.BorderStyle = BorderStyle.Solid
                    TR3_12.BorderStyle = BorderStyle.Solid
                    TR3_13.BorderStyle = BorderStyle.Solid
                    TR3_14.BorderStyle = BorderStyle.Solid
                    TR3_15.BorderStyle = BorderStyle.Solid
                    TR3_16.BorderStyle = BorderStyle.Solid
                    TR3_17.BorderStyle = BorderStyle.Solid
                    TR3_18.BorderStyle = BorderStyle.Solid
                    TR3_19.BorderStyle = BorderStyle.Solid
                    TR3_20.BorderStyle = BorderStyle.Solid
                    TR3_21.BorderStyle = BorderStyle.Solid
                    TR3_22.BorderStyle = BorderStyle.Solid
                    TR3_23.BorderStyle = BorderStyle.Solid
                    TR3_24.BorderStyle = BorderStyle.Solid
                    TR3_25.BorderStyle = BorderStyle.Solid
                    TR3_26.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TR3, TR3_00, 1, 1, "c", I.ToString())
                    RH.AddColumn(TR3, TR3_01, 4, 4, "c", DR(0).ToString())
                    RH.AddColumn(TR3, TR3_02, 4, 4, "c", DR(1).ToString())
                    RH.AddColumn(TR3, TR3_03, 3, 3, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_04, 3, 3, "l", DR(3).ToString())
                    RH.AddColumn(TR3, TR3_05, 4, 4, "c", DR(4).ToString())
                    RH.AddColumn(TR3, TR3_06, 4, 4, "l", DR(5).ToString())
                    RH.AddColumn(TR3, TR3_07, 4, 4, "l", DR(6).ToString())
                    RH.AddColumn(TR3, TR3_08, 4, 4, "l", DR(7).ToString())
                    RH.AddColumn(TR3, TR3_09, 4, 4, "l", DR(8).ToString())
                    RH.AddColumn(TR3, TR3_10, 4, 4, "l", DR(9).ToString())
                    RH.AddColumn(TR3, TR3_11, 4, 4, "l", DR(10).ToString())
                    RH.AddColumn(TR3, TR3_12, 4, 4, "l", DR(11).ToString())
                    RH.AddColumn(TR3, TR3_13, 4, 4, "l", "<a href='ShowRiskFormat.aspx?IncidentID=" + DR(12).ToString() + "&IntID=" + DR(13).ToString() + "&StatusID=1' style='text-align:right;' target='_blank'>" + DR(14).ToString() + "</a>")
                    RH.AddColumn(TR3, TR3_14, 4, 4, "l", DR(15).ToString())
                    RH.AddColumn(TR3, TR3_15, 4, 4, "l", DR(16).ToString())
                    RH.AddColumn(TR3, TR3_16, 2, 2, "l", DR(17).ToString())
                    RH.AddColumn(TR3, TR3_17, 4, 4, "l", DR(18).ToString())
                    RH.AddColumn(TR3, TR3_18, 4, 4, "l", DR(19).ToString())
                    RH.AddColumn(TR3, TR3_19, 4, 4, "l", DR(20).ToString())
                    RH.AddColumn(TR3, TR3_20, 4, 4, "l", DR(21).ToString())
                    RH.AddColumn(TR3, TR3_21, 4, 4, "l", DR(22).ToString())
                    RH.AddColumn(TR3, TR3_22, 4, 4, "l", DR(23).ToString())
                    RH.AddColumn(TR3, TR3_23, 4, 4, "l", DR(24).ToString())
                    RH.AddColumn(TR3, TR3_24, 4, 4, "l", DR(25).ToString())
                    RH.AddColumn(TR3, TR3_25, 3, 3, "l", DR(26).ToString())
                    RH.AddColumn(TR3, TR3_26, 4, 4, "l", DR(27).ToString())

                    tb.Controls.Add(TR3)

                Next
                RH.BlankRow(tb, 30)
                pnDisplay.Controls.Add(tb)
            End If

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        'Try
        WebTools.ExporttoExcel(DT, "Incidents")
        'Catch ex As Exception
        '    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
        '    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        'End Try
    End Sub
End Class
