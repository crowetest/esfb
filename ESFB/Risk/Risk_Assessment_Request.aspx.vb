﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Risk_Assessment_Request
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim DT6 As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Risk Assessment Request"
        Dim UserID As String = CStr(Session("UserID"))

        DT6 = DB.ExecuteDataSet("select Emp_Code from emp_master where emp_code in (select emp_code from emp_master where Cadre_ID <=10 and emp_code='" + UserID + "')").Tables(0)
        If (DT6.Rows.Count > 0) Then
            If GF.FormAccess(CInt(Session("UserID")), 1330) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

        Else

            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        If Not IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        End If
        Me.btnSave.Attributes.Add("onclick", "return SaveOnClick()")
        Me.txtEmpCode.Attributes.Add("onchange", "return EmployeeOnChange()")
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
             Case 1 'Fill Employee Details
                DT = GF.GetQueryResult("select b.Branch_Name,a.Emp_Name,c.Department_Name,d.Designation_Name,p.Cug_No,p.Official_mail_id from EMP_MASTER a left join Emp_Profile p on a.Emp_Code=p.Emp_Code, BRANCH_MASTER b, DEPARTMENT_MASTER c, DESIGNATION_MASTER d where a.Branch_ID=b.Branch_ID and a.Department_ID=c.Department_ID and a.Designation_ID=d.Designation_ID and a.Emp_Code=" & CInt(Data(1)) & " and a.Branch_ID=" & CInt(BranchID) & "")
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "¥" + DT.Rows(0)(1).ToString() + "¥" + DT.Rows(0)(2).ToString() + "¥" + DT.Rows(0)(3).ToString() + "¥" + DT.Rows(0)(4).ToString() + "¥" + DT.Rows(0)(5).ToString()
                End If
        End Select
    End Sub
#End Region

#Region "Confirm"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim AppTrackerID As Integer = 0

        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
        Dim FileName As String = ""
        Dim myFile As HttpPostedFile = fupBrd.PostedFile
        Dim nFileLen As Integer = myFile.ContentLength
        If (nFileLen > 0) Then
            ContentType = myFile.ContentType
            FileName = myFile.FileName
            AttachImg = New Byte(nFileLen - 1) {}
            myFile.InputStream.Read(AttachImg, 0, nFileLen)
        End If

        Try

            Dim Remark As String = CStr(Me.txtRemark.Text)
            Dim Requirement As String = CStr(Me.txtRequirement.Text)
            Dim ReadyDt As Date = CDate(Me.hdnDate.Value)
            Dim EmpCode As Integer = CInt(Me.txtEmpCode.Text)
            Dim RiskTime As String = CStr(Me.hdnTime.Value)
            Dim Cug As String = CStr(Me.txtCug.Text)
            Dim Email As String = CStr(Me.txtEmail.Text)

            Dim Params(13) As SqlParameter
            Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(0).Value = BranchID
            Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(1).Value = UserID
            Params(2) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(2).Value = EmpCode
            Params(3) = New SqlParameter("@RiskDate", SqlDbType.Date)
            Params(3).Value = ReadyDt
            Params(4) = New SqlParameter("@RiskTime", SqlDbType.VarChar, 30)
            Params(4).Value = RiskTime
            Params(5) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
            Params(5).Value = Remark
            Params(6) = New SqlParameter("@Requirement", SqlDbType.VarChar, 500)
            Params(6).Value = Requirement
            Params(7) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
            Params(7).Value = AttachImg
            Params(8) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
            Params(8).Value = ContentType
            Params(9) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
            Params(9).Value = FileName
            Params(10) = New SqlParameter("@Cug", SqlDbType.VarChar, 100)
            Params(10).Value = Cug
            Params(11) = New SqlParameter("@Email", SqlDbType.VarChar, 100)
            Params(11).Value = Email
            Params(12) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(12).Direction = ParameterDirection.Output
            Params(13) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(13).Direction = ParameterDirection.Output
            DB.ExecuteNonQuery("SP_RISK_ASSESSMENT_REQUEST", Params)
            ErrorFlag = CInt(Params(12).Value)
            Message = CStr(Params(13).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "'); ")
        cl_script1.Append("        window.open('Risk_Assessment_Request.aspx', '_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        If ErrorFlag = 0 Then
            initializeControls()
        End If

    End Sub
    Private Sub initializeControls()
        txtRemark.Text = ""
    End Sub
#End Region

End Class
