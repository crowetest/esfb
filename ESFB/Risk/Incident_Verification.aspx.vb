﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Incident_Verification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Information Security Incident Verification"

        If GF.FormAccess(CInt(Session("UserID")), 1332) = False And CInt(Session("BranchID")) <> 0 Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        If Not IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        End If

        Me.cmbIncidentNo.Attributes.Add("onchange", "return IncidentOnChange()")

    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Incident Type
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select incident_id,incident_no from risk_security_incident where status_id=1 or status_id=4")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2 'Fill Employee Details
                Dim InstNo As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select a.incident_id,a.Int_TypeID,replace(convert(char(11),Int_DT,113),' ','-'),a.Int_Time,a.Int_Location,a.emp_code,a.emp_name,a.dept_name,a.designation,a.cug_no,a.off_email,a.int_remarks,b.intfile_name, b.int_id,a.int_priority_id,a.init_action,a.business_imp,a.analysis_dtl,a.recovery_action,a.root_cause,a.prevention_action,a.lesson_learnt,a.disp_action from risk_security_incident a left join risk_incident_attach b on a.Incident_ID=b.Incident_ID where  a.Incident_ID=" & InstNo & "")
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "¥" + DT.Rows(0)(1).ToString() + "¥" + DT.Rows(0)(2).ToString() + "¥" + DT.Rows(0)(3).ToString() + "¥" + DT.Rows(0)(4).ToString() + "¥" + DT.Rows(0)(5).ToString() + "¥" + DT.Rows(0)(6).ToString() + "¥" + DT.Rows(0)(7).ToString() + "¥" + DT.Rows(0)(8).ToString() + "¥" + DT.Rows(0)(9).ToString() + "¥" + DT.Rows(0)(10).ToString() + "¥" + DT.Rows(0)(11).ToString() + "¥" + DT.Rows(0)(12).ToString() + "¥" + DT.Rows(0)(13).ToString() +
                     "¥" + DT.Rows(0)(14).ToString() + "¥" + DT.Rows(0)(15).ToString() + "¥" + DT.Rows(0)(16).ToString() + "¥" + DT.Rows(0)(17).ToString() + "¥" + DT.Rows(0)(18).ToString() + "¥" + DT.Rows(0)(19).ToString() + "¥" + DT.Rows(0)(20).ToString() + "¥" + DT.Rows(0)(21).ToString() + "¥" + DT.Rows(0)(22).ToString()
                End If
                CallBackReturn += "£"
                DT = GF.GetQueryResult("select Int_TypeID,Int_TypeName from risk_incident_type_master where Int_TypeID=" & CInt(DT.Rows(0)(1).ToString()) & " ")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 3
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try

                    Dim Params(13) As SqlParameter
                    Params(0) = New SqlParameter("@IntID", SqlDbType.Int)
                    Params(0).Value = CInt(Data(1))
                    Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(1).Value = UserID
                    Params(2) = New SqlParameter("@PriorityID", SqlDbType.Int)
                    Params(2).Value = CInt(Data(2))
                    Params(3) = New SqlParameter("@Action", SqlDbType.VarChar, 1000)
                    Params(3).Value = CStr(Data(3))
                    Params(4) = New SqlParameter("@Impact", SqlDbType.VarChar, 1000)
                    Params(4).Value = CStr(Data(4))
                    Params(5) = New SqlParameter("@Analysis", SqlDbType.VarChar, 1000)
                    Params(5).Value = CStr(Data(5))
                    Params(6) = New SqlParameter("@Recovery", SqlDbType.VarChar, 1000)
                    Params(6).Value = CStr(Data(6))
                    Params(7) = New SqlParameter("@RootCause", SqlDbType.VarChar, 1000)
                    Params(7).Value = CStr(Data(7))
                    Params(8) = New SqlParameter("@Prevent", SqlDbType.VarChar, 1000)
                    Params(8).Value = CStr(Data(8))
                    Params(9) = New SqlParameter("@Lesson", SqlDbType.VarChar, 1000)
                    Params(9).Value = CStr(Data(9))
                    Params(10) = New SqlParameter("@Displ", SqlDbType.VarChar, 1000)
                    Params(10).Value = CStr(Data(10))
                    Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(11).Direction = ParameterDirection.Output
                    Params(12) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(12).Direction = ParameterDirection.Output
                    Params(13) = New SqlParameter("@status_id", SqlDbType.Int)
                    Params(13).Value = CInt(Data(11))
                    DB.ExecuteNonQuery("SP_RISK_INCIDENT_VERIFY", Params)
                    ErrorFlag = CInt(Params(11).Value)
                    Message = CStr(Params(12).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End Select
    End Sub
#End Region


End Class
