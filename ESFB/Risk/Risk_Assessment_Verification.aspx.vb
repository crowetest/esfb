﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Incident_Verification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Risk Assessment Verification"

        If GF.FormAccess(CInt(Session("UserID")), 1334) = False And CInt(Session("BranchID")) <> 0 Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        If Not IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        End If

        Me.cmbRiskNo.Attributes.Add("onchange", "return RiskOnChange()")

    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Incident Type
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select risk_id,risk_no from risk_assessment_dtl where status_id=1 or status_id=4")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2 'Fill Employee Details
                Dim RiskID As Integer = CInt(Data(1))
                DT = GF.GetQueryResult("select a.Risk_ID,replace(convert(char(11),a.Risk_DT,113),' ','-'),a.Risk_Time,a.Risk_Location,a.emp_code,a.emp_name,a.dept_name,a.designation,a.cug_no,a.off_email,a.Risk_Assessment,a.Bus_Requirement,a.Info_Sec,a.Risk_fore,a.Sec_Controls,a.CISO_Comments,b.riskfile_name,isnull(b.risk_attach_id,0) AttID from risk_assessment_dtl a left join risk_assessment_attach b on a.Risk_ID=b.Risk_ID where a.Risk_ID=" & RiskID & "")
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "¥" + DT.Rows(0)(1).ToString() + "¥" + DT.Rows(0)(2).ToString() + "¥" + DT.Rows(0)(3).ToString() + "¥" + DT.Rows(0)(4).ToString() + "¥" + DT.Rows(0)(5).ToString() + "¥" + DT.Rows(0)(6).ToString() + "¥" + DT.Rows(0)(7).ToString() + "¥" + DT.Rows(0)(8).ToString() + "¥" + DT.Rows(0)(9).ToString() + "¥" + DT.Rows(0)(10).ToString() + "¥" + DT.Rows(0)(11).ToString() + "¥" + DT.Rows(0)(12).ToString() + "¥" + DT.Rows(0)(13).ToString() + "¥" + DT.Rows(0)(14).ToString() + "¥" + DT.Rows(0)(15).ToString() + "¥" + DT.Rows(0)(16).ToString() + "¥" + DT.Rows(0)(17).ToString()
                End If
            Case 3
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(8) As SqlParameter
                    Params(0) = New SqlParameter("@RiskID", SqlDbType.Int)
                    Params(0).Value = CInt(Data(1))
                    Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(1).Value = UserID
                    Params(2) = New SqlParameter("@Info", SqlDbType.VarChar, 1000)
                    Params(2).Value = CStr(Data(2))
                    Params(3) = New SqlParameter("@Risk", SqlDbType.VarChar, 1000)
                    Params(3).Value = CStr(Data(3))
                    Params(4) = New SqlParameter("@Security", SqlDbType.VarChar, 1000)
                    Params(4).Value = CStr(Data(4))
                    Params(5) = New SqlParameter("@CisoCmt", SqlDbType.VarChar, 1000)
                    Params(5).Value = CStr(Data(5))
                    Params(6) = New SqlParameter("@CisoStatus", SqlDbType.Int)
                    Params(6).Value = CInt(Data(6))
                    Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(7).Direction = ParameterDirection.Output
                    Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(8).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_RISK_ASSESSMENT_VERIFY", Params)
                    ErrorFlag = CInt(Params(7).Value)
                    Message = CStr(Params(8).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                End Try
                CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End Select
    End Sub
#End Region


End Class
