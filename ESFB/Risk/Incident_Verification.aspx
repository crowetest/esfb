﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    EnableEventValidation="false" CodeFile="Incident_Verification.aspx.vb" Inherits="Incident_Verification" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {   
                document.getElementById("<%= hdnIncident.ClientID %>").value=0;           
                document.getElementById("<%= cmbIncident.ClientID %>").disabled=true;
                document.getElementById("rAttach").style.display = "none";
                ToServer("1ʘ", 1);
            }
            function IncidentOnChange()
            {
                if (document.getElementById("<%= cmbIncidentNo.ClientID %>").value == -1)
                {
                    alert("Select Incident Number");
                    document.getElementById("<%= cmbIncidentNo.ClientID %>").focus();
                    return false;
                } 
                ToServer("2ʘ" + document.getElementById("<%= cmbIncidentNo.ClientID %>").value, 2);
            }
            function setStartDate(sender, args) 
            {
                sender._endDate = new Date();
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {
                        if(Arg != "")
                        {
                            ComboFill(Arg, "<%= cmbIncidentNo.ClientID %>");
                        }
                        break;
                    }
                    case 2:
                    {
                        if(Arg != "")
                        {
                            var Args = Arg.split("£"); 
                            var Data = Args[0].split("¥");
                            document.getElementById("<%= txtDate.ClientID %>").value=Data[2];
                            document.getElementById("<%= txtTime.ClientID %>").value=Data[3];
                            document.getElementById("<%= txtLocation.ClientID %>").value=Data[4];
                            document.getElementById("<%= txtEmpCode.ClientID %>").value=Data[5];
                            document.getElementById("<%= txtName.ClientID %>").value=Data[6];
                            document.getElementById("<%= txtDept.ClientID %>").value=Data[7];
                            document.getElementById("<%= txtDesig.ClientID %>").value=Data[8];
                            document.getElementById("<%= txtCug.ClientID %>").value=Data[9];
                            document.getElementById("<%= txtEmail.ClientID %>").value=Data[10];
                            document.getElementById("<%= txtRemark.ClientID %>").value=Data[11];
                            document.getElementById("<%= cmbPriority.ClientID %>").value=Data[14];
                            document.getElementById("<%= txtAction.ClientID %>").value=Data[15];
                            document.getElementById("<%= txtImpact.ClientID %>").value=Data[16];
                            document.getElementById("<%= txtAnalysis.ClientID %>").value=Data[17];
                            document.getElementById("<%= txtRecovery.ClientID %>").value=Data[18];
                            document.getElementById("<%= txtRoot.ClientID %>").value=Data[19];
                            document.getElementById("<%= txtPrevent.ClientID %>").value=Data[20];
                            document.getElementById("<%= txtLesson.ClientID %>").value=Data[21];
                            document.getElementById("<%= txtDispl.ClientID %>").value=Data[22];
                            if(Data[13] > 0)
                            {
                                document.getElementById("rAttach").style.display = "";
                                var Tab = "";
                                Tab += "<div id='ScrollDiv' style='width:100%; height:20px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
                                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                                Tab += "<tr class='sub_first';>";
                                Tab += "<td style='width:90%;text-align:left;cursor: pointer;'><a href='ShowRiskFormat.aspx?IncidentID=" + Data[0] + "&IntID=" + Data[13] + "&StatusID=1'>" + Data[12] + "</a></td></tr>";
                                Tab += "</table></div>";
                                document.getElementById("<%= pnlDoc.ClientID %>").innerHTML = Tab;
                            }
                            else{
                                document.getElementById("rAttach").style.display = "none";
                            }

                            ComboFill(Args[1], "<%= cmbIncident.ClientID %>");

                            var IntDtl = Args[1].split("Ř");
                            var cols = IntDtl[1].split("Ĉ");

                            document.getElementById("<%= cmbIncident.ClientID %>").value=cols[0];
                        }
                        break;
                    }
                    case 3:
                    {
                        var Data = Arg.split("Ø");
                        alert(Data[1]);
                        if (Data[0] == 0) window.open("Incident_Verification.aspx", "_self");
                        break;
                    }
                }
            }

            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        
            function btnSubmit_onclick() {
                if (document.getElementById("<%= cmbIncidentNo.ClientID %>").value == -1)
                {
                    alert("Select Incident Type");
                    document.getElementById("<%= cmbIncident.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= cmbPriority.ClientID %>").value == -1)
                {
                    alert("Select Priority");
                    document.getElementById("<%= cmbPriority.ClientID %>").focus();
                    return false;
                } 
                var IncidentNo = document.getElementById("<%= cmbIncidentNo.ClientID %>").value;
                var Priority = document.getElementById("<%= cmbPriority.ClientID %>").value;
                var Action =  document.getElementById("<%= txtAction.ClientID %>").value; 
                var Impact = document.getElementById("<%= txtImpact.ClientID %>").value;
                var Analysis = document.getElementById("<%= txtAnalysis.ClientID %>").value; 
                var Recovery = document.getElementById("<%= txtRecovery.ClientID %>").value; 
                var RootCause = document.getElementById("<%= txtRoot.ClientID %>").value;  
                var Prevent = document.getElementById("<%= txtPrevent.ClientID %>").value; 
                var Lesson = document.getElementById("<%= txtLesson.ClientID %>").value; 
                var Displ = document.getElementById("<%= txtDispl.ClientID %>").value; 
                ToServer("3ʘ" + IncidentNo + "ʘ" + Priority + "ʘ" + Action + "ʘ" + Impact + "ʘ" + Analysis + "ʘ" + Recovery + "ʘ" + RootCause + "ʘ" + Prevent + "ʘ" + Lesson + "ʘ" + Displ + "ʘ2", 3);
            }
             function btnonhold_onclick() {
                if (document.getElementById("<%= cmbIncidentNo.ClientID %>").value == -1)
                {
                    alert("Select Incident Type");
                    document.getElementById("<%= cmbIncident.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%= cmbPriority.ClientID %>").value == -1)
                {
                    alert("Select Priority");
                    document.getElementById("<%= cmbIncident.ClientID %>").focus();
                    return false;
                } 
                var IncidentNo = document.getElementById("<%= cmbIncidentNo.ClientID %>").value;
                var Priority = document.getElementById("<%= cmbPriority.ClientID %>").value;
                var Action =  document.getElementById("<%= txtAction.ClientID %>").value; 
                var Impact = document.getElementById("<%= txtImpact.ClientID %>").value;
                var Analysis = document.getElementById("<%= txtAnalysis.ClientID %>").value; 
                var Recovery = document.getElementById("<%= txtRecovery.ClientID %>").value; 
                var RootCause = document.getElementById("<%= txtRoot.ClientID %>").value;  
                var Prevent = document.getElementById("<%= txtPrevent.ClientID %>").value; 
                var Lesson = document.getElementById("<%= txtLesson.ClientID %>").value; 
                var Displ = document.getElementById("<%= txtDispl.ClientID %>").value; 
                ToServer("3ʘ" + IncidentNo + "ʘ" + Priority + "ʘ" + Action + "ʘ" + Impact + "ʘ" + Analysis + "ʘ" + Recovery + "ʘ" + RootCause + "ʘ" + Prevent + "ʘ" + Lesson + "ʘ" + Displ + "ʘ4", 3);
            }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Incident No</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:DropDownList ID="cmbIncidentNo" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Incident Type</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:DropDownList ID="cmbIncident" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Date of Incident</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtDate" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Time of Incident</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtTime" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Incident Location</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtLocation" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr >
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Employee Code</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtEmpCode" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Employee Name</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtName" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Employee Department</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtDept" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Designation</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtDesig" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        CUG No.</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtCug" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Official Email ID</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:TextBox ID="txtEmail" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr id="rAttach">
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Incident Attachment</td>
                    <td style="width: 50%; text-align: left;">                        
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        Description about the Incident
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRemark" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)'  Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Priority</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbPriority" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                            <asp:ListItem Value="1">LOW</asp:ListItem>
                            <asp:ListItem Value="2">MEDIUM</asp:ListItem>
                            <asp:ListItem Value="3">HIGH</asp:ListItem>
                            <asp:ListItem Value="4">CRITICAL</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Initial Action (Containment action)</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtAction" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Business Impact, if any</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtImpact" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Detailed analysis Details</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtAnalysis" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Recovery Action Taken</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRecovery" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Root Cause</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRoot" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Preventive Action, If any</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtPrevent" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Lessons Learnt</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtLesson" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        &nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        Disciplinary Action, if any</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtDispl" runat="server" class="NormalText" Style="font-family: Cambria;
                            font-size: 10pt; resize: none;" Width="100%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <input id="btnSubmit"  
                            style="font-family: cambria; cursor: pointer; width: 6%;" type="button" 
                            value="SUBMIT" onclick="return btnSubmit_onclick()" />
                            <input id="btnonhold"  
                            style="font-family: cambria; cursor: pointer; width: 8%;" type="button" 
                            value="WORKING" onclick="return btnonhold_onclick()" />
                            &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnIncident" runat="server" />
            <asp:HiddenField ID="hdnDate" runat="server" />
            <asp:HiddenField ID="hdnTime" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
