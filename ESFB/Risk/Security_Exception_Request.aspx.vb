﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Security_Exception_Request
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim DT6 As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Security Exception Request"
        Dim UserID As String = CStr(Session("UserID"))

        DT6 = DB.ExecuteDataSet("select Emp_Code from emp_master where emp_code in (select emp_code from emp_master where Cadre_ID <=10 and emp_code='" + UserID + "')").Tables(0)
        If (DT6.Rows.Count > 0) Then
            If GF.FormAccess(CInt(Session("UserID")), 1330) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

        Else

            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        If Not IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        End If
        Me.btnSave.Attributes.Add("onclick", "return SaveOnClick()")
        Me.txtEmpCode.Attributes.Add("onchange", "return EmployeeOnChange()")
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Employee Details
                DT = GF.GetQueryResult("select b.Branch_Name,a.Emp_Name,c.Department_Name,d.Designation_Name,p.Cug_No,p.Official_mail_id from EMP_MASTER a left join Emp_Profile p on a.Emp_Code=p.Emp_Code, BRANCH_MASTER b, DEPARTMENT_MASTER c, DESIGNATION_MASTER d where a.Branch_ID=b.Branch_ID and a.Department_ID=c.Department_ID and a.Designation_ID=d.Designation_ID and a.Emp_Code=" & CInt(Data(1)) & " and a.Branch_ID=" & CInt(BranchID) & "")
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "¥" + DT.Rows(0)(1).ToString() + "¥" + DT.Rows(0)(2).ToString() + "¥" + DT.Rows(0)(3).ToString() + "¥" + DT.Rows(0)(4).ToString() + "¥" + DT.Rows(0)(5).ToString()
                End If
        End Select
    End Sub
#End Region

#Region "Confirm"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim SecID As Integer = 0
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim AppTrackerID As Integer = 0

        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
        Dim hfc As HttpFileCollection
        hfc = Request.Files

        For i = 0 To hfc.Count - 1
            Dim myFile As HttpPostedFile = hfc(i)
            Dim nFileLen As Integer = myFile.ContentLength
            Dim FileName As String = ""
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                Dim FileLength As Integer = CInt(myFile.ContentLength) 'Convert into bytes
                If FileLength > 4000000 Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('Please Check the Size of attached file');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
                Dim FileExtension As String = Path.GetExtension(myFile.FileName)
                If Not (FileExtension = ".xls" Or FileExtension = ".xlsx" Or FileExtension = ".jpg" Or FileExtension = ".jpeg" Or FileExtension = ".doc" Or FileExtension = ".docx" Or FileExtension = ".zip" Or FileExtension = ".pdf" Or FileExtension = ".PDF" Or FileExtension = ".XLS" Or FileExtension = ".XLSX" Or FileExtension = ".JPG" Or FileExtension = ".JPEG" Or FileExtension = ".DOC" Or FileExtension = ".DOCX" Or FileExtension = ".ZIP") Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
            End If
        Next
        Try
            Dim Remark As String = CStr(Me.txtRemark.Text)
            Dim Requirement As String = CStr(Me.txtRequirement.Text)
            Dim ReadyDt As Date = CDate(Me.hdnDate.Value)
            Dim EmpCode As Integer = CInt(Me.txtEmpCode.Text)
            Dim SecTime As String = CStr(Me.hdnTime.Value)
            Dim Cug As String = CStr(Me.txtCug.Text)
            Dim Email As String = CStr(Me.txtEmail.Text)
            Dim TimeLine As String = CStr(Me.txtTimeLine.Text)

            Dim Params(12) As SqlParameter
            Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(0).Value = BranchID
            Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(1).Value = UserID
            Params(2) = New SqlParameter("@EmpCode", SqlDbType.Int)
            Params(2).Value = EmpCode
            Params(3) = New SqlParameter("@Cug", SqlDbType.VarChar, 100)
            Params(3).Value = Cug
            Params(4) = New SqlParameter("@Email", SqlDbType.VarChar, 100)
            Params(4).Value = Email
            Params(5) = New SqlParameter("@SecDate", SqlDbType.Date)
            Params(5).Value = ReadyDt
            Params(6) = New SqlParameter("@SecTime", SqlDbType.VarChar, 30)
            Params(6).Value = SecTime
            Params(7) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
            Params(7).Value = Remark
            Params(8) = New SqlParameter("@Requirement", SqlDbType.VarChar, 500)
            Params(8).Value = Requirement
            Params(9) = New SqlParameter("@TimeLine", SqlDbType.VarChar, 500)
            Params(9).Value = TimeLine
            Params(10) = New SqlParameter("@SecID", SqlDbType.Int)
            Params(10).Direction = ParameterDirection.Output
            Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(11).Direction = ParameterDirection.Output
            Params(12) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(12).Direction = ParameterDirection.Output
            DB.ExecuteNonQuery("SP_RISK_SECURITY_REQUEST", Params)
            ErrorFlag = CInt(Params(11).Value)
            Message = CStr(Params(12).Value)
            SecID = CInt(Params(10).Value)
            If SecID > 0 And ErrorFlag = 0 And hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)

                        Dim Param(5) As SqlParameter
                        Param(0) = New SqlParameter("@SecID", SqlDbType.Int)
                        Param(0).Value = SecID
                        Param(1) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
                        Param(1).Value = AttachImg
                        Param(2) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                        Param(2).Value = ContentType
                        Param(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                        Param(3).Value = FileName
                        Param(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Param(4).Direction = ParameterDirection.Output
                        Param(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Param(5).Direction = ParameterDirection.Output
                        DB.ExecuteNonQuery("SP_RISK_EXCEPTION_ATTACH", Param)
                    End If
                Next
            End If
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "'); ")
        cl_script1.Append("        window.open('Security_Exception_Request.aspx', '_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        If ErrorFlag = 0 Then
            initializeControls()
        End If

    End Sub
    Private Sub initializeControls()
        txtRemark.Text = ""
    End Sub
#End Region

End Class
