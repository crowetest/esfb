﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Filter_Risk_Assessment
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT, DT2, DT1 As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Dim I As Integer
    Dim TrackerID As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim Type As Integer = GF.Decrypt(Request.QueryString.Get("Type"))
            Dim Status As String = GF.Decrypt(Request.QueryString.Get("Status"))

            Dim StrFromDate As String = ""

            If CStr(Request.QueryString.Get("frmdate")) <> "1" Then
                StrFromDate = CStr(GF.Decrypt(Request.QueryString.Get("frmdate")))
            End If
            Dim StrToDate As String = ""

            If CStr(Request.QueryString.Get("todate")) <> "1" Then
                StrToDate = CStr(GF.Decrypt(Request.QueryString.Get("todate")))
            End If

            Dim DTT As New DataTable

            Dim Struser As String = Session("UserID").ToString()
            Dim StrSubQry As String = ""
            Dim StrSubQry1 As String = ""
            Dim StrQuery As String = ""

            StrQuery += "where "
            If Type = 1 Then 'Requested Queue
                If Status = 5 Then 'Open
                    StrQuery += "  a.status_id=1"
                Else

                    StrQuery += "  a.status_id=1"
                End If


            ElseIf Type = 2 Then 'Verified
                If Status = 2 Then 'Approved
                    StrQuery += "  a.status_id=2"
                ElseIf Status = 3 Then ' Declined
                    StrQuery += "  a.status_id=3"
                ElseIf Status = 6 Then 'Working/OnHold
                    StrQuery += "  a.status_id=4"
                ElseIf Status = 7 Then 'Closed
                    StrQuery += "  a.status_id in(2,3)"
                Else
                    StrQuery += "  a.status_id in(2,3,4)"
                End If
            Else
                StrQuery += "  a.status_id in(1,2,3,4)"
            End If


            DT1 = DB.ExecuteDataSet("select Emp_Code from roles_assigned where emp_code ='" + Struser + "')").Tables(0)
            If (DT1.Rows.Count > 0) Then

                If CStr(Request.QueryString.Get("frmdate")) <> "" And CStr(Request.QueryString.Get("todate")) <> "" Then
                    StrQuery += " and  DATEADD(day,DATEDIFF(day, 0,a.enter_on ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
                End If
            Else
                If CStr(Request.QueryString.Get("frmdate")) <> "" And CStr(Request.QueryString.Get("todate")) <> "" Then
                    StrQuery += "and a.emp_code=" + Struser + " and  DATEADD(day,DATEDIFF(day, 0,a.enter_on ),0)  between '" & CDate(StrFromDate).ToString("MM/dd/yyyy") & "'  and '" & CDate(StrToDate).ToString("MM/dd/yyyy") & "'"
                End If

            End If
            Dim strTeamQry As String = Nothing

            Dim SqlStr As String

            If Type = 1 Then

                SqlStr = "select a.Sec_No,a.Sec_DT,a.Sec_Time,a.Sec_Location,a.emp_name +' (' + convert(varchar,a.emp_code) + ')' OnBehalfOf,a.dept_name,a.designation,a.cug_no,a.off_email,a.Sec_Exception,a.Bus_Requirement,a.Sec_TimeLine,a.enter_by,replace(convert(char(11),a.enter_on,113),' ','-'),a.Sec_ID,(SELECT Stuff((select convert(varchar,b.Sec_Attach_id) + '®' + b.secfile_name + '¥' AS 'data()' from security_exception_attach b where b.Sec_ID=a.Sec_ID FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,0,N'')) from security_exception_dtl a " & StrQuery & ""

                DT = DB.ExecuteDataSet(SqlStr).Tables(0)

                Dim b As Integer = DT.Rows.Count

                RH.Heading(Session("FirmName"), tb, "SECURITY EXCEPTIONS", 150)

                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")

                '
                Dim TRSHead As New TableRow
                TRSHead.Width = "100"
                TRSHead.BorderWidth = "1"
                TRSHead.BorderStyle = BorderStyle.Solid
                TRSHead.Style.Add("background-color", "lightsteelblue")

                Dim TRSHead_00 As New TableCell
                TRSHead_00.BorderWidth = "1"
                TRSHead_00.BorderColor = Drawing.Color.Silver
                TRSHead_00.BorderStyle = BorderStyle.Solid
                Dim StrUserNam As String = Nothing

                DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
                If DTT.Rows.Count > 0 Then
                    StrUserNam = DTT.Rows(0)(0)
                End If
                Dim TRHead_1 As New TableRow

                RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)


                RH.BlankRow(tb, 4)
                tb.Controls.Add(TRSHead)


                'TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14, TRHead_1_15 As New TableCell

                TRHead_1_00.BorderWidth = "1"
                TRHead_1_01.BorderWidth = "1"
                TRHead_1_02.BorderWidth = "1"
                TRHead_1_03.BorderWidth = "1"
                TRHead_1_04.BorderWidth = "1"
                TRHead_1_05.BorderWidth = "1"
                TRHead_1_06.BorderWidth = "1"
                TRHead_1_07.BorderWidth = "1"
                TRHead_1_08.BorderWidth = "1"
                TRHead_1_09.BorderWidth = "1"
                TRHead_1_10.BorderWidth = "1"
                TRHead_1_11.BorderWidth = "1"
                TRHead_1_12.BorderWidth = "1"
                TRHead_1_13.BorderWidth = "1"
                TRHead_1_14.BorderWidth = "1"
                TRHead_1_15.BorderWidth = "1"

                TRHead_1_00.BorderColor = Drawing.Color.Silver
                TRHead_1_01.BorderColor = Drawing.Color.Silver
                TRHead_1_02.BorderColor = Drawing.Color.Silver
                TRHead_1_03.BorderColor = Drawing.Color.Silver
                TRHead_1_04.BorderColor = Drawing.Color.Silver
                TRHead_1_05.BorderColor = Drawing.Color.Silver
                TRHead_1_06.BorderColor = Drawing.Color.Silver
                TRHead_1_07.BorderColor = Drawing.Color.Silver
                TRHead_1_08.BorderColor = Drawing.Color.Silver
                TRHead_1_09.BorderColor = Drawing.Color.Silver
                TRHead_1_10.BorderColor = Drawing.Color.Silver
                TRHead_1_11.BorderColor = Drawing.Color.Silver
                TRHead_1_12.BorderColor = Drawing.Color.Silver
                TRHead_1_13.BorderColor = Drawing.Color.Silver
                TRHead_1_14.BorderColor = Drawing.Color.Silver
                TRHead_1_15.BorderColor = Drawing.Color.Silver

                TRHead_1_00.BorderStyle = BorderStyle.Solid
                TRHead_1_01.BorderStyle = BorderStyle.Solid
                TRHead_1_02.BorderStyle = BorderStyle.Solid
                TRHead_1_03.BorderStyle = BorderStyle.Solid
                TRHead_1_04.BorderStyle = BorderStyle.Solid
                TRHead_1_05.BorderStyle = BorderStyle.Solid
                TRHead_1_06.BorderStyle = BorderStyle.Solid
                TRHead_1_07.BorderStyle = BorderStyle.Solid
                TRHead_1_08.BorderStyle = BorderStyle.Solid
                TRHead_1_09.BorderStyle = BorderStyle.Solid
                TRHead_1_10.BorderStyle = BorderStyle.Solid
                TRHead_1_11.BorderStyle = BorderStyle.Solid
                TRHead_1_12.BorderStyle = BorderStyle.Solid
                TRHead_1_13.BorderStyle = BorderStyle.Solid
                TRHead_1_14.BorderStyle = BorderStyle.Solid
                TRHead_1_15.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TRHead_1, TRHead_1_00, 2, 2, "c", "SlNo")
                RH.AddColumn(TRHead_1, TRHead_1_01, 8, 8, "c", "Security No")
                RH.AddColumn(TRHead_1, TRHead_1_02, 6, 6, "c", "Date")
                RH.AddColumn(TRHead_1, TRHead_1_03, 5, 5, "l", "Time")
                RH.AddColumn(TRHead_1, TRHead_1_04, 8, 8, "l", "Location")
                RH.AddColumn(TRHead_1, TRHead_1_05, 8, 8, "c", "On behalf Of")
                RH.AddColumn(TRHead_1, TRHead_1_06, 6, 6, "l", "Department")
                RH.AddColumn(TRHead_1, TRHead_1_07, 6, 6, "l", "Designation")
                RH.AddColumn(TRHead_1, TRHead_1_08, 6, 6, "l", "Cug")
                RH.AddColumn(TRHead_1, TRHead_1_09, 6, 6, "l", "Official Mail")
                RH.AddColumn(TRHead_1, TRHead_1_10, 7, 7, "l", "Security Exception")
                RH.AddColumn(TRHead_1, TRHead_1_11, 7, 7, "l", "Business Requirement")
                RH.AddColumn(TRHead_1, TRHead_1_12, 7, 7, "l", "TimelineForException")
                RH.AddColumn(TRHead_1, TRHead_1_13, 6, 6, "l", "RequestedBy")
                RH.AddColumn(TRHead_1, TRHead_1_14, 6, 6, "l", "RequestedOn")
                RH.AddColumn(TRHead_1, TRHead_1_15, 6, 6, "l", "Attachments")

                tb.Controls.Add(TRHead_1)
                I = 0
                For Each DR In DT.Rows
                    I = I + 1
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid
                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"
                    TR3_06.BorderWidth = "1"
                    TR3_07.BorderWidth = "1"
                    TR3_08.BorderWidth = "1"
                    TR3_09.BorderWidth = "1"
                    TR3_10.BorderWidth = "1"
                    TR3_11.BorderWidth = "1"
                    TR3_12.BorderWidth = "1"
                    TR3_13.BorderWidth = "1"
                    TR3_14.BorderWidth = "1"
                    TR3_15.BorderWidth = "1"

                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver
                    TR3_06.BorderColor = Drawing.Color.Silver
                    TR3_07.BorderColor = Drawing.Color.Silver
                    TR3_08.BorderColor = Drawing.Color.Silver
                    TR3_09.BorderColor = Drawing.Color.Silver
                    TR3_10.BorderColor = Drawing.Color.Silver
                    TR3_11.BorderColor = Drawing.Color.Silver
                    TR3_12.BorderColor = Drawing.Color.Silver
                    TR3_13.BorderColor = Drawing.Color.Silver
                    TR3_14.BorderColor = Drawing.Color.Silver
                    TR3_15.BorderColor = Drawing.Color.Silver

                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid
                    TR3_06.BorderStyle = BorderStyle.Solid
                    TR3_07.BorderStyle = BorderStyle.Solid
                    TR3_08.BorderStyle = BorderStyle.Solid
                    TR3_09.BorderStyle = BorderStyle.Solid
                    TR3_10.BorderStyle = BorderStyle.Solid
                    TR3_11.BorderStyle = BorderStyle.Solid
                    TR3_12.BorderStyle = BorderStyle.Solid
                    TR3_13.BorderStyle = BorderStyle.Solid
                    TR3_14.BorderStyle = BorderStyle.Solid
                    TR3_15.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TR3, TR3_00, 2, 2, "c", I.ToString())
                    RH.AddColumn(TR3, TR3_01, 8, 8, "c", DR(0).ToString())
                    RH.AddColumn(TR3, TR3_02, 6, 6, "c", DR(1).ToString())
                    RH.AddColumn(TR3, TR3_03, 5, 5, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_04, 8, 8, "l", DR(3).ToString())
                    RH.AddColumn(TR3, TR3_05, 8, 8, "c", DR(4).ToString())
                    RH.AddColumn(TR3, TR3_06, 6, 6, "l", DR(5).ToString())
                    RH.AddColumn(TR3, TR3_07, 6, 6, "l", DR(6).ToString())
                    RH.AddColumn(TR3, TR3_08, 6, 6, "l", DR(7).ToString())
                    RH.AddColumn(TR3, TR3_09, 6, 6, "l", DR(8).ToString())
                    RH.AddColumn(TR3, TR3_10, 7, 7, "l", DR(9).ToString())
                    RH.AddColumn(TR3, TR3_11, 7, 7, "l", DR(10).ToString())
                    RH.AddColumn(TR3, TR3_12, 7, 7, "l", DR(11).ToString())
                    RH.AddColumn(TR3, TR3_13, 6, 6, "l", DR(12).ToString())
                    RH.AddColumn(TR3, TR3_14, 6, 6, "l", DR(13).ToString())

                    Dim StrBind As String = ""
                    Dim Inx As Integer = 0
                    If (DR(15).ToString <> "") Then
                        Dim StrArray() As String = DR(15).split("¥")
                        Dim Cnt As Integer
                        For Cnt = 0 To StrArray.Length - 2
                            Inx = Cnt + 1
                            Dim StrArray2() As String = StrArray(Cnt).Split("®")
                            StrBind += "<a href='ShowRiskFormat.aspx?IncidentID=" + DR(14).ToString() + "&IntID=" + StrArray2(0).ToString() + "&StatusID=3' style='text-align:right;' target='_blank'>" + Inx.ToString + ") " + StrArray2(1).ToString() + ";  </a></br>"
                        Next
                    Else
                        StrBind += ""
                    End If
                    RH.AddColumn(TR3, TR3_15, 6, 6, "l", StrBind)

                    tb.Controls.Add(TR3)

                Next
                RH.BlankRow(tb, 30)
                pnDisplay.Controls.Add(tb)
            Else
                SqlStr = "select a.Sec_No,a.Sec_DT,a.Sec_Time,a.Sec_Location,a.emp_name +' (' + convert(varchar,a.emp_code) + ')' OnBehalfOf,a.dept_name,a.designation,a.cug_no,a.off_email,a.Sec_Exception,a.Bus_Requirement,a.Sec_TimeLine,a.enter_by,replace(convert(char(11),a.enter_on,113),' ','-'),a.Sec_ID,(SELECT Stuff((select convert(varchar,b.Sec_Attach_id) + '®' + b.secfile_name + '¥' AS 'data()' from security_exception_attach b where b.Sec_ID=a.Sec_ID FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,0,N'')),a.Sec_Comments,a.Risk_fore,a.Sec_Controls,a.CISO_Comments,a.verify_by,replace(convert(char(11),a.verify_on,113),' ','-'),case when a.status_id=1 then 'Requested' when a.status_id=2 then 'Approved' when a.status_id=3 then 'Declined' when a.status_id=4 then 'Working' end from security_exception_dtl a " & StrQuery & ""

                DT = DB.ExecuteDataSet(SqlStr).Tables(0)
                Dim b As Integer = DT.Rows.Count

                RH.Heading(Session("FirmName"), tb, "SECURITY EXCEPTIONS", 150)

                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")

                '
                Dim TRSHead As New TableRow
                TRSHead.Width = "100"
                TRSHead.BorderWidth = "1"
                TRSHead.BorderStyle = BorderStyle.Solid
                TRSHead.Style.Add("background-color", "lightsteelblue")

                Dim TRSHead_00 As New TableCell
                TRSHead_00.BorderWidth = "1"
                TRSHead_00.BorderColor = Drawing.Color.Silver
                TRSHead_00.BorderStyle = BorderStyle.Solid
                Dim StrUserNam As String = Nothing

                DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
                If DTT.Rows.Count > 0 Then
                    StrUserNam = DTT.Rows(0)(0)
                End If
                Dim TRHead_1 As New TableRow

                RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)


                RH.BlankRow(tb, 4)
                tb.Controls.Add(TRSHead)


                'TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14, TRHead_1_15, TRHead_1_16, TRHead_1_17, TRHead_1_18, TRHead_1_19, TRHead_1_20, TRHead_1_21, TRHead_1_22 As New TableCell

                TRHead_1_00.BorderWidth = "1"
                TRHead_1_01.BorderWidth = "1"
                TRHead_1_02.BorderWidth = "1"
                TRHead_1_03.BorderWidth = "1"
                TRHead_1_04.BorderWidth = "1"
                TRHead_1_05.BorderWidth = "1"
                TRHead_1_06.BorderWidth = "1"
                TRHead_1_07.BorderWidth = "1"
                TRHead_1_08.BorderWidth = "1"
                TRHead_1_09.BorderWidth = "1"
                TRHead_1_10.BorderWidth = "1"
                TRHead_1_11.BorderWidth = "1"
                TRHead_1_12.BorderWidth = "1"
                TRHead_1_13.BorderWidth = "1"
                TRHead_1_14.BorderWidth = "1"
                TRHead_1_15.BorderWidth = "1"
                TRHead_1_16.BorderWidth = "1"
                TRHead_1_17.BorderWidth = "1"
                TRHead_1_18.BorderWidth = "1"
                TRHead_1_19.BorderWidth = "1"
                TRHead_1_20.BorderWidth = "1"
                TRHead_1_21.BorderWidth = "1"
                TRHead_1_22.BorderWidth = "1"

                TRHead_1_00.BorderColor = Drawing.Color.Silver
                TRHead_1_01.BorderColor = Drawing.Color.Silver
                TRHead_1_02.BorderColor = Drawing.Color.Silver
                TRHead_1_03.BorderColor = Drawing.Color.Silver
                TRHead_1_04.BorderColor = Drawing.Color.Silver
                TRHead_1_05.BorderColor = Drawing.Color.Silver
                TRHead_1_06.BorderColor = Drawing.Color.Silver
                TRHead_1_07.BorderColor = Drawing.Color.Silver
                TRHead_1_08.BorderColor = Drawing.Color.Silver
                TRHead_1_09.BorderColor = Drawing.Color.Silver
                TRHead_1_10.BorderColor = Drawing.Color.Silver
                TRHead_1_11.BorderColor = Drawing.Color.Silver
                TRHead_1_12.BorderColor = Drawing.Color.Silver
                TRHead_1_13.BorderColor = Drawing.Color.Silver
                TRHead_1_14.BorderColor = Drawing.Color.Silver
                TRHead_1_15.BorderColor = Drawing.Color.Silver
                TRHead_1_16.BorderColor = Drawing.Color.Silver
                TRHead_1_17.BorderColor = Drawing.Color.Silver
                TRHead_1_18.BorderColor = Drawing.Color.Silver
                TRHead_1_19.BorderColor = Drawing.Color.Silver
                TRHead_1_20.BorderColor = Drawing.Color.Silver
                TRHead_1_21.BorderColor = Drawing.Color.Silver
                TRHead_1_22.BorderColor = Drawing.Color.Silver

                TRHead_1_00.BorderStyle = BorderStyle.Solid
                TRHead_1_01.BorderStyle = BorderStyle.Solid
                TRHead_1_02.BorderStyle = BorderStyle.Solid
                TRHead_1_03.BorderStyle = BorderStyle.Solid
                TRHead_1_04.BorderStyle = BorderStyle.Solid
                TRHead_1_05.BorderStyle = BorderStyle.Solid
                TRHead_1_06.BorderStyle = BorderStyle.Solid
                TRHead_1_07.BorderStyle = BorderStyle.Solid
                TRHead_1_08.BorderStyle = BorderStyle.Solid
                TRHead_1_09.BorderStyle = BorderStyle.Solid
                TRHead_1_10.BorderStyle = BorderStyle.Solid
                TRHead_1_11.BorderStyle = BorderStyle.Solid
                TRHead_1_12.BorderStyle = BorderStyle.Solid
                TRHead_1_13.BorderStyle = BorderStyle.Solid
                TRHead_1_14.BorderStyle = BorderStyle.Solid
                TRHead_1_15.BorderStyle = BorderStyle.Solid
                TRHead_1_16.BorderStyle = BorderStyle.Solid
                TRHead_1_17.BorderStyle = BorderStyle.Solid
                TRHead_1_18.BorderStyle = BorderStyle.Solid
                TRHead_1_19.BorderStyle = BorderStyle.Solid
                TRHead_1_20.BorderStyle = BorderStyle.Solid
                TRHead_1_21.BorderStyle = BorderStyle.Solid
                TRHead_1_22.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TRHead_1, TRHead_1_00, 2, 2, "c", "SlNo")
                RH.AddColumn(TRHead_1, TRHead_1_01, 5, 5, "c", "Security No")
                RH.AddColumn(TRHead_1, TRHead_1_02, 5, 5, "c", "Date")
                RH.AddColumn(TRHead_1, TRHead_1_03, 5, 5, "l", "Time")
                RH.AddColumn(TRHead_1, TRHead_1_04, 5, 5, "l", "Location")
                RH.AddColumn(TRHead_1, TRHead_1_05, 5, 5, "c", "On behalf Of")
                RH.AddColumn(TRHead_1, TRHead_1_06, 5, 5, "l", "Department")
                RH.AddColumn(TRHead_1, TRHead_1_07, 5, 5, "l", "Designation")
                RH.AddColumn(TRHead_1, TRHead_1_08, 5, 5, "l", "Cug")
                RH.AddColumn(TRHead_1, TRHead_1_09, 5, 5, "l", "Official Mail")
                RH.AddColumn(TRHead_1, TRHead_1_10, 5, 5, "l", "Security Exception")
                RH.AddColumn(TRHead_1, TRHead_1_11, 4, 4, "l", "Business Requirement")
                RH.AddColumn(TRHead_1, TRHead_1_12, 4, 4, "l", "TimelineForException")
                RH.AddColumn(TRHead_1, TRHead_1_13, 4, 4, "l", "RequestedBy")
                RH.AddColumn(TRHead_1, TRHead_1_14, 4, 4, "l", "RequestedOn")
                RH.AddColumn(TRHead_1, TRHead_1_15, 4, 4, "l", "Attachments")
                RH.AddColumn(TRHead_1, TRHead_1_16, 4, 4, "l", "InfoSecurityComments")
                RH.AddColumn(TRHead_1, TRHead_1_17, 4, 4, "l", "Risk Foreseen")
                RH.AddColumn(TRHead_1, TRHead_1_18, 4, 4, "l", "SecurityControls")
                RH.AddColumn(TRHead_1, TRHead_1_19, 4, 4, "l", "CISO Comments")
                RH.AddColumn(TRHead_1, TRHead_1_20, 4, 4, "l", "Verified By")
                RH.AddColumn(TRHead_1, TRHead_1_21, 4, 4, "l", "Verified On")
                RH.AddColumn(TRHead_1, TRHead_1_22, 4, 4, "l", "Status")

                tb.Controls.Add(TRHead_1)
                I = 0
                For Each DR In DT.Rows
                    I = I + 1
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid
                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16, TR3_17, TR3_18, TR3_19, TR3_20, TR3_21, TR3_22 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"
                    TR3_06.BorderWidth = "1"
                    TR3_07.BorderWidth = "1"
                    TR3_08.BorderWidth = "1"
                    TR3_09.BorderWidth = "1"
                    TR3_10.BorderWidth = "1"
                    TR3_11.BorderWidth = "1"
                    TR3_12.BorderWidth = "1"
                    TR3_13.BorderWidth = "1"
                    TR3_14.BorderWidth = "1"
                    TR3_15.BorderWidth = "1"
                    TR3_16.BorderWidth = "1"
                    TR3_17.BorderWidth = "1"
                    TR3_18.BorderWidth = "1"
                    TR3_19.BorderWidth = "1"
                    TR3_20.BorderWidth = "1"
                    TR3_21.BorderWidth = "1"
                    TR3_22.BorderWidth = "1"

                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver
                    TR3_06.BorderColor = Drawing.Color.Silver
                    TR3_07.BorderColor = Drawing.Color.Silver
                    TR3_08.BorderColor = Drawing.Color.Silver
                    TR3_09.BorderColor = Drawing.Color.Silver
                    TR3_10.BorderColor = Drawing.Color.Silver
                    TR3_11.BorderColor = Drawing.Color.Silver
                    TR3_12.BorderColor = Drawing.Color.Silver
                    TR3_13.BorderColor = Drawing.Color.Silver
                    TR3_14.BorderColor = Drawing.Color.Silver
                    TR3_15.BorderColor = Drawing.Color.Silver
                    TR3_16.BorderColor = Drawing.Color.Silver
                    TR3_17.BorderColor = Drawing.Color.Silver
                    TR3_18.BorderColor = Drawing.Color.Silver
                    TR3_19.BorderColor = Drawing.Color.Silver
                    TR3_20.BorderColor = Drawing.Color.Silver
                    TR3_21.BorderColor = Drawing.Color.Silver
                    TR3_22.BorderColor = Drawing.Color.Silver

                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid
                    TR3_06.BorderStyle = BorderStyle.Solid
                    TR3_07.BorderStyle = BorderStyle.Solid
                    TR3_08.BorderStyle = BorderStyle.Solid
                    TR3_09.BorderStyle = BorderStyle.Solid
                    TR3_10.BorderStyle = BorderStyle.Solid
                    TR3_11.BorderStyle = BorderStyle.Solid
                    TR3_12.BorderStyle = BorderStyle.Solid
                    TR3_13.BorderStyle = BorderStyle.Solid
                    TR3_14.BorderStyle = BorderStyle.Solid
                    TR3_15.BorderStyle = BorderStyle.Solid
                    TR3_16.BorderStyle = BorderStyle.Solid
                    TR3_17.BorderStyle = BorderStyle.Solid
                    TR3_18.BorderStyle = BorderStyle.Solid
                    TR3_19.BorderStyle = BorderStyle.Solid
                    TR3_20.BorderStyle = BorderStyle.Solid
                    TR3_21.BorderStyle = BorderStyle.Solid
                    TR3_22.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TR3, TR3_00, 2, 2, "c", I.ToString())
                    RH.AddColumn(TR3, TR3_01, 5, 5, "c", DR(0).ToString())
                    RH.AddColumn(TR3, TR3_02, 5, 5, "c", DR(1).ToString())
                    RH.AddColumn(TR3, TR3_03, 5, 5, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_04, 5, 5, "l", DR(3).ToString())
                    RH.AddColumn(TR3, TR3_05, 5, 5, "c", DR(4).ToString())
                    RH.AddColumn(TR3, TR3_06, 5, 5, "l", DR(5).ToString())
                    RH.AddColumn(TR3, TR3_07, 5, 5, "l", DR(6).ToString())
                    RH.AddColumn(TR3, TR3_08, 5, 5, "l", DR(7).ToString())
                    RH.AddColumn(TR3, TR3_09, 5, 5, "l", DR(8).ToString())
                    RH.AddColumn(TR3, TR3_10, 5, 5, "l", DR(9).ToString())
                    RH.AddColumn(TR3, TR3_11, 4, 4, "l", DR(10).ToString())
                    RH.AddColumn(TR3, TR3_12, 4, 4, "l", DR(11).ToString())
                    RH.AddColumn(TR3, TR3_13, 4, 4, "l", DR(12).ToString())
                    RH.AddColumn(TR3, TR3_14, 4, 4, "l", DR(13).ToString())

                    Dim StrBind As String = ""
                    Dim Inx As Integer = 0
                    If (DR(15).ToString <> "") Then
                        Dim StrArray() As String = DR(15).split("¥")

                        Dim Cnt As Integer
                        For Cnt = 0 To StrArray.Length - 2
                            Inx = Cnt + 1
                            Dim StrArray2() As String = StrArray(Cnt).Split("®")
                            StrBind += "<a href='ShowRiskFormat.aspx?IncidentID=" + DR(14).ToString() + "&IntID=" + StrArray2(0).ToString() + "&StatusID=3' style='text-align:right;' target='_blank'>" + Inx.ToString + ") " + StrArray2(1).ToString() + ";  </a></br>"
                        Next
                    Else
                        StrBind += ""
                    End If
                    RH.AddColumn(TR3, TR3_15, 4, 4, "l", StrBind)
                    RH.AddColumn(TR3, TR3_16, 4, 4, "l", DR(16).ToString())
                    RH.AddColumn(TR3, TR3_17, 4, 4, "l", DR(17).ToString())
                    RH.AddColumn(TR3, TR3_18, 4, 4, "l", DR(18).ToString())
                    RH.AddColumn(TR3, TR3_19, 4, 4, "l", DR(19).ToString())
                    RH.AddColumn(TR3, TR3_20, 4, 4, "l", DR(20).ToString())
                    RH.AddColumn(TR3, TR3_21, 4, 4, "l", DR(21).ToString())
                    RH.AddColumn(TR3, TR3_22, 4, 4, "l", DR(22).ToString())

                    tb.Controls.Add(TR3)

                Next
                RH.BlankRow(tb, 30)
                pnDisplay.Controls.Add(tb)
            End If

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        'Try
        WebTools.ExporttoExcel(DT, "Incidents")
        'Catch ex As Exception
        '    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
        '    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        'End Try
    End Sub
End Class
