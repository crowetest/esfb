﻿Imports System.Data
Partial Class Leave_ShowVetFormat
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim StatusID As Integer = Convert.ToInt32(Request.QueryString.[Get]("StatusID"))
            Dim IncidentID As Integer = Convert.ToInt32(Request.QueryString.[Get]("IncidentID"))
            Dim IntID As Integer = Convert.ToInt32(Request.QueryString.[Get]("IntID"))

            If StatusID = 1 Then
                DT = DB.ExecuteDataSet("select a.int_attach,a.content_type,intfile_name from risk_incident_attach a where a.Incident_ID=" & IncidentID & " and a.int_id= " & IntID & "").Tables(0)
            ElseIf StatusID = 2 Then
                DT = DB.ExecuteDataSet("select a.risk_attach,a.content_type,riskfile_name from risk_assessment_attach a where a.Risk_ID=" & IncidentID & " and a.risk_attach_id= " & IntID & "").Tables(0)
            ElseIf StatusID = 3 Then
                DT = DB.ExecuteDataSet("select a.sec_attach,a.content_type,secfile_name from security_exception_attach a where a.Sec_ID=" & IncidentID & " and a.Sec_Attach_id= " & IntID & "").Tables(0)
            End If

            If DT IsNot Nothing Then
                Dim bytes() As Byte = CType(DT.Rows(0)(0), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = DT.Rows(0)(1).ToString()
                Response.AddHeader("content-disposition", "attachment;filename=" + DT.Rows(0)(2).ToString().Replace(" ", ""))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
