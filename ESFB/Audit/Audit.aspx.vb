﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Audit_Audit
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DTComments As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 36) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Micro Liability Audit"
            Dim AuditID As Integer
            Dim AuditType As Integer
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            AuditID = CInt(GN.Decrypt(Request.QueryString.Get("AuditID")))
            AuditType = CInt(Request.QueryString.Get("AuditType"))

            DTComments = AD.GetAuditGeneralComments(AuditID)
            If DTComments.Rows.Count > 0 Then
                Me.hdnComments.Value = DTComments.Rows(0)(0).ToString() + "Ø" + DTComments.Rows(0)(1).ToString()
            End If
            DT = AD.Get_Audit_Summary(AuditType, AuditID)
            If Not IsNothing(DT) Then
                hdnCenterCount.Value = DT.Rows(0)(0).ToString() + "~" + DT.Rows(0)(1).ToString() + "~" + DT.Rows(0)(2).ToString() + "~" + DT.Rows(0)(3).ToString()
            Else
                hdnCenterCount.Value = "0~0~0~0"
            End If
            hdnAuditID.Value = AuditID.ToString()
            hdnAuditTypeID.Value = AuditType.ToString()
            DT = AD.GetAuditDtl(AuditID)
            lblBranch.Text = DT.Rows(0)(1).ToString()
            lblStartDate.Text = DT.Rows(0)(2).ToString()
            lblAuditType.Text = DT.Rows(0)(3).ToString()
            GN.ComboFill(cmbGroup, AD.GetCheckGroup(AuditType), 0, 1)
            txtManagementNote.Text = AD.GetManagementNote(AuditID)
            txtManagementNote.Text = AD.GetManagementNote(AuditID)
            DT = DB.ExecuteDataSet("select '' as Scheme_abbr union all select scheme_abbr from scheme_master where status_id=2").Tables(0)
            hdnScheme.Value = ""
            For Each DR As DataRow In DT.Rows
                hdnScheme.value += "Ñ" + DR(0).ToString() + "ÿ" + DR(0).ToString()
            Next
            cmbGroup.Attributes.Add("onchange", "GroupOnChange()")
            cmbSubGroup.Attributes.Add("onchange", "SubGroupOnChange()")
            cmbCheckList.Attributes.Add("onchange", "ItemOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim AuditID As Integer = CInt(Data(1))
            Dim ItemID As Integer = CInt(Data(2))
            Dim ObservationDtl As String = Data(3)
            If ObservationDtl <> "" Then
                ObservationDtl = ObservationDtl.Substring(1)
            End If
            Dim CloseFlag As Integer = CInt(Data(4))
            Dim GeneralComments As String = Data(5)
            Dim SeriousComments As String = Data(6)
            Dim ManagementNote As String = Data(7)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(9) As SqlParameter
                Params(0) = New SqlParameter("@AuditID", SqlDbType.Int)
                Params(0).Value = AuditID
                Params(1) = New SqlParameter("@ItemID", SqlDbType.Int)
                Params(1).Value = ItemID
                Params(2) = New SqlParameter("@ObservationDtl", SqlDbType.VarChar)
                Params(2).Value = ObservationDtl
                Params(3) = New SqlParameter("@CloseFlag", SqlDbType.Int)
                Params(3).Value = CloseFlag
                Params(4) = New SqlParameter("@GeneralComments", SqlDbType.VarChar, 2000)
                Params(4).Value = GeneralComments
                Params(5) = New SqlParameter("@SeriousComments", SqlDbType.VarChar, 2000)
                Params(5).Value = SeriousComments
                Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@CenterID", SqlDbType.VarChar, 20)
                Params(8).Value = 1
                Params(9) = New SqlParameter("@ManagementNote", SqlDbType.VarChar, 5000)
                Params(9).Value = ManagementNote
                DB.ExecuteNonQuery("SP_AuditObservation", Params)
                ErrorFlag = CInt(Params(6).Value)
                Message = CStr(Params(7).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            DT = AD.GetCheckSubGroup(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 3 Then
            DT = AD.GetCheckList(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 4 Then
            DT = AD.GetAuditAccounts(CInt(Data(1)), CInt(Data(2)))
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += "¥" + DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString() & "µ" & DT.Rows(n)(4).ToString() & "µ" & DT.Rows(n)(5).ToString() & "µ" & DT.Rows(n)(6).ToString() & "µ" & DT.Rows(n)(7).ToString() & "µ" & DT.Rows(n)(8).ToString() & "µ" & DT.Rows(n)(9).ToString() & "µ" & DT.Rows(n)(10).ToString() & "µ" & DT.Rows(n)(11).ToString() & "µ" & DT.Rows(n)(12).ToString() & "µ" & DT.Rows(n)(13).ToString() & "µ" & DT.Rows(n)(14).ToString() & "µ" & DT.Rows(n)(15).ToString() & "µ" & DT.Rows(n)(16).ToString()
            Next
            DT = DB.ExecuteDataSet("select case when VERY_SERIOUS_FLAG=1 then 'V' when SERIOUS_FLAG=1 then 'S' else 'G' end from audit_check_list where item_id=" + Data(1).ToString()).Tables(0)
            CallBackReturn += "Ø" + DT.Rows(0)(0).ToString()
        ElseIf CInt(Data(0)) = 5 Then
            DT = AD.GetLoanDetails(Data(1), "0")
            If DT.Rows.Count > 0 Then
                If DT.Rows(0)(6).ToString() <> "1" Then
                    CallBackReturn = "1"
                Else
                    CallBackReturn = DT.Rows(0)(0).ToString() & "µ" & DT.Rows(0)(1).ToString() & "µ" & DT.Rows(0)(2).ToString() & "µ" & DT.Rows(0)(3).ToString() & "µ" & DT.Rows(0)(4).ToString() & "µ" & DT.Rows(0)(5).ToString()
                End If

            Else
                CallBackReturn = ""
            End If
        End If
    End Sub
End Class
