﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AuditStaffInvolvementRefund
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim DB As New MS_SQL.Connect
    Dim AuditID As Integer
    Dim AuditType As Integer
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1010) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Refund Staff Involvement"
            If Not IsPostBack Then
                GN.ComboFill(cmbEmployee, AD.GetStaffDetails(CInt(Session("Post_ID")), CInt(Session("UserID"))), 0, 1)
                ' GN.ComboFill(cmbEmployee, AD.GetStaffDetails(6, 1189), 0, 1)
                DT = DB.ExecuteDataSet("SELECT ID,DESCR FROM AUDIT_RECOVERY_TYPE ORDER BY DESCR").Tables(0)
                For Each DR As DataRow In DT.Rows
                    Me.hdnType.Value += "~" + DR(0).ToString() + "!" + DR(1).ToString()
                Next
                Me.hdnType.Value = Me.hdnType.Value.Substring(1)
            End If
            Me.cmbEmployee.Attributes.Add("onchange", "return EmployeeOnchange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim EmpCode As Integer = CInt(Data(1))
            DT = DB.ExecuteDataSet("select a.SINO,a.LOAN_NO,a.REMARKS,c.ITEM_NAME,e.Amount,isnull(e.Refund_Amount,0) Refund_Amount from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_CHECK_LIST c,AUDIT_DTL d,AUDIT_STAFF_INVOLVEMENT e where a.OBSERVATION_ID = b.OBSERVATION_ID and b.ITEM_ID = c.ITEM_ID and b.AUDIT_ID = d.AUDIT_ID and a.SINO = e.SINO and e.EMP_CODE = " & EmpCode & " and e.Status_ID = 1 and e.Amount-isnull(e.Refund_Amount,0) > 0").Tables(0)
            For Each dr As DataRow In DT.Rows
                CallBackReturn += "¥" & dr(0).ToString() & "µ" & dr(1).ToString() & "µ" & dr(2).ToString() & "µ" & dr(3).ToString() & "µ" & dr(4).ToString() & "µ" & dr(5).ToString()
            Next
        ElseIf CInt(Data(0)) = 2 Then
            Dim EmpCode As Integer = CInt(Data(1))
            Dim StaffDtl As String = Data(2).ToString
            Dim Remarks As String = Data(3).ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(0).Value = EmpCode
                Params(1) = New SqlParameter("@StaffDtl", SqlDbType.VarChar, 1000)
                Params(1).Value = StaffDtl.Substring(1)
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = CInt(Session("UserID"))
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                Params(5).Value = Remarks
                DB.ExecuteNonQuery("SP_Audit_Staff_Involvement_Refund", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message
        ElseIf CInt(Data(0)) = 3 Then
            DT = AD.GetAuditForReport(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub
End Class
