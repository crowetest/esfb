﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditCheckList.aspx.vb" Inherits="AuditCheckList" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }   
       
        .style1
        {
            width: 65%;
           
        }
     
     .bg
     {
         background-color:#FFF;
     }
           
    </style>
    <script language="javascript" type="text/javascript">
    function table_fill() 
        {   document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div class=mainhead style='width:85%; height:25px; padding-top:0px;margin:0px auto;background-color:#E31E24;' ><table style='width:100%;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            if (document.getElementById("<%= hid_tab.ClientID %>").value==1){
                tab += "<td style='width:5%;text-align:center;color:#FFF;' class=NormalText>Sl No</td>";
                tab += "<td style='width:53%;text-align:left;color:#FFF;' class=NormalText>Group</td>";
                tab += "<td style='width:22%;text-align:left;color:#FFF;' class=NormalText>SPM Related</td>";
            }
            else if (document.getElementById("<%= hid_tab.ClientID %>").value==2){
                tab += "<td style='width:5%;text-align:center;color:#FFF;' class=NormalText>Sl No</td>";
                tab += "<td style='width:75%;text-align:left;color:#FFF;' class=NormalText>Sub Group</td>";
            }
            else if (document.getElementById("<%= hid_tab.ClientID %>").value==3){
                tab += "<td style='width:5%;text-align:center;color:#FFF;' class=NormalText>Sl No</td>";
                tab += "<td style='width:75%;text-align:left;color:#FFF;' class=NormalText>Check List</td>";

            }
           
            tab += "</tr></table></div><div style='width:85%; height:128px; overflow:auto; margin:0px auto;background-color:#E31E24;' ><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
              if(document.getElementById("<%= hid_dtls.ClientID %>").value!="")
              {
                    row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
               
                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                   
                                if (row_bg == 0) {
                                    row_bg = 1;
                                    tab += "<tr class=sub_first>";
                                }
                                else {
                                    row_bg = 0;
                                    tab += "<tr class=sub_second>";
                                }
                                if (document.getElementById("<%= hid_tab.ClientID %>").value==1){
                                
                                tab += "<td style='width:5%;text-align:center;' >" + col[0] + "</td>";
                                tab += "<td style='width:55%;text-align:left;' >" + col[1] + "</td>";
                                tab += "<td style='width:20%;text-align:left;' >" + col[2] + "</td>";
                                }
                                else{
                                
                                tab += "<td style='width:5%;text-align:center;' >" + col[0] + "</td>";
                                tab += "<td style='width:75%;text-align:left;' >" + col[1] + "</td>";
                                }
                                tab += "</tr>";
                            }
             }       
            tab += "</table></div>";
            
            document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }
        function RequestOnchange()
         {

            var Data = document.getElementById("<%= cmbAuditType.ClientID %>").value;
            if (Data != -1) {
                var ToData = "2Ø" + Data + "Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
                ToServer(ToData, 2);
            }
            ClearCombo("<%= cmbGroup.ClientID %>");
            ClearCombo("<%= cmbSubGroup.ClientID %>");

        }
        function GroupOnchange() {

            var Data = document.getElementById("<%= cmbGroup.ClientID %>").value;
            if (Data != -1) {
                var ToData = "3Ø" + Data + "Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
                ToServer(ToData, 3);
            }
            
            ClearCombo("<%= cmbSubGroup.ClientID %>");

        }
       function SubGroupOnchange() {
        var Data = document.getElementById("<%= cmbsubGroup.ClientID %>").value;
            if (Data != -1) {
                var ToData = "4Ø" + Data + "Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
                ToServer(ToData, 4);
            }
       }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("~");
                if(Data[0]==1){
                    var Dataval = arg.split("Ø");
                    alert(Dataval[1]);
                    
                    var ErrorFlag = Dataval[0].split("~");
                    if (ErrorFlag[1] == 0) {
                        document.getElementById("<%= txtGroup.ClientID %>").value="";
                        document.getElementById("<%= txtSubGroup.ClientID %>").value="";
                        document.getElementById("<%= txtCheckList.ClientID %>").value="";
                        if(document.getElementById("<%= hid_Tab.ClientID %>").value==1){
                            RequestOnchange();
                        }
                        else if(document.getElementById("<%= hid_Tab.ClientID %>").value==2){
                            GroupOnchange();
                        }
                         else if(document.getElementById("<%= hid_Tab.ClientID %>").value==3){
                            SubGroupOnchange();
                        }
                    }
                 }
                else{
                    document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                    table_fill();
                 }

            }
            else if (context == 2) {
                var Data = arg.split("~");
         
                if(Data[0]==1){
                    ComboFill(Data[1], "<%= cmbGroup.ClientID %>");
                }
                else{

                    document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                    table_fill();
                }
                
                return false;
            }
            else if (context == 3) {
                var Data = arg.split("~");
                if(Data[0]==1){
                    ComboFill(Data[1], "<%= cmbSubGroup.ClientID %>");
                }
                else{

                    document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                    table_fill();
                }
                
                
                return false;
            }
             else if (context == 4) {
                var Data = arg.split("~");
                if(Data[0]==2){
                document.getElementById("<%= hid_dtls.ClientID %>").value = Data[1];
                table_fill();
                 }
                return false;
            }
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        
        
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function GroupClick() {
            document.getElementById("AuditType").style.display = '';
            document.getElementById("GroupName").style.display = '';
            document.getElementById("SPM").style.display = '';
            document.getElementById("Group").style.display = 'none';
            document.getElementById("SubGroupName").style.display = 'none';
            document.getElementById("SubGroup").style.display = 'none';
            document.getElementById("CheckListName").style.display = 'none';
            document.getElementById("<%= hid_tab.ClientID %>").value = 1;
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display='none';

            document.getElementById("imgGroup").src = "../Image/1sel.png";
            document.getElementById("imgSubGroup").src = "../Image/2.png";
            document.getElementById("imgCheckList").src = "../Image/3CheckList.png";
            
            ClearCombo("<%= cmbSubGroup.ClientID %>");
            ClearCombo("<%= cmbGroup.ClientID %>");
            document.getElementById("<%= cmbAuditType.ClientID %>").value=-1;
            document.getElementById("<%= cmbGroup.ClientID %>").value=-1;
            document.getElementById("<%= cmbSubGroup.ClientID %>").value=-1;
            document.getElementById("<%= txtGroup.ClientID %>").value="";
            document.getElementById("<%= txtSubGroup.ClientID %>").value="";
            document.getElementById("<%= txtCheckList.ClientID %>").value="";
        }
        function SubGroupClick() {         

            
            document.getElementById("AuditType").style.display = '';
            document.getElementById("GroupName").style.display = 'none';
            document.getElementById("SPM").style.display = 'none';
            document.getElementById("Group").style.display = '';
            document.getElementById("SubGroupName").style.display = ''
            document.getElementById("SubGroup").style.display = 'none'
            document.getElementById("CheckListName").style.display = 'none'
            document.getElementById("<%= hid_tab.ClientID %>").value = 2;
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display='none';

            document.getElementById("imgGroup").src = "../Image/1.png";
            document.getElementById("imgSubGroup").src = "../Image/2sel.png";
            document.getElementById("imgCheckList").src = "../Image/3CheckList.png";
            document.getElementById("<%= cmbAuditType.ClientID %>").value=-1;
            document.getElementById("<%= cmbGroup.ClientID %>").value=-1;
            document.getElementById("<%= cmbSubGroup.ClientID %>").value=-1;
            document.getElementById("<%= txtGroup.ClientID %>").value="";
            document.getElementById("<%= txtSubGroup.ClientID %>").value="";
            document.getElementById("<%= txtCheckList.ClientID %>").value="";
            
            
        }
        function CheckListClick() {
            document.getElementById("AuditType").style.display ='';
            document.getElementById("GroupName").style.display = 'none';
            document.getElementById("SPM").style.display = 'none';
            document.getElementById("Group").style.display = '';
            document.getElementById("SubGroupName").style.display = 'none';
            document.getElementById("SubGroup").style.display = '';
            document.getElementById("CheckListName").style.display = '';
            document.getElementById("<%= hid_tab.ClientID %>").value = 3;
            document.getElementById("<%= hid_dtls.ClientID %>").value = "";
            document.getElementById("<%= pnlDtls.ClientID %>").style.display='none';

            document.getElementById("imgGroup").src = "../Image/1.png";
            document.getElementById("imgSubGroup").src = "../Image/2.png";
            document.getElementById("imgCheckList").src = "../Image/3sel.png";
            document.getElementById("<%= cmbAuditType.ClientID %>").value=-1;
            document.getElementById("<%= cmbGroup.ClientID %>").value=-1;
            document.getElementById("<%= cmbSubGroup.ClientID %>").value=-1;
            document.getElementById("<%= txtGroup.ClientID %>").value="";
            document.getElementById("<%= txtSubGroup.ClientID %>").value="";
            document.getElementById("<%= txtCheckList.ClientID %>").value="";
        }
        
function btnSave_onclick() {

            if (document.getElementById("<%= hid_Tab.ClientID %>").value ==1){
                 if (document.getElementById("<%= cmbAuditType.ClientID %>").value == "-1") 
                {
                    alert("Select Audit Type");
                    document.getElementById("<%= cmbAuditType.ClientID %>").focus();
                    return false;
                }
                 if (document.getElementById("<%= txtGroup.ClientID %>").value == "") 
                {
                    alert("Enter Group Name");
                    document.getElementById("<%= txtGroup.ClientID %>").focus();
                    return false;
                }
                    var MainID	= document.getElementById("<%= cmbAuditType.ClientID %>").value;
                    if (document.getElementById("<%= chkSPM.ClientID %>").checked==true)
                    var SPM =1;
                    else
                    var SPM =0;
	                var Datas	= document.getElementById("<%= txtGroup.ClientID %>").value+"~"+SPM;
           
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value ==2){
                    if (document.getElementById("<%= cmbAuditType.ClientID %>").value == "-1") 
                    {
                        alert("Select Audit Type");
                        document.getElementById("<%= cmbAuditType.ClientID %>").focus();
                        return false;
                    }
                     if (document.getElementById("<%= cmbGroup.ClientID %>").value == "-1") 
                    {
                        alert("Select Group ");
                        document.getElementById("<%= cmbGroup.ClientID %>").focus();
                        return false;
                    }
                     if (document.getElementById("<%= txtSubGroup.ClientID %>").value == "") 
                    {
                        alert("Enter Sub Group Name");
                        document.getElementById("<%= txtSubGroup.ClientID %>").focus();
                        return false;
                    }
                    var MainID	= document.getElementById("<%= cmbGroup.ClientID %>").value;
	                var Datas	= document.getElementById("<%= txtSubGroup.ClientID %>").value+"~0";
                   
            }
            else if (document.getElementById("<%= hid_Tab.ClientID %>").value ==3){
                    if (document.getElementById("<%= cmbAuditType.ClientID %>").value == "-1") 
                    {
                        alert("Select Audit Type");
                        document.getElementById("<%= cmbAuditType.ClientID %>").focus();
                        return false;
                    }
                     if (document.getElementById("<%= cmbGroup.ClientID %>").value == "-1") 
                    {
                        alert("Select Group ");
                        document.getElementById("<%= cmbGroup.ClientID %>").focus();
                        return false;
                    }
                     if (document.getElementById("<%= cmbSubGroup.ClientID %>").value == "-1") 
                    {
                        alert("Select SubGroup ");
                        document.getElementById("<%= cmbSubGroup.ClientID %>").focus();
                        return false;
                    }
                     if (document.getElementById("<%= txtCheckList.ClientID %>").value == "") 
                    {
                        alert("Enter Check List");
                        document.getElementById("<%= txtCheckList.ClientID %>").focus();
                        return false;
                    }
                    var MainID	= document.getElementById("<%= cmbSubGroup.ClientID %>").value;
	                var Datas	= document.getElementById("<%= txtCheckList.ClientID %>").value+"~0";
                     
            }
            var ToData = "1Ø" + MainID + "Ø" + Datas+ "Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value+ "Ø" + document.getElementById("<%= cmbAuditType.ClientID %>").value;
	        ToServer(ToData, 1); 
	       
}

    </script>
</head>
</html>
 <br />
 <div style="width:60%;background-color:#CC1B20;margin:0px auto;height:430px;">
    <div style="width:93%;background-color:#CC1B20;margin:0px auto;">
       <img id="imgGroup" src="../Image/1sel.png" onclick="return GroupClick()" style="cursor:pointer;"/>&nbsp;
       <img id="imgSubGroup" src="../Image/2.png" onclick="return SubGroupClick()" style="cursor:pointer;" />&nbsp;
       <img id="imgCheckList"   src="../Image/3CheckList.png" onclick="return CheckListClick()" style="cursor:pointer;" /><br /></div>
    <div style="width:93%;background-color:white;margin:0px auto; height:330px;">
    <br /><br />
            <table style="width:90%;height:200px;margin:0px auto;">
                <tr id="AuditType" style="height:5px;"> <td style="width:10%;"></td><td style="width:13%; align:left;">Audit Type</td> <td class="style1"><asp:DropDownList ID="cmbAuditType" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="61%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList></td></tr>
                <tr id="GroupName" style="height:5px;"> <td style="width:10%;"></td><td style="width:13%; align:left;">Group Name</td> <td class="style1"><asp:TextBox ID="txtGroup" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="3"  MaxLength="500" />
                        </td></tr>
                <tr id="Group" style="display:none;height:5px;"> <td style="width:10%;"></td><td style="width:13%; align:left;">Group</td> <td class="style1"><asp:DropDownList ID="cmbGroup" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="61%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList></td></tr>
                <tr id="SPM" style="height:5px;"> <td  colspan="2"></td><td>
                    <asp:CheckBox ID="chkSPM" runat="server" />&nbsp;&nbsp;&nbsp; <strong>Whether SPM Related?
                    </strong>
                    </td>
                </tr>
                <tr id="SubGroupName" style="display:none;height:5px;"><td style="width:10%;"></td><td style="width:13%; align:left;">Sub Group Name</td> <td class="style1"><asp:TextBox ID="txtSubGroup" runat="server" style=" font-family:Cambria;font-size:10pt;" Width="61%"  Rows="3"  MaxLength="500" />
                        </td></tr>
                <tr id="SubGroup" style="display:none;height:5px;"> <td style="width:10%;"></td><td style="width:13%; align:left;">Sub Group</td> <td class="style1"><asp:DropDownList ID="cmbSubGroup" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="61%" ForeColor="Black"><asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList></td></tr>
                <tr id="CheckListName" style="display:none;height:5px;"> <td style="width:10%;"></td><td style="width:13%; align:left;">Check List</td> <td class="style1"><asp:TextBox ID="txtCheckList" runat="server" style="resize:none;font-family:Cambria;font-size:10pt;" Width="80%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Rows="2"  MaxLength="500" />
                        <asp:HiddenField ID="hid_dtls" runat="server" />
                        </td></tr>

                        
                <tr>  <td style="text-align:center;" colspan="3"><br /><asp:Panel ID="pnlDtls" style="width:100%; text-align:right;float:right;" runat="server">
            </asp:Panel>
                        <asp:HiddenField ID="hid_tab" runat="server" />
                        </td>
                </tr>
            </table> 
            
    </div>
    <div style="text-align:center;"><br /><input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" 
                            type="button" value="SAVE" onclick="return btnSave_onclick()"  />&nbsp;
                        &nbsp;
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                            type="button" value="EXIT" onclick="return btnExit_onclick()" /></div>
    </div>   
<br /><br />
</asp:Content>

