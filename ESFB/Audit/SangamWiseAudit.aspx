﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="SangamWiseAudit.aspx.vb" Inherits="SangamWiseAudit" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<style type="text/css">
             
     .bg
     {
         background-color:#FFF;
     }
        </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
     <link rel="stylesheet" href="../Style/bootstrap-3.1.1.min.css" type="text/css" />
		<link rel="stylesheet" href="../Style/bootstrap-multiselect.css" type="text/css" />		
        <script type="text/javascript" src="../Script/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../Script/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="../Script/bootstrap-multiselect.js"></script>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    	<script type="text/javascript">
    	    $(document).ready(function () {
    	        $('#chkveg').multiselect();
    	        buttonWidth: '500px'
    	    }); 0
</script>
    <script language="javascript" type="text/javascript">
    var CloseFlag=0;
     $(function() {
			            	        $('#chkveg').multiselect({                                            
			            	            includeSelectAllOption: true
                                        
			            	        });
			            	        $('#btnget').click(function() {
			            	        alert($('#chkveg').val());
			            	        })
			            	    });
        function btnExit_onclick() {
         window.open("AuditStart.aspx", "_self");
        }
        function table_fill() {
            
            var row_bg = 0;
            var tab = "";
              var Dtl=document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ø");
            tab += "<div style='width:100%; height:auto; margin: 0px auto;' class=mainhead>";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr height=30px;>";
                tab += "<td style='width:5%;text-align:center' >#</td>";
                var SelectGroup="<select id='chkveg'  multiple='multiple' style='width:80%; background-color:#B0C4DE' onchange='FilterOnClick()'>";     
                var rrr = Dtl[1].split("Ñ");
                for (a = 2; a < rrr.length-1; a++) {
                    var cols = rrr[a].split("ÿ");
                    SelectGroup+="<option value="+ cols[0] +" selected >"+ cols[1] +"</option>"  ;             
                    }
                SelectGroup+="</select>";            
                tab += "<td style='width:15%;text-align:left' >"+ SelectGroup +"</td>";
                tab += "<td style='width:10%;text-align:left'>Sub Group</td>";
                tab += "<td style='width:20%;text-align:left' >Item</td>";
                tab += "<td style='width:5%;text-align:center' >Serious</td>";
                tab += "<td style='width:5%;text-align:center' >Very Serious</td>";             
                tab += "<td style='width:25%;text-align:left' >Remarks</td>";
                tab += "<td style='width:5%;text-align:left'>Leakage</td>";
                tab += "<td style='width:5%;text-align:left'>Spot</br>Close</td>";
                tab += "<td style='width:5%;text-align:left'>Staff</br>Invlmt.</td>";           
                tab += "</tr>";
                tab += "</table></div>";
                document.getElementById("<%= pnDispHead.ClientID %>").innerHTML = tab;                  
           ItemFill();
          
        }
     
        function ItemFill()
        {
             var tab="";
             var row_bg = 0;
             if(document.getElementById("<%= hid_Items.ClientID %>").value=="")
             {
                 document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none';  
                  document.getElementById("<%= pnDispHead.ClientID %>").style.display = 'none'; 
                 return;
             }
             
             if(document.getElementById("<%= hid_Items.ClientID %>").value=="¥")    
             {
              document.getElementById("<%= pnDispHead.ClientID %>").style.display = '';  
              document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none';  
              return;
             }   
             document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';  
             document.getElementById("<%= pnDispHead.ClientID %>").style.display = '';  
             tab += "<div id='ScrollDiv' style='width:100%; height:274px;overflow: scroll;margin: 0px auto;' class=mainhead >";
             tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='left'>";     
            
               var row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");                               
                for (n = 0; n <= row.length - 1; n++) {
                   var col = row[n].split("µ");
                   if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center;padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second'; style='text-align:center; padding-left:20px;'>";
                    }
                    var i = n + 1;                  
                    if(col[5]!="0")           
                        tab += "<td style='width:5%;text-align:center' ><input type='checkbox' id='chkSelect" + i + "' onclick=SelectOnClick(" + i +   ") checked=true; /></td>";
                    else
                        tab += "<td style='width:5%;text-align:center' ><input type='checkbox' id='chkSelect" + i + "' onclick=SelectOnClick(" + i +   ") /></td>";
                    tab += "<td style='width:15%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:20%;text-align:left'>" + col[3] + "</td>";

                    if (col[5] != "0") {
                        if (col[13] == "S")
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSuspLeakage" + i + "'  disabled=true checked=true;  onclick='chkSuspLeakageOnClick(" + i + ")' /></td>";
                        else
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSuspLeakage" + i + "'  disabled=true onclick='chkSuspLeakageOnClick(" + i + ")' /></td>";
                        if (col[13] == "V")
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + i + "'  disabled=true checked=true;  onclick='chkFraudOnClick(" + i + ")' /></td>";
                        else
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + i + "'  disabled=true onclick='chkFraudOnClick(" + i + ")' /></td>";
                        var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' style='width:99%; float:left;' maxlength='300' onkeypress='return TextAreaCheck(event)'  onblur='return RemarksOnBlur(" + i + ")'>" + col[8] + "</textarea>";
                        if (col[6] == 1)
                            var txtBox1 = "<textarea id='txtLeakage" + i + "' name='txtLeakage" + i + "' type='Text' style='width:99%; height=200px;' class='NormalText' onkeypress='NumericCheck(event)'   disabled=true maxlength='7' >" + parseFloat(col[9]).toFixed(0) + "</textarea>";
                        else
                            var txtBox1 = "<textarea id='txtLeakage" + i + "' name='txtLeakage" + i + "' type='Text' style='width:99%; height=200px;' class='NormalText' onkeypress='NumericCheck(event)'  maxlength='7' >" + parseFloat(col[9]).toFixed(0) + "</textarea>";
                        }
                    else
                    {
                        if (col[13] == "S")
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSuspLeakage" + i + "' checked=true; disabled=true onclick='chkSuspLeakageOnClick(" + i + ")' /></td>";
                        else
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSuspLeakage" + i + "' disabled=true onclick='chkSuspLeakageOnClick(" + i + ")' /></td>";
                        if (col[13] == "V")
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + i + "'  checked=true; disabled=true onclick='chkFraudOnClick(" + i + ")' /></td>";
                        else
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + i + "'  disabled=true onclick='chkFraudOnClick(" + i + ")' /></td>";
                        var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' style='width:99%; float:left;'  maxlength='300' disabled=true onkeypress='return TextAreaCheck(event)'  onblur='return RemarksOnBlur(" + i + ")'></textarea>";
                        var txtBox1 = "<textarea id='txtLeakage" + i + "' name='txtLeakage" + i + "' type='Text' style='width:99%;height=200px;' class='NormalText'  disabled=true onkeypress='NumericCheck(event)'   maxlength='7' ></textarea>";
                        
                        }
                    tab += "<td id='tdRemarks' style='width:25%; text-align:left'>" + txtBox + "</td>";
                    tab += "<td style='width:5%;text-align:left; vertical-align:bottom;'>" + txtBox1 + "</td>";
                    if (col[5] != "0") {
                        if(col[11] == 0)
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSpotClose" + i + "' checked='checked'  /></td>";
                        else
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSpotClose" + i + "' /></td>";
                    }
                    else
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSpotClose" + i + "' disabled=true  /></td>";
                    if (col[5] != "0") {
                        if (col[12] == 1)
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkStaff" + i + "' checked='checked'  /></td>";
                        else if (col[10] == 1 || col[7] == 1)
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkStaff" + i + "' /></td>";
                        else
                            tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkStaff" + i + "' disabled=true  /></td>";
                    }
                    else
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkStaff" + i + "' disabled=true  /></td>";
                    tab += "</tr>";
                
            }
            tab += "</table></div></div></div>";            
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;  
        }
       
        function FilterOnClick()
        {       
          var SelectedItems =$('#chkveg').val();             
          if(SelectedItems!=null)
          {
          var Dtl=document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ø");         
           row = Dtl[0].split("¥");       
           document.getElementById("<%= hid_Items.ClientID %>").value="";         
           for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                  
          if(SelectedItems.indexOf(col[4])>-1){
             document.getElementById("<%= hid_Items.ClientID %>").value+= "¥"+  row[n];
             }
         
          }
           document.getElementById("<%= hid_Items.ClientID %>").value=document.getElementById("<%= hid_Items.ClientID %>").value.substring(1); 
         }
         else
         document.getElementById("<%= hid_Items.ClientID %>").value="¥";
               
         ItemFill();
        }

      
      
          
   function closeOnClick()
    {
        if (document.getElementById("chkCloseAudit").checked == true) {
            if (document.getElementById("<%= hdnSangamVisitCnt.ClientID %>").value > 0 && document.getElementById("<%= hdnAuditTypeID.ClientID %>").value == "4") {
                alert("Sangam Visit Updation is Pending.Closing is only allowed after the updation"); document.getElementById("chkCloseAudit").checked = false; return false;
            } 
            var Comments = document.getElementById("<%= hdnComments.ClientID %>").value.split("Ø");
            document.getElementById("GenComm").style.display = ""; document.getElementById("SerComm").style.display = "";
            document.getElementById("Decl1").style.display = "";
            document.getElementById("Decl2").style.display = "";           
            document.getElementById("txtGeneralComments").value = Comments[0];
            document.getElementById("txtSeriousComments").value = Comments[1];
            if (document.getElementById("<%= hdnAuditTypeID.ClientID %>").value == "1") {
                var Dtl = document.getElementById("<%= hdnCenterCount.ClientID %>").value.split("~");
                Declaration = "I hereby declare that all the disbursements during the audit period (<span style='color:red;'><b>" + Dtl[0] + " centers </b></span>) has been checked. "
                if (Dtl[1] > 0) {
                    Declaration += "</br> Also checked disbursements of <span style='color:red;'><b>" + Dtl[1] + "</b></span>  centers.";
                }
            }
            else {
                var Dtl = document.getElementById("<%= hdnCenterCount.ClientID %>").value.split("~");
                Declaration = "I hereby declare that <span style='color:red;'><b>" + Dtl[0] + "/" + Dtl[2] + "</b></span> new centers and <span style='color:red;'><b>" + Dtl[1] + "/" + Dtl[3] + "</b></span> exisitng centers were visited during this audit.";
            }
            Declaration += "&nbsp;&nbsp;&nbsp;<input id='chkAccept' type='checkbox'/>&nbsp;Accept"
            document.getElementById("<%= pnDeclaration.ClientID %>").innerHTML = Declaration;
        }
        else {
            document.getElementById("GenComm").style.display = "none"; document.getElementById("SerComm").style.display = "none";
            document.getElementById("Decl1").style.display = "none";
            document.getElementById("Decl2").style.display = "none";
           
        }
    }

    
function btnSave_onclick() {
        var CenterID=document.getElementById("<%= cmbSangam.ClientID %>").value;
        var AuditID=document.getElementById("<%= hdnAuditID.ClientID %>").value; 
        var GenComments="";
        var SerComments = "";
        var ManagementNote = ""
        ManagementNote = document.getElementById("<%= txtManagementNote.ClientID %>").value;   
        if(CenterID!="-1")
        {var n=updateValue(); if( n==false ) return false;}
        else
        {document.getElementById("<%= hdnValue.ClientID %>").value=""}
        
        var CloseAudit=(document.getElementById("chkCloseAudit").checked==true) ? 1 :0;
        if (CloseAudit == 1) {
            if (document.getElementById("chkAccept").checked == false) {
                alert("Please accept the declaration.");document.getElementById("chkAccept").focus(); return false;
            }
        if(confirm("Are you sure to close this audit?")==0) {return false;} else
         {
         CloseFlag=1;
         GenComments=document.getElementById("txtGeneralComments").value;
         SerComments=document.getElementById("txtSeriousComments").value;        
         if(GenComments=="")
         {alert("Enter Comments");document.getElementById("txtGeneralComments").focus();return false;}
         if(SerComments=="")
         {if(confirm("Are you sure there is no serious issues in this audit?")==0){document.getElementById("txtSeriousComments").focus();return false;}}
         }
         }
         if (CenterID != -1 && document.getElementById("<%= hdnValue.ClientID %>").value == "") {
             if (confirm("Are You Sure to Remove all the Observations against this sangam?") == true) {
                 var ToData = "1Ø" + AuditID + "Ø" + CenterID + "Ø" + document.getElementById("<%= hdnValue.ClientID %>").value + "Ø" + CloseAudit + "Ø" + GenComments + "Ø" + SerComments + "Ø" + ManagementNote;
                 document.getElementById("btnSave").disabled = true;
                 ToServer(ToData, 1);
             }
         }
         else {
             var ToData = "1Ø" + AuditID + "Ø" + CenterID + "Ø" + document.getElementById("<%= hdnValue.ClientID %>").value + "Ø" + CloseAudit + "Ø" + GenComments + "Ø" + SerComments + "Ø" + ManagementNote;
             document.getElementById("btnSave").disabled = true;
             ToServer(ToData, 1);
         }
        
      }
      
          function FromServer(arg, context) {

              switch (context) {
                  case 1:
                      var Data = arg.split("Ø");
                      document.getElementById("btnSave").disabled = false;
                      alert(Data[1]);
                      if (CloseFlag == 1) {
                          CloseFlag = 0;
                          if (document.getElementById("<%= hdnAuditTypeID.ClientID %>").value == 1) {
                              if (Data[0] == 0 || Data[0] == 2) { window.open("RateABranch.aspx?AuditID=" + btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value) + " &AuditType=4 &TypeID=1", "_self"); }
                              else if (Data[0] == 3) { window.open("RateABranch.aspx?AuditID=" + btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value) + " &AuditType=4 &TypeID=3", "_self"); }
                          }
                          else {
                              if (Data[0] == 0 || Data[0] == 2) { window.open("AuditStart.aspx", "_self"); }
                              else if (Data[0] == 3) { window.open("AuditStaffInvolvement.aspx?AuditID=" + btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value) + " &AuditType=4 ", "_self"); }
                          }
                      }
                      else if (Data[0] == 0 || Data[0] == 2) { document.getElementById("<%= cmbSangam.ClientID %>").value = -1; document.getElementById("<%= hid_Items.ClientID %>").value = ""; ItemFill(); }

                      break;
                  case 2:
                      document.getElementById("<%= hid_Items.ClientID %>").value = arg;
                      var Dtl = document.getElementById("<%= hid_dtls.ClientID %>").value.split("Ø");
                      document.getElementById("<%= hid_dtls.ClientID %>").value = arg + "Ø" + Dtl[1];
                      ItemFill();
                      break;
              }
              }
     function viewReport()
    {
       window.open("Reports/ViewAuditReport.aspx?AuditID="+ btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value) +" &AuditTypeID="+ document.getElementById("<%= hdnAuditTypeID.ClientID %>").value +" &From=3","_self"); 
       return false;
    }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

        function CenterOnChange()
        {
             var AuditID=document.getElementById("<%= hdnAuditID.ClientID %>").value;
             var CenterID = document.getElementById("<%= cmbSangam.ClientID %>").value;
             var AuditType=document.getElementById("<%= hdnAuditTypeID.ClientID  %>").value;
             var ToData = "2Ø" + CenterID + "Ø" + AuditID + "Ø" + AuditType;    
             ToServer(ToData, 2);
        }
        function RemarksOnBlur(val) {
            var textObject = document.getElementById("txtRemarks" + val);
            if (textObject.value == "") {
                alert("Enter Remarks");
                var xxx = "txtRemarks" + val + ".focus();"
                setTimeout(xxx, 1);
                return false;
            }
        }

         function SelectOnClick(val)
            {     
                if(document.getElementById("chkSelect"+val).checked==true)    
                {    
                    
//                    document.getElementById("chkPotential"+val).disabled =false; 
                    document.getElementById("txtRemarks"+val).disabled =false;
                    document.getElementById("txtLeakage" + val).disabled = false;
                    if (document.getElementById("chkFraud" + val).checked == false && document.getElementById("chkSuspLeakage" + val).checked == false)      
                        document.getElementById("chkSpotClose" + val).disabled = false;
                   // document.getElementById("chkStaff" + val).disabled = false;     
                    document.getElementById("txtRemarks" + val).focus();  
                }
                 else
                {
                    
                    document.getElementById("txtRemarks"+val).value="";                
                    document.getElementById("txtRemarks"+val).disabled =true; 
                    document.getElementById("chkSuspLeakage"+val).checked=false;
                    document.getElementById("txtLeakage"+val).value="";
                    document.getElementById("chkSuspLeakage"+val).disabled =true;
                    document.getElementById("txtLeakage" + val).disabled = true;
                    document.getElementById("chkFraud" + val).disabled = true;
                    document.getElementById("chkSpotClose" + val).disabled = true;
                    document.getElementById("chkStaff" + val).disabled = true;    
                }
            }

  function  updateValue()
    {
        document.getElementById("<%= hdnValue.ClientID %>").value="";
        var i = 0;     
        var  row = document.getElementById("<%= hid_Items.ClientID %>").value.split("¥");
         for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    i=n+1;
                    if (document.getElementById("chkSelect" + i).checked == true) {
                        if (document.getElementById("txtRemarks" + i).value == "")
                        { alert("Enter Remarks"); document.getElementById("txtRemarks" + i).focus(); return false; }
                        document.getElementById("<%= hdnValue.ClientID %>").value += "¥" + col[2] + "µ0µ" + document.getElementById("txtRemarks" + i).value + "µ" + ((document.getElementById("chkSuspLeakage" + i).checked == true) ? 1 : 0) + "µ" + Math.abs(document.getElementById("txtLeakage" + i).value) + "µ" + ((document.getElementById("chkFraud" + i).checked == true) ? 1 : 0) + "µ" + ((document.getElementById("chkSpotClose" + i).checked == true) ? 0 : 1) + "µ" + ((document.getElementById("chkStaff" + i).checked == true) ? 1 : 0);
                    }
                    }
                
    }

    </script>
   
</head>
  <table align="center" style="width: 90%; margin:0px auto;">
   <tr><td colspan="4">&nbsp;</td></tr>
       <tr>
            <td style="width:25%; font-weight:bold;" class="mainhead" >
                &nbsp;BRANCH: &nbsp;<asp:Label ID="lblBranch" runat="server"></asp:Label></td>
           
            <td style="width:19%; font-weight:bold;" class="mainhead">
                &nbsp;START ON:&nbsp;<asp:Label ID="lblStartDate" runat="server"></asp:Label></td>
            <td style="width:35%; font-weight:bold;" class="mainhead">
                &nbsp;
            <asp:Label ID="lblAuditType" 
                    runat="server"></asp:Label>
            </td>
            <td style="width:21%; font-weight:bold; text-align:center ;" class="mainhead">
               <asp:ImageButton ID="cmd_viewReport" runat="server" Height="20px" Width="20px" ImageAlign="AbsMiddle" ImageUrl="~/Image/ViewReport.png" ToolTip="View Audit Report" /></td>
           
        </tr>
       <tr>
            <td style="width:25%;"  >
                &nbsp;</td>
           
            <td style="width:19%;">
                Center</td>
            <td style="width:35%; ">
                <asp:DropDownList ID="cmbSangam" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="91%" ForeColor="Black"> <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width:21%; font-weight:bold;">
                &nbsp;</td>
           
        </tr>
       
         <tr>
           <td style="width:100%; text-align:center;" colspan="4">
          
                <asp:Panel ID="pnDispHead" runat="server">
                </asp:Panel>
          
        </td>           
        </tr>
        <tr>
            <td style="width:100%; text-align:center;" colspan="4">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
           
        </tr>
        <tr>
            <td style="width:100%; text-align:center;" colspan="4">
                <input id="chkCloseAudit" title="New" type="checkbox"  onclick="closeOnClick()" style="border-style: none" 
                    />&nbsp;&nbsp;Close Audit</td>
           
        </tr>
        <tr id="GenComm" style="display:none;">
            <td style="width:20%; text-align:center;">
                <asp:HiddenField ID="hdnAuditTypeID" runat="server" />
            </td>
           
            <td style="width:20%; text-align:left;">
                General Comments</td>
           
            <td style="width:60%; text-align:left;" colspan="2">
                <textarea id="txtGeneralComments" cols="20" name="S1" rows="2"   maxlength="1000" onkeypress='return TextAreaCheck(event)' 
                    style="width: 95%"></textarea></td>
           
        </tr>
        <tr id="SerComm" style="display:none;">
            <td style="width:20%; text-align:center;">
                <asp:HiddenField ID="hdnAuditID" runat="server" />
                <asp:HiddenField ID="hdnSangamVisitCnt" runat="server" />
            </td>
           
            <td style="width:20%; text-align:left;">
                Serious Comments</td>
           
            <td style="width:60%; text-align:left;" colspan="2">
                 <textarea id="txtSeriousComments" cols="20" name="S2" rows="2"  maxlength="1000" onkeypress='return TextAreaCheck(event)' 
                    style="width: 95%"></textarea></td>
           
        </tr>
          <tr >
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:20%" >
                Management Note</td>
            <td style="width:60%" colspan="3">
                 <asp:TextBox ID="txtManagementNote" runat="server" Rows="5" 
                     TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Width="95%" maxlength=5000></asp:TextBox>
              </td>
        </tr>
        <tr id="Decl1" style="display:none;">
            <td style="width:20%; text-align:center;" colspan="4" class="mainhead">
               DECLARATION</td>                    
        </tr>
        <tr id="Decl2" style="display:none;" >
            <td style="width:20%; text-align:center;"  colspan="4">
                <asp:Panel ID="pnDeclaration" runat="server">
                </asp:Panel>
                </td>        
        </tr>
        <tr>
            <td style="width:100%; text-align:center;" colspan="4">
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hdnValue" runat="server" />
                <asp:HiddenField ID="hid_Items" runat="server" />
                <asp:HiddenField ID="hdnComments" runat="server" />
                <asp:HiddenField ID="hdnCenterCount" runat="server" />
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE"   onclick="return btnSave_onclick()" /><input id="btnExit" style="font-family: cambria; cursor: pointer; width:67px;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
          
        </tr>
    </table>
 <br />   
</asp:Content>

