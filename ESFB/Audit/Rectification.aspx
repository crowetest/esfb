﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Rectification.aspx.vb" Inherits="Rectification" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }  
        .sub_hdRow
        {
         background-color:#EBCCD6; height:20px;
         font-family:Arial; color:#B84D4D; font-size:8.5pt;  font-weight:bold;
        }      
        .style1
        {
            width: 100%;
        }
    </style>
    <script language="javascript" type="text/javascript">
     
       function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

          function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }

       
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

   function FromServer(arg, context)
    {        
         switch (context) 
         {
                case 1:
                    var Data=arg.split("Ø");
                    alert(Data[1]);      
                    document.getElementById("btnSave").disabled =false ;            
                    if(Data[0]==0) {window.open("Rectification.aspx","_self");}
                    break;            
                 case 2:      
                  var Dtl=arg.split("Ø");   
                  var TypeID=Dtl[0];       
                  document.getElementById("<%= hid_dtls.ClientID %>").value=Dtl[1];
                  table_fill(TypeID,document.getElementById("<%= hid_dtls.ClientID %>").value);
                                 
                  break;     
                 case 3:
                    ComboFill(arg, "<%= cmbCategory.ClientID %>");
                break;          
                default:
                 break;        
         }            
    }
        function AuditOnChange()
        {
                 
          var AuditID=document.getElementById("<%= cmbAudit.ClientID %>").value;
          if(AuditID!=-1){
            var ToData = "3Ø" + AuditID;                           
            ToServer(ToData, 3);
          }
          
        }
          function CategoryOnChange()
        {        
            var AuditID=document.getElementById("<%= cmbCategory.ClientID %>").value;
            var ToData = "2Ø" + AuditID;                           
            ToServer(ToData, 2);
        }
        function table_fill(Type,arg) 
        {      
        if(arg=="")
        {
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none';
            
        }
        else
        {
         if(Type==1)
         {
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:25px; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:Arial;padding-top:5px;' align='center'>";
            tab += "<tr >";           
            tab += "<td style='width:3%;text-align:center;  font-size:9pt;'><b>#</b></td>";
            tab += "<td style='width:0%;text-align:center;  font-size:9pt;display:none;'><b>#</b></td>";
            tab += "<td style='width:9%;text-align:left;  font-size:9pt;'><b>REF NO</b></td>";     
            tab += "<td style='width:13%;text-align:left;  font-size:9pt;'><b>CUSTOMER</b></td>";
            tab += "<td style='width:8%;text-align:left; font-size:9pt;'><b>CENTER</b></td>";   
            tab += "<td style='width:8%;text-align:left; font-size:9pt;'><b>LOAN DT</b></td>";  
            tab += "<td style='width:5%;text-align:center; font-size:9pt;'><b>AMT&nbsp;&nbsp;</b></td>";   
            tab += "<td style='width:17%;text-align:left; font-size:9pt;'><b>CHECK LIST</b></td>";    
            tab += "<td style='width:15%;text-align:left; font-size:9pt;'><b>PREVIOUS REMARKS</b></td>";   
            tab += "<td style='width:8%;text-align:left; font-size:9pt;'><b>STATUS</b></td>";  
            tab += "<td style='width:14%;text-align:left; font-size:9pt;'><b>REMARKS</b></td>";             
            tab += "</tr>";      
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:338px; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>"; 
            if (arg != "") {

                row = arg.split("¥");
               

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_first';>";
                    }
                    i = n + 1;
                  
                                  
                    tab += "<td style='width:3%;text-align:center' >"+ i +"</td>";
                    tab += "<td style='width:0%;text-align:center;display:none;' >"+ col[14] +"</td>";
                    tab += "<td style='width:9%;text-align:left'>" + col[7] + "</td>";                   
                    tab += "<td style='width:13%;text-align:left'>" + col[10] + "</td>"; 
                    tab += "<td  align=left style='width:8%;' >" + col[13] + "</td>";  
                    tab += "<td  align=left style='width:8%;' >" + col[11] + "</td>"; 
                    tab += "<td  align=left style='width:5%;text-align:center;' >" + parseFloat(col[12]).toFixed(0)+"&nbsp;&nbsp;" + "</td>"; 
                    tab += "<td style='width:17%;text-align:left'>"+ col[6] +"</td>"; 
                    tab += "<td style='width:15%;text-align:left'>"+ col[9] +"<br/><a href='Reports/viewPrevRemarksReport.aspx?AuditID="+ btoa(col[14]) +"' style='text-align:right;' target='_blank' >Previous</a></td>"; 
                     var select = "<select id='cmb" + i + "' name='cmb" + i + "' >";
                     select += "<option value=0>---Select---</option>";
                        select += "<option value=1>Rectify</option>";
                        select += "<option value=2>Explanation</option>";
                    tab += "<td style='width:8%;text-align:center'>"+ select +"</td>";
                     var txtVal ="<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' type='Text' style='width:99%;'  maxlength='300'  onkeypress='return TextAreaCheck(event)' ></textarea>";
                tab += "<td style='width:14%;text-align:left'>"+ txtVal +"</td>";                        
                    tab += "</tr>";   
                                  
                }
            }
            tab += "</table></div>";
            }
            else
            {
                 var row_bg = 0;
                document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
               
                var row_bg = 0;
                var tab = "";
                tab += "<div style='width:100%; height:25px; overflow:auto;margin: 0px auto;' class=mainhead>";
                tab += "<table style='width:100%;margin:0px auto;font-family:Arial;padding-top:5px;' align='center'>";
                tab += "<tr >";           
                tab += "<td style='width:5%;text-align:center; '><b>#</b></td>";
                tab += "<td style='width:0%;text-align:center; display:none;'><b>#</b></td>";
                tab += "<td style='width:15%;text-align:left;'><b>Group</b></td>";     
                tab += "<td style='width:20%;text-align:left; '><b>Sub Group</b></td>";
                tab += "<td style='width:15%;text-align:left;'><b>Check List</b></td>";   
                tab += "<td style='width:17%;text-align:left;'><b>Previous Remarks</b></td>";   
                tab += "<td style='width:8%;text-align:left;'><b>Status</b></td>";
                tab += "<td style='width:20%;text-align:left; '><b>Remarks</b></td>";             
                tab += "</tr>";      
                tab += "</table></div>";  
                tab += "<div id='ScrollDiv' style='width:100%; height:325px; overflow:auto;margin: 0px auto;' class=mainhead>";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>"; 
                if (arg != "") {

                row = arg.split("¥");             
                for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");           
               
                 if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second';>";
                    }
               
                i = n + 1;
                var type="No";
                if(col[7]==1)   
                type="Yes";               
                tab += "<td style='width:5%;text-align:center' >"+ i +"</td>";
                tab += "<td style='width:0%;text-align:center;display:none;' >"+ col[9] +"</td>";
                tab += "<td style='width:15%;text-align:left'>" + col[2] + "</td>";                   
                tab += "<td style='width:20%;text-align:left'>" + col[4] + "</td>"; 
                tab += "<td  align=left; style='width:15%;' >" + col[6] + "</td>";  
                             
                tab += "<td style='width:17%;text-align:left'>"+ col[8] +"<br/><a href='Reports/viewPrevRemarksReport.aspx?AuditID="+ btoa(col[9]) +"' style='text-align:right;' target='_blank' >Previous</a></td>"; 
                 var select = "<select id='cmb" + i + "' name='cmb" + i + "' >";
                     select += "<option value=0>---Select---</option>";
                        select += "<option value=1>Rectify</option>";
                        select += "<option value=2>Explanation</option>";
                    tab += "<td style='width:8%;text-align:center'>"+ select +"</td>"; 
                    var txtVal ="<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' type='Text' style='width:99%;'  maxlength='300' onkeypress='return TextAreaCheck(event)'  ></textarea>";
                tab += "<td style='width:20%;text-align:left'>"+ txtVal +"</td>";                        
                tab += "</tr>";              
                }
                }
                tab += "</table></div>";
            }

           
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;
            }
            //--------------------- Clearing Data ------------------------//
        } 
        function FinalStringForSave() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            
                     var Dtl=document.getElementById("<%= cmbCategory.ClientID %>").value.split("Ø");   
                  var TypeID=Dtl[1];
                if (TypeID==3){
                    row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                            i = n + 1;            
                        if (document.getElementById("cmb" + i).value != 0) {

                            document.getElementById("<%= hid_temp.ClientID %>").value +=  col[9] + "µ" +document.getElementById("cmb" + i).value+ "µ" +document.getElementById("txtRemarks" + i).value + "µ"+ col[10] + "¥";
                        }
                    }

                   
                }
                else{
                    row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                            i = n + 1;            
                        if (document.getElementById("cmb" + i).value != 0) {
                            

                            document.getElementById("<%= hid_temp.ClientID %>").value +=  col[14] + "µ" + document.getElementById("cmb" + i).value+ "µ" +document.getElementById("txtRemarks" + i).value + "µ"+ col[15] +"¥";
                        }
            }

                }    
                
            
          return true;
        }

         function Validation() {
            
                    if (document.getElementById("<%= hid_dtls.ClientID %>").value==""){
                            alert("No data exists");
                            return false;
                    }
                    row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                            i = n + 1;            
                        if (document.getElementById("cmb" + i).value != 0 && document.getElementById("txtRemarks" + i).value=='') {
                        document.getElementById("txtRemarks" + i).focus();
                            alert("Enter Remarks");
                            return false;
                        
                    }

                
                }    
            
          return true;
        }
function btnSave_onclick() {           
            if (document.getElementById("<%= cmbAudit.ClientID %>").value == "") 
            {
                alert("Select Audit");
                document.getElementById("<%= cmbAudit.ClientID %>").focus();
                return false;
            }
           
            
          
            if (document.getElementById("<%= cmbCategory.ClientID %>").value == "-1") 
            {
                alert("Select Category");
                document.getElementById("<%= cmbCategory.ClientID %>").focus();
                return false;
            }
           
           if (Validation() == false){ 
           return false;
           }
            FinalStringForSave();            
             if (document.getElementById("<%= hid_temp.ClientID %>").value == "") {
                alert("Select Atleast one Rectification");
                return false;
            }
             document.getElementById("btnSave").disabled =true;
            var Dtl=document.getElementById("<%= cmbCategory.ClientID %>").value.split("Ø");   
                  var TypeID=Dtl[1];                 
	        var ToData = "1Ø" + document.getElementById("<%= hid_temp.ClientID %>").value + "Ø" +TypeID;	       
            ToServer(ToData, 1);
          
}

    </script>
</head>
</html>
<asp:HiddenField ID="hid_dtls" runat="server" />
<br />
    <table class="style1" style="width:100%">        
        <tr> 
            <td style="text-align:center; margin:0px auto; width:30%;">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td> 
            <td style="text-align:left; margin:0px auto; width:5%;">
                Audit</td> 
            <td style="text-align:left; margin:0px auto; width:65%;">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1">---------Select---------</asp:ListItem>
                </asp:DropDownList></td></tr>
        
       
        <tr> 
            <td style="text-align:center; margin:0px auto; width:30%;">
                &nbsp;</td> 
            <td style="text-align:left; margin:0px auto; width:5%;">
                Category</td> 
            <td style="text-align:left; margin:0px auto; width:65%;">
                <asp:DropDownList ID="cmbCategory" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="50%" ForeColor="Black">
                    <asp:ListItem Value="-1">---------Select---------</asp:ListItem>
                </asp:DropDownList></td></tr>
      
        
        <tr> 
            <td colspan="3" style="text-align:center; margin:0px auto;">
                &nbsp;</td></tr>
        
        <tr id="RowNew"> 
            <td colspan="3" >
              <asp:Panel ID="pnDisplay" runat="server" Width="90%" style="text-align:center; margin:0px auto;">                           
            </asp:Panel></td>    </tr>

                  <tr id="RowComments" style="display:none;"> 
            <td  colspan="3">
               
<asp:HiddenField ID="hid_temp" runat="server" />
               
                      </td> 
         </tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <br />
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="SAVE"  onclick="return btnSave_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>   
    <br /> <br /> <br />
</asp:Content>

