﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditCenterVisit.aspx.vb" Inherits="AuditCenterVisit" EnableEventValidation="false"  %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:5%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 100%;
	        border-top-right-radius: 100%;
	        border-bottom-left-radius: 100%;
	        border-bottom-right-radius: 100%;
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #476C91 0%, #B0C4DE 0%, #476C91 100%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }        
        .sub_hdRow
        {
         background-color:#FFFFCC; height:20px;
         font-family:Arial; color:#B84D4D; font-size:8.5pt;  font-weight:bold;
        }    
        .hdRow
        {
         background-color:#FFF; height:20px;
         font-family:Arial; color:#EBCCD6; font-size:8.5pt;  font-weight:bold;
        }        
        .style1
        {
            width: 50%;
        }
    </style>
    <script language="javascript" type="text/javascript">

      function FromServer(arg, context) {
      document.getElementById("btnSave").disabled = false; 
         switch (context) {
             case 1:   
                var Data = arg.split("Ø");
                alert(Data[1]);
                document.getElementById("<%= hid_Display.ClientID %>").value="";
                AuditOnChange();
                 break;            
                
             case 2:
             if(arg != "µµµ")
             {
               
               document.getElementById("<%= hid_Display.ClientID %>").value= arg;            
               FillEmpDetails();
             }
             else
             {
                alert("Invalid Request");document.getElementById("<%= cmbAudit.ClientID %>").focus();
             }
             break;
             case 3:
                  ComboFill(arg, "<%= cmbAudit.ClientID %>");
                  break;
              case 4:                 
                  document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                  table_fill();
                  break;
            default:
            break;
        
            }
            
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
         
   
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        

         function FillEmpDetails() {  
                    var row_bg = 0;
                    var tab = "";
                    tab += "<table style='width:100%;font-family:cambria;frame=box ;border: thin solid #C0C0C0';>";
                    tab += "<tr  class=mainhead>";
                    tab += "<td style='width:5%;text-align:center'>SI No</td>";
                    tab += "<td style='width:15%;text-align:center'>Sangam ID</td>";
                    tab += "<td style='width:25%;text-align:left' >Sangam Name</td>";
                    tab += "<td style='width:15%;text-align:left' >Meeting Day</td>";
                    tab += "<td style='width:30%;text-align:left' >Visited Person</td>";
                    tab += "<td style='width:10%;text-align:left' ></td>";                    
                    tab += "</tr>";
                    var i=1;
                    if (document.getElementById("<%= hid_Display.ClientID %>").value!="")
                    {
                     
                    document.getElementById("rowSangam").style.display='';
                    row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
                   
                            for (n = 0; n < row.length; n++) {                   
                                col = row[n].split("µ");
                                if (row_bg == 0) {
                                    row_bg = 1;
                                    tab += "<tr class=sub_first>";
                                }
                                else {
                                    row_bg = 0;
                                    tab += "<tr class=sub_second>";
                                }
                                tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                                tab += "<td style='width:15%;text-align:left'>" + col[0] + "</td>";
                                tab += "<td style='width:25%;text-align:left'>" + col[1] + "</td>";
                                tab += "<td style='width:15%;text-align:left'>" + col[2] + "</td>";
                                tab += "<td style='width:30%;text-align:left'>" + col[4] + "</td>";
                                
                                var txtid = "chk" + col[0];
                                if (col[3] != "")
                                {
                                    if (col[3] != document.getElementById("<%= hdnEmpCode.ClientID %>").value) 
                                    tab += "<td style='width:10%;text-align:center'><input type='checkbox' id='" + txtid + "' checked=true disabled=true /></td>";
                                    else
                                    tab += "<td style='width:10%;text-align:center'><input type='checkbox' id='" + txtid + "' checked=true disabled=true /></td>";
                                    }
                                else
                                { 
                                    tab += "<td style='width:10%;text-align:center'><input type='checkbox' id='" + txtid + "'  /></td>";
                                }
                                tab += "</tr>";  
                                i+=1;                    
                            }
                    
                           
                        tab += "</table>";
                      }
                      else{

                         document.getElementById("rowSangam").style.display='none';
                      }
                      
                    document.getElementById("<%= pnEmpDetails.ClientID %>").innerHTML = tab;
                    
                    
                    //--------------------- Clearing Data ------------------------//


                }
        function SelectOnClick(val)
            { 
//  if (document.getElementById("chkSelect"+val).checked==true ){
//                        document.getElementById("chkSelect"+val).checked =true;
//                        
//                    }     
               
                                
            }          
                
       function btnSave_onclick() {  
      
        var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;
        var SangamDtl = "";
        row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");            
        for (n = 0; n < row.length; n++) {
             col = row[n].split("µ");
            
             var txtid = "chk" + col[0];
             
             
            if(document.getElementById("chk" + col[0]).checked == true && col[3] == "")            
            { 
                SangamDtl = SangamDtl + "¥"+ col[0] + "µ" + 1;
            }
            else if (document.getElementById("chk" + col[0]).checked == false && col[3] == document.getElementById("<%= hdnEmpCode.ClientID %>").value)
            { 
                 SangamDtl += "¥"+ col[0] + "µ" + 0;
            }
           }
        if (AuditID == -1){  alert("Select Audit ");
            return false;}   
        if ( SangamDtl == "" ) {
            alert("Select Atleast one Sangam ");
            return false;
        }
        
       

	    var ToData = "1Ø" + SangamDtl + "Ø" + AuditID;
        document.getElementById("btnSave").disabled = true; 
	    ToServer(ToData, 1);
    }

      function BranchOnChange() {
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
           
            if (BranchID == -1)
               { ClearCombo("<%= cmbAudit.ClientID %>");
                document.getElementById("rowSangam").style.display='none';
                }
            else {
                var ToData = "3Ø" + BranchID;
                ToServer(ToData, 3);
            }
        }     
          
 function AuditOnChange() {
            var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;
            if (AuditID != -1)
              {
               document.getElementById("rowSangam").style.display='none';
                var ToData = "2Ø" + AuditID;
                ToServer(ToData, 2);
             }
            
        }     
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hdnAuditID" runat="server" />
                <asp:HiddenField ID="hdnEmpCode" runat="server" />
    <asp:HiddenField 
                    ID="hid_Display" runat="server" />
<br />

    <table class="style1" style="width:100%">
          <tr>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                Branch&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:50%">
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black"> <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
          <tr>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                Audit&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:50%">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black"> <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
          <tr>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                &nbsp;</td>
            <td style="text-align:left; width:50%">
                &nbsp;</td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
        
        
        <tr id="rowSangam" > 
            <td style="text-align:center;" colspan="4">
                <table align="center" class="style1" style="margin: 0px auto; text-align:center; width:70%;">
                    <tr>
                        <td style="width:40%;text-align:center;" class="mainhead">
                            SANGAM DETAILS</td>
                   
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            <asp:Panel ID="pnEmpDetails" runat="server">
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td></tr>
      
        <tr>
            <td style="text-align:center;" colspan="4">
                <br />
                &nbsp;<input id="btnSave" style="font-family: cambria; cursor: pointer; width:6%;" 
                type="button" value="SAVE"   onclick="return btnSave_onclick()" 
                     />
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>   

</asp:Content>

