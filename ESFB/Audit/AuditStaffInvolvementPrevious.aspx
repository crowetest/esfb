﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditStaffInvolvementPrevious.aspx.vb" Inherits="Audit_AuditStaffInvolvementPrevious" EnableEventValidation="false"  %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
           width:5%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 100%;
	        border-top-right-radius: 100%;
	        border-bottom-left-radius: 100%;
	        border-bottom-right-radius: 100%;
            cursor:pointer;
             background: -moz-radial-gradient(center, ellipse cover, #476C91 0%, #B0C4DE 0%, #476C91 100%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }        
        .sub_hdRow
        {
         background-color:#FFFFCC; height:20px;
         font-family:Arial; color:#B84D4D; font-size:8.5pt;  font-weight:bold;
        }    
        .hdRow
        {
         background-color:#FFF; height:20px;
         font-family:Arial; color:#EBCCD6; font-size:8.5pt;  font-weight:bold;
        }        
        .style1
        {
            width: 50%;
        }
    </style>
    <script language="javascript" type="text/javascript">

      function FromServer(arg, context) {

         switch (context) {
             case 1:   
                var Data = arg.split("Ø");
                alert(Data[1]);
                document.getElementById("<%= hid_Display.ClientID %>").value="";
                AuditOnChange();
                 break;            
                
             case 2:
             if(arg!="Ø")
             {
               var Dtl=arg.split("Ø");
               document.getElementById("<%= hid_Display.ClientID %>").value+="¥"+Dtl[0]+"µ"+Dtl[1]+"µ"+Dtl[2]+"µ"+Dtl[3]+"µ"+Dtl[4]+"µµ0µ0";            
              FillEmpDetailsRemark();
             }
             else
             {
                alert("Invalid Emp Code");document.getElementById("txtEmpCode").select();
             }
             break;
             case 3:
                  ComboFill(arg, "<%= cmbAudit.ClientID %>");
                  break;
              case 4:                 
                  document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                  table_fill();
                  break;
            default:
            break;
        
            }
            
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
          function FillEmpDetailsRemark() {              
                    var row_bg = 0;
                    var tab = "";
                    tab += "<table style='width:100%;font-family:cambria;frame=box ;border: thin solid #C0C0C0';>";
                    tab += "<tr  class=mainhead>";
                    tab += "<td style='width:5%;text-align:center'>SI No</td>";
                    tab += "<td style='width:5%;text-align:center'>Emp Code</td>";
                    tab += "<td style='width:15%;text-align:left' >Name</td>";
                    tab += "<td style='width:15%;text-align:left' >Location</td>";
                    tab += "<td style='width:15%;text-align:left' >Department</td>";
                    tab += "<td style='width:15%;text-align:left' >Designation</td>";
                    tab += "<td style='width:15%;text-align:left' >Remarks</td>";
                    tab += "<td style='width:5%;text-align:Right' >Intentionally</td>";
                    tab += "<td style='width:5%;text-align:Right' >Amount</td>";                    
                    tab += "<td style='width:5%;text-align:left' ></td>";                    
                    tab += "</tr>";
                    var i=1;
                    if (document.getElementById("<%= hid_Display.ClientID %>").value!="")
                    {
                    row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
                    var txtBoxName = "";
                    var txtBoxName1 = "";
                    for (n = 1; n < row.length; n++) {                   
                        col = row[n].split("µ");
                        if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class=sub_first>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class=sub_second>";
                        }
                        tab += "<td style='width:5%;text-align:center'><small>" + i + "</small></td>";
                        tab += "<td style='width:5%;text-align:left'><small>" + col[0] + "</small></td>";
                        tab += "<td style='width:15%;text-align:left'><small>" + col[1] + "</small></td>";
                        tab += "<td style='width:15%;text-align:left'><small>" + col[2] + "</small></td>";
                        tab += "<td style='width:15%;text-align:left'><small>" + col[3] + "</small></td>";
                        tab += "<td style='width:15%;text-align:left'><small>" + col[4] + "</small></td>";
                        if(n==row.length-1)
                        {
                            var txtBox = "<textarea id='txtRemarks' name='txtRemarks' type='Text' style='width:99%;border:none;'  maxlength='300' onkeypress='return TextAreaCheck(event)'  onblur='updateValue(2)' ></textarea>";
                            tab += "<td style='width:15%;text-align:left'><small>"+ txtBox +"</small></td>";
                            tab += "<td style='width:5%;text-align:center'><input type=checkbox id='chkintent' onchange='updateValue(2)'></td>";
                            var txtBox = "<input id='txtAmt' name='txtAmt' type='Text' style='width:100%;border:none;' class='NormalText' onkeypress='return NumericCheck(event)' onblur='updateValue(3)' />";
                            tab += "<td style='width:5%;text-align:left'><small>"+ txtBox +"</small></td>";
                        }
                        else
                        {                         
                          tab += "<td style='width:15%;text-align:left'><small>" + col[5] + "</small></td>";
                          var Intently="";
                          if(col[7]==1)
                            Intently="Yes";                         
                          tab += "<td style='width:5%;text-align:center'><small>" + Intently + "</small></td>";
                          tab += "<td style='width:5%;text-align:left'><small>" + col[6] + "</small></td>";
                           
                        }
                        
                        tab += "<td style='width:5%;text-align:center' onclick='delete_row(" + n + ")'><small> <img  src='../Image/cross.png'; style='align:middle;cursor:hand' ></small></td>";
                        tab += "</tr>";  
                        i+=1;                    
                    }
                    }                      
                        tab += "</table>";                      
                    document.getElementById("<%= pnEmpDetails.ClientID %>").innerHTML = tab;
                    document.getElementById("txtRemarks").focus();
                    
                    //--------------------- Clearing Data ------------------------//


                }
    function ViewReport(AuditID,AuditType)
    {
       window.open("Reports/viewAuditSamplingReport.aspx?AuditID="+ btoa(AuditID) +" &AuditTypeID="+ AuditType,"_self"); 
       return false;
    }
        function table_fill() 
        {           
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var ItemName="";
            var AuitType=0
            tab += "<div style='width:70%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:scroll; height:275px; text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr >";           
            tab += "<td style='width:5%;text-align:center' >SI No</td>";
            tab += "<td style='width:12%;text-align:left' >Ref No</td>";
            tab += "<td style='width:15%;text-align:left'>Center</td>";     
            tab += "<td style='width:15%;text-align:left'>Customer</td>"; 
            tab += "<td style='width:28%;text-align:left'>Remarks</td>";          
            tab += "<td style='width:10%;text-align:center'>Type</td>";
            tab += "<td style='width:10%;text-align:right'>Est. Amt Involved</td>";
            tab += "<td style='width:5%;text-align:center'></td>";
            tab += "</tr>";             
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                document.getElementById("rowObserv").style.display=''
                document.getElementById("rowStaff").style.display=''
                 document.getElementById("btnSave").disabled=false;
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
             var i=0;
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    i+=1;
                    if (AuitType!=col[9])
                    {
                           tab += "<tr class='hdRow'; style='height:25px;text-align:center; padding-left:20px;'>";
                           tab += "<td style='width:5%;text-align:center' colspan=8 >" + col[8] + "</td></tr>";
                    }
                    if (ItemName!=col[2])
                    {
                           tab += "<tr class='sub_hdRow'; style='height:25px;text-align:center; padding-left:20px;'>";
                           tab += "<td style='width:5%;text-align:left' colspan=8 >" + col[2] + "</td></tr>";
                    }
                    ItemName=col[2];
                    AuitType=col[9];
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='height:25px;text-align:center; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second'; style='height:25px;text-align:center; padding-left:20px;'>";
                    }
                    i = n + 1;
                    //EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time                    
                    tab += "<td style='width:5%;text-align:center' >" + i + "</td>";
                    tab += "<td style='width:12%;text-align:left' >" + col[3] + "</td>";
                    tab += "<td style='width:15%;text-align:left'>" + col[5] + "</td>";                   
                    tab += "<td style='width:15%;text-align:left'>" + col[4] + "</td>"; 
                    tab += "<td style='width:28%;text-align:left'>" + col[1] + "</td>"; 
                    tab += "<td style='width:10%;text-align:center'>" + col[6] + "</td>"; 
                    if(parseFloat(col[7])==0)
                    {
                    tab += "<td style='width:10%;text-align:right'></td>"; }
                    else
                    tab += "<td style='width:10%;text-align:right'>" + parseFloat(col[7]).toFixed(2) + "</td>";
                    tab += "<td style='width:5%; text-align:center; '> <input id='chk"+  i +"' type='checkbox' onclick='CheckBoxOnClick("+ i +")' /></td>";                       
                    tab += "</tr>";
                }
                 tab += "</table></div></div></div>";            
                 document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
                 FillEmpDetails();
            }
            else
            {
                alert("No Observations Found");
                document.getElementById("rowObserv").style.display='none';
                document.getElementById("rowStaff").style.display='none';
                document.getElementById("btnSave").disabled=true;

            }
          
            //--------------------- Clearing Data ------------------------//


        }
        
       function CheckBoxOnClick(ID)
       {
              var Flg=false;
              if(document.getElementById("chk"+ID).checked==true)
                Flg=true;
              row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
              var i=0;
                for (n = 0; n <= row.length - 1; n++) {   
                        i=n+1;                              
                        document.getElementById("chk"+i).checked=false;
                    }
               document.getElementById("chk"+ID).checked=Flg;
       }
       
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function AddOnClick()
        {
        }

         function FillEmpDetails() {              
                    var row_bg = 0;
                    var tab = "";
                    tab += "<table style='width:100%;font-family:cambria;frame=box ;border: thin solid #C0C0C0';>";
                    tab += "<tr  class=mainhead>";
                    tab += "<td style='width:5%;text-align:center'>SI No</td>";
                    tab += "<td style='width:5%;text-align:center'>Emp Code</td>";
                    tab += "<td style='width:15%;text-align:left' >Name</td>";
                    tab += "<td style='width:15%;text-align:left' >Location</td>";
                    tab += "<td style='width:15%;text-align:left' >Department</td>";
                    tab += "<td style='width:15%;text-align:left' >Designation</td>";
                    tab += "<td style='width:15%;text-align:left' >Remarks</td>";
                    tab += "<td style='width:5%;text-align:left' >Intentionally</td>";
                    tab += "<td style='width:5%;text-align:left' >Amount</td>";                    
                    tab += "<td style='width:5%;text-align:left' ></td>";                    
                    tab += "</tr>";
                    var i=1;
                    if (document.getElementById("<%= hid_Display.ClientID %>").value!="")
                    {
                     document.getElementById("rowObserv").style.diplay='';
                     document.getElementById("rowStaff").style.diplay='';
                    row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
                    var txtBoxName = "";
                    var txtBoxName1 = "";
                    for (n = 1; n < row.length; n++) {                   
                        col = row[n].split("µ");
                        if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class=sub_first>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class=sub_second>";
                        }
                        tab += "<td style='width:5%;text-align:center'><small>" + i + "</small></td>";
                        tab += "<td style='width:5%;text-align:left'><small>" + col[0] + "</small></td>";
                        tab += "<td style='width:15%;text-align:left'><small>" + col[1] + "</small></td>";
                        tab += "<td style='width:15%;text-align:left'><small>" + col[2] + "</small></td>";
                        tab += "<td style='width:15%;text-align:left'><small>" + col[3] + "</small></td>";
                        tab += "<td style='width:15%;text-align:left'><small>" + col[4] + "</small></td>";
                        tab += "<td style='width:15%;text-align:left'><small>" + col[5] + "</small></td>";
                        var Intently="";
                        if(col[7]==1)
                            Intently="Yes";
                        tab += "<td style='width:5%;text-align:center'><small>"+ Intently +"</small></td>";
                        tab += "<td style='width:5%;text-align:left'><small>" + col[6] + "</small></td>";
                        
                        tab += "<td style='width:5%;text-align:center' onclick='delete_row(" + n + ")'><small> <img  src='../Image/cross.png'; style='align:middle;cursor:hand' ></small></td>";
                        tab += "</tr>";  
                        i+=1;                    
                    }
                    }
                      if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class=sub_first>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class=sub_second>";
                        }
                        tab += "<td style='width:5%;text-align:center'><small>" + i + "</small></td>";
                        txtBoxName = "txtEmpCode";
                        var txtBox = "<input id='" + txtBoxName + "' name='" + txtBoxName + "' type='Text' style='width:100%; border:none;' class='NormalText'  onblur='updateValue(1)' />";
                        tab += "<td style='width:5%;text-align:left'><small>" + txtBox + "</small></td>";
                        tab += "<td style='width:15%;text-align:left'><small></small></td>";
                        tab += "<td style='width:15%;text-align:left'><small></small></td>";
                        tab += "<td style='width:15%;text-align:left'><small></small></td>";
                        tab += "<td style='width:15%;text-align:left'><small></small></td>";
                        tab += "<td style='width:15%;text-align:left'><small></small></td>";
                        tab += "<td style='width:5%;text-align:left'><small></small></td>";
                        tab += "<td style='width:5%;text-align:left'><small></small></td>";                       
                        tab += "<td style='width:5%;text-align:center'><small></small></td></tr>";
                        tab += "</table>";
                      
                    document.getElementById("<%= pnEmpDetails.ClientID %>").innerHTML = tab;
                    document.getElementById(txtBoxName).focus();
                    
                    //--------------------- Clearing Data ------------------------//


                }
                  function delete_row(a) 
                {
                    var data = "";                    
                    var rows = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
                    var len = Math.abs(rows.length);
                    for (n = 1; n <= rows.length - 1; n++) {
                        if (a != (n)) {
                            data =data+"¥"+ rows[n];
                           
                        }                       
                    }
                    document.getElementById("<%= hid_Display.ClientID %>").value = data;
                    //alert(document.getElementById("<%= hid_Display.ClientID %>").value);
                    FillEmpDetails();
                }
                 function updateValue(ID) {
                     if(ID==1)   
                     {
                        var EmpCode=document.getElementById("txtEmpCode").value;               
                        if(EmpCode!="")
                        {
                            row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
                            for (n = 1; n < row.length; n++) {                   
                            col = row[n].split("µ");
                            if(EmpCode==col[0])
                            {alert("Already Added");document.getElementById(ID).select();;return false}
                            }
                        var Data = "2Ø" + EmpCode;
                        ToServer(Data, 2);
                        }
                     }
                     else 
                     {
                         var Remarks=document.getElementById("txtRemarks").value;
                         var Amount=document.getElementById("txtAmt").value;  
                         var Intently=0;
                         if(document.getElementById("chkintent").checked==true)
                            Intently=1;
                         row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");
                         var NewStr="";
                           for (n = 1; n <= row.length-1; n++) {                  
                          
                            if(n<(row.length-1))
                            { 
                                NewStr+="¥"+row[n];
                                }
                            else
                            {
                              col = row[n].split("µ");
                              NewStr+="¥"+col[0]+"µ"+col[1]+"µ"+col[2]+"µ"+col[3]+"µ"+col[4]+"µ"+Remarks+"µ"+Amount+"µ"+Intently;
                            }
                            }
                            document.getElementById("<%= hid_Display.ClientID %>").value=NewStr;
                         if(Remarks!="" && Amount!="") 
                            FillEmpDetails();  
                     }
                     
                    }
       function btnSave_onclick() {    
        var Sino=0
        var Leakage=0
        var StaffDtl =""
        var i=0
        row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");            
        for (n = 0; n <= row.length - 1; n++) {
             col = row[n].split("µ");
             i=n+1
            if(document.getElementById("chk"+i).checked==true)            
            {
                Sino=col[0];
                Leakage=parseFloat(col[7]);
            }
           }
        if (Sino =="" ) {
            alert("Select  one Observation ");
            return false;
        }
        var TotAmt=0;
        row = document.getElementById("<%= hid_Display.ClientID %>").value.split("¥");            
        for (n = 1; n < row.length ; n++) {
             col = row[n].split("µ");       
                StaffDtl+="¥"+col[0]+"µ"+col[5]+"µ"+col[6]+"µ"+col[7];  
                TotAmt+=parseFloat(col[6]);          
           }
        if (StaffDtl =="" ) {
            alert("Select Atleast one Staff ");
            return false;
        }
        if(Leakage!=TotAmt)
        {alert("Leakage and split up amount must be equal");return false;}
	    var ToData = "1Ø" + Sino+"Ø"+StaffDtl;
	    ToServer(ToData, 1);
    }

      function BranchOnChange() {
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
            if (BranchID == -1)
                ClearCombo("<%= cmbAudit.ClientID %>");
            else {
                var ToData = "3Ø" + BranchID;
                ToServer(ToData, 3);
            }
        }     
          
 function AuditOnChange() {
            var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;
            if (AuditID != -1)
              {
                var Fraud=document.getElementById("chkFraud").checked==true ? 1:0;
                var Serious=document.getElementById("chkSerious").checked==true ? 1:0;
                var General=document.getElementById("chkGeneral").checked==true ? 1:0;
                var Leakge=document.getElementById("chkLeakage").checked==true ? 1:0;
                var ToData = "4Ø" + AuditID+"Ø"+Fraud+"Ø"+Serious+"Ø"+General+"Ø"+Leakge;
                ToServer(ToData, 4);
             }
             else
             {
                alert("Select Audit");
                document.getElementById("rowObserv").style.display='none';
                document.getElementById("rowStaff").style.display='none';
                document.getElementById("btnSave").disabled=true;
                document.getElementById("<%= cmbAudit.ClientID %>").focus();
             }
        }     
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hdnAuditID" runat="server" />
                <asp:HiddenField ID="hdnAuditTypeID" runat="server" />
    <asp:HiddenField 
                    ID="hid_Display" runat="server" />
<br />

    <table class="style1" style="width:100%">
          <tr>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                Branch&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:50%">
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black"> <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
          <tr>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                Audit&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:50%">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black"> <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
          <tr style="height:30px;">
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                </td>
            <td style="text-align:left; width:50%">
                <input id="chkFraud" type="checkbox" checked="checked" />&nbsp;&nbsp;Fraud&nbsp;&nbsp;&nbsp;&nbsp;<input id="chkSerious" type="checkbox" checked="checked"/>&nbsp;&nbsp;Serious&nbsp;&nbsp;&nbsp;&nbsp;<input 
                    id="chkGeneral" type="checkbox" />&nbsp;&nbsp;General&nbsp;&nbsp;&nbsp;&nbsp;<input id="chkLeakage" type="checkbox" />&nbsp;&nbsp;Leakage&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../Image/Refresh1.png" alt="Refresh" title="Refresh" style="width:23px; height:23px; cursor:pointer;" onclick="AuditOnChange()"/></td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
          <tr>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                &nbsp;</td>
            <td style="text-align:left; width:50%">
                &nbsp;</td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
        <tr id="rowObserv" style="display:none;"> 
            <td colspan="4"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        
        <tr id="rowStaff" style="display:none;"> 
            <td style="text-align:center;" colspan="4">
                <table align="center" class="style1" style="margin: 0px auto; text-align:center; width:70%;">
                    <tr>
                        <td style="width:40%;text-align:center;" class="mainhead">
                            STAFF DETAILS</td>
                   
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            <asp:Panel ID="pnEmpDetails" runat="server">
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td></tr>
      
        <tr>
            <td style="text-align:center;" colspan="4">
                <br />
                &nbsp;<input id="btnSave" style="font-family: cambria; cursor: pointer; width:6%;" 
                type="button" value="SAVE"   onclick="return btnSave_onclick()" 
                    disabled="disabled" />
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>   

</asp:Content>

