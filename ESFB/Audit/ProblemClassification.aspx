﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ProblemClassification.aspx.vb" Inherits="ProblemClassification" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }        
    </style>
    <script language="javascript" type="text/javascript">
        function FillData() {
        var ToData="4Ø"+document.getElementById("<%= cmbGroup.ClientID %>").value+"Ø"+document.getElementById("<%= cmbSubGroup.ClientID %>").value+"Ø"+document.getElementById("<%= cmbCheckList.ClientID %>").value;
        ToServer(ToData, 4);
        }
        function GroupOnChange() {
            var GroupID = document.getElementById("<%= cmbGroup.ClientID %>").value;
            var ToData = "2Ø" + GroupID;              
            ToServer(ToData, 2);

        }
        function SubGroupOnChange()
        {
            var SubGroupID = document.getElementById("<%= cmbSubGroup.ClientID %>").value;
            var ToData = "3Ø" + SubGroupID;                  
            ToServer(ToData, 3);
        }

        function table_fill() 
        {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
              tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px;>";      
            tab += "<td style='width:5%;text-align:center' >#</td>"; 
            tab += "<td style='width:9%;text-align:center' >&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' id='chkSelectAll' onclick='SelectOnClick(-1)' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";    
            tab += "<td style='width:21%;text-align:left;' >Group</td>";
            tab += "<td style='width:20%;text-align:left'>Sub Group</td>";
            tab += "<td style='width:30%;text-align:left'>Item</td>";     
            tab += "<td style='width:5%;text-align:center'>SPM</td>";    
            tab += "<td style='width:5%;text-align:center'>Serious</td>"; 
            tab += "<td style='width:5%;text-align:center'>Very Serious</td>";   
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:324px; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 1; n <= row.length -1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                                                       
                    tab += "<td style='width:5%;text-align:center' >" + n  + "</td>";
                    tab += "<td style='width:9%;text-align:center' ><input type='checkbox' id='chkSelect" + col[0] + "' onchange=SelectOnClick(" + col[0] +   ") /></td>";
                    tab += "<td style='width:21%;text-align:left'>" + col[1] + "</td>";                   
                    tab += "<td style='width:20%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:30%;text-align:left'>" + col[3] + "</td>";  
                      
                    if (col[4] ==1){                                  
                    tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSPM" + col[0] + "' checked='true' disabled='true'/></td>";
                    }
                    else{
                    tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSPM" + col[0] + "'   disabled='true'/></td>";
                    }
                    if (col[5] ==1){                                  
                    tab += "<td style='width:5%;text-align:center'><input type='checkbox'  id='chkSerious" + col[0] + "' checked='true' onclick='return CheckClick(" + col[0] + ",1)'  disabled='true'/></td>";
                    }
                    else{
                    tab += "<td style='width:5%;text-align:center'><input type='checkbox'  id='chkSerious" + col[0] + "'  onclick='return CheckClick(" + col[0] + ",1)'  disabled='true'/></td>";
                    }
                    if (col[6] ==1){                                  
                    tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkVerySerious" + col[0] + "' checked='true' onclick='return CheckClick(" + col[0] + ",2)'  disabled='true'/></td>";
                    }
                    else{
                    tab += "<td style='width:5%;text-align:center'><input type='checkbox' name ='serious' id='chkVerySerious" + col[0] + "'  onclick='return CheckClick(" + col[0] + ",2)'  disabled='true'/></td>";
                    }
                    
                    
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
           
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }
        function CheckClick(ID,Type){
       
            if(Type ==1){
                document.getElementById("chkVerySerious"+ID).checked=false;
            }
            else{
                document.getElementById("chkSerious"+ID).checked=false;
            }
        }
          function SelectOnClick(val)
            {    
                if(val<0)
                {                 
                        var row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                        var state=true;
                        
                        if(document.getElementById("chkSelectAll").checked==true)
                           state=false;
                        for (n = 1; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                        
                        document.getElementById("chkSelect"+col[0]).checked=document.getElementById("chkSelectAll").checked;
                        document.getElementById("chkSPM"+col[0]).disabled =state; 
                        document.getElementById("chkSerious"+col[0]).disabled =state;    
                        document.getElementById("chkVerySerious"+col[0]).disabled =state;                       
                     
                     }
                    
                }
                else
                {
                    document.getElementById("chkSelectAll").checked=false;
                    if(document.getElementById("chkSelect"+val).checked==true)    
                    {    
                    
                        document.getElementById("chkSPM"+val).disabled =false; 
                        document.getElementById("chkSerious"+val).disabled =false;
                        document.getElementById("chkVerySerious"+val).disabled =false;   
                        document.getElementById("cmbSPM"+val).focus();  
                    }
                     else
                    {                                       
                        document.getElementById("chkSPM"+val).disabled =true; 
                        document.getElementById("chkSerious"+val).disabled =true; 
                        document.getElementById("chkVerySerious"+val).disabled =true;   
                    }
                }
            }

      
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
       
    function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function FromServer(arg, context) {
         switch (context) {
                case 1:
                    var Data=arg.split("Ø");
                    alert(Data[1]);
                    if(Data[0]==0) {window.open("ProblemClassification.aspx","_self");}
                    break;
                case 2:
                    ComboFill(arg, "<%= cmbSubGroup.ClientID %>");
                    break;
                case 3:
                    ComboFill(arg, "<%= cmbCheckList.ClientID %>");
                    break;
                case 4:
                    document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                    table_fill();
                    break;
                    }
               }
function Button1_onclick() {
FillData();
}

function btSave_onclick() {
 document.getElementById("<%= hid_value.ClientID %>").value="";
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 1; n <= row.length - 1; n++) {
            col = row[n].split("µ");
         if(document.getElementById("chkSelect"+col[0]).checked==true)
            {
                var SPM=(document.getElementById("chkSPM"+col[0]).checked ? 1:0);
                var Serious=(document.getElementById("chkSerious"+col[0]).checked ? 1:0);
                var VSerious=(document.getElementById("chkVerySerious"+col[0]).checked ? 1:0);               
                                              
                document.getElementById("<%= hid_value.ClientID %>").value+="¥"+ col[0]+"µ"+ SPM+"µ"+ Serious+"µ"+ VSerious;
            }
        }
        if(document.getElementById("<%= hid_value.ClientID %>").value=="")
        {alert("No Item Selected to Update");return false;}
         var ToData = "1Ø" + document.getElementById("<%= hid_value.ClientID %>").value;                  
         ToServer(ToData, 1);
}

    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_Value" runat="server" />
<br />

    <table  align="center" style="width: 60%;text-align:right; margin:0px auto;">
          <tr>
            <td style="width:20%;"></td>
            <td style="width:13%; align:left;">Group</td>
            <td style="width:47%; align:left;"><asp:DropDownList ID="cmbGroup" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="98%" ForeColor="Black"><asp:ListItem Value="-1">ALL</asp:ListItem>
                        </asp:DropDownList></td>
            <td style="width:20%; align:left;"></td>
        </tr>
        <tr>
            <td style="width:20%;"></td>
            <td style="width:13%; align:left;">Sub Group</td>
            <td style="width:47%; align:left;"><asp:DropDownList ID="cmbSubGroup" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="98%" ForeColor="Black"><asp:ListItem Value="-1">ALL</asp:ListItem>
                        </asp:DropDownList></td>
            <td style="width:20%; align:left;"></td>
        </tr>
        <tr>
            <td style="width:20%;"></td>
            <td style="width:13%; align:left;">Check List</td>
            <td style="width:47%; align:left;"><asp:DropDownList ID="cmbCheckList" class="NormalText" runat="server" Font-Names="Cambria" 
                            Width="98%" ForeColor="Black"><asp:ListItem Value="-1">ALL</asp:ListItem>
                        </asp:DropDownList></td>
            <td style="width:20%; align:left;">
                <img id="Button1" src="../Image/start.png" width="25px;" height="25px;" onclick="return Button1_onclick()" /></td>
        </tr>
        <tr> 
            <td colspan="4"></td></tr>
        <tr> 
            <td colspan="4"><asp:Panel ID="pnLeaveApproveDtl" runat="server" style="width:100%; text-align:center; margin:opx auto;">
            </asp:Panel></td></tr>
      
            
        <tr>
            <td style="text-align:center;" colspan="4">
                <br />              
                <input id="btSave" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="UPDATE"  onclick="return btSave_onclick()" onclick="return btSave_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
            
    </table>   
    <br />  
</asp:Content>

