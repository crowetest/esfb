﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Audit_AssignCenters
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Dim TypeID As Integer = 1
    Protected Sub Audit_AssignCenters_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 44) = False And GN.FormAccess(CInt(Session("UserID")), 52) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            If Not IsNothing(Request.QueryString.Get("TypeID")) Then
                TypeID = CInt(Request.QueryString.Get("TypeID"))
            End If
            Me.hdnTypeID.Value = TypeID.ToString()
            If TypeID = 1 Then
                Me.Master.subtitle = "Assign Centers"
                GN.ComboFill(cmbAudit, AD.GetAuditForCenterAssignment(CInt(Session("UserID"))), 0, 1)
            ElseIf TypeID = 2 Then
                Me.Master.subtitle = "Audit Sampling"
                GN.ComboFill(cmbAudit, AD.GetAuditForCenterAssignment(-1), 0, 1)
            End If
            cmbAudit.Attributes.Add("onchange", "AuditOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""

        If CInt(Data(0)) = 1 Then
            Dim AuditID As Integer = CInt(Data(1))
            Dim CenterDtl As String = Data(2)
            If CenterDtl <> "" Then
                CenterDtl = CenterDtl.Substring(1)
            End If

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@GroupID", SqlDbType.Int)
                Params(0).Value = AuditID
                Params(1) = New SqlParameter("@CenterDtl", SqlDbType.VarChar, 8000)
                Params(1).Value = CenterDtl
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@AssignTypeID", SqlDbType.Int)
                Params(4).Value = TypeID
                DB.ExecuteNonQuery("SP_ASSIGN_CENTERS", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            DT = AD.GetCentersForAssignNew(CInt(Data(1)), CInt(Data(2)))
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString() & "µ" & DT.Rows(n)(4).ToString() & "µ" & DT.Rows(n)(5).ToString() & "µ" & DT.Rows(n)(6).ToString() & "µ" & DT.Rows(n)(7).ToString()
                If n < DT.Rows.Count - 1 Then
                    CallBackReturn += "¥"
                End If
            Next
            DT = AD.GetAuditRate(CInt(Data(1)), CInt(Data(2)))
            CallBackReturn += "Ø" + DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString()

        End If
    End Sub
End Class
