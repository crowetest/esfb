﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="UpdateEscalationDays.aspx.vb" Inherits="Audit_UpdateEscalationDays" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }        
    </style>
    <script language="javascript" type="text/javascript">
        function table_fill() 
        {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:50%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px;>";      
            tab += "<td style='width:10%;text-align:center' >#</td>";            
            tab += "<td style='width:70%;text-align:left' >Level</td>";
            tab += "<td style='width:20%;text-align:right'>Days</td>";   
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:50%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";  
                 
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
          
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var TotalDays=0;
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    i = n + 1;                                      
                    tab += "<td style='width:10%;text-align:center' >" + i  + "</td>";
                    tab += "<td style='width:70%;text-align:left'>" + col[1] + "</td>";  
                    var txtBox = "<input id='txtDays" + col[0] + "' name='txtDays" + col[0]  + "' type='Text' style='width:99%; text-align:right;'  maxlength='3' onkeypress='NumericCheck(event)' onkeyup='CalcTotal();'  value="+col[2]+" >";                 
                    tab += "<td style='width:25%;text-align:center'>"+ txtBox +"</td>";  
                    tab += "</tr>";
                    TotalDays+=parseFloat(col[2]);
                }               
                 tab += "<tr class='mainhead'; style='text-align:center; height:21px; padding-left:20px;'>";                 
                 tab += "<td style='width:10%;text-align:center' ></td>";
                 tab += "<td style='width:70%;text-align:left'>TOTAL</td>";  
                 var txtBox = "<input id='txtDaysTotal' name='txtDaysTotal' type='Text' style='width:99%; text-align:right; font-weight:bold; color:black'  maxlength='3' disabled=true  value="+ TotalDays +">";                 
                 tab += "<td style='width:25%;text-align:center'>"+ txtBox +"</td>";  
                 tab += "</tr>";
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }

        function CalcTotal()
        {
            var row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            var TotalDays=0;          
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    TotalDays+=parseFloat(document.getElementById("txtDays" + col[0]).value);
                }   
                document.getElementById("txtDaysTotal").value=  TotalDays;          
        }
       
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btSave_onclick() {
             document.getElementById("<%= hid_value.ClientID %>").value="";
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
            col = row[n].split("µ");                                    
            document.getElementById("<%= hid_value.ClientID %>").value+="¥"+ col[0]+"µ"+ document.getElementById("txtDays"+col[0]).value ;
            }
            
         var ToData = "1Ø" + document.getElementById("<%= hid_value.ClientID %>").value;                  
         ToServer(ToData, 1);
        }

        function FromServer(arg, context) {
         switch (context) {
                case 1:
                    var Data=arg.split("Ø");
                    alert(Data[1]);
                    if(Data[0]==0) {window.open("UpdateEscalationDays.aspx","_self");}
                    break;
                    }
               }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_Value" runat="server" />
<br />

    <table  align="center" style="width: 60%;text-align:right; margin:0px auto;">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server" style="width:100%; text-align:center; margin:opx auto;">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <br />               
                <input id="btSave" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="UPDATE"  onclick="return btSave_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
            
    </table>   
    <br />  
</asp:Content>

