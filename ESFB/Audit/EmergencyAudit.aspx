﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="EmergencyAudit.aspx.vb" Inherits="EmergencyAudit" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                Branch</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black" AppendDataBoundItems="True">                
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                From</td>
            <td  style="width:85%;">
                <asp:TextBox ID="txtFromDt" runat="server" class="NormalText" Width="50%"  
                    ReadOnly="true"></asp:TextBox> <asp:CalendarExtender ID="CE1" runat="server" TargetControlID="txtFromDt" onclientshowing="setStartDate"  Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
        </tr>
        <tr>
            <td style="width:15%;">
                To</td>
            <td  style="width:85%;">
                <asp:TextBox ID="txtToDt" runat="server" class="NormalText" Width="50%"  
                    ReadOnly="true"></asp:TextBox> 
                <asp:CalendarExtender ID="CE2" runat="server"  onclientshowing="setStartDate1" 
                    TargetControlID="txtToDt" Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE"  onclick="return btnSave_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
    </table>
     <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">




        function btnExit_onclick() {
            window.open("../Home.aspx", "_self");
        }

        function setStartDate(sender, args) {
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value; 
            if (BranchDtl != "-1") {
                var arr = BranchDtl.split("~");
                var LastAuditDt = new Date(arr[1]);
                var StartDt = AddDay(LastAuditDt, 2);
                sender._startDate = new Date(StartDt);
                sender._endDate = new Date();
                sender.set_selectedDate(new Date(StartDt));      
            }


        }
        function AddDay(strDate, intNum) {
            sdate = new Date(strDate);
            sdate.setDate(sdate.getDate() + intNum);
            return sdate.getMonth() + 1 + " " + sdate.getDate() + " " + sdate.getFullYear();
        }
        function setStartDate1(sender, args) {
            sender._endDate = new Date();         
        }
        function BranchOnChange() {
            setStartDate;
        }
        function btnSave_onclick() {
            var BranchDtl = document.getElementById("<%= cmbBranch.ClientID %>").value;
            if (BranchDtl != "-1") {
                var arr = BranchDtl.split("~");
               var BranchID = arr[0];
            }
            var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDt.ClientID %>").value;
            if (BranchID == "-1")
            { alert("Select Branch"); document.getElementById("<%= cmbBranch.ClientID %>").focus(); return false; }
            if (FromDt == "")
            { alert("Select From Date"); document.getElementById("<%= txtFromDt.ClientID %>").focus(); return false; }
            if (ToDt == "")
            { alert("Select To Date"); document.getElementById("<%= txtToDt.ClientID %>").focus(); return false; }
            var ToData = "1Ø" + BranchID + "Ø" + FromDt + "Ø" + ToDt; 
            ToServer(ToData, 1);
        }
        function FromServer(arg, context) {

            switch (context) {
                case 1:
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    window.open("EmergencyAudit.aspx", "_self");
                    break;
            }
        }


    </script>
</asp:Content>

