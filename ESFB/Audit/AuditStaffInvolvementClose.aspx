﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditStaffInvolvementClose.aspx.vb" Inherits="Audit_AuditStaffInvolvementClose" EnableEventValidation="false"  %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
           width:5%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 100%;
	        border-top-right-radius: 100%;
	        border-bottom-left-radius: 100%;
	        border-bottom-right-radius: 100%;
            cursor:pointer;
             background: -moz-radial-gradient(center, ellipse cover, #476C91 0%, #B0C4DE 0%, #476C91 100%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }        
        .sub_hdRow
        {
         background-color:#FFFFCC; height:20px;
         font-family:Arial; color:#B84D4D; font-size:8.5pt;  font-weight:bold;
        }    
        .hdRow
        {
         background-color:#FFF; height:20px;
         font-family:Arial; color:#EBCCD6; font-size:8.5pt;  font-weight:bold;
        }        
        .style1
        {
            width: 50%;
        }
    </style>
    <script language="javascript" type="text/javascript">

      function FromServer(arg, context) {

         switch (context) {
             case 1:   
                var Data = arg.split("Ø");
                alert(Data[1]);
                EmployeeOnChange();
                 break;           
                
           
              case 4:                 
                  document.getElementById("<%= hid_dtls.ClientID %>").value=arg;
                  table_fill();
                  break;
            default:
            break;
        
            }
            
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
          
    function ViewReport(AuditID,AuditType)
    {
       window.open("Reports/viewAuditSamplingReport.aspx?AuditID="+ btoa(AuditID) +" &AuditTypeID="+ AuditType,"_self"); 
       return false;
    }
        function table_fill() 
        {           
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var ItemName="";
            var AuitType=0
            tab += "<div style='width:90%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:scroll; height:275px; text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr >";           
            tab += "<td style='width:5%;text-align:center' >SI No</td>";
            tab += "<td style='width:10%;text-align:left' >Ref No</td>";
            tab += "<td style='width:10%;text-align:left'>Center</td>";     
            tab += "<td style='width:10%;text-align:left'>Customer</td>"; 
            tab += "<td style='width:25%;text-align:left'>Remarks</td>";          
            tab += "<td style='width:3%;text-align:center'>Type</td>";
            tab += "<td style='width:6%;text-align:right'>Est. Amt Involved</td>";
            tab += "<td style='width:6%;text-align:center'>Status</td>";
            tab += "<td style='width:5%;text-align:center'>Close<br /><input id='chkCloseAll' type='checkbox' onclick='AllSelect(1)' /></td>";
           // tab += "<td style='width:5%;text-align:center'>Remove<br /><input id='chkRemoveAll' type='checkbox' onclick='AllSelect(2)' /></td>";
            tab += "<td style='width:19%;text-align:left'>Reason<br /><input id='chkRemarkAll' type='checkbox' onclick='AllRemark()' /></td>";
            tab += "</tr>";             
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                document.getElementById("rowObserv").style.display=''
                document.getElementById("btnSave").disabled=false;
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
             var i=0;
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    i+=1;
                    if (AuitType!=col[9])
                    {
                           tab += "<tr class='sub_second'; style='height:25px;text-align:center; padding-left:20px;'>";
                           tab += "<td style='width:5%;text-align:center' colspan=11 >" + col[8] + "</td></tr>";
                    }
                    if (ItemName!=col[2])
                    {
                           tab += "<tr class='sub_hdRow'; style='height:25px;text-align:center; padding-left:20px;'>";
                           tab += "<td style='width:5%;text-align:left' colspan=11 >" + col[2] + "</td></tr>";
                    }
                    ItemName=col[2];
                    AuitType=col[9];
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='height:25px;text-align:center; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_first'; style='height:25px;text-align:center; padding-left:20px;'>";
                    }
                    i = n + 1;
                    //EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time                    
                    tab += "<td style='width:5%;text-align:center' >" + i + "</td>";
                    tab += "<td style='width:10%;text-align:left' >" + col[3] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[5] + "</td>";                   
                    tab += "<td style='width:10%;text-align:left'>" + col[4] + "</td>"; 
                    tab += "<td style='width:25%;text-align:left'>" + col[1] + "</td>"; 
                    tab += "<td style='width:3%;text-align:center'>" + col[6] + "</td>"; 
                    if(parseFloat(col[7])==0)
                    {
                    tab += "<td style='width:6%;text-align:right'></td>"; }
                    else
                    tab += "<td style='width:6%;text-align:right'>" + parseFloat(col[7]).toFixed(2) + "</td>";
                    tab += "<td style='width:6%;text-align:center'>" + col[10] + "</td>"; 
                    tab += "<td style='width:5%; text-align:center; '> <input id='chkClose"+ i +"' type='checkbox' onclick='SelectOnClick("+ i +",1)' /></td>";                         
                    //tab += "<td style='width:5%; text-align:center; '> <input id='chkRemove"+ i +"' type='checkbox'  onclick='SelectOnClick("+ i +",2)' /></td>";   
                    var txtBox1 = "<textarea id='txtReason" + i + "' name='txtReason" + i + "'  style='width:99%;'   disabled=true  maxlength='100' onkeypress='return TextAreaCheck(event)'  ></textarea>";
                    tab += "<td style='width:19%; text-align:left; '>"+ txtBox1 +"</td>";   
                    tab += "</tr>";
                }
                 tab += "</table></div></div></div>";            
                 document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
                 FillEmpDetails();
            }
            else
            {
                alert("No Observations Found");
                document.getElementById("rowObserv").style.display='none';              
                document.getElementById("btnSave").disabled=true;

            }
          
            //--------------------- Clearing Data ------------------------//


        }

        function AllRemark()
        {
            if(document.getElementById("chkRemarkAll").checked==true)
            {
                if(document.getElementById("chkCloseAll").checked==false)
                    {alert("Check Close All");document.getElementById("chkRemarkAll").checked=false; return false;}
                var Remark=prompt("Enter General remarks for all selected observations","");
                if(Remark=="")
                    {alert("Enter Remark");document.getElementById("chkRemarkAll").checked=false; return false;}
                var  row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var i=0;  
                for (n = 0; n <= row.length - 1; n++) {
                   col = row[n].split("µ");  
                   i=n+1;              
                  document.getElementById("txtReason"+i).value=Remark;                 
                    }  
            }
        }
        function AllSelect(ID)
        {
            var  row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            var i=0;  
            if(ID==1)
            {                   
              for (n = 0; n <= row.length - 1; n++) {
                   col = row[n].split("µ");  
                   i=n+1;              
                  document.getElementById("chkClose"+i).checked=document.getElementById("chkCloseAll").checked;   
                  document.getElementById("txtReason"+i).disabled=(document.getElementById("chkCloseAll").checked==true) ? false : true;  
//                  document.getElementById("chkRemove"+i).checked= false ;     
                }  
//                document.getElementById("chkRemoveAll").checked= false ;      
           }
           else
           {
              for (n = 0; n <= row.length - 1; n++) {
                   col = row[n].split("µ");  
                   i=n+1;                
//                  document.getElementById("chkRemove"+i).checked=document.getElementById("chkRemoveAll").checked;  
//                  document.getElementById("txtReason"+i).disabled=(document.getElementById("chkRemoveAll").checked==true) ? false : true;  
                   document.getElementById("chkClose"+i).checked= false;  
                    } 
                    document.getElementById("chkCloseAll").checked= false;      
           }
            
        }
        function SelectOnClick(ID,TypeID)
        {  
        if(TypeID==1)    
        {   document.getElementById("chkCloseAll").checked=false;
            if(document.getElementById("chkClose"+ID).checked==true)      
            {     
//               document.getElementById("chkRemove"+ID).checked=false;
               document.getElementById("txtReason"+ID).disabled=false;
               document.getElementById("txtReason"+ID).focus();
               }
//             else if (document.getElementById("chkRemove"+ID).checked==true)
//             {
//                document.getElementById("chkClose"+ID).checked=false;
//                document.getElementById("txtReason"+ID).disabled=false;
//                document.getElementById("txtReason"+ID).focus();
//             }
             else{
                    document.getElementById("txtReason"+ID).value="";
                    document.getElementById("txtReason"+ID).disabled=true;
             }
           }
           else
           {
//            document.getElementById("chkRemoveAll").checked=false;
//            if (document.getElementById("chkRemove"+ID).checked==true)
//             {
//                document.getElementById("chkClose"+ID).checked=false;
//                document.getElementById("txtReason"+ID).disabled=false;
//                document.getElementById("txtReason"+ID).focus();
//             }
//            else 
            if(document.getElementById("chkClose"+ID).checked==true)      
            {     
//               document.getElementById("chkRemove"+ID).checked=false;
               document.getElementById("txtReason"+ID).disabled=false;
               document.getElementById("txtReason"+ID).focus();
               }
            
             else{
                    document.getElementById("txtReason"+ID).value="";
                    document.getElementById("txtReason"+ID).disabled=true;
             }
           }
        }
       
       
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
    
        
                
       function btnSave_onclick() { 
       var EmpCode=document.getElementById("<%= cmbEmployee.ClientID %>").value;
       if(EmpCode<0)
       {
        alert("Select Employee");document.getElementById("cmbEmployee").focus();return false;
       }
        var ObservationDtl=""      
        row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
        var i=0;            
        for (n = 0; n <= row.length - 1; n++) {
             col = row[n].split("µ");
             var StatusID=-1;
              i = n + 1;
            if(document.getElementById("chkClose"+i).checked==true)          
                StatusID=0;           
//            else if(document.getElementById("chkRemove"+i).checked==true)
//                StatusID=9;
            if(StatusID>=0)
            {
                var Reason=document.getElementById("txtReason"+i).value;
                if (Reason=="")
                {
                    alert("Enter Reason");document.getElementById("txtReason"+i).focus();return false;
                }
                ObservationDtl+="¥"+col[0]+"µ"+StatusID+"µ"+Reason
            }
           }
        if (ObservationDtl =="" ) {
            alert("Select Atleast one Observation ");
            return false;
        }        
        document.getElementById("btnSave").disabled=true;
	    var ToData = "1Ø"+ EmpCode + "Ø" + ObservationDtl;
	    ToServer(ToData, 1);
    }

      function EmployeeOnChange() {
            var EmpCode = document.getElementById("<%= cmbEmployee.ClientID %>").value;
            if (EmpCode == -1)
            {
                document.getElementById("<%= hid_dtls.ClientID %>").value="";
                document.getElementById("rowObserv").style.display="none";
                }
            else {
                var ToData = "4Ø" + EmpCode;
                ToServer(ToData, 4);
            }
        }     
          
    
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hdnAuditID" runat="server" />
                <asp:HiddenField ID="hdnAuditTypeID" runat="server" />
    <asp:HiddenField 
                    ID="hid_Display" runat="server" />
<br />

    <table class="style1" style="width:100%">
          <tr>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                Employee&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:50%">
                <asp:DropDownList ID="cmbEmployee" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black"> <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
          <tr>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                &nbsp;</td>
            <td style="text-align:left; width:50%">
                &nbsp;</td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
        <tr id="rowObserv" style="display:none;"> 
            <td colspan="4"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        
      
      
        <tr>
            <td style="text-align:center;" colspan="4">
                <br />
                &nbsp;<input id="btnSave" style="font-family: cambria; cursor: pointer; width:6%;" 
                type="button" value="SAVE"   onclick="return btnSave_onclick()" 
                    disabled="disabled" />
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>   

</asp:Content>

