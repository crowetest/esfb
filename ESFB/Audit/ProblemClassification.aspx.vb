﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class ProblemClassification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 241) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Item Classification"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            GN.ComboFill(cmbGroup, AD.GetCheckGroupNew(1, 1), 0, 1)
            cmbGroup.Attributes.Add("onchange", "GroupOnChange()")
            cmbSubGroup.Attributes.Add("onchange", "SubGroupOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim ClassificationDtl As String = Data(1)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@EMPDtl", SqlDbType.VarChar, 8000)
                Params(0).Value = ClassificationDtl.Substring(1)
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = CInt(Session("UserID"))
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_AUDIT_ITEM_CLASSIFICATION", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            DT = AD.GetCheckSubGroup(CInt(Data(1)), 1)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 3 Then
            DT = AD.GetCheckList(CInt(Data(1)), 1)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 4 Then
            Dim StrQRY As String = ""
            If CStr(Data(1)) <> "-1" Then
                StrQRY = " and c.group_id=" & Data(1).ToString() & ""
            End If
            If CStr(Data(2)) <> "-1" Then
                StrQRY = " and b.Sub_group_ID=" & Data(2).ToString() & ""
            End If
            If CStr(Data(3)) <> "-1" Then
                StrQRY = " and a.ITEM_ID=" & Data(3).ToString() & ""
            End If

            DT = DB.ExecuteDataSet("select ITEM_ID,d.TYPE_NAME +'/'+c.group_name,b.SUB_GROUP_NAME,ITEM_NAME,isnull(SPM_FLAG,0),isnull(SERIOUS_FLAG,0),isnull(VERY_SERIOUS_FLAG,0) from AUDIT_CHECK_LIST a,AUDIT_CHECK_SUBGROUPS b,AUDIT_CHECK_GROUPS c,AUDIT_TYPE d   " & _
                " where a.Sub_group_ID=b.Sub_group_ID and b.group_id=c.group_id and c.AUDIT_TYPE=d.TYPE_ID" + StrQRY.ToString() + " and a.STATUS_ID=1").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "¥" + DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "µ" + DR(5).ToString() + "µ" + DR(6).ToString()
            Next
        End If
    End Sub
End Class
