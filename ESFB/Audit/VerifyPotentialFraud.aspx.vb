﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Audit_VerifyPotentialFraud
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Further Verification"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            GN.ComboFill(cmbBranch, AD.GetBranchForPotentialVerification(), 0, 1)
            cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            cmbAccount.Attributes.Add("onchange", "AccountOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EN As New Enrollment
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim ObservationID As Integer = CInt(Data(1))
            Dim LoanNo As String = Data(2)
            Dim Remarks As String = Data(3)
            Dim StatusID As Integer = CInt(Data(4))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@ObservationID", SqlDbType.Int)
                Params(0).Value = ObservationID
                Params(1) = New SqlParameter("@LoanNo", SqlDbType.VarChar, 20)
                Params(1).Value = LoanNo
                Params(2) = New SqlParameter("@Remarks", SqlDbType.VarChar, 2000)
                Params(2).Value = Remarks
                Params(3) = New SqlParameter("@StatusID", SqlDbType.Int)
                Params(3).Value = StatusID
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = CInt(Session("UserID"))
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_PotentialFraudverification", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            DT = AD.GetPotentialFraudAccounts(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 3 Then
            DT = AD.GetLoanAccountDetail(CInt(Data(1)))
            CallBackReturn += DT.Rows(0)(0).ToString() + "µ" + DT.Rows(0)(1).ToString() + "µ" + DT.Rows(0)(2).ToString() + "µ" + DT.Rows(0)(3).ToString() + "µ" + DT.Rows(0)(4).ToString() + "µ" + DT.Rows(0)(5).ToString() + "µ" + DT.Rows(0)(6).ToString() + "µ" + DT.Rows(0)(7).ToString() + "µ" + DT.Rows(0)(8).ToString()
        ElseIf CInt(Data(0)) = 4 Then
            DT = AD.GetAuditComments(CInt(Data(1)))
            CallBackReturn += DT.Rows(0)(0).ToString()
        ElseIf CInt(Data(0)) = 5 Then
            DT = AD.GetLoanDetails(Data(1))
            If DT.Rows.Count > 0 Then
                CallBackReturn = DT.Rows(0)(0).ToString() & "µ" & DT.Rows(0)(1).ToString() & "µ" & DT.Rows(0)(2).ToString()
            Else
                CallBackReturn = ""
            End If
        End If
    End Sub
End Class
