﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class AuditChecklistApplicability
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 131) = False And GF.FormAccess(CInt(Session("UserID")), 89) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Me.Master.subtitle = "Checklist Applicability Setting"
            DT = DB.ExecuteDataSet("select * from branch_master where status_id = 1 and branch_type = 1 order by branch_name ").Tables(0)
            GF.ComboFill(cmbBranch, DT, 0, 1)

            DT = GF.GetQueryResult("select -1,'---Select---' AuditType union all  select Type_ID,Type_Name from audit_Type where status_id=1 order by 2")
            GF.ComboFill(cmbAuditType, DT, 0, 1)

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Me.cmbBranch.Attributes.Add("onchange", "BranchGroupOnChange()")
            Me.cmbAuditType.Attributes.Add("onchange", "AuditTypeOnChange()")
            Me.cmbGroup.Attributes.Add("onchange", "BranchGroupOnChange()")

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim SQL As String = ""

        If CInt(Data(0)) = 1 Then '' table fill
            Dim Branch_Id As Integer = CInt(Data(1))
            Dim Group_Id As Integer = CInt(Data(2))

            SQL = "select  A.Sub_Group_id, Sub_group_name, A.Item_id, Item_name,Isnull(Max_Mark,0) as Max_Mark ," +
                " case when isnull(Sample_Type,0) = 1 then 'Sample' when isnull(Sample_Type,0) = 2 then 'Non - Sample' else '' end as Sample_Type, " +
                " case when isnull(Category,0) = 1 then 'Business Risk' when isnull(Category,0) = 2 then 'Control Risk' else '' end as Category, " +
                " isnull(Description,'') as Description , CASE WHEN ISNULL(C.APPLICABILITY,0) = 1 THEN 'YES' WHEN ISNULL(C.APPLICABILITY,0) = 2 THEN 'NO' ELSE '' END AS APPLICABILITY " +
                " from audit_check_list A " +
                " inner join AUDIT_CHECK_SUBGROUPS B on A.Sub_Group_id = B.Sub_Group_id " +
                " LEFT join AUDIT_CHECK_LIST_APPLICABILITY C ON A.ITEM_ID = C.ITEM_ID AND C.BRANCH_ID = " + Branch_Id.ToString() +
                " where(B.Group_id = " + Group_Id.ToString() + ")"

            DT = GF.GetQueryResult(SQL)

            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString
                If n < DT.Rows.Count - 1 Then
                    CallBackReturn += "¥"
                End If
            Next

        ElseIf CInt(Data(0)) = 2 Then ' Group fill
            Dim AuditTypeID As Integer = CInt(Data(1))
            
            SQL = "select -1,'---NEW---' Group_Name union all  select Group_ID,Group_Name from Audit_Check_Groups where audit_type=" + AuditTypeID.ToString() + " and status_id=1 order by 2"

            DT = GF.GetQueryResult(SQL)
            If DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                Next
            End If
        ElseIf CInt(Data(0)) = 3 Then
            Dim Branch_id As Integer = CInt(Data(1))
            Dim Audit_Type_id As Integer = CInt(Data(2))
            Dim Group_id As Integer = CInt(Data(3))
            Dim dataval As String = CStr(Data(4))
            Dim UserID As Integer = CInt(Session("UserID"))

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@Branch_Id", SqlDbType.Int)
                Params(0).Value = Branch_id
                Params(1) = New SqlParameter("@Audit_Type_id", SqlDbType.Int)
                Params(1).Value = Audit_Type_id
                Params(2) = New SqlParameter("@Group_id", SqlDbType.Int)
                Params(2).Value = Group_id
                Params(3) = New SqlParameter("@User_id", SqlDbType.Int)
                Params(3).Value = UserID
                Params(4) = New SqlParameter("@AppDtl", SqlDbType.VarChar, 2000)
                Params(4).Value = dataval.Substring(1)
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output

                DB.ExecuteNonQuery("SP_AUDIT_BRANCH_CHECK_APPLICABILITY", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If
    End Sub
#End Region
End Class
