﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="VerifyPotentialFraud.aspx.vb" Inherits="Audit_VerifyPotentialFraud" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }        
        .style1
        {
            width: 100%;
        }
    </style>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
              
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
         function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

         function FromServer(arg, context) {

         switch (context) {
                case 1:
                     var Data=arg.split("Ø");
                    alert(Data[1]);
                    if(Data[0]==0) {window.open("VerifyPotentialFraud.aspx","_self");}
                    break;              
                 case 2:
                    ComboFill(arg,"<%= cmbAccount.ClientID %>");
                    break;  
                 case 3:
                    var Dtl=arg.split("µ");
                    document.getElementById("<%= txtCustName.ClientID %>").value=Dtl[0];
                    document.getElementById("<%= txtClientID.ClientID %>").value=Dtl[1];
                    document.getElementById("<%= txtDisbDate.ClientID %>").value=Dtl[2];
                    document.getElementById("<%= txtAmount.ClientID %>").value=parseFloat(Dtl[3]).toFixed(2);
                    document.getElementById("<%= txtScheme.ClientID %>").value=Dtl[4];
                    document.getElementById("<%= txtPhone.ClientID %>").value=Dtl[5];
                    document.getElementById("<%= txtAddress.ClientID %>").value=Dtl[6];
                    document.getElementById("<%= txtAuditComments.ClientID %>").value=Dtl[7];
                    document.getElementById("<%= txtCenter.ClientID %>").value=Dtl[8];
                    document.getElementById("<%= txtRemarks.ClientID %>").focus();
                 break;   
                  case 4:                  
                    document.getElementById("<%= txtAuditComments.ClientID %>").value=arg;
                    document.getElementById("<%= txtRemarks.ClientID %>").focus();
                 break;                         
            default:
            break;
        
            }
            
        }

          function BranchOnChange() {
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
            var ToData = "2Ø" + BranchID;              
            ToServer(ToData, 2);

        }      
        function btnExit_onclick() {
                window.open("../Home.aspx", "_self");
            }    

      function AccountOnChange()
      {
         var AccountDtl=document.getElementById("<%= cmbAccount.ClientID %>").value;
         if(AccountDtl!="-1")
         {
            var Dtl=AccountDtl.split("~");          
            if(Dtl[0]!="")
            {
                document.getElementById("RowLoanDtl").style.display='';
                document.getElementById("RowLoanDtl1").style.display='';
                document.getElementById("RowLoanDtl2").style.display='';
                document.getElementById("RowLoanDtl3").style.display='';             
                var ToData = "3Ø" + Dtl[2];              
                ToServer(ToData, 3);
            }
            else{
                  document.getElementById("RowLoanDtl").style.display="none";
                  document.getElementById("RowLoanDtl1").style.display="none";
                  document.getElementById("RowLoanDtl2").style.display="none";
                  document.getElementById("RowLoanDtl3").style.display="none";                 
                  var ToData = "4Ø" + Dtl[2];              
                  ToServer(ToData, 4);
            }
         }
         else
         {
            document.getElementById("<%= txtCustName.ClientID %>").value="";
            document.getElementById("<%= txtClientID.ClientID %>").value="";
            document.getElementById("<%= txtDisbDate.ClientID %>").value="";
            document.getElementById("<%= txtAmount.ClientID %>").value="";
            document.getElementById("<%= txtScheme.ClientID %>").value="";
            document.getElementById("<%= txtPhone.ClientID %>").value="";
            document.getElementById("<%= txtAddress.ClientID %>").value="";
            document.getElementById("<%= txtAuditComments.ClientID %>").value="";   
             document.getElementById("<%= txtCenter.ClientID %>").value="";         
            document.getElementById("<%= txtRemarks.ClientID %>").value="";
         }
      }
function btnSave_onclick() {
        
       var AccountDtl=document.getElementById("<%= cmbAccount.ClientID %>").value;
       var Remarks=document.getElementById("<%= txtRemarks.ClientID %>").value;
       var AccountStatus=0;
       if(document.getElementById("<%= rbGenuine.ClientID %>").checked==true)
            AccountStatus=1;
        else  if (document.getElementById("<%= rbFraud.ClientID %>").checked==true)
            AccountStatus=3;
       if(AccountDtl=="-1")
        {
           alert("Select an Account"); document.getElementById("<%= cmbAccount.ClientID %>").focus();return false;
        }
        if(Remarks=="")
        { alert("Enter Remarks"); document.getElementById("<%= txtRemarks.ClientID %>").focus();return false;}
        if(AccountStatus==0)
        {alert("Select if the Status is genuine or Fraud");document.getElementById("<%= rbGenuine.ClientID %>").focus();return false;}
        var Dtl=AccountDtl.split("~");
        var ToData = "1Ø" + Dtl[1]+"Ø"+Dtl[0]+"Ø"+Remarks+"Ø"+AccountStatus;              
            ToServer(ToData, 1);
}

    </script>
    <br />
    <table  align="center" style="width: 80%;text-align:right; margin:0px auto;">
        <tr>
            <td style="width:5%">
                &nbsp;</td>
            <td style="width:10%">
                &nbsp;&nbsp;Branch</td>
            <td style="width:85%">
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black">
                </asp:DropDownList> </td>
        </tr>
        <tr>
            <td style="width:5%">
                &nbsp;</td>
            <td style="width:10%">
                Account</td>
            <td style="width:85%">
                <asp:DropDownList ID="cmbAccount" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList> </td>
        </tr>
        <br />
         <tr>
            <td style="width:5%">
                &nbsp;</td>
            <td style="width:10%">
                &nbsp;</td>
            <td style="width:85%">
                &nbsp;</td>
        </tr>
       
         <tr >
            <td style="width:20%;text-align:center;"colspan="3">
                <table align="center" class="style1">
                    <tr id="RowLoanDtl">
                        <td style="width:10%; text-align:right;">
                            Name &nbsp;&nbsp;</td>
                        <td style="width:40%">
                            <asp:TextBox ID="txtCustName" runat="server" Width="98%" 
                                class="ReadOnlyTextBox" ReadOnly="True" ></asp:TextBox>
                        </td>
                        <td style="width:10%; text-align:right;">
                            Client ID&nbsp;&nbsp;</td>
                        <td style="width:40%">
                            <asp:TextBox ID="txtClientID" runat="server" Width="98%" 
                                class="ReadOnlyTextBox" ReadOnly="True" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="RowLoanDtl1">
                        <td style="width:10%; text-align:right;"> 
                            Disb Date&nbsp;&nbsp;</td>
                        <td style="width:40%">
                            <asp:TextBox ID="txtDisbDate" runat="server" Width="98%" 
                                class="ReadOnlyTextBox" ReadOnly="True" ></asp:TextBox>
                        </td>
                        <td style="width:10%; text-align:right;">
                            Amount&nbsp;&nbsp;</td>
                        <td style="width:40%">
                            <asp:TextBox ID="txtAmount" runat="server" Width="98%" 
                                class="ReadOnlyTextBox" ReadOnly="True" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="RowLoanDtl2">
                        <td style="width:10%; text-align:right;"> 
                            Scheme&nbsp;&nbsp;</td>
                        <td style="width:40%">
                            <asp:TextBox ID="txtScheme" runat="server" Width="98%" 
                                class="ReadOnlyTextBox" ReadOnly="True" ></asp:TextBox>
                        </td>
                        <td style="width:10%; text-align:right;">
                            Phone&nbsp;&nbsp;</td>
                        <td style="width:40%">
                            <asp:TextBox ID="txtPhone" runat="server" Width="98%" 
                                class="ReadOnlyTextBox" ReadOnly="True" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="RowLoanDtl3">
                        <td style="width:10%; text-align:right; vertical-align:top;"> 
                            Address&nbsp;&nbsp;</td>
                        <td style="width:40%">
                            <asp:TextBox ID="txtAddress" runat="server" Width="98%" style="resize: none;"
                                class="ReadOnlyTextBox" ReadOnly="True" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
                        </td>
                        <td style="width:10%; text-align:right; vertical-align:top;">
                            Center&nbsp;&nbsp;</td>
                        <td style="width:40%">
                            <asp:TextBox ID="txtCenter" runat="server" Width="98%"  style="resize: none;"
                                class="ReadOnlyTextBox" ReadOnly="True" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:10%; text-align:right;"> 
                            Audit Comments </td>
                        <td style="width:40%" colspan="3">
                            <asp:TextBox ID="txtAuditComments" runat="server" Width="99%"  style="resize: none;"
                                class="ReadOnlyTextBox" ReadOnly="True" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="width:10%; text-align:right;"> 
                            Remarks&nbsp;&nbsp;</td>
                        <td style="width:40%" colspan="3">
                            <asp:TextBox ID="txtRemarks" runat="server" Width="99%"  class="NormalText"
                                 TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
                        </td>
                        
                    </tr>
                </table>
             </td>            
        </tr>
       
      
          <tr>
            <td style="width:5%; text-align:center;" colspan="3">
                <asp:RadioButton ID="rbGenuine" runat="server" BackColor="#00A400" 
                    Font-Bold="True" ForeColor="White" GroupName="rb" Height="22px" 
                    Text=" Genuine Account" Width="15%" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:RadioButton ID="rbFraud" runat="server" BackColor="#C10000" 
                    Font-Bold="True" ForeColor="White" GroupName="rb" Height="22px" 
                    Text="Need Inspection&nbsp;" Width="15%" />
              </td>
          
        </tr>
       
      
          <tr>
            <td style="width:5%; text-align:center;" colspan="3">
                &nbsp;</td>
          
        </tr>
         <tr>
            <td style="width:20%;text-align:center;"colspan="3">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE"   onclick="return btnSave_onclick()" />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>            
        </tr>
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:80%" colspan="2">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td style="width:20%; height: 21px;">
                <asp:HiddenField ID="hdnAuditID" runat="server" />
            </td>
            <td style="width:80%; height: 21px;" colspan="2">
                <asp:HiddenField ID="hdnAuditDetails" runat="server" />
                </td>
        </tr>
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:80%" colspan="2">
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:80%" colspan="2">
                &nbsp;</td>
        </tr>
    </table>
</head>
</html>
    </asp:Content>

