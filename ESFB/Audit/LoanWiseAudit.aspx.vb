﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Audit_LoanWiseAudit
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTComments As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim DTcheck As New DataTable
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim AuditID As Integer = CInt(Data(1))
            Dim LoanNo As String = Data(2)
            Dim ObservationDtl As String = Data(3)
            If ObservationDtl <> "" Then
                ObservationDtl = ObservationDtl.Substring(1)
            End If
            Dim CloseFlag As Integer = CInt(Data(4))
            Dim GeneralComments As String = Data(5)
            Dim SeriousComments As String = Data(6)
            Dim ManagementNote As String = Data(7)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(8) As SqlParameter
                Params(0) = New SqlParameter("@AuditID", SqlDbType.Int)
                Params(0).Value = AuditID
                Params(1) = New SqlParameter("@LoanNo", SqlDbType.VarChar, 20)
                Params(1).Value = LoanNo
                Params(2) = New SqlParameter("@ObservationDtl", SqlDbType.VarChar)
                Params(2).Value = ObservationDtl
                Params(3) = New SqlParameter("@CloseFlag", SqlDbType.Int)
                Params(3).Value = CloseFlag
                Params(4) = New SqlParameter("@GeneralComments", SqlDbType.VarChar, 2000)
                Params(4).Value = GeneralComments
                Params(5) = New SqlParameter("@SeriousComments", SqlDbType.VarChar, 2000)
                Params(5).Value = SeriousComments
                Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@ManagementNote", SqlDbType.VarChar, 5000)
                Params(8).Value = ManagementNote
                DB.ExecuteNonQuery("SP_AuditObservation_LoanWise", Params)
                ErrorFlag = CInt(Params(6).Value)
                Message = CStr(Params(7).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            DT = AD.GetAuditAccountsLoanWise(Data(1), CInt(Data(2)), CInt(Data(3)))
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString() & "µ" & DT.Rows(n)(4).ToString() & "µ" & DT.Rows(n)(5).ToString() & "µ" & DT.Rows(n)(6).ToString() & "µ" & DT.Rows(n)(7).ToString() & "µ" & DT.Rows(n)(8).ToString() & "µ" & DT.Rows(n)(9).ToString() & "µ" & DT.Rows(n)(10).ToString() & "µ" & DT.Rows(n)(11).ToString() & "µ" & DT.Rows(n)(12).ToString() & "µ" & DT.Rows(n)(13).ToString()
                If n < DT.Rows.Count - 1 Then
                    CallBackReturn += "¥"
                End If
            Next
        ElseIf CInt(Data(0)) = 3 Then
            DT = AD.GetLoanAccounts(Data(1), CInt(Data(2)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub
    Protected Sub Audit_LoanWiseAudit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 36) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Audit Observations"
            Dim AuditID As Integer
            Dim AuditType As Integer
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            AuditID = CInt(GN.Decrypt(Request.QueryString.Get("AuditID")))
            hdnAuditID.Value = AuditID.ToString()
            AuditType = CInt(Request.QueryString.Get("AuditType"))
            Dim Branch_ID As Integer = 0
            Dim DTMislead As New DataTable
            DT = DB.ExecuteDataSet("select distinct d.branch_id from AUDIT_DTL a,AUDIT_MASTER d  where a.GROUP_ID=d.GROUP_ID and a.audit_id=" & AuditID.ToString & " ").Tables(0)
            Branch_ID = CInt(DT.Rows(0)(0))
            'DTMislead = DB.ExecuteDataSet("select isnull(count(DISTINCT a.group_ID),0) from AUDIT_DTL a,AUDIT_BRANCHES k,BRANCH_MASTER b,AUDIT_TYPE c,AUDIT_MASTER d LEFT JOIN AUDIT_OBS_CONFIRMATION G " & _
            '       " ON D.GROUP_ID=G.GROUP_ID,AUDIT_OBSERVATION e,(select * from AUDIT_OBSERVATION_DTL where SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17 ))  f  WHERE  a.branch_id=b.Branch_ID AND a.branch_id=K.Branch_ID and a.GROUP_ID=d.GROUP_ID and a.AUDIT_TYPE=c.TYPE_ID and a.BRANCH_ID= " & _
            '       " b.Branch_ID and a.AUDIT_ID=e.AUDIT_ID and e.OBSERVATION_ID=f.OBSERVATION_ID and f.STATUS_ID =0 and f.confirmation_flag=0 AND G.CLOSED_DATE IS NULL " & _
            '       " AND d.branch_id=" & Branch_ID.ToString() & " ").Tables(0)
            'hdnMislead.Value = DTMislead.Rows(0)(0).ToString
            DTComments = AD.GetAuditGeneralComments(AuditID)
            If DTComments.Rows.Count > 0 Then
                Me.hdnComments.Value = DTComments.Rows(0)(0).ToString() + "Ø" + DTComments.Rows(0)(1).ToString()
            End If
            hdnAuditTypeID.Value = AuditType.ToString()
            DT = AD.GetAuditDtl(AuditID)
            lblBranch.Text = DT.Rows(0)(1).ToString()
            lblStartDate.Text = DT.Rows(0)(2).ToString()
            lblAuditType.Text = DT.Rows(0)(3).ToString()
            GN.ComboFill(cmbSangam, AD.getCenters(AuditID), 0, 1)
            DT = AD.GetCheckGroup(AuditType)
            hid_dtls.Value = "Ø"
            For Each DR As DataRow In DT.Rows
                hid_dtls.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = AD.Get_Audit_Summary(AuditType, AuditID)
            If Not IsNothing(DT) Then
                hdnCenterCount.Value = DT.Rows(0)(0).ToString() + "~" + DT.Rows(0)(1).ToString() + "~" + DT.Rows(0)(2).ToString() + "~" + DT.Rows(0)(3).ToString()
            Else
                hdnCenterCount.Value = "0~0~0~0"
            End If
            cmd_viewReport.Attributes.Add("onclick", " return viewReport()")
            cmbSangam.Attributes.Add("onchange", "CenterOnChange()")
            cmbLoan.Attributes.Add("onchange", "LoanOnChange()")
            txtManagementNote.Text = AD.GetManagementNote(AuditID)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
