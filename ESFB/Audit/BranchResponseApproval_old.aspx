﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="BranchResponseApproval_old.aspx.vb" Inherits="Audit_BranchResponseApproval" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
      <style>
       .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
    <br />
    <table align="center" style="width: 70%; margin:0px auto;" >
      <tr>
            <td style="width:20%; text-align:center;" colspan="2">
                <asp:HiddenField ID="hdnQueueID" runat="server" />
                <asp:HiddenField ID="hdnMaxLevelID" runat="server" />
                <asp:HiddenField ID="hdnMaxPostID" runat="server" />
                <asp:HiddenField ID="hdnPostID" runat="server" />
              </td>
        
        </tr>
        <tr>
            <td style="width:20%; text-align:left;">
                Branch</td>
            <td style="width:80%; text-align:left;">
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="80%" ForeColor="Black">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:20%; text-align:left;">
                Observation</td>
            <td style="width:80%; text-align:left;">
                <asp:DropDownList ID="cmbObservation" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="80%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
          <tr style="height:25px;">
            <td id="lbltimeDisp" style="width:20%; text-align:center; color:red" colspan="2">
              </td>
        
        </tr>
        <tr id="RowHd1" style="display:none;">
            <td style="width:20%; text-align:center;" colspan="2" class="mainhead">
                <strong>AUDIT DETAILS</strong></td>
      
        </tr>
          <tr  id="RowDtl1" style="display:none;">
            <td style="width:20%; text-align:left;" colspan="2">
                <asp:Panel ID="pnAuditDetails" runat="server">
                </asp:Panel>
               </td>
         
        </tr>
         <tr  id="RowHd2" style="display:none;">
            <td style="width:20%; text-align:center;" colspan="2" class="mainhead">
                <strong>LOAN DETAILS</strong></td>
      
        </tr>
        <tr  id="RowDtl2" style="display:none;">
            <td style="width:20%; text-align:left;" colspan="2">
                <asp:Panel ID="pnLoanDetails" runat="server">
                </asp:Panel>
               </td>
         
        </tr>
        <tr  id="RowHd3" style="display:none;">
            <td style="width:20%; text-align:center;" colspan="2" class="mainhead">
                <strong>PREVIOUS RESPONSES</strong></td>
      
        </tr>
        <tr  id="RowDtl3" style="display:none;">
            <td style="width:20%; text-align:left;" colspan="2">
                <asp:Panel ID="pnPreviousResponse" runat="server">
                </asp:Panel>
               </td>
         
        </tr>
          <tr>
            <td style="width:20%; text-align:left;">
                &nbsp;</td>
            <td style="width:80%; text-align:left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:20%; text-align:left;">
                Remarks</td>
            <td style="width:80%; text-align:left;">
                <textarea id="txtRemarks" cols="20" name="S2" rows="3" onkeypress='return TextAreaCheck(event)'  style="width: 80%"></textarea></td>
        </tr>
          <tr>
            <td style="width:20%; text-align:left;">
                &nbsp;</td>
            <td style="width:80%; text-align:left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:20%; text-align:center;" colspan="2">
                <input id="btnReject"  style="font-family: cambria; width: 18%; cursor: pointer;"  type="button" value="REJECT" onclick="return btnReject_onclick()" />&nbsp;
                <input id="btnRecommend" style="font-family: cambria; width: 18%; cursor: pointer;"  
                    type="button" value="RECOMMEND" onclick="return btnRecommend_onclick()" />&nbsp;
                <input id="btnApprove"  style="font-family: cambria; width: 18%; cursor: pointer;" type="button" value="APPROVE" onclick="return btnApprove_onclick()" />&nbsp;
                <input id="btnHigherApproval"  
                    style="font-family: cambria; width: 18%; cursor: pointer;" type="button" 
                    value="NEED HIGHER APPROVAL" onclick="return btnHigherApproval_onclick()" />&nbsp;
                 <input id="btnSatisfied"  
                    style="font-family: cambria; width: 18%; cursor: pointer; "  
                    type="button" value="SATISFIED"  onclick="return btnSatisfied_onclick()" />&nbsp;<input id="btnClose"  
                    style="font-family: cambria; width: 18%; cursor: pointer; "  
                    type="button" value="APPROVE"   onclick="return btnClose_onclick()" />
                 <input id="btnNotSatisfied"  
                    style="font-family: cambria; width: 18%; cursor: pointer; "  
                    type="button" value="NOT SATISFIED"  onclick="return btnNotSatisfied_onclick()" />&nbsp;
                <input id="btnExit"  style="font-family: cambria; width: 18%; cursor: pointer;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
           </td>
          
        </tr>
        <tr>
            <td style="width:20%; text-align:left;">
                &nbsp;</td>
            <td style="width:80%; text-align:left;">
                &nbsp;</td>
        </tr>
     
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[
        var Remarks="";
        var QueueID=0;
        function btnExit_onclick() {
         window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function BranchOnchange() {
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
            if (BranchID != -1) {
                var Data = "2Ø" + BranchID+"Ø"+QueueID;
                ToServer(Data, 2);
            }
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("BranchResponseApproval.aspx", "_self");
            }
            else if (context == 2) {
                ComboFill(arg, "<%= cmbObservation.ClientID %>");             

            }
            else if (context == 3) {
               FillDetails(arg);             

            }
        }
        function FillDetails(Arg)
        { 
            var Dtl=Arg.split("Ø");
            var AuditDtl=Dtl[0].split("Ñ");
            var ResponseDtl=Dtl[1];
            var tab = "";
            tab += "<div style='width:100%; height:auto;  margin: 0px auto;'  class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr class='tblBody' >";           
            tab += "<td style='width:10%;text-align:center' >Period</td>";
            tab += "<td style='width:40%;text-align:left'>"+ AuditDtl[0] +"</td>";
            tab += "<td style='width:10%;text-align:left'>Auditor</td>";
            tab += "<td style='width:40%;text-align:left'>"+AuditDtl[1]+"</td>";  
            tab += "</tr>";    
            tab += "<tr  class='tblBody' >";           
            tab += "<td style='width:10%;text-align:center' >Check List</td>";
            tab += "<td style='width:40%;text-align:left' colspan='3'>"+AuditDtl[8] +"</td>";         
            tab += "</tr>";        
            tab += "</table></div>";    
            document.getElementById("<%= pnAuditDetails.ClientID %>").innerHTML=tab;
            tab = "";
            tab += "<div style='width:100%; height:auto;  margin: 0px auto;'  class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr  class='tblBody'>";           
            tab += "<td style='width:10%;text-align:center' >CenterID</td>";
            tab += "<td style='width:40%;text-align:left'>"+ AuditDtl[2] +"</td>";
            tab += "<td style='width:10%;text-align:left'>Center</td>";
            tab += "<td style='width:40%;text-align:left'>"+AuditDtl[3]+"</td>";  
            tab += "</tr>";    
            tab += "<tr  class='tblBody'>";           
            tab += "<td style='width:10%;text-align:center' >Ref No</td>";
            tab += "<td style='width:40%;text-align:left'>"+AuditDtl[4]+"</td>";
            tab += "<td style='width:10%;text-align:left'>Customer</td>";
            tab += "<td style='width:40%;text-align:left'>"+AuditDtl[5]+"</td>";  
            tab += "</tr>";    
            tab += "<tr  class='tblBody'>";           
            tab += "<td style='width:10%;text-align:center' >Loan Date</td>";
            tab += "<td style='width:40%;text-align:left'>"+AuditDtl[6]+"</td>";
            tab += "<td style='width:10%;text-align:left'>Loan Amount</td>";
            tab += "<td style='width:40%;text-align:left'>"+ parseFloat(AuditDtl[7]).toFixed(2)+"</td>";  
            tab += "</tr>";        
            tab += "</table></div>";  
            document.getElementById("<%= pnLoanDetails.ClientID %>").innerHTML=tab; 
            tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:5%;text-align:center' >#</td>"; 
            tab += "<td style='width:10%;text-align:center' >Date</td>"; 
            tab += "<td style='width:10%;text-align:left' >Level</td>";   
            tab += "<td style='width:15%;text-align:left' >User</td>";
            tab += "<td style='width:20%;text-align:left'>Status</td>";    
            tab += "<td style='width:40%;text-align:left'>Remarks</td>";  
            tab += "</tr>";
            var row_bg = 0;           
            var row = ResponseDtl.split("¥");           
            var n=0;
                for (n = 0; n <=row.length - 1; n++) {
                    col = row[n].split("µ");                    
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='tblBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='tblBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }                                                     
                     tab += "<td style='width:5%;text-align:center'>" + Math.abs(n+1)  + "</td>";                    
                     tab += "<td style='width:10%;text-align:center'>" + col[0] + "</td>";                  
                     tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                     tab += "<td style='width:15%;text-align:left'>" + col[2] + "</td>";
                     tab += "<td style='width:20%;text-align:left' >"+ col[3] +"</td>";
                     tab +="<td style='width:40%;text-align:left'>"+col[4]+"</td>";                  
                     tab += "</tr>";
                }
            
            tab += "</table></div>";           
            document.getElementById("<%= pnPreviousResponse.ClientID %>").innerHTML = tab;
              document.getElementById("RowHd1").style.display='';            
              document.getElementById("RowHd3").style.display='';
              document.getElementById("RowDtl1").style.display='';             
              document.getElementById("RowDtl3").style.display='';
              if(AuditDtl[4]=="0")
              {
                document.getElementById("RowHd2").style.display='none';
                 document.getElementById("RowDtl2").style.display='none';
              }
              else
              {
                 document.getElementById("RowHd2").style.display='';
                 document.getElementById("RowDtl2").style.display='';
              }
        }
        function ObservationOnchange()       
        {
          
            var ObservationDtl = document.getElementById("<%= cmbObservation.ClientID %>").value;
            if (ObservationDtl != "-1") {
                var Dtl=ObservationDtl.split("~");
                var msg=""             
                var Slno=Dtl[1];
                var LoanNo=Dtl[6];
                var AuditID=Dtl[5];
                var ItemID=Dtl[7];               
                Remarks=Dtl[4];
                var LevelID=Dtl[2];
                var LevelUpTo=Dtl[3];
                if(QueueID==1)
                {
                    if(LevelID==document.getElementById("<%= hdnMaxLevelID.ClientID %>").value)
                    {
                       msg="";
                    }
                    else if(Dtl[0]>2)
                    {
                       msg=Dtl[0] + " Days Remaining";
                    }
                    else if(Dtl[0]>0)
                    {
                       msg="<strong>"+ Dtl[0] + " Days Remaining"+"</strong>";
                    }
                    else
                    {
                      msg="<strong>Time Expired.Escalated to Higher Level</strong>"
                    }
                    if(LevelID<LevelUpTo)
                    {
                      document.getElementById("btnHigherApproval").style.display='none';
                      document.getElementById("btnApprove").style.display='none';
                      document.getElementById("btnRecommend").style.display='';
                    }
                    else if (LevelID==document.getElementById("<%= hdnMaxLevelID.ClientID %>").value)
                    {
                       document.getElementById("btnHigherApproval").style.display='none';
                       document.getElementById("btnApprove").style.display='none';
                       document.getElementById("btnReject").style.display='';
                       document.getElementById("btnClose").style.display='';
                       document.getElementById("btnRecommend").style.display='none';
                    }
                    else 
                    {
                      document.getElementById("btnHigherApproval").style.display='';
                      document.getElementById("btnApprove").style.display='';
                      document.getElementById("btnRecommend").style.display='none';
                    }
                }
                  document.getElementById("lbltimeDisp").innerHTML=msg;
                var Data = "3Ø" + Slno + "Ø" + LoanNo + "Ø" + AuditID + "Ø" +ItemID;
                ToServer(Data, 3);
            }
            else{
              document.getElementById("RowHd1").style.display="none";
              document.getElementById("RowHd2").style.display="none";
              document.getElementById("RowHd3").style.display="none";
              document.getElementById("RowDtl1").style.display="none";
              document.getElementById("RowDtl2").style.display="none";
              document.getElementById("RowDtl3").style.display="none";
            }
        }
function btnReject_onclick() {
     document.getElementById("btnReject").disabled =true;
     Confirmation(5);
     document.getElementById("btnReject").disabled =false;
}
function Confirmation(StatusID)
{
      var ObservationDtl = document.getElementById("<%= cmbObservation.ClientID %>").value;
       if (ObservationDtl == "-1") {
       alert("Select Observation");document.getElementById("<%= cmbObservation.ClientID %>").focus();return false;
       }
       var Remarks= document.getElementById("txtRemarks").value;
       if(Remarks=="")
       {alert("Enter Remarks");document.getElementById("txtRemarks").focus();return false;}
       var Dtl=ObservationDtl.split("~");
       var Slno=Dtl[1];
       var LevelID=Dtl[2];
       var Data = "1Ø" + Slno+"Ø"+Remarks+"Ø"+StatusID+"Ø"+LevelID;
       ToServer(Data, 1);
}
function btnRecommend_onclick() {
 document.getElementById("btnRecommend").disabled =true;
  Confirmation(3);
  document.getElementById("btnRecommend").disabled =false;
}

function btnApprove_onclick() {
 document.getElementById("btnApprove").disabled =true;
 Confirmation(4);
 document.getElementById("btnApprove").disabled =false;
}

function btnHigherApproval_onclick() {
document.getElementById("btnHigherApproval").disabled =true;
 Confirmation(6);
 document.getElementById("btnHigherApproval").disabled =false;
}

function window_onload() {
      QueueID=document.getElementById("<%= hdnQueueID.ClientID %>").value;
      if(document.getElementById("<%= hdnQueueID.ClientID %>").value==1 )
      {
         if(document.getElementById("<%= hdnMaxPostID.ClientID %>").value==document.getElementById("<%= hdnPostID.ClientID %>").value)
         {
              document.getElementById("btnHigherApproval").style.display='none';
              document.getElementById("btnApprove").style.display='none';
              document.getElementById("btnRecommend").style.display='none';
              document.getElementById("btnReject").style.display='none';
              document.getElementById("btnSatisfied").style.display='none';
              document.getElementById("btnNotSatisfied").style.display='none';
              document.getElementById("btnClose").style.display='';
          }
          else
          {
              document.getElementById("btnHigherApproval").style.display='';
              document.getElementById("btnApprove").style.display='';
              document.getElementById("btnRecommend").style.display='';
              document.getElementById("btnReject").style.display='';
              document.getElementById("btnSatisfied").style.display='none';
              document.getElementById("btnNotSatisfied").style.display='none';
              document.getElementById("btnClose").style.display='none';
          }
      }
      else{
          document.getElementById("btnHigherApproval").style.display='none';
          document.getElementById("btnApprove").style.display='none';
          document.getElementById("btnRecommend").style.display='none';
          document.getElementById("btnReject").style.display='none';
          document.getElementById("btnSatisfied").style.display='';
          document.getElementById("btnNotSatisfied").style.display='';
          document.getElementById("btnClose").style.display='none';
      }
}

function btnSatisfied_onclick() {
document.getElementById("btnSatisfied").disabled =true;
 Confirmation(8);
 document.getElementById("btnSatisfied").disabled =false;
}

function btnNotSatisfied_onclick() {
document.getElementById("btnNotSatisfied").disabled =true;
 Confirmation(9);
 document.getElementById("btnNotSatisfied").disabled =false;
}

function btnClose_onclick() {
document.getElementById("btnClose").disabled =true;
Confirmation(14);
document.getElementById("btnClose").disabled =false;
}

// ]]>
    </script>
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
</script>
</asp:Content>

