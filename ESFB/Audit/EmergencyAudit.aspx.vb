﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class EmergencyAudit
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Dim TRA_DT As DateTime
    Dim DB As New MS_SQL.Connect
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim BranchID As Integer = CInt(Data(1))
            Dim FromDt As DateTime = CDate(Data(2))
            Dim ToDt As DateTime = CDate(Data(3))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(0).Value = BranchID
                Params(1) = New SqlParameter("@AuditPrdFrom", SqlDbType.Date)
                Params(1).Value = FromDt
                Params(2) = New SqlParameter("@AuditPrdTo", SqlDbType.Date)
                Params(2).Value = ToDt
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = CInt(Session("UserID"))
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_AUDIT_EMERGENCY_AUDIT", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 243) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            DT = GF.GetQueryResult(("SELECT '-1' AS BRANCH_ID,'-----SELECT------' AS BRANCH_NAME UNION ALL SELECT CAST(A.BRANCH_ID AS VARCHAR)+'~'+CONVERT(VARCHAR(24),isnull(C.PERIOD_TO,'')),B.BRANCH_NAME  FROM AUDIT_BRANCHES A left join  " & _
                    " (SELECT BRANCH_ID,MAX(PERIOD_TO)AS PERIOD_TO FROM AUDIT_MASTER WHERE SKIP_FLAG=0 GROUP BY BRANCH_ID ) C on  A.BRANCH_ID=C.BRANCH_ID,BRANCH_MASTER B " & _
                    " WHERE A.BRANCH_ID=B.Branch_ID  order by 2"))
            GF.ComboFill(cmbBranch, DT, 0, 1)
            Me.cmbBranch.Attributes.Add("onchange", "return BranchOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
