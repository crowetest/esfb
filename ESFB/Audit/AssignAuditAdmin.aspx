﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AssignAuditAdmin.aspx.vb" Inherits="AssignAuditAdmin" EnableEventValidation ="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .style1
        {
            width: 30%;
           
        }
        .style2
     {
         text-align: left;
         width: 10%;
     }
      .style3
     {
         width: 5%;
     }
     
     .bg
     {
         background-color:#FFF;
     }
        </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function RequestOnchange() {
            if (document.getElementById("<%= hid_data.ClientID %>").value != ""){
            var Data = document.getElementById("<%= hid_data.ClientID %>").value.split("^");
            document.getElementById("<%= txtName.ClientID %>").value = Data[1];
            document.getElementById("<%= txtBranch.ClientID %>").value = Data[3];
            document.getElementById("<%= txtDepartment.ClientID %>").value = Data[5];
            document.getElementById("<%= txtDesignation.ClientID %>").value = Data[6];
            }
            
            table_fill();
            table_fill_Selected();
            
          }
    
          function RequestID() {
              var EmpID = document.getElementById("<%= txtEmpcode.ClientID %>").value;
              if (EmpID > 0) {
                  var ToData = "2Ø" + EmpID;
                  ToServer(ToData,2);
              }
          }

          function FromServer(arg, context) {
             
              if (context == 1) {
                var Data = arg.split("Ø");
                  alert(Data[1]);
                  if (Data[0] == 0) window.open("AssignAuditAdmin.aspx", "_self");
              }
              else if (context == 2) {
                 
               var Data = arg.split("~");
                 
                  if (Data[0] == 1) {
                       document.getElementById("<%= hid_dtls.ClientID %>").value = Data[2];
                       document.getElementById("<%= hid_data.ClientID %>").value = Data[1];
                      RequestOnchange();
                  }
                  else if (Data[0] == 2)
                  {
                      alert(Data[1]);
                      document.getElementById("<%= txtName.ClientID %>").value = "";
                      document.getElementById("<%= txtBranch.ClientID %>").value = "";
                      document.getElementById("<%= txtDepartment.ClientID %>").value = "";
                      document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                      document.getElementById("<%= txtEmpcode.ClientID %>").value = "";
                      document.getElementById("<%= hid_dtls.ClientID %>").value = 1;
                      table_fill();
                      table_fill_Selected();
                      document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                  }
              }
              
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnConfirm_onclick() {
           
           
            if (document.getElementById("<%= txtEmpcode.ClientID %>").value == "") 
            {
                alert("Select Employee Code");
                document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                return false;
            }
           
            
          
            if (document.getElementById("<%= txtName.ClientID %>").value == "-1") 
            {
                alert("Invalid Employee Code");
                document.getElementById("<%= hid_dtls.ClientID %>").value = 1;
                table_fill();
                table_fill_Selected();
                document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                return false;
            }
            
            var EmpID	= document.getElementById("<%= txtEmpcode.ClientID %>").value;
            FinalStringForSave();
             if (document.getElementById("<%= hid_temp.ClientID %>").value == "") {
                alert("Select Atleast one Branch");
                return false;
            }
	        var ToData = "1Ø" + EmpID + "Ø" +document.getElementById("<%= hid_temp.ClientID %>").value;
	       
            ToServer(ToData, 1);
        }
         function table_fill() 
        {   document.getElementById("<%= pnlAllBranch.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:35px; padding-top:0px;' class=mainhead><table style='width:100%;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:10%;text-align:left' class=NormalText>Select</td>";
            tab += "<td style='width:0%;text-align:left;display:none;' class=NormalText>BranchID</td>";
            tab += "<td style='width:30%;text-align:left;' class=NormalText>Region</td>";
            tab += "<td style='width:20%;text-align:left' class=NormalText>Area</td>";
            tab += "<td style='width:20%;text-align:left' class=NormalText>Branch</td>";
            tab += "<td style='width:0%;text-align:left;display:none;' class=NormalText>value</td>";
            tab += "<td style='width:0%;text-align:left;display:none;' class=NormalText>Exist Auditor</td>";
            tab += "<td style='width:20%;text-align:left;' class=NormalText>Audit Admin</td>";
            tab += "<td style='width:20%;text-align:left;display:none;' class=NormalText>ExistAuditChangeFlag</td>";
            tab += "</tr></table></div><div style='width:100%; height:213px; overflow:auto;' ><table style='width:100%;margin:0px auto;font-family:'cambria';font-size:5px;' align='center'>";
               var DataFlag =0;
               if (document.getElementById("<%= hid_dtls.ClientID %>").value == 1){
                document.getElementById("<%= hid_dtls.ClientID %>").value="µµµµ1µµµ";
                DataFlag =1;
                }
                
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
               
                for (n = 0; n <= row.length - 1; n++) {             
                    col = row[n].split("µ");                       
                    if(col[4] == 1){
                        var strstyle="style='display:none;'";
                    }
                    else{
                     var strstyle="";
                    } 
                    
                    if (col[5]==1){
                            if(strstyle==""){ strstyle = "style='background-color:#FF6666;'";
                            }
                            else{ strstyle = strstyle+'background-color:#FF6666;';
                            }
                          
                    }
                    
                            if (row_bg == 0) {
                                row_bg = 1;
                                tab += "<tr class=sub_first " + strstyle +">";
                            }
                            else {
                                row_bg = 0;
                                tab += "<tr class=sub_second " + strstyle +">";
                            }
                            i = n + 1;
                            

                            var txtid = "chk" + col[0];
                            tab += "<td style='width:10%;text-align:center;'><input id='" + txtid + "' type='checkbox' /></td>";
                            tab += "<td style='width:0%;text-align:left; display:none;' >" + col[0] + "</td>";
                            tab += "<td style='width:30%;text-align:left;font-size:9pt;'>" + col[1] + "</td>";
                            tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" + col[2] + "</td>";
                            tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" + col[3] + "</td>";
                            var txtBox = "<input id='txt" +  col[0] + "' name='txt" +  col[0] + "' value='" + col[4] +"' type='Text' style='width:80%; class='NormalText'/>";
                           tab += "<td style='width:0%;text-align:left;display:none;font-size:9pt;'>" + txtBox + "</td>";
                          
                            var txtAuditBox = "<input id='txtval" +  col[0] + "' name='txtval" +  col[0] + "' value='" + col[5] +"' type='Text' style='width:80%; class='NormalText'/>";
                            tab += "<td style='width:0%;text-align:left;display:none;font-size:9pt;'>" + txtAuditBox + "</td>";
                            tab += "<td style='width:20%;text-align:left;font-size:9pt;'>" + col[7] + "</td>";
                             var txtExistAuditBox = "<input id='txtExist" +  col[0] + "' name='txtExist" +  col[0] + "' value=22 type='Text' style='width:80%; class='NormalText'/>";
                            tab += "<td style='width:0%;text-align:left;font-size:9pt;display:none;'>" + txtExistAuditBox + "</td>";
                            tab += "</tr>";
                        }
                    
            tab += "</table></div>";
            if(DataFlag ==1){
            document.getElementById("<%= hid_dtls.ClientID %>").value = 1;
            }
            document.getElementById("<%= pnlAllBranch.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }
         function table_fill_Selected() 
        {   document.getElementById("<%= pnlSelectedBranch.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:35px;'  class=mainhead><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:10%;text-align:left' class=NormalText>Select</td>";
            tab += "<td style='width:0%;text-align:left;display:none;' class=NormalText>BranchID</td>";
            tab += "<td style='width:30%;text-align:left;' class=NormalText>Region</td>";
            tab += "<td style='width:30%;text-align:left' class=NormalText>Area</td>";
            tab += "<td style='width:30%;text-align:left' class=NormalText>Branch</td>";
            tab += "<td style='width:0%;text-align:left;display:none;' class=NormalText>value</td>";
            tab += "<td style='width:0%;text-align:left;display:none;' class=NormalText>ExistAudit</td>";
            tab += "<td style='width:20%;text-align:left;display:none;' class=NormalText>Current Auditor</td>";
            tab += "<td style='width:20%;text-align:left;display:none;' class=NormalText>ExistAuditChangeFlag</td>";
            tab += "</tr></table></div>";
            tab += "<div style='width:100%; height:213px; overflow:auto;'><table style='width:100%;margin:0px auto;font-family:'cambria'; font-size:9px;' align='center'>";
            var DataFlag =0;
               if (document.getElementById("<%= hid_dtls.ClientID %>").value == 1){
                document.getElementById("<%= hid_dtls.ClientID %>").value="µµµµ0µµµ";
                DataFlag =1;
                }
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
               
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    
                    if(col[4] ==0){
                        var strstyle="style='display:none;'";
                    }
                    else{
                     var strstyle="";
                    }
                   
                    if (col[5]==1){
                            if(strstyle==""){ strstyle = "style='background-color:#B2D0B2;'";
                            }
                            else{ strstyle = strstyle+'background-color:#FF6666;';
                            }
                          
                    }
                            if (row_bg == 0) {
                                row_bg = 1;
                                tab += "<tr class=sub_first " + strstyle +">";
                            }
                            else {
                                row_bg = 0;
                                tab += "<tr class=sub_second " + strstyle +">";
                            }
                            i = n + 1;
                            //EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time

                            var txtid = "chksel" + col[0];
                            tab += "<td style='width:10%;text-align:center'><input id='" + txtid + "' type='checkbox'  /></td>";
                            tab += "<td style='width:0%;text-align:left;display:none;' >" + col[0] + "</td>";
                            tab += "<td style='width:30%;text-align:left;font-size:9pt;'>" + col[1] + "</td>";
                            tab += "<td style='width:30%;text-align:left;font-size:9pt;'>" + col[2] + "</td>";
                            tab += "<td style='width:30%;text-align:left;font-size:9pt;'>" + col[3] + "</td>";
                            
                            var txtBox = "<input id='txtsel" +  col[0] + "' name='txtsel" +  col[0] + "' value='" + col[5] +"'  type='Text' style='width:80%; class='NormalText'/>";
                            tab += "<td style='width:0%;text-align:left;display:none;'>" + txtBox + "</td>";
                            var txtAuditBox = "<input id='txtselval" +  col[0] + "' name='txtselval" +  col[0] + "' value='" + col[6] +"' type='Text' style='width:80%; class='NormalText'/>";
                            tab += "<td style='width:0%;text-align:left;display:none;'>" + txtAuditBox + "</td>";
                            tab += "<td style='width:20%;text-align:left;display:none;'>" + col[7] + "</td>";
                            var txtExistAuditBox = "<input id='txtExistSel" +  col[0] + "' name='txtExistSel" +  col[0] + "' value=22 type='Text' style='width:80%; class='NormalText'/>";
                            tab += "<td style='width:0%;text-align:left;font-size:9pt;display:none;'>" + txtExistAuditBox + "</td>";
                           
                            tab += "</tr>";
                        }
             
                    
            tab += "</table></div>";
           if(DataFlag ==1){
            document.getElementById("<%= hid_dtls.ClientID %>").value = 1;
            }
            document.getElementById("<%= pnlSelectedBranch.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }
         function FinalStringForSave() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
                       
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");   
                   
                if (document.getElementById("txt" + col[0]).value == 1) {                         
                    document.getElementById("<%= hid_temp.ClientID %>").value +=  col[0] + "µ" +col[5] +"µ" +col[6] +"¥";
                }
            }
          return true;
        }
         function UpdateHiddenField() {
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
                      
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
           
            
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                 var txtExist= document.getElementById("txtExist" + col[0]).value;
                if(col[6]==col[7] && col[5]!=22){
                    if (txtExist==33)
                        {
                            var txtvVal= 0;
                        }
                    else{
                            var txtvVal=1;
                        }
                }
                else{
                var txtvVal= document.getElementById("txt" + col[0]).value;
                }
                
              
               
               if (n == row.length - 1){
                document.getElementById("<%= hid_temp.ClientID %>").value +=  col[0] + "µ" +  col[1] + "µ" +  col[2] + "µ"+  col[3] + "µ" + txtvVal + "µ"+  col[5]+ "µ"+  col[6] + "µ"+  col[7]+ "µ"+  col[8]+ "µ"+ txtExist;
               }
               else{
                document.getElementById("<%= hid_temp.ClientID %>").value +=  col[0] + "µ" +  col[1] + "µ" +  col[2] + "µ"+  col[3] + "µ" + txtvVal + "µ"+  col[5] + "µ"+  col[6] + "µ"+  col[7]+ "µ"+  col[8]+ "µ"+  txtExist +"¥";
               }
            }
           
           document.getElementById("<%= hid_dtls.ClientID %>").value= document.getElementById("<%= hid_temp.ClientID %>").value;  
           
           
        }
        function UpdateValue(type) {
        if(type==1){ //--------------------- Select All ------------------------//
                    row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                    document.getElementById("<%= hid_temp.ClientID %>").value="";
                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                        var txtName = "chk" + col[0];                       
                        document.getElementById("txt" + col[0]).value = 1 ;
                       
                    }
                    
                    UpdateHiddenField();
                    table_fill();
                    table_fill_Selected();
           }
        else if (type==2){//--------------------- Select one by one ------------------------//
       
                     row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                     document.getElementById("<%= hid_temp.ClientID %>").value="";
                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");

                        var txtName = "chk" + col[0];
                        if (document.getElementById("chk" + col[0]).checked == 1) {
                            
                                 document.getElementById("txt" + col[0]).value =1 ;
                            }
                           
                        
                    }

                    UpdateHiddenField();
                    table_fill();
                    table_fill_Selected();
        }
         else if (type==3){//--------------------- DeSelect one by one ------------------------//
         
                   row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                  
                    document.getElementById("<%= hid_temp.ClientID %>").value = "";
                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");

                        var txtName = "chksel" + col[0];
                        if (document.getElementById("chksel" + col[0]).checked == 1) {
                            document.getElementById("txt" + col[0]).value =0 ;
                            
                          }
                          
                    }
                   
                    UpdateHiddenField();
                    table_fill();
                    table_fill_Selected();
        }
         else if (type==4){//--------------------- DeSelect All ------------------------//
         
                    row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                    document.getElementById("<%= hid_temp.ClientID %>").value = "";
                    for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");                     
                        document.getElementById("txt" + col[0]).value = 0 ;                    

                   }
                   
                    UpdateHiddenField();
                    
                    table_fill();
                    table_fill_Selected();
        }
        return true;    
        }

        
function BtnSelectAll_onclick() {
UpdateValue(1);
}

function BtnSelect_onclick() {
UpdateValue(2);
}

function BtnDeselect_onclick() {

UpdateValue(3);
}

function BtnDeselectAll_onclick() {
UpdateValue(4);
}





    </script>
   
</head>
</html>
    <asp:HiddenField ID="hid_data" runat="server" />
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_temp" runat="server" /><br />
   <table  style="width:98%;margin: 0px auto;">
    <tr> <td class="style1"></td>
    <td class="style3"></td>
    <td class="style3">Employee Code</td>
    <td class="style3"></td>
    <td style="text-align:left;" class="style3" colspan="4">&nbsp; &nbsp;<asp:TextBox ID="txtEmpcode" class="NormalText" runat="server" Width="30%" 
                MaxLength="5"  ></asp:TextBox></td>
         
            <td class="style1">
               
            </td>
    </tr>
        <tr><td class="style1"></td>
    <td class="style3"></td>
    <td class="style3">Name</td>
    <td class="style3"></td>
         <td style="text-align:left;" class="style3" colspan="4">&nbsp; &nbsp;<asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox></td>
           
            <td class="style1">
               
            </td>

          
       </tr>
       <tr> <td class="style1"></td>
    <td class="style3"></td>
    <td class="style3">Location</td>
    <td class="style3"></td>
       <td style="text-align:left;" class="style3" colspan="4"> &nbsp; &nbsp;<asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox></td>      
            <td class="style1">
               
            </td>

       </tr>
       <tr> <td class="style1"></td>
    <td class="style3"></td>
    <td class="style3">Department</td>
    <td class="style3"></td>
       <td style="text-align:left;" class="style3" colspan="4"> &nbsp; &nbsp;<asp:TextBox ID="txtDepartment" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox></td>
             <td class="style1">
               
            </td>

       </tr>
       <tr><td class="style1"></td>
    <td class="style3"></td>
    <td class="style3">Designation</td>
    <td class="style3"></td>
       <td style="text-align:left;" class="style3" colspan="4"> &nbsp; &nbsp;<asp:TextBox ID="txtDesignation" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox></td>
            <td class="style1">
               
            </td>

       </tr>
       <tr><td colspan="9"><br /></td></tr>
       <tr  style="background-color:#EEB8A6;height:250px; outline:thin solid #6699CC;">
        <td colspan="4">
            <asp:Panel ID="pnlAllBranch" style="width:100%; text-align:right;float:right;" runat="server">
            </asp:Panel>
        </td>
        
       <td style="text-align:center;" class="style2"><input id="BtnSelectAll" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value=">>" onclick="return BtnSelectAll_onclick()" /><br />
                <input id="BtnSelect" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value=">" onclick="return BtnSelect_onclick()" onclick="return BtnSelect_onclick()" /><br /><br />
                <input id="BtnDeselect" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="<" onclick="return BtnDeselect_onclick()" /><br />
                <input id="BtnDeselectAll" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="<<" onclick="return BtnDeselectAll_onclick()" />
       </td>
            <td colspan="4">
               <asp:Panel ID="pnlSelectedBranch" style="width:100%; text-align:left;float:left;" runat="server">
            </asp:Panel>
            </td>

       </tr>
       
       <tr>
            <td style="text-align:center;" colspan="9"><br />
                 <br /><input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE" onclick="return btnConfirm_onclick()" onclick="return btnConfirm_onclick()" />&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
                 </td>
        </tr>

</table>    
 <br />   
</asp:Content>