﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  CodeFile="UpdateMasterData.aspx.vb" Inherits="UpdateMasterData" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload
        {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
        </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

            function window_onload() {
                var ID = document.getElementById("<%= cmbCategory.ClientID %>").value;
                document.getElementById("<%= hdnData.ClientID %>").value = "";
                document.getElementById("<%= hdnUpData.ClientID %>").value = "";
                if(ID == 1)
                {
                    document.getElementById("rowGroup").style.display = "";
                    document.getElementById("rowSubGroup").style.display = "none";
                    document.getElementById("rowCheckList").style.display = "none";
                }
                else if(ID == 2)
                {
                    document.getElementById("rowGroup").style.display = "none";
                    document.getElementById("rowSubGroup").style.display = "";
                    document.getElementById("rowCheckList").style.display = "none";
                }
                else if(ID == 3)
                {
                    document.getElementById("rowGroup").style.display = "none";
                    document.getElementById("rowSubGroup").style.display = "none";
                    document.getElementById("rowCheckList").style.display = "";
                }
                ToServer("1Ø" + ID, 1);
            }

            function CategoryOnChange()
            {
                var ID = document.getElementById("<%= cmbCategory.ClientID %>").value;
                document.getElementById("<%= hdnData.ClientID %>").value = "";
                document.getElementById("<%= hdnUpData.ClientID %>").value = "";
                if(ID == 1)
                {
                    document.getElementById("rowGroup").style.display = "";
                    document.getElementById("rowSubGroup").style.display = "none";
                    document.getElementById("rowCheckList").style.display = "none";
                }
                else if(ID == 2)
                {
                    document.getElementById("rowGroup").style.display = "none";
                    document.getElementById("rowSubGroup").style.display = "";
                    document.getElementById("rowCheckList").style.display = "none";
                }
                else if(ID == 3)
                {
                    document.getElementById("rowGroup").style.display = "none";
                    document.getElementById("rowSubGroup").style.display = "none";
                    document.getElementById("rowCheckList").style.display = "";
                }
                ToServer("1Ø" + ID, 1);
            }

            function btnSave_onclick() {
               document.getElementById("<%= hdnSaveValue.ClientID %>").value= "";
                if (UpdateValueData()==1){
                    return false;
                }
                var ID = document.getElementById("<%= cmbCategory.ClientID %>").value;
                var ChangeData = document.getElementById("<%= hdnSaveValue.ClientID %>").value;

                ToServer("2Ø" + ID + "Ø" + ChangeData, 2);
            }

            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                    {   
                        if(Arg != "")
                        {
                            document.getElementById("<%= hdnData.ClientID %>").value = Arg;
                            var ID = document.getElementById("<%= cmbCategory.ClientID %>").value;
                            if(ID == 1)
                            {
                                DisplayGroup();
                            }
                            else if(ID == 2)
                            {
                                DisplaySubGroup();
                            }
                            else if(ID == 3)
                            {
                                DisplayCheckList();
                            }
                        }
                        break;
                    }
                    case 2:// Confirmation
                    {
                        var Data = Arg.split("ʘ");
                        alert(Data[1]);
                        if (Data[0] == "0") window.open("UpdateMasterData.aspx", "_self");
                        break;
                    }
                }
            }

            function DisplayGroup()
            {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>GROUP</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:5%;text-align:center' ></td>";
                Tab += "<td style='width:55%;text-align:left'>Group&nbsp;Name</td>";
                Tab += "<td style='width:30%;text-align:left'>Audit Type</td>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:360px; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                if (document.getElementById("<%= hdnData.ClientID %>").value != "") {
                    var HidArg = document.getElementById("<%= hdnData.ClientID %>").value.split("¶");

                    document.getElementById("<%= hdnUpData.ClientID %>").value = HidArg[0];
                    document.getElementById("rowGroup").style.display = "";                
                    var j;
                    var Total = 0;
                    var row_bg1 = 0;
                    var SlNo = 0;

                    var HidArg1 = document.getElementById("<%= hdnUpData.ClientID %>").value.split("ʘ");
                    var RowCount = HidArg1.length - 1;
                    for (j = 0; j < RowCount; j++) {
                        if (row_bg1 == 0) {
                            row_bg1 = 1;
                            Tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg1 = 0;
                            Tab += "<tr class='sub_second';>";
                        }
                        SlNo = SlNo + 1;
                        var GroupData = HidArg1[j].split("Æ");
                        Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";                        
                        Tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSelect" + GroupData[0] + "' onchange=SelectOnClick(" + GroupData[0] + ") /></td>";
                        Tab += "<td style='width:55%;text-align:left'><input id='txtName" +  GroupData[0] + "' name='txtName" +  GroupData[0] + "' disabled=true value='" + GroupData[1] + "' type='Text' maxlength='100' style='width:100%;' class='NormalText'/></td>";

                        var select = "<select id='cmbData" + GroupData[0] + "' class='NormalText' name='cmbData" + GroupData[0] + "' disabled=true style='width:100%' >";
                        
                        var rows = HidArg[1].split("Ř"); 
                        for (a = 1; a < rows.length; a++) {
                            var cols = rows[a].split("Ĉ");
                            if (cols[0] == GroupData[2])
                                select += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                            else
                                select += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
                        }
                        select+="</select>";
                        Tab += "<td style='width:30%;text-align:center'>" + select + "</td></tr>";
                    }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div>";
                     Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'></td></tr>";
                Tab += "</table></div>";
                    document.getElementById("<%= pnGroup.ClientID %>").innerHTML = Tab;
                }
                else
                    document.getElementById("rowGroup").style.display = "none";
            }
                                
            function DisplaySubGroup()
            {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>SUB GROUP</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:10%;text-align:center'>#</td>";
                Tab += "<td style='width:5%;text-align:center' ></td>";
                Tab += "<td style='width:30%;text-align:left'>SubGroup</td>";
                Tab += "<td style='width:30%;text-align:left'>Group</td>";
                Tab += "<td style='width:25%;text-align:center'>Audit Type</td>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:360px; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                if (document.getElementById("<%= hdnData.ClientID %>").value != "") {
                    var HidArg = document.getElementById("<%= hdnData.ClientID %>").value.split("¶");

                    document.getElementById("<%= hdnUpData.ClientID %>").value = HidArg[0];
                    document.getElementById("rowSubGroup").style.display = "";                    
                    var j;
                    var Total = 0;
                    var row_bg1 = 0;
                    var SlNo = 0;

                    var HidArg1 = document.getElementById("<%= hdnUpData.ClientID %>").value.split("ʘ");
                    var RowCount = HidArg1.length - 1;
                    for (j = 0; j < RowCount; j++) {
                        if (row_bg1 == 0) {
                            row_bg1 = 1;
                            Tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg1 = 0;
                            Tab += "<tr class='sub_second';>";
                        }
                        SlNo = SlNo + 1;
                        var SubGroupData = HidArg1[j].split("Æ");
                        Tab += "<td style='width:10%;text-align:center'>" + SlNo + "</td>";
                        Tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSelectS" + SubGroupData[0] + "' onchange=SelectOnClickSub(" + SubGroupData[0] + ") /></td>";
                        Tab += "<td style='width:30%;text-align:left'><input id='txtNameS" +  SubGroupData[0] + "' name='txtNameS" +  SubGroupData[0] + "' disabled=true  value='" + SubGroupData[1] + "' type='Text' maxlength='100' style='width:100%;' class='NormalText'/></td>";

                        var select = "<select id='cmbDataS" + SubGroupData[0] + "' class='NormalText' name='cmbDataS" + SubGroupData[0] + "' disabled=true style='width:100%' >";
                        
                        var rows = HidArg[1].split("Ř"); //Group Details
                        for (a = 1; a < rows.length; a++) {
                            var cols = rows[a].split("Ĉ");
                            if (cols[0] == SubGroupData[2])
                                select += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                            else
                                select += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
                        }
                        select+="</select>";
                        Tab += "<td style='width:30%;text-align:center'>" + select + "</td>";
                        Tab += "<td style='width:25%;text-align:left'>" + SubGroupData[4] + "</td>";
                        
                        
                    }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div>";
                     Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'></td></tr>";
                Tab += "</table></div>";
                    document.getElementById("<%= pnSubGroup.ClientID %>").innerHTML = Tab;
                }
                else
                    document.getElementById("rowSubGroup").style.display = "none";
            }

            function DisplayCheckList()
            {
                var Tab = "";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'>CHECK LIST</td></tr>";
                Tab += "</table></div>";
                Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr>";
                Tab += "<td style='width:5%;text-align:center'>#</td>";
                Tab += "<td style='width:5%;text-align:center' ></td>";
                Tab += "<td style='width:30%;text-align:left'>CheckList</td>";
                Tab += "<td style='width:20%;text-align:left'>SubGroup</td>";
                Tab += "<td style='width:15%;text-align:left'>Group</td>";
                Tab += "<td style='width:10%;text-align:left'>Audit Type</td>";
                Tab += "<td style='width:5%;text-align:left'>SPM</td>";
                Tab += "<td style='width:5%;text-align:left'>Serious</td>";
                Tab += "<td style='width:5%;text-align:left'>VSerious</td></tr>";
                Tab += "</table></div>";
                Tab += "<div id='ScrollDiv' style='width:100%; height:360px; overflow-y: scroll;margin: 0px auto;' class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";

                if (document.getElementById("<%= hdnData.ClientID %>").value != "") {
                    var HidArg = document.getElementById("<%= hdnData.ClientID %>").value.split("¶");

                    document.getElementById("<%= hdnUpData.ClientID %>").value = HidArg[0];
                    document.getElementById("rowCheckList").style.display = "";                    
                    var j;
                    var Total = 0;
                    var row_bg1 = 0;
                    var SlNo = 0;
                    
                    var HidArg1 = document.getElementById("<%= hdnUpData.ClientID %>").value.split("ʘ");
                    var RowCount = HidArg1.length - 1;
                    for (j = 0; j < RowCount; j++) {
                        if (row_bg1 == 0) {
                            row_bg1 = 1;
                            Tab += "<tr class='sub_first';>";
                        }
                        else {
                            row_bg1 = 0;
                            Tab += "<tr class='sub_second';>";
                        }
                        SlNo = SlNo + 1;
                        var CheckListData = HidArg1[j].split("Æ");
                        Tab += "<td style='width:5%;text-align:center'>" + SlNo + "</td>";
                        Tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSelectC" + CheckListData[0] + "' onchange=SelectOnClickCheck(" + CheckListData[0] + ") /></td>";
                        Tab += "<td style='width:30%;text-align:left'><textarea id='txtNameC" + CheckListData[0] + "' name='txtNameC" + CheckListData[0] + "' type='Text' style='width:99%;height=200px;' class='NormalText'  disabled=true   maxlength='100' onkeypress='return TextAreaCheck(event)'  >" + CheckListData[1] + "</textarea></td>";
                        
                        var select = "<select id='cmbDataC" + CheckListData[0] + "' class='NormalText' name='cmbDataC" + CheckListData[0] + "' disabled=true style='width:100%' >";
                        
                        var rows = HidArg[1].split("Ř"); //SubGroup Details
                        for (a = 1; a < rows.length; a++) {
                            var cols = rows[a].split("Ĉ");
                            if (cols[0] == CheckListData[2])
                                select += "<option value='" + cols[0] + "' selected=true>" + cols[1] + "</option>";
                            else
                                select += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
                        }
                        select+="</select>";
                        Tab += "<td style='width:20%;text-align:center'>" + select + "</td>";
                        Tab += "<td style='width:15%;text-align:left'>" + CheckListData[3] + "</td>";
                        Tab += "<td style='width:10%;text-align:left'>" + CheckListData[4] + "</td>";
                        if (CheckListData[5] == "1")
                            Tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSPM" + CheckListData[0] + "' checked='checked' disabled=true   /></td>";
                        else                    
                            Tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSPM" + CheckListData[0] + "'  disabled=true  /></td>";
                        
                        if (CheckListData[6] == "1")
                            Tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSerious" + CheckListData[0] + "' checked='checked' disabled=true onclick='SeriousOnClick("+ CheckListData[0] +",1)' /></td>";
                        else                    
                            Tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSerious" + CheckListData[0] + "' disabled=true onclick='SeriousOnClick("+ CheckListData[0] +",1)' /></td>";
                        
                        if (CheckListData[7] == "1")
                            Tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkVSerious" + CheckListData[0] + "' checked='checked' disabled=true onclick='SeriousOnClick("+ CheckListData[0] +",2)'  /></td>";
                        else                    
                            Tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkVSerious" + CheckListData[0] + "'  disabled=true onclick='SeriousOnClick("+ CheckListData[0] +",2)' /></td>";
                        
                        Tab += "</tr>";
                    }
                    if (row_bg1 == 0)
                        Tab += "<tr style='background-color:OldLace'></tr>";
                    else
                        Tab += "<tr style='background-color:Wheat'></tr>";
                    Tab += "</table></div>";
                     Tab += "<div style='width:100%; height:20px; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
                Tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                Tab += "<tr class='sub_second';>";
                Tab += "<td style='width:5%;text-align:center'></td></tr>";
                Tab += "</table></div>";
                    document.getElementById("<%= pnCheckList.ClientID %>").innerHTML = Tab;
                }
                else
                    document.getElementById("rowCheckList").style.display = "none";
            }
            function SelectOnClick(val)
            {   
           
                 if(document.getElementById("chkSelect"+val).checked==true)    
                {    
                        document.getElementById("txtName"+val).disabled =false;                      
                        document.getElementById("txtName" + val).focus();  
                }
                else{
                        document.getElementById("txtName"+val).disabled =true; 
                        document.getElementById("cmbData"+val).disabled =true;
                        document.getElementById("txtName" + val).focus(); 
                }  
               
            }
             function SelectOnClickSub(val)
            {   
            
                 if(document.getElementById("chkSelectS"+val).checked==true)    
                {    
                        document.getElementById("txtNameS"+val).disabled =false; 
                        document.getElementById("cmbDataS"+val).disabled =false;
                        
                        
                        
                    document.getElementById("txtNameS" + val).focus();  
                }
                else{
                        document.getElementById("txtNameS"+val).disabled =true; 
                        document.getElementById("cmbDataS"+val).disabled =true;
                                               
                    document.getElementById("txtNameS" + val).focus(); 
                }  
               
            }
             function SelectOnClickCheck(val)
            {   
           
                 if(document.getElementById("chkSelectC"+val).checked==true)    
                {    
                        document.getElementById("txtNameC"+val).disabled =false; 
                        document.getElementById("cmbDataC"+val).disabled =false;
                        document.getElementById("chkSPM"+val).disabled =false;
                        document.getElementById("chkSerious"+val).disabled =false;
                        document.getElementById("chkVSerious"+val).disabled =false;
                        document.getElementById("txtNameC" + val).focus();  
                }
                else{
                        document.getElementById("txtNameC"+val).disabled =true; 
                        document.getElementById("cmbDataC"+val).disabled =true;
                        document.getElementById("chkSPM"+val).disabled =true;
                        document.getElementById("chkSerious"+val).disabled =true;
                        document.getElementById("chkVSerious"+val).disabled =true;                  
                        document.getElementById("txtNameC" + val).focus(); 
                }  
               
            }
            function UpdateValueData() {            
               
                var Newstr = "";
                var ID = document.getElementById("<%= cmbCategory.ClientID %>").value;
                var row = document.getElementById("<%= hdnUpData.ClientID %>").value.split("ʘ");
                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("Æ");
                           
                            if (ID==1){
                                if(document.getElementById("chkSelect"+ col[0]).checked == true){
                                    if(document.getElementById("txtName" + col[0]).value==""){
                                    alert("Value cannot be empty"); document.getElementById("txtName" + col[0]).focus(); return 1;
                                    }
                                    else if(document.getElementById("cmbData" + col[0]).value==""){
                                    alert("Select the value"); document.getElementById("cmbData" + col[0]).focus(); return 1;
                                    }
                                    Newstr += col[0] + "Æ" + document.getElementById("txtName" + col[0]).value + "Æ" + document.getElementById("cmbData" + col[0]).value  + "¶";
                                    
                                }
                            }
                            else  if (ID==2){
                                if(document.getElementById("chkSelectS"+ col[0]).checked == true){
                                    if(document.getElementById("txtNameS" + col[0]).value==""){
                                    alert("Value cannot be empty"); document.getElementById("txtNameS" + col[0]).focus(); return 1;
                                    }
                                    else if(document.getElementById("cmbDataS" + col[0]).value==""){
                                    alert("Select the value"); document.getElementById("cmbDataS" + col[0]).focus(); return 1;
                                    }
                                    Newstr += col[0] + "Æ" + document.getElementById("txtNameS" + col[0]).value + "Æ" + document.getElementById("cmbDataS" + col[0]).value  + "¶";
                                }
                            
                            }
                            else
                            {   
                                if(document.getElementById("chkSelectC"+ col[0]).checked == true){
                                    if(document.getElementById("txtNameC" + col[0]).value==""){
                                    alert("Value cannot be empty"); document.getElementById("txtNameC" + col[0]).focus(); return 1;
                                    }
                                    else if(document.getElementById("cmbDataC" + col[0]).value==""){
                                    alert("Select the value"); document.getElementById("cmbDataC" + col[0]).focus(); return 1;
                                    }
                                    Newstr += col[0] + "Æ" + document.getElementById("txtNameC" + col[0]).value + "Æ" + document.getElementById("cmbDataC" + col[0]).value + "Æ" + (document.getElementById("chkSPM" + col[0]).checked ==true? 1:0) + "Æ" + (document.getElementById("chkSerious" + col[0]).checked ==true ? 1:0) + "Æ" + (document.getElementById("chkVSerious" + col[0]).checked ==true ? 1:0) + "¶";
                                }
                                
                            }

                    
                        
                    }
                document.getElementById("<%= hdnSaveValue.ClientID %>").value = Newstr;
                
            }
             
            function btnExit_onclick() {
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
            
            function SeriousOnClick(val,id){
                if(id==1){
                    if(document.getElementById("chkSerious"+ val).checked == true){
                        document.getElementById("chkVSerious"+ val).checked = false;
                    }
                }
                else{
                    if(document.getElementById("chkVSerious"+ val).checked == true){
                        document.getElementById("chkSerious"+ val).checked = false;
                    }

                }
            }
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
        </script>
    </head>
    <br />
    <br />
    <table align="center" style="width: 80%; margin: 0px auto; font-family: Cambria;">
        <tr>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 15%; text-align: right;">
                Select Category
            </td>
            <td style="width: 35%; text-align: left;">
                 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:DropDownList ID="cmbCategory" Width="50%" runat="server" Font-Names="Cambria">
                    <asp:ListItem Value="1">Group</asp:ListItem>
                    <asp:ListItem Value="2">SubGroup</asp:ListItem>
                    <asp:ListItem Value="3">CheckList</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr id="rowGroup">
            <td style="text-align: left;" colspan="4">
                <asp:Panel ID="pnGroup" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr id="rowSubGroup">
            <td style="text-align: left;" colspan="4">
                <asp:Panel ID="pnSubGroup" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr id="rowCheckList">
            <td style="text-align: left;" colspan="4">
                <asp:Panel ID="pnCheckList" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 15%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
            <td style="text-align: center;" colspan="2">
                <input id="btnSave" style="font-family: cambria; width: 15%; cursor: pointer;" type="button"
                    value="SAVE" onclick="return btnSave_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;
                <input id="btnExit" onclick="return btnExit_onclick()" style="font-family: cambria;
                    width: 15%; cursor: pointer;" type="button" value="EXIT" />
            </td>
            <td style="width: 25%; text-align: left;">
                &nbsp;
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnData" runat="server" />
    <asp:HiddenField ID="hdnUpData" runat="server" />
    <asp:HiddenField ID="hdnSaveValue" runat="server" />
    <br />
</asp:Content>
