﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="UpdateGrade.aspx.vb" Inherits="Audit_UpdateGrade" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
         #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
           color:#E0E0E0;
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
            color:#036;
        }  
        .style1
        {
            height: 17px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function table_fill() 
        {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:50%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px;>";      
            tab += "<td style='width:10%;text-align:center' >#</td>";            
            tab += "<td style='width:50%;text-align:center' >Grade</td>";
            tab += "<td style='width:20%;text-align:center' >From</td>";
            tab += "<td style='width:20%;text-align:center'>To</td>";   
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:50%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";  
                 
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
          
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var TotalDays=0;
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    i = n + 1;                                      
                    tab += "<td style='width:10%;text-align:center' >" + i  + "</td>";
                    tab += "<td style='width:50%;text-align:center'>" + col[1] + "</td>";  
                   // var txtBox = "<input id='txtFrom" + col[0] + "' name='txtFrom" + col[0]  + "' type='Text' style='width:70%; text-align:right;'  maxlength='3' onkeypress='NumericCheck(event)' disabled=true  value="+col[2]+" >";                 
                    tab += "<td style='width:20%;text-align:center'>"+"<b>>= </b>"+ col[2] +"</td>"; 
                   // var txtBox = "<input id='txtTo" + col[0] + "' name='txtTo" + col[0]  + "' type='Text' style='width:70%; text-align:right;'  maxlength='3' onkeypress='NumericCheck(event)'  value="+col[3]+" >";                 
                   if(col[3] ==100){
                   tab += "<td style='width:20%;text-align:center'>"+"<b><= </b>"+ col[3] +"</td>";   
                   }
                   else{
                   tab += "<td style='width:20%;text-align:center'>"+"<b>< </b>"+ col[3] +"</td>";   
                   }
                    
                    tab += "</tr>";
                    
                }             
               
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
             document.getElementById("btnSave").disabled=true;
            //--------------------- Clearing Data ------------------------//


        }

         function table_fill_Edit() 
        {
            document.getElementById("rowEdit").style.display = '';
            document.getElementById("<%= pnEdit.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            var i=0;
            var GradeNo=0;
            var FocusControl=""
            tab += "<div style='width:50%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px;>";      
            tab += "<td style='width:10%;text-align:center' >#</td>";            
            tab += "<td style='width:50%;text-align:center' >Grade</td>";
            tab += "<td style='width:20%;text-align:center' >From</td>";
            tab += "<td style='width:20%;text-align:center'>To</td>";   
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:50%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";  
            var StartValue=0;  
            if( document.getElementById("<%= hid_value.ClientID %>").value!=""){ 
            row = document.getElementById("<%= hid_value.ClientID %>").value.split("¥");
            var TotalDays=0;
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    i = n + 1;                                      
                    tab += "<td style='width:10%;text-align:center' >" + i  + "</td>";
                    tab += "<td style='width:50%;text-align:center'>" + col[1] + "</td>";
                   // var txtBox = "<input id='txtFrom" + col[0] + "' name='txtFrom" + col[0]  + "' type='Text' style='width:70%; text-align:right;'  maxlength='3' onkeypress='NumericCheck(event)' disabled=true  value="+col[2]+" >";                 
                    tab += "<td style='width:20%;text-align:center'>"+"<b>>= </b>"+ StartValue +"</td>"; 
                    var txtBox = "<input id='txtTo" + i + "' name='txtTo" + i + "' type='Text' style='width:70%; text-align:right;'  maxlength='3' onkeypress='NumericCheck(event)'  value="+col[3]+" >";                 
                    tab += "<td style='width:20%;text-align:center'>"+"<b>< </b>"+ txtBox +"</td>";   
                    tab += "</tr>";
                    StartValue=col[3];
                    GradeNo=col[0];
                } 
                }            
             
               if(StartValue<100)
               {  
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    i = i + 1;                                      
                    tab += "<td style='width:10%;text-align:center' >" + i + "</td>";
                    var select1 = "<select id='cmbGrade" + i + "' class='NormalText' name='cmbGrade" + i + "' style='width:100%'  onchange='updateValue("+ n +")' >"; 
                    FocusControl="cmbGrade" + i;
                    var rows =document.getElementById("<%= hdnGradeDtl.ClientID %>") .value.split("Ñ");                          
                    for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("ÿ");                                  
                     if(cols[0]==(parseFloat(GradeNo)-1))
                        select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
                    else
                        select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
                     }                   
                 
                    tab += "<td style='width:50%;text-align:center'>"+ select1 +"</td>";  
                   // var txtBox = "<input id='txtFrom" + col[0] + "' name='txtFrom" + col[0]  + "' type='Text' style='width:70%; text-align:right;'  maxlength='3' onkeypress='NumericCheck(event)' disabled=true  value="+col[2]+" >";                 
                    tab += "<td style='width:20%;text-align:center'>"+"<b>>= </b>"+ StartValue +"</td>"; 
                    var txtBox = "<input id='txtTo" + i + "' name='txtTo" + i + "' type='Text' style='width:70%; text-align:right;'  maxlength='3' onkeypress='NumericCheck(event)' onkeydown='CreateNewRow(event," + i + ")'  >";                 
                    tab += "<td style='width:20%;text-align:center'>"+"<b>< </b>"+ txtBox +"</td>";   
                    tab += "</tr>";
                              
               }
               else
               {
                document.getElementById("btnSave").disabled=false;
                FocusControl="btnSave";
               }
           
            tab += "</table></div></div></div>";          
            document.getElementById("<%= pnEdit.ClientID %>").innerHTML = tab;
            document.getElementById(FocusControl).focus();
            //--------------------- Clearing Data ------------------------//


        }
       
             function CreateNewRow(e, val) {
                        var n = (window.Event) ? e.which : e.keyCode;
                        if (n == 13 || n == 9) {
                            updateValue(val);
                            table_fill_Edit();                          
                        }
                    }

         function updateValue(id)
        {       
           var Grade = document.getElementById("cmbGrade"+id).options[document.getElementById("cmbGrade"+id).selectedIndex].text;
           var MarksTo=document.getElementById("txtTo"+id).value;
           if( document.getElementById("<%= hid_value.ClientID %>").value!=""){ 
            row = document.getElementById("<%= hid_value.ClientID %>").value.split("¥");
            var StartValue=0;
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");                 
                    i = n + 1;                                      
                  if(col[1]==Grade)
                  {alert("Grade Already Selected");document.getElementById("cmbGrade"+id).focus();return false;}                   
                    StartValue=col[3];
                } 
                }            
                     
           if(parseFloat(MarksTo) <=parseFloat(StartValue))
          
           {alert("Amount should be greater than previous value");document.getElementById("txtTo"+id).focus();return false;}
            var GradeID=document.getElementById("cmbGrade"+id).value;
            var MarksFrom=0;
            if(id>1)
                MarksFrom=document.getElementById("txtTo"+(id-1)).value;           
            if (document.getElementById("<%= hid_value.ClientID %>").value!="")
                document.getElementById("<%= hid_value.ClientID %>").value+="¥"
            document.getElementById("<%= hid_value.ClientID %>").value+=GradeID+"µ"+Grade+"µ"+MarksFrom+"µ"+MarksTo;  
           
              
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btSave_onclick() {           
         var ToData = "1Ø" + document.getElementById("<%= hid_value.ClientID %>").value;                  
         ToServer(ToData, 1);
        }

        function FromServer(arg, context) {
         switch (context) {
                case 1:
                    var Data=arg.split("Ø");
                    alert(Data[1]);
                    if(Data[0]==0) {window.open("UpdateGrade.aspx","_self");}
                    break;
                    }
               }
               function EditOnClick()
               {
                table_fill_Edit();
               }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_Value" runat="server" />
                <asp:HiddenField ID="hdnGradeDtl" runat="server" />
<br />

    <table  align="center" style="width: 60%;text-align:right; margin:0px auto;">
        
        <tr> 
            <td><asp:Panel ID="pnLeaveApproveDtl" runat="server" style="width:100%; text-align:center; margin:opx auto;">
            </asp:Panel></td></tr>
        
        <tr> 
            <td>&nbsp;</td></tr>
        
        <tr> 
            <td style="text-align:center" class="style1"><div id='Button' style="width:10%; text-align:center;margin:0px auto;" onclick="EditOnClick()">EDIT</div></td></tr>
        
        <tr> 
            <td style="text-align:center" class="style1">&nbsp;</td></tr>
        
        <tr id="rowEdit" style="display:none;"> 
            <td style="text-align:center" class="style1"><asp:Panel ID="pnEdit" 
                    runat="server" style="width:100%; text-align:center; margin:opx auto;">
            </asp:Panel></td></tr>
        <tr> 
            <td style="text-align:center;">
                <br />               
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="UPDATE"  onclick="return btSave_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
            
    </table>   
    <br />  
</asp:Content>

